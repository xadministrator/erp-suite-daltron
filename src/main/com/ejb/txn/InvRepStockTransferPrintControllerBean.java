package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvStockTransfer;
import com.ejb.inv.LocalInvStockTransferHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.util.AbstractSessionBean;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepStockTransferPrintDetails;

/**
 * @ejb:bean name="InvRepStockTransferPrintControllerEJB"
 *           display-name="Used for printing stock transfer transactions"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepStockTransferPrintControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepStockTransferPrintController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepStockTransferPrintControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 * 
*/

public class InvRepStockTransferPrintControllerBean extends AbstractSessionBean{
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public ArrayList executeInvRepStockTransferPrint(ArrayList stCodeList, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvRepStockTransferPrintControllerBean executeInvRepStockTransferPrint");
        
        LocalInvStockTransferHome invStockTransferHome = null; 
        LocalInvStockTransferLineHome invStockTransferLineHome = null;
        
        ArrayList list = new ArrayList();
                
        // Initialize EJB Home
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);
            invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }        
        
        try {
        	
        	Iterator i = stCodeList.iterator();
        	
        	while (i.hasNext()) {
        		
        		Integer ST_CODE = (Integer) i.next();
        		
        		LocalInvStockTransfer invStockTransfer = null;	        	
        		
        		try {
        			
        		    invStockTransfer = invStockTransferHome.findByPrimaryKey(ST_CODE);
        			
        		} catch (FinderException ex) {
        			
        			continue;
        			
        		}	        	  	         
        		
        		// get stock transfer lines 
        		
        		Collection invStockTransferLines = invStockTransferLineHome.findByStCode(invStockTransfer.getStCode(), AD_CMPNY);
        		
        		Iterator stlIter = invStockTransferLines.iterator();
        		
        		while(stlIter.hasNext()) {
        			
        			LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)stlIter.next();
        			
        			InvRepStockTransferPrintDetails details = new InvRepStockTransferPrintDetails();
        			
        			details.setStpStDate(invStockTransfer.getStDate());
        			details.setStpStDocumentNumber(invStockTransfer.getStDocumentNumber());
        			details.setStpStReferenceNumber(invStockTransfer.getStReferenceNumber());
        			details.setStpStCreatedBy(invStockTransfer.getStCreatedBy());
        			details.setStpStApprovedRejectedBy(invStockTransfer.getStApprovedRejectedBy());
        			details.setStpStDescription(invStockTransfer.getStDescription());
        			details.setStpStlLocationTo(this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationTo()));
        			details.setStpStlQuantityDelivered(invStockTransferLine.getStlQuantityDelivered());
        			details.setStpStlUnit(invStockTransferLine.getInvUnitOfMeasure().getUomName());
        			details.setStpStlItemName(invStockTransferLine.getInvItem().getIiName());
        			details.setStpStlItemDescription(invStockTransferLine.getInvItem().getIiDescription());
        			details.setStpStlLocationFrom(this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationFrom()));
        			details.setStpStlUnitPrice(invStockTransferLine.getStlUnitCost());
        			details.setStpStlAmount(invStockTransferLine.getStlAmount());
        			details.setStpStlItemCategory(invStockTransferLine.getInvItem().getIiAdLvCategory());
        			
        			list.add(details);
        		}
        	
        	}
        	
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}      
        	
        	// sort list by locationTo
        	
        	Collections.sort(list, InvRepStockTransferPrintDetails.LocationToComparator);
        	
        	return list;        	
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("InvRepStockTransferPrintControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     details.setCmpAddress(adCompany.getCmpAddress());
	     details.setCmpCity(adCompany.getCmpCity());
	     details.setCmpCountry(adCompany.getCmpCountry());
	     details.setCmpPhone(adCompany.getCmpPhone());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	// private methods
	
	private String getInvLocNameByLocCode(Integer LOC_CODE) {
        
        Debug.print("InvRepStockTransferPrintControllerBean getInvLocNameByLocCode");
        
        LocalInvLocationHome invLocationHome = null;
        
        try {
            
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch(NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            return invLocationHome.findByPrimaryKey(LOC_CODE).getLocName();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvRepStockTransferPrintControllerBean ejbCreate");
      
    }

}
