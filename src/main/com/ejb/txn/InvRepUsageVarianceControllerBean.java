package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvPhysicalInventory;
import com.ejb.inv.LocalInvPhysicalInventoryHome;
import com.ejb.inv.LocalInvPhysicalInventoryLine;
import com.ejb.inv.LocalInvPhysicalInventoryLineHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvRepUsageVarianceDetails;

/**
 * @ejb:bean name="InvRepUsageVarianceControllerEJB"
 *           display-name="Used for generation of usage variance reports"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvRepUsageVarianceControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvRepUsageVarianceController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvRepUsageVarianceControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvRepUsageVarianceControllerBean extends AbstractSessionBean {

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvItemCategoryAll(Integer AD_CMPNY) {
		
		Debug.print("InvRepUsageVarianceControllerBean getAdLvInvItemCategoryAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV ITEM CATEGORY", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {
                    
        Debug.print("InvRepUsageVarianceControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
	               
            invLocations = invLocationHome.findLocAll(AD_CMPNY);            
        
	        if (invLocations.isEmpty()) {
	        	
	        	return null;
	        	
	        }
	        
	        Iterator i = invLocations.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();	
	        	String details = invLocation.getLocName();
	        	
	        	list.add(details);
	        	
	        }
	        
	        return list;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
            
    }
    
   /**
    * @ejb:interface-method view-type="remote"
    * @jboss:method-attributes read-only="true"
    **/
    public ArrayList executeInvRepUsageVariance(String CTGRY_NM, String LOC_NM, Date UV_DT, 
        boolean INCLD_ASSMBLY, ArrayList branchList, Integer AD_CMPNY)
    	throws GlobalNoRecordFoundException {
    	
    	Debug.print("InvRepUsageVarianceControllerBean executeInvRepUsageVariance");
    	
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvPhysicalInventoryHome invPhysicalInventoryHome = null;
    	LocalInvPhysicalInventoryLineHome invPhysicalInventoryLineHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	ArrayList list = new ArrayList();
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		invPhysicalInventoryHome = (LocalInvPhysicalInventoryHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryHome.JNDI_NAME, LocalInvPhysicalInventoryHome.class);
    		invPhysicalInventoryLineHome = (LocalInvPhysicalInventoryLineHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvPhysicalInventoryLineHome.JNDI_NAME, LocalInvPhysicalInventoryLineHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {

    	    StringBuffer jbossQl = new StringBuffer();

    	    jbossQl.append("SELECT OBJECT(bil) FROM AdBranchItemLocation bil ");
    	    
            boolean firstArgument = true;
            short ctr = 0;	      
            int criteriaSize = 0;
            
            if (CTGRY_NM != null && CTGRY_NM.length() > 0) criteriaSize++;
            if (LOC_NM != null && LOC_NM.length() > 0) criteriaSize++;
            
            Object obj[] = null;
            
            if (branchList.isEmpty()) {
            	
            	throw new GlobalNoRecordFoundException();
            	
            }
            else {
            	
            	jbossQl.append("WHERE bil.adBranch.brCode in (");
            	
            	boolean firstLoop = true;
            	
            	Iterator j = branchList.iterator();
            	
            	while(j.hasNext()) {
            		
            		if(firstLoop == false) { 
            			jbossQl.append(", "); 
            		}
            		else { 
            			firstLoop = false; 
            		}
            		
            		AdBranchDetails mdetails = (AdBranchDetails) j.next();
            		
            		jbossQl.append(mdetails.getBrCode());
            		
            	}
            	
            	jbossQl.append(") ");
            	
            	firstArgument = false;
            	
            }                    
            
            obj = new Object[criteriaSize];
            
            
            if (CTGRY_NM != null && CTGRY_NM.length() > 0) {
            	
            	if (!firstArgument) {		       	  	
            		jbossQl.append("AND ");		       	     
            	} else {		       	  	
            		firstArgument = false;
            		jbossQl.append("WHERE ");		       	  	 
            	}
				
				jbossQl.append("bil.invItemLocation.invItem.iiAdLvCategory=?" + (ctr+1) + " ");
				obj[ctr] = CTGRY_NM;
				ctr++;
				
			}	
			
			if (LOC_NM != null && LOC_NM.length() > 0) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("bil.invItemLocation.invLocation.locName=?" + (ctr+1) + " ");
				obj[ctr] = LOC_NM;
				ctr++;
				
			}

			if (!firstArgument) {		       	  	
				jbossQl.append("AND ");		       	     
			} else {		       	  	
				firstArgument = false;
				jbossQl.append("WHERE ");		       	  	 
			}
			
			if(INCLD_ASSMBLY) {
				
			    jbossQl.append("bil.bilAdCompany=" + AD_CMPNY + " ORDER BY bil.invItemLocation.invItem.iiName");	
			    
			} else {	
				
			    jbossQl.append("bil.invItemLocation.invItem.iiClass = 'Stock' AND bil.bilAdCompany=" + AD_CMPNY + " ORDER BY bil.invItemLocation.invItem.iiName");
			    
			}					

			Collection adBranchItemLocations = null;
			
			try {
			
				adBranchItemLocations = adBranchItemLocationHome.getBilByCriteria(jbossQl.toString(), obj);
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			if (adBranchItemLocations == null) {
				
				throw new GlobalNoRecordFoundException();
				
			}
    		
    		Iterator i = adBranchItemLocations.iterator();
			
    		String PREVIOUS_ITEM = null;
    		
    		while (i.hasNext()) {
    			
    			double BEG_INVENTORY = 0d;
    			double DELIVERIES = 0d;
    			double ADJUST_QTY = 0d;
    			double END_INVENTORY = 0d;
    			double ACTUAL = 0d;
    			double STANDARD = 0d;
    			double WASTAGE = 0d;
    			double VARIANCE = 0d;
    			
    			LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation) i.next();
    			
    			InvRepUsageVarianceDetails details = new InvRepUsageVarianceDetails();
    		    details.setUvItemName(adBranchItemLocation.getInvItemLocation().getInvItem().getIiName() + "-" + adBranchItemLocation.getInvItemLocation().getInvItem().getIiDescription());
    			
    		    // consolidate locations
    		    
    			if(LOC_NM == null || LOC_NM.length() == 0) {    			        			    
    			    
    			    if(PREVIOUS_ITEM != null && PREVIOUS_ITEM.equals(adBranchItemLocation.getInvItemLocation().getInvItem().getIiName())) 
    			        continue;
    			    
    			    Collection invItemsToConsolidate = null; 
    			    
    			    if(CTGRY_NM != null && CTGRY_NM.length() > 0) {
    			        
    			        invItemsToConsolidate = invItemLocationHome.findByIiNameAndIiAdLvCategory(
    			        		adBranchItemLocation.getInvItemLocation().getInvItem().getIiName(), 
    			        		adBranchItemLocation.getInvItemLocation().getInvItem().getIiAdLvCategory(), AD_CMPNY);
    			    } else {  
    			        
    			        invItemsToConsolidate = invItemLocationHome.findByIiName(
    			        		adBranchItemLocation.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);
    			        
    			    }
    			    
    			    Iterator j = invItemsToConsolidate.iterator(); 
    			    
    			    while(j.hasNext()) {
    			     
			        	//  get remaining quantity of previous date
    			            			            			        
    			        LocalInvItemLocation invItemToConsolidate = (LocalInvItemLocation) j.next();    	    			    			            			            			            			            			        			            			              
			                			        
    	    			try {
    	    				
    	    				LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanCstDateAndIiNameAndLocName(UV_DT, 
    	    				        invItemToConsolidate.getInvItem().getIiName(), 
    	    				        invItemToConsolidate.getInvLocation().getLocName(),
									adBranchItemLocation.getBilAdCompany(), AD_CMPNY);
    	    				
    	    				BEG_INVENTORY += invCosting.getCstRemainingQuantity();
    	    			
    	    			} catch (FinderException ex) {
    	    				
    	    				BEG_INVENTORY += 0d;
    	    				
    	    			}
    	    			
    	    			// get quantity received, adjust quantity, assembly quantity and quantity sold
    	    			
    	    			try {
    	    				
    	    				Collection invCostings = invCostingHome.findByCstDateAndIiNameAndLocName(
	    				        					 UV_DT, invItemToConsolidate.getInvItem().getIiName(), 
	    				        					 invItemToConsolidate.getInvLocation().getLocName(),
													 adBranchItemLocation.getBilAdCompany(), AD_CMPNY);
    	    				
    	    				Iterator l = invCostings.iterator();
    	    				
    	    				while(l.hasNext()) {
    	    					
    	    					LocalInvCosting invCosting = (LocalInvCosting) l.next();
    	    					
    	    					DELIVERIES += invCosting.getCstQuantityReceived();
    	    					ADJUST_QTY += invCosting.getCstAdjustQuantity();
    	    					STANDARD += invCosting.getCstQuantitySold() + (-invCosting.getCstAssemblyQuantity());
    	    						    	    				
    	    				}
    	    				    				    				    				
    	    			} catch (FinderException ex) {
    	    				
    	    				DELIVERIES += 0d;
    	    				ADJUST_QTY += 0d;
    	    				STANDARD += 0d;
    	    				
    	    			}
    	    			
    	    			// get ending inventory of current date
    	    			
    	    			try {
    	    			
    		    			LocalInvPhysicalInventory invPhysicalInventory = invPhysicalInventoryHome.findByPiDateAndLocNameAndBrCode(
		    			        											 UV_DT, invItemToConsolidate.getInvLocation().getLocName(),
		    			        											 adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
    		    			
    		    			if (invPhysicalInventory != null) {			
    		    				
    		    				LocalInvPhysicalInventoryLine invPhysicalInventoryLine = invPhysicalInventoryLineHome.findByPiCodeAndIlCode(
    		    						invPhysicalInventory.getPiCode(), invItemToConsolidate.getIlCode(), AD_CMPNY);
    		    				
    		    				END_INVENTORY += invPhysicalInventoryLine.getPilEndingInventory();
    		    				WASTAGE += invPhysicalInventoryLine.getPilWastage();
    		    				
    		    			}
    	    			
    	    			} catch (FinderException ex) {
    	    				
    	    				END_INVENTORY += 0d;
    	    				WASTAGE += 0d;
    	    					    	    						    					
    	    			}
    	    			    			            			        
    			    }
    			    
    			} else {
    			    
    			    //   get remaining quantity of previous date			        			        			        
	    			
	    			try {
	    				
	    				LocalInvCosting invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanCstDateAndIiNameAndLocName(UV_DT, 
	    						adBranchItemLocation.getInvItemLocation().getInvItem().getIiName(), LOC_NM, adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
	    				
	    				BEG_INVENTORY = invCosting.getCstRemainingQuantity();
	    				
	    			} catch (FinderException ex) {
	    				
	    				BEG_INVENTORY = 0d;
	    				
	    			}
	    			
	    			// get quantity received, adjust quantity, assembly quantity and quantity sold
	    			
	    			try {
	    				
	    				Collection invCostings = invCostingHome.findByCstDateAndIiNameAndLocName(UV_DT, adBranchItemLocation.getInvItemLocation().getInvItem().getIiName(), LOC_NM, adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
	    				
	    				Iterator j = invCostings.iterator();
	    				
	    				while(j.hasNext()) {
	    					
	    					LocalInvCosting invCosting = (LocalInvCosting) j.next();
	    					
	    					DELIVERIES += invCosting.getCstQuantityReceived();
	    					ADJUST_QTY += invCosting.getCstAdjustQuantity();
	    					STANDARD += invCosting.getCstQuantitySold() + (-invCosting.getCstAssemblyQuantity());
	    					
	    					
	    				}
	    				    				    				    				
	    			} catch (FinderException ex) {
	    				
	    				DELIVERIES = 0d;
	    				ADJUST_QTY = 0d;
	    				STANDARD = 0d;
	    				
	    			}
	    			
	    			// get ending inventory of current date
	    			
	    			try {
	    			
		    			LocalInvPhysicalInventory invPhysicalInventory = invPhysicalInventoryHome.findByPiDateAndLocNameAndBrCode(UV_DT, LOC_NM, adBranchItemLocation.getAdBranch().getBrCode(), AD_CMPNY);
		    			
		    			if (invPhysicalInventory != null) {			
		    				
		    				LocalInvPhysicalInventoryLine invPhysicalInventoryLine = invPhysicalInventoryLineHome.findByPiCodeAndIlCode(
		    						invPhysicalInventory.getPiCode(), adBranchItemLocation.getInvItemLocation().getIlCode(), AD_CMPNY);
		    				
		    				END_INVENTORY = invPhysicalInventoryLine.getPilEndingInventory();
		    				WASTAGE = invPhysicalInventoryLine.getPilWastage();
		    				
		    			}
	    			
	    			} catch (FinderException ex) {
	    				
	    				END_INVENTORY = 0d;
	    				WASTAGE = 0d;
	    				
	    			}
			        
		    	}    			        		    			
    			    		    			
    			ACTUAL = (BEG_INVENTORY + DELIVERIES + ADJUST_QTY) - END_INVENTORY;
    			VARIANCE = (ACTUAL - WASTAGE) - STANDARD;
    			
    			details.setUvBegInventory(BEG_INVENTORY);
    			details.setUvDeliveries(DELIVERIES);
    			details.setUvAdjustQuantity(ADJUST_QTY);
    			details.setUvEndInventory(END_INVENTORY);
    			details.setUvActual(ACTUAL);
    			details.setUvStandardUsage(STANDARD);
    			details.setUvWastage(WASTAGE);
    			details.setUvVariance(VARIANCE);
    			
    			list.add(details);  
    			
    			PREVIOUS_ITEM = adBranchItemLocation.getInvItemLocation().getInvItem().getIiName();
    			
    		}
    		
    		if (list.isEmpty()) {
    			
    			throw new GlobalNoRecordFoundException();
    			
    		}		 
    		
    		return list; 	  
    		
    	} catch (GlobalNoRecordFoundException ex) {
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    }			

	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("InvFindItemLocationControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("InvRepUsageVarianceControllerBean ejbCreate");
      
    }
    
}

