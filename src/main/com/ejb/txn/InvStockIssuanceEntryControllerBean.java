package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBillOfMaterial;
import com.ejb.inv.LocalInvBillOfMaterialHome;
import com.ejb.inv.LocalInvBuildOrderLine;
import com.ejb.inv.LocalInvBuildOrderLineHome;
import com.ejb.inv.LocalInvBuildOrderStock;
import com.ejb.inv.LocalInvBuildOrderStockHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvStockIssuance;
import com.ejb.inv.LocalInvStockIssuanceHome;
import com.ejb.inv.LocalInvStockIssuanceLine;
import com.ejb.inv.LocalInvStockIssuanceLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBuildOrderStockDetails;
import com.util.InvModStockIssuanceDetails;
import com.util.InvModStockIssuanceLineDetails;
import com.util.InvModUnitOfMeasureDetails;
import com.util.InvStockIssuanceDetails;

/**
 * @ejb:bean name="InvStockIssuanceEntryControllerEJB" display-name="used for
 *           issuing of stocks" type="Stateless" view-type="remote"
 *           jndi-name="ejb/InvStockIssuanceEntryControllerEJB"
 * 
 * @ejb:interface remote-class="com.ejb.txn.InvStockIssuanceEntryController"
 *                extends="javax.ejb.EJBObject"
 * 
 * @ejb:home remote-class="com.ejb.txn.InvStockIssuanceEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 * 
 * @ejb:transaction type="Required"
 * 
 * @ejb:security-role-ref role-name="invuser" role-link="invuserlink"
 * 
 * @ejb:permission role-name="invuser"
 * 
 */

public class InvStockIssuanceEntryControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     */
    public ArrayList getInvLocAll(Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invLocationHome = (LocalInvLocationHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            invLocations = invLocationHome.findLocAll(AD_CMPNY);
            
            if (invLocations.isEmpty()) {
                
                return null;
                
            }
            
            Iterator i = invLocations.iterator();
            
            while (i.hasNext()) {
                
                LocalInvLocation invLocation = (LocalInvLocation) i.next();
                String details = invLocation.getLocName();
                
                list.add(details);
                
            }
            
            return list;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     */
    public InvModStockIssuanceDetails getInvSiBySiCode(Integer SI_CODE,
            Integer AD_CMPNY) throws GlobalNoRecordFoundException {
        
        Debug.print("InvStockIssuanceEntryControllerBean getInvSiBySiCode");
        
        LocalInvStockIssuanceHome invStockIssuanceHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invStockIssuanceHome = (LocalInvStockIssuanceHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvStockIssuance invStockIssuance = null;
            
            try {
                
                invStockIssuance = invStockIssuanceHome.findByPrimaryKey(SI_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            InvModStockIssuanceDetails details = new InvModStockIssuanceDetails();
            details.setSiCode(invStockIssuance.getSiCode());
            details.setSiDate(invStockIssuance.getSiDate());
            details.setSiDocumentNumber(invStockIssuance.getSiDocumentNumber());
            details.setSiReferenceNumber(invStockIssuance.getSiReferenceNumber());
            details.setSiDescription(invStockIssuance.getSiDescription());
            details.setSiVoid(invStockIssuance.getSiVoid());
            details.setSiApprovalStatus(invStockIssuance.getSiApprovalStatus());
            details.setSiPosted(invStockIssuance.getSiPosted());
            details.setSiCreatedBy(invStockIssuance.getSiCreatedBy());
            details.setSiDateCreated(invStockIssuance.getSiDateCreated());
            details.setSiLastModifiedBy(invStockIssuance.getSiLastModifiedBy());
            details.setSiDateLastModified(invStockIssuance.getSiDateLastModified());
            details.setSiApprovedRejectedBy(invStockIssuance.getSiApprovedRejectedBy());
            details.setSiDateApprovedRejected(invStockIssuance.getSiDateApprovedRejected());
            details.setSiPostedBy(invStockIssuance.getSiPostedBy());
            details.setSiDatePosted(invStockIssuance.getSiDatePosted());
            details.setSiReasonForRejection(invStockIssuance.getSiReasonForRejection());
            
            ArrayList silList = new ArrayList();
            
            Collection invStockIssuanceLines = invStockIssuance
            .getInvStockIssuanceLines();
            
            Iterator i = invStockIssuanceLines.iterator();
            
            while (i.hasNext()) {
                
                LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine) i
                .next();
                
                InvModStockIssuanceLineDetails mdetails = new InvModStockIssuanceLineDetails();
                
                mdetails.setSilIlLocationName(invStockIssuanceLine.getInvItemLocation().getInvLocation().getLocName());
                mdetails.setSilIlIiName(invStockIssuanceLine.getInvItemLocation().getInvItem().getIiName());
                mdetails.setSilIlIiUomName(invStockIssuanceLine.getInvUnitOfMeasure().getUomName());
                mdetails.setSilBosCode(invStockIssuanceLine.getInvBuildOrderStock().getBosCode());
                mdetails.setSilIssueQuantity(invStockIssuanceLine.getSilIssueQuantity());
                
                if (details.getSiSilBolCode() == null) {
                    
                    details.setSiSilBolCode(invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getBolCode());
                    
                }
                
                if (details.getSiSilIlIiName() == null) {
                    
                    details.setSiSilIlIiName(invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiName());
                    
                }
                
                if (details.getSiSilIlLocationName() == null) {
                    
                    details.setSiSilIlLocationName(invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvLocation().getLocName());
                    
                }
                
                if (details.getSiSilBosBolBorDocumentNumber() == null) {
                    
                    details.setSiSilBosBolBorDocumentNumber(invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvBuildOrder().getBorDocumentNumber());
                    
                }
                
                mdetails.setSilBosQtyrequired(this.getQuantityRequiredByIiNameAndUomNameAndBosCode(
                        mdetails.getSilIlIiName(), mdetails.getSilIlIiUomName(), mdetails.getSilBosCode(),
                        AD_CMPNY));
                
                mdetails.setSilBosQtyIssued(this.getQuantityIssuedByIiNameAndUomNameAndBosCode(
                        mdetails.getSilIlIiName(), mdetails.getSilIlIiUomName(), mdetails.getSilBosCode(),
                        AD_CMPNY));
                
                mdetails.setSilUnitCost(invStockIssuanceLine.getSilUnitCost());
                
                mdetails.setSilIlIiDescription(invStockIssuanceLine.getInvItemLocation().getInvItem().getIiDescription());
                
                silList.add(mdetails);
                
            }
            
            details.setSiSilList(silList);
            
            return details;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            Debug.printStackTrace(ex);
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    
    /**
     * @ejb:interface-method view-type="remote"
     */
    public Integer saveInvSiEntry(InvStockIssuanceDetails details,
            ArrayList silList, boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalDocumentNumberNotUniqueException,
    GlobalRecordAlreadyDeletedException,
    GlobalTransactionAlreadyApprovedException,
    GlobalTransactionAlreadyPendingException,
    GlobalTransactionAlreadyPostedException,
    GlobalTransactionAlreadyVoidException,
    GlobalInvItemLocationNotFoundException,
    GlJREffectiveDateNoPeriodExistException,
    GlJREffectiveDatePeriodClosedException,
    GlobalJournalNotBalanceException, 
    GlobalInventoryDateException,
    GlobalBranchAccountNumberInvalidException,
	AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvStockIssuanceEntryControllerBean saveInvSiEntry");
        
        LocalInvStockIssuanceHome invStockIssuanceHome = null;
        LocalInvStockIssuanceLineHome invStockIssuanceLineHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
        LocalInvBuildOrderStockHome invBuildOrderStockHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;		
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;    	
        LocalInvStockIssuance invStockIssuance = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        
        
        // Initialize EJB Home
        
        try {
            
            invStockIssuanceHome = (LocalInvStockIssuanceHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);
            invStockIssuanceLineHome = (LocalInvStockIssuanceLineHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvStockIssuanceLineHome.JNDI_NAME, LocalInvStockIssuanceLineHome.class);
            adApprovalHome = (LocalAdApprovalHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME,	LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invItemLocationHome = (LocalInvItemLocationHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME,LocalAdDocumentSequenceAssignmentHome.class);
            invBuildOrderStockHome = (LocalInvBuildOrderStockHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderStockHome.JNDI_NAME,LocalInvBuildOrderStockHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME,LocalInvUnitOfMeasureHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            // validate if stock issuance is already deleted
            
            try {
                
                if (details.getSiCode() != null) {
                    
                    invStockIssuance = invStockIssuanceHome.findByPrimaryKey(details.getSiCode());
                    
                }
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
                
            }
            
            // validate if stockissuance is already posted, void, approved or
            // pending
            
            if (details.getSiCode() != null) {
                
                if (invStockIssuance.getSiApprovalStatus() != null) {
                    
                    if (invStockIssuance.getSiApprovalStatus().equals("APPROVED")
                            || invStockIssuance.getSiApprovalStatus().equals("N/A")) {
                        
                        throw new GlobalTransactionAlreadyApprovedException();
                        
                    } else if (invStockIssuance.getSiApprovalStatus().equals("PENDING")) {
                        
                        throw new GlobalTransactionAlreadyPendingException();
                        
                    }
                    
                }
                
                if (invStockIssuance.getSiPosted() == EJBCommon.TRUE) {
                    
                    throw new GlobalTransactionAlreadyPostedException();
                    
                }
                
            }
            
            // invoice void
            
            if (details.getSiCode() != null&& details.getSiVoid() == EJBCommon.TRUE
                    && invStockIssuance.getSiPosted() == EJBCommon.FALSE) {
                
                invStockIssuance.setSiVoid(EJBCommon.TRUE);
                invStockIssuance.setSiLastModifiedBy(details.getSiLastModifiedBy());
                invStockIssuance.setSiDateLastModified(details.getSiDateLastModified());
                
                return invStockIssuance.getSiCode();
                
            }
            
            LocalInvStockIssuance invExistingStockIssuance = null;
            
            try {
                
                invExistingStockIssuance = invStockIssuanceHome.findBySiDocumentNumberAndBrCode(details.getSiDocumentNumber(), AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
            }
            
            // validate if document number is unique document number is
            // automatic then set next sequence
            
            if (details.getSiCode() == null) {
                
                
                LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
                LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
                
                if (invExistingStockIssuance != null) {
                    
                    throw new GlobalDocumentNumberNotUniqueException();
                    
                }
                
                try {
                    
                    adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV STOCK ISSUANCE", AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                try {
                    
                    adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A'
                    && (details.getSiDocumentNumber() == null || details.getSiDocumentNumber().trim().length() == 0)) {
                    
                    while (true) {
                        
                        if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
                            
                            try {
                                
                                invStockIssuanceHome.findBySiDocumentNumberAndBrCode(
                                        adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);
                                
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                                
                            } catch (FinderException ex) {
                                
                                details.setSiDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
                                adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));
                                break;
                                
                            }
                            
                        } else {
                            
                            try {
                                
                                invStockIssuanceHome.findBySiDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
                                
                            } catch (FinderException ex) {
                                
                                details.setSiDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
                                adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
                                break;
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            } else {
                
                if (invExistingStockIssuance != null && !invExistingStockIssuance.getSiCode().equals(
                        details.getSiCode())) {
                    
                    throw new GlobalDocumentNumberNotUniqueException();
                    
                }
                
                if (invExistingStockIssuance.getSiDocumentNumber() != details.getSiDocumentNumber()
                        && (details.getSiDocumentNumber() == null || details.getSiDocumentNumber().trim().length() == 0)) {
                    
                    details.setSiDocumentNumber(invExistingStockIssuance.getSiDocumentNumber());
                    
                }
                
            }
            
            // used in checking if invoice should re-generate distribution
            // records and re-calculate taxes
            boolean isRecalculate = true;
            
            // create stock issuance
            
            if (details.getSiCode() == null) {
                
                invStockIssuance = invStockIssuanceHome.create(details.getSiDate(), 
                        details.getSiDocumentNumber(), details.getSiReferenceNumber(), details.getSiDescription(),
                        EJBCommon.FALSE, details.getSiApprovalStatus(), EJBCommon.FALSE, details.getSiCreatedBy(), 
                        details.getSiDateCreated(), details.getSiLastModifiedBy(), details.getSiDateLastModified(), 
                        null, null, null, null, null, AD_BRNCH, AD_CMPNY);
                
            } else {
                
                // check if critical fields are changed
                
                if (silList.size() != invStockIssuance.getInvStockIssuanceLines().size() ||
                        !(invStockIssuance.getSiDate().equals(details.getSiDate()))) {
                    
                    isRecalculate = true;
                    
                } else if (silList.size() == invStockIssuance.getInvStockIssuanceLines().size()) {
                    
                    Iterator silIter = invStockIssuance.getInvStockIssuanceLines().iterator();
                    Iterator silListIter = silList.iterator();
                    
                    while (silIter.hasNext()) {
                        
                        LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine) silIter.next();
                        
                        InvModStockIssuanceLineDetails mdetails = (InvModStockIssuanceLineDetails) silListIter.next();
                        
                        if (!invStockIssuanceLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getSilIlLocationName()) || 
                                !invStockIssuanceLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getSilIlIiName()) || 
                                !invStockIssuanceLine.getInvBuildOrderStock().getBosCode().equals(mdetails.getSilBosCode()) || 
                                invStockIssuanceLine.getSilIssueQuantity() != mdetails.getSilIssueQuantity() ||
                                invStockIssuanceLine.getSilUnitCost() != mdetails.getSilUnitCost()) {
                            
                            isRecalculate = true;
                            break;
                            
                        }
                        
                        isRecalculate = false;
                    }
                    
                } else {
                    
                    isRecalculate = true;
                    
                }
                
                invStockIssuance.setSiDate(details.getSiDate());
                invStockIssuance.setSiDocumentNumber(details.getSiDocumentNumber());
                invStockIssuance.setSiReferenceNumber(details.getSiReferenceNumber());
                invStockIssuance.setSiDescription(details.getSiDescription());
                invStockIssuance.setSiApprovalStatus(details.getSiApprovalStatus());
                invStockIssuance.setSiPosted(details.getSiPosted());
                invStockIssuance.setSiCreatedBy(details.getSiCreatedBy());
                invStockIssuance.setSiDateCreated(details.getSiDateCreated());
                invStockIssuance.setSiLastModifiedBy(details.getSiLastModifiedBy());
                invStockIssuance.setSiDateLastModified(details.getSiDateLastModified());
                
            }
            
            if (isRecalculate) {
                
                // delete lines
                Collection invStockIssuanceLines = invStockIssuance.getInvStockIssuanceLines();
                
                Iterator i = invStockIssuanceLines.iterator();
                
                while (i.hasNext()) {
                    
                    LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine) i.next();
                    
                    LocalInvItemLocation invItemLocation = invStockIssuanceLine.getInvItemLocation();
                    
                    // conversion
                    double convertedQuantity = this.convertByUomFromAndBomItemAndQuantity(
                            invStockIssuanceLine.getInvUnitOfMeasure(), 
                            invStockIssuanceLine.getInvItemLocation().getInvItem(),
                            invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                            Math.abs(invStockIssuanceLine.getSilIssueQuantity()), false, AD_CMPNY);
                    
                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
                    
                    invStockIssuanceLine.getInvBuildOrderStock().setBosLock(EJBCommon.FALSE);
                    
                    i.remove();
                    invStockIssuanceLine.remove();
                    
                }
                
                // remove all distribution records
                
                Collection invDistributionRecords = invStockIssuance.getInvDistributionRecords();
                
                i = invDistributionRecords.iterator();
                
                while (i.hasNext()) {
                    
                    LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) i.next();
                    
                    i.remove();
                    
                    invDistributionRecord.remove();
                    
                }
                
                int line = 0;
                
                // create lines
                i = silList.iterator();
                
                while (i.hasNext()) {
                    
                    line++;
                    
                    InvModStockIssuanceLineDetails mdetails = (InvModStockIssuanceLineDetails) i.next();
                    
                    LocalInvStockIssuanceLine invStockIssuanceLine = invStockIssuanceLineHome.create(mdetails.getSilIssueQuantity(), 0d, 0d, 0d, AD_CMPNY);

                    // set stock issuance line unit cost
                    double UNIT_COST = this.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
                            mdetails.getSilIlIiName(), mdetails.getSilIlLocationName(), 
                            mdetails.getSilIlIiUomName(), details.getSiDate(), AD_BRNCH, AD_CMPNY);
                    invStockIssuanceLine.setSilUnitCost(UNIT_COST);
                    
                    invStockIssuance.addInvStockIssuanceLine(invStockIssuanceLine);
                    
                    LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
                            mdetails.getSilIlIiUomName(), AD_CMPNY);
                    
                    invUnitOfMeasure.addInvStockIssuanceLine(invStockIssuanceLine);
                    
                    // find item location by loc name & item name and add stock
                    // issuance line
                    
                    LocalInvItemLocation invItemLocation = null;
                    
                    try {
                        
                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(mdetails.getSilIlLocationName(), mdetails.getSilIlIiName(), AD_CMPNY);
                        
                    } catch (FinderException ex) {
                        
                        throw new GlobalInvItemLocationNotFoundException(String.valueOf(line));
                        
                    }
                    
                    invItemLocation.addInvStockIssuanceLine(invStockIssuanceLine);
                    
                    // add build order stock
                    LocalInvBuildOrderStock invBuildOrderStock = invBuildOrderStockHome.findByPrimaryKey(mdetails.getSilBosCode());
                    invBuildOrderStock.setBosLock(EJBCommon.TRUE);
                    
                    invBuildOrderStock.addInvStockIssuanceLine(invStockIssuanceLine);
                    
                    // start date validation
                    
                    Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            invStockIssuance.getSiDate(), invItemLocation.getInvItem().getIiName(),
                            invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    if(!invNegTxnCosting.isEmpty()) { 
                        throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }
                    
                    // get costing amount
                    
                    double COST = 0d;
                    
                    try {
                        
                        LocalInvCosting invCosting = 
                            invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                                    details.getSiDate(), invItemLocation.getInvItem().getIiName(),
                                    invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                        
                        COST = Math.abs(invCosting.getCstRemainingValue()
                                / invCosting.getCstRemainingQuantity());
                        
                    } catch (FinderException ex) {
                        
                        COST = invItemLocation.getInvItem().getIiUnitCost();
                        
                    }
                    
                    COST = this.convertCostByUom(invItemLocation.getInvItem().getIiName(),
                            invStockIssuanceLine.getInvUnitOfMeasure().getUomName(), COST, true, AD_CMPNY);
                    
                    LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                    
                    double AMOUNT = EJBCommon.roundIt(invStockIssuanceLine.getSilIssueQuantity()
                            * COST, adCompany.getGlFunctionalCurrency().getFcPrecision());
                    
                    // create distribution records
                    
                    // get assembly item location
                    
                    LocalInvItemLocation invAssemblyLocation = invBuildOrderStock.getInvBuildOrderLine().getInvItemLocation();
                    
                    // check for branch mapping
                    
                    LocalAdBranchItemLocation adBranchItemLocation = null;
                    LocalAdBranchItemLocation adBranchAssemblyLocation = null;
                    
                    try {
                        
                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invStockIssuanceLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
                        adBranchAssemblyLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invAssemblyLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex){
                        
                    }
                    
                    if (adBranchItemLocation==null){
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, AMOUNT,
                                invItemLocation.getIlGlCoaInventoryAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    } else {
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, AMOUNT,
                                adBranchItemLocation.getBilCoaGlInventoryAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    }
                    
                    if (adBranchAssemblyLocation==null){
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "WIP", EJBCommon.TRUE, AMOUNT,
                                invAssemblyLocation.getIlGlCoaWipAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    } else {
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "WIP", EJBCommon.TRUE, AMOUNT,
                                adBranchAssemblyLocation.getBilCoaGlWipAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    }
                    
                    // commit inventory
                    
                    double convertedQuantity = this.convertByUomFromAndBomItemAndQuantity(
                            invStockIssuanceLine.getInvUnitOfMeasure(), 
                            invStockIssuanceLine.getInvItemLocation().getInvItem(),
                            invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                            Math.abs(invStockIssuanceLine.getSilIssueQuantity()), false, AD_CMPNY);
                    
                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);
                    
                }
                
            }
            
            if (!isDraft) {
                
                invStockIssuance.setSiApprovalStatus("N/A");
                this.executeInvSiPost(invStockIssuance.getSiCode(), details.getSiLastModifiedBy(), AD_BRNCH, AD_CMPNY);
                
            }
            
            return invStockIssuance.getSiCode();
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        }catch (GlobalTransactionAlreadyApprovedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPendingException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPostedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalDocumentNumberNotUniqueException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInvItemLocationNotFoundException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDatePeriodClosedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalJournalNotBalanceException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     */
    public void deleteInvSiEntry(Integer SI_CODE, Integer AD_CMPNY)
    throws GlobalRecordAlreadyDeletedException {
        
        Debug.print("InvStockIssuanceEntryControllerBean deleteInvSiEntry");
        
        LocalInvStockIssuanceHome invStockIssuanceHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invStockIssuanceHome = (LocalInvStockIssuanceHome) EJBHomeFactory.
            lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME, LocalInvStockIssuanceHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvStockIssuance invStockIssuance = invStockIssuanceHome.findByPrimaryKey(SI_CODE);
            
            Collection invStockIssuanceLines = invStockIssuance.getInvStockIssuanceLines();
            
            Iterator j = invStockIssuanceLines.iterator();
            
            while (j.hasNext()) {
                
                LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine) j.next();
                
                invStockIssuanceLine.getInvBuildOrderStock().setBosLock(EJBCommon.FALSE);
                
                double convertedQuantity = this.convertByUomFromAndBomItemAndQuantity(
                        invStockIssuanceLine.getInvUnitOfMeasure(), 
                        invStockIssuanceLine.getInvItemLocation().getInvItem(),
                        invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                        Math.abs(invStockIssuanceLine.getSilIssueQuantity()), true, AD_CMPNY);
                
                invStockIssuanceLine.getInvItemLocation().setIlCommittedQuantity(invStockIssuanceLine.getInvItemLocation().getIlCommittedQuantity()
                        - convertedQuantity);
                
            }
            
            invStockIssuance.remove();
            
        } catch (FinderException ex) {
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvStockIssuanceEntryControllerBean getGlFcPrecisionUnit");
       
        LocalAdCompanyHome adCompanyHome = null;
             
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }
     
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     */
    public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean getInvGpQuantityPrecisionUnit");
        
        LocalAdPreferenceHome adPreferenceHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
            .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,LocalAdPreferenceHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
            return adPreference.getPrfInvQuantityPrecisionUnit();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public ArrayList getInvIncompleteBosByBolCode(Integer BOL_CODE, Integer AD_BRNCH, Integer AD_CMPNY) 
    throws GlobalNoRecordFoundException {
        
        Debug.print("InvStockIssuanceEntryControllerBean getInvBosByBorDocumentNumberAndLocName");
        
        LocalInvBuildOrderLineHome invBuildOrderLineHome = null;
        LocalInvBuildOrderStockHome invBuildOrderStockHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        
        try {
            
            invBuildOrderLineHome = (LocalInvBuildOrderLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderLineHome.JNDI_NAME, LocalInvBuildOrderLineHome.class);
            invBuildOrderStockHome = (LocalInvBuildOrderStockHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderStockHome.JNDI_NAME, LocalInvBuildOrderStockHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvBuildOrderLine invBuildOrderLine = null;
            
            try{
                
                invBuildOrderLine = invBuildOrderLineHome.findByPrimaryKey(BOL_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }  
            
            Collection invBuildOrderStocks = null;
            
            try{
                invBuildOrderStocks = invBuildOrderStockHome.findIncompleteBosByBolCode(BOL_CODE, AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
                
            }
            
            ArrayList bosList = new ArrayList();
            
            Iterator i = invBuildOrderStocks.iterator();
            
            while (i.hasNext()) {
                
                LocalInvBuildOrderStock invBuildOrderStock = (LocalInvBuildOrderStock)i.next();
                
                InvModBuildOrderStockDetails mdetails = new InvModBuildOrderStockDetails();
                
                mdetails.setBosCode(invBuildOrderStock.getBosCode());
                mdetails.setBosQuantityRequired(invBuildOrderStock.getBosQuantityRequired());
                mdetails.setBosQuantityIssued(invBuildOrderStock.getBosQuantityIssued());
                mdetails.setBosIiName(invBuildOrderStock.getInvItem().getIiName());
                mdetails.setBosIiDescription(invBuildOrderStock.getInvItem().getIiDescription());
                
                // get bom uom
                LocalInvBillOfMaterial invBillOfMaterial = invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(
                        invBuildOrderStock.getInvItem().getIiName(),
                        invBuildOrderLine.getInvItemLocation().getInvItem().getIiName(),
                        AD_CMPNY);
                
                mdetails.setBosIiUomName(invBillOfMaterial.getInvUnitOfMeasure().getUomName());
                
                // unit cost
                double unitCost = this.convertCostByUom(invBuildOrderStock.getInvItem().getIiName(),
                		invBillOfMaterial.getInvUnitOfMeasure().getUomName(), invBuildOrderStock.getInvItem().getIiUnitCost(), true, AD_CMPNY);
                
                mdetails.setBosIiUnitCost(unitCost);
                
                Collection invItemLocations = null;
                
                try {
                    
                    invItemLocations = invItemLocationHome.findByIiNameAndAdBranch(
                            invBuildOrderStock.getInvItem().getIiName(), AD_BRNCH, AD_CMPNY);
                    
                }catch(FinderException ex) {
                    
                }
                
                if(!invItemLocations.isEmpty()) {
                    
                    bosList.add(mdetails);
                    
                }
                
            }
            
            return bosList;
            
        } catch (GlobalNoRecordFoundException ex) {
            
            Debug.printStackTrace(ex);
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public double getQuantityRequiredByIiNameAndUomNameAndBosCode(String II_NM, String UOM_NM, Integer BOS_CODE, Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryBean getQuantityRequiredByIiNameAndUomNameAndBosCode");
        
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvBuildOrderStockHome invBuildOrderStockHome = null;
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invBuildOrderStockHome = (LocalInvBuildOrderStockHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderStockHome.JNDI_NAME, LocalInvBuildOrderStockHome.class);
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            double BOS_QTY_RQRD = 0d;
            
            LocalInvBuildOrderStock invBuildOrderStock = null;
            
            try {
                
                invBuildOrderStock = invBuildOrderStockHome.findByPrimaryKey(BOS_CODE);
                
                BOS_QTY_RQRD = invBuildOrderStock.getBosQuantityRequired();
                
            } catch (FinderException ex) {
                
            }
            
            LocalInvBillOfMaterial invBillOfMaterial = invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(
                    invBuildOrderStock.getInvItem().getIiName(),
                    invBuildOrderStock.getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiName(),
                    AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultItemUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invBillOfMaterial.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(BOS_QTY_RQRD * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultItemUomConversion.getUmcConversionFactor(), this.getInvGpQuantityPrecisionUnit(AD_CMPNY));
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public double getQuantityIssuedByIiNameAndUomNameAndBosCode(String II_NM, String UOM_NM, Integer BOS_CODE, Integer AD_CMPNY) {
        
        Debug.print("InvSrockIssuanceEntryBean getQuantityIssuedByIiNameAndUomNameAndBosCode");
        
        
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvBuildOrderStockHome invBuildOrderStockHome = null;
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        
        // Initialize EJB Home
        
        try {            
            
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invBuildOrderStockHome = (LocalInvBuildOrderStockHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBuildOrderStockHome.JNDI_NAME, LocalInvBuildOrderStockHome.class);
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            double BOS_QTY_ISSUED = 0d;
            
            LocalInvBuildOrderStock invBuildOrderStock = null;
            try {
                
                invBuildOrderStock = invBuildOrderStockHome.findByPrimaryKey(BOS_CODE);
                
                BOS_QTY_ISSUED = invBuildOrderStock.getBosQuantityIssued();
                
            } catch (FinderException ex) {
                
            }
            
            LocalInvBillOfMaterial invBillOfMaterial = invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(
                    invBuildOrderStock.getInvItem().getIiName(),
                    invBuildOrderStock.getInvBuildOrderLine().getInvItemLocation().getInvItem().getIiName(),
                    AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultItemUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invBillOfMaterial.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(BOS_QTY_ISSUED * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultItemUomConversion.getUmcConversionFactor(), this.getInvGpQuantityPrecisionUnit(AD_CMPNY));
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean getInvUomByIiName");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
            
            LocalInvItem invItem = null;
            LocalInvUnitOfMeasure invItemUnitOfMeasure = null;    	
            
            invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);        	
            invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();
            
            Collection invUnitOfMeasures = invUnitOfMeasureHome.findByUomAdLvClass(
                    invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY);
            
            Iterator i = invUnitOfMeasures.iterator();
            
            while (i.hasNext()) {
                
                LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
                
                InvModUnitOfMeasureDetails mdetails = new InvModUnitOfMeasureDetails();
                
                mdetails.setUomName(invUnitOfMeasure.getUomName());
                
                if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {
                    
                    mdetails.setDefault(true);
                    
                }
                
                list.add(mdetails);
                
            }
            
            return list;
            
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/ 
    public double getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(String II_NM, String LOC_FRM, String UOM_NM, Date SI_DT, Integer AD_BRNCH, Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate");
        
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);  
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);  
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);

        	double COST = invItem.getIiUnitCost();
        	
        	LocalInvItemLocation invItemLocation = null;
        	
        	try { 
        		
        		invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_FRM, II_NM, AD_CMPNY);
  
      			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    			        SI_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
      			if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
      			{
      				
      			    COST = invCosting.getCstRemainingQuantity() == 0 ? COST:
                        Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                    if(COST<=0){
                       COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                    }  

                }
      			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
      			{
      				COST = this.getInvFifoCost(SI_DT, invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
      						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
     			}
      			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
      			{
      				COST = invItemLocation.getInvItem().getIiUnitCost();
     			}
    			
        	} catch (FinderException ex) {
        		
        	}
        	
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    // private methods
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT,
            double DR_AMNT, Integer COA_CODE,LocalInvStockIssuance invStockIssuance, Integer AD_BRNCH, Integer AD_CMPNY)
    throws GlobalBranchAccountNumberInvalidException {
        
        Debug.print("InvStockIssuanceEntryControllerBean addInvDrEntry");
        
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory
            .lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME,LocalInvDistributionRecordHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME,LocalGlChartOfAccountHome.class);
            adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
            .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            // get company
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            // validate coa
            
            LocalGlChartOfAccount glChartOfAccount = null;
            
            try {
                
                glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
                
            } catch(FinderException ex) {
                
                throw new GlobalBranchAccountNumberInvalidException ();
                
            }
            
            // create distribution record
            
            LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome
            .create(DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT,
                    adCompany.getGlFunctionalCurrency().getFcPrecision()), EJBCommon.FALSE,EJBCommon.FALSE, AD_CMPNY);
            
            invStockIssuance.addInvDistributionRecord(invDistributionRecord);
            glChartOfAccount.addInvDistributionRecord(invDistributionRecord);
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            throw new GlobalBranchAccountNumberInvalidException ();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private void executeInvSiPost(Integer SI_CODE, String USR_NM, Integer AD_BRNCH,
            Integer AD_CMPNY) throws GlobalRecordAlreadyDeletedException,
            GlobalTransactionAlreadyPostedException,
            GlJREffectiveDateNoPeriodExistException,
            GlJREffectiveDatePeriodClosedException,
            GlobalJournalNotBalanceException,
        	AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvStockIssuanceEntryControllerBean executeInvSiPost");
        
        LocalInvStockIssuanceHome invStockIssuanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invStockIssuanceHome = (LocalInvStockIssuanceHome) EJBHomeFactory
            .lookUpLocalHome(LocalInvStockIssuanceHome.JNDI_NAME,LocalInvStockIssuanceHome.class);
            adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory
            .lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME,LocalAdCompanyHome.class);
            adPreferenceHome = (LocalAdPreferenceHome) EJBHomeFactory
            .lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME,LocalAdPreferenceHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME,LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME,LocalGlAccountingCalendarValueHome.class);
            glJournalHome = (LocalGlJournalHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlJournalHome.JNDI_NAME,LocalGlJournalHome.class);
            glJournalBatchHome = (LocalGlJournalBatchHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME,LocalGlJournalBatchHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME,LocalGlSuspenseAccountHome.class);
            glJournalLineHome = (LocalGlJournalLineHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME,LocalGlJournalLineHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME,LocalGlJournalSourceHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME,LocalGlJournalCategoryHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME,LocalGlFunctionalCurrencyHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome) EJBHomeFactory
            .lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME,LocalInvDistributionRecordHome.class);
            invCostingHome = (LocalInvCostingHome) EJBHomeFactory
            .lookUpLocalHome(LocalInvCostingHome.JNDI_NAME,LocalInvCostingHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome) EJBHomeFactory
            .lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME,LocalGlChartOfAccountHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            // validate if stockissuance is already deleted
            
            LocalInvStockIssuance invStockIssuance = null;
            
            try {
                
                invStockIssuance = invStockIssuanceHome.findByPrimaryKey(SI_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
                
            }
            
            // validate if stockissuance is already posted or void
            
            if (invStockIssuance.getSiPosted() == EJBCommon.TRUE) {
                
                throw new GlobalTransactionAlreadyPostedException();
                
            }
            
        	// regenerate inventory dr

        	this.regenerateInventoryDr(invStockIssuance, AD_BRNCH, AD_CMPNY);
            
            Collection invStockIssuanceLines = invStockIssuance.getInvStockIssuanceLines();
            
            Iterator c = invStockIssuanceLines.iterator();
            
            while (c.hasNext()) {
                
                LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine) c.next();
                
                invStockIssuanceLine.getInvBuildOrderStock().setBosLock(EJBCommon.FALSE);
                
                String II_NM = invStockIssuanceLine.getInvItemLocation().getInvItem().getIiName();
                String LOC_NM = invStockIssuanceLine.getInvItemLocation().getInvLocation().getLocName();
                double ISSNC_QTY = invStockIssuanceLine.getSilIssueQuantity();
                
                ISSNC_QTY = this.convertByUomFromAndBomItemAndQuantity(invStockIssuanceLine.getInvUnitOfMeasure(),
                        invStockIssuanceLine.getInvBuildOrderStock().getInvItem(),
                        invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                        ISSNC_QTY, false, AD_CMPNY);
                
                LocalInvCosting invCosting = null;
                
                try {
                    
                    invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            invStockIssuance.getSiDate(), II_NM,LOC_NM, AD_BRNCH, AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                }
                
                double COST = invStockIssuanceLine.getInvItemLocation().getInvItem().getIiUnitCost();

                
                if (invCosting == null) {
                    
                    
                    this.post(invStockIssuanceLine, invStockIssuance.getSiDate(), -ISSNC_QTY, -(ISSNC_QTY * COST), -ISSNC_QTY, -(ISSNC_QTY * COST), AD_BRNCH, AD_CMPNY);
                    
                } else {
                    
                    LocalInvCosting invLastCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                            invStockIssuance.getSiDate(), II_NM, LOC_NM, AD_BRNCH, AD_CMPNY);
                    
          			if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average")) {
          				
          				double avgCost = invCosting.getCstRemainingQuantity() == 0 ? COST:
							Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());
						
          				
	                    this.post(invStockIssuanceLine, invStockIssuance.getSiDate(), -ISSNC_QTY, -(ISSNC_QTY * avgCost), invLastCosting.getCstRemainingQuantity() - ISSNC_QTY, 
	                            invLastCosting.getCstRemainingValue() - (ISSNC_QTY * avgCost), AD_BRNCH, AD_CMPNY);
	                    
          			} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO")) {
	        			
	        			double fifoCost = invCosting.getCstRemainingQuantity() == 0 ? COST:
	        				this.getInvFifoCost(invCosting.getCstDate(), invCosting.getInvItemLocation().getIlCode(), 
	        					-ISSNC_QTY, COST, true, AD_BRNCH, AD_CMPNY);

	        			
	        			this.post(invStockIssuanceLine, invStockIssuance.getSiDate(), -ISSNC_QTY, fifoCost * -ISSNC_QTY, 
	        					invCosting.getCstRemainingQuantity() + -ISSNC_QTY, 
	        					invCosting.getCstRemainingValue() + (fifoCost * -ISSNC_QTY), AD_BRNCH, AD_CMPNY);
	        			
          			} else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard")) {
	        			
	        			double standardCost = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
     			
	        			this.post(invStockIssuanceLine, invStockIssuance.getSiDate(), -ISSNC_QTY, standardCost * -ISSNC_QTY, 
	        					invCosting.getCstRemainingQuantity() + -ISSNC_QTY, 
	        					invCosting.getCstRemainingValue() + (standardCost * -ISSNC_QTY), AD_BRNCH, AD_CMPNY);
          			}
                }
                
                // set stock issuance line unit cost
                double UNIT_COST = this.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(
                        II_NM, LOC_NM, invStockIssuanceLine.getInvUnitOfMeasure().getUomName(), 
                        invStockIssuance.getSiDate(), AD_BRNCH, AD_CMPNY);
                invStockIssuanceLine.setSilUnitCost(UNIT_COST);
                
            }
            
            // post stockissuance
            
               
            invStockIssuance.setSiPosted(EJBCommon.TRUE);
            invStockIssuance.setSiPostedBy(USR_NM);
            invStockIssuance.setSiDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
            
            // post to gl if necessary
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            // validate if date has no period and period is closed
            
            LocalGlSetOfBook glJournalSetOfBook = null;
            
            try {
                
                glJournalSetOfBook = glSetOfBookHome.findByDate(
                        invStockIssuance.getSiDate(), AD_CMPNY);
                
            } catch (FinderException ex) {
                
                throw new GlJREffectiveDateNoPeriodExistException();
                
            }
            
            LocalGlAccountingCalendarValue glAccountingCalendarValue = glAccountingCalendarValueHome.findByAcCodeAndDate(
                    glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),invStockIssuance.getSiDate(), AD_CMPNY);
            
            if (glAccountingCalendarValue.getAcvStatus() == 'N'
                || glAccountingCalendarValue.getAcvStatus() == 'C'
                    || glAccountingCalendarValue.getAcvStatus() == 'P') {
                
                throw new GlJREffectiveDatePeriodClosedException();
                
            }
            
            // check if invoice is balance if not check suspense posting
            
            LocalGlJournalLine glOffsetJournalLine = null;
            
            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrBySiCode(invStockIssuance.getSiCode(), AD_CMPNY);
            
            Iterator j = invDistributionRecords.iterator();
            
            double TOTAL_DEBIT = 0d;
            double TOTAL_CREDIT = 0d;
            
            while (j.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();
                
                double DR_AMNT = 0d;
                
                DR_AMNT = invDistributionRecord.getDrAmount();
                
                if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
                    
                    TOTAL_DEBIT += DR_AMNT;
                    
                } else {
                    
                    TOTAL_CREDIT += DR_AMNT;
                    
                }
                
            }
            
            TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
            TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
            
            if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE
                    && TOTAL_DEBIT != TOTAL_CREDIT) {
                
                LocalGlSuspenseAccount glSuspenseAccount = null;
                
                try {
                    
                    glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY","STOCK ISSUANCES", AD_CMPNY);
                    
                } catch (FinderException ex) {
                    
                    throw new GlobalJournalNotBalanceException();
                    
                }
                
                if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
                    
                    glOffsetJournalLine = glJournalLineHome.create(
                            (short) (invDistributionRecords.size() + 1),
                            EJBCommon.TRUE, TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
                    
                } else {
                    
                    glOffsetJournalLine = glJournalLineHome.create(
                            (short) (invDistributionRecords.size() + 1),
                            EJBCommon.FALSE, TOTAL_DEBIT - TOTAL_CREDIT,"", AD_CMPNY);
                    
                }
                
                LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
                glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
                
            } else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE&& TOTAL_DEBIT != TOTAL_CREDIT) {
                
                throw new GlobalJournalNotBalanceException();
                
            }
            
            // create journal batch if necessary
            
            LocalGlJournalBatch glJournalBatch = null;
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
            
            try {
                
                glJournalBatch = glJournalBatchHome.findByJbName(
                        "JOURNAL IMPORT " + formatter.format(new Date()) + " STOCK ISSUANCES", AD_BRNCH, AD_CMPNY);
                
            } catch (FinderException ex) {
                
            }
            
            if (glJournalBatch == null) {
                
                glJournalBatch = glJournalBatchHome.create(
                        "JOURNAL IMPORT " + formatter.format(new Date()) + " STOCK ISSUANCES",
                        "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),USR_NM, AD_BRNCH, AD_CMPNY);
                
            }
            
            // create journal entry
            
            LocalGlJournal glJournal = glJournalHome.create(
                    invStockIssuance.getSiReferenceNumber(),
                    invStockIssuance.getSiDescription(), 
                    invStockIssuance.getSiDate(), 0.0d, null, 
                    invStockIssuance.getSiDocumentNumber(), null, 
                    1d, "N/A", null, 'N', EJBCommon.TRUE, EJBCommon.FALSE, USR_NM,
                    new Date(), USR_NM, new Date(), null, null, USR_NM,
                    EJBCommon.getGcCurrentDateWoTime().getTime(), null,
                    null, EJBCommon.FALSE, null,
                    AD_BRNCH, AD_CMPNY);
            
            LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
            glJournal.setGlJournalSource(glJournalSource);
            
            LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
            glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
            
            LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("STOCK ISSUANCES", AD_CMPNY);
            glJournal.setGlJournalCategory(glJournalCategory);
            
            if (glJournalBatch != null) {
            	
            	glJournal.setGlJournalBatch(glJournalBatch);

            }
            
            // create journal lines
            
            j = invDistributionRecords.iterator();
            
            while (j.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord) j.next();
                
                double DR_AMNT = 0d;
                
                DR_AMNT = invDistributionRecord.getDrAmount();
                
                LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),
                        invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
                
                invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
                
                glJournal.addGlJournalLine(glJournalLine);
                
                invDistributionRecord.setDrImported(EJBCommon.TRUE);
                
            }
            
            if (glOffsetJournalLine != null) {
                
                glJournal.addGlJournalLine(glOffsetJournalLine);
                
            }
            
            // post journal to gl
            
            Collection glJournalLines = glJournal.getGlJournalLines();
            
            Iterator i = glJournalLines.iterator();
            
            while (i.hasNext()) {
                
                LocalGlJournalLine glJournalLine = (LocalGlJournalLine) i.next();
                
                // post current to current acv
                
                this.postToGl(glAccountingCalendarValue, glJournalLine.getGlChartOfAccount(), 
                        true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(),
                        AD_CMPNY);
                
                // post to subsequent acvs (propagate)
                
                Collection glSubsequentAccountingCalendarValues = glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber(
                        glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
                        glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);
                
                Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
                
                while (acvsIter.hasNext()) {
                    
                    LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = (LocalGlAccountingCalendarValue) acvsIter.next();
                    
                    this.postToGl(glSubsequentAccountingCalendarValue,
                            glJournalLine.getGlChartOfAccount(), false,
                            glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
                    
                }
                
                // post to subsequent years if necessary
                
                Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
                        glJournalSetOfBook.getGlAccountingCalendar().getAcYear(),AD_CMPNY);
                
                if (!glSubsequentSetOfBooks.isEmpty()
                        && glJournalSetOfBook.getSobYearEndClosed() == 1) {
                    
                    adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
                    LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(
                            adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY);
                    
                    Iterator sobIter = glSubsequentSetOfBooks.iterator();
                    
                    while (sobIter.hasNext()) {
                        
                        LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook) sobIter.next();
                        
                        String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
                        
                        // post to subsequent acvs of subsequent set of
                        // book(propagate)
                        
                        Collection glAccountingCalendarValues = glAccountingCalendarValueHome.findByAcCode(
                                glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);
                        
                        Iterator acvIter = glAccountingCalendarValues.iterator();
                        
                        while (acvIter.hasNext()) {
                            
                            LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = (LocalGlAccountingCalendarValue) acvIter.next();
                            
                            if (ACCOUNT_TYPE.equals("ASSET")
                                    || ACCOUNT_TYPE.equals("LIABILITY")
                                    || ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
                                
                                this.postToGl(glSubsequentAccountingCalendarValue,glJournalLine.getGlChartOfAccount(),
                                        false,glJournalLine.getJlDebit(),glJournalLine.getJlAmount(),AD_CMPNY);
                                
                            } else { // revenue & expense
                                
                                this.postToGl(glSubsequentAccountingCalendarValue,glRetainedEarningsAccount,false,
                                        glJournalLine.getJlDebit(),glJournalLine.getJlAmount(),AD_CMPNY);
                                
                            }
                            
                        }
                        
                        if (glSubsequentSetOfBook.getSobYearEndClosed() == 0)
                            break;
                        
                    }
                    
                }
                
            }
            
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlJREffectiveDatePeriodClosedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalJournalNotBalanceException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalRecordAlreadyDeletedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch (GlobalTransactionAlreadyPostedException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {
	  	 
		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	       
	     // Initialize EJB Home
	        
	     try {
	         
	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     } 
	     catch (NamingException ex) {
	            
	    	 throw new EJBException(ex.getMessage());
	     }
	    	
		try {
			
			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
			
			if (invFifoCostings.size() > 0) {
				
				Iterator x = invFifoCostings.iterator();
			
	  			if (isAdjustFifo) {
	  				
	  				//executed during POST transaction
	  				
	  				double totalCost = 0d;
	  				double cost;
	  				
	  				if(CST_QTY < 0) {
	  					
	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);
	 	  				
	 	  				while(x.hasNext() && neededQty != 0) {
	 	 	  				
	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	
	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}
	
	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
	 	  						
	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;	 			  				 
	 	  					} else {
	 	  						
	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}
	 	  				
	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {
	 	  					
	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}
	 	  				
	 	  				cost = totalCost / -CST_QTY;
	  				} 
	  				
	  				else {
	  					
	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}
	  			
	  			else {
	  				
	  				//executed during ENTRY transaction
	  				
	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	  				
	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			} 
			else {
				
				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}
				
		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}     

    
    
    
    private void post(LocalInvStockIssuanceLine invStockIssuanceLine,
            Date CST_DT,double CST_ADJST_QTY, double CST_ADJST_CST,
            double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH, Integer AD_CMPNY)throws 
			AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvStockIssuanceEntryControllerBean post");
        
        LocalInvCostingHome invCostingHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
            invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
  	      	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            LocalInvItemLocation invItemLocation = invStockIssuanceLine.getInvItemLocation();
            int CST_LN_NMBR = 0;
            
            CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
            CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
            CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
            
            if (CST_ADJST_QTY < 0) {
                
                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
                
            }
            
            // update build order stock qty issued
            
            // conversion
            double convertedQuantity =  this.convertByUomFromAndBomItemAndQuantity(
                    invStockIssuanceLine.getInvUnitOfMeasure(), 
                    invStockIssuanceLine.getInvItemLocation().getInvItem(),
                    invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                    Math.abs(invStockIssuanceLine.getSilIssueQuantity()), true, AD_CMPNY);
            
            double convertedCost = this.convertCostByUom(invItemLocation.getInvItem().getIiName(),
                    invStockIssuanceLine.getInvUnitOfMeasure().getUomName(), CST_ADJST_CST, true, AD_CMPNY);
            
            invStockIssuanceLine.getInvBuildOrderStock().setBosQuantityIssued(invStockIssuanceLine.getInvBuildOrderStock().getBosQuantityIssued() + Math.abs(convertedQuantity));
            invStockIssuanceLine.setSilIssueCost(invStockIssuanceLine.getSilIssueCost() + Math.abs(CST_ADJST_CST));
            
            // update build order line qty available
            
            double qtyAvailable = 0d;
            boolean isInitial = true;
            
            LocalInvBuildOrderLine invBuildOrderLine = invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine();           
            Collection invBuildOrderStocks = invBuildOrderLine.getInvBuildOrderStocks();           
            Iterator bosIter = invBuildOrderStocks.iterator();
            
            while (bosIter.hasNext()) {
                
                LocalInvBuildOrderStock invBuildOrderStock = (LocalInvBuildOrderStock)bosIter.next();           	  
                double unitQtyRequired = invBuildOrderStock.getBosQuantityRequired() / invBuildOrderLine.getBolQuantityRequired();
                
                if (isInitial) {
                    qtyAvailable = Math.floor(invBuildOrderStock.getBosQuantityIssued() / unitQtyRequired);
                    isInitial = false;
                } else {           	  	  	
                    qtyAvailable = Math.min(qtyAvailable, Math.floor(invBuildOrderStock.getBosQuantityIssued() / unitQtyRequired));
                }           	  	             	 
                
            }
            
            invBuildOrderLine.setBolQuantityAvailable(qtyAvailable - invBuildOrderLine.getBolQuantityAssembled());          

            try {
                
                // generate line number
                
                LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
                
            } catch (FinderException ex) {
                
                CST_LN_NMBR = 1;
                
            }

            //void subsequent cost variance adjustments
            Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
            		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
            Iterator i = invAdjustmentLines.iterator();
            
            while (i.hasNext()){
            	
            	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
            	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
            	
            }

            // create costing
            LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
            invItemLocation.addInvCosting(invCosting);
            invCosting.setInvStockIssuanceLine(invStockIssuanceLine);

            // propagate balance if necessary           
            Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
            
            i = invCostings.iterator();
            
            while (i.hasNext()) {
                
                LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
                
                invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
                invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
                
            }                           

            // regenerate cost varaince
            this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
         } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, LocalGlChartOfAccount glChartOfAccount, boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean postToGl");
        
        LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome) EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);
            adCompanyHome = (LocalAdCompanyHome) EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            LocalGlChartOfAccountBalance glChartOfAccountBalance = glChartOfAccountBalanceHome
            .findByAcvCodeAndCoaCode(glAccountingCalendarValue
                    .getAcvCode(), glChartOfAccount.getCoaCode(),
                    AD_CMPNY);
            
            String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
            short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency()
            .getFcPrecision();
            
            if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE
                    .equals("EXPENSE")) && isDebit == EJBCommon.TRUE)
                    || (!ACCOUNT_TYPE.equals("ASSET")
                            && !ACCOUNT_TYPE.equals("EXPENSE") && isDebit == EJBCommon.FALSE)) {
                
                glChartOfAccountBalance.setCoabEndingBalance(EJBCommon.roundIt(
                        glChartOfAccountBalance.getCoabEndingBalance()
                        + JL_AMNT, FC_EXTNDD_PRCSN));
                
                if (!isCurrentAcv) {
                    
                    glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon
                            .roundIt(glChartOfAccountBalance
                                    .getCoabBeginningBalance()
                                    + JL_AMNT, FC_EXTNDD_PRCSN));
                    
                }
                
            } else {
                
                glChartOfAccountBalance.setCoabEndingBalance(EJBCommon.roundIt(
                        glChartOfAccountBalance.getCoabEndingBalance()
                        - JL_AMNT, FC_EXTNDD_PRCSN));
                
                if (!isCurrentAcv) {
                    
                    glChartOfAccountBalance.setCoabBeginningBalance(EJBCommon
                            .roundIt(glChartOfAccountBalance
                                    .getCoabBeginningBalance()
                                    - JL_AMNT, FC_EXTNDD_PRCSN));
                    
                }
                
            }
            
            if (isCurrentAcv) {
                
                if (isDebit == EJBCommon.TRUE) {
                    
                    glChartOfAccountBalance.setCoabTotalDebit(EJBCommon
                            .roundIt(glChartOfAccountBalance
                                    .getCoabTotalDebit()
                                    + JL_AMNT, FC_EXTNDD_PRCSN));
                    
                } else {
                    
                    glChartOfAccountBalance.setCoabTotalCredit(EJBCommon
                            .roundIt(glChartOfAccountBalance
                                    .getCoabTotalCredit()
                                    + JL_AMNT, FC_EXTNDD_PRCSN));
                }
                
            }
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }
      
    private double convertCostByUom(String II_NM, String UOM_NM, double unitCost, boolean isFromDefault, Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean convertCostByUom");		        
        
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            if (isFromDefault) {	        	
                
                return unitCost * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
                
            } else {
                
                return unitCost * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();
                
            }
            
            
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double convertByUomFromAndBomItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, LocalInvItem invAssembly, double ISSUE_QTY, boolean isFromBom, Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalInvBillOfMaterialHome invBillOfMaterialHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
            invBillOfMaterialHome = (LocalInvBillOfMaterialHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBillOfMaterialHome.JNDI_NAME, LocalInvBillOfMaterialHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvBillOfMaterial invBillOfMaterial = invBillOfMaterialHome.findByBomIiNameAndAssemblyItem(
                    invItem.getIiName(), invAssembly.getIiName(), AD_CMPNY);
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invBomUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invBillOfMaterial.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            if (isFromBom) {	        	
                
                return EJBCommon.roundIt(ISSUE_QTY * invBomUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
                
            } else {
                
                return EJBCommon.roundIt(ISSUE_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
                
            }
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private double convertUomByQuantityRequired(String II_NM, String UOM_NM, double qtyRequired, boolean isFromDefault, Integer AD_CMPNY) {
        
        Debug.print("InvStockIssuanceEntryControllerBean convertUomByQuantityRequired");		        
        
        LocalInvItemHome invItemHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvItem invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            if (isFromDefault) {	        	
                
                return qtyRequired * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor();
                
            } else {
                
                return qtyRequired * invUnitOfMeasureConversion.getUmcConversionFactor() / invDefaultUomConversion.getUmcConversionFactor();
                
            }
            
            
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    } 
    
    private void regenerateInventoryDr(LocalInvStockIssuance invStockIssuance, Integer AD_BRNCH, Integer AD_CMPNY) throws
    GlobalInventoryDateException,
    GlobalBranchAccountNumberInvalidException {
        
        Debug.print("InvStockIssuanceEntryControllerBean regenerateInventoryDr");		        
        
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalInvItemHome invItemHome = null;
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
            invItemHome = (LocalInvItemHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);    
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            // regenerate inventory distribution records
            
            // remove all inventory distribution
            
            Collection invDistributionRecords = invDistributionRecordHome.findImportableDrBySiCode(
                    invStockIssuance.getSiCode(), AD_CMPNY);
            
            Iterator i = invDistributionRecords.iterator();
            
            while (i.hasNext()) {
                
                LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
                
                if(invDistributionRecord.getDrClass().equals("INVENTORY") || invDistributionRecord.getDrClass().equals("COGS")){
                    
                i.remove();
                invDistributionRecord.remove();
                
                }
                
            }
            
            // remove all stock issuance lines committed qty
            
            Collection invStockIssuanceLines = invStockIssuance.getInvStockIssuanceLines();
            
            i = invStockIssuanceLines.iterator();
            
            while (i.hasNext()) {
                
                LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)i.next();
                
                // conversion
                double convertedQuantity = this.convertByUomFromAndBomItemAndQuantity(
                        invStockIssuanceLine.getInvUnitOfMeasure(), 
                        invStockIssuanceLine.getInvItemLocation().getInvItem(),
                        invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                        Math.abs(invStockIssuanceLine.getSilIssueQuantity()), false, AD_CMPNY);
                
                invStockIssuanceLine.getInvItemLocation().setIlCommittedQuantity(invStockIssuanceLine.getInvItemLocation().getIlCommittedQuantity() - convertedQuantity);
                
            }
            
            invStockIssuanceLines = invStockIssuance.getInvStockIssuanceLines();
            
            if(invStockIssuanceLines != null && !invStockIssuanceLines.isEmpty()) {
                
                byte DEBIT = 0;
                double TOTAL_AMOUNT = 0d; 
                
                i = invStockIssuanceLines.iterator();
                
                while(i.hasNext()) {
                    
                    LocalInvStockIssuanceLine invStockIssuanceLine = (LocalInvStockIssuanceLine)i.next();
                    LocalInvItemLocation invItemLocation = invStockIssuanceLine.getInvItemLocation();
                    
                    
                    
                    // start date validation
                    
                    Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
                            invStockIssuance.getSiDate(), invItemLocation.getInvItem().getIiName(),
                            invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                    if(!invNegTxnCosting.isEmpty()) { 
                        throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
                    }
                    // add stock issuance distribution
                    
                    double AMOUNT = 0d;
                    
                    double COST = invStockIssuanceLine.getInvItemLocation().getInvItem().getIiUnitCost();
                    
                    try {
                        
                        LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
                        		invStockIssuance.getSiDate(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
                        
                        if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))

                        	COST = invCosting.getCstRemainingQuantity() <= 0 ? COST:
                        	Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity());

                             if(COST<=0){
                               COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                            }  

                        else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
                        	
                        	COST = invCosting.getCstRemainingQuantity() <= 0 ? COST:
                						this.getInvFifoCost(invStockIssuance.getSiDate(), invItemLocation.getIlCode(), invStockIssuanceLine.getSilIssueQuantity(), 
                								invStockIssuanceLine.getSilUnitCost(), false, AD_BRNCH, AD_CMPNY); 
                        
                        else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
                        	
                        	COST = invItemLocation.getInvItem().getIiUnitCost();
                        
                    } catch (FinderException ex) { }
                    
                    COST = this.convertCostByUom(invItemLocation.getInvItem().getIiName(),
                            invStockIssuanceLine.getInvUnitOfMeasure().getUomName(), COST, true, AD_CMPNY);
                    
                    AMOUNT = EJBCommon.roundIt(invStockIssuanceLine.getSilIssueQuantity() * COST, 
                            this.getGlFcPrecisionUnit(AD_CMPNY));
                    
                    // get assembly item location
                    
                    LocalInvItemLocation invAssemblyLocation =  invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation();
                    
                    // check for branch mapping
                    
                    LocalAdBranchItemLocation adBranchItemLocation = null;
                    LocalAdBranchItemLocation adBranchAssemblyLocation = null;
                    
                    try {
                        
                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invStockIssuanceLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
                        adBranchAssemblyLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                                invAssemblyLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
                        
                    } catch (FinderException ex){
                        
                    }
                    
                    if (adBranchItemLocation==null){
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, AMOUNT,
                                invItemLocation.getIlGlCoaInventoryAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    } else {
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, AMOUNT,
                                adBranchItemLocation.getBilCoaGlInventoryAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    }
                    
                    if (adBranchAssemblyLocation==null){
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "WIP", EJBCommon.TRUE, AMOUNT,
                                invAssemblyLocation.getIlGlCoaWipAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    } else {
                        
                        this.addInvDrEntry(invStockIssuance.getInvDrNextLine(), "WIP", EJBCommon.TRUE, AMOUNT,
                                adBranchAssemblyLocation.getBilCoaGlWipAccount(), invStockIssuance,AD_BRNCH, AD_CMPNY);
                        
                    }
                    
                    // commit inventory
                    
                    double convertedQuantity = this.convertByUomFromAndBomItemAndQuantity(
                            invStockIssuanceLine.getInvUnitOfMeasure(), 
                            invStockIssuanceLine.getInvItemLocation().getInvItem(),
                            invStockIssuanceLine.getInvBuildOrderStock().getInvBuildOrderLine().getInvItemLocation().getInvItem(),
                            Math.abs(invStockIssuanceLine.getSilIssueQuantity()), false, AD_CMPNY);
                    
                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);
                    
                }
                
            }
            
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
            
        } catch(GlobalBranchAccountNumberInvalidException ex) {
            
            throw new GlobalBranchAccountNumberInvalidException ();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvStockIssuanceEntryControllerBean voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			
    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}
    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	Debug.print("InvStockIssuanceEntryControllerBean generateCostVariance");
    	/*
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 
    		
    		
    		
    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;
    		
    		
    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	
    	Debug.print("InvStockIssuanceEntryControllerBean regenerateCostVariance");
    	/*
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}     */   	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE, 
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvStockIssuanceEntryControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					AD_CMPNY);

    		
    		invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvStockIssuanceEntryControllerBean executeInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);

    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    			
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}

    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 
						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {
    				
    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				
    				glJournal.addGlJournalLine(glJournalLine);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				glJournal.addGlJournalLine(glOffsetJournalLine);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
			
			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("InvStockIssuanceEntryControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null,null, 0,0, AL_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		invAdjustment.addInvAdjustmentLine(invAdjustmentLine);	
    		invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    } 
    
    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvStockIssuanceEntryControllerBean saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("InvStockIssuanceEntryControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY : 0, AD_BRNCH, AD_CMPNY);
    		invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
			
		Debug.print("InvStockIssuanceEntryControllerBean convertByUomFromAndItemAndQuantity");		        
        
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
                    
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
                
        try {
        
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
        						    		        		
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
    
}
