package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimit;
import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApproval;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueue;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUser;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdDeleteAuditTrailHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlAccountingCalendarValue;
import com.ejb.gl.LocalGlAccountingCalendarValueHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountBalance;
import com.ejb.gl.LocalGlChartOfAccountBalanceHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.gl.LocalGlFunctionalCurrency;
import com.ejb.gl.LocalGlFunctionalCurrencyHome;
import com.ejb.gl.LocalGlJournal;
import com.ejb.gl.LocalGlJournalBatch;
import com.ejb.gl.LocalGlJournalBatchHome;
import com.ejb.gl.LocalGlJournalCategory;
import com.ejb.gl.LocalGlJournalCategoryHome;
import com.ejb.gl.LocalGlJournalHome;
import com.ejb.gl.LocalGlJournalLine;
import com.ejb.gl.LocalGlJournalLineHome;
import com.ejb.gl.LocalGlJournalSource;
import com.ejb.gl.LocalGlJournalSourceHome;
import com.ejb.gl.LocalGlSetOfBook;
import com.ejb.gl.LocalGlSetOfBookHome;
import com.ejb.gl.LocalGlSuspenseAccount;
import com.ejb.gl.LocalGlSuspenseAccountHome;
import com.ejb.inv.LocalInvAdjustment;
import com.ejb.inv.LocalInvAdjustmentHome;
import com.ejb.inv.LocalInvAdjustmentLine;
import com.ejb.inv.LocalInvAdjustmentLineHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvStockTransfer;
import com.ejb.inv.LocalInvStockTransferHome;
import com.ejb.inv.LocalInvStockTransferLine;
import com.ejb.inv.LocalInvStockTransferLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModStockTransferDetails;
import com.util.InvModStockTransferLineDetails;
import com.util.InvModUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvStockTransferEntryControllerEJB"
 *           display-name="used for transferring stocks from one location to another"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvStockTransferEntryControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvStockTransferEntryController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvStockTransferEntryControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvStockTransferEntryControllerBean extends AbstractSessionBean {
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvLocAll(Integer AD_CMPNY) {
                    
        Debug.print("InvStockTransferEntryControllerBean getInvLocAll");
        
        LocalInvLocationHome invLocationHome = null;
        Collection invLocations = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
	               
            invLocations = invLocationHome.findLocAll(AD_CMPNY);            
        
	        if (invLocations.isEmpty()) {
	        	
	        	return null;
	        	
	        }
	        
	        Iterator i = invLocations.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalInvLocation invLocation = (LocalInvLocation)i.next();	
	        	String details = invLocation.getLocName();
	        	
	        	list.add(details);
	        	
	        }
	        
	        return list;
        
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public InvModStockTransferDetails getInvStByStCode(Integer ST_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException {
       
        Debug.print("InvStockTransferEntryControllerBean getInvStByStCode");
        
        LocalInvStockTransferHome invStockTransferHome = null;        
        
        // Initialize EJB Home
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);
            
        } catch(NamingException ex) {
            
            throw(new EJBException(ex.getMessage()));
            
        }
        
        try {
            
            LocalInvStockTransfer invStockTransfer = null;
            
            try {
                                           
                invStockTransfer = invStockTransferHome.findByPrimaryKey(ST_CODE);
                
            } catch(FinderException ex) {
                
                throw new GlobalNoRecordFoundException();
            }
            
            ArrayList stlList = new ArrayList();
            
            Iterator i = invStockTransfer.getInvStockTransferLines().iterator();
            
            while(i.hasNext()) {
                
                LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();
                
                InvModStockTransferLineDetails mdetails = new InvModStockTransferLineDetails();
                
                mdetails.setStlCode(invStockTransferLine.getStlCode());
                                
                mdetails.setStlLocationNameFrom(this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationFrom()));
                mdetails.setStlLocationNameTo(this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationTo()));
                mdetails.setStlIiName(invStockTransferLine.getInvItem().getIiName());
                mdetails.setStlIiDescription(invStockTransferLine.getInvItem().getIiDescription());
                mdetails.setStlUomName(invStockTransferLine.getInvUnitOfMeasure().getUomName());
                mdetails.setStlUnitCost(invStockTransferLine.getStlUnitCost());
                mdetails.setStlQuantityDelivered(invStockTransferLine.getStlQuantityDelivered());
                mdetails.setStlAmount(invStockTransferLine.getStlAmount());     
                mdetails.setStlMisc(invStockTransferLine.getStlMisc());
                stlList.add(mdetails);
            }
                        
            InvModStockTransferDetails details = new InvModStockTransferDetails();
            details.setStCode(invStockTransfer.getStCode());
            details.setStDocumentNumber(invStockTransfer.getStDocumentNumber());
            details.setStReferenceNumber(invStockTransfer.getStReferenceNumber());
            details.setStDescription(invStockTransfer.getStDescription());
            details.setStDate(invStockTransfer.getStDate());
            details.setStApprovalStatus(invStockTransfer.getStApprovalStatus());
            details.setStPosted(invStockTransfer.getStPosted());
            details.setStCreatedBy(invStockTransfer.getStCreatedBy());
            details.setStDateCreated(invStockTransfer.getStDateCreated());
            details.setStLastModifiedBy(invStockTransfer.getStLastModifiedBy());
            details.setStDateLastModified(invStockTransfer.getStDateLastModified());
            details.setStApprovedRejectedBy(invStockTransfer.getStApprovedRejectedBy());
            details.setStDateApprovedRejected(invStockTransfer.getStDateApprovedRejected());
            details.setStPostedBy(invStockTransfer.getStPostedBy());
            details.setStDatePosted(invStockTransfer.getStDatePosted());
            details.setStReasonForRejection(invStockTransfer.getStReasonForRejection());
            details.setStStlList(stlList);
            
            return details;
            
        } catch (GlobalNoRecordFoundException ex) {
        	
        	Debug.printStackTrace(ex);
        	throw ex;
        	
    	} catch (Exception ex) {
        	
    		Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }           
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomByIiName(String II_NM, Integer AD_CMPNY) {
    	
    	Debug.print("InvStockTransferEntryControllerBean getInvUomByIiName");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        LocalInvItemHome invItemHome = null;
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
        	
        	LocalInvItem invItem = null;
        	LocalInvUnitOfMeasure invItemUnitOfMeasure = null;
        	
        	invItem = invItemHome.findByIiName(II_NM, AD_CMPNY);        	
        	invItemUnitOfMeasure = invItem.getInvUnitOfMeasure();
        	
        	Iterator i = invUnitOfMeasureHome.findByUomAdLvClass(
        			invItemUnitOfMeasure.getUomAdLvClass(), AD_CMPNY).iterator();
        	while (i.hasNext()) {
        		
        		LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure) i.next();
        		InvModUnitOfMeasureDetails details = new InvModUnitOfMeasureDetails();
        		details.setUomName(invUnitOfMeasure.getUomName());
        		
        		if (invUnitOfMeasure.getUomName().equals(invItemUnitOfMeasure.getUomName())) {
        			
        			details.setDefault(true);
        			
        		}
        		
        		list.add(details);
        		
        	}
        	
        	return list;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
	
    }
            
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public Integer saveInvStEntry(com.util.InvStockTransferDetails details, ArrayList stlList,  
    		boolean isDraft, Integer AD_BRNCH, Integer AD_CMPNY) throws
    		GlobalRecordAlreadyDeletedException,    		
    		GlobalTransactionAlreadyApprovedException,
    		GlobalTransactionAlreadyPendingException,
    		GlobalTransactionAlreadyPostedException,
    		GlobalNoApprovalRequesterFoundException,
    		GlobalNoApprovalApproverFoundException,
    		GlobalInvItemLocationNotFoundException,		
    		GlJREffectiveDateNoPeriodExistException,
    		GlJREffectiveDatePeriodClosedException,
    		GlobalJournalNotBalanceException,
    		GlobalDocumentNumberNotUniqueException, 
    		GlobalInventoryDateException,
			GlobalBranchAccountNumberInvalidException,
			AdPRFCoaGlVarianceAccountNotFoundException,
			GlobalRecordInvalidException {
        
        Debug.print("InvStockTransferEntryControllerBean saveInvStEntry");
        
        //switch if the post tyep is from draft to post or not
        boolean IfDraftToPost = true;
        
        LocalInvStockTransferHome invStockTransferHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;        
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvCostingHome invCostingHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);            
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
			    lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        try {
            
            LocalInvStockTransfer invStockTransfer = null;
            
            //  validate if stock transfer is already deleted
            
            try {
        		
        		if (details.getStCode() != null) {
        			
        			invStockTransfer = invStockTransferHome.findByPrimaryKey(details.getStCode());
        			        			
        		}
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalRecordAlreadyDeletedException();
        		
        	}
        	
        	// validate if stock transfer is already posted, void, approved or pending
        	
        	if (details.getStCode() != null) {
	        	
	        	
	        	if (invStockTransfer.getStApprovalStatus() != null) {
        		
	        		if (invStockTransfer.getStApprovalStatus().equals("APPROVED") ||
	        		        invStockTransfer.getStApprovalStatus().equals("N/A")) {         		    	
	        		 
	        		    throw new GlobalTransactionAlreadyApprovedException(); 
	        		    	
	        		    	
	        		} else if (invStockTransfer.getStApprovalStatus().equals("PENDING")) {
	        			
	        			throw new GlobalTransactionAlreadyPendingException();
	        			
	        		}
	        		
	        	}
        			
        		if (invStockTransfer.getStPosted() == EJBCommon.TRUE) {
        			
        			throw new GlobalTransactionAlreadyPostedException();
        			
        		}
        		
        	}
        	
    		
        	LocalInvStockTransfer invExistingStockTransfer= null;
	        
    		try {
    		
    		    invExistingStockTransfer = invStockTransferHome.findByStDocumentNumberAndBrCode(details.getStDocumentNumber(), AD_BRNCH, AD_CMPNY);
        		    
        	} catch (FinderException ex) {
        		
        	}
            
    	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
        	// validate if document number is unique document number is automatic then set next sequence
        	
        	if (details.getStCode() == null) {

	        	LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		        LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
		        
		        if (invExistingStockTransfer != null) {
                    
                    throw new GlobalDocumentNumberNotUniqueException();
                    
                }

	        	try {
	        		
	        		adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV STOCK TRANSFER", AD_CMPNY);
	        		
	        	} catch (FinderException ex) {
	        		
	        	}
	        	
	        	try {
	        		
	        		adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
	        		
	        	} catch (FinderException ex) {
	        		
	        	}
		        	    
		        if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
		            (details.getStDocumentNumber() == null || details.getStDocumentNumber().trim().length() == 0)) {
		            	
		        	while (true) {
		        		
		        		if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
		        			
		        			try {
		        				
		        				invStockTransferHome.findByStDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), AD_BRNCH, AD_CMPNY);		            		
		        				adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		        				
		        			} catch (FinderException ex) {
		        				
		        				details.setStDocumentNumber(adDocumentSequenceAssignment.getDsaNextSequence());
		        				adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		        				break;
		        				
		        			}
		        			
		        		} else {
		        			
		        			try {
		        				
		        				invStockTransferHome.findByStDocumentNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
		        				adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
		        				
		        			} catch (FinderException ex) {
		        				
		        				details.setStDocumentNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
		        				adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
		        				break;
		        				
		        			}
		        			
		        		}
		        		
		        	}		            
		            		            	
		        }
		        
		        
		        
		    } else {
		    	
		    	IfDraftToPost = false;
	        	if (invExistingStockTransfer != null && 
	                !invExistingStockTransfer.getStCode().equals(details.getStCode())) {
	            	
	            	throw new GlobalDocumentNumberNotUniqueException();
	            	
	            }
	        	/*
	        	if (invExistingStockTransfer != null && 
		                invExistingStockTransfer.getStDocumentNumber().equals(details.getStDocumentNumber())) {
		            	
		            	throw new GlobalDocumentNumberNotUniqueException();
		            	
		            }
	        	*/
	            
	            if (invStockTransfer.getStDocumentNumber() != details.getStDocumentNumber() &&
	                (details.getStDocumentNumber() == null || details.getStDocumentNumber().trim().length() == 0)) {
	                	
	                details.setStDocumentNumber(invStockTransfer.getStDocumentNumber());
	                	
	         	}
	            	            		    	
		    }
        	
        	// used in checking if invoice should re-generate distribution records
        	
	        boolean isRecalculate = true;
	        
	        // create stock transfer
	        
	        if (details.getStCode() == null) {
	            
	            invStockTransfer = invStockTransferHome.create(details.getStDocumentNumber(), 
		               details.getStReferenceNumber(), details.getStDescription(), details.getStDate(),
		               details.getStApprovalStatus(), EJBCommon.FALSE, details.getStCreatedBy(), 
		               details.getStDateCreated(), details.getStLastModifiedBy(), details.getStDateLastModified(), 
		               null, null, null, null, null, AD_BRNCH, AD_CMPNY);
	            
	        } else {
    	        
	            if (stlList.size() != invStockTransfer.getInvStockTransferLines().size() ||	        
	               !(invStockTransfer.getStDate().equals(details.getStDate()))) {
	                
	                isRecalculate = true;
	                
	            } else if (stlList.size() == invStockTransfer.getInvStockTransferLines().size()) {
	                
	                Iterator stlIter = invStockTransfer.getInvStockTransferLines().iterator();
	                Iterator stlIterList = stlList.iterator();
	                
	                while(stlIter.hasNext()) {
	                    
	                    LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) stlIter.next();
	                    InvModStockTransferLineDetails mdetails = (InvModStockTransferLineDetails) stlIterList.next(); 
	                    
	                    String invLocationFrom = this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationFrom());
	                    String invLocationTo = this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationTo());
	                    
	                    if (!invLocationFrom.equals(mdetails.getStlLocationNameFrom()) || 
	                        !invLocationTo.equals(mdetails.getStlLocationNameTo()) || 
	                        invStockTransferLine.getStlUnitCost() != mdetails.getStlUnitCost() || 
	                        invStockTransferLine.getStlQuantityDelivered() != mdetails.getStlQuantityDelivered() ||
	                        invStockTransferLine.getStlAmount() != mdetails.getStlAmount() || 
	                        !invStockTransferLine.getInvItem().getIiName().equals(mdetails.getStlIiName()) || 
	                        !invStockTransferLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getStlUomName())) {
	                        
	                        isRecalculate = true;
	                        break;
	                        
	                    }
	                    
	                    // get item cost
	                    
	                    LocalInvItemLocation invItemLocFrom = null;
	                    
	               		try {
	               			
	               			invItemLocFrom = invItemLocationHome.findByLocNameAndIiName(invLocationFrom, invStockTransferLine.getInvItem().getIiName(), AD_CMPNY);
	          	    		
	          	    	} catch (FinderException ex) {
	          	    		
	          	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(invLocationFrom));
	          	    		
	          	    	}
	          	    	
	          	    	double COST = 0d;
	               		
	               		try {
	               			
	               			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
	               					invStockTransfer.getStDate(), invItemLocFrom.getInvItem().getIiName(), invItemLocFrom.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	               			
	               			
	               			if(invCosting.getCstRemainingQuantity()<=0) {
	               				COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
	    	               		
	               			}else {
	               				COST = invItemLocFrom.getInvItem().getIiUnitCost();
	               			}
	               			
	               		} catch (FinderException ex) {
	               			
	               			COST = invItemLocFrom.getInvItem().getIiUnitCost();
	               			
	               		}
	               		
	               		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocFrom.getInvItem().getIiName(), invStockTransferLine.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
	                	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItemLocFrom.getInvItem().getIiName(), invItemLocFrom.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
	                	
	                	COST = EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
	               		
	               		double AMOUNT = 0d;
		      	    	
	               		AMOUNT = EJBCommon.roundIt(invStockTransferLine.getStlQuantityDelivered() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));	                	               
		                
	               		if (invStockTransferLine.getStlUnitCost() != COST) {
	                    	
	               			mdetails.setStlUnitCost(COST);
	               			mdetails.setStlAmount(AMOUNT);
	               			
	                    	isRecalculate = true;
	                        break;
	                        
	                    }
	                    
	                    isRecalculate = false;
	                    
	                }
	                
	            } else {
	                
	                isRecalculate = true;
	                
	            }
	            
	            invStockTransfer.setStDocumentNumber(details.getStDocumentNumber());
	            invStockTransfer.setStReferenceNumber(details.getStReferenceNumber());
	            invStockTransfer.setStDescription(details.getStDescription());
	            invStockTransfer.setStDate(details.getStDate());
	            invStockTransfer.setStApprovalStatus(details.getStApprovalStatus());
	            invStockTransfer.setStLastModifiedBy(details.getStLastModifiedBy());
	            invStockTransfer.setStDateLastModified(details.getStDateLastModified());
	            invStockTransfer.setStReasonForRejection(null);
	            
	        }	        	       
	        
	        double ABS_TOTAL_AMOUNT = 0d;
	        
	        if (isRecalculate) {
	            
	            // remove all stock transfer lines
	            
	            Iterator i = invStockTransfer.getInvStockTransferLines().iterator();
	            
	            short LINE_NUMBER = 0;
	            
	            while(i.hasNext()) {
	                
	                LINE_NUMBER++;
	                
	                LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();
	                
	                String invLocation = this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationFrom());
	                
	                LocalInvItemLocation invItemLocation = null;
	                
	                try {
	                
	                    invItemLocation = invItemLocationHome.findByLocNameAndIiName(invLocation, 
		                        	    invStockTransferLine.getInvItem().getIiName(), AD_CMPNY);
		                
	                } catch(FinderException ex) {
	                    
	                    throw new GlobalInvItemLocationNotFoundException(String.valueOf(LINE_NUMBER));
	                    
	                }
	                
	                double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
	                        invStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(), 
	                        invStockTransferLine.getStlQuantityDelivered(), AD_CMPNY);
	                
	                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
	                
	                i.remove();
	                
	                invStockTransferLine.remove();
	                
	            }
	            
	            // remove all distribution records
	            
	            i = invStockTransfer.getInvDistributionRecords().iterator();
	            
	            while(i.hasNext()) {
	                
	                LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord) i.next();
	                
	                i.remove();
	                
	                arDistributionRecord.remove();
	                
	            }
	            
	            // add new stock transfer entry lines and distribution record
	            
	            byte DEBIT = 0;
	            
	            i = stlList.iterator();
	            
	            while(i.hasNext()) {
	                
	                InvModStockTransferLineDetails mdetails = (InvModStockTransferLineDetails) i.next();	                	                
	                	                	                
	                LocalInvItemLocation invItemLocFrom = null;	                
	                LocalInvItemLocation invItemLocTo = null;
	                
	                try {
		 	  	    	
	 	  	    		invItemLocFrom = invItemLocationHome.findByLocNameAndIiName(
	 	  	    		        mdetails.getStlLocationNameFrom(), 
	 	  	    		        mdetails.getStlIiName(), AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getStlLineNumber() + " - " + mdetails.getStlLocationNameFrom()));
		 	  	    		
		 	  	    }
	 	  	    	
	 	  	    	try {
		 	  	    	
	 	  	    		invItemLocTo = invItemLocationHome.findByLocNameAndIiName(
	 	  	    		        mdetails.getStlLocationNameTo(), 
	 	  	    		        mdetails.getStlIiName(), AD_CMPNY);

	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getStlLineNumber() + " - " + mdetails.getStlLocationNameTo()));
		 	  	    		
		 	  	    }
	                
	                //	start date validation	                	              
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	 	  	    		Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    				invStockTransfer.getStDate(), invItemLocFrom.getInvItem().getIiName(),
	 	  	    				invItemLocFrom.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocFrom.getInvItem().getIiName());
	 	  	    		
	 	  	    		invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    				invStockTransfer.getStDate(), invItemLocTo.getInvItem().getIiName(),
	 	  	    				invItemLocTo.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocTo.getInvItem().getIiName());
	 	  	    	}
	 	  	    	LocalInvStockTransferLine invStockTransferLine = this.addInvStlEntry(mdetails, invStockTransfer, AD_CMPNY);
	 	  	        
	                // add physical inventory distribution
	                
	 	  	    	double COST = this.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(invItemLocFrom.getInvItem().getIiName(), invItemLocFrom.getInvLocation().getLocName(),
	 	  	    			invStockTransferLine.getInvUnitOfMeasure().getUomName(), invStockTransfer.getStDate(),
							AD_BRNCH, AD_CMPNY);
	      	    		      	    	      	    	
	      	    		                
	      	    	double AMOUNT = 0d;
	      	    	
	                AMOUNT = EJBCommon.roundIt(invStockTransferLine.getStlQuantityDelivered() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));	                	               
	                
	                // dr to locationTo
	                
	 	  	    	// check branch mapping
	                
	                LocalAdBranchItemLocation adBranchItemLocation = null;

	 	  	    	try{
	 	  	    	
	 	  	    		adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	 	  	    				invItemLocTo.getIlCode(), AD_BRNCH, AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {

	 	  	    	}
	 	  	    	
	 	  	    	LocalGlChartOfAccount glChartOfAccountTo = null;

	 	  	    	if (adBranchItemLocation == null) {
	 	  	    		
	 	  	    		glChartOfAccountTo = glChartOfAccountHome.findByPrimaryKey(
		 	  	    			invItemLocTo.getIlGlCoaInventoryAccount());

	 	  	    	} else {
	 	  	    		
	 	  	    		glChartOfAccountTo = glChartOfAccountHome.findByPrimaryKey(
	 	  	    				adBranchItemLocation.getBilCoaGlInventoryAccount());
	 	  	    		
	 	  	    	}

	 	  	    	this.addInvDrEntry(invStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE, 
	                        Math.abs(AMOUNT), glChartOfAccountTo.getCoaCode(), invStockTransfer, AD_BRNCH, AD_CMPNY);

	                // cr to locationFrom	                	                
	                
	                // check branch mapping
	                
	                try{
	                	
	                	adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
	                			invItemLocFrom.getIlCode(), AD_BRNCH, AD_CMPNY);
	                	
	                } catch (FinderException ex) {
	                	
	                }

	                LocalGlChartOfAccount glChartOfAccountFrom = null;

	 	  	    	if (adBranchItemLocation == null) {
	 	  	    		
	 	  	    		glChartOfAccountFrom = glChartOfAccountHome.findByPrimaryKey(
                        	invItemLocFrom.getIlGlCoaInventoryAccount());

	 	  	    	} else {
	                	
	 	  	    		glChartOfAccountFrom = glChartOfAccountHome.findByPrimaryKey(
	                        	adBranchItemLocation.getBilCoaGlInventoryAccount());

	                }
	 	  	    	
	 	  	    	this.addInvDrEntry(invStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, 
	                        Math.abs(AMOUNT), glChartOfAccountFrom.getCoaCode(), invStockTransfer, AD_BRNCH, AD_CMPNY);
	                
	                ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
	                
	                // set ilCommittedQuantity
	                
	                double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
	                        invStockTransferLine.getInvUnitOfMeasure(), invItemLocFrom.getInvItem(), 
	                        invStockTransferLine.getStlQuantityDelivered(), AD_CMPNY);
	                
	                invItemLocFrom.setIlCommittedQuantity(invItemLocFrom.getIlCommittedQuantity() + convertedQuantity);
	                
	            }
	            
	        } else {
         	   
	        	Iterator i = stlList.iterator();
	            
	            while(i.hasNext()) {
	                
	                InvModStockTransferLineDetails mdetails = (InvModStockTransferLineDetails) i.next();	                	                
	                	                	                
	                LocalInvItemLocation invItemLocFrom = null;	                
	                LocalInvItemLocation invItemLocTo = null;
	                
	                try {
		 	  	    	
	 	  	    		invItemLocFrom = invItemLocationHome.findByLocNameAndIiName(
	 	  	    		        mdetails.getStlLocationNameFrom(), 
	 	  	    		        mdetails.getStlIiName(), AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getStlLineNumber() + " - " + mdetails.getStlLocationNameFrom()));
		 	  	    		
		 	  	    }
	 	  	    	
	 	  	    	try {
		 	  	    	
	 	  	    		invItemLocTo = invItemLocationHome.findByLocNameAndIiName(
	 	  	    		        mdetails.getStlLocationNameTo(), 
	 	  	    		        mdetails.getStlIiName(), AD_CMPNY);
	 	  	    		
	 	  	    	} catch (FinderException ex) {
	 	  	    	
	 	  	    		throw new GlobalInvItemLocationNotFoundException(String.valueOf(mdetails.getStlLineNumber() + " - " + mdetails.getStlLocationNameTo()));
		 	  	    		
		 	  	    }
	                
	                //	start date validation	                	              
	 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
	 	  	    		Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    				invStockTransfer.getStDate(), invItemLocFrom.getInvItem().getIiName(),
	 	  	    				invItemLocFrom.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocFrom.getInvItem().getIiName());
	 	  	    		
	 	  	    		invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
	 	  	    				invStockTransfer.getStDate(), invItemLocTo.getInvItem().getIiName(),
	 	  	    				invItemLocTo.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
	 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocTo.getInvItem().getIiName());
	 	  	    	}
	            }
	        	
        		i = invStockTransfer.getInvDistributionRecords().iterator();
        		
        		while(i.hasNext()) {
        			
        			LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();
        			
        			if(distributionRecord.getDrDebit() == 1) {
        			
        				ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();
        			
        			}
        			
        		}
        		
	        }
	        
	        
	      //insufficient stock checking
            if (adPreference.getPrfArCheckInsufficientStock() == EJBCommon.TRUE ) {
        		boolean hasInsufficientItems = false;
        		String insufficientItems = "";
        		
        		
        		Collection invStockTransferLines = invStockTransfer.getInvStockTransferLines();
                
                Iterator i = invStockTransferLines.iterator();     	  
                
                while (i.hasNext()) {
                    
                	LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)i.next();
                                                
            	
	            	String LOC_NM_FRM = this.getInvLocNameByLocCode(
	            			invStockTransferLine.getStlLocationFrom());                
	            	String LOC_NM_TO = this.getInvLocNameByLocCode(
	            			invStockTransferLine.getStlLocationTo());
	            	
	            	LocalInvItemLocation invItemLocationFrom = invItemLocationHome.findByLocNameAndIiName(
	            			LOC_NM_FRM, invStockTransferLine.getInvItem().getIiName(),AD_CMPNY);                
	            	
            	
            	double ST_QTY = this.convertByUomFromAndItemAndQuantity(
            			invStockTransferLine.getInvUnitOfMeasure(), 
						invStockTransferLine.getInvItem(), 
						invStockTransferLine.getStlQuantityDelivered(),
						AD_CMPNY);
            	
            	String II_NM = invStockTransferLine.getInvItem().getIiName();
            	
            	LocalInvCosting invCosting = null;
            	
            	try {                    
            		
            		invCosting = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
            				invStockTransfer.getStDate(), II_NM, LOC_NM_FRM, AD_BRNCH, AD_CMPNY);                                                          
            		
            	} catch (FinderException ex) {
            		
            	} 

                
				double LOWEST_QTY = this.convertByUomAndQuantity(
						invStockTransferLine.getInvUnitOfMeasure(),
						invStockTransferLine.getInvItem(),
						1, AD_CMPNY);
				
				
				
				if (invCosting == null || invCosting.getCstRemainingQuantity() == 0 || 
						invCosting.getCstRemainingQuantity() - ST_QTY <= -LOWEST_QTY) {
					
						hasInsufficientItems = true;
						
						insufficientItems += invStockTransferLine.getInvItem().getIiName() + ", ";
				}
		  	   	    
                }
                if(hasInsufficientItems) {
                	
                	throw new GlobalRecordInvalidException(insufficientItems.substring(0, insufficientItems.lastIndexOf(",")));
                }
        	}
            
	        
	        //	generate approval status
 	  	    
 	  	    String INV_APPRVL_STATUS = null;
 	  	    
 	  	    if (!isDraft) {
 	  	        
 	  	        LocalAdApproval adApproval = adApprovalHome.findByAprAdCompany(AD_CMPNY);
 	  	        
 	  	        // check if inv stock transfer approval is enabled
 	  	        
 	  	        if (adApproval.getAprEnableInvStockTransfer() == EJBCommon.FALSE) {
 	  	            
 	  	         	INV_APPRVL_STATUS = "N/A";
 	  	         	
 	  	        } else {
 	  	            
 	  	            // 	check if invoice is self approved
 	  	            
 	  	            LocalAdAmountLimit adAmountLimit = null;
 	  	            
 	  	            try {
			 				
		 				adAmountLimit = adAmountLimitHome.findByAdcTypeAndAuTypeAndUsrName("INV STOCK TRANSFER", "REQUESTER", details.getStLastModifiedBy(), AD_CMPNY);       			
		 				
		 			} catch (FinderException ex) {
		 				
		 				throw new GlobalNoApprovalRequesterFoundException();
		 				
		 			}
		 			
		 			if (ABS_TOTAL_AMOUNT <= adAmountLimit.getCalAmountLimit()) {
		 				
		 				INV_APPRVL_STATUS = "N/A";
		 				
		 			} else {
		 			    
		 			 // for approval, create approval queue
		 			    		 				
	 				 Collection adAmountLimits = adAmountLimitHome.findByAdcTypeAndGreaterThanCalAmountLimit("INV STOCK TRANSFER", adAmountLimit.getCalAmountLimit(), AD_CMPNY);
	 				 
	 				 if (adAmountLimits.isEmpty()) {
	 				 	
	 				 	Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adAmountLimit.getCalCode(), AD_CMPNY);
	 				 	
	 				 	if (adApprovalUsers.isEmpty()) {
	 				 		
	 				 		throw new GlobalNoApprovalApproverFoundException();
	 				 		
	 				 	}
	 				 	        				 	
	 				 	Iterator j = adApprovalUsers.iterator();
	 				 	
	 				 	while (j.hasNext()) {
	 				 		
	 				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
	 				 		
	 				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV STOCK TRANSFER", 
	 				 				invStockTransfer.getStCode(), invStockTransfer.getStDocumentNumber(), invStockTransfer.getStDate(),
									adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
	 				 		
	 				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
	 				 		
	 				 	}        				 	
	 				 	        				 	
	 				 } else {
	 				 	
	 				 	boolean isApprovalUsersFound = false;
	 				 	
	 				 	Iterator i = adAmountLimits.iterator();
	 				 	        				 	
	 				 	while (i.hasNext()) {
	 				 		
	 				 		LocalAdAmountLimit adNextAmountLimit = (LocalAdAmountLimit)i.next();        				 		
	 				 		        				 		        				 		        				 		
	 				 		if (ABS_TOTAL_AMOUNT <= adNextAmountLimit.getCalAmountLimit()) {
	 				 			
	 				 			Collection adApprovalUsers = adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", 
	 				 					adAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
	 				 			
	 				 			Iterator j = adApprovalUsers.iterator();
		        				 	
		        				 	while (j.hasNext()) {
		        				 		
		        				 		isApprovalUsersFound = true;
		        				 		
		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
		        				 		
		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV STOCK TRANSFER", 
		        				 				invStockTransfer.getStCode(), invStockTransfer.getStDocumentNumber(), invStockTransfer.getStDate(), 
												adAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
		        				 		
		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
		        				 		
		        				 	}        				 			
	 				 			        				 			        				 			       				 			
	 				 			break;
		        				 	        				 			
	 				 		} else if (!i.hasNext()) {
	 				 			
	 				 			Collection adApprovalUsers = 
	 				 				adApprovalUserHome.findByAuTypeAndCalCode("APPROVER", adNextAmountLimit.getCalCode(), AD_CMPNY);        				 			           				 	
	 				 			
	 				 			Iterator j = adApprovalUsers.iterator();
		        				 	
		        				 	while (j.hasNext()) {
		        				 		
		        				 		isApprovalUsersFound = true;
		        				 		
		        				 		LocalAdApprovalUser adApprovalUser = (LocalAdApprovalUser)j.next();
		        				 		
		        				 		LocalAdApprovalQueue adApprovalQueue = adApprovalQueueHome.create(EJBCommon.TRUE, "INV STOCK TRANSFER", 
		        				 				invStockTransfer.getStCode(), invStockTransfer.getStDocumentNumber(), invStockTransfer.getStDate(),
												adNextAmountLimit.getCalAndOr(), adApprovalUser.getAuOr(), AD_BRNCH, AD_CMPNY);
		        				 		
		        				 		adApprovalUser.getAdUser().addAdApprovalQueue(adApprovalQueue);
		        				 		
		        				 	}        				 			
	 				 			        				 			        				 			       				 			
	 				 			break;
	 				 			
	 				 		}        				 		
	 				 		        				 		
	 				 		adAmountLimit = adNextAmountLimit;
	 				 		
	 				 	}
	 				 	
	 				 	if (!isApprovalUsersFound) {
	 				 		
	 				 		throw new GlobalNoApprovalApproverFoundException();
	 				 		
	 				 	}        				 
	 				 	
	 			    }
	 			    
	 			    INV_APPRVL_STATUS = "PENDING";
		 			    
		 			}
		 			
 	  	        }
 	  	         	  	        
 	  	    }
 	  	    
 	  	    if(INV_APPRVL_STATUS != null && INV_APPRVL_STATUS.equals("N/A")) { 
 	  	         	  	    
 	  	        this.executeInvStPost(invStockTransfer.getStCode(), invStockTransfer.getStLastModifiedBy(),
 	  	        		AD_BRNCH, AD_CMPNY,IfDraftToPost);
 	  	        
 	  	    }
        	
 	  	    // set stock transfer approval status
		 	
		 	invStockTransfer.setStApprovalStatus(INV_APPRVL_STATUS);
		 	
		 	return invStockTransfer.getStCode();
 	  	    

        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalDocumentNumberNotUniqueException ex) {
            
            getSessionContext().setRollbackOnly();
            throw ex;
        	      
        } catch (GlobalTransactionAlreadyApprovedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyPendingException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
         } catch (GlobalNoApprovalRequesterFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalNoApprovalApproverFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;  
        	
        } catch (GlobalInvItemLocationNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;  
        	
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex; 
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex; 
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;    
        	
        } catch (GlobalInventoryDateException ex) {
            
            getSessionContext().setRollbackOnly();
        	throw ex;    

		} catch(GlobalBranchAccountNumberInvalidException ex) {
			
            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (GlobalRecordInvalidException ex){
        	
            getSessionContext().setRollbackOnly();
            throw ex;

        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deleteInvStEntry(Integer ST_CODE, String AD_USR, Integer AD_CMPNY) throws 
        GlobalRecordAlreadyDeletedException {
                    
        Debug.print("InvStockTransferEntryControllerBean deleteInvStEntry");
        
        LocalInvStockTransferHome invStockTransferHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;        
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalAdDeleteAuditTrailHome adDeleteAuditTrailHome = null;

        // Initialize EJB Home
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);            
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adDeleteAuditTrailHome = (LocalAdDeleteAuditTrailHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdDeleteAuditTrailHome.JNDI_NAME, LocalAdDeleteAuditTrailHome.class); 

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalInvStockTransfer invStockTransfer =  invStockTransferHome.findByPrimaryKey(ST_CODE);
            
            Iterator i = invStockTransfer.getInvStockTransferLines().iterator();
            
            while(i.hasNext()) {
                
                LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine)i.next();
                
                String invLocation = this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationFrom());
                
                LocalInvItemLocation invItemLocation = invItemLocationHome.findByLocNameAndIiName(
                   invLocation, invStockTransferLine.getInvItem().getIiName(), AD_CMPNY);
                
                double convertedQuantity = this.convertByUomFromAndItemAndQuantity(
                       invStockTransferLine.getInvUnitOfMeasure(), 
                       invItemLocation.getInvItem(), 
                       Math.abs(invStockTransferLine.getStlQuantityDelivered()), AD_CMPNY);
                
                invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
            }
            
            if (invStockTransfer.getStApprovalStatus() != null && invStockTransfer.getStApprovalStatus().equals("PENDING")) {
            	
        		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV STOCK TRANSFER", invStockTransfer.getStCode(), AD_CMPNY);
        		
        		Iterator j = adApprovalQueues.iterator();
        		
        		while(j.hasNext()) {
        		
        			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)j.next();
        			
        			adApprovalQueue.remove();
        			
        		}
        	
        	}
            
            adDeleteAuditTrailHome.create("INV STOCK TRANSFER", invStockTransfer.getStDate(), invStockTransfer.getStDocumentNumber(), invStockTransfer.getStReferenceNumber(),
 					0d, AD_USR, new Date(), AD_CMPNY);
            
            invStockTransfer.remove();
            
        } catch (FinderException ex) {	
            
            getSessionContext().setRollbackOnly();
            throw new GlobalRecordAlreadyDeletedException();      	
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
     public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

        Debug.print("InvStockTransferEntryControllerBean getGlFcPrecisionUnit");

       
        LocalAdCompanyHome adCompanyHome = null;
             
        // Initialize EJB Home
         
        try {
             
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
               lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
             
        } catch (NamingException ex) {
             
            throw new EJBException(ex.getMessage());
             
        }

        try {
        	
          LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
             
          return  adCompany.getGlFunctionalCurrency().getFcPrecision();
                                      
        } catch (Exception ex) {
        	 
        	 Debug.printStackTrace(ex);
          throw new EJBException(ex.getMessage());
          
        }

     }
     
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpQuantityPrecisionUnit(Integer AD_CMPNY) {

         Debug.print("InvStockTransferEntryControllerBean getInvGpQuantityPrecisionUnit");
        
         LocalAdPreferenceHome adPreferenceHome = null;         
         
          // Initialize EJB Home
           
          try {
               
          	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                 lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
               
          } catch (NamingException ex) {
               
             throw new EJBException(ex.getMessage());
               
          }
         

          try {
         	
             LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            
             return adPreference.getPrfInvQuantityPrecisionUnit();
            
          } catch (Exception ex) {
           	 
             Debug.printStackTrace(ex);
             throw new EJBException(ex.getMessage());
            
          }

      }
    
     /**
      * @ejb:interface-method view-type="remote"
      * @jboss:method-attributes read-only="true"
      **/
      public short getInvGpInventoryLineNumber(Integer AD_CMPNY) {

         Debug.print("InvStockTransferEntryControllerBean getInvGpInventoryLineNumber");
                    
         LocalAdPreferenceHome adPreferenceHome = null;
        
        
         // Initialize EJB Home
          
         try {
              
         	adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
              
         } catch (NamingException ex) {
              
            throw new EJBException(ex.getMessage());
              
         }
        

         try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           
            return adPreference.getPrfInvInventoryLineNumber();
           
         } catch (Exception ex) {
          	 
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
           
         }
        
      }
      
      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/
      public ArrayList getAdApprovalNotifiedUsersByAdjCode(Integer ST_CODE, Integer AD_CMPNY) {
      	
      	Debug.print("InvStockTransferEntryControllerBean getAdApprovalNotifiedUsersByAdjCode");
      	
      	
      	LocalAdApprovalQueueHome adApprovalQueueHome = null;
      	LocalInvStockTransferHome invStockTransferHome = null;
      	
      	ArrayList list = new ArrayList();
      	
      	
      	// Initialize EJB Home
      	
      	try {
      		
      		adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);  
      		invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);   
      		
      	} catch (NamingException ex) {
      		
      		throw new EJBException(ex.getMessage());
      		
      	}
      	
      	try {
      		
      		LocalInvStockTransfer invStockTransfer = invStockTransferHome.findByPrimaryKey(ST_CODE);
      		
      		if (invStockTransfer.getStPosted() == EJBCommon.TRUE) {
      			
      			list.add("DOCUMENT POSTED");
      			return list;
      			
      		}
      		
      		Collection adApprovalQueues = adApprovalQueueHome.findByAqDocumentAndAqDocumentCode("INV STOCK TRANSFER", ST_CODE, AD_CMPNY);
      		
      		Iterator i = adApprovalQueues.iterator();
      		
      		while(i.hasNext()) {
      			
      			LocalAdApprovalQueue adApprovalQueue = (LocalAdApprovalQueue)i.next();
      			
      			list.add(adApprovalQueue.getAdUser().getUsrDescription());
      			
      		}
      		
      		return list;
      		
      	} catch (Exception ex) {
      		
      		Debug.printStackTrace(ex);
      		throw new EJBException(ex.getMessage());
      		
      	}
      	
      }
      
      /**
       * @ejb:interface-method view-type="remote"
       * @jboss:method-attributes read-only="true"
       **/ 
      public double getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(String II_NM, String LOC_FRM, String UOM_NM, Date ST_DT, Integer AD_BRNCH, Integer AD_CMPNY) throws
      GlobalInvItemLocationNotFoundException {
      	
      	Debug.print("InvStockTransferEntryControllerBean getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate");
      	
      	LocalInvItemHome invItemHome = null;
      	LocalInvItemLocationHome invItemLocationHome = null;
      	LocalInvCostingHome invCostingHome = null;      	
      	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
         
      	// Initialize EJB Home
      	
      	try {
      		
      		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
      		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class); 
      		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
      		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    			lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);  
    
      	} catch (NamingException ex) {
      		
      		throw new EJBException(ex.getMessage());
      		
      	}
      	
      	try {
      		
      		LocalInvItemLocation invItemLocation = null;
      	
      		try { 
      			
      			invItemLocation = invItemLocationHome.findByLocNameAndIiName(LOC_FRM, II_NM, AD_CMPNY);
      			
      		} catch (FinderException ex) {
      			
      			throw new GlobalInvItemLocationNotFoundException(String.valueOf(LOC_FRM + " for item " + II_NM));
      			
      		}
      		
      		double COST = invItemLocation.getInvItem().getIiUnitCost();
      		
      		try {
      			
      			LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
      					ST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
      			
      			if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Average"))
      			{
      				
      				if(invCosting.getCstRemainingQuantity()<=0) {
                        COST = invItemLocation.getInvItem().getIiUnitCost();
      					
      				}else {
      					COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
                         if(COST<=0){
                           COST = invCosting.getInvItemLocation().getInvItem().getIiUnitCost();
                        }  
      				}
      				
      			}else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("FIFO"))
      			{
      				COST = this.getInvFifoCost(ST_DT, invItemLocation.getIlCode(), invCosting.getCstAdjustQuantity(),
      						invCosting.getCstAdjustCost(), false, AD_BRNCH, AD_CMPNY);
     			}
      			else if(invCosting.getInvItemLocation().getInvItem().getIiCostMethod().equals("Standard"))
      			{
      				COST = invItemLocation.getInvItem().getIiUnitCost();
     			}

      			
      		} catch (FinderException ex) {
      			
      			COST = invItemLocation.getInvItem().getIiUnitCost();
      			
      		}
      		System.out.println("COST: " + COST);
      		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, UOM_NM, AD_CMPNY);
        	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(II_NM, invItemLocation.getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
        	System.out.println(EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY)));
        	return EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
        	
      	} catch (GlobalInvItemLocationNotFoundException ex) {
      		
      		throw ex;
      		
      	} catch (Exception ex) {
      		
      		Debug.printStackTrace(ex);
      		throw new EJBException(ex.getMessage());
      		
      	}
      	
      }
      
      // private methods
      
      private String getInvLocNameByLocCode(Integer LOC_CODE) {
      	
      	Debug.print("InvStockTransferEntryControllerBean getInvLocNameByLocCode");
      	
      	LocalInvLocationHome invLocationHome = null;
      	
      	try {
      		
      		invLocationHome = (LocalInvLocationHome)EJBHomeFactory.lookUpLocalHome(
      				LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
      		
      	} catch(NamingException ex) {
      		
      		throw new EJBException(ex.getMessage());
      		
      	}
      	
      	try {
      		
      		return invLocationHome.findByPrimaryKey(LOC_CODE).getLocName();
      		
      	} catch (Exception ex) {
      		
      		Debug.printStackTrace(ex);
      		throw new EJBException(ex.getMessage());
      		
      	}
      }
      
      
      private LocalInvStockTransferLine addInvStlEntry(InvModStockTransferLineDetails mdetails, 
      		LocalInvStockTransfer invStockTransfer, Integer AD_CMPNY) {
      	
		Debug.print("InvStockTransferEntryControllerBean addInvStlEntry");
		
		LocalInvStockTransferLineHome invStockTransferLineHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;		
		LocalInvLocationHome invLocationHome = null;
		LocalInvItemHome invItemHome = null;
	       	          
	    // Initialize EJB Home
	    
	    try {
	        
	    	invStockTransferLineHome = (LocalInvStockTransferLineHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvStockTransferLineHome.JNDI_NAME, LocalInvStockTransferLineHome.class); 
	    	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class); 	    	
	    	invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
	    		lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
	    	invItemHome = (LocalInvItemHome)EJBHomeFactory.
	    		lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }            
	            
	    try {
	    	
	    	LocalInvLocation invLocFrom = invLocationHome.findByLocName(mdetails.getStlLocationNameFrom(), AD_CMPNY);
	    	LocalInvLocation invLocTo = invLocationHome.findByLocName(mdetails.getStlLocationNameTo(), AD_CMPNY);
	    	
	    	LocalInvStockTransferLine invStockTransferLine = invStockTransferLineHome.create(invLocFrom.getLocCode(), 
	    	        	invLocTo.getLocCode(), mdetails.getStlUnitCost(), mdetails.getStlQuantityDelivered(), 
	    				mdetails.getStlAmount(), AD_CMPNY);
	    	
	    	invStockTransfer.addInvStockTransferLine(invStockTransferLine);
	    	
	    	LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
	    	        	mdetails.getStlUomName(), AD_CMPNY);
	    	
	    	invUnitOfMeasure.addInvStockTransferLine(invStockTransferLine);
	    	
	    	LocalInvItem invItem = invItemHome.findByIiName(mdetails.getStlIiName(), AD_CMPNY);
	    	
	    	invItem.addInvStockTransferLine(invStockTransferLine);	
	    	
	    	invStockTransferLine.setStlMisc(mdetails.getStlMisc());
	    	
	    	return invStockTransferLine;
	    							    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}	
    
    private double convertByUomFromAndItemAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
		
		Debug.print("InvStockTransferEntryControllerBean convertByUomFromAndUomToAndQuantity");		        
	    
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
                
	    // Initialize EJB Home
	    
	    try {
	        
	        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
	             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
	        invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
        		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
                    
	    } catch (NamingException ex) {
	        
	        throw new EJBException(ex.getMessage());
	        
	    }
	            
	    try {
	    
	        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
	        
	        LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
                        
        	return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());
	    						    		        		
	    } catch (Exception ex) {
	    	
	    	Debug.printStackTrace(ex);
	    	getSessionContext().setRollbackOnly();
	    	throw new EJBException(ex.getMessage());
	    	
	    }
		
	}	
    
private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("ArInvoicePostControllerBean convertByUomFromAndUomToAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }

    private void addInvDrEntry(short DR_LN, String DR_CLSS,
	    byte DR_DBT, double DR_AMNT, Integer COA_CODE, LocalInvStockTransfer invStockTransfer, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
			
		Debug.print("InvStockTransferEntryControllerBean addInvDrEntry");
		
		LocalAdCompanyHome adCompanyHome = null;
		LocalInvDistributionRecordHome invDistributionRecordHome = null;        
		LocalGlChartOfAccountHome glChartOfAccountHome = null;           
                
        // Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
        	invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
             
                        
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
                
        try {        
        	    
        	// get company
        	
        	LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
        	
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    		
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    		
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
        	        	        	
        	// create distribution record        
		    
		    LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
			    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
			    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
			    
		    invStockTransfer.addInvDistributionRecord(invDistributionRecord);
			glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
		
        } catch (GlobalBranchAccountNumberInvalidException ex) {
			
    		getSessionContext().setRollbackOnly();
    		throw ex;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	getSessionContext().setRollbackOnly();
        	throw new EJBException(ex.getMessage());
        	
        }
		
	}
    
    private void executeInvStPost(Integer ST_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY,boolean IfDraftToPost) throws
	    GlobalRecordAlreadyDeletedException,		
		GlobalTransactionAlreadyPostedException,
		GlJREffectiveDateNoPeriodExistException,
		GlJREffectiveDatePeriodClosedException,
		GlobalJournalNotBalanceException,
		GlobalBranchAccountNumberInvalidException,
		AdPRFCoaGlVarianceAccountNotFoundException {
        
        Debug.print("InvStockTransferEntryControllerBean executeInvStPost");
        
        LocalInvStockTransferHome invStockTransferHome = null;        
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalGlSetOfBookHome glSetOfBookHome = null;
        LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
        LocalInvDistributionRecordHome invDistributionRecordHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        LocalGlJournalBatchHome glJournalBatchHome = null;
        LocalGlJournalHome glJournalHome = null;
        LocalGlJournalSourceHome glJournalSourceHome = null;
        LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
        LocalGlJournalCategoryHome glJournalCategoryHome = null;
        LocalGlJournalLineHome glJournalLineHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
        
        // Initialize EJB Home
        
        try {
            
            invStockTransferHome = (LocalInvStockTransferHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvStockTransferHome.JNDI_NAME, LocalInvStockTransferHome.class);            
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
            glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.lookUpLocalHome(
                    LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.lookUpLocalHome(
                    LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);            
            glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
            glJournalHome = (LocalGlJournalHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
            glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
            glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
            glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
            glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.lookUpLocalHome(
                    LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
            glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.lookUpLocalHome(
                    LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
            
        } catch(NamingException ex) {
            
            throw new EJBException(ex.getMessage());
                     
        }
        
        try {
        	
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);

            
            // validate if stock transfer is already deleted
            
            LocalInvStockTransfer invStockTransfer = null;
            
            try {
                
                invStockTransfer = invStockTransferHome.findByPrimaryKey(ST_CODE);
                
            } catch (FinderException ex) {
                
                throw new GlobalRecordAlreadyDeletedException();
            }
            
            // validate if stock transfer is already posted or void
            
            if (invStockTransfer.getStPosted() == EJBCommon.TRUE) {
                
                throw new GlobalTransactionAlreadyPostedException();
                
            }
            
            // regenearte inventory dr
            
            boolean hasInsufficientItems = false;
    		String insufficientItems = "";
    		
    		if(IfDraftToPost){
    			this.regenerateInventoryDr(invStockTransfer, AD_BRNCH, AD_CMPNY);
    		
    		}
            
            
            Iterator i = invStockTransfer.getInvStockTransferLines().iterator();
            
            while (i.hasNext()) {
            	
            	LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();                               
            	
            	                
            	
            	double ST_QTY = this.convertByUomFromAndItemAndQuantity(
            			invStockTransferLine.getInvUnitOfMeasure(), 
						invStockTransferLine.getInvItem(), 
						invStockTransferLine.getStlQuantityDelivered(),
						AD_CMPNY);
            	
            	String II_NM = invStockTransferLine.getInvItem().getIiName();
            	
            	String LOC_NM_FRM = this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationFrom());
            	
            	LocalInvItemLocation invItemLocationFrom = invItemLocationHome.findByLocNameAndIiName(
            			LOC_NM_FRM, invStockTransferLine.getInvItem().getIiName(),AD_CMPNY);                
            	
            	
            	LocalInvCosting invCostingFrom = null;
            	
            	try {                    
            		
            		invCostingFrom = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
            				invStockTransfer.getStDate(), II_NM, LOC_NM_FRM, AD_BRNCH, AD_CMPNY);                                                          
            		
            	} catch (FinderException ex) {
            		
            	}          
            	
            	double COST = invStockTransferLine.getInvItem().getIiUnitCost();
            	
            	if (invCostingFrom == null) {
            		
            		this.postToInv(invStockTransferLine, invItemLocationFrom, invStockTransfer.getStDate(), 
            				-ST_QTY, -COST * ST_QTY, 
            				-ST_QTY, -COST * ST_QTY, 
            				0d, null, AD_BRNCH, AD_CMPNY);
            		
            	} else {

            		this.postToInv(invStockTransferLine, invItemLocationFrom, invStockTransfer.getStDate(), 
            				-ST_QTY, -COST * ST_QTY, 
            				invCostingFrom.getCstRemainingQuantity() - ST_QTY, invCostingFrom.getCstRemainingValue() - (COST * ST_QTY), 
            				0d, null, AD_BRNCH, AD_CMPNY);
        		
            	}
            	
            	
            	String LOC_NM_TO = this.getInvLocNameByLocCode(invStockTransferLine.getStlLocationTo());

            	LocalInvItemLocation invItemLocationTo = invItemLocationHome.findByLocNameAndIiName(
            			LOC_NM_TO, invStockTransferLine.getInvItem().getIiName(),AD_CMPNY);                                
            	
            	LocalInvCosting invCostingTo = null;
            	try {    
            		
            		invCostingTo = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
            				invStockTransfer.getStDate(), II_NM, LOC_NM_TO, AD_BRNCH, AD_CMPNY);    
            		
            	} catch (FinderException ex) {
            		
            	}            	
            	            	
            	if (invCostingTo == null) {
            		
            		this.postToInv(invStockTransferLine, invItemLocationTo, invStockTransfer.getStDate(), 
            				ST_QTY, COST * ST_QTY, 
            				ST_QTY, COST * ST_QTY,
            				0d, null, AD_BRNCH, AD_CMPNY);
            		
            	} else {

            		this.postToInv(invStockTransferLine, invItemLocationTo, invStockTransfer.getStDate(), 
            				ST_QTY, ST_QTY * COST, 
            				invCostingTo.getCstRemainingQuantity() + ST_QTY, invCostingTo.getCstRemainingValue() + (COST * ST_QTY), 
            				0d, USR_NM, AD_BRNCH, AD_CMPNY);
        		
            	}
            	
            }
            
            // set invoice post status
            
            invStockTransfer.setStPosted(EJBCommon.TRUE);
            invStockTransfer.setStPostedBy(USR_NM);
            invStockTransfer.setStDatePosted(EJBCommon.getGcCurrentDateWoTime().getTime());
            
            // post to GL
            
            //  validate if date has no period and period is closed
    		
    		LocalGlSetOfBook glJournalSetOfBook = null;
    		
    		try {
    			
    			glJournalSetOfBook = glSetOfBookHome.findByDate(invStockTransfer.getStDate(), AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlJREffectiveDateNoPeriodExistException();
    			
    		}
    		
    		LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    			glAccountingCalendarValueHome.findByAcCodeAndDate(glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invStockTransfer.getStDate(), AD_CMPNY);
    		
    		if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    				glAccountingCalendarValue.getAcvStatus() == 'C' ||
					glAccountingCalendarValue.getAcvStatus() == 'P') {
    			
    			throw new GlJREffectiveDatePeriodClosedException();
    			
    		}
    		
    		// check if debit and credit is balance
    		
    		LocalGlJournalLine glOffsetJournalLine = null;
    		
    		Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByStCode(invStockTransfer.getStCode(), AD_CMPNY);
    		
    		i = invDistributionRecords.iterator();
    		
    		double TOTAL_DEBIT = 0d;
    		double TOTAL_CREDIT = 0d;
    		
    		while (i.hasNext()) {
    			
    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			
    			double DR_AMNT = 0d;
    			
    			DR_AMNT = invDistributionRecord.getDrAmount();
    			
    			if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    				
    				TOTAL_DEBIT += DR_AMNT;
    				
    			} else {
    				
    				TOTAL_CREDIT += DR_AMNT;
    				
    			}
    		}
    		
    		TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());    		    		
    		
    		if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
        	    TOTAL_DEBIT != TOTAL_CREDIT) {
        	    	
        	    LocalGlSuspenseAccount glSuspenseAccount = null;
        	    	
        	    try { 	
        	    	
        	        glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "STOCK TRANSFERS", AD_CMPNY);
        	        
        	    } catch (FinderException ex) {
        	    	
        	    	throw new GlobalJournalNotBalanceException();
        	    	
        	    }
        	               	    
        	    if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
        	    	
        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
        	    	    EJBCommon.TRUE,TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
        	    	              	        
        	    } else {
        	    	
        	    	glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1),
        	    	    EJBCommon.FALSE,TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
        	    	
        	    }
        	    
        	    LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
        	    glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
        	    
        	    	
			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
			    TOTAL_DEBIT != TOTAL_CREDIT) {
			    
				throw new GlobalJournalNotBalanceException();		    	
			    	
			}
    		
    		// create journal batch if necessary
    		
    		LocalGlJournalBatch glJournalBatch = null;
    		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    		
    		try {
    			
    			glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) + " STOCK TRANSFERS", AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		if (glJournalBatch == null) {
    			
    			glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) + " STOCK TRANSFERS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(), USR_NM, AD_BRNCH, AD_CMPNY);
    			
    		}
    		
    		// create journal entry			            	
    		
    		LocalGlJournal glJournal = glJournalHome.create(invStockTransfer.getStReferenceNumber(),
    				invStockTransfer.getStDescription(), invStockTransfer.getStDate(),
					0.0d, null, invStockTransfer.getStDocumentNumber(), null, 1d, "N/A", null,
					'N', EJBCommon.TRUE, EJBCommon.FALSE, USR_NM, new Date(), USR_NM, new Date(), null, null,
					USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), null, null, EJBCommon.FALSE,
					null, 
					AD_BRNCH, AD_CMPNY);
    		
    		LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    		glJournal.setGlJournalSource(glJournalSource);
    		
    		LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    		glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    		
    		LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("STOCK TRANSFERS", AD_CMPNY);
    		glJournal.setGlJournalCategory(glJournalCategory);
    		
    		if (glJournalBatch != null) {
    			
    			glJournalBatch.addGlJournal(glJournal);
    			
    		}           		    
    		
    		// create journal lines
    		
    		i = invDistributionRecords.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			
    			double DR_AMNT = 0d;
    			
    			DR_AMNT = invDistributionRecord.getDrAmount();
    			
    			LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
						invDistributionRecord.getDrDebit(),DR_AMNT, "", AD_CMPNY);
    			
    			invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    			
    			glJournal.addGlJournalLine(glJournalLine);
    			
    			invDistributionRecord.setDrImported(EJBCommon.TRUE);
    			
    		}
    		
    		if (glOffsetJournalLine != null) {
    			
    			glJournal.addGlJournalLine(glOffsetJournalLine);
    			
    		}		
    		
    		// post journal to gl
    		
    		Collection glJournalLines = glJournal.getGlJournalLines();
    		
    		i = glJournalLines.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    			
    			// post current to current acv
    			
    			this.postToGl(glAccountingCalendarValue,
    					glJournalLine.getGlChartOfAccount(),
						true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    			    			
    			// post to subsequent acvs (propagate)
    			
    			Collection glSubsequentAccountingCalendarValues = 
    				glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
							glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    			
    			Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    			
    			while (acvsIter.hasNext()) {
    				
    				LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    					(LocalGlAccountingCalendarValue)acvsIter.next();
    				
    				this.postToGl(glSubsequentAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    			}
    			
    			// post to subsequent years if necessary
    			
    			Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    			
    			if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    				
    				adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    				LocalGlChartOfAccount glRetainedEarningsAccount = glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(), AD_BRNCH, AD_CMPNY); 	  	
    				
    				Iterator sobIter = glSubsequentSetOfBooks.iterator();
    				
    				while (sobIter.hasNext()) {
    					
    					LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    					
    					String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    					
    					// post to subsequent acvs of subsequent set of book(propagate)
    					
    					Collection glAccountingCalendarValues = 
    						glAccountingCalendarValueHome.findByAcCode(glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    					
    					Iterator acvIter = glAccountingCalendarValues.iterator();
    					
    					while (acvIter.hasNext()) {
    						
    						LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    							(LocalGlAccountingCalendarValue)acvIter.next();
    						
    						if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    								ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    							
    							this.postToGl(glSubsequentAccountingCalendarValue,
    									glJournalLine.getGlChartOfAccount(),
										false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    							
    						} else { 
    							// revenue & expense
    							
    							this.postToGl(glSubsequentAccountingCalendarValue,
    									glRetainedEarningsAccount,
										false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    							
    						}
    					}
    					
    					if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    					
    				}
    			}
    		}	
             
        } catch (GlJREffectiveDateNoPeriodExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlJREffectiveDatePeriodClosedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalJournalNotBalanceException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (GlobalTransactionAlreadyPostedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();
        	
        } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
    	
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
        
    }
    
    private double getInvFifoCost(Date CST_DT, Integer IL_CODE, double CST_QTY, double CST_COST, 
   		 boolean isAdjustFifo, Integer AD_BRNCH, Integer AD_CMPNY)
	 {
	  	 
		 LocalInvCostingHome invCostingHome = null;
	  	 LocalInvItemLocationHome invItemLocationHome = null;
	       
	     // Initialize EJB Home
	        
	     try {
	         
	    	 invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
	    	 invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
	    	 	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
	     } 
	     catch (NamingException ex) {
	            
	    	 throw new EJBException(ex.getMessage());
	     }
	    	
		try {
			
			Collection invFifoCostings = invCostingHome.findFifoRemainingQuantityByLessThanOrEqualCstDateAndIlCodeAndBrCode(CST_DT, IL_CODE, AD_BRNCH, AD_CMPNY);
			
			if (invFifoCostings.size() > 0) {
				
				Iterator x = invFifoCostings.iterator();
			
	  			if (isAdjustFifo) {
	  				
	  				//executed during POST transaction
	  				
	  				double totalCost = 0d;
	  				double cost;
	  				
	  				if(CST_QTY < 0) {
	  					
	  					//for negative quantities
	 	  				double neededQty = -(CST_QTY);
	 	  				
	 	  				while(x.hasNext() && neededQty != 0) {
	 	 	  				
	 	 	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	
	 		 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {  				
	 		 	  				cost = invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived();  				
	 		 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
	 		 	  				cost = invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold();
	 		 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {  				
	 		 	  				cost = invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity();
	 		 	  			} else {
	 		 	  				cost = invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity();
	 		 	  			}
	
	 	  					if(neededQty <= invFifoCosting.getCstRemainingLifoQuantity()) {
	 	  						
	 			  				invFifoCosting.setCstRemainingLifoQuantity(invFifoCosting.getCstRemainingLifoQuantity() - neededQty);
	 			  				totalCost += (neededQty * cost);
	 			  				neededQty = 0d;	 			  				 
	 	  					} else {
	 	  						
	 	  						neededQty -= invFifoCosting.getCstRemainingLifoQuantity();
	 	  						totalCost += (invFifoCosting.getCstRemainingLifoQuantity() * cost);
	 	  						invFifoCosting.setCstRemainingLifoQuantity(0);
	 	  					}
	 	  				}
	 	  				
	 	  				//if needed qty is not yet satisfied but no more quantities to fetch, get the default cost
	 	  				if(neededQty != 0) {
	 	  					
	 	  					LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
	 	  					totalCost += (neededQty * invItemLocation.getInvItem().getIiUnitCost());
	 	  				}
	 	  				
	 	  				cost = totalCost / -CST_QTY;
	  				} 
	  				
	  				else {
	  					
	  					//for positive quantities
	  					cost = CST_COST;
	  				}
	  				return cost;
	  			}
	  			
	  			else {
	  				
	  				//executed during ENTRY transaction
	  				
	  				LocalInvCosting invFifoCosting = (LocalInvCosting)x.next();
	  				
	 	  			if (invFifoCosting.getApPurchaseOrderLine() != null || invFifoCosting.getApVoucherLineItem() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstItemCost() / invFifoCosting.getCstQuantityReceived(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if(invFifoCosting.getArInvoiceLineItem() != null) {
		 	  			return  EJBCommon.roundIt(invFifoCosting.getCstCostOfSales() / invFifoCosting.getCstQuantitySold(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else if (invFifoCosting.getInvBuildUnbuildAssemblyLine() != null) {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAssemblyCost() / invFifoCosting.getCstAssemblyQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			} else {
	 	  				return EJBCommon.roundIt(invFifoCosting.getCstAdjustCost() /invFifoCosting.getCstAdjustQuantity(), this.getGlFcPrecisionUnit(AD_CMPNY));
	 	  			}
	  			}
			} 
			else {
				
				//most applicable in 1st entries of data
				LocalInvItemLocation invItemLocation = invItemLocationHome.findByPrimaryKey(IL_CODE);
				return invItemLocation.getInvItem().getIiUnitCost();
			}
				
		}
		catch (Exception ex) {
			Debug.printStackTrace(ex);
		    throw new EJBException(ex.getMessage());
		}
	}     

    
    private void postToInv(LocalInvStockTransferLine invStockTransferLine, LocalInvItemLocation invItemLocation, Date CST_DT,
    		double CST_ST_QTY, double CST_ST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM,
			Integer AD_BRNCH, Integer AD_CMPNY)throws 
			AdPRFCoaGlVarianceAccountNotFoundException {
        
    	Debug.print("InvStockTransferEntryControllerBean post");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	      	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              

        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);           
           int CST_LN_NMBR = 0;
           
           CST_ST_QTY = EJBCommon.roundIt(CST_ST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ST_CST = EJBCommon.roundIt(CST_ST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
                      
           if (CST_ST_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ST_QTY));
           
           }

           try {
           
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
           
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();
           
           while (i.hasNext()){
           	
           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
           	
           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){
        	   
           }

           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ST_QTY, CST_ST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ST_QTY > 0 ? CST_ST_QTY : 0, AD_BRNCH, AD_CMPNY);
           invItemLocation.addInvCosting(invCosting);
           invCosting.setInvStockTransferLine(invStockTransferLine);
           
//         Get Latest Expiry Dates          
           if(invStockTransferLine.getInvItem().getIiTraceMisc()!=0){
        	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
     			   System.out.println("apPurchaseOrderLine.getPlMisc(): "+invStockTransferLine.getStlMisc().length());

     			   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
     				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
     				   
     				   String miscList2Prpgt = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt, "False");
     				   ArrayList miscList = this.expiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt);
     				   String propagateMiscPrpgt = "";
     				   String ret = "";
     				  String check="";
     		            System.out.println("CST_ST_QTY Before Trans: " + CST_ST_QTY);
     				  //ArrayList miscList2 = null;
     				   if(CST_ST_QTY>0){
     					  prevExpiryDates = prevExpiryDates.substring(1);
     					   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
     				   }else{
     					   Iterator mi = miscList.iterator();
     					   propagateMiscPrpgt = prevExpiryDates;
     					   ret = propagateMiscPrpgt;
     					   String Checker = "";
     					   while(mi.hasNext()){
     						   String miscStr = (String)mi.next();

     						   ArrayList miscList2 = this.expiryDates("$"+ret, qtyPrpgt);
     						   Iterator m2 = miscList2.iterator();
     						   ret = "";
     						   String ret2 = "false";
     						   int a = 0;
     						   
     						   while(m2.hasNext()){
     							   String miscStr2 = (String)m2.next();

     							   if(ret2=="1st"){
     								   ret2 = "false";
     							   }

     							   if(miscStr2.equals(miscStr)){
     								   if(a==0){
     									   a = 1;
     									   ret2 = "1st";
     									   Checker = "true";
     								   }else{
     									   a = a+1;
     									   ret2 = "true";
     								   }
     							   }

     							   if(!miscStr2.equals(miscStr) || a>1){
     								   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
     									   if (miscStr2!=""){
     										   miscStr2 = "$" + miscStr2;
     										   ret = ret + miscStr2;
     										   ret2 = "false";
     									   }
     								   }
     							   }

     						   }
     						   ret = ret + "$";
     						   qtyPrpgt= qtyPrpgt -1;
     					   }
     					   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
     					   propagateMiscPrpgt = ret;
     					   System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
     					   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
     					   if(Checker=="true"){
     	  					  //invCosting.setCstExpiryDate(ret);
     	  					  System.out.println("check: " + check);
     	  				   }else{
     	  	  					   throw new GlobalExpiryDateNotFoundException();
     	  				   }
     				   
     				   }
     				 invCosting.setCstExpiryDate(propagateMiscPrpgt);
     				   
     			   }else{
     				   invCosting.setCstExpiryDate(prevExpiryDates);
     				   System.out.println("prevExpiryDates");
     			   }
               }else{
            	   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
            		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
         			   String initialPrpgt = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), initialQty, "False");
         			   
                 	   invCosting.setCstExpiryDate(initialPrpgt);
            	   }else{
            		   invCosting.setCstExpiryDate(prevExpiryDates);
     				   System.out.println("prevExpiryDates");
            	   }
             	   
                }
           }
          
 			   
           
			// if cost variance is not 0, generate cost variance for the transaction 
           if(CST_VRNC_VL != 0) {

        	   this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
        			   "INVST" + invStockTransferLine.getInvStockTransfer().getStDocumentNumber(),
        			   invStockTransferLine.getInvStockTransfer().getStDescription(),
        			   invStockTransferLine.getInvStockTransfer().getStDate(), USR_NM, AD_BRNCH, AD_CMPNY);

           }

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           i = invCostings.iterator();
           
           String miscList ="";
           ArrayList miscList2 = null;
           if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
        	   double qty = Double.parseDouble(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
               miscList = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), qty, "False");
               miscList2 = this.expiryDates(invStockTransferLine.getStlMisc(), qty);
           }
           
           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
           String ret = "";
           String Checker = "";

           System.out.println("CST_ST_QTY: " + CST_ST_QTY);

           while (i.hasNext()) {
           
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ST_QTY);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ST_CST);
               if(invStockTransferLine.getInvItem().getIiTraceMisc()!=0){
            	   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
            		   if(CST_ST_QTY>0){
            			   miscList = miscList.substring(1);
            			   propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
            			   System.out.println("propagateMiscPrpgt : "+propagateMisc);
            		   }else{
            			   Iterator mi = miscList2.iterator();

            			   propagateMisc = prevExpiryDates;
            			   ret = propagateMisc;
            			   while(mi.hasNext()){
            				   String miscStr = (String)mi.next();
            				   ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
            				   Iterator m2 = miscList3.iterator();
            				   ret = "";
            				   String ret2 = "false";
            				   int a = 0;
            				   while(m2.hasNext()){
            					   String miscStr2 = (String)m2.next();

            					   if(ret2=="1st"){
            						   ret2 = "false";
            					   }

            					   if(miscStr2.equals(miscStr)){
            						   if(a==0){
            							   a = 1;
            							   ret2 = "1st";
            							   Checker = "true";
            						   }else{
            							   a = a+1;
            							   ret2 = "true";
            						   }
            					   }

            					   if(!miscStr2.equals(miscStr) || a>1){
            						   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            							   if (miscStr2!=""){
            								   miscStr2 = "$" + miscStr2;
            								   ret = ret + miscStr2;
            								   ret2 = "false";
            							   }
            						   }
            					   }

            				   }
            				   ret = ret + "$";
            				   qtyPrpgt= qtyPrpgt -1;
            			   }
            			   propagateMisc = ret;
            			   System.out.println("propagateMiscPrpgt: " + propagateMisc);

            			   if(Checker=="true"){
            				   //invPropagatedCosting.setCstExpiryDate(propagateMisc);
            			   }else{
            				   throw new GlobalExpiryDateNotFoundException();
            			   }
            		   }

            		   invPropagatedCosting.setCstExpiryDate(propagateMisc);
            	   }else{
            		   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
            		   System.out.println("prevExpiryDates");
            	   }
               }
               

           }                           

           // regenerate cost varaince
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

       } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
       	
       	getSessionContext().setRollbackOnly();
       	throw ex;
       	
        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
    
    	
    
    }
    
    
    private void postQC(LocalInvStockTransferLine invStockTransferLine,LocalInvCosting invCosting1, LocalInvItemLocation invItemLocation, Date CST_DT,
    		double CST_ST_QTY, double CST_ST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, double CST_VRNC_VL, String USR_NM,
			Integer AD_BRNCH, Integer AD_CMPNY)throws 
			AdPRFCoaGlVarianceAccountNotFoundException {
        
    	Debug.print("InvStockTransferEntryControllerBean post");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
        // Initialize EJB Home
        
        try {
            
          invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
              lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
          adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
          adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
          invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
	      	lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);              

        } catch (NamingException ex) {
            
          throw new EJBException(ex.getMessage());
            
        }
      
        try {
        
           LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
           LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);           
           int CST_LN_NMBR = 0;
           
           CST_ST_QTY = EJBCommon.roundIt(CST_ST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_ST_CST = EJBCommon.roundIt(CST_ST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
           CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
           CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
                      
           if (CST_ST_QTY < 0) {
                      
           	   invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ST_QTY));
           
           }

           try {
           
           	   // generate line number
               LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
               CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
           
           } catch (FinderException ex) {
           
           	   CST_LN_NMBR = 1;
           
           }

           //void subsequent cost variance adjustments
           Collection invAdjustmentLines = invAdjustmentLineHome.findUnvoidAndIsCostVarianceGreaterThanAdjDateAndIlCodeAndBrCode(
           		CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
           Iterator i = invAdjustmentLines.iterator();
           
           while (i.hasNext()){
           	
           	LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();
           	this.voidInvAdjustment(invAdjustmentLine.getInvAdjustment(), AD_BRNCH, AD_CMPNY);
           	
           }

           String prevExpiryDates = "";
           String miscListPrpgt ="";
           double qtyPrpgt = 0;
           try {           
        	   LocalInvCosting prevCst = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIlCode(
        			   CST_DT, invItemLocation.getIlCode(), AD_BRNCH, AD_CMPNY);
        	   
        	   prevExpiryDates = prevCst.getCstExpiryDate();
        	   qtyPrpgt = prevCst.getCstRemainingQuantity();

        	   if (prevExpiryDates==null){
        		   prevExpiryDates="";
        	   }

           }catch (Exception ex){
        	   
           }

           // create costing
           LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d, CST_ST_QTY, CST_ST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, 0d, AD_BRNCH, AD_CMPNY);
           invItemLocation.addInvCosting(invCosting);
           invCosting.setInvStockTransferLine(invStockTransferLine);
           invCosting.setCstQCNumber(invCosting1.getCstQCNumber());
           invCosting.setCstQCExpiryDate(invCosting1.getCstQCExpiryDate());
           
           
//         Get Latest Expiry Dates          
           if(invStockTransferLine.getInvItem().getIiTraceMisc()!=0){
        	   if(prevExpiryDates!=null && prevExpiryDates!="" && prevExpiryDates.length()!=0){
     			   System.out.println("apPurchaseOrderLine.getPlMisc(): "+invStockTransferLine.getStlMisc().length());

     			   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
     				   int qty2Prpgt = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
     				   
     				   String miscList2Prpgt = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt, "False");
     				   ArrayList miscList = this.expiryDates(invStockTransferLine.getStlMisc(), qty2Prpgt);
     				   String propagateMiscPrpgt = "";
     				   String ret = "";
     				  String check="";
     		            System.out.println("CST_ST_QTY Before Trans: " + CST_ST_QTY);
     				  //ArrayList miscList2 = null;
     				   if(CST_ST_QTY>0){
     					  prevExpiryDates = prevExpiryDates.substring(1);
     					   propagateMiscPrpgt = miscList2Prpgt + prevExpiryDates;
     				   }else{
     					   Iterator mi = miscList.iterator();
     					   propagateMiscPrpgt = prevExpiryDates;
     					   ret = propagateMiscPrpgt;
     					   String Checker = "";
     					   while(mi.hasNext()){
     						   String miscStr = (String)mi.next();

     						   ArrayList miscList2 = this.expiryDates("$"+ret, qtyPrpgt);
     						   Iterator m2 = miscList2.iterator();
     						   ret = "";
     						   String ret2 = "false";
     						   int a = 0;
     						   
     						   while(m2.hasNext()){
     							   String miscStr2 = (String)m2.next();

     							   if(ret2=="1st"){
     								   ret2 = "false";
     							   }

     							   if(miscStr2.equals(miscStr)){
     								   if(a==0){
     									   a = 1;
     									   ret2 = "1st";
     									   Checker = "true";
     								   }else{
     									   a = a+1;
     									   ret2 = "true";
     								   }
     							   }

     							   if(!miscStr2.equals(miscStr) || a>1){
     								   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
     									   if (miscStr2!=""){
     										   miscStr2 = "$" + miscStr2;
     										   ret = ret + miscStr2;
     										   ret2 = "false";
     									   }
     								   }
     							   }

     						   }
     						   ret = ret + "$";
     						   qtyPrpgt= qtyPrpgt -1;
     					   }
     					   //propagateMiscPrpgt = propagateMiscPrpgt.replace(" ", "$");
     					   propagateMiscPrpgt = ret;
     					   System.out.println("propagateMiscPrpgt: " + propagateMiscPrpgt);
     					   //propagateMiscPrpgt = this.propagateExpiryDates(propagateMiscPrpgt, a, "True");
     					   if(Checker=="true"){
     	  					  //invCosting.setCstExpiryDate(ret);
     	  					  System.out.println("check: " + check);
     	  				   }else{
     	  	  					   throw new GlobalExpiryDateNotFoundException();
     	  				   }
     				   
     				   }
     				 invCosting.setCstExpiryDate(propagateMiscPrpgt);
     				   
     			   }else{
     				   invCosting.setCstExpiryDate(prevExpiryDates);
     				   System.out.println("prevExpiryDates");
     			   }
               }else{
            	   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
            		   int initialQty = Integer.parseInt(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
         			   String initialPrpgt = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), initialQty, "False");
         			   
                 	   invCosting.setCstExpiryDate(initialPrpgt);
            	   }else{
            		   invCosting.setCstExpiryDate(prevExpiryDates);
     				   System.out.println("prevExpiryDates");
            	   }
             	   
                }
           }
          
 			   
           
			// if cost variance is not 0, generate cost variance for the transaction 
           if(CST_VRNC_VL != 0) {

        	   this.generateCostVariance(invCosting.getInvItemLocation(), CST_VRNC_VL,
        			   "INVST" + invStockTransferLine.getInvStockTransfer().getStDocumentNumber(),
        			   invStockTransferLine.getInvStockTransfer().getStDescription(),
        			   invStockTransferLine.getInvStockTransfer().getStDate(), USR_NM, AD_BRNCH, AD_CMPNY);

           }

           // propagate balance if necessary           
           Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);

           i = invCostings.iterator();
           
           String miscList ="";
           ArrayList miscList2 = null;
           if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
        	   double qty = Double.parseDouble(this.getQuantityExpiryDates(invStockTransferLine.getStlMisc()));
               miscList = this.propagateExpiryDates(invStockTransferLine.getStlMisc(), qty, "False");
               miscList2 = this.expiryDates(invStockTransferLine.getStlMisc(), qty);
           }
           
           System.out.println("miscList Propagate:" + miscList);
           String propagateMisc ="";
           String ret = "";
           String Checker = "";

           System.out.println("CST_ST_QTY: " + CST_ST_QTY);

           while (i.hasNext()) {
           
               LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
               
               invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ST_QTY);
               invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ST_CST);
               if(invStockTransferLine.getInvItem().getIiTraceMisc()!=0){
            	   if(invStockTransferLine.getStlMisc()!=null && invStockTransferLine.getStlMisc()!="" && invStockTransferLine.getStlMisc().length()!=0){
            		   if(CST_ST_QTY>0){
            			   miscList = miscList.substring(1);
            			   propagateMisc = invPropagatedCosting.getCstExpiryDate() + miscList;
            			   System.out.println("propagateMiscPrpgt : "+propagateMisc);
            		   }else{
            			   Iterator mi = miscList2.iterator();

            			   propagateMisc = prevExpiryDates;
            			   ret = propagateMisc;
            			   while(mi.hasNext()){
            				   String miscStr = (String)mi.next();
            				   ArrayList miscList3 = this.expiryDates("$" + ret, qtyPrpgt);
            				   Iterator m2 = miscList3.iterator();
            				   ret = "";
            				   String ret2 = "false";
            				   int a = 0;
            				   while(m2.hasNext()){
            					   String miscStr2 = (String)m2.next();

            					   if(ret2=="1st"){
            						   ret2 = "false";
            					   }

            					   if(miscStr2.equals(miscStr)){
            						   if(a==0){
            							   a = 1;
            							   ret2 = "1st";
            							   Checker = "true";
            						   }else{
            							   a = a+1;
            							   ret2 = "true";
            						   }
            					   }

            					   if(!miscStr2.equals(miscStr) || a>1){
            						   if((ret2!="1st")||(ret2=="false")||(ret2=="true")){
            							   if (miscStr2!=""){
            								   miscStr2 = "$" + miscStr2;
            								   ret = ret + miscStr2;
            								   ret2 = "false";
            							   }
            						   }
            					   }

            				   }
            				   ret = ret + "$";
            				   qtyPrpgt= qtyPrpgt -1;
            			   }
            			   propagateMisc = ret;
            			   System.out.println("propagateMiscPrpgt: " + propagateMisc);

            			   if(Checker=="true"){
            				   //invPropagatedCosting.setCstExpiryDate(propagateMisc);
            			   }else{
            				   throw new GlobalExpiryDateNotFoundException();
            			   }
            		   }

            		   invPropagatedCosting.setCstExpiryDate(propagateMisc);
            	   }else{
            		   invPropagatedCosting.setCstExpiryDate(prevExpiryDates);
            		   System.out.println("prevExpiryDates");
            	   }
               }
               

           }                           

           // regenerate cost varaince
           this.regenerateCostVariance(invCostings, invCosting, AD_BRNCH, AD_CMPNY);

       } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
       	
       	getSessionContext().setRollbackOnly();
       	throw ex;
       	
        } catch (Exception ex) {
      	
      	   Debug.printStackTrace(ex);
      	   getSessionContext().setRollbackOnly();
           throw new EJBException(ex.getMessage());
         
        }
    
    	
    
    }
    
    public String getQuantityExpiryDates(String qntty){
    	String separator = "$";

    	// Remove first $ character
    	qntty = qntty.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = qntty.indexOf(separator, start);
    	int length = nextIndex - start;	
    	String y;
    	y = (qntty.substring(start, start + length));
    	System.out.println("Y " + y);
    	
    	return y;
    }
    
    private ArrayList expiryDates(String misc, double qty) throws Exception{
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	String separator ="$";
    	

    	// Remove first $ character
    	misc = misc.substring(1);

    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	

    	System.out.println("qty" + qty);
    	ArrayList miscList = new ArrayList();
		
    	for(int x=0; x<qty; x++) {
    		try{
//    			 Date
        		start = nextIndex + 1;
        		nextIndex = misc.indexOf(separator, start);
        		length = nextIndex - start;
        		System.out.println("x"+x);
        		String checker = misc.substring(start, start + length);
        		System.out.println("checker"+checker);
        		if(checker.length()!=0 || checker!="null"){
        			miscList.add(checker);
        		}else{
        			miscList.add("null");
        		}
    		}catch(Exception ex){
    			
    		}
    	}	
		
		System.out.println("miscList :" + miscList);
		return miscList;
    }
    
    public String propagateExpiryDates(String misc, double qty, String reverse) throws Exception {
    	//ActionErrors errors = new ActionErrors();
    	Debug.print("ApReceivingItemControllerBean getExpiryDates");
    	System.out.println("misc: " + misc);
    	
    	String separator = "";
    	if(reverse=="False"){
    		separator ="$";
    	}else{
    		separator =" ";
    	}
 
    	// Remove first $ character
    	misc = misc.substring(1);
    	System.out.println("misc: " + misc);
    	// Counter
    	int start = 0;
    	int nextIndex = misc.indexOf(separator, start);
    	int length = nextIndex - start;	
    	
    	String miscList = new String();
		
		for(int x=0; x<qty; x++) {
			
			// Date
			start = nextIndex + 1;
			nextIndex = misc.indexOf(separator, start);
			length = nextIndex - start;
			String g= misc.substring(start, start + length);
			System.out.println("g: " + g);
			System.out.println("g length: " + g.length());
				if(g.length()!=0){
					miscList = miscList + "$" + g;	
					System.out.println("miscList G: " + miscList);
				}
		}	
		
		miscList = miscList+"$";
		System.out.println("miscList :" + miscList);
		return (miscList);
    }
    
    
    private void postToGl(LocalGlAccountingCalendarValue glAccountingCalendarValue, 
    	LocalGlChartOfAccount glChartOfAccount, 
		boolean isCurrentAcv, byte isDebit, double JL_AMNT, Integer AD_CMPNY) {
    	
    	Debug.print("InvStockTransferEntryControllerBean postToGl");
    	
    	LocalGlChartOfAccountBalanceHome glChartOfAccountBalanceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		glChartOfAccountBalanceHome = (LocalGlChartOfAccountBalanceHome)EJBHomeFactory.
			    lookUpLocalHome(LocalGlChartOfAccountBalanceHome.JNDI_NAME, LocalGlChartOfAccountBalanceHome.class);    
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			    lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);                   
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {          
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);                       
    		
    		LocalGlChartOfAccountBalance glChartOfAccountBalance = glChartOfAccountBalanceHome.findByAcvCodeAndCoaCode(
    		    glAccountingCalendarValue.getAcvCode(), glChartOfAccount.getCoaCode(), AD_CMPNY);
    		
    		String ACCOUNT_TYPE = glChartOfAccount.getCoaAccountType();
    		short FC_EXTNDD_PRCSN = adCompany.getGlFunctionalCurrency().getFcPrecision();
    		
    		if (((ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("EXPENSE")) &&
    			isDebit == EJBCommon.TRUE) || (!ACCOUNT_TYPE.equals("ASSET") && 
    			!ACCOUNT_TYPE.equals("EXPENSE") && isDebit == EJBCommon.FALSE)) {				    
    			
    			glChartOfAccountBalance.setCoabEndingBalance(
    			    EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
    			
    			if (!isCurrentAcv) {
    				
    				glChartOfAccountBalance.setCoabBeginningBalance(
    				    EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() + JL_AMNT, FC_EXTNDD_PRCSN)); 					   
    				
    			}
    		} else {
    			
    			glChartOfAccountBalance.setCoabEndingBalance(
    					EJBCommon.roundIt(glChartOfAccountBalance.getCoabEndingBalance() - JL_AMNT, FC_EXTNDD_PRCSN));
    			
    			if (!isCurrentAcv) {
    				
    				glChartOfAccountBalance.setCoabBeginningBalance(
    				    EJBCommon.roundIt(glChartOfAccountBalance.getCoabBeginningBalance() - JL_AMNT, FC_EXTNDD_PRCSN)); 					   
    				
    			}
    		}
    		
    		if (isCurrentAcv) { 
    			
    			if (isDebit == EJBCommon.TRUE) {
    				
    				glChartOfAccountBalance.setCoabTotalDebit(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalDebit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
    				
    			} else {
    				
    				glChartOfAccountBalance.setCoabTotalCredit(
    						EJBCommon.roundIt(glChartOfAccountBalance.getCoabTotalCredit() + JL_AMNT, FC_EXTNDD_PRCSN));	 			
    			}       	   
    		}
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		throw new EJBException(ex.getMessage());
    		
    	}
    }
    
    private void regenerateInventoryDr(LocalInvStockTransfer invStockTransfer, Integer AD_BRNCH, Integer AD_CMPNY) throws
		GlobalInventoryDateException,
		GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvStockTransferEntryControllerBean regenerateInventoryDr");		        
    	
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalInvLocationHome invLocationHome = null;
    	LocalInvItemLocationHome invItemLocationHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
 				lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class); 
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
 				lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class); 
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
 				lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class); 
    		invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
 				lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class); 
    		invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		
    		// regenerate inventory distribution records
    		
    		Collection invDistributionRecords = invDistributionRecordHome.findImportableDrByStCode(
    			invStockTransfer.getStCode(), AD_CMPNY);
    		
    		Iterator i = invDistributionRecords.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    			
    			if(invDistributionRecord.getDrClass().equals("INVENTORY")){
    				
    				i.remove();
    				invDistributionRecord.remove();
    				
    			}
    			
    		}
    		
    		Collection invStockTransferLines = invStockTransfer.getInvStockTransferLines();
    		
    		i = invStockTransferLines.iterator();
            
            while(i.hasNext()) {
                
                LocalInvStockTransferLine invStockTransferLine = (LocalInvStockTransferLine) i.next();	                	                
                	                	
                LocalInvLocation invLocFrom = null;
                LocalInvLocation invLocTo = null;
                
                try {
	 	  	    	
 	  	    		invLocFrom = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationFrom());
 	  	    		invLocTo = invLocationHome.findByPrimaryKey(invStockTransferLine.getStlLocationTo());
 	  	    		
 	  	    	} catch (FinderException ex) {
 	  	    			
	 	  	    }
                
                LocalInvItemLocation invItemLocFrom = null;	                
                LocalInvItemLocation invItemLocTo = null;
                
                try {
	 	  	    	
 	  	    		invItemLocFrom = invItemLocationHome.findByLocNameAndIiName(
 	  	    			invLocFrom.getLocName(), invStockTransferLine.getInvItem().getIiName(), AD_CMPNY);
 	  	    		
 	  	    	} catch (FinderException ex) {
 	  	    	
 	  	    		throw new GlobalInvItemLocationNotFoundException(invStockTransferLine.getInvItem().getIiName() + " - " + invLocFrom.getLocName());
	 	  	    		
	 	  	    }
 	  	    	
 	  	    	try {
	 	  	    	
 	  	    		invItemLocTo = invItemLocationHome.findByLocNameAndIiName(
 	  	    		    invLocTo.getLocName(), invStockTransferLine.getInvItem().getIiName(), AD_CMPNY);
 	  	    		
 	  	    	} catch (FinderException ex) {
 	  	    	
 	  	    		throw new GlobalInvItemLocationNotFoundException(invStockTransferLine.getInvItem().getIiName() + " - " + invLocTo.getLocName());
	 	  	    		
	 	  	    }
                
                //	start date validation	                	              
 	  	    	if (adPreference.getPrfArAllowPriorDate() == EJBCommon.FALSE) {
 	  	    		Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
 	  	    				invStockTransfer.getStDate(), invItemLocFrom.getInvItem().getIiName(),
 	  	    				invItemLocFrom.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocFrom.getInvItem().getIiName());
 	  	    		
 	  	    		invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
 	  	    				invStockTransfer.getStDate(), invItemLocTo.getInvItem().getIiName(),
 	  	    				invItemLocTo.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
 	  	    		if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocTo.getInvItem().getIiName());
 	  	    	}
 	  	    	// add physical inventory distribution
                
	  	    	double COST = this.getInvIiUnitCostByIiNameAndLocFromAndUomNameAndDate(invItemLocFrom.getInvItem().getIiName(), invItemLocFrom.getInvLocation().getLocName(),
 	  	    			invStockTransferLine.getInvUnitOfMeasure().getUomName(), invStockTransfer.getStDate(),
						AD_BRNCH, AD_CMPNY);
                
      	    	double AMOUNT = 0d;
      	    	
                AMOUNT = EJBCommon.roundIt(invStockTransferLine.getStlQuantityDelivered() * COST, 
                        this.getGlFcPrecisionUnit(AD_CMPNY));
                
                // dr to locationTo
                
                // check branch mapping
                
                LocalAdBranchItemLocation adBranchItemLocation = null;
                
                try{
                	
                	adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                			invItemLocTo.getIlCode(), AD_BRNCH, AD_CMPNY);
                	
                } catch (FinderException ex) {
                	
                }

                LocalGlChartOfAccount glChartOfAccountTo = null;
                
                if (adBranchItemLocation == null) {

                	glChartOfAccountTo = glChartOfAccountHome.findByPrimaryKey(
                			invItemLocTo.getIlGlCoaInventoryAccount());

                } else {
                	
                	glChartOfAccountTo = glChartOfAccountHome.findByPrimaryKey(
                        	adBranchItemLocation.getBilCoaGlInventoryAccount());

                }

                this.addInvDrEntry(invStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE, 
                        Math.abs(AMOUNT), glChartOfAccountTo.getCoaCode(), invStockTransfer, AD_BRNCH, AD_CMPNY);

                // cr to locationFrom
                
                // check branch mapping
                
                try{
                	
                	adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
                			invItemLocFrom.getIlCode(), AD_BRNCH, AD_CMPNY);
                	
                } catch (FinderException ex) {
                	
                }

                LocalGlChartOfAccount glChartOfAccountFrom = null;
                
                if (adBranchItemLocation == null) {

                	glChartOfAccountFrom = glChartOfAccountHome.findByPrimaryKey(
                       	invItemLocFrom.getIlGlCoaInventoryAccount());

                } else {
                	
                	glChartOfAccountFrom = glChartOfAccountHome.findByPrimaryKey(
                        	adBranchItemLocation.getBilCoaGlInventoryAccount());

                }

                this.addInvDrEntry(invStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, 
                        Math.abs(AMOUNT), glChartOfAccountFrom.getCoaCode(), invStockTransfer, AD_BRNCH, AD_CMPNY);
                
            }
    	
    	
    	} catch (GlobalInventoryDateException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch(GlobalBranchAccountNumberInvalidException ex) {
			
			throw new GlobalBranchAccountNumberInvalidException ();

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void voidInvAdjustment(LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvStockTransferEntryControllerBean voidInvAdjustment");
    	
    	try{

    			Collection invDistributionRecords = invAdjustment.getInvDistributionRecords();
    			ArrayList list = new ArrayList();
    			
    			Iterator i = invDistributionRecords.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();
    				
    				list.add(invDistributionRecord);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)i.next();    	    			    			
    				
    				this.addInvDrEntry(invAdjustment.getInvDrNextLine(), invDistributionRecord.getDrClass(), 
    						invDistributionRecord.getDrDebit() == EJBCommon.TRUE ? EJBCommon.FALSE : EJBCommon.TRUE,
    								invDistributionRecord.getDrAmount(), EJBCommon.TRUE,
									invDistributionRecord.getInvChartOfAccount().getCoaCode(), invAdjustment, AD_BRNCH, AD_CMPNY);
    				
    			}	    
    			
    			Collection invAdjustmentLines = invAdjustment.getInvAdjustmentLines();
    			i = invAdjustmentLines.iterator();
    			list.clear();

    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();
    				
    				list.add(invAdjustmentLine);
    				
    			}
    			
    			i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine)i.next();    	    			    			
    				
    				this.addInvAlEntry(invAdjustmentLine.getInvItemLocation(),
    						invAdjustment, (invAdjustmentLine.getAlUnitCost()) * - 1, EJBCommon.TRUE, AD_CMPNY);

    			}

    			
    			invAdjustment.setAdjVoid(EJBCommon.TRUE);
    			
    			this.executeInvAdjPost(invAdjustment.getAdjCode(), invAdjustment.getAdjLastModifiedBy(), AD_BRNCH, AD_CMPNY);

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void generateCostVariance(LocalInvItemLocation invItemLocation, double CST_VRNC_VL, String ADJ_RFRNC_NMBR,
    		String ADJ_DSCRPTN, Date ADJ_DT, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
			AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("InvStockTransferEntryControllerBean generateCostVariance");
    	
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try{ 
    		
    		
    		
    		LocalInvAdjustment newInvAdjustment = this.saveInvAdjustment(ADJ_RFRNC_NMBR, ADJ_DSCRPTN, ADJ_DT, USR_NM, AD_BRNCH,
    				AD_CMPNY);
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalGlChartOfAccount glCoaVarianceAccount = null;
    		
    		
    		if(adPreference.getPrfInvGlCoaVarianceAccount() == null)
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    		
    		try{
    			
    			glCoaVarianceAccount = glChartOfAccountHome.findByPrimaryKey(adPreference.getPrfInvGlCoaVarianceAccount());
    			glCoaVarianceAccount.addInvAdjustment(newInvAdjustment);
    			
    		} catch (FinderException ex) {
    			
    			throw new AdPRFCoaGlVarianceAccountNotFoundException();
    			
    		}
    		
    		LocalInvAdjustmentLine invAdjustmentLine = this.addInvAlEntry(invItemLocation, newInvAdjustment,
    				CST_VRNC_VL, EJBCommon.FALSE, AD_CMPNY);
    		
    		// check for branch mapping
    		
    		LocalAdBranchItemLocation adBranchItemLocation = null;
    		
    		try{
    			
    			adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
    					invAdjustmentLine.getInvItemLocation().getIlCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		LocalGlChartOfAccount glInventoryChartOfAccount = null;
    		
    		if (adBranchItemLocation == null) {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					invAdjustmentLine.getInvItemLocation().getIlGlCoaInventoryAccount());
    		} else {
    			
    			glInventoryChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
    					adBranchItemLocation.getBilCoaGlInventoryAccount());
    			
    		}
    		
    		
    		boolean isDebit = CST_VRNC_VL < 0 ? false : true;
    		
    		//inventory dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(), "INVENTORY",
    				isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glInventoryChartOfAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		//variance dr
    		this.addInvDrEntry(newInvAdjustment.getInvDrNextLine(),"VARIANCE", 
    				!isDebit == true ? EJBCommon.TRUE : EJBCommon.FALSE, Math.abs(CST_VRNC_VL), EJBCommon.FALSE,
    						glCoaVarianceAccount.getCoaCode(), newInvAdjustment, AD_BRNCH, AD_CMPNY);
    		
    		this.executeInvAdjPost(newInvAdjustment.getAdjCode(), newInvAdjustment.getAdjLastModifiedBy(), AD_BRNCH,
    				AD_CMPNY);
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;

    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	*/
    }

    private void regenerateCostVariance(Collection invCostings, LocalInvCosting invCosting, Integer AD_BRNCH, Integer AD_CMPNY)
    throws AdPRFCoaGlVarianceAccountNotFoundException {
    	/*
    	Debug.print("InvStockTransferEntryControllerBean regenerateCostVariance");
    	
    	try {
    		
    		Iterator i = invCostings.iterator();
    		LocalInvCosting prevInvCosting = invCosting;
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			if(prevInvCosting.getCstRemainingQuantity() < 0) {
    				
    				double TTL_CST = 0;
    				double QNTY = 0;
    				String ADJ_RFRNC_NMBR = "";
    				String ADJ_DSCRPTN = "";
    				String ADJ_CRTD_BY = "";

    				// get unit cost adjusment, document number and unit of measure
    				if (invPropagatedCosting.getApPurchaseOrderLine() != null) {

    					TTL_CST = invPropagatedCosting.getApPurchaseOrderLine().getPlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApPurchaseOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApPurchaseOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApPurchaseOrderLine().getPlQuantity(), AD_CMPNY);
    					
    					ADJ_DSCRPTN = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoPostedBy();
    					ADJ_RFRNC_NMBR = "APRI" +
						invPropagatedCosting.getApPurchaseOrderLine().getApPurchaseOrder().getPoDocumentNumber();
    					
    				} else if (invPropagatedCosting.getApVoucherLineItem() != null){

    					TTL_CST = invPropagatedCosting.getApVoucherLineItem().getVliAmount();
						QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getApVoucherLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getApVoucherLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getApVoucherLineItem().getVliQuantity(), AD_CMPNY);
    					
    					if (invPropagatedCosting.getApVoucherLineItem().getApVoucher() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouPostedBy();
    						ADJ_RFRNC_NMBR = "APVOU" +
							invPropagatedCosting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber();
    						
    					} else if (invPropagatedCosting.getApVoucherLineItem().getApCheck() != null) {
    						
    						ADJ_DSCRPTN = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDescription();
    						ADJ_CRTD_BY = invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkPostedBy();
    						ADJ_RFRNC_NMBR = "APCHK" +
							invPropagatedCosting.getApVoucherLineItem().getApCheck().getChkDocumentNumber();
    						
    					}
    					
    				} else if (invPropagatedCosting.getArInvoiceLineItem() != null){

    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArInvoiceLineItem().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArInvoiceLineItem().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArInvoiceLineItem().getIliQuantity(), AD_CMPNY);
   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
   						
   						if(invPropagatedCosting.getArInvoiceLineItem().getArInvoice() != null){
   	   						
   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARCM" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArInvoice().getInvNumber();
   	    					
   	   						} else if(invPropagatedCosting.getArInvoiceLineItem().getArReceipt() != null){

   	   							ADJ_DSCRPTN = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctDescription();
   	   							ADJ_CRTD_BY = invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctPostedBy();
   	   							ADJ_RFRNC_NMBR = "ARMR" + 
   								invPropagatedCosting.getArInvoiceLineItem().getArReceipt().getRctNumber();

   	   						}
   	   						
    				} else if (invPropagatedCosting.getArSalesOrderInvoiceLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY = this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getArSalesOrderInvoiceLine().getArSalesOrderLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getArSalesOrderInvoiceLine().getSilQuantityDelivered(), AD_CMPNY);

    					ADJ_DSCRPTN = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvPostedBy();
    					ADJ_RFRNC_NMBR = "ARCM" + 
						invPropagatedCosting.getArSalesOrderInvoiceLine().getArInvoice().getInvNumber();
    					
    				} else if (invPropagatedCosting.getInvAdjustmentLine() != null){

    					ADJ_DSCRPTN = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjPostedBy();
    					ADJ_RFRNC_NMBR = "INVADJ" +
						invPropagatedCosting.getInvAdjustmentLine().getInvAdjustment().getAdjDocumentNumber();
    					
    					if(invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity() != 0) {
    						
    						TTL_CST = (invPropagatedCosting.getInvAdjustmentLine().getAlUnitCost() * 
    								invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity());
    						QNTY =  this.convertByUomFromAndItemAndQuantity(
    								invPropagatedCosting.getInvAdjustmentLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvAdjustmentLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvAdjustmentLine().getAlAdjustQuantity(), AD_CMPNY);
    						
    					}

    				} else if (invPropagatedCosting.getInvAssemblyTransferLine() != null){

    					TTL_CST = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleCost();
    					QNTY = invPropagatedCosting.getInvAssemblyTransferLine().getAtlAssembleQuantity();
    					ADJ_DSCRPTN = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrPostedBy();
    					ADJ_RFRNC_NMBR = "INVAT" +
						invPropagatedCosting.getInvAssemblyTransferLine().getInvAssemblyTransfer().getAtrDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvBranchStockTransferLine() != null){

    					if(invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstTransferOutNumber()
    							!= null) {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantityReceived(), AD_CMPNY);
							
    					} else {

    						TTL_CST = invPropagatedCosting.getInvBranchStockTransferLine().getBslAmount();
							QNTY =  this.convertByUomFromAndItemAndQuantity(
									invPropagatedCosting.getInvBranchStockTransferLine().getInvUnitOfMeasure(), 
									invPropagatedCosting.getInvBranchStockTransferLine().getInvItemLocation().getInvItem(),
									invPropagatedCosting.getInvBranchStockTransferLine().getBslQuantity(), AD_CMPNY);
    					}
    					
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstPostedBy();
    					ADJ_RFRNC_NMBR = "INVBST" + 
						invPropagatedCosting.getInvBranchStockTransferLine().getInvBranchStockTransfer().getBstNumber();
    					
    				} else if (invPropagatedCosting.getInvBuildUnbuildAssemblyLine() != null){

   						TTL_CST = prevInvCosting.getCstRemainingValue() - invPropagatedCosting.getCstRemainingValue();
    					QNTY =  invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getBlBuildQuantity();
    					ADJ_DSCRPTN =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDescription();
    					ADJ_CRTD_BY =
    						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaPostedBy();
    					ADJ_RFRNC_NMBR = "INVBUA" + 
						invPropagatedCosting.getInvBuildUnbuildAssemblyLine().getInvBuildUnbuildAssembly().getBuaDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockIssuanceLine()!= null){

    					TTL_CST = invPropagatedCosting.getInvStockIssuanceLine().getSilIssueCost();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockIssuanceLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockIssuanceLine().getInvItemLocation().getInvItem(),
								invPropagatedCosting.getInvStockIssuanceLine().getSilIssueQuantity(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiPostedBy();
    					ADJ_RFRNC_NMBR = "INVSI" +
						invPropagatedCosting.getInvStockIssuanceLine().getInvStockIssuance().getSiDocumentNumber();
    					
    				} else if (invPropagatedCosting.getInvStockTransferLine()!= null) {

    					TTL_CST = invPropagatedCosting.getInvStockTransferLine().getStlAmount();
						QNTY =  this.convertByUomFromAndItemAndQuantity(
								invPropagatedCosting.getInvStockTransferLine().getInvUnitOfMeasure(), 
								invPropagatedCosting.getInvStockTransferLine().getInvItem(),
								invPropagatedCosting.getInvStockTransferLine().getStlQuantityDelivered(), AD_CMPNY);
    					ADJ_DSCRPTN = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDescription();
    					ADJ_CRTD_BY = invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStPostedBy();
    					ADJ_RFRNC_NMBR = "INVST" +
						invPropagatedCosting.getInvStockTransferLine().getInvStockTransfer().getStDocumentNumber();
    					
    				} else {
    					
    					prevInvCosting = invPropagatedCosting;
    					continue;
    					
    				}
    				
    				// if quantity is equal 0, no variance.
    				if(QNTY == 0) continue;
    				
    				// compute new cost variance
    				double UNT_CST = TTL_CST/QNTY;
    				double CST_VRNC_VL = (invPropagatedCosting.getCstRemainingQuantity() * UNT_CST -
    						invPropagatedCosting.getCstRemainingValue());
    				
    				if(CST_VRNC_VL != 0)
    					this.generateCostVariance(invPropagatedCosting.getInvItemLocation(), CST_VRNC_VL, ADJ_RFRNC_NMBR,
    							ADJ_DSCRPTN, invPropagatedCosting.getCstDate(), ADJ_CRTD_BY, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// set previous costing
    			prevInvCosting = invPropagatedCosting;
    			
    		}
    		
    	} catch (AdPRFCoaGlVarianceAccountNotFoundException ex){
    		
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}    */    	
    }
    
    private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, byte DR_RVRSL, Integer COA_CODE, 
    		LocalInvAdjustment invAdjustment, Integer AD_BRNCH, Integer AD_CMPNY)
    
    throws GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvStockTransferEntryControllerBean addInvDrEntry");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;        
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;           
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
    		
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {        
    		
    		// get company
    		
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		
    		// validate coa
    		
    		LocalGlChartOfAccount glChartOfAccount = null;
    		
    		try {
    			
    			glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
    			
    		} catch(FinderException ex) {
    			
    			throw new GlobalBranchAccountNumberInvalidException ();
    			
    		}
    		
    		// create distribution record        
    		
    		LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(DR_LN, DR_CLSS, DR_DBT,
    				EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()), DR_RVRSL, EJBCommon.FALSE,
					AD_CMPNY);
    		
    		invAdjustment.addInvDistributionRecord(invDistributionRecord);
    		glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
    		
    	} catch(GlobalBranchAccountNumberInvalidException ex) {
    		
    		throw new GlobalBranchAccountNumberInvalidException ();
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }
    
    private void executeInvAdjPost(Integer ADJ_CODE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) throws
	GlobalRecordAlreadyDeletedException,		
	GlobalTransactionAlreadyPostedException,
	GlJREffectiveDateNoPeriodExistException,
	GlJREffectiveDatePeriodClosedException,
	GlobalJournalNotBalanceException,
	GlobalBranchAccountNumberInvalidException {
    	
    	Debug.print("InvStockTransferEntryControllerBean executeInvAdjPost");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;        
    	LocalAdCompanyHome adCompanyHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalGlSetOfBookHome glSetOfBookHome = null;
    	LocalGlAccountingCalendarValueHome glAccountingCalendarValueHome = null;
    	LocalGlJournalHome glJournalHome = null;
    	LocalGlJournalBatchHome glJournalBatchHome = null;
    	LocalGlSuspenseAccountHome glSuspenseAccountHome = null;
    	LocalGlJournalLineHome glJournalLineHome = null;
    	LocalGlJournalSourceHome glJournalSourceHome = null;
    	LocalGlJournalCategoryHome glJournalCategoryHome = null;
    	LocalGlFunctionalCurrencyHome glFunctionalCurrencyHome = null;
    	LocalInvDistributionRecordHome invDistributionRecordHome = null;
    	LocalInvCostingHome invCostingHome = null;
    	LocalGlChartOfAccountHome glChartOfAccountHome = null;
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		glSetOfBookHome = (LocalGlSetOfBookHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSetOfBookHome.JNDI_NAME, LocalGlSetOfBookHome.class);
    		glAccountingCalendarValueHome = (LocalGlAccountingCalendarValueHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlAccountingCalendarValueHome.JNDI_NAME, LocalGlAccountingCalendarValueHome.class);
    		glJournalHome = (LocalGlJournalHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalHome.JNDI_NAME, LocalGlJournalHome.class);
    		glJournalBatchHome = (LocalGlJournalBatchHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalBatchHome.JNDI_NAME, LocalGlJournalBatchHome.class);
    		glSuspenseAccountHome = (LocalGlSuspenseAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlSuspenseAccountHome.JNDI_NAME, LocalGlSuspenseAccountHome.class);
    		glJournalLineHome = (LocalGlJournalLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalLineHome.JNDI_NAME, LocalGlJournalLineHome.class);
    		glJournalSourceHome = (LocalGlJournalSourceHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalSourceHome.JNDI_NAME, LocalGlJournalSourceHome.class);
    		glJournalCategoryHome = (LocalGlJournalCategoryHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlJournalCategoryHome.JNDI_NAME, LocalGlJournalCategoryHome.class);
    		glFunctionalCurrencyHome = (LocalGlFunctionalCurrencyHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlFunctionalCurrencyHome.JNDI_NAME, LocalGlFunctionalCurrencyHome.class);
    		invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
			lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class);

    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try {
    		
    		// validate if adjustment is already deleted
    		
    		LocalInvAdjustment invAdjustment = null;
    		
    		try {
    			
    			invAdjustment = invAdjustmentHome.findByPrimaryKey(ADJ_CODE);
    			
    		} catch (FinderException ex) {
    			
    			throw new GlobalRecordAlreadyDeletedException();
    			
    		}
    		
    		// validate if adjustment is already posted or void
    		
    		if (invAdjustment.getAdjPosted() == EJBCommon.TRUE) {

    			if (invAdjustment.getAdjVoid() != EJBCommon.TRUE)
    			throw new GlobalTransactionAlreadyPostedException();
    			
    		}
    		
    		Collection invAdjustmentLines = null;
    		
    		if(invAdjustment.getAdjVoid() == EJBCommon.FALSE)
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.FALSE, invAdjustment.getAdjCode(), AD_CMPNY);
    		else
    			invAdjustmentLines = invAdjustmentLineHome.findByAlVoidAndAdjCode(EJBCommon.TRUE, invAdjustment.getAdjCode(), AD_CMPNY);
    		
    		Iterator i = invAdjustmentLines.iterator();
    		
    		while(i.hasNext()) {
    			
    			
    			LocalInvAdjustmentLine invAdjustmentLine = (LocalInvAdjustmentLine) i.next();

    			LocalInvCosting invCosting =
    				invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
    						invAdjustment.getAdjDate(), invAdjustmentLine.getInvItemLocation().getInvItem().getIiName(),
    						invAdjustmentLine.getInvItemLocation().getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    			
				this.postInvAdjustmentToInventory(invAdjustmentLine, invAdjustment.getAdjDate(), 0,
						invAdjustmentLine.getAlUnitCost(), invCosting.getCstRemainingQuantity(),
						invCosting.getCstRemainingValue() + invAdjustmentLine.getAlUnitCost(), AD_BRNCH, AD_CMPNY);
    			
    		}			
    		
    		// post to gl if necessary
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    			
    			// validate if date has no period and period is closed
    			
    			LocalGlSetOfBook glJournalSetOfBook = null;
    			
    			try {
    				
    				glJournalSetOfBook = glSetOfBookHome.findByDate(invAdjustment.getAdjDate(), AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    				throw new GlJREffectiveDateNoPeriodExistException();
    				
    			}
    			
    			LocalGlAccountingCalendarValue glAccountingCalendarValue = 
    				glAccountingCalendarValueHome.findByAcCodeAndDate(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcCode(), invAdjustment.getAdjDate(), AD_CMPNY);
    			
    			
    			if (glAccountingCalendarValue.getAcvStatus() == 'N' ||
    					glAccountingCalendarValue.getAcvStatus() == 'C' ||
						glAccountingCalendarValue.getAcvStatus() == 'P') {
    				
    				throw new GlJREffectiveDatePeriodClosedException();
    				
    			}
    			
    			// check if invoice is balance if not check suspense posting
    			
    			LocalGlJournalLine glOffsetJournalLine = null;
    			
    			Collection invDistributionRecords = null;
    			
    			if (invAdjustment.getAdjVoid() == EJBCommon.FALSE) {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.FALSE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			} else {
    				
    				invDistributionRecords = invDistributionRecordHome.findImportableDrByDrReversedAndAdjCode(EJBCommon.TRUE,
    						invAdjustment.getAdjCode(), AD_CMPNY);
    				
    			}

    			
    			Iterator j = invDistributionRecords.iterator();
    			
    			double TOTAL_DEBIT = 0d;
    			double TOTAL_CREDIT = 0d;
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				if (invDistributionRecord.getDrDebit() == EJBCommon.TRUE) {
    					
    					TOTAL_DEBIT += DR_AMNT;
    					
    				} else {
    					
    					TOTAL_CREDIT += DR_AMNT;
    					
    				}
    				
    			}
    			
    			TOTAL_DEBIT = EJBCommon.roundIt(TOTAL_DEBIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			TOTAL_CREDIT = EJBCommon.roundIt(TOTAL_CREDIT, adCompany.getGlFunctionalCurrency().getFcPrecision());
    			
    			if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.TRUE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				LocalGlSuspenseAccount glSuspenseAccount = null;
    				
    				try { 	
    					
    					glSuspenseAccount = glSuspenseAccountHome.findByJsNameAndJcName("INVENTORY", "INVENTORY ADJUSTMENTS",
    							AD_CMPNY);
    					
    				} catch (FinderException ex) {
    					
    					throw new GlobalJournalNotBalanceException();
    					
    				}
    				
    				if (TOTAL_DEBIT - TOTAL_CREDIT < 0) {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.TRUE, 
    							TOTAL_CREDIT - TOTAL_DEBIT, "", AD_CMPNY);
    					
    				} else {
    					
    					glOffsetJournalLine = glJournalLineHome.create((short)(invDistributionRecords.size() + 1), EJBCommon.FALSE,
    							TOTAL_DEBIT - TOTAL_CREDIT, "", AD_CMPNY);
    					
    				}
    				
    				LocalGlChartOfAccount glChartOfAccount = glSuspenseAccount.getGlChartOfAccount();
    				glChartOfAccount.addGlJournalLine(glOffsetJournalLine);
    				
    				
    			} else if (adPreference.getPrfAllowSuspensePosting() == EJBCommon.FALSE &&
    					TOTAL_DEBIT != TOTAL_CREDIT) {
    				
    				throw new GlobalJournalNotBalanceException();		    	
    				
    			}
    			
    			// create journal batch if necessary
    			
    			LocalGlJournalBatch glJournalBatch = null;
    			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
    			
    			try {
    				
    				glJournalBatch = glJournalBatchHome.findByJbName("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", AD_BRNCH, AD_CMPNY);
    				
    			} catch (FinderException ex) {
    				
    			}
    			
    			if (glJournalBatch == null) {
    				
    				glJournalBatch = glJournalBatchHome.create("JOURNAL IMPORT " + formatter.format(new Date()) +
    						" INVENTORY ADJUSTMENTS", "JOURNAL IMPORT", "CLOSED", EJBCommon.getGcCurrentDateWoTime().getTime(),
							USR_NM, AD_BRNCH, AD_CMPNY);
    				
    			}
    			
    			// create journal entry			            	
    			
    			LocalGlJournal glJournal = glJournalHome.create(invAdjustment.getAdjReferenceNumber(),
    					invAdjustment.getAdjDescription(), invAdjustment.getAdjDate(),
						0.0d, null, invAdjustment.getAdjDocumentNumber(), null, 1d, "N/A", null,
						'N', EJBCommon.TRUE, EJBCommon.FALSE,
						USR_NM, new Date(),
						USR_NM, new Date(),
						null, null,
						USR_NM, EJBCommon.getGcCurrentDateWoTime().getTime(), 
						null, null, EJBCommon.FALSE, null, 
						AD_BRNCH, AD_CMPNY);
    			
    			LocalGlJournalSource glJournalSource = glJournalSourceHome.findByJsName("INVENTORY", AD_CMPNY);
    			glJournal.setGlJournalSource(glJournalSource);
    			
    			LocalGlFunctionalCurrency glFunctionalCurrency = glFunctionalCurrencyHome.findByFcName(adCompany.getGlFunctionalCurrency().getFcName(), AD_CMPNY);
    			glJournal.setGlFunctionalCurrency(glFunctionalCurrency);
    			
    			LocalGlJournalCategory glJournalCategory = glJournalCategoryHome.findByJcName("INVENTORY ADJUSTMENTS", AD_CMPNY);
    			glJournal.setGlJournalCategory(glJournalCategory);
    			
    			if (glJournalBatch != null) {
    				
    				glJournal.setGlJournalBatch(glJournalBatch);
    				
    			}           		    
    			
    			// create journal lines
    			
    			j = invDistributionRecords.iterator();
    			
    			while (j.hasNext()) {
    				
    				LocalInvDistributionRecord invDistributionRecord = (LocalInvDistributionRecord)j.next();
    				
    				double DR_AMNT = 0d;
    				
    				DR_AMNT = invDistributionRecord.getDrAmount();
    				
    				LocalGlJournalLine glJournalLine = glJournalLineHome.create(invDistributionRecord.getDrLine(),	            			
    						invDistributionRecord.getDrDebit(), DR_AMNT, "", AD_CMPNY);
    				
    				invDistributionRecord.getInvChartOfAccount().addGlJournalLine(glJournalLine);
    				
    				glJournal.addGlJournalLine(glJournalLine);
    				
    				invDistributionRecord.setDrImported(EJBCommon.TRUE);
    				
    				
    			}
    			
    			if (glOffsetJournalLine != null) {
    				
    				glJournal.addGlJournalLine(glOffsetJournalLine);
    				
    			}		
    			
    			// post journal to gl
    			
    			Collection glJournalLines = glJournal.getGlJournalLines();
    			
    			i = glJournalLines.iterator();
    			
    			while (i.hasNext()) {
    				
    				LocalGlJournalLine glJournalLine = (LocalGlJournalLine)i.next();	
    				
    				// post current to current acv
    				
    				this.postToGl(glAccountingCalendarValue,
    						glJournalLine.getGlChartOfAccount(),
							true, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    				
    				
    				// post to subsequent acvs (propagate)
    				
    				Collection glSubsequentAccountingCalendarValues = 
    					glAccountingCalendarValueHome.findSubsequentAcvByAcCodeAndAcvPeriodNumber( 
    							glJournalSetOfBook.getGlAccountingCalendar().getAcCode(),
								glAccountingCalendarValue.getAcvPeriodNumber(), AD_CMPNY);     
    				
    				Iterator acvsIter = glSubsequentAccountingCalendarValues.iterator();
    				
    				while (acvsIter.hasNext()) {
    					
    					LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    						(LocalGlAccountingCalendarValue)acvsIter.next();
    					
    					this.postToGl(glSubsequentAccountingCalendarValue,
    							glJournalLine.getGlChartOfAccount(),
								false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    					
    				}
    				
    				// post to subsequent years if necessary
    				
    				Collection glSubsequentSetOfBooks = glSetOfBookHome.findSubsequentSobByAcYear(
    						glJournalSetOfBook.getGlAccountingCalendar().getAcYear(), AD_CMPNY);
    				
    				if (!glSubsequentSetOfBooks.isEmpty() && glJournalSetOfBook.getSobYearEndClosed() == 1) {
    					
    					adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);		  	  	
    					LocalGlChartOfAccount glRetainedEarningsAccount =
    						glChartOfAccountHome.findByCoaAccountNumberAndBranchCode(adCompany.getCmpRetainedEarnings(),
    								AD_BRNCH, AD_CMPNY); 	  	
    					
    					Iterator sobIter = glSubsequentSetOfBooks.iterator();
    					
    					while (sobIter.hasNext()) {
    						
    						LocalGlSetOfBook glSubsequentSetOfBook = (LocalGlSetOfBook)sobIter.next();
    						
    						String ACCOUNT_TYPE = glJournalLine.getGlChartOfAccount().getCoaAccountType();
    						
    						// post to subsequent acvs of subsequent set of book(propagate)
    						
    						Collection glAccountingCalendarValues = 
    							glAccountingCalendarValueHome.findByAcCode(
    									glSubsequentSetOfBook.getGlAccountingCalendar().getAcCode(), AD_CMPNY);     
    						
    						Iterator acvIter = glAccountingCalendarValues.iterator();
    						
    						while (acvIter.hasNext()) {
    							
    							LocalGlAccountingCalendarValue glSubsequentAccountingCalendarValue = 
    								(LocalGlAccountingCalendarValue)acvIter.next();
    							
    							if (ACCOUNT_TYPE.equals("ASSET") || ACCOUNT_TYPE.equals("LIABILITY") ||
    									ACCOUNT_TYPE.equals("OWNERS EQUITY")) {
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glJournalLine.getGlChartOfAccount(),
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);
    								
    							} else { // revenue & expense
    								
    								this.postToGl(glSubsequentAccountingCalendarValue,
    										glRetainedEarningsAccount,
											false, glJournalLine.getJlDebit(), glJournalLine.getJlAmount(), AD_CMPNY);					        
    								
    							}
    							
    						}
    						
    						if (glSubsequentSetOfBook.getSobYearEndClosed() == 0) break;
    						
    					}
    					
    				}
    				
    			}
			
			invAdjustment.setAdjPosted(EJBCommon.TRUE);

    	} catch (GlJREffectiveDateNoPeriodExistException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlJREffectiveDatePeriodClosedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalJournalNotBalanceException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalRecordAlreadyDeletedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (GlobalTransactionAlreadyPostedException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw ex;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    }

    private LocalInvAdjustmentLine addInvAlEntry(LocalInvItemLocation invItemLocation, LocalInvAdjustment invAdjustment,
    		double CST_VRNC_VL, byte AL_VD, Integer AD_CMPNY) {
    	
    	Debug.print("InvStockTransferEntryControllerBean addInvAlEntry");
    	
    	LocalInvAdjustmentLineHome invAdjustmentLineHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invAdjustmentLineHome = (LocalInvAdjustmentLineHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentLineHome.JNDI_NAME, LocalInvAdjustmentLineHome.class); 
    		
    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}            
    	
    	try {
    		
    		// create dr entry
    		LocalInvAdjustmentLine invAdjustmentLine = null;
    		invAdjustmentLine = invAdjustmentLineHome.create(CST_VRNC_VL,null,null, 0,0, AL_VD, AD_CMPNY);
    		
    		// map adjustment, unit of measure, item location
    		invAdjustment.addInvAdjustmentLine(invAdjustmentLine);	
    		invItemLocation.getInvItem().getInvUnitOfMeasure().addInvAdjustmentLine(invAdjustmentLine);
    		invItemLocation.addInvAdjustmentLine(invAdjustmentLine);
    		
    		return invAdjustmentLine;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    } 
    
    private LocalInvAdjustment saveInvAdjustment(String ADJ_RFRNC_NMBR, String ADJ_DSCRPTN,
    		Date ADJ_DATE, String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY){
    	
    	Debug.print("InvStockTransferEntryControllerBean saveInvAdjustment");
    	
    	LocalInvAdjustmentHome invAdjustmentHome = null;
    	LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;
    	LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
    	// Initialize EJB Home    	
    	
    	try{
    	
    		invAdjustmentHome = (LocalInvAdjustmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvAdjustmentHome.JNDI_NAME, LocalInvAdjustmentHome.class);
			adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
			adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class); 

    	} catch (NamingException ex) {
    		
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	try{
    		
    		// generate adj document number
    		String ADJ_DCMNT_NMBR = null;
    		
    		LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
    		LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
    		
    		try {
    			
    			adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV ADJUSTMENT", AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		try {
    			
    			adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(
    					adDocumentSequenceAssignment.getDsaCode(), AD_BRNCH, AD_CMPNY);
    			
    		} catch (FinderException ex) {
    			
    		}
    		
    		while (true) {
    			
    			if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(),
    							AD_BRNCH, AD_CMPNY);		            		
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adDocumentSequenceAssignment.getDsaNextSequence();
    					adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(
    							adDocumentSequenceAssignment.getDsaNextSequence()));	
    					break;
    					
    				}
    				
    			} else {
    				
    				try {
    					
    					invAdjustmentHome.findByAdjDocumentNumberAndBrCode(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence(), AD_BRNCH, AD_CMPNY);		            		
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));
    					
    				} catch (FinderException ex) {
    					
    					ADJ_DCMNT_NMBR = adBranchDocumentSequenceAssignment.getBdsNextSequence();
    					adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(
    							adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
    					break;
    					
    				}
    				
    			}
    			
    		}		            
    		
    		LocalInvAdjustment invAdjustment = invAdjustmentHome.create(ADJ_DCMNT_NMBR, ADJ_RFRNC_NMBR,
    				ADJ_DSCRPTN, ADJ_DATE, "COST-VARIANCE", "N/A", EJBCommon.FALSE, USR_NM, ADJ_DATE, USR_NM, ADJ_DATE, null, null,
					USR_NM, ADJ_DATE, null, null, EJBCommon.TRUE, EJBCommon.FALSE, AD_BRNCH, AD_CMPNY);
    		
    		return invAdjustment;
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}       
    	
    	
    }
    
    private void postInvAdjustmentToInventory(LocalInvAdjustmentLine invAdjustmentLine, Date CST_DT, double CST_ADJST_QTY,
    		double CST_ADJST_CST, double CST_RMNNG_QTY, double CST_RMNNG_VL, Integer AD_BRNCH,Integer AD_CMPNY) {
    	
    	Debug.print("InvStockTransferEntryControllerBean postInvAdjustmentToInventory");
    	
    	LocalInvCostingHome invCostingHome = null;
    	LocalAdPreferenceHome adPreferenceHome = null;
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
    	
    	try {
    		
    		invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
			lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
    		adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);
    		adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);              
    		
    	} catch (NamingException ex) {
    		
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	try {
    		
    		LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
    		LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
    		LocalInvItemLocation invItemLocation = invAdjustmentLine.getInvItemLocation();
    		int CST_LN_NMBR = 0;
    		
    		CST_ADJST_QTY = EJBCommon.roundIt(CST_ADJST_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_ADJST_CST = EJBCommon.roundIt(CST_ADJST_CST, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		CST_RMNNG_QTY = EJBCommon.roundIt(CST_RMNNG_QTY, adPreference.getPrfInvQuantityPrecisionUnit());
    		CST_RMNNG_VL = EJBCommon.roundIt(CST_RMNNG_VL, adCompany.getGlFunctionalCurrency().getFcPrecision());
    		
    		if (CST_ADJST_QTY < 0) {
    			
    			invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - Math.abs(CST_ADJST_QTY));
    			
    		}
    		
    		// create costing
    		
    		try {
    			
    			// generate line number
    			
    			LocalInvCosting invCurrentCosting = invCostingHome.getByMaxCstLineNumberAndCstDateToLongAndIiNameAndLocName(
    					CST_DT.getTime(), invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(),
						AD_BRNCH, AD_CMPNY);
    			CST_LN_NMBR = invCurrentCosting.getCstLineNumber() + 1;
    			
    		} catch (FinderException ex) {
    			
    			CST_LN_NMBR = 1;
    			
    		}
    		
    		LocalInvCosting invCosting = invCostingHome.create(CST_DT, CST_DT.getTime(), CST_LN_NMBR, 0d, 0d, 0d, 0d,
    				CST_ADJST_QTY, CST_ADJST_CST, 0d, 0d, CST_RMNNG_QTY, CST_RMNNG_VL, 0d, 0d, CST_ADJST_QTY > 0 ? CST_ADJST_QTY  : 0, AD_BRNCH, AD_CMPNY);
    		invItemLocation.addInvCosting(invCosting);
    		invCosting.setInvAdjustmentLine(invAdjustmentLine);
    		
    		// propagate balance if necessary           
    		
    		Collection invCostings = invCostingHome.findByGreaterThanCstDateAndIiNameAndLocName(CST_DT, invItemLocation.getInvItem().getIiName(), invItemLocation.getInvLocation().getLocName(), AD_BRNCH, AD_CMPNY);
    		
    		Iterator i = invCostings.iterator();
    		
    		while (i.hasNext()) {
    			
    			LocalInvCosting invPropagatedCosting = (LocalInvCosting)i.next();
    			
    			invPropagatedCosting.setCstRemainingQuantity(invPropagatedCosting.getCstRemainingQuantity() + CST_ADJST_QTY);
    			invPropagatedCosting.setCstRemainingValue(invPropagatedCosting.getCstRemainingValue() + CST_ADJST_CST);
    			
    		}                           
    		
    		
    	} catch (Exception ex) {
    		
    		Debug.printStackTrace(ex);
    		getSessionContext().setRollbackOnly();
    		throw new EJBException(ex.getMessage());
    		
    	}
    	
    	
    	
    }
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvStockTransferEntryControllerBean ejbCreate");
      
    }
          
}
