
/*
 * ArMiscReceiptsSyncControllerBean
 *
 * Created on November 9, 2005, 1:01 PM
 *
 * @author  Dann Ryan M. Hilario
 * 
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdAmountLimitHome;
import com.ejb.ad.LocalAdApprovalHome;
import com.ejb.ad.LocalAdApprovalQueueHome;
import com.ejb.ad.LocalAdApprovalUserHome;
import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignment;
import com.ejb.ad.LocalAdBranchDocumentSequenceAssignmentHome;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.InvATRAssemblyQtyGreaterThanAvailableQtyException;
import com.ejb.gl.LocalAdDocumentSequenceAssignment;
import com.ejb.gl.LocalAdDocumentSequenceAssignmentHome;
import com.ejb.gl.LocalGlChartOfAccount;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.inv.LocalInvBranchStockTransfer;
import com.ejb.inv.LocalInvBranchStockTransferHome;
import com.ejb.inv.LocalInvBranchStockTransferLine;
import com.ejb.inv.LocalInvBranchStockTransferLineHome;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvDistributionRecord;
import com.ejb.inv.LocalInvDistributionRecordHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvModBranchStockTransferDetails;
import com.util.InvModBranchStockTransferLineDetails;

/**
 * @ejb:bean name="InvTransferSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="InvTransferSync"
 *
 * @jboss:port-component uri="omega-ejb/InvTransferSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.InvTransferSyncWS"
 * 
 */

public class InvTransferSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	/**
	 * @ejb:interface-method
	 **/
	public int setInvTransferAllNewAndVoid(String[] newTransfers, String[] voidTransfers, String BR_BRNCH_CODE, Integer AD_CMPNY){
		
		
		LocalInvBranchStockTransferHome invBranchStockTransferHome = null;
        LocalAdDocumentSequenceAssignmentHome adDocumentSequenceAssignmentHome = null;        
        LocalInvItemLocationHome invItemLocationHome = null;
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        LocalAdApprovalHome adApprovalHome = null;
        LocalAdAmountLimitHome adAmountLimitHome = null;
        LocalAdApprovalUserHome adApprovalUserHome = null;
        LocalAdApprovalQueueHome adApprovalQueueHome = null;
        LocalInvCostingHome invCostingHome = null;
        LocalAdBranchDocumentSequenceAssignmentHome adBranchDocumentSequenceAssignmentHome = null;
        LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
        LocalAdBranchHome adBranchHome = null;
        LocalInvLocationHome invLocationHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        LocalAdBranch adBranchFrom = null;
        
        try {
            
            invBranchStockTransferHome = (LocalInvBranchStockTransferHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvBranchStockTransferHome.JNDI_NAME, LocalInvBranchStockTransferHome.class);            
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            adDocumentSequenceAssignmentHome = (LocalAdDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdDocumentSequenceAssignmentHome.class);
            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
            	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
            adApprovalHome = (LocalAdApprovalHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalHome.JNDI_NAME, LocalAdApprovalHome.class);
            adAmountLimitHome = (LocalAdAmountLimitHome)EJBHomeFactory.
           		lookUpLocalHome(LocalAdAmountLimitHome.JNDI_NAME, LocalAdAmountLimitHome.class);
            adApprovalUserHome = (LocalAdApprovalUserHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalUserHome.JNDI_NAME, LocalAdApprovalUserHome.class);
            adApprovalQueueHome = (LocalAdApprovalQueueHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdApprovalQueueHome.JNDI_NAME, LocalAdApprovalQueueHome.class);
            invCostingHome = (LocalInvCostingHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvCostingHome.JNDI_NAME, LocalInvCostingHome.class);
            adBranchDocumentSequenceAssignmentHome = (LocalAdBranchDocumentSequenceAssignmentHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchDocumentSequenceAssignmentHome.JNDI_NAME, LocalAdBranchDocumentSequenceAssignmentHome.class);
            adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class); 
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class); 
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            	lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class); 
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
        }
		
        short quantityPrecisionUnit = 0;
        
        int row = 0;
        
        
        
        quantityPrecisionUnit = this.getGlFcPrecisionUnit(AD_CMPNY);

        // Start of the giant try block
        try{
        	 
        	LocalInvBranchStockTransfer invBranchStockTransfer = null;
        	LocalAdBranch adBranch = adBranchHome.findByBrBranchCode(BR_BRNCH_CODE, AD_CMPNY);
        
			// new transfers
			if (!newTransfers[0].equals(null)){
				
				String[][] transfer = null;
				
				String[][][] transferLines = null;
				
				StringTokenizer tokenizer = null;
				
				
				for (int i = 0; i < newTransfers.length; i++){
				
					tokenizer = new StringTokenizer(newTransfers[i], "~", false);
					transfer[i] = transferDecode(tokenizer.nextToken(), AD_CMPNY);
					transferLines[i] = new String[tokenizer.countTokens()][];
					
					int count=0;
					
					while(tokenizer.hasMoreTokens()){
						transferLines[i][count] = transferLineDecode(tokenizer.nextToken());
						System.out.println(transferLines[i][count]);
						count++;
					}
		
				}
				
				do{
					InvModBranchStockTransferDetails details = new InvModBranchStockTransferDetails();
		    		
		    		details.setBstNumber(transfer[row][0]);
		    		details.setBstTransferOutNumber(transfer[row][1]);
		    		details.setBstType(transfer[row][2]);
		    		details.setBstDate(EJBCommon.convertStringToSQLDate(transfer[row][3]));
		    		details.setBstBranchTo(transfer[row][4]);
		    		details.setBstTransitLocation(transfer[row][4]);
					details.setBstDateLastModified(EJBCommon.convertStringToSQLDate(EJBCommon.convertSQLDateToString(new java.util.Date())));
					details.setBstDescription("");
		    		
		    		
		    		// line items
		    		ArrayList bslList = new ArrayList();
		    		
		    		for (int lines = 0; lines <transferLines.length; lines++){
			    		InvModBranchStockTransferLineDetails mdetails = new InvModBranchStockTransferLineDetails();
		    			
		    			mdetails.setBslLineNumber(Short.valueOf(transferLines[row][lines][0]).shortValue());
		    			mdetails.setBslQuantity(Double.valueOf(transferLines[row][lines][1]).doubleValue());
		    			mdetails.setBslUomName(transferLines[row][lines][2]);
		    			mdetails.setBslIiName(transferLines[row][lines][3]);
		    			mdetails.setBslLocationName(transferLines[row][lines][4]);
		    			
		    			Collection invItemLocations = invItemLocationHome.findByIiName(mdetails.getBslIiName(), AD_CMPNY);
		    			
		    			Iterator i = invItemLocations.iterator();
		    			while(i.hasNext()){
		    				LocalInvItemLocation invItemLocation = (LocalInvItemLocation)i.next();
		    				mdetails.setBslIiDescription(invItemLocation.getInvItem().getIiDescription());
		    			}
		    			
		    			bslList.add(mdetails);
		    			
		    		}
		    		
		    		//LocalInvBranchStockTransfer invBranchStockTransfer = null;
		    		
		    		 // validate if branch stock transfer is already deleted
		            try {
		                
		                if (details.getBstCode() != null) {
		                    
		                    invBranchStockTransfer = invBranchStockTransferHome.findByPrimaryKey(details.getBstCode());
		                    
		                }
		                
		            } catch (FinderException ex) {
		                
		                //throw new GlobalRecordAlreadyDeletedException();
		                
		            }
		            
		            // validate if branch stock transfer is already posted, void, approved or pending
		            if (details.getBstCode() != null) {
		                
		                if (invBranchStockTransfer.getBstApprovalStatus() != null) {
		                    
		                    if (invBranchStockTransfer.getBstApprovalStatus().equals("APPROVED") ||
		                            invBranchStockTransfer.getBstApprovalStatus().equals("N/A")) {         		    	
		                        
		                        throw new GlobalTransactionAlreadyApprovedException(); 
		                        
		                        
		                    } else if (invBranchStockTransfer.getBstApprovalStatus().equals("PENDING")) {
		                        
		                        throw new GlobalTransactionAlreadyPendingException();
		                        
		                    }
		                    
		                }
		                
		                if (invBranchStockTransfer.getBstPosted() == EJBCommon.TRUE) {
		                    
		                    throw new GlobalTransactionAlreadyPostedException();
		                    
		                }
		                
		            }
		            
		            LocalInvBranchStockTransfer invBranchExistingStockTransfer = null;
		            
		            try {
		                
		                invBranchExistingStockTransfer = invBranchStockTransferHome.findByBstNumberAndBrCode(details.getBstNumber(), adBranch.getBrCode(), AD_CMPNY);
		                
		            } catch (FinderException ex) {
		                
		            }
		            
		            // validate if document number is unique and if document number is automatic then set next sequence
		           
		            if (details.getBstCode() == null) {
		                
		                LocalAdBranchDocumentSequenceAssignment adBranchDocumentSequenceAssignment = null;
		                LocalAdDocumentSequenceAssignment adDocumentSequenceAssignment = null;
		                
		                if (invBranchExistingStockTransfer != null) {
		                    
		                    throw new GlobalDocumentNumberNotUniqueException();
		                    
		                }
		                 
		                 try {
		                 	 if (details.getBstType().equalsIgnoreCase("OUT")){
		                 	 	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BRANCH STOCK TRANSFER-OUT", AD_CMPNY);
		                 	 } else  if (details.getBstType().equalsIgnoreCase("IN")){
		                 	 	adDocumentSequenceAssignment = adDocumentSequenceAssignmentHome.findByDcName("INV BRANCH STOCK TRANSFER-IN", AD_CMPNY);
		                 	 }
		                     
		                 } catch (FinderException ex) {
		                     
		                 }
		                 
		                 try {
		                     
		                     adBranchDocumentSequenceAssignment = adBranchDocumentSequenceAssignmentHome.findBdsByDsaCodeAndBrCode(adDocumentSequenceAssignment.getDsaCode(), adBranch.getBrCode(), AD_CMPNY);
		                     
		                 } catch (FinderException ex) {
		                     
		                 }
		                 
		                 if (adDocumentSequenceAssignment.getAdDocumentSequence().getDsNumberingType() == 'A' && 
		                         (details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {
		                     
		                     while (true) {
		                         
		                         if (adBranchDocumentSequenceAssignment == null || adBranchDocumentSequenceAssignment.getBdsNextSequence() == null) {
		                             
		                             try {
		                                 
		                                 invBranchStockTransferHome.findByBstNumberAndBrCode(adDocumentSequenceAssignment.getDsaNextSequence(), adBranch.getBrCode(), AD_CMPNY);		            		
		                                 adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		                                 
		                             } catch (FinderException ex) {
		                                 
		                                 details.setBstNumber(adDocumentSequenceAssignment.getDsaNextSequence());
		                                 adDocumentSequenceAssignment.setDsaNextSequence(EJBCommon.incrementStringNumber(adDocumentSequenceAssignment.getDsaNextSequence()));	
		                                 break;
		                                 
		                             }
		                             
		                         } else {
		                             
		                             try {
		                                 
		                                 invBranchStockTransferHome.findByBstNumberAndBrCode(adBranchDocumentSequenceAssignment.getBdsNextSequence(), adBranch.getBrCode(), AD_CMPNY);		            		
		                                 adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));
		                                 
		                             } catch (FinderException ex) {
		                                 
		                                 details.setBstNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence());
		                                 adBranchDocumentSequenceAssignment.setBdsNextSequence(EJBCommon.incrementStringNumber(adBranchDocumentSequenceAssignment.getBdsNextSequence()));	
		                                 break;
		                                 
		                             }
		                             
		                         }
		                         
		                     }		            
		                     
		                 }
		                 
		             } else {
		                 
		                 if (invBranchExistingStockTransfer != null && 
		                         !invBranchExistingStockTransfer.getBstCode().equals(details.getBstCode())) {
		                     
		                     throw new GlobalDocumentNumberNotUniqueException();
		                     
		                 }
		                 
		                 if (invBranchStockTransfer.getBstNumber() != details.getBstNumber() &&
		                         (details.getBstNumber() == null || details.getBstNumber().trim().length() == 0)) {
		                     
		                     details.setBstNumber(invBranchStockTransfer.getBstNumber());
		                     
		                 }
		                 
		             }
		             
		            //Used in checking if branch stock transfer should re-generate distribution records
		             
		            boolean isRecalculate = true;
		            
		            //start ORDER type
		            if (details.getBstType().equalsIgnoreCase("ORDER")){	
		             	
		             	// create branch stock transfer
		                
		                if (details.getBstCode() == null) {
		                    
		                    invBranchStockTransfer = invBranchStockTransferHome.create(details.getBstDate(), 
		                            "ORDER", details.getBstNumber(), null, null, details.getBstDescription(),
		    						details.getBstApprovalStatus(), details.getBstPosted(), details.getBstReasonForRejection(),
		    						details.getBstCreatedBy(), details.getBstDateCreated(), details.getBstLastModifiedBy(),
		    						details.getBstDateLastModified(), details.getBstApprovedRejectedBy(), 
		                            details.getBstDateApprovedRejected(), details.getBstPostedBy(), details.getBstDatePosted(),
		                            EJBCommon.FALSE, EJBCommon.FALSE, adBranch.getBrCode(), AD_CMPNY);
		                    
		                } else {
		                    
		                    if (bslList.size() != invBranchStockTransfer.getInvBranchStockTransferLines().size() ||	        
		                            !(invBranchStockTransfer.getBstDate().equals(details.getBstDate()))) {
		                        
		                        isRecalculate = true;
		                        
		                    } else if (bslList.size() == invBranchStockTransfer.getInvBranchStockTransferLines().size()) {
		                        
		                        Iterator bslIter = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
		                        Iterator bslIterList = bslList.iterator();
		                        
		                        while(bslIter.hasNext()) {
		                            
		                            LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) bslIter.next();
		                            InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) bslIterList.next(); 
		                            
		                            
		                            if (!invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getBslLocationName()) || 
		                                    invBranchStockTransferLine.getBslUnitCost() != mdetails.getBslUnitCost() || 
		                                    invBranchStockTransferLine.getBslQuantity() != mdetails.getBslQuantity() ||
		                                    invBranchStockTransferLine.getBslAmount() != mdetails.getBslAmount() || 
		                                    !invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getBslIiName()) || 
		                                    !invBranchStockTransferLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getBslUomName())) {
		                                
		                                isRecalculate = true;
		                                break;
		                                
		                            }
		                            
		                            /* get item cost
		                            double COST = 0d;
		                            
		                            try {
		                                
		                                LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndRemainingQuantityNotEqualToZeroAndIiNameAndLocName(
		                                        invBranchStockTransfer.getBstDate(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), 
		    									invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), 
		    									adBranch.getBrCode(), AD_CMPNY);
		                                
		                                COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
		                                
		                            } catch (FinderException ex) {
		                                
		                                COST = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiUnitCost();
		                            }
		                            
		                            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
		    	                	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
		    	                	
		    	                	COST = EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
		    	               		
		                           	double AMOUNT = 0d;
		                            
		                            AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantity() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));	                	               
		                            
		                            if (invBranchStockTransferLine.getBslUnitCost() != COST) {
		                                
		                                mdetails.setBslUnitCost(COST);
		                                mdetails.setBslAmount(AMOUNT);
		                                
		                                isRecalculate = true;
		                                break;
		                                
		                            }
		                            */
		                            isRecalculate = false;
		                            
		                        }
		                        
		                    } else {
		                        
		                        isRecalculate = true;
		                        
		                    }
		                    
		                    invBranchStockTransfer.setBstType("ORDER");
		                    invBranchStockTransfer.setBstNumber(details.getBstNumber());
		                    invBranchStockTransfer.setBstDescription(details.getBstDescription());
		                    invBranchStockTransfer.setBstDate(details.getBstDate());
		                    invBranchStockTransfer.setBstApprovalStatus(details.getBstApprovalStatus());
		                    invBranchStockTransfer.setBstLastModifiedBy(details.getBstLastModifiedBy());
		                    invBranchStockTransfer.setBstDateLastModified(details.getBstDateLastModified());
		                    invBranchStockTransfer.setBstReasonForRejection(null);
		                    
		                }
		                adBranch = adBranchHome.findByBrName(details.getBstBranchFrom(), AD_CMPNY);
		                adBranch.addInvBranchStockTransfer(invBranchStockTransfer);
		                
		                LocalInvLocation invLocation = invLocationHome.findByLocName(details.getBstTransitLocation(), AD_CMPNY);
		                invLocation.addInvBranchStockTransfer(invBranchStockTransfer);
		             	
		             }	// end order type 
		           
	             	if (details.getBstType().equalsIgnoreCase("OUT")){	//start out type
		             	
		             	// create branch stock transfer
		                
		                if (details.getBstCode() == null) {
		                    
		                    invBranchStockTransfer = invBranchStockTransferHome.create(details.getBstDate(), 
		                            "OUT", details.getBstNumber(), null, details.getBstTransferOrderNumber(), details.getBstDescription(),
		    						details.getBstApprovalStatus(), details.getBstPosted(), details.getBstReasonForRejection(),
		    						details.getBstCreatedBy(), details.getBstDateCreated(), details.getBstLastModifiedBy(),
		    						details.getBstDateLastModified(), details.getBstApprovedRejectedBy(), 
		                            details.getBstDateApprovedRejected(), details.getBstPostedBy(), details.getBstDatePosted(),
		                            EJBCommon.FALSE, EJBCommon.FALSE, adBranch.getBrCode(), AD_CMPNY);
		                    
		                } else {
		                    
		                    if (bslList.size() != invBranchStockTransfer.getInvBranchStockTransferLines().size() ||	        
		                            !(invBranchStockTransfer.getBstDate().equals(details.getBstDate()))) {
		                        
		                        isRecalculate = true;
		                        
		                    } else if (bslList.size() == invBranchStockTransfer.getInvBranchStockTransferLines().size()) {
		                        
		                        Iterator bslIter = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
		                        Iterator bslIterList = bslList.iterator();
		                        
		                        while(bslIter.hasNext()) {
		                            
		                            LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) bslIter.next();
		                            InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) bslIterList.next(); 
		                            
		                            
		                            if (!invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getBslLocationName()) || 
		                                    invBranchStockTransferLine.getBslUnitCost() != mdetails.getBslUnitCost() || 
		                                    invBranchStockTransferLine.getBslQuantity() != mdetails.getBslQuantity() ||
		                                    invBranchStockTransferLine.getBslAmount() != mdetails.getBslAmount() || 
		                                    !invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName().equals(mdetails.getBslIiName()) || 
		                                    !invBranchStockTransferLine.getInvUnitOfMeasure().getUomName().equals(mdetails.getBslUomName())) {
		                                
		                                isRecalculate = true;
		                                break;
		                                
		                            }
		                            
		                            // get item cost
		                            double COST = 0d;
		                            
		                            try {
		                                
		                                LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
		                                        invBranchStockTransfer.getBstDate(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), 
		    									invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), 
		    									adBranch.getBrCode(), AD_CMPNY);
		                                
		                                COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
		                                
		                            } catch (FinderException ex) {
		                                
		                                COST = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiUnitCost();
		                            }
		                            
		                            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
		    	                	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
		    	                	
		    	                	COST = EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
		    	               		
		                           	double AMOUNT = 0d;
		                            
		                            AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantity() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));	                	               
		                            
		                            if (invBranchStockTransferLine.getBslUnitCost() != COST) {
		                                
		                                mdetails.setBslUnitCost(COST);
		                                mdetails.setBslAmount(AMOUNT);
		                                
		                                isRecalculate = true;
		                                break;
		                                
		                            }
		                            
		                            isRecalculate = false;
		                            
		                        }
		                        
		                    } else {
		                        
		                        isRecalculate = true;
		                        
		                    }
		                    
		                    invBranchStockTransfer.setBstType("OUT");
		                    invBranchStockTransfer.setBstNumber(details.getBstNumber());
		                    invBranchStockTransfer.setBstTransferOrderNumber(details.getBstTransferOrderNumber());
		                    invBranchStockTransfer.setBstDescription(details.getBstDescription());
		                    invBranchStockTransfer.setBstDate(details.getBstDate());
		                    invBranchStockTransfer.setBstApprovalStatus(details.getBstApprovalStatus());
		                    invBranchStockTransfer.setBstLastModifiedBy(details.getBstLastModifiedBy());
		                    invBranchStockTransfer.setBstDateLastModified(details.getBstDateLastModified());
		                    invBranchStockTransfer.setBstReasonForRejection(null);
		                    
		                }
		                adBranch = adBranchHome.findByBrName(details.getBstBranchTo(), AD_CMPNY);
		                adBranch.addInvBranchStockTransfer(invBranchStockTransfer);
		                
		                LocalInvLocation invLocation = invLocationHome.findByLocName(details.getBstTransitLocation(), AD_CMPNY);
		                invLocation.addInvBranchStockTransfer(invBranchStockTransfer);
		                // lock corresponding transfer order
		                
		                LocalInvBranchStockTransfer invBranchStockTransferOrder =
		                	invBranchStockTransferHome.findByBstNumberAndBrCode(invBranchStockTransfer.getBstTransferOrderNumber(),
		                			invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);
		                
		                invBranchStockTransferOrder.setBstLock(EJBCommon.TRUE);
		                
		                // get transfer out branch
	                
		                try {
		                	
		                	adBranchFrom = adBranchHome.findByPrimaryKey(invBranchStockTransferOrder.getBstAdBranch());
		                	
		                } catch(FinderException ex) {
		                	
		                }
		             }	// end out type
		             
		             if (details.getBstType().equalsIgnoreCase("IN")){	//start in type
		             
		             	if (details.getBstCode() == null) {
		                    
		                    invBranchStockTransfer = invBranchStockTransferHome.create(details.getBstDate(), 
		                            "IN", details.getBstNumber(), details.getBstTransferOutNumber(), null,
		                            details.getBstDescription(), details.getBstApprovalStatus(), details.getBstPosted(), 
		                            details.getBstReasonForRejection(), details.getBstCreatedBy(), details.getBstDateCreated(), 
		                            details.getBstLastModifiedBy(), details.getBstDateLastModified(), details.getBstApprovedRejectedBy(), 
		                            details.getBstDateApprovedRejected(), details.getBstPostedBy(), details.getBstDatePosted(),
		                            EJBCommon.FALSE, EJBCommon.FALSE, adBranch.getBrCode(), AD_CMPNY);
		                    
		                } else {
		                    
		                    if (bslList.size() != invBranchStockTransfer.getInvBranchStockTransferLines().size() ||	        
		                            !(invBranchStockTransfer.getBstDate().equals(details.getBstDate()))) {
		                        
		                        isRecalculate = true;
		                        
		                    } else if (bslList.size() == invBranchStockTransfer.getInvBranchStockTransferLines().size()) {
		                        
		                        Iterator bslIter = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
		                        Iterator bslIterList = bslList.iterator();
		                        
		                        while(bslIter.hasNext()) {
		                            
		                            LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) bslIter.next();
		                            InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) bslIterList.next(); 
		                            
		                            
		                            if (!invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName().equals(mdetails.getBslLocationName()) || 
		                                 invBranchStockTransferLine.getBslQuantityReceived() != mdetails.getBslQuantityReceived()) {
		                                
		                                isRecalculate = true;
		                                break;
		                                
		                            }
		                            
		                            isRecalculate = false;
		                            
		                        }
		                        
		                    } else {
		                        
		                        isRecalculate = true;
		                        
		                    }
		                    
		                    invBranchStockTransfer.setBstType("IN");
		                    invBranchStockTransfer.setBstNumber(details.getBstNumber());
		                    invBranchStockTransfer.setBstDescription(details.getBstDescription());
		                    invBranchStockTransfer.setBstDate(details.getBstDate());
		                    invBranchStockTransfer.setBstTransferOutNumber(details.getBstTransferOutNumber());
		                    invBranchStockTransfer.setBstApprovalStatus(details.getBstApprovalStatus());
		                    invBranchStockTransfer.setBstLastModifiedBy(details.getBstLastModifiedBy());
		                    invBranchStockTransfer.setBstDateLastModified(details.getBstDateLastModified());
		                    invBranchStockTransfer.setBstReasonForRejection(null);
		                    
		                }	 

		                // mapping to branch from
		                adBranch = adBranchHome.findByBrName(details.getBstBranchFrom(), AD_CMPNY);
		                adBranch.addInvBranchStockTransfer(invBranchStockTransfer);

		                // lock corresponding transfer out
		                
		                LocalInvBranchStockTransfer invBranchStockTransferOut =
		                	invBranchStockTransferHome.findByBstNumberAndBrCode(invBranchStockTransfer.getBstTransferOutNumber(),
		                			invBranchStockTransfer.getAdBranch().getBrCode(), AD_CMPNY);
		                
		                invBranchStockTransferOut.setBstLock(EJBCommon.TRUE);
		                
		                // get transfer out branch
	                
		                try {
		                	
		                	adBranchFrom = adBranchHome.findByPrimaryKey(invBranchStockTransferOut.getBstAdBranch());
		                	
		                } catch(FinderException ex) {
		                	
		                }
		    			
		                
		                LocalInvLocation invLocation = invLocationHome.findByLocName(details.getBstTransitLocation(), AD_CMPNY);
		                invLocation.addInvBranchStockTransfer(invBranchStockTransfer);
		                
		             }	//end in type
		             
		             double ABS_TOTAL_AMOUNT = 0d;
		             
		             if (isRecalculate) {
		                 
		                 // remove all branch stock transfer lines
		                 
		                 Iterator i = invBranchStockTransfer.getInvBranchStockTransferLines().iterator();
		                 
		                 short LINE_NUMBER = 0;
		                 
		                 while(i.hasNext()) {
		                     
		                     LINE_NUMBER++;
		                     
		                     LocalInvBranchStockTransferLine invBranchStockTransferLine = (LocalInvBranchStockTransferLine) i.next();
		                     
		                     LocalInvItemLocation invItemLocation = null;
		                     
		                     try {
		                         
		                         invItemLocation = invItemLocationHome.findByLocNameAndIiName(
		                                 invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), 
		                                 invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), AD_CMPNY);
		                         
		                     } catch(FinderException ex) {
		                         
		                         throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(LINE_NUMBER) + " - " + invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName());
		                         
		                     }
		                     double convertedQuantity=0d;
		                     
		                     if (details.getBstType().equalsIgnoreCase("OUT")){
		                     	
		                     	convertedQuantity = this.convertByUomAndQuantity(
		                            invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(), 
		                            invBranchStockTransferLine.getBslQuantity(), AD_CMPNY);
		                     	
		                     } else if (details.getBstType().equalsIgnoreCase("IN")){
		                     
		                     	convertedQuantity = this.convertByUomAndQuantity(
		                            invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(), 
		                            invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);
		                     	
		                     }
		                     
		                     invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() - convertedQuantity);
		                     
		                     i.remove();
		                     
		                     invBranchStockTransferLine.remove();
		                     
		                 }
		                
		                //	Remove all distribution records
		                
		                 i = invBranchStockTransfer.getInvDistributionRecords().iterator();
		                 
		                 while(i.hasNext()) {
		                     
		                     LocalInvDistributionRecord arDistributionRecord = (LocalInvDistributionRecord) i.next();
		                     
		                     i.remove();
		                     
		                     arDistributionRecord.remove();
		                     
		                 }
		                
		                //	Add new branch stock transfer entry lines and distribution record
		                
		                 byte DEBIT = 0;
		                 
		                 i = bslList.iterator();
		                 
		                 while(i.hasNext()) {
		                     
		                     InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();	                	                
		                     
		                     LocalInvItemLocation invItemLocation = null;
		                     
		                     try {
		                         
		                         invItemLocation = invItemLocationHome.findByLocNameAndIiName(
		                                 mdetails.getBslLocationName(), 
		                                 mdetails.getBslIiName(), AD_CMPNY);
		                         
		                     } catch (FinderException ex) {
		                         
		                         throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));
		                         
		                     }
		                     
		                     LocalInvItemLocation invItemTransitLocation = null;
		                     
		                     try {
		                         
		                         invItemTransitLocation = invItemLocationHome.findByLocNameAndIiName(
		                                 details.getBstTransitLocation(), 
		                                 mdetails.getBslIiName(), AD_CMPNY);
		                         
		                     } catch (FinderException ex) {
		                         
		                         throw new GlobalInvItemLocationNotFoundException("Transit Location " + String.valueOf(details.getBstTransitLocation()));
		                         
		                     }
		                     
		                     // start date validation
		                     Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		                             invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
		                             invItemLocation.getInvLocation().getLocName(), adBranch.getBrCode(), AD_CMPNY);
		                     if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
		                     
		                     
		                     LocalInvBranchStockTransferLine invBranchStockTransferLine = this.addInvBslEntry(mdetails, invBranchStockTransfer, AD_CMPNY);
		                     
		                    //	add physical inventory distribution
		                    
		                     double AMOUNT = 0d;
		                     
		                    if (details.getBstType().equalsIgnoreCase("OUT")){
        	                    
		                    	double COST = 0d;
		                        
		                        try {
		                            
		                            LocalInvCosting invCosting  = invCostingHome.getByMaxCstDateToLongAndMaxCstLineNumberAndLessThanEqualCstDateAndIiNameAndLocName(
		                                    invBranchStockTransfer.getBstDate(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), 
		    								invBranchStockTransferLine.getInvItemLocation().getInvLocation().getLocName(), 
		    								adBranch.getBrCode(), AD_CMPNY);
		                            
		                            COST = EJBCommon.roundIt(Math.abs(invCosting.getCstRemainingValue() / invCosting.getCstRemainingQuantity()), this.getGlFcPrecisionUnit(AD_CMPNY));
		                            
		                        } catch (FinderException ex) {
		                            
		                            COST = invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiUnitCost();
		                        }
		                   		
		                   		LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
		                    	LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invBranchStockTransferLine.getInvItemLocation().getInvItem().getIiName(), invBranchStockTransferLine.getInvItemLocation().getInvItem().getInvUnitOfMeasure().getUomName(), AD_CMPNY);
		                    	
		                    	COST = EJBCommon.roundIt(COST * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), this.getGlFcPrecisionUnit(AD_CMPNY));
		                   	    
		                        AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantity() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));	                	               
						        					        
					        } else if (details.getBstType().equalsIgnoreCase("IN")){
					        		
					        	double COST = invBranchStockTransferLine.getBslUnitCost();
			                    
			                    AMOUNT = EJBCommon.roundIt(invBranchStockTransferLine.getBslQuantityReceived() * COST, this.getGlFcPrecisionUnit(AD_CMPNY));	                	               
			                    
					        } 
		                    
		                    //	check branch mapping
		                    
		                    LocalAdBranchItemLocation adBranchItemLocation = null;
		                    
		                    try{
		                        
		                        adBranchItemLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
		                                invItemLocation.getIlCode(), adBranch.getBrCode(), AD_CMPNY);
		                        
		                        
		                    } catch (FinderException ex) {
		                        
		                    }
		                    
		                    LocalGlChartOfAccount glChartOfAccount = null;
		                    
		                    if (adBranchItemLocation == null) {
		                        
		                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
		                                invItemLocation.getIlGlCoaInventoryAccount());
		                        
		                    } else {
		                        
		                        glChartOfAccount = glChartOfAccountHome.findByPrimaryKey(
		                                adBranchItemLocation.getBilCoaGlInventoryAccount());
		                        
		                    }
		                    
		                    //	add dr for inventory
		                    
		                    if (details.getBstType().equalsIgnoreCase("OUT")){
		                    	this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, 
		                                Math.abs(AMOUNT), glChartOfAccount.getCoaCode(), invBranchStockTransfer, adBranch.getBrCode(), AD_CMPNY);
		                    }
		                    
		                    if (details.getBstType().equalsIgnoreCase("IN")){
		                    	this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE, 
		                                Math.abs(AMOUNT), glChartOfAccount.getCoaCode(), invBranchStockTransfer, adBranch.getBrCode(), AD_CMPNY);
		                    }
		                    
		                    //	check branch mapping for transit location
		                    
		                    LocalAdBranchItemLocation adBranchItemTransitLocation = null;
		                    
		                    try{
		                    	if (details.getBstType().equalsIgnoreCase("OUT")){
		                    		adBranchItemTransitLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
		                                    invItemTransitLocation.getIlCode(), adBranch.getBrCode(), AD_CMPNY);
		                    	} else if (details.getBstType().equalsIgnoreCase("IN")){
		                    		
		                    		adBranchItemTransitLocation = adBranchItemLocationHome.findBilByIlCodeAndBrCode(
		                                    invItemTransitLocation.getIlCode(), adBranchFrom.getBrCode(), AD_CMPNY);
		                    		
		                    	}
		                        
		                        
		                    } catch (FinderException ex) {
		                        
		                    }
		                    
		                    LocalGlChartOfAccount glChartOfAccountTransit = null;
		                    
		                    if (adBranchItemTransitLocation == null) {
		                        
		                        glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
		                                invItemTransitLocation.getIlGlCoaInventoryAccount());
		                        
		                    } else {
		                        
		                        glChartOfAccountTransit = glChartOfAccountHome.findByPrimaryKey(
		                                adBranchItemTransitLocation.getBilCoaGlInventoryAccount());
		                        
		                    }
		                    
		                    double convertedQuantity = 0d;
		                    
		                    if (details.getBstType().equalsIgnoreCase("OUT")){
		                    	
//		                    	 add dr for inventory transit location
		                        
		                        this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.TRUE, 
		                                Math.abs(AMOUNT), glChartOfAccountTransit.getCoaCode(), invBranchStockTransfer, adBranch.getBrCode(), AD_CMPNY);
		                        
		                        
		                        ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
		                        
		                        // set ilCommittedQuantity
		                        
		                        convertedQuantity = this.convertByUomAndQuantity(
		                                invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(), 
		                                invBranchStockTransferLine.getBslQuantity(), AD_CMPNY);
		                   
		                    } else if (details.getBstType().equalsIgnoreCase("IN")){
		                    	//	add dr for inventory transit location
		                        
		                    	this.addInvDrEntry(invBranchStockTransfer.getInvDrNextLine(), "INVENTORY", EJBCommon.FALSE, 
		                                Math.abs(AMOUNT), glChartOfAccountTransit.getCoaCode(), invBranchStockTransfer, adBranch.getBrCode(), AD_CMPNY);
		                        
		                        
		                        ABS_TOTAL_AMOUNT += Math.abs(AMOUNT);
		                        
		                        // set ilCommittedQuantity
		                        
		                        convertedQuantity = this.convertByUomAndQuantity(
		                                invBranchStockTransferLine.getInvUnitOfMeasure(), invItemLocation.getInvItem(), 
		                                invBranchStockTransferLine.getBslQuantityReceived(), AD_CMPNY);
		                        
		                    }
		                    
		                    invItemLocation.setIlCommittedQuantity(invItemLocation.getIlCommittedQuantity() + convertedQuantity);

		                }   
		                
		             } else {
		                
		                Iterator i = bslList.iterator();
		                
		                while(i.hasNext()) {
		                    
		                    InvModBranchStockTransferLineDetails mdetails = (InvModBranchStockTransferLineDetails) i.next();	                	                
		                    
		                    LocalInvItemLocation invItemLocation = null;	                
		                    
		                    try {
		                        
		                        invItemLocation = invItemLocationHome.findByLocNameAndIiName(
		                                mdetails.getBslLocationName(), 
		                                mdetails.getBslIiName(), AD_CMPNY);
		                        
		                    } catch (FinderException ex) {
		                        
		                        throw new GlobalInvItemLocationNotFoundException("Line " + String.valueOf(mdetails.getBslLineNumber() + " - " + mdetails.getBslLocationName()));
		                        
		                    }
		                    
		                    //	start date validation	                	              
		                    
		                    Collection invNegTxnCosting = invCostingHome.findNegTxnByGreaterThanCstDateAndIiNameAndLocName(
		                            invBranchStockTransfer.getBstDate(), invItemLocation.getInvItem().getIiName(),
		                            invItemLocation.getInvLocation().getLocName(), adBranch.getBrCode(), AD_CMPNY);
		                    if(!invNegTxnCosting.isEmpty()) throw new GlobalInventoryDateException(invItemLocation.getInvItem().getIiName());
		                    
		                    
		                    i = invBranchStockTransfer.getInvDistributionRecords().iterator();
		                    
		                    while(i.hasNext()) {
		                        
		                        LocalInvDistributionRecord distributionRecord = (LocalInvDistributionRecord)i.next();
		                        
		                        if(distributionRecord.getDrDebit() == 1) {
		                            
		                            ABS_TOTAL_AMOUNT += distributionRecord.getDrAmount();
		                            
		                        }
		                        
		                    }
		                    
		                }
		                
		            }
		                		                    
		             row++;

		        } while (row < transfer.length);
		             			

		             // void transfers
			
				if (!voidTransfers[0].equals(null)){
				
				//TODO:
		     
			}
						
		}

			
		return 0;
		
        // End of the giant try block
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            ctx.setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
		
	}
		
	
	private String[] transferDecode(String transfer, Integer AD_CMPNY){
		Debug.print("InvTransferSyncControllerBean transferDecode");
		
		String separator = new String(EJBCommon.SEPARATOR + "");
		String decodedResult[] = new String[14];
		
		StringTokenizer strToken = new StringTokenizer(transfer, separator, true);        
		
		String sep = strToken.nextToken();
		
		// transfer in number           
		decodedResult[0] = strToken.nextToken();
		if(decodedResult[0].equals(separator)) {
			decodedResult[0] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// transfer in number           
		decodedResult[1] = strToken.nextToken();
		if(decodedResult[1].equals(separator)) {
			decodedResult[1] = "";
		} else {
			sep = strToken.nextToken();
		}

		// transfer in number           
		decodedResult[2] = strToken.nextToken();
		if(decodedResult[2].equals(separator)) {
			decodedResult[2] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// date transferred           
		decodedResult[3] = strToken.nextToken();
		if(decodedResult[3].equals(separator)) {
			decodedResult[3] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// branch code           
		decodedResult[4] = strToken.nextToken();
		if(decodedResult[4].equals(separator)) {
			decodedResult[4] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// transit location           
		decodedResult[5] = strToken.nextToken();
		if(decodedResult[5].equals(separator)) {
			decodedResult[5] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		return  decodedResult;
	}
	
	private String[] transferLineDecode(String transferLines) {
		
		Debug.print("InvTransferSyncControllerBean transferLineDecode");
		
		String separator = new String(EJBCommon.SEPARATOR + "");
		String[] decodedResult= new String[4];
		
		StringTokenizer strToken = new StringTokenizer(transferLines, separator, true);
		
		String sep = strToken.nextToken();
		
		// line num        
		decodedResult[0] = strToken.nextToken();
		if(decodedResult[0].equals(separator)) {
			decodedResult[0] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// quantity        
		decodedResult[1] = strToken.nextToken();
		if(decodedResult[1].equals(separator)) {
			decodedResult[1] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// UOM
		decodedResult[2] = strToken.nextToken();
		if(decodedResult[2].equals(separator)) {
			decodedResult[2] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// item code        
		decodedResult[3] = strToken.nextToken();
		if(decodedResult[3].equals(separator)) {
			decodedResult[3] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		// location name        
		decodedResult[4] = strToken.nextToken();
		if(decodedResult[4].equals(separator)) {
			decodedResult[4] = "";
		} else {
			sep = strToken.nextToken();
		}
		
		return decodedResult;
	}
	
	
	   private short getGlFcPrecisionUnit(Integer AD_CMPNY) {
        
        Debug.print("InvBranchStockTransferOutEntryControllerBean getGlFcPrecisionUnit");
        
        
        LocalAdCompanyHome adCompanyHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
            
            return  adCompany.getGlFunctionalCurrency().getFcPrecision();
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            throw new EJBException(ex.getMessage());
            
        }
        
    }

	   private double convertByUomFromAndUomToAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvUnitOfMeasure invToUnitOfMeasure, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBranchStockTransferOutEntryControllerBean convertByUomFromAndUomToAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            return EJBCommon.roundIt(ADJST_QTY * invFromUnitOfMeasure.getUomConversionFactor() / invToUnitOfMeasure.getUomConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            ctx.setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	
	   
	   private LocalInvBranchStockTransferLine addInvBslEntry(InvModBranchStockTransferLineDetails mdetails, 
            LocalInvBranchStockTransfer invBranchStockTransfer, Integer AD_CMPNY) 
    	throws InvATRAssemblyQtyGreaterThanAvailableQtyException {
        
        Debug.print("InvBranchStockTransferInEntryControllerBean addInvBslEntry");
        
        LocalInvBranchStockTransferLineHome invBranchStockTransferLineHome = null;
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;		
        LocalInvLocationHome invLocationHome = null;
        LocalInvItemLocationHome invItemLocationHome= null;
        
        // Initialize EJB Home
        
        try {
            
            invBranchStockTransferLineHome = (LocalInvBranchStockTransferLineHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvBranchStockTransferLineHome.JNDI_NAME, LocalInvBranchStockTransferLineHome.class); 
            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class); 	    	
            invLocationHome = (LocalInvLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvLocationHome.JNDI_NAME, LocalInvLocationHome.class);
            invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
            lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }            
        
        try {

        	if(mdetails.getBslQuantity() < mdetails.getBslQuantityReceived())
        		throw new InvATRAssemblyQtyGreaterThanAvailableQtyException();

            LocalInvBranchStockTransferLine invBranchStockTransferLine = 
                invBranchStockTransferLineHome.create(mdetails.getBslQuantity(),mdetails.getBslQuantityReceived(),
                        mdetails.getBslUnitCost(),mdetails.getBslAmount(), AD_CMPNY);
            
            invBranchStockTransfer.addInvBranchStockTransferLine(invBranchStockTransferLine);
            
            LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(
                    mdetails.getBslUomName(), AD_CMPNY);
            
            invUnitOfMeasure.addInvBranchStockTransferLine(invBranchStockTransferLine);
            
            
            LocalInvItemLocation invItemLocation = invItemLocationHome.findByIiNameAndLocName(
                    mdetails.getBslIiName(), mdetails.getBslLocationName(), AD_CMPNY); 
            
            invItemLocation.addInvBranchStockTransferLine(invBranchStockTransferLine);
            
            return invBranchStockTransferLine;

        } catch (InvATRAssemblyQtyGreaterThanAvailableQtyException ex) {
            
            ctx.setRollbackOnly();
            throw ex;
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            ctx.setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }	

	   private void addInvDrEntry(short DR_LN, String DR_CLSS, byte DR_DBT, double DR_AMNT, 
	            Integer COA_CODE, LocalInvBranchStockTransfer invBranchStockTransfer, 
	            Integer AD_BRNCH, Integer AD_CMPNY)
	    throws GlobalBranchAccountNumberInvalidException {
	        
	        Debug.print("InvBranchStockTransferOutEntryControllerBean addInvDrEntry");
	        
	        LocalAdCompanyHome adCompanyHome = null;
	        LocalInvDistributionRecordHome invDistributionRecordHome = null;        
	        LocalGlChartOfAccountHome glChartOfAccountHome = null;           
	        
	        // Initialize EJB Home
	        
	        try {
	            
	            adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	            lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);    
	            invDistributionRecordHome = (LocalInvDistributionRecordHome)EJBHomeFactory.
	            lookUpLocalHome(LocalInvDistributionRecordHome.JNDI_NAME, LocalInvDistributionRecordHome.class);            
	            glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
	            lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);                       
	            
	            
	        } catch (NamingException ex) {
	            
	            throw new EJBException(ex.getMessage());
	            
	        }            
	        
	        try {        
	            
	            // get company
	            
	            LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	            
	            // validate coa
	            
	            LocalGlChartOfAccount glChartOfAccount = null;
	            
	            try {
	                
	                glChartOfAccount = glChartOfAccountHome.findByCoaCodeAndBranchCode(COA_CODE, AD_BRNCH, AD_CMPNY);
	                
	            } catch(FinderException ex) {
	                
	                throw new GlobalBranchAccountNumberInvalidException ();
	                
	            }
	            
	            // create distribution record        
	            
	            LocalInvDistributionRecord invDistributionRecord = invDistributionRecordHome.create(
	                    DR_LN, DR_CLSS, DR_DBT, EJBCommon.roundIt(DR_AMNT, adCompany.getGlFunctionalCurrency().getFcPrecision()),
	                    EJBCommon.FALSE, EJBCommon.FALSE, AD_CMPNY);
	            
	            invBranchStockTransfer.addInvDistributionRecord(invDistributionRecord);
	            glChartOfAccount.addInvDistributionRecord(invDistributionRecord);		   
	            
	        } catch (GlobalBranchAccountNumberInvalidException ex) {
	            
	            ctx.setRollbackOnly();
	            throw ex;
	            
	        } catch (Exception ex) {
	            
	            Debug.printStackTrace(ex);
	            ctx.setRollbackOnly();
	            throw new EJBException(ex.getMessage());
	            
	        }
	        
	    }
	   
	   private double convertByUomAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvItem invItem, double ADJST_QTY, Integer AD_CMPNY) {
        
        Debug.print("InvBranchStockTransferOutEntryControllerBean convertByUomFromAndUomToAndQuantity");		        
        
        LocalAdPreferenceHome adPreferenceHome = null;
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
        
        // Initialize EJB Home
        
        try {
            
            adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
            lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
            invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
    		lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);   
       
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
            LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
            
            LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invFromUnitOfMeasure.getUomName(), AD_CMPNY);
            LocalInvUnitOfMeasureConversion invDefaultUomConversion = invUnitOfMeasureConversionHome.findUmcByIiNameAndUomName(invItem.getIiName(), invItem.getInvUnitOfMeasure().getUomName(), AD_CMPNY);
            
            return EJBCommon.roundIt(ADJST_QTY * invDefaultUomConversion.getUmcConversionFactor() / invUnitOfMeasureConversion.getUmcConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
            
        } catch (Exception ex) {
            
            Debug.printStackTrace(ex);
            ctx.setRollbackOnly();
            throw new EJBException(ex.getMessage());
            
        }
        
    }
	   
	public void ejbCreate() throws CreateException {       
		Debug.print("InvTransferSyncControllerBean ejbCreate");        
	}                
	
	public void ejbRemove() {};
	
	public void ejbActivate() {}
	public void ejbPassivate() {}
	
	public void setSessionContext(SessionContext ctx) {}
}
