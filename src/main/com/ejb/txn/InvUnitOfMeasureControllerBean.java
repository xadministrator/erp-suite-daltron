package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.InvUOMOneBaseUnitIsAllowedException;
import com.ejb.exception.InvUOMShortNameAlreadyExistException;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvUnitOfMeasureDetails;

/**
 * @ejb:bean name="InvUnitOfMeasureControllerEJB"
 *           display-name="Used for entering unit of measures"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvUnitOfMeasureControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvUnitOfMeasureController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvUnitOfMeasureControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvUnitOfMeasureControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUomAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvUnitOfMeasureControllerBean getInvUomAll");

        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {

        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	        	
	        Collection invUnitOfMeasures = invUnitOfMeasureHome.findUomAll(AD_CMPNY);
	
	        if (invUnitOfMeasures.isEmpty()) {
	
	            throw new GlobalNoRecordFoundException();
	        	
	        }
	            
	        Iterator i = invUnitOfMeasures.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalInvUnitOfMeasure invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
	        	
	        	InvUnitOfMeasureDetails details = new InvUnitOfMeasureDetails();
	        		details.setUomCode(invUnitOfMeasure.getUomCode());
	        		details.setUomName(invUnitOfMeasure.getUomName());
	        		details.setUomDescription(invUnitOfMeasure.getUomDescription());
	        		details.setUomShortName(invUnitOfMeasure.getUomShortName());
	        		details.setUomAdLvClass(invUnitOfMeasure.getUomAdLvClass());
	        		details.setUomConversionFactor(invUnitOfMeasure.getUomConversionFactor());
	        		details.setUomBaseUnit(invUnitOfMeasure.getUomBaseUnit());
	        		details.setUomEnable(invUnitOfMeasure.getUomEnable());
	                	                	              				    	 
	        		list.add(details);
	        		
	        }              
	                                                        		        		        
	        return list;
	        
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addInvUomEntry(com.util.InvUnitOfMeasureDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException,
               InvUOMShortNameAlreadyExistException,
               InvUOMOneBaseUnitIsAllowedException {
                    
        Debug.print("InvUnitOfMeasureControllerBean addInvUomEntry");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
                                      
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
            LocalInvUnitOfMeasure invUnitOfMeasure = null;
        
	        try { 
	            
	        	invUnitOfMeasure = invUnitOfMeasureHome.findByUomName(details.getUomName(), AD_CMPNY);
	            
	            throw new GlobalRecordAlreadyExistException();
	            
	        } catch (FinderException ex) {
	         		        	 
	        }
	        
	        try { 
	            
	        	invUnitOfMeasure = invUnitOfMeasureHome.findByUomShortName(details.getUomShortName(), AD_CMPNY);
	            
	            throw new InvUOMShortNameAlreadyExistException();
	            
	        } catch (FinderException ex) {
	         		        	 	
	        }
	        
	        if (details.getUomBaseUnit() == EJBCommon.TRUE) {
	        	
	        	try {	
	        		
	        		invUnitOfMeasure = invUnitOfMeasureHome.findByUomAdLvClassAndBaseUnitEqualsTrue(details.getUomAdLvClass(), AD_CMPNY);
	        			        		
	        		throw new InvUOMOneBaseUnitIsAllowedException();
	        			
	        	} catch (FinderException ex) {	        		
	        		
	        	}
	        	
	        }
	        	        	        	      
	    	// create new unit of measure
	    				    	        	        	    	
	        invUnitOfMeasure = invUnitOfMeasureHome.create(details.getUomName(),
	    	        details.getUomDescription(), details.getUomShortName(), details.getUomAdLvClass(),
	    	        details.getUomConversionFactor(), details.getUomBaseUnit(), details.getUomEnable(), 'N', AD_CMPNY);
	        
	        // create new unit of measure conversion
	        
	        this.updateIiUomConversion(invUnitOfMeasure, AD_CMPNY);
	        
        	    		        	    		    		    			        	        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (InvUOMShortNameAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (InvUOMOneBaseUnitIsAllowedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updateInvUomEntry(com.util.InvUnitOfMeasureDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException, 
               InvUOMShortNameAlreadyExistException,
               InvUOMOneBaseUnitIsAllowedException {
                    
        Debug.print("InvUnitOfMeasureControllerBean updateInvUomEntry");
        
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
        
        LocalInvUnitOfMeasure invUnitOfMeasure = null;

        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {

            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        
        	LocalInvUnitOfMeasure invExistingUnitOfMeasure = null;
        	
        	try {
        	                   
	            invExistingUnitOfMeasure = invUnitOfMeasureHome.findByUomName(details.getUomName(), AD_CMPNY);
	            
	            if (!invExistingUnitOfMeasure.getUomCode().equals(details.getUomCode())) {
	            
	                 throw new GlobalRecordAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }
	        
        	try {
        	                   
	            invExistingUnitOfMeasure = invUnitOfMeasureHome.findByUomShortName(details.getUomShortName(), AD_CMPNY);
	            
	            if (!invExistingUnitOfMeasure.getUomCode().equals(details.getUomCode())) {
	            
	                 throw new InvUOMShortNameAlreadyExistException();
	                 
	            }
	            
	        } catch (FinderException ex) {
	        	
	        }	        
	        
	        if (details.getUomBaseUnit() == EJBCommon.TRUE) {
	        	
	        	try {
	        		
	        		invExistingUnitOfMeasure = invUnitOfMeasureHome.findByUomAdLvClassAndBaseUnitEqualsTrue(details.getUomAdLvClass(), AD_CMPNY);
	        		
	        		if (!invExistingUnitOfMeasure.getUomCode().equals(details.getUomCode())) {
	        		
	        			throw new InvUOMOneBaseUnitIsAllowedException();
	        			
	        		}
	        		
	        	} catch (FinderException ex) {	        		
	        		
	        	}
	        	
	        }	        
            
				// find and update unit of measure

			invUnitOfMeasure = invUnitOfMeasureHome.findByPrimaryKey(details.getUomCode());

				invUnitOfMeasure.setUomName(details.getUomName());
				invUnitOfMeasure.setUomDescription(details.getUomDescription());
				invUnitOfMeasure.setUomShortName(details.getUomShortName());
				invUnitOfMeasure.setUomAdLvClass(details.getUomAdLvClass());
				invUnitOfMeasure.setUomConversionFactor(details.getUomConversionFactor());
				invUnitOfMeasure.setUomBaseUnit(details.getUomBaseUnit());
				invUnitOfMeasure.setUomEnable(details.getUomEnable());
				
				if (invUnitOfMeasure.getUomDownloadStatus()=='N'){
					
					invUnitOfMeasure.setUomDownloadStatus('U');
					
				}else if (invUnitOfMeasure.getUomDownloadStatus()=='D'){
					
					invUnitOfMeasure.setUomDownloadStatus('X');
					
				}
          										    		    		        			    		    		        
        } catch (GlobalRecordAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (InvUOMShortNameAlreadyExistException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	
        	
        } catch (InvUOMOneBaseUnitIsAllowedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;        	
        	        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
   /**
    * @ejb:interface-method view-type="remote"
    **/
    public void deleteInvUomEntry(Integer UOM_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException,
                GlobalRecordAlreadyAssignedException {

        Debug.print("InvUnitOfMeasureControllerBean deleteInvUomEntry");
      
        LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;

        // Initialize EJB Home
        
        try {

            invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);           
      
        } catch (NamingException ex) {

            throw new EJBException(ex.getMessage());

        }
      
        try {
      	
      	    LocalInvUnitOfMeasure invUnitOfMeasure = null;                
      
	        try {
	      	
	            invUnitOfMeasure = invUnitOfMeasureHome.findByPrimaryKey(UOM_CODE);
	         
	        } catch (FinderException ex) {
	      	
	            throw new GlobalRecordAlreadyDeletedException();
	            
	        }
	         
	        if (!invUnitOfMeasure.getInvItems().isEmpty() || 
	            !invUnitOfMeasure.getInvBuildUnbuildAssemblyLines().isEmpty() ||
	            !invUnitOfMeasure.getInvAdjustmentLines().isEmpty() || 
	            !invUnitOfMeasure.getApPurchaseOrderLines().isEmpty() ||
				!invUnitOfMeasure.getApPurchaseRequisitionLines().isEmpty() || 
				!invUnitOfMeasure.getApVoucherLineItems().isEmpty() ||
				!invUnitOfMeasure.getArInvoiceLineItems().isEmpty() || 
				!invUnitOfMeasure.getArSalesOrderLines().isEmpty() ||
				!invUnitOfMeasure.getInvBranchStockTransferLines().isEmpty() || 
				!invUnitOfMeasure.getInvOverheadMemoLines().isEmpty() || 
				!invUnitOfMeasure.getInvOverheadLines().isEmpty() ||
				!invUnitOfMeasure.getInvStockTransferLines().isEmpty() ||
				!invUnitOfMeasure.getInvBillOfMaterials().isEmpty() || 
				!invUnitOfMeasure.getInvPhysicalInventoryLines().isEmpty() || 
				!invUnitOfMeasure.getInvStockIssuanceLines().isEmpty() ||
				!invUnitOfMeasure.getInvLineItems().isEmpty()) {
	            
	            throw new GlobalRecordAlreadyAssignedException();
	            
	        }
	        
	        // if already downloaded to pos
	        if (invUnitOfMeasure.getUomDownloadStatus()=='D' || invUnitOfMeasure.getUomDownloadStatus()=='X'){
	        	
	        	throw new GlobalRecordAlreadyAssignedException();
	        	
	        }
	                            	
		    invUnitOfMeasure.remove();
	    
        } catch (GlobalRecordAlreadyDeletedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	
        } catch (GlobalRecordAlreadyAssignedException ex) {
        	
        	getSessionContext().setRollbackOnly();
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }    
    
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvQuantityPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvUnitOfMeasureControllerBean getAdPrfInvQuantityPrecisionUnit");
		
		LocalAdPreferenceHome adPreferenceHome = null;
				
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfInvQuantityPrecisionUnit();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}         
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getAdLvInvUnitOfMeasureClassAll(Integer AD_CMPNY) {
		
		Debug.print("InvUnitOfMeasureControllerBean getAdLvInvUnitOfMeasureClassAll");
		
		LocalAdLookUpValueHome adLookUpValueHome = null;               
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			adLookUpValueHome = (LocalAdLookUpValueHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdLookUpValueHome.JNDI_NAME, LocalAdLookUpValueHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}        
		
		try {
			
			Collection adLookUpValues = adLookUpValueHome.findByLuName("INV UNIT OF MEASURE CLASS", AD_CMPNY);
			
			Iterator i = adLookUpValues.iterator();
			
			while (i.hasNext()) {
				
				LocalAdLookUpValue adLookUpValue = (LocalAdLookUpValue)i.next();
				
				list.add(adLookUpValue.getLvName());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}			
	

	/**
	  * @ejb:interface-method view-type="remote"
	  * @jboss:method-attributes read-only="true"
	  **/
	public void updateIiUomConversion(LocalInvUnitOfMeasure invUnitOfMeasure, Integer AD_CMPNY) {
		
	 	Debug.print("InvUnitOfMeasureControllerBean updateIiUomConversion");
	 	
	 	LocalInvItemHome invItemHome = null;
	 	LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
	 	
	 	// Initialize EJB Home
	 	
	 	try {
	 		
	 		invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
	 		invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
	 		
	 	} catch (NamingException ex) {
            
           throw new EJBException(ex.getMessage());
            
	 	}
	 	
	 	try {
	 		
	 		Collection invItems = invItemHome.findIiByUmcAdLvClass(invUnitOfMeasure.getUomAdLvClass(), AD_CMPNY);
	 		
	 		if(!invItems.isEmpty()) {
				
	 			Iterator i = invItems.iterator();
				
				while (i.hasNext()) {
					
					LocalInvItem invItem = (LocalInvItem)i.next();
					
					// create umc
					LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.create(invUnitOfMeasure.getUomConversionFactor(), EJBCommon.FALSE, AD_CMPNY);
					
					// map uom
					invUnitOfMeasureConversion.setInvUnitOfMeasure(invUnitOfMeasure);
					
					// map item
					invUnitOfMeasureConversion.setInvItem(invItem);
					
				}
				
			}

		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			getSessionContext().setRollbackOnly();
			throw new EJBException(ex.getMessage());
			
		}

	}
   
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("InvUnitOfMeasureControllerBean ejbCreate");
      
    }
    
}

