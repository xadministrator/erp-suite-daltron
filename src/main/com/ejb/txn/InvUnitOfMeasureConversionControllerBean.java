
/*
 * AdAmountLimitControllerBean.java
 *
 * Created on March 22, 2004, 8:16 PM
 *
 * @author  Rey Limosenero
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchItemLocation;
import com.ejb.ad.LocalAdBranchItemLocationHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.inv.LocalInvItemLocation;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.InvModUnitOfMeasureConversionDetails;

/**
 * @ejb:bean name="InvUnitOfMeasureConversionControllerEJB"
 *           display-name="Used for entering UOM conversion factor"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/InvUnitOfMeasureConversionControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.InvUnitOfMeasureConversionController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.InvUnitOfMeasureConversionControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="invuser"
 *                        role-link="invuserlink"
 *
 * @ejb:permission role-name="invuser"
 * 
*/

public class InvUnitOfMeasureConversionControllerBean extends AbstractSessionBean {
	

	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public short getAdPrfInvQuantityPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("InvUnitOfMeasureConversionControllerBean getAdPrfInvQuantityPrecisionUnit");
		
		LocalAdPreferenceHome adPreferenceHome = null;
				
		// Initialize EJB Home
		
		try {
			
			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			return adPreference.getPrfInvQuantityPrecisionUnit();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getInvUmcByIiCode(Integer II_CODE, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("InvUnitOfMeasureConversionControllerBean getInvUmcByIiCode");
        
        LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;

        // Initialize EJB Home
        
        try {
			
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			
		} catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalInvItem invItem = null;
        	
        	try {
        		
        		// get inv item
            	invItem = invItemHome.findByPrimaryKey(II_CODE);
        		
        	} catch (FinderException ex) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	
        	ArrayList list = new ArrayList();

        	// get unit of measure conversion
        		
        	Collection invUnitOfMeasureConversions = invItem.getInvUnitOfMeasureConversions();
			
        	Iterator i = invUnitOfMeasureConversions.iterator();
	               
	        while (i.hasNext()) {
	        	
	        	LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = (LocalInvUnitOfMeasureConversion)i.next();
	        	
	        	InvModUnitOfMeasureConversionDetails mdetails = new InvModUnitOfMeasureConversionDetails();
	        	
	        	mdetails.setUomName(invUnitOfMeasureConversion.getInvUnitOfMeasure().getUomName());
	        	mdetails.setUomShortName(invUnitOfMeasureConversion.getInvUnitOfMeasure().getUomShortName());
	        	mdetails.setUomAdLvClass(invUnitOfMeasureConversion.getInvUnitOfMeasure().getUomAdLvClass());
	        	mdetails.setUmcConversionFactor(invUnitOfMeasureConversion.getUmcConversionFactor());
	        	mdetails.setUmcBaseUnit(invUnitOfMeasureConversion.getUmcBaseUnit());
	        	
			    list.add(mdetails);
			    
	        }
	        
        	if (list.isEmpty()) {
        		
        		throw new GlobalNoRecordFoundException();
        		
        	}
        	        	        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
        	
        	throw ex;
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
	/**
	 * @ejb:interface-method view-type="remote"
	 **/
	public void saveInvUMCEntry(Integer II_CODE, ArrayList umcList, Integer AD_CMPNY) throws GlobalNoRecordFoundException {
		
		Debug.print("InvUnitOfMeasureConversionControllerBean saveInvUOMConversionEntry");
		
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;
		LocalInvItemLocationHome invItemLocationHome= null;
		LocalAdBranchItemLocationHome adBranchItemLocationHome = null;
		
		// initialize EJB
		
		try{
			
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			invItemLocationHome = (LocalInvItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemLocationHome.JNDI_NAME, LocalInvItemLocationHome.class);
			adBranchItemLocationHome = (LocalAdBranchItemLocationHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchItemLocationHome.JNDI_NAME, LocalAdBranchItemLocationHome.class);
			
		} catch (NamingException ex){
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalInvItem invItem = invItemHome.findByPrimaryKey(II_CODE);
			
			// remove all unit of mesure conversion by item
			
			Collection unitOfMeasureConversions = invItem.getInvUnitOfMeasureConversions();
			
			Iterator i = unitOfMeasureConversions.iterator();     	  
			
			while (i.hasNext()) {
				
				LocalInvUnitOfMeasureConversion unitOfMeasureConversion = (LocalInvUnitOfMeasureConversion)i.next();
				
				i.remove();
				
				unitOfMeasureConversion.remove();
				
			}
			
			// add new unit of mesure conversion
			
			i = umcList.iterator();
			
			while(i.hasNext()) {
				
				InvModUnitOfMeasureConversionDetails mdetails = (InvModUnitOfMeasureConversionDetails) i.next();
				
				double conversionFactor = mdetails.getUmcConversionFactor();
				byte umcBaseUnit = mdetails.getUmcBaseUnit();
				String uomName = mdetails.getUomName();
				String uomAdLvClass = mdetails.getUomAdLvClass();
				
				this.addInvUmcEntry(invItem, uomName, uomAdLvClass, conversionFactor, umcBaseUnit, AD_CMPNY);
				
			}              
			
			try{
	            LocalInvItemLocation invItemLocation = null;
	            
	            Collection invItemLocations = invItemLocationHome.findByIiName(invItem.getIiName(), AD_CMPNY);
	            
	            Iterator iterIl = invItemLocations.iterator();
	            
	            while(iterIl.hasNext()){
	            	
	            	invItemLocation = (LocalInvItemLocation)iterIl.next();
	            	
	            	Collection adBranchItemLocations = adBranchItemLocationHome.findByInvIlAll(invItemLocation.getIlCode(), AD_CMPNY);
	            	
	            	Iterator iterBil = adBranchItemLocations.iterator();
	            	while(iterBil.hasNext()){
	            		LocalAdBranchItemLocation adBranchItemLocation = (LocalAdBranchItemLocation)iterBil.next();
	            		
	            		if (adBranchItemLocation.getBilItemDownloadStatus()=='N'){
	            			adBranchItemLocation.setBilItemDownloadStatus('N');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='D'){
	            			adBranchItemLocation.setBilItemDownloadStatus('X');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='U'){
	            			adBranchItemLocation.setBilItemDownloadStatus('U');
	            		}else if(adBranchItemLocation.getBilItemDownloadStatus()=='X'){
	            			adBranchItemLocation.setBilItemDownloadStatus('X');
	            		}
	            		
	            	}
	            	
	            }
            }catch (FinderException ex){
            	
            }
			
		} catch (GlobalNoRecordFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}				
	
	// private
	
	private void addInvUmcEntry(LocalInvItem invItem,  String uomName, String uomAdLvClass, double conversionFactor, byte umcBaseUnit, Integer AD_CMPNY) throws GlobalNoRecordFoundException {
		
		Debug.print("InvUnitOfMeasureConversionControllerBean addInvUmcEntry");
		
		LocalInvUnitOfMeasureConversionHome invUnitOfMeasureConversionHome = null;
		LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
		LocalInvItemHome invItemHome = null;
		
		// initialize EJB
		
		try{
			
			invUnitOfMeasureConversionHome = (LocalInvUnitOfMeasureConversionHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureConversionHome.JNDI_NAME, LocalInvUnitOfMeasureConversionHome.class);
			invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
			invItemHome = (LocalInvItemHome)EJBHomeFactory.
				lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
			
		} catch (NamingException ex){
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			// create umc
			LocalInvUnitOfMeasureConversion invUnitOfMeasureConversion = invUnitOfMeasureConversionHome.create(conversionFactor, umcBaseUnit, AD_CMPNY);
			
			try {
				
				// map uom
				LocalInvUnitOfMeasure invUnitOfMeasure = invUnitOfMeasureHome.findByUomNameAndUomAdLvClass(uomName, uomAdLvClass, AD_CMPNY);
				invUnitOfMeasureConversion.setInvUnitOfMeasure(invUnitOfMeasure);
				
			} catch (FinderException ex) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			// map item
			invUnitOfMeasureConversion.setInvItem(invItem);
			
		} catch (GlobalNoRecordFoundException ex) {
			
			getSessionContext().setRollbackOnly();
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}				
	
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("InvUnitOfMeasureConversionControllerBean ejbCreate");
      
    }
    
}
