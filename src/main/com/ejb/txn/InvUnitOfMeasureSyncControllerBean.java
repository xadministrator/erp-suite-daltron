
/*
 * InvUnitOfMeasureSyncControllerBean
 *
 * Created on February 2, 2006, 2:44 PM
 *
 * @author  Franco Antonio R. Roig
 */

package com.ejb.txn;

import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.inv.LocalInvUnitOfMeasureConversion;
import com.ejb.inv.LocalInvUnitOfMeasureConversionHome;
import com.ejb.inv.LocalInvUnitOfMeasureHome;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="InvUnitOfMeasureSyncControllerBean"
 *           type="Stateless"
 *           view-type="service-endpoint"
 *           
 * @wsee:port-component name="InvUnitOfMeasureSync"
 *
 * @jboss:port-component uri="omega-ejb/InvUnitOfMeasureSyncWS"
 *
 * @ejb:interface service-endpoint-class="com.ejb.txn.InvUnitOfMeasureSyncWS"
 * 
*/

public class InvUnitOfMeasureSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
			
	/**
     * @ejb:interface-method
     **/
    public String[] getInvUnitOfMeasuresAll(Integer AD_CMPNY) {    	
    
    	Debug.print("InvUnitOfMeasureSyncControllerBean getInvUnitOfMeasuresAll");
    	
    	LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    	LocalInvUnitOfMeasure invUnitOfMeasure = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	Collection invUnitOfMasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);
        	
        	String[] results = new String[invUnitOfMasures.size()];
        	
        	Iterator i = invUnitOfMasures.iterator();
        	int ctr = 0;
	        while (i.hasNext()) {
	        	
	        	invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
	        	results[ctr] = uomRowEncode(invUnitOfMeasure);
	        	System.out.println(results[ctr]);
	        	ctr++;
	        	
	        }
    		
	        return results;
        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    /**
     * @ejb:interface-method
     **/
    public int setInvUnitOfMeasuresAllSuccessConfirmation(Integer AD_CMPNY) {    	
    
    	Debug.print("InvUnitOfMeasureSyncControllerBean setInvUnitOfMeasuresAllSuccessConfirmation");
    	
    	LocalInvUnitOfMeasureHome invUnitOfMeasureHome = null;
    	LocalInvUnitOfMeasure invUnitOfMeasure = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	invUnitOfMeasureHome = (LocalInvUnitOfMeasureHome)EJBHomeFactory.
                lookUpLocalHome(LocalInvUnitOfMeasureHome.JNDI_NAME, LocalInvUnitOfMeasureHome.class);
            
        } catch (NamingException ex) {
        	
        	throw new EJBException(ex.getMessage());
        	
        }
        
        try {
        	
        	Collection invUnitOfMeasures = invUnitOfMeasureHome.findEnabledUomAll(AD_CMPNY);
        	
        	Iterator i = invUnitOfMeasures.iterator();        	
        	while (i.hasNext()) {
        		
        		invUnitOfMeasure = (LocalInvUnitOfMeasure)i.next();
        		invUnitOfMeasure.setUomDownloadStatus('D');
        		
        	}
        	
        } catch (Exception ex) {
        	
        	ctx.setRollbackOnly();
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
        
        return 0;
    }
    
    private String uomRowEncode(LocalInvUnitOfMeasure invUnitOfMeasure) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer encodedResult = new StringBuffer();

    	// Start separator
    	encodedResult.append(separator);
    	
    	// Primary Key
    	encodedResult.append(invUnitOfMeasure.getUomCode());
    	encodedResult.append(separator);
    	
    	// Name / OPOS: UOM Code
    	encodedResult.append(invUnitOfMeasure.getUomName());
    	encodedResult.append(separator);
    	
    	// Description / OPOS: UOM Name
    	encodedResult.append(invUnitOfMeasure.getUomDescription());
    	encodedResult.append(separator);
    	
    	// Short Name / OPOS: Short Name
    	encodedResult.append(invUnitOfMeasure.getUomShortName());
    	encodedResult.append(separator);
    	
    	// UOM Class
    	encodedResult.append(invUnitOfMeasure.getUomAdLvClass());
    	encodedResult.append(separator);
    	
    	// End separator
    	encodedResult.append(separator);
    	
    	return encodedResult.toString();
    	
    }
    
    public void ejbCreate() throws CreateException {

       Debug.print("InvUnitOfMeasureSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}