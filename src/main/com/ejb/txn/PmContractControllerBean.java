
/*
 * PmContractControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmContract;
import com.ejb.pm.LocalPmContractHome;
import com.util.AbstractSessionBean;
import com.util.PmContractDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmContractControllerBean"
 *           display-name="Used for entering project"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmContractControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmContractController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmContractControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmContractControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmCtrAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmContractControllerBean getPmCtrAll");
        
        LocalPmContractHome pmContractHome = null;
        
        Collection pmContracts = null;
        
        LocalPmContract pmContract = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmContracts = pmContractHome.findCtrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmContracts.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmContracts.iterator();
               
        while (i.hasNext()) {
        	
        	pmContract = (LocalPmContract)i.next();
        
                
        	PmContractDetails details = new PmContractDetails();
        	
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmCtrEntry(com.util.PmContractDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmContractControllerBean addPmCtrEntry");
        
        LocalPmContractHome pmContractHome = null;

        LocalPmContract pmContract = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
                              
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	pmContract = pmContractHome.findCtrByReferenceID(details.getCtrReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new bank
        	System.out.println("details.getCtrReferenceID()="+details.getCtrReferenceID());
        	
        	pmContract = pmContractHome.create(details.getCtrReferenceID(), details.getCtrClientID(), details.getCtrPrice(), AD_CMPNY);	        
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmCtrEntry(com.util.PmContractDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmContractControllerBean updatePmCtrEntry");
        
        LocalPmContractHome pmContractHome = null;

        LocalPmContract pmContract = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalPmContract adExistingProject = 
                	pmContractHome.findCtrByReferenceID(details.getCtrReferenceID(), AD_CMPNY);
            
            
            
            if (!adExistingProject.getCtrCode().equals(details.getCtrCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmContract = pmContractHome.findByPrimaryKey(details.getCtrCode());

        	pmContract.setCtrClientID(details.getCtrClientID());
        	pmContract.setCtrPrice(details.getCtrPrice());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmContractEntry(Integer CTR_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("PmContractControllerBean deletePmContractEntry");
        
        LocalPmContractHome pmContractHome = null;

        LocalPmContract pmContract = null;     
        
        // Initialize EJB Home
        
        try {
            
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmContract = pmContractHome.findByPrimaryKey(CTR_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmContract.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmContractControllerBean ejbCreate");
      
    }
}
