
/*
 * PmContractTermControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmContract;
import com.ejb.pm.LocalPmContractHome;
import com.ejb.pm.LocalPmContractTerm;
import com.ejb.pm.LocalPmContractTermHome;
import com.util.AbstractSessionBean;
import com.util.PmContractTermDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmContractTermControllerBean"
 *           display-name="Used for entering project"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmContractTermControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmContractTermController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmContractTermControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmContractTermControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmCtAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmContractTermControllerBean getPmCtAll");
        
        LocalPmContractTermHome pmContractTermHome = null;
        
        Collection pmContractTerms = null;
        
        LocalPmContractTerm pmContractTerm = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmContractTerms = pmContractTermHome.findCtAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmContractTerms.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmContractTerms.iterator();
               
        while (i.hasNext()) {
        	
        	pmContractTerm = (LocalPmContractTerm)i.next();
        
                
        	PmContractTermDetails details = new PmContractTermDetails();
        	
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmCtEntry(com.util.PmContractTermDetails details, Integer CONTRACT, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmContractTermControllerBean addPmCtEntry");
        
        LocalPmContractTermHome pmContractTermHome = null;
        LocalPmContractHome pmContractHome = null;

        LocalPmContractTerm pmContractTerm = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
                              
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	pmContractTerm = pmContractTermHome.findCtByReferenceID(details.getCtReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new bank
        	System.out.println("details.getCtReferenceID()="+details.getCtReferenceID());
        	
        	pmContractTerm = pmContractTermHome.create(details.getCtReferenceID(), details.getCtTermDescription(), details.getCtValue(), AD_CMPNY);	        
        	System.out.println("CONTRACT="+CONTRACT);
        	LocalPmContract pmContract = pmContractHome.findCtrByReferenceID(CONTRACT, AD_CMPNY);
        	pmContractTerm.setPmContract(pmContract);
        	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmCtEntry(com.util.PmContractTermDetails details, Integer CONTRACT, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmContractTermControllerBean updatePmCtEntry");
        
        LocalPmContractTermHome pmContractTermHome = null;
        LocalPmContractHome pmContractHome = null;

        LocalPmContractTerm pmContractTerm = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
        	
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
            
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalPmContractTerm adExistingProject = 
                	pmContractTermHome.findCtByReferenceID(details.getCtReferenceID(), AD_CMPNY);
            
            if (!adExistingProject.getCtCode().equals(details.getCtCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmContractTerm = pmContractTermHome.findByPrimaryKey(details.getCtCode());
        	
        	pmContractTerm.setCtTermDescription(details.getCtTermDescription());
        	pmContractTerm.setCtValue(details.getCtValue());
        	
        	System.out.println("CONTRACT="+CONTRACT);
        	LocalPmContract pmContract = pmContractHome.findCtrByReferenceID(CONTRACT, AD_CMPNY);
        	pmContractTerm.setPmContract(pmContract);
        	
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmContractTermEntry(Integer CTR_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("PmContractTermControllerBean deletePmContractTermEntry");
        
        LocalPmContractTermHome pmContractTermHome = null;

        LocalPmContractTerm pmContractTerm = null;     
        
        // Initialize EJB Home
        
        try {
            
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmContractTerm = pmContractTermHome.findByPrimaryKey(CTR_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmContractTerm.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmContractTermControllerBean ejbCreate");
      
    }
}
