
/*
 * PmFindProjectControllerBean.java
 *
 * Created on June 3, 2004, 10:44 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranchProjectTypeTypeHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;

import com.ejb.exception.GlobalNoRecordFoundException;

import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;

import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.PmModProjectDetails;

/**
 * @ejb:bean name="PmFindProjectControllerEJB"
 *           display-name="Used for finding projects"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmFindProjectControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmFindProjectController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmFindProjectControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmFindProjectControllerBean extends AbstractSessionBean {

    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmPrjByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, String ORDER_BY,  Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmFindProjectControllerBean getPmPrjByCriteria");
        
        LocalPmProjectHome pmProjectHome = null;
        LocalAdBranchProjectTypeTypeHome adBranchProjectTypeTypeHome = null;
        LocalPmProjectTypeHome pmProjectTypeHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	adBranchProjectTypeTypeHome = (LocalAdBranchProjectTypeTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdBranchProjectTypeTypeHome.JNDI_NAME, LocalAdBranchProjectTypeTypeHome.class);
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);

        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size() + 2;
        	
        	Object obj[] = null;		      


        	jbossQl.append("SELECT OBJECT(prj) FROM PmProject prj ");
        	
        	// Allocate the size of the object parameter
        	
        	

        	if (criteria.containsKey("projectName")) {
        		
        		criteriaSize--;
        		
        	}

        	if (criteria.containsKey("projectDescription")) {
        		
        		criteriaSize--;
        		
        	} 

        	obj = new Object[criteriaSize];    

        	if (criteria.containsKey("projectName")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append(" WHERE ");
        			
        		}
        		
        		jbossQl.append("prj.prjProjectCode LIKE '%" + (String)criteria.get("projectName") + "%' ");		
        		
        	}
        	
        	
        	
        	
        	

        	if (criteria.containsKey("projectDescription")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append(" WHERE ");
        			
        		}
        		
        		jbossQl.append("prj.prjName LIKE '%" + (String)criteria.get("projectDescription") + "%' ");		
        		
        	}
        	
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append(" WHERE ");
        		
        	}
        	
        	jbossQl.append("prj.prjAdCompany=" + AD_CMPNY + " ");
        	
        	String orderBy = null;
        	
        	
        		
        	if (ORDER_BY.equals("PROJECT NAME")) {
        		
        		orderBy = "prj.prjName";
        		
        	} else {
        		
        		orderBy = "prj.prjName";
        		
        	}
        	
        	jbossQl.append("ORDER BY " + orderBy);
        	
        	jbossQl.append(" OFFSET ?" + (ctr + 1));	        
        	obj[ctr] = OFFSET;	      
        	ctr++;
        	
        	jbossQl.append(" LIMIT ?" + (ctr + 1));
        	obj[ctr] = LIMIT;
        	ctr++;		      
        	System.out.println(jbossQl.toString() + "<== query");
        	Collection pmProjects = pmProjectHome.getPrjByCriteria(jbossQl.toString(), obj);	         
        	
        	
        	if (pmProjects.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	Iterator i = pmProjects.iterator();
        		
        	while (i.hasNext()) {
        		
        		LocalPmProject pmProject = (LocalPmProject)i.next();   	  
        		
        		PmModProjectDetails mdetails = new PmModProjectDetails();
        		
        		mdetails.setPrjCode(pmProject.getPrjCode());
        		mdetails.setPrjProjectCode(pmProject.getPrjProjectCode());
        		mdetails.setPrjName(pmProject.getPrjName());
        		
        		
        		list.add(mdetails);

        	}
        	
        	return list;
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getPmPrjSizeByCriteria(HashMap criteria, Integer AD_BRNCH, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmFindProjectControllerBean getPmPrjSizeByCriteria");
        
        LocalPmProjectHome pmProjectHome = null;
        
        //initialized EJB Home
        
        try {
            
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	StringBuffer jbossQl = new StringBuffer();

        	
        	jbossQl.append("SELECT OBJECT(prj) FROM PmProject prj ");
        	
        	boolean firstArgument = true;
        	short ctr = 0;
        	int criteriaSize = criteria.size();
        	
        	
        	Object obj[] = null;		      
        	
        	// Allocate the size of the object parameter


        	if (criteria.containsKey("projectName")) {
        		
        		criteriaSize--;
        		
        	} 
        	
        	
        	
        	obj = new Object[criteriaSize];    
        	
        	if (criteria.containsKey("projectName")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append(" WHERE ");
        		}
        		
        		jbossQl.append("prj.prjProjectCode LIKE '%" + (String)criteria.get("projectName") + "%' ");		
        		
        	}
        	
        	
        	
        	if (!firstArgument) {
        		
        		jbossQl.append("AND ");
        		
        	} else {
        		
        		firstArgument = false;
        		jbossQl.append(" WHERE ");
        		
        	}
        	
        	jbossQl.append("prj.prjAdCompany=" + AD_CMPNY + " ");
        	
        	
        	Collection pmProjects = pmProjectHome.getPrjByCriteria(jbossQl.toString(), obj);	         
        	
        	if (pmProjects.size() == 0)
        		throw new GlobalNoRecordFoundException();
        	
        	return new Integer(pmProjects.size());
        	
        } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
	   /**
	    * @ejb:interface-method view-type="remote"
	    * @jboss:method-attributes read-only="true"
	    **/
	    public short getGlFcPrecisionUnit(Integer AD_CMPNY) {

	       Debug.print("PmProjectEntryControllerBean getGlFcPrecisionUnit");

	      
	       LocalAdCompanyHome adCompanyHome = null;
	      
	     
	       // Initialize EJB Home
	        
	       try {
	            
	           adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	              lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
	            
	       } catch (NamingException ex) {
	            
	           throw new EJBException(ex.getMessage());
	            
	       }

	       try {
	       	
	         LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	            
	         return  adCompany.getGlFunctionalCurrency().getFcPrecision();
	                                     
	       } catch (Exception ex) {
	       	 
	       	 Debug.printStackTrace(ex);
	         throw new EJBException(ex.getMessage());
	         
	       }

	    }	    

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmFindProjectControllerBean ejbCreate");
      
    }
}
