
/*
 * PmFindProjectTypeControllerBean.java
 *
 * Created on Sept 7, 2018 01:28 PM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.util.AbstractSessionBean;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.PmProjectTypeDetails;

/**
 * @ejb:bean name="PmFindProjectTypeControllerEJB"
 *           display-name="Used for finding projects"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmFindProjectTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmFindProjectTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmFindProjectTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmFindProjectTypeControllerBean extends AbstractSessionBean {

		
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmProjTypByCriteria(HashMap criteria,
        Integer OFFSET, Integer LIMIT, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmFindProjectTypeControllerBean getPmProjTypByCriteria");
        
        LocalPmProjectTypeHome pmProjectTypeHome = null;
        
        ArrayList list = new ArrayList();
        
        //initialized EJB Home
        
        try {
            
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(pt) FROM PmProjectType pt ");
		  
		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size() + 2;
		
		  
		  Object obj[] = null;		      
		
		  // Alptate the size of the object parameter
				   
		  if (criteria.containsKey("projectTypeCode")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  
		  
		  obj = new Object[criteriaSize];    
		       	      		
	      if (criteria.containsKey("projectTypeCode")) {
	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("pt.ptValue LIKE '%" + (String)criteria.get("projectTypeCode") + "%' ");		
	  	 
	      }
	      
		      
			
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("pt.ptAdCompany=" + AD_CMPNY + " ");
		  
		  jbossQl.append("ORDER BY " + "pt.ptValue");
		  
		  jbossQl.append(" OFFSET ?" + (ctr + 1));	        
		  obj[ctr] = OFFSET;	      
		  ctr++;
		  
		  jbossQl.append(" LIMIT ?" + (ctr + 1));
		  obj[ctr] = LIMIT;
		  ctr++;		      
			  	      	
	      Collection pmProjectTypes = pmProjectTypeHome.getProjTypByCriteria(jbossQl.toString(), obj);	         
		  
		  if (pmProjectTypes.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  Iterator i = pmProjectTypes.iterator();
		
		  while (i.hasNext()) {
		  	
		  	  LocalPmProjectType pmProjectType = (LocalPmProjectType)i.next();   	  
		  	  
		  	  PmProjectTypeDetails details = new PmProjectTypeDetails();
		  	  
		  	  details.setPtProjectTypeCode(pmProjectType.getPtProjectTypeCode());
		  	  
			      	  	      	  		     
			  list.add(details);
			         
		  }
			     
		  return list;
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public Integer getPmProjTypSizeByCriteria(HashMap criteria, Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmFindProjectTypeControllerBean getPmProjTypSizeByCriteria");
        
        LocalPmProjectTypeHome pmProjectTypeHome = null;
        
        //initialized EJB Home
        
        try {
            
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
            
        } catch (NamingException ex) 	{
            
            throw new EJBException(ex.getMessage());
            
        }
        
		try { 
           
		  StringBuffer jbossQl = new StringBuffer();
		  jbossQl.append("SELECT OBJECT(pt) FROM PmProjectType pt ");
		  
		  boolean firstArgument = true;
		  short ctr = 0;
		  int criteriaSize = criteria.size();
		
		  
		  Object obj[] = null;		      
		
		  // Alptate the size of the object parameter
				   
		  if (criteria.containsKey("projectTypeCode")) {
		  	
		  	 criteriaSize--;
		  	 
		  } 
		  
		  
		  
		  obj = new Object[criteriaSize];    
		       	      		
	      if (criteria.containsKey("projectTypeCode")) {
	  	
		  	 if (!firstArgument) {
		  	 	jbossQl.append("AND ");
		  	 } else {
		  	 	firstArgument = false;
		  	 	jbossQl.append("WHERE ");
		  	 }
	  	 
	   	     jbossQl.append("pt.ptProjectTypeCode LIKE '%" + (String)criteria.get("projectTypeCode") + "%' ");		
	  	 
	      }
	      
		  
		  
		  if (!firstArgument) {
       	  	
       	     jbossQl.append("AND ");
       	     
       	  } else {
       	  	
       	  	 firstArgument = false;
       	  	 jbossQl.append("WHERE ");
       	  	 
       	  }
       	  
       	  jbossQl.append("pt.ptAdCompany=" + AD_CMPNY + " ");
		  
		    	      	
	      Collection pmProjectTypes = pmProjectTypeHome.getProjTypByCriteria(jbossQl.toString(), obj);	         
		  
		  if (pmProjectTypes.size() == 0)
		     throw new GlobalNoRecordFoundException();
		     
		  return new Integer(pmProjectTypes.size());
		      
	  } catch (GlobalNoRecordFoundException ex) {
	  	 
	  	  throw ex;
	  	
	  } catch (Exception ex) {
	  	

	  	  ex.printStackTrace();
	  	  throw new EJBException(ex.getMessage());
	  	
	  }
        
    }

    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmFindProjectTypeControllerBean ejbCreate");
      
    }
}
