
/*
 * PmFindProjectTypeTypeControllerBean.java
 *
 * Created on June 8, 2004 10:26 AM
 *
 * @author  Enrico C. Yap
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdModBranchItemLocationDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.PmModProjectTypeTypeDetails;

/**
 * @ejb:bean name="PmFindProjectTypeTypeControllerEJB"
 *           display-name="Used for finding project types"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmFindProjectTypeTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmFindProjectTypeTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmFindProjectTypeTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
 */

public class PmFindProjectTypeTypeControllerBean extends AbstractSessionBean {
	
	
	
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getPmPrjAll(Integer AD_CMPNY) {
		
		Debug.print("PmFindProjectTypeTypeControllerBean getPmPrjAll");
		
		LocalPmProjectHome pmProjectHome = null;
		LocalPmProject pmProject = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
			lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection invProjects = pmProjectHome.findPrjAll(AD_CMPNY);
			
			Iterator i = invProjects.iterator();
			
			while (i.hasNext()) {
				
				pmProject = (LocalPmProject)i.next();
				
				list.add(pmProject.getPrjProjectCode());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getPmProjTypAll(Integer AD_CMPNY) {
		
		Debug.print("PmFindProjectTypeTypeControllerBean getPmProjTypAll");
		
		LocalPmProjectTypeHome pmProjectTypeHome = null;
		LocalPmProjectType pmProjectType = null;
		
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
			lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			Collection pmProjectTypes = pmProjectTypeHome.findPtAll(AD_CMPNY);
			
			Iterator i = pmProjectTypes.iterator();
			
			while (i.hasNext()) {
				
				pmProjectType = (LocalPmProjectType)i.next();
				
				list.add(pmProjectType.getPtProjectTypeCode());
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}   
	
		    
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("PmFindProjectTypeTypeControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibpttities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibpttities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibpttities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibpttities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }

	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public ArrayList getPmPttByCriteria(HashMap criteria, ArrayList branchList, 
			Integer OFFSET, Integer LIMIT, String ORDER_BY, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException {
		
		Debug.print("PmFindProjectTypeTypeControllerBean getPmPttByCriteria");
		
		LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
		
		ArrayList list = new ArrayList();
		
		//initialized EJB Home
		
		try {
			
			pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			
			if(branchList.size() > 0) {
				
				jbossQl.append("SELECT DISTINCT OBJECT(ptt) FROM PmProjectTypeType ptt, IN(il.adBranchProjectTypeTypes)bptt ");
				
			} else {
				
				jbossQl.append("SELECT OBJECT(ptt) FROM PmProjectTypeType ptt ");
				
			}
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = criteria.size() + 2 + branchList.size();
			
			Object obj[] = null;		
			
			if (criteria.containsKey("category")) {
        		
        		criteriaSize--;
        		
        	}

			if (criteria.containsKey("itemName")) {
        		
        		criteriaSize--;
        		
        	} 
			
			if (criteria.containsKey("itemDescription")) {
        		
        		criteriaSize--;
        		
        	} 
			
			obj = new Object[criteriaSize];
			
			
			if (criteria.containsKey("category")) {
        		
        		if (!firstArgument) {
        			jbossQl.append("AND ");
        		} else {
        			firstArgument = false;
        			jbossQl.append(" WHERE ");
        		}
        		
        		jbossQl.append("il.pmProject.iiAdLvCategory = '" + (String)criteria.get("category") + "' ");
        		
        		
        	}
			
			if (criteria.containsKey("itemName")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("il.pmProject.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");		
        		
        	}
			
			if (criteria.containsKey("itemDescription")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("il.pmProject.iiDescription LIKE '%" + (String)criteria.get("itemDescription") + "%' ");		
        		
        	}
			
			if (criteria.containsKey("locationName")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("il.pmProjectType.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationName");
				ctr++;
				
			}
			
			if(branchList.size() > 0) {
			
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				AdModBranchItemLocationDetails details = null;
				
				Iterator i = branchList.iterator();
				
				details = (AdModBranchItemLocationDetails)i.next();
				
				jbossQl.append("(bptt.adBranch.brCode=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)details.getBilBrCode();
				ctr++;
				
				while(i.hasNext()) {
					
					details = (AdModBranchItemLocationDetails)i.next();
					
					jbossQl.append("OR bptt.adBranch.brCode=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)details.getBilBrCode();
					ctr++;
						
				}
				
				jbossQl.append(") ");
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("il.ilAdCompany=" + AD_CMPNY + " ");
			
			String orderBy = null;
			
			if (ORDER_BY.equals("ITEM NAME")) {          
				
				orderBy = "il.pmProject.iiName";
				
			} else {
				
				orderBy = "il.pmProjectType.locName";
				
			} 
			
			jbossQl.append("ORDER BY " + orderBy);
			
			jbossQl.append(" OFFSET ?" + (ctr + 1));	        
			obj[ctr] = OFFSET;	      
			ctr++;
			
			jbossQl.append(" LIMIT ?" + (ctr + 1));
			obj[ctr] = LIMIT;
			ctr++;		      
			
			Collection pmProjectTypeTypes = pmProjectTypeTypeHome.getPttByCriteria(jbossQl.toString(), obj);	         
			
			if (pmProjectTypeTypes.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			Iterator i = pmProjectTypeTypes.iterator();
			
			while (i.hasNext()) {
				
				LocalPmProjectTypeType pmProjectLocation = (LocalPmProjectTypeType)i.next();   	  

				PmModProjectTypeTypeDetails mdetails = new PmModProjectTypeTypeDetails();
				mdetails.setPttCode(pmProjectLocation.getPttCode());
				mdetails.setPttPrjName(pmProjectLocation.getPmProject().getPrjName());
				
				list.add(mdetails);
				
			}
			
			return list;
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
	 * @ejb:interface-method view-type="remote"
	 * @jboss:method-attributes read-only="true"
	 **/
	public Integer getPmPttSizeByCriteria(HashMap criteria, ArrayList branchList, Integer AD_CMPNY) 
		throws GlobalNoRecordFoundException {
		
		Debug.print("PmFindProjectTypeTypeControllerBean getPmPttSizeByCriteria");
		
		LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;

		//initialized EJB Home
		
		try {
			
			pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
			
		} catch (NamingException ex) 	{
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try { 
			
			StringBuffer jbossQl = new StringBuffer();
			
			if(branchList.size() > 0) {
				
				jbossQl.append("SELECT DISTINCT OBJECT(ptt) FROM PmProjectTypeType ptt, IN(il.adBranchProjectTypeTypes)bptt ");
				
			} else {
				
				jbossQl.append("SELECT OBJECT(ptt) FROM PmProjectTypeType ptt ");
				
			}
			
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = criteria.size() + branchList.size();			
			Object obj[] = null;
			
			

			if (criteria.containsKey("itemName")) {
        		
        		criteriaSize--;
        		
        	} 
			
			
			obj = new Object[criteriaSize];
			
			

			if (criteria.containsKey("itemName")) {
        		
        		if (!firstArgument) {
        			
        			jbossQl.append("AND ");
        			
        		} else {
        			
        			firstArgument = false;
        			jbossQl.append("WHERE ");
        			
        		}
        		
        		jbossQl.append("il.pmProject.iiName LIKE '%" + (String)criteria.get("itemName") + "%' ");		
        		
        	}
			
			
			
			if (criteria.containsKey("locationName")) {
				
				if (!firstArgument) {		       	  	
					jbossQl.append("AND ");		       	     
				} else {		       	  	
					firstArgument = false;
					jbossQl.append("WHERE ");		       	  	 
				}
				
				jbossQl.append("il.pmProjectType.locName=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("locationName");
				ctr++;
				
			}
			
			if(branchList.size() > 0) {
			
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				AdModBranchItemLocationDetails details = null;
				
				Iterator i = branchList.iterator();
				
				details = (AdModBranchItemLocationDetails)i.next();
				
				jbossQl.append("(bptt.adBranch.brCode=?" + (ctr+1) + " ");
				obj[ctr] = (Integer)details.getBilBrCode();
				ctr++;
				
				while(i.hasNext()) {
					
					details = (AdModBranchItemLocationDetails)i.next();
					
					jbossQl.append("OR bptt.adBranch.brCode=?" + (ctr+1) + " ");
					obj[ctr] = (Integer)details.getBilBrCode();
					ctr++;
						
				}
				
				jbossQl.append(") ");
			}
			
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}
			
			jbossQl.append("il.ilAdCompany=" + AD_CMPNY + " ");
			
			Collection pmProjectTypeTypes = pmProjectTypeTypeHome.getPttByCriteria(jbossQl.toString(), obj);	         
			
			if (pmProjectTypeTypes.size() == 0)
				throw new GlobalNoRecordFoundException();
			
			return new Integer(pmProjectTypeTypes.size());
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	// SessionBean methods
	
	/**
	 * @ejb:create-method view-type="remote"
	 **/
	public void ejbCreate() throws CreateException {
		
		Debug.print("PmFindProjectTypeTypeControllerBean ejbCreate");
		
	}
}
