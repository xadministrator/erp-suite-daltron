
/*
 * PmProjectControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmContract;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.util.AbstractSessionBean;
import com.util.PmProjectDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmProjectControllerBean"
 *           display-name="Used for entering project"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmProjectControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmProjectController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmProjectControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmProjectControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmPrjAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmProjectControllerBean getPmPrjAll");
        
        LocalPmProjectHome pmProjectHome = null;
        
        Collection pmProjects = null;
        
        LocalPmProject pmProject = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmProjects = pmProjectHome.findPrjAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmProjects.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmProjects.iterator();
               
        while (i.hasNext()) {
        	
        	pmProject = (LocalPmProject)i.next();
        
                
        	PmProjectDetails details = new PmProjectDetails();
        	
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmPrjEntry(com.util.PmProjectDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectControllerBean addPmPrjEntry");
        
        LocalPmProjectHome pmProjectHome = null;

        LocalPmProject pmProject = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
                              
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	pmProject = pmProjectHome.findPrjByReferenceID(details.getPrjReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	
        	pmProject = pmProjectHome.create( details.getPrjReferenceID(), 
        			details.getPrjProjectCode(), details.getPrjName(), details.getPrjClientID(),
        			details.getPrjStatus(), AD_CMPNY); 	        
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmPrjEntry(com.util.PmProjectDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectControllerBean updatePmPrjEntry");
        
        LocalPmProjectHome pmProjectHome = null;

        LocalPmProject pmProject = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
        	
        	
            LocalPmProject adExistingProject = 
                	pmProjectHome.findPrjByReferenceID(details.getPrjReferenceID(), AD_CMPNY);
            
            if (!adExistingProject.getPrjCode().equals(details.getPrjCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmProject = pmProjectHome.findByPrimaryKey(details.getPrjCode());
        	
        	pmProject.setPrjReferenceID(details.getPrjReferenceID());
        	pmProject.setPrjName(details.getPrjName());
        	pmProject.setPrjClientID(details.getPrjClientID());
        	pmProject.setPrjStatus(details.getPrjStatus());
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmProjectEntry(Integer PP_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("PmProjectControllerBean deletePmProjectEntry");
        
        LocalPmProjectHome pmProjectHome = null;

        LocalPmProject pmProject = null;     
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmProject = pmProjectHome.findByPrimaryKey(PP_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmProject.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmProjectControllerBean ejbCreate");
      
    }
}
