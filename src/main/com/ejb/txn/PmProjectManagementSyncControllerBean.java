
/*
 * PmProjectManagementSyncControllerBean
 *
 * Created on July 22, 2018, 14:49 PM
 *
 * @author Clint Arrogante
 */

package com.ejb.txn;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.inv.LocalInvItem;
import com.ejb.inv.LocalInvItemHome;
import com.ejb.pm.LocalPmContractTerm;
import com.ejb.pm.LocalPmContractTermHome;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectPhase;
import com.ejb.pm.LocalPmProjectPhaseHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.util.ArInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;

/**
 * 
 * @ejb:bean name="PmProjectManagementSyncControllerBean"
 * 			 type="Stateless"
 * 			 view-type="service-endpoint"
 * 
 * @wsee:port-component name ="PmProjectManagementSync"
 * 
 * @jboss:port-component uri="omega-ejb/PmProjectManagementSyncWS"
 * 
 * @ejb:interface service-endpoint-class="com.ejb.txn.PmProjectManagementSyncWS"
 * 
 **/

public class PmProjectManagementSyncControllerBean implements SessionBean {
	
	private SessionContext ctx;
	
	
	/**
     * @ejb:interface-method
     **/
    public String getCompanyName(String CMP_SHRT_NM) {    	
    
    	Debug.print("PmProjectManagementSyncControllerBean getCompanyName");
    	
    	LocalAdCompanyHome adCompanyHome = null;
    	
    	// Initialize EJB Home
        
        try {
            
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);

        	return adCompany.getCmpName();
        	        	
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArCustomers(String CMP_SHRT_NM) {    	
    
    	Debug.print("PmProjectManagementSyncControllerBean getArCustomers");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArCustomerHome arCustomerHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	
        	Collection arCustomers = arCustomerHome.findEnabledCstAllByCcName("Trade-Acct", adCompany.getCmpCode());
        	
        	Iterator i = arCustomers.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArCustomer arCustomer = (LocalArCustomer)i.next();
        		
        		list.add(arCustomer);
        		

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArCustomer arCustomer = (LocalArCustomer)j.next();
	        	
	        	results[ctr] = customerRowEncode(arCustomer);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArProjectBillings(String CMP_SHRT_NM) {    	
    
    	Debug.print("PmProjectManagementSyncControllerBean getArProjectBillings");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArInvoiceHome arInvoiceHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);

        	Collection arInvoices = arInvoiceHome.findByInvPmProject((byte)0, adCompany.getCmpCode());
        	
        	Iterator i = arInvoices.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArInvoice arInvoice = (LocalArInvoice)i.next();

        		list.add(arInvoice);
        		

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArInvoice arInvoice = (LocalArInvoice)j.next();
	        	
	        	results[ctr] = billingRowEncode(arInvoice);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getArProjectCollections(String INV_NMBR, String CMP_SHRT_NM) {    	
    
    	Debug.print("PmProjectManagementSyncControllerBean getArProjectCollections");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        	arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	
        	
        	Collection arAppliedInvoices = arAppliedInvoiceHome.findAiInvNumber( INV_NMBR, adCompany.getCmpCode());
        	
        	System.out.println("arAppliedInvoices.size()="+arAppliedInvoices.size());
        	Iterator i = arAppliedInvoices.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();

        		list.add(arAppliedInvoice);
        		

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)j.next();
	        	
	        	results[ctr] = collectionRowEncode(arAppliedInvoice);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String[] getInvItems(String CTGRY, String CMP_SHRT_NM) {    	
    
    	Debug.print("PmProjectManagementSyncControllerBean getInvAllItems");

    	LocalAdCompanyHome adCompanyHome = null;
    	LocalInvItemHome invItemHome = null;
    	
    	// Initialize EJB Home    	
        try {
            
        	
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);

        	invItemHome = (LocalInvItemHome)EJBHomeFactory.
                	lookUpLocalHome(LocalInvItemHome.JNDI_NAME, LocalInvItemHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	
        	
        	Collection invItems = invItemHome.findEnabledIiByIiAdLvCategory(CTGRY, adCompany.getCmpCode());
        	
        	Iterator i = invItems.iterator();
        	
        	ArrayList list = new ArrayList();
        	
        	while(i.hasNext()) {
        		
        		LocalInvItem invItem = (LocalInvItem)i.next();

        		list.add(invItem);
        		

        	}
        	
        	
        	String[] results = new String[list.size()];
        	Iterator j = list.iterator();
        	
        	int ctr = 0;
	        while (j.hasNext()) {
	        	
	        	LocalInvItem invItem = (LocalInvItem)j.next();
	        	
	        	results[ctr] = itemRowEncode(invItem);
	        	ctr++;	        	
	        }
	        
	        
	        return results;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
    
    
    
    /**
     * @ejb:interface-method
     **/
    public String setArProjectBilling(Integer PROJECT, Integer PROJECT_TYPE_TYPE, Integer PROJECT_PHASE, Integer CONTRACT_TERM, String INV_DATE, Double INV_AMNT, String CMP_SHRT_NM) {    	
    
    	Debug.print("PmProjectManagementSyncControllerBean setArProjectBilling");

        LocalArInvoiceHome arInvoiceHome = null;
        LocalAdCompanyHome adCompanyHome = null;
        
        LocalPmProjectHome pmProjectHome = null;
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null; 
        LocalPmProjectPhaseHome pmProjectPhaseHome = null;
        LocalPmContractTermHome pmContractTermHome = null;

        ArInvoiceEntryControllerHome homeINV = null;
        ArInvoiceEntryController ejbINV = null;

        // Initialize EJB Home

        System.out.println("PROJECT="+PROJECT);
        System.out.println("PROJECT_TYPE_TYPE="+PROJECT_TYPE_TYPE);
        System.out.println("PROJECT_PHASE="+PROJECT_PHASE);
        System.out.println("CONTRACT_TERM="+CONTRACT_TERM);
        System.out.println("INV_DATE="+INV_DATE);
        System.out.println("INV_AMNT="+INV_AMNT);
        System.out.println("CMP_SHRT_NM="+CMP_SHRT_NM);
        
        
        try {
        	
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
        	
        	pmProjectPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);
        	
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
        	
        	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
        	adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
        	
        	
        	homeINV = (ArInvoiceEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArInvoiceEntryControllerEJB", ArInvoiceEntryControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbINV = homeINV.create();
        	
        	
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	LocalAdCompany adCompany = adCompanyHome.findByCmpShortName(CMP_SHRT_NM);
        	
        	Integer AD_CMPNY = adCompany.getCmpCode();
        	ArInvoiceDetails details = new ArInvoiceDetails();

        	Date date = new SimpleDateFormat("yyyy-MM-dd").parse(INV_DATE);
        	
        	
        	
			details.setInvCode(null);
			details.setInvType("MEMO LINES");
			 details.setInvDate(date);
			 details.setInvNumber(null);
			 details.setInvReferenceNumber(null);
			 details.setInvVoid((byte)0);
			 details.setInvDescription("PM INTEGRATION");           
			 details.setInvBillToAddress("");
			 details.setInvBillToContact("");
			 details.setInvBillToAltContact("");
			 details.setInvBillToPhone("");
			 details.setInvBillingHeader("");
			 details.setInvBillingFooter("");
			 details.setInvBillingHeader2("");
			 details.setInvBillingFooter2("");
			 details.setInvBillingHeader3(null);
			 details.setInvBillingFooter3(null);
			 details.setInvBillingSignatory("");
			 details.setInvSignatoryTitle("");
			 details.setInvShipToAddress("");
			 details.setInvShipToContact("");
			 details.setInvShipToAltContact("");
			 details.setInvShipToPhone("");
			 details.setInvLvFreight("");
			 details.setInvShipDate(EJBCommon.convertStringToSQLDate(null));
			 details.setInvConversionDate(EJBCommon.convertStringToSQLDate(null));
			 details.setInvConversionRate(1d);
			 details.setInvDebitMemo((byte)0);
			 details.setInvSubjectToCommission((byte)0);
			 details.setInvClientPO("");
			 details.setInvEffectivityDate(date);
			 details.setInvRecieveDate(date);
			
			 details.setInvCreatedBy(null);
			 details.setInvDateCreated(new java.util.Date());
			    
			 details.setInvLastModifiedBy(null);
			 details.setInvDateLastModified(new java.util.Date());

			 
			 LocalPmProjectTypeType pmProjectTypeType = pmProjectTypeTypeHome.findPttByReferenceIDAndPrjByReferenceID(
					 		PROJECT_TYPE_TYPE, PROJECT, AD_CMPNY);
			 
			 LocalPmProjectPhase pmProjectPhase = pmProjectPhaseHome.findPpByReferenceIDAndPttByReferenceID(
					 		PROJECT_PHASE, PROJECT_TYPE_TYPE, AD_CMPNY);
			 
			 LocalPmContractTerm pmContractTerm = pmContractTermHome.findCtByReferenceIDAndCtrByReferenceID(
					 		CONTRACT_TERM, pmProjectTypeType.getPmContract().getCtrReferenceID(), AD_CMPNY);
			 

			 String CLIENT_CODE = pmProjectTypeType.getPmProject().getPrjClientID();
			 
			 
			 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();
			 ArrayList ilList = new ArrayList();
			 mdetails.setIlSmlName("PROJECT PHASE");
             mdetails.setIlDescription(pmProjectPhase.getPpName());
             mdetails.setIlQuantity(1d);
             mdetails.setIlUnitPrice(INV_AMNT );
			 mdetails.setIlAmount(INV_AMNT);
			 mdetails.setIlTax((byte)1);
			 ilList.add(mdetails);
	        
			 
			 try {
				 Integer invoiceCode = ejbINV.saveArInvEntry(details, "IMMEDIATE", "VAT INCLUSIVE", "NONE", adCompany.getGlFunctionalCurrency().getFcName(), 
						 CLIENT_CODE,"BATCH NAME", ilList, true, null, 
						 
						 pmProjectTypeType.getPmProject().getPrjProjectCode(), 
						 pmProjectTypeType.getPmProjectType().getPtProjectTypeCode(),
						 pmProjectPhase.getPpName(),
						 pmContractTerm.getCtTermDescription(),
						 
						 1, AD_CMPNY);
				 
				 LocalArInvoice arInvoice = arInvoiceHome.findByPrimaryKey(invoiceCode);
				 return arInvoice.getInvNumber();
         	}catch (Exception e) {}
			 
	        return null;
        	
        } catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    }
    
    
   
    
private String billingRowEncode(LocalArInvoice arInvoice) {
	
	
	LocalArInvoiceHome arInvoiceHome = null;
	
	// Initialize EJB Home    	
    try {

    	arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
            	lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
    	
    } catch (NamingException ex) {
        
        throw new EJBException(ex.getMessage());
        
    }
    
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

 
    	// project ID
    	tempResult.append(arInvoice.getPmProject().getPrjReferenceID());
    	tempResult.append(separator);
    	
    	// project type type ID
    	tempResult.append(arInvoice.getPmProjectTypeType().getPttReferenceID());
    	tempResult.append(separator);
    	
    	// project phase ID
    	tempResult.append(arInvoice.getPmProjectPhase() != null ? arInvoice.getPmProjectPhase().getPpReferenceID(): null);
    	tempResult.append(separator);
    	
    	// contract term ID
    	tempResult.append(arInvoice.getPmContractTerm().getCtReferenceID());
    	tempResult.append(separator);
    	
    	// invoice date
    	SimpleDateFormat formatDate = new SimpleDateFormat("yyyyy-MM-dd");
    	tempResult.append(formatDate.format(arInvoice.getInvDate()));
    	tempResult.append(separator);
    	
    	// invoice number
    	tempResult.append(arInvoice.getInvNumber());
    	tempResult.append(separator);
    	
    	try {
    		
    	
    	Collection creditMemos = arInvoiceHome.findByCmInvoiceNumberCreditMemoAndCompanyCode(arInvoice.getInvNumber(), arInvoice.getInvAdBranch(), arInvoice.getInvAdCompany());
    	
  		Iterator y = creditMemos.iterator();
  		double creditAmount = 0d;
  		while(y.hasNext()){
  			
  			LocalArInvoice arInvoiceCreditMemo = (LocalArInvoice)y.next();
  			System.out.println("CREDIT MEMO EXIST="+arInvoiceCreditMemo.getInvNumber());
  			creditAmount += arInvoiceCreditMemo.getInvAmountDue();
  			
  		}
    	
    	// amount due
    	tempResult.append(arInvoice.getInvAmountDue());
    	tempResult.append(separator);
    	
    	// credit amount
    	tempResult.append(creditAmount);
    	tempResult.append(separator);

    	// amount paid
    	tempResult.append(arInvoice.getInvAmountPaid() - creditAmount);
    	tempResult.append(separator);
    	
    	// remaining balance
    	tempResult.append(arInvoice.getInvAmountDue() - arInvoice.getInvAmountPaid());
    	tempResult.append(separator);
    	
    	// status
    	tempResult.append(arInvoice.getInvPosted() == (byte)1 ? "POSTED" : "UNPOSTED");
    	tempResult.append(separator);
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();    	
    	    	
    	encodedResult = encodedResult.replace("'", "\'");    	
    	
    	return encodedResult;
    	
    	} catch (Exception ex) {
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
    	
    }


private String collectionRowEncode(LocalArAppliedInvoice arAppliedInvoice) {
	
	char separator = EJBCommon.SEPARATOR;
	StringBuffer tempResult = new StringBuffer();
	String encodedResult = new String();

	// Start separator
	tempResult.append(separator);


	// project ID
	tempResult.append(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProject().getPrjReferenceID());
	tempResult.append(separator);
	
	// project type  ID
	tempResult.append(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProject().getPrjReferenceID());
	tempResult.append(separator);
	
	// project phase ID
	tempResult.append(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProjectPhase().getPpReferenceID());
	tempResult.append(separator);
	
	// contract term ID
	tempResult.append(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmContractTerm().getCtReferenceID());
	tempResult.append(separator);
	
	// invoice number
	tempResult.append(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getInvNumber());
	tempResult.append(separator);
	
	// or date
	SimpleDateFormat formatDate = new SimpleDateFormat("yyyyy-MM-dd");
	tempResult.append(formatDate.format(arAppliedInvoice.getArReceipt().getRctDate()));
	tempResult.append(separator);
	
	// or number
	tempResult.append(arAppliedInvoice.getArReceipt().getRctNumber());
	tempResult.append(separator);
	
	// amount paid
	tempResult.append(arAppliedInvoice.getArReceipt().getRctAmount());
	tempResult.append(separator);

	// status
	tempResult.append(arAppliedInvoice.getArReceipt().getRctPosted() == (byte)1 ? "POSTED" : "UNPOSTED");
	tempResult.append(separator);
	
	// remove unwanted chars from encodedResult;
	encodedResult = tempResult.toString();    	
	    	
	encodedResult = encodedResult.replace("'", "\'");    	
	
	return encodedResult;
	
}



private String itemRowEncode(LocalInvItem invItem) {
	
	char separator = EJBCommon.SEPARATOR;
	StringBuffer tempResult = new StringBuffer();
	String encodedResult = new String();

	// Start separator
	tempResult.append(separator);


	// item Code
	tempResult.append(invItem.getIiName());
	tempResult.append(separator);
	
	// item name
	tempResult.append(invItem.getIiDescription());
	tempResult.append(separator);
	
	// uom
	tempResult.append(invItem.getInvUnitOfMeasure().getUomName());
	tempResult.append(separator);

	
	// remove unwanted chars from encodedResult;
	encodedResult = tempResult.toString();    	
	    	
	encodedResult = encodedResult.replace("'", "\'");    	
	
	return encodedResult;
	
}

   
	
    
    private String customerRowEncode(LocalArCustomer arCustomer) {
    	
    	char separator = EJBCommon.SEPARATOR;
    	StringBuffer tempResult = new StringBuffer();
    	String encodedResult = new String();

    	// Start separator
    	tempResult.append(separator);

  
    	// customer Code
    	tempResult.append(arCustomer.getCstCustomerCode());
    	tempResult.append(separator);
    	
    	// Name
    	tempResult.append(arCustomer.getCstName());
    	tempResult.append(separator);
    	
    	// Tax rate
    	tempResult.append(arCustomer.getArCustomerClass().getArTaxCode().getTcRate());
    	tempResult.append(separator);
    	
    	// Address
    	if (arCustomer.getCstAddress().length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {
    		
    		tempResult.append(arCustomer.getCstAddress());
    		tempResult.append(separator);
    		
    	}
    	
    	// Phone
    	if (arCustomer.getCstPhone() .length() < 1) {
    		
    		tempResult.append("not specified");
    		tempResult.append(separator);
    		
    	} else {
    		
    		tempResult.append(arCustomer.getCstPhone());
    		tempResult.append(separator);
    		
    	}
    	
    	   	
    	
    	// remove unwanted chars from encodedResult;
    	encodedResult = tempResult.toString();    	
    	    	
    	encodedResult = encodedResult.replace("'", "\'");    	
    	
    	return encodedResult;
    	
    }
    
    
    
    
    
    
    public void ejbCreate() throws CreateException {

       Debug.print("PmProjectManagementSyncControllerBean ejbCreate");
      
    }
    
    public void ejbRemove() {};

    public void ejbActivate() {}
    public void ejbPassivate() {}

    public void setSessionContext(SessionContext ctx) {}

}