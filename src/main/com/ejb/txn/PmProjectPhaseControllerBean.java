
/*
 * PmProjectPhaseControllerBean.java
 *
 * Created on Aug 8, 2018, 12:00 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmProjectPhase;
import com.ejb.pm.LocalPmProjectPhaseHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.ejb.pm.LocalPmContractTerm;
import com.ejb.pm.LocalPmContractTermHome;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.util.AbstractSessionBean;
import com.util.PmProjectPhaseDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmProjectPhaseControllerEJB"
 *           display-name="Used for entering banks"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmProjectPhaseControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmProjectPhaseController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmProjectPhaseControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="hruser"
 *                        role-link="hruserlink"
 *
 * @ejb:permission role-name="hruser"
 * 
*/

public class PmProjectPhaseControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmPpAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmProjectPhaseControllerBean getPmPpAll");
        
        LocalPmProjectPhaseHome pmPhaseHome = null;
        
        Collection pmPhases = null;
        
        LocalPmProjectPhase pmPhase = null;
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmPhases = pmPhaseHome.findPpAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmPhases.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmPhases.iterator();
               
        while (i.hasNext()) {
        	
        	pmPhase = (LocalPmProjectPhase)i.next();
        
                
        	PmProjectPhaseDetails details = new PmProjectPhaseDetails();
	    		/*details.setPhCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmPpEntry(com.util.PmProjectPhaseDetails details, Integer CONTRACT_TERM, Integer PROJECT_TYPE_TYPE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectPhaseControllerBean addPmPpEntry");
        
        LocalPmProjectPhaseHome pmProjectPhaseHome = null;
        LocalPmContractTermHome pmContractTermHome = null;
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;

        LocalPmProjectPhase pmProjectPhase = null;
        
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
        	
        	             
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	pmProjectPhase = pmProjectPhaseHome.findPpByReferenceID(details.getPpReferenceID(), AD_CMPNY);

            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new bank
        	
        	pmProjectPhase = pmProjectPhaseHome.create(details.getPpReferenceID(), details.getPpName(), AD_CMPNY);
        	
        	try {
        		LocalPmContractTerm pmContractTerm = pmContractTermHome.findCtByReferenceID(CONTRACT_TERM, AD_CMPNY);
            	pmProjectPhase.setPmContractTerm(pmContractTerm);
        	}catch (FinderException ex) {}
        	
        	try {
        		LocalPmProjectTypeType pmProjectTypeType = pmProjectTypeTypeHome.findPttByReferenceID(PROJECT_TYPE_TYPE, AD_CMPNY);
            	pmProjectPhase.setPmProjectTypeType(pmProjectTypeType);
        	}catch (FinderException ex) {}
        	
        	
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmPpEntry(com.util.PmProjectPhaseDetails details, Integer CONTRACT_TERM, Integer PROJECT_TYPE_TYPE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectPhaseControllerBean updatePmPpEntry");
        
        LocalPmProjectPhaseHome pmPhaseHome = null;
        LocalPmContractTermHome pmContractTermHome = null;
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;

        LocalPmProjectPhase pmProjectPhase = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);
        	
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
        	
        	
        	              
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalPmProjectPhase adExistingPhase = pmPhaseHome.findPpByReferenceID(details.getPpReferenceID(), AD_CMPNY);
            
            if (!adExistingPhase.getPpCode().equals(details.getPpCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmProjectPhase = pmPhaseHome.findByPrimaryKey(details.getPpCode());

        	pmProjectPhase.setPpName(details.getPpName());
        	
        	try {
        		LocalPmContractTerm pmContractTerm = pmContractTermHome.findCtByReferenceID(CONTRACT_TERM, AD_CMPNY);
            	pmProjectPhase.setPmContractTerm(pmContractTerm);
        	}catch (FinderException ex) {}
        	
        	try {
        		LocalPmProjectTypeType pmProjectTypeType = pmProjectTypeTypeHome.findPttByReferenceID(PROJECT_TYPE_TYPE, AD_CMPNY);
            	pmProjectPhase.setPmProjectTypeType(pmProjectTypeType);
        	}catch (FinderException ex) {}
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmProjectPhaseEntry(Integer PP_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("PmProjectPhaseControllerBean deletePmProjectPhaseEntry");
        
        LocalPmProjectPhaseHome pmPhaseHome = null;

        
        LocalPmProjectPhase pmPhase = null;           
               
        // Initialize EJB Home
        
        try {
            
        	pmPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);                      
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmPhase = pmPhaseHome.findByPrimaryKey(PP_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmPhase.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmProjectPhaseControllerBean ejbCreate");
      
    }
}
