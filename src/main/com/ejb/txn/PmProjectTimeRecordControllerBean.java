
/*
 * PmProjectTimeRecordControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmContract;
import com.ejb.pm.LocalPmContractHome;
import com.ejb.pm.LocalPmContractTermHome;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.ejb.pm.LocalPmUser;
import com.ejb.pm.LocalPmUserHome;
import com.ejb.pm.LocalPmProjectTimeRecord;
import com.ejb.pm.LocalPmProjectTimeRecordHome;
import com.util.AbstractSessionBean;
import com.util.PmProjectTimeRecordDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmProjectTimeRecordControllerBean"
 *           display-name="Used for entering project type type"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmProjectTimeRecordControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmProjectTimeRecordController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmProjectTimeRecordControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmProjectTimeRecordControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmPtrAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmProjectTimeRecordControllerBean getPmPtrAll");
        
        LocalPmProjectTimeRecordHome pmProjectTypeTypeHome = null;
        
        Collection pmProjectTypes = null;
        
        LocalPmProjectTimeRecord pmProjectType = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeTypeHome = (LocalPmProjectTimeRecordHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectTimeRecordHome.JNDI_NAME, LocalPmProjectTimeRecordHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmProjectTypes = pmProjectTypeTypeHome.findPtrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmProjectTypes.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmProjectTypes.iterator();
               
        while (i.hasNext()) {
        	
        	pmProjectType = (LocalPmProjectTimeRecord)i.next();
        
                
        	PmProjectTimeRecordDetails details = new PmProjectTimeRecordDetails();
        	
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmPtrEntry(com.util.PmProjectTimeRecordDetails details, Integer PROJECT, Integer USER, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectTimeRecordControllerBean addPmPtrEntry");
        
        LocalPmProjectTimeRecordHome pmProjectTimeRecordHome = null;
        
        LocalPmProjectHome pmProjectHome = null;
        LocalPmUserHome pmUserHome = null;
        
        LocalPmProjectTimeRecord pmProjectTimeRecord= null;

                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTimeRecordHome = (LocalPmProjectTimeRecordHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTimeRecordHome.JNDI_NAME, LocalPmProjectTimeRecordHome.class);
        	
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
        	                  
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	pmProjectTimeRecord = pmProjectTimeRecordHome.findPtrByPrjReferenceIDAndUsrReferenceID(PROJECT, USER, AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create Project Type
        	
        	pmProjectTimeRecord = pmProjectTimeRecordHome.create(details.getPtrRegularRate(), 
        			details.getPtrOvertimeRate(), details.getPtrRegularHours(), details.getPtrOvertimeHours(), AD_CMPNY);

        	try {
        		LocalPmProject pmProject = pmProjectHome.findPrjByReferenceID(PROJECT, AD_CMPNY);
        		pmProjectTimeRecord.setPmProject(pmProject);
        	} catch(FinderException ex){}
        	
        	try {
        		LocalPmUser pmUser = pmUserHome.findUsrByReferenceID(USER, AD_CMPNY);
        		pmProjectTimeRecord.setPmUser(pmUser);
        	} catch(FinderException ex){}

        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmPtrEntry(com.util.PmProjectTimeRecordDetails details, Integer PROJECT, Integer USER, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectTimeRecordControllerBean updatePmPtrEntry");
        
        LocalPmProjectTimeRecordHome pmProjectTimeRecordHome = null;
        
        LocalPmProjectHome pmProjectHome = null;
        LocalPmUserHome pmUserHome = null;
        
        LocalPmProjectTimeRecord pmProjectTimeRecord= null;

                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTimeRecordHome = (LocalPmProjectTimeRecordHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTimeRecordHome.JNDI_NAME, LocalPmProjectTimeRecordHome.class);
        	
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
        	                  
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalPmProjectTimeRecord adExistingProject = 
            		pmProjectTimeRecordHome.findPtrByPrjReferenceIDAndUsrReferenceID(PROJECT, USER, AD_CMPNY);
        
            if (!adExistingProject.getPtrCode().equals(details.getPtrCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmProjectTimeRecord = pmProjectTimeRecordHome.findByPrimaryKey(details.getPtrCode());
        	
        	pmProjectTimeRecord.setPtrRegularRate(details.getPtrRegularRate());
        	pmProjectTimeRecord.setPtrOvertimeRate(details.getPtrOvertimeRate());
        	pmProjectTimeRecord.setPtrRegularHours(details.getPtrRegularHours());
        	pmProjectTimeRecord.setPtrOvertimeHours(details.getPtrOvertimeHours());

        	try {
        		LocalPmProject pmProject = pmProjectHome.findPrjByReferenceID(PROJECT, AD_CMPNY);
        		pmProjectTimeRecord.setPmProject(pmProject);
        	} catch(FinderException ex){}
        	
        	try {
        		LocalPmUser pmUser = pmUserHome.findUsrByReferenceID(USER, AD_CMPNY);
        		pmProjectTimeRecord.setPmUser(pmUser);
        	} catch(FinderException ex){}
        	
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmProjectTimeRecordEntry(Integer PTT_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("HrPayrollPeriodControllerBean deletePmProjectTimeRecordEntry");
        
        LocalPmProjectTimeRecordHome pmProjectTypeTypeHome = null;

        LocalPmProjectTimeRecord pmProjectType = null;     
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeTypeHome = (LocalPmProjectTimeRecordHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTimeRecordHome.JNDI_NAME, LocalPmProjectTimeRecordHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmProjectType = pmProjectTypeTypeHome.findByPrimaryKey(PTT_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmProjectType.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmProjectTimeRecordControllerBean ejbCreate");
      
    }
}
