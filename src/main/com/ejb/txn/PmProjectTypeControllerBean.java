
/*
 * PmProjectTypeControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.util.AbstractSessionBean;
import com.util.PmProjectTypeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmProjectTypeControllerBean"
 *           display-name="Used for entering project"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmProjectTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmProjectTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmProjectTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmProjectTypeControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmPtAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmProjectTypeControllerBean getPmPtAll");
        
        LocalPmProjectTypeHome pmProjectTypeHome = null;
        
        Collection pmProjectTypes = null;
        
        LocalPmProjectType pmProjectType = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmProjectTypes = pmProjectTypeHome.findPtAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmProjectTypes.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmProjectTypes.iterator();
               
        while (i.hasNext()) {
        	
        	pmProjectType = (LocalPmProjectType)i.next();
        
                
        	PmProjectTypeDetails details = new PmProjectTypeDetails();
        	
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmPtEntry(com.util.PmProjectTypeDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("HrPayrollPeriodControllerBean addPmPtEntry");
        
        LocalPmProjectTypeHome pmProjectTypeHome = null;

        LocalPmProjectType pmProjectType = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
                              
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	pmProjectType = pmProjectTypeHome.findPtByReferenceID(details.getPtReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create Project Type
        	
        	pmProjectType = pmProjectTypeHome.create(details.getPtReferenceID(), 
        			details.getPtProjectTypeCode(), details.getPtValue(), AD_CMPNY);      
                	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmPtEntry(com.util.PmProjectTypeDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectTypeControllerBean updatePmPtEntry");
        
        LocalPmProjectTypeHome pmProjectTypeHome = null;

        LocalPmProjectType pmProjectType = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalPmProjectType adExistingProject = 
                	pmProjectTypeHome.findPtByReferenceID(details.getPtReferenceID(), AD_CMPNY);
            
            if (!adExistingProject.getPtCode().equals(details.getPtCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmProjectType = pmProjectTypeHome.findByPrimaryKey(details.getPtCode());
        	
        	pmProjectType.setPtProjectTypeCode(details.getPtProjectTypeCode());
        	pmProjectType.setPtValue(details.getPtValue());

      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmProjectTypeEntry(Integer PP_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("HrPayrollPeriodControllerBean deletePmProjectTypeEntry");
        
        LocalPmProjectTypeHome pmProjectTypeHome = null;

        LocalPmProjectType pmProjectType = null;     
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmProjectType = pmProjectTypeHome.findByPrimaryKey(PP_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmProjectType.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmProjectTypeControllerBean ejbCreate");
      
    }
}
