
/*
 * PmProjectTypeTypeControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmContract;
import com.ejb.pm.LocalPmContractHome;
import com.ejb.pm.LocalPmContractTermHome;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.util.AbstractSessionBean;
import com.util.PmProjectTypeTypeDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmProjectTypeTypeControllerBean"
 *           display-name="Used for entering project type type"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmProjectTypeTypeControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmProjectTypeTypeController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmProjectTypeTypeControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmProjectTypeTypeControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmPttPrjAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmProjectTypeTypeControllerBean getPmPttPrjAll");
        
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
        
        Collection pmProjectTypes = null;
        
        LocalPmProjectTypeType pmProjectType = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmProjectTypes = pmProjectTypeTypeHome.findPttPrjAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmProjectTypes.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmProjectTypes.iterator();
               
        while (i.hasNext()) {
        	
        	pmProjectType = (LocalPmProjectTypeType)i.next();
        
                
        	PmProjectTypeTypeDetails details = new PmProjectTypeTypeDetails();
        	
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmPttEntry(com.util.PmProjectTypeTypeDetails details, Integer PROJECT, Integer CONTRACT, Integer PROJECT_TYPE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectTypeTypeControllerBean addPmPttEntry");
        
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
        
        LocalPmProjectHome pmProjectHome = null;
        LocalPmContractHome pmContractHome = null;
        LocalPmProjectTypeHome pmProjectTypeHome = null;

        LocalPmProjectTypeType pmProjectTypeType = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
        	
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
                              
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
        	pmProjectTypeType = pmProjectTypeTypeHome.findPttByReferenceID(details.getPttReferenceID(), AD_CMPNY);
            
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create Project Type
        	
        	pmProjectTypeType = pmProjectTypeTypeHome.create(details.getPttReferenceID(), AD_CMPNY);
        	
        	try {
        		LocalPmContract pmContract = pmContractHome.findCtrByReferenceID(CONTRACT, AD_CMPNY);
        		pmProjectTypeType.setPmContract(pmContract);
        	} catch(FinderException ex){}
        	
        	try {
        		LocalPmProject pmProject = pmProjectHome.findPrjByReferenceID(PROJECT, AD_CMPNY);
        		pmProjectTypeType.setPmProject(pmProject);
        	} catch(FinderException ex){}
        	
        	try {
        		LocalPmProjectType pmProjectType = pmProjectTypeHome.findPtByReferenceID(PROJECT_TYPE, AD_CMPNY);
        		pmProjectTypeType.setPmProjectType(pmProjectType);
        	} catch(FinderException ex){}
        	
  	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmPttEntry(com.util.PmProjectTypeTypeDetails details, Integer PROJECT, Integer CONTRACT, Integer PROJECT_TYPE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmProjectTypeTypeControllerBean updatePmPttEntry");
        
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
        
        LocalPmProjectHome pmProjectHome = null;
        LocalPmContractHome pmContractHome = null;
        LocalPmProjectTypeHome pmProjectTypeHome = null;


        LocalPmProjectTypeType pmProjectTypeType = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
        	
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalPmProjectTypeType adExistingProject = 
                	pmProjectTypeTypeHome.findPttByReferenceID(details.getPttReferenceID(), AD_CMPNY);
            
            if (!adExistingProject.getPttCode().equals(details.getPttCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmProjectTypeType = pmProjectTypeTypeHome.findByPrimaryKey(details.getPttCode());
        	
        	try {
        		LocalPmContract pmContract = pmContractHome.findCtrByReferenceID(CONTRACT, AD_CMPNY);
        		pmProjectTypeType.setPmContract(pmContract);
        	} catch(FinderException ex){}
        	
        	try {
        		LocalPmProject pmProject = pmProjectHome.findPrjByReferenceID(PROJECT, AD_CMPNY);
        		pmProjectTypeType.setPmProject(pmProject);
        	} catch(FinderException ex){}
        	
        	try {
        		LocalPmProjectType pmProjectType = pmProjectTypeHome.findPtByReferenceID(PROJECT_TYPE, AD_CMPNY);
        		pmProjectTypeType.setPmProjectType(pmProjectType);
        	} catch(FinderException ex){}
        	
      	        	                            	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmProjectTypeTypeEntry(Integer PTT_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("HrPayrollPeriodControllerBean deletePmProjectTypeTypeEntry");
        
        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;

        LocalPmProjectTypeType pmProjectType = null;     
        
        // Initialize EJB Home
        
        try {
            
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmProjectType = pmProjectTypeTypeHome.findByPrimaryKey(PTT_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmProjectType.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmProjectTypeTypeControllerBean ejbCreate");
      
    }
}
