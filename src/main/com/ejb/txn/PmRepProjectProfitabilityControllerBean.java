package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdBranch;
import com.ejb.ad.LocalAdBranchHome;
import com.ejb.ad.LocalAdBranchResponsibility;
import com.ejb.ad.LocalAdBranchResponsibilityHome;
import com.ejb.ad.LocalAdCompany;
import com.ejb.ad.LocalAdCompanyHome;
import com.ejb.ad.LocalAdLookUpValue;
import com.ejb.ad.LocalAdLookUpValueHome;
import com.ejb.ad.LocalAdPreference;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ap.LocalApVoucherLineItem;
import com.ejb.ap.LocalApVoucherLineItemHome;
import com.ejb.ar.LocalArAppliedInvoice;
import com.ejb.ar.LocalArAppliedInvoiceHome;
import com.ejb.ar.LocalArInvoice;
import com.ejb.ar.LocalArInvoiceHome;
import com.ejb.ar.LocalArInvoiceLineItemHome;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.inv.LocalInvCosting;
import com.ejb.inv.LocalInvCostingHome;
import com.ejb.inv.LocalInvItemLocationHome;
import com.ejb.inv.LocalInvLocation;
import com.ejb.inv.LocalInvLocationHome;
import com.ejb.inv.LocalInvOverheadLine;
import com.ejb.inv.LocalInvOverheadLineHome;
import com.ejb.inv.LocalInvOverheadMemoLineHome;
import com.ejb.inv.LocalInvUnitOfMeasure;
import com.ejb.pm.LocalPmProjectTimeRecord;
import com.ejb.pm.LocalPmProjectTimeRecordHome;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.ejb.pm.LocalPmProjecting;
import com.ejb.pm.LocalPmProjectingHome;
import com.util.AbstractSessionBean;
import com.util.AdBranchDetails;
import com.util.AdCompanyDetails;
import com.util.Debug;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.InvRepInventoryProfitabilityDetails;
import com.util.InvRepItemLedgerDetails;
import com.util.PmRepProjectProfitabilityDetails;

/**
* @ejb:bean name="PmRepProjectProfitabilityControllerEJB"
*           display-name="Used for generation of project profitability reports"
*           type="Stateless"
*           view-type="remote"
*           jndi-name="ejb/PmRepProjectProfitabilityControllerEJB"
*
* @ejb:interface remote-class="com.ejb.txn.PmRepProjectProfitabilityController"
*                extends="javax.ejb.EJBObject"
*
* @ejb:home remote-class="com.ejb.txn.PmRepProjectProfitabilityControllerHome"
*           extends="javax.ejb.EJBHome"
*
* @ejb:transaction type="Required"
*
* @ejb:security-role-ref role-name="pmuser"
*                        role-link="pmuserlink"
*
* @ejb:permission role-name="pmuser"
* 
*/

public class PmRepProjectProfitabilityControllerBean extends AbstractSessionBean {
	
	
	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList getPmProjTypAll(Integer AD_CMPNY) {
		
		Debug.print("PmRepProjectProfitabilityControllerBean getPmProjTypAll");
		
		LocalPmProjectTypeHome pmProjectTypeHome = null;
		Collection pmProjectTypes = null;
		ArrayList list = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
				lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			pmProjectTypes = pmProjectTypeHome.findPtAll(AD_CMPNY);            
			
			if (pmProjectTypes.isEmpty()) {
				
				return null;
				
			}
			
			Iterator i = pmProjectTypes.iterator();
			
			while (i.hasNext()) {
				
				LocalPmProjectType pmProjectType = (LocalPmProjectType)i.next();	
				String details = pmProjectType.getPtProjectTypeCode();
				
				list.add(details);
				
			}
			
			return list;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}

	
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public ArrayList executePmRepProjectProfitability(HashMap criteria, ArrayList branchList, String GROUP_BY, Integer AD_CMPNY)
		throws GlobalNoRecordFoundException {
		
		Debug.print("PmRepProjectProfitabilityControllerBean executePmRepProjectProfitability");

		LocalPmProjectingHome pmProjectingHome = null;
		LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
		LocalPmProjectTimeRecordHome pmProjectTimeRecordHome = null;
		
		LocalAdPreferenceHome adPreferenceHome = null;
		LocalArInvoiceHome arInvoiceHome = null;
		LocalApVoucherLineItemHome apVoucherLineItemHome = null;
		LocalArAppliedInvoiceHome arAppliedInvoiceHome = null;

		ArrayList list = new ArrayList();
		ArrayList tempList = new ArrayList();
		
		// Initialize EJB Home
		
		try {
			
			pmProjectingHome = (LocalPmProjectingHome)EJBHomeFactory.
					lookUpLocalHome(LocalPmProjectingHome.JNDI_NAME, LocalPmProjectingHome.class);
			
			pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
					lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);
			
			pmProjectTimeRecordHome = (LocalPmProjectTimeRecordHome)EJBHomeFactory.
					lookUpLocalHome(LocalPmProjectTimeRecordHome.JNDI_NAME, LocalPmProjectTimeRecordHome.class);

			

			adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
				lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);

			arInvoiceHome = (LocalArInvoiceHome)EJBHomeFactory.
					lookUpLocalHome(LocalArInvoiceHome.JNDI_NAME, LocalArInvoiceHome.class);
			
			apVoucherLineItemHome = (LocalApVoucherLineItemHome)EJBHomeFactory.
					lookUpLocalHome(LocalApVoucherLineItemHome.JNDI_NAME, LocalApVoucherLineItemHome.class);
			
			arAppliedInvoiceHome = (LocalArAppliedInvoiceHome)EJBHomeFactory.
					lookUpLocalHome(LocalArAppliedInvoiceHome.JNDI_NAME, LocalArAppliedInvoiceHome.class);

				
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);
			
			if (branchList.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			
			StringBuffer jbossQl = new StringBuffer();
			jbossQl.append("SELECT OBJECT(pjt) FROM PmProjecting pjt ");
			
			boolean firstArgument = true;
			short ctr = 0;	      
			int criteriaSize = 0;
			
			Object obj[] = null;			
			            			
			// Allocate the size of the object parameter


			
			
			if (criteria.containsKey("projectType")) {
				
				criteriaSize++;
				
			}

			
			if (criteria.containsKey("dateFrom")) {

				criteriaSize++;

			}

			if (criteria.containsKey("dateTo")) {

				criteriaSize++;

			}
			
			obj = new Object[criteriaSize];

			jbossQl.append(" WHERE pjt.pjtAdBranch in (");
			
			boolean firstLoop = true;
			
			Iterator i = branchList.iterator();
			
			while(i.hasNext()) {
				
				if(firstLoop == false) { 
					jbossQl.append(", "); 
				}
				else { 
					firstLoop = false; 
				}
				
				AdBranchDetails mdetails = (AdBranchDetails) i.next();
				
				jbossQl.append(mdetails.getBrCode());
				
			}
			
			jbossQl.append(") ");
			
			firstArgument = false;
			
			
			if (criteria.containsKey("projectName")) {

				if (!firstArgument) {

					jbossQl.append("AND ");	

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("pjt.invItemLocation.pmProject.prjProjectCode  LIKE '%" + (String)criteria.get("projectName") + "%' ");

			}

			
			if (criteria.containsKey("projectType")) {

				if (!firstArgument) {
					jbossQl.append("AND ");

				} else {

					firstArgument = false;
					jbossQl.append("WHERE ");

				}

				jbossQl.append("pjt.pmProjectTypeType.pmProjectType.ptProjectTypeCode=?" + (ctr+1) + " ");
				obj[ctr] = (String)criteria.get("projectType");
				ctr++;

			} 
			
			
			if (criteria.containsKey("dateFrom")) {
				
				if (!firstArgument) {
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pjt.pjtDate>=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateFrom");
				ctr++;
				
			}  
			
			if (criteria.containsKey("dateTo")) {
				
				if (!firstArgument) {
					
					jbossQl.append("AND ");
					
				} else {
					
					firstArgument = false;
					jbossQl.append("WHERE ");
					
				}
				
				jbossQl.append("pjt.pjtDate<=?" + (ctr+1) + " ");
				obj[ctr] = (Date)criteria.get("dateTo");
				ctr++;
				
			}
										
			if (!firstArgument) {
				
				jbossQl.append("AND ");
				
			} else {
				
				firstArgument = false;
				jbossQl.append("WHERE ");
				
			}

			jbossQl.append("pjt.pjtAdCompany=" + AD_CMPNY + " ORDER BY pjt.pmProject.prjProjectCode, pjt.pjtDate, pjt.pjtLineNumber");
			
			Collection pmProjectings = pmProjectingHome.getPjtByCriteria(jbossQl.toString(), obj);
			
			System.out.println("jbossQl="+jbossQl);
			System.out.println("pmProjectings.size()="+pmProjectings.size());
			
			Iterator x = pmProjectings.iterator();
			
			while(x.hasNext()) {
				
				LocalPmProjecting pmProjecting = (LocalPmProjecting)x.next();

				PmRepProjectProfitabilityDetails details = new PmRepProjectProfitabilityDetails();
				
				details.setPpProjectName(pmProjecting.getPmProject().getPrjProjectCode());
				details.setPpProjectDescription(pmProjecting.getPmProject().getPrjName());
				details.setPpProjectTypeName(pmProjecting.getPmProjectTypeType() != null ? pmProjecting.getPmProjectTypeType().getPmProjectType().getPtProjectTypeCode() :"" );
				details.setPpProjectTypeDescription(pmProjecting.getPmProjectTypeType() != null ? pmProjecting.getPmProjectTypeType().getPmProjectType().getPtValue() :"" );
				details.setPpProjectPhaseName(pmProjecting.getPmProjectPhase() != null ? pmProjecting.getPmProjectPhase().getPpName() :" " );
				details.setPpContractAmount(0d);

				details.setPpDate(pmProjecting.getPjtDate());
				details.setPpDocumentNumber(pmProjecting.getApVoucherLineItem().getApVoucher().getVouDocumentNumber());
				details.setPpItemName(pmProjecting.getInvItemLocation().getInvItem().getIiName());
				details.setPpItemDescription(pmProjecting.getInvItemLocation().getInvItem().getIiDescription());
				details.setPpAmount(pmProjecting.getPjtProjectCost());

				list.add(details);
				
			}
			
			Collection pmProjectTypeTypes = pmProjectTypeTypeHome.findPttPrjAll(AD_CMPNY);
			
			i = pmProjectTypeTypes.iterator();
			
			while(i.hasNext()) {
				
				LocalPmProjectTypeType pmProjectTypeType = (LocalPmProjectTypeType)i.next();
				
				PmRepProjectProfitabilityDetails details = new PmRepProjectProfitabilityDetails();

				System.out.println(pmProjectTypeType.getPmProject().getPrjProjectCode() +" = "+pmProjectTypeType.getPmProject().getPrjName());
				details.setPpProjectName(pmProjectTypeType.getPmProject().getPrjProjectCode());
				details.setPpProjectDescription(pmProjectTypeType.getPmProject().getPrjName());
				details.setPpProjectTypeName(pmProjectTypeType.getPmProjectType().getPtProjectTypeCode());
				details.setPpProjectTypeDescription(pmProjectTypeType.getPmProjectType().getPtValue() );
				details.setPpProjectPhaseName("");
				details.setPpContractAmount(pmProjectTypeType.getPmContract() != null ? pmProjectTypeType.getPmContract().getCtrPrice() : 0d);
				details.setPpDocumentNumber("PROJECT");
				details.setPpItemName("PROJECT");
				details.setPpItemDescription("PROJECT");
				details.setPpAmount(pmProjectTypeType.getPmContract() != null ? pmProjectTypeType.getPmContract().getCtrPrice() : 0d);
				
				list.add(details);

			}

			Collection pmProjectTimeRecords = pmProjectTimeRecordHome.findPtrAll(AD_CMPNY);
			
			i = pmProjectTimeRecords.iterator();
			
			while(i.hasNext()) {
				
				LocalPmProjectTimeRecord pmProjectTimeRecord = (LocalPmProjectTimeRecord)i.next();
				
				PmRepProjectProfitabilityDetails details = new PmRepProjectProfitabilityDetails();

				details.setPpProjectName(pmProjectTimeRecord.getPmProject().getPrjProjectCode());
				details.setPpProjectDescription(pmProjectTimeRecord.getPmProject().getPrjName());
				details.setPpProjectTypeName(" ");
				details.setPpProjectTypeDescription(" ");
				details.setPpProjectPhaseName("");
				details.setPpContractAmount(0d);
				details.setPpDocumentNumber("TIME RECORD");

				if(pmProjectTimeRecord.getPtrRegularHours() > 0) {
					details.setPpItemName("DIRECT COST STAFF");
					details.setPpItemDescription("DIRECT COST STAFF");
					details.setPpAmount(pmProjectTimeRecord.getPtrRegularHours() * pmProjectTimeRecord.getPtrRegularRate());
					
					list.add(details);
				}
				
				if(pmProjectTimeRecord.getPtrOvertimeHours() > 0) {
					details.setPpItemName("OVERTIME");
					details.setPpItemDescription("OVERTIME");
					details.setPpAmount(pmProjectTimeRecord.getPtrOvertimeHours() * pmProjectTimeRecord.getPtrOvertimeRate());
					
					list.add(details);
				}

			}

			Collection arInvoices = arInvoiceHome.findByInvPmProject((byte)0, AD_CMPNY);
			i = arInvoices.iterator();
			
			while(i.hasNext()) {
				
				LocalArInvoice arInvoice = (LocalArInvoice)i.next();
				
				PmRepProjectProfitabilityDetails details = new PmRepProjectProfitabilityDetails();
				
				details.setPpProjectName(arInvoice.getPmProject().getPrjProjectCode());
				details.setPpProjectDescription(arInvoice.getPmProject().getPrjName());
				details.setPpProjectTypeName(arInvoice.getPmProjectTypeType() != null ? arInvoice.getPmProjectTypeType().getPmProjectType().getPtProjectTypeCode() :"" );
				details.setPpProjectTypeDescription(arInvoice.getPmProjectTypeType() != null ? arInvoice.getPmProjectTypeType().getPmProjectType().getPtValue() :"" );
				details.setPpProjectPhaseName(arInvoice.getPmProjectPhase() != null ? arInvoice.getPmProjectPhase().getPpName() :" " );
				details.setPpContractAmount(0d);
				details.setPpDate(arInvoice.getInvDate());
				details.setPpDocumentNumber(arInvoice.getInvNumber());
				details.setPpItemName("INVOICE");
				details.setPpItemDescription(arInvoice.getPmContractTerm().getCtTermDescription());
				details.setPpAmount(arInvoice.getInvAmountDue());

				list.add(details);
				
			}
			
			Collection arAppliedInvoices = arAppliedInvoiceHome.findAiPmProject(AD_CMPNY);
			i = arAppliedInvoices.iterator();
			
			while(i.hasNext()) {
				
				LocalArAppliedInvoice arAppliedInvoice = (LocalArAppliedInvoice)i.next();
				
				PmRepProjectProfitabilityDetails details = new PmRepProjectProfitabilityDetails();
				
				details.setPpProjectName(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProject().getPrjProjectCode());
				details.setPpProjectDescription(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProject().getPrjName());
				details.setPpProjectTypeName(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProjectTypeType() != null ? arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProjectTypeType().getPmProjectType().getPtProjectTypeCode() :"" );
				details.setPpProjectTypeDescription(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProjectTypeType() != null ? arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProjectTypeType().getPmProjectType().getPtValue() :"" );
				details.setPpProjectPhaseName(arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProjectPhase() != null ? arAppliedInvoice.getArInvoicePaymentSchedule().getArInvoice().getPmProjectPhase().getPpName() :" " );
				details.setPpContractAmount(0d);
				details.setPpDate(arAppliedInvoice.getArReceipt().getRctDate());
				details.setPpDocumentNumber(arAppliedInvoice.getArReceipt().getRctNumber());
				details.setPpItemName("COLLECTION");
				details.setPpItemDescription("COLLECTION");
				details.setPpAmount(arAppliedInvoice.getAiApplyAmount());
				
				
				list.add(details);
			}

			
			Collection apVoucherLineItems = apVoucherLineItemHome.findUnpostedVliPmProject(AD_CMPNY);
			i = apVoucherLineItems.iterator();
			
			while(i.hasNext()) {
				
				LocalApVoucherLineItem apVoucherLineItem = (LocalApVoucherLineItem)i.next();
				
				PmRepProjectProfitabilityDetails details = new PmRepProjectProfitabilityDetails();
				
				details.setPpProjectName(apVoucherLineItem.getPmProject().getPrjProjectCode());
				details.setPpProjectDescription(apVoucherLineItem.getPmProject().getPrjName());
				details.setPpProjectTypeName(apVoucherLineItem.getPmProjectTypeType() != null ? apVoucherLineItem.getPmProjectTypeType().getPmProjectType().getPtProjectTypeCode() :"" );
				details.setPpProjectTypeDescription(apVoucherLineItem.getPmProjectTypeType() != null ? apVoucherLineItem.getPmProjectTypeType().getPmProjectType().getPtValue() :"" );
				details.setPpProjectPhaseName(apVoucherLineItem.getPmProjectPhase() != null ? apVoucherLineItem.getPmProjectPhase().getPpName() :" " );
				details.setPpContractAmount(0d);
				details.setPpDate(apVoucherLineItem.getApVoucher().getVouDate());
				details.setPpDocumentNumber(apVoucherLineItem.getApVoucher().getVouDocumentNumber());
				details.setPpItemName(apVoucherLineItem.getInvItemLocation().getInvItem().getIiName());
				details.setPpItemDescription(apVoucherLineItem.getInvItemLocation().getInvItem().getIiDescription());
				details.setPpAmount(apVoucherLineItem.getVliAmount() + apVoucherLineItem.getVliTaxAmount());
				
				
				list.add(details);
			}
			

			if (list.isEmpty()) {
				
				throw new GlobalNoRecordFoundException();
				
			}
			
			Collections.sort(list, PmRepProjectProfitabilityDetails.ProjectProfitabilityComparator);
			
			return list; 	  
			
		} catch (GlobalNoRecordFoundException ex) {
			
			throw ex;
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
	}
	
	/**
	* @ejb:interface-method view-type="remote"
	* @jboss:method-attributes read-only="true"
	**/
	public com.util.AdCompanyDetails getAdCompany(Integer AD_CMPNY) {
	
	  Debug.print("PmRepProjectProfitabilityControllerBean getAdCompany");      
	  
	  LocalAdCompanyHome adCompanyHome = null;
	        
	  // Initialize EJB Home
	    
	  try {
	        
	       adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
	           lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);
	        
	  } catch (NamingException ex) {
	        
	      throw new EJBException(ex.getMessage());
	        
	  }
	
	  try {
	      
	     LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
	     
	     AdCompanyDetails details = new AdCompanyDetails();
	     details.setCmpName(adCompany.getCmpName());
	     
	     return details;  	
	  	       	
	  } catch (Exception ex) {
	  	
	  	 Debug.printStackTrace(ex);
	  	 throw new EJBException(ex.getMessage());
	  	  
	  }
	  
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
	public short getGlFcPrecisionUnit(Integer AD_CMPNY) {
		
		Debug.print("PmRepProjectProfitabilityControllerBean getGlFcPrecisionUnit");
		
		
		LocalAdCompanyHome adCompanyHome = null;
		
		
		// Initialize EJB Home
		
		try {
			
			adCompanyHome = (LocalAdCompanyHome)EJBHomeFactory.
			lookUpLocalHome(LocalAdCompanyHome.JNDI_NAME, LocalAdCompanyHome.class);       
			
		} catch (NamingException ex) {
			
			throw new EJBException(ex.getMessage());
			
		}
		
		try {
			
			LocalAdCompany adCompany = adCompanyHome.findByPrimaryKey(AD_CMPNY);
			
			return  adCompany.getGlFunctionalCurrency().getFcPrecision();
			
		} catch (Exception ex) {
			
			Debug.printStackTrace(ex);
			throw new EJBException(ex.getMessage());
			
		}
		
	}
	
	/**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    
    public ArrayList getAdBrResAll(Integer RS_CODE, Integer AD_CMPNY) 
    	throws GlobalNoRecordFoundException{
    	
    	Debug.print("PmRepProjectProfitabilityControllerBean getAdBrResAll");

    	LocalAdBranchResponsibilityHome adBranchResponsibilityHome = null;
    	LocalAdBranchHome adBranchHome = null;
    	
    	LocalAdBranchResponsibility adBranchResponsibility = null;
    	LocalAdBranch adBranch = null;

    	Collection adBranchResponsibilities = null;
    	
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home

        try {
            
            adBranchResponsibilityHome = (LocalAdBranchResponsibilityHome)EJBHomeFactory.
                lookUpLocalHome(LocalAdBranchResponsibilityHome.JNDI_NAME, LocalAdBranchResponsibilityHome.class);
            adBranchHome = (LocalAdBranchHome)EJBHomeFactory.
            	lookUpLocalHome(LocalAdBranchHome.JNDI_NAME, LocalAdBranchHome.class);

        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	adBranchResponsibilities = adBranchResponsibilityHome.findByAdResponsibility(RS_CODE, AD_CMPNY);
		
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (adBranchResponsibilities.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        try {
        
	        Iterator i = adBranchResponsibilities.iterator();
	        
	        while(i.hasNext()) {
	        	
	        	adBranchResponsibility = (LocalAdBranchResponsibility)i.next();
	        		
		    	adBranch = adBranchHome.findByPrimaryKey(adBranchResponsibility.getAdBranch().getBrCode());
		    	
		    	AdBranchDetails details = new AdBranchDetails();
		    	details.setBrCode(adBranch.getBrCode());
		    	details.setBrBranchCode(adBranch.getBrBranchCode());
		    	details.setBrName(adBranch.getBrName());
		    	details.setBrHeadQuarter(adBranch.getBrHeadQuarter());
		    	
		    	list.add(details);
	            	
	        }
	        
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        return list;
    	
    }
	
	// private methods
	
	private double convertByUomFromAndUomToAndQuantity(LocalInvUnitOfMeasure invFromUnitOfMeasure, LocalInvUnitOfMeasure invToUnitOfMeasure, double QTY_SLD, Integer AD_CMPNY) {
		
	Debug.print("PmRepProjectProfitabilityControllerBean convertByUomFromAndUomToAndQuantity");		        
    
	LocalAdPreferenceHome adPreferenceHome = null;
            
    // Initialize EJB Home
    
    try {
        
        adPreferenceHome = (LocalAdPreferenceHome)EJBHomeFactory.
             lookUpLocalHome(LocalAdPreferenceHome.JNDI_NAME, LocalAdPreferenceHome.class);   
                    
    } catch (NamingException ex) {
        
        throw new EJBException(ex.getMessage());
        
    }
            
    try {
    
        LocalAdPreference adPreference = adPreferenceHome.findByPrfAdCompany(AD_CMPNY);               	          	        	
    	        	
    	return EJBCommon.roundIt(QTY_SLD * invFromUnitOfMeasure.getUomConversionFactor() / invToUnitOfMeasure.getUomConversionFactor(), adPreference.getPrfInvQuantityPrecisionUnit());       	        		        		       	
    						    		        		
    } catch (Exception ex) {
    	
    	Debug.printStackTrace(ex);
    	getSessionContext().setRollbackOnly();
    	throw new EJBException(ex.getMessage());
    	
    }
	
}
	
	// SessionBean methods
	
	/**
* @ejb:create-method view-type="remote"
**/
	public void ejbCreate() throws CreateException {
		
		Debug.print("PmRepProjectProfitabilityControllerBean ejbCreate");
		
	}
	
}

