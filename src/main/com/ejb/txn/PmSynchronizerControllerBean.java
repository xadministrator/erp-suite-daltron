package com.ejb.txn;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;

import com.ejb.ad.LocalAdUser;
import com.ejb.ad.LocalAdUserHome;
import com.ejb.ad.LocalAdUserResponsibility;
import com.ejb.ad.LocalAdUserResponsibilityHome;
import com.ejb.ap.LocalApSupplier;
import com.ejb.ap.LocalApSupplierClass;
import com.ejb.ap.LocalApSupplierClassHome;
import com.ejb.ap.LocalApSupplierHome;
import com.ejb.ar.LocalArCustomer;
import com.ejb.ar.LocalArCustomerClass;
import com.ejb.ar.LocalArCustomerClassHome;
import com.ejb.ar.LocalArCustomerHome;
import com.ejb.gl.LocalGlChartOfAccountHome;
import com.ejb.pm.LocalPmContract;
import com.ejb.pm.LocalPmContractHome;
import com.ejb.pm.LocalPmContractTerm;
import com.ejb.pm.LocalPmContractTermHome;
import com.ejb.pm.LocalPmProject;
import com.ejb.pm.LocalPmProjectHome;
import com.ejb.pm.LocalPmProjectPhase;
import com.ejb.pm.LocalPmProjectPhaseHome;
import com.ejb.pm.LocalPmProjectTimeRecord;
import com.ejb.pm.LocalPmProjectTimeRecordHome;
import com.ejb.pm.LocalPmProjectType;
import com.ejb.pm.LocalPmProjectTypeHome;
import com.ejb.pm.LocalPmProjectTypeType;
import com.ejb.pm.LocalPmProjectTypeTypeHome;
import com.ejb.pm.LocalPmUser;
import com.ejb.pm.LocalPmUserHome;
import com.google.gson.Gson;
import com.util.AbstractSessionBean;
import com.util.AdModBranchCustomerDetails;
import com.util.AdModBranchSupplierDetails;
import com.util.AdUserDetails;
import com.util.AdUserResponsibilityDetails;
import com.util.ApModSupplierDetails;
import com.util.ArModCustomerDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;
import com.util.PmContractDetails;
import com.util.PmContractTermDetails;
import com.util.PmModSyncContractDetails;
import com.util.PmModSyncContractTermDetails;
import com.util.PmModSyncProjectDetails;
import com.util.PmModSyncProjectPhaseDetails;
import com.util.PmModSyncProjectTimeRecordDetails;
import com.util.PmModSyncProjectTypeDetails;
import com.util.PmModSyncProjectTypeTypeDetails;
import com.util.PmModSyncUserDetails;
import com.util.PmProjectDetails;
import com.util.PmProjectPhaseDetails;
import com.util.PmProjectTimeRecordDetails;
import com.util.PmProjectTypeDetails;
import com.util.PmProjectTypeTypeDetails;
import com.util.PmUserDetails;

import sun.net.www.protocol.http.HttpURLConnection;

/*
 * PmSynchronizerControllerBean
 *
 * Created on July 22, 2018, 14:49 PM
 *
 * @author Clint Arrogante
 */


/**
 * @ejb:bean name="PmSynchronizerControllerEJB"
 *           display-name="Used for synchronizing data for PMA"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmSynchronizerControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmSynchronizerController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmSynchronizerControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmSynchronizerControllerBean extends AbstractSessionBean {

	private String ip_adress = "http://dev.jigzen.com";
	//private String ip_adress = "http://localhost";
	
	/**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmProject(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmProject");

        LocalPmProjectHome pmProjectHome = null;
        
        PmProjectControllerHome homePRJ = null;
        PmProjectController ejbPRJ = null;
        
        // Initialize EJB Home

        try {
        	
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);

        	homePRJ = (PmProjectControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmProjectControllerEJB", PmProjectControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbPRJ = homePRJ.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	URL url = new URL(ip_adress+"/pma/public/api/all-projects");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);
    		

    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/
        	
        	Gson gson = new Gson();  
        	PmModSyncProjectDetails[] projectsArray = gson.fromJson(response, PmModSyncProjectDetails[].class);
        	
        	for(int i = 0; i < projectsArray.length; i++) {
        		
        		PmModSyncProjectDetails project = (PmModSyncProjectDetails)projectsArray[i];

        		PmProjectDetails details = new PmProjectDetails();

        		details.setPrjCode(null);
        		try {
        			System.out.println("project.getPmProjectID()="+project.getPmProjectID());
        			LocalPmProject pmProject = pmProjectHome.findPrjByReferenceID(project.getPmProjectID(), AD_CMPNY);
        			
        			System.out.println("pmProject.getPrjCode()="+pmProject.getPrjCode());
        			details.setPrjCode(pmProject.getPrjCode());
        		}catch (FinderException e) { }

        		details.setPrjReferenceID(project.getPmProjectID());
        		details.setPrjProjectCode(project.getPmProjectCode());
				details.setPrjName(project.getPmProjectName());
				details.setPrjClientID(project.getPmClientID());
				details.setPrjStatus(project.getPmStatus());

            	try {
        		
        		if(details.getPrjCode() == null) {

        			ejbPRJ.addPmPrjEntry(details, AD_CMPNY);
        			
        		} else {
        			
        			ejbPRJ.updatePmPrjEntry(details, AD_CMPNY);
        			
        		}
            		
        		
            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmContract(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmContract");

        LocalPmContractHome pmContractHome = null;
        
        PmContractControllerHome homeCTR = null;
        PmContractController ejbCTR = null;
        
        // Initialize EJB Home

        try {
        	
        	pmContractHome = (LocalPmContractHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmContractHome.JNDI_NAME, LocalPmContractHome.class);

        	homeCTR = (PmContractControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmContractControllerEJB", PmContractControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbCTR = homeCTR.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	URL url = new URL(ip_adress+"/pma/public/api/all-contracts");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);
    		

    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/
        	
        	Gson gson = new Gson();  
        	PmModSyncContractDetails[] contractsArray = gson.fromJson(response, PmModSyncContractDetails[].class);
        	
        	for(int i = 0; i < contractsArray.length; i++) {
        		
        		PmModSyncContractDetails contract = (PmModSyncContractDetails)contractsArray[i];

        		PmContractDetails details = new PmContractDetails();

        		details.setCtrCode(null);
        		try {
        			System.out.println("project.getPmContractID()="+contract.getPmContractID());
        			LocalPmContract pmContract = pmContractHome.findCtrByReferenceID(contract.getPmContractID(), AD_CMPNY);
        			
        			System.out.println("pmContract.getCtrCode()="+pmContract.getCtrCode());
        			details.setCtrCode(pmContract.getCtrCode());
        		}catch (FinderException e) { }

        		details.setCtrReferenceID(contract.getPmContractID());
				details.setCtrClientID(contract.getPmClientID());
				details.setCtrPrice(contract.getPmContractPrice());

            	try {
        		
        		if(details.getCtrCode() == null) {

        			ejbCTR.addPmCtrEntry(details, AD_CMPNY);
        			
        		} else {
        			
        			ejbCTR.updatePmCtrEntry(details, AD_CMPNY);
        		}
            		
        		
            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmContractTerm(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmContractTerm");

        LocalPmContractTermHome pmContractTermHome = null;
        
        PmContractTermControllerHome homeCT = null;
        PmContractTermController ejbCT = null;
        
        // Initialize EJB Home

        try {
        	
        	pmContractTermHome = (LocalPmContractTermHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmContractTermHome.JNDI_NAME, LocalPmContractTermHome.class);

        	homeCT = (PmContractTermControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmContractTermControllerEJB", PmContractTermControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbCT = homeCT.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	URL url = new URL(ip_adress+"/pma/public/api/all-contract-terms");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);
    		

    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/
        	
        	Gson gson = new Gson();  
        	PmModSyncContractTermDetails[] contractTermsArray = gson.fromJson(response, PmModSyncContractTermDetails[].class);
        	
        	for(int i = 0; i < contractTermsArray.length; i++) {
        		
        		PmModSyncContractTermDetails contractTerm = (PmModSyncContractTermDetails)contractTermsArray[i];

        		PmContractTermDetails details = new PmContractTermDetails();

        		details.setCtCode(null);
        		try {
        			System.out.println("contractTerm.getPmContractTermID()="+contractTerm.getPmContractTermID());
        			LocalPmContractTerm pmContractTerm = pmContractTermHome.findCtByReferenceID(contractTerm.getPmContractTermID(), AD_CMPNY);
        			
        			System.out.println("pmContractTerm.getCtCode()="+pmContractTerm.getCtCode());
        			details.setCtCode(pmContractTerm.getCtCode());
        		}catch (FinderException e) { }

        		details.setCtReferenceID(contractTerm.getPmContractTermID());
        		details.setCtTermDescription(contractTerm.getPmTermDescription());
				details.setCtValue(contractTerm.getPmValue());

            	try {
        		
        		if(details.getCtCode() == null) {

        			ejbCT.addPmCtEntry(details, contractTerm.getPmContractID(), AD_CMPNY);
        			
        		} else {
        			
        			ejbCT.updatePmCtEntry(details, contractTerm.getPmContractID(), AD_CMPNY);
        			
        		}
        		
        		
            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    
    
    
    
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmProjectType(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmProjectType");

        LocalPmProjectTypeHome pmProjectTypeHome = null;
        
        PmProjectTypeControllerHome homePRJTYP = null;
        PmProjectTypeController ejbPRJTYP = null;
        
        // Initialize EJB Home

        try {
        	
        	pmProjectTypeHome = (LocalPmProjectTypeHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectTypeHome.JNDI_NAME, LocalPmProjectTypeHome.class);

        	homePRJTYP = (PmProjectTypeControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmProjectTypeControllerEJB", PmProjectTypeControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbPRJTYP = homePRJTYP.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	URL url = new URL(ip_adress+"/pma/public/api/all-project-types");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);
    		

    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/
        	
        	Gson gson = new Gson();  
        	PmModSyncProjectTypeDetails[] projectTypesArray = gson.fromJson(response, PmModSyncProjectTypeDetails[].class);
        	
        	for(int i = 0; i < projectTypesArray.length; i++) {
        		
        		PmModSyncProjectTypeDetails projectType = (PmModSyncProjectTypeDetails)projectTypesArray[i];

        		PmProjectTypeDetails details = new PmProjectTypeDetails();

        		details.setPtCode(null);
        		try {
        			System.out.println("projectType.getPmProjectTypeID()="+projectType.getPmProjectTypeID());
        			LocalPmProjectType pmProjectType = pmProjectTypeHome.findPtByReferenceID(projectType.getPmProjectTypeID(), AD_CMPNY);
        			
        			System.out.println("pmProjectType.getPtCode()="+pmProjectType.getPtCode());
        			details.setPtCode(pmProjectType.getPtCode());
        		}catch (FinderException e) { }

        		details.setPtReferenceID(projectType.getPmProjectTypeID());
        		details.setPtProjectTypeCode(projectType.getPmProjectTypeCode());
				details.setPtValue(projectType.getPmProjectTypeValue());
            	try {
        		
        		if(details.getPtCode() == null) {

        			ejbPRJTYP.addPmPtEntry(details, AD_CMPNY);
        			
        		} else {
        			
        			ejbPRJTYP.updatePmPtEntry(details, AD_CMPNY);
        			
        		}
            		
        		
            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmProjectTypeType(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmProjectTypeType");

        LocalPmProjectTypeTypeHome pmProjectTypeTypeHome = null;
        
        PmProjectTypeTypeControllerHome homePTT = null;
        PmProjectTypeTypeController ejbPTT = null;
        
        // Initialize EJB Home

        try {
        	
        	pmProjectTypeTypeHome = (LocalPmProjectTypeTypeHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectTypeTypeHome.JNDI_NAME, LocalPmProjectTypeTypeHome.class);

        	homePTT = (PmProjectTypeTypeControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmProjectTypeTypeControllerEJB", PmProjectTypeTypeControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbPTT = homePTT.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	URL url = new URL(ip_adress+"/pma/public/api/all-project-type-types");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);
    		

    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/
        	
        	Gson gson = new Gson();  
        	PmModSyncProjectTypeTypeDetails[] projectTypeTypesArray = gson.fromJson(response, PmModSyncProjectTypeTypeDetails[].class);
        	
        	for(int i = 0; i < projectTypeTypesArray.length; i++) {
        		
        		PmModSyncProjectTypeTypeDetails projectTypeType = (PmModSyncProjectTypeTypeDetails)projectTypeTypesArray[i];

        		PmProjectTypeTypeDetails details = new PmProjectTypeTypeDetails();

        		details.setPttCode(null);
        		try {
        			System.out.println("projectTypeType.getPmProjectTypeTypeID()="+projectTypeType.getPmProjectTypeTypeID());
        			LocalPmProjectTypeType pmProjectTypeType = pmProjectTypeTypeHome.findPttByReferenceID(projectTypeType.getPmProjectTypeTypeID(), AD_CMPNY);

        			System.out.println("pmProject.getPrjCode()="+pmProjectTypeType.getPttCode());
        			details.setPttCode(pmProjectTypeType.getPttCode());
        		}catch (FinderException e) { }

        		details.setPttReferenceID(projectTypeType.getPmProjectTypeTypeID());
			

            	try {
        		
        		if(details.getPttCode() == null) {
        			
        			ejbPTT.addPmPttEntry(details, projectTypeType.getPmProjectID(), projectTypeType.getPmContractID(), projectTypeType.getPmProjectTypeID(), AD_CMPNY);
        			
        		} else {
        			
        			ejbPTT.updatePmPttEntry(details, projectTypeType.getPmProjectID(), projectTypeType.getPmContractID(), projectTypeType.getPmProjectTypeID(), AD_CMPNY);

        		}
            		
        		
            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmProjectPhase(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmProjectPhase");

        LocalPmProjectPhaseHome pmProjectPhaseHome = null;
        
        PmProjectPhaseControllerHome homePP = null;
        PmProjectPhaseController ejbPP = null;
        
        // Initialize EJB Home

        try {
        	
        	pmProjectPhaseHome = (LocalPmProjectPhaseHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectPhaseHome.JNDI_NAME, LocalPmProjectPhaseHome.class);

        	homePP = (PmProjectPhaseControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmProjectPhaseControllerEJB", PmProjectPhaseControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbPP = homePP.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	URL url = new URL(ip_adress+"/pma/public/api/all-project-phases");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);
    		

    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/
        	
        	Gson gson = new Gson();  
        	PmModSyncProjectPhaseDetails[] projectPhasesArray = gson.fromJson(response, PmModSyncProjectPhaseDetails[].class);
        	
        	for(int i = 0; i < projectPhasesArray.length; i++) {
        		
        		PmModSyncProjectPhaseDetails projectPhase = (PmModSyncProjectPhaseDetails)projectPhasesArray[i];

        		PmProjectPhaseDetails details = new PmProjectPhaseDetails();

        		details.setPpCode(null);
        		try {
        			System.out.println("project.getPmProjectID()="+projectPhase.getPmProjectPhaseID());
        			LocalPmProjectPhase pmProjectPhase = pmProjectPhaseHome.findPpByReferenceID(projectPhase.getPmProjectPhaseID(), AD_CMPNY);

        			System.out.println("pmProject.getPrjCode()="+pmProjectPhase.getPpCode());
        			details.setPpCode(pmProjectPhase.getPpCode());
        		}catch (FinderException e) { }

        		details.setPpReferenceID(projectPhase.getPmProjectPhaseID());
				details.setPpName(projectPhase.getPmProjectPhaseName());

            	try {
        		
        		if(details.getPpCode() == null) {
        			
        			ejbPP.addPmPpEntry(details, projectPhase.getPmContractTermID(), projectPhase.getPmProjectTypeTypeID(), AD_CMPNY);
        			
        		} else {
        			
        			ejbPP.updatePmPpEntry(details, projectPhase.getPmContractTermID(), projectPhase.getPmProjectTypeTypeID(), AD_CMPNY);

        		}
            		
        		
            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    
    
    

/**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmUser(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmUser");

        LocalPmUserHome pmUserHome = null;
        
        PmUserControllerHome homeUSR = null;
        PmUserController ejbUSR= null;
        
        // Initialize EJB Home

        try {
        	
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);

        	homeUSR = (PmUserControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmUserControllerEJB", PmUserControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbUSR = homeUSR.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	URL url = new URL(ip_adress+"/pma/public/api/all-users");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod( "GET" );
            conn.setDoInput( true );
            conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream(), "UTF-8")
            );

            String response = new String();
            for (String line; (line = rd.readLine()) != null; response += line);
    		

    		System.out.println("response="+response+"------------");
    		conn.disconnect();
        	/*Gson gson = new GsonBuilder().create();
        	JsonReader reader = new JsonReader(new StringReader(response));
        	reader.setLenient(true);*/
        	
        	Gson gson = new Gson();  
        	PmModSyncUserDetails[] usersArray = gson.fromJson(response, PmModSyncUserDetails[].class);
        	
        	for(int i = 0; i < usersArray.length; i++) {
        		
        		PmModSyncUserDetails user = (PmModSyncUserDetails)usersArray[i];

        		PmUserDetails details = new PmUserDetails();

        		details.setUsrCode(null);
        		try {
        			//System.out.println("user.getPmUserID()="+user.getPmUserID());
        			LocalPmUser pmUser = pmUserHome.findUsrByReferenceID(user.getPmUserID(), AD_CMPNY);
        			
        			//System.out.println("pmUser.getUsrCode()="+pmUser.getUsrCode());
        			details.setUsrCode(pmUser.getUsrCode());
        		}catch (FinderException e) { }
        		
        		details.setUsrReferenceID(user.getPmUserID());
				details.setUsrName(user.getPmName());
				details.setUsrUserName(user.getPmUserName());
				details.setUsrEmployeeNumber(user.getPmEmployeeNumber());
				details.setUsrRegularRate(user.getPmRegularRate() != null ? user.getPmRegularRate() : 0d);
				details.setUsrOvertimeRate(user.getPmOvertimeRate() != null ? user.getPmOvertimeRate() : 0d);
				details.setUsrPosition(user.getPmPosition());
				details.setUsrStatus(user.getPmStatus());

            	try {
        		
        		if(details.getUsrCode() == null) {

        			ejbUSR.addPmUsrEntry(details, AD_CMPNY);
        			
        		} else {
        			
        			ejbUSR.updatePmUsrEntry(details, AD_CMPNY);
        		}
            		
        		
            	}catch (Exception e) {
            		continue;
				}
        	}

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }

    
    
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmProjectTimeRecord(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmProjectTimeRecord");

        LocalPmProjectTimeRecordHome pmProjectTimeRecordHome = null;
        LocalPmProjectHome pmProjectHome = null;

        PmProjectTimeRecordControllerHome homePTR = null;
        PmProjectTimeRecordController ejbPTR = null;
        
        // Initialize EJB Home

        try {
        	
        	pmProjectTimeRecordHome = (LocalPmProjectTimeRecordHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectTimeRecordHome.JNDI_NAME, LocalPmProjectTimeRecordHome.class);
        	
        	pmProjectHome = (LocalPmProjectHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmProjectHome.JNDI_NAME, LocalPmProjectHome.class);

        	homePTR = (PmProjectTimeRecordControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/PmProjectTimeRecordControllerEJB", PmProjectTimeRecordControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbPTR = homePTR.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	
        	Collection pmProjects = pmProjectHome.findPrjAll(AD_CMPNY);
        	
        	Iterator i = pmProjects.iterator();
        	
        	while(i.hasNext()) {
        		
        		LocalPmProject pmProject = (LocalPmProject)i.next();
        		
        		int PRJ_CODE = pmProject.getPrjReferenceID();
        		
        		URL url = new URL(ip_adress+"/pma/public/api/get-project-time-records/"+PRJ_CODE);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod( "GET" );
                conn.setDoInput( true );
                conn.setRequestProperty("apikey", "8bje3|F?@y>rV2Uc_[1fS@.4\"e$AX>EM[-;+U};zi=^>F{wWzB!G$3Kqeh5dXHp");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }

                BufferedReader rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "UTF-8")
                );

                String response = new String();
                for (String line; (line = rd.readLine()) != null; response += line);
        		
                System.out.println("url="+url);
        		System.out.println("response="+response+"------------");
        		conn.disconnect();
            	/*Gson gson = new GsonBuilder().create();
            	JsonReader reader = new JsonReader(new StringReader(response));
            	reader.setLenient(true);*/
            	
            	Gson gson = new Gson();  
            	PmModSyncProjectTimeRecordDetails[] projectTimeRecordsArray = gson.fromJson(response, PmModSyncProjectTimeRecordDetails[].class);
            	 
            	for(int x = 0; x < projectTimeRecordsArray.length; x++) {
            		
            		PmModSyncProjectTimeRecordDetails projectTimeRecord = (PmModSyncProjectTimeRecordDetails)projectTimeRecordsArray[x];

            		PmProjectTimeRecordDetails details = new PmProjectTimeRecordDetails();
            		
            		details.setPtrCode(null);
            		try {
            			System.out.println("projectTimeRecord.getPmProjectID()="+projectTimeRecord.getPmProjectID());
            			System.out.println("projectTimeRecord.getPmUserID()="+projectTimeRecord.getPmUserID());
            			
            			LocalPmProjectTimeRecord pmProjectTimeRecord = pmProjectTimeRecordHome.findPtrByPrjReferenceIDAndUsrReferenceID(projectTimeRecord.getPmProjectID(), projectTimeRecord.getPmUserID(), AD_CMPNY);
            			
            			
            			details.setPtrCode(pmProjectTimeRecord.getPtrCode());
            		}catch (FinderException e) { }

            		details.setPtrRegularRate(projectTimeRecord.getPmRegularRate());
            		details.setPtrOvertimeRate(projectTimeRecord.getPmOvertimeRate());
            		details.setPtrRegularHours(projectTimeRecord.getPmRegularHours());
            		details.setPtrOvertimeHours(projectTimeRecord.getPmOvertimeHours());

                	try {
            		
            		if(details.getPtrCode() == null) {

            			ejbPTR.addPmPtrEntry(details, projectTimeRecord.getPmProjectID(), projectTimeRecord.getPmUserID(), AD_CMPNY);
            			
            		} else {

            			ejbPTR.updatePmPtrEntry(details, projectTimeRecord.getPmProjectID(), projectTimeRecord.getPmUserID(), AD_CMPNY);
            		}
                		
            		
                	}catch (Exception e) {
                		continue;
    				}
            	}
        		
        		
        		
        	}
        	
        	

	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmUserAsOmegaUser(Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("PmSynchronizerControllerBean setPmUserAsOmegaUser");

        LocalPmUserHome pmUserHome = null;
        LocalAdUserHome adUserHome = null;
        LocalAdUserResponsibilityHome adUserResponsibilityHome = null;

        AdUserControllerHome homeUSR = null;
        AdUserController ejbUSR = null;
        
        AdUserResponsibilityControllerHome homeUR = null;
        AdUserResponsibilityController ejbUR = null;
        
        // Initialize EJB Home

        try {
        	
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
        	
        	adUserHome = (LocalAdUserHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdUserHome.JNDI_NAME, LocalAdUserHome.class);
        	
        	adUserResponsibilityHome = (LocalAdUserResponsibilityHome)EJBHomeFactory.
                	lookUpLocalHome(LocalAdUserResponsibilityHome.JNDI_NAME, LocalAdUserResponsibilityHome.class);
        	

        	homeUSR = (AdUserControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/AdUserControllerEJB", AdUserControllerHome.class);
        	
        	homeUR = (AdUserResponsibilityControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/AdUserResponsibilityControllerEJB", AdUserResponsibilityControllerHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	
        	ejbUSR = homeUSR.create();
        	
        	ejbUR = homeUR.create();

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
        	

        	Collection pmUsers = pmUserHome.findAllUsrEmployeeNumber(AD_CMPNY);
        	
        	Iterator i = pmUsers.iterator();
        	
        	while(i.hasNext()){

        		LocalPmUser pmUser = (LocalPmUser)i.next();
        		
        		AdUserDetails details = new AdUserDetails();

        		details.setUsrCode(null);
        		try {
        			
        			LocalAdUser aduser = adUserHome.findByUsrPmUsrReferenceID(pmUser.getUsrReferenceID(), AD_CMPNY);

        			details.setUsrCode(aduser.getUsrCode());
        		}catch (FinderException e) { }

        		
                details.setUsrName(pmUser.getUsrUserName());
                details.setUsrDept("");
                details.setUsrDescription(pmUser.getUsrName());
                details.setUsrHead((byte)0);
                details.setUsrPassword(pmUser.getUsrUserName());
                details.setUsrPosition(pmUser.getUsrPosition());
                details.setUsrInspector((byte)0);
                
                //NONE EXPYR
                details.setUsrPasswordExpirationCode((short)2);	          

                details.setUsrPasswordExpirationDays((short)0);
                details.setUsrPasswordExpirationAccess((short)0);
                details.setUsrDateFrom(new Date());
                //details.setUsrDateTo(Common.convertStringToSQLDate(actionForm.getDateTo()));

            	try {
            		Integer USR_CODE = null;
        		
	        		if(details.getUsrCode() == null) {
	        			USR_CODE = ejbUSR.addAdUsrEntry(details, pmUser.getUsrEmployeeNumber(), AD_CMPNY);
	        		} else {
	        			USR_CODE = ejbUSR.updateAdUsrEntry(details, pmUser.getUsrEmployeeNumber(), AD_CMPNY);
	        		}

	        		AdUserResponsibilityDetails urdetails = new AdUserResponsibilityDetails();
        			
	        		urdetails.setUrCode(null);
	        		try {
	        			
	        			LocalAdUserResponsibility adUserResponsibility = adUserResponsibilityHome.findByUrPmUsrReferenceID(pmUser.getUsrReferenceID(), AD_CMPNY);
	        			
	        			urdetails.setUrCode(adUserResponsibility.getUrCode());
	        		}catch (FinderException e) { }
	        		
	        		urdetails.setUrDateFrom(new Date());
	        		
	        		if(urdetails.getUrCode() == null) {
	        			
	        			ejbUR.addAdUrEntry(urdetails, USR_CODE, 
	                    	    "Officers and Employees",AD_CMPNY);
	        		} else {
	        			
	        			ejbUR.updateAdUrEntry(urdetails, USR_CODE, 
	                    	    "Officers and Employees",AD_CMPNY);
	        		}
	        		
	        		
            	}catch (Exception e) {
            		continue;
				}

            	
	
        	}
        	
	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public int setPmUsersAsCustomerAndSupplier(String USR_NM, Integer AD_BRNCH, Integer AD_CMPNY) {
                    
        Debug.print("HrSynchronizerControllerBean setPmUsersAsCustomerAndSupplier");

        
        ApSupplierEntryControllerHome homeSPL = null;
        ApSupplierEntryController ejbSPL = null;
        
        ArCustomerEntryControllerHome homeCST = null;
        ArCustomerEntryController ejbCST = null;
        
        LocalArCustomerHome arCustomerHome = null;
        LocalApSupplierHome apSupplierHome = null;
        LocalArCustomerClassHome arCustomerClassHome = null;
        LocalApSupplierClassHome apSupplierClassHome = null;
        
        LocalPmUserHome pmUserHome = null;
        
        LocalGlChartOfAccountHome glChartOfAccountHome = null;
        
        // Initialize EJB Home

        try {

        	homeSPL = (ApSupplierEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ApSupplierEntryControllerEJB", ApSupplierEntryControllerHome.class);
        	
        	homeCST = (ArCustomerEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArCustomerEntryControllerEJB", ArCustomerEntryControllerHome.class);
        	
        	arCustomerHome = (LocalArCustomerHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerHome.JNDI_NAME, LocalArCustomerHome.class);
        	apSupplierHome = (LocalApSupplierHome)EJBHomeFactory.
                	lookUpLocalHome(LocalApSupplierHome.JNDI_NAME, LocalApSupplierHome.class);
        	
        	arCustomerClassHome = (LocalArCustomerClassHome)EJBHomeFactory.
                	lookUpLocalHome(LocalArCustomerClassHome.JNDI_NAME, LocalArCustomerClassHome.class);
        	
        	apSupplierClassHome = (LocalApSupplierClassHome)EJBHomeFactory.
                	lookUpLocalHome(LocalApSupplierClassHome.JNDI_NAME, LocalApSupplierClassHome.class);
        	
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                	lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
        	
        	
        	glChartOfAccountHome = (LocalGlChartOfAccountHome)EJBHomeFactory.
                	lookUpLocalHome(LocalGlChartOfAccountHome.JNDI_NAME, LocalGlChartOfAccountHome.class);
        	
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
        	ejbSPL = homeSPL.create();
        	
        	ejbCST = homeCST.create();
        	
        	
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CreateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {

        	LocalArCustomerClass arCustomerClass = arCustomerClassHome.findByCcName("Officers and Employees", AD_CMPNY);
        	LocalApSupplierClass apSupplierClass = apSupplierClassHome.findByScName("Officers and Employees", AD_CMPNY);
        	
        	
        	String RECEIVABLE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaReceivableAccount(), AD_CMPNY).getCoaAccountNumber();
        	String REVENUE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaRevenueAccount(), AD_CMPNY).getCoaAccountNumber();
        	String UNEARNED_INT_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaUnEarnedInterestAccount(), AD_CMPNY).getCoaAccountNumber();
        	String EARNED_INT_ACCNT = (String) glChartOfAccountHome.findByCoaCode(arCustomerClass.getCcGlCoaEarnedInterestAccount(), AD_CMPNY).getCoaAccountNumber();
        	
        	String PAYABLE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(apSupplierClass.getScGlCoaPayableAccount(), AD_CMPNY).getCoaAccountNumber();
        	String EXPENSE_ACCNT = (String) glChartOfAccountHome.findByCoaCode(apSupplierClass.getScGlCoaExpenseAccount(), AD_CMPNY).getCoaAccountNumber();
        	
        	
        	Collection pmUsers = pmUserHome.findAllUsrEmployeeNumber(AD_CMPNY);
        	
        	Iterator i = pmUsers.iterator();
        	
        	while(i.hasNext()) {
        		
        		LocalPmUser pmUser = (LocalPmUser)i.next();
        		
        		
        		Integer PM_USR_ID = pmUser.getUsrReferenceID();
        		String PM_USR_EMP_NMBR = pmUser.getUsrEmployeeNumber();
        		String NAME = pmUser.getUsrName();

        		ApModSupplierDetails splDetails = new ApModSupplierDetails();
        		
        		splDetails.setSplCode(null); 
        		
        		try {

        			System.out.println("PM_USR_EMP_NMBR="+PM_USR_EMP_NMBR);
        			LocalApSupplier apSupplier = apSupplierHome.findBySplSupplierCode(PM_USR_EMP_NMBR,AD_CMPNY);
        			splDetails.setSplCode(apSupplier.getSplCode());
            		
        		}catch (Exception e) { }

        		splDetails.setSplEnable((byte)1);
        		splDetails.setSplSupplierCode(PM_USR_EMP_NMBR);
        		splDetails.setSplName(NAME);
        		
				ArrayList bSplList = new ArrayList();

				AdModBranchSupplierDetails splmDetails = new AdModBranchSupplierDetails();
				splmDetails.setBSplBranchCode(AD_BRNCH);
				splmDetails.setBSplPayableAccountNumber(PAYABLE_ACCNT);
				splmDetails.setBSplExpenseAccountNumber(EXPENSE_ACCNT);

				bSplList.add(splmDetails);
					
				
        		
        		ArModCustomerDetails cstDetails = new ArModCustomerDetails();
        		
        		cstDetails.setCstCode(null); 
        		
        		try {
        			LocalArCustomer arCustomer = arCustomerHome.findByCstPmUsrReferenceID(PM_USR_ID, AD_CMPNY);
        			cstDetails.setCstCode(arCustomer.getCstCode());
            		
        		}catch (Exception e) { }

        		
        		cstDetails.setCstEnable((byte)1);
        		cstDetails.setCstAutoComputeInterest((byte)1);
        		cstDetails.setCstCustomerCode(PM_USR_EMP_NMBR);
        		cstDetails.setCstName(NAME);
        		cstDetails.setCstEmployeeID(PM_USR_EMP_NMBR);
        		cstDetails.setCstSquareMeter(0d);
        		cstDetails.setCstNumbersParking(0d);
        		cstDetails.setCstMonthlyInterestRate(1d);
        		cstDetails.setCstRealPropertyTaxRate(0d);
        		cstDetails.setCstAssociationDuesRate(0d);
        		
    			cstDetails.setCstCreatedBy(USR_NM);
    			cstDetails.setCstDateCreated(new java.util.Date());
    			cstDetails.setCstLastModifiedBy(USR_NM);
    			cstDetails.setCstDateLastModified(new java.util.Date());
        		
        		
        		ArrayList bCstList = new ArrayList();
	
            	AdModBranchCustomerDetails cstmDetails = new AdModBranchCustomerDetails();
            	
            	cstmDetails.setBcstBranchCode("HQ");
            	cstmDetails.setBcstBranchName("Head Office");
            	cstmDetails.setBcstReceivableAccountNumber(RECEIVABLE_ACCNT);
            	cstmDetails.setBcstRevenueAccountNumber(REVENUE_ACCNT);
            	cstmDetails.setBcstUnEarnedInterestAccountNumber(UNEARNED_INT_ACCNT);
            	cstmDetails.setBcstEarnedInterestAccountNumber(EARNED_INT_ACCNT);
            	cstmDetails.setBcstUnEarnedPenaltyAccountNumber(null);
            	cstmDetails.setBcstEarnedPenaltyAccountNumber(null);
            	
            	bCstList.add(cstmDetails);

            	cstDetails.setBcstList(bCstList);
                	
                
            	try {
        		
        		ejbSPL.saveApSplEntry(splDetails, null, "IMMEDIATE", apSupplierClass.getScName(), 
    					PAYABLE_ACCNT, EXPENSE_ACCNT, 
        				"Security Bank", "Super", bSplList, null, AD_CMPNY);
            		
        		
        		ejbCST.saveArCstEntry(cstDetails, null, "IMMEDIATE", PM_USR_EMP_NMBR, arCustomerClass.getCcName(), 
        				RECEIVABLE_ACCNT, REVENUE_ACCNT, UNEARNED_INT_ACCNT, EARNED_INT_ACCNT, null, null, 
        				"Security Bank", (int)1, null, null, null, 
        				null, null, //HR
        				PM_USR_EMP_NMBR, //PM
        				
        				false, AD_BRNCH, AD_CMPNY);
        		
            	}catch (Exception e) {
            		continue;
				}
        		
        		
        	}
        	
           
	        return 0;
	        
        } catch (Exception ex) {
        	
        	Debug.printStackTrace(ex);
        	throw new EJBException(ex.getMessage());
        	
        }
            
    }


    
    
       
    
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

        Debug.print("PmSynchronizerControllerBean ejbCreate");
      
    }
    
}

