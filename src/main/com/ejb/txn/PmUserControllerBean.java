
/*
 * PmUserControllerBean.java
 *
 * Created on May 13, 2003, 9:13 AM
 *
 * @author  Clint Arrogante
 */

package com.ejb.txn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.FinderException;
import javax.naming.NamingException;


import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.pm.LocalPmUser;
import com.ejb.pm.LocalPmUserHome;
import com.util.AbstractSessionBean;
import com.util.PmUserDetails;
import com.util.Debug;
import com.util.EJBHomeFactory;

/**
 * @ejb:bean name="PmUserControllerBean"
 *           display-name="Used for entering project"
 *           type="Stateless"
 *           view-type="remote"
 *           jndi-name="ejb/PmUserControllerEJB"
 *
 * @ejb:interface remote-class="com.ejb.txn.PmUserController"
 *                extends="javax.ejb.EJBObject"
 *
 * @ejb:home remote-class="com.ejb.txn.PmUserControllerHome"
 *           extends="javax.ejb.EJBHome"
 *
 * @ejb:transaction type="Required"
 *
 * @ejb:security-role-ref role-name="pmuser"
 *                        role-link="pmuserlink"
 *
 * @ejb:permission role-name="pmuser"
 * 
*/

public class PmUserControllerBean extends AbstractSessionBean {
    
    
    /**
     * @ejb:interface-method view-type="remote"
     * @jboss:method-attributes read-only="true"
     **/
    public ArrayList getPmUsrAll(Integer AD_CMPNY) 
        throws GlobalNoRecordFoundException {
                    
        Debug.print("PmUserControllerBean getPmUsrAll");
        
        LocalPmUserHome pmUserHome = null;
        
        Collection pmUsers = null;
        
        LocalPmUser pmUser = null;
        
        
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
            
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try {
            
        	pmUsers = pmUserHome.findUsrAll(AD_CMPNY);
            
        } catch (FinderException ex) {
            
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
        }
        
        if (pmUsers.isEmpty()) {
        	
            throw new GlobalNoRecordFoundException();
        	
        }
        
        Iterator i = pmUsers.iterator();
               
        while (i.hasNext()) {
        	
        	pmUser = (LocalPmUser)i.next();
        
                
        	PmUserDetails details = new PmUserDetails();
        	
	    		/*details.setPpCode(adBank.getBnkCode());        		
	            details.setBnkName(adBank.getBnkName());
	            details.setBnkBranch(adBank.getBnkBranch());
	            details.setBnkNumber(adBank.getBnkNumber());
	            details.setBnkInstitution(adBank.getBnkInstitution());
	            details.setBnkDescription(adBank.getBnkDescription());
	            details.setBnkAddress(adBank.getBnkAddress());
	            details.setBnkEnable(adBank.getBnkEnable());*/
                                                        	
        	list.add(details);
        }
        
        return list;
            
    }
    
    /**
     * @ejb:interface-method view-type="remote"
     **/    
    public void addPmUsrEntry(com.util.PmUserDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmUserControllerBean addPmUsrEntry");
        
        LocalPmUserHome pmUserHome = null;

        LocalPmUser pmUser = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
                              
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            System.out.println("1----------------->details.getUsrReferenceID()="+details.getUsrReferenceID());
        	pmUser = pmUserHome.findUsrByReferenceID(details.getUsrReferenceID(), AD_CMPNY);
        	 System.out.println("----------------");
           throw new GlobalRecordAlreadyExistException();
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
           throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
           throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	
        	// create new user
        	System.out.println("2----------------");
        	pmUser = pmUserHome.create(details.getUsrReferenceID(), details.getUsrName(), details.getUsrUserName(), details.getUsrEmployeeNumber(), 
        			details.getUsrPosition(), details.getUsrRegularRate(), details.getUsrOvertimeRate() , details.getUsrStatus(), 
        			AD_CMPNY);
        	System.out.println("3----------------");    	       
        } catch (Exception ex) {
        	
        	getSessionContext().setRollbackOnly();
            throw new EJBException(ex.getMessage());
        	
        }    
        
    }
    
    
    
    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void updatePmUsrEntry(com.util.PmUserDetails details, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyExistException {
                    
        Debug.print("PmUserControllerBean updatePmUsrEntry");
        
        LocalPmUserHome pmUserHome = null;

        LocalPmUser pmUser = null;
                
        ArrayList list = new ArrayList();
        
        // Initialize EJB Home
        
        try {
            
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
        try { 
            
            LocalPmUser adExistingProject = 
                	pmUserHome.findUsrByReferenceID(details.getUsrReferenceID(), AD_CMPNY);
            
            
            
            if (!adExistingProject.getUsrCode().equals(details.getUsrCode())) {
            
                 throw new GlobalRecordAlreadyExistException();
                 
            }
                                             
        } catch (GlobalRecordAlreadyExistException ex) {
        	
            throw ex;
        	
        } catch (FinderException ex) {
        	 	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
               
        try {
        	        	
        	// find and update bank
        	
        	pmUser = pmUserHome.findByPrimaryKey(details.getUsrCode());

        	pmUser.setUsrName(details.getUsrName());
        	pmUser.setUsrUserName(details.getUsrUserName());
        	pmUser.setUsrPosition(details.getUsrPosition());
        	pmUser.setUsrEmployeeNumber(details.getUsrEmployeeNumber());
        	pmUser.setUsrRegularRate(details.getUsrRegularRate());
        	pmUser.setUsrOvertimeRate(details.getUsrOvertimeRate());
        	pmUser.setUsrStatus(details.getUsrStatus());
        	
        	
        } catch (Exception ex) {
            
            throw new EJBException(ex.getMessage());
            
        }
        
    }
    

    /**
     * @ejb:interface-method view-type="remote"
     **/
    public void deletePmUserEntry(Integer CTR_CODE, Integer AD_CMPNY) 
        throws GlobalRecordAlreadyDeletedException {
                    
        Debug.print("PmUserControllerBean deletePmUserEntry");
        
        LocalPmUserHome pmUserHome = null;

        LocalPmUser pmUser = null;     
        
        // Initialize EJB Home
        
        try {
            
        	pmUserHome = (LocalPmUserHome)EJBHomeFactory.
                    lookUpLocalHome(LocalPmUserHome.JNDI_NAME, LocalPmUserHome.class);
        	
        } catch (NamingException ex) {
            
            throw new EJBException(ex.getMessage());
            
        }

	    try {
         
	    	pmUser = pmUserHome.findByPrimaryKey(CTR_CODE);

	    } catch (FinderException ex) {
	    	
	         throw new GlobalRecordAlreadyDeletedException();
	    
	    }
	    
	    try {

	    	pmUser.remove();        	

        } catch (Exception ex) {
        	
        	 throw new EJBException(ex.getMessage());
        	
        }
        
	}
    
        
    // SessionBean methods

    /**
     * @ejb:create-method view-type="remote"
     **/
    public void ejbCreate() throws CreateException {

       Debug.print("PmUserControllerBean ejbCreate");
      
    }
}
