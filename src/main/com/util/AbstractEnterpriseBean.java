/*
 * JBoss, the OpenSource J2EE webOS
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package com.util;

import javax.naming.Context;

/**
 *      
 *   @see <related>
 *   @author $Author: root $
 *   @version $Revision: 1.1.1.1 $
 */
public abstract class AbstractEnterpriseBean
{
   // Constants -----------------------------------------------------
    
   // Attributes ----------------------------------------------------
   private Context ctx;
   
   // Static --------------------------------------------------------

   // Constructors --------------------------------------------------
   
   // Public --------------------------------------------------------
   
   // Protected -----------------------------------------------------
}
