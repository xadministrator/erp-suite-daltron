package com.util;

import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.RemoveException;

public abstract class AbstractEntityBean
   extends AbstractEnterpriseBean
   implements EntityBean
{
   // Constants -----------------------------------------------------
    
   // Attributes ----------------------------------------------------
   protected EntityContext entityCtx;
   
   // Static --------------------------------------------------------

   // Constructors --------------------------------------------------
   
   // Public --------------------------------------------------------
   public EntityContext getEntityContext() 
   {
       return entityCtx;
   }
   
   public void setEntityContext(EntityContext ctx) 
   {
      entityCtx = ctx;
   }
   public void unsetEntityContext() 
   { 
   }
	
   public void ejbActivate() 
   {
   }
	
   public void ejbPassivate() 
   {
   }
	
   public void ejbLoad()
   {
   }
	
   public void ejbStore() 
   { 
   }
	
   public void ejbRemove() 
      throws RemoveException
   {
   }
}
