package com.util;

import javax.ejb.CreateException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import org.apache.log4j.Category;

public abstract class AbstractSessionBean
   extends AbstractEnterpriseBean
   implements SessionBean
{
   protected transient Category log = Category.getInstance(getClass());

   protected SessionContext sessionCtx;
   
   public void ejbCreate()
      throws CreateException
   {
   }
   
   public void setSessionContext(SessionContext ctx) 
   {
      sessionCtx = ctx;
   }
   
   public SessionContext getSessionContext()
   {
      return sessionCtx;
   }
   	
   public void ejbActivate() 
   {
   }
	
   public void ejbPassivate() 
   {
   }
	
   public void ejbRemove() 
   {
   }

   private void writeObject(java.io.ObjectOutputStream stream)
      throws java.io.IOException
   {
      // nothing
   }
   
   private void readObject(java.io.ObjectInputStream stream)
      throws java.io.IOException, ClassNotFoundException
   {
      // reset logging
      log = Category.getInstance(getClass());
   }
}
