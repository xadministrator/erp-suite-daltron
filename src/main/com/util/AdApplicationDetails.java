package com.util;


public class AdApplicationDetails implements java.io.Serializable {

   private Integer APP_CODE;
   private String APP_NM;
   private String APP_DESC;

   public AdApplicationDetails () { ; }

   public AdApplicationDetails (Integer APP_CODE, String APP_NM,
      String APP_DESC) {

      this.APP_CODE = APP_CODE;
      this.APP_NM = APP_NM;
      this.APP_DESC = APP_DESC;

   }

   public Integer getAppCode() {
      return APP_CODE;
   }

   public String getAppName() {
      return APP_NM;
   }

   public String getAppDescription() {
      return APP_DESC;
   }

   public String toString() {
      String s = "APP_CODE = " + APP_CODE + " APP_NM = " + APP_NM +
         " APP_DESC = " + APP_DESC;
      return s;
   }

} // AdRecurringApplicatoinDetails class   
