package com.util;


public class AdDocumentCategoryDetails implements java.io.Serializable {

   private Integer DC_CODE;
   private String DC_NM;
   private String DC_DESC;

   public AdDocumentCategoryDetails () { ; }

   public AdDocumentCategoryDetails (Integer DC_CODE, String DC_NM,
      String DC_DESC) {

      this.DC_CODE = DC_CODE;
      this.DC_NM = DC_NM;
      this.DC_DESC = DC_DESC;

   }
   
   public AdDocumentCategoryDetails(String DC_NM, String DC_DESC) {
   	
   	  this.DC_NM = DC_NM;
   	  this.DC_DESC = DC_DESC;
   	  
   }   	  

   public Integer getDcCode() {
      return DC_CODE;
   }

   public String getDcName() {
      return DC_NM;
   }

   public String getDcDescription() {
      return DC_DESC;
   }

   public String toString() {
      String s = "DC_CODE = " + DC_CODE + " DC_NM = " + DC_NM + 
         " DC_DESC = " + DC_DESC;
      return s;
   }

} // AdDocumentCategoryDetails class   
