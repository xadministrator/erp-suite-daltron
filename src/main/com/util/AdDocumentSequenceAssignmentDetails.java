package com.util;


public class AdDocumentSequenceAssignmentDetails implements java.io.Serializable {

   private Integer DSA_CODE;
   private Integer DSA_SOB_CODE;
   private String DSA_NXT_SQNC;

   public AdDocumentSequenceAssignmentDetails () { ; }

   public AdDocumentSequenceAssignmentDetails (Integer DSA_CODE, Integer DSA_SOB_CODE,
      String DSA_NXT_SQNC) {

      this.DSA_CODE = DSA_CODE;
      this.DSA_SOB_CODE = DSA_SOB_CODE;
      this.DSA_NXT_SQNC = DSA_NXT_SQNC;

   }

   public Integer getDsaCode() {
      return DSA_CODE;
   }

   public Integer getDsaSobCode() {
      return DSA_SOB_CODE;
   }

   public String getDsaNextSequence() {
      return DSA_NXT_SQNC;
   }

   public String toString() {
      String s = "DSA_CODE = " + DSA_CODE + " DSA_SOB_CODE = " + DSA_SOB_CODE +
	 " DSA_NXT_SQNC = " + DSA_NXT_SQNC;
      return s;
   }

} // AdDocumentSequenceAssignmentDetails class   
