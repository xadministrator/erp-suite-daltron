package com.util;


public class AdModApprovalCoaLineDetails extends AdApprovalCoaLineDetails implements java.io.Serializable {

   private String ACL_COA_ACCNT_NMBR;
   private String ACL_COA_ACCNT_DESC;
   
   public AdModApprovalCoaLineDetails() { ; }

   public String getAclCoaAccountNumber() {
   	
   	  return ACL_COA_ACCNT_NMBR;
   	
   }
   
   public void setAclCoaAccountNumber(String ACL_COA_ACCNT_NMBR) {
   	
   	  this.ACL_COA_ACCNT_NMBR = ACL_COA_ACCNT_NMBR;
   	
   }
   
   public String getAclCoaDescription() {
   	
   	  return ACL_COA_ACCNT_DESC;
   	
   }
   
   public void setAclCoaDescription(String ACL_COA_ACCNT_DESC) {
   	
   	  this.ACL_COA_ACCNT_DESC = ACL_COA_ACCNT_DESC;
   	
   }

} // AdModApprovalCoaLineDetails class
