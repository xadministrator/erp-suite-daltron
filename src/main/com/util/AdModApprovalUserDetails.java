package com.util;


public class AdModApprovalUserDetails extends AdApprovalUserDetails implements java.io.Serializable {

    private String AU_USR_NM;

    public AdModApprovalUserDetails() { ; }

    public String getAuUsrName() {
   	
   	    return AU_USR_NM;
   	
    }
   
    public void setAuUsrName(String AU_USR_NM) {
   	
   	    this.AU_USR_NM = AU_USR_NM;
   	
    }
       	    	      
} // AdModApprovalUserDetails class
