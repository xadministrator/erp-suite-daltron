package com.util;


public class AdModBranchDetails extends AdBranchDetails implements java.io.Serializable {

    private String BR_GL_COA_ACCNT_NMBR;
    private String BR_GL_COA_ACCNT_DESC;
    
    public AdModBranchDetails() { ; }
        
    public String getBrGlCoaAccountNumber() {
   	
   	    return BR_GL_COA_ACCNT_NMBR;
   	
    }
   
    public void setBrGlCoaAccountNumber(String BR_GL_COA_ACCNT_NMBR) {
   	
   	    this.BR_GL_COA_ACCNT_NMBR = BR_GL_COA_ACCNT_NMBR;
   	
    }
   
    public String getBrGlCoaAccountDescription() {
   	
   	    return BR_GL_COA_ACCNT_DESC;
   	 
    }
    
    public void setBrGlCoaAccountDescription(String BR_GL_COA_ACCNT_DESC) {
    	
    	this.BR_GL_COA_ACCNT_DESC = BR_GL_COA_ACCNT_DESC;
    	
    } 
    	    	    	      
} // AdModBranchDetails class
