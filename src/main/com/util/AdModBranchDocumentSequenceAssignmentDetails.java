
package com.util;

public class AdModBranchDocumentSequenceAssignmentDetails extends AdBranchDocumentSequenceAssignmentDetails implements java.io.Serializable { 
    
    private Integer BR_CODE;
    
    public AdModBranchDocumentSequenceAssignmentDetails() { ; }
    
    public Integer getBrCode() {
        
        return BR_CODE;
        
    }
    
    public void setBrCode(Integer BR_CODE) {
        
        this.BR_CODE = BR_CODE;
        
    }
 
} // AdModBranchDocumentSequenceAssignmentDetails class
