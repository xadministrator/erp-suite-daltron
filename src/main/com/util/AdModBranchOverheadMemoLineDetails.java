package com.util;

/**
 * 
 * @author Franco Antonio R. Roig
 * Created: 10/27/2005	1:04 PM
 * 
 */

public class AdModBranchOverheadMemoLineDetails {
	
	private Integer BR_CODE = null;
	private String BR_NM = null;
	private String BOML_GL_COA_OVRHD_ACCNT_NMBR = null;
	private String BOML_GL_COA_OVRHD_ACCNT_DESC = null;
	private String BOML_GL_COA_LBLTY_ACCNT_NMBR = null;
	private String BOML_GL_COA_LBLTY_ACCNT_DESC = null;
	
	public AdModBranchOverheadMemoLineDetails() { ; }
	
	public Integer getBOMLBranchCode() {
		
		return this.BR_CODE;
		
	}
	
	public void setBOMLBranchCode(Integer BR_CODE) {
		
		this.BR_CODE = BR_CODE;
		
	}
	
	public String getBOMLBranchName() {
		
		return this.BR_NM;
		
	}
	
	public void setBOMLBranchName(String BR_NM) {
		
		this.BR_NM = BR_NM;
		
	}
	
	public String getBOMLOverheadAccountNumber() {
		
		return BOML_GL_COA_OVRHD_ACCNT_NMBR;
		
	}
	
	public void setBOMLOverheadAccountNumber(String BOML_GL_COA_OVRHD_ACCNT_NMBR) {
		
		this.BOML_GL_COA_OVRHD_ACCNT_NMBR = BOML_GL_COA_OVRHD_ACCNT_NMBR;
		
	}
	
	public String getBOMLOverheadAccountDesc() {
		
		return BOML_GL_COA_OVRHD_ACCNT_DESC;
		
	}
	
	public void setBOMLOverheadAccountDesc(String BOML_GL_COA_OVRHD_ACCNT_DESC) {
		
		this.BOML_GL_COA_OVRHD_ACCNT_DESC = BOML_GL_COA_OVRHD_ACCNT_DESC;
		
	}
	
	public String getBOMLLiabilityAccountNumber() {
		
		return BOML_GL_COA_LBLTY_ACCNT_NMBR;
		
	}
	
	public void setBOMLLiabilityAccountNumber(String BOML_GL_COA_LBLTY_ACCNT_NMBR) {
		
		this.BOML_GL_COA_LBLTY_ACCNT_NMBR = BOML_GL_COA_LBLTY_ACCNT_NMBR;
		
	}
	
	public String getBOMLLiabilityAccountDesc() {
		
		return BOML_GL_COA_LBLTY_ACCNT_NMBR;
		
	}
	
	public void setBOMLLiabilityAccountDesc(String BOML_GL_COA_LBLTY_ACCNT_DESC) {
		
		this.BOML_GL_COA_LBLTY_ACCNT_DESC = BOML_GL_COA_LBLTY_ACCNT_DESC;
		
	}
}