package com.util;


public class AdModFormFunctionResponsibilityDetails extends AdFormFunctionResponsibilityDetails implements java.io.Serializable {

   private String FR_FF_NM;
   private String FR_RS_NM;
   private Integer FR_FF_CODE;

   public AdModFormFunctionResponsibilityDetails () { ; }

   public AdModFormFunctionResponsibilityDetails (Integer FR_CODE, String FR_PARAM, Integer FR_FF_CODE, String FR_FF_NM) {
         	
      super(FR_CODE, FR_PARAM, new Integer(0));  
        
      this.FR_FF_NM = FR_FF_NM;
      this.FR_FF_CODE = FR_FF_CODE;

   }
   
   public String getFrFfFormFunctionName() {
   	
   	   return FR_FF_NM;
   	
   }
   
   public void setFrFfFormFunctionName(String FR_FF_NM) {
   	
   	   this.FR_FF_NM = FR_FF_NM;
   	
   }
   
   public Integer getFrFfCode() {
   	
   	   return FR_FF_CODE;
   	
   }
   
   public void setFrFfCode(Integer FR_FF_CODE) {
   	
   	   this.FR_FF_CODE = FR_FF_CODE;
   	
   }
   
}
 

// AdModFormFunctionResponsibilityDetails class   
