package com.util;


public class AdModPaymentTermDetails extends AdPaymentTermDetails implements java.io.Serializable {

    private String PT_GL_COA_ACCNT_NMBR;
    private String PT_GL_COA_ACCNT_DESC;
    
    public AdModPaymentTermDetails() { ; }
        
    public String getPtGlCoaAccountNumber() {
   	
   	    return PT_GL_COA_ACCNT_NMBR;
   	
    }
   
    public void setPtGlCoaAccountNumber(String PT_GL_COA_ACCNT_NMBR) {
   	
   	    this.PT_GL_COA_ACCNT_NMBR = PT_GL_COA_ACCNT_NMBR;
   	
    }
   
    public String getPtGlCoaAccountDescription() {
   	
   	    return PT_GL_COA_ACCNT_DESC;
   	 
    }
    
    public void setPtGlCoaAccountDescription(String PT_GL_COA_ACCNT_DESC) {
    	
    	this.PT_GL_COA_ACCNT_DESC = PT_GL_COA_ACCNT_DESC;
    	
    } 
    	    	    	      
} // AdModPaymentTermDetails class
