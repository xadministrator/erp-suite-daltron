package com.util;

import java.util.Comparator;


public class AdModUserDetails extends AdUserDetails implements java.io.Serializable {
   
 

   public AdModUserDetails() { ; }

 
   
 //AD_USER_DEPARTMENT
   public static Comparator DepartmentComparator = new Comparator() {
	   
	   public int compare(Object SMT, Object anotherSMT) {
		   
		   String AD_USR_DPT1 = ((AdModUserDetails) SMT).getUsrDept();	
		   String AD_USR_CD1 = ((AdModUserDetails) SMT).getUsrName();
		   
		   String AD_USR_DPT2 = ((AdModUserDetails) anotherSMT).getUsrDept();
		   String AD_USR_CD2 = ((AdModUserDetails) SMT).getUsrName();
		   
		
		   
		   if (!(AD_USR_DPT1.equals(AD_USR_DPT2))) {
			   
			   return AD_USR_DPT1.compareTo(AD_USR_DPT2);
			   
		   } else {
			   
			   return AD_USR_CD1.compareTo(AD_USR_CD2);
			   
		   }
		   
	   }
	   
   };


} // AdModCompanyDetails class
