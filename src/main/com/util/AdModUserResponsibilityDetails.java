package com.util;

import java.util.Date;

public class AdModUserResponsibilityDetails extends AdUserResponsibilityDetails implements java.io.Serializable {

   private String UR_RS_NM; 
    
   public AdModUserResponsibilityDetails () { ; }

   public AdModUserResponsibilityDetails (Integer UR_CODE, 
         Date UR_DT_FRM, Date UR_DT_TO, String UR_RS_NM) {
         	
      super(UR_CODE, UR_DT_FRM, UR_DT_TO, new Integer(0));  
        
      this.UR_RS_NM = UR_RS_NM;
      
   }
   
   public String getUrResponsibilityName() {
   	
   	   return UR_RS_NM;
   	
   }
   
   public void setUrResponsibilityName(String UR_RS_NM) {
   	
   	   this.UR_RS_NM = UR_RS_NM;
   	
   }
   
} // AdModUserResponsibilityDetails class   
