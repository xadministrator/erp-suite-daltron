package com.util;

public class ApModCanvassDetails extends ApCanvassDetails implements java.io.Serializable {
	
	private String CNV_SPL_SPPLR_CD = null;
	private String CNV_SPL_SPPLR_NAME = null;
	
	public ApModCanvassDetails() { ; }
	
	public String getCnvSplSupplierCode() {
		
		return CNV_SPL_SPPLR_CD;
		
	}
	
	public void setCnvSplSupplierCode(String CNV_SPL_SPPLR_CD) {
		
		this.CNV_SPL_SPPLR_CD = CNV_SPL_SPPLR_CD;
		
	}
	
	public String getCnvSplSupplierName() {
		
		return CNV_SPL_SPPLR_NAME;
		
	}
	
	public void setCnvSplSupplierName(String CNV_SPL_SPPLR_NAME) {
		
		this.CNV_SPL_SPPLR_NAME = CNV_SPL_SPPLR_NAME;
		
	}
	
}