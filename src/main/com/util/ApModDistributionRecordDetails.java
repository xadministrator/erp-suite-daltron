package com.util;


public class ApModDistributionRecordDetails extends ApDistributionRecordDetails implements java.io.Serializable {

   private String DR_COA_ACCNT_NMBR;
   private String DR_COA_ACCNT_DESC;
   private byte DR_COA_DBT;
   private double DR_AMNT;
   private String DR_CLSS;
   
   public ApModDistributionRecordDetails() { ; }

   public String getDrCoaAccountNumber() {
   	
   	  return DR_COA_ACCNT_NMBR;
   	
   }
   
   public void setDrCoaAccountNumber(String DR_COA_ACCNT_NMBR) {
   	
   	  this.DR_COA_ACCNT_NMBR = DR_COA_ACCNT_NMBR;
   	
   }
   
   public String getDrCoaAccountDescription() {
   	
   	  return DR_COA_ACCNT_DESC;
   	
   }
   
   public void setDrCoaAccountDescription(String DR_COA_ACCNT_DESC) {
   	
   	  this.DR_COA_ACCNT_DESC = DR_COA_ACCNT_DESC;
   	
   }
   
   public byte getDrDebit()
   {
	  return this.DR_COA_DBT;
   }

   public void setDrDebit( byte DR_COA_DBT )
   {
	  this.DR_COA_DBT = DR_COA_DBT;

   }
   
   public double getDrAmount()
   {
	  return this.DR_AMNT;
   }

   public void setDrAmount( double DR_AMNT )
   {
	  this.DR_AMNT = DR_AMNT;


   }
   
   public java.lang.String getDrClass()
   {
	  return this.DR_CLSS;
   }

   public void setDrClass( java.lang.String DR_CLSS )
   {
	  this.DR_CLSS = DR_CLSS;

   }

} // ApModDistributionRecordDetails class
