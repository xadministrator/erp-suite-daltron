package com.util;


public class ApModSupplierTypeDetails extends ApSupplierTypeDetails implements java.io.Serializable {

    private String ST_BA_NM;

    public ApModSupplierTypeDetails() { ; }

    public String getStBaName() {
    	
    	return ST_BA_NM;
    	
    }
    
    public void setStBaName(String ST_BA_NM) {
    	
    	this.ST_BA_NM = ST_BA_NM;
    	
    }        
    	    	      
} // ApModSupplierTypeDetails class
