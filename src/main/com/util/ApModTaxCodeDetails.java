package com.util;


public class ApModTaxCodeDetails extends ApTaxCodeDetails implements java.io.Serializable {

    private String TC_COA_ACCNT_NMBR;
    private String TC_COA_ACCNT_DESC;

    public ApModTaxCodeDetails() { ; }

    public String getTcCoaGlTaxAccountNumber() {
   	
   	    return TC_COA_ACCNT_NMBR;
   	
    }
   
    public void setTcCoaGlTaxAccountNumber(String TC_COA_ACCNT_NMBR) {
   	
   	    this.TC_COA_ACCNT_NMBR = TC_COA_ACCNT_NMBR;
   	
    }
   
    public String getTcCoaGlTaxDescription() {
   	
   	    return TC_COA_ACCNT_DESC;
   	 
    }
    
    public void setTcCoaGlTaxDescription(String TC_COA_ACCNT_DESC) {
    	
    	this.TC_COA_ACCNT_DESC = TC_COA_ACCNT_DESC;
    	
    }
    	    	      
} // ApModTaxCodeDetails class
