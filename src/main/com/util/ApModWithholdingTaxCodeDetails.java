package com.util;


public class ApModWithholdingTaxCodeDetails extends ApWithholdingTaxCodeDetails implements java.io.Serializable {

    private String WTC_COA_ACCNT_NMBR;
    private String WTC_COA_ACCNT_DESC;

    public ApModWithholdingTaxCodeDetails() { ; }

    public String getWtcCoaGlWithholdingTaxAccountNumber() {
   	
   	    return WTC_COA_ACCNT_NMBR;
   	
    }
   
    public void setWtcCoaGlWithholdingTaxAccountNumber(String WTC_COA_ACCNT_NMBR) {
   	
   	    this.WTC_COA_ACCNT_NMBR = WTC_COA_ACCNT_NMBR;
   	
    }
   
    public String getWtcCoaGlWithholdingTaxAccountDescription() {
   	
   	    return WTC_COA_ACCNT_DESC;
   	 
    }
    
    public void setWtcCoaGlWithholdingTaxAccountDescription(String WTC_COA_ACCNT_DESC) {
    	
    	this.WTC_COA_ACCNT_DESC = WTC_COA_ACCNT_DESC;
    	
    }
    	    	      
} // ApModWithholdingTaxCodeDetails class
