package com.util;


public class ArModCustomerTypeDetails extends ArCustomerTypeDetails implements java.io.Serializable {

    private String CT_BA_NM;

    public ArModCustomerTypeDetails() { ; }

    public String getCtBaName() {
    	
    	return CT_BA_NM;
    	
    }
    
    public void setCtBaName(String CT_BA_NM) {
    	
    	this.CT_BA_NM = CT_BA_NM;
    	
    }        
    	    	      
} // ArModCustomerTypeDetails class
