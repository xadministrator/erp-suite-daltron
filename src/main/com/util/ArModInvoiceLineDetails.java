package com.util;


public class ArModInvoiceLineDetails extends ArInvoiceLineDetails implements java.io.Serializable {

   private String IL_SML_NM;
   private String IL_RCPT_NMBR;
   
   public ArModInvoiceLineDetails () { ; }

   public String getIlSmlName() {
   	
   	  return IL_SML_NM;
   	
   }
   
   public void setIlSmlName(String IL_SML_NM) {
   	
   	  this.IL_SML_NM = IL_SML_NM;
   	
   }
   
   public String getIlReceiptNumber() {
   	
   	  return IL_RCPT_NMBR;
   	
   }
   
   public void setIlReceiptNumber(String IL_RCPT_NMBR) {
   	
   	  this.IL_RCPT_NMBR = IL_RCPT_NMBR;
   	
   }

} // ArModInvoiceLineDetails class   
