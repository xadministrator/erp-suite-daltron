package com.util;

import java.util.ArrayList;
import java.util.Date;


public class ArModPersonelDetails extends ArPersonelDetails implements java.io.Serializable ,  Cloneable{
	
	/*private Integer HR_EMPLOYEE;
	private Integer HR_DEPLOYED_BRANCH;
	*/   
	
    private String PE_PT_SHRT_NM;
    private String PE_PT_NM;
    
    
    

    
    public ArModPersonelDetails () { ; }
    
  
    public Object clone() throws CloneNotSupportedException {
     	 

 		try {
 		   return (ArModPersonelDetails)super.clone();
 		 }
 		  catch (CloneNotSupportedException e) {
 			  throw e;
 		
 		  }	
 	 }
    
    
    public String getPePtShortName() {
    	return PE_PT_SHRT_NM;
    }
    
    public void setPePtShortName(String PE_PT_SHRT_NM) {
    	this.PE_PT_SHRT_NM = PE_PT_SHRT_NM;
    }
    
    
    public String getPePtName() {
    	return PE_PT_NM;
    }
    
    public void setPePtName(String PE_PT_NM) {
    	this.PE_PT_NM = PE_PT_NM;
    }

    
} // ArModCustomerDetails class   
