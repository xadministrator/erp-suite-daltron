package com.util;

import java.util.ArrayList;

public class ArModReceiptImportPreferenceDetails extends ArReceiptImportPreferenceDetails implements java.io.Serializable {
	
	private ArrayList ripRilList = new ArrayList();
	
	public ArModReceiptImportPreferenceDetails() { ; }
	
	public ArReceiptImportPreferenceLineDetails getRipRilListByIndex(int index){
		
		return ((ArReceiptImportPreferenceLineDetails)ripRilList.get(index));
		
	}
	
	public int getRipRilListSize(){
		
		return(ripRilList.size());
		
	}
	
	public void saveRipRilList(Object newRipRilList){
		
		ripRilList.add(newRipRilList);   	  
		
	}
	
	public ArrayList getRipRilList() {
		
		return ripRilList;
		
	}
	
	public void setRipRilList(ArrayList ripRilList) {
		
		this.ripRilList = ripRilList;
		
	}
	
}