package com.util;


public class ArModStandardMemoLineDetails extends ArStandardMemoLineDetails implements java.io.Serializable {

    private String SML_GL_COA_ACCNT_NMBR;
    private String SML_GL_COA_ACCNT_DESC;
    
    private String SML_GL_COA_RCVBL_ACCNT_NMBR;
    private String SML_GL_COA_RCVBL_ACCNT_DESC;
    private String SML_GL_COA_RVN_ACCNT_NMBR;
    private String SML_GL_COA_RVN_ACCNT_DESC;
    
    private String SML_INTRM_ACCNT_NMBR;
    private String SML_INTRM_ACCNT_DESC;

    public ArModStandardMemoLineDetails() { ; }
        
    public String getSmlGlCoaAccountNumber() {
   	
   	    return SML_GL_COA_ACCNT_NMBR;
   	
    }
   
    public void setSmlGlCoaAccountNumber(String SML_GL_COA_ACCNT_NMBR) {
   	
   	    this.SML_GL_COA_ACCNT_NMBR = SML_GL_COA_ACCNT_NMBR;
   	
    }
   
    public String getSmlGlCoaAccountDescription() {
   	
   	    return SML_GL_COA_ACCNT_DESC;
   	 
    }
    
    public void setSmlGlCoaAccountDescription(String SML_GL_COA_ACCNT_DESC) {
    	
    	this.SML_GL_COA_ACCNT_DESC = SML_GL_COA_ACCNT_DESC;
    	
    } 
    
    
    
    public String getSmlGlCoaReceivableAccountNumber() {
       	
   	    return SML_GL_COA_RCVBL_ACCNT_NMBR;
   	
    }
   
    public void setSmlGlCoaReceivableAccountNumber(String SML_GL_COA_RCVBL_ACCNT_NMBR) {
   	
   	    this.SML_GL_COA_RCVBL_ACCNT_NMBR = SML_GL_COA_RCVBL_ACCNT_NMBR;
   	
    }
   
    public String getSmlGlCoaReceivableAccountDescription() {
   	
   	    return SML_GL_COA_RCVBL_ACCNT_DESC;
   	 
    }
    
    public void setSmlGlCoaReceivableAccountDescription(String SML_GL_COA_RCVBL_ACCNT_DESC) {
    	
    	this.SML_GL_COA_RCVBL_ACCNT_DESC = SML_GL_COA_RCVBL_ACCNT_DESC;
    	
    }
    
    
    
    
    public String getSmlGlCoaRevenueAccountNumber() {
       	
   	    return SML_GL_COA_RVN_ACCNT_NMBR;
   	
    }
   
    public void setSmlGlCoaRevenueAccountNumber(String SML_GL_COA_RVN_ACCNT_NMBR) {
   	
   	    this.SML_GL_COA_RVN_ACCNT_NMBR = SML_GL_COA_RVN_ACCNT_NMBR;
   	
    }
   
    public String getSmlGlCoaRevenueAccountDescription() {
   	
   	    return SML_GL_COA_RVN_ACCNT_DESC;
   	 
    }
    
    public void setSmlGlCoaRevenueAccountDescription(String SML_GL_COA_RVN_ACCNT_DESC) {
    	
    	this.SML_GL_COA_RVN_ACCNT_DESC = SML_GL_COA_RVN_ACCNT_DESC;
    	
    }
    
    
    public String getSmlInterimAccountNumber() {
    	
    	return SML_INTRM_ACCNT_NMBR;
    	
    }
    
    public void setSmlInterimAccountNumber(String SML_INTRM_ACCNT_NMBR) {
    	
    	
    	this.SML_INTRM_ACCNT_NMBR = SML_INTRM_ACCNT_NMBR;
    }
    
    public String getSmlInterimAccountDescription() {
    	
    	return SML_INTRM_ACCNT_DESC;
    	
    }
    
    public void setSmlInterimAccountDescription(String SML_INTRM_ACCNT_DESC) {
    	    	
    	this.SML_INTRM_ACCNT_DESC = SML_INTRM_ACCNT_DESC;
    	
    }
    	    	    	      
} // ArModStandardMemoLineDetails class
