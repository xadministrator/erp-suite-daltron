package com.util;


public class ArRepCustomerTypeListDetails implements java.io.Serializable {

   private String CTL_CT_NM; 
   private String CTL_CT_DESC; 
   private String CTL_BNK_ACCNT; 
   private byte CTL_ENBL;

   public ArRepCustomerTypeListDetails() { ; }

   public byte getCtlEnable() {
   	
   	 return CTL_ENBL;
   
   }
   
   public void setCtlEnable(byte CTL_ENBL) {
   
   	 this.CTL_ENBL = CTL_ENBL;
   
   }

   public String getCtlCtDescription() {
   
   	 return CTL_CT_DESC;
   
   }
   
   public void setCtlCtDescription(String CTL_CT_DESC) {
   
   	 this.CTL_CT_DESC = CTL_CT_DESC;
   
   }
   
   public String getCtlCtName() {
   
   	 return CTL_CT_NM;
   
   }
   
   public void setCtlCtName(String CTL_CT_NM) {
   
   	 this.CTL_CT_NM = CTL_CT_NM;
   
   }
   
   public String getCtlBankAccount() {
   
   	 return CTL_BNK_ACCNT;
   
   }
   
   public void setCtlBankAccount(String CTL_BNK_ACCNT) {
   
   	 this.CTL_BNK_ACCNT = CTL_BNK_ACCNT;
   
   }
   
} // ArRepCustomerTypeListDetails class
