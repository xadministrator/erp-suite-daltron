package com.util;

import java.util.Comparator;
import java.util.Date;

public class ArRepStatementDetails implements Cloneable, java.io.Serializable {

	//PMA
	private String SMT_PM_PRJCT_CODE;
	private String SMT_PM_PRJCT_TYP;
	private String SMT_PM_PRJCT_NM;
	private double SMT_PM_CNTRCT_PRC;
	private String SMT_PM_CNTRCT_TRM_NM;
	private double SMT_PM_CNTRCT_TRM_VL;

   private String SMT_CSTMR_CODE;
   private String SMT_CSTMR_BTCH;
   private String SMT_CSTMR_DPRTMNT;
   private Date SMT_CSTMR_ENTRY_DT;
   private Date SMT_DT;
   private Date SMT_DUE_DT;
   private Date SMT_LST_DUE_DT;

   private String SMT_TRNSCTN;
   private byte SMT_PSTD;
   private String SMT_TRNSCTN_TYP;
   private String SMT_OR_NUM;
   private String SMT_INVC_NMBR;
   private Date SMT_LST_OR_DT;
   private double SMT_INVC_AMNT_DUE;
   private double SMT_INVC_DWN_PYMNT;
   private double SMT_INVC_UNERND_INT;
   private double SMT_INVC_PNT_DUE;
   private double SMT_AMNT;
   private double SMT_NMBR;
   private double SMT_PNT;
   private double SMT_AMNT_DUE;
   private double SMT_IPS_AMNT_DUE;
   private double SMT_AMNT_PD;
   private double SMT_INT_DUE;
   private double SMT_AMNT_DTLS;
   private double SMT_DBT;
   private double SMT_CRDT;
   private double SMT_CRDT_AMNT;
   private double SMT_RMNNG_BLNC;
   private String SMT_BTCH_NM;
   private String SMT_DESC;
   private String SMT_ITM_DESC;
   private double SMT_TERM;
   private String SMT_TERM_NM;

   private double SMT_PRV_RDNG;
   private double SMT_PRSNT_RDNG;
   private double SMT_QTY;
   private double SMT_UNT_PRC;

   private double SMT_TX_AMNT;
   private double SMT_WTX_AMNT;
   private String SMT_TX_CD;
   private String SMT_CRRNCY;

   private String SMT_CSTMR_NM;
   private String SMT_CSTMR_ADDRSS;
   private String SMT_CST_CNTCT;
   private String SMT_CST_TIN;
   private double SMT_CST_SQ_MTR;
   private String SMT_PRKNG_ID;
   private String SMT_CST_EML;
   private String SMT_CST_WP_CSTMR_ID;
   private String SMT_CST_EMP_ID;
   private String SMT_CST_ACCNT_NO;
   private String SMT_CST_FRST_NM;
   private String SMT_CST_LST_NM;


   private String SMT_CST_BLLNG_FTR;
   private String SMT_CST_BLLNG_HDR;
   private String SMT_CST_BLLNG_SGNTRY;
   private String SMT_CST_SGNTRY_TTL;
   private String SMT_RCPT_NMBR;
   private String SMT_SML_WP_PRDCT_ID;
   private Date SMT_RCPT_DT;
   private double SMT_RCPT_APPLY_AMNT;
   private String SMT_RFRNC_NMBR;
   private String SMT_CRDT_MMO_NMBR;
   private Date SMT_CRDT_MMO_DT;
   private double SMT_CRDT_MMO_AMNT;
   private String SMT_INVC_BTCH_NM;

   private String SMT_NO_DYS;
   private double SMT_EXC_ADV;
   private double SMT_INC_ADV;


   private double SMT_BCKT0;
   private double SMT_BCKT1;
   private double SMT_BCKT2;
   private double SMT_BCKT3;
   private double SMT_BCKT4;
   private double SMT_BCKT5;

   private double SMT_BCKT_ASD0;
   private double SMT_BCKT_ASD1;
   private double SMT_BCKT_ASD2;
   private double SMT_BCKT_ASD3;
   private double SMT_BCKT_ASD4;
   private double SMT_BCKT_ASD5;

   private double SMT_BCKT_RPT0;
   private double SMT_BCKT_RPT1;
   private double SMT_BCKT_RPT2;
   private double SMT_BCKT_RPT3;
   private double SMT_BCKT_RPT4;
   private double SMT_BCKT_RPT5;

   private double SMT_BCKT_PD0;
   private double SMT_BCKT_PD1;
   private double SMT_BCKT_PD2;
   private double SMT_BCKT_PD3;
   private double SMT_BCKT_PD4;
   private double SMT_BCKT_PD5;

   private double SMT_BCKT_WTR0;
   private double SMT_BCKT_WTR1;
   private double SMT_BCKT_WTR2;
   private double SMT_BCKT_WTR3;
   private double SMT_BCKT_WTR4;
   private double SMT_BCKT_WTR5;

   private double SMT_BCKT_MISC0;
   private double SMT_BCKT_MISC1;
   private double SMT_BCKT_MISC2;
   private double SMT_BCKT_MISC3;
   private double SMT_BCKT_MISC4;
   private double SMT_BCKT_MISC5;

   private double SMT_BCKT_ADV0;
   private double SMT_BCKT_ADV1;
   private double SMT_BCKT_ADV2;
   private double SMT_BCKT_ADV3;
   private double SMT_BCKT_ADV4;
   private double SMT_BCKT_ADV5;

   private String ORDER_BY;

   public ArRepStatementDetails() { ; }


   
   public Object clone() throws CloneNotSupportedException {
  	 

 		try {
 		   return (ArRepStatementDetails)super.clone();
 		 }
 		  catch (CloneNotSupportedException e) {
 			  throw e;
 		
 		  }	
 	 }


   // PMA
   public String getSmtPmProjectCode(){
	   return SMT_PM_PRJCT_CODE;
   }

   public void setSmtPmProjectCode(String SMT_PM_PRJCT_CODE){
	   this.SMT_PM_PRJCT_CODE =  SMT_PM_PRJCT_CODE;
   }



   public String getSmtPmProjectType(){
	   return SMT_PM_PRJCT_TYP;
   }

   public void setSmtPmProjectType(String SMT_PM_PRJCT_TYP){
	   this.SMT_PM_PRJCT_TYP =  SMT_PM_PRJCT_TYP;
   }


	public String getSmtPmProjectName(){
	   return SMT_PM_PRJCT_NM;
   }

   public void setSmtPmProjectName(String SMT_PM_PRJCT_NM){
	   this.SMT_PM_PRJCT_NM =  SMT_PM_PRJCT_NM;
   }

	public double getSmtPmContractPrice(){
	   return SMT_PM_CNTRCT_PRC;
   }

   public void setSmtPmContractPrice(double SMT_PM_CNTRCT_PRC){
	   this.SMT_PM_CNTRCT_PRC =  SMT_PM_CNTRCT_PRC;
   }


	public String getSmtPmContractTermName(){
	   return SMT_PM_CNTRCT_TRM_NM;
   }

   public void setSmtPmContractTermName(String SMT_PM_CNTRCT_TRM_NM){
	   this.SMT_PM_CNTRCT_TRM_NM =  SMT_PM_CNTRCT_TRM_NM;
   }

	public double getSmtPmContractValue(){
	   return SMT_PM_CNTRCT_TRM_VL;
   }

   public void setSmtPmContractValue(double SMT_PM_CNTRCT_TRM_VL){
	   this.SMT_PM_CNTRCT_TRM_VL =  SMT_PM_CNTRCT_TRM_VL;
   }

















   public String getSmtInvoiceBatchName(){
	   return SMT_INVC_BTCH_NM;
   }

   public void setSmtInvoiceBatchName(String SMT_INVC_BTCH_NM){
	   this.SMT_INVC_BTCH_NM =  SMT_INVC_BTCH_NM;
   }

   public double getSmtBucket0() {

	   	  return SMT_BCKT0;

	   }

	   public void setSmtBucket0(double SMT_BCKT0) {

	   	  this.SMT_BCKT0 = SMT_BCKT0;

	   }

	   public double getSmtBucket1() {

	   	  return SMT_BCKT1;

	   }

	   public void setSmtBucket1(double SMT_BCKT1) {

	   	  this.SMT_BCKT1 = SMT_BCKT1;

	   }

	   public double getSmtBucket2() {

	   	  return SMT_BCKT2;

	   }

	   public void setSmtBucket2(double SMT_BCKT2) {

	   	  this.SMT_BCKT2 = SMT_BCKT2;

	   }

	   public double getSmtBucket3() {

	   	  return SMT_BCKT3;

	   }

	   public void setSmtBucket3(double SMT_BCKT3) {

	   	  this.SMT_BCKT3 = SMT_BCKT3;

	   }


	   public double getSmtBucket4() {

	   	  return SMT_BCKT4;

	   }

	   public void setSmtBucket4(double SMT_BCKT4) {

	   	  this.SMT_BCKT4 = SMT_BCKT4;

	   }


	   public double getSmtBucket5() {

	   	  return SMT_BCKT5;

	   }

	   public void setSmtBucket5(double SMT_BCKT5) {

	   	  this.SMT_BCKT5 = SMT_BCKT5;

	   }



	   public double getSmtBucketASD0() {

		   	  return SMT_BCKT_ASD0;

		   }

		   public void setSmtBucketASD0(double SMT_BCKT_ASD0) {

		   	  this.SMT_BCKT_ASD0 = SMT_BCKT_ASD0;

		   }

		   public double getSmtBucketASD1() {

		   	  return SMT_BCKT_ASD1;

		   }

		   public void setSmtBucketASD1(double SMT_BCKT_ASD1) {

		   	  this.SMT_BCKT_ASD1 = SMT_BCKT_ASD1;

		   }

		   public double getSmtBucketASD2() {

		   	  return SMT_BCKT_ASD2;

		   }

		   public void setSmtBucketASD2(double SMT_BCKT_ASD2) {

		   	  this.SMT_BCKT_ASD2 = SMT_BCKT_ASD2;

		   }

		   public double getSmtBucketASD3() {

		   	  return SMT_BCKT_ASD3;

		   }

		   public void setSmtBucketASD3(double SMT_BCKT_ASD3) {

		   	  this.SMT_BCKT_ASD3 = SMT_BCKT_ASD3;

		   }


		   public double getSmtBucketASD4() {

		   	  return SMT_BCKT_ASD4;

		   }

		   public void setSmtBucketASD4(double SMT_BCKT_ASD4) {

		   	  this.SMT_BCKT_ASD4 = SMT_BCKT_ASD4;

		   }


		   public double getSmtBucketASD5() {

		   	  return SMT_BCKT_ASD5;

		   }

		   public void setSmtBucketASD5(double SMT_BCKT_ASD5) {

		   	  this.SMT_BCKT_ASD5 = SMT_BCKT_ASD5;

		   }





		   public double getSmtBucketRPT0() {

			   	  return SMT_BCKT_RPT0;

			   }

			   public void setSmtBucketRPT0(double SMT_BCKT_RPT0) {

			   	  this.SMT_BCKT_RPT0 = SMT_BCKT_RPT0;

			   }

			   public double getSmtBucketRPT1() {

			   	  return SMT_BCKT_RPT1;

			   }

			   public void setSmtBucketRPT1(double SMT_BCKT_RPT1) {

			   	  this.SMT_BCKT_RPT1 = SMT_BCKT_RPT1;

			   }

			   public double getSmtBucketRPT2() {

			   	  return SMT_BCKT_RPT2;

			   }

			   public void setSmtBucketRPT2(double SMT_BCKT_RPT2) {

			   	  this.SMT_BCKT_RPT2 = SMT_BCKT_RPT2;

			   }

			   public double getSmtBucketRPT3() {

			   	  return SMT_BCKT_RPT3;

			   }

			   public void setSmtBucketRPT3(double SMT_BCKT_RPT3) {

			   	  this.SMT_BCKT_RPT3 = SMT_BCKT_RPT3;

			   }


			   public double getSmtBucketRPT4() {

			   	  return SMT_BCKT_RPT4;

			   }

			   public void setSmtBucketRPT4(double SMT_BCKT_RPT4) {

			   	  this.SMT_BCKT_RPT4 = SMT_BCKT_RPT4;

			   }


			   public double getSmtBucketRPT5() {

			   	  return SMT_BCKT_RPT5;

			   }

			   public void setSmtBucketRPT5(double SMT_BCKT_RPT5) {

			   	  this.SMT_BCKT_RPT5 = SMT_BCKT_RPT5;

			   }





			   public double getSmtBucketPD0() {

				   	  return SMT_BCKT_PD0;

				   }

				   public void setSmtBucketPD0(double SMT_BCKT_PD0) {

				   	  this.SMT_BCKT_PD0 = SMT_BCKT_PD0;

				   }

				   public double getSmtBucketPD1() {

				   	  return SMT_BCKT_PD1;

				   }

				   public void setSmtBucketPD1(double SMT_BCKT_PD1) {

				   	  this.SMT_BCKT_PD1 = SMT_BCKT_PD1;

				   }

				   public double getSmtBucketPD2() {

				   	  return SMT_BCKT_PD2;

				   }

				   public void setSmtBucketPD2(double SMT_BCKT_PD2) {

				   	  this.SMT_BCKT_PD2 = SMT_BCKT_PD2;

				   }

				   public double getSmtBucketPD3() {

				   	  return SMT_BCKT_PD3;

				   }

				   public void setSmtBucketPD3(double SMT_BCKT_PD3) {

				   	  this.SMT_BCKT_PD3 = SMT_BCKT_PD3;

				   }


				   public double getSmtBucketPD4() {

				   	  return SMT_BCKT_PD4;

				   }

				   public void setSmtBucketPD4(double SMT_BCKT_PD4) {

				   	  this.SMT_BCKT_PD4 = SMT_BCKT_PD4;

				   }


				   public double getSmtBucketPD5() {

				   	  return SMT_BCKT_PD5;

				   }

				   public void setSmtBucketPD5(double SMT_BCKT_PD5) {

				   	  this.SMT_BCKT_PD5 = SMT_BCKT_PD5;

				   }



				   public double getSmtBucketWTR0() {

					   	  return SMT_BCKT_WTR0;

					   }

					   public void setSmtBucketWTR0(double SMT_BCKT_WTR0) {

					   	  this.SMT_BCKT_WTR0 = SMT_BCKT_WTR0;

					   }

					   public double getSmtBucketWTR1() {

					   	  return SMT_BCKT_WTR1;

					   }

					   public void setSmtBucketWTR1(double SMT_BCKT_WTR1) {

					   	  this.SMT_BCKT_WTR1 = SMT_BCKT_WTR1;

					   }

					   public double getSmtBucketWTR2() {

					   	  return SMT_BCKT_WTR2;

					   }

					   public void setSmtBucketWTR2(double SMT_BCKT_WTR2) {

					   	  this.SMT_BCKT_WTR2 = SMT_BCKT_WTR2;

					   }

					   public double getSmtBucketWTR3() {

					   	  return SMT_BCKT_WTR3;

					   }

					   public void setSmtBucketWTR3(double SMT_BCKT_WTR3) {

					   	  this.SMT_BCKT_WTR3 = SMT_BCKT_WTR3;

					   }


					   public double getSmtBucketWTR4() {

					   	  return SMT_BCKT_WTR4;

					   }

					   public void setSmtBucketWTR4(double SMT_BCKT_WTR4) {

					   	  this.SMT_BCKT_WTR4 = SMT_BCKT_WTR4;

					   }


					   public double getSmtBucketWTR5() {

					   	  return SMT_BCKT_WTR5;

					   }

					   public void setSmtBucketWTR5(double SMT_BCKT_WTR5) {

					   	  this.SMT_BCKT_WTR5 = SMT_BCKT_WTR5;

					   }



					   public double getSmtBucketMISC0() {

						   	  return SMT_BCKT_MISC0;

						   }

						   public void setSmtBucketMISC0(double SMT_BCKT_MISC0) {

						   	  this.SMT_BCKT_MISC0 = SMT_BCKT_MISC0;

						   }

						   public double getSmtBucketMISC1() {

						   	  return SMT_BCKT_MISC1;

						   }

						   public void setSmtBucketMISC1(double SMT_BCKT_MISC1) {

						   	  this.SMT_BCKT_MISC1 = SMT_BCKT_MISC1;

						   }

						   public double getSmtBucketMISC2() {

						   	  return SMT_BCKT_MISC2;

						   }

						   public void setSmtBucketMISC2(double SMT_BCKT_MISC2) {

						   	  this.SMT_BCKT_MISC2 = SMT_BCKT_MISC2;

						   }

						   public double getSmtBucketMISC3() {

						   	  return SMT_BCKT_MISC3;

						   }

						   public void setSmtBucketMISC3(double SMT_BCKT_MISC3) {

						   	  this.SMT_BCKT_MISC3 = SMT_BCKT_MISC3;

						   }


						   public double getSmtBucketMISC4() {

						   	  return SMT_BCKT_MISC4;

						   }

						   public void setSmtBucketMISC4(double SMT_BCKT_MISC4) {

						   	  this.SMT_BCKT_MISC4 = SMT_BCKT_MISC4;

						   }


						   public double getSmtBucketMISC5() {

						   	  return SMT_BCKT_MISC5;

						   }

						   public void setSmtBucketMISC5(double SMT_BCKT_MISC5) {

						   	  this.SMT_BCKT_MISC5 = SMT_BCKT_MISC5;

						   }


						   public double getSmtBucketADV0() {

							   	  return SMT_BCKT_ADV0;

							   }

							   public void setSmtBucketADV0(double SMT_BCKT_ADV0) {

							   	  this.SMT_BCKT_ADV0 = SMT_BCKT_ADV0;

							   }

							   public double getSmtBucketADV1() {

							   	  return SMT_BCKT_ADV1;

							   }

							   public void setSmtBucketADV1(double SMT_BCKT_ADV1) {

							   	  this.SMT_BCKT_ADV1 = SMT_BCKT_ADV1;

							   }

							   public double getSmtBucketADV2() {

							   	  return SMT_BCKT_ADV2;

							   }

							   public void setSmtBucketADV2(double SMT_BCKT_ADV2) {

							   	  this.SMT_BCKT_ADV2 = SMT_BCKT_ADV2;

							   }

							   public double getSmtBucketADV3() {

							   	  return SMT_BCKT_ADV3;

							   }

							   public void setSmtBucketADV3(double SMT_BCKT_ADV3) {

							   	  this.SMT_BCKT_ADV3 = SMT_BCKT_ADV3;

							   }


							   public double getSmtBucketADV4() {

							   	  return SMT_BCKT_ADV4;

							   }

							   public void setSmtBucketADV4(double SMT_BCKT_ADV4) {

							   	  this.SMT_BCKT_ADV4 = SMT_BCKT_ADV4;

							   }


							   public double getSmtBucketADV5() {

							   	  return SMT_BCKT_ADV5;

							   }

							   public void setSmtBucketADV5(double SMT_BCKT_ADV5) {

							   	  this.SMT_BCKT_ADV5 = SMT_BCKT_ADV5;

							   }
   public String getSmtCustomerCode() {

   	   return SMT_CSTMR_CODE;

   }

   public void setSmtCustomerCode(String SMT_CSTMR_CODE) {

   	   this.SMT_CSTMR_CODE = SMT_CSTMR_CODE;

   }



   public String getSmtCustomerBatch() {

   	   return SMT_CSTMR_BTCH;

   }

   public void setSmtCustomerBatch(String SMT_CSTMR_BTCH) {

   	   this.SMT_CSTMR_BTCH = SMT_CSTMR_BTCH;

   }

   public String getSmtCustomerDepartment() {

   	   return SMT_CSTMR_DPRTMNT;

   }

   public void setSmtCustomerDepartment(String SMT_CSTMR_DPRTMNT) {

   	   this.SMT_CSTMR_DPRTMNT = SMT_CSTMR_DPRTMNT;

   }

   public Date getSmtCustomerEntryDate() {

   	   return SMT_CSTMR_ENTRY_DT;

   }

   public void setSmtCustomerEntryDate(Date SMT_CSTMR_ENTRY_DT) {

   	   this.SMT_CSTMR_ENTRY_DT = SMT_CSTMR_ENTRY_DT;

   }



   public Date getSmtDate() {

   	   return SMT_DT;

   }

   public void setSmtDate(Date SMT_DT) {

   	   this.SMT_DT = SMT_DT;

   }

   public Date getSmtDueDate() {

   	   return SMT_DUE_DT;

   }

   public void setSmtDueDate(Date SMT_DUE_DT) {

   	   this.SMT_DUE_DT = SMT_DUE_DT;

   }

   public Date getSmtLastDueDate() {

   	   return SMT_LST_DUE_DT;

   }

   public void setSmtLastDueDate(Date SMT_LST_DUE_DT) {

   	   this.SMT_LST_DUE_DT = SMT_LST_DUE_DT;

   }

   public String getSmtTransaction() {

   	   return SMT_TRNSCTN;

   }

   public void setSmtTransaction(String SMT_TRNSCTN) {

   	   this.SMT_TRNSCTN = SMT_TRNSCTN;

   }



   public byte getSmtPosted() {

   	   return SMT_PSTD;

   }

   public void setSmtPosted(byte SMT_PSTD) {

   	   this.SMT_PSTD = SMT_PSTD;

   }

   public String getSmtTransactionType() {

   	   return SMT_TRNSCTN_TYP;

   }

   public void setSmtTransactionType(String SMT_TRNSCTN_TYP) {

   	   this.SMT_TRNSCTN_TYP = SMT_TRNSCTN_TYP;

   }

   public String getSmtOrNumber() {

   	   return SMT_OR_NUM;

   }

   public void setSmtOrNumber(String SMT_OR_NUM) {

   	   this.SMT_OR_NUM = SMT_OR_NUM;

   }

   public String getSmtInvoiceNumber() {

   	   return SMT_INVC_NMBR;

   }

   public void setSmtInvoiceNumber(String SMT_INVC_NMBR) {

   	   this.SMT_INVC_NMBR = SMT_INVC_NMBR;

   }

   public Date getSmtLastOrDate() {

   	   return SMT_LST_OR_DT;

   }

   public void setSmtLastOrDate(Date SMT_LST_OR_DT) {

   	   this.SMT_LST_OR_DT = SMT_LST_OR_DT;

   }



   public double getSmtInvoiceAmountDue() {

   	   return SMT_INVC_AMNT_DUE;

   }

   public void setSmtInvoiceAmountDue(double SMT_INVC_AMNT_DUE) {

   	   this.SMT_INVC_AMNT_DUE = SMT_INVC_AMNT_DUE;

   }


   public double getSmtInvoiceDownPayment() {

   	   return SMT_INVC_DWN_PYMNT;

   }

   public void setSmtInvoiceDownPayment(double SMT_INVC_DWN_PYMNT) {

   	   this.SMT_INVC_DWN_PYMNT = SMT_INVC_DWN_PYMNT;

   }

   public double getSmtInvoiceUnearnedInterest() {

   	   return SMT_INVC_UNERND_INT;

   }

   public void setSmtInvoiceUnearnedInterest(double SMT_INVC_UNERND_INT) {

   	   this.SMT_INVC_UNERND_INT = SMT_INVC_UNERND_INT;

   }

   public double getSmtInvoicePenaltyDue() {

   	   return SMT_INVC_PNT_DUE;

   }

   public void setSmtInvoicePenaltyDue(double SMT_INVC_PNT_DUE) {

   	   this.SMT_INVC_PNT_DUE = SMT_INVC_PNT_DUE;

   }

   public double getSmtNumber() {

   	   return SMT_NMBR;

   }

   public void setSmtNumber(double SMT_NMBR) {

   	   this.SMT_NMBR = SMT_NMBR;

   }


   public double getSmtAmount() {

   	   return SMT_AMNT;

   }

   public void setSmtAmount(double SMT_AMNT) {

   	   this.SMT_AMNT = SMT_AMNT;

   }
   public double getSmtPenalty() {

   	   return SMT_PNT;

   }

   public void setSmtPenalty(double SMT_PNT) {

   	   this.SMT_PNT = SMT_PNT;

   }

   public double getSmtAmountDue() {

   	   return SMT_AMNT_DUE;

   }

   public void setSmtAmountDue(double SMT_AMNT_DUE) {

   	   this.SMT_AMNT_DUE = SMT_AMNT_DUE;

   }

   public double getSmtIpsAmountDue() {

   	   return SMT_IPS_AMNT_DUE;

   }

   public void setSmtIpsAmountDue(double SMT_IPS_AMNT_DUE) {

   	   this.SMT_IPS_AMNT_DUE = SMT_IPS_AMNT_DUE;

   }

   public double getSmtAmountPaid() {

   	   return SMT_AMNT_PD;

   }

   public void setSmtAmountPaid(double SMT_AMNT_PD) {

   	   this.SMT_AMNT_PD = SMT_AMNT_PD;

   }

   public double getSmtInterestDue() {

   	   return SMT_INT_DUE;

   }

   public void setSmtInterestDue(double SMT_INT_DUE) {

   	   this.SMT_INT_DUE = SMT_INT_DUE;

   }

   public double getSmtAmountDetails() {

   	   return SMT_AMNT_DTLS;

   }

   public void setSmtAmountDetails(double SMT_AMNT_DTLS) {

   	   this.SMT_AMNT_DTLS = SMT_AMNT_DTLS;

   }

   public double getSmtDebit() {

   	   return SMT_DBT;

   }

   public void setSmtDebit(double SMT_DBT) {

   	   this.SMT_DBT = SMT_DBT;

   }

   public double getSmtCredit() {

   	   return SMT_CRDT;

   }

   public void setSmtCredit(double SMT_CRDT) {

   	   this.SMT_CRDT = SMT_CRDT;

   }



   public double getSmtCreditAmount() {

   	   return SMT_CRDT_AMNT;

   }

   public void setSmtCreditAmount(double SMT_CRDT_AMNT) {

   	   this.SMT_CRDT_AMNT = SMT_CRDT_AMNT;

   }

   public double getSmtRemainingBalance() {

   	   return SMT_RMNNG_BLNC;

   }

   public void setSmtRemainingBalance(double SMT_RMNNG_BLNC) {

   	   this.SMT_RMNNG_BLNC = SMT_RMNNG_BLNC;

   }

   public String getSmtBatchName() {

   	   return SMT_BTCH_NM;

   }

   public void setSmtBatchName(String SMT_BTCH_NM) {

   	   this.SMT_BTCH_NM = SMT_BTCH_NM;

   }

   public String getSmtDescription() {

   	   return SMT_DESC;

   }

   public void setSmtDescription(String SMT_DESC) {

   	   this.SMT_DESC = SMT_DESC;

   }


   public String getSmtItemDescription() {

   	   return SMT_ITM_DESC;

   }

   public void setSmtItemDescription(String SMT_ITM_DESC) {

   	   this.SMT_ITM_DESC = SMT_ITM_DESC;

   }


   public double getSmtPaymentTerm() {

   	   return SMT_TERM;

   }

   public void  setSmtPaymentTerm(double SMT_TERM) {

   	   this.SMT_TERM = SMT_TERM;

   }

   public String getSmtPaymentTermName() {

   	   return SMT_TERM_NM;

   }

   public void  setSmtPaymentTermName(String SMT_TERM_NM) {

   	   this.SMT_TERM_NM = SMT_TERM_NM;

   }


   public double getSmtPreviousReading() {

   	   return SMT_PRV_RDNG;

   }

   public void setSmtPreviousReading(double SMT_PRV_RDNG) {

   	   this.SMT_PRV_RDNG = SMT_PRV_RDNG;

   }

   public double getSmtPresentReading() {

   	   return SMT_PRSNT_RDNG;

   }

   public void setSmtPresentReading(double SMT_PRSNT_RDNG) {

   	   this.SMT_PRSNT_RDNG = SMT_PRSNT_RDNG;

   }

   public double getSmtQuantity() {

   	   return SMT_QTY;

   }

   public void setSmtQuantity(double SMT_QTY) {

   	   this.SMT_QTY = SMT_QTY;

   }

   public double getSmtUnitPrice() {

   	   return SMT_UNT_PRC;

   }

   public void setSmtUnitPrice(double SMT_UNT_PRC) {

   	   this.SMT_UNT_PRC = SMT_UNT_PRC;

   }




   public double getSmtTaxAmount() {

   	   return SMT_TX_AMNT;

   }

   public void setSmtTaxAmount(double SMT_TX_AMNT) {

   	   this.SMT_TX_AMNT = SMT_TX_AMNT;

   }



   public double getSmtWithholdingTaxAmount() {

   	   return SMT_WTX_AMNT;

   }

   public void setSmtWithholdingTaxAmount(double SMT_WTX_AMNT) {

   	   this.SMT_WTX_AMNT = SMT_WTX_AMNT;

   }


   public String getSmtTaxCode() {

   	   return SMT_TX_CD;

   }

   public void setSmtTaxCode(String SMT_TX_CD) {

   	   this.SMT_TX_CD = SMT_TX_CD;

   }


   public String getSmtCurrency() {

   	   return SMT_CRRNCY;

   }

   public void setSmtCurrency(String SMT_CRRNCY) {

   	   this.SMT_CRRNCY = SMT_CRRNCY;

   }


   public String getSmtCustomerName() {

   	   return SMT_CSTMR_NM;

   }

   public void setSmtCustomerName(String SMT_CSTMR_NM) {

   	   this.SMT_CSTMR_NM = SMT_CSTMR_NM;

   }

   public String getSmtCustomerAddress() {

   	   return SMT_CSTMR_ADDRSS;

   }

   public void setSmtCustomerAddress(String SMT_CSTMR_ADDRSS) {

   	   this.SMT_CSTMR_ADDRSS = SMT_CSTMR_ADDRSS;

   }

   public String getSmtCstContact() {

   	   return SMT_CST_CNTCT;

   }

   public void setSmtCstContact(String SMT_CST_CNTCT) {

   	   this.SMT_CST_CNTCT = SMT_CST_CNTCT;

   }

   public String getSmtCstTin() {

   	   return SMT_CST_TIN;

   }

   public void setSmtCstTin(String SMT_CST_TIN) {

   	   this.SMT_CST_TIN = SMT_CST_TIN;

   }

   public double getSmtCstSquareMeter() {

	   return SMT_CST_SQ_MTR;

   }

   public void setSmtCstSquareMeter(double SMT_CST_SQ_MTR) {

	   this.SMT_CST_SQ_MTR = SMT_CST_SQ_MTR;

   }

   public String getSmtCstFirstName() {

	   return SMT_CST_FRST_NM;

   }

   public void setSmtCstFirstName(String SMT_CST_FRST_NM) {

	   this.SMT_CST_FRST_NM = SMT_CST_FRST_NM;

   }


   public String getSmtCstLastName() {

	   return SMT_CST_LST_NM;

   }

   public void setSmtCstLastName(String SMT_CST_LST_NM) {

	   this.SMT_CST_LST_NM = SMT_CST_LST_NM;

   }




   public String getSmtCstParkingID(){
	   return SMT_PRKNG_ID;
   }

   public void setSmtCstParkingID(String SMT_PRKNG_ID){
	   this.SMT_PRKNG_ID = SMT_PRKNG_ID;
   }

   public String getSmtCstEML(){
	   return SMT_CST_EML;
   }

   public void setSmtCstEML(String SMT_CST_EML){
	   this.SMT_CST_EML = SMT_CST_EML;
   }


   public String getSmtCstCustomerID(){
	   return SMT_CST_WP_CSTMR_ID;
   }

   public void setSmtCstCustomerID(String SMT_CST_WP_CSTMR_ID){
	   this.SMT_CST_WP_CSTMR_ID = SMT_CST_WP_CSTMR_ID;
   }



   public String getSmtCstEmployeeID(){
	   return SMT_CST_EMP_ID;
   }

   public void setSmtCstEmployeeID(String SMT_CST_EMP_ID){
	   this.SMT_CST_EMP_ID = SMT_CST_EMP_ID;
   }


   public String getSmtCstAccountNumber(){
	   return SMT_CST_ACCNT_NO;
   }

   public void setSmtCstAccountNumber(String SMT_CST_ACCNT_NO){
	   this.SMT_CST_ACCNT_NO = SMT_CST_ACCNT_NO;
   }


   public String getSmtCstBillingFooter() {

   	   return SMT_CST_BLLNG_FTR;

   }

   public void setSmtCstBillingFooter(String SMT_CST_BLLNG_FTR) {

   	   this.SMT_CST_BLLNG_FTR = SMT_CST_BLLNG_FTR;

   }

   public String getSmtCstBillingHeader() {

   	   return SMT_CST_BLLNG_HDR;

   }

   public void setSmtCstBillingHeader(String SMT_CST_BLLNG_HDR) {

   	   this.SMT_CST_BLLNG_HDR = SMT_CST_BLLNG_HDR;

   }

   public String getSmtCstBillingSignatory() {

   	   return SMT_CST_BLLNG_SGNTRY;

   }

   public void setSmtCstBillingSignatory(String SMT_CST_BLLNG_SGNTRY) {

   	   this.SMT_CST_BLLNG_SGNTRY = SMT_CST_BLLNG_SGNTRY;

   }

   public String getSmtCstSignatoryTitle() {

   	   return SMT_CST_SGNTRY_TTL;

   }

   public void setSmtCstSignatoryTitle(String SMT_CST_SGNTRY_TTL) {

   	   this.SMT_CST_SGNTRY_TTL = SMT_CST_SGNTRY_TTL;

   }

   public String getSmtReceiptNumber() {

	   return SMT_RCPT_NMBR;

   }

   public void setSmtReceiptNumber(String SMT_RCPT_NMBR) {

	   this.SMT_RCPT_NMBR = SMT_RCPT_NMBR;

   }


   public String getSmtSmlWpProductID() {

	   return SMT_SML_WP_PRDCT_ID;

   }

   public void setSmtSmlWpProductID(String SMT_SML_WP_PRDCT_ID) {

	   this.SMT_SML_WP_PRDCT_ID = SMT_SML_WP_PRDCT_ID;

   }

   public Date getSmtReceiptDate() {

	   return SMT_RCPT_DT;

   }

   public void setSmtReceiptDate(Date SMT_RCPT_DT) {

	   this.SMT_RCPT_DT = SMT_RCPT_DT;

   }

   public double getSmtReceiptApplyAmount() {

	   return SMT_RCPT_APPLY_AMNT;

   }

   public void setSmtReceiptApplyAmount(double SMT_RCPT_APPLY_AMNT) {

	   this.SMT_RCPT_APPLY_AMNT = SMT_RCPT_APPLY_AMNT;

   }

   public String getSmtReferenceNumber() {

	   return SMT_RFRNC_NMBR;

   }

   public void setSmtReferenceNumber(String SMT_RFRNC_NMBR) {

	   this.SMT_RFRNC_NMBR = SMT_RFRNC_NMBR;

   }

   public String getSmtCreditMemoNumber() {

	   return SMT_CRDT_MMO_NMBR;

   }

   public void setSmtCreditMemoNumber(String SMT_CRDT_MMO_NMBR) {

	   this.SMT_CRDT_MMO_NMBR = SMT_CRDT_MMO_NMBR;

   }

   public Date getSmtCreditMemoDate() {

	   return SMT_CRDT_MMO_DT;

   }

   public void setSmtCreditMemoDate(Date SMT_CRDT_MMO_DT) {

	   this.SMT_CRDT_MMO_DT = SMT_CRDT_MMO_DT;

   }

   public double getSmtCreditMemoAmount() {

	   return SMT_CRDT_MMO_AMNT;

   }

   public void setSmtCreditMemoAmount(double SMT_CRDT_MMO_AMNT) {

	   this.SMT_CRDT_MMO_AMNT = SMT_CRDT_MMO_AMNT;

   }

   public String getOrderBy() {

	   return ORDER_BY;

   }

   public void setOrderBy(String ORDER_BY) {

	   this.ORDER_BY = ORDER_BY;

   }

   public String getSmtNumberDays(){

	   return SMT_NO_DYS;
   }

   public void setSmtNumberDays(String SMT_NO_DYS) {

	   	  this.SMT_NO_DYS = SMT_NO_DYS;

   }

   public Double getSmtExclusiveAdvance(){

	   return SMT_EXC_ADV;
   }

   public void setSmtExclusiveAdvance(Double SMT_EXC_ADV) {

	   	  this.SMT_EXC_ADV = SMT_EXC_ADV;

   }

   public Double getSmtInclusiveAdvance(){

	   return SMT_INC_ADV;
   }

   public void setSmtInclusiveAdvance(Double SMT_INC_ADV) {

	   	  this.SMT_INC_ADV = SMT_INC_ADV;

   }





   //SMT_TRNSCTN
   public static Comparator TransactionComparator = new Comparator() {

	   public int compare(Object SMT, Object anotherSMT) {

		   String SMT_TRNSCTN1 = ((ArRepStatementDetails) SMT).getSmtTransaction();

		   String SMT_TRNSCTN2 = ((ArRepStatementDetails) anotherSMT).getSmtTransaction();

		   String ORDER_BY = ((ArRepStatementDetails) SMT).getOrderBy();

		   if (!(SMT_TRNSCTN1.equals(SMT_TRNSCTN2))) {

			   return SMT_TRNSCTN1.compareTo(SMT_TRNSCTN2);

		   } else {

			   return SMT_TRNSCTN1.compareTo(SMT_TRNSCTN2);

		   }

	   }

   };

 //SMT_TRNSCTN
   public static Comparator CustomerCodeComparator = new Comparator() {

	   public int compare(Object SMT, Object anotherSMT) {

		   String SMT_CSTMR_CD1 = ((ArRepStatementDetails) SMT).getSmtCustomerCode() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerCode();

		   String SMT_CSTMR_CD2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode();

		   String ORDER_BY = ((ArRepStatementDetails) SMT).getOrderBy();
		   System.out.println("TRANSACTION="+((ArRepStatementDetails) anotherSMT).getSmtTransaction());
		   System.out.println("SMT_CSTMR_CD1="+SMT_CSTMR_CD1);
		   System.out.println("SMT_CSTMR_CD2="+SMT_CSTMR_CD2);

		   if (!(SMT_CSTMR_CD1.equals(SMT_CSTMR_CD2))) {

			   return SMT_CSTMR_CD1.compareTo(SMT_CSTMR_CD2);

		   } else {

			   return SMT_CSTMR_CD1.compareTo(SMT_CSTMR_CD2);

		   }

	   }

   };


public static Comparator DueDateComparator = new Comparator() {

	   public int compare(Object SMT, Object anotherSMT) {

		   String SMT_CST_BTCH1 = ((ArRepStatementDetails) SMT).getSmtCustomerBatch() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerBatch();

		   String SMT_CST_BTCH2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerBatch() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerBatch();


		   String SMT_TRANS1 = ((ArRepStatementDetails) SMT).getSmtTransaction() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtTransaction();

		   String SMT_TRANS2 = ((ArRepStatementDetails) anotherSMT).getSmtTransaction() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtTransaction();



		   Date SM_DT1 = ((ArRepStatementDetails) SMT).getSmtDate();

		   Date SM_DT2 = ((ArRepStatementDetails) anotherSMT).getSmtDate();

		   Date SM_DUE_DT1 = ((ArRepStatementDetails) SMT).getSmtDueDate();

		   Date SM_DUE_DT2 = ((ArRepStatementDetails) anotherSMT).getSmtDueDate();

		   String SMT_CSTMR_CD1 = ((ArRepStatementDetails) SMT).getSmtCustomerCode() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerCode();

		   String SMT_CSTMR_CD2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode();

		   String SMT_ITM1 = ((ArRepStatementDetails) SMT).getSmtItemDescription() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtItemDescription();

		   String SMT_ITM2 = ((ArRepStatementDetails) anotherSMT).getSmtItemDescription() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtItemDescription();


		   String ORDER_BY = ((ArRepStatementDetails) SMT).getOrderBy();

		   if (!(SMT_CSTMR_CD1.equals(SMT_CSTMR_CD2))) {

			   return SMT_CSTMR_CD1.compareTo(SMT_CSTMR_CD2);

		   } else if (!(SM_DT1.equals(SM_DT2))) {

			   return SM_DT1.compareTo(SM_DT2);

		   } else if (!(SMT_TRANS1.equals(SMT_TRANS2))) {

			   return SMT_TRANS1.compareTo(SMT_TRANS2);

		   } else {

			   return SM_DUE_DT1.compareTo(SM_DUE_DT2);

		   }

	   }

   };

   public static Comparator DateComparator = new Comparator() {

	   public int compare(Object SMT, Object anotherSMT) {

		   String SMT_CST_BTCH1 = ((ArRepStatementDetails) SMT).getSmtCustomerBatch() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerBatch();

		   String SMT_CST_BTCH2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerBatch() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerBatch();

		   Date SM_DT1 = ((ArRepStatementDetails) SMT).getSmtDate();

		   Date SM_DT2 = ((ArRepStatementDetails) anotherSMT).getSmtDate();

		   String SMT_CSTMR_CD1 = ((ArRepStatementDetails) SMT).getSmtCustomerCode() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerCode();

		   String SMT_CSTMR_CD2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode();

		   String SMT_ITM1 = ((ArRepStatementDetails) SMT).getSmtItemDescription() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtItemDescription();

		   String SMT_ITM2 = ((ArRepStatementDetails) anotherSMT).getSmtItemDescription() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtItemDescription();


		   String SMT_CST_DEP1 = ((ArRepStatementDetails) SMT).getSmtCustomerDepartment() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerDepartment();

		   String SMT_CST_DEP2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerDepartment() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerDepartment();


		   String ORDER_BY = ((ArRepStatementDetails) SMT).getOrderBy();

		   if (!(SMT_CST_BTCH1.equals(SMT_CST_BTCH2))) {

			   return SMT_CST_BTCH1.compareTo(SMT_CST_BTCH2);

		   } else if (!(SMT_CST_DEP1.equals(SMT_CST_DEP2))) {

			   return SMT_CST_DEP1.compareTo(SMT_CST_DEP2);

		   } else if (!(SMT_CSTMR_CD1.equals(SMT_CSTMR_CD2))) {

			   return SMT_CSTMR_CD1.compareTo(SMT_CSTMR_CD2);

		   } else {

			   return SM_DT1.compareTo(SM_DT2);

		   }

	   }

   };


 //SMT_TRNSCTN
   public static Comparator CustomerLedgerComparator = new Comparator() {

	   public int compare(Object SMT, Object anotherSMT) {

		   String SMT_CST_BTCH1 = ((ArRepStatementDetails) SMT).getSmtCustomerBatch() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerBatch();

		   String SMT_CST_BTCH2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerBatch() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerBatch();

		   Date SM_DT1 = ((ArRepStatementDetails) SMT).getSmtDate();

		   Date SM_DT2 = ((ArRepStatementDetails) anotherSMT).getSmtDate();

		   String SMT_CSTMR_CD1 = ((ArRepStatementDetails) SMT).getSmtCustomerCode() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerCode();

		   String SMT_CSTMR_CD2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode();

		   String SMT_ITM1 = ((ArRepStatementDetails) SMT).getSmtItemDescription() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtItemDescription();

		   String SMT_ITM2 = ((ArRepStatementDetails) anotherSMT).getSmtItemDescription() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtItemDescription();


		   String ORDER_BY = ((ArRepStatementDetails) SMT).getOrderBy();

		   if (!(SMT_CSTMR_CD1.equals(SMT_CSTMR_CD2))) {

			   return SMT_CSTMR_CD1.compareTo(SMT_CSTMR_CD2);

		   } else if (!(SMT_ITM1.equals(SMT_ITM2))) {

			   return SMT_ITM1.compareTo(SMT_ITM2);

		   } else {

			   return SM_DT1.compareTo(SM_DT2);

		   }

	   }

   };



   public static Comparator AccountSummaryComparator = new Comparator() {

	   public int compare(Object SMT, Object anotherSMT) {


		   Date SM_DT1 = ((ArRepStatementDetails) SMT).getSmtDate();

		   Date SM_DT2 = ((ArRepStatementDetails) anotherSMT).getSmtDate();

		   String SMT_CSTMR_CD1 = ((ArRepStatementDetails) SMT).getSmtCustomerCode() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtCustomerCode();
		   String SMT_CSTMR_CD2 = ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtCustomerCode();

		   String SMT_PM_PRJCT_CODE1 = ((ArRepStatementDetails) SMT).getSmtPmProjectCode() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtPmProjectCode();
		   String SMT_PM_PRJCT_CODE2 = ((ArRepStatementDetails) anotherSMT).getSmtPmProjectCode() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtPmProjectCode();

		   String SMT_PM_PRJCT_TYP1 = ((ArRepStatementDetails) SMT).getSmtPmProjectType() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtPmProjectType();
		   String SMT_PM_PRJCT_TYP2 = ((ArRepStatementDetails) anotherSMT).getSmtPmProjectType() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtPmProjectType();

		   String SMT_PM_CNTRCT_TRM_NM1 = ((ArRepStatementDetails) SMT).getSmtPmContractTermName() == null ? "" :  ((ArRepStatementDetails) SMT).getSmtPmContractTermName();
		   String SMT_PM_CNTRCT_TRM_NM2 = ((ArRepStatementDetails) anotherSMT).getSmtPmContractTermName() == null ? "" : ((ArRepStatementDetails) anotherSMT).getSmtPmContractTermName();



		   if (!(SMT_CSTMR_CD1.equals(SMT_CSTMR_CD2))) {

			   return SMT_CSTMR_CD1.compareTo(SMT_CSTMR_CD2);

		   } else if (!(SMT_PM_PRJCT_CODE1.equals(SMT_PM_PRJCT_CODE2))) {

			   return SMT_PM_PRJCT_CODE1.compareTo(SMT_PM_PRJCT_CODE2);

		   } else if (!(SMT_PM_PRJCT_TYP1.equals(SMT_PM_PRJCT_TYP2))) {

			   return SMT_PM_PRJCT_TYP1.compareTo(SMT_PM_PRJCT_TYP2);

		   } else if (!(SMT_PM_CNTRCT_TRM_NM1.equals(SMT_PM_CNTRCT_TRM_NM2))) {

			   return SMT_PM_CNTRCT_TRM_NM1.compareTo(SMT_PM_CNTRCT_TRM_NM2);

		   } else {

			   return SM_DT1.compareTo(SM_DT2);

		   }

	   }

   };

} // ArRepStatementDetails class




