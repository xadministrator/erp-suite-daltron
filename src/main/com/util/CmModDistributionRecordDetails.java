package com.util;


public class CmModDistributionRecordDetails extends CmDistributionRecordDetails implements java.io.Serializable {

   private String DR_COA_ACCNT_NMBR;
   private String DR_COA_ACCNT_DESC;

   public CmModDistributionRecordDetails () { ; }

   public CmModDistributionRecordDetails (Integer DR_CODE, 
         short DR_LN, String DR_CLSS, double DR_AMNT, byte DR_DBT, byte DR_RVRSL,
         byte DR_IMPRTD, String DR_COA_ACCNT_NMBR, String DR_COA_ACCNT_DESC) {
         	
      super(DR_CODE, DR_LN, DR_CLSS, DR_AMNT, DR_DBT, DR_RVRSL, DR_IMPRTD, new Integer(0));  
        
      this.DR_COA_ACCNT_NMBR = DR_COA_ACCNT_NMBR;
      this.DR_COA_ACCNT_DESC = DR_COA_ACCNT_DESC;

   }
   
   public String getDrCoaAccountNumber() {
   	
   	  return DR_COA_ACCNT_NMBR;
   	
   }
   
   public void setDrCoaAccountNumber(String DR_COA_ACCNT_NMBR) {
   	
   	  this.DR_COA_ACCNT_NMBR = DR_COA_ACCNT_NMBR;
   	
   }
   
   public String getDrCoaAccountDescription() {
   	
   	  return DR_COA_ACCNT_DESC;
   	
   }
   
   public void setDrCoaAccountDescription(String DR_COA_ACCNT_DESC) {
   	
   	  this.DR_COA_ACCNT_DESC = DR_COA_ACCNT_DESC;
   	
   }
  
}
 

// CmModDistributionRecordDetails class   
