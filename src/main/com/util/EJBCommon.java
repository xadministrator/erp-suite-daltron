package com.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class EJBCommon implements java.io.Serializable {

   public final static byte TRUE = 1;
   public final static byte FALSE = 0;
   public final static char SEPARATOR = '$';
   public final static char COMMA_DELIMITER = ',';
   public final static String DELIMETER = "$";

   public static double roundIt(double AMNT, short PRCSN) {
   
      /*** Round number to precision */

      return Math.round(AMNT * Math.pow(10, (double)PRCSN)) / Math.pow(10, (double)PRCSN);

   }

   public static boolean validateAmount(double AMNT, double MNMM_ACCNT_UNIT) {
      
      String strAmount = Double.toString(MNMM_ACCNT_UNIT);
      strAmount = strAmount.substring(strAmount.indexOf('.'));
      double precision = strAmount.length();
     
      if(((AMNT * Math.pow(10, (double)precision)) / (MNMM_ACCNT_UNIT * Math.pow(10,(double)precision))) ==
         Math.round((AMNT * Math.pow(10, (double)precision)) / (MNMM_ACCNT_UNIT * Math.pow(10,(double)precision)))) {
	 return true;
      }

      return false;
   }

   public static GregorianCalendar getGcCurrentDateWoTime(){
      GregorianCalendar gcCurrDate = new GregorianCalendar();
      gcCurrDate.set(gcCurrDate.get(Calendar.YEAR), gcCurrDate.get(Calendar.MONTH), gcCurrDate.get(Calendar.DATE), 0, 0, 0);
      gcCurrDate.set(Calendar.MILLISECOND, 0);
      return(gcCurrDate);
   }
   
   public static Date getIntendedDate(int YR) {
      GregorianCalendar intendedGc = new GregorianCalendar(YR, 0,
            1, 0, 0, 0); 
      intendedGc.set(Calendar.MILLISECOND, 0);
      
      Date intendedDate = new Date(intendedGc.getTime().getTime());
      return (intendedDate);
   }
   
   public static String incrementStringNumber(String stringNumber) {
   	   		
		String strNumberExtract = stringNumber.replaceFirst(".*\\D(\\d+)\\D*$", "$1");
				
		long num;
		
		try {
			
			num = Long.parseLong(strNumberExtract);
			
		} catch (NumberFormatException ex) {
			
			return strNumberExtract + "1";
			
		}
		
		String strIncrementedNumberExtract = Long.toString(++num);
	
		int diffLen = strNumberExtract.length() - strIncrementedNumberExtract.length();	
	
		String zeroPadding = "";
	
		if (diffLen > 0) for (int i = 0; i < diffLen; i++) zeroPadding = zeroPadding + "0";
	
		return stringNumber.replaceFirst("\\d+(\\D*)$", zeroPadding + strIncrementedNumberExtract + "$1");
       	    
   }

   public static Date convertStringToSQLDate(String strDateInput){
   	
   		String DATE_FORMAT_INPUT = "MM/dd/yy";
   	
	   	if(strDateInput != null && strDateInput.length() >= 1){
	   		
	   		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_INPUT);
	   		sdf.setLenient(false);
	   		
	   		try{
	   			
	   			return(sdf.parse(strDateInput));
	   			
	   		}catch(ParseException e){
	   			
	   			return(null);
	   			
	   		}
	   		
	   	} else{
	   		
	   		return(null);
	   		
	   	}
	   	
   }
   
   public static ArrayList miscList(String longtext){
       ArrayList miscList = new ArrayList();

       if (longtext.length() == 0)
       {
           return miscList;
       }

       if (longtext.substring(0, 1).equals(DELIMETER))
           longtext = longtext.substring(1);


       while (longtext.length() <= 1 || !longtext.equals(DELIMETER))
       {
           int index = longtext.indexOf(DELIMETER);
           if (index < 0)
               break;

           String IMEI = longtext.substring(0, index);
           
           miscList.add(IMEI);
           
           longtext = longtext.substring(index + 1);
       }

       return miscList;
   }
   
   public static String convertSQLDateToString(Date dtInput){
   	
   	String DATE_FORMAT = "MM/dd/yyyy";
   	
    if(dtInput != null){
       SimpleDateFormat sdfDateOutput = new SimpleDateFormat(DATE_FORMAT);
       return(sdfDateOutput.format(dtInput));  
    }
    else{
       return(null);
    }
 }
   
   
   public static String convertDoubleToStringMoney(double number, short precisionUnit){
      String decimalFormat = new String("#,###,###,###,###,###,##0.");
      for(int i=0; i<precisionUnit; i++){
         decimalFormat = decimalFormat + "0";
      }
      DecimalFormat dcf = new DecimalFormat(decimalFormat);
      return dcf.format(number);
   }
   
}
