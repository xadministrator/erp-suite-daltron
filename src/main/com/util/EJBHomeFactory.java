package com.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJBHome;
import javax.ejb.EJBLocalHome;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class EJBHomeFactory
{
	private String jndiPath;
    private String jndiLocalPath;
    private Map ejbHomes;
    private Context initialCtx;
    private static EJBHomeFactory instance;

    public synchronized static void init() throws NamingException
    {
        
    instance = new EJBHomeFactory(null);
    }

    public synchronized static void init(Context ctx) throws NamingException
    {
        
        instance = new EJBHomeFactory(ctx);
    }

    public static EJBHomeFactory getFactory()
    {
      
        if( instance == null ) {
            
            try{
                
                EJBHomeFactory.init();
                
            } catch (NamingException e) {
                
                Debug.print("Fatal error!");
                
            }
        }
        
        return instance;     
    }

    public static EJBHome lookUpHome( String jndiName, Class homeClass ) throws NamingException
    {
    	try {
    	
	        return EJBHomeFactory.getFactory().lookUp(EJBHomeFactory.getFactory().lookUpJndi() +  jndiName.substring(jndiName.indexOf('/'), jndiName.length()), homeClass );    	
	        
    	} catch (NamingException ex) {
    		
    		return EJBHomeFactory.getFactory().lookUp(jndiName, homeClass );
    		
    	}
            	
    }

    public static EJBLocalHome lookUpLocalHome( String jndiName, Class localHomeClass ) throws NamingException
    {        
    	try {
    		
    		return EJBHomeFactory.getFactory().lookUpLocal(EJBHomeFactory.getFactory().lookUpLocalJndi() + jndiName.substring(jndiName.indexOf('/'), jndiName.length()), localHomeClass );
    		
    	} catch (NamingException ex) {

    		return EJBHomeFactory.getFactory().lookUpLocal(jndiName, localHomeClass );
    	
    	}

    }
        
    public String lookUpJndi() throws NamingException
	{
    	
    	if (jndiPath == null) {
    		
    		jndiPath = (String) new InitialContext().lookup("java:comp/env/jndiPath");
    		
    	}
    	
    	return jndiPath;
	}

    public String lookUpLocalJndi() throws NamingException
	{
    	
    	if (jndiLocalPath == null) {
    		
    		jndiLocalPath = (String) new InitialContext().lookup("java:comp/env/jndiLocalPath");
    		
    	}
    	
    	return jndiLocalPath;
	}


    public EJBHome lookUp(String jndiName, Class homeClass) throws NamingException
    {

        // input validate arguments
        if( jndiName == null ) 
            throw new IllegalArgumentException( "supplied jndiName to lookup was null" );

        if( homeClass == null )
            throw new IllegalArgumentException( "supplied home class to lookup was null" );

        // lookup ejb home in cache
        EJBHome ejbHome = (EJBHome)ejbHomes.get(homeClass);

        // if not there, create and add to cache
        if( ejbHome == null )
        {
                         
            ejbHome = (EJBHome) new InitialContext().lookup(jndiName);  
            ejbHomes.put( homeClass, ejbHome );
            
        }

        // send back the specified home interface
        return ejbHome;
    }

    public EJBLocalHome lookUpLocal(String jndiName, Class homeClass) throws NamingException
    {
        
        //input validate arguments
        if( jndiName == null ) 
            throw new IllegalArgumentException( "supplied jndiName to lookup was null" );

        if( homeClass == null )
            throw new IllegalArgumentException( "supplied home class to lookup was null" );


        // lookup ejb home in cache
        EJBLocalHome ejbLocalHome = (EJBLocalHome)ejbHomes.get(homeClass);

        // if not there, create and add to cache
        if( ejbLocalHome == null ){
            
            ejbLocalHome = (EJBLocalHome) new InitialContext().lookup(jndiName);  
            ejbHomes.put( homeClass, ejbLocalHome );
        }

        // send back the specified home interface
        return ejbLocalHome;
    }

    private EJBHomeFactory(Context ctx) throws NamingException
    {
        if( (initialCtx = ctx) == null )
            initialCtx = new InitialContext();

        ejbHomes = Collections.synchronizedMap( new HashMap() );
    }

    private Context getInitialContext()
    {
        
        return initialCtx;
    }
}