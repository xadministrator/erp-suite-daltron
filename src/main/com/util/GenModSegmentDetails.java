package com.util;


public class GenModSegmentDetails extends GenSegmentDetails implements java.io.Serializable {

   private char SG_FL_SGMNT_SPRTR;
   
   public GenModSegmentDetails() { ; } 

   public char getSgFlSegmentSeparator() {
   	
   	   return SG_FL_SGMNT_SPRTR;
   	
   }
   
   public void setSgFlSegmentSeparator(char SG_FL_SGMNT_SPRTR) {
   	
   	   this.SG_FL_SGMNT_SPRTR = SG_FL_SGMNT_SPRTR;
   	
   }

} // GenModSegmentDetails
