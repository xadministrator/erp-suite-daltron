package com.util;


public class GenModValueSetDetails extends GenValueSetDetails implements java.io.Serializable {

   private byte VS_SG_NTRL;
   private short VS_SGMNT_NMBR;
   
   public GenModValueSetDetails() { ; }

   public GenModValueSetDetails (byte VS_SG_NTRL, short VS_SGMNT_NMBR) {

      this.VS_SG_NTRL = VS_SG_NTRL;
      this.VS_SGMNT_NMBR = VS_SGMNT_NMBR;
      
   }

   public byte getVsSgNatural() {
   	
      return VS_SG_NTRL;
      
   }
   
   public void setVsSgNatural(byte VS_SG_NTRL) {
   	
   	  this.VS_SG_NTRL = VS_SG_NTRL;
    
   }
   
   public short getVsSegmentNumber() {
   	
   	  return VS_SGMNT_NMBR;
   	  
   }
   
   public void setVsSegmentNumber(short VS_SGMNT_NMBR) {
   	
   	  this.VS_SGMNT_NMBR = VS_SGMNT_NMBR;
   	  
   }
   
} // GenModValueSetDetails
