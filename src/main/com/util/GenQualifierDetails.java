package com.util;

public class GenQualifierDetails implements java.io.Serializable {

   private Integer QL_CODE;
   private String QL_ACCNT_TYP;
   private byte QL_BDGTNG_ALLWD;
   private byte QL_PSTNG_ALLWD;

   public GenQualifierDetails () { ; }

   public GenQualifierDetails (Integer QL_CODE, String QL_ACCNT_TYP,
      byte QL_BDGTNG_ALLWD, byte QL_PSTNG_ALLWD) {

      this.QL_CODE = QL_CODE;
      this.QL_ACCNT_TYP = QL_ACCNT_TYP;
      this.QL_BDGTNG_ALLWD = QL_BDGTNG_ALLWD;
      this.QL_PSTNG_ALLWD = QL_PSTNG_ALLWD;
   }

   public Integer getQlCode() {
      return QL_CODE;
   }

   public String getQlAccountType() {
      return QL_ACCNT_TYP;
   }

   public byte getQlBudgetingAllowed() {
      return QL_BDGTNG_ALLWD;
   }

   public byte getQlPostingAllowed() {
      return QL_PSTNG_ALLWD;
   }

   public String toString() {
      String s = "QL_CODE = " + QL_CODE + " QL_ACCNT_TYP = " + QL_ACCNT_TYP +
         " QL_BDGTNG_ALLWD = " + QL_BDGTNG_ALLWD + " QL_PSTNG_ALLWD = " + QL_PSTNG_ALLWD;
      return s;
   }

} // GenQualifierDetails
