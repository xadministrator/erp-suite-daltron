package com.util;


public class GenRollupGroupAssignmentDetails implements java.io.Serializable {

   private Integer RLGA_CODE;

   public GenRollupGroupAssignmentDetails() { ; }

   public GenRollupGroupAssignmentDetails (Integer RLGA_CODE) {

      this.RLGA_CODE = RLGA_CODE;

   }

   public Integer getRlgaCode() {
      return RLGA_CODE;
   }

   public String toString() {
      String s = "RLGA_CODE = " + RLGA_CODE;
      return s;
   }

} // GenRollupGroupAssignmentDetails class   
