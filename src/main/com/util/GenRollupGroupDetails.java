package com.util;


public class GenRollupGroupDetails implements java.io.Serializable {

   private Integer RLG_CODE;
   private String RLG_NM;
   private String RLG_DESC;

   public GenRollupGroupDetails() { ; }

   public GenRollupGroupDetails (Integer RLG_CODE, String RLG_NM, String RLG_DESC) {
   
      this.RLG_CODE = RLG_CODE;
      this.RLG_NM = RLG_NM;
      this.RLG_DESC = RLG_DESC;
   }

   public Integer getRlgCode() {
      return RLG_CODE;
   }

   public String getRlgName() {
      return RLG_NM;
   }

   public String getRlgDescription() {
      return  RLG_DESC;
   }

   public String toString() {
      String s = "RLG_CODE = " + RLG_CODE + " RLG_NM = " + RLG_NM +
         " RLG_DESC = " + RLG_DESC;
      return s;
   }

} // GenRollupGroupDetails
