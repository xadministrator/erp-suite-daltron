package com.util;

public class GenStructureDetails implements java.io.Serializable {

   private Integer ST_CODE;
   private String ST_NM;
   private String ST_DESC;
   private byte ST_ENBL;

   public GenStructureDetails() { ; }

   public GenStructureDetails (Integer ST_CODE, String ST_NM,
      String ST_DESC, byte ST_ENBL) {

      this.ST_CODE = ST_CODE;
      this.ST_NM = ST_NM;
      this.ST_DESC = ST_DESC;
      this.ST_ENBL = ST_ENBL;

   }

   public Integer getStCode() {
      return ST_CODE;
   }

   public String getStName() {
      return ST_NM;
   }

   public String getStDescription() {
      return ST_DESC;
   }

   public byte getStEnable() {
      return ST_ENBL;
   }

   public String toString() {
      String s = "ST_CODE = " + ST_CODE + " ST_NM = " + ST_NM +
         " ST_DESC = " + ST_DESC + " ST_ENBL = " + ST_ENBL;
      return s;
   }

} // GenStructureDetails
