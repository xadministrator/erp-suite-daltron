package com.util;

public class GlApprovalQueueDetails implements java.io.Serializable {

   private Integer AQ_CODE;

   public GlApprovalQueueDetails() { ; }
   
   public GlApprovalQueueDetails (Integer AQ_CODE) {

      this.AQ_CODE = AQ_CODE;

   }

   public Integer getAqCode() {
      return AQ_CODE;
   }

   public String toString() {
      String s = "AQ_CODE = " + AQ_CODE;
      return s;
   }

} // GlApprovalQueueDetails
