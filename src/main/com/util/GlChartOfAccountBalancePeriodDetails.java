package com.util;


public class GlChartOfAccountBalancePeriodDetails implements java.io.Serializable {

   private Integer COABP_CODE;
   private short COABP_PRD_NMBR;

   public GlChartOfAccountBalancePeriodDetails() { ; }
   
   public GlChartOfAccountBalancePeriodDetails (Integer COABP_CODE,
      short COABP_PRD_NMBR) {

      this.COABP_CODE = COABP_CODE;
      this.COABP_PRD_NMBR = COABP_PRD_NMBR;

   }

   public Integer getCoabpCode() {
      return COABP_CODE;
   }

   public short getCoabpPeriodNumber() {
      return COABP_PRD_NMBR;
   }

   public String toString() {
      String s = "COABP_CODE = " + COABP_CODE +
         "COABP_PRD_NMBR = " + COABP_PRD_NMBR;
      return s;
   }

}  // GlChartOfAccountBalancePeriodDetails class

      

   
