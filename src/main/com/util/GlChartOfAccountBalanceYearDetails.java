package com.util;


public class GlChartOfAccountBalanceYearDetails implements java.io.Serializable {

   private Integer COABY_CODE;
   private short COABY_YR;

   public GlChartOfAccountBalanceYearDetails() { ; }

   public GlChartOfAccountBalanceYearDetails (Integer COABY_CODE,
      short COABY_YR) {

      this.COABY_CODE = COABY_CODE;
      this.COABY_YR = COABY_YR;
   }

   public Integer getCoabyCode() {
      return COABY_CODE;
   }

   public short GetCoabyYear() {
      return COABY_YR;
   }
  
   public String toString() {
      String s = "COABY_CODE = " + COABY_CODE + "COABY_YR = " + COABY_YR;
      return s;
   }
} // GlChartOfAccountBalanceYearDetails class
