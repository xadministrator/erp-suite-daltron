package com.util;

public class GlCurrencyRateDetails implements java.io.Serializable {

   private Integer CRR_CODE;
   private String CRR_NM;
   private String CRR_DESC;
   private char CRR_TYP;

   public GlCurrencyRateDetails() { ; }

   public GlCurrencyRateDetails (Integer CRR_CODE, String CRR_NM,
      String CRR_DESC, char CRR_TYP) {
      
      this.CRR_CODE = CRR_CODE;
      this.CRR_NM = CRR_NM;
      this.CRR_DESC = CRR_DESC;
      this.CRR_TYP = CRR_TYP;

   }

   public Integer getCrrCode() {
      return CRR_CODE;
   }

   public String getCrrName() {
      return CRR_NM;
   }

   public String getCrrDescription() {
      return CRR_DESC;
   }

   public char getCrrType() {
      return CRR_TYP;
   }

   public String toString() {
      String s = "CRR_CODE = " + CRR_CODE + " CRR_NM = " + CRR_NM +
         " CRR_DESC = " + CRR_DESC + " CRR_TYP = " + CRR_TYP;
      return s;
   }

} // GlCurrencyRateDetails class
