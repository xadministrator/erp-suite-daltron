package com.util;

public class GlModBudgetAccountAssignmentDetails extends GlBudgetAccountAssignmentDetails implements java.io.Serializable {

   private String BAA_BO_NM;
   private String BAA_ACCNT_FRM_DESC;
   private String BAA_ACCNT_TO_DESC;

   public GlModBudgetAccountAssignmentDetails() { ; }


   public String getBaaBudgetOrganizationName() {
   	
   	  return BAA_BO_NM;
   	
   }
   
   public void setBaaBudgetOrganizationName(String BAA_BO_NM) {
   	
   	  this.BAA_BO_NM = BAA_BO_NM;
   	
   }
   
   public String getBaaAccountFromDescription() {
   	
   	  return BAA_ACCNT_FRM_DESC;
   	
   }
   
   public void setBaaAccountFromDescription(String BAA_ACCNT_FRM_DESC) {
   	
   	  this.BAA_ACCNT_FRM_DESC = BAA_ACCNT_FRM_DESC;
   	
   }
   
   public String getBaaAccountToDescription() {
   	
   	  return BAA_ACCNT_TO_DESC;
   	
   }
   
   public void setBaaAccountToDescription(String BAA_ACCNT_TO_DESC) {
   	
   	  this.BAA_ACCNT_TO_DESC = BAA_ACCNT_TO_DESC;
   	
   }

} // GlModBudgetAccountAssignmentDetails class
