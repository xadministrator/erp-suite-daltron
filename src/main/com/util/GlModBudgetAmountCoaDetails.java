package com.util;

import java.util.ArrayList;

public class GlModBudgetAmountCoaDetails extends GlBudgetAmountCoaDetails implements java.io.Serializable {

   private String BC_COA_ACCNT_NMBR = null;
   private String BC_COA_ACCNT_DESC = null;
   private ArrayList glBcBudgetAmountPeriodList;

   public GlModBudgetAmountCoaDetails() { ; }
   
   public String getBcCoaAccountNumber() {
   	
   	   return BC_COA_ACCNT_NMBR;
   	
   }
   
   public void setBcCoaAccountNumber(String BC_COA_ACCNT_NMBR) {
   	
   	   this.BC_COA_ACCNT_NMBR = BC_COA_ACCNT_NMBR;
   	
   }
   
   public String getBcCoaAccountDescription() {
   	
   	   return BC_COA_ACCNT_DESC;
   	
   }
   
   public void setBcCoaAccountDescription(String BC_COA_ACCNT_DESC) {
   	
   	   this.BC_COA_ACCNT_DESC = BC_COA_ACCNT_DESC;
   	
   }

   
   public ArrayList getBcBudgetAmountPeriodList() {
   	
   	  return glBcBudgetAmountPeriodList;
   	
   }
   
   public void setBcBudgetAmountPeriodList(ArrayList glBcBudgetAmountPeriodList) {
   	
   	  this.glBcBudgetAmountPeriodList = glBcBudgetAmountPeriodList;
   	
   }

} // GlModBudgetAmountDetails class
