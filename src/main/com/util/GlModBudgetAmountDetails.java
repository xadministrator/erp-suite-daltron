package com.util;

import java.util.ArrayList;

public class GlModBudgetAmountDetails extends GlBudgetAmountDetails implements java.io.Serializable {

   private ArrayList bgaBudgetAmountCoaList;
   private String bgaPassword;
   private String bgaBoName;
   private String bgaBgtName;

   public GlModBudgetAmountDetails() { ; }

   
   public ArrayList getBgaBudgetAmountCoaList() {
   	
   	  return bgaBudgetAmountCoaList;
   	
   }
   
   public void setBgaBudgetAmountCoaList(ArrayList bgaBudgetAmountCoaList) {
   	
   	  this.bgaBudgetAmountCoaList = bgaBudgetAmountCoaList;
   	
   }
   
   public String getBgaPassword() {
   	  
   	  return bgaPassword;
   	
   }
   
   public void setBgaPassword(String bgaPassword) {
   	
   	  this.bgaPassword = bgaPassword;
   	
   }
   
   public String getBgaBoName() {
   	
   	  return bgaBoName;
   	 
   }
   
   public void setBgaBoName(String bgaBoName) {
   	
   	  this.bgaBoName = bgaBoName;  
   	
   }
   
   public String getBgaBgtName() {
   	
   	  return bgaBgtName;
   	 
   }
   
   public void setBgaBgtName(String bgaBgtName) {
   	
   	  this.bgaBgtName = bgaBgtName;  
   	
   }

} // GlModBudgetAmountDetails class
