package com.util;

public class GlModBudgetAmountPeriodDetails extends GlBudgetAmountPeriodDetails implements java.io.Serializable {

   private String BAP_ACV_PRD_PRFX = null;
   private int BAP_ACV_YR = 0;

   public GlModBudgetAmountPeriodDetails() { ; }

   
   public String getBapAcvPeriodPrefix() {
   	
   	  return BAP_ACV_PRD_PRFX;
   	
   }
   
   public void setBapAcvPeriodPrefix(String BAP_ACV_PRD_PRFX) {
   	
   	  this.BAP_ACV_PRD_PRFX = BAP_ACV_PRD_PRFX;
   	
   }
   
   public int getBapAcvYear() {
   	
   	  return BAP_ACV_YR;
   	  
   }
   
   public void setBapAcvYear(int BAP_ACV_YR) {
   	
   	  this.BAP_ACV_YR = BAP_ACV_YR;
   	  
   }

} // GlModBudgetAmountDetails class
