package com.util;

public class GlModBudgetDetails extends GlBudgetDetails implements java.io.Serializable {

   private boolean BGT_IS_DFLT;

   public GlModBudgetDetails() { ; }

   public boolean getBgtIsDefault() {
   	
   	  return BGT_IS_DFLT;
   	 
   }
   
   public void setBgtIsDefault(boolean BGT_IS_DFLT) {
   	
   	  this.BGT_IS_DFLT = BGT_IS_DFLT;  
   	
   }

} // GlModBudgetAmountDetails class
