package com.util;


public class GlModFrgFinancialReportDetails extends GlFrgFinancialReportDetails implements java.io.Serializable {

   private String FR_RS_NM;  
   private String FR_CS_NM; 
   
   public GlModFrgFinancialReportDetails () { ; }

   public GlModFrgFinancialReportDetails (Integer FR_CODE, String FR_NM, String FR_DESC,
      String FR_TTLE, int FR_FNT_SZ, String FR_FNT_STYL, String FRG_HRZNTL_ALGN, String FR_RS_NM, String FR_CS_NM) {
   	
   	  super(FR_CODE, FR_NM, FR_DESC, FR_TTLE, FR_FNT_SZ, FR_FNT_STYL, FRG_HRZNTL_ALGN, new Integer(0));  
   	
   	  this.FR_RS_NM = FR_RS_NM;
   	  this.FR_CS_NM = FR_CS_NM;
   	
   }
   
   public String getFrRowName() {
   	
   	   return FR_RS_NM;
   	
   }
   
   public void setFrRowName(String FR_RS_NM) {
   	
   	   this.FR_RS_NM = FR_RS_NM;
   	
   }
   
   public String getFrColumnName() {
   	
   	   return FR_CS_NM;
   	
   }
   
   public void setFrColumnName(String FR_CS_NM) {
   	
   	   this.FR_CS_NM = FR_CS_NM;
   	
   }
   	      	      	      	      	      	      	                 
} // GlModFrgFinancialReportDetails class   
