package com.util;


public class GlModFunctionalCurrencyDetails implements java.io.Serializable {

   private Integer FC_CODE;
   private String FC_NM;
   private byte FC_SOB;

   public GlModFunctionalCurrencyDetails() { ; }

   public GlModFunctionalCurrencyDetails (Integer FC_CODE, String FC_NM,
      byte FC_SOB) {

      this.FC_CODE = FC_CODE;
      this.FC_NM = FC_NM;
      this.FC_SOB = FC_SOB;

   }

   public Integer getFcCode() {
      return FC_CODE;
   }

   public String getFcName() {
      return FC_NM;
   }

   public byte getFcSob() {
      return FC_SOB;
   }

   public String toString() {
      String s = "FC_CODE = " + FC_CODE + " FC_NM = " + FC_NM +
	 " FC_SOB = " + FC_SOB;
      return s;
   }

} // GlFunctionalCurrencyDetails class
