package com.util;

public class GlModJournalLineDetails extends GlJournalLineDetails implements java.io.Serializable {

   private String JL_COA_ACCNT_NMBR;
   private String JL_COA_ACCNT_DESC;

   public GlModJournalLineDetails() { ; }

   public String getJlCoaAccountNumber() {
   	
   	  return JL_COA_ACCNT_NMBR;
   	
   }
   
   public void setJlCoaAccountNumber(String JL_COA_ACCNT_NMBR) {
   	
   	  this.JL_COA_ACCNT_NMBR = JL_COA_ACCNT_NMBR;
   	
   }
   
   public String getJlCoaAccountDescription() {
   	
   	  return JL_COA_ACCNT_DESC;
   	
   }
   
   public void setJlCoaAccountDescription(String JL_COA_ACCNT_DESC) {
   	
   	  this.JL_COA_ACCNT_DESC = JL_COA_ACCNT_DESC;
   	
   }

   
} // GlModJournalLineDetails class
