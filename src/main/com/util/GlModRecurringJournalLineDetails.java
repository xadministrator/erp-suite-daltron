package com.util;

public class GlModRecurringJournalLineDetails extends GlRecurringJournalLineDetails implements java.io.Serializable {

   private String RJL_COA_ACCNT_NMBR;
   private String RJL_COA_ACCNT_DESC;

   public GlModRecurringJournalLineDetails() { ; }

   public String getRjlCoaAccountNumber() {
   	
   	  return RJL_COA_ACCNT_NMBR;
   	
   }
   
   public void setRjlCoaAccountNumber(String RJL_COA_ACCNT_NMBR) {
   	
   	  this.RJL_COA_ACCNT_NMBR = RJL_COA_ACCNT_NMBR;
   	
   }
   
   public String getRjlCoaAccountDescription() {
   	
   	  return RJL_COA_ACCNT_DESC;
   	
   }
   
   public void setRjlCoaAccountDescription(String RJL_COA_ACCNT_DESC) {
   	
   	  this.RJL_COA_ACCNT_DESC = RJL_COA_ACCNT_DESC;
   	
   }

   
} // GlModRecurringJournalLineDetails class
