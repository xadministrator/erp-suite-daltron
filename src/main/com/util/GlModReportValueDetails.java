package com.util;


public class GlModReportValueDetails extends GlReportValueDetails implements java.io.Serializable {

   private String RV_COA_ACCNT_NMBR = null;
   private String RV_COA_ACCNT_DESC = null;


   public GlModReportValueDetails() { ; }


   public String getRvCoaAccountNumber() {
	   return RV_COA_ACCNT_NMBR;
   }

   public void setRvCoaAccountNumber(String RV_COA_ACCNT_NMBR) {
	   this.RV_COA_ACCNT_NMBR =  RV_COA_ACCNT_NMBR;
   }


   public String getRvCoaAccountDescription() {
	   return RV_COA_ACCNT_DESC;
   }

   public void setRvCoaAccountDescription(String RV_COA_ACCNT_DESC) {
	   this.RV_COA_ACCNT_DESC =  RV_COA_ACCNT_DESC;
   }



} // GlRepMonthlyVatDeclarationDetailsDetails class
