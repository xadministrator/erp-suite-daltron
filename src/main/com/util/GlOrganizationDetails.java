package com.util;


public class GlOrganizationDetails implements java.io.Serializable {

   private Integer ORG_CODE;
   private String ORG_NM;
   private String ORG_DESC;
   private Integer ORG_MSTR_CODE;

   public GlOrganizationDetails() { ; }

   public GlOrganizationDetails (Integer ORG_CODE, String ORG_NM,
      String ORG_DESC, Integer ORG_MSTR_CODE) {

      this.ORG_CODE = ORG_CODE;
      this.ORG_NM = ORG_NM;
      this.ORG_DESC = ORG_DESC;
      this.ORG_MSTR_CODE = ORG_MSTR_CODE;

   }

 public GlOrganizationDetails (String ORG_NM,
    String ORG_DESC, Integer ORG_MSTR_CODE) {

    this.ORG_NM = ORG_NM;
    this.ORG_DESC = ORG_DESC;
    this.ORG_MSTR_CODE = ORG_MSTR_CODE;
    
 }

   public Integer getOrgCode() {
      return ORG_CODE;
   }

   public String getOrgName() {
      return ORG_NM;
   }

   public String getOrgDescription() {
      return ORG_DESC;
   }

   public Integer getOrgMasterCode() {
      return ORG_MSTR_CODE;
   }

   public String toString() {
      String s = "ORG_CODE = " + ORG_CODE + " ORG_NM = " + ORG_NM + 
         " ORG_DESC = " + ORG_DESC + " ORG_MSTR = " + ORG_MSTR_CODE;
      return s;
   }

} // GlOrganizationDetails class   
