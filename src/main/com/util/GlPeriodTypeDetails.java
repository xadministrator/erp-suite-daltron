package com.util;


public class GlPeriodTypeDetails implements java.io.Serializable {

   private Integer PT_CODE;
   private String PT_NM;
   private String PT_DESC;
   private short PT_PRD_PER_YR;
   private char PT_YR_TYP;

   public GlPeriodTypeDetails() { ; }

   public GlPeriodTypeDetails (Integer PT_CODE, String PT_NM,
      String PT_DESC, short PT_PRD_PER_YR, char PT_YR_TYP) {

      this.PT_CODE = PT_CODE;
      this.PT_NM = PT_NM;
      this.PT_DESC = PT_DESC;
      this.PT_PRD_PER_YR = PT_PRD_PER_YR;
      this.PT_YR_TYP = PT_YR_TYP;

   }

   public GlPeriodTypeDetails (String PT_NM,
      String PT_DESC, short PT_PRD_PER_YR, char PT_YR_TYP) {

      this.PT_NM = PT_NM;
      this.PT_DESC = PT_DESC;
      this.PT_PRD_PER_YR = PT_PRD_PER_YR;
      this.PT_YR_TYP = PT_YR_TYP;

   }

   public Integer getPtCode() {
      return PT_CODE;
   }

   public String getPtName() {
      return PT_NM;
   }

   public String getPtDescription() {
      return PT_DESC;
   }

   public short getPtPeriodPerYear() {
      return PT_PRD_PER_YR;
   }

   public char getPtYearType() {
      return PT_YR_TYP;
   }

   public String toString() {
      String s = PT_CODE + "&nbsp;&nbsp;&nbsp;&nbsp;" +
                 PT_NM + "&nbsp;&nbsp;&nbsp;&nbsp;" +
		 PT_DESC + "&nbsp;&nbsp;&nbsp;&nbsp;" +
		 PT_PRD_PER_YR + "&nbsp;&nbsp;&nbsp;&nbsp;" +
		 PT_YR_TYP;
      return s;
   }

} // GlPeriodTypeDetails class
