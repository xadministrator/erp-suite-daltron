package com.util;

import java.util.Date;

public class GlRegisteredAccountDetails implements java.io.Serializable {

   private Integer RA_CODE;
   private Date RA_DT_RGSTRD;

   public GlRegisteredAccountDetails() { ; }
   
   public GlRegisteredAccountDetails (Integer RA_CODE, Date RA_DT_RGSTRD) {

      this.RA_CODE = RA_CODE;
      this.RA_DT_RGSTRD = RA_DT_RGSTRD;

   }

   public Integer getRaCode() {
      return RA_CODE;
   }

   public Date getRaDateRegistered() {
      return RA_DT_RGSTRD;
   }

   public String toString() {
      String s = "RA_CODE = " + RA_CODE + " RA_DT_RGSTRD = " + RA_DT_RGSTRD;
      return s;
   }

} // GlRegisteredAccountDetails class   
