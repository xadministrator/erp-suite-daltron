package com.util;


public class GlResponsibilityDetails implements java.io.Serializable {

   private Integer RES_CODE;
   private byte RES_ENBL;

   public GlResponsibilityDetails() { ; }
   
   public GlResponsibilityDetails (Integer RES_CODE, byte RES_ENBL) {
   
      this.RES_CODE = RES_CODE;
      this.RES_ENBL = RES_ENBL;
   
   } 
   
   public Integer getResCode() {
      return RES_CODE;
   }
   
   public byte getResEnable() {
      return RES_ENBL;
   }
   
   public String toString() {
      String s = "RES_CODE = " + RES_CODE + " RES_ENBL = " + RES_ENBL;
      return s;
   }
      
} //GlSummaryAccountDetails class
