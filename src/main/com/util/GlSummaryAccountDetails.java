package com.util;


public class GlSummaryAccountDetails implements java.io.Serializable {

   private Integer SMA_CODE;
   private String SMA_ACCNT_NMBR;
   private double SMA_BLNC;

   public GlSummaryAccountDetails() { ; }

   public GlSummaryAccountDetails (Integer SMA_CODE,
      String SMA_ACCNT_NMBR, double SMA_BLNC) {

      this.SMA_CODE = SMA_CODE;
      this.SMA_ACCNT_NMBR = SMA_ACCNT_NMBR;
      this.SMA_BLNC = SMA_BLNC;

   }

   public Integer getSmaCode() {
      return SMA_CODE;
   }

   public String getSmaAccountNumber() {
      return SMA_ACCNT_NMBR;
   }

   public double getSmaBalance() {
      return SMA_BLNC;
   }

   public String toString() {
      String s = " SMA_CODE = " + SMA_CODE + " SMA_ACCNT_NMBR = " + SMA_ACCNT_NMBR +
         " SMA_BLNC = " + SMA_BLNC;
      return s;

   }

} // GlSummaryAccountDetails class
