package com.util;


public class GlSummaryAccountTemplateDetails implements java.io.Serializable {

   private Integer SAT_CODE;
   private String SAT_NM;
   private String SAT_TMPLT;

   public GlSummaryAccountTemplateDetails() { ; }

   public GlSummaryAccountTemplateDetails (Integer SAT_CODE,
      String SAT_NM, String SAT_TMPLT) {

      this.SAT_CODE = SAT_CODE;
      this.SAT_NM = SAT_NM;
      this.SAT_TMPLT = SAT_TMPLT;

   }

   public Integer getSatCode() {
     return SAT_CODE;
   }

   public String getSatName() {
      return SAT_NM;
   }

   public String getSatTemplate() {
      return SAT_TMPLT;
   }

   public String toString() {
      String s = "SAT_CODE = " + SAT_CODE + " SAT_NM = " + SAT_NM +
         " SAT_TMPLT = " + SAT_TMPLT;
      return s;
   }

} // GlSummaryAccountTemplateDetails class
