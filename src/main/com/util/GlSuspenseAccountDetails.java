package com.util;


public class GlSuspenseAccountDetails implements java.io.Serializable {

   private Integer SA_CODE;
   private String SA_NM;
   private String SA_DESC;

   public GlSuspenseAccountDetails() { ; }
   
   public GlSuspenseAccountDetails (Integer SA_CODE, String SA_NM,
      String SA_DESC) {

      this.SA_CODE = SA_CODE;
      this.SA_NM = SA_NM;
      this.SA_DESC = SA_DESC;

   }

   public GlSuspenseAccountDetails (String SA_NM, String SA_DESC){

      this.SA_NM = SA_NM;
      this.SA_DESC = SA_DESC;

   }

   public Integer getSaCode() {
      return SA_CODE;
   }

   public String getSaName() {
      return SA_NM;
   }

   public String getSaDescription() {
      return SA_DESC;
   }

   public String toString() {
      String s = "SA_CODE = " + SA_CODE + " SA_NM = " + SA_NM +
         " SA_DESC = " + SA_DESC;
      return s;
   }
 
} // GlSuspenseAccountDetails class
