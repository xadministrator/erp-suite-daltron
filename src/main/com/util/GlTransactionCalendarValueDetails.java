package com.util;

import java.util.Date;

public class GlTransactionCalendarValueDetails implements java.io.Serializable {

   private Integer TCV_CODE;
   private Date TCV_DT;
   private short TCV_DY_OF_WK;
   private byte TCV_BSNSS_DY;

   public GlTransactionCalendarValueDetails() { ; }

   public GlTransactionCalendarValueDetails (Integer TCV_CODE,
      Date TCV_DT, short TCV_DY_OF_WK, byte TCV_BSNSS_DY) {

      this.TCV_CODE = TCV_CODE;
      this.TCV_DT = TCV_DT;
      this.TCV_DY_OF_WK = TCV_DY_OF_WK;
      this.TCV_BSNSS_DY = TCV_BSNSS_DY;

   }

   public GlTransactionCalendarValueDetails (
      Date TCV_DT, short TCV_DY_OF_WK, byte TCV_BSNSS_DY) {

      this.TCV_DT = TCV_DT;
      this.TCV_DY_OF_WK = TCV_DY_OF_WK;
      this.TCV_BSNSS_DY = TCV_BSNSS_DY;

   }

   public GlTransactionCalendarValueDetails (
      Integer TCV_CODE, byte TCV_BSNSS_DY) {

      this.TCV_CODE = TCV_CODE;
      this.TCV_BSNSS_DY = TCV_BSNSS_DY;

   }

   public Integer getTcvCode() {
      return TCV_CODE;
   }

   public Date getTcvDate() {
      return TCV_DT;
   }

   public short getTcvDayOfWeek() {
      return TCV_DY_OF_WK;
   }

   public byte getTcvBusinessDay() {
      return TCV_BSNSS_DY;
   }

   public String toString() {
      String s = "TCV_CODE = " + TCV_CODE + " TCV_DT = " + TCV_DT +
         " TCV_DY_OF_WK = " + TCV_DY_OF_WK + " TCV_BSNSS_DY = " + TCV_BSNSS_DY;
      return s;
   }

} // GlTransactionCalendarValueDetails class
