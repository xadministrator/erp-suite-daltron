package com.util;

public class HrModAppliedDeductions {
	
	private Integer employee_id = null;
	private Integer payroll_period_id = null;
	private String managing_branch = null;
	private String dedution_type = null;
	private String omega_ips = null;
	private String amount = null;
	

   public HrModAppliedDeductions() { ; }

	   public Integer getHrEmployeeID() {
		   	
	   	  return employee_id;
	   	
	   }
	   
	   public void setHrEmployeeID(Integer employee_id) {
	   	
	   	  this.employee_id = employee_id;
	   	
	   }
	   
	   public Integer getHrPayrollPeriodID() {
		   	
	   	  return payroll_period_id;
	   	
	   }
	   
	   public void setHrPayrollPeriodID(Integer payroll_period_id) {
	   	
	   	  this.payroll_period_id = payroll_period_id;
	   	
	   }
	   
	   public String getHrManagingBranch() {
		   	
	   	  return managing_branch;
	   	
	   }
	   
	   public void setHrManagingBranch(String managing_branch) {
	   	
	   	  this.managing_branch = managing_branch;
	   	
	   }
	   
	   public String getHrDeductionType() {
		   	
	   	  return dedution_type;
	   	
	   }
	   
	   public void setHrDeductionType(String dedution_type) {
	   	
	   	  this.dedution_type = dedution_type;
	   	
	   }
	   
	   public String getHrOmegaIPS() {
		   	
	   	  return omega_ips;
	   	
	   }
	   
	   public void setHrOmegaIPS(String omega_ips) {
	   	
	   	  this.omega_ips = omega_ips;
	   	
	   }
	   
	   public String getHrAmount() {
		   	
	   	  return amount;
	   	
	   }
	   
	   public void setHrAmount(String amount) {
	   	
	   	  this.amount = amount;
	   	
	   }
	   
	   
	   
   
} // ArModInvoiceLineItemDetails class
