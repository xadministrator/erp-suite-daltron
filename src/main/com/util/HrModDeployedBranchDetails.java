package com.util;

public class HrModDeployedBranchDetails {
	
	private Integer id = null;
	private String client_code = null;
	private String client_name = null;
	private String address = null;
	private String tin = null;
	private String managing_branch = null;

   public HrModDeployedBranchDetails() { ; }

   public Integer getHrDeployedBranchID() {
	   	
   	  return id;
   	
   }
   
   public void setHrDeployedBranchID(Integer id) {
   	
   	  this.id = id;
   	
   }
   
   public String getHrClientCode() {
	   	
   	  return client_code;
   	
   }
   
   public void setHrClientCode(String client_code) {
   	
   	  this.client_code = client_code;
   	
   }
	   
   
   public String getHrClientName() {
	   	
   	  return client_name;
   	
   }
   
   public void setHrClientName(String client_name) {
   	
   	  this.client_name = client_name;
   	
   }
   
   public String getHrAddress() {
	   	
   	  return address;
   	
   }
   
   public void setHrAddress(String address) {
   	
   	  this.address = address;
   	
   }
	
	public String getHrTin() {
	   	
   	  return tin;
   	
   }
   
   public void setHrTin(String tin) {
   	
   	  this.tin = tin;
   	
   }
	
	public String getHrManagingBranch() {
	   	
   	  return managing_branch;
   	
   }
   
   public void setHrManagingBranch(String managing_branch) {
   	
   	  this.managing_branch = managing_branch;
   	
   }
	   
	   
	   
   
} // ArModInvoiceLineItemDetails class
