package com.util;

public class HrModEmployeeDetails extends HrEmployeeDetails implements java.io.Serializable {
	
	private Integer employee_id = null;
	private Integer applicant_id = null;
	private String emp_num = null;
	private String bio_no = null;
	private Integer deployed_at_branch = null;
	private String first_name = null;
	private String middle_name = null;
	private String last_name = null;
	private String managing_branch = null;
	private String current_job_position = null;
	
	
	private Integer HR_DEPLOYED_BRANCH;
   

   public HrModEmployeeDetails() { ; }

	   public Integer getHrEmployeeID() {
		   	
	   	  return employee_id;
	   	
	   }
	   
	   public void setHrEmployeeID(Integer employee_id) {
	   	
	   	  this.employee_id = employee_id;
	   	
	   }
	   
	   public Integer getHrApplicantID() {
		   	
	   	  return applicant_id;
	   	
	   }
	   
	   public void setHrApplicantID(Integer applicant_id) {
	   	
	   	  this.applicant_id = applicant_id;
	   	
	   }
	   
	   public String getHrEmployeeNumber() {
		   	
	   	  return emp_num;
	   	
	   }
	   
	   public void setHrEmployeeNumber(String emp_num) {
	   	
	   	  this.emp_num = emp_num;
	   	
	   }
	   
	   public String getHrBioNumber() {
		   	
	   	  return bio_no;
	   	
	   }
	   
	   public void setHrBioNumber(String bio_no) {
	   	
		   this.bio_no = bio_no;
   	
	   }
	   
	   
	   public String getHrFirstName() {
		   	
	   	  return first_name;
	   	
	   }
	   
	   public void setHrFirstName(String first_name) {
	   	
	   	  this.first_name = first_name;
	   	
	   }
	   
	   public String getHrMiddleName() {
		   	
	   	  return middle_name;
	   	
	   }
	   
	   public void setHrMiddleName(String middle_name) {
	   	
	   	  this.middle_name = middle_name;
	   	
	   }
	   
	   
	   public String getHrLastName() {
		   	
	   	  return last_name;
	   	
	   }
	   
	   public void setHrLastName(String last_name) {
	   	
	   	  this.last_name = last_name;
	   	
	   }
	   
	 
	   
	   public Integer getHrDeployedBranch() {
		   	
	   	  return deployed_at_branch;
	   	
	   }
	   
	   public void setHrDeployedBranch(Integer deployed_at_branch) {
	   	
	   	  this.deployed_at_branch = deployed_at_branch;
	   	
	   }
	   
	   
	   public String getHrManagingBranch() {
		   	
	   	  return managing_branch;
	   	
	   }
	   
	   public void setHrManagingBranch(String managing_branch) {
	   	
	   	  this.managing_branch = managing_branch;
	   	
	   }
	   
	   
	   
	   public String getHrCurrentJobPosition() {
		   	
	   	  return current_job_position;
	   	
	   }
	   
	   public void setHrCurrentJobPosition(String current_job_position) {
	   	
	   	  this.current_job_position = current_job_position;
	   	
	   }
	   
	   
	   
	   
   
} // ArModInvoiceLineItemDetails class
