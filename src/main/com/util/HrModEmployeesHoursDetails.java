package com.util;

public class HrModEmployeesHoursDetails {
	
	private String employee_id = null;
	private String payroll_period_id = null;
	private String regular_hrs = null;
	private String lh_hrs = null;
	private String lh_ot = null;
	private String sh_hrs = null;
	private String sh_ot = null;
	private String wd_hrs = null;
	private String wd_ot = null;
	private String ot = null;
	private String ut = null;
	private String nd = null;
   

   public HrModEmployeesHoursDetails() { ; }

	   public String getHrEmployeeID() {
		   	
	   	  return employee_id;
	   	
	   }
	   
	   public void setHrEmployeeID(String employee_id) {
	   	
	   	  this.employee_id = employee_id;
	   	
	   }
	   
	   public String getHrPayrollPeriodID() {
		   	
	   	  return payroll_period_id;
	   	
	   }
	   
	   public void setHrPayrollPeriodID(String payroll_period_id) {
	   	
	   	  this.payroll_period_id = payroll_period_id;
	   	
	   }
	   
	   public String getHrRegularHours() {
		   	
	   	  return regular_hrs;
	   	
	   }
	   
	   public void setHrRegularHours(String regular_hrs) {
	   	
	   	  this.regular_hrs = regular_hrs;
	   	
	   }
	   
	   
	   public String getHrLegalHolidayHours() {
		   	
	   	  return lh_hrs;
	   	
	   }
	   
	   public void setHrLegalHolidayHours(String lh_hrs) {
	   	
	   	  this.lh_hrs = lh_hrs;
	   	
	   }
	   
	   
	   public String getHrLegalHolidayOTHours() {
		   	
	   	  return lh_ot;
	   	
	   }
	   
	   public void setHrLegalHolidayOTHours(String lh_ot) {
	   	
	   	  this.lh_ot = lh_ot;
	   	
	   }
	   
	   public String getHrSpecialHolidayHours() {
		   	
	   	  return sh_hrs;
	   	
	   }
	   
	   public void setHrSpecialHolidayHours(String sh_hrs) {
	   	
	   	  this.sh_hrs = sh_hrs;
	   	
	   }
	   
	   
	   public String getHrSpecialHolidayOTHours() {
		   	
	   	  return sh_ot;
	   	
	   }
	   
	   public void setHrSpecialHolidayOTHours(String sh_ot) {
	   	
	   	  this.sh_ot = sh_ot;
	   }
	   
	   
	   public String getHrWorkDayoffHours() {
		   	
	   	  return wd_hrs;
	   	
	   }
	   
	   public void setHrWorkDayoffHours(String wd_hrs) {
	   	
	   	  this.wd_hrs = wd_hrs;
	   	
	   }
	   
	   
	   public String getHrWorkDayoffOTHours() {
		   	
	   	  return wd_ot;
	   	
	   }
	   
	   public void setHrWorkDayoffOTHours(String wd_ot) {
	   	
	   	  this.wd_ot = wd_ot;
	   	
	   }
	   
	   
	   
	   public String getHrOverTimeHours() {
		   	
	   	  return ot;
	   	
	   }
	   
	   public void setHrOverTimeHours(String ot) {
	   	
	   	  this.ot = ot;
	   	
	   }


	   public String getHrUnderTimeHours() {
		   	
	   	  return ut;
	   	
	   }
	   
	   public void setHrUnderTimeHours(String ut) {
	   	
	   	  this.ut = ut;
	   	
	   }


	   public String getHrNightDifferentialHours() {
		   	
	   	  return nd;
	   	
	   }
	   
	   public void setHrNightDifferentialHours(String nd) {
	   	
	   	  this.nd = nd;
	   	
	   }
	   
	   
	   
	   
   
} // ArModInvoiceLineItemDetails class
