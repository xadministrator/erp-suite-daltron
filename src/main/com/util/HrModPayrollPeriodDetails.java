package com.util;

import java.util.Date;

public class HrModPayrollPeriodDetails {
	
	private Integer id = null;
	private String type = null;
	private String title = null;
	private String start_date;
	private String end_date;
	private String status = null;

   public HrModPayrollPeriodDetails() { ; }

	   public Integer getHrPayrollPeriodID() {
		   	
	   	  return id;
	   	
	   }
	   
	   public void setHrPayrollPeriodID(Integer id) {
	   	
	   	  this.id = id;
	   	
	   }
	   
	   public String getHrType() {
		   	
	   	  return type;
	   	
	   }
	   
	   public void setHrType(String type) {
	   	
	   	  this.type = type;
	   	
	   }
	   
	   public String getHrTitle() {
		   	
	   	  return title;
	   	
	   }
	   
	   public void setHrTitle(String title) {
	   	
	   	  this.title = title;
	   	
	   }
	   
	   public String getHrStartDate() {
		   	
	   	  return start_date;
	   	
	   }
	   
	   public void setHrStartDate(String start_date) {
	   	
	   	  this.start_date = start_date;
	   	
	   }
	   
	   public String getHrEndDate() {
		   	
	   	  return end_date;
	   	
	   }
	   
	   public void setHrEndDate(String end_date) {
	   	
	   	  this.end_date = end_date;
	   	
	   }
	   
	   
	   public String getHrStatus() {
		   	
	   	  return status;
	   	
	   }
	   
	   public void setHrStatus(String status) {
	   	
	   	  this.status = status;
	   	
	   }
	   
	   
	   
   
} // HrModPayrollPeriodDetails class
