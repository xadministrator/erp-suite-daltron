package com.util;

import java.util.ArrayList;

public class InvModAssemblyTransferDetails extends InvAssemblyTransferDetails implements java.io.Serializable {

	private ArrayList ATR_ATL_LST;
    
	public InvModAssemblyTransferDetails() { ; }
	    
	public ArrayList getAtrAtlList() {
		
		return ATR_ATL_LST;
		
	}
	
	public void setAtrAtlList(ArrayList ATR_ATL_LST) {
		
		this.ATR_ATL_LST = ATR_ATL_LST;
		
	}

} // InvModAssemblyTransferDetails class
