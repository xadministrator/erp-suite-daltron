package com.util;

public class InvModAssemblyTransferLineDetails extends InvAssemblyTransferLineDetails implements java.io.Serializable {

	private double atlBolQtyRequired;
	private double atlBolQtyAssembled;
	private String atlBolIlIiName;
	private String atlBolIlLocName;
	private String atlBolIlIiUomName;
	private String atlBolBorDcmntNumber;
	
    public InvModAssemblyTransferLineDetails() { ; }
		
    public double getAtlBolQtyRequired() {
	
    	return atlBolQtyRequired;
    	
	}
    
	public void setAtlBolQtyRequired(double atlBolQtyRequired) {
		
		this.atlBolQtyRequired = atlBolQtyRequired;
		
	}
	
	public double getAtlBolQtyAssembled() {
		
		return atlBolQtyAssembled;
		
	}
	public void setAtlBolQtyAssembled(double atlBolQtyAssembled) {
		
		this.atlBolQtyAssembled = atlBolQtyAssembled;
		
	}
	
	public String getAtlBolIlIiName() {
		
		return atlBolIlIiName;
		
	}
	
	public void setAtlBolIlIiName(String atlBolIlIiName) {
	
		this.atlBolIlIiName = atlBolIlIiName;
		
	}
	
	public String getAtlBolIlLocName() {
	
		return atlBolIlLocName;
		
	}
	
	public void setAtlBolIlLocName(String atlBolIlLocName) {
	
		this.atlBolIlLocName = atlBolIlLocName;
		
	}
	
	public String getAtlBolIlIiUomName() {
	
		return atlBolIlIiUomName;
		
	}
	public void setAtlBolIlIiUomName(String atlBolIlIiUomName) {
		
		this.atlBolIlIiUomName = atlBolIlIiUomName;
		
	}
	
	public String getAtlBolBorDcmntNumber() {
		
		return atlBolBorDcmntNumber;
		
	}
	
	public void setAtlBolBorDcmntNumber(String atlBolBorDcmntNumber) {
	
		this.atlBolBorDcmntNumber = atlBolBorDcmntNumber;
		
	}
		
} // InvModAssemblyTransferLineDetails class
