package com.util;

public class InvModBillOfMaterialDetails extends InvBillOfMaterialDetails implements java.io.Serializable {

   private String BOM_II_NM;
   private String BOM_II_CTGRY;
   private String BOM_UOM_NM;
   private double BOM_UNT_CST;

   public InvModBillOfMaterialDetails () { ; }

   public String getBomIiItemName() {
   	
   	  return BOM_II_NM;
   	
   }
   
   public void setBomIiItemName(String BOM_II_NM) {
   	
   	  this.BOM_II_NM = BOM_II_NM;
   	
   }
   
   public String getBomIiItemCategory() {
	   	
   	  return BOM_II_CTGRY;
   	
   }
   
   public void setBomIiItemCategory(String BOM_II_CTGRY) {
   	
   	  this.BOM_II_CTGRY = BOM_II_CTGRY;
   	
   }
   
   public String getBomUomName() {
   	
   	  return BOM_UOM_NM;
   	
   }
   
   public void setBomUomName(String BOM_UOM_NM) {
   	
   	  this.BOM_UOM_NM = BOM_UOM_NM;
   	
   }
   
   public double getBomUnitCost() {
   	
   	  return BOM_UNT_CST;
   	
   }
   
   public void setBomUnitCost(double BOM_UNT_CST) {
   	
   	  this.BOM_UNT_CST = BOM_UNT_CST;
   	
   }
   
} // InvModBillOfMaterialDetails class   

