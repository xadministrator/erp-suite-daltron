package com.util;

import java.util.ArrayList;

public class InvModBuildOrderDetails extends InvBuildOrderDetails implements java.io.Serializable {

    private ArrayList borBolLst;
    private String borCustomerName;
    

    public InvModBuildOrderDetails() { ; }

	
	public ArrayList getBorBolList() {
		
		return borBolLst;
		
	}
	
	public void setBorBolList(ArrayList borBolLst) {
		
		this.borBolLst = borBolLst;
		
	}
	
	public String getBorCustomerName() {
		
		return borCustomerName;
	}
	
	public void setBorCustomerName(String borCustomerName) {
		
		this.borCustomerName = borCustomerName;
	}
	
	
	

} // InvModBuildOrderDetails class
