package com.util;

public class InvModBuildOrderLineDetails extends InvBuildOrderLineDetails implements java.io.Serializable {

    private String bolIlIiUomName;
    private String bolIlLocName;
    private String bolIlIiName;
    private String bolIlIiDescription;
    private String bolBorDocumentNumber;
    String BOL_MISC = null;
    
    //private double bolQuantityRequired = 0d;
    private double bolRemaining = 0d;
    private double bolUnitCost = 0d;
    private double bolTotalCost = 0d;
    private double bolTotalDiscount = 0d;
    private String bolLineNumber;
    private boolean bolIssue = false;
    
	public InvModBuildOrderLineDetails() { ; }
	
    public String getBolIlIiUomName() {
		return bolIlIiUomName;
	}
	
	public void setBolIlIiUomName(String bolIlIiUomName) {
		this.bolIlIiUomName = bolIlIiUomName;
	}
	
	public String getBolIlLocName() {
		return bolIlLocName;
	}
	
	public void setBolIlLocName(String bolIlLocName) {
		this.bolIlLocName = bolIlLocName;
	}
    
	public String getBolIlIiName() {
		return bolIlIiName;
	}
	
	public void setBolIlIiName(String bolIlIiName) {
		this.bolIlIiName = bolIlIiName;
	}
	
	public String getBolIlIiDescription() {
		return bolIlIiDescription;
	}
	
	public void setBolIlIiDescription(String bolIlIiDescription) {
		this.bolIlIiDescription = bolIlIiDescription;
	}
	
	public String getBolBorDocumentNumber() {
		return bolBorDocumentNumber;
	}
	public void setBolBorDocumentNumber(String bolBorDocumentNumber) {
		this.bolBorDocumentNumber = bolBorDocumentNumber;
	}

	public double getBolRemaining() {
        return bolRemaining;
    }
    
    public void setBolRemaining(double bolRemaining) {
        this.bolRemaining = bolRemaining;
    }
    
    public boolean getBolIssue() {
        return bolIssue;
    }
         
    public void setBolIssue(boolean bolIssue) {
        this.bolIssue = bolIssue;
    }
    
    /*public double getBolQuantityRequired() {
        return bolQuantityRequired;
    }
    
    public void setBolQuantityRequired(double bolQuantityRequired) {
        this.bolQuantityRequired = bolQuantityRequired;
    }*/

	public double getBolUnitCost() {
		return bolUnitCost;
	}
	
	public void setBolUnitCost(double bolUnitCost) {
		this.bolUnitCost = bolUnitCost;
	}

	public double getBolTotalCost() {
		return bolTotalCost;
	}
	
	public void setBolTotalCost(double bolTotalCost) {
		this.bolTotalCost = bolTotalCost;
	}

	public double getBolTotalDiscount() {
		return bolTotalDiscount;
	}
	
	public void setBolTotalDiscount(double bolTotalDiscount) {
		this.bolTotalDiscount = bolTotalDiscount;
	}
	
	public String getBolLineNumber() {
		return bolLineNumber;
	}
	
	public void setBolLineNumber(String bolLineNumber) {
		this.bolLineNumber = bolLineNumber;
	}
	
	public String getBolMisc() {

		return BOL_MISC;

	}

	public void setBolMisc(String BOL_MISC) {

		this.BOL_MISC = BOL_MISC;
		System.out.println("BOL_MISC : " + BOL_MISC);

	}
} // InvModBuildOrderLineDetails class
