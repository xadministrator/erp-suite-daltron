package com.util;

public class InvModBuildOrderStockDetails extends InvBuildOrderStockDetails implements java.io.Serializable {

    private String bosIiUomName;
    private String bosIiName;
    private String bosIiDescription;
    private double bosIiUnitCost;
    

    public InvModBuildOrderStockDetails() { ; }
	
    public String getBosIiUomName() {
		return bosIiUomName;
	}
	
	public void setBosIiUomName(String bosIiUomName) {
		this.bosIiUomName = bosIiUomName;
	}
    
	public String getBosIiName() {
		return bosIiName;
	}
	
	public void setBosIiName(String bosIiName) {
		this.bosIiName = bosIiName;
	}
	
	public String getBosIiDescription() {
		return bosIiDescription;
	}
	
	public void setBosIiDescription(String bosIiDescription) {
		this.bosIiDescription = bosIiDescription;
	}
	
	public double getBosIiUnitCost(){
		return bosIiUnitCost;
	}
	
	public void setBosIiUnitCost(double bosIiUnitCost){
		this.bosIiUnitCost = bosIiUnitCost;
	}
	
} // InvModBuildOrderStockDetails class
