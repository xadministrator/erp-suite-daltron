package com.util;

import java.util.ArrayList;

public class InvModBuildUnbuildAssemblyOrderDetails extends InvBuildUnbuildAssemblyDetails implements java.io.Serializable {

	
    private String buaCoaAccountNumber;
    private ArrayList buaBlList;
    private String BUA_CST_CSTMR_CODE;
    private String BUA_CST_NM;
    private String BUA_BB_NM;

    public InvModBuildUnbuildAssemblyOrderDetails() { ; }
    
    public String getBuaBbName() {
       	
    	   return BUA_BB_NM;
    	
    }
    
    public void setBuaBbName(String BUA_BB_NM) {
    	
    	   this.BUA_BB_NM = BUA_BB_NM;
    	
    }

	public String getBuaCoaAccountNumber() {
		
		return buaCoaAccountNumber;
		
	}
	
	public void setBuaCoaAccountNumber(String buaCoaAccountNumber) {
		
		this.buaCoaAccountNumber = buaCoaAccountNumber;
		
	}
	
	public ArrayList getBuaBlList() {
		
		return buaBlList;
		
	}
	
	public void setBuaBlList(ArrayList buaBlList) {
		
		this.buaBlList = buaBlList;
		
	}
	
	public String getBuaCstCustomerCode() {
	   	
   	   return BUA_CST_CSTMR_CODE;
   	
   }
   
   public void setBuaCstCustomerCode(String BUA_CST_CSTMR_CODE) {
   	
   	   this.BUA_CST_CSTMR_CODE = BUA_CST_CSTMR_CODE;
   	
   }
   
   public String getBuaCstName() {
   	
   	   return BUA_CST_NM;
   	
   }
   
   public void setBuaCstName(String BUA_CST_NM) {
   	
   	   this.BUA_CST_NM = BUA_CST_NM;
   	
   }

} // InvModBuildUnbuildAssemblyDetails class
