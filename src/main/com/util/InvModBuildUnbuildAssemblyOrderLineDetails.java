package com.util;

import java.util.ArrayList;

public class InvModBuildUnbuildAssemblyOrderLineDetails extends InvBuildUnbuildAssemblyLineDetails implements java.io.Serializable {

    private String blUomName;
    private String blLocName;
    private String blIiName;
    private String blIiDescription;
    private double blCstQuantity;
    private double blRemaining;
    private double blReceived;
    private short blLineNumber;
    
    byte BL_TRC_MSC;
    String BL_MISC = null;
    ArrayList blTagList;

    public InvModBuildUnbuildAssemblyOrderLineDetails() { ; }
	
    public byte getTraceMisc() {
		
 	   return BL_TRC_MSC;
 		
    }
 	
    public void setTraceMisc(byte BL_TRC_MSC) {
 		
 	   this.BL_TRC_MSC = BL_TRC_MSC;
 		
    }
    
	public double getBlCstQuantity() {
		return blCstQuantity;
	}
	
	public void setBlCstQuantity(double blCstQuantity) {
		this.blCstQuantity = blCstQuantity;
	}
	
	
	public double getBlRemaining() {
		return blRemaining;
	}
	
	public void setBlRemaining(double blRemaining) {
		this.blRemaining = blRemaining;
	}
	
	public String getBlIiName() {
		return blIiName;
	}
	
	public void setBlIiName(String blIiName) {
		this.blIiName = blIiName;
	}
	
	public String getBlIiDescription() {
		return blIiDescription;
	}
	
	public void setBlIiDescription(String blIiDescription) {
		this.blIiDescription = blIiDescription;
	}
	
	public short getBlLineNumber() {
		return blLineNumber;
	}
	
	public void setBlLineNumber(short blLineNumber) {
		this.blLineNumber = blLineNumber;
	}
	
	public String getBlLocName() {
		return blLocName;
	}
	
	public void setBlLocName(String blLocName) {
		this.blLocName = blLocName;
	}
	
	public String getBlUomName() {
		return blUomName;
	}

	public void setBlUomName(String blUomName) {
		this.blUomName = blUomName;
	}
	
	 public ArrayList getBlTagList() {
		   	
	   return blTagList;
	   	  
   }
	   
   public void setBlTagList(ArrayList blTagList) {
	   	
	   this.blTagList = blTagList;
	   	   
   }

	public String getBlMisc() {

		return BL_MISC;

	}

	public void setBlMisc(String BL_MISC) {

		this.BL_MISC = BL_MISC;
		System.out.println("BL_MISC : " + BL_MISC);

	}
	
	public double getBlReceived() {
		
		return blReceived;
	}
	
	public void setBlReceived(double blReceived) {
		
		this.blReceived = blReceived;
	}
	
} // InvModBuildUnbuildAssemblyLineDetails class
