package com.util;


public class InvModDistributionRecordDetails extends InvDistributionRecordDetails implements java.io.Serializable {

   private String DR_COA_ACCNT_NMBR;
   private String DR_COA_ACCNT_DESC;
   
   public InvModDistributionRecordDetails() { ; }

   public String getDrCoaAccountNumber() {
   	
   	  return DR_COA_ACCNT_NMBR;
   	
   }
   
   public void setDrCoaAccountNumber(String DR_COA_ACCNT_NMBR) {
   	
   	  this.DR_COA_ACCNT_NMBR = DR_COA_ACCNT_NMBR;
   	
   }
   
   public String getDrCoaAccountDescription() {
   	
   	  return DR_COA_ACCNT_DESC;
   	
   }
   
   public void setDrCoaAccountDescription(String DR_COA_ACCNT_DESC) {
   	
   	  this.DR_COA_ACCNT_DESC = DR_COA_ACCNT_DESC;
   	
   }

} // InvModDistributionRecordDetails class
