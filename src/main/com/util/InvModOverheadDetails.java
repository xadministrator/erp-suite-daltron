package com.util;

import java.util.ArrayList;

public class InvModOverheadDetails extends InvOverheadDetails implements java.io.Serializable {

    private ArrayList OH_OHL_LST;

    public InvModOverheadDetails() { ; }

	public ArrayList getOhOhlList() {
		
		return OH_OHL_LST;
		
	}
	
	public void setOhOhlList(ArrayList OH_OHL_LST) {
		
		this.OH_OHL_LST = OH_OHL_LST;
		
	}
	
} // InvModOverheadDetails class
