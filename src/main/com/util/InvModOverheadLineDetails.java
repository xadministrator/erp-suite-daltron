package com.util;

public class InvModOverheadLineDetails extends InvOverheadLineDetails implements java.io.Serializable {

	private String ohlOmlName;
    private String ohlUomName;    
    private double ohlAmount;
    private short ohlLineNumber;

    public InvModOverheadLineDetails() { ; }
	
	public String getOhlOmlName() {
		
		return ohlOmlName;
		
	}
	
	public void setOhlOmlName(String ohlOmlName) {
		
		this.ohlOmlName = ohlOmlName;
		
	}
	
	public String getOhlUomName() {
		
		return ohlUomName;
	}
	
	public void setOhlUomName(String ohlUomName) {
		
		this.ohlUomName = ohlUomName;
		
	}
	
	public double getOhlAmount() {
		
		return ohlAmount;
		
	}
	
	public void setOhlAmount(double ohlAmount) {
		
		this.ohlAmount = ohlAmount;
		
	}
	
	public short getOhlLineNumber() {
		
		return ohlLineNumber;
		
	}
	
	public void setOhlLineNumber(short ohlLineNumber) {
		
		this.ohlLineNumber = ohlLineNumber;
		
	}
	
} // InvModOverheadLineDetails class
