package com.util;


public class InvModOverheadMemoLineDetails extends InvOverheadMemoLineDetails implements java.io.Serializable {
	
	private String OML_UOM_NM;
	private String OML_COA_OVRHD_ACCNT;
	private String OML_COA_OVRHD_ACCNT_DESC;
	private String OML_COA_LBLTY_ACCNT;
	private String OML_COA_LBLTY_ACCNT_DESC;

	public InvModOverheadMemoLineDetails () { ; }
	
	public String getOmlUomName() {
		
		return OML_UOM_NM;
		
	}
	
	public void setOmlUomName(String OML_UOM_NM) {
		
		this.OML_UOM_NM = OML_UOM_NM;
		
	}
	
	public String getOmlCoaOverheadAccount() {
		
		return OML_COA_OVRHD_ACCNT;
		
	}
	
	public void setOmlCoaOverheadAccount(String OML_COA_OVRHD_ACCNT) {
		
		this.OML_COA_OVRHD_ACCNT = OML_COA_OVRHD_ACCNT;
		
	}
	
	public String getOmlCoaOverheadAccountDescription() {
		
		return OML_COA_OVRHD_ACCNT_DESC;
		
	}
	
	public void setOmlCoaOverheadAccountDescription(String OML_COA_OVRHD_ACCNT_DESC) {
		
		this.OML_COA_OVRHD_ACCNT_DESC = OML_COA_OVRHD_ACCNT_DESC;
		
	}
	
	public String getOmlCoaLiabilityAccount() {
		
		return OML_COA_LBLTY_ACCNT;
		
	}
	
	public void setOmlCoaLiabilityAccount(String OML_COA_LBLTY_ACCNT) {
		
		this.OML_COA_LBLTY_ACCNT = OML_COA_LBLTY_ACCNT;
		
	}
	
	public String getOmlCoaLiabilityAccountDescription() {
		
		return OML_COA_LBLTY_ACCNT_DESC;
		
	}
	
	public void setOmlCoaLiabilityAccountDescription(String OML_COA_LBLTY_ACCNT_DESC) {
		
		this.OML_COA_LBLTY_ACCNT_DESC = OML_COA_LBLTY_ACCNT_DESC;
		
	}
		
} // InvModOverheadMemoLineDetails class   
