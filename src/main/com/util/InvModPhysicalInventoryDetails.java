package com.util;

import java.util.ArrayList;

public class InvModPhysicalInventoryDetails extends InvPhysicalInventoryDetails implements java.io.Serializable {

	private String PI_LOC_NM;
	private ArrayList piPilList;
	private String PI_WSTG_ACCNT;
	private String PI_WSTG_ACCNT_DESC;
	private String PI_VRNC_ACCNT;
	private String PI_VRNC_ACCNT_DESC;

    public InvModPhysicalInventoryDetails() { ; }
    
    public String getPiLocName() {
    	
    	return PI_LOC_NM;
    	
    }
    
    public void setPiLocName(String PI_LOC_NM) {
    	
    	this.PI_LOC_NM = PI_LOC_NM;
    	
    }

	public ArrayList getPiPilList() {
		
		return piPilList;
		
	}
	
	public void setPiPilList(ArrayList piPilList) {
		
		this.piPilList = piPilList;
		
	}
	
	public void setPiWastageAccount(String PI_WSTG_ACCNT) {
		this.PI_WSTG_ACCNT = PI_WSTG_ACCNT;
	}
	
	public String getPiWastageAccount() {
		return PI_WSTG_ACCNT;
	}
	
	public void setPiWastageAccountDescription(String PI_WSTG_ACCNT_DESC) {
		this.PI_WSTG_ACCNT_DESC = PI_WSTG_ACCNT_DESC;
	}
	
	public String getPiWastageAccountDescription() {
		return PI_WSTG_ACCNT_DESC;
	}
	
	public void setPiVarianceAccount(String PI_VRNC_ACCNT) {
		this.PI_VRNC_ACCNT = PI_VRNC_ACCNT;
	}
	
	public String getPiVarianceAccount() {
		return PI_VRNC_ACCNT;
	}
	
	public void setPiVarianceAccountDescription(String PI_VRNC_ACCNT_DESC) {
		this.PI_VRNC_ACCNT_DESC = PI_VRNC_ACCNT_DESC;
	}
	
	public String getPiVarianceAccountDescription() {
		return PI_VRNC_ACCNT_DESC;
	}
	
} // InvModPhysicalInventoryDetails class
