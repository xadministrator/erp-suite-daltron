/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.util;

/**
 *
 * @author ASUS-OMEGA
 */
public class InvModPriceLevelDateDetails extends InvPriceLevelDateDetails implements java.io.Serializable{
    
    private String PD_LN = null;
    private Integer II_CODE = null;
    private String II_NM = null;
    
    
 
    public String getPdLine(){
        return PD_LN;
    }
    
    public void setPdLine(String PD_LN){
        this.PD_LN = PD_LN;
    }
    
    
 	public Integer getIiCode(){
        return II_CODE;
    }
    
    public void setIiCode(Integer II_CODE){
        this.II_CODE = II_CODE;
    }
    
    
    public String getIiName(){
        return II_NM;
    }
    
    public void setIiName(String II_NM){
        this.II_NM = II_NM;
    }
    
    
    
}
