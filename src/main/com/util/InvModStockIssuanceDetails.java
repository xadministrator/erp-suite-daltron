package com.util;

import java.util.ArrayList;

public class InvModStockIssuanceDetails extends InvStockIssuanceDetails implements java.io.Serializable {

	private Integer siSilBolCode;
    private String siSilBosBolBorDocumentNumber;
    private String siSililIiName;
    private String siSilIlLocationName;
    private String silIlIiUomName;    
    private ArrayList siSilList;

    public InvModStockIssuanceDetails() { ; }
    
    public Integer getSiSilBolCode() {
    	
    	return siSilBolCode;
    	
    }
    
    public void setSiSilBolCode(Integer siSilBolCode) {
    	
    	this.siSilBolCode = siSilBolCode;
    	
    }

	public String getSiSilBosBolBorDocumentNumber() {
		
		return siSilBosBolBorDocumentNumber;
		
	}
	
	public void setSiSilBosBolBorDocumentNumber(String siSilBosBolBorDocumentNumber) {
		
		this.siSilBosBolBorDocumentNumber = siSilBosBolBorDocumentNumber;
		
	}
	
	public String getSiSilIlIiName() {
		
		return siSililIiName;
		
	}
	
	public void setSiSilIlIiName(String siSililIiName) {
		
		this.siSililIiName = siSililIiName;
		
	}
	
	public String getSilIlIiUomName() {
		
		return silIlIiUomName;
		
	}
	
	public void setSilIlIiUomName(String silIlIiUomName) {
		
		this.silIlIiUomName = silIlIiUomName;
		
	}
	
	public String getSiSilIlLocationName() {
		
		return siSilIlLocationName;
		
	}
	
	public void setSiSilIlLocationName(String siSilIlLocationName) {
		
		this.siSilIlLocationName = siSilIlLocationName;
		
	}
	
	public ArrayList getSiSilList() {
		
		return siSilList;
		
	}
	
	public void setSiSilList(ArrayList siSilList) {
		
		this.siSilList = siSilList;
		
	}

} // InvModStockIssuanceDetails class
