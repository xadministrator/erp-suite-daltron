package com.util;

public class InvModStockIssuanceLineDetails extends InvStockIssuanceLineDetails implements java.io.Serializable {

    private String silIlLocationName;
    private String silIlIiName;
    private String silIlIiUomName;
    private Integer silBosCode;
    private double silBosQtyrequired;
    private double silBosQtyIssued;
    private short silLineNumber;
    private String silIlIiDescription;

    public InvModStockIssuanceLineDetails() { ; }
	
	public String getSilIlLocationName() {
		return silIlLocationName;
	}
	
	public void setSilIlLocationName(String silIlLocationName) {
		this.silIlLocationName = silIlLocationName;
	}
	
	public String getSilIlIiName() {
		return silIlIiName;
	}
	
	public void setSilIlIiName(String silIlIiName) {
		this.silIlIiName = silIlIiName;
	}
	
	public String getSilIlIiUomName() {
		return silIlIiUomName;		
	}
	
	public void setSilIlIiUomName(String silIlIiUomName) {
		this.silIlIiUomName = silIlIiUomName;
	}
	
	public Integer getSilBosCode() {
		return silBosCode;
	}
	
	public void setSilBosCode(Integer silBosCode) {
		this.silBosCode = silBosCode;
	}
	
	public double getSilBosQtyrequired() {
		return silBosQtyrequired;
	}
	
	public void setSilBosQtyrequired(double silBosQtyrequired) {
		this.silBosQtyrequired = silBosQtyrequired;
	}
	
	public double getSilBosQtyIssued() {
		return silBosQtyIssued;
	}
	
	public void setSilBosQtyIssued(double silBosQtyIssued) {
		this.silBosQtyIssued = silBosQtyIssued;
	}
	
	public short getSilLineNumber() {
		return silLineNumber;
	}
	
	public void setSilLineNumber(short silLineNumber) {
		this.silLineNumber = silLineNumber;
	}
	
	public String getSilIlIiDescription() {
		return silIlIiDescription;
	}
	
	public void setSilIlIiDescription(String silIlIiDescription) {
		this.silIlIiDescription = silIlIiDescription;
	}
	
} // InvModAdjustmentLineDetails class
