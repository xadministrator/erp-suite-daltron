
package com.util;

import java.util.ArrayList;

public class InvModStockTransferDetails extends InvStockTransferDetails implements java.io.Serializable {
    
    private ArrayList ST_STL_LST;
    
    public InvModStockTransferDetails() { ; }
    
    public ArrayList getStStlList() {        
        return ST_STL_LST;        
    }
    
    public void setStStlList(ArrayList ST_STL_LST) {        
        this.ST_STL_LST = ST_STL_LST;        
    }

} // InvModStockTransferDetails class
