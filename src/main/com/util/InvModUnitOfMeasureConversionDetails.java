package com.util;


public class InvModUnitOfMeasureConversionDetails extends InvUnitOfMeasureConversionDetails implements java.io.Serializable {
	
	// variables to hold umc list details : uom nm, uom symbol, uom class
	
	private String UOM_NM;
	private String UOM_SHRT_NM;
    private String UOM_AD_LV_CLSS;
    
    
    public InvModUnitOfMeasureConversionDetails() { ; }

    public String getUomName() {
    	
    	   return UOM_NM;
    	
    }  
    
    public void setUomName(String UOC_NM) {
    	
    	  this.UOM_NM = UOC_NM;
    	
    }
    
    
    public String getUomShortName() {
    	
    	   return UOM_SHRT_NM;
    	
    }  
    
    public void setUomShortName(String UOM_SHRT_NM) {
    	
    	  this.UOM_SHRT_NM = UOM_SHRT_NM;
    	
    }
    
    public String getUomAdLvClass() {
    	
    	   return UOM_AD_LV_CLSS;
    	
    }  
    
    public void setUomAdLvClass(String UOM_AD_LV_CLSS) {
    	
    	  this.UOM_AD_LV_CLSS = UOM_AD_LV_CLSS;
    	
    }
    
} // InvModUnitOfMeasureConversionDetails class
