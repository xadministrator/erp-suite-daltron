package com.util;

public class InvModUnitOfMeasureDetails extends InvUnitOfMeasureDetails implements java.io.Serializable {

    private boolean isDefault = false;

    public InvModUnitOfMeasureDetails() { ; }

	public boolean isDefault() {
		
		return isDefault;
		
	}
	
	public void setDefault(boolean isDefault) {
		
		this.isDefault = isDefault;
		
	}
	
}