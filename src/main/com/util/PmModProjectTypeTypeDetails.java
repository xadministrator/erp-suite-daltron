package com.util;

import java.util.ArrayList;


public class PmModProjectTypeTypeDetails extends PmProjectTypeTypeDetails implements java.io.Serializable {
	
	private String PTT_PRJ_NM;
	
	private ArrayList brPttList = new ArrayList();
	
	public PmModProjectTypeTypeDetails () { ; }
	
	public String getPttPrjName() {
		
		return PTT_PRJ_NM;
		
	}
	
	public void setPttPrjName(String PTT_PRJ_NM) {
		
		this.PTT_PRJ_NM = PTT_PRJ_NM;
		
	}
	
	
	
	public Object[] getBrPttList(){
		
		return brPttList.toArray();
		
	}
	
	public AdModBranchItemLocationDetails getBrPttListByIndex(int index){
		
		return ((AdModBranchItemLocationDetails)brPttList.get(index));
		
	}
	
	public int getBrPttListSize(){
		
		return(brPttList.size());
		
	}
	
	public void saveBrPttList(Object newBrPttList){
		
		brPttList.add(newBrPttList);   	  
		
	}
	
	
} // InvModItemLocationDetails class   
