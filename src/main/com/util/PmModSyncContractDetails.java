package com.util;

import java.util.Date;

public class PmModSyncContractDetails {
	
	private Integer id = null;
	private String client_id = null;
	private Double contract_price;

	public PmModSyncContractDetails() { ; }

	public Integer getPmContractID() {
	   	
	   	  return id;
	   	
	   }
	   
	   public void setPmContractID(Integer id) {
	   	
	   	  this.id = id;
	   	
	   }
	   
	   
	   
	   public String getPmClientID() {
		   	
	   	  return client_id;
	   	
	   }
	   
	   public void setPmClientID(String client_id) {
	   	
	   	  this.client_id = client_id;
	   	
	   }
	   
	   
	   public Double getPmContractPrice() {
		   	
	   	  return contract_price;
	   	
	   }
	   
	   public void setPmContractPrice(Double contract_price) {
	   	
	   	  this.contract_price = contract_price;
	   	
	   }
	   

} // HrModPayrollPeriodDetails class
