package com.util;

import java.util.Date;

public class PmModSyncContractTermDetails {
	
	private Integer id = null;
	private Integer contract_id = null;
	private String term_description = null;
	private Double value;

	public PmModSyncContractTermDetails() { ; }

	public Integer getPmContractTermID() {
	   	
	   	  return id;
	   	
	   }
	   
	   public void setPmContractTermID(Integer id) {
	   	
	   	  this.id = id;
	   	
	   }
	   
	   
	   public Integer getPmContractID() {
		   	
	   	  return contract_id;
	   	
	   }
	   
	   public void setPmContractID(Integer contract_id) {
	   	
	   	  this.contract_id = contract_id;
	   	
	   }
	   
	   
	   
	   public String getPmTermDescription() {
		   	
	   	  return term_description;
	   	
	   }
	   
	   public void setPmTermDescription(String term_description) {
	   	
	   	  this.term_description = term_description;
	   	
	   }
	   
	   
	   public Double getPmValue() {
		   	
	   	  return value;
	   	
	   }
	   
	   public void setPmValue(Double value) {
	   	
	   	  this.value = value;
	   	
	   }
	   

} // HrModPayrollPeriodDetails class
