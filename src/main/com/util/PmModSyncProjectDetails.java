package com.util;

import java.util.Date;

public class PmModSyncProjectDetails {
	
	private Integer id = null;
	private String project_code = null;
	private String project_name = null;
	private String client_id;
	private String status = null;
	
	
	public PmModSyncProjectDetails() { ; }

	   public Integer getPmProjectID() {
		   	
	   	  return id;
	   	
	   }
	   
	   public void setPmProjectID(Integer id) {
	   	
	   	  this.id = id;
	   	
	   }
	   
	   public String getPmProjectCode() {
		   	
	   	  return project_code;
	   	
	   }
	   
	   public void setPmProjectCode(String project_code) {
	   	
	   	  this.project_code = project_code;
	   	
	   }
	   
	   public String getPmProjectName() {
		   	
	   	  return project_name;
	   	
	   }
	   
	   public void setPmProjectName(String project_name) {
	   	
	   	  this.project_name = project_name;
	   	
	   }
	   
	   
	   public String getPmClientID() {
		   	
	   	  return client_id;
	   	
	   }
	   
	   public void setPmClientID(String client_id) {
	   	
	   	  this.client_id = client_id;
	   	
	   }
	   
	   
	   public String getPmStatus() {
		   	
	   	  return status;
	   	
	   }
	   
	   public void setPmStatus(String status) {
	   	
	   	  this.status = status;
	   	
	   }
	   

   
} // HrModPayrollPeriodDetails class
