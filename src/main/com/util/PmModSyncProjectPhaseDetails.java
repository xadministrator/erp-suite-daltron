package com.util;

import java.util.List;

public class PmModSyncProjectPhaseDetails {
	
	
	private Integer id = null;
	private Integer project_type_types_id = null;
	private Integer contract_term_id = null; 
	private String name = null;
	
   public Integer getPmProjectPhaseID() {
	   	
   	  return id;
   	
   }
   
   public void setPmProjectPhaseID(Integer id) {
   	
   	  this.id = id;
   	
   }
   
   public Integer getPmProjectTypeTypeID() {
	   	
   	  return project_type_types_id;
   	
   }
   
   public void setPmProjectTypeTypeID(Integer project_type_types_id) {
   	
   	  this.project_type_types_id = project_type_types_id;
   	
   }
   
   public Integer getPmContractTermID() {
	   	
	   	  return contract_term_id;
	   	
	   }
	   
	   public void setPmContractTermID(Integer contract_term_id) {
	   	
	   	  this.contract_term_id = contract_term_id;
	   	
	   }
   
   
   public String getPmProjectPhaseName() {
	   	
   	  return name;
   	
   }
   
   public void setPmProjectPhaseName(String name) {
   	
   	  this.name = name;
   	
   }
	      
	      
	 

   
} // PmModPhase class
