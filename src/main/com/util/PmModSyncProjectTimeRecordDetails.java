package com.util;

import java.util.Date;

public class PmModSyncProjectTimeRecordDetails {
	
	private Integer project_id = null;
	private Integer user_id = null;
	private Double reg_rate;
	private Double ot_rate;
	private Double reg_hrs;
	private Double ot_hrs;

	public PmModSyncProjectTimeRecordDetails() { ; }

	public Integer getPmProjectID() {
	   	
	   	  return project_id;
	   	
	   }
	   
	   public void setPmProjectID(Integer project_id) {
	   	
	   	  this.project_id = project_id;
	   	
	   }
	   
	   public Integer getPmUserID() {
		   	
	   	  return user_id;
	   	
	   }
	   
	   public void setPmUserID(Integer user_id) {
	   	
	   	  this.user_id = user_id;
	   	
	   }  
	   
	   
	   public Double getPmRegularRate() {
		   	
	   	  return reg_rate;
	   	
	   }
	   
	   public void setPmRegularRate(Double reg_rate) {
	   	
	   	  this.reg_rate = reg_rate;
	   	
	   }
	   
	   
	   public Double getPmOvertimeRate() {
		   	
	   	  return ot_rate;
	   	
	   }
	   
	   public void setPmOvertimeRate(Double ot_rate) {
	   	
	   	  this.ot_rate = ot_rate;
	   	
	   }
	   
	   
		
		public Double getPmRegularHours() {
		   	
	   	  return reg_hrs;
	   	
	   }
	   
	   public void setPmRegularHours(Double reg_hrs) {
	   	
	   	  this.reg_hrs = reg_hrs;
	   	
	   }
	   
		public Double getPmOvertimeHours() {
		   	
	   	  return ot_hrs;
	   	
	   }
	   
	   public void setPmOvertimeHours(Double ot_hrs) {
	   	
	   	  this.ot_hrs = ot_hrs;
	   	
	   }
	   
	   
	   

} // HrModPayrollPeriodDetails class
