package com.util;


public class PmModSyncProjectTypeDetails {
	
	private Integer id = null;
	private String code = null;
	private String value = null;

	public PmModSyncProjectTypeDetails() { ; }

	   public Integer getPmProjectTypeID() {
		   	
	   	  return id;
	   	
	   }
	   
	   public void setPmProjectTypeID(Integer id) {
	   	
	   	  this.id = id;
	   	
	   }
	   
	   public String getPmProjectTypeCode() {
		   	
	   	  return code;
	   	
	   }
	   
	   public void setPmProjectCode(String code) {
	   	
	   	  this.code = code;
	   	
	   }
	   
	   public String getPmProjectTypeValue() {
		   	
	   	  return value;
	   	
	   }
	   
	   public void setPmProjectTypeValue(String value) {
	   	
	   	  this.value = value;
	   	
	   }
	   
	   
	   
	   
   
} // PmModProjectTypeDetails class
