package com.util;


public class PmModSyncProjectTypeTypeDetails {
	
	private Integer id = null;
	private Integer project_id = null;
	private Integer project_type_id = null;
	private Integer contract_id = null;

	public PmModSyncProjectTypeTypeDetails() { ; }

	   public Integer getPmProjectTypeTypeID() {
		   	
	   	  return id;
	   	
	   }
	   
	   public void setPmProjectTypeTypeID(Integer id) {
	   	
	   	  this.id = id;
	   	
	   }
	   
	   public Integer getPmProjectID() {
		   	
	   	  return project_id;
	   	
	   }
	   
	   public void setPmProjectID(Integer project_id) {
	   	
	   	  this.project_id = project_id;
	   	
	   }
	   
	   
	   public Integer getPmProjectTypeID() {
		   	
	   	  return project_type_id;
	   	
	   }
	   
	   public void setPmProjectTypeID(Integer project_type_id) {
	   	
	   	  this.project_type_id = project_type_id;
	   	
	   }
	   
	   public Integer getPmContractID() {
		   	
	   	  return contract_id;
	   	
	   }
	   
	   public void setPmContractID(Integer contract_id) {
	   	
	   	  this.contract_id = contract_id;
	   	
	   }
	   
	   
	   
	   
   
} // PmModProjectTypeDetails class
