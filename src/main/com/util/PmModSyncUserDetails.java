package com.util;

import java.util.Date;

public class PmModSyncUserDetails {
	
	private Integer id = null;
	private String name = null;
	private String username = null;
	private String employee_number = null;
	private String position = null;
	private Double rate;
	private Double ot_rate;
	private String status = null;

	public PmModSyncUserDetails() { ; }

	public Integer getPmUserID() {
	   	
	   	  return id;
	   	
	   }
	   
	   public void setPmUserID(Integer id) {
	   	
	   	  this.id = id;
	   	
	   }
	   
	   
	   
	   public String getPmName() {
		   	
	   	  return name;
	   	
	   }
	   
	   public void setPmName(String name) {
	   	
	   	  this.name = name;
	   	
	   }
	   
	   public String getPmUserName() {
		   	
	   	  return username;
	   	
	   }
	   
	   public void setPmUserName(String username) {
	   	
	   	  this.username = username;
	   	
	   }
	   
	   public String getPmEmployeeNumber() {
		   	
	   	  return employee_number;
	   	
	   }
	   
	   public void setPmEmployeeNumber(String employee_number) {
	   	
	   	  this.employee_number = employee_number;
	   	
	   }
	   
	   
	   public String getPmPosition() {
		   	
	   	  return position;
	   	
	   }
	   
	   public void setPmPosition(String position) {
	   	
	   	  this.position = position;
	   	
	   }
	   
	   
	   public Double getPmRegularRate() {
		   	
	   	  return rate;
	   	
	   }
	   
	   public void setPmRegularRate(Double rate) {
	   	
	   	  this.rate = rate;
	   	
	   }
	   
	   public Double getPmOvertimeRate() {
		   	
	   	  return ot_rate;
	   	
	   }
	   
	   public void setPmOvertimeRate(Double ot_rate) {
	   	
	   	  this.ot_rate = ot_rate;
	   	
	   }
	   
	   public String getPmStatus() {
		   	
	   	  return status;
	   	
	   }
	   
	   public void setPmStatus(String status) {
	   	
	   	  this.status = status;
	   	
	   }
	   

} // HrModPayrollPeriodDetails class
