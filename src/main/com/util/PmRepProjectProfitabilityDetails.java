package com.util;

import java.util.Comparator;
import java.util.Date;

public class PmRepProjectProfitabilityDetails implements java.io.Serializable {

	private String PP_PRJ_NM; 
	private String PP_PRJ_DESC;
	
	private String PP_PRJ_TYP_NM;
	private String PP_PRJ_TYP_DESC;
	
	private String PP_PRJ_PHSE_NM;
	
	private String PP_ITM_NM; 
	private String PP_ITM_DESC;
	
	private Date PP_DT;
	private String PP_DOC_NMBR;
	private double PP_AMNT;
	
	private double PP_CTRCT_AMNT;

	public PmRepProjectProfitabilityDetails() { ; }
	
	
	
	public double getPpContractAmount() {
		
		return PP_CTRCT_AMNT;
	
	}
	
	public void setPpContractAmount(double PP_CTRCT_AMNT) {
	
		this.PP_CTRCT_AMNT = PP_CTRCT_AMNT;
	
	}
	
	public Date getPpDate() {
		
		return PP_DT;
	
	}
	
	public void setPpDate(Date PP_DT) {
		
		this.PP_DT = PP_DT;
	
	}
	
	
	public double getPpAmount() {
		
		return PP_AMNT;
	
	}
	
	public void setPpAmount(double PP_AMNT) {
	
		this.PP_AMNT = PP_AMNT;
	
	}
	
	public String getPpItemDescription() {
	
		return PP_ITM_DESC;
	
	}
	
	public void setPpItemDescription(String PP_ITM_DESC) {
	
		this.PP_ITM_DESC = PP_ITM_DESC;
	
	}
	
	
	public String getPpDocumentNumber() {
		
		return PP_DOC_NMBR;
	
	}
	
	public void setPpDocumentNumber(String PP_DOC_NMBR) {
	
		this.PP_DOC_NMBR = PP_DOC_NMBR;
	
	}
	
	public String getPpItemName() {
	
		return PP_ITM_NM;
	
	}
	
	public void setPpItemName(String PP_ITM_NM) {
	
		this.PP_ITM_NM = PP_ITM_NM;
	
	}
	
	public String getPpProjectName() {
		
		return PP_PRJ_NM;
	
	}
	
	public void setPpProjectName(String PP_PRJ_NM) {
	
		this.PP_PRJ_NM = PP_PRJ_NM;
	
	}
	
	
	public String getPpProjectDescription() {
	
		return PP_PRJ_DESC;
	
	}
	
	public void setPpProjectDescription(String PP_PRJ_DESC) {
	
		this.PP_PRJ_DESC = PP_PRJ_DESC;
	
	}
	
	
	public String getPpProjectTypeName() {
		
		return PP_PRJ_TYP_NM;
	
	}
	
	public void setPpProjectTypeName(String PP_PRJ_TYP_NM) {
	
		this.PP_PRJ_TYP_NM = PP_PRJ_TYP_NM;
	
	}
	

	
	public String getPpProjectTypeDescription() {
		
		return PP_PRJ_TYP_DESC;
	
	}
	
	public void setPpProjectTypeDescription(String PP_PRJ_TYP_DESC) {
	
		this.PP_PRJ_TYP_DESC = PP_PRJ_TYP_DESC;
	
	}
	
	
	public String getPpProjectPhaseName() {
		
		return PP_PRJ_PHSE_NM;
	
	}
	
	public void setPpProjectPhaseName(String PP_PRJ_PHSE_NM) {
	
		this.PP_PRJ_PHSE_NM = PP_PRJ_PHSE_NM;
	
	}
	
	
	
	public static Comparator ProjectProfitabilityComparator = new Comparator() {
		
		public int compare(Object PP, Object anotherPP) {
			
			String PP_PRJ_NM1 = ((PmRepProjectProfitabilityDetails) PP).getPpProjectName();
			String PP_PRJ_TYP_NM1 = ((PmRepProjectProfitabilityDetails) PP).getPpProjectTypeName();
			String PP_PRJ_PHSE_NM1 = ((PmRepProjectProfitabilityDetails) PP).getPpProjectPhaseName();
			String PP_ITM_NM1 = ((PmRepProjectProfitabilityDetails) PP).getPpItemName();
			Date PP_DT1 = ((PmRepProjectProfitabilityDetails) PP).getPpDate();
			
			String PP_PRJ_NM2 = ((PmRepProjectProfitabilityDetails) anotherPP).getPpProjectName();
			String PP_PRJ_TYP_NM2 = ((PmRepProjectProfitabilityDetails) anotherPP).getPpProjectTypeName();
			String PP_PRJ_PHSE_NM2 = ((PmRepProjectProfitabilityDetails) anotherPP).getPpProjectPhaseName();
			String PP_ITM_NM2 = ((PmRepProjectProfitabilityDetails) anotherPP).getPpItemName();
			Date PP_DT2 = ((PmRepProjectProfitabilityDetails) anotherPP).getPpDate();
			
			if (!(PP_PRJ_NM1.equals(PP_PRJ_NM2))) {
				
				return PP_PRJ_NM1.compareTo(PP_PRJ_NM2);
				
			} else if (!(PP_PRJ_TYP_NM1.equals(PP_PRJ_TYP_NM2))) {
				
				return PP_PRJ_TYP_NM1.compareTo(PP_PRJ_TYP_NM2);
				
			} else if (!(PP_PRJ_PHSE_NM1.equals(PP_PRJ_PHSE_NM2))) {
				
				return PP_PRJ_PHSE_NM1.compareTo(PP_PRJ_PHSE_NM2);
			
			} else {
				
				return PP_ITM_NM1.compareTo(PP_ITM_NM2);

			} /*else  {

				return PP_DT1.compareTo(PP_DT2);
			}*/
		}
	};
	
	
	
	
} // InvRepCostOfSaleDetails class
