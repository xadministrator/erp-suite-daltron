package com.struts.ad.agingbucket;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdAgingBucketController;
import com.ejb.txn.AdAgingBucketControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdAgingBucketDetails;

public final class AdAgingBucketAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdAgingBucketAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdAgingBucketForm actionForm = (AdAgingBucketForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_AGING_BUCKET_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adAgingBucket");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdAgingBucketController EJB
*******************************************************/

         AdAgingBucketControllerHome homeAB = null;
         AdAgingBucketController ejbAB = null;

         try {

            homeAB = (AdAgingBucketControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdAgingBucketControllerEJB", AdAgingBucketControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdAgingBucketAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAB = homeAB.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdAgingBucketAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- AD AB Show Details Action --
*******************************************************/

      if (request.getParameter("showDetailsButton") != null) {
      		      	
	     actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	     return(mapping.findForward("adAgingBucket"));
	     
/*******************************************************
   -- AD AB Hide Details Action --
*******************************************************/	     
	     
	  } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	     actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	     return(mapping.findForward("adAgingBucket"));         
         
         
/*******************************************************
   -- Ad AB Save Action --
*******************************************************/

      } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdAgingBucketDetails details = new AdAgingBucketDetails();
            details.setAbName(actionForm.getAgingBucketName());
            details.setAbDescription(actionForm.getAgingBucketDescription());            
            details.setAbType(actionForm.getAgingBucketType());
            details.setAbEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            try {
            	
            	ejbAB.addAdAbEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("agingBucket.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAgingBucketAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad AB Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad AB Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdAgingBucketList adABList =
	            actionForm.getAdABByIndex(actionForm.getRowSelected());
                        
            AdAgingBucketDetails details = new AdAgingBucketDetails();
            details.setAbCode(adABList.getAgingBucketCode());
            details.setAbName(actionForm.getAgingBucketName());
            details.setAbDescription(actionForm.getAgingBucketDescription());            
            details.setAbType(actionForm.getAgingBucketType());
            details.setAbEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            try {
            	
            	ejbAB.updateAdAbEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("agingBucket.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAgingBucketAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad AB Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad AB Edit Action --
*******************************************************/

         } else if (request.getParameter("adABList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdABRow(actionForm.getRowSelected());
            
            return mapping.findForward("adAgingBucket");
            
/*******************************************************
   -- Ad AB Aging Bucket Values Action --
*******************************************************/

         } else if (request.getParameter("adABList[" +
            actionForm.getRowSelected() + "].agingBucketValuesButton") != null){
            
            AdAgingBucketList adABList =
	            actionForm.getAdABByIndex(actionForm.getRowSelected());

	        String path = "/adAgingBucketValue.do?forward=1" +
	           "&agingBucketCode=" + adABList.getAgingBucketCode() + 
	           "&agingBucketName=" +  adABList.getAgingBucketName() + 
	           "&agingBucketDescription=" + adABList.getAgingBucketDescription() +
	           "&agingBucketType=" + adABList.getAgingBucketType() + 
	           "&enable=" + adABList.getEnable(); 
	        
	        return(new ActionForward(path));            
            
/*******************************************************
   -- Ad AB Delete Action --
*******************************************************/

         } else if (request.getParameter("adABList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdAgingBucketList adABList =
	            actionForm.getAdABByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbAB.deleteAdAbEntry(adABList.getAgingBucketCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("agingBucket.error.deleteAgingBucketAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("agingBucket.error.agingBucketAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdAgingBucketAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad AB Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adAgingBucket");

            }
            
            ArrayList list = null;
            Iterator i = null;

            actionForm.clearAdABList(); 
                      	                        	                          	            	
	        try {	        
	           
	           list = ejbAB.getAdAbAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
    	            			            	
	              AdAgingBucketDetails details = (AdAgingBucketDetails)i.next();
	            	
	              AdAgingBucketList adABList = new AdAgingBucketList(actionForm,
	            	    details.getAbCode(),
	            	    details.getAbName(),
	            	    details.getAbDescription(),
	            	    details.getAbType(),
	            	    Common.convertByteToBoolean(details.getAbEnable()));
	            	 	            	    
	              actionForm.saveAdABList(adABList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
            
           if (log.isInfoEnabled()) {

              log.info("EJBException caught in AdAgingBucketAction.execute(): " + ex.getMessage() +
              " session: " + session.getId());
              return mapping.findForward("cmnErrorPage"); 
              
           }

        }
                  

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	            actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adAgingBucket"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdAgingBucketAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}