package com.struts.ad.agingbucket;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdAgingBucketForm extends ActionForm implements Serializable {

   private Integer agingBucketCode = null;
   private String agingBucketName = null;
   private String agingBucketDescription = null;
   private String agingBucketType = null;
   private ArrayList agingBucketTypeList = new ArrayList();
   private String tableType = null;
   private boolean enable = false;

   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;
   private String pageState = new String();
   private ArrayList adABList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdAgingBucketList getAdABByIndex(int index) {

      return((AdAgingBucketList)adABList.get(index));

   }

   public Object[] getAdABList() {

      return adABList.toArray();

   }

   public int getAdABListSize() {

      return adABList.size();

   }

   public void saveAdABList(Object newAdABList) {

      adABList.add(newAdABList);

   }

   public void clearAdABList() {
      adABList.clear();
   }

   public void setRowSelected(Object selectedAdABList, boolean isEdit) {

      this.rowSelected = adABList.indexOf(selectedAdABList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdABRow(int rowSelected) {
   	
       this.agingBucketName = ((AdAgingBucketList)adABList.get(rowSelected)).getAgingBucketName();
       this.agingBucketDescription = ((AdAgingBucketList)adABList.get(rowSelected)).getAgingBucketDescription();
       this.agingBucketType = ((AdAgingBucketList)adABList.get(rowSelected)).getAgingBucketType();
       this.enable = ((AdAgingBucketList)adABList.get(rowSelected)).getEnable();
       
   }

   public void updateAdABRow(int rowSelected, Object newAdABList) {

      adABList.set(rowSelected, newAdABList);

   }

   public void deleteAdABList(int rowSelected) {

      adABList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
      this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }		   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getAgingBucketCode() {
   	
   	  return agingBucketCode;
   	
   }
   
   public void setAgingBucketCode(Integer agingBucketCode) {
   	
   	  this.agingBucketCode = agingBucketCode;
   	
   }

   public String getAgingBucketName() {

      return agingBucketName;

   }

   public void setAgingBucketName(String agingBucketName) {

      this.agingBucketName = agingBucketName;

   }
   
   public String getAgingBucketDescription() {

      return agingBucketDescription;

   }

   public void setAgingBucketDescription(String agingBucketDescription) {

      this.agingBucketDescription = agingBucketDescription;

   }

   public String getAgingBucketType() {

      return agingBucketType;

   }

   public void setAgingBucketType(String agingBucketType) {

      this.agingBucketType = agingBucketType;

   }

   public ArrayList getAgingBucketTypeList() {
   	
   	   return agingBucketTypeList;
   	
   }
   
   public String getTableType() {
    	
       return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }         
   
   public boolean getEnable() {

      return enable;

   }

   public void setEnable(boolean enable) {

      this.enable = enable;

   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      agingBucketName = null; 
      agingBucketDescription = null;
      agingBucketTypeList.clear();
      agingBucketTypeList.add(Constants.AD_AB_4_BUCKET_AGING);
      agingBucketTypeList.add(Constants.AD_AB_7_BUCKET_AGING);
      agingBucketTypeList.add(Constants.AD_AB_CREDIT_SNAPSHOT);
      agingBucketTypeList.add(Constants.AD_AB_STATEMENT_AGING);
      agingBucketType = Constants.AD_AB_4_BUCKET_AGING;      
      enable = false;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
      showDetailsButton = null;
      hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(agingBucketName)) {

            errors.add("agingBucketName",
               new ActionMessage("agingBucket.error.agingBucketNameRequired"));

         }

         if (Common.validateRequired(agingBucketType)) {

            errors.add("agingBucketType",
               new ActionMessage("agingBucket.error.agingBucketTypeRequired"));

         }     

      }
         
      return errors;

   }
}