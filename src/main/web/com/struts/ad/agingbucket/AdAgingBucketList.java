package com.struts.ad.agingbucket;

import java.io.Serializable;

public class AdAgingBucketList implements Serializable {

   private Integer agingBucketCode = null;
   private String agingBucketName = null;
   private String agingBucketDescription = null;
   private String agingBucketType = null;
   private boolean enable = false;

   private String editButton = null;
   private String deleteButton = null;
   private String agingBucketValuesButton = null;
    
   private AdAgingBucketForm parentBean;
    
   public AdAgingBucketList(AdAgingBucketForm parentBean,
      Integer agingBucketCode,
      String agingBucketName,
      String agingBucketDescription,
      String agingBucketType,
      boolean enable) {

      this.parentBean = parentBean;
      this.agingBucketCode = agingBucketCode;
      this.agingBucketName = agingBucketName;
      this.agingBucketDescription = agingBucketDescription;
      this.agingBucketType = agingBucketType;
      this.enable = enable;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   
   
   public void setAgingBucketValuesButton(String agingBucketValuesButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }

   public Integer getAgingBucketCode() {

      return agingBucketCode;

   }
   
   public String getAgingBucketName() {
       
       return agingBucketName;
       
   }

   public String getAgingBucketDescription() {

      return agingBucketDescription;

   }

   public String getAgingBucketType() {

      return agingBucketType;

   }
   
   public boolean getEnable() {

      return enable;

   }

}