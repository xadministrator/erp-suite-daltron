package com.struts.ad.agingbucketvalue;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdAgingBucketValueController;
import com.ejb.txn.AdAgingBucketValueControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdAgingBucketDetails;
import com.util.AdAgingBucketValueDetails;

public final class AdAgingBucketValueAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdAgingBucketValueAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdAgingBucketValueForm actionForm = (AdAgingBucketValueForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_AGING_BUCKET_VALUE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adAgingBucketValue");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdAgingBucketValueController EJB
*******************************************************/

         AdAgingBucketValueControllerHome homeAV = null;
         AdAgingBucketValueController ejbAV = null;

         try {

            homeAV = (AdAgingBucketValueControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdAgingBucketValueControllerEJB", AdAgingBucketValueControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdAgingBucketValueAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAV = homeAV.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdAgingBucketValueAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad AV Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	       actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	       return(mapping.findForward("adAgingBucketValue"));
	     
/*******************************************************
   -- Ad AV Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	       actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	       return(mapping.findForward("adAgingBucketValue"));         
         
/*******************************************************
   -- Ad AV Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdAgingBucketValueDetails details = new AdAgingBucketValueDetails();
            details.setAvSequenceNumber(Common.convertStringToShort(actionForm.getSequenceNumber()));
            details.setAvType(actionForm.getType());
            details.setAvDaysFrom(Common.convertStringToLong(actionForm.getDaysFrom()));
            details.setAvDaysTo(Common.convertStringToLong(actionForm.getDaysTo()));
            details.setAvDescription(actionForm.getDescription());
            
            try {
            	
            	ejbAV.addAdAvEntry(details, actionForm.getAgingBucketCode(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("agingBucketValue.error.segmentNumberAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAgingBucketValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad AV Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad AV Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdAgingBucketValueList adAVList =
	            actionForm.getAdAVByIndex(actionForm.getRowSelected());
                        
            AdAgingBucketValueDetails details = new AdAgingBucketValueDetails();
            details.setAvCode(adAVList.getAgingBucketValueCode());
            details.setAvSequenceNumber(Common.convertStringToShort(actionForm.getSequenceNumber()));
            details.setAvType(actionForm.getType());
            details.setAvDaysFrom(Common.convertStringToLong(actionForm.getDaysFrom()));
            details.setAvDaysTo(Common.convertStringToLong(actionForm.getDaysTo()));
            details.setAvDescription(actionForm.getDescription());
            
            try {
            	
            	ejbAV.updateAdAvEntry(details, actionForm.getAgingBucketCode(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("agingBucketValue.error.segmentNumberAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAgingBucketValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad AV Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad AV Edit Action --
*******************************************************/

         } else if (request.getParameter("adAVList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdAVRow(actionForm.getRowSelected());
            
            return mapping.findForward("adAgingBucketValue");        
            
/*******************************************************
   -- Ad AV Delete Action --
*******************************************************/

         } else if (request.getParameter("adAVList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdAgingBucketValueList adAVList =
	            actionForm.getAdAVByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbAV.deleteAdAvEntry(adAVList.getAgingBucketValueCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("agingBucketValue.error.agingBucketValueAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdAgingBucketValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad AV Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adAgingBucketValue");

            }
                                  	                        	                          	            	
            AdAgingBucketDetails abDetails = null;

	        if (request.getParameter("forward") != null) {

	            abDetails = ejbAV.getAdAbByAbCode(new Integer(request.getParameter("agingBucketCode")), user.getCmpCode());        		
	    	    actionForm.setAgingBucketCode(new Integer(request.getParameter("agingBucketCode")));

	        } else {
	     		
	            abDetails = ejbAV.getAdAbByAbCode(actionForm.getAgingBucketCode(), user.getCmpCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setAgingBucketCode(actionForm.getAgingBucketCode());
            actionForm.setAgingBucketName(abDetails.getAbName());
            actionForm.setAgingBucketDescription(abDetails.getAbDescription());                        	
            actionForm.setAgingBucketType(abDetails.getAbType());        		        		
    	    actionForm.setEnable(Common.convertByteToBoolean(abDetails.getAbEnable()));

            ArrayList list = null;
            Iterator i = null;
            
            

	        try {
	        	
	        	actionForm.clearAdAVList();
	        		        		    	
	           list = ejbAV.getAdAvByAbCode(actionForm.getAgingBucketCode(), user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdAgingBucketValueDetails details = (AdAgingBucketValueDetails)i.next();
	            	
	              AdAgingBucketValueList adAVList = new AdAgingBucketValueList(actionForm,
	            	    details.getAvCode(),
	            	    Common.convertShortToString(details.getAvSequenceNumber()),
	            	    details.getAvType(),
	            	    Common.convertLongToString(details.getAvDaysFrom()),
	            	    Common.convertLongToString(details.getAvDaysTo()),
	            	    details.getAvDescription());
	            	 	            	    
	              actionForm.saveAdAVList(adAVList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAgingBucketValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adAgingBucketValue"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdAgingBucketValueAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}