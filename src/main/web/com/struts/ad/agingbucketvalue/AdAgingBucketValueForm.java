package com.struts.ad.agingbucketvalue;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdAgingBucketValueForm extends ActionForm implements Serializable {

   private Integer agingBucketCode = null;
   private String agingBucketName = null;
   private String agingBucketDescription = null;
   private String agingBucketType = null;
   private boolean enable = false;
   private String sequenceNumber = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String daysFrom = null; 
   private String daysTo = null; 
   private String description = null;
   private String tableType = null;
      
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;   
   private String pageState = new String();
   private ArrayList adAVList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdAgingBucketValueList getAdAVByIndex(int index) {

      return((AdAgingBucketValueList)adAVList.get(index));

   }

   public Object[] getAdAVList() {

      return adAVList.toArray();

   }

   public int getAdAVListSize() {

      return adAVList.size();

   }

   public void saveAdAVList(Object newAdAVList) {

      adAVList.add(newAdAVList);

   }

   public void clearAdAVList() {
      adAVList.clear();
   }

   public void setRowSelected(Object selectedAdAVList, boolean isEdit) {

      this.rowSelected = adAVList.indexOf(selectedAdAVList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdAVRow(int rowSelected) {
   	
       this.sequenceNumber = ((AdAgingBucketValueList)adAVList.get(rowSelected)).getSequenceNumber();
       this.type = ((AdAgingBucketValueList)adAVList.get(rowSelected)).getType();
       this.daysFrom = ((AdAgingBucketValueList)adAVList.get(rowSelected)).getDaysFrom();
       this.daysTo = ((AdAgingBucketValueList)adAVList.get(rowSelected)).getDaysTo();
       this.description = ((AdAgingBucketValueList)adAVList.get(rowSelected)).getDescription();
       
   }

   public void updateAdAVRow(int rowSelected, Object newAdAVList) {

      adAVList.set(rowSelected, newAdAVList);

   }

   public void deleteAdAVList(int rowSelected) {

      adAVList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getAgingBucketCode() {
   	
   	  return agingBucketCode;
   	
   }
   
   public void setAgingBucketCode(Integer agingBucketCode) {
   	
   	  this.agingBucketCode = agingBucketCode;
   	
   }
   
   public String getAgingBucketName() {

      return agingBucketName;

   }

   public void setAgingBucketName(String agingBucketName) {

      this.agingBucketName = agingBucketName;

   }
   
   public String getAgingBucketDescription() {

      return agingBucketDescription;

   }

   public void setAgingBucketDescription(String agingBucketDescription) {

      this.agingBucketDescription = agingBucketDescription;

   }

   public String getAgingBucketType() {

      return agingBucketType;

   }

   public void setAgingBucketType(String agingBucketType) {

      this.agingBucketType = agingBucketType;

   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public boolean getEnable() {

      return enable;

   }

   public void setEnable(boolean enable) {

      this.enable = enable;

   }      

   public String getSequenceNumber() {

      return sequenceNumber;

   }

   public void setSequenceNumber(String sequenceNumber) {

      this.sequenceNumber = sequenceNumber;

   }
   
   public String getType() {

      return type;

   }

   public void setType(String type) {

      this.type = type;

   }

   public ArrayList getTypeList() {
   	
   	   return typeList;
   	
   }

   public String getDaysFrom() {

      return daysFrom;

   }

   public void setDaysFrom(String daysFrom) {

      this.daysFrom = daysFrom;

   }

   public String getDaysTo() {

      return daysTo;

   }

   public void setDaysTo(String daysTo) {

      this.daysTo = daysTo;

   }
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }   
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      sequenceNumber = null; 
      typeList.clear();
      typeList.add(Constants.AD_AV_CURRENT);
      typeList.add(Constants.AD_AV_PAST_DUE);
      typeList.add(Constants.AD_AV_DISPUTE_ONLY);
      typeList.add(Constants.AD_AV_PENDING_ADJUSTMENT_ONLY);
      typeList.add(Constants.AD_AV_DISPUTE_AND_PENDING_ADJUSTMENTS);
      typeList.add(Constants.AD_AV_FUTURE);
      type = Constants.AD_AV_CURRENT; 
      daysFrom = null;
      daysTo = null;
      description = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(sequenceNumber)) {

            errors.add("sequenceNumber",
               new ActionMessage("agingBucketValue.error.sequenceNumberRequired"));

         }

         if (!Common.validateNumberFormat(sequenceNumber)) {

            errors.add("sequenceNumber",
               new ActionMessage("agingBucketValue.error.sequenceNumberInvalid"));

         } 

         if (Common.validateRequired(type)) {

            errors.add("type",
               new ActionMessage("agingBucketValue.error.typeRequired"));

         }
         
         if (Common.validateRequired(daysFrom)) {

            errors.add("daysFrom",
               new ActionMessage("agingBucketValue.error.daysFromRequired"));

         }         
                          
         
         if (!Common.validateNumberFormat(daysFrom)) {

            errors.add("daysFrom",
               new ActionMessage("agingBucketValue.error.daysFromInvalid"));

         }         
         
         if (Common.validateRequired(daysTo)) {

            errors.add("daysTo",
               new ActionMessage("agingBucketValue.error.daysToRequired"));

         }         
                          
         
         if (!Common.validateNumberFormat(daysTo)) {

            errors.add("daysTo",
               new ActionMessage("agingBucketValue.error.daysToInvalid"));

         }
         
         if (Common.validateRequired(description)) {

            errors.add("description",
               new ActionMessage("agingBucketValue.error.descriptionRequired"));

         }                                    

      }
         
      return errors;

   }
}