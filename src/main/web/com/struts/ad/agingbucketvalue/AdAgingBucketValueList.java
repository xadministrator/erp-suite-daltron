package com.struts.ad.agingbucketvalue;

import java.io.Serializable;

public class AdAgingBucketValueList implements Serializable {

   private Integer agingBucketValueCode = null;
   private String sequenceNumber = null;
   private String type = null;
   private String daysFrom = null;
   private String daysTo = null;
   private String description = null;

   private String editButton = null;
   private String deleteButton = null;
   private String discountsButton = null;
    
   private AdAgingBucketValueForm parentBean;
    
   public AdAgingBucketValueList(AdAgingBucketValueForm parentBean,
      Integer agingBucketValueCode,
      String sequenceNumber,
      String type,
      String daysFrom,
      String daysTo,
      String description) {

      this.parentBean = parentBean;
      this.agingBucketValueCode = agingBucketValueCode;
      this.sequenceNumber = sequenceNumber;
      this.type = type;
      this.daysFrom = daysFrom;
      this.daysTo = daysTo;
      this.description = description;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   

   public Integer getAgingBucketValueCode() {

      return agingBucketValueCode;

   }
   
   public String getSequenceNumber() {
       
       return sequenceNumber;
       
   }

   public String getType() {

      return type;

   }

   public String getDaysFrom() {

      return daysFrom;

   }

   public String getDaysTo() {

      return daysTo;

   }
   
   public String getDescription() {

      return description;

   }   
   
}