package com.struts.ad.amountlimit;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdAmountLimitController;
import com.ejb.txn.AdAmountLimitControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdAmountLimitDetails;
import com.util.AdApprovalDocumentDetails;
import com.util.AdModApprovalCoaLineDetails;

public final class AdAmountLimitAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdAmountLimitAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdAmountLimitForm actionForm = (AdAmountLimitForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_AMOUNT_LIMIT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adAmountLimit");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdAmountLimitController EJB
*******************************************************/

         AdAmountLimitControllerHome homeCAL = null;
         AdAmountLimitController ejbCAL = null;

         try {

            homeCAL = (AdAmountLimitControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdAmountLimitControllerEJB", AdAmountLimitControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdAmountLimitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCAL = homeCAL.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdAmountLimitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   Call AdAmountLimitController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         
         try {
         	
            precisionUnit = ejbCAL.getGlFcPrecisionUnit(user.getCmpCode());       
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in AdAmountLimitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
         
/*******************************************************
   -- Ad CAL Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	       actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	       return(mapping.findForward("adAmountLimit"));
	     
/*******************************************************
   -- Ad CAL Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	       actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	       return(mapping.findForward("adAmountLimit"));         
         
/*******************************************************
   -- Ad CAL Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdAmountLimitDetails details = new AdAmountLimitDetails();
            details.setCalDept(actionForm.getDepartment());
            details.setCalAmountLimit(Common.convertStringMoneyToDouble(actionForm.getAmountLimit(), precisionUnit));
            details.setCalAndOr(actionForm.getAndOr());
            
            try {
            	
            	ejbCAL.addAdCalEntry(details, actionForm.getApprovalDocumentCode(), actionForm.getApprovalCoaLineCode(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("amountLimit.error.amountLimitAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAmountLimitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad CAL Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Ad CAL Back Action --
*******************************************************/

         } else if (request.getParameter("approvalDocumentButton") != null) {
         	
			return(new ActionForward("/adApprovalDocument.do"));
			
         } else if (request.getParameter("approvalCoaLineButton") != null) {
         	
			return(new ActionForward("/adApprovalCoaLine.do"));			

/*******************************************************
   -- Ad CAL Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdAmountLimitList adCALList =
	            actionForm.getAdCALByIndex(actionForm.getRowSelected());
                        
            AdAmountLimitDetails details = new AdAmountLimitDetails();
            details.setCalCode(adCALList.getAmountLimitCode());
            details.setCalDept(actionForm.getDepartment());
            details.setCalAmountLimit(Common.convertStringMoneyToDouble(actionForm.getAmountLimit(), precisionUnit));
            details.setCalAndOr(actionForm.getAndOr());
            
            
            try {
            	
            	ejbCAL.updateAdCalEntry(details, actionForm.getApprovalDocumentCode(), actionForm.getApprovalCoaLineCode(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("amountLimit.error.amountLimitAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAmountLimitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad CAL Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad CAL Edit Action --
*******************************************************/

         } else if (request.getParameter("adCALList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdCALRow(actionForm.getRowSelected());
            
            return mapping.findForward("adAmountLimit");
            
/*******************************************************
   -- Ad CAL Approver Users Action --
*******************************************************/

         // forward to approval user module             	
            	
         } else if (request.getParameter("adCALList[" +
            actionForm.getRowSelected() + "].approvalUserButton") != null) {
            	
            AdAmountLimitList adCALList =
	            actionForm.getAdCALByIndex(actionForm.getRowSelected());

	  	    String approvalUserPath = "/adApprovalUser.do?forward=1" +
		           "&amountLimitCode=" + adCALList.getAmountLimitCode() +
		           "&department=" + adCALList.getDepartment() +
		           "&amountLimit=" + adCALList.getAmountLimit() +
		           "&andOr=" + adCALList.getAndOr();
            
			return(new ActionForward(approvalUserPath));    
            
/*******************************************************
   -- Ad CAL Delete Action --
*******************************************************/

         } else if (request.getParameter("adCALList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdAmountLimitList adCALList =
	            actionForm.getAdCALByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbCAL.deleteAdCalEntry(adCALList.getAmountLimitCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("amountLimit.error.amountLimitAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdAmountLimitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad CAL Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adAmountLimit");

            }
                                  	                        	                          	            	
            AdApprovalDocumentDetails adcDetails = null;            
            AdModApprovalCoaLineDetails mAclDetails = null;

	        if (request.getParameter("forward") != null) {

	        	if (request.getParameter("approvalDocumentCode") != null) {
	        		
	        		actionForm.setApprovalCoaLineCode(null);
                   
		            adcDetails = ejbCAL.getAdAdcByAdcCode(new Integer(request.getParameter("approvalDocumentCode")), user.getCmpCode());        		
		    	    actionForm.setApprovalDocumentCode(new Integer(request.getParameter("approvalDocumentCode")));
		    	    
		    	    actionForm.setApprovalDocumentCode(actionForm.getApprovalDocumentCode());
		            actionForm.setType(adcDetails.getAdcType());
		    	    
		    	    
		    	} else {
		    		
		    		actionForm.setApprovalDocumentCode(null);
		    		
		            mAclDetails = ejbCAL.getAdAclByAclCode(new Integer(request.getParameter("approvalCoaLineCode")), user.getCmpCode());        		
		    	    actionForm.setApprovalCoaLineCode(new Integer(request.getParameter("approvalCoaLineCode")));		    		
		    	    
		        	actionForm.setApprovalCoaLineCode(actionForm.getApprovalCoaLineCode());
		        	actionForm.setAccountNumber(mAclDetails.getAclCoaAccountNumber());
		        	actionForm.setAccountDescription(mAclDetails.getAclCoaDescription());		    	    
		    	    
		    	}

	        } else {
	        	
	        	if (request.getParameter("approvalDocumentCode") != null || actionForm.getApprovalDocumentCode() != null) {
	        		
	        		actionForm.setApprovalCoaLineCode(null);
	     		
	            	adcDetails = ejbCAL.getAdAdcByAdcCode(actionForm.getApprovalDocumentCode(), user.getCmpCode());
	            	
		    	    actionForm.setApprovalDocumentCode(actionForm.getApprovalDocumentCode());
		            actionForm.setType(adcDetails.getAdcType());	    	    	            	
	            	
	            } else {
	            	
	            	actionForm.setApprovalDocumentCode(null);
	            	
	            	mAclDetails = ejbCAL.getAdAclByAclCode(actionForm.getApprovalCoaLineCode(), user.getCmpCode());
	            	
		        	actionForm.setApprovalCoaLineCode(actionForm.getApprovalCoaLineCode());
		        	actionForm.setAccountNumber(mAclDetails.getAclCoaAccountNumber());
		        	actionForm.setAccountDescription(mAclDetails.getAclCoaDescription());		    	    	            	
	            }
	            
            }	            
            
            ArrayList list = null;
            Iterator i = null;
                        
	        try {
	        	
	        	actionForm.clearAdCALList();
	        	
	        	if (actionForm.getApprovalDocumentCode() != null) {   

		            list = ejbCAL.getAdCalByAdcCode(actionForm.getApprovalDocumentCode(), user.getCmpCode()); 
		           
		            i = list.iterator();
		            
		            while(i.hasNext()) {
	        	            			            	
		                AdAmountLimitDetails details = (AdAmountLimitDetails)i.next();
		            	
		                AdAmountLimitList adCALList = new AdAmountLimitList(actionForm,
		            	    details.getCalCode(),
		            	    details.getCalDept(),
		            	    Common.convertDoubleToStringMoney(details.getCalAmountLimit(), precisionUnit),
		            	    details.getCalAndOr());
		            	 	            	    
		               	actionForm.saveAdCALList(adCALList);
		            	
		           	}
		           	
		        } else {

		            list = ejbCAL.getAdCalByAclCode(actionForm.getApprovalCoaLineCode(), user.getCmpCode()); 
		           
		            i = list.iterator();
		            
		            while(i.hasNext()) {
	        	            			            	
		                AdAmountLimitDetails details = (AdAmountLimitDetails)i.next();
		            	
		                AdAmountLimitList adCALList = new AdAmountLimitList(actionForm,
		            	    details.getCalCode(),
		            	    details.getCalDept(),
		            	    Common.convertDoubleToStringMoney(details.getCalAmountLimit(), precisionUnit),
		            	    details.getCalAndOr());
		            	 	            	    
		               	actionForm.saveAdCALList(adCALList);		        	
		               			        			        	
		            }
		            
                }
	        	
	        	
	        	

				   
			   actionForm.clearDepartmentList();
			   
			   list = ejbCAL.getAdLvDEPARTMENT(user.getCmpCode());
		       i = list.iterator();
		       
				if (list == null || list.size() == 0) {  		
					actionForm.setDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);
				} else {     		            		
					i = list.iterator();
					while (i.hasNext()) {
					    actionForm.setDepartmentList((String)i.next());
					}
				}
	        	
	        	
	        	
	        	
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAmountLimitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adAmountLimit"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdAmountLimitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}