package com.struts.ad.amountlimit;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdAmountLimitForm extends ActionForm implements Serializable {

   private Integer approvalDocumentCode = null;
   private Integer approvalCoaLineCode = null;
   private String type = null;
   private String accountNumber = null;
   private String accountDescription = null;
   
   private String department = null;
   private ArrayList departmentList = new ArrayList();
   
   private String amountLimit = null;
   private String andOr = null;
   private ArrayList andOrList = new ArrayList();
   private String tableType = null;
      
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;   
   private String pageState = new String();
   private ArrayList adCALList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdAmountLimitList getAdCALByIndex(int index) {

      return((AdAmountLimitList)adCALList.get(index));

   }

   public Object[] getAdCALList() {

      return adCALList.toArray();

   }

   public int getAdCALListSize() {

      return adCALList.size();

   }

   public void saveAdCALList(Object newAdCALList) {

      adCALList.add(newAdCALList);

   }

   public void clearAdCALList() {
      adCALList.clear();
   }

   public void setRowSelected(Object selectedAdCALList, boolean isEdit) {

      this.rowSelected = adCALList.indexOf(selectedAdCALList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdCALRow(int rowSelected) {
   	
	   this.department = ((AdAmountLimitList)adCALList.get(rowSelected)).getDepartment();
       this.amountLimit = ((AdAmountLimitList)adCALList.get(rowSelected)).getAmountLimit();
       this.andOr = ((AdAmountLimitList)adCALList.get(rowSelected)).getAndOr();
       
   }

   public void updateAdCALRow(int rowSelected, Object newAdCALList) {

      adCALList.set(rowSelected, newAdCALList);

   }

   public void deleteAdCALList(int rowSelected) {

      adCALList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getApprovalDocumentCode() {
   	
   	  return approvalDocumentCode;
   	
   }
   
   public void setApprovalDocumentCode(Integer approvalDocumentCode) {
   	
   	  this.approvalDocumentCode = approvalDocumentCode;
   	
   }
   
   public Integer getApprovalCoaLineCode() {
   	
   	  return approvalCoaLineCode;
   	  
   }
   
   public void setApprovalCoaLineCode(Integer approvalCoaLineCode) {
   	
   	  this.approvalCoaLineCode = approvalCoaLineCode;
   	  
   }
   
   public String getType() {

      return type;

   }

   public void setType(String type) {

      this.type = type;

   }
   
   public String getAmountLimit() {

      return amountLimit;

   }

   public void setAmountLimit(String amountLimit) {

      this.amountLimit = amountLimit;

   }
   
   
   public String getDepartment() {
	   	
	   return department;
	   	
	}
	
	public void setDepartment(String department) {
		
		  this.department = department;
		
	}
	
	public ArrayList getDepartmentList() {
		
		  return departmentList;
		
	}
	
	public void setDepartmentList(String department) {
		
		  departmentList.add(department);
		
	}
	
	public void clearDepartmentList() {
		
		  departmentList.clear();
		  departmentList.add(Constants.GLOBAL_BLANK);
		
	}
   
   public String getAndOr() {

      return andOr;

   }

   public void setAndOr(String andOr) {

      this.andOr = andOr;

   }   
       
   public ArrayList getAndOrList() {
   	
   	  return andOrList;
   	
   }
   
   public String getAccountNumber() {
   	
   	  return accountNumber;
   	   
   }
   
   public void setAccountNumber(String accountNumber) {
   	
   	  this.accountNumber = accountNumber;
   	   
   }
   
   public String getAccountDescription() {
   	
   	  return accountDescription;
   	  
   }
   
   public void setAccountDescription(String accountDescription) {
   	
   	  this.accountDescription = accountDescription;
   	  
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }   
   
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }      

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	
      amountLimit = null;
      department = Constants.GLOBAL_BLANK;
      andOrList.clear();
      andOrList.add(Constants.GLOBAL_BLANK);
      andOrList.add(Constants.AD_CAL_AND);
      andOrList.add(Constants.AD_CAL_OR);
      andOr = Constants.GLOBAL_BLANK;   

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(amountLimit)) {

            errors.add("amountLimit",
               new ActionMessage("amountLimit.error.amountLimitRequired"));

         }

         if (!Common.validateNumberFormat(amountLimit)) {

            errors.add("amountLimit",
               new ActionMessage("amountLimit.error.amountLimitInvalid"));

         } 

         if (Common.validateRequired(andOr)) {

            errors.add("andOr",
               new ActionMessage("amountLimit.error.andOrRequired"));

         }                 

      }
         
      return errors;

   }
}