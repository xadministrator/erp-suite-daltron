package com.struts.ad.amountlimit;

import java.io.Serializable;

public class AdAmountLimitList implements Serializable {

   private Integer amountLimitCode = null;
   private String department = null;
   private String amountLimit = null;
   private String andOr = null;

   private String editButton = null;
   private String deleteButton = null;
   private String approvalUserButton = null;
    
   private AdAmountLimitForm parentBean;
    
   public AdAmountLimitList(AdAmountLimitForm parentBean,
      Integer amountLimitCode,
      String department,
      String amountLimit,
      String andOr) {

      this.parentBean = parentBean;
      this.amountLimitCode = amountLimitCode;
      this.department = department;
      this.amountLimit = amountLimit;
      this.andOr = andOr;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setApprovalUserButton(String approvalUserButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }   

   public Integer getAmountLimitCode() {

      return amountLimitCode;

   }
   
   public String getDepartment() {

      return department;

   }
   
   public String getAmountLimit() {

      return amountLimit;

   }

   public String getAndOr() {

      return andOr;

   }

}