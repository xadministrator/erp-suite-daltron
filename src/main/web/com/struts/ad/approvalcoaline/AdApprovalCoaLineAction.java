package com.struts.ad.approvalcoaline;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdApprovalCoaLineController;
import com.ejb.txn.AdApprovalCoaLineControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdApprovalCoaLineDetails;
import com.util.AdApprovalDocumentDetails;
import com.util.AdModApprovalCoaLineDetails;

public final class AdApprovalCoaLineAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdApprovalCoaLineAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdApprovalCoaLineForm actionForm = (AdApprovalCoaLineForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_APPROVAL_COA_LINE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adApprovalCoaLine");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdApprovalCoaLineController EJB
*******************************************************/

         AdApprovalCoaLineControllerHome homeACL = null;
         AdApprovalCoaLineController ejbACL = null;

         try {

            homeACL = (AdApprovalCoaLineControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdApprovalCoaLineControllerEJB", AdApprovalCoaLineControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdApprovalCoaLineAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbACL = homeACL.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdApprovalCoaLineAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad ACL Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	       actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	       return(mapping.findForward("adApprovalCoaLine"));
	     
/*******************************************************
   -- Ad ACL Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	       actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	       return(mapping.findForward("adApprovalCoaLine"));         
         
/*******************************************************
   -- Ad ACL Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            try {
            	
            	ejbACL.addAdAclEntry(actionForm.getAccountNumber(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("amountLimit.error.amountLimitAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {
            	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("coaLine.error.coaAccountNumberInvalid"));
        
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAmountLimitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad ACL Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Ad ACL Back Action --
*******************************************************/

         } else if (request.getParameter("approvalDocument") != null) {
         	
			return(new ActionForward("/adApprovalDocument.do"));               

/*******************************************************
   -- Ad ACL Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdApprovalCoaLineList adACLList =
	            actionForm.getAdACLByIndex(actionForm.getRowSelected());
                        
            AdApprovalCoaLineDetails details = new AdApprovalCoaLineDetails();
            details.setAclCode(adACLList.getApprovalCoaLineCode());
            
            try {
            	
            	ejbACL.updateAdAclEntry(details, actionForm.getAccountNumber(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("coaLine.error.recordAlreadyExist"));
                 
            } catch (GlobalAccountNumberInvalidException ex) {
            	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("coaLine.error.coaAccountNumberInvalid"));                 

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdApprovalCoaLineAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
	 -- Ad ACL Amount Limits Action --
*******************************************************/

         // forward to amount limit module             	
            	
         } else if (request.getParameter("adACLList[" +
            actionForm.getRowSelected() + "].amountLimitButton") != null) {
            	
            AdApprovalCoaLineList adACLList =
	            actionForm.getAdACLByIndex(actionForm.getRowSelected());

	  	    String amountLimitPath = "/adAmountLimit.do?forward=1" +
		           "&approvalCoaLineCode=" + adACLList.getApprovalCoaLineCode() +
		           "&accountNumber=" + adACLList.getAccountNumber() +
		           "&accountDescription=" + adACLList.getAccountDescription();

			return(new ActionForward(amountLimitPath));                           
            
/*******************************************************
   -- Ad ACL Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad ACL Edit Action --
*******************************************************/

         } else if (request.getParameter("adACLList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdACLRow(actionForm.getRowSelected());
            
            return mapping.findForward("adApprovalCoaLine");                   
            
/*******************************************************
   -- Ad ACL Delete Action --
*******************************************************/

         } else if (request.getParameter("adACLList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdApprovalCoaLineList adACLList =
	            actionForm.getAdACLByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbACL.deleteAdAclEntry(adACLList.getApprovalCoaLineCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("coaLine.error.recordAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdApprovalCoaLineAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad ACL Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adApprovalCoaLine");

            }
                                  	                        	                          	            	
            AdApprovalDocumentDetails adcDetails = null;

	        if (request.getParameter("forward") != null) {

	            adcDetails = ejbACL.getAdAdcByAdcCode(new Integer(request.getParameter("approvalDocumentCode")), user.getCmpCode());        		

	        } else {
	     		
	            adcDetails = ejbACL.getAdAdcByAdcCode(actionForm.getApprovalDocumentCode(), user.getCmpCode());
	     		
	        }

            actionForm.setType(adcDetails.getAdcType());                    	

            ArrayList list = null;
            Iterator i = null;
            
	        try {
	        	
	           actionForm.clearAdACLList();
	        		        		    	
	           list = ejbACL.getAdAclAll(user.getCmpCode());
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdModApprovalCoaLineDetails details = (AdModApprovalCoaLineDetails)i.next();
	            	
	              AdApprovalCoaLineList adACLList = new AdApprovalCoaLineList(actionForm,
	            	    details.getAclCode(),
	            	    details.getAclCoaAccountNumber(),
	            	    details.getAclCoaDescription());
	            	 	            	    
	              actionForm.saveAdACLList(adACLList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdAmountLimitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adApprovalCoaLine"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdAmountLimitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}