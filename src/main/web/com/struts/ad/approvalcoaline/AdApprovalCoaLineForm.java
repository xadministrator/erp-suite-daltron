package com.struts.ad.approvalcoaline;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdApprovalCoaLineForm extends ActionForm implements Serializable {
	
	private Integer approvalDocumentCode = null;
	private String type = null;
	private String approvalDocumentButton = null;
	private String accountNumber = null;
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;
	private String saveButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String closeButton = null;
	
	private String pageState = new String();
	private ArrayList adACLList = new ArrayList();
	private int rowSelected = 0;
    private String userPermission = new String();
    private String txnStatus = new String();

	public int getRowSelected() {

      return rowSelected;

   }

   public AdApprovalCoaLineList getAdACLByIndex(int index) {

      return((AdApprovalCoaLineList)adACLList.get(index));

   }

   public Object[] getAdACLList() {

      return adACLList.toArray();

   }

   public int getAdACLListSize() {

      return adACLList.size();

   }

   public void saveAdACLList(Object newAdACLList) {

      adACLList.add(newAdACLList);

   }

   public void clearAdACLList() {
      adACLList.clear();
   }

   public void setRowSelected(Object selectedAdACLList, boolean isEdit) {

      this.rowSelected = adACLList.indexOf(selectedAdACLList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }
   
   public void showAdACLRow(int rowSelected) {
   	
       this.accountNumber = ((AdApprovalCoaLineList)adACLList.get(rowSelected)).getAccountNumber();
       
   }

   public void updateAdACLRow(int rowSelected, Object newAdACLList) {

      adACLList.set(rowSelected, newAdACLList);

   }

   public void deleteAdACLList(int rowSelected) {

      adACLList.remove(rowSelected);

   }
   
   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setApprovalDocumentButton(String approvalDocumentButton) {

      this.approvalDocumentButton = approvalDocumentButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getApprovalDocumentCode() {
   	
   	  return approvalDocumentCode;
   	
   }
   
   public void setApprovalDocumentCode(Integer approvalDocumentCode) {
   	
   	  this.approvalDocumentCode = approvalDocumentCode;
   	
   }
   
   public String getType() {

      return type;

   }

   public void setType(String type) {

      this.type = type;

   }
	
   public void setAccountNumber(String accountNumber) {
		
		this.accountNumber = accountNumber; 
		
	}

	public String getAccountNumber() {
		
		return (this.accountNumber); 
		
	}
	
    public void setTableType(String tableType) {
    	
		this.tableType = tableType; 
		
	}

	public String getTableType() {
		
		return (this.tableType); 
		
	}
   
    public void reset(ActionMapping mapping, HttpServletRequest request) {

	  accountNumber = null; 

   }
   
   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(accountNumber)) {

            errors.add("accountNumber",
               new ActionMessage("coaLine.error.coaAccountNumberRequired"));

         }

      }
      
      return errors;

   }
	
}