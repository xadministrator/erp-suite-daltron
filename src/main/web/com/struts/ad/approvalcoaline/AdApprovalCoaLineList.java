package com.struts.ad.approvalcoaline;

import java.io.Serializable;

public class AdApprovalCoaLineList implements Serializable {

   private Integer approvalCoaLineCode = null;
   private String accountNumber = null;
   private String accountDescription = null;
  
   private String editButton = null;
   private String deleteButton = null;
   private String amountLimitButton = null;
    
   private AdApprovalCoaLineForm parentBean;
    
   public AdApprovalCoaLineList(AdApprovalCoaLineForm parentBean,
      Integer approvalCoaLineCode,
      String accountNumber,
      String accountDescription) {

      this.parentBean = parentBean;
      this.approvalCoaLineCode = approvalCoaLineCode;
      this.accountNumber = accountNumber;
      this.accountDescription = accountDescription;
   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   
   
   public void setAmountLimitButton(String amountLimitButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public Integer getApprovalCoaLineCode() {
   	
   	  return approvalCoaLineCode;
   	
   }

   public String getAccountNumber() {

      return accountNumber;

   }
   
   public String getAccountDescription() {
       
       return accountDescription;
       
   }

}