package com.struts.ad.approvaldocument;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdApprovalDocumentController;
import com.ejb.txn.AdApprovalDocumentControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdApprovalDocumentDetails;

public final class AdApprovalDocumentAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdApprovalDocumentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdApprovalDocumentForm actionForm = (AdApprovalDocumentForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_APPROVAL_DOCUMENT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adApprovalDocument");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdApprovalDocumentController EJB
*******************************************************/

         AdApprovalDocumentControllerHome homeADC = null;
         AdApprovalDocumentController ejbADC = null;

         try {

            homeADC = (AdApprovalDocumentControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdApprovalDocumentControllerEJB", AdApprovalDocumentControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdApprovalDocumentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbADC = homeADC.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdApprovalDocumentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Ad ADC Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	       actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	       return(mapping.findForward("adApprovalDocument"));
	     
/*******************************************************
   -- Ad ADC Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	       actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	       return(mapping.findForward("adApprovalDocument"));    

/*******************************************************
   -- Ad ADC Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));   
            
/*******************************************************
   -- Ad ADC Back Action --
*******************************************************/

        } else if (request.getParameter("approvalSetup") != null) {

			return(new ActionForward("/adApprovalSetup.do"));                  
         
/*******************************************************
   -- Ad ADC Save Action --
*******************************************************/

        } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdApprovalDocumentList adADCList =
	            actionForm.getAdADCByIndex(actionForm.getRowSelected());
                        
            AdApprovalDocumentDetails details = new AdApprovalDocumentDetails();
            details.setAdcCode(adADCList.getApprovalDocumentCode());
            details.setAdcType(actionForm.getType());
            details.setAdcPrintOption(actionForm.getPrintOption());
            details.setAdcAllowDuplicate(Common.convertBooleanToByte(actionForm.getAllowDuplicate()));
            details.setAdcTrackDuplicate(Common.convertBooleanToByte(actionForm.getTrackDuplicate()));
            
            if(actionForm.getType().equalsIgnoreCase("AR INVOICE")) 
            	details.setAdcEnableCreditLimitChecking(Common.convertBooleanToByte(
            		actionForm.getEnableCreditLimitChecking()));
            
            try {
            	
            	ejbADC.updateAdAdcEntry(details, user.getCmpCode());
                
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdApprovalDocumentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
	 -- Ad CAL Amount Limits Action --
*******************************************************/

         // forward to amount limit module             	
            	
         } else if (request.getParameter("adADCList[" +
            actionForm.getRowSelected() + "].amountLimitButton") != null) {
            	
            AdApprovalDocumentList adADCList =
	            actionForm.getAdADCByIndex(actionForm.getRowSelected());

	  	    String amountLimitPath = "/adAmountLimit.do?forward=1" +
		           "&approvalDocumentCode=" + adADCList.getApprovalDocumentCode() +
		           "&type=" + adADCList.getType();

			return(new ActionForward(amountLimitPath));               
			
/*******************************************************
	 -- Ad CAL COA Limits Action --
*******************************************************/

         // forward to amount limit module             	
            	
         } else if (request.getParameter("adADCList[" +
            actionForm.getRowSelected() + "].coaLimitButton") != null) {
            	
            AdApprovalDocumentList adADCList =
	            actionForm.getAdADCByIndex(actionForm.getRowSelected());  
	            
	        String approvalCoaLinePath = "/adApprovalCoaLine.do?forward=1" +
		           "&approvalDocumentCode=" + adADCList.getApprovalDocumentCode();
		           
		    return(new ActionForward(approvalCoaLinePath));    
						 
/*******************************************************
   -- Ad ADC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad ADC Edit Action --
*******************************************************/

         } else if (request.getParameter("adADCList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdADCRow(actionForm.getRowSelected());
            
            return mapping.findForward("adApprovalDocument");        
                          
/*******************************************************
   -- Ad ADC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adApprovalDocument");

            }

	        ArrayList list = null;
	        Iterator i = null;
	      
	        try {
	        	
	        	actionForm.clearAdADCList();
	        		        		    	
	           list = ejbADC.getAdAdcAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
	            			            	
	              AdApprovalDocumentDetails details = (AdApprovalDocumentDetails)i.next();
	            	
	              AdApprovalDocumentList adADCList = new AdApprovalDocumentList(actionForm,
	            	    details.getAdcCode(),
	            	    details.getAdcType(),
						details.getAdcPrintOption(),
						Common.convertByteToBoolean(details.getAdcAllowDuplicate()),
						Common.convertByteToBoolean(details.getAdcTrackDuplicate()),
						Common.convertByteToBoolean(details.getAdcEnableCreditLimitChecking()));
	            	 	            	    
	              actionForm.saveAdADCList(adADCList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
	        
		       if (log.isInfoEnabled()) {
		
		          log.info("EJBException caught in AdApprovalDocumentAction.execute(): " + ex.getMessage() +
		          " session: " + session.getId());
		          return mapping.findForward("cmnErrorPage"); 
		          
		       }
	
	        }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);   
             	
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }

            actionForm.setPageState(Constants.PAGE_STATE_ADD);
            
            return(mapping.findForward("adApprovalDocument"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdApprovalDocumentAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}