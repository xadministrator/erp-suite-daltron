package com.struts.ad.approvaldocument;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class AdApprovalDocumentForm extends ActionForm implements Serializable {

   private Integer approvalDocumentCode = null;
   private String type = null;
   private String printOption = null;
   private ArrayList printOptionList = new ArrayList();
   private boolean allowDuplicate = false;
   private boolean trackDuplicate = false;
   private boolean enableCreditLimitChecking = false;
   private boolean showEnableCreditLimitChecking = false;
   
   private String tableType = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;   
   private String pageState = new String();
   private ArrayList adADCList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdApprovalDocumentList getAdADCByIndex(int index) {

      return((AdApprovalDocumentList)adADCList.get(index));

   }

   public Object[] getAdADCList() {

      return adADCList.toArray();

   }

   public int getAdADCListSize() {

      return adADCList.size();

   }

   public void saveAdADCList(Object newAdADCList) {

      adADCList.add(newAdADCList);

   }

   public void clearAdADCList() {
      adADCList.clear();
   }

   public void setRowSelected(Object selectedAdADCList, boolean isEdit) {

      this.rowSelected = adADCList.indexOf(selectedAdADCList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdADCRow(int rowSelected) {
   	
       this.type = ((AdApprovalDocumentList)adADCList.get(rowSelected)).getType();
       this.printOption = ((AdApprovalDocumentList)adADCList.get(rowSelected)).getPrintOption();
       this.allowDuplicate = ((AdApprovalDocumentList)adADCList.get(rowSelected)).getAllowDuplicate();
       this.trackDuplicate = ((AdApprovalDocumentList)adADCList.get(rowSelected)).getTrackDuplicate();
       this.enableCreditLimitChecking = ((AdApprovalDocumentList)adADCList.get(rowSelected)).getEnableCreditLimitChecking();
       this.showEnableCreditLimitChecking = (this.type.equalsIgnoreCase("AR INVOICE") ? true : false);
       
   }

   public void updateAdADCRow(int rowSelected, Object newAdADCList) {

      adADCList.set(rowSelected, newAdADCList);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getApprovalDocumentCode() {
   	
   	  return approvalDocumentCode;
   	
   }
   
   public void setApprovalDocumentCode(Integer approvalDocumentCode) {
   	
   	  this.approvalDocumentCode = approvalDocumentCode;
   	
   }

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }    
      
   public String getType() {

      return type;

   }

   public void setType(String type) {

      this.type = type;

   }
   
   public String getPrintOption() {

      return printOption;

   }

   public void setPrintOption(String printOption) {

      this.printOption = printOption;

   }
   
   public ArrayList getPrintOptionList() {
   
      return printOptionList;
   
   }
   
   public boolean getAllowDuplicate() {

      return allowDuplicate;

   }

   public void setAllowDuplicate(boolean allowDuplicate) {

      this.allowDuplicate = allowDuplicate;

   }
   
   public boolean getTrackDuplicate() {

      return trackDuplicate;

   }

   public void setTrackDuplicate(boolean trackDuplicate) {

      this.trackDuplicate = trackDuplicate;

   }    

   public boolean getEnableCreditLimitChecking() {
   	
   	return enableCreditLimitChecking;
   	
   }
   
   public void setEnableCreditLimitChecking(boolean enableCreditLimitChecking) {
   	
   	this.enableCreditLimitChecking = enableCreditLimitChecking;
   	
   } 
   
   public boolean getShowEnableCreditLimitChecking() {
   	
   	return showEnableCreditLimitChecking;
   	
   }
   
   public void setShowEnableCreditLimitChecking(boolean showEnableCreditLimitChecking) {
   	
   	this.showEnableCreditLimitChecking = showEnableCreditLimitChecking;
   	
   } 
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	
      printOptionList.clear();
      printOptionList.add("PRINT ANYTIME");
      printOptionList.add("PRINT APPROVED ONLY");
      printOptionList.add("PRINT UNAPPROVED ONLY");
      printOption = "PRINT ANYTIME";
      allowDuplicate = false;
      trackDuplicate = false;
      enableCreditLimitChecking=false;
      showEnableCreditLimitChecking=false;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("updateButton") != null) {                                

      }
         
      return errors;

   }
}