package com.struts.ad.approvaldocument;

import java.io.Serializable;

public class AdApprovalDocumentList implements Serializable {

   private Integer approvalDocumentCode = null;
   private String type = null;
   private String printOption = null;
   private boolean allowDuplicate = false;
   private boolean trackDuplicate = false;
   private boolean enableCreditLimitChecking = false;
   
   private String editButton = null;
   private String amountLimitButton = null;
   private String coaLimitButton = null;
    
   private AdApprovalDocumentForm parentBean;
    
   public AdApprovalDocumentList(AdApprovalDocumentForm parentBean,
      Integer approvalDocumentCode,
      String type,
	  String printOption,
	  boolean allowDuplicate,
	  boolean trackDuplicate,
	  boolean enableCreditLimitChecking) {

      this.parentBean = parentBean;
      this.approvalDocumentCode = approvalDocumentCode;
      this.type = type;
      this.printOption = printOption;
      this.allowDuplicate = allowDuplicate;
      this.trackDuplicate = trackDuplicate;
      this.enableCreditLimitChecking = enableCreditLimitChecking;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setAmountLimitButton(String amountLimitButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setCoaLimitButton(String coaLimitButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }
   
   public Integer getApprovalDocumentCode() {

      return approvalDocumentCode;

   }

   public String getType() {

      return type;

   }

   public String getPrintOption() {
   
   	  return printOption;
   	
   }
   
   public boolean getAllowDuplicate() {
   	
   	  return allowDuplicate;
   	
   }
   
   public boolean getTrackDuplicate() {
   	
   	  return trackDuplicate;
   	
   }
   
   public boolean getEnableCreditLimitChecking() {
   
   return enableCreditLimitChecking;
   
   }
}