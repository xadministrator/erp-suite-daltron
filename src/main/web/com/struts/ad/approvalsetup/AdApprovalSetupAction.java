package com.struts.ad.approvalsetup;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.AdApprovalSetupController;
import com.ejb.txn.AdApprovalSetupControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdApprovalDetails;

public final class AdApprovalSetupAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdApprovalSetupAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdApprovalSetupForm actionForm = (AdApprovalSetupForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_APPROVAL_SETUP_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adApprovalSetup");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdApprovalSetupController EJB
*******************************************************/

         AdApprovalSetupControllerHome homeAS = null;
         AdApprovalSetupController ejbAS = null;

         try {

            homeAS = (AdApprovalSetupControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdApprovalSetupControllerEJB", AdApprovalSetupControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdApprovalSetupAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAS = homeAS.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdApprovalSetupAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad AS Close Action --
*******************************************************/

         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad AS Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            AdApprovalDetails details = new AdApprovalDetails();
            details.setAprCode(actionForm.getApprovalCode());
            details.setAprEnableGlJournal(Common.convertBooleanToByte(actionForm.getEnableGlJournal()));
            details.setAprEnableApVoucher(Common.convertBooleanToByte(actionForm.getEnableApVoucher()));
            details.setAprEnableApVoucherDepartment(Common.convertBooleanToByte(actionForm.getEnableApVoucherDepartment()));
            details.setAprEnableApDebitMemo(Common.convertBooleanToByte(actionForm.getEnableApDebitMemo()));
            details.setAprEnableApCheck(Common.convertBooleanToByte(actionForm.getEnableApCheck()));
            details.setAprEnableApPurchaseOrder(Common.convertBooleanToByte(actionForm.getEnableApPurchaseOrder()));
            details.setAprEnableApReceivingItem(Common.convertBooleanToByte(actionForm.getEnableApReceivingItem()));
            
            details.setAprEnableArInvoice(Common.convertBooleanToByte(actionForm.getEnableArInvoice()));
            details.setAprEnableArCreditMemo(Common.convertBooleanToByte(actionForm.getEnableArCreditMemo()));
            details.setAprEnableArReceipt(Common.convertBooleanToByte(actionForm.getEnableArReceipt()));
            details.setAprEnableCmFundTransfer(Common.convertBooleanToByte(actionForm.getEnableCmFundTransfer()));
            details.setAprEnableCmAdjustment(Common.convertBooleanToByte(actionForm.getEnableCmAdjustment()));
            details.setAprApprovalQueueExpiration(Common.convertStringToInt(actionForm.getApprovalQueueExpiration()));
            details.setAprEnableInvAdjustment(Common.convertBooleanToByte(actionForm.getEnableInvAdjustment()));
            details.setAprEnableInvBuild(Common.convertBooleanToByte(actionForm.getEnableInvBuildUnbuildAssembly()));
            details.setAprEnableInvBranchStockTransferOrder(Common.convertBooleanToByte(actionForm.getEnableInvBranchStockTransferOrder()));
            details.setAprEnableApPurReq(Common.convertBooleanToByte(actionForm.getEnableApPurchaseRequisition()));
            details.setAprEnableApCanvass(Common.convertBooleanToByte(actionForm.getEnableApCanvass()));
            details.setAprEnableInvAdjustmentRequest(Common.convertBooleanToByte(actionForm.getEnableInvAdjustmentRequest()));
            
            details.setAprEnableInvStockTransfer(Common.convertBooleanToByte(actionForm.getEnableInvStockTransfer()));
            details.setAprEnableInvBranchStockTransfer(Common.convertBooleanToByte(actionForm.getEnableInvBranchStockTransfer()));
            details.setAprEnableArSalesOrder(Common.convertBooleanToByte(actionForm.getEnableArSalesOrder()));
            
            
            details.setAprEnableApCheckPaymentRequest(Common.convertBooleanToByte(actionForm.getEnableApCheckPaymentRequest()));
            details.setAprEnableApCheckPaymentRequestDepartment(Common.convertBooleanToByte(actionForm.getEnableApCheckPaymentRequestDepartment()));
            details.setAprEnableArCustomer(Common.convertBooleanToByte(actionForm.getEnableArCustomer()));
            
            try {
            	
            	ejbAS.saveAdAprEntry(details, user.getCmpCode());

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdApprovalSetupAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
	 -- Ad AS Approval Document Action --
*******************************************************/

         // forward to approval document module             	
            	
         } else if (request.getParameter("approvalDocumentButton") != null) {

			return(new ActionForward("/adApprovalDocument.do"));             
            
/*******************************************************
   -- Ad AS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adApprovalSetup");

            }
                         	                          	            	
	        try {
	        	
            	AdApprovalDetails details = ejbAS.getAdApr(user.getCmpCode());
            		
				actionForm.setEnableGlJournal(Common.convertByteToBoolean(details.getAprEnableGlJournal()));
		        actionForm.setEnableApVoucher(Common.convertByteToBoolean(details.getAprEnableApVoucher()));
		        actionForm.setEnableApVoucherDepartment(Common.convertByteToBoolean(details.getAprEnableApVoucherDepartment()));
		        actionForm.setEnableApDebitMemo(Common.convertByteToBoolean(details.getAprEnableApDebitMemo()));
		        actionForm.setEnableApCheck(Common.convertByteToBoolean(details.getAprEnableApCheck()));
		        actionForm.setEnableApPurchaseOrder(Common.convertByteToBoolean(details.getAprEnableApPurchaseOrder()));
		        actionForm.setEnableApReceivingItem(Common.convertByteToBoolean(details.getAprEnableApReceivingItem()));
		        
		        actionForm.setEnableArInvoice(Common.convertByteToBoolean(details.getAprEnableArInvoice()));
		        actionForm.setEnableArCreditMemo(Common.convertByteToBoolean(details.getAprEnableArCreditMemo()));
		        actionForm.setEnableArReceipt(Common.convertByteToBoolean(details.getAprEnableArReceipt()));
		        actionForm.setEnableCmFundTransfer(Common.convertByteToBoolean(details.getAprEnableCmFundTransfer()));
		        actionForm.setEnableCmAdjustment(Common.convertByteToBoolean(details.getAprEnableCmAdjustment()));
		        actionForm.setApprovalQueueExpiration(String.valueOf(details.getAprApprovalQueueExpiration()));
		        actionForm.setEnableInvAdjustment(Common.convertByteToBoolean(details.getAprEnableInvAdjustment()));
		        actionForm.setEnableInvBuildUnbuildAssembly(Common.convertByteToBoolean(details.getAprEnableInvBuild()));
		        actionForm.setEnableInvBranchStockTransferOrder(Common.convertByteToBoolean(details.getAprEnableInvBranchStockTransferOrder()));
		        actionForm.setEnableApPurchaseRequisition(Common.convertByteToBoolean(details.getAprEnableApPurReq()));
		        actionForm.setEnableApCanvass(Common.convertByteToBoolean(details.getAprEnableApCanvass()));
		        actionForm.setEnableInvAdjustmentRequest(Common.convertByteToBoolean(details.getAprEnableInvAdjustmentRequest()));
		        actionForm.setEnableApCheckPaymentRequest(Common.convertByteToBoolean(details.getAprEnableApCheckPaymentRequest()));
		        actionForm.setEnableApCheckPaymentRequestDepartment(Common.convertByteToBoolean(details.getAprEnableApCheckPaymentRequestDepartment()));
		        
		        actionForm.setEnableInvStockTransfer(Common.convertByteToBoolean(details.getAprEnableInvStockTransfer()));
		        actionForm.setEnableInvBranchStockTransfer(Common.convertByteToBoolean(details.getAprEnableInvBranchStockTransfer()));
		        actionForm.setEnableArSalesOrder(Common.convertByteToBoolean(details.getAprEnableArSalesOrder()));
		        actionForm.setEnableArCustomer(Common.convertByteToBoolean(details.getAprEnableArCustomer()));
		        	            	    
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdApprovalSetupAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("adApprovalSetup"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdApprovalSetupAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}