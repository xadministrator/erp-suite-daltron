package com.struts.ad.approvalsetup;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdApprovalSetupForm extends ActionForm implements Serializable {

	private Integer approvalCode = null;
    private boolean enableGlJournal = false;
    private boolean enableApVoucher = false;
    private boolean enableApVoucherDepartment = false;
    private boolean enableApDebitMemo = false;
    private boolean enableApCheck = false;
    private boolean enableApPurchaseOrder = false;
    private boolean enableApReceivingItem = false;
    private boolean enableArInvoice = false;
    private boolean enableArCreditMemo = false;
    private boolean enableArReceipt = false;
    private boolean enableCmFundTransfer = false;
    private boolean enableCmAdjustment = false;
    private String approvalQueueExpiration = null;
    private boolean enableInvAdjustment = false;
    private boolean enableInvBuildUnbuildAssembly = false;
    private boolean enableApPurchaseRequisition = false;
    private boolean enableApCanvass = false;
    private boolean enableInvAdjustmentRequest = false;
    private boolean enableInvBranchStockTransferOrder = false;
    private boolean enableApCheckPaymentRequest = false;
    private boolean enableApCheckPaymentRequestDepartment = false;
    
    private boolean enableInvStockTransfer = false;
    private boolean enableInvBranchStockTransfer = false;
    private boolean enableArSalesOrder = false;
    private boolean enableArCustomer = false;
    
	private String saveButton = null;
	private String closeButton = null;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}

	public Integer getApprovalCode() {
		
		return approvalCode;
		
	}
	
	public void setApprovalCode(Integer approvalCode) {
		
		this.approvalCode = approvalCode;
		
	}
	
	public boolean getEnableGlJournal() {
		
		return enableGlJournal;
		
	}
	
	public void setEnableGlJournal(boolean enableGlJournal) {
		
		this.enableGlJournal = enableGlJournal;
		
	}
	
	public boolean getEnableInvBranchStockTransferOrder() {
		
		return enableInvBranchStockTransferOrder;
		
	}
	
	public void setEnableInvBranchStockTransferOrder(boolean enableInvBranchStockTransferOrder) {
		
		this.enableInvBranchStockTransferOrder = enableInvBranchStockTransferOrder;
		
	}
	
	public boolean getEnableApVoucher() {
		
		return enableApVoucher;
		
	}
	
	public void setEnableApVoucher(boolean enableApVoucher) {
		
		this.enableApVoucher = enableApVoucher;
		
	}
	
	public boolean getEnableApVoucherDepartment() {
		
		return enableApVoucherDepartment;
		
	}
	
	public void setEnableApVoucherDepartment(boolean enableApVoucherDepartment) {
		
		this.enableApVoucherDepartment = enableApVoucherDepartment;
		
	}
	
	public boolean getEnableApCheckPaymentRequest() {
		
		return enableApCheckPaymentRequest;
		
	}
	
	public void setEnableApCheckPaymentRequest(boolean enableApCheckPaymentRequest) {
		
		this.enableApCheckPaymentRequest = enableApCheckPaymentRequest;
		
	}
	
	public boolean getEnableApCheckPaymentRequestDepartment() {
		
		return enableApCheckPaymentRequestDepartment;
		
	}
	
	public void setEnableApCheckPaymentRequestDepartment(boolean enableApCheckPaymentRequestDepartment) {
		
		this.enableApCheckPaymentRequestDepartment = enableApCheckPaymentRequestDepartment;
		
	}
	
	public boolean getEnableApDebitMemo() {
		
		return enableApDebitMemo;
		
	}
	
	public void setEnableApDebitMemo(boolean enableApDebitMemo) {
		
		this.enableApDebitMemo = enableApDebitMemo;
		
	}
	
	public boolean getEnableApCheck() {
		
		return enableApCheck;
		
	}
	
	public void setEnableApCheck(boolean enableApCheck) {
		
		this.enableApCheck = enableApCheck;
		
	}
	
	public boolean getEnableApPurchaseOrder() {
		
		return enableApPurchaseOrder;
		
	}
	
	public void setEnableApPurchaseOrder(boolean enableApPurchaseOrder) {
		
		this.enableApPurchaseOrder = enableApPurchaseOrder;
		
	}
	
	public boolean getEnableApReceivingItem() {
		
		return enableApReceivingItem;
		
	}
	
	public void setEnableApReceivingItem(boolean enableApReceivingItem) {
		
		this.enableApReceivingItem = enableApReceivingItem;
		
	}
	
	public boolean getEnableArInvoice() {
		
		return enableArInvoice;
		
	}
	
	public void setEnableArInvoice(boolean enableArInvoice) {
		
		this.enableArInvoice = enableArInvoice;
		
	}
	
	public boolean getEnableArCreditMemo() {
		
		return enableArCreditMemo;
		
	}
	
	public void setEnableArCreditMemo(boolean enableArCreditMemo) {
		
		this.enableArCreditMemo = enableArCreditMemo;
		
	}
	
	public boolean getEnableArReceipt() {
		
		return enableArReceipt;
		
	}
	
	public void setEnableArReceipt(boolean enableArReceipt) {
		
		this.enableArReceipt = enableArReceipt;
		
	}
	
	public boolean getEnableCmFundTransfer() {
		
		return enableCmFundTransfer;
		
	}
	
	public void setEnableCmFundTransfer(boolean enableCmFundTransfer) {
		
		this.enableCmFundTransfer = enableCmFundTransfer;
		
	}
	
	public boolean getEnableCmAdjustment() {
		
		return enableCmAdjustment;
		
	}
	
	public void setEnableCmAdjustment(boolean enableCmAdjustment) {
		
		this.enableCmAdjustment = enableCmAdjustment;
		
	}
	
	public String getApprovalQueueExpiration() {
		
		return approvalQueueExpiration;
		
	}
	
	public void setApprovalQueueExpiration(String approvalQueueExpiration) {
		
		this.approvalQueueExpiration = approvalQueueExpiration;
		
	}
	
	public boolean getEnableInvAdjustment() {
		
		return enableInvAdjustment;
		
	}
	
	public void setEnableInvAdjustment(boolean enableInvAdjustment) {
		
		this.enableInvAdjustment = enableInvAdjustment;
	}
	
	public boolean getEnableInvBuildUnbuildAssembly() {
		
		return enableInvBuildUnbuildAssembly;
		
	}
	
	public void setEnableInvBuildUnbuildAssembly(boolean enableInvBuildUnbuildAssembly) {
		
		this.enableInvBuildUnbuildAssembly = enableInvBuildUnbuildAssembly;
		
	}
	
	
	
	
	
	public boolean getEnableApPurchaseRequisition() {
		
		return enableApPurchaseRequisition;
		
	}
	
	public void setEnableApPurchaseRequisition(boolean enableApPurchaseRequisition) {
		
		this.enableApPurchaseRequisition = enableApPurchaseRequisition;
		
	}
	
	
	public boolean getEnableApCanvass() {
		
		return enableApCanvass;
		
	}
	
	public void setEnableApCanvass(boolean enableApCanvass) {
		
		this.enableApCanvass = enableApCanvass;
		
	}
	
	public boolean getEnableInvAdjustmentRequest() {
		
		return enableInvAdjustmentRequest;
		
	}
	
	public void setEnableInvAdjustmentRequest(boolean enableInvAdjustmentRequest) {
		
		this.enableInvAdjustmentRequest = enableInvAdjustmentRequest;
		
	}
	
	
	
	public boolean getEnableInvStockTransfer() {
	    
	    return enableInvStockTransfer;
	    
	}
	
	public void setEnableInvStockTransfer(boolean enableInvStockTransfer) {
	    
	    this.enableInvStockTransfer = enableInvStockTransfer;
	  
	}
	
	public boolean getEnableInvBranchStockTransfer() {

	    return enableInvBranchStockTransfer;
	    
	}
	
	public void setEnableInvBranchStockTransfer(boolean enableInvBranchStockTransfer) {
	    
	    this.enableInvBranchStockTransfer = enableInvBranchStockTransfer;
	  
	}

	public boolean getEnableArSalesOrder() {

	    return enableArSalesOrder;
	    
	}
	
	public void setEnableArSalesOrder(boolean enableArSalesOrder) {
	    
	    this.enableArSalesOrder = enableArSalesOrder;
	  
	}
	
	public boolean getEnableArCustomer() {

	    return enableArCustomer;
	    
	}
	
	public void setEnableArCustomer(boolean enableArCustomer) {
	    
	    this.enableArCustomer = enableArCustomer;
	  
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {		
		
		enableGlJournal = false;
		enableApVoucher = false;
		enableApVoucherDepartment = false;
		enableApDebitMemo = false;
		enableApCheck = false;
		enableApPurchaseOrder = false;
		enableApReceivingItem = false;
		enableArInvoice = false;
		enableArCreditMemo = false;
		enableArReceipt = false;
		enableCmFundTransfer = false;
		enableCmAdjustment = false;
		enableInvAdjustment = false;
		enableInvBuildUnbuildAssembly = false;
		enableApPurchaseRequisition = false;
		enableApCanvass = false;
		enableInvAdjustmentRequest = false;
		enableInvStockTransfer = false;
		enableInvBranchStockTransfer = false;
		enableArSalesOrder = false;
		enableArCustomer = false;
		enableInvBranchStockTransferOrder = false;
		enableApCheckPaymentRequest = false;
		enableApCheckPaymentRequestDepartment = false;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null) {
	
			if (Common.validateRequired(approvalQueueExpiration)) {
			
				errors.add("approvalQueueExpiration",
			   		new ActionMessage("approvalSetup.error.approvalQueueExpirationRequired"));
			
			}
			
			if (!Common.validateNumberFormat(approvalQueueExpiration)) {
			
				errors.add("approvalQueueExpiration",
			   		new ActionMessage("approvalSetup.error.approvalQueueExpirationInvalid"));
								
		    }
			
	  	}
	     
	  	return errors;
	
	}
	
}