package com.struts.ad.approvaluser;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdAURequesterMustBeEnteredOnceException;
import com.ejb.exception.AdAUUserCannotBeARequesterException;
import com.ejb.exception.AdAUUserCannotBeAnApproverException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdApprovalUserController;
import com.ejb.txn.AdApprovalUserControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdAmountLimitDetails;
import com.util.AdApprovalUserDetails;
import com.util.AdModApprovalUserDetails;

public final class AdApprovalUserAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdApprovalUserAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdApprovalUserForm actionForm = (AdApprovalUserForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_APPROVAL_USER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adApprovalUser");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdApprovalUserController EJB
*******************************************************/

         AdApprovalUserControllerHome homeAU = null;
         AdApprovalUserController ejbAU = null;

         try {

            homeAU = (AdApprovalUserControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdApprovalUserControllerEJB", AdApprovalUserControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdApprovalUserAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAU = homeAU.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdApprovalUserAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
         short precisionUnit = 0;
         
         try {
         	
            precisionUnit = ejbAU.getGlFcPrecisionUnit(user.getCmpCode());       
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in AdApprovalUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }         

/*******************************************************
   -- Ad AU Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	       actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	       return(mapping.findForward("adApprovalUser"));
	     
/*******************************************************
   -- Ad AU Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	       actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	       return(mapping.findForward("adApprovalUser"));         
         
/*******************************************************
   -- Ad AU Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdApprovalUserDetails details = new AdApprovalUserDetails();
            details.setAuLevel(actionForm.getLevel());
            details.setAuType(actionForm.getType());
            details.setAuOr(Common.convertBooleanToByte(actionForm.getOr()));
            
            try {
            	
            	ejbAU.addAdAuEntry(details, actionForm.getAmountLimitCode(), actionForm.getUser(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("approvalUser.error.userAlreadyExist"));
                 
            } catch (AdAURequesterMustBeEnteredOnceException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("approvalUser.error.requesterMustBeEnteredOnce"));                 
                 
            } catch (AdAUUserCannotBeAnApproverException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("approvalUser.error.userCannotBeAnApprover"));                                  
                 
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdApprovalUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad AU Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Ad AU Back Action --
*******************************************************/

         } else if (request.getParameter("amountLimitButton") != null) {
         	
			return(new ActionForward("/adAmountLimit.do"));               

/*******************************************************
   -- Ad AU Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdApprovalUserList adAUList =
	            actionForm.getAdAUByIndex(actionForm.getRowSelected());
                        
            AdApprovalUserDetails details = new AdApprovalUserDetails();
            details.setAuCode(adAUList.getApprovalUserCode());
            details.setAuLevel(actionForm.getLevel());
            details.setAuType(actionForm.getType());
            details.setAuOr(Common.convertBooleanToByte(actionForm.getOr()));
            
            try {
            	
            	ejbAU.updateAdAuEntry(details, actionForm.getAmountLimitCode(), actionForm.getUser(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("approvalUser.error.userAlreadyExist"));
                 
            } catch (AdAURequesterMustBeEnteredOnceException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("approvalUser.error.requesterMustBeEnteredOnce"));  
                 
            } catch (AdAUUserCannotBeAnApproverException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("approvalUser.error.userCannotBeAnApprover"));
                 
            } catch (AdAUUserCannotBeARequesterException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("approvalUser.error.userCannotBeARequester"));                 

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdApprovalUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad AU Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad AU Edit Action --
*******************************************************/

         } else if (request.getParameter("adAUList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdAURow(actionForm.getRowSelected());
            
            return mapping.findForward("adApprovalUser");
            
/*******************************************************
   -- Ad AU Delete Action --
*******************************************************/

         } else if (request.getParameter("adAUList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdApprovalUserList adAUList =
	            actionForm.getAdAUByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbAU.deleteAdAuEntry(adAUList.getApprovalUserCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("approvalUser.error.approvalUserAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdApprovalUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad AU Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adApprovalUser");

            }
                                                          	                        	                          	            	
            AdAmountLimitDetails calDetails = null;

	        if (request.getParameter("forward") != null) {

	            calDetails = ejbAU.getAdCalByCalCode(new Integer(request.getParameter("amountLimitCode")), user.getCmpCode());        		
	    	    actionForm.setAmountLimitCode(new Integer(request.getParameter("amountLimitCode")));

	        } else {
	     		
	            calDetails = ejbAU.getAdCalByCalCode(actionForm.getAmountLimitCode(), user.getCmpCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setAmountLimitCode(actionForm.getAmountLimitCode());
    	    actionForm.setDepartment(actionForm.getDepartment());
            actionForm.setAmountLimit(Common.convertDoubleToStringMoney(calDetails.getCalAmountLimit(), precisionUnit));
            actionForm.setAndOr(calDetails.getCalAndOr());

            ArrayList list = null;
            Iterator i = null;
            
            
	        try {
	        	
	        	actionForm.clearAdAUList();
	        		        		        	
               	actionForm.clearUserList();           	
            	
               	list = ejbAU.getAdUsrAll(user.getCmpCode());
            	
               	if (list == null || list.size() == 0) {
            		
                	actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
               	} else {
            		           		            		
            		i = list.iterator();
            		
                  	while (i.hasNext()) {
            			
            	    	actionForm.setUserList((String)i.next());
            			
            	  	}
            		            		
               	} 	           	        	
	        		        		    	
				list = ejbAU.getAdAuByCalCode(actionForm.getAmountLimitCode(), user.getCmpCode()); 
				
				i = list.iterator();
				
				while(i.hasNext()) {
        	            			            	
					AdModApprovalUserDetails mdetails = (AdModApprovalUserDetails)i.next();
					
					AdApprovalUserList adAUList = new AdApprovalUserList(actionForm,
						mdetails.getAuCode(),
						mdetails.getAuLevel(),
						mdetails.getAuUsrName(),
						mdetails.getAuType(),
						Common.convertByteToBoolean(mdetails.getAuOr()));
	            	 	            	    
	              	actionForm.saveAdAUList(adAUList);
	            	
	           	}
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdApprovalUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adApprovalUser"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdApprovalUserAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}