package com.struts.ad.approvaluser;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdApprovalUserForm extends ActionForm implements Serializable {

	private Integer amountLimitCode = null;
	private String department = null;
    private String amountLimit = null;
    private String andOr = null;
	private String user = null;
	private String type = null;
	private String level = null; 
	private ArrayList userList = new ArrayList();
	private ArrayList typeList = new ArrayList();
	private ArrayList levelList = new ArrayList();
	private String tableType = null;
	private boolean or = false;
	  
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String showDetailsButton = null;
	private String hideDetailsButton = null;   
	private String pageState = new String();
	private ArrayList adAUList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
   
	public int getRowSelected() {
	
	  	return rowSelected;
	
	}
	
	public AdApprovalUserList getAdAUByIndex(int index) {
	
	  	return((AdApprovalUserList)adAUList.get(index));
	
	}
	
	public Object[] getAdAUList() {
	
	  	return adAUList.toArray();
	
	}
	
	public int getAdAUListSize() {
	
	  	return adAUList.size();
	
	}
	
	public void saveAdAUList(Object newAdAUList) {
	
	  	adAUList.add(newAdAUList);
	
	}
	
	public void clearAdAUList() {
		
	  	adAUList.clear();
	  	
	}
	
	public void setRowSelected(Object selectedAdAUList, boolean isEdit) {
	
	  	this.rowSelected = adAUList.indexOf(selectedAdAUList);
	
	  	if (isEdit) {
	
	     	this.pageState = Constants.PAGE_STATE_EDIT;
	
	  	}
	
	}
	
	public void showAdAURow(int rowSelected) {
	
		this.user = ((AdApprovalUserList)adAUList.get(rowSelected)).getUser();
		this.level = ((AdApprovalUserList)adAUList.get(rowSelected)).getLevel();
		this.type = ((AdApprovalUserList)adAUList.get(rowSelected)).getType();
		this.or = ((AdApprovalUserList)adAUList.get(rowSelected)).getOr();
	   
	}
	
	public void updateAdAURow(int rowSelected, Object newAdAUList) {
	
	  	adAUList.set(rowSelected, newAdAUList);
	
	}
	
	public void deleteAdAUList(int rowSelected) {
	
	  	adAUList.remove(rowSelected);
	
	}
	
	public void setUpdateButton(String updateButton) {
	
	  	this.updateButton = updateButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
	
	  	this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	  	this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	  	this.closeButton = closeButton;
	
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
	  	this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
	  	this.hideDetailsButton = hideDetailsButton;
		
	}
	
	public void setPageState(String pageState) {
	
	  	this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	  	return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	  
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getAmountLimitCode() {
	
	  	return amountLimitCode;
	
	}
	
	public void setAmountLimitCode(Integer amountLimitCode) {
	
	  	this.amountLimitCode = amountLimitCode;
	
	}
	
	
	
	public String getDepartment() {
		
		return department;
		
	}
	
	public void setDepartment(String department) {
		
		this.department = department;
		
	}
	
	public String getAmountLimit() {
		
		return amountLimit;
		
	}
	
	public void setAmountLimit(String amountLimit) {
		
		this.amountLimit = amountLimit;
		
	}
	
	public String getAndOr() {
		
		return andOr;
				
	}
	
	public void setAndOr(String andOr) {
		
		this.andOr = andOr;
		
	}
	
	public String getUser() {
	
	  	return user;
	  	
	}
	
	public void setUser(String user) {
		
		this.user = user;
		
	}
	
	public ArrayList getUserList() {

		return userList;
	
	}
	
	public void setUserList(String user) {
		
		userList.add(user);
		
	}
	
	public void clearUserList() {
		
		userList.clear();
		userList.add(Constants.GLOBAL_BLANK);
		
	}	
	
	public String getType() {
	
	  	return type;
	
	}
	
	public void setType(String type) {
	
	  	this.type = type;
	
	}
		
	public ArrayList getTypeList() {
	
	   	return typeList;
	
	}
	
	public String getLevel() {
		
	  	return level;
	
	}
	
	
	public void setLevel(String level) {
		
	  	this.level = level;
	
	}
		
	public ArrayList getLevelList() {
	
	   	return levelList;
	
	}
	
	public String getTableType() {
		
	  	return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
	  	this.tableType = tableType;
		
	}   
	
	public boolean getOr() {
		
		return(or);
		
	}
	
	public void setOr(boolean or) {
		
		this.or = or;
		
	}
			
	public void reset(ActionMapping mapping, HttpServletRequest request) {
	
		user = null;
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
		typeList.add(Constants.AD_AU_APPROVER);
		typeList.add(Constants.AD_AU_REQUESTER);
		type = Constants.GLOBAL_BLANK;
		
		levelList.clear();
		levelList.add(Constants.GLOBAL_BLANK);
		levelList.add(Constants.AD_AU_LEVEL1);
		levelList.add(Constants.AD_AU_LEVEL2);
		levelList.add(Constants.AD_AU_LEVEL3);
		levelList.add(Constants.AD_AU_LEVEL4);
		levelList.add(Constants.AD_AU_LEVEL5);
		level = Constants.GLOBAL_BLANK;
		
		or = false;
			
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
	  	ActionErrors errors = new ActionErrors();
	  	if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
	     	if (Common.validateRequired(user)) {
	
	        	errors.add("user",
	           		new ActionMessage("approvalUser.error.userRequired"));
	
	     	}
	
	     	if (Common.validateRequired(type)) {
	
	        	errors.add("type",
	           		new ActionMessage("approvalUser.error.typeRequired"));
	
	     	}                 
	
	  	}
	     
	  	return errors;
	
	}
	
}