package com.struts.ad.approvaluser;

import java.io.Serializable;

public class AdApprovalUserList implements Serializable {

	private Integer approvalUserCode = null;
	private String level = null;
	private String user = null;
	private String type = null;
	private boolean or = false;
	
	private String editButton = null;
	private String deleteButton = null;
	
	private AdApprovalUserForm parentBean;
	
	public AdApprovalUserList(AdApprovalUserForm parentBean,
		Integer approvalUserCode,
		String level,
		String user,
		String type,
		boolean or) {
		
		this.parentBean = parentBean;
		this.approvalUserCode = approvalUserCode;
		this.level = level;
		this.user = user;
		this.type = type;
		this.or = or;

	}
	
	public void setEditButton(String editButton) {
	
	  	parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	  	parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getApprovalUserCode() {
	
	  	return approvalUserCode;
	
	}
	
	public String getLevel() {
		
	  	return level;
	
	}
	
	public String getUser() {
	
	  	return user;
	
	}
	
	public String getType() {
	   
	   	return type;
	   
	}
	
	public boolean getOr() {
		
		return or;
		
	}
	
}