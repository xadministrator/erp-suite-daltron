package com.struts.ad.bank;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdBankController;
import com.ejb.txn.AdBankControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBankDetails;

public final class AdBankAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdBankAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdBankForm actionForm = (AdBankForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_BANK_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adBank");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdBankController EJB
*******************************************************/

         AdBankControllerHome homeBNK = null;
         AdBankController ejbBNK = null;

         try {

            homeBNK = (AdBankControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdBankControllerEJB", AdBankControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdBankAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBNK = homeBNK.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdBankAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad BNK Show Details Action --
*******************************************************/

      if (request.getParameter("showDetailsButton") != null) {
      		      	
	     actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	     return(mapping.findForward("adBank"));
	     
/*******************************************************
   -- Ad BNK Hide Details Action --
*******************************************************/	     
	     
	  } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	     actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	     return(mapping.findForward("adBank"));         

/*******************************************************
   -- Ad BNK Save Action --
*******************************************************/

      } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdBankDetails details = new AdBankDetails();
            details.setBnkInstitution(actionForm.getInstitution());
            details.setBnkName(actionForm.getBankName());
            details.setBnkBranch(actionForm.getBankBranch());
            details.setBnkDescription(actionForm.getDescription());            
            details.setBnkAddress(actionForm.getAddress());
            details.setBnkEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            try {
            	
            	ejbBNK.addAdBnkEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bank.error.bankNameAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBankAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad BNK Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad BNK Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            AdBankList adBNKList =
	            actionForm.getAdBNKByIndex(actionForm.getRowSelected());
          
            AdBankDetails details = new AdBankDetails();
            details.setBnkCode(adBNKList.getBankCode());
            details.setBnkInstitution(actionForm.getInstitution());
            details.setBnkName(actionForm.getBankName());
            details.setBnkBranch(actionForm.getBankBranch());
            details.setBnkDescription(actionForm.getDescription());            
            details.setBnkAddress(actionForm.getAddress());
            details.setBnkEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            try {
            	
            	ejbBNK.updateAdBnkEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bank.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBankAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad BNK Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad BNK Edit Action --
*******************************************************/

         } else if (request.getParameter("adBNKList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdBNKRow(actionForm.getRowSelected());
            
            return mapping.findForward("adBank");
 
/*******************************************************
   -- Ad BNK Delete Action --
*******************************************************/

         } else if (request.getParameter("adBNKList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdBankList adBNKList =
               actionForm.getAdBNKByIndex(actionForm.getRowSelected());
	 
	     try { 
	     
	        ejbBNK.deleteAdBankEntry(adBNKList.getBankCode(), user.getCmpCode());

         } catch (GlobalRecordAlreadyAssignedException ex) {
         	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("bank.error.recordAlreadyAssigned"));
	        
         } catch (GlobalRecordAlreadyDeletedException ex) {
         	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("bank.error.recordAlreadyDeleted"));
                   
         } catch (EJBException ex) {
         	
                if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in AdBankAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                   
                }                
      	 } 
 
            
/*******************************************************
   -- Ad BNK Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adBank");

            } 
            
            ArrayList list = null;
            Iterator i = null;
                   	
            actionForm.clearAdBNKList();
            
	    try {
	            
	                        
	        list = ejbBNK.getAdBnkAll(user.getCmpCode());
	            
	        i = list.iterator();
	            
	        while(i.hasNext()) {
        	            			            	
	            AdBankDetails mdetails = (AdBankDetails)i.next();
	            	
	            AdBankList adBNKList = new AdBankList(actionForm,
	            	    mdetails.getBnkCode(),	            	    
                        mdetails.getBnkName(),
                        mdetails.getBnkBranch(),
                        mdetails.getBnkInstitution(),
	            	    mdetails.getBnkDescription(),
                        mdetails.getBnkAddress(),                            
	            	    Common.convertByteToBoolean(mdetails.getBnkEnable()));
	            	    
	                actionForm.saveAdBNKList(adBNKList);
	            	
	            }
	            
	    } catch (GlobalNoRecordFoundException ex) {
	        	        	
	    } catch (EJBException ex) {
                
                if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBankAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("adBank"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdBankAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}