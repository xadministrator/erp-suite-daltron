package com.struts.ad.bank;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdBankForm extends ActionForm implements Serializable {

   private Integer bankCode = null;
   private String institution = null;
   private ArrayList institutionList = new ArrayList();
   private String bankName = null;
   private String bankBranch = null;
   private String description = null;
   private String address = null;
   private String tableType = null;
   private boolean enable = false;

   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;
   private String pageState = new String();
   private ArrayList adBNKList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdBankList getAdBNKByIndex(int index) {

      return((AdBankList)adBNKList.get(index));

   }

   public Object[] getAdBNKList() {

      return adBNKList.toArray();

   }

   public int getAdBNKListSize() {

      return adBNKList.size();

   }

   public void saveAdBNKList(Object newAdBNKList) {

      adBNKList.add(newAdBNKList);

   }

   public void clearAdBNKList() {
      adBNKList.clear();
   }

   public void setRowSelected(Object selectedAdBNKList, boolean isEdit) {

      this.rowSelected = adBNKList.indexOf(selectedAdBNKList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdBNKRow(int rowSelected) {
   	
       this.bankName = ((AdBankList)adBNKList.get(rowSelected)).getBankName();
       this.bankBranch = ((AdBankList)adBNKList.get(rowSelected)).getBankBranch();
       this.institution = ((AdBankList)adBNKList.get(rowSelected)).getInstitution();       
       this.description = ((AdBankList)adBNKList.get(rowSelected)).getDescription();
       this.address = ((AdBankList)adBNKList.get(rowSelected)).getAddress();
       this.enable = ((AdBankList)adBNKList.get(rowSelected)).getEnable();
       
   }

   public void updateAdBNKRow(int rowSelected, Object newAdBNKList) {

      adBNKList.set(rowSelected, newAdBNKList);

   }

   public void deleteAdBNKList(int rowSelected) {

      adBNKList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getBankCode() {
   	
   	  return bankCode;
   	
   }
   
   public void setBankCode(Integer bankCode) {
   	
   	  this.bankCode = bankCode;
   	
   }

   public String getInstitution() {

      return institution;

   }

   public void setInstitution(String institution) {

      this.institution = institution;

   }
   
   public ArrayList getInstitutionList() {
   	
   	   return institutionList;
   	
   }   
   
   public String getBankName() {

      return bankName;

   }

   public void setBankName(String bankName) {

      this.bankName = bankName;

   }

   public String getBankBranch() {

      return bankBranch;

   }

   public void setBankBranch(String bankBranch) {

      this.bankBranch = bankBranch;

   }

   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }

   public String getAddress() {

      return address;

   }

   public void setAddress(String address) {

      this.address = address;

   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }         

   public boolean getEnable() {

      return enable;

   }

   public void setEnable(boolean enable) {

      this.enable = enable;

   }

   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      institutionList.clear();
      institutionList.add(Constants.AD_BANK_CLEARING_HOUSE);
      institutionList.add(Constants.AD_BANK);
      institution = Constants.AD_BANK_CLEARING_HOUSE; 
      bankName = null;
      bankBranch = null;
      description = null;
      address = null;
      enable = false;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(institution)) {

            errors.add("institution",
               new ActionMessage("bank.error.institutionRequired"));

         }

         if (Common.validateRequired(bankName)) {

            errors.add("bankName",
               new ActionMessage("bank.error.bankNameRequired"));

         }

         if (Common.validateRequired(bankBranch)) {

            errors.add("bankBranch",
               new ActionMessage("bank.error.bankBranchRequired"));

         }
         
      }
         
      return errors;

   }
}