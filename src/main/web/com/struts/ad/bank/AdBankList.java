package com.struts.ad.bank;

import java.io.Serializable;

public class AdBankList implements Serializable {

   private Integer bankCode = null;
   private String bankName = null;
   private String bankBranch = null;
   private String institution = null;
   private String description = null;
   private String address = null;
   private boolean enable = false;

   private String editButton = null;
   private String deleteButton = null;
    
   private AdBankForm parentBean;
    
   public AdBankList(AdBankForm parentBean,
      Integer bankCode,
      String bankName,
      String bankBranch,
      String institution,
      String description,
      String address,
      boolean enable) {

      this.parentBean = parentBean;
      this.bankCode = bankCode;
      this.bankName = bankName;
      this.bankBranch = bankBranch;
      this.institution = institution;
      this.description = description;
      this.address = address;
      this.enable = enable;
   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }

   public void setDeleteButton(String deleteButton) {
   	
   	  parentBean.setRowSelected(this, false);
   	
   }

   public Integer getBankCode() {

      return bankCode;

   }
   
   public String getBankName() {
       
       return bankName;
       
   }

   public String getBankBranch() {

      return bankBranch;

   }

   public String getInstitution() {

      return institution;

   }

   public String getDescription() {

      return description;

   }

   public String getAddress() {

      return address;

   }
      
   public boolean getEnable() {
       
       return enable;
       
   }

}