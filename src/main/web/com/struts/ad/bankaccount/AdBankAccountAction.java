package com.struts.ad.bankaccount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRTextElement;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignReportFont;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JasperDesign;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdBACoaGlAccountReceiptNotFoundException;
import com.ejb.exception.AdBACoaGlAdjustmentAccountNotFoundException;
import com.ejb.exception.AdBACoaGlAdvanceAccountNotFoundException;
import com.ejb.exception.AdBACoaGlBankChargeAccountNotFoundException;
import com.ejb.exception.AdBACoaGlCashAccountNotFoundException;
import com.ejb.exception.AdBACoaGlCashDiscountNotFoundException;
import com.ejb.exception.AdBACoaGlClearingAccountNotFoundException;
import com.ejb.exception.AdBACoaGlInterestAccountNotFoundException;
import com.ejb.exception.AdBACoaGlSalesDiscountNotFoundException;
import com.ejb.exception.AdBACoaGlUnappliedCheckNotFoundException;
import com.ejb.exception.AdBACoaGlUnappliedReceiptNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdBankAccountController;
import com.ejb.txn.AdBankAccountControllerHome;
import com.struts.ad.documentsequenceassignments.AdBranchDocumentSequenceAssignmentsList;
import com.struts.ad.documentsequenceassignments.AdDocumentSequenceAssignmentsForm;
import com.struts.ad.documentsequenceassignments.AdDocumentSequenceAssignmentsList;
import com.struts.ar.customerentry.ArBranchCustomerList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.AdBankAccountDetails;
import com.util.AdBranchDetails;
import com.util.AdDocumentSequenceAssignmentDetails;
import com.util.AdModBankAccountDetails;
import com.util.AdModBranchBankAccountDetails;
import com.util.AdModBranchCustomerDetails;
import com.util.AdModBranchDocumentSequenceAssignmentDetails;
import com.util.AdResponsibilityDetails;
import com.util.GlModFunctionalCurrencyDetails;

public final class AdBankAccountAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdBankAccountAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdBankAccountForm actionForm = (AdBankAccountForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_BANK_ACCOUNT_ID);

         if(frParam != null){
   	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
   	         ActionErrors fieldErrors = ((AdBankAccountForm)form).validateFields(mapping, request);
                  if(!fieldErrors.isEmpty()){
                     saveErrors(request, new ActionMessages(fieldErrors));
                     return(mapping.findForward("adBankAccount"));
                  }
               }
               ((AdBankAccountForm)form).setUserPermission(frParam.trim());
            }else{
               ((AdBankAccountForm)form).setUserPermission(Constants.NO_ACCESS);
            }
/*******************************************************
   Initialize AdBankAccountController EJB
*******************************************************/

         AdBankAccountControllerHome homeBA = null;
         AdBankAccountController ejbBA = null;

         try {

            homeBA = (AdBankAccountControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdBankAccountControllerEJB", AdBankAccountControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdBankAccountAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBA = homeBA.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdBankAccountAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbBA.getGlFcPrecisionUnit(user.getCmpCode());
            
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in AdBankAccountAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }
         
/*******************************************************
   -- Ad BA Show Details Action --
*******************************************************/

      if (request.getParameter("showDetailsButton") != null) {
      		      	
    	  ((AdBankAccountForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	     return(mapping.findForward("adBankAccount"));
	     
	  
	     
/*******************************************************
   -- Ad BA Hide Details Action --
*******************************************************/	     
	     
	  } else if (request.getParameter("hideDetailsButton") != null) { 
	  
		  ((AdBankAccountForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	     return(mapping.findForward("adBankAccount"));                  

/*******************************************************
   -- Ad BA Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
        		 ((AdBankAccountForm)form).getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdModBankAccountDetails details = new AdModBankAccountDetails();
            details.setBaName(actionForm.getAccountName());
            details.setBaDescription(actionForm.getDescription());            
            details.setBaAccountType(actionForm.getAccountType());
            details.setBaAccountNumber(actionForm.getAccountNumber());
            details.setBaAccountUse(actionForm.getAccountUse());     
            details.setBaNextCheckNumber(actionForm.getInitialCheckNumber());
            details.setBaEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            details.setBaAccountNumberShow(Common.convertBooleanToByte(actionForm.getAccountNumberShow()));
            details.setBaAccountNumberTop(Common.convertStringToInt(actionForm.getAccountNumberTop()));
            details.setBaAccountNumberLeft(Common.convertStringToInt(actionForm.getAccountNumberLeft()));
            
            details.setBaFontSize(Common.convertStringToInt(actionForm.getFontSize()));
            details.setBaFontStyle(actionForm.getFontStyle());
			
            details.setBaAccountNameShow(Common.convertBooleanToByte(actionForm.getAccountNameShow()));
            details.setBaAccountNameTop(Common.convertStringToInt(actionForm.getAccountNameTop()));
            details.setBaAccountNameLeft(Common.convertStringToInt(actionForm.getAccountNameLeft()));
            
            details.setBaNumberShow(Common.convertBooleanToByte(actionForm.getNumberShow()));
            details.setBaNumberTop(Common.convertStringToInt(actionForm.getNumberTop()));
            details.setBaNumberLeft(Common.convertStringToInt(actionForm.getNumberLeft()));
            
            details.setBaDateShow(Common.convertBooleanToByte(actionForm.getDateShow()));
            details.setBaDateTop(Common.convertStringToInt(actionForm.getDateTop()));
            details.setBaDateLeft(Common.convertStringToInt(actionForm.getDateLeft()));
            
            details.setBaPayeeShow(Common.convertBooleanToByte(actionForm.getPayeeShow()));
            details.setBaPayeeTop(Common.convertStringToInt(actionForm.getPayeeTop()));
            details.setBaPayeeLeft(Common.convertStringToInt(actionForm.getPayeeLeft()));
            
            details.setBaAmountShow(Common.convertBooleanToByte(actionForm.getAmountShow()));
            details.setBaAmountTop(Common.convertStringToInt(actionForm.getAmountTop()));
            details.setBaAmountLeft(Common.convertStringToInt(actionForm.getAmountLeft()));
            
            details.setBaWordAmountShow(Common.convertBooleanToByte(actionForm.getWordAmountShow()));
            details.setBaWordAmountTop(Common.convertStringToInt(actionForm.getWordAmountTop()));
            details.setBaWordAmountLeft(Common.convertStringToInt(actionForm.getWordAmountLeft()));
            
            details.setBaCurrencyShow(Common.convertBooleanToByte(actionForm.getCurrencyShow()));
            details.setBaCurrencyTop(Common.convertStringToInt(actionForm.getCurrencyTop()));
            details.setBaCurrencyLeft(Common.convertStringToInt(actionForm.getCurrencyLeft()));
            
            details.setBaAddressShow(Common.convertBooleanToByte(actionForm.getAddressShow()));
            details.setBaAddressTop(Common.convertStringToInt(actionForm.getAddressTop()));
            details.setBaAddressLeft(Common.convertStringToInt(actionForm.getAddressLeft()));
            
            details.setBaMemoShow(Common.convertBooleanToByte(actionForm.getMemoShow()));
            details.setBaMemoTop(Common.convertStringToInt(actionForm.getMemoTop()));
            details.setBaMemoLeft(Common.convertStringToInt(actionForm.getMemoLeft()));
            
            details.setBadocNumberShow(Common.convertBooleanToByte(actionForm.getDocNumberShow()));
            details.setBadocNumberTop(Common.convertStringToInt(actionForm.getDocNumberTop()));
            details.setBadocNumberLeft(Common.convertStringToInt(actionForm.getDocNumberLeft()));
            
            details.setBaIsCashAccount(Common.convertBooleanToByte(actionForm.getIsCashAccount()));
            
          
            ArrayList branchList = new ArrayList();
            
            for(int i=0; i<actionForm.getAdBBAListSize(); i++) {
                
            	AdBranchBankAccountList adBbaList = actionForm.getAdBBAByIndex(i);
                
                if(adBbaList.getBranchCheckbox() == true) {                                          
                    
                	AdModBranchBankAccountDetails brBbaDetails = new AdModBranchBankAccountDetails();                                          
                    
                	brBbaDetails.setBbaAdCompany(user.getCmpCode());

                	brBbaDetails.setBbaBranchCode(adBbaList.getBranchCode());
                	brBbaDetails.setBbaBranchName(adBbaList.getBranchName());
                    
                    if ( Common.validateRequired(adBbaList.getBranchCashAccount()) ||
             				Common.validateRequired(adBbaList.getBranchCashAccountDescription())) {

                    	brBbaDetails.setBbaCashAccountNumber(actionForm.getCashAccount());
                    	brBbaDetails.setBbaCashAccountDescription(actionForm.getCashAccountDescription());
                    	
             			
             		}
             		
             		if ( Common.validateRequired(adBbaList.getBranchBankChargeAccount()) ||
             				Common.validateRequired(adBbaList.getBranchBankChargeAccountDescription())) {
             			
             			brBbaDetails.setBbaBankChargeAccountNumber(actionForm.getBankChargeAccount());
             			brBbaDetails.setBbaBankChargeAccountDescription(actionForm.getBankChargeAccountDescription());

             		}
             		
             		if ( Common.validateRequired(adBbaList.getBranchInterestAccount()) ||
             				Common.validateRequired(adBbaList.getBranchInterestAccountDescription())) {

             			brBbaDetails.setBbaInterestAccountNumber(actionForm.getInterestAccount());
             			brBbaDetails.setBbaInterestAccountDescription(actionForm.getInterestAccountDescription());

             		}
             		
             		if ( Common.validateRequired(adBbaList.getBranchAdjustmentAccount()) ||
             				Common.validateRequired(adBbaList.getBranchAdjustmentAccountDescription())) {

             			brBbaDetails.setBbaAdjustmentAccountNumber(actionForm.getAdjustmentAccount());
             			brBbaDetails.setBbaAdjustmentAccountDescription(actionForm.getAdjustmentAccountDescription());


             		}
             		
             		if ( Common.validateRequired(adBbaList.getBranchSalesDiscountAccount()) ||
             				Common.validateRequired(adBbaList.getBranchSalesDiscountAccountDescription())) {

             			brBbaDetails.setBbaSalesDiscountAccountNumber(actionForm.getSalesDiscountAccount());
             			brBbaDetails.setBbaSalesDiscountAccountDescription(actionForm.getAdjustmentAccountDescription());

             		}
             		
             		if ( Common.validateRequired(adBbaList.getBranchAdvanceAccount()) ||
             				Common.validateRequired(adBbaList.getBranchAdvanceAccountDescription())) {

             			brBbaDetails.setBbaAdvanceAccountNumber(actionForm.getAdvanceAccount());
             			brBbaDetails.setBbaAdvanceAccountDescription(actionForm.getAdjustmentAccountDescription());


             		}

                    branchList.add(brBbaDetails);
                }
            }
            
            
            
            
            try {
            	
            	ejbBA.addAdBaEntry(details, 
            		((AdBankAccountForm)form).getCashAccount(), 
	            	  null, 
	            	  null,
	            	  ((AdBankAccountForm)form).getBankChargeAccount(), 
	            	  null,
	            	  ((AdBankAccountForm)form).getInterestAccount(), 
	            	  ((AdBankAccountForm)form).getAdjustmentAccount(), 
	            	  null, 
	            	  ((AdBankAccountForm)form).getSalesDiscountAccount(),
	            	   null, 
	            	   ((AdBankAccountForm)form).getAdvanceAccount(),
	            	   ((AdBankAccountForm)form).getBankName(), 
	            	   ((AdBankAccountForm)form).getCurrency(), 
	            	   branchList,
	            	   user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.bankNameAlreadyExist"));

            } catch (AdBACoaGlCashAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.cashAccountNotFound"));

            } catch (AdBACoaGlAccountReceiptNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.accountReceiptNotFound"));

            } catch (AdBACoaGlUnappliedReceiptNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.unappliedReceiptNotFound"));
                 
            } catch (AdBACoaGlUnappliedCheckNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.unappliedCheckNotFound"));                 

            } catch (AdBACoaGlBankChargeAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.bankChargeAccountNotFound"));

            } catch (AdBACoaGlClearingAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.clearingAccountNotFound"));
                 
            } catch (AdBACoaGlInterestAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.interestAccountNotFound"));
                 
            } catch (AdBACoaGlAdjustmentAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.adjustmentAccountNotFound"));

            } catch (AdBACoaGlCashDiscountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.cashDiscountNotFound"));

            } catch (AdBACoaGlSalesDiscountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.salesDiscountNotFound"));
               
            } catch (AdBACoaGlAdvanceAccountNotFoundException ex) {
                
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("bankAccount.error.advanceAccountNotFound"));


            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBankAccountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad BA Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad BA Update Action --
*******************************************************/
 	 
         }else if(request.getParameter("updateButton") != null &&
                 ((AdBankAccountForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
         
        	 
        	 
        	 
        	 AdBankAccountList adDSAList =
                     ((AdBankAccountForm)form).getAdBAByIndex(
                     ((AdBankAccountForm) form).getRowSelected());
                 
        	 AdModBankAccountDetails details = new AdModBankAccountDetails();
                 
        	 details.setBaCode(adDSAList.getBankAccountCode());
        	 details.setBaName(actionForm.getAccountName());
             details.setBaDescription(actionForm.getDescription());            
             details.setBaAccountType(actionForm.getAccountType());
             details.setBaAccountNumber(actionForm.getAccountNumber());
             details.setBaAccountUse(actionForm.getAccountUse());     
             details.setBaNextCheckNumber(actionForm.getInitialCheckNumber());
             details.setBaEnable(Common.convertBooleanToByte(actionForm.getEnable()));
             details.setBaAccountNumberShow(Common.convertBooleanToByte(actionForm.getAccountNumberShow()));
             details.setBaAccountNumberTop(Common.convertStringToInt(actionForm.getAccountNumberTop()));
             details.setBaAccountNumberLeft(Common.convertStringToInt(actionForm.getAccountNumberLeft()));
             
             details.setBaFontSize(Common.convertStringToInt(actionForm.getFontSize()));
             details.setBaFontStyle(actionForm.getFontStyle());
 			
             details.setBaAccountNameShow(Common.convertBooleanToByte(actionForm.getAccountNameShow()));
             details.setBaAccountNameTop(Common.convertStringToInt(actionForm.getAccountNameTop()));
             details.setBaAccountNameLeft(Common.convertStringToInt(actionForm.getAccountNameLeft()));
             
             details.setBaNumberShow(Common.convertBooleanToByte(actionForm.getNumberShow()));
             details.setBaNumberTop(Common.convertStringToInt(actionForm.getNumberTop()));
             details.setBaNumberLeft(Common.convertStringToInt(actionForm.getNumberLeft()));
             
             details.setBaDateShow(Common.convertBooleanToByte(actionForm.getDateShow()));
             details.setBaDateTop(Common.convertStringToInt(actionForm.getDateTop()));
             details.setBaDateLeft(Common.convertStringToInt(actionForm.getDateLeft()));
             
             details.setBaPayeeShow(Common.convertBooleanToByte(actionForm.getPayeeShow()));
             details.setBaPayeeTop(Common.convertStringToInt(actionForm.getPayeeTop()));
             details.setBaPayeeLeft(Common.convertStringToInt(actionForm.getPayeeLeft()));
             
             details.setBaAmountShow(Common.convertBooleanToByte(actionForm.getAmountShow()));
             details.setBaAmountTop(Common.convertStringToInt(actionForm.getAmountTop()));
             details.setBaAmountLeft(Common.convertStringToInt(actionForm.getAmountLeft()));
             
             details.setBaWordAmountShow(Common.convertBooleanToByte(actionForm.getWordAmountShow()));
             details.setBaWordAmountTop(Common.convertStringToInt(actionForm.getWordAmountTop()));
             details.setBaWordAmountLeft(Common.convertStringToInt(actionForm.getWordAmountLeft()));
             
             details.setBaCurrencyShow(Common.convertBooleanToByte(actionForm.getCurrencyShow()));
             details.setBaCurrencyTop(Common.convertStringToInt(actionForm.getCurrencyTop()));
             details.setBaCurrencyLeft(Common.convertStringToInt(actionForm.getCurrencyLeft()));
             
             details.setBaAddressShow(Common.convertBooleanToByte(actionForm.getAddressShow()));
             details.setBaAddressTop(Common.convertStringToInt(actionForm.getAddressTop()));
             details.setBaAddressLeft(Common.convertStringToInt(actionForm.getAddressLeft()));
             
             details.setBaMemoShow(Common.convertBooleanToByte(actionForm.getMemoShow()));
             details.setBaMemoTop(Common.convertStringToInt(actionForm.getMemoTop()));
             details.setBaMemoLeft(Common.convertStringToInt(actionForm.getMemoLeft()));
             
             details.setBadocNumberShow(Common.convertBooleanToByte(actionForm.getDocNumberShow()));
             details.setBadocNumberTop(Common.convertStringToInt(actionForm.getDocNumberTop()));
             details.setBadocNumberLeft(Common.convertStringToInt(actionForm.getDocNumberLeft()));
             
             details.setBaIsCashAccount(Common.convertBooleanToByte(actionForm.getIsCashAccount()));
             
            
            
            
         // For Branch Setting
            ArrayList branchList = new ArrayList();
                     
                     for (int i = 0; i<actionForm.getAdBBAListSize(); i++) {
                     	
                    	 AdBranchBankAccountList adBbaList = actionForm.getAdBBAByIndex(i);           	   
                     	
                    	
                     	if (adBbaList.getBranchCheckbox() == true ){ 
                     		
                     		// checks if branches glcoa values are null
                     		
                     		if ( Common.validateRequired(adBbaList.getBranchCashAccount()) ||
                     				Common.validateRequired(adBbaList.getBranchCashAccountDescription())) {

                     			adBbaList.setBranchCashAccount(actionForm.getCashAccount());
                     			adBbaList.setBranchCashAccountDescription(actionForm.getCashAccountDescription());

                     		}
                     		
                     		if ( Common.validateRequired(adBbaList.getBranchBankChargeAccount()) ||
                     				Common.validateRequired(adBbaList.getBranchBankChargeAccountDescription())) {

                     			adBbaList.setBranchBankChargeAccount(actionForm.getBankChargeAccount());
                     			adBbaList.setBranchBankChargeAccountDescription(actionForm.getBankChargeAccountDescription());

                     		}
                     		
                     		if ( Common.validateRequired(adBbaList.getBranchInterestAccount()) ||
                     				Common.validateRequired(adBbaList.getBranchInterestAccountDescription())) {

                     			adBbaList.setBranchInterestAccount(actionForm.getInterestAccount());
                     			adBbaList.setBranchInterestAccountDescription(actionForm.getInterestAccountDescription());

                     		}
                     		
                     		if ( Common.validateRequired(adBbaList.getBranchAdjustmentAccount()) ||
                     				Common.validateRequired(adBbaList.getBranchAdjustmentAccountDescription())) {

                     			adBbaList.setBranchAdjustmentAccount(actionForm.getAdjustmentAccount());
                     			adBbaList.setBranchAdjustmentAccountDescription(actionForm.getAdjustmentAccountDescription());

                     		}
                     		
                     		if ( Common.validateRequired(adBbaList.getBranchSalesDiscountAccount()) ||
                     				Common.validateRequired(adBbaList.getBranchSalesDiscountAccountDescription())) {

                     			adBbaList.setBranchSalesDiscountAccount(actionForm.getSalesDiscountAccount());
                     			adBbaList.setBranchSalesDiscountAccountDescription(actionForm.getSalesDiscountAccountDescription());

                     		}
                     		
                     		if ( Common.validateRequired(adBbaList.getBranchAdvanceAccount()) ||
                     				Common.validateRequired(adBbaList.getBranchAdvanceAccountDescription())) {

                     			adBbaList.setBranchAdvanceAccount(actionForm.getAdvanceAccount());
                     			adBbaList.setBranchAdvanceAccountDescription(actionForm.getAdvanceAccountDescription());

                     		}
                     		
                     		AdModBranchBankAccountDetails brBaDetails = new AdModBranchBankAccountDetails();
                     		brBaDetails.setBbaAdCompany(user.getCmpCode());
                     		brBaDetails.setBbaBranchCode(adBbaList.getBranchCode()  );
                     		brBaDetails.setBbaBranchName(adBbaList.getBranchName() );
                         	
                     		brBaDetails.setBbaCashAccountNumber(adBbaList.getBranchCashAccount());
                     		brBaDetails.setBbaCashAccountDescription(adBbaList.getBranchCashAccountDescription());

                     		brBaDetails.setBbaBankChargeAccountNumber(adBbaList.getBranchAdjustmentAccount());
                     		brBaDetails.setBbaBankChargeAccountDescription(adBbaList.getBranchBankChargeAccountDescription());
                         	
                     		brBaDetails.setBbaInterestAccountNumber(adBbaList.getBranchInterestAccount());
                     		brBaDetails.setBbaInterestAccountDescription(adBbaList.getBranchInterestAccountDescription());
                         	
                     		brBaDetails.setBbaAdjustmentAccountNumber(adBbaList.getBranchAdjustmentAccount());
                     		brBaDetails.setBbaAdjustmentAccountDescription(adBbaList.getBranchAdjustmentAccountDescription());
                         	
                     		brBaDetails.setBbaSalesDiscountAccountNumber(adBbaList.getBranchSalesDiscountAccount());
                     		brBaDetails.setBbaSalesDiscountAccountDescription(adBbaList.getBranchSalesDiscountAccountDescription());
                         	
                     		brBaDetails.setBbaAdvanceAccountNumber(adBbaList.getBranchAdvanceAccount());
                     		brBaDetails.setBbaAdvanceAccountDescription(adBbaList.getBranchAdvanceAccountDescription());
                         	
                     		
                         	branchList.add(brBaDetails);

                     	}
                     	
                     	
                     }
            
            
            

            
            AdResponsibilityDetails mdetails  = ejbBA.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
            
            try {
            	
            	ejbBA.updateAdBaEntry(details, 
                		((AdBankAccountForm)form).getCashAccount(), 
  	            	  null, 
  	            	  null,
  	            	  ((AdBankAccountForm)form).getBankChargeAccount(), 
  	            	  null,
  	            	  ((AdBankAccountForm)form).getInterestAccount(), 
  	            	  ((AdBankAccountForm)form).getAdjustmentAccount(), 
  	            	  null, 
  	            	  ((AdBankAccountForm)form).getSalesDiscountAccount(),
  	            	   null, 
  	            	   ((AdBankAccountForm)form).getAdvanceAccount(),
  	            	   ((AdBankAccountForm)form).getBankName(), 
  	            	   ((AdBankAccountForm)form).getCurrency(),
  	            	   mdetails.getRsName(),
  	            	   branchList,
  	            	   user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.recordAlreadyExist"));

            } catch (AdBACoaGlCashAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.cashAccountNotFound"));

            } catch (AdBACoaGlAccountReceiptNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.accountReceiptNotFound"));

            } catch (AdBACoaGlUnappliedReceiptNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.unappliedReceiptNotFound"));
                 
            } catch (AdBACoaGlUnappliedCheckNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.unappliedCheckNotFound"));                                  

            } catch (AdBACoaGlBankChargeAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.bankChargeAccountNotFound"));

            } catch (AdBACoaGlClearingAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.clearingAccountNotFound"));
                 
            } catch (AdBACoaGlInterestAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.interestAccountNotFound"));
                 
            } catch (AdBACoaGlAdjustmentAccountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.adjustmentAccountNotFound"));

            } catch (AdBACoaGlCashDiscountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.cashDiscountNotFound"));

            } catch (AdBACoaGlSalesDiscountNotFoundException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("bankAccount.error.salesDiscountNotFound"));
               
            } catch (AdBACoaGlAdvanceAccountNotFoundException ex) {
                
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("bankAccount.error.advanceAccountNotFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBankAccountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad BA Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {
         	
         	
/*******************************************************
   -- Ad BA Preview Action --
*******************************************************/

         } else if (request.getParameter("previewButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            	
            // get report session and if not null set it to null 
            
            Report report = new Report();
            
			Report reportSession = 
			   (Report)session.getAttribute(Constants.REPORT_KEY);
			    
			if (reportSession != null){
			 	
			   reportSession.setBytes(null);
			   session.setAttribute(Constants.REPORT_KEY, reportSession);
			    
			}
			
			// create jasper design report properties
		    
		    // jasper design
		    
			JasperDesign jasperDesign = new JasperDesign();
			jasperDesign.setName("Check Preview");
			
				
   		    jasperDesign.setPageWidth(595);
			jasperDesign.setPageHeight(842);		
			jasperDesign.setColumnWidth(515);
			jasperDesign.setColumnSpacing(0);
			jasperDesign.setLeftMargin(0);
			jasperDesign.setRightMargin(0);
			jasperDesign.setTopMargin(0);
			jasperDesign.setBottomMargin(0);
			
			// fonts
			
			JRDesignReportFont normalFont = new JRDesignReportFont();
			normalFont.setName("Arial_Normal");
			normalFont.setDefault(true);
			normalFont.setFontName("Arial");
			normalFont.setSize(Common.convertStringToInt(actionForm.getFontSize()));
			normalFont.setPdfFontName(actionForm.getFontStyle());
			normalFont.setPdfEncoding("Cp1252");
			normalFont.setPdfEmbedded(false);
			jasperDesign.addFont(normalFont);
	
			JRDesignReportFont boldFont = new JRDesignReportFont();
			boldFont.setName("Arial_Bold");
			boldFont.setDefault(false);
			boldFont.setFontName("Arial");
			boldFont.setSize(Common.convertStringToInt(actionForm.getFontSize()));
			boldFont.setBold(true);
			boldFont.setPdfFontName(actionForm.getFontStyle());
			boldFont.setPdfEncoding("Cp1252");
			boldFont.setPdfEmbedded(false);
			jasperDesign.addFont(boldFont);
	
			JRDesignReportFont italicFont = new JRDesignReportFont();
			italicFont.setName("Arial_Italic");
			italicFont.setDefault(false);
			italicFont.setFontName("Arial");
			italicFont.setSize(Common.convertStringToInt(actionForm.getFontSize()));
			italicFont.setItalic(true);
			italicFont.setPdfFontName(actionForm.getFontStyle());
			italicFont.setPdfEncoding("Cp1252");
			italicFont.setPdfEmbedded(false);
			jasperDesign.addFont(italicFont);
			
			JRDesignBand band = new JRDesignBand();
			JRDesignStaticText staticText = new JRDesignStaticText();
			
			// title band
			
			band = new JRDesignBand();
			jasperDesign.setTitle(band);
			
			// page header band
			
			band = new JRDesignBand();
			jasperDesign.setPageHeader(band);
	
			// column header band
			
			band = new JRDesignBand();
			jasperDesign.setPageHeader(band);
			
			// detail band
			
			band = new JRDesignBand();
			band.setHeight(700);
			
			if (actionForm.getAccountNumberShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getAccountNumberLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getAccountNumberTop()));
				staticText.setWidth(150);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX ACCOUNT NUMBER XXX");
				band.addElement(staticText);
				
			}
			
			
			if (actionForm.getAccountNameShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getAccountNameLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getAccountNameTop()));
				staticText.setWidth(150);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX ACCOUNT NAME XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getNumberShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getNumberLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getNumberTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX NUMBER XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getDateShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getDateLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getDateTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX DATE XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getPayeeShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getPayeeLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getPayeeTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX PAYEE XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getAmountShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getAmountLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getAmountTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX AMOUNT XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getWordAmountShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getWordAmountLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getWordAmountTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX WORD AMOUNT XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getCurrencyShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getCurrencyLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getCurrencyTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX CURRENCY XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getAddressShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getAddressLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getAddressTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX ADDRESS XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getMemoShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getMemoLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getMemoTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX MEMO XXX");
				band.addElement(staticText);
				
			}
			
			if (actionForm.getMemoShow()) {
				
				staticText = new JRDesignStaticText();
				staticText.setX(Common.convertStringToInt(actionForm.getDocNumberLeft()));
				staticText.setY(Common.convertStringToInt(actionForm.getDocNumberTop()));
				staticText.setWidth(100);
				staticText.setHeight(15);
				staticText.setFont(normalFont);
				staticText.setTextAlignment(JRTextElement.TEXT_ALIGN_LEFT);
				staticText.setText("XXX DOC NUMBER XXX");
				band.addElement(staticText);
				
			}
			
			jasperDesign.setDetail(band);
	
			// column footer band
			
			band = new JRDesignBand();
			jasperDesign.setColumnFooter(band);
	
			// page footer band
			
			band = new JRDesignBand();
			jasperDesign.setPageFooter(band);
	
			// summary band
			
			band = new JRDesignBand();
			jasperDesign.setSummary(band);
			
			try {
				
				// fill report parameters, fill report to pdf and set report session
			    
			    Map parameters = new HashMap();

			    report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
			    report.setBytes(
				   JasperRunManager.runReportToPdf(JasperCompileManager.compileReport(jasperDesign), parameters, 
				     new AdBankAccountDS()));   				     
			    			     
			} catch (Exception ex) {
				ex.printStackTrace();
				errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("bankAcccount.error.compilationError"));				
                   
                saveErrors(request, new ActionMessages(errors));  
				return mapping.findForward("adBankAccount");
				
			}
					   
			session.setAttribute(Constants.REPORT_KEY, report);
                        
            actionForm.setReport(Constants.STATUS_SUCCESS);
            		  			      
			return mapping.findForward("adBankAccount");

/*******************************************************
   -- Ad BA Edit Action --
*******************************************************/

         }else if(request.getParameter("adBAList[" + 
                 ((AdBankAccountForm)form).getRowSelected() + "].editButton") != null &&
                 ((AdBankAccountForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
                  	 
        	 System.out.println("------------->1");
        	 
             actionForm.reset(mapping, request);
             
             Object[] adBAList = actionForm.getAdBAList();
             System.out.println("-------------->2");
             ArrayList list = null;
             
             try {                                  
                 
                 list = ejbBA.getAdBrBaAll(((AdBankAccountList)adBAList[actionForm.getRowSelected()]).getBankAccountCode(), user.getCmpCode());
                 
                 
             } catch(GlobalNoRecordFoundException ex) {
                 
                 
             }
             System.out.println("list.size()="+list.size());
             
             //actionForm.showAdBARow(list, actionForm.getRowSelected());
             
             System.out.println("((AdBankAccountForm) form).getRowSelected()="+((AdBankAccountForm) form).getRowSelected());
             ((AdBankAccountForm)form).showAdBARow(list,((AdBankAccountForm) form).getRowSelected());
             
             return mapping.findForward("adBankAccount");
            
             
/*******************************************************
   -- Ad BA Delete Action --
*******************************************************/

         } else if (request.getParameter("adBAList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdBankAccountList adBAList =
               actionForm.getAdBAByIndex(actionForm.getRowSelected());
	 
	     try { 
	     
	        ejbBA.deleteAdBaEntry(adBAList.getBankAccountCode(), user.getCmpCode());

         } catch (GlobalRecordAlreadyAssignedException ex) {
         	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("bankAccount.error.recordAlreadyAssigned"));
	        
         } catch (GlobalRecordAlreadyDeletedException ex) {
         	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("bankAccount.error.recordAlreadyDeleted"));
                   
         } catch (EJBException ex) {
         	
                if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in AdBankAccountAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                   
                }                
      	 } 
 
            
/*******************************************************
   -- Ad BA Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adBankAccount");

            }

            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            actionForm.reset(mapping, request);


            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	actionForm.clearAdBAList();
            	           	
            	actionForm.clearBankNameList();
            	
            	list = ejbBA.getAdBnkAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	
            	actionForm.clearCurrencyList();
            	
            	list = ejbBA.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null  || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		            		
            		while (i.hasNext()) {
            			
            			GlModFunctionalCurrencyDetails mdetails = (GlModFunctionalCurrencyDetails)i.next();
            			
            		    if (mdetails.getFcSob() == 1) {
            		    	
            		    	actionForm.setCurrencyList(mdetails.getFcName());
            		    	actionForm.setCurrency(mdetails.getFcName());
            		    	
            		    } else {
            		    	
            		    	actionForm.setCurrencyList(mdetails.getFcName());
            		    	
            		    }
            			
            		}
            		
            	}
            	
            	
            	
            	
            	actionForm.clearAdBBAList();
                
                list = ejbBA.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                
                i = list.iterator();
                
                while(i.hasNext()) {
                
                    AdBranchDetails details = (AdBranchDetails)i.next();
                
                    AdBranchBankAccountList adBBAList = new AdBranchBankAccountList(actionForm,
                        details.getBrCode(),
                        details.getBrName(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null);
                    
                    if ( request.getParameter("forward") == null )
                    	adBBAList.setBranchCheckbox(true);
                                 
                    actionForm.saveAdBBAList(adBBAList);
                    
                }

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBankAccountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
         
            
            try {
		            	                        
		        list = ejbBA.getAdBaAll(user.getCmpCode());
		        
		        i = list.iterator();
		            
		        while(i.hasNext()) {
	        	            			            	
		            AdModBankAccountDetails mdetails = (AdModBankAccountDetails)i.next();
		            	
		            AdBankAccountList adBAList = new AdBankAccountList(actionForm,
	        	      mdetails.getBaCode(),	            	    
	                  mdetails.getBaBankName(),
	                  mdetails.getBaName(),
	        	      mdetails.getBaDescription(),
	        	      mdetails.getBaFcName(),
	        	      mdetails.getBaAccountType(),
	        	      mdetails.getBaAccountNumber(),
	        	      mdetails.getBaAccountUse(),
	        	      mdetails.getBaCoaCashAccount(),
	        	      mdetails.getBaCashAccountDescription(),
	        	      mdetails.getBaCoaBankChargeAccount(),
	        	      mdetails.getBaBankChargeAccountDescription(),
	        	      mdetails.getBaCoaInterestAccount(),
	        	      mdetails.getBaInterestAccountDescription(),
	        	      mdetails.getBaCoaAdjustmentAccount(),
	        	      mdetails.getBaAdjustmentAccountDescription(),
	        	      mdetails.getBaCoaSalesDiscount(),
	        	      mdetails.getBaSalesDiscountDescription(),
	        	      mdetails.getBaCoaAdvanceAccount(),
	        	      mdetails.getBaAdvanceAccountDescription(),
	        	      
	        	      Common.convertDoubleToStringMoney(mdetails.getBaAvailableBalance(), precisionUnit),	        	      
	        	      mdetails.getBaNextCheckNumber(),                         
	        	      Common.convertByteToBoolean(mdetails.getBaEnable()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaAccountNumberShow()),
	        	      String.valueOf(mdetails.getBaAccountNumberTop()),
	        	      String.valueOf(mdetails.getBaAccountNumberLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaAccountNameShow()),
	        	      String.valueOf(mdetails.getBaAccountNameTop()),
	        	      String.valueOf(mdetails.getBaAccountNameLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaNumberShow()),
	        	      String.valueOf(mdetails.getBaNumberTop()),
	        	      String.valueOf(mdetails.getBaNumberLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaDateShow()),
	        	      String.valueOf(mdetails.getBaDateTop()),
	        	      String.valueOf(mdetails.getBaDateLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaPayeeShow()),
	        	      String.valueOf(mdetails.getBaPayeeTop()),
	        	      String.valueOf(mdetails.getBaPayeeLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaAmountShow()),
	        	      String.valueOf(mdetails.getBaAmountTop()),
	        	      String.valueOf(mdetails.getBaAmountLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaWordAmountShow()),
	        	      String.valueOf(mdetails.getBaWordAmountTop()),
	        	      String.valueOf(mdetails.getBaWordAmountLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaCurrencyShow()),
	        	      String.valueOf(mdetails.getBaCurrencyTop()),
	        	      String.valueOf(mdetails.getBaCurrencyLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaAddressShow()),
	        	      String.valueOf(mdetails.getBaAddressTop()),
	        	      String.valueOf(mdetails.getBaAddressLeft()),	        	      
	        	      Common.convertByteToBoolean(mdetails.getBaMemoShow()),
	        	      String.valueOf(mdetails.getBaMemoTop()),
	        	      String.valueOf(mdetails.getBaMemoLeft()),
	        	      Common.convertByteToBoolean(mdetails.getBadocNumberShow()),
	        	      String.valueOf(mdetails.getBadocNumberTop()),
	        	      String.valueOf(mdetails.getBadocNumberLeft()),
					  String.valueOf(mdetails.getBaFontSize()),
					  mdetails.getBaFontStyle(),
					  Common.convertByteToBoolean(mdetails.getBaIsCashAccount()));

		              actionForm.saveAdBAList(adBAList);
		            	
		         }
		            
		    } catch (GlobalNoRecordFoundException ex) {
		        
		        
		    } catch (EJBException ex) {
                
                if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBankAccountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

            	if((request.getParameter("saveButton") != null || 
                        request.getParameter("updateButton") != null || 
                        request.getParameter("adBAList[" + 
                        ((AdBankAccountForm) form).getRowSelected() + "].deleteButton") != null) && 
                        ((AdBankAccountForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                        ((AdBankAccountForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
                    }
            }

            if (((AdBankAccountForm)form).getTableType() == null) {
                
                ((AdBankAccountForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
                
            }                
                
            ((AdBankAccountForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("adBankAccount"));
            

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdBankAccountAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}