package com.struts.ad.bankaccount;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ad.documentsequenceassignments.AdBranchDocumentSequenceAssignmentsList;
import com.struts.ar.customerentry.ArBranchCustomerList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdBranchDetails;
import com.util.AdModBranchBankAccountDetails;
import com.util.Debug;


public class AdBankAccountForm extends ActionForm implements Serializable {

   private Integer bankAccountCode = null;
   private String bankName = null;
   private ArrayList bankNameList = new ArrayList();
   private String accountName = null;
   private String description = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();  
   private String accountType = null;
   private String accountNumber = null;
   private String accountUse = null;
   private ArrayList accountUseList = new ArrayList();
   private String cashAccount = null;
   private String cashAccountDescription = null;
   private String adjustmentAccount = null;
   private String adjustmentAccountDescription = null;
   private String interestAccount = null;
   private String interestAccountDescription = null;
   private String bankChargeAccount = null;
   private String bankChargeAccountDescription = null;
   private String salesDiscountAccount = null;
   private String salesDiscountAccountDescription = null;
   private String advanceAccount = null;
   private String advanceAccountDescription = null;
   private String availableBalance = null;
   private String initialCheckNumber = null;
   private boolean enable = false;
   private boolean accountNumberShow = false;
   private String accountNumberTop = null;
   private String accountNumberLeft = null;
   private boolean accountNameShow = false;
   private String accountNameTop = null;
   private String accountNameLeft = null;
   private boolean numberShow = false;
   private String numberTop = null;
   private String numberLeft = null;
   private boolean dateShow = false;
   private String dateTop = null;
   private String dateLeft = null;
   private boolean payeeShow = false;
   private String payeeTop = null;
   private String payeeLeft = null;
   private boolean amountShow = false;
   private String amountTop = null;
   private String amountLeft = null;
   private boolean wordAmountShow = false;
   private String wordAmountTop = null;
   private String wordAmountLeft = null;
   private boolean currencyShow = false;
   private String currencyTop = null;
   private String currencyLeft = null;
   private boolean addressShow = false;
   private String addressTop = null;
   private String addressLeft = null;
   private boolean memoShow = false;
   private String memoTop = null;
   private String memoLeft = null;
   private boolean docNumberShow = false;
   private String docNumberTop = null;
   private String docNumberLeft = null;
   private String tableType = null;
   private String fontSize = null;
   private String fontStyle = null;
   private ArrayList fontStyleList = new ArrayList();
   private boolean isCashAccount = false;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String previewButton = null;
   
   private String report = null;
  
   private String pageState = new String();
   private ArrayList adBAList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   private ArrayList adBBAList = new ArrayList();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdBankAccountList getAdBAByIndex(int index) {

      return((AdBankAccountList)adBAList.get(index));

   }
   

   public Object[] getAdBAList() {

      return adBAList.toArray();

   }

   public int getAdBAListSize() {

      return adBAList.size();

   }

   public void saveAdBAList(Object newAdBAList) {

      adBAList.add(newAdBAList);

   }

   public void clearAdBAList() {
      adBAList.clear();
   }

   public void setRowSelected(Object selectedAdBAList, boolean isEdit) {

      this.rowSelected = adBAList.indexOf(selectedAdBAList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdBARow(ArrayList branchList, int rowSelected) {
	   
	   Debug.print("AdBankAccountForm showAdBARow");
   	
       this.bankName = ((AdBankAccountList)adBAList.get(rowSelected)).getBankName();
       this.accountName = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountName();       
       this.description = ((AdBankAccountList)adBAList.get(rowSelected)).getDescription();
       
       if (!this.currencyList.contains(((AdBankAccountList)adBAList.get(rowSelected)).getCurrency())) {  
       		
       		if (this.currencyList.contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
       			
       			this.currencyList.clear();
       			
       		}       	
       		this.currencyList.add(((AdBankAccountList)adBAList.get(rowSelected)).getCurrency());
       		
       }
       this.currency = ((AdBankAccountList)adBAList.get(rowSelected)).getCurrency();
       
       this.accountType = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountType();
       this.accountNumber = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountNumber();
       this.accountUse = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountUse();
       
       
       this.cashAccount = ((AdBankAccountList)adBAList.get(rowSelected)).getCashAccount();
       this.cashAccountDescription = ((AdBankAccountList)adBAList.get(rowSelected)).getCashAccountDescription();
       this.bankChargeAccount = ((AdBankAccountList)adBAList.get(rowSelected)).getBankChargeAccount();
       this.bankChargeAccountDescription = ((AdBankAccountList)adBAList.get(rowSelected)).getBankChargeAccountDescription();
       this.interestAccount = ((AdBankAccountList)adBAList.get(rowSelected)).getInterestAccount();
       this.interestAccountDescription = ((AdBankAccountList)adBAList.get(rowSelected)).getInterestAccountDescription();
       this.adjustmentAccount = ((AdBankAccountList)adBAList.get(rowSelected)).getAdjustmentAccount();
       this.adjustmentAccountDescription = ((AdBankAccountList)adBAList.get(rowSelected)).getAdjustmentAccountDescription();
       this.salesDiscountAccount = ((AdBankAccountList)adBAList.get(rowSelected)).getSalesDiscountAccount();
       this.salesDiscountAccountDescription = ((AdBankAccountList)adBAList.get(rowSelected)).getSalesDiscountAccountDescription();
       this.advanceAccount = ((AdBankAccountList)adBAList.get(rowSelected)).getAdvanceAccount();
       this.advanceAccountDescription = ((AdBankAccountList)adBAList.get(rowSelected)).getAdvanceAccountDescription();
       
       this.availableBalance = ((AdBankAccountList)adBAList.get(rowSelected)).getAvailableBalance();
       this.initialCheckNumber = ((AdBankAccountList)adBAList.get(rowSelected)).getInitialCheckNumber();
       this.enable = ((AdBankAccountList)adBAList.get(rowSelected)).getEnable();
       this.fontSize = ((AdBankAccountList)adBAList.get(rowSelected)).getFontSize();
       this.fontStyle = ((AdBankAccountList)adBAList.get(rowSelected)).getFontStyle();
       
       this.accountNumberShow = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountNumberShow();       
       this.accountNumberTop = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountNumberTop();       
       this.accountNumberLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountNumberLeft();       
       
       this.accountNameShow = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountNameShow();       
       this.accountNameTop = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountNameTop();       
       this.accountNameLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getAccountNameLeft();       
       
       this.numberShow = ((AdBankAccountList)adBAList.get(rowSelected)).getNumberShow();       
       this.numberTop = ((AdBankAccountList)adBAList.get(rowSelected)).getNumberTop();       
       this.numberLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getNumberLeft();       
       
       this.dateShow = ((AdBankAccountList)adBAList.get(rowSelected)).getDateShow();       
       this.dateTop = ((AdBankAccountList)adBAList.get(rowSelected)).getDateTop();       
       this.dateLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getDateLeft();       
       
       this.payeeShow = ((AdBankAccountList)adBAList.get(rowSelected)).getPayeeShow();       
       this.payeeTop = ((AdBankAccountList)adBAList.get(rowSelected)).getPayeeTop();       
       this.payeeLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getPayeeLeft();       
       
       this.amountShow = ((AdBankAccountList)adBAList.get(rowSelected)).getAmountShow();       
       this.amountTop = ((AdBankAccountList)adBAList.get(rowSelected)).getAmountTop();       
       this.amountLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getAmountLeft();       
       
       this.wordAmountShow = ((AdBankAccountList)adBAList.get(rowSelected)).getWordAmountShow();       
       this.wordAmountTop = ((AdBankAccountList)adBAList.get(rowSelected)).getWordAmountTop();       
       this.wordAmountLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getWordAmountLeft();       
       
       this.currencyShow = ((AdBankAccountList)adBAList.get(rowSelected)).getCurrencyShow();       
       this.currencyTop = ((AdBankAccountList)adBAList.get(rowSelected)).getCurrencyTop();       
       this.currencyLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getCurrencyLeft();       
       
       this.addressShow = ((AdBankAccountList)adBAList.get(rowSelected)).getAddressShow();       
       this.addressTop = ((AdBankAccountList)adBAList.get(rowSelected)).getAddressTop();       
       this.addressLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getAddressLeft();       
       
       this.memoShow = ((AdBankAccountList)adBAList.get(rowSelected)).getMemoShow();       
       this.memoTop = ((AdBankAccountList)adBAList.get(rowSelected)).getMemoTop();       
       this.memoLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getMemoLeft(); 
       
       this.docNumberShow = ((AdBankAccountList)adBAList.get(rowSelected)).getDocNumberShow();       
       this.docNumberTop = ((AdBankAccountList)adBAList.get(rowSelected)).getDocNumberTop();       
       this.docNumberLeft = ((AdBankAccountList)adBAList.get(rowSelected)).getDocNumberLeft();
       
       this.isCashAccount =  ((AdBankAccountList)adBAList.get(rowSelected)).getIsCashAccount();
       
      
       System.out.println("((AdBankAccountList)adBAList.get(rowSelected)).getBankName()="+((AdBankAccountList)adBAList.get(rowSelected)).getBankName());

       Object[] obj = this.getAdBBAList();
       
       this.clearAdBBAList();
       
       System.out.println("obj.length="+obj.length);
       System.out.println("branchList="+branchList);
       
       for(int i=0; i<obj.length; i++) {
           
           AdBranchBankAccountList bbaList = (AdBranchBankAccountList)obj[i];
           
           if(branchList != null) {
               
               Iterator brListIter = branchList.iterator();
               
               System.out.println("branchList.size()="+branchList.size());
               while(brListIter.hasNext()) {
            	   
            	   System.out.println("ITERATE");
                   //AdBranchDetails brDetails = (AdBranchDetails)brListIter.next();
                   AdModBranchBankAccountDetails brBaDetails = (AdModBranchBankAccountDetails)brListIter.next();
                   
                   System.out.println("brBaDetails.getBbaBranchCode()="+brBaDetails.getBbaBranchCode());
                   System.out.println("bbaList.getBranchCode()="+bbaList.getBranchCode());
                   
                   if(brBaDetails.getBbaBranchCode().equals(bbaList.getBranchCode())) {
                	   
                	   bbaList.setBranchCheckbox(true);  
                	   bbaList.setBranchCashAccount(brBaDetails.getBbaCashAccountNumber());
                	   bbaList.setBranchCashAccountDescription(brBaDetails.getBbaCashAccountDescription());
                	   bbaList.setBranchBankChargeAccount(brBaDetails.getBbaBankChargeAccountNumber());
                	   bbaList.setBranchBankChargeAccountDescription(brBaDetails.getBbaBankChargeAccountDescription());
                	   bbaList.setBranchInterestAccount(brBaDetails.getBbaInterestAccountNumber());
                	   bbaList.setBranchInterestAccountDescription(brBaDetails.getBbaInterestAccountDescription());
                	   bbaList.setBranchAdjustmentAccount(brBaDetails.getBbaAdjustmentAccountNumber());
                	   bbaList.setBranchAdjustmentAccountDescription(brBaDetails.getBbaAdjustmentAccountDescription());
                	   bbaList.setBranchSalesDiscountAccount(brBaDetails.getBbaSalesDiscountAccountNumber());
                	   bbaList.setBranchSalesDiscountAccountDescription(brBaDetails.getBbaSalesDiscountAccountDescription());
                	   bbaList.setBranchAdvanceAccount(brBaDetails.getBbaAdvanceAccountNumber());
                	   bbaList.setBranchAdvanceAccountDescription(brBaDetails.getBbaAdvanceAccountDescription());
                       
                       
                       break;
                       
                   }
                   
               }
           }
           
           this.adBBAList.add(bbaList);
           
       }
   }

   public void updateAdBARow(int rowSelected, Object newAdBAList) {

      adBAList.set(rowSelected, newAdBAList);

   }

   public void deleteAdBAList(int rowSelected) {

      adBAList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setPreviewButton(String previewButton) {
   	
   	  this.previewButton = previewButton;
   	
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }
   
   public String getReport() {
   	
   	  return report;
   	
   }
   
   public void setReport(String report) {
   	
   	  this.report = report;
   	
   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getBankAccountCode() {
   	
   	  return bankAccountCode;
   	
   }
   
   public void setBankAccountCode(Integer bankAccountCode) {
   	
   	  this.bankAccountCode = bankAccountCode;
   	
   }

   public String getBankName() {

      return bankName;

   }

   public void setBankName(String bankName) {

      this.bankName = bankName;

   }
   
   public ArrayList getBankNameList() {
   	
   	   return bankNameList;
   	
   }   

   public void setBankNameList(String bankName) {

      bankNameList.add(bankName);

   }
   
   public void clearBankNameList() {

      bankNameList.clear();
      bankNameList.add(Constants.GLOBAL_BLANK);
   }


   public String getAccountName() {

      return accountName;

   }

   public void setAccountName(String accountName) {

      this.accountName = accountName;

   }

   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }
   
   public String getCurrency() {
   	
   	   return currency;
   	
   }
   
   public void setCurrency(String currency) {
   	
   	   this.currency = currency;
   	
   }
   
   public ArrayList getCurrencyList() {
   	
   	   return currencyList;
   	
   }
   
   public void clearCurrencyList() {
   	
   	   currencyList.clear();   	   
   	
   }
   
   public void setCurrencyList(String currency) {
   	
   	   currencyList.add(currency);
   	
   }

   public String getAccountType() {

      return accountType;

   }

   public void setAccountType(String accountType) {

      this.accountType = accountType;

   }

   public String getAccountNumber() {

      return accountNumber;

   }

   public void setAccountNumber(String accountNumber) {

      this.accountNumber = accountNumber;

   }

   public String getAccountUse() {

      return accountUse;

   }

   public void setAccountUse(String accountUse) {

      this.accountUse = accountUse;

   }

   public ArrayList getAccountUseList() {
   	
   	   return accountUseList;
   	
   }

   public String getCashAccount() {

      return cashAccount;

   }

   public void setCashAccount(String cashAccount) {

      this.cashAccount = cashAccount;

   }

   public String getCashAccountDescription() {

      return cashAccountDescription;

   }

   public void setCashAccountDescription(String cashAccountDescription) {

      this.cashAccountDescription = cashAccountDescription;

   }
   
   public String getBankChargeAccount() {

      return bankChargeAccount;

   }

   public void setBankChargeAccount(String bankChargeAccount) {

      this.bankChargeAccount = bankChargeAccount;

   }

   public String getBankChargeAccountDescription() {

      return bankChargeAccountDescription;

   }

   public void setBankChargeAccountDescription(String bankChargeAccountDescription) {

      this.bankChargeAccountDescription = bankChargeAccountDescription;

   }
   
   public String getInterestAccount() {

      return interestAccount;

   }

   public void setInterestAccount(String interestAccount) {

      this.interestAccount = interestAccount;

   }

   public String getInterestAccountDescription() {

      return interestAccountDescription;

   }

   public void setInterestAccountDescription(String interestAccountDescription) {

      this.interestAccountDescription = interestAccountDescription;

   }
   
   public String getAdjustmentAccount() {

      return adjustmentAccount;

   }

   public void setAdjustmentAccount(String adjustmentAccount) {

      this.adjustmentAccount = adjustmentAccount;

   }

   public String getAdjustmentAccountDescription() {

      return adjustmentAccountDescription;

   }

   public void setAdjustmentAccountDescription(String adjustmentAccountDescription) {

      this.adjustmentAccountDescription = adjustmentAccountDescription;

   }

   public String getSalesDiscountAccount() {

      return salesDiscountAccount;

   }

   public void setSalesDiscountAccount(String salesDiscountAccount) {

      this.salesDiscountAccount = salesDiscountAccount;

   }

   public String getSalesDiscountAccountDescription() {

      return salesDiscountAccountDescription;

   }

   public void setSalesDiscountAccountDescription(String salesDiscountAccountDescription) {

      this.salesDiscountAccountDescription = salesDiscountAccountDescription;

   }
   
   
   
   
   public String getAdvanceAccount() {

	      return advanceAccount;

	   }

	   public void setAdvanceAccount(String advanceAccount) {

	      this.advanceAccount = advanceAccount;

	   }

	   public String getAdvanceAccountDescription() {

	      return advanceAccountDescription;

	   }

	   public void setAdvanceAccountDescription(String advanceAccountDescription) {

	      this.advanceAccountDescription = advanceAccountDescription;

	   }
	   

   public String getAvailableBalance() {

      return availableBalance;

   }

   public void setAvailableBalance(String availableBalance) {

      this.availableBalance = availableBalance;

   }

   public String getInitialCheckNumber() {

      return initialCheckNumber;

   }

   public void setInitialCheckNumber(String initialCheckNumber) {

      this.initialCheckNumber = initialCheckNumber;

   }

   public boolean getEnable() {

      return enable;

   }

   public void setEnable(boolean enable) {

      this.enable = enable;

   } 
   
   
   public boolean getAccountNumberShow() {
   	
   	   return accountNumberShow;
   	
   }
   
   public void setAccountNumberShow(boolean accountNumberShow) {
   	
   	   this.accountNumberShow = accountNumberShow;
   	
   }
   
   public String getAccountNumberTop() {
   	
   	   return accountNumberTop;
   	
   }
   
   public void setAccountNumberTop(String accountNumberTop) {
   	
   	   this.accountNumberTop = accountNumberTop;
   	
   }
   
   public String getAccountNumberLeft() {
   	
   	  return accountNumberLeft;
   	
   }
   
   public void setAccountNumberLeft(String accountNumberLeft) {
   	
   	   this.accountNumberLeft = accountNumberLeft;
   	
   }
   
   
   public boolean getAccountNameShow() {
   	
   	   return accountNameShow;
   	
   }
   
   public void setAccountNameShow(boolean accountNameShow) {
   	
   	   this.accountNameShow = accountNameShow;
   	
   }
   
   public String getAccountNameTop() {
   	
   	   return accountNameTop;
   	
   }
   
   public void setAccountNameTop(String accountNameTop) {
   	
   	   this.accountNameTop = accountNameTop;
   	
   }
   
   public String getAccountNameLeft() {
   	
   	  return accountNameLeft;
   	
   }
   
   public void setAccountNameLeft(String accountNameLeft) {
   	
   	   this.accountNameLeft = accountNameLeft;
   	
   }
   
   public boolean getNumberShow() {
   	
   	   return numberShow;
   	
   }
   
   public void setNumberShow(boolean numberShow) {
   	
   	   this.numberShow = numberShow;
   	
   }
   
   public String getNumberTop() {
   	
   	   return numberTop;
   	
   }
   
   public void setNumberTop(String numberTop) {
   	
   	   this.numberTop = numberTop;
   	
   }
   
   public String getNumberLeft() {
   	
   	  return numberLeft;
   	
   }
   
   public void setNumberLeft(String numberLeft) {
   	
   	   this.numberLeft = numberLeft;
   	
   }
   
   public boolean getDateShow() {
   	
   	   return dateShow;
   	
   }
   
   public void setDateShow(boolean dateShow) {
   	
   	   this.dateShow = dateShow;
   	
   }
   
   public String getDateTop() {
   	
   	   return dateTop;
   	
   }
   
   public void setDateTop(String dateTop) {
   	
   	   this.dateTop = dateTop;
   	
   }
   
   public String getDateLeft() {
   	
   	  return dateLeft;
   	
   }
   
   public void setDateLeft(String dateLeft) {
   	
   	   this.dateLeft = dateLeft;
   	
   }
   
   public boolean getPayeeShow() {
   	
   	   return payeeShow;
   	
   }
   public void setPayeeShow(boolean payeeShow) {
   	
   	   this.payeeShow = payeeShow;
   	
   }
   
   public String getPayeeTop() {
   	
   	   return payeeTop;
   	
   }
   
   public void setPayeeTop(String payeeTop) {
   	
   	   this.payeeTop = payeeTop;
   	
   }
   
   public String getPayeeLeft() {
   	
   	  return payeeLeft;
   	
   }
   
   public void setPayeeLeft(String payeeLeft) {
   	
   	   this.payeeLeft = payeeLeft;
   	
   }
   
   public boolean getAmountShow() {
   	
   	   return amountShow;
   	
   }
   
   public void setAmountShow(boolean amountShow) {
   	
   	   this.amountShow = amountShow;
   	
   }
   
   public String getAmountTop() {
   	
   	   return amountTop;
   	
   }
   
   public void setAmountTop(String amountTop) {
   	
   	   this.amountTop = amountTop;
   	
   }
   
   public String getAmountLeft() {
   	
   	  return amountLeft;
   	
   }
   
   public void setAmountLeft(String amountLeft) {
   	
   	   this.amountLeft = amountLeft;
   	
   }
   
   public boolean getWordAmountShow() {
   	
   	   return wordAmountShow;
   	
   }
   
   public void setWordAmountShow(boolean wordAmountShow) {
   	
   	   this.wordAmountShow = wordAmountShow;
   	
   }
   
   public String getWordAmountTop() {
   	
   	   return wordAmountTop;
   	
   }
   
   public void setWordAmountTop(String wordAmountTop) {
   	
   	   this.wordAmountTop = wordAmountTop;
   	
   }
   
   public String getWordAmountLeft() {
   	
   	  return wordAmountLeft;
   	
   }
   
   public void setWordAmountLeft(String wordAmountLeft) {
   	
   	   this.wordAmountLeft = wordAmountLeft;
   	
   }
   
   public boolean getCurrencyShow() {
   	
   	   return currencyShow;
   	
   }
   
   public void setCurrencyShow(boolean currencyShow) {
   	
   	   this.currencyShow = currencyShow;
   	
   }
   
   public String getCurrencyTop() {
   	
   	   return currencyTop;
   	
   }
   
   public void setCurrencyTop(String currencyTop) {
   	
   	   this.currencyTop = currencyTop;
   	
   }
   
   public String getCurrencyLeft() {
   	
   	  return currencyLeft;
   	
   }
   
   public void setCurrencyLeft(String currencyLeft) {
   	
   	   this.currencyLeft = currencyLeft;
   	
   }
   
   
   public boolean getAddressShow() {
   	
   	   return addressShow;
   	
   }
   
   public void setAddressShow(boolean addressShow) {
   	
   	   this.addressShow = addressShow;
   	
   }
   
   public String getAddressTop() {
   	
   	   return addressTop;
   	
   }
   
   public void setAddressTop(String addressTop) {
   	
   	   this.addressTop = addressTop;
   	
   }
   
   public String getAddressLeft() {
   	
   	  return addressLeft;
   	
   }
   
   public void setAddressLeft(String addressLeft) {
   	
   	   this.addressLeft = addressLeft;
   	
   }
   
   public boolean getMemoShow() {
   	
   	   return memoShow;
   	
   }
   
   public void setMemoShow(boolean memoShow) {
   	
   	   this.memoShow = memoShow;
   	
   }
   
   public String getMemoTop() {
   	
   	   return memoTop;
   	
   }
   
   public void setMemoTop(String memoTop) {
   	
   	   this.memoTop = memoTop;
   	
   }
   
   public String getMemoLeft() {
   	
   	  return memoLeft;
   	
   }   
   
   public void setMemoLeft(String memoLeft) {
   	
   	   this.memoLeft = memoLeft;
   	
   }

   public boolean getDocNumberShow() {
   	
   	   return docNumberShow;
   	
   }
   
   public void setDocNumberShow(boolean docNumberShow) {
   	
   	   this.docNumberShow = docNumberShow;
   	
   }
   
   public String getDocNumberTop() {
   	
   	   return docNumberTop;
   	
   }
   
   public void setDocNumberTop(String docNumberTop) {
   	
   	   this.docNumberTop = docNumberTop;
   	
   }
   
   public String getDocNumberLeft() {
   	
   	  return docNumberLeft;
   	
   }   
   
   public void setDocNumberLeft(String docNumberLeft) {
   	
   	   this.docNumberLeft = docNumberLeft;
   	
   }
   
   public String getFontSize() {
   	
   	   return fontSize;
   	   
   }
   
   public void setFontSize(String fontSize) {
   	
   	   this.fontSize = fontSize;
   	   
   }
   
   public ArrayList getFontStyleList() {
   	
   	   return fontStyleList;
   	 
   }
   
   public String getFontStyle() {
   	
   	   return fontStyle;
   	   
   }
   
   public void setFontStyle(String fontStyle) {
   	
   	   this.fontStyle = fontStyle;
   	   
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }      
   
   public Object[] getAdBBAList(){
       
       return adBBAList.toArray();
       
   }
   
   public AdBranchBankAccountList getAdBBAByIndex(int index){
       
       return ((AdBranchBankAccountList)adBBAList.get(index));
       
   }
   
   public int getAdBBAListSize(){
       
       return(adBBAList.size());
       
   }
   
   public void saveAdBBAList(Object newAdBBAList){
       
       adBBAList.add(newAdBBAList);   	  
       
   }
   
   public void clearAdBBAList(){
       
       adBBAList.clear();
       
   }
   
   public void setAdBBAList(ArrayList adBBAList) {
       
       this.adBBAList = adBBAList;
       
   }

   public boolean getIsCashAccount() {
   	
   	return isCashAccount;
   	
   }
   
   public void setIsCashAccount(boolean isCashAccount) {
   	
   	this.isCashAccount = isCashAccount;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {
         
       Debug.print("Actionform reset");
       System.out.println("adBBAList.size()="+adBBAList.size());
       for (int i=0; i<adBBAList.size(); i++) {
           
           AdBranchBankAccountList list = (AdBranchBankAccountList)adBBAList.get(i);
           list.setBranchCheckbox(false);
           list.setBranchCashAccount(null);
           list.setBranchCashAccountDescription(null);
           list.setBranchBankChargeAccount(null);
           list.setBranchBankChargeAccountDescription(null);
           list.setBranchInterestAccount(null);
           list.setBranchInterestAccountDescription(null);
           list.setBranchAdjustmentAccount(null);
           list.setBranchAdjustmentAccountDescription(null);
           list.setBranchSalesDiscountAccount(null);
           list.setBranchSalesDiscountAccountDescription(null);
           list.setBranchAdvanceAccount(null);
           list.setBranchAdvanceAccountDescription(null);
       }  
       

       
		bankName = Constants.GLOBAL_BLANK; 
		accountName = null;
		description = null;
		accountType = null;
		accountNumber = null;
		accountUseList.clear();
		accountUseList.add(Constants.AD_BANK_ACCOUNT_USE_INTERNAL);
		accountUseList.add(Constants.AD_BANK_ACCOUNT_USE_SUPPLIER);                
		accountUse = Constants.AD_BANK_ACCOUNT_USE_INTERNAL;
		cashAccount = null;
		cashAccountDescription = null;
		bankChargeAccount = null;
		bankChargeAccountDescription = null;
		interestAccount = null;
		interestAccountDescription = null;
		adjustmentAccount = null;
		adjustmentAccountDescription = null;
		salesDiscountAccount = null;
		salesDiscountAccountDescription = null;
		advanceAccount = null;
		advanceAccountDescription = null;
		availableBalance = null;
		initialCheckNumber = null;
		enable = false;
		accountNumberShow = false;
	    accountNumberTop = null;
	    accountNumberLeft = null;
	    accountNameShow = false;
	    accountNameTop = null;
	    accountNameLeft = null;
	    numberShow = false;
	    numberTop = null;
	    numberLeft = null;
	    dateShow = false;
	    dateTop = null;
	    dateLeft = null;
	    payeeShow = false;
	    payeeTop = null;
	    payeeLeft = null;
	    amountShow = false;
	    amountTop = null;
	    amountLeft = null;
	    wordAmountShow = false;
	    wordAmountTop = null;
	    wordAmountLeft = null;
	    currencyShow = false;
	    currencyTop = null;
	    currencyLeft = null;
	    addressShow = false;
	    addressTop = null;
	    addressLeft = null;
	    memoShow = false;
	    memoTop = null;
	    memoLeft = null;
	    docNumberShow = false;
	    docNumberTop = null;
	    docNumberLeft = null;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
	    showDetailsButton = null;
	    hideDetailsButton = null;		
		report = null;
		fontSize = null;
		isCashAccount = false;
		
		fontStyleList.clear();
		fontStyleList.add("Helvetica");
		fontStyleList.add("Courier");
		fontStyleList.add("Times-Roman");
		
		fontStyle = "Helvetica";
		
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null
      		|| request.getParameter("previewButton") != null) {
      	  
      	  if (!Common.validateNumberFormat(accountNumberTop)) {
         	
         	errors.add("accountNumberTop",
               new ActionMessage("bankAccount.error.accountNumberTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(accountNumberLeft)) {
         	
         	errors.add("accountNumberLeft",
               new ActionMessage("bankAccount.error.accountNumberLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(accountNameTop)) {
         	
         	errors.add("accountNameTop",
               new ActionMessage("bankAccount.error.accountNameTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(accountNameLeft)) {
         	
         	errors.add("accountNameLeft",
               new ActionMessage("bankAccount.error.accountNameLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(numberTop)) {
         	
         	errors.add("numberTop",
               new ActionMessage("bankAccount.error.numberTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(numberLeft)) {
         	
         	errors.add("numberLeft",
               new ActionMessage("bankAccount.error.numberLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(dateTop)) {
         	
         	errors.add("dateTop",
               new ActionMessage("bankAccount.error.dateTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(dateLeft)) {
         	
         	errors.add("dateLeft",
               new ActionMessage("bankAccount.error.dateLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(payeeTop)) {
         	
         	errors.add("payeeTop",
               new ActionMessage("bankAccount.error.payeeTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(payeeLeft)) {
         	
         	errors.add("payeeLeft",
               new ActionMessage("bankAccount.error.payeeLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(amountTop)) {
         	
         	errors.add("amountTop",
               new ActionMessage("bankAccount.error.amountTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(amountLeft)) {
         	
         	errors.add("amountLeft",
               new ActionMessage("bankAccount.error.amountLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(wordAmountTop)) {
         	
         	errors.add("wordAmountTop",
               new ActionMessage("bankAccount.error.wordAmountTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(wordAmountLeft)) {
         	
         	errors.add("wordAmountLeft",
               new ActionMessage("bankAccount.error.wordAmountLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(currencyTop)) {
         	
         	errors.add("currencyTop",
               new ActionMessage("bankAccount.error.currencyTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(currencyLeft)) {
         	
         	errors.add("currencyLeft",
               new ActionMessage("bankAccount.error.currencyLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(addressTop)) {
         	
         	errors.add("addressTop",
               new ActionMessage("bankAccount.error.addressTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(addressLeft)) {
         	
         	errors.add("addressLeft",
               new ActionMessage("bankAccount.error.addressLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(memoTop)) {
         	
         	errors.add("memoTop",
               new ActionMessage("bankAccount.error.memoTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(memoLeft)) {
         	
         	errors.add("memoLeft",
               new ActionMessage("bankAccount.error.memoLeftInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(docNumberTop)) {
         	
         	errors.add("docNumberTop",
               new ActionMessage("bankAccount.error.docNumberTopInvalid"));
         	
         } 
         
         if (!Common.validateNumberFormat(docNumberLeft)) {
         	
         	errors.add("docNumberLeft",
               new ActionMessage("bankAccount.error.docNumberLeftInvalid"));
         	
         } 
         
         if ((!Common.validateNumberFormat(fontSize)) || (Common.convertStringToInt(fontSize) > 13)) {
         	
         	errors.add("fontSize",
               new ActionMessage("bankAccount.error.fontSizeInvalid"));
         	
         } 
      	
         if (Common.validateRequired(fontSize)) {

            errors.add("fontSize",
               new ActionMessage("bankAccount.error.fontSizeRequired"));

         }
          
      }
      
      if (request.getParameter("saveButton") != null) {
         
         
         if (Common.validateRequired(currency)) {

            errors.add("currency",
               new ActionMessage("bankAccount.error.currencyRequired"));

         }
         
         if (Common.validateRequired(bankName)) {

            errors.add("bankName",
               new ActionMessage("bankAccount.error.bankNameRequired"));

         }

         if (Common.validateRequired(cashAccount)) {

            errors.add("cashAccount",
               new ActionMessage("bankAccount.error.cashAccountRequired"));

         }
                 
         
         if (Common.validateRequired(bankChargeAccount)) {

            errors.add("bankChargeAccount",
               new ActionMessage("bankAccount.error.bankChargeAccountRequired"));

         }
         
         if (Common.validateRequired(interestAccount)) {

            errors.add("interestAccount",
               new ActionMessage("bankAccount.error.interestAccountRequired"));

         }

         if (Common.validateRequired(adjustmentAccount)) {

            errors.add("adjustmentAccount",
               new ActionMessage("bankAccount.error.adjustmentAccountRequired"));

         }

         if (Common.validateRequired(salesDiscountAccount)) {

            errors.add("salesDiscountAccount",
               new ActionMessage("bankAccount.error.salesDiscountRequired"));

         }
         
         if (Common.validateRequired(advanceAccount)) {

             errors.add("advanceAccount",
                new ActionMessage("bankAccount.error.advanceAccountRequired"));

          }
         
     
      }
         
      return errors;

   }
}