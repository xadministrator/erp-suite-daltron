package com.struts.ad.bankaccount;

import java.io.Serializable;

public class AdBankAccountList implements Serializable {

   private Integer bankAccountCode = null;
   private String bankName = null;
   private String accountName = null;
   private String description = null;
   private String currency = null;
   private String accountType = null;
   private String accountNumber = null;
   private String accountUse = null;
   private String cashAccount = null;
   private String cashAccountDescription = null;
   private String bankChargeAccount = null;
   private String bankChargeAccountDescription = null;
   private String interestAccount = null;
   private String interestAccountDescription = null;
   private String adjustmentAccount = null;
   private String adjustmentAccountDescription = null;
   private String salesDiscountAccount = null;
   private String salesDiscountAccountDescription = null;
   private String advanceAccount = null;
   private String advanceAccountDescription = null;
   private String availableBalance = null;
   private String initialCheckNumber = null;
   private boolean enable = false;
   private boolean accountNumberShow = false;
   private String accountNumberTop = null;
   private String accountNumberLeft = null;
   private boolean accountNameShow = false;
   private String accountNameTop = null;
   private String accountNameLeft = null;
   private boolean numberShow = false;
   private String numberTop = null;
   private String numberLeft = null;
   private boolean dateShow = false;
   private String dateTop = null;
   private String dateLeft = null;
   private boolean payeeShow = false;
   private String payeeTop = null;
   private String payeeLeft = null;
   private boolean amountShow = false;
   private String amountTop = null;
   private String amountLeft = null;
   private boolean wordAmountShow = false;
   private String wordAmountTop = null;
   private String wordAmountLeft = null;
   private boolean currencyShow = false;
   private String currencyTop = null;
   private String currencyLeft = null;
   private boolean addressShow = false;
   private String addressTop = null;
   private String addressLeft = null;
   private boolean memoShow = false;
   private String memoTop = null;
   private String memoLeft = null;  
   private boolean docNumberShow = false;
   private String docNumberTop = null;
   private String docNumberLeft = null; 
   private String fontSize = null;
   private String fontStyle = null;
   
   private String editButton = null;
   private String deleteButton = null;
   
   private boolean isCashAccount = false;
    
   private AdBankAccountForm parentBean;
    
   public AdBankAccountList(AdBankAccountForm parentBean,
		Integer bankAccountCode,
		String bankName,
		String accountName,
		String description,
		String currency,
		String accountType,
		String accountNumber,
		String accountUse,
		String cashAccount,
		String cashAccountDescription,
		String bankChargeAccount,
		String bankChargeAccountDescription,
		String interestAccount,
		String interestAccountDescription,
		String adjustmentAccount,
		String adjustmentAccountDescription,
		String salesDiscountAccount,
		String salesDiscountAccountDescription,
		String advanceAccount,
		String advanceAccountDescription,
		String availableBalance,
		String initialCheckNumber,
		boolean enable,
		boolean accountNumberShow, 
		String accountNumberTop,
		String accountNumberLeft,
		boolean accountNameShow, 
		String accountNameTop,
		String accountNameLeft,
		boolean numberShow, 
		String numberTop,
		String numberLeft,
		boolean dateShow, 
		String dateTop,
		String dateLeft,
		boolean payeeShow, 
		String payeeTop,
		String payeeLeft,
		boolean amountShow, 
		String amountTop,
		String amountLeft,
		boolean wordAmountShow, 
		String wordAmountTop,
		String wordAmountLeft,
		boolean currencyShow, 
		String currencyTop,
		String currencyLeft,
		boolean addressShow, 
		String addressTop,
		String addressLeft,
		boolean memoShow, 
		String memoTop,
		String memoLeft,
		boolean docNumberShow,
		String docNumberTop,
		String docNumberLeft,
		String fontSize,
		String fontStyle,
		boolean isCashAccount) {
		
		this.parentBean = parentBean;
        this.bankAccountCode = bankAccountCode;
        this.bankName = bankName;
        this.accountName = accountName;
        this.description = description;
        this.currency = currency;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
        this.accountUse = accountUse;
        this.cashAccount = cashAccount;
        this.cashAccountDescription = cashAccountDescription;
        this.bankChargeAccount = bankChargeAccount;
        this.bankChargeAccountDescription = bankChargeAccountDescription;
        this.interestAccount = interestAccount;
        this.interestAccountDescription = interestAccountDescription;
        this.adjustmentAccount = adjustmentAccount;
        this.adjustmentAccountDescription = adjustmentAccountDescription;
        this.salesDiscountAccount = salesDiscountAccount;
        this.salesDiscountAccountDescription = salesDiscountAccountDescription;
        this.advanceAccount = advanceAccount;
        this.advanceAccountDescription = advanceAccountDescription;
        
        this.availableBalance = availableBalance;
        this.initialCheckNumber = initialCheckNumber;        
		this.enable = enable;
		this.accountNumberShow = accountNumberShow;
		this.accountNumberTop = accountNumberTop;
		this.accountNumberLeft = accountNumberLeft;
		this.accountNameShow = accountNameShow;
		this.accountNameTop = accountNameTop;
		this.accountNameLeft = accountNameLeft;
		this.numberShow = numberShow;
		this.numberTop = numberTop;
		this.numberLeft = numberLeft;
		this.dateShow = dateShow;
		this.dateTop = dateTop;
		this.dateLeft = dateLeft;
		this.payeeShow = payeeShow;
		this.payeeTop = payeeTop;
		this.payeeLeft = payeeLeft;
		this.amountShow = amountShow;
		this.amountTop = amountTop;
		this.amountLeft = amountLeft;
		this.wordAmountShow = wordAmountShow;
		this.wordAmountTop = wordAmountTop;
		this.wordAmountLeft = wordAmountLeft;
		this.currencyShow = currencyShow;
		this.currencyTop = currencyTop;
		this.currencyLeft = currencyLeft;
		this.addressShow = addressShow;
		this.addressTop = addressTop;
		this.addressLeft = addressLeft;
		this.memoShow = memoShow;
		this.memoTop = memoTop;
		this.memoLeft = memoLeft;
		this.docNumberShow = docNumberShow;
		this.docNumberTop = docNumberTop;
		this.docNumberLeft = docNumberLeft;
		this.fontSize = fontSize;
		this.fontStyle = fontStyle;
		this.isCashAccount = isCashAccount;
      
   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }

   public void setDeleteButton(String deleteButton) {
   	
   	  parentBean.setRowSelected(this, false);
   	
   }

   public Integer getBankAccountCode() {

      return bankAccountCode;

   }
   
   public String getBankName() {
       
       return bankName;
       
   }

   public String getAccountName() {

      return accountName;

   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }
   
   public String getCurrency() {
   	
   	  return currency;
   	
   }
   
   public String getAccountType() {
   	
   	  return accountType;
   	  
   }
   
   public String getAccountNumber() {
   	
   	  return accountNumber;
   	  
   }
   
   public String getAccountUse() {
   	
   	  return accountUse;
   	  
   }
   
   public String getCashAccount() {
   	
   	  return cashAccount;
   	  
   }
   
   public String getCashAccountDescription() {
   	
   	  return cashAccountDescription;
   	  
   }
      
   public String getBankChargeAccount() {
   	
   	  return bankChargeAccount;
   	  
   }
   
   public String getBankChargeAccountDescription() {
   	
   	  return bankChargeAccountDescription;
   	  
   }
   
   public String getInterestAccount() {
   	
   	   return interestAccount;
   	
   }
   
   public String getInterestAccountDescription() {
   	
   	   return interestAccountDescription;
   	
   }
   
   public String getAdjustmentAccount() {
   	
   	   return adjustmentAccount;
   	
   }
   
   public String getAdjustmentAccountDescription() {
   	
   	   return adjustmentAccountDescription;
   	
   }
      
   public String getSalesDiscountAccount() {
   	  
   	  return salesDiscountAccount;
   	  
   }
   
   public String getSalesDiscountAccountDescription() {
   	
   	  return salesDiscountAccountDescription;
   	  
   }
   
   
   public String getAdvanceAccount() {
	   	  
	   	  return advanceAccount;
	   	  
	   }
	   
	   public String getAdvanceAccountDescription() {
	   	
	   	  return advanceAccountDescription;
	   	  
	   }
      
   public String getAvailableBalance() {
   	
   	  return availableBalance;
   	  
   }
      
   public String getInitialCheckNumber() {
   	
   	  return initialCheckNumber;
   	  
   }    
  
   public boolean getEnable() {
       
       return enable;
       
   }
   
   public boolean getAccountNumberShow() {
   	
   	   return accountNumberShow;
   	
   }
   
   public String getAccountNumberTop() {
   	
   	   return accountNumberTop;
   	
   }
   
   public String getAccountNumberLeft() {
   	
   	  return accountNumberLeft;
   	
   }
   
   
   public boolean getAccountNameShow() {
   	
   	   return accountNameShow;
   	
   }
   
   public String getAccountNameTop() {
   	
   	   return accountNameTop;
   	
   }
   
   public String getAccountNameLeft() {
   	
   	  return accountNameLeft;
   	
   }
   
   public boolean getNumberShow() {
   	
   	   return numberShow;
   	
   }
   
   public String getNumberTop() {
   	
   	   return numberTop;
   	
   }
   
   public String getNumberLeft() {
   	
   	  return numberLeft;
   	
   }
   
   public boolean getDateShow() {
   	
   	   return dateShow;
   	
   }
   
   public String getDateTop() {
   	
   	   return dateTop;
   	
   }
   
   public String getDateLeft() {
   	
   	  return dateLeft;
   	
   }
   
   public boolean getPayeeShow() {
   	
   	   return payeeShow;
   	
   }
   
   public String getPayeeTop() {
   	
   	   return payeeTop;
   	
   }
   
   public String getPayeeLeft() {
   	
   	  return payeeLeft;
   	
   }
   
   public boolean getAmountShow() {
   	
   	   return amountShow;
   	
   }
   
   public String getAmountTop() {
   	
   	   return amountTop;
   	
   }
   
   public String getAmountLeft() {
   	
   	  return amountLeft;
   	
   }
   
   public boolean getWordAmountShow() {
   	
   	   return wordAmountShow;
   	
   }
   
   public String getWordAmountTop() {
   	
   	   return wordAmountTop;
   	
   }
   
   public String getWordAmountLeft() {
   	
   	  return wordAmountLeft;
   	
   }
   
   public boolean getCurrencyShow() {
   	
   	   return currencyShow;
   	
   }
   
   public String getCurrencyTop() {
   	
   	   return currencyTop;
   	
   }
   
   public String getCurrencyLeft() {
   	
   	  return currencyLeft;
   	
   }
   
   
   public boolean getAddressShow() {
   	
   	   return addressShow;
   	
   }
   
   public String getAddressTop() {
   	
   	   return addressTop;
   	
   }
   
   public String getAddressLeft() {
   	
   	  return addressLeft;
   	
   }
   
   public boolean getMemoShow() {
   	
   	   return memoShow;
   	
   }
   
   public String getMemoTop() {
   	
   	   return memoTop;
   	
   }
   
   public String getMemoLeft() {
   	
   	  return memoLeft;
   	
   }

   public boolean getDocNumberShow() {
   	
   	   return docNumberShow;
   	
   }
   
   public String getDocNumberTop() {
   	
   	   return docNumberTop;
   	
   }
   
   public String getDocNumberLeft() {
   	
   	  return docNumberLeft;
   	
   }
   
   public String getFontSize() {
   	
   	  return fontSize;
   	  
   }
   
   public String getFontStyle() {
   	
   	  return fontStyle;
   	  
   }
   
   public boolean getIsCashAccount() {
   	
   	  return isCashAccount;
   	  
   }


}