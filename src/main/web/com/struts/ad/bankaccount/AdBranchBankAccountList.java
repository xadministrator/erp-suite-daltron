package com.struts.ad.bankaccount;

import java.io.Serializable;

public class AdBranchBankAccountList implements Serializable {
	
	
	private Integer branchCode = null;
	private String branchName = null;
	
	private boolean branchCheckbox = false;	
	private String branchCashAccount = null;
	private String branchCashAccountDescription = null;
	private String branchBankChargeAccount = null;
	private String branchBankChargeAccountDescription = null;
	private String branchInterestAccount = null;
	private String branchInterestAccountDescription = null;
	private String branchAdjustmentAccount = null;
	private String branchAdjustmentAccountDescription = null;
	private String branchSalesDiscountAccount = null;
	private String branchSalesDiscountAccountDescription = null;
	private String branchAdvanceAccount = null;
	private String branchAdvanceAccountDescription = null;
	
	private AdBankAccountForm parentBean;
	
	public AdBranchBankAccountList(AdBankAccountForm parentBean,
			Integer branchCode,
			String branchName,
			String branchCashAccount,
			String branchCashAccountDescription,
			String branchBankChargeAccount,
			String branchBankChargeAccountDescription,
			String branchInterestAccount,
			String branchInterestAccountDescription,
			String branchAdjustmentAccount,
			String branchAdjustmentAccountDescription,
			String branchSalesDiscountAccount,
			String branchSalesDiscountAccountDescription,
			String branchAdvanceAccount,
			String branchAdvanceAccountDescription
			
			) {
		
		this.parentBean = parentBean;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.branchCashAccount = branchCashAccount;
		this.branchCashAccountDescription = branchCashAccountDescription;
		this.branchBankChargeAccount = branchBankChargeAccount;
		this.branchBankChargeAccountDescription = branchBankChargeAccountDescription;
		this.branchInterestAccount = branchInterestAccount;
		this.branchInterestAccountDescription = branchInterestAccountDescription;
		this.branchAdjustmentAccount = branchAdjustmentAccount;
		this.branchAdjustmentAccountDescription = branchAdjustmentAccountDescription;
		this.branchSalesDiscountAccount = branchSalesDiscountAccount;
		this.branchSalesDiscountAccountDescription = branchSalesDiscountAccountDescription;
		this.branchAdvanceAccount = branchAdvanceAccount;
		this.branchAdvanceAccountDescription = branchAdvanceAccountDescription;
		
		
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBranchCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
	
	
	
	
	
	
	
	
	
public String getBranchCashAccount() {
		
		return this.branchCashAccount;
		
	}
	
	public void setBranchCashAccount(String branchCashAccount) {
		
		this.branchCashAccount = branchCashAccount;
		
	}
	
	public String getBranchCashAccountDescription() {
		
		return this.branchCashAccountDescription;
		
	}
	
	public void setBranchCashAccountDescription(String branchCashAccountDescription) {
		
		this.branchCashAccountDescription = branchCashAccountDescription;
		
	}
	
	public String getBranchBankChargeAccount() {
		
		return this.branchBankChargeAccount;
		
	}
	
	public void setBranchBankChargeAccount(String branchBankChargeAccount) {
		
		this.branchBankChargeAccount = branchBankChargeAccount;
		
	}
	
	public String getBranchBankChargeAccountDescription() {
		
		return this.branchBankChargeAccountDescription;
		
	}
	
	public void setBranchBankChargeAccountDescription(String branchBankChargeAccountDescription) {
		
		this.branchBankChargeAccountDescription = branchBankChargeAccountDescription;
		
	}
	
	
	
	
	public String getBranchInterestAccount() {
		
		return this.branchInterestAccount;
		
	}
	
	public void setBranchInterestAccount(String branchInterestAccount) {
		
		this.branchInterestAccount = branchInterestAccount;
		
	}
	
	public String getBranchInterestAccountDescription() {
		
		return this.branchInterestAccountDescription;
		
	}
	
	public void setBranchInterestAccountDescription(String branchInterestAccountDescription) {
		
		this.branchInterestAccountDescription = branchInterestAccountDescription;
		
	}
	
	
	
	public String getBranchAdjustmentAccount() {
		
		return this.branchAdjustmentAccount;
		
	}
	
	public void setBranchAdjustmentAccount(String branchAdjustmentAccount) {
		
		this.branchAdjustmentAccount = branchAdjustmentAccount;
		
	}
	
	public String getBranchAdjustmentAccountDescription() {
		
		return this.branchAdjustmentAccountDescription;
		
	}
	
	public void setBranchAdjustmentAccountDescription(String branchAdjustmentAccountDescription) {
		
		this.branchAdjustmentAccountDescription = branchAdjustmentAccountDescription;
		
	}
	
	
	
	
public String getBranchSalesDiscountAccount() {
		
		return this.branchSalesDiscountAccount;
		
	}
	
	public void setBranchSalesDiscountAccount(String branchSalesDiscountAccount) {
		
		this.branchSalesDiscountAccount = branchSalesDiscountAccount;
		
	}
	
	public String getBranchSalesDiscountAccountDescription() {
		
		return this.branchSalesDiscountAccountDescription;
		
	}
	
	public void setBranchSalesDiscountAccountDescription(String branchSalesDiscountAccountDescription) {
		
		this.branchSalesDiscountAccountDescription = branchSalesDiscountAccountDescription;
		
	}
	
	
public String getBranchAdvanceAccount() {
		
		return this.branchAdvanceAccount;
		
	}
	
	public void setBranchAdvanceAccount(String branchAdvanceAccount) {
		
		this.branchAdvanceAccount = branchAdvanceAccount;
		
	}
	
	public String getBranchAdvanceAccountDescription() {
		
		return this.branchAdvanceAccountDescription;
		
	}
	
	public void setBranchAdvanceAccountDescription(String branchAdvanceAccountDescription) {
		
		this.branchAdvanceAccountDescription = branchAdvanceAccountDescription;
		
	}
	
}