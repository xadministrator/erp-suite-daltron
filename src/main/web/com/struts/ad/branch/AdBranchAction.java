package com.struts.ad.branch;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdBRBranchCodeAlreadyExistsException;
import com.ejb.exception.AdBRBranchNameAlreadyExistsException;
import com.ejb.exception.AdBRHeadQuarterAlreadyExistsException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.AdBranchController;
import com.ejb.txn.AdBranchControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchDetails;

public final class AdBranchAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdBranchAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdBranchForm actionForm = (AdBranchForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_BRANCH_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adBranch");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdBranchController EJB
*******************************************************/

         AdBranchControllerHome homeBR = null;
         AdBranchController ejbBR = null;

         try {

            homeBR = (AdBranchControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdBranchControllerEJB", AdBranchControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdBranchAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBR = homeBR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdBranchAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }         
        
         ActionErrors errors = new ActionErrors();
         
/*******************************************************
Call AdBranchController EJB
getAdPrfInvQuantityPrecisionUnit
*******************************************************/			

         short precisionUnit = 0;
         
         try {
         	
         	precisionUnit = ejbBR.getGlFcPrecisionUnit(user.getCmpCode());
         	
         } catch(EJBException ex) {
         	
         	if (log.isInfoEnabled()) {
         		
         		log.info("EJBException caught in AdBranchAction.execute(): " + ex.getMessage() +
         				" session: " + session.getId());
         	}
         	
         	return(mapping.findForward("cmnErrorPage"));
         	
         }			

/*******************************************************
   -- Ad BR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adBranch"));
	     
/*******************************************************
   -- Ad BR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adBranch"));                  
         
/*******************************************************
   -- Ad RS Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdBranchDetails details = new AdBranchDetails();
            details.setBrBranchCode(actionForm.getBrBranchCode());
            details.setBrName(actionForm.getBranchName());
            details.setBrDescription(actionForm.getDescription());
            details.setBrType(actionForm.getType());
            details.setBrHeadQuarter(Common.convertBooleanToByte(actionForm.getHeadQuarter()));
            details.setBrAddress(actionForm.getAddress());
            details.setBrContactPerson(actionForm.getContactPerson());
            details.setBrContactNumber(actionForm.getContactNumber());
            details.setBrApplyShipping(Common.convertBooleanToByte(actionForm.getApplyShipping()));
            details.setBrPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), precisionUnit));
            
            try {
            	
            	ejbBR.addAdBrEntry(details, actionForm.getShippingAccount(), user.getCmpCode());
            	
            	actionForm.reset(mapping, request);
                
            } catch (AdBRBranchCodeAlreadyExistsException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("branch.error.branchCodeAlreadyExists"));
               
            } catch (AdBRBranchNameAlreadyExistsException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("branch.error.branchNameAlreadyExists"));
               
            } catch (AdBRHeadQuarterAlreadyExistsException ex) {
            	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("branch.error.headQuarterAlreadyExists"));

            } catch (GlobalAccountNumberInvalidException ex) {

            	 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("branch.error.accountNumberInvalid"));

            } catch (EJBException ex) {

            	if (log.isInfoEnabled()) {

            		log.info("EJBException caught in AdBranchAction.execute(): " + ex.getMessage() +
            				" session: " + session.getId());
            		return mapping.findForward("cmnErrorPage"); 

            	}

            }
            
/*******************************************************
   -- Ad BR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad BR Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
        		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


        	 AdBranchList adBRList =
        		 actionForm.getAdBRByIndex(actionForm.getRowSelected());

        	 AdBranchDetails details = new AdBranchDetails();
        	 details.setBrCode(adBRList.getBranchCode());
        	 details.setBrBranchCode(actionForm.getBrBranchCode());
        	 details.setBrName(actionForm.getBranchName());
        	 details.setBrDescription(actionForm.getDescription());
        	 details.setBrType(actionForm.getType());
        	 details.setBrHeadQuarter(Common.convertBooleanToByte(actionForm.getHeadQuarter()));
        	 details.setBrAddress(actionForm.getAddress());
        	 details.setBrContactPerson(actionForm.getContactPerson());
        	 details.setBrContactNumber(actionForm.getContactNumber());
        	 details.setBrApplyShipping(Common.convertBooleanToByte(actionForm.getApplyShipping()));
        	 details.setBrPercentMarkup(Common.convertStringMoneyToDouble(actionForm.getPercentMarkup(), precisionUnit));

        	 try {

        		 ejbBR.updateAdBrEntry(details, actionForm.getShippingAccount(), user.getCmpCode());

        		 actionForm.reset(mapping, request);

        	 } catch (AdBRBranchCodeAlreadyExistsException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("branch.error.branchCodeAlreadyExists"));

        	 } catch (AdBRBranchNameAlreadyExistsException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("branch.error.branchNameAlreadyExists"));

        	 } catch (AdBRHeadQuarterAlreadyExistsException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("branch.error.headQuarterAlreadyExists"));

        	 } catch (GlobalAccountNumberInvalidException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("branch.error.accountNumberInvalid"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in AdBranchAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage"); 

        		 }

        	 }
            
/*******************************************************
   -- Ad BR Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad BR Edit Action --
*******************************************************/

         } else if (request.getParameter("adBRList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	actionForm.reset(mapping, request);
         	
         	actionForm.showAdBRRow(actionForm.getRowSelected());
         	
            return mapping.findForward("adBranch");
            
/*******************************************************
   -- Ad BR Delete Action --
*******************************************************/

         } else if (request.getParameter("adBRList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdBranchList adBRList =
              actionForm.getAdBRByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbBR.deleteAdBrEntry(adBRList.getBranchCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("branch.error.deleteBranchAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("branch.error.branchAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdBranchAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad BR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adBranch");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               actionForm.clearAdBRList();	        
	           
	           list = ejbBR.getAdBrAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdModBranchDetails mdetails = (AdModBranchDetails)i.next();
	            	
	              AdBranchList adBRList = new AdBranchList(actionForm,
	            	    mdetails.getBrCode(),
						mdetails.getBrBranchCode(),
	            	    mdetails.getBrName(),
	            	    mdetails.getBrDescription(),
	            	    mdetails.getBrType(),
	            	    Common.convertByteToBoolean(mdetails.getBrHeadQuarter()),
						mdetails.getBrAddress(),
						mdetails.getBrContactPerson(),
						mdetails.getBrContactNumber(),
						Common.convertByteToBoolean(mdetails.getBrApplyShipping()),
						Common.convertDoubleToStringMoney(mdetails.getBrPercentMarkup(), precisionUnit),
						mdetails.getBrGlCoaAccountNumber(),
						mdetails.getBrGlCoaAccountDescription());
	            	 	            	    
	              actionForm.saveAdBRList(adBRList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdBranchAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }        
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adBranch"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdBranchAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

         e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

      }

   }
   
}