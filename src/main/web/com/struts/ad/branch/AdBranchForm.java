package com.struts.ad.branch;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdBranchForm extends ActionForm implements Serializable {

   private Integer branchCode = null;
   private String brBranchCode = null;
   private String branchName = null;
   private String description = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private boolean headQuarter = false;
   private String address = null;
   private String contactPerson = null;
   private String contactNumber = null;
   
   private String tableType = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;   
   private String pageState = new String();
   private ArrayList adBRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private boolean applyShipping = false;
   private String percentMarkup = null;
   private String shippingAccount = null;
   private String shippingAccountDescription = null; 
   
   public int getRowSelected() {

      return rowSelected;

   }

   public Object[] getAdBRList(){
   	
   	  return adBRList.toArray();
   	  
   }
   
   public AdBranchList getAdBRByIndex(int index){
   	
   	  return ((AdBranchList)adBRList.get(index));
   	  
   }
   
   public int getAdBRListSize(){
   	
   	  return(adBRList.size());
   	  
   }
   
   public void saveAdBRList(Object newAdBRList){
   	
   	  adBRList.add(newAdBRList);   	  
   	  
   }
   
   public void clearAdBRList(){
   	
   	  adBRList.clear();
   	  
   }
   
   public void setAdBRList(ArrayList adBRList) {
   	
   	  this.adBRList = adBRList;
   	
   }
   
   public void setRowSelected(Object selectedAdBRList, boolean isEdit) {

      this.rowSelected = adBRList.indexOf(selectedAdBRList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdBRRow(int rowSelected) {
   	
   	   this.branchCode = ((AdBranchList)adBRList.get(rowSelected)).getBranchCode();
   	   this.brBranchCode = ((AdBranchList)adBRList.get(rowSelected)).getBrBranchCode();
   	   this.branchName = ((AdBranchList)adBRList.get(rowSelected)).getBranchName();
       this.description = ((AdBranchList)adBRList.get(rowSelected)).getDescription();
       this.type = ((AdBranchList)adBRList.get(rowSelected)).getType();
       this.headQuarter = ((AdBranchList)adBRList.get(rowSelected)).getHeadQuarter();
       this.address = ((AdBranchList)adBRList.get(rowSelected)).getAddress();
       this.contactPerson = ((AdBranchList)adBRList.get(rowSelected)).getContactPerson();
       this.contactNumber = ((AdBranchList)adBRList.get(rowSelected)).getContactNumber();
       this.applyShipping = ((AdBranchList)adBRList.get(rowSelected)).getApplyShipping();
       this.percentMarkup = ((AdBranchList)adBRList.get(rowSelected)).getPercentMarkup();
       this.shippingAccount = ((AdBranchList)adBRList.get(rowSelected)).getShippingAccount();
       this.shippingAccountDescription = ((AdBranchList)adBRList.get(rowSelected)).getShippingAccountDescription();
       
   }

   public void updateAdBrRow(int rowSelected, Object newAdBrList) {
   	
   	  adBRList.set(rowSelected, newAdBrList);
   	
   }
   
   public void deleteAdBrList(int rowSelected) {
   	
   	  adBRList.remove(rowSelected);
   	
   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getBranchCode() {
   	
   	  return branchCode;
   	
   }
   
   public void setBranchCode(Integer branchCode) {
   	
   	  this.branchCode = branchCode;
   	
   }
   
   public String getBrBranchCode() {
   	
   	  return brBranchCode;
   	
   }
   
   public void setBrBranchCode(String brBranchCode) {
   	
   	  this.brBranchCode = brBranchCode;
   	
   }
   
   public String getBranchName() {
   	
   	  return branchName;
   	  
   }
   
   public void setBranchName(String branchName) {
   	
   	  this.branchName = branchName;
   	
   }
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }
   
   public String getType() {
   	
   	  return type;
   	
   }
   
   public void setType(String type) {
   	
   	  this.type = type;
   	
   }
   
   public ArrayList getTypeList() {
   	
   	  return typeList;
   	
   }
   
   public boolean getHeadQuarter() {
   	
   	  return headQuarter;
   	
   }
   
   public void setHeadQuarter(boolean headQuarter) {
   	
   	  this.headQuarter = headQuarter;
   	
   }
   
   public String getAddress() {
   	
   	  return address;
   	
   }
   
   public void setAddress(String address) {
   	
   	  this.address = address;
   	
   }

   public String getContactPerson() {
   	
   	  return contactPerson;
   	
   }
   
   public void setContactPerson(String contactPerson) {
   	
   	  this.contactPerson = contactPerson;
   	
   }
   
   public String getContactNumber() {
   	
   	  return contactNumber;
   	
   }
   
   public void setContactNumber(String contactNumber) {
   	
   	  this.contactNumber = contactNumber;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }     
   
   public boolean getApplyShipping() {
	   
	   return applyShipping;
	   
   }
   
   public void setApplyShipping(boolean applyShipping) {
	   
	   this.applyShipping = applyShipping;
	   
   }
   
   public String getPercentMarkup() {
	   
	   return percentMarkup;
	   
   }
   
   public void setPercentMarkup(String percentMarkup) {
	   
	   this.percentMarkup = percentMarkup;
	   
   }
   
   public String getShippingAccount() {
	   
	   return shippingAccount;
	   
   }
   
   public void setShippingAccount(String shippingAccount) {
	   
	   this.shippingAccount = shippingAccount;
	   
   }
   
   public String getShippingAccountDescription() {

	   return shippingAccountDescription;

   }

   public void setShippingAccountDescription(String shippingAccountDescription) {

	   this.shippingAccountDescription = shippingAccountDescription;

   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
        	  
   	  typeList.clear();
   	  typeList.add(Constants.GLOBAL_BLANK);
   	  typeList.add("STORE");
   	  typeList.add("WAREHOUSE");
   	  typeList.add("OTHERS");
   	  type = Constants.GLOBAL_BLANK;
   	  
   	  branchCode = null;
   	  brBranchCode = null;
   	  branchName = null; 
      description = null;
      headQuarter = false;
      address = null;
      contactPerson = null;
      contactNumber = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
	  applyShipping = false;
	  percentMarkup = null;
	  shippingAccount = null;
	  shippingAccountDescription = null;
	  
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
      	
      	if (Common.validateRequired(brBranchCode)) {
      		
      		errors.add("brBranchCode",
      				new ActionMessage("branch.error.branchCodeRequired"));
      		
      	}
      	
      	if (Common.validateRequired(branchName)) {
      		
      		errors.add("branchName",
      				new ActionMessage("branch.error.branchNameRequired"));
      		
      	}
      	
      	if(!Common.validateMoneyFormat(percentMarkup)){
      		
			errors.add("percentMarkup",
					new ActionMessage("branch.error.percentMarkupInvalid"));
			
		}
      	
      	if(applyShipping && Common.validateRequired(shippingAccount)) {
         	
            errors.add("shippingAccount",
               new ActionMessage("branch.error.shippingAccountRequired"));
     	
     }
      	
      }
         
      return errors;

   }
}