package com.struts.ad.branch;

import java.io.Serializable;

public class AdBranchList implements Serializable {

   private Integer branchCode = null;
   private String brBranchCode = null;
   private String branchName = null;
   private String description = null;
   private String type = null;
   private boolean headQuarter = false;
   private String address = null;
   private String contactPerson = null;
   private String contactNumber = null;
   private boolean applyShipping = false;
   private String percentMarkup = null;
   private String shippingAccount = null;
   private String shippingAccountDescription = null;
   
   private String editButton = null;
   private String deleteButton = null;
    
   private AdBranchForm parentBean;
    
   public AdBranchList(AdBranchForm parentBean,
      Integer branchCode,
      String brBranchCode,
      String branchName,
      String description,
      String type,
      boolean headQuarter,
	  String address,
   	  String contactPerson,
      String contactNumber,
      boolean applyShipping,
      String percentMarkup,
      String shippingAccount,
      String shippingAccountDescription) {

      this.parentBean = parentBean;
      this.branchCode = branchCode;
      this.brBranchCode = brBranchCode;
      this.branchName = branchName;
      this.description = description;
      this.type = type;
      this.headQuarter = headQuarter;
      this.address = address;
      this.contactPerson = contactPerson;
      this.contactNumber = contactNumber;
      this.applyShipping = applyShipping;
      this.percentMarkup = percentMarkup;
      this.shippingAccount = shippingAccount;
      this.shippingAccountDescription = shippingAccountDescription;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, false);

   }   
   
   public Integer getBranchCode() {

      return branchCode;

   }
   
   public String getBrBranchCode() {

    return brBranchCode;

 }
   
   public String getBranchName() {
       
       return branchName;
       
   }

   public String getDescription() {

      return description;

   }

   public String getType() {

      return type;

   }
   
   public boolean getHeadQuarter() {

      return headQuarter;

   }
   
   public String getAddress() {
   	
   	  return address;
   	
   }
   
   public String getContactPerson() {
   	
   	  return contactPerson;
   	
   }
   
   public String getContactNumber() {
   	
   	  return contactNumber;
   	
   }
   
   public boolean getApplyShipping() {
	   
	   return applyShipping;
	   
   }
   
   public String getPercentMarkup() {
	   
	   return percentMarkup;
	   
   }
   
   public String getShippingAccount() {
	   
	   return shippingAccount;
	   
   }
   
   public String getShippingAccountDescription() {
	   
	   return shippingAccountDescription;
	   
   }
   
}