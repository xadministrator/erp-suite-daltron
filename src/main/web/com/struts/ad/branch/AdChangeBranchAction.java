package com.struts.ad.branch;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Branch;
import com.struts.util.Constants;
import com.struts.util.User;

public final class AdChangeBranchAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) 
	throws IOException, ServletException {
		
		HttpSession session = request.getSession();
		
		try {
			int brnchCode = Integer.parseInt(request.getParameter("brnchCode"));
			User user = (User)session.getAttribute(Constants.USER_KEY);
			if(user != null){
				if(log.isInfoEnabled()){
					log.info("AdChangeResAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}				
			} 
			else {
				if (log.isInfoEnabled())
					log.info("User is not logged on in session " + session.getId());
				return mapping.findForward("adLogon");
			}
			
			Branch brnch = null;
			boolean brnchFound = true;
			
			for(int i=0; i<user.getBrnchCount(); i++){
				brnch = user.getBranch(i);
				if(brnch.getBrCode() == brnchCode){
					brnchFound = true;
					user.setCurrentBranch(brnch);
					break;
				}
			}
			
			ActionErrors errors = new ActionErrors();
			// Check if selected branch exists.
			if(!brnchFound)
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("changeBrnch.error.brnchNotExisting"));
			
			if (!errors.isEmpty()) {
				saveErrors(request, new ActionMessages(errors));
				return(mapping.findForward("cmnMain"));
			}
			
			Branch br = user.getCurrentBranch();
			
			session.setAttribute(Constants.USER_KEY, user);

			String returnLink = request.getParameter("location");
			
			if ( returnLink.equalsIgnoreCase("/adLogon.do") )
				return mapping.findForward("cmnMain");
			if ( returnLink.equalsIgnoreCase("/adChangeRes.do") )
				return mapping.findForward("cmnMain");
			if ( returnLink.equalsIgnoreCase("/adChangeApp.do") )
				return mapping.findForward("cmnMain");
			else
				return (new ActionForward(returnLink));
				
		} catch (Exception e){
			if(log.isInfoEnabled())
				log.info("Exception caught in ChangeResAction.execute(): " + e.getMessage() +
						" session: " + session.getId());
			return(mapping.findForward("cmnErrorPage"));
		}
	}
}