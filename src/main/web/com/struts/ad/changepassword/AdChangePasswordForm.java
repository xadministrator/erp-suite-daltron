package com.struts.ad.changepassword;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdChangePasswordForm extends ActionForm implements Serializable {

   private Integer userCode = null;
   private String userPassword = null;
   private String newPassword = null;
   private String confirmPassword = null;

   private String saveButton = null;
   private String closeButton = null;
   private String userPermission = new String();
   private String txnStatus = new String();

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getUserCode() {
   	
   	  return userCode;
   	
   }
   
   public void setUserCode(Integer userCode) {
   	
   	  this.userCode = userCode;
   	
   }

   public String getUserPassword() {

      return userPassword;

   }

   public void setUserPassword(String userPassword) {

      this.userPassword = userPassword;

   }
   
   public String getNewPassword() {
   	
   	  return newPassword;
   	  
   }
   
   public void setNewPassword(String newPassword) {
   	
   	  this.newPassword = newPassword;
   	  
   }
   
   public String getConfirmPassword() {
   	
   	  return confirmPassword;
   	
   }    
   
   public void setConfirmPassword(String confirmPassword) {
   	
   	  this.confirmPassword = confirmPassword;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      userPassword = null;
      newPassword = null;
      confirmPassword = null; 
      saveButton = null;
      closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null) {

         if (Common.validateRequired(userPassword)) {

            errors.add("userPassword",
               new ActionMessage("changePassword.error.userPasswordRequired"));

         }
         
         if (Common.validateRequired(newPassword)) {

            errors.add("newPassword",
               new ActionMessage("changePassword.error.newPasswordRequired"));

         }
         
         if (Common.validateRequired(confirmPassword)) {

            errors.add("confirmPassword",
               new ActionMessage("changePassword.error.confirmPasswordRequired"));

         }
         
         if (!Common.validateRequired(newPassword) && !Common.validateRequired(confirmPassword) &&
             !newPassword.equals(confirmPassword)) {
             	
             errors.add("confirmPassword",
               new ActionMessage("changePassword.error.passwordNotConfirmed"));
               
         }                                       

      }
         
      return errors;

   }
}