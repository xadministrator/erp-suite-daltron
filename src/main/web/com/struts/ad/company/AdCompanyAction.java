package com.struts.ad.company;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.AdCompanyController;
import com.ejb.txn.AdCompanyControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdCompanyDetails;
import com.util.AdModCompanyDetails;

public final class AdCompanyAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         
         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdCompanyAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdCompanyForm actionForm = (AdCompanyForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_COMPANY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adCompany");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdCompanyController EJB
*******************************************************/

         AdCompanyControllerHome homeCMP = null;
         AdCompanyController ejbCMP = null;

         try {

            homeCMP = (AdCompanyControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdCompanyControllerEJB", AdCompanyControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdCompanyAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCMP = homeCMP.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdCompanyAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad CMP Close Action --
*******************************************************/

         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad CMP Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            AdCompanyDetails details = new AdCompanyDetails();
            details.setCmpName(actionForm.getCompanyName());
            details.setCmpTaxPayerName(actionForm.getTaxPayerName());
            details.setCmpContact(actionForm.getContacts());
            details.setCmpDescription(actionForm.getDescription());
            details.setCmpAddress(actionForm.getAddress());
            details.setCmpCity(actionForm.getCity());
            details.setCmpZip(actionForm.getZipCode());
            details.setCmpCountry(actionForm.getCountry());
            details.setCmpPhone(actionForm.getTelephoneNumber());
            details.setCmpFax(actionForm.getFaxNumber());
            details.setCmpEmail(actionForm.getEmailAddress());
            details.setCmpTin(actionForm.getTinNumber());
            details.setCmpMailSectionNo(actionForm.getMailSectionNo());
            details.setCmpMailLotNo(actionForm.getMailLotNo());
            details.setCmpMailStreet(actionForm.getMailStreet());
            details.setCmpMailPoBox(actionForm.getMailPoBox());
            details.setCmpMailCountry(actionForm.getMailCountry());
            details.setCmpMailProvince(actionForm.getMailProvince());
            details.setCmpMailPostOffice(actionForm.getMailPostOffice());
            details.setCmpMailCareOff(actionForm.getMailCareOff());
            details.setCmpTaxPeriodFrom(actionForm.getTaxPeriodFrom());
            details.setCmpTaxPeriodTo(actionForm.getTaxPeriodTo());
            details.setCmpPublicOfficeName(actionForm.getPublicOfficeName());
            details.setCmpDateAppointment(actionForm.getDateAppointment());
            
            details.setCmpRevenueOffice(actionForm.getRevenueOffice());
            details.setCmpFiscalYearEnding(actionForm.getFiscalYearEnding());
            details.setCmpIndustry(actionForm.getIndustry());
            details.setCmpRetainedEarnings(actionForm.getRetainedEarnings());
            
            try {
            	
            	ejbCMP.saveArCmpEntry(details, actionForm.getCurrency(), actionForm.getGenericField(), user.getCmpCode());

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdCompanyAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad CMP Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adCompany");

            }
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	
            	actionForm.clearCurrencyList();
            	
            	list = ejbCMP.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearGenericFieldList();
            	
            	list = ejbCMP.getGenFlAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setGenericFieldList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setGenericFieldList((String)i.next());
            			
            		}
            		
            	}  
            	
            	AdModCompanyDetails mdetails = ejbCMP.getArCmp(user.getCmpCode());
            		
				actionForm.setCompanyName(mdetails.getCmpName());
				actionForm.setTaxPayerName(mdetails.getCmpTaxPayerName());
				actionForm.setContacts(mdetails.getCmpContact());
	            actionForm.setDescription(mdetails.getCmpDescription());
	            actionForm.setAddress(mdetails.getCmpAddress());
	            actionForm.setCity(mdetails.getCmpCity());
	            actionForm.setZipCode(mdetails.getCmpZip());
	            actionForm.setCountry(mdetails.getCmpCountry());
	            actionForm.setTelephoneNumber(mdetails.getCmpPhone());
	            actionForm.setFaxNumber(mdetails.getCmpFax());
	            actionForm.setEmailAddress(mdetails.getCmpEmail());
	            actionForm.setTinNumber(mdetails.getCmpTin());
	            actionForm.setMailSectionNo(mdetails.getCmpMailSectionNo());
	            actionForm.setMailLotNo(mdetails.getCmpMailLotNo());
	            actionForm.setMailStreet(mdetails.getCmpMailStreet());
	            actionForm.setMailPoBox(mdetails.getCmpMailPoBox());
	            actionForm.setMailCountry(mdetails.getCmpMailCountry());
	            actionForm.setMailProvince(mdetails.getCmpMailProvince());
	            actionForm.setMailPostOffice(mdetails.getCmpMailPostOffice());
	            actionForm.setMailCareOff(mdetails.getCmpMailCareOff());
	            actionForm.setTaxPeriodFrom(mdetails.getCmpTaxPeriodFrom());
	            actionForm.setTaxPeriodTo(mdetails.getCmpTaxPeriodTo());
	            actionForm.setPublicOfficeName(mdetails.getCmpPublicOfficeName());
	            actionForm.setDateAppointment(mdetails.getCmpDateAppointment());
	            actionForm.setRevenueOffice(mdetails.getCmpRevenueOffice());
	            actionForm.setFiscalYearEnding(mdetails.getCmpFiscalYearEnding());
	            actionForm.setIndustry(mdetails.getCmpIndustry());
	            actionForm.setRetainedEarnings(mdetails.getCmpRetainedEarnings());
	            
	            if (!actionForm.getCurrencyList().contains(mdetails.getCmpGlFcName())) {
	            	
	            	if(actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
	            		
	            		actionForm.clearCurrencyList();
	            		
	            	}
	            	actionForm.setCurrencyList(mdetails.getCmpGlFcName());
	            	
	            }	            
            	actionForm.setCurrency(mdetails.getCmpGlFcName());
            	
            	actionForm.setGenericField(mdetails.getCmpGenFlName());
            	
            			            	    
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdCompanyAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("adCompany"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdCompanyAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}