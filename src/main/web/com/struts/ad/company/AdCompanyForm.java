package com.struts.ad.company;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdCompanyForm extends ActionForm implements Serializable {

	private Integer companyCode = null;
	private String companyName = null;
	private String taxPayerName = null;
	private String contacts = null;
	private String description = null;
	private String address = null;
	private String city = null;
	private String zipCode = null;
	private String country = null;
	private String telephoneNumber = null;
	private String faxNumber = null;
	private String emailAddress = null;
	private String tinNumber = null;
	private String mailSectionNo = null;
	private String mailLotNo = null; 
	private String mailStreet = null;
	private String mailPoBox = null;
	private String mailCountry = null;
	private String mailProvince = null;
	private String mailPostOffice = null;
	private String mailCareOff = null;
	private String taxPeriodFrom = null;
	private String taxPeriodTo = null;
	private String publicOfficeName = null;
	private String dateAppointment = null;
	
	private String fiscalYearEnding = null;
	private String revenueOffice = null;
	private String industry = null;
	private String retainedEarnings = null;
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
    private String genericField = null;
	private ArrayList genericFieldList = new ArrayList();

	private String userPermission = new String();
	private String txnStatus = new String();
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getCompanyCode() {
		
		return companyCode;
		
	}
	
	public void setCompanyCode(Integer companyCode) {
		
		this.companyCode = companyCode;
		
	}
	
	public String getCompanyName() {
		
		return companyName;
		
	}
	
	public void setCompanyName(String companyName) {
		
		this.companyName = companyName;
		
	}
	
	
	public String getTaxPayerName() {
		
		return taxPayerName;
		
	}
	
	public void setTaxPayerName(String taxPayerName) {
		
		this.taxPayerName = taxPayerName;
		
	}
	
	public String getContacts() {
		
		return contacts;
		
	}
	
	public void setContacts(String contacts) {
		
		this.contacts = contacts;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
	    this.description = description;
	    
	}
	
	public String getAddress() {
		
		return address;
		
	}
	
	public void setAddress(String address) {
		
		this.address = address;
		
	}
	
	public String getCity() {
		
		return city;
		
	}
	
	public void setCity(String city) {
		
		this.city = city;
		
	}
	
	public String getZipCode() {
		
		return zipCode;
		
	}
	
	public void setZipCode(String zipCode) {
		
		this.zipCode = zipCode;
		
	}
	
	public String getCountry() {
		
		return country;
		
    }
    
    public void setCountry(String country) {
    	
    	this.country = country;
    	
    }
    
    public String getTelephoneNumber() {
    	
    	return telephoneNumber;
    	
    }
    
    public void setTelephoneNumber(String telephoneNumber) {
    	
    	this.telephoneNumber = telephoneNumber;
    	
    }
    
    public String getFaxNumber() {
    	
    	return faxNumber;
    	
    }
    
    public void setFaxNumber(String faxNumber) {
    	
    	this.faxNumber = faxNumber;
    	
    }
    
    public String getEmailAddress() {
    	
    	return emailAddress;
    	
    }
    
    public void setEmailAddress(String emailAddress) {
    	
    	this.emailAddress = emailAddress;
    	
    }
    
    public String getTinNumber() {
    	
    	return tinNumber;
    	
    }
    
    public void setTinNumber(String tinNumber) {
    	
    	this.tinNumber = tinNumber;
    	
    }
    
    
    public String getMailSectionNo() {
    	
    	return mailSectionNo;
    	
    }
    
    public void setMailSectionNo(String mailSectionNo) {
    	
    	this.mailSectionNo = mailSectionNo;
    	
    }
    
    
    public String getMailLotNo() {
    	
    	return mailLotNo;
    	
    }
    
    public void setMailLotNo(String mailLotNo) {
    	
    	this.mailLotNo = mailLotNo;
    	
    }
    
    
    public String getMailStreet() {
    	
    	return mailStreet;
    	
    }
    
    public void setMailStreet(String mailStreet) {
    	
    	this.mailStreet = mailStreet;
    	
    }
    
    
    public String getMailPoBox() {
    	
    	return mailPoBox;
    	
    }
    
    public void setMailPoBox(String mailPoBox) {
    	
    	this.mailPoBox = mailPoBox;
    	
    }
    
    public String getMailCountry() {
    	
    	return mailCountry;
    	
    }
    
    public void setMailCountry(String mailCountry) {
    	
    	this.mailCountry = mailCountry;
    	
    }
    
    
    public String getMailProvince() {
    	
    	return mailProvince;
    	
    }
    
    public void setMailProvince(String mailProvince) {
    	
    	this.mailProvince = mailProvince;
    	
    }
    
    
    public String getMailPostOffice() {
    	
    	return mailPostOffice;
    	
    }
    
    public void setMailPostOffice(String mailPostOffice) {
    	
    	this.mailPostOffice = mailPostOffice;
    	
    }
    
    
    public String getMailCareOff() {
    	
    	return mailCareOff;
    	
    }
    
    public void setMailCareOff(String mailCareOff) {
    	
    	this.mailCareOff = mailCareOff;
    	
    }
    
    
    public String getTaxPeriodFrom() {
    	
    	return taxPeriodFrom;
    	
    }
    
    public void setTaxPeriodFrom(String taxPeriodFrom) {
    	
    	this.taxPeriodFrom = taxPeriodFrom;
    	
    }
    
    
    public String getTaxPeriodTo() {
    	
    	return taxPeriodTo;
    	
    }
    
    public void setTaxPeriodTo(String taxPeriodTo) {
    	
    	this.taxPeriodTo = taxPeriodTo;
    	
    }
    
    
    public String getPublicOfficeName() {
    	
    	return publicOfficeName;
    	
    }
    
    public void setPublicOfficeName(String publicOfficeName) {
    	
    	this.publicOfficeName = publicOfficeName;
    	
    }
    
    
    public String getDateAppointment() {
    	
    	return dateAppointment;
    	
    }
    
    public void setDateAppointment(String dateAppointment) {
    	
    	this.dateAppointment = dateAppointment;
    	
    }
    
    
    
    

    
    public String getFiscalYearEnding() {
    	
    	return fiscalYearEnding;
    	
    }
    
    public void setFiscalYearEnding(String fiscalYearEnding) {
    	
    	this.fiscalYearEnding = fiscalYearEnding;
    	
    }
    
    public String getRevenueOffice() {
    	
    	return revenueOffice;
    	
    }
    
    public void setRevenueOffice(String revenueOffice) {
    	
    	this.revenueOffice = revenueOffice;
    	
    }
    
    public String getIndustry() {
    	
    	return industry;
    	
    }
    
    public void setIndustry(String industry) {
    	
    	this.industry = industry;
    	
    }
    
    public String getRetainedEarnings() {
    	
    	return retainedEarnings;
    	
    }
    
    public void setRetainedEarnings(String retainedEarnings) {
    	
    	this.retainedEarnings = retainedEarnings;
    	
    }
    
    public String getCurrency() {
    	
    	return currency;
    	
    }
    
    public void setCurrency(String currency) {
    	
    	this.currency = currency;
    	
    }
    
    public ArrayList getCurrencyList() {
    	
    	return currencyList;
    	
    }

    public void setCurrencyList(String currency) {

        currencyList.add(currency);

    }
   
    public void clearCurrencyList() {

        currencyList.clear();
        currencyList.add(Constants.GLOBAL_BLANK);

    }   

	public String getGenericField() {
    	
    	return genericField;
    	
    }
    
    public void setGenericField(String genericField) {
    	
    	this.genericField = genericField;
    	
    }
    
    public ArrayList getGenericFieldList() {
    	
    	return genericFieldList;
    	
    }

    public void setGenericFieldList(String genericField) {

        genericFieldList.add(genericField);

    }
   
    public void clearGenericFieldList() {

        genericFieldList.clear();
        genericFieldList.add(Constants.GLOBAL_BLANK);

    } 
	
 	public void reset(ActionMapping mapping, HttpServletRequest request) {

		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null) {
	
			if (Common.validateRequired(companyName)) {
			
				errors.add("companyName",
			   		new ActionMessage("adCompany.error.companyNameRequired"));
			
			}
			
			if (Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

	            errors.add("currency",
	               new ActionMessage("adCompany.error.currencyRequired"));

            }
            
            if (Common.validateRequired(genericField) || genericField.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

	            errors.add("genericField",
	               new ActionMessage("adCompany.error.genericFieldRequired"));

            }
			
	  	}		

	  	return errors;
	
	}
	
}