package com.struts.ad.console;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.txn.AdConsoleController;
import com.ejb.txn.AdConsoleControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.GenSegmentDetails;
import com.util.GenValueSetValueDetails;
import com.util.GlChartOfAccountDetails;

public final class AdConsoleAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      String consoleUser = (String) session.getAttribute(Constants.CONSOLE_USER_KEY);

      if (consoleUser != null) {

         if (log.isInfoEnabled()) {

             log.info("AdConsoleAction performed this action on session " + session.getId());
         }

      } else {

         if (log.isInfoEnabled()) {

            log.info("Console User is not logged on in session" + session.getId());

         }

         return(mapping.findForward("adConsoleLogon"));

      }
      
      AdConsoleForm actionForm = (AdConsoleForm)form;
      
      ActionErrors errors = new ActionErrors();
      
      try {
      	
      	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
      	
        if (!fieldErrors.isEmpty()) {

           saveErrors(request, new ActionMessages(fieldErrors));
           return mapping.findForward("adConsole");
           
        }
      	
      	if (request.getParameter("nextButton") != null) {
      		
      		actionForm.setPageNumber(actionForm.getPageNumber() + 1);
      		
      	} else if (request.getParameter("previousButton") != null) {
      	
      		if (actionForm.getPageNumber() == 1) return mapping.findForward("adConsole");
      		
      		actionForm.setPageNumber(actionForm.getPageNumber() - 1);
      		      		
      	} else if (request.getParameter("finishButton") != null) {
      		
      		AdConsoleControllerHome homeCNS = null;
      		AdConsoleController ejbCNS = null;

            try {

            	homeCNS = (AdConsoleControllerHome)com.util.EJBHomeFactory.
                   lookUpHome("ejb/AdConsoleControllerEJB", AdConsoleControllerHome.class);
               
            } catch (NamingException e) {

               if (log.isInfoEnabled()) {

                   log.info("NamingException caught in AdConsoleAction.execute(): " + e.getMessage() +
                  " session: " + session.getId());
               }

               return mapping.findForward("cmnErrorPage");

            }

            try {

            	ejbCNS = homeCNS.create();
            	
            } catch (CreateException e) {

               if (log.isInfoEnabled()) {

                   log.info("CreateException caught in AdConsoleAction.execute(): " + e.getMessage() +
                  " session: " + session.getId());

               }
               return mapping.findForward("cmnErrorPage");

            }
            
            try {
            	
            	String CMP_RTND_EARNNGS = null;
            	
            	ArrayList segmentList = new ArrayList();
            	
            	Iterator i = actionForm.getSegmentList().iterator();
            	
            	while (i.hasNext()) {
            		
            		AdConsoleSegmentList consoleSegmentList = (AdConsoleSegmentList) i.next();
            		
            		GenSegmentDetails segmentDetails = new GenSegmentDetails();
            		segmentDetails.setSgName(consoleSegmentList.getSegmentName());
            		segmentDetails.setSgDescription(consoleSegmentList.getSegmentName());
            		segmentDetails.setSgMaxSize(Short.parseShort(consoleSegmentList.getMaximumSize()));
            		segmentDetails.setSgSegmentNumber(Short.parseShort(consoleSegmentList.getSegmentNumber()));
            		
            		if (consoleSegmentList.getNaturalAccount()) {
            			
            			segmentDetails.setSgSegmentType('N');
            			
            		} else {
            			
            			segmentDetails.setSgSegmentType('X');
            			
            		}
            		
            		segmentList.add(segmentDetails);
            		
            		
            	}
            	
            	ArrayList vsvList = new ArrayList();
            	
            	CSVParser csvParser = new CSVParser(actionForm.getSegmentsFile().getInputStream());
     		   
     		    String[][] values = csvParser.getAllValues();
     		   
     		    for (int ind=0; ind<values.length; ind++) {
     		    	
     		       if (values[ind].length != 4 || !Common.validateNumberFormat(values[ind][2])) {
     		       	
     		           errors.add(ActionMessages.GLOBAL_MESSAGE,
     	                 new ActionMessage("console..error.invalidSegmentsFile", String.valueOf(ind+1)));     		           
     		           saveErrors(request, new ActionMessages(errors));
     		           return mapping.findForward("adConsole");
     		       	
     		       }
     		       
     		   	   GenValueSetValueDetails details = new GenValueSetValueDetails(values[ind][0],
     		   	   		values[ind][1], Short.parseShort(values[ind][2]), values[ind][3] == "\\N" ? null : values[ind][3]);
     		   	   
     		   	   vsvList.add(details);
     		   	   
     		    }
            	
            	ArrayList coaList = new ArrayList();
            	
            	csvParser = new CSVParser(actionForm.getCoaFile().getInputStream());
     		   
     		    values = csvParser.getAllValues();
     		   
     		    for (int ind=0; ind<values.length; ind++) {
     		    	
     		       if (values[ind].length != 12 || !Common.validateNumberFormat(values[ind][11])) {
     		       	
     		           errors.add(ActionMessages.GLOBAL_MESSAGE,
     	                 new ActionMessage("console.error.invalidCoaFile", String.valueOf(ind+1)));     		           
     		           saveErrors(request, new ActionMessages(errors));
     		           return mapping.findForward("adConsole");
     		       	
     		       }
     		   	   
     		   	   SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
         		   sdf.setLenient(false);
         	   	
     		   	   GlChartOfAccountDetails details = new GlChartOfAccountDetails(values[ind][0],
     		   	   null,null, null, null, null,null, sdf.parse("2000/01/01"), sdf.parse("2050/01/01"), 
     		   	   values[ind][1] == "\\N" ? null : values[ind][1], 
     		   	   values[ind][2] == "\\N" ? null : values[ind][2],
     		   	   values[ind][3] == "\\N" ? null : values[ind][3], 
     		   	   values[ind][4] == "\\N" ? null : values[ind][4], 
     		   	   values[ind][5] == "\\N" ? null : values[ind][5], 
     		   	   values[ind][6] == "\\N" ? null : values[ind][6], 
     		   	   values[ind][7] == "\\N" ? null : values[ind][7], 
     		   	   values[ind][8] == "\\N" ? null : values[ind][8],
     		   	   values[ind][9] == "\\N" ? null : values[ind][9], 
     		   	   values[ind][10] == "\\N" ? null : values[ind][10], (byte)1);
     		   	   
     		   	   coaList.add(details);
     		   	   
     		   	   if (Byte.parseByte(values[ind][11]) == 1) {
     		   	   	
     		   	   	  CMP_RTND_EARNNGS = values[ind][0];
     		   	   	
     		   	   }
     		   	   
     		    }
            	
            	ejbCNS.executeAdConsole(actionForm.getCompanyName(), actionForm.getShortName(),
            			actionForm.getWelcomeNote(),
						Short.parseShort(actionForm.getNumberOfSegment()),
						actionForm.getSegmentSeparator(), CMP_RTND_EARNNGS, segmentList, vsvList, coaList);
						
			} catch (GlobalRecordAlreadyExistException ex) {
			
				errors.add(ActionMessages.GLOBAL_MESSAGE,
     	                 new ActionMessage("console.error.recordAlreadyExist"));     		           
     		           saveErrors(request, new ActionMessages(errors));
     		           return mapping.findForward("adConsole");
			
            } catch (GlobalRecordInvalidException ex) {
            
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
     	                 new ActionMessage("console.error.recordNoRetainedEarnings"));     		           
     		           saveErrors(request, new ActionMessages(errors));
     		           return mapping.findForward("adConsole");
            	
            } catch (EJBException ex) {
            	
            	if (log.isInfoEnabled()) {

                    log.info("EJBException caught in AdConsoleAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());

                }
                return mapping.findForward("cmnErrorPage");

            	
            }
            
            session.invalidate();
      		
      		return mapping.findForward("adLogon");
      		
      	}
      	
      	if (request.getParameter("nextButton") == null && request.getParameter("previousButton") == null) { 
      	  	  			
      		actionForm.setPageNumber(1);
      		actionForm.setCompanyName(null);
      		actionForm.setShortName(null);
      		actionForm.setWelcomeNote(null);
      		actionForm.setNumberOfSegment(null);
      		actionForm.setSegmentSeparator(null);
      		actionForm.setSegmentList(new ArrayList());
      		actionForm.setCoaFile(null);
      		actionForm.setSegmentsFile(null);
      		
      	}
      	
      	if (actionForm.getPageNumber() == 2) {
  			  			
  			this.generateSegmentPage(actionForm);
  			
  		}
		
		return mapping.findForward("adConsole");

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdConsoleAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
   private void generateSegmentPage(AdConsoleForm actionForm) {
   	
   	   ArrayList segmentList = actionForm.getSegmentList();
   	   ArrayList newSegmentList = new ArrayList();
   	   String nullString = null;
   	
       for (int i=0; i < Integer.parseInt(actionForm.getNumberOfSegment()); i++) {
       	
       	   try {
       	
	       	   AdConsoleSegmentList consoleSegmentList = (AdConsoleSegmentList)segmentList.get(i);

       	       AdConsoleSegmentList newConsoleSegmentList = new  AdConsoleSegmentList(actionForm,
       	    		consoleSegmentList.getSegmentNumber(),
					consoleSegmentList.getSegmentName(),
					consoleSegmentList.getMaximumSize(),
					consoleSegmentList.getNaturalAccount());
       	    
       	        newSegmentList.add(newConsoleSegmentList);
				
	        
       	   } catch (IndexOutOfBoundsException ex) {
       	   	
       	   		AdConsoleSegmentList newConsoleSegmentList = new  AdConsoleSegmentList(actionForm,
    	    	   String.valueOf(i + 1), nullString, nullString, false);
        		
        		newSegmentList.add(newConsoleSegmentList);
       	   	
       	   }
           
           
       
       }
       
       actionForm.setSegmentList(newSegmentList);
   	
   }
   
   
}