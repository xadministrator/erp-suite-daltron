package com.struts.ad.console;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;


public class AdConsoleForm extends ActionForm implements Serializable {

	private String companyName = null;
	private String shortName = null;
	private String welcomeNote = null;	
	private String numberOfSegment = null;
	private String segmentSeparator = null;
	
	private ArrayList segmentList = new ArrayList();
	private FormFile segmentsFile = null;
	private FormFile coaFile = null;
		
	private int pageNumber = 0;
	
	public ArrayList getSegmentList() {
		
		return segmentList;
		
	}
	
	public void setSegmentList(ArrayList segmentList) {
		
		this.segmentList = segmentList;
		
	}
	
	public void clearSegmentList() {
		
		this.segmentList.clear();
		
	}
				
	public String getCompanyName() {
		
		return companyName;
		
	}
	
	public void setCompanyName(String companyName) {
		
		this.companyName = companyName;
		
	}
	
	public String getShortName() {
		
		return shortName;
		
	}
	
	public void setShortName(String shortName) {
		
	    this.shortName = shortName;
	    
	}
	
	public String getWelcomeNote() {
		
		return welcomeNote;
		
	}
	
	public void setWelcomeNote(String welcomeNote) {
		
	    this.welcomeNote = welcomeNote;
	    
	}
	
	public String getNumberOfSegment() {
		
		return numberOfSegment;
		
	}
	
	public void setNumberOfSegment(String numberOfSegment) {
		
	    this.numberOfSegment = numberOfSegment;
	    
	}
	
	public String getSegmentSeparator() {
		
		return segmentSeparator;
		
	}
	
	public void setSegmentSeparator(String segmentSeparator) {
		
	    this.segmentSeparator = segmentSeparator;
	    
	}
	
	public int getPageNumber() {
		
		return pageNumber;
		
	}
	
	public void setPageNumber(int pageNumber) {
		
		this.pageNumber = pageNumber;
		
	}
	
	public FormFile getSegmentsFile() {
		
		return segmentsFile;
		
	}
	
	public void setSegmentsFile(FormFile segmentsFile) {
		
		this.segmentsFile = segmentsFile;
		
	}
	
	public FormFile getCoaFile() {
		
		return coaFile;
		
	}
	
	public void setCoaFile(FormFile coaFile) {
		
		this.coaFile = coaFile;
		
	}
		
 	public void reset(ActionMapping mapping, HttpServletRequest request) {
 		
 		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("nextButton") != null && pageNumber == 1) {
	      	  
	      	  if (Common.validateRequired(companyName)) {
	         	
	         	errors.add("companyName",
	               new ActionMessage("console.error.companyNameRequired"));
	         	
	          }
	      	  
	      	  if (Common.validateRequired(shortName)) {
	         	
	         	errors.add("companyName",
	               new ActionMessage("console.error.shortNameRequired"));
	         	
	          }
	      	  
	      	  if (Common.validateRequired(welcomeNote)) {
	         	
	         	errors.add("welcomeNote",
	               new ActionMessage("console.error.welcomeNoteRequired"));
	         	
	          }
	          
	          if (Common.validateRequired(numberOfSegment)) {
	         	
	         	errors.add("numberOfSegment",
	               new ActionMessage("console.error.numberOfSegmentRequired"));
	         	
	          }
	          
	          if (!Common.validateNumberFormat(numberOfSegment)) {
	         	
	         	errors.add("numberOfSegment",
	               new ActionMessage("console.error.numberOfSegmentInvalid"));
	         	
	          }
	          
	          if (Common.validateRequired(segmentSeparator)) {
	         	
	         	errors.add("segmentSeparator",
	               new ActionMessage("console.error.segmentSeparatorRequired"));
	         	
	          }
	      	  
		} else if (request.getParameter("finishButton") != null) {
		
			  Iterator i = segmentList.iterator();
			  
			  int ctr = 1;
			  
			  boolean isNaturalAccountFound = false;
			  
			  while (i.hasNext()) {
			  
			  		AdConsoleSegmentList consoleSegmentList = (AdConsoleSegmentList)i.next();
				  	
			    	if (Common.validateRequired(consoleSegmentList.getSegmentName())) {
		     	
			         	errors.add("segmentName",
			               new ActionMessage("console.error.segmentNameRequired", String.valueOf(ctr)));
			         	
			        }
			        
			        if (Common.validateRequired(consoleSegmentList.getMaximumSize())) {
		     	
			         	errors.add("maximumSize",
			               new ActionMessage("console.error.maximumSizeRequired", String.valueOf(ctr)));
			         	
			        }
			        
			        if (!Common.validateNumberFormat(consoleSegmentList.getMaximumSize())) {
		     	
			         	errors.add("maximumSize",
			               new ActionMessage("console.error.maximumSizeInvalid", String.valueOf(ctr)));
			         	
			        }
			        
			        if (!isNaturalAccountFound && consoleSegmentList.getNaturalAccount()) isNaturalAccountFound = true;
			        
			        ctr++;			        		        
			        			  
			  }
			  
			  if (!isNaturalAccountFound) {
			  
			  	errors.add("maximumSize",
			               new ActionMessage("console.error.segmentsMustHaveOneNaturalAccount"));
			  			  
			  }
			  
			  			  
			  if (Common.validateRequired(coaFile.getFileName())) {
	         	
	         	errors.add("coaFile",
	               new ActionMessage("console.error.coaFileRequired"));
	         	
	          }
	          
	          if (Common.validateRequired(segmentsFile.getFileName())) {
	         	
	         	errors.add("segmentsFile",
	               new ActionMessage("console.error.segmentsFileRequired"));
	         	
	          }
		
		}

	  	return errors;
	
	}
	
}