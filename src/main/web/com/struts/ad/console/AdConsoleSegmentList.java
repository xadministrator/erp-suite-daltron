package com.struts.ad.console;

import java.io.Serializable;

public class AdConsoleSegmentList implements Serializable {

   private String segmentNumber = null;
   private String segmentName = null;
   private String maximumSize = null;
   private boolean naturalAccount = false;
       
   private AdConsoleForm parentBean;
    
   public AdConsoleSegmentList(AdConsoleForm parentBean,
      String segmentNumber,
      String segmentName,
      String maximumSize,
	  boolean naturalAccount) {

      this.parentBean = parentBean;
      this.segmentNumber = segmentNumber;
      this.segmentName = segmentName;
      this.maximumSize = maximumSize;
      this.naturalAccount = naturalAccount;

   }
   
   public String getSegmentNumber() {
   	
   	  return segmentNumber;
   	
   }
   
   public void setSegmentNumber(String segmentNumber) {
   	
   	  this.segmentNumber = segmentNumber;
   	
   }
   
   public String getSegmentName() {
       
       return segmentName;
       
   }
   
   public void setSegmentName(String segmentName) {
   	
   	  this.segmentName = segmentName;
   	
   }

   public String getMaximumSize() {

      return maximumSize;

   }
   
   public void setMaximumSize(String maximumSize) {
   	
   	  this.maximumSize = maximumSize;
   	
   }
   
   public boolean getNaturalAccount() {
   	
   	  return naturalAccount;   	
   	
   }
   
   public void setNaturalAccount(boolean naturalAccount) {
   	
   	  this.naturalAccount = naturalAccount;
   	
   }
   
}