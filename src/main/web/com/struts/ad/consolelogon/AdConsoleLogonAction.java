package com.struts.ad.consolelogon;

import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.struts.util.Constants;
import com.util.AppCallbackHandler;

public final class AdConsoleLogonAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping, ActionForm form,
	   HttpServletRequest request, HttpServletResponse response) 
	   throws Exception {
		
	   ActionErrors errors = new ActionErrors();
       
       HttpSession session = request.getSession();
              
       if(request.getParameter("resetButton") != null){
	      ((AdConsoleLogonForm) form).reset(mapping, request);
	      return mapping.findForward("adConsoleLogon");
	   }else if(request.getParameter("connectButton") != null){
	   	
	   	String userName = ((AdConsoleLogonForm) form).getUserName().trim();
		String password = ((AdConsoleLogonForm) form).getPassword();
		
		if (!userName.equals("xclint") || !password.equals("123qren5")) {
			
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("consoleLogon.error.invalidPassword"));
			saveErrors(request, new ActionMessages(errors));
			return(new ActionForward(mapping.getInput()));
			
		}
				
		// EJB Login
		
		MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
		
		LoginContext lc = null;
 		AppCallbackHandler handler = null;
		
		try {
			
			handler = new AppCallbackHandler("omegabci", "omegabusinessconsulting");
			lc = new LoginContext(appProperties.getMessage("app.customName"), handler);
			lc.login();
			
	    } catch (LoginException le) {
	    	le.printStackTrace();
	    	if(log.isInfoEnabled()) 
	    	    log.info("LoginException caught in LogonAction.execute(): " + le.getMessage());
	    	    
	    	return(mapping.findForward("adErrorPage"));
	    	
	    } 
	    
	    try{
			
	   
	       String consoleUser = new String("consoleUser");
		   session.setAttribute(Constants.CONSOLE_USER_KEY, consoleUser);	   
		   		   		   
		   
           if(log.isInfoEnabled())
              log.info("ConsoleLogonAction: User'" + userName +
                    "' logged on in session " + session.getId());
           
		}catch(Exception e){
		    if(log.isInfoEnabled())
               log.info("Exception caught in ConsoleLogonAction.execute(): " + e.getMessage());
            return(mapping.findForward("adErrorPage"));
		}
	        // Success: Forward to Console Page	
		return(mapping.findForward("adConsole"));
		
	}else{
		
	   return(mapping.findForward("adConsoleLogon"));
	   
	}
   }	

}
