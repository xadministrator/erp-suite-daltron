package com.struts.ad.consolelogon;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public final class AdConsoleLogonForm extends ActionForm{

	private String password = null;
	private String userName = null;
	private String connectButton = null;
	private String resetButton = null;
	private String setupButton = null;

	public void setResetButton(String resetButton){
	   this.resetButton = resetButton;
	}
	
	public void setConnectButton(String connectButton){
	   this.connectButton = connectButton;
	}
	
	public void setSetupButton(String setupButton){
	   this.setupButton = setupButton;
    }
		
	public String getPassword(){
 	   return password;
	}

	public void setPassword(String password){
 	   this.password = password;
	}

	public String getUserName(){
           return userName;
	}

	public void setUserName(String userName){
	   this.userName = userName;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request){
		password = null;
		userName = null;
		connectButton = null;
		resetButton = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request){
	   ActionErrors errors = new ActionErrors();
	   if(request.getParameter("connectButton") != null){
	      if ((userName == null) || (userName.length() < 1)) {
	         errors.add("userName", new ActionMessage("consolelogon.error.userNameRequired"));
	      }
	      if ((password == null) || (password.length() < 1)) {
	         errors.add("password", new ActionMessage("consolelogon.error.passwordRequired"));
	      }
	   }
	   return(errors);
	}

}
