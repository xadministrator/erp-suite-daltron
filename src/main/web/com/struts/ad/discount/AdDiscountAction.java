package com.struts.ad.discount;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdDiscountController;
import com.ejb.txn.AdDiscountControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdDiscountDetails;
import com.util.AdPaymentScheduleDetails;

public final class AdDiscountAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdDiscountAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdDiscountForm actionForm = (AdDiscountForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_DISCOUNT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adDiscount");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdDiscountController EJB
*******************************************************/

         AdDiscountControllerHome homeDSC = null;
         AdDiscountController ejbDSC = null;

         try {

            homeDSC = (AdDiscountControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdDiscountControllerEJB", AdDiscountControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdDiscountAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbDSC = homeDSC.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdDiscountAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad DSC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adDiscount"));
	     
/*******************************************************
   -- Ad DSC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adDiscount"));                  
         
         
/*******************************************************
   -- Ad DSC Save Action --
*******************************************************/

         }  else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdDiscountDetails details = new AdDiscountDetails();
            details.setDscDiscountPercent(Common.convertStringToDouble(actionForm.getDiscountPercent()));
            details.setDscPaidWithinDay(Common.convertStringToShort(actionForm.getPaidWithinDay()));
            
            try {
            	
            	ejbDSC.addAdDscEntry(details, actionForm.getPaymentScheduleCode(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("discount.error.paidWithinDayAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdDiscountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad DSC Back Action --
*******************************************************/
            
         } else if (request.getParameter("paymentSchedules") != null) {
         	
         	return(new ActionForward("/adPaymentSchedule.do"));

/*******************************************************
   -- Ad DSC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad DSC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            AdDiscountList adDSCList =
	            actionForm.getAdDSCByIndex(actionForm.getRowSelected());
            
            
            AdDiscountDetails details = new AdDiscountDetails();
            details.setDscCode(adDSCList.getDiscountCode());
            details.setDscDiscountPercent(Common.convertStringToDouble(actionForm.getDiscountPercent()));
            details.setDscPaidWithinDay(Common.convertStringToShort(actionForm.getPaidWithinDay()));
            
            try {
            	
            	ejbDSC.updateAdDscEntry(details, actionForm.getPaymentScheduleCode(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("discount.error.paidWithinDayAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdDiscountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad DSC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad DSC Edit Action --
*******************************************************/

         } else if (request.getParameter("adDSCList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdDSCRow(actionForm.getRowSelected());
            
            return mapping.findForward("adDiscount");
            
/*******************************************************
   -- Ad DSC Delete Action --
*******************************************************/

         } else if (request.getParameter("adDSCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdDiscountList adDSCList =
              actionForm.getAdDSCByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbDSC.deleteAdDscEntry(adDSCList.getDiscountCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("discount.error.discountAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdDiscountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad DSC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adDiscount");

            }
                                  	                        	                          	            	
            AdPaymentScheduleDetails psDetails = null;

	        if (request.getParameter("forward") != null) {

	            psDetails = ejbDSC.getAdPsByPsCode(new Integer(request.getParameter("paymentScheduleCode")), user.getCmpCode());        		
	    	    actionForm.setPaymentScheduleCode(new Integer(request.getParameter("paymentScheduleCode")));

	        } else {
	     		
	            psDetails = ejbDSC.getAdPsByPsCode(actionForm.getPaymentScheduleCode(), user.getCmpCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setPaymentScheduleCode(actionForm.getPaymentScheduleCode());
            actionForm.setLineNumber(Common.convertShortToString(psDetails.getPsLineNumber()));
            actionForm.setRelativeAmount(Common.convertDoubleToStringMoney(psDetails.getPsRelativeAmount(), Constants.DEFAULT_PRECISION));        		        		
    	    actionForm.setDueDay(Common.convertShortToString(psDetails.getPsDueDay()));

            ArrayList list = null;
            Iterator i = null;
                        
	        try {
	        	
	           actionForm.clearAdDSCList();
	        		        		    	
	           list = ejbDSC.getAdDscByPsCode(actionForm.getPaymentScheduleCode(), user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdDiscountDetails details = (AdDiscountDetails)i.next();
	            	
	              AdDiscountList adDSCList = new AdDiscountList(actionForm,
	            	    details.getDscCode(),
	            	    Common.convertDoubleToStringMoney(details.getDscDiscountPercent(), Short.parseShort("3")),
	            	    Common.convertShortToString(details.getDscPaidWithinDay()));
	            	 	            	    
	              actionForm.saveAdDSCList(adDSCList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdDiscountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adDiscount"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdDiscountAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}