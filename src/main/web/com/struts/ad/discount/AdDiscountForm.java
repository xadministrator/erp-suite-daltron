package com.struts.ad.discount;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdDiscountForm extends ActionForm implements Serializable {

   private Integer discountCode = null;
   private String lineNumber = null;
   private String relativeAmount = null;
   private String dueDay = null;   
   private String discountPercent = null;
   private String paidWithinDay = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	      
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList adDSCList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private String paymentSchedules = null;
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdDiscountList getAdDSCByIndex(int index) {

      return((AdDiscountList)adDSCList.get(index));

   }

   public Object[] getAdDSCList() {

      return adDSCList.toArray();

   }

   public int getAdDSCListSize() {

      return adDSCList.size();

   }

   public void saveAdDSCList(Object newAdDSCList) {

      adDSCList.add(newAdDSCList);

   }

   public void clearAdDSCList() {
   	
      adDSCList.clear();
      
   }

   public void setRowSelected(Object selectedAdDSCList, boolean isEdit) {

      this.rowSelected = adDSCList.indexOf(selectedAdDSCList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdDSCRow(int rowSelected) {
   	
       this.discountPercent = ((AdDiscountList)adDSCList.get(rowSelected)).getDiscountPercent();
       this.paidWithinDay = ((AdDiscountList)adDSCList.get(rowSelected)).getPaidWithinDay();
       
   }

   public void updateAdDSCRow(int rowSelected, Object newAdDSCList) {

      adDSCList.set(rowSelected, newAdDSCList);

   }

   public void deleteAdDSCList(int rowSelected) {

      adDSCList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getPaymentScheduleCode() {
   	
   	  return discountCode;
   	
   }
   
   public void setPaymentScheduleCode(Integer discountCode) {
   	
   	  this.discountCode = discountCode;
   	
   }
   
   public String getLineNumber() {

      return lineNumber;

   }

   public void setLineNumber(String lineNumber) {

      this.lineNumber = lineNumber;

   }
   
   public String getRelativeAmount() {

      return relativeAmount;

   }

   public void setRelativeAmount(String relativeAmount) {

      this.relativeAmount = relativeAmount;

   }

   public String getDueDay() {

      return dueDay;

   }

   public void setDueDay(String dueDay) {

      this.dueDay = dueDay;

   }
   
   public String getDiscountPercent() {
   	
   	  return discountPercent;
   	  
   }
   
   public void setDiscountPercent(String discountPercent) {
   	
   	  this.discountPercent = discountPercent;
   	  
   }
   
   public String getPaidWithinDay() {
   	
   	  return paidWithinDay;
   	  
   }
   
   public void setPaidWithinDay(String paidWithinDay) {
   	
   	  this.paidWithinDay = paidWithinDay;
   	  
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }      
   
   public void setPaymentSchedules(String paymentSchedules) {
   	
   	  this.paymentSchedules = paymentSchedules;
   	  
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      discountPercent = null; 
      paidWithinDay = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(discountPercent)) {

            errors.add("discountPercent",
               new ActionMessage("discount.error.discountPercentRequired"));

         }

         if (Common.validateRequired(paidWithinDay)) {

            errors.add("paidWithinDay",
               new ActionMessage("discount.error.paidWithinDayRequired"));

         }
         
         if (!Common.validateNumberFormat(discountPercent)) {

            errors.add("discountPercent",
               new ActionMessage("discount.error.discountPercentInvalid"));

         }                  
         
         if (!Common.validateMoneyFormat(paidWithinDay)) {

            errors.add("paidWithinDay",
               new ActionMessage("discount.error.paidWithinDayInvalid"));

         }         
         
      }
         
      return errors;

   }

}