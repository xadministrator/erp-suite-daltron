package com.struts.ad.discount;

import java.io.Serializable;

public class AdDiscountList implements Serializable {

   private Integer discountCode = null;
   private String discountPercent = null;
   private String paidWithinDay = null;

   private String editButton = null;
   private String deleteButton = null;
    
   private AdDiscountForm parentBean;
    
   public AdDiscountList(AdDiscountForm parentBean,
      Integer discountCode,
      String discountPercent,
      String paidWithinDay) {

      this.parentBean = parentBean;
      this.discountCode = discountCode;
      this.discountPercent = discountPercent;
      this.paidWithinDay = paidWithinDay;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   
   
   public Integer getDiscountCode() {

      return discountCode;

   }
   
   public String getDiscountPercent() {
       
       return discountPercent;
       
   }

   public String getPaidWithinDay() {

      return paidWithinDay;

   }
   
}