package com.struts.ad.documentcategories;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdDocumentCategoryController;
import com.ejb.txn.AdDocumentCategoryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdDocumentCategoryDetails;
import com.util.AdModDocumentCategoryDetails;

public final class AdDocumentCategoriesAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdDocumentCategoriesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdDocumentCategoriesForm actionForm = (AdDocumentCategoriesForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_DOCUMENT_CATEGORY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adDocumentCategories");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdDocumentCategoryController EJB
*******************************************************/

         AdDocumentCategoryControllerHome homeDC = null;
         AdDocumentCategoryController ejbDC = null;

         try {

            homeDC = (AdDocumentCategoryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdDocumentCategoryControllerEJB", AdDocumentCategoryControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdDocumentCategoriesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbDC = homeDC.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdDocumentCategoriesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad DC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adDocumentCategories"));
	     
/*******************************************************
   -- Ad DC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adDocumentCategories"));                  
         
/*******************************************************
   -- Ad DC Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdDocumentCategoryDetails details = new AdDocumentCategoryDetails(actionForm.getDocumentName(), actionForm.getDescription());
            
            try {
            	
            	ejbDC.addAdDcEntry(details, actionForm.getApplicationName(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("documentCategories.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdDocumentCategoriesAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad DC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad DC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            AdDocumentCategoriesList adDCList =
	            actionForm.getAdDCByIndex(actionForm.getRowSelected());
            
            
            AdDocumentCategoryDetails details = new AdDocumentCategoryDetails(adDCList.getDocumentCategoryCode(),
                actionForm.getDocumentName(), actionForm.getDescription());
            
            try {
            	
            	ejbDC.updateAdDcEntry(details, actionForm.getApplicationName(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("documentCategories.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdDocumentCategoriesAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad DC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad DC Edit Action --
*******************************************************/

         } else if (request.getParameter("adDCList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdDCRow(actionForm.getRowSelected());
            
            return mapping.findForward("adDocumentCategories");
            
/*******************************************************
   -- Ar DC Delete Action --
*******************************************************/

         } else if (request.getParameter("adDCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdDocumentCategoriesList adDCList =
              actionForm.getAdDCByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbDC.deleteAdDcEntry(adDCList.getDocumentCategoryCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("documentCategories.error.deleteDocumentCategoryAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("documentCategories.error.documentCategoryAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdDocumentCategoriesAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad DC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adDocumentCategories");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	
                        	                          	
            	
	        try {
	    	
	    	   actionForm.clearApplicationNameList();
	            	                        
	           list = ejbDC.getAdAppAll(user.getCmpCode()); 
	        
               if (list == null || list.size() == 0) {
            		
                  actionForm.setApplicationNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
               } else {
            		           		            		
                  i = list.iterator();
            		
                  while (i.hasNext()) {
            			
                     actionForm.setApplicationNameList((String)i.next());
            			
                  }
            		
               }
                              
               actionForm.clearAdDCList();	        
	           
	           list = ejbDC.getAdDcAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdModDocumentCategoryDetails mdetails = (AdModDocumentCategoryDetails)i.next();
	            	
	              AdDocumentCategoriesList adDCList = new AdDocumentCategoriesList(actionForm,
	            	    mdetails.getDcCode(),
	            	    mdetails.getDcAppName(),
	            	    mdetails.getDcName(),
	            	    mdetails.getDcDescription());                            
	            	 	            	    
	              actionForm.saveAdDCList(adDCList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	           
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdDocumentCategoriesAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adDocumentCategories"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdDocumentCategoriesAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}