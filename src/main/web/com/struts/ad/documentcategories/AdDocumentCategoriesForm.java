package com.struts.ad.documentcategories;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdDocumentCategoriesForm extends ActionForm implements Serializable {

   private Integer documentCategoryCode = null;
   private String applicationName = null;
   private ArrayList applicationNameList = new ArrayList();
   private String documentName = null;
   private String description = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	   
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList adDCList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdDocumentCategoriesList getAdDCByIndex(int index) {

      return((AdDocumentCategoriesList)adDCList.get(index));

   }

   public Object[] getAdDCList() {

      return adDCList.toArray();

   }

   public int getAdDCListSize() {

      return adDCList.size();

   }

   public void saveAdDCList(Object newAdDCList) {

      adDCList.add(newAdDCList);

   }

   public void clearAdDCList() {
      adDCList.clear();
   }

   public void setRowSelected(Object selectedAdDCList, boolean isEdit) {

      this.rowSelected = adDCList.indexOf(selectedAdDCList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdDCRow(int rowSelected) {
   	
       this.applicationName = ((AdDocumentCategoriesList)adDCList.get(rowSelected)).getApplicationName();
       this.documentName = ((AdDocumentCategoriesList)adDCList.get(rowSelected)).getDocumentName();
       this.description = ((AdDocumentCategoriesList)adDCList.get(rowSelected)).getDescription();
              
   }

   public void updateAdDCRow(int rowSelected, Object newAdDCList) {

      adDCList.set(rowSelected, newAdDCList);

   }

   public void deleteAdDCList(int rowSelected) {

      adDCList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getDocumentCategoryCode() {
   	
   	  return documentCategoryCode;
   	
   }
   
   public void setDocumentCategoryCode(Integer documentCategoryCode) {
   	
   	  this.documentCategoryCode = documentCategoryCode;
   	
   }

   public String getApplicationName() {

      return applicationName;

   }

   public void setApplicationName(String applicationName) {

      this.applicationName = applicationName;

   }
   
   public ArrayList getApplicationNameList() {
   	
   	   return applicationNameList;
   	
   }
   
   public void setApplicationNameList(String applicationName) {

      applicationNameList.add(applicationName);

   }

   public void clearApplicationNameList() {

      applicationNameList.clear();
      applicationNameList.add(Constants.GLOBAL_BLANK);
   }      
   
   public String getDocumentName() {

      return documentName;

   }

   public void setDocumentName(String documentName) {

      this.documentName = documentName;

   }

   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }         

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      applicationName = Constants.GLOBAL_BLANK; 
      documentName = null;
      description = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(applicationName)) {

            errors.add("applicationName",
               new ActionMessage("documentCategories.error.applicationNameRequired"));

         }

         if (Common.validateRequired(documentName)) {

            errors.add("documentName",
               new ActionMessage("documentCategories.error.documentNameRequired"));

         }

      }
         
      return errors;

   }
}