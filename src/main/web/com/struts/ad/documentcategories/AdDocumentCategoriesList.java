package com.struts.ad.documentcategories;

import java.io.Serializable;

public class AdDocumentCategoriesList implements Serializable {

   private Integer documentCategoryCode = null;
   private String applicationName = null;
   private String documentName = null;
   private String description = null;

   private String editButton = null;
   private String deleteButton = null;
    
   private AdDocumentCategoriesForm parentBean;
    
   public AdDocumentCategoriesList(AdDocumentCategoriesForm parentBean,
      Integer documentCategoryCode,
      String applicationName,
      String documentName,
      String description) {

      this.parentBean = parentBean;
      this.documentCategoryCode = documentCategoryCode;
      this.applicationName = applicationName;
      this.documentName = documentName;
      this.description = description;      

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   

   public Integer getDocumentCategoryCode() {

      return documentCategoryCode;

   }
   
   public String getApplicationName() {
       
       return applicationName;
       
   }

   public String getDocumentName() {

      return documentName;

   }

   public String getDescription() {

      return description;

   }

}