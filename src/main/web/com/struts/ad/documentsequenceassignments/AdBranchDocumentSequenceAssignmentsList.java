package com.struts.ad.documentsequenceassignments;

import java.io.Serializable;

public class AdBranchDocumentSequenceAssignmentsList implements Serializable {
	
	private String bdsName = null;
	private Integer bdsCode = null;
	private boolean branchCheckbox = false;
	private String nextSequence = null;
	
	private AdDocumentSequenceAssignmentsForm parentBean;
	
	public AdBranchDocumentSequenceAssignmentsList(AdDocumentSequenceAssignmentsForm parentBean, Integer bdsCode, 
			String bdsName , String nextSequence) {
		
		this.parentBean = parentBean;
		this.bdsCode = bdsCode;
		this.bdsName = bdsName;		
		this.nextSequence = nextSequence;
		
	}
	
	public String getBdsName() {
		
		return bdsName;
		
	}
	
	public void setBdsName(String bdsName) {
		
		this.bdsName = bdsName;
		
	}
	
	public Integer getBdsCode() {
		
		return bdsCode;
		
	}
	
	public void setBdsCode(Integer bdsCode) {
		
		this.bdsCode = bdsCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
	public String getNextSequence() {
	    
	    return nextSequence;
	    
	}
	
	public void setNextSequence(String nextSequence) {
	    
	    this.nextSequence = nextSequence;
	    
	}
}