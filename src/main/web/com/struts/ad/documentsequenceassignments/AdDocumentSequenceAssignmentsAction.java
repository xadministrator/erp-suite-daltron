package com.struts.ad.documentsequenceassignments;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdDCDocumentCategoryAlreadyExistException;
import com.ejb.exception.AdDCNoDocumentCategoryFoundException;
import com.ejb.exception.AdDSADocumentSequenceAssignmentAlreadyAssignedException;
import com.ejb.exception.AdDSADocumentSequenceAssignmentAlreadyDeletedException;
import com.ejb.exception.AdDSANoDocumentSequenceAssignmentFoundException;
import com.ejb.exception.AdDSNoDocumentSequenceFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.AdDocumentSequenceAssignmentController;
import com.ejb.txn.AdDocumentSequenceAssignmentControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdDocumentCategoryDetails;
import com.util.AdDocumentSequenceAssignmentDetails;
import com.util.AdDocumentSequenceDetails;
import com.util.AdModBranchDocumentSequenceAssignmentDetails;
import com.util.AdModDocumentSequenceAssignmentDetails;
import com.util.AdResponsibilityDetails;

public final class AdDocumentSequenceAssignmentsAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("AdDocumentSequenceAssignmentsAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         
         AdDocumentSequenceAssignmentsForm actionForm = (AdDocumentSequenceAssignmentsForm) form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_DOCUMENT_SEQUENCE_ASSIGNMENTS_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((AdDocumentSequenceAssignmentsForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("adDocumentSequenceAssignments"));
               }
            }
            ((AdDocumentSequenceAssignmentsForm)form).setUserPermission(frParam.trim());
         }else{
            ((AdDocumentSequenceAssignmentsForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize AdDocumentSequenceAssignmentController EJB
*******************************************************/

         AdDocumentSequenceAssignmentControllerHome homeDSA = null;
         AdDocumentSequenceAssignmentController ejbDSA = null;

         try{
            
            homeDSA = (AdDocumentSequenceAssignmentControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/AdDocumentSequenceAssignmentControllerEJB", AdDocumentSequenceAssignmentControllerHome.class);
              
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in AdDocumentSequenceAssignmentsAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbDSA = homeDSA.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in AdDocumentSequenceAssignmentsAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Ad  DSA Show Details Action --
*******************************************************/
      
         if (request.getParameter("showDetailsButton") != null) {
      		      	
	         ((AdDocumentSequenceAssignmentsForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	         return(mapping.findForward("adDocumentSequenceAssignments"));
	     
/*******************************************************
   -- Ad  DSA Hide Details Action --
*******************************************************/	     

         } else if (request.getParameter("hideDetailsButton") != null) {
         	
         	
         	((AdDocumentSequenceAssignmentsForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
         	
         	return(mapping.findForward("adDocumentSequenceAssignments"));
         	             	      	      	
/*******************************************************
   -- Ad  DSA Save Action --
*******************************************************/

         } else if(request.getParameter("saveButton") != null &&
            ((AdDocumentSequenceAssignmentsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

             AdDocumentSequenceAssignmentDetails details = new AdDocumentSequenceAssignmentDetails();
                          
             ArrayList branchList = new ArrayList();
             
             for(int i=0; i<actionForm.getAdBDSListSize(); i++) {
                 
                 AdBranchDocumentSequenceAssignmentsList adBrDsaList = actionForm.getAdBDSByIndex(i);
                 
                 if(adBrDsaList.getBranchCheckbox() == true) {                                          
                     
                     AdModBranchDocumentSequenceAssignmentDetails brDsaDetails = new AdModBranchDocumentSequenceAssignmentDetails();                                          
                     
                     brDsaDetails.setBdsAdCompany(user.getCmpCode());
                     
                     if(adBrDsaList.getNextSequence() == null || adBrDsaList.getNextSequence().length() < 1)
                         adBrDsaList.setNextSequence(actionForm.getNextSequence());
                     
                     brDsaDetails.setBdsNextSequence(adBrDsaList.getNextSequence());                     
                     brDsaDetails.setBrCode(adBrDsaList.getBdsCode());                    
                     
                     branchList.add(brDsaDetails);
                 }
             }

/*******************************************************
   Call AdDocumentSequenceAssignmentController EJB 
   addAdDsaEntry method
*******************************************************/

	       try{
	          ejbDSA.addAdDsaEntry(details, 
		     ((AdDocumentSequenceAssignmentsForm)form).getDocumentCategory().trim().toUpperCase(),
		     ((AdDocumentSequenceAssignmentsForm)form).getDocumentSequence().trim().toUpperCase(), 
		     branchList, user.getCmpCode());
	       }catch(AdDCNoDocumentCategoryFoundException ex){
	          errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("documentSequenceAssignments.error.documentCategoryNotFound"));
	       }catch(AdDSNoDocumentSequenceFoundException ex){
	          errors.add(ActionMessages.GLOBAL_MESSAGE,
		     new ActionMessage("documentSequenceAssignments.error.documentSequenceNotFound"));
	       }catch(AdDCDocumentCategoryAlreadyExistException ex){
	          errors.add(ActionMessages.GLOBAL_MESSAGE,
		     new ActionMessage("documentSequenceAssignments.error.documentCategoryAlreadyExists"));
	       }catch(EJBException ex){
	          if(log.isInfoEnabled()){
                    log.info("EJBException caught in AdDocumentSequenceAssignmentsAction.execute(): " + ex.getMessage() +
                    " session: " + session.getId());
                  }
                  return(mapping.findForward("cmnErrorPage"));
	       }

/*******************************************************
   -- Ad  DSA Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad  DSA Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
                 ((AdDocumentSequenceAssignmentsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
             
             AdDocumentSequenceAssignmentsList adDSAList =
                 ((AdDocumentSequenceAssignmentsForm)form).getAdDSAByIndex(
                 ((AdDocumentSequenceAssignmentsForm) form).getRowSelected());
             
             AdDocumentSequenceAssignmentDetails details = new AdDocumentSequenceAssignmentDetails(
                     adDSAList.getDocumentSequenceAssignmentCode(), new Integer(1), ((AdDocumentSequenceAssignmentsForm)form).getNextSequence());
             
             
             ArrayList branchList = new ArrayList();
             
             for(int i=0; i<actionForm.getAdBDSListSize(); i++) {
                 
                 AdBranchDocumentSequenceAssignmentsList adBrDsaList = actionForm.getAdBDSByIndex(i);
                 
                 if(adBrDsaList.getBranchCheckbox() == true) {                                          
                     
                     AdModBranchDocumentSequenceAssignmentDetails brDsaDetails = new AdModBranchDocumentSequenceAssignmentDetails();                                          
                     
                     brDsaDetails.setBdsAdCompany(user.getCmpCode());
                     
                     if(adBrDsaList.getNextSequence() == null || adBrDsaList.getNextSequence().length() < 1)
                         adBrDsaList.setNextSequence(actionForm.getNextSequence());
                     
                     brDsaDetails.setBdsNextSequence(adBrDsaList.getNextSequence());
                     brDsaDetails.setBrCode(adBrDsaList.getBdsCode());
                     
                     branchList.add(brDsaDetails);
                     
                 }
                 
             }
             
             AdResponsibilityDetails mdetails = ejbDSA.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
        
/*******************************************************
   Call AdDocumentSequenceAssignmentController EJB
   updateAdDsaEntry method
*******************************************************/
      
	    try{
	       ejbDSA.updateAdDsaEntry(details, 
                  ((AdDocumentSequenceAssignmentsForm)form).getDocumentCategory().trim().toUpperCase(),
                  ((AdDocumentSequenceAssignmentsForm)form).getDocumentSequence().trim().toUpperCase(), 
                  mdetails.getRsName(), branchList, user.getCmpCode());
	    }catch(AdDCNoDocumentCategoryFoundException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("documentSequenceAssignments.error.documentCategoryNotFound"));
            }catch(AdDSNoDocumentSequenceFoundException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("documentSequenceAssignments.error.documentSequenceNotFound"));
            }catch(AdDCDocumentCategoryAlreadyExistException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("documentSequenceAssignments.error.documentCategoryAlreadyExists"));
            }catch(AdDSADocumentSequenceAssignmentAlreadyDeletedException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
	          new ActionMessage("documentSequenceAssignments.error.documentSequenceAssignmentAlreadyDeleted"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in AdDocumentSequenceAssignmentsAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

/*******************************************************
   -- Ad  DSA Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Ad  DSA Edit Action --
*******************************************************/

         }else if(request.getParameter("adDSAList[" + 
            ((AdDocumentSequenceAssignmentsForm)form).getRowSelected() + "].editButton") != null &&
            ((AdDocumentSequenceAssignmentsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
             System.out.println("EDIT BUTTON");
             actionForm.reset(mapping, request);
             
             Object[] adDSAList = actionForm.getAdDSAList();
             
             ArrayList list = null;
             
             try {                                  
                 
                 list = ejbDSA.getAdBrDsaAll(((AdDocumentSequenceAssignmentsList)adDSAList[actionForm.getRowSelected()]).getDocumentSequenceAssignmentCode(), user.getCmpCode());
                 
             } catch(GlobalNoRecordFoundException ex) {
                 
                 
             }

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate AdDocumentSequenceAssignmentsForm properties
*******************************************************/             
           ((AdDocumentSequenceAssignmentsForm)form).showAdDSARow(list,((AdDocumentSequenceAssignmentsForm) form).getRowSelected());
           System.out.println("END EDIT BUTTON");
           return(mapping.findForward("adDocumentSequenceAssignments"));
	   
/*******************************************************
   -- Ad  DSA Delete Action --
*******************************************************/

         }else if(request.getParameter("adDSAList[" +
            ((AdDocumentSequenceAssignmentsForm)form).getRowSelected() + "].deleteButton") != null &&
            ((AdDocumentSequenceAssignmentsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

             AdDocumentSequenceAssignmentsList adDSAList =
               ((AdDocumentSequenceAssignmentsForm)form).getAdDSAByIndex(
                  ((AdDocumentSequenceAssignmentsForm) form).getRowSelected());

/*****************************************************
   Call AdDocumentSequenceAssignmentController EJB 
   deleteAdDsaEntry method
*******************************************************/

	     try{
	        ejbDSA.deleteAdDsaEntry(adDSAList.getDocumentSequenceAssignmentCode(), user.getCmpCode());
             }catch(AdDSADocumentSequenceAssignmentAlreadyDeletedException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
	           new ActionMessage("documentSequenceAssignments.error.documentSequenceAssignmentAlreadyDeleted"));
             }catch(AdDSADocumentSequenceAssignmentAlreadyAssignedException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
		   new ActionMessage("documentSequenceAssignments.error.documentSequenceAssignmentAlreadyAssigned"));
	     }catch(EJBException ex){
	        if(log.isInfoEnabled()){
	           log.info("EJBException caught in AdDocumentSequenceAssignmentsAction.execute(): " + ex.getMessage() +
	           " session: " + session.getId());
	        }
	        return(mapping.findForward("cmnErrorPage"));
	     }
					    
/*******************************************************
   -- Ad  DSA Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("adDocumentSequenceAssignments"));
            }

	    ((AdDocumentSequenceAssignmentsForm)form).clearAdDSAList();

/*******************************************************
   Call AdDocumentSequenceAssignmentController EJB 
   getAdDcAll method
   Populate documentCategoryList from method ArrayList
   return
*******************************************************/
            
	    ((AdDocumentSequenceAssignmentsForm)form).clearDocumentCategoryList();
	    Iterator i = null;
	    
	    try{
	       ArrayList ejbAdDCList = ejbDSA.getAdDcAll(user.getCmpCode());
	       i = ejbAdDCList.iterator();
	       while(i.hasNext()){
	          ((AdDocumentSequenceAssignmentsForm)form).setDocumentCategoryList(
		     ((AdDocumentCategoryDetails)i.next()).getDcName());
	       }
	    }catch(AdDCNoDocumentCategoryFoundException ex){
	       ((AdDocumentSequenceAssignmentsForm)form).setDocumentCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	    }catch(EJBException ex){
	       if(log.isInfoEnabled()){
	          log.info("EJBException caught in AdDocumentSequenceAssignmentsAction.execute(): " + ex.getMessage() +
	          " session: " + session.getId());
	       }
	       return(mapping.findForward("cmnErrorPage"));
	    }

/*******************************************************
   Call AdDocumentSequenceAssignmentController EJB
   getAdDsAll method
   Populate documentSequenceList from method ArrayList
   return
*******************************************************/

           ((AdDocumentSequenceAssignmentsForm)form).clearDocumentSequenceList();
            try{
               ArrayList ejbAdDSList = ejbDSA.getAdDsAll(user.getCmpCode());
               i = ejbAdDSList.iterator();
               while(i.hasNext()){
                  ((AdDocumentSequenceAssignmentsForm)form).setDocumentSequenceList(
                  ((AdDocumentSequenceDetails)i.next()).getDsSequenceName());
               }
            }catch(AdDSNoDocumentSequenceFoundException ex){
               ((AdDocumentSequenceAssignmentsForm)form).setDocumentSequenceList(Constants.GLOBAL_NO_RECORD_FOUND);
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in AdDocumentSequenceAssignmentsAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

/*******************************************************
   Call AdDocumentAssignmentSequenceController EJB
   getAdDsaAll method
   Populate ArrayList property from
   ArrayList return
*******************************************************/

            try{
            	
            	
               ArrayList ejbAdDSAList = ejbDSA.getAdDsaAll(user.getCmpCode());
               
               
               i = ejbAdDSAList.iterator();
               
               while(i.hasNext()){
            	   
               AdModDocumentSequenceAssignmentDetails mdetails = (AdModDocumentSequenceAssignmentDetails)i.next();
               AdDocumentSequenceAssignmentsList adDSAList = new AdDocumentSequenceAssignmentsList(
	          ((AdDocumentSequenceAssignmentsForm)form),
                  mdetails.getDsaCode(),
                  mdetails.getDsaDcName(),
                  mdetails.getDsaDsName(),
				  mdetails.getDsaNextSequence());
		  
                  ((AdDocumentSequenceAssignmentsForm)form).saveAdDSAList(adDSAList);
                  
                }
               
             }catch(AdDSANoDocumentSequenceAssignmentFoundException ex){
            	 
            	 
                
             }catch(EJBException ex){
            	 
            	 
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in AdDocumentSequenceAssignmentsAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                }
                return(mapping.findForward("cmnErrorPage"));
             }
                                                  
             try {
             	
             	((AdDocumentSequenceAssignmentsForm)form).reset(mapping, request);
             	
             	 ArrayList list = null;
                 i = null;
                 
                 actionForm.clearAdBDSList();
                 
                 list = ejbDSA.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                 
                 i = list.iterator();
                 
                 while(i.hasNext()) {
                 
                     AdBranchDetails details = (AdBranchDetails)i.next();
                 
                     AdBranchDocumentSequenceAssignmentsList adBDSList = new AdBranchDocumentSequenceAssignmentsList(
                    		 actionForm,
                         details.getBrCode(),
                         details.getBrName(),                         
                         null);
                     if ( request.getParameter("forward") == null )
                     	adBDSList.setBranchCheckbox(true);
                                  
                     actionForm.saveAdBDSList(adBDSList);
                     
                 }
                 
             } catch (GlobalNoRecordFoundException ex) {
                 
             } catch (EJBException ex) {
                 
                 if (log.isInfoEnabled()) {
                     
                     log.info("EJBException caught in AdDocumentSequenceAssignmentsAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage"); 
                     
                 }
                 
             }        
															
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
            }else{
               if((request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null || 
                   request.getParameter("adDSAList[" + 
                   ((AdDocumentSequenceAssignmentsForm) form).getRowSelected() + "].deleteButton") != null) && 
                   ((AdDocumentSequenceAssignmentsForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                   ((AdDocumentSequenceAssignmentsForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
               }
            }

            if (((AdDocumentSequenceAssignmentsForm)form).getTableType() == null) {
                
                ((AdDocumentSequenceAssignmentsForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
                
            }                
                
            ((AdDocumentSequenceAssignmentsForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("adDocumentSequenceAssignments"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()){
             log.info("Exception caught in AdDocumentSequenceAssignmentsAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
