package com.struts.ad.documentsequenceassignments;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdModBranchDocumentSequenceAssignmentDetails;


public class AdDocumentSequenceAssignmentsForm extends ActionForm implements Serializable {

   private String documentCategory = null;
   private ArrayList documentCategoryList = new ArrayList();
   private String documentSequence = null;
   private ArrayList documentSequenceList = new ArrayList();
   private String nextSequence = null;
   private String tableType = null;

   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;
   private String pageState = new String();
   private ArrayList adDSAList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private ArrayList adBDSList = new ArrayList();

   public int getRowSelected(){
      return rowSelected;
   }

   public AdDocumentSequenceAssignmentsList getAdDSAByIndex(int index){
      return((AdDocumentSequenceAssignmentsList)adDSAList.get(index));
   }

   public Object[] getAdDSAList(){
      return(adDSAList.toArray());
   }

   public int getAdDSAListSize(){
      return(adDSAList.size());
   }

   public void saveAdDSAList(Object newAdDSAList){
      adDSAList.add(newAdDSAList);
   }

   public void clearAdDSAList(){
      adDSAList.clear();
   }

   public void setRowSelected(Object selectedAdDSAList, boolean isEdit){
      this.rowSelected = adDSAList.indexOf(selectedAdDSAList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showAdDSARow(ArrayList branchList, int rowSelected){
      
	   System.out.println("showAdDSARow");
	   
	   this.documentCategory = ((AdDocumentSequenceAssignmentsList)adDSAList.get(rowSelected)).getDocumentCategory();
      this.documentSequence = ((AdDocumentSequenceAssignmentsList)adDSAList.get(rowSelected)).getDocumentSequence();
      this.nextSequence = ((AdDocumentSequenceAssignmentsList)adDSAList.get(rowSelected)).getNextSequence();
      
      Object[] obj = this.getAdBDSList();
      
      this.clearAdBDSList();
      
      for(int i=0; i<obj.length; i++) {
          
          AdBranchDocumentSequenceAssignmentsList brDsList = (AdBranchDocumentSequenceAssignmentsList)obj[i];
          
          if(branchList != null) {
              
              Iterator brListIter = branchList.iterator();
              
              while(brListIter.hasNext()) {
                  
                  AdModBranchDocumentSequenceAssignmentDetails brDsaDetails = (AdModBranchDocumentSequenceAssignmentDetails)brListIter.next();
                  
                  System.out.println("brDsaDetails.getBrCode()="+brDsaDetails.getBrCode());
                  System.out.println("brDsList.getBdsCode()="+brDsList.getBdsCode());
                  if(brDsaDetails.getBrCode().equals(brDsList.getBdsCode())) {
                      
                      brDsList.setBranchCheckbox(true);
                      System.out.println("brDsaDetails.getBdsNextSequence()="+brDsaDetails.getBdsNextSequence());
                      brDsList.setNextSequence(brDsaDetails.getBdsNextSequence());                      
                      break;
                      
                  }
                  
              }
          }
          
          this.adBDSList.add(brDsList);
          
      }
            
   }

   public void updateAdDSARow(int rowSelected, Object newAdDSAList){
      adDSAList.set(rowSelected, newAdDSAList);
   }

   public void deleteAdDSAList(int rowSelected){
      adDSAList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
   	
   	  this.showDetailsButton = showDetailsButton;
   	  
   }   	  
   	  
   public void setHideDetailsButton(String hideDetailsButton) {
   	
   	  this.hideDetailsButton = hideDetailsButton;
   	  
   }
   	     	  
   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getDocumentCategory(){
      return(documentCategory);
   }

   public void setDocumentCategory(String documentCategory){
      this.documentCategory = documentCategory;
   }

   public ArrayList getDocumentCategoryList(){
      return(documentCategoryList);
   }

   public void setDocumentCategoryList(String documentCategory){
      documentCategoryList.add(documentCategory);
   }

   public void clearDocumentCategoryList(){
      documentCategoryList.clear();
      documentCategoryList.add(Constants.GLOBAL_BLANK);
   }

   public String getDocumentSequence(){
      return(documentSequence);
   }

   public void setDocumentSequence(String documentSequence){
      this.documentSequence = documentSequence;
   }

   public ArrayList getDocumentSequenceList(){
      return(documentSequenceList);
   }

   public void setDocumentSequenceList(String documentSequence){
      documentSequenceList.add(documentSequence);
   }

   public void clearDocumentSequenceList(){
       documentSequenceList.clear();
       documentSequenceList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getNextSequence() {
       
       return nextSequence;
       
   }
   
   public void setNextSequence(String nextSequence) {
       
       this.nextSequence = nextSequence;
       
   }
   
   public Object[] getAdBDSList(){
       
       return adBDSList.toArray();
       
   }
   
   public AdBranchDocumentSequenceAssignmentsList getAdBDSByIndex(int index){
       
       return ((AdBranchDocumentSequenceAssignmentsList)adBDSList.get(index));
       
   }
   
   public int getAdBDSListSize(){
       
       return(adBDSList.size());
       
   }
   
   public void saveAdBDSList(Object newAdBDSList){
       
       adBDSList.add(newAdBDSList);   	  
       
   }
   
   public void clearAdBDSList(){
       
       adBDSList.clear();
       
   }
   
   public void setAdBDSList(ArrayList adBDSList) {
       
       this.adBDSList = adBDSList;
       
   }
   
   public String getTableType() {
   	
   	  return(tableType);
   	  
   }
   
   public void setTableType(String tableType) {
   	
   	  this.tableType = tableType;
   	  
   } 

   public void reset(ActionMapping mapping, HttpServletRequest request){
       
       for (int i=0; i<adBDSList.size(); i++) {
           
           AdBranchDocumentSequenceAssignmentsList list = (AdBranchDocumentSequenceAssignmentsList)adBDSList.get(i);
           list.setBranchCheckbox(false);
           list.setNextSequence(null);
           
       } 
       
       documentCategory = Constants.GLOBAL_BLANK;
       documentSequence = Constants.GLOBAL_BLANK;
       nextSequence = null;
       saveButton = null;
       updateButton = null;
       cancelButton = null;
       closeButton = null;
       showDetailsButton = null;
       hideDetailsButton = null;
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(documentCategory)){
            errors.add("documentCategory",
               new ActionMessage("documentSequenceAssignments.error.documentCategoryRequired"));
         }
         if(documentCategory.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("documentCategory",
               new ActionMessage("documentSequenceAssignments.error.documentCategoryRequired"));
         }
         if(!Common.validateStringExists(documentCategoryList, documentCategory)){
            errors.add("documentCategory",
               new ActionMessage("documentSequenceAssignments.error.documentCategoryInvalid"));
         }
         if(Common.validateRequired(documentSequence)){
            errors.add("documentSequence",
               new ActionMessage("documentSequenceAssignments.error.documentSequenceRequired"));
         }
         if(documentSequence.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("documentSequence",
               new ActionMessage("documentSequenceAssignments.error.documentSequenceRequired"));
         }
         if(!Common.validateStringExists(documentSequenceList, documentSequence)){
            errors.add("documentSequence",
               new ActionMessage("documentSequenceAssignments.error.documentSequenceInvalid"));
         }
         if(Common.validateRequired(nextSequence)){
            errors.add("nextSequence",
               new ActionMessage("documentSequenceAssignments.error.nextSequenceRequired"));
         }
      }
      return(errors);
   }
     
}
