package com.struts.ad.documentsequenceassignments;

import java.io.Serializable;

public class AdDocumentSequenceAssignmentsList implements Serializable {

   private Integer documentSequenceAssignmentCode = null;
   private String documentCategory = null;
   private String documentSequence = null;
   private String nextSequence = null;

   private String deleteButton = null;
   private String editButton = null;
    
   private AdDocumentSequenceAssignmentsForm parentBean;
    
   public AdDocumentSequenceAssignmentsList(AdDocumentSequenceAssignmentsForm parentBean,
      Integer documentSequenceAssignmentCode,
      String documentCategory,
      String documentSequence,
	  String nextSequence){

      this.parentBean = parentBean;
      this.documentSequenceAssignmentCode = documentSequenceAssignmentCode;
      this.documentCategory = documentCategory;
      this.documentSequence = documentSequence;
      this.nextSequence = nextSequence;
   }

   public void setDeleteButton(String deleteButton){
      parentBean.setRowSelected(this, false);
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public Integer getDocumentSequenceAssignmentCode(){
      return(documentSequenceAssignmentCode);
   }

   public String getDocumentCategory(){
      return(documentCategory);
   }

   public String getDocumentSequence(){
      return(documentSequence);
   }
   
   public String getNextSequence() {
   	  return(nextSequence);
   }

}
