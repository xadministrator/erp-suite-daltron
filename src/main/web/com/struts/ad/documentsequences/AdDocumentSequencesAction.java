package com.struts.ad.documentsequences;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdAPPNoApplicationFoundException;
import com.ejb.exception.AdDSDocumentSequenceAlreadyAssignedException;
import com.ejb.exception.AdDSDocumentSequenceAlreadyDeletedException;
import com.ejb.exception.AdDSDocumentSequenceAlreadyExistException;
import com.ejb.exception.AdDSNoDocumentSequenceFoundException;
import com.ejb.txn.AdDocumentSequenceController;
import com.ejb.txn.AdDocumentSequenceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdApplicationDetails;
import com.util.AdDocumentSequenceDetails;
import com.util.AdModDocumentSequenceDetails;


public final class AdDocumentSequencesAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
   
	
       HttpSession session = request.getSession();
       try{
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          if (user != null) {
             if (log.isInfoEnabled()){
                log.info("AdDocumentSequencesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             }
          }else{
             if (log.isInfoEnabled()){
	        log.info("User is not logged on in session " + session.getId());
             }
	     return(mapping.findForward("adLogon"));
          }
	  String frParam = Common.getUserPermission(user, Constants.AD_DOCUMENT_SEQUENCES_ID);
          if(frParam != null){
  	     if(frParam.trim().equals(Constants.FULL_ACCESS)){
	        ActionErrors fieldErrors = ((AdDocumentSequencesForm)form).validateFields(mapping, request);
		if(!fieldErrors.isEmpty()){
		   saveErrors(request, new ActionMessages(fieldErrors));
		   return(mapping.findForward("adDocumentSequences"));
	        }
	     }
             ((AdDocumentSequencesForm)form).setUserPermission(frParam.trim());
          }else{
	     ((AdDocumentSequencesForm)form).setUserPermission(Constants.NO_ACCESS);
	  }
	  
/*******************************************************
   Initialize AdDocumentSequenceController EJB
*******************************************************/

          AdDocumentSequenceControllerHome homeDS = null;
          AdDocumentSequenceController ejbDS = null;
       
          try{
           
		     homeDS = (AdDocumentSequenceControllerHome)com.util.EJBHomeFactory.
	              lookUpHome("ejb/AdDocumentSequenceControllerEJB", AdDocumentSequenceControllerHome.class);
	              
          }catch(NamingException e){
             if(log.isInfoEnabled()){
	       log.info("NamingException caught in AdDocumentSequencesAction.execute(): " + e.getMessage() + 
	          " session: " + session.getId()); 
	     }
             return(mapping.findForward("cmnErrorPage")); 
          }
       
          try{
              ejbDS = homeDS.create();
          }catch(CreateException e){
             if(log.isInfoEnabled()){
                log.info("CreateException caught in AdDocumentSequencesAction.execute(): " + e.getMessage() + 
	           " session: " + session.getId());
             }
             return(mapping.findForward("cmnErrorPage"));
          }
	  
          ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Ad DS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((AdDocumentSequencesForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adDocumentSequences"));
	     
/*******************************************************
   -- Ad DS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((AdDocumentSequencesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adDocumentSequences"));         
	          	  
/*******************************************************
   -- Ad DS  Save Action --
*******************************************************/

	  } else if(request.getParameter("saveButton") != null &&
	     ((AdDocumentSequencesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
	     AdDocumentSequenceDetails details = new AdDocumentSequenceDetails(
	        ((AdDocumentSequencesForm)form).getDocumentSequenceName().trim().toUpperCase(),
		Common.convertStringToSQLDate(((AdDocumentSequencesForm)form).getEffectiveDateFrom()),
		Common.convertStringToSQLDate(((AdDocumentSequencesForm)form).getEffectiveDateTo()),
		Common.getAdNumberingTypeCode(((AdDocumentSequencesForm)form).getNumberingType()),
		((AdDocumentSequencesForm)form).getInitialValue());
	  
/*******************************************************
   Call AdDocumentSequenceController EJB 
   addAdDsEntry method
*******************************************************/

	     try{
                ejbDS.addAdDsEntry(details, ((AdDocumentSequencesForm)form).getApplication().trim().toUpperCase(), user.getCmpCode());
	     }catch(AdDSDocumentSequenceAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("documentSequences.error.documentSequenceNameAlreadyExists"));
             }catch(AdAPPNoApplicationFoundException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
		   new ActionMessage("documentSequences.error.applicationNotFound"));
             }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in AdDocumentSequencesAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
	        }
                return(mapping.findForward("cmnErrorPage"));
	     }
	     
/*******************************************************
   -- Ad DS Close Action --
*******************************************************/

	  }else if(request.getParameter("closeButton") != null){
	     return(mapping.findForward("cmnMain"));
	      
/*******************************************************
   -- Ad DS Update Action --
*******************************************************/

	  }else if(request.getParameter("updateButton") != null &&
	     ((AdDocumentSequencesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
	     AdDocumentSequencesList adDSList = 
	        ((AdDocumentSequencesForm)form).getAdDSByIndex(((AdDocumentSequencesForm) form).getRowSelected());
	    
	     AdDocumentSequenceDetails details = new AdDocumentSequenceDetails(
	        adDSList.getDocumentSequenceCode(),
		((AdDocumentSequencesForm)form).getDocumentSequenceName().trim().toUpperCase(),
                Common.convertStringToSQLDate(((AdDocumentSequencesForm)form).getEffectiveDateFrom()),
                Common.convertStringToSQLDate(((AdDocumentSequencesForm)form).getEffectiveDateTo()),
                Common.getAdNumberingTypeCode(((AdDocumentSequencesForm)form).getNumberingType()),
                ((AdDocumentSequencesForm)form).getInitialValue());
	     
/*******************************************************
   Call AdDocumentSequenceController EJB updateAdDsEntry
   method
*******************************************************/

	     try {
                ejbDS.updateAdDsEntry(details, ((AdDocumentSequencesForm)form).getApplication().trim().toUpperCase(), user.getCmpCode());
             }catch(AdAPPNoApplicationFoundException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("documentSequences.error.applicationNotFound"));
	     }catch(AdDSDocumentSequenceAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("documentSequences.error.documentSequenceNameAlreadyExists"));
	     }catch(AdDSDocumentSequenceAlreadyAssignedException  ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
		   new ActionMessage("documentSequences.error.updateDocumentSequenceAlreadyAssigned"));
	     }catch(AdDSDocumentSequenceAlreadyDeletedException ex){
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("documentSequences.error.documentSequenceAlreadyDeleted"));
	     }catch(EJBException ex){
	        if(log.isInfoEnabled()){
		   log.info("EJBException caught in AdDocumentSequencesAction.execute(): " + ex.getMessage() +
	              " session: " + session.getId());
	        }
	        return(mapping.findForward("cmnErrorPage"));
	     }

/*******************************************************
   -- Ad DS Cancel Action --
*******************************************************/

	  }else if(request.getParameter("cancelButton") != null){
	       
/*******************************************************
   -- Ad DS Edit Action --
*******************************************************/

	  }else if(request.getParameter("adDSList[" + 
             ((AdDocumentSequencesForm) form).getRowSelected() + "].editButton") != null &&
	     ((AdDocumentSequencesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate AdDocumentSequencesForm properties
*******************************************************/

	        ((AdDocumentSequencesForm) form).showAdDSRow(((AdDocumentSequencesForm) form).getRowSelected());
		return(mapping.findForward("adDocumentSequences"));
		     
/*******************************************************
  -- Ad DS Delete Action --
*******************************************************/

	   }else if(request.getParameter("adDSList[" + 
	      ((AdDocumentSequencesForm) form).getRowSelected() + "].deleteButton") != null &&
	      ((AdDocumentSequencesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	      
	      AdDocumentSequencesList adDSList =
                 ((AdDocumentSequencesForm)form).getAdDSByIndex(((AdDocumentSequencesForm) form).getRowSelected());

/*******************************************************
   Call AdDocumentSequenceController EJB 
   deleteAdDsEntry method
*******************************************************/

              try{
	         ejbDS.deleteAdDsEntry(adDSList.getDocumentSequenceCode(), user.getCmpCode());
	      }catch(AdDSDocumentSequenceAlreadyAssignedException  ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
		    new ActionMessage("documentSequences.error.deleteDocumentSequenceAlreadyAssigned"));
	      }catch(AdDSDocumentSequenceAlreadyDeletedException  ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
		    new ActionMessage("documentSequences.error.documentSequenceAlreadyDeleted"));
	      }catch(EJBException ex){
                 if(log.isInfoEnabled()){
                    log.info("EJBException caught in AdDocumentSequencesAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	         }
	         return(mapping.findForward("cmnErrorPage"));
	      }

/*******************************************************
   -- Ad DS Load Action --
*******************************************************/

	   }
	   if(frParam != null){
   	      if (!errors.isEmpty()) {
	         saveErrors(request, new ActionMessages(errors));
		 return(mapping.findForward("adDocumentSequences"));
	      }	     
	      		      			    
  	      ((AdDocumentSequencesForm)form).clearAdDSList();
  	      ArrayList ejbGlACList = null;
		       
/*******************************************************
   Call AdDocumentSequenceController EJB getAdAppAll
   Populate applicationList from method ArrayList
   return
*******************************************************/

              ((AdDocumentSequencesForm)form).clearAppList();
              Iterator i = null;
	      try{
	         ArrayList ejbAdAPList = ejbDS.getAdAppAll(user.getCmpCode());
	         i = ejbAdAPList.iterator();
	         while(i.hasNext()){
	            ((AdDocumentSequencesForm)form).setAppList(
	            ((AdApplicationDetails)i.next()).getAppName());
	         }
	      }catch(AdAPPNoApplicationFoundException ex){
	         ((AdDocumentSequencesForm)form).setAppList(Constants.GLOBAL_NO_RECORD_FOUND);
	      }
		    
/*******************************************************
   Call AdDocumentSequenceController EJB
   getAdDsAll method
   Populate ArrayList property from 
   ArrayList return
*******************************************************/
       
	      try{
	         ArrayList ejbAdDSList = ejbDS.getAdDsAll(user.getCmpCode());
		 i = ejbAdDSList.iterator();
                 while(i.hasNext()){
		    AdModDocumentSequenceDetails mdetails = (AdModDocumentSequenceDetails)i.next();
                    AdDocumentSequencesList adDSList = new AdDocumentSequencesList(((AdDocumentSequencesForm)form),
                       mdetails.getDsCode(),
                       mdetails.getDsSequenceName(),
                       mdetails.getDsAppName(),
                       Common.convertSQLDateToString(mdetails.getDsDateFrom()),
		       Common.convertSQLDateToString(mdetails.getDsDateTo()),
		       Common.getAdNumberingTypeDescription(mdetails.getDsNumberingType()),
		       mdetails.getDsInitialValue());
                      ((AdDocumentSequencesForm)form).saveAdDSList(adDSList);
                 }
              }catch(AdDSNoDocumentSequenceFoundException ex){ 
	         
	      }catch(EJBException ex){
	         if(log.isInfoEnabled()){
	            log.info("EJBException caught in AdDocumentSequencesAction.execute(): " + ex.getMessage() + 
	              " session: " + session.getId());
	          }
	          return(mapping.findForward("cmnErrorPage"));
	      }
	
	      if(!errors.isEmpty()){
	         saveErrors(request, new ActionMessages(errors));
	      }else{
	         if((request.getParameter("saveButton") != null || request.getParameter("updateButton") != null ||
		    request.getParameter("adDSList[" +
		    ((AdDocumentSequencesForm) form).getRowSelected() + "].deleteButton") != null) &&
		    ((AdDocumentSequencesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

		    ((AdDocumentSequencesForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
		 }
	      }
	      
	      ((AdDocumentSequencesForm)form).reset(mapping, request);
	      	      	      	      
	        if (((AdDocumentSequencesForm)form).getTableType() == null) {
      		      	
	           ((AdDocumentSequencesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
                                    	          
	      ((AdDocumentSequencesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
              return(mapping.findForward("adDocumentSequences"));
	   }else{
	      errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
              saveErrors(request, new ActionMessages(errors));
               		   
	      return(mapping.findForward("cmnMain"));
	   }
      }catch(Exception e){

/*******************************************************
   Failed: Forward to error page
********************************************************/

         if(log.isInfoEnabled()){
	    log.info("Exception caught in AdDocumentSequencesAction.execute(): " + e.getMessage() + " session: " 
	       + session.getId());
	 }
 	return(mapping.findForward("cmnErrorPage")); 
      }
    }
}
