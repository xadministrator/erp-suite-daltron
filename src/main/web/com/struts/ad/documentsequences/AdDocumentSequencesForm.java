package com.struts.ad.documentsequences;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class AdDocumentSequencesForm extends ActionForm implements Serializable{

	private String documentSequenceName = null;	
	private String application = null;
	private ArrayList appList = new ArrayList();
	private String effectiveDateFrom = null;
	private String effectiveDateTo = null;
	private String numberingType = null;
	private ArrayList numberingTypeList = new ArrayList();
	private String initialValue = null;
    private String tableType = null;

    private String showDetailsButton = null;
    private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList adDSList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String(); 
	
	public int getRowSelected(){
	   return rowSelected;
	}
	
        public AdDocumentSequencesList getAdDSByIndex(int index){
	   return((AdDocumentSequencesList)adDSList.get(index));
	}

	public Object[] getAdDSList(){
	    return(adDSList.toArray());
	}

	public int getAdDSListSize(){
	   return(adDSList.size());
	}

	public void saveAdDSList(Object newAdDSList){
	   adDSList.add(newAdDSList);
	}

	public void clearAdDSList(){
	   adDSList.clear();
	}
	
	public void setRowSelected(Object selectedAdDSList, boolean isEdit){
	   this.rowSelected = adDSList.indexOf(selectedAdDSList);
	   if(isEdit){
	      this.pageState = Constants.PAGE_STATE_EDIT;
	   }
	}
	
	public void showAdDSRow(int rowSelected){
	   this.documentSequenceName = ((AdDocumentSequencesList)adDSList.get(rowSelected)).getDocumentSequenceName();
	   this.application = ((AdDocumentSequencesList)adDSList.get(rowSelected)).getApplication();
	   this.effectiveDateFrom = ((AdDocumentSequencesList)adDSList.get(rowSelected)).getEffectiveDateFrom();
	   this.effectiveDateTo =  ((AdDocumentSequencesList)adDSList.get(rowSelected)).getEffectiveDateTo();
	   this.numberingType =  ((AdDocumentSequencesList)adDSList.get(rowSelected)).getNumberingType();
	   this.initialValue =  ((AdDocumentSequencesList)adDSList.get(rowSelected)).getInitialValue();
	}

	public void updateAdDSRow(int rowSelected, Object newAdDSList){
	   adDSList.set(rowSelected, newAdDSList);
	}

	public void deleteAdDSList(int rowSelected){
	   adDSList.remove(rowSelected);
	}
	
	public void setUpdateButton(String updateButton){
	   this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton){
	   this.cancelButton = cancelButton;
	}
	
	public void setSaveButton(String saveButton){
	   this.saveButton = saveButton;
	}

	public void setCloseButton(String closeButton){
	   this.closeButton = closeButton;
	}
				
    public void setShowDetailsButton(String showDetailsButton) {
		
	   this.showDetailsButton = showDetailsButton;
		
    }
    
    public void setHideDetailsButton(String hideDetailsButton) {
    	
       this.hideDetailsButton = hideDetailsButton;
    	
    }
								
	public void setPageState(String pageState){
	   this.pageState = pageState;
	}

	public String getPageState(){
	    return(pageState);
	}

        public String getTxnStatus(){
	   String passTxnStatus = txnStatus;
	   txnStatus = Constants.GLOBAL_BLANK;
	   return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
	   this.txnStatus = txnStatus;
	}

        public String getDocumentSequenceName(){
	   return(documentSequenceName);
	}

	public void setDocumentSequenceName(String documentSequenceName){
	   this.documentSequenceName = documentSequenceName;
	}

	public String getApplication(){
	  return(application);
	}

	public void setApplication(String application){
	   this.application = application;
	}

	public ArrayList getAppList(){
	   return(appList);
	}

	public void setAppList(String application){
	   appList.add(application);
	}

	public void clearAppList(){
	   appList.clear();
	   appList.add(Constants.GLOBAL_BLANK);
	}

	public String getEffectiveDateFrom(){
	   return(effectiveDateFrom);
	}

	public void setEffectiveDateFrom(String effectiveDateFrom){
	   this.effectiveDateFrom = effectiveDateFrom;
	}

	public String getEffectiveDateTo(){
	   return(effectiveDateTo);
	}

	public void setEffectiveDateTo(String effectiveDateTo){
	   this.effectiveDateTo = effectiveDateTo;
	}

	public String getNumberingType(){
	   return(numberingType);
	}

	public void setNumberingType(String numberingType){
	   this.numberingType = numberingType;
	}

	public ArrayList getNumberingTypeList(){
	   return(numberingTypeList);
	}

	public String getInitialValue(){
	   return(initialValue);
	}

	public void setInitialValue(String initialValue){
	   this.initialValue = initialValue;
	}
				
    public String getTableType() {
    	
       return(tableType);
    	
    }
    
    public void setTableType(String tableType) {
    	
       this.tableType = tableType;
    	
    }
                    	
    public String getUserPermission(){
	   return(userPermission);
	}

	public void setUserPermission(String userPermission){
	   this.userPermission = userPermission;
	}

        public void reset(ActionMapping mapping, HttpServletRequest request){
	   documentSequenceName = null;
	   application = Constants.GLOBAL_BLANK;
	   effectiveDateFrom = null;
	   effectiveDateTo = null;
	   numberingTypeList.clear();
	   numberingTypeList.add(Constants.AD_NUMBERING_TYPE_AUTO);
	   numberingTypeList.add(Constants.AD_NUMBERING_TYPE_MANUAL);
	   numberingType = Constants.AD_NUMBERING_TYPE_AUTO;
	   initialValue = "0";
	   saveButton = null;
	   updateButton = null;
	   closeButton = null;
	   updateButton = null;
	   cancelButton = null;	   
	   showDetailsButton = null;
	   hideDetailsButton = null;
	  
        }

        public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
           ActionErrors errors = new ActionErrors();
           if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
             if(Common.validateRequired(documentSequenceName)) {
                errors.add("documentSequenceName", 
		   new ActionMessage("documentSequences.error.documentSequenceNameRequired"));
             }
	     if(Common.validateRequired(application)){
	        errors.add("application", new ActionMessage("documentSequences.error.applicationRequired"));
	     }
	     if(!Common.validateStringExists(appList, application)){
	        errors.add("application", new ActionMessage("documentSequences.error.applicationInvalid"));
	     }
	     if(application.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
	        errors.add("application", new ActionMessage("documentSequences.error.applicationRequired"));
	     }
             if(Common.validateRequired(effectiveDateFrom)) {
                errors.add("effectiveDateFrom", new ActionMessage("documentSequences.error.effectiveDateFromRequired"));
             }
             if(!Common.validateDateFormat(effectiveDateFrom)){
                errors.add("effectiveDateFrom", new ActionMessage("documentSequences.error.effectiveDateFromInvalid"));
             }
             if(!Common.validateDateFormat(effectiveDateTo)){
                errors.add("effectiveDateTo", new ActionMessage("documentSequences.error.effectiveDateToInvalid"));
             }
             if(!Common.validateDateFromTo(effectiveDateFrom, effectiveDateTo)){
                errors.add("effectiveDateFrom", new ActionMessage("documentSequences.error.effectiveDateFromToInvalid"));
             }
             if(!Common.validateDateGreaterThanCurrent(effectiveDateTo)){
                errors.add("effectiveDateTo", new ActionMessage("documentSequences.error.effectiveDateToLessThanCurrent"));
             }
	     if(Common.validateRequired(numberingType)){
                errors.add("numberingType", new ActionMessage("documentSequences.error.numberingTypeRequired"));
             }
             if(!Common.validateStringExists(numberingTypeList, numberingType)){
                errors.add("numberingType", new ActionMessage("documentSequences.error.numberingTypeInvalid"));
             }
             if(Common.validateRequired(initialValue)){
                errors.add("initialValue", new ActionMessage("documentSequences.error.initialValueRequired"));
             }             
	   }
	  return(errors);
	}
		
}
