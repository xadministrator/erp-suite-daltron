package com.struts.ad.documentsequences;

import java.io.Serializable;

public class AdDocumentSequencesList implements Serializable{

	private Integer documentSequenceCode = null;
	private String documentSequenceName = null;	
	private String application = null;
	private String effectiveDateFrom = null;
	private String effectiveDateTo = null;
	private String numberingType = null;
	private String initialValue = null;
	
	private String deleteButton = null;
	private String editButton = null;
	
	private AdDocumentSequencesForm parentBean;

	public AdDocumentSequencesList(AdDocumentSequencesForm parentBean, Integer documentSequenceCode, 
	      String documentSequenceName, String application, String effectiveDateFrom, 
	      String effectiveDateTo, String numberingType, String initialValue){
	   this.parentBean = parentBean;
	   this.documentSequenceCode = documentSequenceCode;
	   this.documentSequenceName = documentSequenceName;
	   this.application = application;
	   this.effectiveDateFrom = effectiveDateFrom;	   
	   this.effectiveDateTo = effectiveDateTo;
	   this.numberingType = numberingType;
	   this.initialValue = initialValue;
	}

	public void setParentBean(AdDocumentSequencesForm parentBean){
	   this.parentBean = parentBean;
	}

	public void setDeleteButton(String deleteButton){
	   parentBean.setRowSelected(this, false);
	}

	public void setEditButton(String editButton){
	   parentBean.setRowSelected(this, true);
	}

	public Integer getDocumentSequenceCode(){
	   return(documentSequenceCode);
	}

        public String getDocumentSequenceName(){
	   return(documentSequenceName);
	}

	public String getApplication(){
	   return(application);
	}

	public String getEffectiveDateFrom(){
	   return(effectiveDateFrom);
	}

	public String getEffectiveDateTo(){
	   return(effectiveDateTo);
	}

	public String getNumberingType(){
	   return(numberingType);
	}

	public String getInitialValue(){
	   return(initialValue);
	}
}
