package com.struts.ad.formfunctionresponsibility;

import java.util.ArrayList;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.AdFormFunctionResponsibilityController;
import com.ejb.txn.AdFormFunctionResponsibilityControllerHome;
import com.struts.ad.formfunctionresponsibility.AdFormFunctionResponsibilityArList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModFormFunctionResponsibilityDetails;
import com.util.AdResponsibilityDetails;

public final class AdFormFunctionResponsibilityAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdFormFunctionResponsibilityAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         AdFormFunctionResponsibilityForm actionForm = (AdFormFunctionResponsibilityForm)form;

         String frParam = Common.getUserPermission(user, Constants.AD_FORM_FUNCTION_RESPONSIBILITY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adFormFunctionResponsibility");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdFormFunctionResponsibilityController EJB
*******************************************************/

         AdFormFunctionResponsibilityControllerHome homeFR = null;
         AdFormFunctionResponsibilityController ejbFR = null;

         try {

            homeFR = (AdFormFunctionResponsibilityControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdFormFunctionResponsibilityControllerEJB", AdFormFunctionResponsibilityControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdFormFunctionResponsibilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFR = homeFR.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdFormFunctionResponsibilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

 /*******************************************************
     -- Ad FR Save Action --
  *******************************************************/

         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArrayList list = new ArrayList();

            for (int i=0; i<actionForm.getAdFRGlListSize(); i++) {

                AdFormFunctionResponsibilityGlList frList = actionForm.getAdFRGlByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }

            for (int i=0; i<actionForm.getAdFRApListSize(); i++) {

                AdFormFunctionResponsibilityApList frList = actionForm.getAdFRApByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }

            for (int i=0; i<actionForm.getAdFRArListSize(); i++) {

                AdFormFunctionResponsibilityArList frList = actionForm.getAdFRArByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }

            for (int i=0; i<actionForm.getAdFRCmListSize(); i++) {

                AdFormFunctionResponsibilityCmList frList = actionForm.getAdFRCmByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }

            for (int i=0; i<actionForm.getAdFRAdListSize(); i++) {

                AdFormFunctionResponsibilityAdList frList = actionForm.getAdFRAdByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }

            for (int i=0; i<actionForm.getAdFRInvListSize(); i++) {

                AdFormFunctionResponsibilityInvList frList = actionForm.getAdFRInvByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }


            for (int i=0; i<actionForm.getAdFRHrListSize(); i++) {

                AdFormFunctionResponsibilityHrList frList = actionForm.getAdFRHrByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }


            for (int i=0; i<actionForm.getAdFRPmListSize(); i++) {

                AdFormFunctionResponsibilityPmList frList = actionForm.getAdFRPmByIndex(i);

                if (frList.getSelect() == false) continue;

	           	   AdModFormFunctionResponsibilityDetails mdetails = new AdModFormFunctionResponsibilityDetails();

	           	   mdetails.setFrParameter(frList.getParameter());
	           	   mdetails.setFrFfCode(frList.getFormFunctionCode());

	           	   list.add(mdetails);
            }

            try {

            	ejbFR.saveAdFrEntry(list, actionForm.getResponsibilityCode(), user.getCmpCode());

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdFormFunctionResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

/*******************************************************
   -- Ad FR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad FR Load Action --
*******************************************************/

         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adFormFunctionResponsibility");

            }

            AdResponsibilityDetails rDetails = null;

	        if (request.getParameter("forward") != null) {

	            rDetails = ejbFR.getAdRsByRsCode(new Integer(request.getParameter("responsibilityCode")), user.getCmpCode());
	    	    actionForm.setResponsibilityCode(new Integer(request.getParameter("responsibilityCode")));

	        } else {

	            rDetails = ejbFR.getAdRsByRsCode(actionForm.getResponsibilityCode(), user.getCmpCode());

	        }

    	    actionForm.setResponsibilityCode(actionForm.getResponsibilityCode());
            actionForm.setResponsibilityName(rDetails.getRsName());
            actionForm.setDescription(rDetails.getRsDescription());

			actionForm.clearAdFRGlList();
			actionForm.clearAdFRApList();
			actionForm.clearAdFRArList();
			actionForm.clearAdFRCmList();
			actionForm.clearAdFRAdList();
			actionForm.clearAdFRInvList();
			actionForm.clearAdFRHrList();
			actionForm.clearAdFRPmList();

			// create form function responsibility

			AdFormFunctionResponsibilityGlList adFRGlList = null;
			AdFormFunctionResponsibilityApList adFRApList = null;
			AdFormFunctionResponsibilityArList adFRArList = null;
			AdFormFunctionResponsibilityCmList adFRCmList = null;
			AdFormFunctionResponsibilityAdList adFRAdList = null;
			AdFormFunctionResponsibilityInvList adFRInvList = null;
			AdFormFunctionResponsibilityHrList adFRHrList = null;
			AdFormFunctionResponsibilityPmList adFRPmList = null;

			ArrayList parameterList = new ArrayList();
			parameterList.add("F");
			parameterList.add("Q");

			AdModFormFunctionResponsibilityDetails details = null;

			try {

				//General Ledger

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(13), "Journal Batch Entry", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(13), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(14), "Find Journal Batch", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(14), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(1), "Journal Entry", "F", false);
			    adFRGlList.setParameterList(parameterList);
			    actionForm.saveAdFRGlList(adFRGlList);
			    details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(2), "Find Journal", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(11), "Journal Batch Print", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(11), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(507), "Journal Print", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(507), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(511), "Journal Edit List", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(511), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(12), "Journal Batch Submit",  "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(12), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }


				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(15), "Journal Batch Copy", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(15), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(6), "Recurring Journal Entry", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(6), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(8), "Find Recurring Journal", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(8), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(9), "Recurring Journal Generate", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(9), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(5), "Journal Reversal", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(4), "Journal Import", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(76), "FOREX Revaluation", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(76), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(517), "FOREX Revaluation Print", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(517), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(10), "Approval", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(10), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(3), "Journal Post", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(37), "Open/Close Periods", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(37), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(7), "Year-end Closing", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(67), "Budget Entry", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(67), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(68), "Find Budget", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(68), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(64), "Budget Definition", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(64), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(65), "Budget Organization", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(65), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(66), "Budget Account Assignment", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(66), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }


				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm,new Integer(22), "Chart Of Accounts", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(22), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(60), "Find Chart Of Accounts", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(60), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(75), "Find Segment", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(75), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm,new Integer(70), "COA Generator", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(70), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(1018), "Segment Values", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1018), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(42), "Currencies", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(42), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(43), "Daily Rates", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(43), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(61), "Find Daily Rates", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(61), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(23), "Period Types", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(23), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(24), "Accounting Calendar", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(24), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(25), "Accounting Calendar Lines", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(25), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(27), "Transaction Calendar Lines", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(27), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(33), "Journal Sources", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(33), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(34), "Journal Categories", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(34), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(35), "Suspense Accounts", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(35), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }


				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(45), "Organization Entry", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(45), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(46), "Organization Account Assignment", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(46), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(47), "Organization Responsibility Assignment", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(47), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(71), "Tax Interface Run", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(71), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(72), "Tax Interface Maintenance", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(72), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(53), "Financial Report Entry", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(53), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(54), "Row Set", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(54), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(56), "Rows", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(56), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(55), "Column Set", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(55), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(57), "Columns", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(57), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(58), "Account Assignment", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(58), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(59), "Calculation", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(59), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(69), "Journal Interface Upload", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(69), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(62), "Find Journal Interface", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(62), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(63), "Journal Interface Maintenance", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(63), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(73), "Static Report", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(73), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(74), "User Static Report", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(74), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(505), "Financial Report Run", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(505), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(501), "Detail Trial Balance", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(501), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(502), "Detail Income Statement", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(502), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(503), "Detail Balance Sheet", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(503), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(504), "General Ledger", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(504), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(519), "Investor Ledger", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(519), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }


			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(516), "Journal Register", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(516), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(506), "Chart Of Account List", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(506), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

			    adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(513), "Budget", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(513), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(508), "Income Tax Withheld", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(508), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(509), "Monthly Vat Declaration", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(509), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(510), "Quarterly Vat Return", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(510), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(512), "Csv Quarterly Vat Return", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(512), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(515), "Static Report", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(515), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				adFRGlList = new AdFormFunctionResponsibilityGlList(actionForm, new Integer(518), "Daily Rate List", "F", false);
				adFRGlList.setParameterList(parameterList);
				actionForm.saveAdFRGlList(adFRGlList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(518), user.getCmpCode());
			    if (details != null) { adFRGlList.setSelect(true); adFRGlList.setParameter(details.getFrParameter()); }

				// Administration

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1027), "Branches", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1027), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1016), "Responsibilities", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1016), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1017), "Form Function Responsibilities", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1017), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1014), "User", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1014), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1015), "User Responsibility", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1015), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1019), "Change Password", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1019), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1026), "User Sessions", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1026), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1013), "Global Preference", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1013), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1020), "Company", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1020), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1021), "Approval Setup", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1021), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1022), "Approval Document", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1022), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1023), "Amount Limit", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1023), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1024), "Approval Coa Line", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1024), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1025), "Approval User", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1025), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1002), "Document Sequences", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1002), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1003), "Document Sequences Assignment", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1003), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1007), "Document Categories", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1007), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1006), "Look Up", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1006), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1501), "Transaction Log", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1501), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1504), "Transaction Summary", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1504), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1010), "Payment Terms", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1010), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1011), "Payment Schedules", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1011), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

				adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1012), "Discounts", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1012), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1502), "Bank Account List", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1502), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1503), "Value Set Value List", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1503), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1505), "Responsibility List", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1505), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1506), "User List", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1506), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1507), "Approval List", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1507), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }

			    adFRAdList = new AdFormFunctionResponsibilityAdList(actionForm, new Integer(1508), "Stored Procedure Setup", "F", false);
				adFRAdList.setParameterList(parameterList);
				actionForm.saveAdFRAdList(adFRAdList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1508), user.getCmpCode());
			    if (details != null) { adFRAdList.setSelect(true); adFRAdList.setParameter(details.getFrParameter()); }





			    // Account Receivables

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2311), "Invoice Batch Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2311), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2312), "Find Invoice Batch", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2312), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2003), "Invoice Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2003), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2005), "Credit Memo Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2005), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2004), "Find Invoice", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2004), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2307), "Invoice Batch Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2307), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2501), "Invoice Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2501), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2522), "Delivery Receipt Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2522), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2524), "Transmittal Document Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2524), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2502), "Credit Memo Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2502), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2511), "Invoice Edit List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2511), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2309), "Invoice Batch Submit", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2309), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2315), "Invoice Batch Copy", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2315), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2019), "Invoice Import", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2019), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2017), "Sales Order Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2017), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2018), "Find Sales Order", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2018), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2521), "Sales Order Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2521), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2313), "Receipt Batch Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2313), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2314), "Find Receipt Batch", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2314), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2008), "Receipt Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2008), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2007), "Misc Receipt Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2007), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2006), "Find Receipt", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2006), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			   
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2021), "Job Order Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2021), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2022), "Find Job Order", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2022), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2023), "Job Order Assignment", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2023), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2024), "Find Personel", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2024), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2308), "Receipt Batch Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2308), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2503), "Receipt Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2503), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2512), "Receipt Edit List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2512), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2310), "Receipt Batch Submit", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2310), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2316), "Receipt Batch Copy", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2316), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2020), "Receipt Import", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2020), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2012), "Journal", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2012), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2013), "Approval", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2013), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2014), "Pdc Entry", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2014), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2015), "Find Pdc", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2015), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2016), "Pdc Invoice Generation", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2016), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2527), "Pdc Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2527), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2009), "Invoice Post", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2009), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2010), "Receipt Post", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2010), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2011), "GL Journal Interface Run", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2011), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2001), "Customer Entry", "F", false);
				 ArrayList specialParameterList = new ArrayList();
				    specialParameterList.add("F");
				    specialParameterList.add("Q");
				    specialParameterList.add("S");
				adFRArList.setParameterList(specialParameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2001), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2002), "Find Customer", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2002), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2301), "Tax Codes", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2301), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2302), "Withholding Tax Codes", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2302), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2305), "Customer Classes", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2305), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2303), "Customer Types", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2303), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2306), "Standard Memo Lines", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2306), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2317), "Salesperson", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2317), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2318), "Personel", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2318), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    
			    
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2319), "Job Order Type", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2319), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    
			    
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2320), "Personel Type", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2320), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    
			    
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2321), "Delivery", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2321), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    
			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2322), "Find Delivery", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2322), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
			    

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2504), "Statement of Accounts", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2504), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2505), "Aging Detail", "F", false);
	            adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2505), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2520), "Aging Summary", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2520), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2506), "Sales Register", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2506), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2507), "OR Register", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2507), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2509), "Output Tax", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2509), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2510), "Creditable Withholding Tax", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2510), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2508), "Customer List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2508), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2513), "Sales", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2513), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2514), "Tax Code List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2514), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2515), "Withholding Tax Code List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2515), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2516), "Customer Class List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2516), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2517), "Customer Type List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2517), user.getCmpCode());
			    if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

			    adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2518), "Standard Memo Line List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2518), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2519), "Post Dated Check List", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2519), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2523), "Salesperson Report", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2523), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2525), "Delivery Receipt Report", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2525), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2526), "Sales Order Report", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2526), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
				
				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2528), "Job Order Print", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2528), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
				
				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2529), "Job Status Report", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2529), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }
				
				adFRArList = new AdFormFunctionResponsibilityArList(actionForm, new Integer(2530), "Personel Summary", "F", false);
				adFRArList.setParameterList(parameterList);
				actionForm.saveAdFRArList(adFRArList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(2530), user.getCmpCode());
				if (details != null) { adFRArList.setSelect(true); adFRArList.setParameter(details.getFrParameter()); }

				// Account Payables

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3021), "Voucher Batch Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3021), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3023), "Find Voucher Batch", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3023), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3003), "Voucher Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3003), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3005), "Debit Memo Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3005), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3004), "Find Voucher", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3004), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3017), "Voucher Batch Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3017), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3501), "Voucher Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3501), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3502), "Debit Memo Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3502), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3511), "Voucher Edit List", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3511), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3019), "Voucher Batch Submit", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3019), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3025), "Voucher Batch Copy", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3025), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3030), "Voucher Import", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3030), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3027), "Purchase Order Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3027), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3029), "Receiving Item Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3029), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3028), "Find Purchase Order", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3028), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3517), "Purchase Order Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3517), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3033), "Purchase Requisition Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3033), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3034), "Find Purchase Requisition", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3034), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3032), "Generate Purchase Requisition", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3032), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3522), "Purchase Requisition Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3522), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3523), "Canvass Report Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3523), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3525), "Annual Procurement Plan", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3525), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3518), "Receiving Report Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3518), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3006), "Recurring Voucher Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3006), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3008), "Find Recurring Voucher", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3008), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3007), "Recurring Voucher Generate", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3007), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3022), "Check Batch Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3022), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3024), "Find Check Batch", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3024), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3035), "Check Payment Request Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3035), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3037), "Annual Procurement Plan Upload", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3036), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3036), "Canvass", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3036), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3009), "Payment Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3009), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3010), "Direct Check Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3010), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3011), "Find Check", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3011), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3018), "Check Batch Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3018), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3503), "Check Voucher Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3503), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3504), "Check Print", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3504), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3512), "Check Edit List", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3512), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3020), "Check Batch Submit", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3020), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3026), "Check Batch Copy", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3026), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3031), "Check Import", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3031), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3015), "Journal", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3015), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3016), "Approval", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3016), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3012), "Voucher Post", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3012), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3013), "Check Post", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3013), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3014), "GL Journal Interface Run", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3014), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3001), "Supplier Entry", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3001), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3002), "Find Supplier", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3002), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3301), "Tax Codes", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3301), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3304), "Withholding Tax Codes", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3304), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3302), "Supplier Classes", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3302), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3303), "Supplier Types", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3303), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3507), "Aging Detail", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3507), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3519), "Aging Summary", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3519), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3505), "AP Register", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3505), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3506), "Check Register", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3506), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3508), "Input Tax", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3508), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3509), "Withholding Tax Expanded", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3509), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

				adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3510), "Supplier List", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3510), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3513), "Tax Code List", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3513), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3514), "Withholding Tax Code List", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3514), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3515), "Supplier Class List", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3515), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3521), "Purchase Order", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3521), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3520), "Receiving Item", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3520), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }

			    adFRApList = new AdFormFunctionResponsibilityApList(actionForm, new Integer(3524), "Purchase Requisition Register", "F", false);
				adFRApList.setParameterList(parameterList);
				actionForm.saveAdFRApList(adFRApList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(3524), user.getCmpCode());
			    if (details != null) { adFRApList.setSelect(true); adFRApList.setParameter(details.getFrParameter()); }



			    // Cash Management

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4002), "Adjustment Entry", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4002), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4003), "Find Adjustment", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4003), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4011), "Adjustment Batch Submit", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4011), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4004), "Fund Transfer Entry", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4004), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4005), "Find Fund Transfer", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4005), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4012), "Fund Transfer Batch Submit", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4012), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4001), "Bank Reconciliation", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4001), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4007), "Journal", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4007), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4008), "Approval", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4008), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4009), "Adjustment Post", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4009), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4010), "Fund Transfer Post", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4010), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4013), "Releasing Checks", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4013), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4006), "GL Journal Interface Run", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4006), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(1004), "Banks", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1004), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(1005), "Bank Accounts", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(1005), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4509), "Bank Reconciliation", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4509), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4501), "Deposit List", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4501), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4502), "Released Checks List", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4502), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4503), "Daily Cash Position Detail", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4503), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4504), "Daily Cash Position Summary", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4504), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4505), "Daily Cash Forecast Detail", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4505), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4506), "Daily Cash Forecast Summary", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4506), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4507), "W-Cash Forecast Detail", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4507), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4508), "W-Cash Forecast Summary", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4508), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

				adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4510), "Cash Position", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4510), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }

			    adFRCmList = new AdFormFunctionResponsibilityCmList(actionForm, new Integer(4511), "Adjustment Print", "F", false);
				adFRCmList.setParameterList(parameterList);
				actionForm.saveAdFRCmList(adFRCmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(4511), user.getCmpCode());
			    if (details != null) { adFRCmList.setSelect(true); adFRCmList.setParameter(details.getFrParameter()); }


	            // Inventory

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5001), "Adjustment Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5001), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5026), "Adjustment Request", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5026), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5002), "Find Adjustment", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5002), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5518), "Adjustment Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5518), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5526), "Adjustment Request Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5526), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5003), "Build Unbuild Assembly Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5003), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5527), "Find Build Unbuild Assembly Batch", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5527), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5528), "Build Unbuild Assembly Batch Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5528), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5529), "Build Unbuild Assembly Order Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5529), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5004), "Find Build Unbuild Assembly", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5004), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5525), "Build Unbuild Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5525), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5005), "Physical Inventory Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5005), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5006), "Find Physical Inventory", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5006), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5011), "Overhead Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5011), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5012), "Find Overhead", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5012), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5013), "Build Order Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5013), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5014), "Find Build Order", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5014), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5508), "Build Order Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5508), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5015), "Stock Issuance Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5015), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5016), "Find Stock Issuance", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5016), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5509), "Stock Issuance Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5509), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5017), "Assembly Transfer Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5017), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5018), "Find Assembly Transfer", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5018), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5510), "Assembly Transfer Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5510), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5019), "Find Build Order Line", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5019), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5020), "Stock Transfer Entry", "F", false);
			    adFRInvList.setParameterList(parameterList);
			    actionForm.saveAdFRInvList(adFRInvList);
			    details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5020), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5021), "Find Stock Transfer", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5021), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5514), "Stock Transfer Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5514), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5025), "Branch Stock Transfer Order", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5025), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5027), "Transactional Budget", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5027), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5022), "Branch Stock Transfer-Out", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5022), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5023), "Branch Stock Transfer-In", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5023), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5024), "Find Branch Stock Transfer", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5024), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5515), "Branch Stock Transfer Out Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5515), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5516), "Branch Stock Transfer In Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5516), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5007), "Approval", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5007), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5008), "Adjustment Post", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5008), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5309), "Build/Unbuild Assembly Post", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5309), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5010), "Journal", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5010), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5009), "GL Journal Interface Run", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5009), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5301), "Item Entry", "F", false);
				adFRInvList.setParameterList(specialParameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5301), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5310), "Assembly Item Entry", "F", false);

			    /*
			     * Special Added Parameter for Assembly Item Entry
			     *
			     */
			    /*ArrayList specialParameterList = new ArrayList();
			    specialParameterList.add("F");
			    specialParameterList.add("Q");
			    specialParameterList.add("S");*/

			    adFRInvList.setParameterList(specialParameterList);
			    actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5310), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5302), "Find Item", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5302), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5311), "Unit Of Measure Conversion Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5311), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5312), "Price Levels", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5312), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5303), "Location Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5303), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5304), "Find Location", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5304), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5305), "Item Location Entry", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5305), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5306), "Find Item Location", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5306), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5307), "Unit Of Measures", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5307), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5308), "Overhead Memo Lines", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5308), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5313), "Item Templates", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5313), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5501), "Usage Variance Report", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5501), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5505), "Count Variance Report", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5505), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5507), "Inventory Profitability", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5507), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5502), "Item Costing Report", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5502), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5506), "Cost Of Sales", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5506), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5522), "Adjustment Register", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5522), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5523), "Branch Stock Transfer Register", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5523), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5511), "Stock Transfer", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5511), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5513), "Reorder Items", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5513), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5512), "Stock On Hand", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5512), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5521), "Inventory List", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5521), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5524), "Inventory Markup List", "F", false);
				adFRInvList.setParameterList(specialParameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5524), user.getCmpCode());

				if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5503), "Item List", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5503), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

				adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5504), "Assembly List", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5504), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5517), "Physical Inventory Worksheet", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5517), user.getCmpCode());
			    if (details != null) {adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }




			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5519), "Stock Card", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5519), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5520), "Item Ledger", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5520), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5530), "Build Unbuild Assembly Order", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5530), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }

			    adFRInvList = new AdFormFunctionResponsibilityInvList(actionForm, new Integer(5531), "Physical Inventory Print", "F", false);
				adFRInvList.setParameterList(parameterList);
				actionForm.saveAdFRInvList(adFRInvList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(5531), user.getCmpCode());
			    if (details != null) { adFRInvList.setSelect(true); adFRInvList.setParameter(details.getFrParameter()); }



			    // Human Resource

			    adFRHrList = new AdFormFunctionResponsibilityHrList(actionForm, new Integer(6000), "Synchronizer", "F", false);
				adFRHrList.setParameterList(parameterList);
				actionForm.saveAdFRHrList(adFRHrList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(6000), user.getCmpCode());
			    if (details != null) { adFRHrList.setSelect(true); adFRHrList.setParameter(details.getFrParameter()); }


			    // Project Management

			    // PM Maintenance
			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7000), "Synchronizer", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7000), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }


			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7001), "Project Entry", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7001), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }

			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7002), "Find Project", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7002), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }

			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7003), "Project Type Entry", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7003), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }

			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7004), "Find Project Type", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7004), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }

			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7005), "Project Type Type Entry", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7005), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }

			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7006), "Find Project Type Type", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7006), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }

			    // PM REPORTS
			    adFRPmList = new AdFormFunctionResponsibilityPmList(actionForm, new Integer(7100), "Project Profitability", "F", false);
				adFRPmList.setParameterList(parameterList);
				actionForm.saveAdFRPmList(adFRPmList);
				details = ejbFR.getAdFrByRsCodeAndFfCode(actionForm.getResponsibilityCode(), new Integer(7100), user.getCmpCode());
			    if (details != null) { adFRPmList.setSelect(true); adFRPmList.setParameter(details.getFrParameter()); }



	        } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdFormFunctionResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

	        if (actionForm.getTableType() == null) {

	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

            }

            actionForm.setPageState(Constants.PAGE_STATE_SAVE);

            return(mapping.findForward("adFormFunctionResponsibility"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(ArithmeticException e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdFormFunctionResponsibilityAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }

}