package com.struts.ad.formfunctionresponsibility;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class AdFormFunctionResponsibilityForm extends ActionForm implements Serializable {

   private Integer responsibilityCode = null;
   private String responsibilityName = null;
   private String description = null;
   private String tableType = null;
   private ArrayList parameterList = new ArrayList();

   private String pageState = new String();
   private ArrayList adFRGlList = new ArrayList();
   private ArrayList adFRApList = new ArrayList();
   private ArrayList adFRArList = new ArrayList();
   private ArrayList adFRCmList = new ArrayList();
   private ArrayList adFRAdList = new ArrayList();
   private ArrayList adFRInvList = new ArrayList();
   private ArrayList adFRHrList = new ArrayList();
   private ArrayList adFRPmList = new ArrayList();
   
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdFormFunctionResponsibilityGlList getAdFRGlByIndex(int index) {

      return((AdFormFunctionResponsibilityGlList)adFRGlList.get(index));

   }
   
   public AdFormFunctionResponsibilityApList getAdFRApByIndex(int index) {

      return((AdFormFunctionResponsibilityApList)adFRApList.get(index));

   }
   
   public AdFormFunctionResponsibilityArList getAdFRArByIndex(int index) {

      return((AdFormFunctionResponsibilityArList)adFRArList.get(index));

   }
   
   public AdFormFunctionResponsibilityCmList getAdFRCmByIndex(int index) {

      return((AdFormFunctionResponsibilityCmList)adFRCmList.get(index));

   }
   
   public AdFormFunctionResponsibilityAdList getAdFRAdByIndex(int index) {

      return((AdFormFunctionResponsibilityAdList)adFRAdList.get(index));

   }
   
   public AdFormFunctionResponsibilityInvList getAdFRInvByIndex(int index) {

      return((AdFormFunctionResponsibilityInvList)adFRInvList.get(index));

   }
   
   public AdFormFunctionResponsibilityHrList getAdFRHrByIndex(int index) {

      return((AdFormFunctionResponsibilityHrList)adFRHrList.get(index));

   }
   
   public AdFormFunctionResponsibilityPmList getAdFRPmByIndex(int index) {

	      return((AdFormFunctionResponsibilityPmList)adFRPmList.get(index));

	   }

   public Object[] getAdFRGlList() {

      return adFRGlList.toArray();

   }
   
   public Object[] getAdFRApList() {

      return adFRApList.toArray();

   }
   
   public Object[] getAdFRArList() {

      return adFRArList.toArray();

   }
   
   public Object[] getAdFRCmList() {

      return adFRCmList.toArray();

   }
   
   public Object[] getAdFRAdList() {

      return adFRAdList.toArray();

   }
   
   public Object[] getAdFRInvList() {

      return adFRInvList.toArray();

   }
   
   public Object[] getAdFRHrList() {

      return adFRHrList.toArray();

   }
   
   public Object[] getAdFRPmList() {

	      return adFRPmList.toArray();

	   }
   

   public int getAdFRGlListSize() {

      return adFRGlList.size();

   }   
   
   public int getAdFRApListSize() {

      return adFRApList.size();

   }   
   
   public int getAdFRArListSize() {

      return adFRArList.size();

   }   
   
   public int getAdFRCmListSize() {

      return adFRCmList.size();

   }   
   
   public int getAdFRAdListSize() {

      return adFRAdList.size();

   }   
   
   public int getAdFRInvListSize() {

      return adFRInvList.size();

   }
   
   public int getAdFRHrListSize() {

      return adFRHrList.size();

   }
   
   public int getAdFRPmListSize() {

	      return adFRPmList.size();

	   }
   
      
   public void saveAdFRGlList(Object newAdFRList) {

      adFRGlList.add(newAdFRList);
      
   }
   
   public void saveAdFRApList(Object newAdFRList) {

      adFRApList.add(newAdFRList);
      
   }
   
   public void saveAdFRArList(Object newAdFRList) {

      adFRArList.add(newAdFRList);
      
   }
   
   public void saveAdFRCmList(Object newAdFRList) {

      adFRCmList.add(newAdFRList);
      
   }
   
   public void saveAdFRAdList(Object newAdFRList) {

      adFRAdList.add(newAdFRList);
      
   }
   
   public void saveAdFRInvList(Object newAdFRList) {

      adFRInvList.add(newAdFRList);
      
   }
   
   public void saveAdFRHrList(Object newAdFRList) {

	      adFRHrList.add(newAdFRList);
	      
   }
   
   public void saveAdFRPmList(Object newAdFRList) {

	      adFRPmList.add(newAdFRList);
	      
   }
   
   
   public int getAdFRGlIndexOf(Object obj) {
   	
   	  return adFRGlList.indexOf(obj);
   	
   }
   
   public int getAdFRApIndexOf(Object obj) {
   	
   	  return adFRApList.indexOf(obj);
   	
   }
   
   public int getAdFRArIndexOf(Object obj) {
   	
   	  return adFRArList.indexOf(obj);
   	
   }
   
   public int getAdFRCmIndexOf(Object obj) {
   	
   	  return adFRCmList.indexOf(obj);
   	
   }
   
   public int getAdFRAdIndexOf(Object obj) {
   	
   	  return adFRAdList.indexOf(obj);
   	
   }
   
   public int getAdFRInvIndexOf(Object obj) {
   	
   	  return adFRInvList.indexOf(obj);
   	
   }
   
   public int getAdFRHrIndexOf(Object obj) {
	   	
	   	  return adFRHrList.indexOf(obj);
	   	
	   }
   
   public int getAdFRPmIndexOf(Object obj) {
	   	
	   	  return adFRPmList.indexOf(obj);
	   	
	   }
   
   
        
   public void clearAdFRGlList() {
   	
      adFRGlList.clear();
   }
   
   public void clearAdFRApList() {
   	
      adFRApList.clear();
   }
   
   public void clearAdFRArList() {
   	
      adFRArList.clear();
   }
   
   public void clearAdFRCmList() {
   	
      adFRCmList.clear();
   }
   
   public void clearAdFRAdList() {
   	
      adFRAdList.clear();
   }
   
   public void clearAdFRInvList() {
   	
      adFRInvList.clear();
   }
   
   public void clearAdFRHrList() {
	   	
      adFRHrList.clear();
   }
   
   public void clearAdFRPmList() {
	   	
	      adFRPmList.clear();
	   }
   
   
   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getResponsibilityCode() {
   	
   	  return responsibilityCode;
   	
   }
   
   public void setResponsibilityCode(Integer responsibilityCode) {
   	
   	  this.responsibilityCode = responsibilityCode;
   	
   }
   
   public String getResponsibilityName() {
   	
   	  return responsibilityName;
   	  
   }
   
   public void setResponsibilityName(String responsibilityName) {
   	
   	  this.responsibilityName = responsibilityName;   	  
   
   }
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }
   
   public ArrayList getParameterList() {
   	
   	   return parameterList;
   	
   }
   
   public void setParameterList(String parameter) {
   	
   	   parameterList.add(parameter);
   	
   }
   
   public void clearParameterList() {
   	
   	   parameterList.clear();
   	   parameterList.add(Constants.GLOBAL_BLANK);
   	
   }

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	
	  for (int i=0; i<adFRGlList.size(); i++) {
	  	
	  	  AdFormFunctionResponsibilityGlList actionList = (AdFormFunctionResponsibilityGlList)adFRGlList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
	  
	  for (int i=0; i<adFRApList.size(); i++) {
	  	
	  	  AdFormFunctionResponsibilityApList actionList = (AdFormFunctionResponsibilityApList)adFRApList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
	  
	  for (int i=0; i<adFRArList.size(); i++) {
	  	
	  	  AdFormFunctionResponsibilityArList actionList = (AdFormFunctionResponsibilityArList)adFRArList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
	  
	  for (int i=0; i<adFRCmList.size(); i++) {
	  	
	  	  AdFormFunctionResponsibilityCmList actionList = (AdFormFunctionResponsibilityCmList)adFRCmList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
	  
	  for (int i=0; i<adFRAdList.size(); i++) {
	  	
	  	  AdFormFunctionResponsibilityAdList actionList = (AdFormFunctionResponsibilityAdList)adFRAdList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
	  
	  for (int i=0; i<adFRInvList.size(); i++) {
	  	
	  	  AdFormFunctionResponsibilityInvList actionList = (AdFormFunctionResponsibilityInvList)adFRInvList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
	  
	  for (int i=0; i<adFRHrList.size(); i++) {
		  	
	  	  AdFormFunctionResponsibilityHrList actionList = (AdFormFunctionResponsibilityHrList)adFRHrList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
	  
	  for (int i=0; i<adFRPmList.size(); i++) {
		  	
	  	  AdFormFunctionResponsibilityPmList actionList = (AdFormFunctionResponsibilityPmList)adFRPmList.get(i);
	  	  actionList.setSelect(false);
	  	
	  }
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      return errors;

   }
}