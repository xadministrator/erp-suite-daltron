package com.struts.ad.formfunctionresponsibility;

import java.io.Serializable;
import java.util.ArrayList;

public class AdFormFunctionResponsibilityGlList implements Serializable {

   private Integer formFunctionCode = null;
   private String formFunctionName = null;
   private String parameter = null;
   private ArrayList parameterList = new ArrayList();
   private boolean select = false;
    
   private AdFormFunctionResponsibilityForm parentBean;
    
   public AdFormFunctionResponsibilityGlList(AdFormFunctionResponsibilityForm parentBean,
      Integer formFunctionCode,
      String formFunctionName,
      String parameter,
	  boolean select) {

      this.parentBean = parentBean;
      this.formFunctionCode = formFunctionCode;
      this.formFunctionName = formFunctionName;
      this.parameter = parameter;
      this.select = select;

   }
   
   public Integer getFormFunctionCode() {
    
    return formFunctionCode;
    
   }

   public String getFormFunctionName() {
       
       return formFunctionName;
       
   }

   public String getParameter() {

      return parameter;

   }
   
   public void setParameter(String parameter) {
   	
   	  this.parameter = parameter;
   	  
   }
   
   public ArrayList getParameterList() {
   	
   	  return parameterList;
   	
   }
   
   public void setParameterList(ArrayList parameterList) {
   	
   	  this.parameterList = parameterList;
   	
   }

   public boolean getSelect() {

      return select;

   }
   
   public void setSelect(boolean select) {
   	
   	   this.select = select;
   	
   }

}