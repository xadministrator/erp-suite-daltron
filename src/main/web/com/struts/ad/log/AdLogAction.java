package com.struts.ad.log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.ad.LocalAdLog;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdAgingBucketController;
import com.ejb.txn.AdAgingBucketControllerHome;
import com.ejb.txn.AdLogController;
import com.ejb.txn.AdLogControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdLogDetails;

public final class AdLogAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdLogAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdLogForm actionForm = (AdLogForm)form;
         
        
/*******************************************************
   Initialize AdLogController EJB
*******************************************************/

         AdLogControllerHome homeLOG = null;
         AdLogController ejbLOG = null;
     
         try {

        	 homeLOG = (AdLogControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdLogControllerEJB", AdLogControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdLogAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbLOG = homeLOG.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdLogAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         

    
            
/*******************************************************
   -- Ad AB Load Action --
*******************************************************/


        if (!errors.isEmpty()) {

           saveErrors(request, new ActionMessages(errors));
           return mapping.findForward("adLog");

        }
        
        
        

        if (request.getParameter("forward") != null ) {
	
            
        	if(request.getParameter("module")!= null && request.getParameter("moduleKey")!= null) {
        		
        		String module = request.getParameter("module");
        		Integer module_key = Common.convertStringToInteger(request.getParameter("moduleKey"));
        		String transactionNumber = request.getParameter("transactionNumber");
        		actionForm.clearAdLOGList();
        		
        		ArrayList list = ejbLOG.getAdLogAllByLogMdlAndLogMdlKy(module, module_key, user.getCmpCode());
        		
        		Iterator i = list.iterator();
        		
        		while(i.hasNext()) {
        			
        			AdLogDetails details = (AdLogDetails)i.next();
        			
        			AdLogList adLogList = new AdLogList(actionForm, details.getLogCode(), Common.convertSQLDateAndTimeToString(details.getLogDate()),details.getLogUsername(), details.getLogAction(), details.getLogDescription(), details.getLogModule(), details.getLogCode());
        			
        			
        			actionForm.saveAdLOGList(adLogList);
        				
        			
        		}
        		
        		
        		actionForm.setModule(module);
        		actionForm.setTransaction(transactionNumber);
        		
        		
        		
        		
        		return(mapping.findForward("adLog"));
        		
        		
        	}
        	
        	
        	
        	
            
        }
        
        
        
        
          

        if (!errors.isEmpty()) {

           saveErrors(request, new ActionMessages(errors));
           
        } 

        actionForm.reset(mapping, request);
        
       
        
        return(mapping.findForward("adLog"));

       

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdLogAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}