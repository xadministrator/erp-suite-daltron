package com.struts.ad.log;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ad.agingbucket.AdAgingBucketList;
import com.struts.util.Common;
import com.struts.util.Constants;


public class AdLogForm extends ActionForm implements Serializable {


  
   private String dateFrom = null;
   private String dateTo = null;
   private String username = null;
   private ArrayList usernameList = new ArrayList();
   private String action = null;
   private ArrayList actionList = new ArrayList();
   private String description = null;
   private String transactionNumber = null;
   private String module = null;
   private ArrayList moduleList = new ArrayList();
   private Integer moduleKey = null;
  
   private String userPermission = new String();
   private String txnStatus = new String();
   
   
   private ArrayList adLOGList = new ArrayList();
  
   public AdLogList getAdLOGByIndex(int index) {

      return((AdLogList)adLOGList.get(index));

   }

   public Object[] getAdLOGList() {

      return (adLOGList.toArray());

   }

   public int getAdLOGListSize() {

      return adLOGList.size();

   }

   public void saveAdLOGList(Object newAdLOGList) {

	   adLOGList.add(newAdLOGList);

   }

   public void clearAdLOGList() {
	   adLOGList.clear();
   }
  
   
   public String getDateFrom() {
	return dateFrom;
	}
	
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	
	public String getDateTo() {
		return dateTo;
	}
	
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getTransactionNumber() {
		return transactionNumber;
	}
	
	public void setTransaction(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}
	
	public String getModule() {
		return module;
	}
	
	public void setModule(String module) {
		this.module = module;
	}

public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
 
   public void clearUsernameList() {
	   usernameList.clear();
   }
   
   public ArrayList getUsernameList() {
	   return this.usernameList;
   }
   
   public void setUsernameList(String username) {
	   this.usernameList.add(username);
   }
   
   
   public void clearActionList() {
	   actionList.clear();
   }
   
   public ArrayList getActionList() {
	   return this.actionList;
   }
   
   public void setActionList(String action) {
	   this.actionList.add(action);
   }
   
   
   public void clearModuleList() {
	   moduleList.clear();
   }
   
   public ArrayList getModuleList() {
	   return this.moduleList;
   }
   
   public void setModuleList(String module) {
	   this.moduleList.add(module);
   }
   
   
   
   
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

	   
	   dateFrom = null;
	   dateTo = null;
	   username = null;
	   action = null;
	   module = null;
	
	
	

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
    
      return errors;

   }
}