package com.struts.ad.log;

import java.io.Serializable;

public class AdLogList implements Serializable {

	 private Integer logCode = null;
	   private String date = null;
	   private String username = null;
	   private String action = null;
	   private String description = null;
	   private String module = null;
	   private Integer moduleKey = null;
    
   private AdLogForm parentBean;
    
   public AdLogList(AdLogForm parentBean,
      Integer logCode,
      String date,
      String username,
      String action,
      String description,
      String module,
      Integer moduleKey) {

      this.parentBean = parentBean;
      this.logCode = logCode;
      this.date = date;
      this.username = username;
      this.action = action;
      this.description = description;
      this.module = module;
      this.moduleKey = moduleKey;

   }

	public Integer getLogCode() {
		return logCode;
	}
	
	public void setLogCode(Integer logCode) {
		this.logCode = logCode;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getModule() {
		return module;
	}
	
	public void setModule(String module) {
		this.module = module;
	}
	
	public Integer getModuleKey() {
		return moduleKey;
	}
	
	public void setModuleKey(Integer moduleKey) {
		this.moduleKey = moduleKey;
	}
	
	public AdLogForm getParentBean() {
		return parentBean;
	}
	
	public void setParentBean(AdLogForm parentBean) {
		this.parentBean = parentBean;
	}

  

   
}