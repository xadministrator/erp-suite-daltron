package com.struts.ad.logon;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;
import com.struts.util.User;

public final class AdLogoffAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
 
    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
      
       HttpSession session = request.getSession();
       try{
          User user = (User) session.getAttribute(Constants.USER_KEY);

          String company = user.getCompany();
          
          if (user != null) {
              if (log.isInfoEnabled())
                  log.info("LogoffAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                      "' logged off in session " + session.getId());
          } else {
              if (log.isInfoEnabled())
                  log.info("LogoffActon: User logged off in session " +
                    session.getId());
              
              return (mapping.findForward("adLogon"));
              
          }
          
          // remove session from servlet context
          
          HashMap sessions = (HashMap)servlet.getServletContext().getAttribute(Constants.SESSIONS_KEY);
          sessions.remove(user.getUserName().trim() + user.getCompany().trim());
          servlet.getServletContext().setAttribute(Constants.SESSIONS_KEY, sessions);
          
                    	
          session.invalidate();
              
          	        
          return (new ActionForward("/adLogon.jsp?companyCode=" + company));
          
      }catch(Exception e){
         if(log.isInfoEnabled())
	    log.info("Exception caught in LogoffAction.execute(): " + e.getMessage() + " session: " + session.getId());
	 return(mapping.findForward("adErrorPage"));
      }

    }
}
