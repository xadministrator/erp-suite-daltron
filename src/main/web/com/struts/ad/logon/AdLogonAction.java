package com.struts.ad.logon;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.ejb.CreateException;
import javax.naming.NamingException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.txn.AdBranchResponsibilityController;
import com.ejb.txn.AdBranchResponsibilityControllerHome;
import com.ejb.txn.AdPreferenceController;
import com.ejb.txn.AdPreferenceControllerHome;
import com.struts.util.Branch;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.FormFunctionRes;
import com.struts.util.LogonDB;
import com.struts.util.Responsibility;
import com.struts.util.User;
import com.struts.util.UserRes;
import com.util.AdBranchResponsibilityDetails;
import com.util.AdModPreferenceDetails;
import com.util.AppCallbackHandler;

public final class AdLogonAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping, ActionForm form,
	   HttpServletRequest request, HttpServletResponse response) 
	   throws Exception {
		
	   ActionErrors errors = new ActionErrors();
	   MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
	   

	   byte[] sigToVerify = null;
	   java.security.Signature sig = null;
	   
	   if (appProperties.getMessage("app.enableLicense").equals("true")) {
			
		   byte[] encKey = { 48,-126,1,-73,48,-126,1,44,6,7,42,-122,72,-50,56,4,1,48,-126,1,31,2,-127,-127,0,
									-3,127,83,-127,29,117,18,41,82,-33,74,-100,46,-20,-28,-25,-10,17,-73,82,60,-17,68
									,0,-61,30,63,-128,-74,81,38,105,69,93,64,34,81,-5,89,61,-115,88,-6,-65,-59,-11,
									-70,48,-10,-53,-101,85,108,-41,-127,59,-128,29,52,111,-14,102,96,-73,107,-103,80
									,-91,-92,-97,-97,-24,4,123,16,34,-62,79,-69,-87,-41,-2,-73,-58,27,-8,59,87,-25,
									-58,-88,-90,21,15,4,-5,-125,-10,-45,-59,30,-61,2,53,84,19,90,22,-111,50,-10,117,
									-13,-82,43,97,-41,42,-17,-14,34,3,25,-99,-47,72,1,-57,2,21,0,-105,96,80,-113,21,35
									,11,-52,-78,-110,-71,-126,-94,-21,-124,11,-16,88,28,-11,2,-127,-127,0,-9,-31,-96
									,-123,-42,-101,61,-34,-53,-68,-85,92,54,-72,87,-71,121,-108,-81,-69,-6,58,-22,-126
									,-7,87,76,11,61,7,-126,103,81,89,87,-114,-70,-44,89,79,-26,113,7,16,-127,-128
									,-76,73,22,113,35,-24,76,40,22,19,-73,-49,9,50,-116,-56,-90,-31,60,22,122,-117,84
									,124,-115,40,-32,-93,-82,30,43,-77,-90,117,-111,110,-93,127,11,-6,33,53,98,-15,
									-5,98,122,1,36,59,-52,-92,-15,-66,-88,81,-112,-119,-88,-125,-33,-31,90,-27,-97,6
									,-110,-117,102,94,-128,123,85,37,100,1,76,59,-2,-49,73,42,3,-127,-124,0,2,-127,-128
									,106,55,114,-52,-23,-40,25,127,8,88,64,-61,77,-64,-94,68,52,-64,1,115,19,-120
									,28,30,35,-46,-112,-95,12,-23,-37,17,-95,-94,-68,-50,-61,25,77,-80,-108,69,18,83
									,45,93,-113,79,-111,-5,36,-60,104,-64,56,-76,25,-105,-3,-119,100,-57,111,-93,89,
									85,1,59,-76,10,-116,69,40,19,94,-32,104,55,82,92,-116,99,-102,81,104,-49,103,-67
									,72,-115,122,-119,101,-2,-98,22,-40,78,-99,36,-108,-110,-39,51,43,-74,-84,116,-82
									,-123,-57,84,105,54,24,94,121,104,32,-27,35,22,-14,104,-30,15,-116,-107 };
	       
	       java.security.spec.X509EncodedKeySpec pubKeySpec = new java.security.spec.X509EncodedKeySpec(encKey);
	
	       java.security.KeyFactory keyFactory = java.security.KeyFactory.getInstance("DSA", "SUN");
	       java.security.PublicKey pubKey = keyFactory.generatePublic(pubKeySpec); 
	
	       java.io.FileInputStream sigfis = null;
	       try {
	          sigfis = new java.io.FileInputStream(appProperties.getMessage("app.license"));
	       } catch (java.io.FileNotFoundException ex) {
	       	  errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.missingLicense"));
		   	  saveErrors(request, new ActionMessages(errors));
			  return(new ActionForward(mapping.getInput())); 
	       }
	       
	       sigToVerify = new byte[sigfis.available()]; 
	       sigfis.read(sigToVerify);
	
	       sigfis.close();
	
	       sig = java.security.Signature.getInstance("SHA1withDSA", "SUN");
	       sig.initVerify(pubKey);
	
	       java.io.FileInputStream datafis = new java.io.FileInputStream(appProperties.getMessage("app.earLocation"));
	       java.io.BufferedInputStream bufin = new java.io.BufferedInputStream(datafis);
	
	       byte[] buffer = new byte[1024];
	       int len;
	       while (bufin.available() != 0) {
	           len = bufin.read(buffer);
	           sig.update(buffer, 0, len);
	       }
	
	       bufin.close(); 	       
	       
	   }
       
       HttpSession session = request.getSession();
              
       if(request.getParameter("resetButton") != null){
       	
	      ((AdLogonForm) form).reset(mapping, request);
	      return mapping.findForward("adLogon");
	      
	   }else if(request.getParameter("setupButton") != null){
	   	
	   	  return mapping.findForward("adSetup");
	   	  
	   }else if(appProperties.getMessage("app.enableLicense").equals("true") && 
	   		(!sig.verify(sigToVerify) || this.validateExpirationDate(appProperties))) {
	   	
	   	  errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.invalidLicense"));
	   	  saveErrors(request, new ActionMessages(errors));
		  return(new ActionForward(mapping.getInput()));
		  
	   }else if(request.getParameter("connectButton") != null){
	   	
	   	String userName = ((AdLogonForm) form).getUserName().trim();
		String password = ((AdLogonForm) form).getPassword();
		String company = ((AdLogonForm) form).getCompany().trim().toUpperCase();
		String companyName = null;
		String welcomeNote = null;
		
		// validate multiple login
	    
	   	HashMap sessions = (HashMap)servlet.getServletContext().getAttribute(Constants.SESSIONS_KEY);
	   	
	 // get branches for current responsibility
	      
	   	  AdPreferenceControllerHome homeAP = null;
	   	  AdPreferenceController ejbAP = null;
	      
	      

	      try {
	    	  homeAP = (AdPreferenceControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/AdPreferenceControllerEJB", AdPreferenceControllerHome.class);
	      } catch (NamingException e) {
	      	
	      	if (log.isInfoEnabled()) {
	      		
	      		log.info("NamingException caught in AdLogonAction.execute(): " + e.getMessage() +
	      				" session: " + session.getId());
	      	}
		      	
	      	return mapping.findForward("cmnErrorPage");
		      	
	      }
	      
	      
	      try {
	    	  	ejbAP = homeAP.create();
		      } catch (CreateException e) {
		      	
		      	if (log.isInfoEnabled()) {
		      		
		      		log.info("CreateException caught in AdLogonAction.execute(): " + e.getMessage() +
		      				" session: " + session.getId());
		      		
		      	}
		      	return mapping.findForward("cmnErrorPage");
		       }
	      
	      
	      boolean disableMultipleLogin = false;
	      try {
	    	  disableMultipleLogin = Common.convertByteToBoolean(ejbAP.getAdPrfAdDisableMultipleLogin(company));
		      
	      } catch (Exception ex) {
	    	  disableMultipleLogin = false;
	      }
	      
		   	if(disableMultipleLogin == false){
		   		
		   	
			   	if (sessions != null && sessions.size() != 0) {
			   		
			   	    HttpSession existingSession = (HttpSession)sessions.get(userName + company);
			   	    
			   	    try {
			   	    	
				   	    if (existingSession != null && ((User)existingSession.getAttribute(Constants.USER_KEY)) != null) {
			       	
				           errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.multipleLogin"));    	
				           saveErrors(request, new ActionMessages(errors));
						   return(new ActionForward(mapping.getInput()));
			       	
				        }
				        
				    } catch (IllegalStateException ex) {
				    	
				    	sessions.remove(userName);
		          		servlet.getServletContext().setAttribute(Constants.SESSIONS_KEY, sessions);
				    	
				    }
			        
			 	}
	   		
	   	}
		   
		LogonDB logonDB = new LogonDB();
		int status = 0;
		try{

		
		      status = logonDB.validateUser(
		      (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY),
		      userName, password, company);
		}catch(SQLException sqle){
		   if(log.isInfoEnabled()){
		      log.info("SQLException caught in LogonAction.execute(): " + sqle.getMessage());
		   }
		   return(mapping.findForward("adErrorPage"));
		}catch(Exception e){
		   if(log.isInfoEnabled()){
		      log.info("Exception caught in LogonAction.execute(): " + e.getMessage());
		   }
		   return(mapping.findForward("adErrorPage"));
		}				
    	
		if(status == Constants.ADMIN_INVALID_LOGON)
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.mismatch"));
		else if(status == Constants.ADMIN_EXPIRED_ACCESS)
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.expirationAccess"));
		else if(status == Constants.ADMIN_EXPIRED_DAYS)
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.expirationDays"));
		else if(status == Constants.ADMIN_USER_DISABLED)
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.expirationDate"));
		else if(status == Constants.ADMIN_NO_RESPONSIBILITY)
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.noResponsibility"));
		else if(status == Constants.ADMIN_USER_RES_DISABLED)
		 	errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.userResDisabled"));
		else if(status == Constants.ADMIN_USER_ASSIGNED_RES_DISABLED)
	        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("logon.error.userAssignedResDisabled"));
		
		
		if (!errors.isEmpty()) {
			saveErrors(request, new ActionMessages(errors));
			return(new ActionForward(mapping.getInput()));
		}
		
        // Load user profiles to System State Beans 
		DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		User user = new User();
		int cmpCode = 0;
		try{
		   conn = dataSource.getConnection();
		   
		   stmt = conn.prepareStatement("SELECT CMP_CODE, CMP_NM, CMP_WLCM_NT FROM AD_CMPNY WHERE CMP_SHRT_NM=?");
		     
		   stmt.setString(1, company);
		     
		   rs = stmt.executeQuery();
		     
		   while(rs.next()) {
		   	cmpCode = rs.getInt(1);
		   	companyName = rs.getString(2);
		   	welcomeNote = rs.getString(3);
		   }
		   
		   stmt = conn.prepareStatement("SELECT USR_CODE, USR_NM, USR_DESC, USR_DEPT FROM AD_USR WHERE USR_NM = ? AND USR_AD_CMPNY = ?");

		   stmt.setString(1, userName.trim());
		   stmt.setInt(2, cmpCode);
		   rs = stmt.executeQuery();

		   while(rs.next()){
		     user.setUserCode(rs.getInt(1));
		     user.setUserName(rs.getString(2));
		     user.setUserDescription(rs.getString(3));
		     user.setUserDepartment(rs.getString(4));
		     SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		     user.setLoginTime(sdf.format(new Date()));		     
		     user.setCurrentResCode(logonDB.getDefaultResCode());
		     user.setCurrentAppCode(1);
		     user.setCmpCode(new Integer(cmpCode));
		     user.setCompany(company);
		     user.setCompanyName(companyName);
		     user.setWelcomeNote(welcomeNote);
		   }

		   // get branches for current responsibility
		      
	      AdBranchResponsibilityControllerHome homeBR = null;
	      AdBranchResponsibilityController ejbBR = null;

	      try {
	      	homeBR = (AdBranchResponsibilityControllerHome)com.util.EJBHomeFactory.
			lookUpHome("ejb/AdBranchResponsibilityControllerEJB", AdBranchResponsibilityControllerHome.class);
	      } catch (NamingException e) {
	      	
	      	if (log.isInfoEnabled()) {
	      		
	      		log.info("NamingException caught in AdPaymentTermAction.execute(): " + e.getMessage() +
	      				" session: " + session.getId());
	      	}
		      	
	      	return mapping.findForward("cmnErrorPage");
		      	
	      }
	      
	      try {
	      	ejbBR = homeBR.create();
	      } catch (CreateException e) {
	      	
	      	if (log.isInfoEnabled()) {
	      		
	      		log.info("CreateException caught in AdPaymentTermAction.execute(): " + e.getMessage() +
	      				" session: " + session.getId());
	      		
	      	}
	      	return mapping.findForward("cmnErrorPage");
	       }
          
	      ArrayList branches = new ArrayList();
	      branches = ejbBR.getAdBrnchRspnsbltyAll(user.getCurrentResCode(), user.getCmpCode());
		   
	      
		   stmt = conn.prepareStatement("SELECT UR_CODE, UR_RS_CODE, UR_DT_FRM, UR_DT_TO " +
		   				"FROM AD_USR_RSPNSBLTY " +
						"WHERE UR_USR_CODE = ? AND UR_AD_CMPNY = ?");

		   stmt.setInt(1, user.getUserCode());
		   stmt.setInt(2, cmpCode);

		   rs = stmt.executeQuery();

		   while(rs.next()){
		      UserRes userRes = new UserRes();
		      userRes.setURCode(rs.getInt(1));
		      userRes.setURResCode(rs.getInt(2));
		      userRes.setURDateFrom(rs.getDate(3));
		      userRes.setURDateTo(rs.getDate(4));
		      user.setUserRes(userRes);
		   }	   
		   		   
		   for(int i=0; i<user.getUserResCount(); i++){
		      UserRes userResVal = user.getUserRes(i);
		      stmt = conn.prepareStatement("SELECT RS_CODE, RS_NM, RS_DT_FRM, RS_DT_TO FROM AD_RSPNSBLTY " +
		                                    "WHERE RS_CODE = ? AND RS_AD_CMPNY = ?");
						    
		      stmt.setInt(1, userResVal.getURResCode());
		      stmt.setInt(2, cmpCode);
		      
		      rs = stmt.executeQuery();

		      while(rs.next()){
		         Responsibility res = new Responsibility();
				 res.setRSCode(rs.getInt(1));
				 res.setRSName(rs.getString(2));
				 res.setRSDateFrom(rs.getDate(3));
				 res.setRSDateTo(rs.getDate(4));
				 user.setRes(res);
		      }

		    }
		   
		   Iterator j = branches.iterator();
		   user.clearBranches();
		   while ( j.hasNext() ) {

		   		stmt = conn.prepareStatement("SELECT BR_CODE, BR_BRNCH_CODE, BR_NM, BR_HD_QTR FROM AD_BRNCH " +
		   									 "WHERE BR_CODE = ? AND BR_AD_CMPNY = ?");
		   	
		   		stmt.setInt(1, ((AdBranchResponsibilityDetails)j.next()).getBrsCode().intValue() );
		   		stmt.setInt(2, cmpCode);
		   	
		   		rs = stmt.executeQuery();
		   		while(rs.next()){
		   			Branch brnch = new Branch();
		   			brnch.setBrCode(rs.getInt(1));
		   			brnch.setBrBranchCode(rs.getString(2));
		   			brnch.setBrName(rs.getString(3));
		   			user.setBranch(brnch);
		   			if (rs.getInt(4) == 1) user.setCurrentBranch(brnch);
		   		}	
		   		
		   }
		   if (user.getCurrentBranch() == null) {
			  Branch br = user.getBranch(0);
			  user.setCurrentBranch(br);
		   }

		   for(int i=0; i<user.getResCount(); i++){
		      Responsibility resVal = user.getRes(i);
		      stmt = conn.prepareStatement("SELECT FR_CODE, FR_RS_CODE, FR_FF_CODE, FR_PARAM FROM AD_FF_RSPNSBLTY " +
		                                    "WHERE FR_RS_CODE = ? AND FR_AD_CMPNY = ?");
		      
		      stmt.setInt(1, resVal.getRSCode());
		      stmt.setInt(2, cmpCode);

		      rs = stmt.executeQuery();

		      while(rs.next()){
		         FormFunctionRes ffRes = new FormFunctionRes();
			 ffRes.setFrCode(rs.getInt(1));
			 ffRes.setFrRsCode(rs.getInt(2));
			 ffRes.setFrFfCode(rs.getInt(3));
			 ffRes.setFrParam(rs.getString(4));
			 user.setFFRes(String.valueOf(ffRes.getFrFfCode()) + String.valueOf(ffRes.getFrRsCode()), ffRes);
		      }
		   }
		   
		   stmt = conn.prepareStatement("SELECT APP_NM FROM AD_APPLCTN WHERE APP_INSTLLD = 1 AND APP_AD_CMPNY = ?");
		   
		   stmt.setInt(1, cmpCode);

		   rs = stmt.executeQuery();

           ArrayList userApps = new ArrayList();
 
		   while(rs.next()) {
		      		  
			   System.out.println(rs.getString(1));
		      userApps.add(rs.getString(1));
		      
		   }
           
           user.setUserApps(userApps);

		}catch(SQLException sqle){
           	   if(log.isInfoEnabled())
		      log.info("SQLException caught in LogonAction.execute(): " + sqle.getMessage());
                   return(mapping.findForward("adErrorPage"));
		}catch(Exception e){
		   if(log.isInfoEnabled())
		      log.info("Exception caught in LogonAction.execute(): " + e.getMessage());
		   return(mapping.findForward("adErrorPage"));
		}finally{
	           try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
                   rs = null;
                   try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
                   stmt = null;
                   try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
                   conn = null;
		}
		
		// EJB Login
		
		LoginContext lc = null;
 		AppCallbackHandler handler = null;
		
		try {
			
			handler = new AppCallbackHandler("omegabci", "omegabusinessconsulting");
			lc = new LoginContext(appProperties.getMessage("app.customName"), handler);
			lc.login();
			
	    } catch (LoginException le) {
	    	le.printStackTrace();
	    	if(log.isInfoEnabled()) 
	    	    log.info("LoginException caught in LogonAction.execute(): " + le.getMessage());
	    	    
	    	return(mapping.findForward("adErrorPage"));
	    	
	    } 
	    
	    Set set = lc.getSubject().getPrincipals();
	    
	    Iterator iter = set.iterator();
	    
	    if(log.isInfoEnabled()) log.info("Subject Principals: ");
	    
	    while (iter.hasNext()) {
	    	
	    	java.security.Principal p = (java.security.Principal) iter.next();
	    	if(log.isInfoEnabled()) log.info("p: " + p);
	    	
	    } 
		   
		
		try{
			
		   // Store user profiles to the session.
		   
		   session.setAttribute(Constants.USER_KEY, user);	   
		   		   
		   if (sessions == null) {
		   	
		   	   sessions = new HashMap();
		   	   		   	   		   	
		   }
		   
		   
		   // Store sessions to servlet context	   	
		   sessions.put(userName + company, session);
		   servlet.getServletContext().setAttribute(Constants.SESSIONS_KEY, sessions);
		   
		   
           if(log.isInfoEnabled())
              log.info("LogonAction: User'" + userName +
                    "' logged on in session " + session.getId());
		}catch(Exception e){
		    if(log.isInfoEnabled())
               log.info("Exception caught in LogonAction.execute(): " + e.getMessage());
            return(mapping.findForward("adErrorPage"));
		}
	        // Success: Forward to Main Page	
		return(mapping.findForward("cmnMain"));
		
	}else{
		
	   return(mapping.findForward("adLogon"));
	   
	}
   }	
	
	// validate expiration date
	private boolean validateExpirationDate(MessageResources appProperties) {
		
		if(appProperties.getMessage("app.expirationDay").equals("-1"))
			return false;
		
		GregorianCalendar calendar = new GregorianCalendar();
		Date currentDate = new Date();
		Date expirationDate = new Date();
		
		try {
			
			String dateFormat = "MM/dd/yy";
			DateFormat myDateFormat = new SimpleDateFormat(dateFormat);	
			
			Date dateInstalled = myDateFormat.parse(appProperties.getMessage("app.dateInstalled"));
			calendar.setTime(dateInstalled);
			calendar.add(Calendar.DATE, Integer.parseInt(appProperties.getMessage("app.expirationDay")));
			String strExpiration = (calendar.get(Calendar.MONTH)+1)+"/"+calendar.get(Calendar.DATE)+"/"+calendar.get(Calendar.YEAR);
			expirationDate = myDateFormat.parse(strExpiration);			
			
		} catch (ParseException e) {
			
		}
		
		if(currentDate.after(expirationDate)) {			
			return true;			
		} else {			
			return false;			
		}
		
	}

}
