package com.struts.ad.lookupvalue;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdLUNoLookUpFoundException;
import com.ejb.exception.AdLVMasterCodeNotFoundException;
import com.ejb.exception.AdLVNoLookUpValueFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdLookUpValueController;
import com.ejb.txn.AdLookUpValueControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdLookUpValueDetails;

public final class AdLookUpValuesAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception { 
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("AdLookUpValuesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session " + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			AdLookUpValuesForm actionForm = (AdLookUpValuesForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.AD_LOOK_UP_VALUES_ID);
			
			if(frParam != null) {
				
				if(frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if(!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return(mapping.findForward("adLookUpValues"));
						
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
   Initialize AdLookUpValues EJB
*******************************************************/
			
			AdLookUpValueControllerHome homeLV = null;
			AdLookUpValueController ejbLV = null;
			
			try{
				
				homeLV = (AdLookUpValueControllerHome)com.util.EJBHomeFactory.
					lookUpHome("ejb/AdLookUpValueControllerEJB", AdLookUpValueControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("NamingException caught in AdLookUpValuesAction.execute(): " + e.getMessage() + 
							" session: " + session.getId()); 
					
				} 
				
				return(mapping.findForward("cmnErrorPage")); 
				
			}
			
			try {
				
				ejbLV = homeLV.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in AdLookUpValuesAction.execute(): " + e.getMessage() + 
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
   -- Ad LV Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("adLookUpValues"));
				
/*******************************************************
   -- Ad LV Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("adLookUpValues"));                   
				
/*******************************************************
   -- Ad LV Save Action --
*******************************************************/
				
			} else if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				AdLookUpValueDetails details = new AdLookUpValueDetails();
				details.setLvName(actionForm.getLookUpValueName());
				details.setLvDescription(actionForm.getDescription());
				details.setLvMasterCode(Common.convertStringToInteger(actionForm.getMasterCode()));
				details.setLvEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				
				try {
					
					ejbLV.addAdLvEntry(details, actionForm.getLookUpName(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("lookUpValues.error.lookUpValueNameAlreadyExist"));
					
				} catch (AdLVMasterCodeNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("lookUpValues.error.masterCodeNotFound"));                 
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in AdLookUpValuesAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}        
				
/*******************************************************
   -- AD LV Close Action --
*******************************************************/
				
			} else if(request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
				
/*******************************************************
   -- Ad LV Update Action --
*******************************************************/
				
			} else if (request.getParameter("updateButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				AdLookUpValuesList adLVList =
					actionForm.getAdLVByIndex(actionForm.getRowSelected());
				
				AdLookUpValueDetails details = new AdLookUpValueDetails();
				details.setLvCode(adLVList.getLookUpValueCode());
				details.setLvName(actionForm.getLookUpValueName());
				details.setLvDescription(actionForm.getDescription());
				details.setLvMasterCode(Common.convertStringToInteger(actionForm.getMasterCode()));
				details.setLvEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				
				try {
					
					ejbLV.updateAdLvEntry(details, actionForm.getLookUpName(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("lookUpValues.error.lookUpValueNameAlreadyExist"));
					
				} catch (AdLVMasterCodeNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("lookUpValues.error.masterCodeNotFound"));                  
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in AdLookUpValuesAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				} 
				
/*******************************************************
   -- AD LV Cancel Action --
*******************************************************/
				
			} else if(request.getParameter("cancelButton") != null) {
				
				actionForm.reset(mapping, request);
				actionForm.setPageState(Constants.PAGE_STATE_SAVE);
				
				return(mapping.findForward("adLookUpValues"));
				
/*******************************************************
   -- AD LV Edit Action --
*******************************************************/
				
			} else if(request.getParameter("adLVList[" + 
					actionForm.getRowSelected() + "].editButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				actionForm.showAdLVRow(actionForm.getRowSelected());
				return(mapping.findForward("adLookUpValues"));
				
/*******************************************************
   -- Ad LV Delete Action --
*******************************************************/
				
			} else if (request.getParameter("adLVList[" +
					actionForm.getRowSelected() + "].deleteButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				AdLookUpValuesList adLVList =
					actionForm.getAdLVByIndex(actionForm.getRowSelected());
				
				try {

					ejbLV.deleteAdLvEntry(adLVList.getLookUpValueCode(), user.getCmpCode());
					
			    } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	                errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("lookUpValues.error.deleteLookUpValueAlreadyAssigned"));
					
				} catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("lookUpValues.error.lookUpValueAlreadyDeleted"));
					
				} catch(EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in AdLookUpValuesAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}				
				
/*******************************************************
   -- Ap LV Look Up Name Enter Action --
*******************************************************/
				
			} else if (request.getParameter("isLookUpNameEntered") != null &&
					request.getParameter("isLookUpNameEntered").equals("true") &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				try {
					
					ArrayList list = ejbLV.getAdLvByLuName(actionForm.getLookUpName(), user.getCmpCode());
					
					actionForm.clearAdLVList();
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						AdLookUpValueDetails lvDetails = (AdLookUpValueDetails)i.next();
						
						AdLookUpValuesList adLvList = new AdLookUpValuesList(actionForm,
								lvDetails.getLvCode(),
								lvDetails.getLvName(), 
								lvDetails.getLvDescription(),
								Common.convertIntegerToString(lvDetails.getLvMasterCode()),
								Common.convertByteToBoolean(lvDetails.getLvEnable()));  
						
						actionForm.saveAdLVList(adLvList);
						
					}
					
				} catch (AdLUNoLookUpFoundException ex) {
					
					actionForm.clearAdLVList();
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("lookUpValues.error.noLookUpFound"));
					
				} catch (AdLVNoLookUpValueFoundException ex) {
					
					actionForm.clearAdLVList();
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in AdLookUpValuesAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}  	
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				}
				
				return(mapping.findForward("adLookUpValues"));
								
/*******************************************************
   -- Ad LV Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("adLookUpValues");
					
				}
				
				actionForm.reset(mapping, request);
				
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
					
					actionForm.clearAdLVList();
					
					actionForm.clearLookUpNameList();
					
					list = ejbLV.getAdLuAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setLookUpNameList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setLookUpNameList((String)i.next());
							
						}
						
					}
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in AdLookUpValuesAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}  
					
				}  
				
				if (actionForm.getLookUpName() != null) {   
					
					try {
						
						ArrayList lvList = ejbLV.getAdLvByLuName(actionForm.getLookUpName(), user.getCmpCode());
						
						actionForm.clearAdLVList();
						
						Iterator j = lvList.iterator();
						
						while (j.hasNext()) {
							
							AdLookUpValueDetails lvDetails = (AdLookUpValueDetails)j.next();
							
							AdLookUpValuesList adLvList = new AdLookUpValuesList(actionForm,
									lvDetails.getLvCode(),
									lvDetails.getLvName(), 
									lvDetails.getLvDescription(),
									Common.convertIntegerToString(lvDetails.getLvMasterCode()),
									Common.convertByteToBoolean(lvDetails.getLvEnable()));  
							
							actionForm.saveAdLVList(adLvList);
							
						}
						
					} catch (AdLUNoLookUpFoundException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("lookUpValues.error.noLookUpFound"));
						
					} catch (AdLVNoLookUpValueFoundException ex) {

						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in AdLookUpValuesAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}  	
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if (request.getParameter("saveButton") != null || 
							request.getParameter("updateButton") != null) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
				}
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                                  
				
				actionForm.setPageState(Constants.PAGE_STATE_SAVE);
				actionForm.setEnable(true);
				
				return(mapping.findForward("adLookUpValues"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in AdLookUpValuesAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
	
}
