package com.struts.ad.lookupvalue;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class AdLookUpValuesForm extends ActionForm implements Serializable {
	
	private Integer lookUpValueCode = null;
	private String lookUpName = null;
	private ArrayList lookUpNameList = new ArrayList();
	
	private String lookUpValueName = null;	
	private String description = null;
	private String masterCode = null;
	private boolean enable = false;
	private boolean isLookUpNameEntered = false;
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList adLVList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public AdLookUpValuesList getAdLVByIndex(int index) {
		
		return((AdLookUpValuesList)adLVList.get(index));
		
	}
	
	public Object[] getAdLVList() {
		
		return(adLVList.toArray());
		
	}
	
	public int getAdLVListSize() {
		
		return(adLVList.size());
		
	}
	
	public void saveAdLVList(Object newAdLVList) { 
		
		adLVList.add(newAdLVList);
		
	}
	
	public void clearAdLVList() { 
		
		adLVList.clear();
		
	}
	
	public void setRowSelected(Object selectedAdLVList, boolean isEdit) {
		
		this.rowSelected = adLVList.indexOf(selectedAdLVList);
		
		if(isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void showAdLVRow(int rowSelected) {
		
		this.lookUpValueCode = ((AdLookUpValuesList)adLVList.get(rowSelected)).getLookUpValueCode();   		
		this.lookUpValueName = ((AdLookUpValuesList)adLVList.get(rowSelected)).getLookUpValueName();
		this.description = ((AdLookUpValuesList)adLVList.get(rowSelected)).getDescription();
		this.masterCode = ((AdLookUpValuesList)adLVList.get(rowSelected)).getMasterCode();
		this.enable = ((AdLookUpValuesList)adLVList.get(rowSelected)).getEnable();
		
	}
	
	public void updateAdLVRow(int rowSelected, Object newAdLVList) {
		
		adLVList.set(rowSelected, newAdLVList);
		
	}
	
	public void deleteAdLVList(int rowSelected) {
		
		adLVList.remove(rowSelected);
		
	}
	
	public void setUpdateButton(String updateButton) {
		
		this.updateButton = updateButton;
		
	}
	
	public void setCancelButton(String cancelButton) {
		
		this.cancelButton = cancelButton;
		
	}
	
	public void setSaveButton(String saveButton) {
		
		this.saveButton = saveButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}	
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return(pageState);
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getLookUpValueCode() {
		
		return lookUpValueCode;
		
	}
	
	public void setLookUpValueCode(Integer lookUpValueCode) {
		
		this.lookUpValueCode = lookUpValueCode;
		
	}
	
	public String getLookUpName() {
		
		return lookUpName;
		
	}
	
	public void setLookUpName(String lookUpName) {
		
		this.lookUpName = lookUpName;
		
	}
	
	public ArrayList getLookUpNameList() {
		
		return(lookUpNameList);
		
	}
	
	public void setLookUpNameList(String lookUpName) {
		
		lookUpNameList.add(lookUpName);
		
	}
	
	public void clearLookUpNameList() {
		
		lookUpNameList.clear();
		lookUpNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getLookUpValueName() {
		
		return lookUpValueName;
		
	}
	
	public void setLookUpValueName(String lookUpValueName){
		this.lookUpValueName = lookUpValueName;
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) { 
		
		this.description = description;
		
	}
	
	public String getMasterCode() {
		
		return masterCode;
		
	}
	
	public void setMasterCode(String masterCode) {
		
		this.masterCode = masterCode;
		
	}
	
	public boolean getEnable() {
		
		return enable;
		
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		
	}
	
	public boolean getIsLookUpNameEntered() {
		
		return isLookUpNameEntered;
		
	}
	
	public void setIsLookUpNameEntered(boolean isLookUpNameEntered) {
		
		this.isLookUpNameEntered = isLookUpNameEntered;
		
	}	
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}         
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		lookUpValueName = null;
		description = null;
		masterCode = null;
		enable = false;
		saveButton = null;
		closeButton = null;
		updateButton = null;
		cancelButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;	   
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
			
			if(Common.validateRequired(lookUpName) || 
					lookUpName.equals(Constants.GLOBAL_NO_RECORD_FOUND) ) {
				
				errors.add("lookUpName", 
						new ActionMessage("lookUpValues.error.lookUpNameRequired"));
				
			}
			
		}
		
		if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
			
			if(Common.validateRequired(lookUpValueName))  {
				
				errors.add("lookUpValueName", 
						new ActionMessage("lookUpValues.error.lookUpValueNameRequired"));
				
			}
			
		}
		
		return(errors);
	}
	
}
