package com.struts.ad.lookupvalue;

import java.io.Serializable;

public class AdLookUpValuesList implements Serializable{
	
	private Integer lookUpValueCode = null;
	private String lookUpValueName = null;	
	private String description = null;
	private String masterCode = null;
	private boolean enable = false;
	
	private String editButton = null;
	private String deleteButton = null;
	
	private AdLookUpValuesForm parentBean;
	
	public AdLookUpValuesList(AdLookUpValuesForm parentBean, 
			Integer lookUpValueCode,
			String lookUpValueName, 
			String description, 
			String masterCode, 
			boolean enable){
		
		this.parentBean = parentBean;
		this.lookUpValueCode = lookUpValueCode;
		this.lookUpValueName = lookUpValueName;
		this.description = description;
		this.masterCode = masterCode;
		this.enable = enable;
		
	}
	
	public void setParentBean(AdLookUpValuesForm parentBean) {
		
		this.parentBean = parentBean;
		
	}
	
	public void setEditButton(String editButton) {
		
		parentBean.setRowSelected(this, true);
		
	}
	
	public void setDeleteButton(String deleteButton) {

	    parentBean.setRowSelected(this, true);

	}
	
	public Integer getLookUpValueCode() {
		
		return(lookUpValueCode);
		
	}
	
	public String getLookUpValueName() {
		
		return(lookUpValueName);
		
	}
	
	public String getDescription() {
		
		return(description);
		
	}
	
	public String getMasterCode() {
		
		return(masterCode);
		
	}
	
	public boolean getEnable() {
		
		return(enable);
		
	}	
	
}
