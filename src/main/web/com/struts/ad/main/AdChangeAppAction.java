package com.struts.ad.main;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Constants;
import com.struts.util.User;

public final class AdChangeAppAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping, ActionForm form,
		HttpServletRequest request, HttpServletResponse response) 
		throws IOException, ServletException {
	   
	    HttpSession session = request.getSession(); 
	    
        try { 
        
	      int appCode = Integer.parseInt(request.getParameter("appCode"));
	      User user = (User)session.getAttribute(Constants.USER_KEY);
	      
	      if(user != null) {
	      	
	         if(log.isInfoEnabled()){
	         	
			    log.info("AdChangeAppAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
			                "' performed this action on session " + session.getId());
			                         
			 }
			 			 
	      } else {
	      	
	         if (log.isInfoEnabled())
	            log.info("User is not logged on in session " + session.getId());
	         return mapping.findForward("adLogon");
	         
	      }
	      	      	      
	      ActionErrors errors = new ActionErrors();
	      // Check if selected application exists.
	      if(!(appCode >= 1 && appCode <= 8)) {
	      	
	      	 errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("changeApp.error.userAppNotExisting"));	     
	      	 saveErrors(request, new ActionMessages(errors));
             return(mapping.findForward("cmnMain"));
	         
	         
	      }
   
	      user.setCurrentAppCode(appCode);
	      session.setAttribute(Constants.USER_KEY, user);
	    
	      return(mapping.findForward("cmnMain"));
	      
	  }catch(Exception e){
	     if(log.isInfoEnabled())
	        log.info("Exception caught in ChangeResAction.execute(): " + e.getMessage() +
		   " session: " + session.getId());
             return(mapping.findForward("cmnErrorPage"));
	  }

	}	

}
