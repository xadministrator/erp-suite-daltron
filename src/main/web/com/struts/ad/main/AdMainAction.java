package com.struts.ad.main;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.txn.AdMainNotificationController;
import com.ejb.txn.AdMainNotificationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;

public final class AdMainAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("AdMainAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }

         AdMainForm actionForm = (AdMainForm)form;

/*******************************************************
   Initialize AdMainNotificationController EJB
*******************************************************/

         AdMainNotificationControllerHome homeMN = null;
         AdMainNotificationController ejbMN = null;

         try{
            
            homeMN = (AdMainNotificationControllerHome)com.util.EJBHomeFactory.
	              lookUpHome("ejb/AdMainNotificationControllerEJB", AdMainNotificationControllerHome.class);
	              
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in AdMainAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbMN = homeMN.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in AdMainAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         
         ActionMessages messages = new ActionMessages();
			ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call ArInvoiceEntryController EJB
     getGlFcPrecisionUnit
     getAdPrfArInvoiceLineNumber
     getAdPrfEnableArInvoiceBatch
     getInvGpQuantityPrecisionUnit
     getAdPrfEnableInvShift
     getAdPrfArUseCustomerPulldown
     getAdPrfArEnablePaymentTerm
     getAdPrfArDisableSalesPrice
  *******************************************************/
         
         
         boolean enableInvoiceInterestGeneration = false;
         
         try {
        	 
        	 enableInvoiceInterestGeneration = Common.convertByteToBoolean(ejbMN.getAdPrfEnableArInvoiceInterestGeneration((user.getCmpCode())));
   
        	 System.out.println("enableInvoiceInterestGeneration="+enableInvoiceInterestGeneration);
             actionForm.setShowGenerateInterest(enableInvoiceInterestGeneration);
        	 //ejbMN.getAdPrfEnableArInvoiceInterestGeneration
         
         } catch(EJBException ex) {
             
             if (log.isInfoEnabled()) {
                 
                 log.info("EJBException caught in AdMainAction.execute(): " + ex.getMessage() +
                         " session: " + session.getId());
             }
             
             return(mapping.findForward("cmnErrorPage"));
             
         }
         
         
         
         
 
        	
        		
		 try {
		 	
		 	 // set user description
		 	 
		 	 actionForm.setUserDescription(user.getUserDescription() + "! ");
		 	 
		 	 // set today
		 	 
		 	 GregorianCalendar gc = new GregorianCalendar();		 	 		 	 
		 	 SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
		 	 String day = null;
             
             if (gc.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
			    day = "Sunday";
		     else if (gc.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			    day = "Monday";
			 else if (gc.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY)
			    day = "Tuesday";
			 else if (gc.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY)
			    day = "Wednesday";
			 else if (gc.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
			    day = "Thursday";
			 else if (gc.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY)
			    day = "Friday";
			 else if (gc.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
			    day = "Saturday";
			   
             
             actionForm.setToday(sdf.format(gc.getTime()) + " " + day + ".");
             
		 	 int num = 0; 
		 	 
		 	 if (user.getCurrentAppCode() == 1) {
		 	 	
			 	 // get number of journals to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("GL JOURNAL", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setJournalsToApprove(String.valueOf(num));
			     
			     // get number of journals in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("GL JOURNAL", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setJournalsInDraft(String.valueOf(num));
			     
			     // get number of recurring journals to generate 
			     
			     num = ejbMN.executeGlRjbUserNotification(new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setRecurringJournalsToGenerate(String.valueOf(num));
			     
			     // get number of journals for reversal
			     
			     num = ejbMN.executeGlJrForReversal(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setJournalsForReversal(String.valueOf(num));			     
			     
		     
		     } else if (user.getCurrentAppCode() == 2) {
		    	 
		    	 System.out.println("user.getCurrentAppCode()="+user.getCurrentAppCode());
		    	 System.out.println("request.getParameter(generateLoan)="+request.getParameter("generateLoan"));
		    	 /*******************************************************
		    	 -- Generate Loan Interest
		    	 *******************************************************/

		    			if (request.getParameter("generateLoan") != null) {
		    				
		    				try{
		    					
		    					int generateLoan = ejbMN.generateLoan(user.getUserName(), user.getCurrentBranch().getBrCode(), user.getCmpCode());
		    			
		    					System.out.println("generateLoan="+generateLoan);
		    					
		    					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.messages", "Successfully generated " + generateLoan + " invoices"));
		    			
		    					saveMessages(request, messages); 
		    					
		    				}catch(GlJREffectiveDatePeriodClosedException gx){
		    					
		    					errors.add(ActionMessages.GLOBAL_MESSAGE,
		    							new ActionMessage("standardMemoLine.error.datePeriodNotOpen"));
		    					
		    					
		    				}catch (Exception ex) {
		    					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.errorInfo", "Error Generating Loan"));
		    					
		    				}
		    			}	
		    			
		     	
		     	 // get number of pdc invoices to generate
		     	
		     	 num = ejbMN.executeArPdcNumberOfInvoicesToGenerate(new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPdcInvoicesToGenerate(String.valueOf(num));
			     
			     // get number of invoices to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AR INVOICE", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvoicesToApprove(String.valueOf(num));
			     
			     
			     
			     // get number of invoice to generate interest
			 	 
			     num = ejbMN.executeArOverDueInvoices(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setOverDueInvoices(String.valueOf(num));

			     // get number of past due invoice to generate penalty
			 	 
			     num = ejbMN.executeArPastDueInvoices(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPastDueInvoices(String.valueOf(num));
			     
			     // get number of accrued interest IS
			 	 
			     num = ejbMN.executeArGenerateAccruedInterestIS(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setAccruedInterestIS(String.valueOf(num));
			     
			     // get number of accrued interest TB
			 	 
			     num = ejbMN.executeArGenerateAccruedInterestTB(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setAccruedInterestTB(String.valueOf(num));

			     
			     // get number of invoices for posting ITEM
			     
			     num = ejbMN.executeArInvoiceForPosting("ITEMS", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvoicesForPostingItem(String.valueOf(num));
			     
			     // get number of invoices for posting MEMO LINES
			     
			     num = ejbMN.executeArInvoiceForPosting("MEMO LINES", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvoicesForPostingMemoLines(String.valueOf(num));
			     
			     
			     // get number of sales order in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AR SALES ORDER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setOrdersInDraft(String.valueOf(num));
			     
			     //number of sales order status delivery in draft
			     
			     num = ejbMN.executeSalesOrderTransactionStatus("READY TO DELIVER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setSalesOrderDeliveryStatus(String.valueOf(num));
			     
			     
			     // get number of sales order approve today
			     
			     num = ejbMN.executeAdDocumentInDraft("AR SALES ORDER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setOrdersInDraft(String.valueOf(num));
			     
			     
			     
			     // get number of job order approve today
			     
			     num = ejbMN.executeAdDocumentInDraft("AR JOB ORDER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setJobOrdersInDraft(String.valueOf(num));
			     
			     // get number of sales orders for processing
			     
			     num = ejbMN.executeArSalesOrdersForProcessing(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());;
			     actionForm.setSalesOrdersForProcessing(String.valueOf(num));
			     
			     // get number of job orders for processing
			     
			     num = ejbMN.executeArJobOrdersForProcessing(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());;
			     actionForm.setJobOrdersForProcessing(String.valueOf(num));
			     
			     // get number of invoices in draft ITEM
			     
			     num = ejbMN.executeAdDocumentInDraft("AR INVOICE ITEMS", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvoicesInDraftItem(String.valueOf(num));
			     
			     // get number of invoices in draft MEMO LINES
			     
			     num = ejbMN.executeAdDocumentInDraft("AR INVOICE MEMO LINES", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvoicesInDraftMemoLines(String.valueOf(num));
			     
			     // get number of invoices in draft SO MATCHED
			     
			     num = ejbMN.executeAdDocumentInDraft("AR INVOICE SO MATCHED", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvoicesInDraftSoMatched(String.valueOf(num));
			     
		     	// get number of invoices in draft JO MATCHED
			     
			     num = ejbMN.executeAdDocumentInDraft("AR INVOICE JO MATCHED", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvoicesInDraftJoMatched(String.valueOf(num));

			     // get number of credit memos to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AR CREDIT MEMO", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCreditMemosToApprove(String.valueOf(num));
			     
			     // get number of credit memos to posting
			     
			     num = ejbMN.executeArInvoiceCreditMemoForPosting(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCreditMemosForPosting(String.valueOf(num));
			     
			     
			     // get number of credit memos in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AR CREDIT MEMO", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCreditMemosInDraft(String.valueOf(num));
			     
			     // get number of credit memos in draft ITEM
			     
			     num = ejbMN.executeAdDocumentInDraft("AR CREDIT MEMO ITEMS", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCreditMemosInDraftItems(String.valueOf(num));
			     
			     // get number of credit memos in draft MEMO LINES
			     
			     num = ejbMN.executeAdDocumentInDraft("AR CREDIT MEMO MEMO LINES", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCreditMemosInDraftMemoLines(String.valueOf(num));
			     System.out.println("AR CREDIT MEMO MEMO LINES" + num);
			     // get number of receipts to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AR RECEIPT", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setReceiptsToApprove(String.valueOf(num));
			     
			     // get number of receipts collection in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AR RECEIPT COLLECTION", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setReceiptsCollectionInDraft(String.valueOf(num));
			     
			     // get number of receipts MISC in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AR RECEIPT MISC", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setReceiptsMiscInDraft(String.valueOf(num));
			     
			     
			     // get number of receipt collecton for posting
			     
			     num = ejbMN.executeArReceiptForPosting("COLLECTION", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setReceiptsCollectionForPosting(String.valueOf(num));
			     
			     
			     // get number of receipt misc for posting
			     
			     num = ejbMN.executeArReceiptForPosting("MISC", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setReceiptsMiscForPosting(String.valueOf(num));
			     
			     
			     
			     // get number of customers to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AR CUSTOMER", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCustomerToApprove(String.valueOf(num));
			     
			     
			     //number of customer rejected
			     
			     num = ejbMN.executeArCustomerRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setCustomerRejected(String.valueOf(num));
			     
			     
			     //number of Investors Monthly Bonus and Interest
			     
			     num = ejbMN.executeArInvestorBonusAndInterest(user.getCurrentBranch().getBrCode(), user.getCmpCode());
			     actionForm.setInvestorBonusAndInterest(String.valueOf(num));
			     
			     
			   //  num = ejbMN.executeArInvestorBonus(user.getCurrentBranch().getBrCode(), user.getCmpCode());
			   //  actionForm.setInvestorBonus(String.valueOf(num));
			     
			  //   num = ejbMN.executeArInvestorInterest(user.getCurrentBranch().getBrCode(), user.getCmpCode());
			  //   actionForm.setInvestorInterest(String.valueOf(num));
			     
			     
			     
			     
		     
		     } else if (user.getCurrentAppCode() == 3) {
		     			     			     
			     // get number of check for Loan Interest
		    	 
		    	 num = ejbMN.executeApGenerateLoan(user.getCurrentBranch().getBrCode(), user.getCmpCode());
			     actionForm.setGenerateLoan(String.valueOf(num));
			     
			 	 
			     num = ejbMN.executeApRvNumberOfVouchersToGenerate(new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     System.out.println("executeApRvNumberOfVouchersToGenerate -->"+num);
			     actionForm.setRecurringVouchersToGenerate(String.valueOf(num));
			     
			     // get number of check payment requests in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP CHECK PAYMENT REQUEST", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPaymentRequestInDraft(String.valueOf(num));
			     
			     // get number of check payment requests to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP CHECK PAYMENT REQUEST", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCheckPaymentRequestsToApprove(String.valueOf(num));
			     
			     //number of check payment request rejected
			     
			     num = ejbMN.executeApPaymentRequestRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setPaymentRequestRejected(String.valueOf(num));
			     
			      // get number of vouchers to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP VOUCHER", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setVouchersToApprove(String.valueOf(num));
			     
			     // get number of vouchers in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP VOUCHER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setVouchersInDraft(String.valueOf(num));
			     
			     
			     // get number of vouchers for processing
			     
			     num = ejbMN.executeApVoucherForProcessing(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setVouchersForProcessing(String.valueOf(num));
			     
			     //number of vouchers rejected
			     
			     num = ejbMN.executeApVoucherRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setVouchersRejected(String.valueOf(num));
			     
			     // get number of debit memos to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP DEBIT MEMO", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setDebitMemosToApprove(String.valueOf(num));
			     
			     // get number of debit memos in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP DEBIT MEMO", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setDebitMemosInDraft(String.valueOf(num));
			     
			     // get number of checks to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP CHECK", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setChecksToApprove(String.valueOf(num));
			     
			     // get number of checks in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP CHECK", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setChecksInDraft(String.valueOf(num));
			     
			     //number of checks rejected
			     
			     num = ejbMN.executeApChecksRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setChecksRejected(String.valueOf(num));
			     
			     // get number of direct checks in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP DIRECT CHECK", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setDirectChecksInDraft(String.valueOf(num));
			     
			     
			     
			     // get number of checks for printing
			     
			     num = ejbMN.executeChecksForPrinting(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());;
			     actionForm.setChecksForPrinting(String.valueOf(num));
			     
			     // get number of receiving item to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP RECEIVING ITEM", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setReceivingItemsToApprove(String.valueOf(num));
			     
			     // get number of receiving items in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP RECEIVING ITEM", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setReceivingItemsInDraft(String.valueOf(num));
			     
			     // get number of purchase orders to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP PURCHASE ORDER", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPurchaseOrdersToApprove(String.valueOf(num));

			     // get number of purchase orders in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP PURCHASE ORDER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPurchaseOrdersInDraft(String.valueOf(num));
			     
			     //number of purchase order rejected
			     
			     num = ejbMN.executeApPurchaseOrderRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setPurchaseOrdersRejected(String.valueOf(num));
			     
			     //number of purchase order receiving rejected
			     
			     num = ejbMN.executeApPurchaseOrderReceivingRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setReceivingItemsRejected(String.valueOf(num));
			     
			     // get number of purchase orders generations
			     
			     num = ejbMN.executeApPurchaseOrdersForGeneration(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());   
			     actionForm.setPurchaseOrdersForGeneration(String.valueOf(num));

			     // get number of purchase orders for printing
			     
			     num = ejbMN.executeApPurchaseOrdersForPrinting(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());   
			     actionForm.setPurchaseOrdersForPrinting(String.valueOf(num));

			     
			     // get number of recurring vouchers to generate
			 	 
			     num = ejbMN.executeApPrNumberOfPurchaseRequisitionsToGenerate(new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     System.out.println("executeApPrNumberOfPurchaseRequisitionsToGenerate ------------------>"+num);
			     actionForm.setRecurringPurchaseRequisitionsToGenerate(String.valueOf(num));
			     
			     
			     // get number of purchase requisitions to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP PURCHASE REQUISITION", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPurchaseRequisitionsToApprove(String.valueOf(num));
			     
			     // get number of purchase requisitions in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP PURCHASE REQUISITION", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPurchaseRequisitionsInDraft(String.valueOf(num));
			     
			     // get number of purchase requisitions for Canvass
			     
			     num = ejbMN.executeApPurchaseReqForCanvass(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPurchaseRequisitionsForCanvass(String.valueOf(num));

			     //number of purchase requisitions rejected
			     
			     num = ejbMN.executeApPurchaseReqRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setPurchaseRequisitionsRejected(String.valueOf(num));

			     //number of purchase requisition canvass rejected
			     
			     num = ejbMN.executeApCanvassRejected(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setCanvassRejected(String.valueOf(num));
			     
				 // get number of purchase requisitions to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("AP CANVASS", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCanvassToApprove(String.valueOf(num));
			     
			     // get number of purchase requisitions in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("AP CANVASS", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setCanvassInDraft(String.valueOf(num));
			     
			     
			     num = ejbMN.executeApPaymentRequestDraft(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPaymentRequestDraft(String.valueOf(num));
			     
			     num = ejbMN.executeApPaymentRequestGeneration(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPaymentRequestGeneration(String.valueOf(num));
			     
			     num = ejbMN.executeApPaymentRequestProcessing(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setPaymentRequestProcessing(String.valueOf(num));
			     
			     num = ejbMN.executeApPaymentsForProcessing(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());;
			     actionForm.setPaymentsForProcessing(String.valueOf(num));
			     
		     	
		     } else if (user.getCurrentAppCode() == 5) {		     			     			     
			     
			      // get number of bank adjustments to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("CM ADJUSTMENT", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setBankAdjustmentsToApprove(String.valueOf(num));
			     
			     // get number of adjustments in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("CM ADJUSTMENT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setBankAdjustmentsInDraft(String.valueOf(num));
			     
			     // get number of fund transfers to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("CM FUND TRANSFER", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setFundTransfersToApprove(String.valueOf(num));
			     
			     // get number of fund transfers in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("CM FUND TRANSFER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setFundTransfersInDraft(String.valueOf(num));
			     
			     num = ejbMN.executeApChecksForRelease(new Integer(user.getCurrentBranch().getBrCode()), user.getUserName(), user.getCmpCode());
			     actionForm.setChecksForRelease(String.valueOf(num));
			     

		     } else if (user.getCurrentAppCode() == 6) {		     			     			     
			     
			      // get number of inventory adjustments to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("INV ADJUSTMENT", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvAdjustmentsToApprove(String.valueOf(num));

			     // get number of inventory adjustments in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("INV ADJUSTMENT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInventoryAdjustmentsInDraft(String.valueOf(num));

			     // get number of inv branch stock transfer in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("INV BRANCH STOCK TRANSFER ORDER DRAFT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvBranchStockTransferOrderInDraft(String.valueOf(num));

			     // get number of inv branch stock transfer orders unserved
			     
			     num = ejbMN.executeInvBSTOrdersUnserved(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());//ejbMN.executeAdDocumentInDraft("INV BRANCH STOCK TRANSFER ORDER DRAFT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvBranchStockTransferUnservedOrders(String.valueOf(num));

			     // get number of inv branch stock transfer out in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("INV BRANCH STOCK TRANSFER OUT DRAFT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvBranchStockTransferOutInDraft(String.valueOf(num));

			     // get number of inv branch stock transfer out incoming
			     
			     num = ejbMN.executeAdDocumentInDraft("INV BRANCH STOCK TRANSFER OUT INCOMING", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvBranchStockTransferOutIncoming(String.valueOf(num));

			     // get number of inv branch stock transfer in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("INV BRANCH STOCK TRANSFER IN DRAFT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvBranchStockTransferInDraft(String.valueOf(num));
			     
			     // get number of inventory build/unbuild assembly to approve
			 	 
			     num = ejbMN.executeAdDocumentToApprove("INV BUILD ASSEMBLY", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvBuildUnbuildAssemblyToApprove(String.valueOf(num));
			     
			     // get number of inventory build/unbuild assembly in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("INV BUILD ASSEMBLY DRAFT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setBuildUnbuildAssemblyInDraft(String.valueOf(num));
			     
			     // get number of inventory build/unbuild assembly in draft
			     
			     num = ejbMN.executeAdDocumentInDraft("INV BUILD ASSEMBLY ORDER DRAFT", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setBuildUnbuildAssemblyOrderInDraft(String.valueOf(num));
			     
			     
			     // get number of inventory build/unbuild assembly draft will due on 2 weeks 
			     
			     num = ejbMN.executeBuildUnbuildAssembliesOrderInDue( new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setBuildUnbuildAssemblyOrderInDue(String.valueOf(num));
			
                             
                             // get number of inventory stock transfers to approve
			    
			     num = ejbMN.executeInvStockTransferUnposted(user.getCmpCode());
			     actionForm.setInvStockTransferDraft(String.valueOf(num));
                             
                             
			     // get number of inventory stock transfers to approve
			     
			     num = ejbMN.executeAdDocumentToApprove("INV STOCK TRANSFER", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvStockTransferToApprove(String.valueOf(num));
			     
		     	// get number of inventory branch stock transfer orders to approve
			     
			     num = ejbMN.executeAdDocumentToApprove("INV BRANCH STOCK TRANSFER ORDER", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			     actionForm.setInvBranchStockTransferOrderToApprove(String.valueOf(num));
			     
		     	// get number of inventory adjustment request to approve
			     
			     num = ejbMN.executeAdDocumentToApprove("INV ADJUSTMENT REQUEST", new Integer(user.getUserCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());;
			     actionForm.setInvAdjustmentRequestToApprove(String.valueOf(num));
			     
		     	// get number of inventory adjustment request to approve
			     
			     num = ejbMN.executeInvAdjustmentRequestToGenerate(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());;
			     actionForm.setInvAdjustmentRequestToGenerate(String.valueOf(num));
			 
		     } else if (user.getCurrentAppCode() == 7) {	
		    	 
		     } else if (user.getCurrentAppCode() == 8) {	
		     }
		     
		 }catch(EJBException ex){
	            if(log.isInfoEnabled()){
		       log.info("EJBException caught in AdMainAction.execute(): " + ex.getMessage() +
		       " session: " + session.getId());
		    }
		    return(mapping.findForward("cmnErrorPage"));
		 }		 
		 
		 return(mapping.findForward("adMain"));

      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()){
             log.info("Exception caught in AdMainAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
