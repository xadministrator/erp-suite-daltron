package com.struts.ad.main;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


public class AdMainForm extends ActionForm implements Serializable {

   private String userDescription = null;
   private String today = null;
   private String journalsToApprove = null;
   private String recurringJournalsToGenerate = null;
   private String journalsForReversal = null;
   private String recurringVouchersToGenerate = null;
   private String invoicesToApprove = null;
   private String overDueInvoices = null;
   private String accruedInterestIS = null;
   private String accruedInterestTB = null;
   private String investorBonusAndInterest = null;
   private String salesOrderDeliveryStatus = null;
   private String investorBonus = null;
   private String investorInterest = null;
   
   private String generateLoan = null;
   
   
   private String pastDueInvoices = null;
   
   private String checkPaymentRequestsToApprove = null;
   private String checkPaymentRequestsToGenerate = null;
   private String creditMemosToApprove = null;
   private String creditMemosForPosting = null;
   private String receiptsToApprove = null;
   private String receiptsCollectionForPosting = null;
   private String receiptsMiscForPosting = null;
   private String paymentRequestInDraft = null;
   private String paymentRequestRejected = null;
   private String vouchersToApprove = null;
   private String vouchersRejected = null;
   private String vouchersForProcessing = null;
   private String debitMemosToApprove = null;
   
   private String bankAdjustmentsToApprove = null;
   private String fundTransfersToApprove = null;
   private String invAdjustmentsToApprove = null;
   private String invBuildUnbuildAssemblyToApprove = null;
   private String journalsInDraft = null;
   private String vouchersInDraft = null;
   private String debitMemosInDraft = null;
   
   private String checksInDraft = null;
   private String checksToApprove = null;
   private String checksRejected = null;
   
   private String directChecksInDraft = null;
   private String directChecksToApprove = null;
   private String directChecksRejected = null;
   
   private String invoicesInDraft = null;
   private String ordersInDraft = null;
   private String jobOrdersInDraft = null;
   private String invoicesInDraftItem = null;
   private String invoicesInDraftMemoLines = null;
   private String invoicesInDraftSoMatched = null;
   private String invoicesInDraftJoMatched = null;
   private String invoicesForPosting = null;
   private String invoicesForPostingItem = null;
   private String invoicesForPostingMemoLines = null;
   private String creditMemosInDraft = null;
   private String creditMemosInDraftItems = null;
   private String creditMemosInDraftMemoLines = null;
   
   private String receiptsCollectionInDraft = null;
   private String receiptsMiscInDraft = null;
   private String bankAdjustmentsInDraft = null;
   private String fundTransfersInDraft = null;
   private String inventoryAdjustmentsInDraft = null;
   private String buildUnbuildAssemblyInDraft = null;
   private String buildUnbuildAssemblyOrderInDraft = null;
   private String buildUnbuildAssemblyOrderInDue = null;
   private String pdcInvoicesToGenerate = null;
   private String invStockTransferDraft = null;
   private String invStockTransferToApprove = null;
   private String invBranchStockTransferOrderToApprove = null;
   private String purchaseOrdersInDraft = null;
   private String receivingItemsInDraft = null;
   private String purchaseOrdersToApprove = null;
   private String purchaseOrdersRejected = null;
   private String receivingItemsToApprove = null;
   private String receivingItemsRejected = null;
   private String purchaseOrdersForGeneration = null;
   private String purchaseOrdersForPrinting = null;
   private String recurringPurchaseRequisitionsToGenerate = null;
   private String purchaseRequisitionsInDraft = null;
   private String purchaseRequisitionsToApprove = null;
   private String purchaseRequisitionsForCanvass = null;
   private String purchaseRequisitionsRejected = null;
   private String invBranchStockTransferOrderInDraft = null;
   private String invBranchStockTransferUnservedOrders = null;
   private String invBranchStockTransferOutInDraft = null;
   private String invBranchStockTransferOutIncoming = null;
   private String invBranchStockTransferInDraft = null;
   private String invAdjustmentRequestToApprove = null;
   private String invAdjustmentRequestToGenerate = null;
   
   private String canvassInDraft = null;
   private String canvassToApprove = null;
   private String canvassRejected = null;
   private String salesOrdersToApprove = null;
   private String jobOrdersToApprove = null;
   private String customerToApprove = null;
   private String customerRejected = null;
   private String salesOrdersForProcessing = null;
   private String jobOrdersForProcessing = null;
   private String salesOrdersApproveToday = null;
   private String jobOrdersApproveToday = null;
   

   private String checksForRelease = null;
   private String checksForPrinting = null;
   private String paymentRequestDraft = null;
   private String paymentRequestGeneration = null;
   private String paymentRequestProcessing = null;
   private String paymentsForProcessing = null;
   
   private boolean showGenerateInterest = false;
   
   private String userPermission = new String();
   
   
   
   public String getJobOrdersToApprove() {
	return jobOrdersToApprove;
}

public void setJobOrdersToApprove(String jobOrdersToApprove) {
	this.jobOrdersToApprove = jobOrdersToApprove;
}

public String getJobOrdersInDraft() {
	return jobOrdersInDraft;
}

	public String getInvoicesInDraftJoMatched() {
		return invoicesInDraftJoMatched;
	}
	
	public String getJobOrdersForProcessing() {
		return jobOrdersForProcessing;
	}
	
	public String getJobOrdersApproveToday() {
		return jobOrdersApproveToday;
	}
	
	public void setJobOrdersInDraft(String jobOrdersInDraft) {
		this.jobOrdersInDraft = jobOrdersInDraft;
	}
	
	public void setInvoicesInDraftJoMatched(String invoicesInDraftJoMatched) {
		this.invoicesInDraftJoMatched = invoicesInDraftJoMatched;
	}
	
	public void setJobOrdersForProcessing(String jobOrdersForProcessing) {
		this.jobOrdersForProcessing = jobOrdersForProcessing;
	}
	
	public void setJobOrdersApproveToday(String jobOrdersApproveToday) {
		this.jobOrdersApproveToday = jobOrdersApproveToday;
	}

	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
   
   public boolean getShowGenerateInterest() {
		
		return showGenerateInterest;
		
	}
	
	public void setShowGenerateInterest(boolean showGenerateInterest) {
		
		this.showGenerateInterest = showGenerateInterest;
		
	}
   
   public String getUserDescription() {
   	
   	  return userDescription;
   	
   }
   
   public void setUserDescription(String userDescription) {
   	
   	  this.userDescription = userDescription;
   	
   }
   
   public String getToday() {
   	
   	   return today;
   	
   }
   
   public void setToday(String today) {
   	
   	   this.today = today;
   	
   }

   public String getJournalsToApprove(){
      return(journalsToApprove);
   }

   public void setJournalsToApprove(String journalsToApprove){
      this.journalsToApprove = journalsToApprove;
   }

   public String getRecurringJournalsToGenerate(){
      return(recurringJournalsToGenerate);
   }

   public void setRecurringJournalsToGenerate(String recurringJournalsToGenerate){
      this.recurringJournalsToGenerate = recurringJournalsToGenerate;
   }
   
   public String getJournalsForReversal() {
   	  return(journalsForReversal);
   }
   
   public void setJournalsForReversal(String journalsForReversal){
   	  this.journalsForReversal = journalsForReversal;
   }
   
   public String getRecurringVouchersToGenerate() {
   	
   	  return recurringVouchersToGenerate;
   	
   }
   
   public void setRecurringVouchersToGenerate(String recurringVouchersToGenerate) {
   	
   	  this.recurringVouchersToGenerate = recurringVouchersToGenerate;
   	
   }
   
   public String getInvoicesToApprove() {
   	
   	  return invoicesToApprove;
   	
   }
   
   public void setInvoicesToApprove(String invoicesToApprove) {
   	
   	  this.invoicesToApprove = invoicesToApprove;
   	
   }
   
   public String getOverDueInvoices() {
	   	
   	  return overDueInvoices;
   	
   }
   
   public void setOverDueInvoices(String overDueInvoices) {
   	
   	  this.overDueInvoices = overDueInvoices;
   	
   }
   
   public String getAccruedInterestIS() {
	   	
	   	  return accruedInterestIS;
	   	
	   }
	   
	   public void setAccruedInterestIS(String accruedInterestIS) {
	   	
	   	  this.accruedInterestIS = accruedInterestIS;
	   	
	   }
	   
	   public String getAccruedInterestTB() {
		   	
		   	  return accruedInterestTB;
		   	
		   }
		   
		   public void setAccruedInterestTB(String accruedInterestTB) {
		   	
		   	  this.accruedInterestTB = accruedInterestTB;
		   	
		   }
		   
		public String getInvestorBonusAndInterest(){
			return investorBonusAndInterest;
		}
		   
		
		public void setInvestorBonusAndInterest(String investorBonusAndInterest){
			this.investorBonusAndInterest = investorBonusAndInterest;
		}

		   
	   public String getInvestorBonus() {
		   	
	   	  return investorBonus;
	   	
	   }
		   
	   public void setInvestorBonus(String investorBonus) {
	   	
	   	  this.investorBonus = investorBonus;
	   	
	   }
	
		   
	   public String getInvestorInterest() {
		   	
	   	  return investorInterest;
	   	
	   }
			   
	   public void setInvestorInterest(String investorInterest) {
	   	
	   	  this.investorInterest = investorInterest;
	   	
	   }
	   
	   
	   
	   public String getGenerateLoan() {
		   	
		   	  return generateLoan;
		   	
		   }
				   
		   public void setGenerateLoan(String generateLoan) {
		   	
		   	  this.generateLoan = generateLoan;
		   	
		   }
   
   
   public String getPastDueInvoices() {
	   	
	   	  return pastDueInvoices;
	   	
	   }
	   
	   public void setPastDueInvoices(String pastDueInvoices) {
	   	
	   	  this.pastDueInvoices = pastDueInvoices;
	   	
	   }
   
   
   public String getCreditMemosToApprove() {
   	
   	  return creditMemosToApprove;
   	
   }
   
   public void setCreditMemosToApprove(String creditMemosToApprove) {
   	
   	  this.creditMemosToApprove = creditMemosToApprove;
   	
   }
   
   
   public String getCreditMemosForPosting() {
	   	
	  return creditMemosForPosting;
	   	
   }
   
	   
   public void setCreditMemosForPosting(String creditMemosForPosting) {
	   	
	   this.creditMemosForPosting = creditMemosForPosting;
	   	
   }
   
   public String getReceiptsToApprove() {
   	
   	  return receiptsToApprove;
   	
   }
   
   public void setReceiptsToApprove(String receiptsToApprove) {
   	
   	  this.receiptsToApprove = receiptsToApprove;
   	
   }
   
   
   public String getReceiptsCollectionForPosting() {
	   	
	  return receiptsCollectionForPosting;
	   	
   }
	   
   public void setReceiptsCollectionForPosting(String receiptsCollectionForPosting) {
	   	
	  this.receiptsCollectionForPosting = receiptsCollectionForPosting;
	   	
   }
   
   
   public String getReceiptsMiscForPosting() {
	   	
	 return receiptsMiscForPosting;
		   	
   }
		   
   public void setReceiptsMiscForPosting(String receiptsMiscForPosting) {
		   	
	   this.receiptsMiscForPosting = receiptsMiscForPosting;
		   	
   }
   
   
   
   
   
   public String getPaymentRequestInDraft() {
	   	
   	  return paymentRequestInDraft;
   	
   }
   
   public void setPaymentRequestInDraft(String paymentRequestInDraft) {
   	
   	  this.paymentRequestInDraft = paymentRequestInDraft;
   	
   }
   
   public String getPaymentRequestRejected() {
	   	
   	  return paymentRequestRejected;
   	
   }
   
   public void setPaymentRequestRejected(String paymentRequestRejected) {
   	
   	  this.paymentRequestRejected = paymentRequestRejected;
   	
   }
   
   public String getVouchersToApprove() {
   	
   	  return vouchersToApprove;
   	
   }
   
   public void setVouchersToApprove(String vouchersToApprove) {
   	
   	  this.vouchersToApprove = vouchersToApprove;
   	
   }
   
   
   public String getVouchersRejected() {
	   	
   	  return vouchersRejected;
   	
   }
   
   public void setVouchersRejected(String vouchersRejected) {
   	
   	  this.vouchersRejected = vouchersRejected;
   	
   }
   
   
   public String getVouchersForProcessing() {
	   	
	   	  return vouchersForProcessing;
	   	
   }
   
   public void setVouchersForProcessing(String vouchersForProcessing) {
   	
   	  this.vouchersForProcessing = vouchersForProcessing;
   	
   }
	   
	   
   public String getCheckPaymentRequestsToApprove() {
	   	
	   	  return checkPaymentRequestsToApprove;
	   	
   }
   
   public void setCheckPaymentRequestsToApprove(String checkPaymentRequestsToApprove) {
   	
   	  this.checkPaymentRequestsToApprove = checkPaymentRequestsToApprove;
   	
   }
   
   public String getCheckPaymentRequestsToGenerate() {
	   	
	   	  return checkPaymentRequestsToGenerate;
	   	
	}
	
	public void setCheckPaymentRequestsToGenerate(String checkPaymentRequestsToGenerate) {
		
		  this.checkPaymentRequestsToGenerate = checkPaymentRequestsToGenerate;
		
	}
   
   
   public String getDebitMemosToApprove() {
   	
   	  return debitMemosToApprove;
   	
   }
   
   public void setDebitMemosToApprove(String debitMemosToApprove) {
   	
   	  this.debitMemosToApprove = debitMemosToApprove;
   	
   }
   
   
   
   
   public String getBankAdjustmentsToApprove() {
   	
   	  return bankAdjustmentsToApprove;
   	  
   }
   
   public void setBankAdjustmentsToApprove(String bankAdjustmentsToApprove) {
   	
   	  this.bankAdjustmentsToApprove = bankAdjustmentsToApprove;
   	  
   }
   
   public String getFundTransfersToApprove() {
   	  
   	  return fundTransfersToApprove;
   	  
   }
   
   public void setFundTransfersToApprove(String fundTransfersToApprove) {
   	
   	  this.fundTransfersToApprove = fundTransfersToApprove;
   	  
   }
   
   public String getInvAdjustmentsToApprove() {
   	
   	  return invAdjustmentsToApprove;
   	  
   }
   
   public void setInvAdjustmentsToApprove(String invAdjustmentsToApprove) {
   	
   	  this.invAdjustmentsToApprove = invAdjustmentsToApprove;
   	  
   }
   
   public String getInvBuildUnbuildAssemblyToApprove() {
   	  
   	  return invBuildUnbuildAssemblyToApprove;
   	  
   }
   
   public void setInvBuildUnbuildAssemblyToApprove(String invBuildUnbuildAssemblyToApprove) {
   	
   	  this.invBuildUnbuildAssemblyToApprove = invBuildUnbuildAssemblyToApprove;
   	  
   }
   
   public String getJournalsInDraft() {
      	
   	  return journalsInDraft;
   	
   }
   
   public void setJournalsInDraft(String journalsInDraft) {
  	
	  this.journalsInDraft = journalsInDraft;
	
   }
   
   public String getVouchersInDraft() {
      	
   	  return vouchersInDraft;
   	
   }
   
   public void setVouchersInDraft(String vouchersInDraft) {
  	
	  this.vouchersInDraft = vouchersInDraft;
	
   }
   
   public String getDebitMemosInDraft() {
      	
   	  return debitMemosInDraft;
   	
   }
   
   public void setDebitMemosInDraft(String debitMemosInDraft) {
  	
	  this.debitMemosInDraft = debitMemosInDraft;
	
   }
   
   public String getChecksInDraft() {
      	
   	  return checksInDraft;
   	
   }
   
   public void setChecksInDraft(String checksInDraft) {
  	
	  this.checksInDraft = checksInDraft;
	
   }
   
   public String getChecksToApprove() {
	   	
   	  return checksToApprove;
   	
   }
   
   public void setChecksToApprove(String checksToApprove) {
   	
   	  this.checksToApprove = checksToApprove;
   	
   }
   
   
   public String getChecksRejected() {
	   	
   	  return checksRejected;
   	
   }
   
   public void setChecksRejected(String checksRejected) {
   	
   	  this.checksRejected = checksRejected;
   	
   }
   
   
   
   public String getDirectChecksInDraft() {
     	
   	  return directChecksInDraft;
   	
   }
   
   public void setDirectChecksInDraft(String directChecksInDraft) {
  	
	  this.directChecksInDraft = directChecksInDraft;
	
   }
	   
   public String getDirectChecksToApprove() {
	   	
   	  return directChecksToApprove;
   	
   }
   
   public void setDirectChecksToApprove(String directChecksToApprove) {
   	
   	  this.directChecksToApprove = directChecksToApprove;
   	
   }
   
   
   public String getDirectChecksRejected() {
	   	
   	  return directChecksRejected;
   	
   }
   
   public void setDirectChecksRejected(String directChecksRejected) {
   	
   	  this.directChecksRejected = directChecksRejected;
   	
   }
	   
   
   public String getInvoicesInDraft() {
      	
   	  return invoicesInDraft;
   	
   }
   
   public void setInvoicesInDraft(String invoicesInDraft) {
  	
	  this.invoicesInDraft = invoicesInDraft;
	
   }
   
   public String getOrdersInDraft() {
     	
   	  return ordersInDraft;
   	
   }
   
   public void setOrdersInDraft(String ordersInDraft) {
  	
	  this.ordersInDraft = ordersInDraft;
	
   }
   

   public String getInvoicesForPosting() {
     	
	   	  return invoicesForPosting;
	   	
   }
	   
   public void setInvoicesForPosting(String invoicesForPosting) {
  	
	  this.invoicesForPosting = invoicesForPosting;
	
   }
   
   public String getCreditMemosInDraft() {
      	
   	  return creditMemosInDraft;
   	
   }
   
   public void setCreditMemosInDraft(String creditMemosInDraft) {
  	
	  this.creditMemosInDraft = creditMemosInDraft;
	
   }
   
   public String getCreditMemosInDraftItems() {
 	
   	  return creditMemosInDraftItems;
   	
   }
   
   public void setCreditMemosInDraftItems(String creditMemosInDraftItems){
  	
	  this.creditMemosInDraftItems = creditMemosInDraftItems;
	
   }
   
   public String getCreditMemosInDraftMemoLines() {
	 	
   	  return creditMemosInDraftMemoLines;
   	
   }
   
   public void setCreditMemosInDraftMemoLines(String creditMemosInDraftMemoLines) {
  	
	  this.creditMemosInDraftMemoLines = creditMemosInDraftMemoLines;
	
   }
	   
	   
   public String getReceiptsCollectionInDraft() {
      	
   	  return receiptsCollectionInDraft;
   	
   }
   
   public void setReceiptsCollectionInDraft(String receiptsCollectionInDraft) {
  	
	  this.receiptsCollectionInDraft = receiptsCollectionInDraft;
	
   }
   
   
   public String getReceiptsMiscInDraft() {
     	
   	  return receiptsMiscInDraft;
   	
   }
   
   public void setReceiptsMiscInDraft(String receiptsMiscInDraft) {
  	
	  this.receiptsMiscInDraft = receiptsMiscInDraft;
	
   }
   
   public String getBankAdjustmentsInDraft() {
      	
   	  return bankAdjustmentsInDraft;
   	
   }
   
   public void setBankAdjustmentsInDraft(String bankAdjustmentsInDraft) {
  	
	  this.bankAdjustmentsInDraft = bankAdjustmentsInDraft;
	
   }
   
   public String getFundTransfersInDraft() {
      	
   	  return fundTransfersInDraft;
   	
   }
   
   public void setFundTransfersInDraft(String fundTransfersInDraft) {
  	
	  this.fundTransfersInDraft = fundTransfersInDraft;
	
   }
   
   public String getInventoryAdjustmentsInDraft() {
      	
   	  return inventoryAdjustmentsInDraft;
   	
   }
   
   public void setInventoryAdjustmentsInDraft(String inventoryAdjustmentsInDraft) {
  	
	  this.inventoryAdjustmentsInDraft = inventoryAdjustmentsInDraft;
	
   }
   
   public String getBuildUnbuildAssemblyInDraft() {
      	
   	  return buildUnbuildAssemblyInDraft;
   	
   }
   
   public void setBuildUnbuildAssemblyInDraft(String buildUnbuildAssemblyInDraft) {
  	
	  this.buildUnbuildAssemblyInDraft = buildUnbuildAssemblyInDraft;
	
   }
   
   public String getBuildUnbuildAssemblyOrderInDraft() {
     	
	   	  return buildUnbuildAssemblyOrderInDraft;
	   	
	   }
	   
	   public void setBuildUnbuildAssemblyOrderInDraft(String buildUnbuildAssemblyOrderInDraft) {
	  	
		  this.buildUnbuildAssemblyOrderInDraft = buildUnbuildAssemblyOrderInDraft;
		
	   }
   
   public String getBuildUnbuildAssemblyOrderInDue() {
     	
   	  return buildUnbuildAssemblyOrderInDue;
   	
   }
   
   public void setBuildUnbuildAssemblyOrderInDue(String buildUnbuildAssemblyOrderInDue) {
  	
	  this.buildUnbuildAssemblyOrderInDue = buildUnbuildAssemblyOrderInDue;
	
   }
   
   public String getPdcInvoicesToGenerate() {
   	
   	  return pdcInvoicesToGenerate;
   	  
   }
   
   public void setPdcInvoicesToGenerate(String pdcInvoicesToGenerate) {
   	
   	  this.pdcInvoicesToGenerate = pdcInvoicesToGenerate;
   	  
   }
   
   public String getInvStockTransferToApprove() {
       
       return invStockTransferToApprove;
       
   }
   
   public void setInvStockTransferToApprove(String invStockTransferToApprove) {
       
       this.invStockTransferToApprove = invStockTransferToApprove;
       
   }
   
   
    public String getInvStockTransferDraft() {
       
       return invStockTransferDraft;
       
   }
   
   public void setInvStockTransferDraft(String invStockTransferDraft) {
       
       this.invStockTransferDraft = invStockTransferDraft;
       
   }
   
   public String getInvBranchStockTransferOrderToApprove() {
       
       return invBranchStockTransferOrderToApprove;
       
   }
   
   public void setInvBranchStockTransferOrderToApprove(String invBranchStockTransferOrderToApprove) {
       
       this.invBranchStockTransferOrderToApprove = invBranchStockTransferOrderToApprove;
       
   }
   
   public String getPurchaseOrdersInDraft() {
   	
   	   return purchaseOrdersInDraft;
   	   
   }
   
   public void setPurchaseOrdersInDraft(String purchaseOrdersInDraft) {
   	
   	  this.purchaseOrdersInDraft = purchaseOrdersInDraft;
   	  
   }
   
   public String getReceivingItemsInDraft() {
	   	
   	   return receivingItemsInDraft;
   	   
   }
   
   public void setReceivingItemsInDraft(String receivingItemsInDraft) {
   	
   	  this.receivingItemsInDraft = receivingItemsInDraft;
   	  
   }
   
   public String getPurchaseOrdersToApprove() {
   	
   	   return purchaseOrdersToApprove;
   	   
   }
   
   public void setPurchaseOrdersToApprove(String purchaseOrdersToApprove) {
   	
   	  this.purchaseOrdersToApprove = purchaseOrdersToApprove;
   	  
   }
   
   public String getPurchaseOrdersRejected() {
	   	
   	   return purchaseOrdersRejected;
   	   
   }
   
   public void setPurchaseOrdersRejected(String purchaseOrdersRejected) {
   	
   	  this.purchaseOrdersRejected = purchaseOrdersRejected;
   	  
   }
   
   public String getReceivingItemsToApprove() {
	   	
   	   return receivingItemsToApprove;
   	   
   }
   
   public void setReceivingItemsToApprove(String receivingItemsToApprove) {
   	
   	  this.receivingItemsToApprove = receivingItemsToApprove;
   	  
   }
   
   public String getReceivingItemsRejected() {
	   	
   	   return receivingItemsRejected;
   	   
   }
   
   public void setReceivingItemsRejected(String receivingItemsRejected) {
   	
   	  this.receivingItemsRejected = receivingItemsRejected;
   	  
   }
   
   public String getPurchaseOrdersForGeneration() {
	   	
   	   return purchaseOrdersForGeneration;
   	   
   }
   
   public void setPurchaseOrdersForGeneration(String purchaseOrdersForGeneration) {
   	
   	  this.purchaseOrdersForGeneration = purchaseOrdersForGeneration;
   	  
   }
   
   public String getPurchaseOrdersForPrinting() {
	   	
   	   return purchaseOrdersForPrinting;
   	   
   }
   
   public void setPurchaseOrdersForPrinting(String purchaseOrdersForPrinting) {
   	
   	  this.purchaseOrdersForPrinting = purchaseOrdersForPrinting;
   	  
   }

   
   public String getRecurringPurchaseRequisitionsToGenerate() {
	   	
	   	  return recurringPurchaseRequisitionsToGenerate;
	   	
   }
	   
   public void setRecurringPurchaseRequisitionsToGenerate(String recurringPurchaseRequisitionsToGenerate) {
	   	
	   	  this.recurringPurchaseRequisitionsToGenerate = recurringPurchaseRequisitionsToGenerate;
	   	
   }
	   
	   
   
   public String getPurchaseRequisitionsInDraft() {
	   	
   	   return purchaseRequisitionsInDraft;
   	   
   }
   
   public void setPurchaseRequisitionsInDraft(String purchaseRequisitionsInDraft) {
   	
   	  this.purchaseRequisitionsInDraft = purchaseRequisitionsInDraft;
   	  
   }
   
   public String getPurchaseRequisitionsToApprove() {
   	
   	   return purchaseRequisitionsToApprove;
   	   
   }
   
   public void setPurchaseRequisitionsToApprove(String purchaseRequisitionsToApprove) {
   	
   	  this.purchaseRequisitionsToApprove = purchaseRequisitionsToApprove;
   	  
   }
   
   public String getPurchaseRequisitionsForCanvass() {
	   	
   	   return purchaseRequisitionsForCanvass;
   	   
   }
   
   public void setPurchaseRequisitionsForCanvass(String purchaseRequisitionsForCanvass) {
   	
   	  this.purchaseRequisitionsForCanvass = purchaseRequisitionsForCanvass;
   	  
   }
   
   
   public String getInvBranchStockTransferOrderInDraft() {
	   	
   	   return invBranchStockTransferOrderInDraft;
   	   
   }
   
   public void setInvBranchStockTransferOrderInDraft(String invBranchStockTransferOrderInDraft) {
   	
   	  this.invBranchStockTransferOrderInDraft = invBranchStockTransferOrderInDraft;
   	  
   }
   
   
   public String getInvBranchStockTransferUnservedOrders() {
	   	
   	   return invBranchStockTransferUnservedOrders;
   	   
   }
   
   public void setInvBranchStockTransferUnservedOrders(String invBranchStockTransferUnservedOrders) {
   	
   	  this.invBranchStockTransferUnservedOrders = invBranchStockTransferUnservedOrders;
   	  
   }
   
   public String getInvBranchStockTransferOutInDraft() {
	   	
   	   return invBranchStockTransferOutInDraft;
   	   
   }
   
   public void setInvBranchStockTransferOutInDraft(String invBranchStockTransferOutInDraft) {
   	
   	  this.invBranchStockTransferOutInDraft = invBranchStockTransferOutInDraft;
   	  
   }   
   
   public String getInvBranchStockTransferOutIncoming() {
	   	
   	   return invBranchStockTransferOutIncoming;
   	   
   }
   
   public void setInvBranchStockTransferOutIncoming(String invBranchStockTransferOutIncoming) {
   	
   	  this.invBranchStockTransferOutIncoming = invBranchStockTransferOutIncoming;
   	  
   }   
   
   public String getInvBranchStockTransferInDraft() {
	   	
   	   return invBranchStockTransferInDraft;
   	   
   }
   
   public void setInvBranchStockTransferInDraft(String invBranchStockTransferInDraft) {
   	
   	  this.invBranchStockTransferInDraft = invBranchStockTransferInDraft;
   	  
   }   
   
   public String getInvAdjustmentRequestToApprove() {
	   	
   	   return invAdjustmentRequestToApprove;
   	   
   }
   
   public void setInvAdjustmentRequestToApprove(String invAdjustmentRequestToApprove) {
   	
   	  this.invAdjustmentRequestToApprove = invAdjustmentRequestToApprove;
   	  
   }   
   
   public String getInvAdjustmentRequestToGenerate() {
	   	
   	   return invAdjustmentRequestToGenerate;
   	   
   }
   
   public void setInvAdjustmentRequestToGenerate(String invAdjustmentRequestToGenerate) {
   	
   	  this.invAdjustmentRequestToGenerate = invAdjustmentRequestToGenerate;
   	  
   }
   
   
   public String getPurchaseRequisitionsRejected() {
	   	
   	   return purchaseRequisitionsRejected;
   	   
   }
   
   public void setPurchaseRequisitionsRejected(String purchaseRequisitionsRejected) {
   	
   	  this.purchaseRequisitionsRejected = purchaseRequisitionsRejected;
   	  
   }
 
   public String getCanvassInDraft() {
	   	
   	   return canvassInDraft;
   	   
   }
   
   public void setCanvassInDraft(String canvassInDraft) {
   	
   	  this.canvassInDraft = canvassInDraft;
   	  
   }
   
   public String getCanvassToApprove() {
   	
   	   return canvassToApprove;
   	   
   }
   
   public void setCanvassToApprove(String canvassToApprove) {
   	
   	  this.canvassToApprove = canvassToApprove;
   	  
   }
   
   public String getCanvassRejected() {
   	
   	   return canvassRejected;
   	   
   }
   
   public void setCanvassRejected(String canvassRejected) {
   	
   	  this.canvassRejected = canvassRejected;
   	  
   }
      
   public String getSalesOrdersToApprove() {
	   	
   	   return salesOrdersToApprove;
   	   
   }
   
   public void setSalesOrdersToApprove(String salesOrdersToApprove) {
   	
   	  this.salesOrdersToApprove = salesOrdersToApprove;
   	  
   }
   
   public String getSalesOrderDeliveryStatus() {
	   	
 	   return salesOrderDeliveryStatus;
 	   
 }
 
 public void setSalesOrderDeliveryStatus(String salesOrderDeliveryStatus) {
 	
 	  this.salesOrderDeliveryStatus = salesOrderDeliveryStatus;
 	  
 }
   
   public String getCustomerToApprove() {
	   	
   	   return customerToApprove;
   	   
   }
   
   public void setCustomerToApprove(String customerToApprove) {
   	
   	  this.customerToApprove = customerToApprove;
   	  
   }
   
   public String getCustomerRejected() {
	   	
   	   return customerRejected;
   	   
   }
   
   public void setCustomerRejected(String customerRejected) {
   	
   	  this.customerRejected = customerRejected;
   	  
   }
   
   public String getSalesOrdersForProcessing() {
	   	
   	   return salesOrdersForProcessing;
   	   
   }
   
   public void setSalesOrdersForProcessing(String salesOrdersForProcessing) {
   	
   	  this.salesOrdersForProcessing = salesOrdersForProcessing;
   	  
   }
   
   public String getSalesOrdersApproveToday() {
	   	
   	   return salesOrdersApproveToday;
   	   
   }
   
   public void setSalesOrdersApproveToday(String salesOrdersApproveToday) {
   	
   	  this.salesOrdersApproveToday = salesOrdersApproveToday;
   	  
   }
   
   public String getChecksForRelease() {
	   	
   	   return checksForRelease;
   	   
   }
   
   public void setChecksForRelease(String checksForRelease) {
   	
   	  this.checksForRelease = checksForRelease;
   	  
   }
   
   public String getChecksForPrinting() {
	   	
   	   return checksForPrinting;
   	   
   }
   
   public void setChecksForPrinting(String checksForPrinting) {
   	
   	  this.checksForPrinting = checksForPrinting;
   	  
   }
   
   public String getPaymentRequestDraft() {
	   	
   	   return paymentRequestDraft;
   	   
   }
   
   public void setPaymentRequestDraft(String paymentRequestDraft) {
   	
   	  this.paymentRequestDraft = paymentRequestDraft;
   	  
   }
   
   public String getPaymentRequestGeneration() {
	   	
   	   return paymentRequestGeneration;
   	   
   }
   
   public void setPaymentRequestGeneration(String paymentRequestGeneration) {
   	
   	  this.paymentRequestGeneration = paymentRequestGeneration;
   	  
   }
   
   public String getPaymentRequestProcessing() {
	   	
   	   return paymentRequestProcessing;
   	   
   }
   
   public void setPaymentRequestProcessing(String paymentRequestProcessing) {
   	
   	  this.paymentRequestProcessing = paymentRequestProcessing;
   	  
   }
   
   public String getPaymentsForProcessing() {
	   	
   	   return paymentsForProcessing;
   	   
   }
   
   public void setPaymentsForProcessing(String paymentsForProcessing) {
   	
   	  this.paymentsForProcessing = paymentsForProcessing;
   	  
   }
   
   
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
   	
      userDescription = null;
      today = null;
      journalsToApprove = null;
      recurringJournalsToGenerate = null;
      journalsForReversal = null;
      recurringVouchersToGenerate = null;
      invoicesToApprove = null;
      overDueInvoices = null;
      accruedInterestIS = null;
      accruedInterestTB = null;
      investorBonus = null;
      investorInterest = null;
      generateLoan = null;
      pastDueInvoices = null;
      creditMemosToApprove = null;
      creditMemosForPosting = null;
      receiptsToApprove = null;
      vouchersToApprove = null;
      vouchersRejected = null;
      vouchersForProcessing = null;
      checkPaymentRequestsToApprove = null;
      checkPaymentRequestsToGenerate = null;
      debitMemosToApprove = null;
      
      bankAdjustmentsToApprove = null;
      fundTransfersToApprove = null;
      invAdjustmentsToApprove = null;
      invAdjustmentRequestToGenerate = null;
      invBuildUnbuildAssemblyToApprove = null;
      journalsInDraft = null;
      paymentRequestInDraft = null;
      paymentRequestRejected = null;
      vouchersInDraft = null;
      debitMemosInDraft = null;
      checksInDraft = null;
      checksToApprove = null;
      
      checksRejected = null;
      
      directChecksInDraft = null;
      directChecksToApprove = null;
      directChecksRejected = null;
      
      invoicesInDraft = null;
      ordersInDraft = null;
      invoicesInDraftItem = null;
      invoicesInDraftMemoLines = null;
      invoicesInDraftSoMatched = null;
      invoicesForPosting = null;
      invoicesForPostingItem = null;
      invoicesForPostingMemoLines = null;
      
      creditMemosInDraft = null;
      receiptsCollectionInDraft = null;
      receiptsMiscInDraft = null;
      receiptsCollectionForPosting = null;
      receiptsMiscForPosting = null;
      bankAdjustmentsInDraft = null;
      fundTransfersInDraft = null;
      inventoryAdjustmentsInDraft = null;
      buildUnbuildAssemblyInDraft = null;
      buildUnbuildAssemblyOrderInDraft = null;
      buildUnbuildAssemblyOrderInDue = null;
      pdcInvoicesToGenerate = null;
      invBranchStockTransferOrderToApprove = null;
      invStockTransferToApprove = null;
      invStockTransferDraft = null;
      purchaseOrdersInDraft = null;
      receivingItemsInDraft = null;
      receivingItemsRejected = null;
      purchaseOrdersToApprove = null;
      purchaseOrdersRejected = null;
      receivingItemsToApprove = null;
      purchaseOrdersForGeneration = null;
      purchaseOrdersForPrinting = null;
      recurringPurchaseRequisitionsToGenerate = null;
      purchaseRequisitionsInDraft = null;
      purchaseRequisitionsToApprove = null;
      purchaseRequisitionsForCanvass = null;
      purchaseRequisitionsRejected = null;
      canvassInDraft = null;
      canvassToApprove = null;
      canvassRejected = null;
      salesOrdersToApprove = null;
      customerToApprove = null;
      customerRejected = null;
      salesOrdersApproveToday = null;

      checksForRelease = null;
      checksForPrinting = null;
      paymentRequestDraft = null;
      paymentRequestGeneration = null;
      paymentRequestProcessing = null;
      
   }

public String getInvoicesForPostingMemoLines() {
	return invoicesForPostingMemoLines;
}

public void setInvoicesForPostingMemoLines(String invoicesForPostingMemoLines) {
	this.invoicesForPostingMemoLines = invoicesForPostingMemoLines;
}

public String getInvoicesForPostingItem() {
	return invoicesForPostingItem;
}

public void setInvoicesForPostingItem(String invoicesForPostingItem) {
	this.invoicesForPostingItem = invoicesForPostingItem;
}

public String getInvoicesInDraftMemoLines() {
	return invoicesInDraftMemoLines;
}

public void setInvoicesInDraftMemoLines(String invoicesInDraftMemoLines) {
	this.invoicesInDraftMemoLines = invoicesInDraftMemoLines;
}


public String getInvoicesInDraftSoMatched() {
	return invoicesInDraftSoMatched;
}

public void setInvoicesInDraftSoMatched(String invoicesInDraftSoMatched) {
	this.invoicesInDraftSoMatched = invoicesInDraftSoMatched;
}

public String getInvoicesInDraftItem() {
	return invoicesInDraftItem;
}

public void setInvoicesInDraftItem(String invoicesInDraftItem) {
	this.invoicesInDraftItem = invoicesInDraftItem;
}

}
