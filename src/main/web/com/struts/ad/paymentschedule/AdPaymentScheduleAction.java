package com.struts.ad.paymentschedule;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPSRelativeAmountLessPytBaseAmountException;
import com.ejb.exception.AdPSRelativeAmountOverPytBaseAmountException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.AdPaymentScheduleController;
import com.ejb.txn.AdPaymentScheduleControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdPaymentScheduleDetails;
import com.util.AdPaymentTermDetails;

public final class AdPaymentScheduleAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdPaymentScheduleAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdPaymentScheduleForm actionForm = (AdPaymentScheduleForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_PAYMENT_SCHEDULE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adPaymentSchedule");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdPaymentScheduleController EJB
*******************************************************/

         AdPaymentScheduleControllerHome homePS = null;
         AdPaymentScheduleController ejbPS = null;

         try {

            homePS = (AdPaymentScheduleControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdPaymentScheduleControllerEJB", AdPaymentScheduleControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdPaymentScheduleAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbPS = homePS.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdPaymentScheduleAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad PS Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	ArrayList psList = new ArrayList();
         	short lineNumber = 0;
         	
         	for(int i=0; i<actionForm.getAdPSListSize(); i++){
         		
         		AdPaymentScheduleList adPSList = actionForm.getAdPSByIndex(i);
         		
         		if (Common.validateRequired(adPSList.getRelativeAmount()) &&
          	 	       Common.validateRequired(adPSList.getDueDay()))continue;
         		
         		lineNumber++;
         	         		
         		AdPaymentScheduleDetails details = new AdPaymentScheduleDetails();
         		details.setPsCode(adPSList.getPaymentScheduleCode());
                details.setPsLineNumber(lineNumber);
                details.setPsRelativeAmount(Common.convertStringMoneyToDouble(adPSList.getRelativeAmount(), (short)6));
                details.setPsDueDay(Common.convertStringToShort(adPSList.getDueDay()));
                
                psList.add(details);
         	}
         	
         	try {
            	
            	ejbPS.addAdPsEntry(psList, actionForm.getPaymentTermCode(), user.getCmpCode());
                
            } catch (AdPSRelativeAmountOverPytBaseAmountException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("paymentSchedule.error.relativeAmountIsGreaterThanBaseAmount"));

            } catch (AdPSRelativeAmountLessPytBaseAmountException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("paymentSchedule.error.relativeAmountIsLessThanBaseAmount"));
               
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdPaymentScheduleAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad PS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Ad PS Back Action --
*******************************************************/
            
         } else if (request.getParameter("paymentTerms") != null) {
         	
         	return(new ActionForward("/adPaymentTerm.do"));
         	
/*******************************************************
   -- Ad PS Discount Action --
*******************************************************/

         } else if (request.getParameter("adPSList[" +
            actionForm.getRowSelected() + "].discountsButton") != null){

	        AdPaymentScheduleList adPSList =
               actionForm.getAdPSByIndex(actionForm.getRowSelected());

	        String path = "/adDiscount.do?forward=1" +
	           "&paymentScheduleCode=" + adPSList.getPaymentScheduleCode() + 
	           "&lineNumber=" +  adPSList.getLineNumber() + 
	           "&relativeAmount=" + adPSList.getRelativeAmount() +
	           "&dueDay=" + adPSList.getDueDay(); 
	        
	        return(new ActionForward(path));            

/*******************************************************
   -- Ad PS Add Lines Action --
*******************************************************/
	        
         } else if(request.getParameter("addLinesButton") != null && 
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	int listSize = actionForm.getAdPSListSize();
         	
         	for (int x = listSize + 1; x <= listSize + 4; x++) {
         		
         		AdPaymentScheduleList adPSList = new AdPaymentScheduleList(actionForm,
        				null, new Integer(x).toString(), null, null);
         		
         		actionForm.saveAdPSList(adPSList);
         		
         	}	        
         	
         	return(mapping.findForward("adPaymentSchedule"));
         	
/*******************************************************
   -- Ad PS Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && 
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	for (int i = 0; i<actionForm.getAdPSListSize(); i++) {
         		
         		AdPaymentScheduleList adPSList = actionForm.getAdPSByIndex(i);
         		         		
         		if (adPSList.getDeleteCheckbox()) {
         			
         			actionForm.deleteAdPSList(i);
         			i--;
         			
         			// delete payment schedule
         			if(adPSList.getPaymentScheduleCode()!=null){
         				
         				try {
         					
         					ejbPS.deleteAdPsEntry(adPSList.getPaymentScheduleCode(), user.getCmpCode());
         					
         				} catch (GlobalRecordAlreadyDeletedException ex) {
         					
         					errors.add(ActionMessages.GLOBAL_MESSAGE,
         							new ActionMessage("paymentSchedule.error.paymentScheduleAlreadyDeleted"));
         					
         				} catch(EJBException ex) {
         					
         					if (log.isInfoEnabled()) {
         						
         						log.info("EJBException caught in AdPaymentScheduleAction.execute(): " + ex.getMessage() +
         								" session: " + session.getId());
         					}
         					
         					return(mapping.findForward("cmnErrorPage"));
         					
         				} 	
         				
         			}
         		}
         		
         	}
         	
         	for (int i = 0; i<actionForm.getAdPSListSize(); i++) {
         		
         		AdPaymentScheduleList adPSList = actionForm.getAdPSByIndex(i);
         		adPSList.setLineNumber(String.valueOf(i+1));
         		
         	}
         	
         	return(mapping.findForward("adPaymentSchedule"));
                     
/*******************************************************
   -- Ar PS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adPaymentSchedule");

            }
                                  	                        	                          	            	
            AdPaymentTermDetails ptDetails = null;

	        if (request.getParameter("forward") != null) {

	            ptDetails = ejbPS.getAdPytByPytCode(new Integer(request.getParameter("paymentTermCode")), user.getCmpCode());        		
	    	    actionForm.setPaymentTermCode(new Integer(request.getParameter("paymentTermCode")));

	        } else {
	     		
	            ptDetails = ejbPS.getAdPytByPytCode(actionForm.getPaymentTermCode(), user.getCmpCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setPaymentTermCode(actionForm.getPaymentTermCode());
            actionForm.setTermName(ptDetails.getPytName());
            actionForm.setDescription(ptDetails.getPytDescription());                        	
            actionForm.setBaseAmount(Common.convertDoubleToStringMoney(ptDetails.getPytBaseAmount(),  (short)6));        		        		
    	    actionForm.setEnable(Common.convertByteToBoolean(ptDetails.getPytEnable()));

            ArrayList list = null;
            Iterator i = null;
            
	        try {
	        	
	        	actionForm.clearAdPSList();
	        	
	        	list = ejbPS.getAdPsByPytCode(actionForm.getPaymentTermCode(), user.getCmpCode()); 
	        	
	        	i = list.iterator();
	        	
	        	while(i.hasNext()) {
	        		
	        		AdPaymentScheduleDetails details = (AdPaymentScheduleDetails)i.next();
	        		
	        		AdPaymentScheduleList adPSList = new AdPaymentScheduleList(actionForm,
	        				details.getPsCode(),
							Common.convertShortToString(details.getPsLineNumber()),
							//Common.convertDoubleToStringMoney(details.getPsRelativeAmount(), Constants.DEFAULT_PRECISION),
							Common.convertDoubleToStringMoney(details.getPsRelativeAmount(), (short)6),
							Common.convertShortToString(details.getPsDueDay()));
	        		
	        		actionForm.saveAdPSList(adPSList);
	        		
	        	}
	        	
	        	int remainingList = 4 - (list.size() % 4) + list.size();
    			
    			for (int x = list.size() + 1; x <= remainingList; x++) {
    				
    				AdPaymentScheduleList adPSList = new AdPaymentScheduleList(actionForm,
            				null, new Integer(x).toString(), null, null);
            		
            		actionForm.saveAdPSList(adPSList);
    				
    			}
	        	
	        } catch (GlobalNoRecordFoundException ex) {
	        	
        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdPaymentScheduleAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            actionForm.setShowAddLinesButton(true);
   			actionForm.setShowDeleteLinesButton(true);
   			
            return(mapping.findForward("adPaymentSchedule"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdPaymentScheduleAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}