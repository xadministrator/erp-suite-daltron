package com.struts.ad.paymentschedule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdPaymentScheduleForm extends ActionForm implements Serializable {

   private Integer paymentTermCode = null;
   private String termName = null;
   private String description = null;
   private String baseAmount = null;
   private boolean enable = false;
   
   private String saveButton = null;
   private String closeButton = null;
   private String paymentTerms = null;
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   
   private ArrayList adPSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdPaymentScheduleList getAdPSByIndex(int index) {

      return((AdPaymentScheduleList)adPSList.get(index));

   }

   public Object[] getAdPSList() {

      return adPSList.toArray();

   }

   public int getAdPSListSize() {

      return adPSList.size();

   }

   public void saveAdPSList(Object newAdPSList) {

      adPSList.add(newAdPSList);

   }

   public void clearAdPSList() {
      
   	  adPSList.clear();
   	  
   }

   public void setRowSelected(Object selectedAdPSList, boolean isEdit) {

      this.rowSelected = adPSList.indexOf(selectedAdPSList);

   }

   public void updateApVOURow(int rowSelected, Object newAdPSList){
   
   	  adPSList.set(rowSelected, newAdPSList);
   	  
   }
   
   public void deleteAdPSList(int rowSelected) {

      adPSList.remove(rowSelected);

   }
   
   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setPaymentTerms(String paymentTerms) {
   	
   	  this.paymentTerms = paymentTerms;
   	  
   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
      
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getPaymentTermCode() {
   	
   	  return paymentTermCode;
   	
   }
   
   public void setPaymentTermCode(Integer paymentTermCode) {
   	
   	  this.paymentTermCode = paymentTermCode;
   	
   }
   
   public String getTermName() {

      return termName;

   }

   public void setTermName(String termName) {

      this.termName = termName;

   }
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }

   public String getBaseAmount() {

      return baseAmount;

   }

   public void setBaseAmount(String baseAmount) {

      this.baseAmount = baseAmount;

   }
   
   public boolean getEnable() {

      return enable;

   }

   public void setEnable(boolean enable) {

      this.enable = enable;

   }  
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      saveButton = null;
      closeButton = null;
	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null) {

      	String prevDueDay = null;
      	      	
      	Iterator i = adPSList.iterator();      	 
      	
      	while (i.hasNext()) {
      		
      		AdPaymentScheduleList psList = (AdPaymentScheduleList)i.next();      	 	 
      		
      		if (Common.validateRequired(psList.getRelativeAmount()) &&
        	 	       Common.validateRequired(psList.getDueDay()))continue;
      		
      		if (Common.validateRequired(psList.getRelativeAmount())) {
      			
      			errors.add("relativeAmount",
      					new ActionMessage("paymentSchedule.error.relativeAmountRequired", psList.getLineNumber()));
      			
      		}
      		
      		if (Common.validateRequired(psList.getDueDay())) {
      			
      			errors.add("dueDay",
      					new ActionMessage("paymentSchedule.error.dueDayRequired", psList.getLineNumber()));
      			
      		}         
      		
      		if (!Common.validateMoneyFormat(psList.getRelativeAmount())) {
      			
      			errors.add("relativeAmount",
      					new ActionMessage("paymentSchedule.error.relativeAmountInvalid", psList.getLineNumber()));
      			
      		}         
      		
      		if (!Common.validateNumberFormat(psList.getDueDay())) {
      			
      			errors.add("dueDay",
      					new ActionMessage("paymentSchedule.error.dueDayInvalid", psList.getLineNumber()));
      			
      		}
      		
      		if(prevDueDay == null) {
      		
      			prevDueDay = psList.getDueDay();
      			
      		} else {
      			
      			if(Common.convertStringToInt(psList.getDueDay()) < Common.convertStringToInt(prevDueDay)) {
      				
      				errors.add("dueDay",
          					new ActionMessage("paymentSchedule.error.dueDayLessThanPreviousRow", psList.getLineNumber()));
      				
      			}
				
      		}

      		// check deciaml places
      		if (psList.getRelativeAmount() != null && psList.getRelativeAmount().length() > 0) {
      			
      			int index = psList.getRelativeAmount().indexOf(".");
      			int ctr = 0;
      			
      			for(int j=index + 1; j<psList.getRelativeAmount().length(); j++){
      				if(!psList.getRelativeAmount().substring(j, j + 1).equals("0")) {
      					ctr++;
      				}
      			}
      			
      			if(ctr > 6) {
      				errors.add("relativeAmount",
          					new ActionMessage("paymentSchedule.error.relativeAmountDecimalInvalid", psList.getLineNumber()));
      			}
      			
      		}
      		
      		      		
      	}
      	
      }
         
      return errors;

   }
}