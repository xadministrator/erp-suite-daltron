package com.struts.ad.paymentschedule;

import java.io.Serializable;

public class AdPaymentScheduleList implements Serializable {

   private Integer paymentScheduleCode = null;
   private String lineNumber = null;
   private String relativeAmount = null;
   private String dueDay = null;

   private boolean deleteCheckbox = false;
   private String discountsButton = null;
    
   private AdPaymentScheduleForm parentBean;
    
   public AdPaymentScheduleList(AdPaymentScheduleForm parentBean,
      Integer paymentScheduleCode,
      String lineNumber,
      String relativeAmount,
      String dueDay) {

      this.parentBean = parentBean;
      this.paymentScheduleCode = paymentScheduleCode;
      this.lineNumber = lineNumber;
      this.relativeAmount = relativeAmount;
      this.dueDay = dueDay;

   }

   public boolean getDeleteCheckbox() {   	
   	  
   	  return deleteCheckbox;
   	  
   }
   
   public void setDeleteCheckbox(boolean deleteCheckbox) {
   	
   	  this.deleteCheckbox = deleteCheckbox;
   	  
   }  
   
   public String getDiscountsButton() {
   	
   	  return discountsButton;
   	  
   }
   public void setDiscountsButton(String discountsButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }

   public Integer getPaymentScheduleCode() {

      return paymentScheduleCode;

   }
   
   public String getLineNumber() {
   	
   	  return lineNumber;
   	
   }
   
   public void setLineNumber(String lineNumber) {
   	
   	  this.lineNumber = lineNumber;
   	
   }
   
   public String getRelativeAmount() {
   	
   	  return relativeAmount;
   	
   }
   
   public void setRelativeAmount(String relativeAmount) {
   	
   	  this.relativeAmount = relativeAmount;
   	
   }
   
   public String getDueDay() {
   	
   	  return dueDay;
   	
   }
   
   public void setDueDay(String dueDay) {
   	
   	  this.dueDay = dueDay;
   	
   }
   
}