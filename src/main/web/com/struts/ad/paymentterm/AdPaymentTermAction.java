package com.struts.ad.paymentterm;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.txn.AdPaymentTermController;
import com.ejb.txn.AdPaymentTermControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdPaymentTermDetails;
import com.util.AdModPaymentTermDetails;

public final class AdPaymentTermAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdPaymentTermAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdPaymentTermForm actionForm = (AdPaymentTermForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_PAYMENT_TERM_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adPaymentTerm");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdPaymentTermController EJB
*******************************************************/

         AdPaymentTermControllerHome homePT = null;
         AdPaymentTermController ejbPT = null;

         try {

            homePT = (AdPaymentTermControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdPaymentTermControllerEJB", AdPaymentTermControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdPaymentTermAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbPT = homePT.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdPaymentTermAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad PT Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adPaymentTerm"));
	     
/*******************************************************
   -- Ad PT Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adPaymentTerm"));                  
         
/*******************************************************
   -- Ad PT Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdPaymentTermDetails details = new AdPaymentTermDetails();
            details.setPytName(actionForm.getTermName());
            details.setPytDescription(actionForm.getDescription());            
            details.setPytBaseAmount(Common.convertStringMoneyToDouble(actionForm.getBaseAmount(), Constants.DEFAULT_PRECISION));
            details.setPytMonthlyInterestRate(Common.convertStringMoneyToDouble(actionForm.getMonthlyInterestRate(),(short)6));
            
            details.setPytEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            details.setPytEnableRebate(Common.convertBooleanToByte(actionForm.getEnableRebate()));
            details.setPytEnableInterest(Common.convertBooleanToByte(actionForm.getEnableInterest()));
            details.setPytDiscountOnInvoice(Common.convertBooleanToByte(actionForm.getDiscountOnInvoice()));
            details.setPytDiscountDescription(actionForm.getDiscountDescription());
            details.setPytScheduleBasis(actionForm.getScheduleBasis());
            
            try {
            	
            	ejbPT.addAdPytEntry(details, actionForm.getDiscountAccount(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("paymentTerm.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("paymentTerm.error.accountNumberInvalid"));   
               
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdPaymentTermAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ar PT Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar PT Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            AdPaymentTermList adPTList =
	            actionForm.getAdPTByIndex(actionForm.getRowSelected());
            
            if (!actionForm.getTermName().equals(adPTList.getTermName()) && adPTList.getTermName().equals("IMMEDIATE")) {
        	    	
    	    	errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("paymentTerm.error.systemKeywordError"));
    	    	
    	    	saveErrors(request, new ActionMessages(errors));
                return(mapping.findForward("adPaymentTerm"));
                
    	    }
            
            
            AdPaymentTermDetails details = new AdPaymentTermDetails();
            details.setPytCode(adPTList.getPaymentTermCode());
            details.setPytName(actionForm.getTermName());
            details.setPytDescription(actionForm.getDescription());            
            details.setPytBaseAmount(Common.convertStringMoneyToDouble(actionForm.getBaseAmount(), Constants.DEFAULT_PRECISION));
            details.setPytMonthlyInterestRate(Common.convertStringMoneyToDouble(actionForm.getMonthlyInterestRate(), (short)6));
            
            details.setPytEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            details.setPytEnableRebate(Common.convertBooleanToByte(actionForm.getEnableRebate()));
            details.setPytEnableInterest(Common.convertBooleanToByte(actionForm.getEnableInterest()));
            details.setPytDiscountOnInvoice(Common.convertBooleanToByte(actionForm.getDiscountOnInvoice()));
            details.setPytDiscountDescription(actionForm.getDiscountDescription());
            details.setPytScheduleBasis(actionForm.getScheduleBasis());
            
            try {
            	
            	ejbPT.updateAdPytEntry(details, actionForm.getDiscountAccount(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("paymentTerm.error.recordAlreadyExist"));
            
            } catch (GlobalAccountNumberInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("paymentTerm.error.accountNumberInvalid"));
               
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdPaymentTermAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad PT Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad PT Edit Action --
*******************************************************/

         } else if (request.getParameter("adPTList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdPTRow(actionForm.getRowSelected());
            
            return mapping.findForward("adPaymentTerm");
            
/*******************************************************
   -- Ad PT Payment Schedule Action --
*******************************************************/

         } else if (request.getParameter("adPTList[" +
            actionForm.getRowSelected() + "].paymentSchedulesButton") != null){

	        AdPaymentTermList adPTList =
               actionForm.getAdPTByIndex(actionForm.getRowSelected());

	        String path = "/adPaymentSchedule.do?forward=1" +
	           "&paymentTermCode=" + adPTList.getPaymentTermCode() + 
	           "&termName=" +  adPTList.getTermName() + 
	           "&description=" + adPTList.getDescription() +
	           "&baseAmount=" + adPTList.getBaseAmount() + 
	           //"&monthlyInterestRate=" + adPTList.getMonthlyInterestRate()+ 
	           "&enable=" + adPTList.getEnable(); 
	        
	        return(new ActionForward(path));          
	        
/*******************************************************
   -- Ad PT Delete Action --
*******************************************************/

         } else if (request.getParameter("adPTList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdPaymentTermList adPTList =
              actionForm.getAdPTByIndex(actionForm.getRowSelected());
            
            if (adPTList.getTermName().equals("IMMEDIATE")) {
    	    	
		    	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("paymentTerm.error.systemKeywordError"));
		    	
		    	saveErrors(request, new ActionMessages(errors));
	            return(mapping.findForward("adPaymentTerm"));
	            
		    }

            try {
            	
	            ejbPT.deleteAdPytEntry(adPTList.getPaymentTermCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("paymentTerm.error.deletePaymentTermAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("paymentTerm.error.paymentTermAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdPaymentTermAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar PT Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adPaymentTerm");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               actionForm.clearAdPTList();	        
	           
	           list = ejbPT.getAdPytAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdModPaymentTermDetails mdetails = (AdModPaymentTermDetails)i.next();
	            	
	              AdPaymentTermList adPTList = new AdPaymentTermList(actionForm,
	            	    mdetails.getPytCode(),
	            	    mdetails.getPytName(),
	            	    mdetails.getPytDescription(),
	            	    Common.convertDoubleToStringMoney(mdetails.getPytBaseAmount(), Constants.DEFAULT_PRECISION),
	            	    Common.convertDoubleToStringMoney(mdetails.getPytMonthlyInterestRate(), (short)6),
	            	    
	            	    Common.convertByteToBoolean(mdetails.getPytEnable()),
	            	    Common.convertByteToBoolean(mdetails.getPytEnableRebate()),
	            	    Common.convertByteToBoolean(mdetails.getPytEnableInterest()),
						Common.convertByteToBoolean(mdetails.getPytDiscountOnInvoice()),
						mdetails.getPtGlCoaAccountNumber(),
						mdetails.getPtGlCoaAccountDescription(),
						mdetails.getPytDiscountDescription(),
						mdetails.getPytScheduleBasis());
	            	 	            	    
	              actionForm.saveAdPTList(adPTList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdPaymentTermAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("adPaymentTerm"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdPaymentTermAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}