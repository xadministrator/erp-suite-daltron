package com.struts.ad.paymentterm;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdPaymentTermForm extends ActionForm implements Serializable {

   private Integer paymentTermCode = null;
   private String termName = null;
   private String description = null;
   private String baseAmount = null;
   private String monthlyInterestRate = null;
   private boolean enable = false;
   private boolean enableRebate = false;
   private boolean enableInterest= false;
   private boolean discountOnInvoice = false;
   private String discountAccount = null;
   private String discountAccountDescription = null;
   private String discountDescription = null;
   private String scheduleBasis = null;
   private ArrayList scheduleBasisList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;   
   private String pageState = new String();
   private ArrayList adPTList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdPaymentTermList getAdPTByIndex(int index) {

      return((AdPaymentTermList)adPTList.get(index));

   }

   public Object[] getAdPTList() {

      return adPTList.toArray();

   }

   public int getAdPTListSize() {

      return adPTList.size();

   }

   public void saveAdPTList(Object newAdPTList) {

      adPTList.add(newAdPTList);

   }

   public void clearAdPTList() {
      adPTList.clear();
   }

   public void setRowSelected(Object selectedAdPTList, boolean isEdit) {

      this.rowSelected = adPTList.indexOf(selectedAdPTList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdPTRow(int rowSelected) {
   	
       this.termName = ((AdPaymentTermList)adPTList.get(rowSelected)).getTermName();
       this.description = ((AdPaymentTermList)adPTList.get(rowSelected)).getDescription();
       this.baseAmount = ((AdPaymentTermList)adPTList.get(rowSelected)).getBaseAmount();
       this.monthlyInterestRate = ((AdPaymentTermList)adPTList.get(rowSelected)).getMonthlyInterestRate();
       
       this.enable = ((AdPaymentTermList)adPTList.get(rowSelected)).getEnable();
       this.enableRebate = ((AdPaymentTermList)adPTList.get(rowSelected)).getEnableRebate();
       this.enableInterest = ((AdPaymentTermList)adPTList.get(rowSelected)).getEnableInterest();
       this.discountOnInvoice = ((AdPaymentTermList)adPTList.get(rowSelected)).getDiscountOnInvoice();
       this.discountAccount = ((AdPaymentTermList)adPTList.get(rowSelected)).getDiscountAccount();
       this.discountAccountDescription = ((AdPaymentTermList)adPTList.get(rowSelected)).getDiscountAccountDescription();
       this.discountDescription = ((AdPaymentTermList)adPTList.get(rowSelected)).getDiscountDescription();
       this.scheduleBasis = ((AdPaymentTermList)adPTList.get(rowSelected)).getScheduleBasis();
   }

   public void updateAdPTRow(int rowSelected, Object newAdPTList) {

      adPTList.set(rowSelected, newAdPTList);

   }

   public void deleteAdPTList(int rowSelected) {

      adPTList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getPaymentTermCode() {
   	
   	  return paymentTermCode;
   	
   }
   
   public void setPaymentTermCode(Integer paymentTermCode) {
   	
   	  this.paymentTermCode = paymentTermCode;
   	
   }

   public String getTermName() {

      return termName;

   }

   public void setTermName(String termName) {

      this.termName = termName;

   }
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }

   public String getBaseAmount() {

      return baseAmount;

   }

   public void setBaseAmount(String baseAmount) {

      this.baseAmount = baseAmount;

   }
   
   public String getMonthlyInterestRate() {

	      return monthlyInterestRate;

	   }

	   public void setMonthlyInterestRate(String monthlyInterestRate) {

	      this.monthlyInterestRate = monthlyInterestRate;

	   }
   
   public boolean getEnable() {

      return enable;

   }

   public void setEnable(boolean enable) {

      this.enable = enable;

   }
   
   public boolean getEnableRebate() {

      return enableRebate;

   }

   public void setEnableRebate(boolean enableRebate) {

      this.enableRebate = enableRebate;

   }
   
   public boolean getEnableInterest() {

      return enableInterest;

   }

   public void setEnableInterest(boolean enableInterest) {

      this.enableInterest = enableInterest;

   }   
   
   public boolean getDiscountOnInvoice() {
   	
   	  return discountOnInvoice;
   	
   }
   
   public void setDiscountOnInvoice(boolean discountOnInvoice) {
   	
   	  this.discountOnInvoice = discountOnInvoice;
   	
   }
   
   public String getDiscountAccount() {
   	
   	  return discountAccount;
   	
   }
   
   public void setDiscountAccount(String discountAccount) {
   	
   	  this.discountAccount = discountAccount;
   	
   }
   
   public String getDiscountAccountDescription() {
   	
   	  return discountAccountDescription;
   	
   }
   
   public void setDiscountAccountDescription(String discountAccountDescription) {
   	
   	  this.discountAccountDescription = discountAccountDescription;
   	
   }
   public String getDiscountDescription() {
   	
   	  return discountDescription;
   	
   }
   
   public void setDiscountDescription(String discountDescription) {
   	
   	  this.discountDescription = discountDescription;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public String getScheduleBasis() {

	   return(scheduleBasis);

   }

   public void setScheduleBasis(String scheduleBasis) {

	   this.scheduleBasis = scheduleBasis;

   }
   
   public ArrayList getScheduleBasisList(){
	
	   return(scheduleBasisList);
	   
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      termName = null; 
      description = null;
      baseAmount = "100";
      enable = false;
      enableRebate = false;
      enableInterest = false;
      discountOnInvoice = false;
      discountAccount = null;
      discountAccountDescription = null;
      discountDescription = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null; 
      closeButton = null;
      showDetailsButton = null;
      hideDetailsButton = null;
      
      scheduleBasisList.clear();
      scheduleBasisList.add("DEFAULT");
      scheduleBasisList.add("MONTHLY");
      scheduleBasisList.add("BI-MONTHLY");
      scheduleBasis = "DEFAULT";
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(termName)) {

            errors.add("termName",
               new ActionMessage("paymentTerm.error.termNameRequired"));

         }

         if (Common.validateRequired(baseAmount)) {

            errors.add("baseAmount",
               new ActionMessage("paymentTerm.error.baseAmountRequired"));

         }
         
         if(discountOnInvoice && Common.validateRequired(discountAccount)) {
         	
                errors.add("discountAccount",
                   new ActionMessage("paymentTerm.error.discountAccountRequired"));
         	
         }
         
         if(!discountOnInvoice && !Common.validateRequired(discountAccount)) {
         	
                errors.add("discountAccount",
                   new ActionMessage("paymentTerm.error.discountAccountNotRequired"));
         	
         }
         
         if (!Common.validateMoneyFormat(baseAmount)) {

            errors.add("baseAmount",
               new ActionMessage("paymentTerm.error.baseAmountInvalid"));

         }         

      }
         
      return errors;

   }
}