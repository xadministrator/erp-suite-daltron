package com.struts.ad.paymentterm;

import java.io.Serializable;

public class AdPaymentTermList implements Serializable {

   private Integer paymentTermCode = null;
   private String termName = null;
   private String description = null;
   private String baseAmount = null;
   private String monthlyInterestRate = null;
   private boolean enable = false;
   private boolean enableRebate = false;
   private boolean enableInterest = false;
   private boolean discountOnInvoice = false;
   private String discountAccount = null;
   private String discountAccountDescription = null;
   private String discountDescription = null;
   private String scheduleBasis = null;

   private String editButton = null;
   private String deleteButton = null;
   private String paymentSchedulesButton = null;
    
   private AdPaymentTermForm parentBean;
    
   public AdPaymentTermList(AdPaymentTermForm parentBean,
      Integer paymentTermCode,
      String termName,
      String description,
      String baseAmount,
      String monthlyInterestRate,
      boolean enable,
      boolean enableRebate,
      boolean enableInterest,
	  boolean discountOnInvoice,
	  String discountAccount,
	  String discountAccountDescription,
	  String discountDescription,
	  String scheduleBasis) {

      this.parentBean = parentBean;
      this.paymentTermCode = paymentTermCode;
      this.termName = termName;
      this.description = description;
      this.baseAmount = baseAmount;
      this.monthlyInterestRate = monthlyInterestRate;
      this.enable = enable;
      this.enableRebate = enableRebate;
      this.enableInterest = enableInterest;
      this.discountOnInvoice = discountOnInvoice;
      this.discountAccount = discountAccount;
      this.discountAccountDescription = discountAccountDescription;
      this.discountDescription = discountDescription;
      this.scheduleBasis = scheduleBasis;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   
   
   public void setPaymentSchedulesButton(String paymentSchedulesButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }

   public Integer getPaymentTermCode() {

      return paymentTermCode;

   }
   
   public String getTermName() {
       
       return termName;
       
   }

   public String getDescription() {

      return description;

   }

   public String getBaseAmount() {

      return baseAmount;

   }
   
   public String getMonthlyInterestRate() {

	      return monthlyInterestRate;

	   }
   
   
   public boolean getEnable() {

      return enable;

   }
   
   public boolean getEnableRebate() {

	      return enableRebate;

	   }
   
   
   public boolean getEnableInterest() {

	      return enableInterest;

	   }
   
   public boolean getDiscountOnInvoice() {
   	
   	  return discountOnInvoice;
   	  
   }
   
   public String getDiscountAccount() {
   	
   	  return discountAccount;
   	  
   }
   
   public String getDiscountAccountDescription() {
   	  
   	  return discountAccountDescription;
   	  
   }
   
   public String getDiscountDescription() {
   	
   	  return discountDescription;
   	  
   }
   
   public String getScheduleBasis() {
	   
	  return scheduleBasis;
	  
   }

}