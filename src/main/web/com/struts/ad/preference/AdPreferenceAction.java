package com.struts.ad.preference;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlAccruedVatAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlCustomerDepositAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPOSAdjustmentAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPettyCashAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosDineInChargeAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosDiscountAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosGiftCertificateAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlPosServiceChargeAccountNotFoundException;
import com.ejb.txn.AdPreferenceController;
import com.ejb.txn.AdPreferenceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModPreferenceDetails;

public final class AdPreferenceAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdPreferenceAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         AdPreferenceForm actionForm = (AdPreferenceForm)form;

         String frParam = Common.getUserPermission(user, Constants.AD_PREFERENCE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adPreference");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdPreferenceController EJB
*******************************************************/

         AdPreferenceControllerHome homePRF = null;
         AdPreferenceController ejbPRF = null;

         try {

            homePRF = (AdPreferenceControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdPreferenceControllerEJB", AdPreferenceControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdPreferenceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbPRF = homePRF.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdPreferenceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

 /*******************************************************
     Call ArInvoiceEntryController EJB
     getGlFcPrecisionUnit
     getAdPrfArInvoiceLineNumber
     getAdPrfEnableArInvoiceBatch
     getInvGpQuantityPrecisionUnit
     getAdPrfEnableInvShift
     getAdPrfArUseCustomerPulldown
     getAdPrfArEnablePaymentTerm
     getAdPrfArDisableSalesPrice
  *******************************************************/


         short precisionUnit = 0;

         try {


        	 precisionUnit = ejbPRF.getGlFcPrecisionUnit(user.getCmpCode());

         }catch(EJBException ex) {

             if (log.isInfoEnabled()) {

                 log.info("EJBException caught in AdPreferenceAction.execute(): " + ex.getMessage() +
                         " session: " + session.getId());
             }

             return(mapping.findForward("cmnErrorPage"));

         }

/*******************************************************
   -- Ad PRF Close Action --
*******************************************************/

         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad PRF Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


            AdModPreferenceDetails mdetails = new AdModPreferenceDetails();
            mdetails.setPrfCode(actionForm.getPreferenceCode());
            mdetails.setPrfAllowSuspensePosting(Common.convertBooleanToByte(actionForm.getAllowSuspensePosting()));
            mdetails.setPrfGlJournalLineNumber(Common.convertStringToShort(actionForm.getGlJournalLineNumber()));
            mdetails.setPrfApJournalLineNumber(Common.convertStringToShort(actionForm.getApJournalLineNumber()));
            mdetails.setPrfArInvoiceLineNumber(Common.convertStringToShort(actionForm.getArInvoiceLineNumber()));
            mdetails.setPrfApWTaxRealization(actionForm.getApWTaxRealization());
            mdetails.setPrfArWTaxRealization(actionForm.getArWTaxRealization());
            mdetails.setPrfEnableGlJournalBatch(Common.convertBooleanToByte(actionForm.getEnableGlJournalBatch()));
            mdetails.setPrfEnableGlRecomputeCoaBalance(Common.convertBooleanToByte(actionForm.getEnableGlRecomputeCoaBalance()));

            mdetails.setPrfEnableApVoucherBatch(Common.convertBooleanToByte(actionForm.getEnableApVoucherBatch()));
            mdetails.setPrfEnableApPOBatch(Common.convertBooleanToByte(actionForm.getEnableApPOBatch()));

            mdetails.setPrfEnableApCheckBatch(Common.convertBooleanToByte(actionForm.getEnableApCheckBatch()));
            mdetails.setPrfEnableArInvoiceBatch(Common.convertBooleanToByte(actionForm.getEnableArInvoiceBatch()));
            mdetails.setPrfEnableArInvoiceInterestGeneration(Common.convertBooleanToByte(actionForm.getEnableArInvoiceInterestGeneration()));
            mdetails.setPrfEnableArReceiptBatch(Common.convertBooleanToByte(actionForm.getEnableArReceiptBatch()));
            mdetails.setPrfEnableArMiscReceiptBatch(Common.convertBooleanToByte(actionForm.getEnableArMiscReceiptBatch()));

            mdetails.setPrfApGlPostingType(actionForm.getApGlPostingType());
            mdetails.setPrfArGlPostingType(actionForm.getArGlPostingType());
            mdetails.setPrfCmGlPostingType(actionForm.getCmGlPostingType());
            mdetails.setPrfCmUseBankForm(Common.convertBooleanToByte(actionForm.getCmUseBankForm()));
            mdetails.setPrfInvInventoryLineNumber(Common.convertStringToShort(actionForm.getInvInventoryLineNumber()));
            mdetails.setPrfInvQuantityPrecisionUnit(Common.convertStringToShort(actionForm.getInvQuantityPrecisionUnit()));
            mdetails.setPrfInvCostPrecisionUnit(Common.convertStringToShort(actionForm.getInvCostPrecisionUnit()));
            mdetails.setPrfInvGlPostingType(actionForm.getInvGlPostingType());
            mdetails.setPrfGlPostingType(actionForm.getGlPostingType());
            mdetails.setPrfInvEnableShift(Common.convertBooleanToByte(actionForm.getEnableInvShift()));
            mdetails.setPrfEnableInvBUABatch(Common.convertBooleanToByte(actionForm.getEnableInvBUABatch()));
            mdetails.setPrfApFindCheckDefaultType(actionForm.getApFindCheckDefaultType());
            mdetails.setPrfArFindReceiptDefaultType(actionForm.getArFindReceiptDefaultType());
            mdetails.setPrfApUseAccruedVat(Common.convertBooleanToByte(actionForm.getApUseAccruedVat()));
            mdetails.setPrfApDefaultCheckDate(actionForm.getApDefaultCheckDate());
            mdetails.setPrfApDefaultChecker(actionForm.getApDefaultChecker());
            mdetails.setPrfApDefaultApprover(actionForm.getApDefaultApprover());
            mdetails.setPrfApGlCoaAccruedVatAccountNumber(actionForm.getAccruedVatAccount());
            mdetails.setPrfApGlCoaAccruedVatAccountDescription(actionForm.getAccruedVatAccountDescription());
            mdetails.setPrfApGlCoaPettyCashAccountNumber(actionForm.getPettyCashAccount());
            mdetails.setPrfApGlCoaPettyCashAccountDescription(actionForm.getPettyCashAccountDescription());
            mdetails.setPrfApUseSupplierPulldown(Common.convertBooleanToByte(actionForm.getApUseSupplierPulldown()));
            mdetails.setPrfArUseCustomerPulldown(Common.convertBooleanToByte(actionForm.getArUseCustomerPulldown()));
            mdetails.setPrfApAutoGenerateSupplierCode(Common.convertBooleanToByte(actionForm.getApAutoGenerateSupplierCode()));
            mdetails.setPrfArAutoGenerateCustomerCode(Common.convertBooleanToByte(actionForm.getArAutoGenerateCustomerCode()));
            mdetails.setPrfApNextSupplierCode(actionForm.getNextSupplierCode());
            mdetails.setPrfArNextCustomerCode(actionForm.getNextCustomerCode());
            mdetails.setPrfApReferenceNumberValidation(Common.convertBooleanToByte(actionForm.getApReferenceNumberValidation()));
            mdetails.setPrfInvEnablePosIntegration(Common.convertBooleanToByte(actionForm.getEnableInvPosIntegration()));
            mdetails.setPrfInvEnablePosAutoPostUpload(Common.convertBooleanToByte(actionForm.getEnableInvPosAutoPostUpload()));
            mdetails.setPrfApCheckVoucherDataSource(actionForm.getApCheckVoucherDataSource());
            mdetails.setPrfArGlCoaCustomerDepositAccountNumber(actionForm.getCustomerDepositAccount());
            mdetails.setPrfArGlCoaCustomerDepositAccountDescription(actionForm.getCustomerDepositAccountDescription());
            mdetails.setPrfArSalesInvoiceDataSource(actionForm.getArSalesInvoiceDataSource());

            if(actionForm.getEnableInvPosIntegration() == true) {

            	mdetails.setPrfInvGlCoaPosAdjustmentAccountNumber(actionForm.getInvPosAdjustmentAccount());

            }

        	mdetails.setPrfInvGlCoaGeneralAdjustmentAccountNumber(actionForm.getInvGeneralAdjustmentAccount());
        	mdetails.setPrfInvGlCoaIssuanceAdjustmentAccountNumber(actionForm.getInvIssuanceAdjustmentAccount());
        	mdetails.setPrfInvGlCoaProductionAdjustmentAccountNumber(actionForm.getInvProductionAdjustmentAccount());
        	mdetails.setPrfInvGlCoaWastageAdjustmentAccountNumber(actionForm.getInvWastageAccount());
        	mdetails.setPrfInvGlCoaWastageAdjustmentAccountDescription(actionForm.getInvWastageAccountDescription());
            mdetails.setPrfInvGlCoaAdjustmentRequestAccountNumber(actionForm.getInvPosAdjustmentAccount());
            mdetails.setPrfInvGlCoaAdjustmentRequestAccountDescription(actionForm.getInvPosAdjustmentAccountDescription());
            mdetails.setPrfMiscGlCoaPosDiscountAccountNumber(actionForm.getMiscPosDiscountAccount());
            mdetails.setPrfMiscGlCoaPosGiftCertificateAccountNumber(actionForm.getMiscPosGiftCertificateAccount());
            mdetails.setPrfMiscGlCoaPosServiceChargeAccountNumber(actionForm.getMiscPosServiceChargeAccount());
            mdetails.setPrfMiscGlCoaPosDineInChargeAccountNumber(actionForm.getMiscPosDineInChargeAccount());
            mdetails.setPrfApDefaultPrTax(actionForm.getApDefaultPRTaxCode());
            mdetails.setPrfApDefaultPrCurrency(actionForm.getApDefaultPRCurrency());
            mdetails.setPrfInvGlCoaVarianceAccountNumber(actionForm.getInvVarianceAccount());
            mdetails.setPrfArAutoComputeCogs(Common.convertBooleanToByte(actionForm.getArAutoComputeCogs()));

            mdetails.setPrfArMonthlyInterestRate(Common.convertStringMoneyToDouble(actionForm.getArMonthlyInterestRate(), precisionUnit));
            mdetails.setPrfApAgingBucket(Common.convertStringToInt(actionForm.getApAgingBucket()));
            mdetails.setPrfArAgingBucket(Common.convertStringToInt(actionForm.getArAgingBucket()));
            mdetails.setPrfArAllowPriorDate(Common.convertBooleanToByte(actionForm.getArAllowPriorDate()));
            mdetails.setPrfApShowPrCost(Common.convertBooleanToByte(actionForm.getApShowPRCost()));
            mdetails.setPrfApDebitMemoOverrideCost(Common.convertBooleanToByte(actionForm.getApDebitMemoOverrideCost()));
            mdetails.setPrfGlYearEndCloseRestriction(Common.convertBooleanToByte(actionForm.getGlYearEndCloseRestriction()));
            mdetails.setPrfArEnablePaymentTerm(Common.convertBooleanToByte(actionForm.getArEnablePaymentTerm()));
            mdetails.setPrfArDisableSalesPrice(Common.convertBooleanToByte(actionForm.getArDisableSalesPrice()));
            mdetails.setPrfInvItemLocationShowAll(Common.convertBooleanToByte(actionForm.getInvItemLocationShowAll()));
            mdetails.setPrfInvItemLocationAddByItemList(Common.convertBooleanToByte(actionForm.getInvItemLocationAddByItemList()));
            mdetails.setPrfArValidateCustomerEmail(Common.convertBooleanToByte(actionForm.getArValidateCustomerEmail()));
            mdetails.setPrfArCheckInsufficientStock(Common.convertBooleanToByte(actionForm.getArCheckStock()));
            mdetails.setPrfArDetailedReceivable(Common.convertBooleanToByte(actionForm.getArDetailedReceivable()));
           /* mdetails.setPrfInvGlCoaSalesTaxAccountNumber(actionForm.getInvSalesTaxCodeAccount());
            mdetails.setPrfInvGlCoaPreparedFoodTaxAccountNumber(actionForm.getInvPreparedFoodTaxCodeAccount());*/

            mdetails.setPrfAdDisableMultipleLogin(Common.convertBooleanToByte(actionForm.getAdDisableMultipleLogin()));
            mdetails.setPrfAdEnableEmailNotification(Common.convertBooleanToByte(actionForm.getAdEnableEmailNotification()));

            
            mdetails.setPrfArSoSalespersonRequired(Common.convertBooleanToByte(actionForm.getArSoSalespersonRequired()));
            mdetails.setPrfArInvcSalespersonRequired(Common.convertBooleanToByte(actionForm.getArInvcSalespersonRequired()));
            
            mdetails.setPrfMailHost(actionForm.getMailHost());
            mdetails.setPrfMailSocketFactoryPort(actionForm.getMailSocketFactoryPort());
            mdetails.setPrfMailPort(actionForm.getMailPort());
            mdetails.setPrfMailFrom(actionForm.getMailFrom());
            mdetails.setPrfMailAuthenticator(Common.convertBooleanToByte(actionForm.getMailAuthenticator()));
            mdetails.setPrfMailPassword(actionForm.getMailPassword());
            mdetails.setPrfMailTo(actionForm.getMailTo());
            mdetails.setPrfMailCc(actionForm.getMailCc());
            mdetails.setPrfMailBcc(actionForm.getMailBcc());
            mdetails.setPrfMailConfig(actionForm.getMailConfig());
            mdetails.setPrfCentralWarehouseBranch(actionForm.getCentralWarehouseBranch());
            mdetails.setPrfCentralWarehouseLocation(actionForm.getCentralWarehouseLocation());
            

            try {

            	ejbPRF.saveAdPrfEntry(mdetails, actionForm.getDefaultWTaxCode(), user.getCmpCode());

                Common.setDateFormat(actionForm.getDateFormatInput(), actionForm.getDateFormatOutput());
            } catch (AdPRFCoaGlAccruedVatAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.accruedVatAccountNotFound"));

            } catch (AdPRFCoaGlPettyCashAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.pettyCashAccountNotFound"));

            } catch (AdPRFCoaGlPOSAdjustmentAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.POSAdjustmentAccountNotFound"));

            } catch (AdPRFCoaGlPosDiscountAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.POSDiscountAccountNotFound"));

            } catch (AdPRFCoaGlPosGiftCertificateAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.POSGiftCertificateAccountNotFound"));

            } catch (AdPRFCoaGlPosServiceChargeAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.POSServiceChargeAccountNotFound"));

            } catch (AdPRFCoaGlPosDineInChargeAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.POSDineInChargeAccountNotFound"));

            } catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("preference.error.customerDepositAccountNotFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdPreferenceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

/*******************************************************
   -- Ad PRF Load Action --
*******************************************************/

         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adPreference");

            }

	        try {

	        	actionForm.clearDefaultWTaxCodeList();

            	ArrayList list = ejbPRF.getArWtcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setDefaultWTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setDefaultWTaxCodeList((String)i.next());

            		}

            	}

            	actionForm.clearApDefaultPRTaxCodeList();

            	list = ejbPRF.getArTcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setApDefaultPRTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setApDefaultPRTaxCodeList((String)i.next());

            		}

            	}

            	actionForm.clearApDefaultPRCurrencyList();

            	list = ejbPRF.getGlFcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setApDefaultPRCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setApDefaultPRCurrencyList((String)i.next());

            		}

            	}

                actionForm.clearBranchList();
                
                list = ejbPRF.getAdBranchAll(user.getCmpCode());
                
                if (list == null || list.size() == 0) {

                     list = new ArrayList();
                    list.add(Constants.GLOBAL_NO_RECORD_FOUND);
               

                }  
                 actionForm.setBranchList(list);
                
                
                actionForm.clearLocationList();
                
                list = ejbPRF.getInvLocationAll(user.getCmpCode());
                
                if (list == null || list.size() == 0) {

                    list = new ArrayList();
                    list.add(Constants.GLOBAL_NO_RECORD_FOUND);
                   
                } 
                actionForm.setLocationList(list);

                
                
                
                
                
            	AdModPreferenceDetails mdetails = ejbPRF.getAdPrf(user.getCmpCode());

	            actionForm.setAllowSuspensePosting(Common.convertByteToBoolean(mdetails.getPrfAllowSuspensePosting()));
	            actionForm.setGlJournalLineNumber(Common.convertShortToString(mdetails.getPrfGlJournalLineNumber()));
	            actionForm.setApJournalLineNumber(Common.convertShortToString(mdetails.getPrfApJournalLineNumber()));
	            actionForm.setArInvoiceLineNumber(Common.convertShortToString(mdetails.getPrfArInvoiceLineNumber()));
	            actionForm.setApWTaxRealization(mdetails.getPrfApWTaxRealization());
	            actionForm.setArWTaxRealization(mdetails.getPrfArWTaxRealization());
	            actionForm.setEnableGlJournalBatch(Common.convertByteToBoolean(mdetails.getPrfEnableGlJournalBatch()));
	            actionForm.setEnableGlRecomputeCoaBalance(Common.convertByteToBoolean(mdetails.getPrfEnableGlRecomputeCoaBalance()));
	            actionForm.setEnableApVoucherBatch(Common.convertByteToBoolean(mdetails.getPrfEnableApVoucherBatch()));
	            actionForm.setEnableApPOBatch(Common.convertByteToBoolean(mdetails.getPrfEnableApPOBatch()));

                    actionForm.setEnableApCheckBatch(Common.convertByteToBoolean(mdetails.getPrfEnableApCheckBatch()));
	            actionForm.setEnableArInvoiceBatch(Common.convertByteToBoolean(mdetails.getPrfEnableArInvoiceBatch()));
	            actionForm.setEnableArInvoiceInterestGeneration(Common.convertByteToBoolean(mdetails.getPrfEnableArInvoiceInterestGeneration()));

	            actionForm.setEnableArReceiptBatch(Common.convertByteToBoolean(mdetails.getPrfEnableArReceiptBatch()));
	            actionForm.setEnableArMiscReceiptBatch(Common.convertByteToBoolean(mdetails.getPrfEnableArMiscReceiptBatch()));

	            actionForm.setApGlPostingType(mdetails.getPrfApGlPostingType());
	            actionForm.setArGlPostingType(mdetails.getPrfArGlPostingType());
	            actionForm.setCmGlPostingType(mdetails.getPrfCmGlPostingType());
	            actionForm.setCmUseBankForm(Common.convertByteToBoolean(mdetails.getPrfCmUseBankForm()));
	            actionForm.setInvInventoryLineNumber(Common.convertShortToString(mdetails.getPrfInvInventoryLineNumber()));
	            actionForm.setInvQuantityPrecisionUnit(Common.convertShortToString(mdetails.getPrfInvQuantityPrecisionUnit()));
	            actionForm.setInvCostPrecisionUnit(Common.convertShortToString(mdetails.getPrfInvCostPrecisionUnit()));
            	actionForm.setInvGlPostingType(mdetails.getPrfInvGlPostingType());
            	actionForm.setGlPostingType(mdetails.getPrfGlPostingType());
            	actionForm.setEnableInvShift(Common.convertByteToBoolean(mdetails.getPrfInvEnableShift()));
            	actionForm.setEnableInvBUABatch(Common.convertByteToBoolean(mdetails.getPrfEnableInvBUABatch()));
            	actionForm.setApFindCheckDefaultType(mdetails.getPrfApFindCheckDefaultType());
            	actionForm.setArFindReceiptDefaultType(mdetails.getPrfArFindReceiptDefaultType());
            	actionForm.setDefaultWTaxCode(mdetails.getPrfWtcName());
            	actionForm.setApUseAccruedVat(Common.convertByteToBoolean(mdetails.getPrfApUseAccruedVat()));
            	actionForm.setApDefaultCheckDate(mdetails.getPrfApDefaultCheckDate());
            	actionForm.setApDefaultChecker(mdetails.getPrfApDefaultChecker());
            	actionForm.setApDefaultApprover(mdetails.getPrfApDefaultApprover());
            	actionForm.setAccruedVatAccount(mdetails.getPrfApGlCoaAccruedVatAccountNumber());
            	actionForm.setAccruedVatAccountDescription(mdetails.getPrfApGlCoaAccruedVatAccountDescription());
            	actionForm.setPettyCashAccount(mdetails.getPrfApGlCoaPettyCashAccountNumber());
            	actionForm.setPettyCashAccountDescription(mdetails.getPrfApGlCoaPettyCashAccountDescription());
            	actionForm.setApUseSupplierPulldown(Common.convertByteToBoolean(mdetails.getPrfApUseSupplierPulldown()));
            	actionForm.setArUseCustomerPulldown(Common.convertByteToBoolean(mdetails.getPrfArUseCustomerPulldown()));
            	actionForm.setApAutoGenerateSupplierCode(Common.convertByteToBoolean(mdetails.getPrfApAutoGenerateSupplierCode()));
	            actionForm.setArAutoGenerateCustomerCode(Common.convertByteToBoolean(mdetails.getPrfArAutoGenerateCustomerCode()));
	            actionForm.setNextSupplierCode(mdetails.getPrfApNextSupplierCode());
	            actionForm.setNextCustomerCode(mdetails.getPrfArNextCustomerCode());
	            actionForm.setApReferenceNumberValidation(Common.convertByteToBoolean(mdetails.getPrfApReferenceNumberValidation()));
	            actionForm.setEnableInvPosIntegration(Common.convertByteToBoolean(mdetails.getPrfInvEnablePosIntegration()));
	            actionForm.setEnableInvPosAutoPostUpload(Common.convertByteToBoolean(mdetails.getPrfInvEnablePosAutoPostUpload()));

	            actionForm.setApCheckVoucherDataSource(mdetails.getPrfApCheckVoucherDataSource());
	            actionForm.setCustomerDepositAccount(mdetails.getPrfArGlCoaCustomerDepositAccountNumber());
            	actionForm.setCustomerDepositAccountDescription(mdetails.getPrfArGlCoaCustomerDepositAccountDescription());
            	actionForm.setArSalesInvoiceDataSource(mdetails.getPrfArSalesInvoiceDataSource());

	            if(actionForm.getEnableInvPosIntegration() == true || mdetails.getPrfInvGlCoaPosAdjustmentAccountNumber()!=null) {

	            	actionForm.setInvPosAdjustmentAccount(mdetails.getPrfInvGlCoaPosAdjustmentAccountNumber());
	            	actionForm.setInvPosAdjustmentAccountDescription(mdetails.getPrfInvGlCoaPosAdjustmentAccountDescription());

	            }

	            actionForm.setMiscPosDiscountAccount(mdetails.getPrfMiscGlCoaPosDiscountAccountNumber());
	            actionForm.setMiscPosDiscountAccountDescription(mdetails.getPrfMiscGlCoaPosDiscountAccountDescription());
	            actionForm.setMiscPosGiftCertificateAccount(mdetails.getPrfMiscGlCoaPosGiftCertificateAccountNumber());
	            actionForm.setMiscPosGiftCertificateAccountDescription(mdetails.getPrfMiscGlCoaPosGiftCertificateAccountDescription());
	            actionForm.setMiscPosServiceChargeAccount(mdetails.getPrfMiscGlCoaPosServiceChargeAccountNumber());
	            actionForm.setMiscPosServiceChargeAccountDescription(mdetails.getPrfMiscGlCoaPosServiceChargeAccountDescription());
	            actionForm.setMiscPosDineInChargeAccount(mdetails.getPrfMiscGlCoaPosDineInChargeAccountNumber());
	            actionForm.setMiscPosDineInChargeAccountDescription(mdetails.getPrfMiscGlCoaPosDineInChargeAccountDescription());

	            actionForm.setApDefaultPRTaxCode(mdetails.getPrfApDefaultPrTax());
	            actionForm.setApDefaultPRCurrency(mdetails.getPrfApDefaultPrCurrency());
	            actionForm.setInvVarianceAccount(mdetails.getPrfInvGlCoaVarianceAccountNumber());
	            actionForm.setInvVarianceAccountDescription(mdetails.getPrfInvGlCoaVarianceAccountDescription());
	            actionForm.setInvGeneralAdjustmentAccount(mdetails.getPrfInvGlCoaGeneralAdjustmentAccountNumber());
	            actionForm.setInvGeneralAdjustmentAccountDescription(mdetails.getPrfInvGlCoaGeneralAdjustmentAccountDescription());
	            actionForm.setInvIssuanceAdjustmentAccount(mdetails.getPrfInvGlCoaIssuanceAdjustmentAccountNumber());
	            actionForm.setInvIssuanceAdjustmentAccountDescription(mdetails.getPrfInvGlCoaIssuanceAdjustmentAccountDescription());
	            actionForm.setInvProductionAdjustmentAccount(mdetails.getPrfInvGlCoaProductionAdjustmentAccountNumber());
	            actionForm.setInvProductionAdjustmentAccountDescription(mdetails.getPrfInvGlCoaProductionAdjustmentAccountDescription());
	            actionForm.setInvWastageAccount(mdetails.getPrfInvGlCoaWastageAdjustmentAccountNumber());
	            actionForm.setInvWastageAccountDescription(mdetails.getPrfInvGlCoaWastageAdjustmentAccountDescription());
	            actionForm.setArAutoComputeCogs(Common.convertByteToBoolean(mdetails.getPrfArAutoComputeCogs()));

	            actionForm.setArMonthlyInterestRate((Common.convertDoubleToStringMoney(mdetails.getPrfArMonthlyInterestRate(), precisionUnit) ));

	            actionForm.setApAgingBucket(String.valueOf(mdetails.getPrfApAgingBucket()));
	            actionForm.setArAgingBucket(String.valueOf(mdetails.getPrfArAgingBucket()));
	            actionForm.setArAllowPriorDate(Common.convertByteToBoolean(mdetails.getPrfArAllowPriorDate()));
	            actionForm.setApShowPRCost(Common.convertByteToBoolean(mdetails.getPrfApShowPrCost()));
	            actionForm.setApDebitMemoOverrideCost(Common.convertByteToBoolean(mdetails.getPrfApDebitMemoOverrideCost()));
	            actionForm.setGlYearEndCloseRestriction(Common.convertByteToBoolean(mdetails.getPrfGlYearEndCloseRestriction()));
	            actionForm.setArEnablePaymentTerm(Common.convertByteToBoolean(mdetails.getPrfArEnablePaymentTerm()));
	            actionForm.setArDisableSalesPrice(Common.convertByteToBoolean(mdetails.getPrfArDisableSalesPrice()));
	            actionForm.setInvItemLocationShowAll(Common.convertByteToBoolean(mdetails.getPrfInvItemLocationShowAll()));
	            actionForm.setInvItemLocationAddByItemList(Common.convertByteToBoolean(mdetails.getPrfInvItemLocationAddByItemList()));
	            actionForm.setArValidateCustomerEmail(Common.convertByteToBoolean(mdetails.getPrfArValidateCustomerEmail()));
	            actionForm.setArCheckStock(Common.convertByteToBoolean(mdetails.getPrfArCheckInsufficientStock()));
	            actionForm.setArDetailedReceivable(Common.convertByteToBoolean(mdetails.getPrfArDetailedReceivable()));
	            /* actionForm.setInvSalesTaxCodeAccount(mdetails.getPrfInvGlCoaSalesTaxAccountNumber());
	            actionForm.setInvSalesTaxCodeAccountDescription(mdetails.getPrfInvGlCoaSalesTaxAccountDescription());
	            actionForm.setInvPreparedFoodTaxCodeAccount(mdetails.getPrfInvGlCoaPreparedFoodTaxAccountNumber());
	            actionForm.setInvPreparedFoodTaxCodeAccountDescription(mdetails.getPrfInvGlCoaPreparedFoodTaxAccountDescription());*/

	            actionForm.setAdDisableMultipleLogin(Common.convertByteToBoolean(mdetails.getPrfAdDisableMultipleLogin()));
	            actionForm.setAdEnableEmailNotification(Common.convertByteToBoolean(mdetails.getPrfAdEnableEmailNotification()));
                actionForm.setDateFormatInput(Common.getDateFormatInput());
                actionForm.setDateFormatOutput(Common.getDateFormatOutput());

                actionForm.setMailHost(mdetails.getPrfMailHost());
                actionForm.setMailSocketFactoryPort(mdetails.getPrfMailSocketFactoryPort());
                actionForm.setMailPort(mdetails.getPrfMailPort());
                actionForm.setMailFrom(mdetails.getPrfMailFrom());
                actionForm.setMailAuthenticator(Common.convertByteToBoolean(mdetails.getPrfMailAuthenticator()));
                actionForm.setMailPassword(mdetails.getPrfMailPassword());
                actionForm.setMailTo(mdetails.getPrfMailTo());
                actionForm.setMailCc(mdetails.getPrfMailCc());
                actionForm.setMailBcc(mdetails.getPrfMailBcc());
                actionForm.setMailConfig(mdetails.getPrfMailConfig());
                actionForm.setCentralWarehouseBranch(mdetails.getPrfCentralWarehouseBranch());
                actionForm.setCentralWarehouseLocation(mdetails.getPrfCentralWarehouseLocation());
	        } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdPreferenceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("adPreference"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdPreferenceAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }

}