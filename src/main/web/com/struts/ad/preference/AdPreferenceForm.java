	package com.struts.ad.preference;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdPreferenceForm extends ActionForm implements Serializable {

	private Integer preferenceCode = null;
	private boolean allowSuspensePosting = false;
	private String glJournalLineNumber = null;
	private String apJournalLineNumber = null;
	private String arInvoiceLineNumber = null;
	private String apWTaxRealization = null;
	private ArrayList apWTaxRealizationList = new ArrayList();
	private String arWTaxRealization = null;
	private ArrayList arWTaxRealizationList = new ArrayList();
	private boolean enableGlJournalBatch = false;
	private boolean enableGlRecomputeCoaBalance = false;

	private boolean enableApVoucherBatch = false;
    private boolean enableApPOBatch = false;
	private boolean enableApCheckBatch = false;
	private boolean enableArInvoiceBatch = false;
	private boolean enableArInvoiceInterestGeneration = false;
	private boolean enableArReceiptBatch = false;
	private boolean enableArMiscReceiptBatch = false;
	private String apGlPostingType = null;
	private ArrayList apGlPostingTypeList = new ArrayList();
	private String arGlPostingType = null;
	private ArrayList arGlPostingTypeList = new ArrayList();
	private String cmGlPostingType = null;
	private ArrayList cmGlPostingTypeList = new ArrayList();
	private String apFindCheckDefaultType = null;
	private ArrayList apFindCheckDefaultTypeList = new ArrayList();
	private String arFindReceiptDefaultType = null;
	private ArrayList arFindReceiptDefaultTypeList = new ArrayList();
	private String invInventoryLineNumber = null;
	private String invQuantityPrecisionUnit = null;
	private String invCostPrecisionUnit = null;
	private String invGlPostingType = null;
	private ArrayList invGlPostingTypeList = new ArrayList();
	private String glPostingType = null;
	private ArrayList glPostingTypeList = new ArrayList();
	private boolean enableInvShift = false;
	private boolean enableInvBUABatch = false;


	private boolean apUseAccruedVat = false;
	private String apDefaultCheckDate = null;
	private ArrayList apDefaultCheckDateList = new ArrayList();
	private String apDefaultChecker = null;
	private String apDefaultApprover = null;
	private String accruedVatAccount = null;
	private String accruedVatAccountDescription = null;
	private String pettyCashAccount = null;
	private String pettyCashAccountDescription = null;
	private ArrayList defaultWTaxCodeList = new ArrayList();
	private String defaultWTaxCode = null;
	private boolean cmUseBankForm = false;
	private boolean apUseSupplierPulldown = false;
	private boolean arUseCustomerPulldown = false;
	private boolean apAutoGenerateSupplierCode = false;
	private boolean arAutoGenerateCustomerCode = false;
	private boolean enableNextSupplierCode = false;
	private boolean enableNextCustomerCode = false;
	private String saveButton = null;
	private String closeButton = null;
	private String userPermission = new String();
	private String txnStatus = new String();
	private String nextSupplierCode = null;
	private String nextCustomerCode = null;
	private boolean apReferenceNumberValidation = false;
	private boolean enableInvPosIntegration = false;
	private boolean enableInvPosAutoPostUpload = false;
	private String invPosAdjustmentAccount = null;
	private String invPosAdjustmentAccountDescription = null;
	private String invGeneralAdjustmentAccount = null;
	private String invGeneralAdjustmentAccountDescription = null;
	private String invIssuanceAdjustmentAccount = null;
	private String invIssuanceAdjustmentAccountDescription = null;
	private String invProductionAdjustmentAccount = null;
	private String invProductionAdjustmentAccountDescription = null;
	private String invAdjustmentRequestAccount = null;
	private String invAdjustmentRequestAccountDescription = null;
	private String apCheckVoucherDataSource = null;
	private ArrayList apCheckVoucherDataSourceList = new ArrayList();
	private String arSalesInvoiceDataSource = null;
	private ArrayList arSalesInvoiceDataSourceList = new ArrayList();
	private String customerDepositAccount = null;
	private String customerDepositAccountDescription = null;
	private String miscPosDiscountAccount = null;
	private String miscPosDiscountAccountDescription = null;
	private String miscPosGiftCertificateAccount = null;
	private String miscPosGiftCertificateAccountDescription = null;
	private String miscPosServiceChargeAccount = null;
	private String miscPosServiceChargeAccountDescription = null;
	private String miscPosDineInChargeAccount = null;
	private String miscPosDineInChargeAccountDescription = null;
	private String apDefaultPRTaxCode = null;
	private ArrayList apDefaultPRTaxCodeList = new ArrayList();
	private String apDefaultPRCurrency = null;
	private ArrayList apDefaultPRCurrencyList = new ArrayList();
	private String invVarianceAccount = null;
	private String invVarianceAccountDescription = null;
	private String invWastageAccount = null;
	private String invWastageAccountDescription = null;
	private String invSalesTaxCodeAccount = null;
	private String invSalesTaxCodeAccountDescription = null;
	private String invPreparedFoodTaxCodeAccount = null;
	private String invPreparedFoodTaxCodeAccountDescription = null;
	private boolean arAutoComputeCogs = false;

	private String arMonthlyInterestRate = null;

	private String apAgingBucket = null;
	private String arAgingBucket = null;
	private boolean arAllowPriorDate = false;
	private boolean apShowPRCost = false;
	private boolean apDebitMemoOverrideCost = false;
	private boolean arEnablePaymentTerm = false;
	private boolean arDisableSalesPrice = false;
	private boolean invItemLocationShowAll = false;
	private boolean invItemLocationAddByItemList = false;
	private boolean arValidateCustomerEmail = false;
	private boolean arCheckStock = false;
	private boolean arDetailedReceivable = false;
	private boolean glYearEndCloseRestriction = false;

	private boolean adDisableMultipleLogin = false;
	private boolean adEnableEmailNotification = false;

    private String dateFormatInput = null;
    private String dateFormatOutput = null;
    private ArrayList dateFormatList = new ArrayList();


    private boolean arSoSalespersonRequired = false;
    private boolean arInvcSalespersonRequired = false;
    
    private String mailHost = null;
    private String mailSocketFactoryPort = null;
    private String mailPort = null;
    private String mailFrom = null;
    private boolean mailAuthenticator = false;
    private String mailPassword = null;
    private String mailTo = null;
    private String mailCc = null;
    private String mailBcc = null;

    private String mailConfig = null;
    
    private ArrayList branchList = new ArrayList();
    private String centralWarehouseBranch = null;
    
    private ArrayList locationList = new ArrayList();
    private String centralWarehouseLocation = null;
    
    
    public ArrayList getBranchList(){
        return this.branchList;
    }
    
    public void setBranchList(ArrayList branchList){
        this.branchList = branchList;
    }
    
    public void clearBranchList(){
        this.branchList.clear();
    }
    
    public ArrayList getLocationList(){
        return this.locationList;
    }
    
    public void setLocationList(ArrayList locationList){
        this.locationList = locationList;
    }
    
    public void clearLocationList(){
        this.locationList.clear();
    }
    
    public String getCentralWarehouseBranch(){
        return this.centralWarehouseBranch;
    }
    
    public void setCentralWarehouseBranch(String centralWarehouseBranch){
        this.centralWarehouseBranch = centralWarehouseBranch;
    }
    
    public String getCentralWarehouseLocation(){
        return this.centralWarehouseLocation;
    }
    
    public void setCentralWarehouseLocation(String centralWarehouseLocation){
    
        this.centralWarehouseLocation = centralWarehouseLocation;
    }

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;

	}

	public void setTxnStatus(String txnStatus) {

	  	this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

	  	return userPermission;

	}

	public void setUserPermission(String userPermission) {

	  	this.userPermission = userPermission;

	}

   public Integer getPreferenceCode() {

   	  return preferenceCode;

   }

   public void setPreferenceCode(Integer preferenceCode) {

   	  this.preferenceCode = preferenceCode;

   }

   public boolean getAllowSuspensePosting() {

      return allowSuspensePosting;

   }

   public void setAllowSuspensePosting(boolean allowSuspensePosting) {

      this.allowSuspensePosting = allowSuspensePosting;

   }

   public String getGlJournalLineNumber() {

   	  return glJournalLineNumber;

   }

   public void setGlJournalLineNumber(String glJournalLineNumber) {

   	  this.glJournalLineNumber = glJournalLineNumber;

   }

   public String getApJournalLineNumber() {

   	  return apJournalLineNumber;

   }

   public void setApJournalLineNumber(String apJournalLineNumber) {

   	  this.apJournalLineNumber = apJournalLineNumber;

   }

   public String getArInvoiceLineNumber() {

   	  return arInvoiceLineNumber;

   }

   public void setArInvoiceLineNumber(String arInvoiceLineNumber) {

   	  this.arInvoiceLineNumber = arInvoiceLineNumber;

   }

   public String getApWTaxRealization() {

   	  return apWTaxRealization;


   }

   public void setApWTaxRealization(String apWTaxRealization) {

   	  this.apWTaxRealization = apWTaxRealization;

   }

   public ArrayList getApWTaxRealizationList() {

   	  return apWTaxRealizationList;

   }

   public String getArWTaxRealization() {

   	  return arWTaxRealization;


   }

   public void setArWTaxRealization(String arWTaxRealization) {

   	  this.arWTaxRealization = arWTaxRealization;

   }

   public ArrayList getArWTaxRealizationList() {

   	  return arWTaxRealizationList;

   }

   public boolean getEnableGlJournalBatch() {

      return enableGlJournalBatch;

   }

   public void setEnableGlJournalBatch(boolean enableGlJournalBatch) {

      this.enableGlJournalBatch = enableGlJournalBatch;

   }

   public boolean getEnableGlRecomputeCoaBalance() {

	      return enableGlRecomputeCoaBalance;

	   }

	   public void setEnableGlRecomputeCoaBalance(boolean enableGlRecomputeCoaBalance) {

	      this.enableGlRecomputeCoaBalance = enableGlRecomputeCoaBalance;

	   }

   public boolean getEnableApVoucherBatch() {

      return enableApVoucherBatch;

   }

   public void setEnableApVoucherBatch(boolean enableApVoucherBatch) {

      this.enableApVoucherBatch = enableApVoucherBatch;

   }


    public boolean getEnableApPOBatch() {

      return enableApPOBatch;

   }

   public void setEnableApPOBatch(boolean enableApPOBatch) {

      this.enableApPOBatch = enableApPOBatch;

   }
   public boolean getEnableApCheckBatch() {

      return enableApCheckBatch;

   }

   public void setEnableApCheckBatch(boolean enableApCheckBatch) {

      this.enableApCheckBatch = enableApCheckBatch;

   }


   public boolean getEnableArInvoiceBatch() {

      return enableArInvoiceBatch;

   }

   public void setEnableArInvoiceBatch(boolean enableArInvoiceBatch) {

      this.enableArInvoiceBatch = enableArInvoiceBatch;

   }

   public boolean getEnableArInvoiceInterestGeneration() {

	      return enableArInvoiceInterestGeneration;

	   }

	   public void setEnableArInvoiceInterestGeneration(boolean enableArInvoiceInterestGeneration) {

	      this.enableArInvoiceInterestGeneration = enableArInvoiceInterestGeneration;

	   }


   public boolean getEnableArReceiptBatch() {

      return enableArReceiptBatch;

   }

   public void setEnableArReceiptBatch(boolean enableArReceiptBatch) {

      this.enableArReceiptBatch = enableArReceiptBatch;

   }

   public boolean getEnableArMiscReceiptBatch() {

	      return enableArMiscReceiptBatch;

	   }

	   public void setEnableArMiscReceiptBatch(boolean enableArMiscReceiptBatch) {

	      this.enableArMiscReceiptBatch = enableArMiscReceiptBatch;

	   }

   public String getApGlPostingType() {

   	  return apGlPostingType;


   }

   public void setApGlPostingType(String apGlPostingType) {

   	  this.apGlPostingType = apGlPostingType;

   }

   public ArrayList getApGlPostingTypeList() {

   	  return apGlPostingTypeList;

   }

   public String getArGlPostingType() {

   	  return arGlPostingType;


   }

   public void setArGlPostingType(String arGlPostingType) {

   	  this.arGlPostingType = arGlPostingType;

   }

   public ArrayList getArGlPostingTypeList() {

   	  return arGlPostingTypeList;

   }

   public String getCmGlPostingType() {

   	  return cmGlPostingType;


   }

   public void setCmGlPostingType(String cmGlPostingType) {

   	  this.cmGlPostingType = cmGlPostingType;

   }

   public ArrayList getCmGlPostingTypeList() {

   	  return cmGlPostingTypeList;

   }

   public String getApFindCheckDefaultType() {

   	  return apFindCheckDefaultType;

   }

   public void setApFindCheckDefaultType(String apFindCheckDefaultType) {

      this.apFindCheckDefaultType = apFindCheckDefaultType;

   }

   public ArrayList getApFindCheckDefaultTypeList() {

   	  return apFindCheckDefaultTypeList;

   }

   public String getArFindReceiptDefaultType() {

   	  return arFindReceiptDefaultType;

   }

   public void setArFindReceiptDefaultType(String arFindReceiptDefaultType) {

      this.arFindReceiptDefaultType = arFindReceiptDefaultType;

   }

   public ArrayList getArFindReceiptDefaultTypeList() {

   	  return arFindReceiptDefaultTypeList;

   }

   public String getInvInventoryLineNumber() {

   		return invInventoryLineNumber;

   }

   public void setInvInventoryLineNumber(String invInventoryLineNumber) {

   		this.invInventoryLineNumber = invInventoryLineNumber;

   }

   public String getInvQuantityPrecisionUnit() {

   		return invQuantityPrecisionUnit;

   }

   public void setInvQuantityPrecisionUnit(String invQuantityPrecisionUnit) {

   		this.invQuantityPrecisionUnit = invQuantityPrecisionUnit;

   }

   public String getInvCostPrecisionUnit() {

  		return invCostPrecisionUnit;

  }

  public void setInvCostPrecisionUnit(String invCostPrecisionUnit) {

  		this.invCostPrecisionUnit = invCostPrecisionUnit;

  }

   public String getInvGlPostingType() {

   	  return invGlPostingType;

   }

   public void setInvGlPostingType(String invGlPostingType) {

   	  this.invGlPostingType = invGlPostingType;

   }

   public ArrayList getInvGlPostingTypeList() {

   	  return invGlPostingTypeList;

   }

   public String getGlPostingType() {

   	  return glPostingType;


   }

   public void setGlPostingType(String glPostingType) {

   	  this.glPostingType = glPostingType;

   }

   public ArrayList getGlPostingTypeList() {

   	  return glPostingTypeList;

   }

   public boolean getEnableInvShift() {

   		return enableInvShift;

   }

   public void setEnableInvShift(boolean enableInvShift) {

   		this.enableInvShift = enableInvShift;

   }

   public boolean getEnableInvBUABatch() {

  		return enableInvBUABatch;

  }

  public void setEnableInvBUABatch(boolean enableInvBUABatch) {

  		this.enableInvBUABatch = enableInvBUABatch;

  }

   public ArrayList getDefaultWTaxCodeList() {

   	    return defaultWTaxCodeList;

   }

   public void setDefaultWTaxCodeList(String defaultWTaxCode) {

   	    defaultWTaxCodeList.add(defaultWTaxCode);

   }

   public void clearDefaultWTaxCodeList() {

   	   defaultWTaxCodeList.clear();
       defaultWTaxCodeList.add(Constants.GLOBAL_BLANK);

   }

   public String getDefaultWTaxCode() {

   	   return defaultWTaxCode;

   }

   public void setDefaultWTaxCode(String defaultWTaxCode) {

   	   this.defaultWTaxCode = defaultWTaxCode;

   }

   public boolean getApUseAccruedVat() {

   	   return apUseAccruedVat;

   }

   public void setApUseAccruedVat(boolean apUseAccruedVat) {

   		this.apUseAccruedVat = apUseAccruedVat;

   }

   public ArrayList getApDefaultCheckDateList() {

   		return apDefaultCheckDateList;

   }

   public String getApDefaultCheckDate() {

   	 	return apDefaultCheckDate;

   }

   public void setApDefaultCheckDate(String apDefaultCheckDate) {

   	 	this.apDefaultCheckDate = apDefaultCheckDate;

   }

   public String getApDefaultChecker() {

   		return apDefaultChecker;

   }

   public void setApDefaultChecker(String apDefaultChecker) {

   		this.apDefaultChecker = apDefaultChecker;

   }

   public String getApDefaultApprover() {

		return apDefaultApprover;

   }

   public void setApDefaultApprover(String apDefaultApprover) {

			this.apDefaultApprover = apDefaultApprover;

   }

   public String getAccruedVatAccount() {

   		return accruedVatAccount;

   }

   public void setAccruedVatAccount(String accruedVatAccount) {

   		this.accruedVatAccount = accruedVatAccount;

   }

   public String getAccruedVatAccountDescription() {

   		return accruedVatAccountDescription;

   }

   public void setAccruedVatAccountDescription(String accruedVatAccountDescription) {

   		this.accruedVatAccountDescription = accruedVatAccountDescription;

   }

   public boolean getCmUseBankForm() {

   	   return cmUseBankForm;

   }

   public void setCmUseBankForm(boolean cmUseBankForm) {

   		this.cmUseBankForm = cmUseBankForm;

   }

   public String getPettyCashAccount() {

   		return pettyCashAccount;

   }

   public void setPettyCashAccount(String pettyCashAccount) {

   		this.pettyCashAccount = pettyCashAccount;

   }

   public String getPettyCashAccountDescription() {

   		return pettyCashAccountDescription;

   }

   public void setPettyCashAccountDescription(String pettyCashAccountDescription) {

   		this.pettyCashAccountDescription = pettyCashAccountDescription;

   }

   public boolean getApUseSupplierPulldown() {

   	   	return apUseSupplierPulldown;

   }

   public void setApUseSupplierPulldown(boolean apUseSupplierPulldown) {

   	  	this.apUseSupplierPulldown = apUseSupplierPulldown;

   }

   public boolean getArUseCustomerPulldown() {

   		return arUseCustomerPulldown;

   }

   public void setArUseCustomerPulldown(boolean arUseCustomerPulldown) {

   	 	this.arUseCustomerPulldown = arUseCustomerPulldown;

   }

   public boolean getApAutoGenerateSupplierCode() {

       return apAutoGenerateSupplierCode;

   }

   public void setApAutoGenerateSupplierCode(boolean apAutoGenerateSupplierCode) {

       this.apAutoGenerateSupplierCode = apAutoGenerateSupplierCode;

   }

   public boolean getArAutoGenerateCustomerCode() {

       return arAutoGenerateCustomerCode;

   }

   public void setArAutoGenerateCustomerCode(boolean arAutoGenerateCustomerCode) {

       this.arAutoGenerateCustomerCode = arAutoGenerateCustomerCode;

   }

   public boolean getEnableNextSupplierCode() {

       return enableNextSupplierCode;

   }

   public void setEnableNextSupplierCode(boolean enableNextSupplierCode) {

       this.enableNextSupplierCode = enableNextSupplierCode;

   }

   public boolean getEnableNextCustomerCode() {

       return enableNextCustomerCode;

   }

   public void setEnableNextCustomerCode(boolean enableNextCustomerCode) {

       this.enableNextCustomerCode = enableNextCustomerCode;

   }

   public String getNextSupplierCode() {

       return nextSupplierCode;

   }

   public void setNextSupplierCode(String nextSupplierCode) {

       this.nextSupplierCode = nextSupplierCode;

   }

   public String getNextCustomerCode() {

       return nextCustomerCode;

   }

   public void setNextCustomerCode(String nextCustomerCode) {

       this.nextCustomerCode = nextCustomerCode;

   }

   public boolean getApReferenceNumberValidation() {

   	   return apReferenceNumberValidation;

   }

   public void setApReferenceNumberValidation(boolean apReferenceNumberValidation) {

   	   this.apReferenceNumberValidation = apReferenceNumberValidation;

   }

   public boolean getEnableInvPosIntegration() {

   	   return enableInvPosIntegration;

   }

   public void setEnableInvPosIntegration(boolean enableInvPosIntegration) {

   	   this.enableInvPosIntegration = enableInvPosIntegration;

   }

   public boolean getEnableInvPosAutoPostUpload() {

   	   return enableInvPosAutoPostUpload;

   }

   public void setEnableInvPosAutoPostUpload(boolean enableInvPosAutoPostUpload) {

   	   this.enableInvPosAutoPostUpload = enableInvPosAutoPostUpload;

   }

   public String getInvPosAdjustmentAccount() {

   	   return invPosAdjustmentAccount;

   }

   public void setInvPosAdjustmentAccount(String invPosAdjustmentAccount) {

   	   this.invPosAdjustmentAccount = invPosAdjustmentAccount;

   }

   public String getInvPosAdjustmentAccountDescription() {

   	   return invPosAdjustmentAccountDescription;

   }

   public void setInvPosAdjustmentAccountDescription(String invPosAdjustmentAccountDescription) {

   	   this.invPosAdjustmentAccountDescription = invPosAdjustmentAccountDescription;

   }

   public String getInvGeneralAdjustmentAccount() {

   	   return invGeneralAdjustmentAccount;

   }

   public void setInvGeneralAdjustmentAccount(String invGeneralAdjustmentAccount) {

   	   this.invGeneralAdjustmentAccount = invGeneralAdjustmentAccount;

   }

   public String getInvGeneralAdjustmentAccountDescription() {

   	   return invGeneralAdjustmentAccountDescription;

   }

   public void setInvGeneralAdjustmentAccountDescription(String invGeneralAdjustmentAccountDescription) {

   	   this.invGeneralAdjustmentAccountDescription = invGeneralAdjustmentAccountDescription;

   }

   public String getInvIssuanceAdjustmentAccount() {

   	   return invIssuanceAdjustmentAccount;

   }

   public void setInvIssuanceAdjustmentAccount(String invIssuanceAdjustmentAccount) {

   	   this.invIssuanceAdjustmentAccount = invIssuanceAdjustmentAccount;

   }

   public String getInvIssuanceAdjustmentAccountDescription() {

   	   return invIssuanceAdjustmentAccountDescription;

   }

   public void setInvIssuanceAdjustmentAccountDescription(String invIssuanceAdjustmentAccountDescription) {

   	   this.invIssuanceAdjustmentAccountDescription = invIssuanceAdjustmentAccountDescription;

   }

   public String getInvProductionAdjustmentAccount() {

   	   return invProductionAdjustmentAccount;

   }

   public void setInvProductionAdjustmentAccount(String invProductionAdjustmentAccount) {

   	   this.invProductionAdjustmentAccount = invProductionAdjustmentAccount;

   }

   public String getInvProductionAdjustmentAccountDescription() {

   	   return invProductionAdjustmentAccountDescription;

   }

   public void setInvProductionAdjustmentAccountDescription(String invProductionAdjustmentAccountDescription) {

   	   this.invProductionAdjustmentAccountDescription = invProductionAdjustmentAccountDescription;

   }

   public String getInvAdjustmentRequestAccount() {

   	   return invAdjustmentRequestAccount;

   }

   public void setInvAdjustmentRequestAccount(String invAdjustmentRequestAccount) {

   	   this.invAdjustmentRequestAccount = invAdjustmentRequestAccount;

   }

   public String getInvAdjustmentRequestAccountDescription() {

   	   return invAdjustmentRequestAccountDescription;

   }

   public void setInvAdjustmentRequestAccountDescription(String invAdjustmentRequestAccountDescription) {

   	   this.invAdjustmentRequestAccountDescription = invAdjustmentRequestAccountDescription;

   }

   public String getApCheckVoucherDataSource() {
   		return apCheckVoucherDataSource;
   }

   public void setApCheckVoucherDataSource(String apCheckVoucherDataSource) {
   		this.apCheckVoucherDataSource = apCheckVoucherDataSource;
   }

   public ArrayList getApCheckVoucherDataSourceList() {
   		return apCheckVoucherDataSourceList;
   }

   public String getArSalesInvoiceDataSource() {
		return arSalesInvoiceDataSource;
   }

   public void setArSalesInvoiceDataSource(String arSalesInvoiceDataSource) {
		this.arSalesInvoiceDataSource = arSalesInvoiceDataSource;
   }

   public ArrayList getArSalesInvoiceDataSourceList() {
   		return arSalesInvoiceDataSourceList;
   }

   public String getMiscPosDiscountAccount() {

   	   return miscPosDiscountAccount;

   }

   public void setMiscPosDiscountAccount(String miscPosDiscountAccount) {

   	   this.miscPosDiscountAccount = miscPosDiscountAccount;

   }

   public String getMiscPosDiscountAccountDescription() {

   	   return miscPosDiscountAccountDescription;

   }

   public void setMiscPosDiscountAccountDescription(String miscPosDiscountAccountDescription) {

   	   this.miscPosDiscountAccountDescription = miscPosDiscountAccountDescription;

   }

   public String getMiscPosGiftCertificateAccount() {

   	   return miscPosGiftCertificateAccount;

   }

   public void setMiscPosGiftCertificateAccount(String miscPosGiftCertificateAccount) {

   	   this.miscPosGiftCertificateAccount = miscPosGiftCertificateAccount;

   }

   public String getMiscPosGiftCertificateAccountDescription() {

   	   return miscPosGiftCertificateAccountDescription;

   }

   public void setMiscPosGiftCertificateAccountDescription(String miscPosGiftCertificateAccountDescription) {

   	   this.miscPosGiftCertificateAccountDescription = miscPosGiftCertificateAccountDescription;

   }

   public String getMiscPosServiceChargeAccount() {

   	   return miscPosServiceChargeAccount;

   }

   public void setMiscPosServiceChargeAccount(String miscPosServiceChargeAccount) {

   	   this.miscPosServiceChargeAccount = miscPosServiceChargeAccount;

   }

   public String getMiscPosServiceChargeAccountDescription() {

   	   return miscPosServiceChargeAccountDescription;

   }

   public void setMiscPosServiceChargeAccountDescription(String miscPosServiceChargeAccountDescription) {

   	   this.miscPosServiceChargeAccountDescription = miscPosServiceChargeAccountDescription;

   }

   public String getMiscPosDineInChargeAccount() {

   	   return miscPosDineInChargeAccount;

   }

   public void setMiscPosDineInChargeAccount(String miscPosDineInChargeAccount) {

   	   this.miscPosDineInChargeAccount = miscPosDineInChargeAccount;

   }

   public String getMiscPosDineInChargeAccountDescription() {

   	   return miscPosDineInChargeAccountDescription;

   }

   public void setMiscPosDineInChargeAccountDescription(String miscPosDineInChargeAccountDescription) {

   	   this.miscPosDineInChargeAccountDescription = miscPosDineInChargeAccountDescription;

   }

   public String getApDefaultPRTaxCode() {

   	   return apDefaultPRTaxCode;

   }

   public void setApDefaultPRTaxCode(String apDefaultPRTaxCode) {

   	   this.apDefaultPRTaxCode = apDefaultPRTaxCode;

   }

   public ArrayList getApDefaultPRTaxCodeList() {

   	   return apDefaultPRTaxCodeList;

   }

   public void setApDefaultPRTaxCodeList(String apDefaultPRTaxCode) {

   	    apDefaultPRTaxCodeList.add(apDefaultPRTaxCode);

   }

   public void clearApDefaultPRTaxCodeList() {

   	   apDefaultPRTaxCodeList.clear();
   	   apDefaultPRTaxCodeList.add(Constants.GLOBAL_BLANK);

   }

   public String getApDefaultPRCurrency() {

   	   return apDefaultPRCurrency;

   }

   public void setApDefaultPRCurrency(String apDefaultPRCurrency) {

   	   this.apDefaultPRCurrency = apDefaultPRCurrency;

   }

   public ArrayList getApDefaultPRCurrencyList() {

   	   return apDefaultPRCurrencyList;

   }

   public void setApDefaultPRCurrencyList(String apDefaultPRCurrency) {

   	    apDefaultPRCurrencyList.add(apDefaultPRCurrency);

   }

   public void clearApDefaultPRCurrencyList() {

   	   apDefaultPRCurrencyList.clear();
   	   apDefaultPRCurrencyList.add(Constants.GLOBAL_BLANK);

   }

   public  String getCustomerDepositAccount() {

   		return customerDepositAccount;
   }

   public void setCustomerDepositAccount(String customerDepositAccount) {

		this.customerDepositAccount = customerDepositAccount;
   }

   public  String getCustomerDepositAccountDescription() {

		return customerDepositAccountDescription;
   }

   public void setCustomerDepositAccountDescription(String customerDepositAccountDescription) {

   		this.customerDepositAccountDescription = customerDepositAccountDescription;
   }

   public String getInvVarianceAccount() {

   	   return invVarianceAccount;

   }

   public void setInvVarianceAccount(String varianceAccount) {

   	   this.invVarianceAccount = varianceAccount;

   }

   public String getInvVarianceAccountDescription() {

   	   return invVarianceAccountDescription;

   }

   public void setInvVarianceAccountDescription(String varianceAccountDescription) {

   	   this.invVarianceAccountDescription = varianceAccountDescription;

   }

   public String getInvWastageAccount() {

   	   return invWastageAccount;

   }

   public void setInvWastageAccount(String invWastageAccount) {

   	   this.invWastageAccount = invWastageAccount;

   }

   public String getInvWastageAccountDescription() {

   	   return invWastageAccountDescription;

   }

   public void setInvWastageAccountDescription(String invWastageAccountDescription) {

   	   this.invWastageAccountDescription = invWastageAccountDescription;

   }

   public String getInvSalesTaxCodeAccount() {

   	   return invSalesTaxCodeAccount;

   }

   public void setInvSalesTaxCodeAccount(String invSalesTaxCodeAccount) {

   	   this.invSalesTaxCodeAccount = invSalesTaxCodeAccount;

   }

   public String getInvSalesTaxCodeAccountDescription() {

   	   return invSalesTaxCodeAccountDescription;

   }

   public void setInvSalesTaxCodeAccountDescription(String invSalesTaxCodeAccountDescription) {

   	   this.invSalesTaxCodeAccountDescription = invSalesTaxCodeAccountDescription;

   }

   public String getInvPreparedFoodTaxCodeAccount() {

   	   return invPreparedFoodTaxCodeAccount;

   }

   public void setInvPreparedFoodTaxCodeAccount(String invPreparedFoodTaxCodeAccount) {

   	   this.invPreparedFoodTaxCodeAccount = invPreparedFoodTaxCodeAccount;

   }

   public String getInvPreparedFoodTaxCodeAccountDescription() {

   	   return invPreparedFoodTaxCodeAccountDescription;

   }

   public void setInvPreparedFoodTaxCodeAccountDescription(String invPreparedFoodTaxCodeAccountDescription) {

   	   this.invPreparedFoodTaxCodeAccountDescription = invPreparedFoodTaxCodeAccountDescription;

   }


   public boolean getArAutoComputeCogs() {

   	   return arAutoComputeCogs;

   }

   public void setArAutoComputeCogs(boolean arAutoComputeCogs) {

   	   this.arAutoComputeCogs = arAutoComputeCogs;

   }



   public String getArMonthlyInterestRate() {

	   return arMonthlyInterestRate;

   }

   public void setArMonthlyInterestRate(String arMonthlyInterestRate) {

   	   this.arMonthlyInterestRate = arMonthlyInterestRate;

   }

   public String getApAgingBucket() {

	   return apAgingBucket;

   }

   public void setApAgingBucket(String apAgingBucket) {

	   this.apAgingBucket = apAgingBucket;

   }

   public String getArAgingBucket() {

	   return arAgingBucket;

   }

   public void setArAgingBucket(String arAgingBucket) {

	   this.arAgingBucket = arAgingBucket;

   }

   public boolean getArAllowPriorDate() {

	   return arAllowPriorDate;

   }

   public void setArAllowPriorDate(boolean arAllowPriorDate) {

	   this.arAllowPriorDate = arAllowPriorDate;

   }

   public boolean getApShowPRCost() {

	   return apShowPRCost;

   }

   public void setApShowPRCost(boolean apShowPRCost) {

	   this.apShowPRCost = apShowPRCost;

   }

   public boolean getApDebitMemoOverrideCost() {

	   return apDebitMemoOverrideCost;

   }

   public void setApDebitMemoOverrideCost(boolean apDebitMemoOverrideCost) {

	   this.apDebitMemoOverrideCost = apDebitMemoOverrideCost;

   }

   public boolean getGlYearEndCloseRestriction() {

	   return glYearEndCloseRestriction;

   }

   public void setGlYearEndCloseRestriction(boolean glYearEndCloseRestriction) {

	   this.glYearEndCloseRestriction = glYearEndCloseRestriction;

   }

   public boolean getAdDisableMultipleLogin() {

	   return adDisableMultipleLogin;

   }

   public void setAdDisableMultipleLogin(boolean adDisableMultipleLogin) {

	   this.adDisableMultipleLogin = adDisableMultipleLogin;

   }

   public boolean getAdEnableEmailNotification() {

	   return adEnableEmailNotification;

   }

   public void setAdEnableEmailNotification(boolean adEnableEmailNotification) {

	   this.adEnableEmailNotification = adEnableEmailNotification;

   }


   public boolean getArEnablePaymentTerm() {

	   return arEnablePaymentTerm;

   }

   public void setArEnablePaymentTerm(boolean arEnablePaymentTerm) {

	   this.arEnablePaymentTerm = arEnablePaymentTerm;

   }

   public boolean getArDisableSalesPrice() {

	   return arDisableSalesPrice;

   }

   public void setArDisableSalesPrice(boolean arDisableSalesPrice) {

	   this.arDisableSalesPrice = arDisableSalesPrice;

   }

   public boolean getInvItemLocationShowAll() {

	   return invItemLocationShowAll;

   }

   public void setInvItemLocationShowAll(boolean invItemLocationShowAll) {

	   this.invItemLocationShowAll = invItemLocationShowAll;

   }

public boolean getInvItemLocationAddByItemList() {

	   return invItemLocationAddByItemList;

   }

   public void setInvItemLocationAddByItemList(boolean invItemLocationAddByItemList) {

	   this.invItemLocationAddByItemList = invItemLocationAddByItemList;

   }

   public boolean getArValidateCustomerEmail() {

	   return arValidateCustomerEmail;

   }

   public void setArValidateCustomerEmail(boolean arValidateCustomerEmail) {

	   this.arValidateCustomerEmail = arValidateCustomerEmail;

   }

   public boolean getArCheckStock() {

	   return arCheckStock;

   }

   public void setArCheckStock(boolean arCheckStock) {

	   this.arCheckStock = arCheckStock;

   }

public boolean getArDetailedReceivable() {

	   return arDetailedReceivable;

   }

   public void setArDetailedReceivable(boolean arDetailedReceivable) {

	   this.arDetailedReceivable = arDetailedReceivable;

   }


   public String getMailHost() {

	   return mailHost;

   }

   public void setMailHost(String mailHost) {

	   this.mailHost = mailHost;

   }


   public String getMailSocketFactoryPort() {

	   return mailSocketFactoryPort;

   }

   public void setMailSocketFactoryPort(String mailSocketFactoryPort) {

	   this.mailSocketFactoryPort = mailSocketFactoryPort;

   }

   public String getMailPort() {

	   return mailPort;

   }

   public void setMailPort(String mailPort) {

	   this.mailPort = mailPort;

   }

   public String getMailFrom() {

	   return mailFrom;

   }

   public void setMailFrom(String mailFrom) {

	   this.mailFrom = mailFrom;

   }
   
   public boolean getMailAuthenticator() {

	   return mailAuthenticator;

   }

   public void setMailAuthenticator(boolean mailAuthenticator) {

	   this.mailAuthenticator = mailAuthenticator;

   }

   public String getMailPassword() {

	   return mailPassword;

   }

   public void setMailPassword(String mailPassword) {

	   this.mailPassword = mailPassword;

   }
   public String getMailTo() {

	   return mailTo;

   }

   public void setMailTo(String mailTo) {

	   this.mailTo = mailTo;

   }


   public String getMailCc() {

	   return mailCc;

   }

   public void setMailCc(String mailCc) {

	   this.mailCc = mailCc;

   }

   public String getMailBcc() {

	   return mailBcc;

   }

   public void setMailBcc(String mailBcc) {

	   this.mailBcc = mailBcc;

   }
   
   public String getMailConfig() {

	   return mailConfig;

   }

   public void setMailConfig(String mailConfig) {

	   this.mailConfig = mailConfig;

   }

   public String getDateFormatInput() {

	   return dateFormatInput;

   }

   public void setDateFormatInput(String dateFormatInput) {

	   this.dateFormatInput = dateFormatInput;

   }

   public String getDateFormatOutput() {

	   return dateFormatOutput;

   }

   public void setDateFormatOutput(String dateFormatOutput) {

	   this.dateFormatOutput = dateFormatOutput;

   }

    public ArrayList getDateFormatList() {

   	  return dateFormatList;

   }
    
  public Boolean getArSoSalespersonRequired() {
	  return arSoSalespersonRequired;
  }
  
  public void setArSoSalespersonRequired(Boolean arSoSalespersonRequired) {
	  this.arSoSalespersonRequired = arSoSalespersonRequired;
  }
  
  
  public Boolean getArInvcSalespersonRequired() {
	  return arInvcSalespersonRequired;
  }
  
  public void setArInvcSalespersonRequired(Boolean arInvcSalespersonRequired) {
	  this.arInvcSalespersonRequired = arInvcSalespersonRequired;
  }
    
    

   public void reset(ActionMapping mapping, HttpServletRequest request) {

		allowSuspensePosting = false;
		enableGlJournalBatch = false;
		enableGlRecomputeCoaBalance=false;

		enableApVoucherBatch = false;
        enableApPOBatch = false;
		enableApCheckBatch = false;
		enableArInvoiceBatch = false;
		enableArInvoiceInterestGeneration = false;
		enableArReceiptBatch = false;
		enableArMiscReceiptBatch = false;
		enableInvShift = false;
		enableInvBUABatch = false;
		apUseAccruedVat = false;
		enableInvPosIntegration = false;
		enableInvPosAutoPostUpload = false;
		invPosAdjustmentAccount = null;
		invPosAdjustmentAccountDescription = null;
		invGeneralAdjustmentAccount = null;
		invGeneralAdjustmentAccountDescription = null;
		invIssuanceAdjustmentAccount = null;
		invIssuanceAdjustmentAccountDescription = null;
		invProductionAdjustmentAccount = null;
		invProductionAdjustmentAccountDescription = null;
		apDefaultCheckDate = null;
		apDefaultChecker = null;
		apDefaultApprover = null;
		accruedVatAccount = null;
		accruedVatAccountDescription = null;
		cmUseBankForm = false;
		pettyCashAccount = null;
		pettyCashAccountDescription = null;
		apUseSupplierPulldown = false;
		arUseCustomerPulldown = false;
		apAutoGenerateSupplierCode = false;
		arAutoGenerateCustomerCode = false;
		apReferenceNumberValidation = false;
		miscPosDiscountAccount = null;
		miscPosDiscountAccountDescription = null;
		miscPosGiftCertificateAccount = null;
		miscPosGiftCertificateAccountDescription = null;
		miscPosServiceChargeAccount = null;
		miscPosServiceChargeAccountDescription = null;
		miscPosDineInChargeAccount = null;
		miscPosDineInChargeAccountDescription = null;
		customerDepositAccount = null;
		customerDepositAccountDescription = null;
		apDefaultPRTaxCode = null;
		apDefaultPRCurrency = null;
		invPreparedFoodTaxCodeAccount=null;
		invPreparedFoodTaxCodeAccountDescription=null;
		invSalesTaxCodeAccount=null;
		invSalesTaxCodeAccountDescription=null;

		if (apWTaxRealizationList.isEmpty()) {

			apWTaxRealizationList.add("VOUCHER");
			apWTaxRealizationList.add("PAYMENT");

		}

		if (arWTaxRealizationList.isEmpty()) {

			arWTaxRealizationList.add("INVOICE");
			arWTaxRealizationList.add("COLLECTION");

		}

		if (apGlPostingTypeList.isEmpty()) {

		   apGlPostingTypeList.add("AUTO-POST UPON APPROVAL");
		   apGlPostingTypeList.add("USE SL POSTING");
		   apGlPostingTypeList.add("USE JOURNAL INTERFACE");

		}

		if (arGlPostingTypeList.isEmpty()) {

		   arGlPostingTypeList.add("AUTO-POST UPON APPROVAL");
		   arGlPostingTypeList.add("USE SL POSTING");
		   arGlPostingTypeList.add("USE JOURNAL INTERFACE");

		}

		if (cmGlPostingTypeList.isEmpty()) {

		   cmGlPostingTypeList.add("AUTO-POST UPON APPROVAL");
		   cmGlPostingTypeList.add("USE SL POSTING");
		   cmGlPostingTypeList.add("USE JOURNAL INTERFACE");

		}

		if (invGlPostingTypeList.isEmpty()) {

		   invGlPostingTypeList.add("AUTO-POST UPON APPROVAL");
		   invGlPostingTypeList.add("USE SL POSTING");
		   invGlPostingTypeList.add("USE JOURNAL INTERFACE");

	    }

		if (apDefaultCheckDateList.isEmpty()) {

			apDefaultCheckDateList.add("CURRENT DATE");
			apDefaultCheckDateList.add("DUE DATE");

		}

		if (glPostingTypeList.isEmpty()) {

		   glPostingTypeList.add("AUTO-POST UPON APPROVAL");
		   glPostingTypeList.add("USE POSTING");

		}

		if (apFindCheckDefaultTypeList.isEmpty()) {

		    apFindCheckDefaultTypeList.add("PAYMENT");
		    apFindCheckDefaultTypeList.add("DIRECT");
		    apFindCheckDefaultTypeList.add("LAST USED");


		}

		if (arFindReceiptDefaultTypeList.isEmpty()) {

		    arFindReceiptDefaultTypeList.add("COLLECTION");
		    arFindReceiptDefaultTypeList.add("MISC");
		    arFindReceiptDefaultTypeList.add("LAST USED");


		}

		if (apCheckVoucherDataSourceList.isEmpty()) {

			apCheckVoucherDataSourceList.add("AP DISTRIBUTION RECORD");
			apCheckVoucherDataSourceList.add("AP VOUCHER");

		}

		if (arSalesInvoiceDataSourceList.isEmpty()) {

			arSalesInvoiceDataSourceList.add("AR DISTRIBUTION RECORD");
			arSalesInvoiceDataSourceList.add("AR PAYMENT SCHEDULE");

		}

                if(dateFormatList.isEmpty()){
                    dateFormatList.add("dd/MM/yyyy");
                    dateFormatList.add("MM/dd/yyyy");
                }

		defaultWTaxCode = Constants.GLOBAL_BLANK;
		arAutoComputeCogs = false;
		arMonthlyInterestRate = null;
		apAgingBucket = null;
		arAgingBucket = null;
		arAllowPriorDate = false;
		apShowPRCost = false;
		apDebitMemoOverrideCost = false;
		glYearEndCloseRestriction =false;
		adDisableMultipleLogin = false;
		adEnableEmailNotification = false;
		arEnablePaymentTerm = false;
		arDisableSalesPrice = false;
		invItemLocationShowAll = false;
		invItemLocationAddByItemList = false;
		arValidateCustomerEmail = false;
		arCheckStock = false;
		arDetailedReceivable = false;
                dateFormatInput = null;
                dateFormatOutput = null;

                arSoSalespersonRequired = false;
                arInvcSalespersonRequired = false;
                
        mailHost = null;
        mailSocketFactoryPort = null;
        mailPort = null;
        mailFrom = null;
        mailAuthenticator = false;
        mailPassword = null;
        mailTo = null;
        mailCc = null;
        mailBcc = null;
        mailConfig = null;
        centralWarehouseBranch = null;
        centralWarehouseLocation = null;
	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null) {

	         if (Common.validateRequired(glJournalLineNumber)) {

	            errors.add("glJournalLineNumber",
	               new ActionMessage("preference.error.glJournalLineNumberRequired"));

	         }

	         if (!Common.validateNumberFormat(glJournalLineNumber)) {

	            errors.add("glJournalLineNumber",
	               new ActionMessage("preference.error.glJournalLineNumberInvalid"));

	         }

	         if (Common.validateRequired(apJournalLineNumber)) {

	            errors.add("apJournalLineNumber",
	               new ActionMessage("preference.error.apJournalLineNumberRequired"));

	         }

	         if (!Common.validateNumberFormat(apJournalLineNumber)) {

	            errors.add("apJournalLineNumber",
	               new ActionMessage("preference.error.apJournalLineNumberInvalid"));

	         }

	         if (Common.validateRequired(arInvoiceLineNumber)) {

	            errors.add("arInvoiceLineNumber",
	               new ActionMessage("preference.error.arInvoiceLineNumberRequired"));

	         }

	         if (!Common.validateNumberFormat(arInvoiceLineNumber)) {

	            errors.add("arInvoiceLineNumber",
	               new ActionMessage("preference.error.arInvoiceLineNumberInvalid"));

	         }

	         if (Common.validateRequired(apWTaxRealization)) {

	            errors.add("apWTaxRealization",
	               new ActionMessage("preference.error.apWTaxRealizationRequired"));

	         }

	         if (Common.validateRequired(arWTaxRealization)) {

	            errors.add("arWTaxRealization",
	               new ActionMessage("preference.error.arWTaxRealizationRequired"));

	         }

	         if (Common.validateRequired(apGlPostingType)) {

	            errors.add("apGlPostingType",
	               new ActionMessage("preference.error.apGlPostingTypeRequired"));

	         }

	         if (Common.validateRequired(arGlPostingType)) {

	            errors.add("arGlPostingType",
	               new ActionMessage("preference.error.arGlPostingTypeRequired"));

	         }

	         if (Common.validateRequired(cmGlPostingType)) {

	            errors.add("cmGlPostingType",
	               new ActionMessage("preference.error.cmGlPostingTypeRequired"));

	         }

	         if (Common.validateRequired(invInventoryLineNumber)) {
	         	errors.add("invInventoryLineNumber",
	         		new ActionMessage("preference.error.invInventoryLineNumberRequired"));

	         }

	         if (!Common.validateNumberFormat(invInventoryLineNumber)) {

	            errors.add("invInventoryLineNumber",
	               new ActionMessage("preference.error.invInventoryLineNumberInvalid"));

	         }

	         if (Common.validateRequired(invQuantityPrecisionUnit)) {

	         	errors.add("invQuantityPrecisionUnit",
	         		new ActionMessage("preference.error.invQuantityPrecisionUnitRequired"));

	         }

	         if (!Common.validateNumberFormat(invQuantityPrecisionUnit)) {

	            errors.add("invQuantityPrecisionUnit",
	               new ActionMessage("preference.error.invQuantityPrecisionUnitInvalid"));

	         }

	         if (Common.validateRequired(invCostPrecisionUnit)) {

		         	errors.add("invCostPrecisionUnit",
		         		new ActionMessage("preference.error.invCostPrecisionUnitRequired"));

		         }

		         if (!Common.validateNumberFormat(invCostPrecisionUnit)) {

		            errors.add("invCostPrecisionUnit",
		               new ActionMessage("preference.error.invCostPrecisionUnitInvalid"));

		         }

	         if (Common.validateRequired(invGlPostingType)) {

	            errors.add("invGlPostingType",
	               new ActionMessage("preference.error.invGlPostingTypeRequired"));

	         }

	         if (Common.validateRequired(glPostingType)) {

	            errors.add("glPostingType",
	               new ActionMessage("preference.error.glPostingTypeRequired"));

	         }

	         if (Common.validateRequired(apFindCheckDefaultType)) {

	            errors.add("apFindCheckDefaultType",
	               new ActionMessage("preference.error.apFindCheckDefaultTypeRequired"));

	         }

	         if (Common.validateRequired(arFindReceiptDefaultType)) {

	            errors.add("arFindReceiptDefaultType",
	               new ActionMessage("preference.error.arFindReceiptDefaultTypeRequired"));

	         }

	         if (apUseAccruedVat == true && Common.validateRequired(accruedVatAccount)) {

	            errors.add("accruedVatAccount",
	               new ActionMessage("preference.error.accruedVatAccountRequired"));

	         }

	         if(arAutoGenerateCustomerCode == true && Common.validateRequired(nextCustomerCode)) {

	         	errors.add("nextCustomerCode",
	 	               new ActionMessage("preference.error.nextCustomerCodeRequired"));

	         }

	         if(apAutoGenerateSupplierCode == true && Common.validateRequired(nextSupplierCode)) {

	         	errors.add("nextSupplierCode",
	 	               new ActionMessage("preference.error.nextSupplierCodeRequired"));

	         }

	         if( Common.validateRequired(arMonthlyInterestRate)) {

		         	errors.add("arMonthlyInterest",
		 	               new ActionMessage("preference.error.arMonthlyInterestRateRequired"));

		         }

	         if(enableInvPosIntegration == true && Common.validateRequired(invPosAdjustmentAccount)) {

	         	errors.add("invPosAdjustmentAccount",
	         		   new ActionMessage("preference.error.posAdjustmentAccountRequired"));

	         }

	         if (Common.validateRequired(apDefaultPRTaxCode)) {

	            errors.add("apDefaultPRTaxCode",
	               new ActionMessage("preference.error.apDefaultPRTaxCodeRequired"));

	         }

	         if (Common.validateRequired(apDefaultPRCurrency)) {

	            errors.add("apDefaultPRCurrency",
	               new ActionMessage("preference.error.apDefaultPRCurrencyRequired"));

	         }

	         if (Common.validateRequired(apAgingBucket)) {

	        	 errors.add("apAgingBucket",
	        			 new ActionMessage("preference.error.apAgingBucketRequired"));

	         }

	         if (!Common.validateNumberFormat(apAgingBucket)) {

	        	 errors.add("apAgingBucket",
	        			 new ActionMessage("preference.error.apAgingBucketInvalid"));

	         }

	         if (Common.validateRequired(arAgingBucket)) {

	        	 errors.add("arAgingBucket",
	        			 new ActionMessage("preference.error.arAgingBucketRequired"));

	         }

	         if (!Common.validateNumberFormat(arAgingBucket)) {

	        	 errors.add("arAgingBucket",
	        			 new ActionMessage("preference.error.arAgingBucketInvalid"));

	         }

	         if (Common.convertStringToInt(apAgingBucket) < 1 || Common.convertStringToInt(arAgingBucket) < 1) {

	        	 errors.add("apAgingBucket",
	        			 new ActionMessage("preference.error.agingBucketMustBeGreaterThanZero"));

	         }
	         
	         
	          if (Common.validateRequired(centralWarehouseBranch)) {

                 errors.add("centralWarehouseBranch",
                         new ActionMessage("preference.error.centralWarehouseBranchRequired"));

             }
             
              if (Common.validateRequired(centralWarehouseLocation)) {

                 errors.add("centralWarehouseLocation",
                         new ActionMessage("preference.error.centralWarehouseLocationRequired"));

             }


	  	}

	  	return errors;

	}

}