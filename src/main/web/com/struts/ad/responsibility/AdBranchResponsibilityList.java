package com.struts.ad.responsibility;

import java.io.Serializable;

public class AdBranchResponsibilityList implements Serializable {
	
	private String branchName = null;
	private Integer branchCode = null;
	private boolean branchCheckbox = false;
	
	private AdResponsibilityForm parentBean;
	
	public AdBranchResponsibilityList(AdResponsibilityForm parentBean, 
			String branchName, Integer branchCode) {
		
		this.parentBean = parentBean;
		this.branchName = branchName;
		this.branchCode = branchCode;
		
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBranchCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}