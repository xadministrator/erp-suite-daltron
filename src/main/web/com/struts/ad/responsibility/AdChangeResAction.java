package com.struts.ad.responsibility;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.AdBranchResponsibilityController;
import com.ejb.txn.AdBranchResponsibilityControllerHome;
import com.struts.util.Branch;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Responsibility;
import com.struts.util.User;
import com.struts.util.UserRes;
import com.util.AdBranchResponsibilityDetails;

public final class AdChangeResAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) 
	throws IOException, ServletException {
		
		DataSource dataSource = (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY);
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		HttpSession session = request.getSession(); 
		try{
			
			int resCode = Integer.parseInt(request.getParameter("resCode"));
			User user = (User)session.getAttribute(Constants.USER_KEY);
			if(user != null){
				if(log.isInfoEnabled()){
					log.info("AdChangeResAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
			}else{
				if (log.isInfoEnabled())
					log.info("User is not logged on in session " + session.getId());
				return mapping.findForward("adLogon");
			}
			UserRes userRes = null;
			Responsibility res = null;
			boolean userResFound = false;
			boolean resFound = false;
			for(int i=0; i<user.getUserResCount(); i++){
				userRes = user.getUserRes(i);
				if(userRes.getURResCode() == resCode){
					userResFound = true; 
					break;   
				}
			}
			for(int i=0; i<user.getResCount(); i++){
				res = user.getRes(i);
				if(res.getRSCode() == resCode){
					resFound = true;
					break;
				}
			}

			ActionErrors errors = new ActionErrors();
			// Check if selected responsibility exists.
			if(!resFound && !userResFound)
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("changeRes.error.userResNotExisting"));
			
			// Check if selected responsibility is enabled.
			else if(!Common.validateDateEnabled(userRes.getURDateFrom(), userRes.getURDateTo()))
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("changeRes.error.selectedUserResDisabled"));
			
			// Check if user's selected responsibility is enabled.
			else if(!Common.validateDateEnabled(res.getRSDateFrom(), res.getRSDateTo()))
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("changeRes.error.selectedResDisabled"));
			
			if (!errors.isEmpty()) {
				saveErrors(request, new ActionMessages(errors));
				return(mapping.findForward("cmnMain"));
			}
			
			user.setCurrentResCode(resCode);
			
			// get branches for this responsibility
			
			AdBranchResponsibilityControllerHome homeBR = null;
			AdBranchResponsibilityController ejbBR = null;
			
			try {
				homeBR = (AdBranchResponsibilityControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/AdBranchResponsibilityControllerEJB", AdBranchResponsibilityControllerHome.class);
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					log.info("NamingException caught in AdPaymentTermAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				return mapping.findForward("cmnErrorPage");
			}
			
			try {
				ejbBR = homeBR.create();
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					log.info("CreateException caught in AdPaymentTermAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				return mapping.findForward("cmnErrorPage");
			}
			
			ArrayList branches = new ArrayList();
			branches = ejbBR.getAdBrnchRspnsbltyAll(resCode, user.getCmpCode());
			
			Iterator j = branches.iterator();
			conn = dataSource.getConnection(); 
			user.clearBranches();
			while ( j.hasNext() ) {
				
				stmt = conn.prepareStatement("SELECT BR_CODE, BR_BRNCH_CODE, BR_NM, BR_HD_QTR FROM AD_BRNCH " +
				"WHERE BR_CODE = ? AND BR_AD_CMPNY = ?");
				
				stmt.setInt(1, ((AdBranchResponsibilityDetails)j.next()).getBrsCode().intValue() );
				stmt.setInt(2, user.getCmpCode().intValue());
				
				rs = stmt.executeQuery();
				while(rs.next()){
					Branch brnch = new Branch();
					brnch.setBrCode(rs.getInt(1));
					brnch.setBrBranchCode(rs.getString(2));
					brnch.setBrName(rs.getString(3));
					user.setBranch(brnch);
					if (rs.getInt(4) == 1) user.setCurrentBranch(brnch);
				}	
			}
			if (user.getCurrentBranch() == null) {
			Branch br = user.getBranch(0);
			
			user.setCurrentBranch(br);
			System.out.println("BRANCH USER -->"+user.getCurrentBranch());
			}
			
			session.setAttribute(Constants.USER_KEY, user);
			
			return(mapping.findForward("cmnMain"));
			
		} catch (Exception e){
			if(log.isInfoEnabled())
				log.info("Exception caught in ChangeResAction.execute(): " + e.getMessage() +
						" session: " + session.getId());
			return(mapping.findForward("cmnErrorPage"));
		}finally{
			try { if(rs != null) rs.close(); }catch(SQLException f){ ; }
			rs = null;
			try { if(stmt != null) stmt.close(); }catch(SQLException f){ ; }
			stmt = null;
			try{ if(conn != null) conn.close(); }catch(SQLException f){ ; }
			conn = null;
		}
		
	}	
	
}
