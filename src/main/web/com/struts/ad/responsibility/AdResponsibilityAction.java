package com.struts.ad.responsibility;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdRSResponsibilityNotAssignedToBranchException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdResponsibilityController;
import com.ejb.txn.AdResponsibilityControllerHome;
import com.struts.gl.rowset.GlFrgRowSetList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdResponsibilityDetails;
import com.util.GlFrgRowSetDetails;

public final class AdResponsibilityAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdResponsibilityAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdResponsibilityForm actionForm = (AdResponsibilityForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_RESPONSIBILITY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adResponsibility");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdResponsibilityController EJB
*******************************************************/

         AdResponsibilityControllerHome homeRS = null;
         AdResponsibilityController ejbRS = null;

         try {

            homeRS = (AdResponsibilityControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdResponsibilityControllerEJB", AdResponsibilityControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdResponsibilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbRS = homeRS.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdResponsibilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }         
        
         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad RS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adResponsibility"));
	     
/*******************************************************
   -- Ad RS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adResponsibility"));                  
         
/*******************************************************
   -- Ad RS Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdResponsibilityDetails details = new AdResponsibilityDetails();
            details.setRsName(actionForm.getResponsibilityName());
            details.setRsDescription(actionForm.getDescription());            
            details.setRsDateFrom(Common.convertStringToSQLDate(actionForm.getDateFrom()));
            details.setRsDateTo(Common.convertStringToSQLDate(actionForm.getDateTo()));
            
            ArrayList branchList = new ArrayList();
            
            for(int i=0; i<actionForm.getAdBRListSize(); i++) {
            	
	        	AdBranchResponsibilityList brList = actionForm.getAdBRByIndex(i);
	        	
	        	if(brList.getBranchCheckbox() == true) {
	        		
	        		AdBranchDetails brDetails = new AdBranchDetails();
		        	
		        	brDetails.setBrAdCompany(user.getCmpCode());
		        	brDetails.setBrCode(brList.getBranchCode());
		        	
		        	branchList.add(brDetails);
	        	}
            }
            
            try {
            	
            	ejbRS.addAdRsEntry(details, branchList, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("responsibility.error.recordAlreadyExist"));

            } catch (AdRSResponsibilityNotAssignedToBranchException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("responsibility.error.branchRequired"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            actionForm.reset(mapping, request);

/*******************************************************
   -- Ar RS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar RS Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            AdResponsibilityList adRSList =
	            actionForm.getAdRSByIndex(actionForm.getRowSelected());
            
            AdResponsibilityDetails details = new AdResponsibilityDetails();
            details.setRsCode(adRSList.getResponsibilityCode());
            details.setRsName(actionForm.getResponsibilityName());
            details.setRsDescription(actionForm.getDescription());            
            details.setRsDateFrom(Common.convertStringToSQLDate(actionForm.getDateFrom()));
            details.setRsDateTo(Common.convertStringToSQLDate(actionForm.getDateTo()));
            
            ArrayList branchList = new ArrayList();
            
            for(int i=0; i<actionForm.getAdBRListSize(); i++) {
            	
	        	AdBranchResponsibilityList brList = actionForm.getAdBRByIndex(i);
	        	
	        	if(brList.getBranchCheckbox() == true) {
	        		
	        		AdBranchDetails brDetails = new AdBranchDetails();
		        	
		        	brDetails.setBrAdCompany(user.getCmpCode());
		        	brDetails.setBrCode(brList.getBranchCode());
		        	
		        	branchList.add(brDetails);
	        	}
            }
            
            try {
            	
            	ejbRS.updateAdRsEntry(details, branchList, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("responsibility.error.recordAlreadyExist"));

            } catch (AdRSResponsibilityNotAssignedToBranchException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("responsibility.error.branchRequired"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            //actionForm.reset(mapping, request);
            
/*******************************************************
   -- Ad RS Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad RS Edit Action --
*******************************************************/

         } else if (request.getParameter("adRSList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	actionForm.reset(mapping, request);
         	
         	Object[] adRSList = actionForm.getAdRSList();
         	
         	ArrayList list = null;
         	
         	try {
         	
         		list = ejbRS.getAdBrResAll(((AdResponsibilityList)adRSList[actionForm.getRowSelected()]).getResponsibilityCode(), user.getCmpCode());
         		
         	} catch(GlobalNoRecordFoundException ex) {
         		
         		
         	}
         	
         	actionForm.showAdRSRow(list, actionForm.getRowSelected());
         	
            return mapping.findForward("adResponsibility");
            
/*******************************************************
   -- Ad RS Form Function Responsibility Action --
*******************************************************/

         } else if (request.getParameter("adRSList[" +
            actionForm.getRowSelected() + "].formFunctionResponsibilitiesButton") != null){

	        AdResponsibilityList adRSList =
               actionForm.getAdRSByIndex(actionForm.getRowSelected());

	        String path = "/adFormFunctionResponsibility.do?forward=1" +
	           "&responsibilityCode=" + adRSList.getResponsibilityCode() + 
	           "&responsibilityName=" +  adRSList.getResponsibilityName() + 
	           "&description=" + adRSList.getDescription(); 
	        
	        return(new ActionForward(path));    
	        
        
/*******************************************************
   -- Gl FRG Copy Action --
*******************************************************/      

          } else if (request.getParameter("adRSList[" +
             actionForm.getRowSelected() + "].copyButton") != null){
             	 
     	 	     try {
     	 	    	 System.out.println("COPYYYYYYYYYYYYYYYYY");
     	 	    	 AdResponsibilityList adRSList =
     	        		 actionForm.getAdRSByIndex(actionForm.getRowSelected());
     	 	    	AdResponsibilityDetails details = new AdResponsibilityDetails();
     	        	 
     	        	details.setRsCode(adRSList.getResponsibilityCode());
     	        	details.setRsName(adRSList.getResponsibilityName());
     	        	details.setRsDescription(adRSList.getDescription());          
     	            details.setRsDateFrom(Common.convertStringToSQLDate(adRSList.getDateFrom()));
     	            details.setRsDateTo(Common.convertStringToSQLDate(adRSList.getDateTo()));
     	            
     	        	 
     	        	
     	           ArrayList branchList = new ArrayList();
  	              
  	              for(int i=0; i<actionForm.getAdBRListSize(); i++) {
  	              	
  	  	        	AdBranchResponsibilityList brList = actionForm.getAdBRByIndex(i);
  	  	        
  	  	        	if(brList.getBranchCheckbox() == true) {
  	  	        		
  	  	        		AdBranchDetails brDetails = new AdBranchDetails();
  	  		        	
  	  		        	brDetails.setBrAdCompany(user.getCmpCode());
  	  		        	brDetails.setBrCode(brList.getBranchCode());
  	  		        	
  	  		        	branchList.add(brDetails);
  	  	        	}
  	              } 
     	            
     
     	           
     	        	 ejbRS.copyAdRsEntry(details, branchList,user.getCmpCode());
     	        	 System.out.println("OLD Code = "+adRSList.getResponsibilityCode()+" NEW Code = "+details.getRsCode());
     	            
     	                 
      	         } catch (EJBException ex) {
     	 	        	 
      	        	 if (log.isInfoEnabled()) {
     	 	        		 
      	        		 log.info("EJBException caught in AdResponsibiltyAction.execute(): " + ex.getMessage() +
      	        				 " session: " + session.getId());
     	               
      	        		 return mapping.findForward("cmnErrorPage");
     	 	        		 
      	        	 }
     	 	        	 
      	         }
            
/*******************************************************
   -- Ad RS Delete Action --
*******************************************************/

         } else if (request.getParameter("adRSList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdResponsibilityList adRSList =
              actionForm.getAdRSByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbRS.deleteAdRsEntry(adRSList.getResponsibilityCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("responsibility.error.deleteResponsibilityAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("responsibility.error.responsibilityAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar RS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adResponsibility");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               actionForm.clearAdRSList();	        
	           
	           list = ejbRS.getAdRsAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdResponsibilityDetails details = (AdResponsibilityDetails)i.next();
	            	
	              AdResponsibilityList adRSList = new AdResponsibilityList(actionForm,
	            	    details.getRsCode(),
	            	    details.getRsName(),
	            	    details.getRsDescription(),
	            	    Common.convertSQLDateToString(details.getRsDateFrom()),
	            	    Common.convertSQLDateToString(details.getRsDateTo()));
	            	 	            	    
	              actionForm.saveAdRSList(adRSList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }        
	        
	        list = null;
            i = null;
                      	                        	                          	            	
	        try {
	        	
	           actionForm.reset(mapping, request);
	    	
	           actionForm.clearAdBRList();	        
	           
	           list = ejbRS.getAdBrAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			        
	           		AdBranchDetails details = (AdBranchDetails)i.next();
		           	
		           	AdBranchResponsibilityList adBRList = new AdBranchResponsibilityList(actionForm,
		           			details.getBrName(), details.getBrCode());
		           	
		           	if ( request.getParameter("forward") == null )
		           		adBRList.setBranchCheckbox(true);
	            	 	            	    
		           	actionForm.saveAdBRList(adBRList);
	            	
	           }
	           	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
	        
	        
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adResponsibility"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdResponsibilityAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

         e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

      }

   }
   
}