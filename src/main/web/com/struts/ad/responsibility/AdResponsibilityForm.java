package com.struts.ad.responsibility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdBranchDetails;


public class AdResponsibilityForm extends ActionForm implements Serializable {

   private Integer responsibilityCode = null;
   private String responsibilityName = null;
   private String description = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;   
   private String pageState = new String();
   private ArrayList adRSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private ArrayList adBRList = new ArrayList();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdResponsibilityList getAdRSByIndex(int index) {

      return((AdResponsibilityList)adRSList.get(index));

   }

   public Object[] getAdRSList() {

      return adRSList.toArray();

   }

   public int getAdRSListSize() {

      return adRSList.size();

   }

   public void saveAdRSList(Object newAdRSList) {

      adRSList.add(newAdRSList);

   }

   public void clearAdRSList() {
      adRSList.clear();
   }

   public void setRowSelected(Object selectedAdRSList, boolean isEdit) {

      this.rowSelected = adRSList.indexOf(selectedAdRSList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdRSRow(ArrayList branchList, int rowSelected) {
   	
   	   this.responsibilityName = ((AdResponsibilityList)adRSList.get(rowSelected)).getResponsibilityName();
       this.description = ((AdResponsibilityList)adRSList.get(rowSelected)).getDescription();
       this.dateFrom = ((AdResponsibilityList)adRSList.get(rowSelected)).getDateFrom();
       this.dateTo = ((AdResponsibilityList)adRSList.get(rowSelected)).getDateTo();
       
       Object[] obj = this.getAdBRList();
       
       this.clearAdBRList();

       for(int i=0; i<obj.length; i++) {
       		
       		AdBranchResponsibilityList brList = (AdBranchResponsibilityList)obj[i];

       		if(branchList != null) {
       		
	       		Iterator brListIter = branchList.iterator();
	       		
	       		while(brListIter.hasNext()) {
	       			
	       			AdBranchDetails brDetails = (AdBranchDetails)brListIter.next();
	       			if(brDetails.getBrCode().equals(brList.getBranchCode())) {
	       				brList.setBranchCheckbox(true);
	       				break;
	       				
	       			}
	       			
	       		}
       		}
       		
       		this.adBRList.add(brList);
       
       }
       
   }

   public void updateAdRSRow(int rowSelected, Object newAdRSList) {

      adRSList.set(rowSelected, newAdRSList);

   }

   public void deleteAdRSList(int rowSelected) {

      adRSList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getResponsibilityCode() {
   	
   	  return responsibilityCode;
   	
   }
   
   public void setResponsibilityCode(Integer responsibilityCode) {
   	
   	  this.responsibilityCode = responsibilityCode;
   	
   }

   public String getResponsibilityName() {

      return responsibilityName;

   }

   public void setResponsibilityName(String responsibilityName) {

      this.responsibilityName = responsibilityName;

   }
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }

   public String getDateFrom() {

      return dateFrom;

   }

   public void setDateFrom(String dateFrom) {

      this.dateFrom = dateFrom;

   }
   
   public String getDateTo() {

      return dateTo;

   }

   public void setDateTo(String dateTo) {

      this.dateTo = dateTo;

   }   
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }     
   
   public Object[] getAdBRList(){
   	
   	  return adBRList.toArray();
   	  
   }
   
   public AdBranchResponsibilityList getAdBRByIndex(int index){
   	
   	  return ((AdBranchResponsibilityList)adBRList.get(index));
   	  
   }
   
   public int getAdBRListSize(){
   	
   	  return(adBRList.size());
   	  
   }
   
   public void saveAdBRList(Object newAdBRList){
   	
   	  adBRList.add(newAdBRList);   	  
   	  
   }
   
   public void clearAdBRList(){
   	
   	  adBRList.clear();
   	  
   }
   
   public void setAdBRList(ArrayList adBRList) {
   	
   	  this.adBRList = adBRList;
   	
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
        	  
   	  for (int i=0; i<adBRList.size(); i++) {
	  	
	  	  AdBranchResponsibilityList actionList = (AdBranchResponsibilityList)adBRList.get(i);
	  	  actionList.setBranchCheckbox(false);
	  	
	  }
   	
   	  responsibilityName = null; 
      description = null;
      dateFrom = null;
      dateTo = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(responsibilityName)) {

            errors.add("responsibilityName",
               new ActionMessage("responsibility.error.responsibilityNameRequired"));

         }

         if (Common.validateRequired(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("responsibility.error.dateFromRequired"));

         }
         
         if (!Common.validateDateFormat(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("responsibility.error.dateFromInvalid"));

         }         
         
         if (!Common.validateDateFormat(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("responsibility.error.dateToInvalid"));

         }                  
         
	     if (!Common.validateDateFromTo(dateFrom, dateTo)){
	   
	        errors.add("dateFrom", 
	           new ActionMessage("responsibility.error.dateFromToInvalid"));
	 
	     }                  

      }
         
      return errors;

   }
}