package com.struts.ad.responsibility;

import java.io.Serializable;
import java.util.ArrayList;

public class AdResponsibilityList implements Serializable {

   private Integer responsibilityCode = null;
   private String responsibilityName = null;
   private String description = null;
   private String dateFrom = null;
   private String dateTo = null;
   
   private ArrayList adBRList = new ArrayList();

   private String editButton = null;
   private String deleteButton = null;
   private String copyButton = null;
   private String formFunctionResponsibilitiesButton = null;
    
   private AdResponsibilityForm parentBean;
    
   public AdResponsibilityList(AdResponsibilityForm parentBean,
      Integer responsibilityCode,
      String responsibilityName,
      String description,
      String dateFrom,
      String dateTo) {

      this.parentBean = parentBean;
      this.responsibilityCode = responsibilityCode;
      this.responsibilityName = responsibilityName;
      this.description = description;
      this.dateFrom = dateFrom;
      this.dateTo = dateTo;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, false);

   }   
   
   public void setCopyButton(String copyButton) {

	   parentBean.setRowSelected(this, false);

   }   
   
   public void setFormFunctionResponsibilitiesButton(String formFunctionResponsibilitiesButton) {
   	
   	  parentBean.setRowSelected(this, false);
   	  
   }

   public Integer getResponsibilityCode() {

      return responsibilityCode;

   }
   
   public String getResponsibilityName() {
       
       return responsibilityName;
       
   }

   public String getDescription() {

      return description;

   }

   public String getDateFrom() {

      return dateFrom;

   }
   
   public String getDateTo() {

      return dateTo;

   }
   
   public Object[] getAdBRList(){
   	
   	  return adBRList.toArray();
   	  
   }

}