package com.struts.ad.storedproceduresetup;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.AdStoredProcedureSetupController;
import com.ejb.txn.AdStoredProcedureSetupControllerHome;


import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdStoredProcedureDetails;

public final class AdStoredProcedureSetupAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdStoredProcedureSetupAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdStoredProcedureSetupForm actionForm = (AdStoredProcedureSetupForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_STORED_PROCEDURE_SETUP_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adStoredProcedureSetup");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize adStoredProcedureSetupController EJB
*******************************************************/

         AdStoredProcedureSetupControllerHome homeSP = null;
         AdStoredProcedureSetupController ejbSP = null;

         try {

        	 homeSP = (AdStoredProcedureSetupControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdStoredProcedureSetupControllerEJB", AdStoredProcedureSetupControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdStoredProcedureSetupAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbSP = homeSP.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdStoredProcedureSetupAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad AS Close Action --
*******************************************************/

         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad AS Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
        	 
        	 AdStoredProcedureDetails details = new AdStoredProcedureDetails();
        	 details.setSpCode(actionForm.getStoredProcedureCode());
        	 
        	 System.out.println("actionForm.getEnableGlGeneralLedgerReport()="+actionForm.getEnableGlGeneralLedgerReport());
        	 System.out.println("actionForm.getNameGlGeneralLedgerReport()="+actionForm.getNameGlGeneralLedgerReport());
        	 
        	 System.out.println("actionForm.getEnableGlTrialBalanceReport()="+actionForm.getEnableGlTrialBalanceReport());
        	 System.out.println("actionForm.getNameGlTrialBalanceReport()="+actionForm.getNameGlTrialBalanceReport());
        	 
        	 System.out.println("actionForm.getEnableGlIncomeStatementReport()="+actionForm.getEnableGlIncomeStatementReport());
        	 System.out.println("actionForm.getNameGlIncomeStatementReport()="+actionForm.getNameGlIncomeStatementReport());
        	 
        	 System.out.println("actionForm.getEnableGlBalanceSheetReport()="+actionForm.getEnableGlBalanceSheetReport());
        	 System.out.println("actionForm.getNameGlBalanceSheetReport()="+actionForm.getNameGlBalanceSheetReport());
        	 
        	 System.out.println("actionForm.getEnableArStatementOfAccountReport()="+actionForm.getEnableArStatementOfAccountReport());
        	 System.out.println("actionForm.getNameArStatementOfAccountReport()="+actionForm.getNameArStatementOfAccountReport());
        	 
        	 
        	 details.setSpEnableGlGeneralLedgerReport(Common.convertBooleanToByte(actionForm.getEnableGlGeneralLedgerReport()));
        	 details.setSpNameGlGeneralLedgerReport(actionForm.getNameGlGeneralLedgerReport());
        	 
        	 details.setSpEnableGlTrialBalanceReport(Common.convertBooleanToByte(actionForm.getEnableGlTrialBalanceReport()));
        	 details.setSpNameGlTrialBalanceReport(actionForm.getNameGlTrialBalanceReport());
        	 
        	 details.setSpEnableGlIncomeStatementReport(Common.convertBooleanToByte(actionForm.getEnableGlIncomeStatementReport()));
        	 details.setSpNameGlIncomeStatementReport(actionForm.getNameGlIncomeStatementReport());
        	 
        	 details.setSpEnableGlBalanceSheetReport(Common.convertBooleanToByte(actionForm.getEnableGlBalanceSheetReport()));
        	 details.setSpNameGlBalanceSheetReport(actionForm.getNameGlBalanceSheetReport());
        	 
                 
                 details.setSpEnableArStatementOfAccountReport(Common.convertBooleanToByte(actionForm.getEnableArStatementOfAccountReport()));
        	 details.setSpNameArStatementOfAccountReport(actionForm.getNameArStatementOfAccountReport());
              
            try {
            	
            	ejbSP.saveAdSpEntry(details, user.getCmpCode());

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdStoredProcedureSetupAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
	 -- Ad AS Approval Document Action --
*******************************************************/

         // forward to approval document module             	
            	
/*         } else if (request.getParameter("approvalDocumentButton") != null) {

			return(new ActionForward("/adApprovalDocument.do"));     */        
            
/*******************************************************
   -- Ad AS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adStoredProcedureSetup");

            }
                         	                          	            	
	        try {
	        	
            	AdStoredProcedureDetails details = ejbSP.getAdSp(user.getCmpCode());
            		
				actionForm.setEnableGlGeneralLedgerReport(Common.convertByteToBoolean(details.getSpEnableGlGeneralLedgerReport()));
				actionForm.setNameGlGeneralLedgerReport(details.getSpNameGlGeneralLedgerReport());
				actionForm.setEnableGlTrialBalanceReport(Common.convertByteToBoolean(details.getSpEnableGlTrialBalanceReport()));
				actionForm.setNameGlTrialBalanceReport(details.getSpNameGlTrialBalanceReport());
				actionForm.setEnableGlIncomeStatementReport(Common.convertByteToBoolean(details.getSpEnableGlIncomeStatementReport()));
				actionForm.setNameGlIncomeStatementReport(details.getSpNameGlIncomeStatementReport());
				actionForm.setEnableGlBalanceSheetReport(Common.convertByteToBoolean(details.getSpEnableGlBalanceSheetReport()));
				actionForm.setNameGlBalanceSheetReport(details.getSpNameGlBalanceSheetReport());
                                actionForm.setEnableArStatementOfAccountReport(Common.convertByteToBoolean(details.getSpEnableArStatementOfAccountReport()));
				actionForm.setNameArStatementOfAccountReport(details.getSpNameArStatementOfAccountReport());
			
				
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdStoredProcedureSetupAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("adStoredProcedureSetup"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdStoredProcedureSetupAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}