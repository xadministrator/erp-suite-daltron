package com.struts.ad.user;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdUserController;
import com.ejb.txn.AdUserControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdUserDetails;

public final class AdUserAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdUserAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdUserForm actionForm = (AdUserForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_USER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adUser");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdUserController EJB
*******************************************************/

         AdUserControllerHome homeUSR = null;
         AdUserController ejbUSR = null;

         try {

            homeUSR = (AdUserControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdUserControllerEJB", AdUserControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdUserAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbUSR = homeUSR.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdUserAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad USR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adUser"));
	     
/*******************************************************
   -- Ad USR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adUser"));                  
         
/*******************************************************
   -- Ad USR Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdUserDetails details = new AdUserDetails();
            details.setUsrName(actionForm.getUserName());
            details.setUsrDept(actionForm.getUserDepartment());
            details.setUsrDescription(actionForm.getDescription());
            details.setUsrPosition(actionForm.getPosition());
            details.setUsrEmailAddress(actionForm.getEmailAddress());
            details.setUsrInspector(Common.convertBooleanToByte(actionForm.getInspector()));
            details.setUsrHead(Common.convertBooleanToByte(actionForm.getHead()));
                       
            details.setUsrPassword(actionForm.getPassword());
            
	        if(actionForm.getPasswordExpirationCode().equals(Constants.AD_USR_DAYS)) {
	                  
	           details.setUsrPasswordExpirationCode((short)0);
	        
	        } else if(actionForm.getPasswordExpirationCode().equals(Constants.AD_USR_ACCESS)) {
	           
	           details.setUsrPasswordExpirationCode((short)1);
	        
	        } else if(actionForm.getPasswordExpirationCode().equals(Constants.AD_USR_NONE)) {
	        
	           details.setUsrPasswordExpirationCode((short)2);	
	        
	        }	
            
            details.setUsrPasswordExpirationDays(Common.convertStringToShort(actionForm.getPasswordExpirationDays()));
            details.setUsrPasswordExpirationAccess(Common.convertStringToShort(actionForm.getPasswordExpirationAccess()));
            details.setUsrDateFrom(Common.convertStringToSQLDate(actionForm.getDateFrom()));
            details.setUsrDateTo(Common.convertStringToSQLDate(actionForm.getDateTo()));
            
            try {
            	
            	ejbUSR.addAdUsrEntry(details, null, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("user.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad USR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad USR Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdUserList adUSRList =
	            actionForm.getAdUSRByIndex(actionForm.getRowSelected());
                        
            AdUserDetails details = new AdUserDetails();
            details.setUsrCode(adUSRList.getUserCode());
            details.setUsrName(actionForm.getUserName());
            details.setUsrDept(actionForm.getUserDepartment());
            details.setUsrDescription(actionForm.getDescription());
            details.setUsrHead(Common.convertBooleanToByte(actionForm.getHead()));
            details.setUsrPassword(actionForm.getPassword());
            details.setUsrPosition(actionForm.getPosition());
            details.setUsrEmailAddress(actionForm.getEmailAddress());
            details.setUsrInspector(Common.convertBooleanToByte(actionForm.getInspector()));
	        if(actionForm.getPasswordExpirationCode().equals(Constants.AD_USR_DAYS)) {
	                  
	           details.setUsrPasswordExpirationCode((short)0);
	        
	        } else if(actionForm.getPasswordExpirationCode().equals(Constants.AD_USR_ACCESS)) {
	           
	           details.setUsrPasswordExpirationCode((short)1);
	        
	        } else if(actionForm.getPasswordExpirationCode().equals(Constants.AD_USR_NONE)) {
	        
	           details.setUsrPasswordExpirationCode((short)2);	
	        
	        }          

            details.setUsrPasswordExpirationDays(Common.convertStringToShort(actionForm.getPasswordExpirationDays()));
            details.setUsrPasswordExpirationAccess(Common.convertStringToShort(actionForm.getPasswordExpirationAccess()));
            details.setUsrDateFrom(Common.convertStringToSQLDate(actionForm.getDateFrom()));
            details.setUsrDateTo(Common.convertStringToSQLDate(actionForm.getDateTo()));
            
            try {
            	
            	ejbUSR.updateAdUsrEntry(details, null, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("user.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad USR Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad USR Edit Action --
*******************************************************/

         } else if (request.getParameter("adUSRList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdUSRRow(actionForm.getRowSelected());
            
            return mapping.findForward("adUser");
            
/*******************************************************
   -- Ad USR User Responsibility Action --
*******************************************************/

         } else if (request.getParameter("adUSRList[" +
            actionForm.getRowSelected() + "].userResponsibilityButton") != null){
            
            AdUserList adUSRList =
	            actionForm.getAdUSRByIndex(actionForm.getRowSelected());

	        String path = "/adUserResponsibility.do?forward=1" +
	           "&userCode=" + adUSRList.getUserCode() + 
	           "&userName=" +  adUSRList.getUserName() +
	           "&userDepartment=" +  adUSRList.getUserDepartment() +
	           "&description=" + adUSRList.getDescription() +
	           "&head=" + adUSRList.getHead() +
	           "&position=" + adUSRList.getPosition() +
	           "&emailAddress=" + adUSRList.getEmailAddress() +
	           "&inspector=" + adUSRList.getInspector() +
	           "&password=" + adUSRList.getPassword() + 
	           "&passwordExpirationCode=" + adUSRList.getPasswordExpirationCode() + 
	           "&passwordExpirationDays=" + adUSRList.getPasswordExpirationDays() + 
	           "&passwordExpirationAccess=" + adUSRList.getPasswordExpirationAccess() + 
	           "&dateFrom=" + adUSRList.getDateFrom() + 
	           "&dateTo=" + adUSRList.getDateTo(); 
	        
	        return(new ActionForward(path));            
            
/*******************************************************
   -- Ad USR Delete Action --
*******************************************************/

         } else if (request.getParameter("adUSRList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdUserList adUSRList =
	            actionForm.getAdUSRByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbUSR.deleteAdUsrEntry(adUSRList.getUserCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("user.error.userAlreadyDeleted"));
               
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	           errors.add(ActionMessages.GLOBAL_MESSAGE,
	  	              new ActionMessage("user.error.userAlreadyAssigned"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdUserAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad USR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adUser");

            }
            
            ArrayList list = null;
            Iterator i = null;
            String EXP_CODE = null; 
            
                 	                        	                          	            	
	        try {
	        	actionForm.clearAdUSRList(); 
	            
	           list = ejbUSR.getAdUsrAll(user.getCmpCode()); 
	           i = list.iterator();
	           
	
	            
	           while(i.hasNext()) {
    	            			            	
	              AdUserDetails details = (AdUserDetails)i.next();
	              
	              if(details.getUsrPasswordExpirationCode() == 0) {
	              	
	              	 EXP_CODE = Constants.AD_USR_DAYS;	              	
	              
	              } else if(details.getUsrPasswordExpirationCode() == 1) {
	              	
	              	 EXP_CODE = Constants.AD_USR_ACCESS;

	              } else if(details.getUsrPasswordExpirationCode() == 2) {
	              	
	              	 EXP_CODE = Constants.AD_USR_NONE;
	              
	              }		
	            	
	              AdUserList adUSRList = new AdUserList(actionForm,
	            	    details.getUsrCode(),
	            	    details.getUsrName(),
	            	    details.getUsrDept(),
	            	    details.getUsrDescription(),
	            	    details.getUsrPosition(),
	            	    details.getUsrEmailAddress(),
	            	    Common.convertByteToBoolean(details.getUsrHead()),
	            	    Common.convertByteToBoolean(details.getUsrInspector()),
	            	    details.getUsrPassword(),
	            	    EXP_CODE,
	            	    Common.convertShortToString(details.getUsrPasswordExpirationDays()),
	            	    Common.convertShortToString(details.getUsrPasswordExpirationAccess()),
	            	    Common.convertSQLDateToString(details.getUsrDateFrom()),
	            	    Common.convertSQLDateToString(details.getUsrDateTo()));
	            	 	            	    
	              actionForm.saveAdUSRList(adUSRList);
	            	
	           }
	           
			   
			   actionForm.clearUserDepartmentList();
			   
			   list = ejbUSR.getAdLvDEPARTMENT(user.getCmpCode());
		       i = list.iterator();
		       
				if (list == null || list.size() == 0) {  		
					actionForm.setUserDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);
				} else {     		            		
					i = list.iterator();
					while (i.hasNext()) {
					    actionForm.setUserDepartmentList((String)i.next());
					}
				}
	           
	           
	           
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
            
           if (log.isInfoEnabled()) {

              log.info("EJBException caught in AdUserAction.execute(): " + ex.getMessage() +
              " session: " + session.getId());
              return mapping.findForward("cmnErrorPage"); 
              
           }

        }
                  

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adUser"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdUserAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}