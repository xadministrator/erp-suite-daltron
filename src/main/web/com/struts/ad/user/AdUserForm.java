package com.struts.ad.user;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdUserForm extends ActionForm implements Serializable {

   private Integer userCode = null;
   private String userName = null;
   private String description = null;
   private String position = null;
   private String emailAddress = null;
   private boolean head = false;
   private boolean inspector = false;
   private String password = null;
   private String confirmPassword = null;
   private String passwordExpirationCode = null;
   private ArrayList passwordExpirationCodeList = new ArrayList();
   
   private String userDepartment = null;
   private ArrayList userDepartmentList = new ArrayList();
   
   private String passwordExpirationDays = null;
   private String passwordExpirationAccess = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;   
   private String pageState = new String();
   private ArrayList adUSRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   
   
   
   
   
   
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdUserList getAdUSRByIndex(int index) {

      return((AdUserList)adUSRList.get(index));

   }

   public Object[] getAdUSRList() {

      return adUSRList.toArray();

   }

   public int getAdABListSize() {

      return adUSRList.size();

   }

   public void saveAdUSRList(Object newAdUSRList) {

      adUSRList.add(newAdUSRList);

   }

   public void clearAdUSRList() {
      adUSRList.clear();
   }

   public void setRowSelected(Object selectedAdUSRList, boolean isEdit) {

      this.rowSelected = adUSRList.indexOf(selectedAdUSRList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdUSRRow(int rowSelected) {
   	
       this.userName = ((AdUserList)adUSRList.get(rowSelected)).getUserName();
       this.userDepartment = ((AdUserList)adUSRList.get(rowSelected)).getUserDepartment();
       this.description = ((AdUserList)adUSRList.get(rowSelected)).getDescription();
       this.position = ((AdUserList)adUSRList.get(rowSelected)).getPosition();
       this.emailAddress = ((AdUserList)adUSRList.get(rowSelected)).getEmailAddress();
       this.head = ((AdUserList)adUSRList.get(rowSelected)).getHead();
       this.inspector = ((AdUserList)adUSRList.get(rowSelected)).getInspector();
       this.password = ((AdUserList)adUSRList.get(rowSelected)).getPassword();
       this.confirmPassword = ((AdUserList)adUSRList.get(rowSelected)).getPassword();
       this.passwordExpirationCode = ((AdUserList)adUSRList.get(rowSelected)).getPasswordExpirationCode();
       this.passwordExpirationDays = ((AdUserList)adUSRList.get(rowSelected)).getPasswordExpirationDays();
       this.passwordExpirationAccess = ((AdUserList)adUSRList.get(rowSelected)).getPasswordExpirationAccess();
       this.dateFrom = ((AdUserList)adUSRList.get(rowSelected)).getDateFrom();
       this.dateTo = ((AdUserList)adUSRList.get(rowSelected)).getDateTo();
       
   }

   public void updateAdUSRRow(int rowSelected, Object newAdUSRList) {

      adUSRList.set(rowSelected, newAdUSRList);

   }

   public void deleteAdUSRList(int rowSelected) {

      adUSRList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getUserCode() {
   	
   	  return userCode;
   	
   }
   
   public void setUserCode(Integer userCode) {
   	
   	  this.userCode = userCode;
   	
   }

   public String getUserName() {

      return userName;

   }

   public void setUserName(String userName) {

      this.userName = userName;

   }
   
   
   
   
   
   
   
   
	public String getUserDepartment() {
		   	
		   return userDepartment;
		   	
	}
	
	public void setUserDepartment(String userDepartment) {
		
		  this.userDepartment = userDepartment;
		
	}
	
	public ArrayList getUserDepartmentList() {
		
		  return userDepartmentList;
		
	}
	
	public void setUserDepartmentList(String userDepartment) {
		
		  userDepartmentList.add(userDepartment);
		
	}
	
	public void clearUserDepartmentList() {
		
		  userDepartmentList.clear();
		  userDepartmentList.add(Constants.GLOBAL_BLANK);
		
	}


	
	
	public boolean getHead() {
		
		return head;
		
	}
	
	public void setHead(boolean head) {
		
		this.head = head;
		
	}

	public boolean getInspector() {
		
		return inspector;
		
	}
	
	public void setInspector(boolean inspector) {
		
		this.inspector = inspector;
		
	}

	public String getPosition() {

	      return position;

	   }

	public void setPosition(String position) {

		this.position = position;

	}
	
	
	public String getEmailAddress() {

	      return emailAddress;

	   }

	public void setEmailAddress(String emailAddress) {

		this.emailAddress = emailAddress;

	}
   
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }

   public String getPassword() {

      return password;

   }

   public void setPassword(String password) {

      this.password = password;

   }
   
   public String getPasswordExpirationCode() {

      return passwordExpirationCode;

   }
   
   public void setConfirmPassword(String confirmPassword) {
   	
   	  this.confirmPassword = confirmPassword;
   	
   }
   
   public String getConfirmPassword() {
   	
   	  return confirmPassword;
   	
   }

   public void setPasswordExpirationCode(String passwordExpirationCode) {

      this.passwordExpirationCode = passwordExpirationCode;

   }   

   public ArrayList getPasswordExpirationCodeList() {
   	
   	   return passwordExpirationCodeList;
   	
   }   
   
   public String getPasswordExpirationDays() {

      return passwordExpirationDays;

   }

   public void setPasswordExpirationDays(String passwordExpirationDays) {

      this.passwordExpirationDays = passwordExpirationDays;

   }
   
   public String getPasswordExpirationAccess() {

      return passwordExpirationAccess;

   }

   public void setPasswordExpirationAccess(String passwordExpirationAccess) {

      this.passwordExpirationAccess = passwordExpirationAccess;

   } 
   
   public String getDateFrom() {

      return dateFrom;

   }
   
   public void setDateFrom(String dateFrom) {

      this.dateFrom = dateFrom;

   }      
  
   public String getDateTo() {

      return dateTo;

   }
   
   public void setDateTo(String dateTo) {
   	
      this.dateTo = dateTo;

   }    
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }             
          
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      userName = null; 
      description = null;
      position = null;
      emailAddress = null;
      head = false;
      inspector = false;
      password = null;
      confirmPassword = null;
      passwordExpirationCodeList.clear();
      passwordExpirationCodeList.add(Constants.AD_USR_DAYS);
      passwordExpirationCodeList.add(Constants.AD_USR_ACCESS);
      passwordExpirationCodeList.add(Constants.AD_USR_NONE);
      passwordExpirationCode = Constants.AD_USR_DAYS; 
      passwordExpirationDays = null;
      passwordExpirationAccess = null;
      userDepartment = Constants.GLOBAL_BLANK;
      dateFrom = null;
      dateTo = null;    
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
      showDetailsButton = null;
      hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(userName)) {

            errors.add("userName",
               new ActionMessage("user.error.userNameRequired"));

         }
         
         if (Common.validateRequired(description)) {

             errors.add("userName",
                new ActionMessage("user.error.descriptionRequired"));

          }
         
         if (Common.validateRequired(password)) {

            errors.add("password",
               new ActionMessage("user.error.passwordRequired"));

         }
         
         if (Common.validateRequired(confirmPassword)) {

            errors.add("confirmPassword",
               new ActionMessage("user.error.confirmPasswordRequired"));

         }
         
         if (!Common.validateRequired(password) && !Common.validateRequired(confirmPassword) &&
             !password.equals(confirmPassword)) {
             	
             errors.add("confirmPassword",
               new ActionMessage("user.error.passwordNotConfirmed"));
               
         }
         

         if (Common.validateRequired(passwordExpirationCode)) {

            errors.add("passwordExpirationCode",
               new ActionMessage("user.error.passwordExpirationCodeRequired"));

         }          
         
		     if(passwordExpirationCode.equals(Constants.AD_USR_DAYS)) {
		     
			     if (Common.validateRequired(passwordExpirationDays)) {
			
			        errors.add("passwordExpirationDays",
			           new ActionMessage("user.error.passwordExpirationDaysRequired"));
			
			     }    
			     
			     if (!Common.validateNumberFormat(passwordExpirationDays)) {
			
			        errors.add("passwordExpirationDays",
			           new ActionMessage("user.error.passwordExpirationDaysInvalid"));
			
			     }
			     
			     
			     
			 } else if (passwordExpirationCode.equals(Constants.AD_USR_ACCESS)) { 		                             
			     
			     if (Common.validateRequired(passwordExpirationAccess)) {
			
			        errors.add("passwordExpirationAccess",
			           new ActionMessage("user.error.passwordExpirationAccessRequired"));
			
			     }
			     
			     if (!Common.validateNumberFormat(passwordExpirationAccess)) {
			
			        errors.add("passwordExpirationAccess",
			           new ActionMessage("user.error.passwordExpirationAccessInvalid"));
			
			     }
			     
			 } else if (passwordExpirationCode.equals(Constants.AD_USR_NONE)) {  
		
		     }
         
         
         if (Common.validateRequired(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("user.error.dateFromRequired"));

         }         
                          
         
         if (!Common.validateNumberFormat(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("user.error.dateFromInvalid"));

         }                     
         
         if (!Common.validateNumberFormat(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("user.error.dateToInvalid"));

         } 
         
         if(!Common.validateDateFromTo(dateFrom, dateTo)) {
            errors.add("dateFrom", 
               new ActionMessage("user.error.dateFromToInvalid"));
               
         }                                

      }
         
      return errors;

   }
}