package com.struts.ad.user;

import java.io.Serializable;

public class AdUserList implements Serializable {

   private Integer userCode = null;
   private String userName = null;
   private String userDepartment = null;
   private String description = null;
   private String position = null;
   private String emailAddress = null;
   private boolean head = false;
   private boolean inspector = false;
   private String password = null;
   private String passwordExpirationCode = null;
   private String passwordExpirationDays = null;
   private String passwordExpirationAccess = null;
   private String dateFrom = null;
   private String dateTo = null;

   private String editButton = null;
   private String deleteButton = null;
   private String userResponsibilityButton = null;
    
   private AdUserForm parentBean;
    
   public AdUserList(AdUserForm parentBean,
      Integer userCode,
      String userName,
      String userDepartment,
      String description,
      String position,
      String emailAddress,
      boolean head,
      boolean inspector,
      String password,
      String passwordExpirationCode,
      String passwordExpirationDays,
      String passwordExpirationAccess,
      String dateFrom,
      String dateTo) {

      this.parentBean = parentBean;
      this.userCode = userCode;
      this.userName = userName;
      this.userDepartment = userDepartment;
      this.description = description;
      this.position = position;
      this.emailAddress = emailAddress;
      this.head = head;
      this.inspector = inspector;
      this.password = password;
      this.passwordExpirationCode = passwordExpirationCode;
      this.passwordExpirationDays = passwordExpirationDays;
      this.passwordExpirationAccess = passwordExpirationAccess;
      this.dateFrom = dateFrom;
      this.dateTo = dateTo;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   
   
   public void setUserResponsibilityButton(String userResponsibilityButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }

   public Integer getUserCode() {

      return userCode;

   }
   
   public String getUserName() {
       
       return userName;
       
   }
   
	public String getUserDepartment() {
	       
	       return userDepartment;
	       
	  }

   public String getDescription() {

      return description;

   }
   
   public String getPosition() {

	   return position;

   }
   
   public String getEmailAddress() {

	   return emailAddress;

   }
   
   public boolean getHead() {

	   return head;

   }
   public boolean getInspector() {

	   return inspector;

   }

   public String getPassword() {

      return password;

   }
   
   public String getPasswordExpirationCode() {

      return passwordExpirationCode;

   }
   
   public String getPasswordExpirationDays() {

      return passwordExpirationDays;

   }
   
   public String getPasswordExpirationAccess() {

      return passwordExpirationAccess;

   } 
   
   public String getDateFrom() {

      return dateFrom;

   }  
  
   public String getDateTo() {

      return dateTo;

   }           

}