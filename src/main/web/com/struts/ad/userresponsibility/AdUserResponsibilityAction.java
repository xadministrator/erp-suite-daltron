package com.struts.ad.userresponsibility;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdUserResponsibilityController;
import com.ejb.txn.AdUserResponsibilityControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModUserResponsibilityDetails;
import com.util.AdUserDetails;
import com.util.AdUserResponsibilityDetails;

public final class AdUserResponsibilityAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdUserResponsibilityAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdUserResponsibilityForm actionForm = (AdUserResponsibilityForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_USER_RESPONSIBILITY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adUserResponsibility");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize AdUserResponsibilityController EJB
*******************************************************/

         AdUserResponsibilityControllerHome homeUR = null;
         AdUserResponsibilityController ejbUR = null;

         try {

            homeUR = (AdUserResponsibilityControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/AdUserResponsibilityControllerEJB", AdUserResponsibilityControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in AdUserResponsibilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbUR = homeUR.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in AdUserResponsibilityAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad UR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("adUserResponsibility"));
	     
/*******************************************************
   -- Ad UR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("adUserResponsibility"));                  
         
/*******************************************************
   -- Ad UR Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            AdUserResponsibilityDetails details = new AdUserResponsibilityDetails();
            details.setUrDateFrom(Common.convertStringToSQLDate(actionForm.getUserDateFrom()));
            details.setUrDateTo(Common.convertStringToSQLDate(actionForm.getUserDateTo()));
            
            try {
            	
            	ejbUR.addAdUrEntry(details, actionForm.getUserCode(), 
            	    actionForm.getResponsibility(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("userResponsibility.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdUserResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ad UR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad UR Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdUserResponsibilityList adURList =
	            actionForm.getAdURByIndex(actionForm.getRowSelected());
                        
            AdUserResponsibilityDetails details = new AdUserResponsibilityDetails();
            details.setUrCode(adURList.getUserResponsibilityCode());
            details.setUrDateFrom(Common.convertStringToSQLDate(actionForm.getUserDateFrom()));
            details.setUrDateTo(Common.convertStringToSQLDate(actionForm.getUserDateTo()));
            
            try {
            	
            	ejbUR.updateAdUrEntry(details, actionForm.getUserCode(),
            	     actionForm.getResponsibility(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("userResponsibility.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdUserResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ad UR Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad UR Edit Action --
*******************************************************/

         } else if (request.getParameter("adURList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showAdURRow(actionForm.getRowSelected());
            
            return mapping.findForward("adUserResponsibility");        
            
/*******************************************************
   -- Ad UR Delete Action --
*******************************************************/

         } else if (request.getParameter("adURList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            AdUserResponsibilityList adURList =
	            actionForm.getAdURByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbUR.deleteAdUrEntry(adURList.getUserResponsibilityCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("userResponsibility.error.userResponsibilityAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in AdUserResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ad UR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adUserResponsibility");

            }
                                  	                        	                          	            	
            AdUserDetails usrDetails = null;
            String USR_EXP_CODE = null;
            

	        if (request.getParameter("forward") != null) {

	            usrDetails = ejbUR.getAdUsrByUsrCode(new Integer(request.getParameter("userCode")), user.getCmpCode());        		
	    	    actionForm.setUserCode(new Integer(request.getParameter("userCode")));

	        } else {
	     		
	            usrDetails = ejbUR.getAdUsrByUsrCode(actionForm.getUserCode(), user.getCmpCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setUserCode(actionForm.getUserCode());
            actionForm.setUserName(usrDetails.getUsrName());
            actionForm.setDescription(usrDetails.getUsrDescription());                        	
            			
			if(usrDetails.getUsrPasswordExpirationCode() == 0) {
			
				USR_EXP_CODE = Constants.AD_USR_DAYS;	              	
			
			} else if(usrDetails.getUsrPasswordExpirationCode() == 1) {
			
				USR_EXP_CODE = Constants.AD_USR_ACCESS;
			
			} else if(usrDetails.getUsrPasswordExpirationCode() == 2) {
			
				USR_EXP_CODE = Constants.AD_USR_NONE;
			
			}	            
                    		        		
    	    actionForm.setPasswordExpirationCode(USR_EXP_CODE);
            actionForm.setPasswordExpirationDays(Common.convertShortToString(usrDetails.getUsrPasswordExpirationDays()));
            actionForm.setPasswordExpirationAccess(Common.convertShortToString(usrDetails.getUsrPasswordExpirationAccess()));
            actionForm.setDateFrom(Common.convertSQLDateToString(usrDetails.getUsrDateFrom()));
            actionForm.setDateTo(Common.convertSQLDateToString(usrDetails.getUsrDateTo()));
              
            ArrayList list = null;
            Iterator i = null;
                       

	        try {	        	
	        	
            	actionForm.clearResponsibilityList();
            	
            	list = ejbUR.getAdRsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setResponsibilityList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setResponsibilityList((String)i.next());
            			
            		}
            		
            	} 	        	
	        	
	        	actionForm.clearAdURList();
	        		        		    	
	           list = ejbUR.getAdUrByUsrCode(actionForm.getUserCode(), user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              AdModUserResponsibilityDetails mdetails = (AdModUserResponsibilityDetails)i.next();
	            	
	              AdUserResponsibilityList adURList = new AdUserResponsibilityList(actionForm,
	            	    mdetails.getUrCode(),
	            	    mdetails.getUrResponsibilityName(),
	            	    Common.convertSQLDateToString(mdetails.getUrDateFrom()),
	            	    Common.convertSQLDateToString(mdetails.getUrDateTo()));
	            	 	            	    
	              actionForm.saveAdURList(adURList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in AdUserResponsibilityAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("adUserResponsibility"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdUserResponsibilityAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}