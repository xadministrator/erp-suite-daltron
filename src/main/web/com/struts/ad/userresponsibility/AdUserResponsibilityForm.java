package com.struts.ad.userresponsibility;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class AdUserResponsibilityForm extends ActionForm implements Serializable {

   private Integer userCode = null;
   private String userName = null;
   private String description = null;
   private String passwordExpirationCode = null;
   private String passwordExpirationDays = null;
   private String passwordExpirationAccess = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	   
   private String responsibility = null;
   private ArrayList responsibilityList = new ArrayList();
   private String userDateFrom = null; 
   private String userDateTo = null;   
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList adURList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdUserResponsibilityList getAdURByIndex(int index) {

      return((AdUserResponsibilityList)adURList.get(index));

   }

   public Object[] getAdURList() {

      return adURList.toArray();

   }

   public int getAdURListSize() {

      return adURList.size();

   }

   public void saveAdURList(Object newAdURList) {

      adURList.add(newAdURList);

   }

   public void clearAdURList() {
      adURList.clear();
   }

   public void setRowSelected(Object selectedAdURList, boolean isEdit) {

      this.rowSelected = adURList.indexOf(selectedAdURList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showAdURRow(int rowSelected) {
   	
       this.responsibility = ((AdUserResponsibilityList)adURList.get(rowSelected)).getResponsibility();
       this.userDateFrom = ((AdUserResponsibilityList)adURList.get(rowSelected)).getUserDateFrom();
       this.userDateTo = ((AdUserResponsibilityList)adURList.get(rowSelected)).getUserDateTo();
       
   }

   public void updateAdURRow(int rowSelected, Object newAdURList) {

      adURList.set(rowSelected, newAdURList);

   }

   public void deleteAdURList(int rowSelected) {

      adURList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getUserCode() {
   	
   	  return userCode;
   	
   }
   
   public void setUserCode(Integer userCode) {
   	
   	  this.userCode = userCode;
   	
   }
   
   public String getUserName() {

      return userName;

   }

   public void setUserName(String userName) {

      this.userName = userName;

   }
   
   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }
   
   public String getPasswordExpirationCode() {

      return passwordExpirationCode;

   }

   public void setPasswordExpirationCode(String passwordExpirationCode) {

      this.passwordExpirationCode = passwordExpirationCode;

   }    
   
   public String getPasswordExpirationDays() {

      return passwordExpirationDays;

   }

   public void setPasswordExpirationDays(String passwordExpirationDays) {

      this.passwordExpirationDays = passwordExpirationDays;

   }
   
   public String getPasswordExpirationAccess() {

      return passwordExpirationAccess;

   }

   public void setPasswordExpirationAccess(String passwordExpirationAccess) {

      this.passwordExpirationAccess = passwordExpirationAccess;

   } 
   
   public String getDateFrom() {

      return dateFrom;

   }
   
   public void setDateFrom(String dateFrom) {

      this.dateFrom = dateFrom;

   }      
  
   public String getDateTo() {

      return dateTo;

   }
   
   public void setDateTo(String dateTo) {
   	
      this.dateTo = dateTo;

   }      
   
   public String getResponsibility() {

      return responsibility;

   }

   public void setResponsibility(String responsibility) {

      this.responsibility = responsibility;

   }

   public ArrayList getResponsibilityList() {
   	
   	   return responsibilityList;
   	
   }
   
   public void setResponsibilityList(String responsibility) {

      responsibilityList.add(responsibility);

   }
   
   public void clearResponsibilityList() {

      responsibilityList.clear();
      responsibilityList.add(Constants.GLOBAL_BLANK);
   }   

   public String getUserDateFrom() {

      return userDateFrom;

   }
   
   public void setUserDateFrom(String userDateFrom) {

      this.userDateFrom = userDateFrom;

   }      
  
   public String getUserDateTo() {

      return userDateTo;

   }
   
   public void setUserDateTo(String userDateTo) {
   	
      this.userDateTo = userDateTo;

   }      
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }         
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      responsibility = Constants.GLOBAL_BLANK; 
      userDateFrom = null;
      userDateTo = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(responsibility)) {

            errors.add("responsibility",
               new ActionMessage("userResponsibility.error.responsibilityRequired"));

         }

         if (Common.validateRequired(userDateFrom)) {

            errors.add("userDateFrom",
               new ActionMessage("userResponsibility.error.userDateFromRequired"));

         }     
                          
         
         if (!Common.validateNumberFormat(userDateFrom)) {

            errors.add("userDateFrom",
               new ActionMessage("userResponsibility.error.userDateFromInvalid"));

         }              
                          
         
         if (!Common.validateNumberFormat(userDateTo)) {

            errors.add("userDateTo",
               new ActionMessage("userResponsibility.error.userDateToInvalid"));

         }
         
         if(!Common.validateDateFromTo(userDateFrom, userDateTo)) {
            errors.add("userDateFrom", 
               new ActionMessage("userResponsibility.error.userDateFromToInvalid"));
               
         }                                            

      }
         
      return errors;

   }
}