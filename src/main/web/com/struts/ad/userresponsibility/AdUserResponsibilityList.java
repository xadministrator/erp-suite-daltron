package com.struts.ad.userresponsibility;

import java.io.Serializable;

public class AdUserResponsibilityList implements Serializable {

   private Integer userResponsibilityCode = null;
   private String responsibility = null;
   private String userDateFrom = null;
   private String userDateTo = null;

   private String editButton = null;
   private String deleteButton = null;
    
   private AdUserResponsibilityForm parentBean;
    
   public AdUserResponsibilityList(AdUserResponsibilityForm parentBean,
      Integer userResponsibilityCode,
      String responsibility,
      String userDateFrom,
      String userDateTo) {

      this.parentBean = parentBean;
      this.userResponsibilityCode = userResponsibilityCode;
      this.responsibility = responsibility;
      this.userDateFrom = userDateFrom;
      this.userDateTo = userDateTo;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   

   public Integer getUserResponsibilityCode() {

      return userResponsibilityCode;

   }
   
   public String getResponsibility() {
       
       return responsibility;
       
   }

   public String getUserDateFrom() {

      return userDateFrom;

   }

   public String getUserDateTo() {

      return userDateTo;

   }
   
}