package com.struts.ad.usersession;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;

public final class AdUserSessionAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("AdUserSessionAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         AdUserSessionForm actionForm = (AdUserSessionForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AD_USER_SESSION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("adUserSession");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

         ActionErrors errors = new ActionErrors();
         
         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad US Logout Action --
*******************************************************/

         } else if (request.getParameter("adUSList[" + 
            actionForm.getRowSelected() + "].logoutButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            AdUserSessionList actionList =
	            actionForm.getAdUSByIndex(actionForm.getRowSelected());

           // remove session from servlet context
          
           HashMap sessions = (HashMap)servlet.getServletContext().getAttribute(Constants.SESSIONS_KEY);
                      
           HttpSession userSession = (HttpSession)sessions.get(actionList.getUsername() + user.getCompany());
           
           if (userSession != null) {
           
           	  userSession.invalidate();
           	
           }
                      
           sessions.remove(actionList.getUsername() + user.getCompany().trim());
           servlet.getServletContext().setAttribute(Constants.SESSIONS_KEY, sessions);
                              	
                                    
/*******************************************************
   -- Ad US Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("adUserSession");

            } 
            
            actionForm.clearAdUSList();            	            
	                        	    	
	    	HashMap sessionsMap = (HashMap)servlet.getServletContext().getAttribute(Constants.SESSIONS_KEY);
	    	
	    	Collection sessions = sessionsMap.values();
	    	
	    	Iterator i = sessions.iterator();
	    	
	    	while (i.hasNext()) {
	    		
	    		HttpSession userSession = (HttpSession)i.next();
	    		
	    		try {
	    				    		
		    		if (userSession != null) {	    		
		    		
			    		User userLogged = (User)userSession.getAttribute(Constants.USER_KEY);

			    		if (userLogged != null) {
			    			
			    			if (userLogged.getCompany().equals(user.getCompany())) {
			    			
				    			AdUserSessionList actionList = new AdUserSessionList(actionForm,
				    				userLogged.getUserName(), userLogged.getUserDescription(),
									userLogged.getLoginTime());
				    			
				    			actionForm.saveAdUSList(actionList);
				    			
			    			}
			    					    		
			    		}	
			    		
		    		}
		    		
	    		} catch (IllegalStateException ex) {
	    			
	    			i.remove();
	    			sessions.remove(userSession);
	    			continue;
	    			
	    		}
	    		
	    	}   	
	    		    		    	    		    		        	                        
            if (request.getParameter("adUSList[" + actionForm.getRowSelected() + "].logoutButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            
            actionForm.reset(mapping, request);
                        
            return(mapping.findForward("adUserSession"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in AdUserSessionAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}