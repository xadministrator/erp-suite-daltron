package com.struts.ad.usersession;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;

public class AdUserSessionForm extends ActionForm implements Serializable {

   private ArrayList adUSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public AdUserSessionList getAdUSByIndex(int index) {

      return((AdUserSessionList)adUSList.get(index));

   }

   public Object[] getAdUSList() {

      return adUSList.toArray();

   }

   public int getAdUSListSize() {

      return adUSList.size();

   }

   public void saveAdUSList(Object newAdUSList) {

      adUSList.add(newAdUSList);

   }

   public void clearAdUSList() {
      adUSList.clear();
   }

   public void setRowSelected(Object selectedAdUSList) {

      this.rowSelected = adUSList.indexOf(selectedAdUSList);

   }

   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   

   public void reset(ActionMapping mapping, HttpServletRequest request) {
  

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
               
      return errors;

   }
}