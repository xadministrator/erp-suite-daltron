package com.struts.ad.usersession;

import java.io.Serializable;

public class AdUserSessionList implements Serializable {

   private String username = null;
   private String description = null;
   private String loginTime = null;
   private String logoutButton = null;
    
   private AdUserSessionForm parentBean;
    
   public AdUserSessionList(AdUserSessionForm parentBean,
      String username,
      String description,
      String loginTime) {

      this.parentBean = parentBean;
      this.username = username;
      this.description = description;
      this.loginTime = loginTime;
   }

   public void setLogoutButton(String logoutButton) {

      parentBean.setRowSelected(this);

   }
   
   public String getUsername() {
       
       return username;
       
   }

   public String getDescription() {

      return description;

   }

   public String getLoginTime() {

      return loginTime;

   }

}