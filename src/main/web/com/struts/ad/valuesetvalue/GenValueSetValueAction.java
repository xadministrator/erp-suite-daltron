package com.struts.ad.valuesetvalue;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GenVSVNoValueSetValueFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalSegmentValueInvalidException;
import com.ejb.txn.GenValueSetValueController;
import com.ejb.txn.GenValueSetValueControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GenModValueSetValueDetails;
import com.util.GenValueSetValueDetails;

public final class GenValueSetValueAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
       HttpServletRequest request, HttpServletResponse response)
	   throws Exception { 
 
       HttpSession session = request.getSession();
       
       try {
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          
          if (user != null) {
          	
             if (log.isInfoEnabled()) {
             	 
                log.info("GenValueSetValueAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             
             }
             
          } else {
          	
             if (log.isInfoEnabled()) {
             	
	             log.info("User is not logged on in session " + session.getId());
	             
             }
             
	      return(mapping.findForward("adLogon"));
	     
          }
          
          GenValueSetValueForm actionForm = (GenValueSetValueForm)form;

          String frParam = Common.getUserPermission(user, Constants.GEN_VALUE_SET_VALUE_ID);
          
          if(frParam != null) {
          	
  	     if(frParam.trim().equals(Constants.FULL_ACCESS)) {
  	     	
	        ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	        
		if(!fieldErrors.isEmpty()) {
			
		   saveErrors(request, new ActionMessages(fieldErrors));
		   
		   return(mapping.findForward("genValueSetValue"));
		   
		}
		
	     }
	     
	     actionForm.setUserPermission(frParam.trim());
	     
	  } else {
	  	
	     actionForm.setUserPermission(Constants.NO_ACCESS);
	  
	  }
	  
/*******************************************************
   Initialize GenValueSetValue EJB
*******************************************************/

          GenValueSetValueControllerHome homeVSV = null;
          GenValueSetValueController ejbVSV = null;
       
          try{
          	
	         homeVSV = (GenValueSetValueControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GenValueSetValueControllerEJB", GenValueSetValueControllerHome.class);
                
          } catch(NamingException e) {
          	
             if(log.isInfoEnabled()) {
             	
		        log.info("NamingException caught in GenValueSetValueAction.execute(): " + e.getMessage() + 
		          " session: " + session.getId()); 
	         
	         } 
	         
             return(mapping.findForward("cmnErrorPage")); 
             
          }
       
          try {
          	
              ejbVSV = homeVSV.create();
              
          } catch(CreateException e) {
          	
             if(log.isInfoEnabled()) {
             	 
                log.info("CreateException caught in GenValueSetValueAction.execute(): " + e.getMessage() + 
	              " session: " + session.getId());
	              
             }
             
             return(mapping.findForward("cmnErrorPage"));
             
          }
	  
          ActionErrors errors = new ActionErrors();
          
/*******************************************************
   -- Gen VSV Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("genValueSetValue"));
	     
/*******************************************************
   -- Gen VSV Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("genValueSetValue"));                   
 
/*******************************************************
   -- Gen VSV Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GenValueSetValueDetails details = new GenValueSetValueDetails();
            details.setVsvValue(actionForm.getValue());
            details.setVsvDescription(actionForm.getDescription());
            details.setVsvLevel(Common.convertStringToShort(actionForm.getLevel()));
            details.setVsvParent(Common.convertBooleanToByte(actionForm.getParent()));
            details.setVsvEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            try {
            	
            	ejbVSV.addGenVsvEntry(details, actionForm.getValueSetName(), 
            	    actionForm.getQualifier(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.recordAlreadyExist"));    
               
            } catch (GlobalRecordInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.recordInvalid")); 	
               
            } catch (GlobalSegmentValueInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.segmentValueInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GenValueSetValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }        
	     
/*******************************************************
   -- Gen VSV Close Action --
*******************************************************/

	  } else if(request.getParameter("closeButton") != null) {
	  	
	     return(mapping.findForward("cmnMain"));


/*******************************************************
   -- Gen VSV Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GenValueSetValueList genVSVList =
	            actionForm.getGenVSVByIndex(actionForm.getRowSelected());

            GenValueSetValueDetails details = new GenValueSetValueDetails();
            details.setVsvCode(genVSVList.getValueSetValueCode());
            details.setVsvValue(actionForm.getValue());
            details.setVsvDescription(actionForm.getDescription());
            details.setVsvLevel(Common.convertStringToShort(actionForm.getLevel()));
            details.setVsvParent(Common.convertBooleanToByte(actionForm.getParent()));
            details.setVsvEnable(Common.convertBooleanToByte(actionForm.getEnable()));

            try {
            	
            	ejbVSV.updateGenVsvEntry(details, actionForm.getValueSetName(), 
            	    actionForm.getQualifier(), user.getCmpCode()); 	
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.recordAlreadyExist"));
               
            } catch (GlobalRecordAlreadyAssignedException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.updateRecordAlreadyAssigned")); 
               
            } catch (GlobalRecordInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.recordInvalid")); 	
               
            } catch (GlobalSegmentValueInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.segmentValueInvalid"));


            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GenValueSetValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  
            
/*******************************************************
   -- Gen VSV Delete Action --
*******************************************************/

         } else if (request.getParameter("genVSVList[" + 
             actionForm.getRowSelected() + "].deleteButton") != null &&
	     actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GenValueSetValueList genVSVList =
	            actionForm.getGenVSVByIndex(actionForm.getRowSelected());

            try {
            	
            	ejbVSV.deleteGenVsvEntry(genVSVList.getValueSetValueCode(), user.getCmpCode());
                               
            } catch (GlobalRecordAlreadyAssignedException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("valueSetValue.error.deleteRecordAlreadyAssigned")); 

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GenValueSetValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 

/*******************************************************
   -- Gen VSV Cancel Action --
*******************************************************/

	  } else if(request.getParameter("cancelButton") != null) {
	  	
	     actionForm.reset(mapping, request);
	     actionForm.setPageState(Constants.PAGE_STATE_SAVE);
	       
	     return(mapping.findForward("genValueSetValue"));

/*******************************************************
   -- Gen VSV Edit Action --
*******************************************************/

	  } else if(request.getParameter("genVSVList[" + 
             actionForm.getRowSelected() + "].editButton") != null &&
	     actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

	     actionForm.showGenVSVRow(actionForm.getRowSelected());
		   return(mapping.findForward("genValueSetValue"));

/*******************************************************
   -- Gen VSV Value Set Name Enter Action --
*******************************************************/

     } else if (request.getParameter("isValueSetNameEntered") != null &&
        request.getParameter("isValueSetNameEntered").equals("true")) {
        	
         try {
         	        	        	
             char SG_SGMNT_TYP = ejbVSV.getGenSgSegmentTypeByVsName(actionForm.getValueSetName(), user.getCmpCode());
             
             actionForm.setSegmentType(Common.convertCharToString(SG_SGMNT_TYP));
             
             actionForm.clearGenVSVList();

             ArrayList list = ejbVSV.getGenVsvByVsName(actionForm.getValueSetName(), user.getCmpCode());
                                       
      		 Iterator i = list.iterator();
        		            		
    		 while (i.hasNext()) {
    			
    			 GenModValueSetValueDetails mdetails = (GenModValueSetValueDetails)i.next();
		         
		         GenValueSetValueList genVSVList = new GenValueSetValueList(actionForm,
					 mdetails.getVsvCode(),
					 mdetails.getVsvValue(),
					 mdetails.getVsvDescription(),
					 Common.convertShortToString(mdetails.getVsvLevel()),
					 Common.convertByteToBoolean(mdetails.getVsvParent()),
					 mdetails.getVsvQlAccountType(),
					 Common.convertByteToBoolean(mdetails.getVsvEnable())); 
					 
		     	 actionForm.saveGenVSVList(genVSVList);
    		    
    		 }

	     } catch (GenVSVNoValueSetValueFoundException ex) {
	        	
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
             new ActionMessage("valueSetValue.error.noValueSetValueFound"));

         } catch (EJBException ex) {

           if (log.isInfoEnabled()) {

              log.info("EJBException caught in GenValueSetValueAction.execute(): " + ex.getMessage() +
              " session: " + session.getId());
              return mapping.findForward("cmnErrorPage"); 
              
           }

        }

        
      }  	
      
/*******************************************************
   -- Gen VSV Load Action --
*******************************************************/
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("genValueSetValue");

            }
            
            actionForm.reset(mapping, request);


            ArrayList list = null;
            Iterator i = null;
            
            try {

                actionForm.clearGenVSVList();
                
            	actionForm.clearValueSetNameList();
            	
            	list = ejbVSV.getGenVsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setValueSetNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setValueSetNameList((String)i.next());
            			
            		}
            		
            	}

            	actionForm.clearQualifierList();
            	
            	list = ejbVSV.getGenQlfrAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setQualifierList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setQualifierList((String)i.next());
            			
            		}
            		
            	}

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GenValueSetValueAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }  
               
            }  
            
            if (actionForm.getValueSetName() != null) {   
            
		         try {
		         	
		             char SG_SGMNT_TYP = ejbVSV.getGenSgSegmentTypeByVsName(actionForm.getValueSetName(), user.getCmpCode());
		             
		             actionForm.setSegmentType(Common.convertCharToString(SG_SGMNT_TYP));	         	
		             
		             actionForm.clearGenVSVList();
		         	
		             list = ejbVSV.getGenVsvByVsName(actionForm.getValueSetName(), user.getCmpCode());
		                         		
		      		 Iterator j = list.iterator();
		        		            		
		    		 while (j.hasNext()) {
		    			
		    			 GenModValueSetValueDetails mdetails = (GenModValueSetValueDetails)j.next();
				         
		    			 System.out.println(mdetails.getVsvDescription() + " = " + mdetails.getVsvQlAccountType());
				         GenValueSetValueList genVSVList = new GenValueSetValueList(actionForm,
							 mdetails.getVsvCode(),
							 mdetails.getVsvValue(),
							 mdetails.getVsvDescription(),
							 Common.convertShortToString(mdetails.getVsvLevel()),
							 Common.convertByteToBoolean(mdetails.getVsvParent()),
							 mdetails.getVsvQlAccountType(),
							 Common.convertByteToBoolean(mdetails.getVsvEnable())); 
							 
				     	 actionForm.saveGenVSVList(genVSVList);
		    		    
		    		 }
		
			     } catch (GenVSVNoValueSetValueFoundException ex) {
			        	
			         errors.add(ActionMessages.GLOBAL_MESSAGE,
		             new ActionMessage("valueSetValue.error.noValueSetValueFound"));
		
		         } catch (EJBException ex) {
		
		           if (log.isInfoEnabled()) {
		
		              log.info("EJBException caught in GenValueSetValueAction.execute(): " + ex.getMessage() +
		              " session: " + session.getId());
		              return mapping.findForward("cmnErrorPage"); 
		              
		           }
		
		        }
			          	
		    }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("genValueSetValue"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GenValueSetValueAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}
