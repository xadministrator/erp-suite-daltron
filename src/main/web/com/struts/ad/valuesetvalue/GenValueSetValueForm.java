package com.struts.ad.valuesetvalue;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GenValueSetValueForm extends ActionForm implements Serializable {

    private Integer valueSetValueCode = null;
	private String valueSetName = null;
	private ArrayList valueSetNameList = new ArrayList();
	
	private String value = null;
	private String description = null;
	private String level = null;
	private boolean parent = false;	
	private String qualifier = null;
    private ArrayList qualifierList = new ArrayList();
	private boolean enable = false;
	private boolean isValueSetNameEntered = false;
	private String segmentType = null;
    private String tableType = null;

    private String showDetailsButton = null;
    private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList genVSVList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
		
	   return rowSelected;
	
	}
	
    public GenValueSetValueList getGenVSVByIndex(int index) {
    	
	   return((GenValueSetValueList)genVSVList.get(index));
	   
	}
	
	public Object[] getGenVSVList() {
		
	   return(genVSVList.toArray());
	
	}

	public int getGenVSVListSize() {
	
	   return(genVSVList.size());
	
	}

	public void saveGenVSVList(Object newGenVSVList) { 
	
	   genVSVList.add(newGenVSVList);
	
	}

	public void clearGenVSVList() { 
	
	   genVSVList.clear();
	
	}
	
	public void setRowSelected(Object selectedGenVSVList, boolean isEdit) {
		
	   this.rowSelected = genVSVList.indexOf(selectedGenVSVList);
	   
	   if(isEdit) {
	   	
	      this.pageState = Constants.PAGE_STATE_EDIT;
	   
	   }
	
	}
	
	public void showGenVSVRow(int rowSelected) {
		 		
	   this.value = ((GenValueSetValueList)genVSVList.get(rowSelected)).getValue();
	   this.description = ((GenValueSetValueList)genVSVList.get(rowSelected)).getDescription();
	   this.level = ((GenValueSetValueList)genVSVList.get(rowSelected)).getLevel();
	   this.parent = ((GenValueSetValueList)genVSVList.get(rowSelected)).getParent();
	   this.qualifier = ((GenValueSetValueList)genVSVList.get(rowSelected)).getQualifier();
	   this.enable = ((GenValueSetValueList)genVSVList.get(rowSelected)).getEnable();
	   
	}

	public void updateGenVSVRow(int rowSelected, Object newGenVSVList) {
		
	   genVSVList.set(rowSelected, newGenVSVList);
	
	}

	public void deleteGenVSVList(int rowSelected) {
		
	   genVSVList.remove(rowSelected);
	
	}
	
	public void setUpdateButton(String updateButton) {
		
	   this.updateButton = updateButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
		
	   this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
		
	   this.saveButton = saveButton;
	
	}

	public void setCloseButton(String closeButton) {
		
	   this.closeButton = closeButton;
	
	}
	
    public void setShowDetailsButton(String showDetailsButton) {
		
	   this.showDetailsButton = showDetailsButton;
		
    }
    
    public void setHideDetailsButton(String hideDetailsButton) {
    	
       this.hideDetailsButton = hideDetailsButton;
    	
    }	

	public void setPageState(String pageState) {
		
	   this.pageState = pageState;
	
	}

	public String getPageState() {
		
	    return(pageState);
	
	}

	public String getTxnStatus() {
		
	   String passTxnStatus = txnStatus;
	   txnStatus = Constants.GLOBAL_BLANK;
	   return(passTxnStatus);
	
	}

	public void setTxnStatus(String txnStatus) {
		
	   this.txnStatus = txnStatus;
	
	}

    public String getUserPermission() {
    	
	   return(userPermission);
	
	}

	public void setUserPermission(String userPermission) {
		
	   this.userPermission = userPermission;
	
	}

	public Integer getValueSetValueCode() {
		
	   return valueSetValueCode;
	
	}

	public void setValueSetValueCode(Integer valueSetValueCode) {
		
	   this.valueSetValueCode = valueSetValueCode;
	
	}

	public String getValueSetName() {
		
	   return valueSetName;
	
	}

	public void setValueSetName(String valueSetName) {
		
	   this.valueSetName = valueSetName;
	
	}

	public ArrayList getValueSetNameList() {
		
	   return valueSetNameList;
	
	}

	public void setValueSetNameList(String valueSetName) {
		
	   valueSetNameList.add(valueSetName);
	
	}

	public void clearValueSetNameList() {
		
	   valueSetNameList.clear();
	   valueSetNameList.add(Constants.GLOBAL_BLANK);

	}

	public String getValue() {
		
	   return value;
	
	}

	public void setValue(String value) { 
	
	   this.value = value;
	
	}

	public String getDescription() {
		
	   return description;
	
	}

	public void setDescription(String description) { 
	
	   this.description = description;
	
	}

	public String getLevel() {
		
	   return level;
	
	}

	public void setLevel(String level) { 
	
	   this.level = level;
	
	}

	public boolean getParent() {
		
	   return parent;
	
	}

	public void setParent(boolean parent) { 
	
	   this.parent = parent;
	
	}

	public String getQualifier() {
		
	   return qualifier;
	
	}

	public void setQualifier(String qualifier) { 
	
	   this.qualifier = qualifier;
	
	}

	public ArrayList getQualifierList() {
		
	   return qualifierList;
	
	}

	public void setQualifierList(String qualifier) {
		
	   qualifierList.add(qualifier);
	
	}

	public void clearQualifierList() {
		
	   qualifierList.clear();
	   qualifierList.add(Constants.GLOBAL_BLANK);

	}
	
	public boolean getEnable() {
		
	   return enable;
	
	}

	public void setEnable(boolean enable) {
		
	   this.enable = enable;
	
	}
	
   public boolean getIsValueSetNameEntered() {
   	
   	   return isValueSetNameEntered;
   	
   }
   
   public void setIsValueSetNameEntered(boolean isValueSetNameEntered) {
   	
   	   this.isValueSetNameEntered = isValueSetNameEntered;
   	
   }
   
	public String getSegmentType() {
		
	   return segmentType;
	
	}

	public void setSegmentType(String segmentType) { 
	
	   this.segmentType = segmentType;
	
	}   
	
    public String getTableType() {
    	
       return(tableType);
    	
    }
    
    public void setTableType(String tableType) {
    	
       this.tableType = tableType;
    	
    }      		

    public void reset(ActionMapping mapping, HttpServletRequest request) {
       
	   value = null;
	   description = null;
	   level = null;
	   parent = false;
	   enable = false;	
	   saveButton = null;
	   closeButton = null;
	   updateButton = null;
	   cancelButton = null;
	   showDetailsButton = null;
	   hideDetailsButton = null;
	   qualifier = null;
	
	}
	   
    public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
       	
      ActionErrors errors = new ActionErrors();

	   if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

			if (Common.validateRequired(valueSetName)) {
			
			   errors.add("valueSetName",
			   new ActionMessage("valueSetValue.error.valueSetNameRequired"));
			
			} 

			if (Common.validateRequired(value)) {
			
			   errors.add("value",
			   new ActionMessage("valueSetValue.error.valueRequired"));
			
			}         

			if (Common.validateRequired(qualifier) && segmentType.equals(Constants.GEN_SEGMENT_TYPE_N)) {
			
			   errors.add("qualifier",
			   new ActionMessage("valueSetValue.error.qualifierRequired"));
			
			} else {
				
		    }   
	     
      }
      
	  return(errors);
	}
		
}
