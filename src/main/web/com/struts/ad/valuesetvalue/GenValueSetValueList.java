package com.struts.ad.valuesetvalue;

import java.io.Serializable;

public class GenValueSetValueList implements Serializable{

    private Integer valueSetValueCode = null;
	private String value = null;
	private String description = null;
	private String level = null;
	private boolean parent = false;
	private String qualifier = null;
	private boolean enable = false; 
	
	private String editButton = null;
	private String deleteButton = null;
	
	private GenValueSetValueForm parentBean;

	public GenValueSetValueList(GenValueSetValueForm parentBean, 
	   Integer valueSetValueCode,
	   String value, 
	   String description,
	   String level,
	   boolean parent,
	   String qualifier,
	   boolean enable){
	   
	   this.parentBean = parentBean;
	   this.valueSetValueCode = valueSetValueCode;
	   this.value = value;
	   this.description = description;
	   this.level = level;
	   this.parent = parent;
	   this.qualifier = qualifier;
	   this.enable = enable;

	}

	public void setParentBean(GenValueSetValueForm parentBean) {
		
	   this.parentBean = parentBean;
	   
	}

	public void setEditButton(String editButton) {
		
	   parentBean.setRowSelected(this, true);
	   
	}
	
	public void setDeleteButton(String deleteButton) {
		
	   parentBean.setRowSelected(this, false);
	   
	}

	public Integer getValueSetValueCode() {
		
	   return valueSetValueCode;
	   
	}

	public String getValue() {
		
	   return value;
	
	}

	public String getDescription() {
		
	   return description;
	   
	}
	
	public String getLevel() {
		
	   return level;
	   
    }
    
    public boolean getParent() {
    	
       return parent;
       
    }
    
    public String getQualifier() {
    	
       return qualifier;
       
    }
    
    public boolean getEnable() {
    	
       return enable;
       
    } 
	
}
