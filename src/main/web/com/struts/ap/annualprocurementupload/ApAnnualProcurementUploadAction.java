package com.struts.ap.annualprocurementupload;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.txn.ApAnnualProcurementUploadController;
import com.ejb.txn.ApAnnualProcurementUploadControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
//import com.util.ApModVoucherDetails;
//import com.util.ApModVoucherLineItemDetails;
import com.util.ApModAnnualProcurementDetails;
import com.util.InvModTransactionalBudgetDetails;

public final class ApAnnualProcurementUploadAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
        
        
        try {
        	
/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApAnnualProcurementUploadAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }

        } else {

            if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ApAnnualProcurementUploadForm actionForm = (ApAnnualProcurementUploadForm)form;
        
        String frParam = Common.getUserPermission(user, Constants.AP_ANNUAL_PROCUREMENT_UPLOAD);

        if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

		        ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	            if (!fieldErrors.isEmpty()) {
	
	                saveErrors(request, new ActionMessages(fieldErrors));
	
	                return mapping.findForward("apAnnualProcurementUpload");
	                
	            }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ApVoucherImportController EJB
*******************************************************/

        ApAnnualProcurementUploadControllerHome homeAPU = null;
        ApAnnualProcurementUploadController ejbAPU = null;

        try {

        	homeAPU = (ApAnnualProcurementUploadControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApAnnualProcurementUploadControllerEJB", ApAnnualProcurementUploadControllerHome.class);
             
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApAnnualProcurementUploadAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

        	ejbAPU = homeAPU.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApAnnualProcurementUploadAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
/*******************************************************
    Call ApVoucherImportController EJB
    getGlFcPrecisionUnit
    getInvGpQuantityPrecisionUnit
*******************************************************/

        short precisionUnit = 0;
        short quantityPrecisionUnit = 0;
        
        try {
        	
        	precisionUnit = ejbAPU.getGlFcPrecisionUnit(user.getCmpCode());
        	quantityPrecisionUnit = ejbAPU.getInvGpQuantityPrecisionUnit(user.getCmpCode());
        	precisionUnit = (short)(10);
        	quantityPrecisionUnit = (short)(10);
        	
        } catch(EJBException ex) {
        	
        	if (log.isInfoEnabled()) {
        		
        		log.info("EJBException caught in ApAnnualProcurementUploadAction.execute(): " + ex.getMessage() +
        				" session: " + session.getId());
        	}
        	
        	return(mapping.findForward("cmnErrorPage"));
        	
        }
                	
/*******************************************************
   -- Ap VI Import Action --
*******************************************************/
        	
          if (request.getParameter("importButton") != null &&  
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {            
            
            //ArrayList headerList = new ArrayList();
        	ArrayList detailsList = new ArrayList();
        	
        	//CSVParser csvParser1 = new CSVParser(actionForm.getHeaderFile().getInputStream());
        	CSVParser csvParser2 = new CSVParser(actionForm.getDetailsFile().getInputStream());
        	
        	//String[][] header = csvParser1.getAllValues();
        	String[][] details = csvParser2.getAllValues();
        	
        	LinkedHashSet set = new LinkedHashSet();

    		
        	for (int i=0; i <= details.length-1; i++) {
        		
        		//ApModAnnualProcurementDetails mDetails = new ApModAnnualProcurementDetails();
        		InvModTransactionalBudgetDetails mdetails = new InvModTransactionalBudgetDetails();
        		System.out.println(details[i][0].trim() + "<== details [i][0] item name ");
        		//System.out.println(details[i][1].trim() + "<== details [i][1] item desc ");
        		System.out.println(details[i][2].trim() + "<== details [i][2] uom ");
        		System.out.println(details[i][3].trim() + "<== details [i][3] jan ");
        		System.out.println(details[i][4].trim() + "<== details [i][4] feb ");
        		System.out.println(details[i][5].trim() + "<== details [i][5] march ");
        		System.out.println(details[i][6].trim() + "<== details [i][6] aprl ");
        		System.out.println(details[i][7].trim() + "<== details [i][7] may ");
        		System.out.println(details[i][8].trim() + "<== details [i][8] jun ");
        		System.out.println(details[i][9].trim() + "<== details [i][9] jul ");
        		System.out.println(details[i][10].trim() + "<== details [i][10] aug? ");
        		System.out.println(details[i][11].trim() + "<== details [i][11] sep? ");
        		System.out.println(details[i][12].trim() + "<== details [i][12] oct 2? ");
        		System.out.println(details[i][13].trim() + "<== details [i][13] nov 2? ");
        		System.out.println(details[i][14].trim() + "<== details [i][14] dec ");
        		//System.out.println(details[i][15].trim() + "<== details [i][15] yearly? ");
        		System.out.println(details[i][16].trim() + "<== details [i][16] unitcost? ");
        		System.out.println(details[i][17].trim() + "<== details [i][17] totalcost? ");
        		//mdetails.setTbCode(null);
        		mdetails.setTbItemName(details[i][0].trim());
        		mdetails.setTbDesc(details[i][1].trim());
        		mdetails.setTbUnit(details[i][2].trim());
        		mdetails.setTbQtyJan(Common.convertStringMoneyToDouble(details[i][3].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyFeb(Common.convertStringMoneyToDouble(details[i][4].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyMrch(Common.convertStringMoneyToDouble(details[i][5].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyAprl(Common.convertStringMoneyToDouble(details[i][6].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyMay(Common.convertStringMoneyToDouble(details[i][7].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyJun(Common.convertStringMoneyToDouble(details[i][8].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyJul(Common.convertStringMoneyToDouble(details[i][9].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyAug(Common.convertStringMoneyToDouble(details[i][10].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtySep(Common.convertStringMoneyToDouble(details[i][11].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyOct(Common.convertStringMoneyToDouble(details[i][12].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyNov(Common.convertStringMoneyToDouble(details[i][13].trim(), quantityPrecisionUnit));
        		mdetails.setTbQtyDec(Common.convertStringMoneyToDouble(details[i][14].trim(), quantityPrecisionUnit));
        		
        		mdetails.setTbMonth(details[i][15].trim());
        		
        		mdetails.setTbUnitCost(Common.convertStringMoneyToDouble(details[i][16].trim(), quantityPrecisionUnit));
        		mdetails.setTbTotalCost(Common.convertStringMoneyToDouble(details[i][17].trim(), quantityPrecisionUnit));
        		mdetails.setTbYear(Common.convertStringToInteger(actionForm.getYear()));
        		
        		System.out.println(mdetails.getTbItemName() + "<== mdetails  item name ");
        		System.out.println(mdetails.getTbDesc() + "<== mdetails item desc ");
        		System.out.println(mdetails.getTbUnit() + "<== mdetails  uom");
        		System.out.println(mdetails.getTbQtyJan() + "<== mdetails  jan ");
        		System.out.println(mdetails.getTbQtyFeb() + "<== mdetails feb ");
        		System.out.println(mdetails.getTbQtyMrch() + "<== mdetails  mrch ");
        		System.out.println(mdetails.getTbQtyAprl() + "<== mdetails  aprl ");
        		System.out.println(mdetails.getTbQtyMay() + "<== mdetails  may ");
        		System.out.println(mdetails.getTbQtyJun() + "<== mdetails  jun");
        		System.out.println(mdetails.getTbQtyJul() + "<== mdetails  jul ");
        		System.out.println(mdetails.getTbQtyAug() + "<== mdetails  aug");
        		System.out.println(mdetails.getTbQtySep() + "<== mdetails  sep ");
        		System.out.println(mdetails.getTbQtyOct() + "<== mdetails  oct");
        		System.out.println(mdetails.getTbQtyNov() + "<== mdetails  nov ");
        		System.out.println(mdetails.getTbQtyDec() + "<== mdetails  dec ");
        		
        		System.out.println(mdetails.getTbMonth() + "<== mdetails dummy? ");
        		System.out.println(mdetails.getTbUnitCost() + "<== mdetails unit cost");
        		System.out.println(mdetails.getTbTotalCost() + "<== mdetails total cost ");
        		detailsList.add(mdetails);
        	}
        	System.out.println(detailsList + "<== detailsList");

    		
    		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
    		String VB_NM = "IMPORT " + formatter.format(new java.util.Date());
    		
    		if (/*headerList.isEmpty() || */detailsList.isEmpty()) {
    			
    			errors.add(ActionMessages.GLOBAL_MESSAGE,
    					new ActionMessage("annualProcurementUpload.error.noAnnualProcurementPlanToUpload"));
    			
    		} else {
    			
    			try {
    				
    				ejbAPU.uploadAnnualProcurement(detailsList, actionForm.getUserDepartment(),
    						new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    				
    			} catch (GlobalRecordInvalidException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("annualProcurementUpload.error.recordInvalid", ex.getMessage()));
    				
    			} catch (EJBException ex) {
    				if (log.isInfoEnabled()) {
    					
    					log.info("EJBException caught in ApAnnualProcurementUploadAction.execute(): " + ex.getMessage() +
    							" session: " + session.getId());
    				}
    				
    				return(mapping.findForward("cmnErrorPage"));
    			}
    			
    		}
    		//csvParser1.close();
        	csvParser2.close();
          
/*******************************************************
   -- Ap VI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap VI Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {
            	
            	if ((request.getParameter("importButton") != null) && 
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            		
            		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            		
            	}
            	
            }
            
            ArrayList list = null;
            Iterator i = null;
            String userDepartment = ejbAPU.getAdUsrDeptartment(user.getUserName(), user.getCmpCode());   
            
            actionForm.reset(mapping, request);
            /*
            list = ejbAPU.getAdLvDEPARTMENT(user.getCmpCode());
            i = list.iterator();

            if (list == null || list.size() == 0) {  		
              	 actionForm.setUserDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);
            } else {     		            		
              	 i = list.iterator();
              	 while (i.hasNext()) {
   					    actionForm.setUserDepartmentList(userDepartment);
              	 }
            }*/
            actionForm.setUserDepartmentList(userDepartment);
            return(mapping.findForward("apAnnualProcurementUpload"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	
      	  e.printStackTrace();

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApAnnualProcurementUploadAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
    }