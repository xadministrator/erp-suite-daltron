package com.struts.ap.annualprocurementupload;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApAnnualProcurementUploadForm extends ActionForm implements Serializable {
	
	private FormFile headerFile = null;
	private FormFile detailsFile = null;
	private String importButton = null;
	private String closeButton = null;
	private String year = null;
	private String userDepartment = null;
	private ArrayList userDepartmentList = new ArrayList();
	
	private String pageState = new String();
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public void setImportButton(String importButton) {
		
		this.importButton = importButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	public String getUserDepartment() {
		return userDepartment;
	}
		
	public void setUserDepartment(String userDepartment) {
		this.userDepartment = userDepartment;
	}
	   
	public ArrayList getUserDepartmentList() {
		return userDepartmentList;
	}
	   
	public void setUserDepartmentList(String userDepartmentList) {
			
		this.userDepartmentList.add(userDepartmentList);
			
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getYear() {
		
		return year;
		
	}
	
	public void setYear(String year) {
		
		this.year = year;
		
	}
	
	public FormFile getHeaderFile() {
		
		return headerFile;
		
	}
	
	public void setHeaderFile(FormFile headerFile) {
		
		this.headerFile = headerFile;
		
	}
	
	public FormFile getDetailsFile() {
		
		return detailsFile;
		
	}
	
	public void setDetailsFile(FormFile detailsFile) {
		
		this.detailsFile = detailsFile;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		year = null;
		headerFile = null;
		detailsFile = null;
		importButton = null;
		closeButton = null;		
		userDepartment = Constants.GLOBAL_BLANK;
		userDepartmentList.clear();
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("importButton") != null) {
			/*
			if(Common.validateRequired(headerFile.getFileName())){
				errors.add("headerFile",
						new ActionMessage("annualProcurementUpload.error.headerFileRequired"));
			}
			*/
			if(Common.validateRequired(detailsFile.getFileName())){
				errors.add("detailsFile",
						new ActionMessage("annualProcurementUpload.error.detailsFileRequired"));
			}
			if(Common.validateRequired(year)){
				errors.add("year",
						new ActionMessage("annualProcurementUpload.error.yearRequired"));
			}
			if(Common.validateRequired(userDepartment)){
				errors.add("userDepartment",new ActionMessage("annualProcurementUpload.error.departmentRequired"));
			}
			/*
			if(!headerFile.getFileName().substring(headerFile.getFileName().indexOf(".") + 1, headerFile.getFileName().length()).equalsIgnoreCase("CSV")){
				errors.add("headerFile",
						new ActionMessage("annualProcurementUpload.error.filenameInvalid"));
			}
			*/
			if(!detailsFile.getFileName().substring(detailsFile.getFileName().indexOf(".") + 1, detailsFile.getFileName().length()).equalsIgnoreCase("CSV")){
				errors.add("detailsFile",
						new ActionMessage("annualProcurementUpload.error.filenameInvalid"));
			}
			
		}
		
		return errors;
		
	}
}