package com.struts.ap.approval;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.txn.ApApprovalController;
import com.ejb.txn.ApApprovalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModApprovalQueueDetails;

public final class ApApprovalAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApApprovalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApApprovalForm actionForm = (ApApprovalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_APPROVAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apApproval");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApApprovalController EJB
*******************************************************/

         ApApprovalControllerHome homeAPR = null;
         ApApprovalController ejbAPR = null;

         try {
          
            homeAPR = (ApApprovalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApApprovalControllerEJB", ApApprovalControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApApprovalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAPR = homeAPR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApApprovalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbAPR.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApApprovalAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ap APR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apApproval"));
	     
/*******************************************************
   -- Ap APR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apApproval"));         

/*******************************************************
   -- Ap APR Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ap APR Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Ap APR Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
         		request.getParameter("previousButton") != null) {
         	
         	if (request.getParameter("goButton") != null) {
         		
         		HashMap criteria = new HashMap();
         		
         		if (!Common.validateRequired(actionForm.getDocument())) {
         			
         			criteria.put("document", actionForm.getDocument());
         		}	        		        	
         		
         		if (!Common.validateRequired(actionForm.getDateFrom())) {
         			
         			criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
         			
         		}
         		
         		if (!Common.validateRequired(actionForm.getDateTo())) {
         			
         			criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
         			
         		}
         		
         		if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
         			
         			criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
         			
         		}
         		
         		if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
         			
         			criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
         			
         		}
         		
         		// save criteria
         		
         		actionForm.setLineCount(0);
         		actionForm.setCriteria(criteria);
         		
         		// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbAPR.getAdAqByAqDocumentAndUserName(actionForm.getCriteria(), user.getUserName(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ApApprovalAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
         		
         	}
         	
         	try {
         		
         		actionForm.clearApAPRList();
         		
         		ArrayList list = ejbAPR.getAdAqByAqDocumentAndUserName(actionForm.getCriteria(), user.getUserName(),
         				new Integer(actionForm.getLineCount()), 
         				new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         		
         		// check if prev should be disabled
         		if (actionForm.getLineCount() == 0) {
         			
         			actionForm.setDisablePreviousButton(true);
         			
         		} else {
         			
         			actionForm.setDisablePreviousButton(false);
         			
         		}
         		
         		// check if next should be disabled
         		if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
         			
         			actionForm.setDisableNextButton(true);
         			
         		} else {
         			
         			actionForm.setDisableNextButton(false);
         			
         			//remove last record
         			list.remove(list.size() - 1);
         			
         		}
         		
         		Iterator i = list.iterator();
         		
         		while (i.hasNext()) {
         			
         			AdModApprovalQueueDetails mdetails = (AdModApprovalQueueDetails)i.next();
         			
         			ApApprovalList apAPRList = new ApApprovalList(actionForm,
         					mdetails.getAqCode(),
         					mdetails.getAqDocumentCode(),
							mdetails.getAqDocument(),
							mdetails.getAqDepartment(),
							Common.convertSQLDateToString(mdetails.getAqDate()),
							mdetails.getAqSupplierCode(),
							mdetails.getAqSupplierName(),
							mdetails.getAqDocumentNumber(),                        
							Common.convertDoubleToStringMoney(mdetails.getAqAmount(), precisionUnit),
							mdetails.getAqDocumentType(),
							mdetails.getAqType(),
							mdetails.getAqReferenceNumber(),
							mdetails.getAqBrCode());
         			
         			actionForm.saveApAPRList(apAPRList);
         			
         		}
         		
         	} catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("apApproval.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApApprovalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apApproval");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("apApproval")); 

/*******************************************************
   -- Ap APR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap APR Approve Reject Action --
*******************************************************/

         } else if (request.getParameter("approveRejectButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get posted journals
        	 ArrayList aqList = new ArrayList();
                        
		    for(int i=0; i<actionForm.getApAPRListSize(); i++) {
		    
		       ApApprovalList actionList = actionForm.getApAPRByIndex(i);
		       
		       if (Common.validateRequired(actionList.getReasonForRejection()) && actionList.getReject()) {
		    	   errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("apApproval.error.reasonForRejectionRequired"));
	               saveErrors(request, new ActionMessages(errors));
	               actionList.setReject(false);
	               return mapping.findForward("apApproval");
		       }
		       
               if (actionList.getApprove() || actionList.getReject()) {
            	   
               	     try {
               	     	
               	     	ejbAPR.executeApApproval(actionList.getDocument(), 
               	     	    actionList.getDocumentCode(), user.getUserName(),
               	     	    actionList.getApprove(), actionList.getReasonForRejection(), actionList.getBrCode(), user.getCmpCode());
               	     	
               	     	aqList.add(actionList.getApprovalCode());
               	     	actionForm.deleteApAPRList(i);
               	     	i--;
               	  		                  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("apApproval.error.recordAlreadyDeleted", actionList.getDocumentNumber()));
		               
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("apApproval.error.transactionAlreadyPosted", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalTransactionAlreadyVoidException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("apApproval.error.transactionAlreadyVoid", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalTransactionAlreadyVoidPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("apApproval.error.transactionAlreadyVoidPosted", actionList.getDocumentNumber()));
		             
		             } catch (GlJREffectiveDateNoPeriodExistException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("apApproval.error.effectiveDateNoPeriodExist", actionList.getDocumentNumber()));
		                  
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("apApproval.error.effectiveDatePeriodClosed", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalJournalNotBalanceException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("apApproval.error.journalNotBalance", actionList.getDocumentNumber()));    
		             		
		             } catch (GlobalInventoryDateException ex) {
		              	
			      	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			      			new ActionMessage("apApproval.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		            	
		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("apApproval.error.noNegativeInventoryCostingCOA"));

		             } catch (EJBException ex) {
		             	
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ApApprovalAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            }
               	     
	            
	           }
	            
	        }	
	        
	        
		   if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apApproval");

           }
	        
	        try {
	        	
         		actionForm.setLineCount(0);
            	actionForm.clearApAPRList();
            	                        
            	ArrayList list = ejbAPR.getAdAqByAqDocumentAndUserName(actionForm.getCriteria(), user.getUserName(),
         				new Integer(actionForm.getLineCount()), 
         				new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		AdModApprovalQueueDetails mdetails = (AdModApprovalQueueDetails)i.next();
            		
            		ApApprovalList apAPRList = new ApApprovalList(actionForm,
            				mdetails.getAqCode(),
            		    mdetails.getAqDocumentCode(),
            		    mdetails.getAqDocument(),
            		    mdetails.getAqDepartment(),
            		    Common.convertSQLDateToString(mdetails.getAqDate()),
                        mdetails.getAqSupplierCode(),
                        mdetails.getAqSupplierName(),
                        mdetails.getAqDocumentNumber(),                        
                        Common.convertDoubleToStringMoney(mdetails.getAqAmount(), precisionUnit),
                        mdetails.getAqDocumentType(),
						mdetails.getAqType(),
						mdetails.getAqReferenceNumber(),
						mdetails.getAqBrCode());
            		    
            		actionForm.saveApAPRList(apAPRList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApApprovalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
	        
	        if (!errors.isEmpty()) {
            	saveErrors(request, new ActionMessages(errors));
            } else {
            	
            	if (request.getParameter("approveRejectButton") != null && 
            			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            		
            		
            		Iterator x = aqList.iterator();
            		
            		while (x.hasNext()) {
            			Integer aqCode = (Integer)x.next();
            			
            			System.out.println("--------------->aqCode="+aqCode);
            			
            			try {
            				ArrayList list = ejbAPR.getAdApprovalNotifiedUsersByAqCode(aqCode, user.getCmpCode());
            				System.out.println("list.isEmpty()="+list.isEmpty());
            				System.out.println("list="+list);
                			if (list.isEmpty()) {
                				messages.add(ActionMessages.GLOBAL_MESSAGE,
                						new ActionMessage("messages.documentSentForPosting"));
                			} else if (list.contains("DOCUMENT POSTED")) {
                				messages.add(ActionMessages.GLOBAL_MESSAGE,	
                						new ActionMessage("messages.documentPosted"));
                			} else if (list.contains("DOCUMENT REJECTED")) {	
                				messages.add(ActionMessages.GLOBAL_MESSAGE,	
                						new ActionMessage("messages.documentRejected"));
                				
                			} else {
                				Iterator i = list.iterator();
                				String APPROVAL_USERS = "";
                				while (i.hasNext()) {
                					APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
                					if (i.hasNext()) {	
                						APPROVAL_USERS = APPROVAL_USERS + ", ";	
                					}
                				}
                				messages.add(ActionMessages.GLOBAL_MESSAGE,
                						new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
                			}	
                		
                		} catch(EJBException ex) {
                			
                		}
                		
                		saveMessages(request, messages);
                		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                		
            			
            		}

            		
            	}
            	
            }
	        
	        
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("apApproval")); 
	        	     

/*******************************************************
   -- Ap APR Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apApproval");

            }
            
            try {
            
            	actionForm.clearApAPRList();
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApApprovalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }                    
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("apApproval"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApApprovalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}