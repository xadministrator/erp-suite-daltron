package com.struts.ap.approval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApApprovalForm extends ActionForm implements Serializable {

   private String document = null;
   private ArrayList documentList = new ArrayList();
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	   
   private String pageState = new String();
   private ArrayList apAPRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;

   private int lineCount = 0;
   private HashMap criteria = new HashMap();
   
   private String maxRows = null;
   private String queryCount = null;
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }  

   public ApApprovalList getApAPRByIndex(int index) {

      return((ApApprovalList)apAPRList.get(index));

   }

   public Object[] getApAPRList() {

      return apAPRList.toArray();

   }

   public int getApAPRListSize() {

      return apAPRList.size();

   }

   public void saveApAPRList(Object newApAPRList) {

      apAPRList.add(newApAPRList);

   }

   public void clearApAPRList() {
      apAPRList.clear();
   }

   public void setRowSelected(Object selectedApAPRList, boolean isEdit) {

      this.rowSelected = apAPRList.indexOf(selectedApAPRList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showApAPRRow(int rowSelected) {

   }

   public void updateApAPRRow(int rowSelected, Object newApAPRList) {

      apAPRList.set(rowSelected, newApAPRList);

   }

   public void deleteApAPRList(int rowSelected) {

      apAPRList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getDocument() {

      return document;

   }

   public void setDocument(String document) {

      this.document = document;

   }

   public ArrayList getDocumentList() {

      return documentList;

   }
   
   public String getDocumentNumberFrom() {
   	
   	  return documentNumberFrom;
   	
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom) {
   	
   	  this.documentNumberFrom = documentNumberFrom;
   	
   }
   
   public String getDocumentNumberTo() {
   	
   	  return documentNumberTo;
   	
   }
   
   public void setDocumentNumberTo(String documentNumberTo) {
   	
   	  this.documentNumberTo = documentNumberTo;
   	
   }
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   	
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	
   }
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
      
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	
   	documentNumberFrom = null;
   	documentNumberTo = null;
   	dateFrom = null;
   	dateTo = null;

   	if (documentList.isEmpty()) {
   		
   		documentList.clear();
   		
   		documentList.add("AP VOUCHER");
   		documentList.add("AP DEBIT MEMO");
   		documentList.add("AP CHECK PAYMENT REQUEST");
   		documentList.add("AP CHECK");
   		documentList.add("AP PURCHASE ORDER");
   		documentList.add("AP RECEIVING ITEM");
   		documentList.add("AP PURCHASE REQUISITION");
   		documentList.add("AP CANVASS");
   		
   		document = "AP VOUCHER";
   		
   	}
   	
   	if (orderByList.isEmpty()) {
   		
   		orderByList.clear();
   		orderByList.add(Constants.GLOBAL_BLANK);
   		orderByList.add("DATE");
   		orderByList.add("DOCUMENT NUMBER");
   		orderBy = "DOCUMENT NUMBER";
   		
   	}
   	
   	for (int i=0; i<apAPRList.size(); i++) {
   		
   		ApApprovalList actionList = (ApApprovalList)apAPRList.get(i);          
   		actionList.setApprove(false);
   		actionList.setReject(false);
   		
   	}
   	
   	previousButton = null;
   	nextButton = null;
   	showDetailsButton = null;
   	hideDetailsButton = null;
   	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
      		request.getParameter("previousButton") != null) {   
      	
      	if (Common.validateRequired(maxRows)) {
      		
      		errors.add("maxRows",
      				new ActionMessage("apApproval.error.maxRowsRequired"));
      		
      	}
      	
      	if (!Common.validateNumberFormat(maxRows)) {
      		
      		errors.add("maxRows",
      				new ActionMessage("apApproval.error.maxRowsInvalid"));
      		
      	}
      	
      	if (!Common.validateDateFormat(dateFrom)) {
      		
      		errors.add("dateFrom",
      				new ActionMessage("apApproval.error.dateFromInvalid"));
      		
      	}         
      	
      	if (!Common.validateDateFormat(dateTo)) {
      		
      		errors.add("dateTo",
      				new ActionMessage("apApproval.error.dateToInvalid"));
      		
      	}
      	
      }
            
      return errors;

   }
   
}