package com.struts.ap.approval;

import java.io.Serializable;

public class ApApprovalList implements Serializable {
	
	private Integer approvalCode = null;
   private Integer documentCode = null;
   private String document = null;   
   private String department = null;
   private String date = null;
   private String supplierCode = null;   
   private String documentNumber = null;
   private String amount = null;
   private String documentType = null;
   private String reasonForRejection = null;
   private String type = null;
   private String supplierName = null;
   private String referenceNumber = null;
   
   private boolean approve = false;
   private boolean reject = false;
   private Integer brCode = null;
       
   private ApApprovalForm parentBean;
    
   public ApApprovalList(ApApprovalForm parentBean,
	  Integer approvalCode,
      Integer documentCode,
      String document,
      String department,
      String date,
      String supplierCode,
      String supplierName,
      String documentNumber,
      String amount,
      String documentType,
	  String type,
	  String referenceNumber,
	  Integer brCode) {

      this.parentBean = parentBean;
      this.approvalCode = approvalCode;
      this.documentCode = documentCode;
      this.document = document;
      this.department = department;
      this.date = date;
      this.supplierCode = supplierCode;
      this.supplierName = supplierName;
      this.documentNumber = documentNumber;
      this.amount = amount;
      this.documentType = documentType;
      this.type = type;
      this.referenceNumber = referenceNumber;
      this.brCode = brCode;
      
   }

   public Integer getApprovalCode() {

      return approvalCode;

   }
   
   public Integer getDocumentCode() {

      return documentCode;

   }
   
   public String getDocument() {
   	
   	  return document;
   	
   }
   
   public String getDepartment() {
	   
      return department;
      
   }
   
   public String getDate() {
   
      return date;
      
   }

   public String getSupplierCode() {

      return supplierCode;
      
   }
      
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getAmount() {
   
      return amount;
   
   }
   
   public String getDocumentType() {
   	
   	  return documentType;
   	
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }

   public boolean getApprove() {
   	
   	  return approve;
   	
   }
   
   public void setApprove(boolean approve) {
   	
   	  this.approve = approve;
   	
   }
   
   public boolean getReject() {
   	
   	  return reject;
   	
   }
   
   public void setReject(boolean reject) {
   	
   	  this.reject = reject;
   	  
   }
   
   public String getReasonForRejection() {
   	
   	  return reasonForRejection;
   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
   
   public String getSupplierName() {
	   
	   return supplierName;
	   
   }
   
   public String getReferenceNumber() {
	   
	   return referenceNumber;
	   
   }
   
   public Integer getBrCode() {
	   return brCode;
   }
         
}