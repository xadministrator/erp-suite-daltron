package com.struts.ap.canvass;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ApCanvassController;
import com.ejb.txn.ApCanvassControllerHome;
import com.ejb.txn.ApPurchaseRequisitionEntryController;
import com.ejb.txn.ApPurchaseRequisitionEntryControllerHome;
import com.struts.ap.voucherentry.ApVoucherLineItemList;
import com.struts.ap.voucherentry.ApVoucherPurchaseOrderLineList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApCanvassDetails;
import com.util.ApModCanvassDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModPurchaseRequisitionDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.ApModSupplierDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApCanvassAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApCanvassAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApCanvassForm actionForm = (ApCanvassForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_CANVASS_ID);

         if (frParam != null) {
         	
         	if (frParam.trim().equals(Constants.FULL_ACCESS)) {
         		
         		ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
         		if (!fieldErrors.isEmpty()) {
         			
         			saveErrors(request, new ActionMessages(fieldErrors));
         			
         			return mapping.findForward("apCanvass");
         		}
         		
         	}
         	
         	actionForm.setUserPermission(frParam.trim());
         	
         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApCanvassController EJB
*******************************************************/

         ApCanvassControllerHome homeCNV = null;
         ApCanvassController ejbCNV = null;
         ApPurchaseRequisitionEntryController ejbPR = null;
         ApPurchaseRequisitionEntryControllerHome homePR = null;

         try {

            homeCNV = (ApCanvassControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApCanvassControllerEJB", ApCanvassControllerHome.class);      
            
            homePR = (ApPurchaseRequisitionEntryControllerHome)com.util.EJBHomeFactory.
            lookUpHome("ejb/ApPurchaseRequisitionEntryControllerEJB", ApPurchaseRequisitionEntryControllerHome.class);
        
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApCanvassAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {
        	 ejbPR = homePR.create();
            ejbCNV = homeCNV.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApCanvassAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call ApCanvassController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short canvassLineNumber = 0;
         //short precisionUnit = 0;
         byte useSupplierPulldown = 0;
         
         try {
         	
            precisionUnit = ejbCNV.getGlFcPrecisionUnit(user.getCmpCode());
            canvassLineNumber = ejbCNV.getAdPrfApJournalLineNumber(user.getCmpCode());
          
            useSupplierPulldown = ejbCNV.getAdPrfApUseSupplierPulldown(user.getCmpCode());
            
            actionForm.setUseSupplierPulldown(Common.convertByteToBoolean(useSupplierPulldown));
            actionForm.setPrecisionUnit(precisionUnit);
            
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApCanvassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }	
      
         actionForm.clearSelectedApCNVPoList();
         
/*******************************************************
   -- Ap CNV Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	ArrayList list = new ArrayList();
         	
         	for(int i=0; i<actionForm.getApCNVListSize(); i++) {
         		
         		ApCanvassList apCNVList = (ApCanvassList)actionForm.getApCNVByIndex(i);
         		
         		if(Common.validateRequired(apCNVList.getSupplier()) ||
         			Common.validateRequired(apCNVList.getQuantity()) ||
					Common.validateRequired(apCNVList.getAmount())) continue;
         		
         		ApModCanvassDetails mdetails = new ApModCanvassDetails();
         		
         		mdetails.setCnvLine(Short.parseShort(apCNVList.getLineNumber()));
         		mdetails.setCnvSplSupplierCode(apCNVList.getSupplier());
         		mdetails.setCnvQuantity(Common.convertStringMoneyToDouble(apCNVList.getQuantity(), precisionUnit));
         		mdetails.setCnvUnitCost(Common.convertStringMoneyToDouble(apCNVList.getUnitPrice(), precisionUnit));
         		mdetails.setCnvAmount(Common.convertStringMoneyToDouble(apCNVList.getAmount(), precisionUnit));
         		mdetails.setCnvSplSupplierName(apCNVList.getSupplierName());
         		mdetails.setCnvRemarks(apCNVList.getRemarks());
         		mdetails.setCnvPo(Common.convertBooleanToByte(apCNVList.getPoCheckbox()));
         		
         		list.add(mdetails);
         		
         		if(apCNVList.getPoCheckbox() == true) {
         			
         			actionForm.saveSelectedApCNVPoList(apCNVList);
         			
         		}
         		
         	}
         	
         	try {
         		
         		//save ap canvass
         		
         		ejbCNV.saveApCnvss(list, new Integer(Integer.parseInt(actionForm.getPurchaseRequisitionLineCode())), user.getCmpCode());
         		
         	} catch (GlobalRecordAlreadyExistException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
		    			new ActionMessage("apCanvass.error.supplierAlreadyExists", ex.getMessage()));
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ApCanvassAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage"); 
         			
         		}
         		
         	}  
         	       	
/*******************************************************
 -- Ap CNV Add Lines Action --
 *******************************************************/
         	
         } else if(request.getParameter("addLinesButton") != null) {
         	
         	int listSize = actionForm.getApCNVListSize();
         	
         	for (int x = listSize + 1; x <= listSize + canvassLineNumber; x++) {
         		
         		ApCanvassList apCNVList = new ApCanvassList(actionForm, String.valueOf(x), null, null, null, null, null, null, false);
         		
         		apCNVList.setSupplierList(actionForm.getSupplierList());
         		
         		actionForm.saveApCNVList(apCNVList);
         		
         	}	        
         	
         	return(mapping.findForward("apCanvass"));
         	
/*******************************************************
 -- Ap CNV Delete Lines Action --
 *******************************************************/
         	
         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getApCNVListSize(); i++) {
         		
         		ApCanvassList apCNVList = actionForm.getApCNVByIndex(i);
         		
         		if (apCNVList.getDeleteCheckbox()) {
         			
         			actionForm.deleteApCNVList(i);
         			i--;
         		}
         		
         	}
         	
         	for (int i = 0; i<actionForm.getApCNVListSize(); i++) {
         		
         		ApCanvassList apCNVList = actionForm.getApCNVByIndex(i);
         		
         		apCNVList.setLineNumber(String.valueOf(i+1));
         		
         	}
         	
         	return(mapping.findForward("apCanvass"));	 
 
/*******************************************************
-- Ap Canvass Supplier Enter Action --
*******************************************************/

              } else if(!Common.validateRequired(request.getParameter("ApCNVList[" + 
                actionForm.getRowSelected() + "].isSupplierEntered"))) {
            	  
            	  ApCanvassList ApCnvPoList = 
              		actionForm.getApCNVByIndex(actionForm.getRowSelected());   	    	
              	
              	            
              	try {
                  	
              		System.out.println("SUPPLIER ENTERED="+ApCnvPoList.getSupplierName());   
              		      		      		
                  } catch (EJBException ex) {

                    if (log.isInfoEnabled()) {

                       log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                       " session: " + session.getId());
                       return mapping.findForward("cmnErrorPage"); 
                       
                    }

                 }  
        	                               
                 return(mapping.findForward("apCanvass"));
/*******************************************************
 -- Ap CNV Close Action --
 *******************************************************/
         	
         } else if (request.getParameter("closeButton") != null) {
            
            return(mapping.findForward("cmnMain"));
          
/*******************************************************
   -- Ap CNV Back Action --
*******************************************************/

     	} else if (request.getParameter("backButton") != null) {
     		
     		String path = null;
     		
     		path = "/apPurchaseRequisitionEntry.do?forward=1&purchaseRequisitionCode=" + actionForm.getPurchaseRequisitionCode() +"&conversionRate="+actionForm.getConversionRate() +"&currency="+actionForm.getCurrency();
     		
     		return(new ActionForward(path));
     		
/*******************************************************
 -- Ap CNV Load Action --
 *******************************************************/
     		
     	}
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apCanvass");

            }
            
            actionForm.reset(mapping, request);
            
            try {
            	
        		ArrayList list = new ArrayList();
        		ArrayList prList = new ArrayList();
        		Iterator i = null;
        		Iterator y = null;
        		double amount=0;
        		double qty=0;
        		
        		//suppliers
        		
        		if(actionForm.getUseSupplierPulldown()) {
            		
	            	actionForm.clearSupplierList();           	
	            	
	            	list = ejbCNV.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	System.out.println("SUPPLIER list.size()="+list.size());
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
            	}
        		
        		if (request.getParameter("forward") != null) {
        			
        			System.out.println("YES");
        			actionForm.clearApCNVList();
        			
        			int prlCode = Integer.parseInt(request.getParameter("purchaseRequisitionLineCode")); 
        			System.out.println("purchaseRequisitionLineCode" + request.getParameter("purchaseRequisitionLineCode"));
        			
        			
        			
        			ApModPurchaseRequisitionLineDetails prlDetails = ejbCNV.getApPrlByPrlCode(new Integer(prlCode), user.getCmpCode());
        			System.out.println("prlDetails.getPrlPrNumber()="+prlDetails.getPrlPrNumber());
        			actionForm.setDocumentNumber(prlDetails.getPrlPrNumber());
	       			actionForm.setDate(Common.convertSQLDateToString(prlDetails.getPrlPrDate()));
	       			actionForm.setItemName(prlDetails.getPrlIlIiName());
	       			actionForm.setItemDescription(prlDetails.getPrlIlIiDescription());
	       			actionForm.setLocation(prlDetails.getPrlIlLocName());
	       			actionForm.setUomName(prlDetails.getPrlUomName());
	       			actionForm.setQuantity(Common.convertDoubleToStringMoney(prlDetails.getPrlQuantity(), precisionUnit));
	       			actionForm.setPurchaseRequisitionCode(request.getParameter("purchaseRequisitionCode"));
	       			actionForm.setPurchaseRequisitionLineCode(request.getParameter("purchaseRequisitionLineCode"));
	       			actionForm.setPosted(request.getParameter("posted"));
	       			actionForm.setCanvassApprovalStatus(request.getParameter("canvassApprovalStatus"));
	       			actionForm.setConversionRate(request.getParameter("conversionRate"));
	       			actionForm.setCurrency(request.getParameter("currency"));
	       			
        			list = prlDetails.getCnvList();
        			
        			i = list.iterator();
        			
        			        			
        			while(i.hasNext()) {
        				
        				ApModCanvassDetails mdetails = (ApModCanvassDetails)i.next();
        				
        				
        				
        				ApCanvassList apCNVList = new ApCanvassList(actionForm, String.valueOf(mdetails.getCnvLine()), mdetails.getCnvSplSupplierCode(), 
        						Common.convertDoubleToStringMoney(mdetails.getCnvQuantity(), precisionUnit), 
								Common.convertDoubleToStringMoney(mdetails.getCnvUnitCost(), precisionUnit), 
								Common.convertDoubleToStringMoney(mdetails.getCnvAmount(), precisionUnit),
								mdetails.getCnvSplSupplierName(), mdetails.getCnvRemarks(),
								Common.convertByteToBoolean(mdetails.getCnvPo()));

        				apCNVList.setSupplierList(actionForm.getSupplierList());
        				actionForm.saveApCNVList(apCNVList);
        				
        			}
        			/*
        			 if(list.size() == 0){
 			        	System.out.println("list.size()" + list.size());
 			        	ApModPurchaseRequisitionDetails prldetails = ejbPR.getApPrByPrCode(Common.convertStringToInteger(request.getParameter("purchaseRequisitionCode")), user.getCmpCode());
 	            		
 	        			prList = prldetails.getPrPrlList();
 	        			y = prList.iterator();
 	        			System.out.println("prListsize()" + prList.size());
 						while (y.hasNext()) {
 	            			
 	            			ApModPurchaseRequisitionLineDetails mPrlDetails = (ApModPurchaseRequisitionLineDetails)y.next();
 	            			System.out.println("mPrlDetails.getPrlAmount() = "+mPrlDetails.getPrlAmount()+"mPrlDetails.getPrlQuantity()="+mPrlDetails.getPrlQuantity());
 	            			amount=mPrlDetails.getPrlAmount();
 	            			qty=mPrlDetails.getPrlQuantity();
 						}
 						
 								        	
 			        }*/
        			 
	       			int remainingList = canvassLineNumber - (list.size() % canvassLineNumber) + list.size();
			        System.out.println("remainingList = "+remainingList + "list.size() = "+list.size()+"canvassLineNumber = "+canvassLineNumber);
			        
			        for (int x = list.size() + 1; x <= remainingList; x++) {
			        	System.out.println("weee2");
			        	ApCanvassList apCNVList = new ApCanvassList(actionForm, String.valueOf(x), null, actionForm.getQuantity(), Common.convertDoubleToStringMoney(prlDetails.getPrlAmount(), precisionUnit), Common.convertDoubleToStringMoney(prlDetails.getPrlQuantity() * prlDetails.getPrlAmount(), precisionUnit), null, null, false);
			        	
			        	apCNVList.setSupplierList(actionForm.getSupplierList());
				        actionForm.saveApCNVList(apCNVList);
				        			        	
			        }
			        
			       
	       			
	       		}
        		System.out.println("actionForm.getSelectedApCNVPoListSize()="+actionForm.getSelectedApCNVPoListSize());
        		
        		
        		for(int a=0; a<actionForm.getSelectedApCNVPoListSize(); a++) {
        			
        			ApCanvassList selectedCnvPo = actionForm.getSelectedApCNVPoByIndex(a);
        			
        		}
        		
        		for(int j=0; j<actionForm.getSelectedApCNVPoListSize(); j++) {
        			
        			ApCanvassList selectedCnvPo = actionForm.getSelectedApCNVPoByIndex(j);
        			
        			for(int k=0; k<actionForm.getApCNVListSize(); k++) {
						
						ApCanvassList apCnvList = (ApCanvassList)actionForm.getApCNVByIndex(k);
						
						if(selectedCnvPo.getLineNumber().equals(apCnvList.getLineNumber())) {
							
							apCnvList.setPoCheckbox(true);
							break;
							
						}
						
					}
        			
        		}
        		
    			this.setFormProperties(actionForm);    	
    			
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCanvassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {//from update to saveButton
            	
               if ((request.getParameter("saveButton") != null) && 
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            return(mapping.findForward("apCanvass"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApCanvassAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          
          e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

       }

    }
   
//   private void setFormProperties(ApCanvassForm actionForm) {
//   	
//   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
//   		
//   		if (actionForm.getEnableFields() == true) {
//   			
//   			actionForm.setEnableFields(true);
//   			actionForm.setShowAddLinesButton(true);
//   			actionForm.setShowDeleteLinesButton(true);
//   			
//   		} else {
//   			
//   			actionForm.setShowAddLinesButton(false);
//   			actionForm.setShowDeleteLinesButton(false);
//   			
//   		}
//   		
//   	} else {
//   		
//   		actionForm.setShowAddLinesButton(false);
//   		actionForm.setShowDeleteLinesButton(false);
//   		
//   	}
//   	
//   }
   
   private void setFormProperties(ApCanvassForm actionForm) {
   	
   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
   		
   		System.out.println("actionForm.getCanvassApprovalStatus()="+actionForm.getCanvassApprovalStatus());

   		if(actionForm.getCanvassApprovalStatus().equals("") || actionForm.getCanvassApprovalStatus().equals(null)
   				)
   		
   		{
   			actionForm.setEnableFields(true);
   			actionForm.setEnablePoCheckbox(true);
   			actionForm.setShowSaveButton(true);
   			actionForm.setShowAddLinesButton(true);
   			actionForm.setShowDeleteLinesButton(true);	
   		} else {
   			
   			actionForm.setEnableFields(false);
   			actionForm.setEnablePoCheckbox(false);
   			actionForm.setShowSaveButton(false);
   			actionForm.setShowAddLinesButton(false);
   			actionForm.setShowDeleteLinesButton(false);
   			
   		}
   		
   		
   	}
   	
   }

}