package com.struts.ap.canvass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.EJBCommon;

public class ApCanvassForm extends ActionForm implements Serializable {
	
	private short precisionUnit = 0;
	private String purchaseRequisitionCode = null;
	private String purchaseRequisitionLineCode = null;
	private String documentNumber = null;
	private String date = null;
	private String itemName = null;
	private String itemDescription = null;

	private String location = null;
	private String uomName = null;
	private String quantity = null;
	private String conversionRate = null;
	private String currency = null;
	private ArrayList supplierList = new ArrayList();
	
	
	private String txnStatus = null;
	private String userPermission = new String();
	private int rowSelected = 0;
	private String pageState = new String();
	private boolean useSupplierPulldown = false;
	private short quantityPrecisionUnit = 0;
	private String posted = null;
	private String canvassApprovalStatus = null;
	
	private boolean enableFields = false;
	private boolean enablePoCheckbox = false;
	private String backButton = null;
	private String closeButton = null;
	private boolean showSaveButton = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	
	private ArrayList apCNVList = new ArrayList();
	private int apCNVListSize = 0;
	private ArrayList selectedApCnvPoList = new ArrayList();
	
	public short getPrecisionUnit() {
		
		return precisionUnit;
		
	}
	
	public void setPrecisionUnit(short precisionUnit) {
		
		this.precisionUnit = precisionUnit;
		
	}
	
	public String getPurchaseRequisitionCode() {
		
		return purchaseRequisitionCode;
		
	}
	
	public void setPurchaseRequisitionCode(String purchaseRequisitionCode) {
		
		this.purchaseRequisitionCode = purchaseRequisitionCode;
		
	}
	
	public String getPurchaseRequisitionLineCode() {
		
		return purchaseRequisitionLineCode;
		
	}
	
	public void setPurchaseRequisitionLineCode(String purchaseRequisitionLineCode) {
		
		this.purchaseRequisitionLineCode = purchaseRequisitionLineCode;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public String getUomName() {
		
		return uomName;
		
	}
	
	public void setUomName(String uomName) {
		
		this.uomName = uomName;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	public ArrayList getSupplierList() {
		
		return supplierList;
		
	}
	
	public void setSupplierList(String supplier) {
		
		supplierList.add(supplier);
		
	}
	
	public void clearSupplierList() {
		
		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	   
	
	public ApCanvassList getApCNVByIndex(int index) {
		
		return((ApCanvassList)apCNVList.get(index));
		
	}
	
	public Object[] getApCNVList() {
		
		return apCNVList.toArray();
		
	}
	
	public int getApCNVListSize() {
		
		apCNVListSize = apCNVList.size();
		return apCNVList.size();
		
	}
	
	public void saveApCNVList(Object newApCNVList) {
		
		apCNVList.add(newApCNVList);
		
	}
	
	public void deleteApCNVList(int rowSelected){
		
		apCNVList.remove(rowSelected);
		
	}
	
	public void clearApCNVList() {
		
		apCNVList.clear();
		
	}
	
	public ApCanvassList getSelectedApCNVPoByIndex(int index) {
		
		return((ApCanvassList)selectedApCnvPoList.get(index));
		
	}
	
	public Object[] getSelectedApCNVPoList() {
		
		return selectedApCnvPoList.toArray();
		
	}
	
	public int getSelectedApCNVPoListSize() {
		
		return selectedApCnvPoList.size();
		
	}
	
	public void saveSelectedApCNVPoList(Object newSelectedApCNVPoList) {
		
		selectedApCnvPoList.add(newSelectedApCNVPoList);
		
	}
	
	public void deleteSelectedApCNVPoList(int rowSelected){
		
		selectedApCnvPoList.remove(rowSelected);
		
	}
	
	public void clearSelectedApCNVPoList() {
		
		selectedApCnvPoList.clear();
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public void setRowSelected(Object selectedApCNVList, boolean isEdit) {
		
		this.rowSelected = apCNVList.indexOf(selectedApCNVList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public boolean getUseSupplierPulldown() {
		
		return useSupplierPulldown;
		
	}
	
	public void setUseSupplierPulldown(boolean useSupplierPulldown) {
		
		this.useSupplierPulldown = useSupplierPulldown;
		
	}
	
	public short getQuantityPrecisionUnit() {
		
		return quantityPrecisionUnit;
		
	}
	
	public void setQuantityPrecisionUnit(short quantityPrecisionUnit) {
		
		this.quantityPrecisionUnit = quantityPrecisionUnit;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public String getConversionRate() {
		
		return conversionRate;
		
	}
	
	public void setConversionRate(String conversionRate) {
		
		this.conversionRate = conversionRate;
		
	}
	
	public String getCurrency() {
		
		return currency;
		
	}
	
	public void setCurrency(String currency) {
		
		this.currency = currency;
		
	}
	
	public String getCanvassApprovalStatus() {
			
			return canvassApprovalStatus;
			
	}
	
	public void setCanvassApprovalStatus(String canvassApprovalStatus) {
		
		this.canvassApprovalStatus = canvassApprovalStatus;
		
	}
	
	
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public boolean getEnablePoCheckbox() {
		
		return enablePoCheckbox;
		
	}
	
	public void setEnablePoCheckbox(boolean enablePoCheckbox) {
		
		this.enablePoCheckbox = enablePoCheckbox;
		
	}
	
	public void setBackButton(String backButton) {
		
		this.backButton = backButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getShowAddLinesButton() {
		
		return showAddLinesButton;
		
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		
		this.showAddLinesButton = showAddLinesButton;
		
	}
	
	public boolean getShowDeleteLinesButton() {
		
		return showDeleteLinesButton;
		
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		
		this.showDeleteLinesButton = showDeleteLinesButton;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for(int i=0; i<apCNVList.size(); i++) {
			
			ApCanvassList actionList = (ApCanvassList)apCNVList.get(i);
			actionList.setPoCheckbox(false);
			
		}
		
		backButton = null;
		closeButton = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveButton") != null) {
			
			double total = 0d;
			int numOfLines = 0;
			
			Iterator i = apCNVList.iterator();
			
			while(i.hasNext()) {
				
				ApCanvassList cnvList = (ApCanvassList)i.next();
				
				if(cnvList.getPoCheckbox() == false || Common.validateRequired(cnvList.getSupplier()) || Common.validateRequired(cnvList.getQuantity()) ||
					Common.validateRequired(cnvList.getAmount())) {
					
					continue;
					
				}
				
				if(Common.validateRequired(cnvList.getSupplier())) {
					errors.add("supplier",
				               new ActionMessage("apCanvass.error.supplierRequired", cnvList.getLineNumber()));
				}
				
				if(Common.validateRequired(cnvList.getQuantity())) {
					errors.add("quantity",
				               new ActionMessage("apCanvass.error.quantityRequired", cnvList.getLineNumber()));
				}
				
				if(!Common.validateNumberFormat(cnvList.getQuantity())) {
					errors.add("quantity",
				               new ActionMessage("apCanvass.error.quantityInvalid", cnvList.getLineNumber()));
				}
				
				if(Common.validateRequired(cnvList.getUnitPrice())) {
					errors.add("unitPrice",
				               new ActionMessage("apCanvass.error.unitPriceRequired", cnvList.getLineNumber()));
				}
				
				if(!Common.validateMoneyFormat(cnvList.getUnitPrice())) {
					errors.add("unitPrice",
				               new ActionMessage("apCanvass.error.unitPriceInvalid", cnvList.getLineNumber()));
				}
				
				numOfLines++;
				
				total += Common.convertStringMoneyToDouble(cnvList.getQuantity(), quantityPrecisionUnit);
				
			}
			
			System.out.println("--------------------numOfLines="+numOfLines);
			if(numOfLines == 0) {
				
				errors.add("apCNVList",
			               new ActionMessage("apCanvass.error.canvassMustHaveAtLeastOneLine"));
				
			}
			
			if(total <= 0) {
				
				errors.add("apCNVList",
			               new ActionMessage("apCanvass.error.negativeOrZeroQuantityNotAllowed"));
				
			}
			
		}
		
		return errors;
		
	}
	
}