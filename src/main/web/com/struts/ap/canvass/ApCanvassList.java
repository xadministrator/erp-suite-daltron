package com.struts.ap.canvass;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.util.Constants;

public class ApCanvassList implements Serializable {
	
	private String lineNumber = null;
	private String supplier = null;
	private ArrayList supplierList = new ArrayList();
	private String quantity = null;
	private String unitPrice = null;
	private String amount = null;
	private boolean deleteCheckBox = false;
	private boolean poCheckbox = false;
	private String supplierName = null;
	private String remarks = null;
	
	private String supplierButton = null;
	
	private ApCanvassForm parentBean = null;
	
	public ApCanvassList(ApCanvassForm parentBean, String lineNumber, String supplier, String quantity, 
			String unitPrice, String amount,String supplierName, String remarks, boolean poCheckbox) {
		
		this.parentBean = parentBean;
		this.lineNumber = lineNumber;
		this.supplier = supplier;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.supplierName = supplierName;
		this.remarks = remarks;
		this.poCheckbox = poCheckbox;
		
	}
	
	public void setSupplierButton(String supplierButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	
	public String getSupplierName() {
	   	
	   	return supplierName;
	   	
	  }
	   
	public void setSupplierName(String supplierName) {
   	
		this.supplierName = supplierName;
   	
  	}
	
	public String getRemarks() {
	   	
	   	return remarks;
	   	
	  }
	   
	public void setRemarks(String remarks) {
   	
		this.remarks = remarks;
   	
  	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
	
	public String getSupplier() {
		
		return supplier;
		
	}
	
	public void setSupplier(String supplier) {
		
		this.supplier = supplier;
		
	}
	
	public ArrayList getSupplierList() {
		
		return supplierList;
		
	}
	
	public void setSupplierList(ArrayList supplierList) {
		
		this.supplierList = supplierList;
		
	}
	
	public void clearSupplierList() {
		
		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	public String getUnitPrice() {
		
		return unitPrice;
		
	}
	
	public void setUnitPrice(String unitPrice) {
		
		this.unitPrice = unitPrice;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
	public boolean getDeleteCheckbox() {
		
		return deleteCheckBox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckBox) {
		
		this.deleteCheckBox = deleteCheckBox;
		
	}
	
	public boolean getPoCheckbox() {
		
		return poCheckbox;
		
	}
	
	public void setPoCheckbox(boolean poCheckbox) {
		
		this.poCheckbox = poCheckbox;
		
	}
	
	
}