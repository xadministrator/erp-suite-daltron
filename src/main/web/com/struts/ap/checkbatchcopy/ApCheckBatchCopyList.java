package com.struts.ap.checkbatchcopy;

import java.io.Serializable;

public class ApCheckBatchCopyList implements Serializable {

   private Integer checkCode = null;
   private String checkType = null;
   private String supplierCode = null;
   private String bankAccount = null;
   private String date = null;
   private String checkNumber = null;
   private String documentNumber = null;
   private String amount = null;
   private boolean released = false;

   private boolean copy = false;
       
   private ApCheckBatchCopyForm parentBean;
    
   public ApCheckBatchCopyList(ApCheckBatchCopyForm parentBean,
	  Integer checkCode,
	  String checkType,  
	  String supplierCode,  
	  String bankAccount,  
	  String date,  
	  String checkNumber,
	  String documentNumber,  
	  String amount,  
	  boolean released) {

      this.parentBean = parentBean;
      this.checkCode = checkCode;
      this.checkType = checkType;
      this.supplierCode = supplierCode;
      this.bankAccount = bankAccount;
      this.date = date;
      this.checkNumber = checkNumber;
      this.documentNumber = documentNumber;
      this.amount = amount;
      this.released = released;
      
   }
   
   public Integer getCheckCode() {
   	
   	  return checkCode;
   	  
   }
   
   public String getCheckType() {
   	
   	  return checkType;
   	  
   }

   public String getSupplierCode() {

      return supplierCode;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	 
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public boolean getReleased() {
   	
   	  return released;
   	  
   }
   
   public boolean getCopy() {
   	
   	  return copy;
   	
   }
   
   public void setCopy(boolean copy) {
   	
   	  this.copy = copy;
   	
   }
      
}