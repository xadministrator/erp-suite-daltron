package com.struts.ap.checkbatchprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApCheckBatchPrintController;
import com.ejb.txn.ApCheckBatchPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModCheckDetails;

public final class ApCheckBatchPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApCheckBatchPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApCheckBatchPrintForm actionForm = (ApCheckBatchPrintForm)form;
         
         actionForm.setCheckReport(null);
         actionForm.setCheckVoucherReport(null);
         actionForm.setCheckEditListReport(null);
         actionForm.setCheckCodeList(null);
         
         String frParam = Common.getUserPermission(user, Constants.AP_CHECK_BATCH_PRINT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apCheckBatchPrint");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize ApCheckBatchPrintController EJB
*******************************************************/

         ApCheckBatchPrintControllerHome homeCBP = null;
         ApCheckBatchPrintController ejbCBP = null;

         try {

            homeCBP = (ApCheckBatchPrintControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApCheckBatchPrintControllerEJB", ApCheckBatchPrintControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApCheckBatchPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCBP = homeCBP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApCheckBatchPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
 /*******************************************************
     Call ApCheckBatchPostController EJB
     getGlFcPrecisionUnit
  *******************************************************/
         
         short precisionUnit = 0;
         boolean enableCheckBatch = false;
         boolean useSupplierPulldown = true;
         
         try { 
         	
            precisionUnit = ejbCBP.getGlFcPrecisionUnit(user.getCmpCode());
            enableCheckBatch = Common.convertByteToBoolean(ejbCBP.getAdPrfEnableApCheckBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableCheckBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbCBP.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);            
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApCheckBatchPrintAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ap CBP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apCheckBatchPrint"));
	     
/*******************************************************
   -- Ap CBP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apCheckBatchPrint"));                         

/*******************************************************
   -- Ap CBP Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ap CBP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 

/*******************************************************
   -- Ap CBP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("refresh") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCheckType())) {
	        		
	        		criteria.put("checkType", actionForm.getCheckType());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	        	}	        	

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getNumberFrom())) {
	        		
	        		criteria.put("checkNumberFrom", actionForm.getNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getNumberTo())) {
	        		
	        		criteria.put("checkNumberTo", actionForm.getNumberTo());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	 
	        	if (!Common.validateRequired(actionForm.getReleased())) {
	        			        	   		        		
	        		criteria.put("released", actionForm.getReleased());
	        	
	            }
	            
	            if (actionForm.getCheckVoid()) {	        	
		        	   		        		
	        		criteria.put("checkVoid", new Byte((byte)1));
                
                }
                
                if (!actionForm.getCheckVoid()) {
                	
                	criteria.put("checkVoid", new Byte((byte)0));
                	
                }
	        
                
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbCBP.getApChkByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ApCheckBatchPrintAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }

	        	
	        	
	     	}
            
            try {
            	
            	actionForm.clearApCBPList();
            	
            	ArrayList list = ejbCBP.getApChkByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModCheckDetails mdetails = (ApModCheckDetails)i.next();
      		
            		ApCheckBatchPrintList apCBPList = new ApCheckBatchPrintList(actionForm,
            		    mdetails.getChkCode(),
            		    mdetails.getChkType(),
            		    mdetails.getChkSplSupplierCode(),
            		    mdetails.getChkBaName(),
            		    Common.convertSQLDateToString(mdetails.getChkDate()),
            		    mdetails.getChkNumber(),
            		    mdetails.getChkDocumentNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getChkReleased()));
            		    
            		actionForm.saveApCBPList(apCBPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findCheck.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apCheckBatchPrint");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("apCheckBatchPrint"));

/*******************************************************
   -- Ap CBP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap CBP Print Check Action --
*******************************************************/

         } else if (request.getParameter("printCheckButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
  	         // get selected checks
  	         
  	        ArrayList checkCodeList = new ArrayList();
  	          	        
  	        int j = 0;
                        
		    for(int i=0; i<actionForm.getApCBPListSize(); i++) {
		    
		       ApCheckBatchPrintList actionList = actionForm.getApCBPByIndex(i);
		    	
               if (actionList.getPrint()) {
               	               	
               		checkCodeList.add(actionList.getCheckCode());

               }
	           
	       }	
	       
	       if (checkCodeList.size() > 0) {
	       		       	   	
	       	   	actionForm.setCheckReport(Constants.STATUS_SUCCESS);	      
	       	   	actionForm.setCheckCodeList(checkCodeList);
	       	   	       	   	       	
	       }
	       
	       try {
            	
            	actionForm.clearApCBPList();
            	
            	ArrayList list = ejbCBP.getApChkByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModCheckDetails mdetails = (ApModCheckDetails)i.next();
      		
            		ApCheckBatchPrintList apCBPList = new ApCheckBatchPrintList(actionForm,
            		    mdetails.getChkCode(),
            		    mdetails.getChkType(),
            		    mdetails.getChkSplSupplierCode(),
            		    mdetails.getChkBaName(),
            		    Common.convertSQLDateToString(mdetails.getChkDate()),
            		    mdetails.getChkNumber(),
            		    mdetails.getChkDocumentNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getChkReleased()));
            		    
            		actionForm.saveApCBPList(apCBPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }	                       
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("apCheckBatchPrint"));
	       
/*******************************************************
   -- Ap CBP Print CV Action --
*******************************************************/

         } else if (request.getParameter("printCVButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
  	         // get selected checks
  	         
  	        ArrayList checkCodeList = new ArrayList();
  	          	        
  	        int j = 0;
                        
		    for(int i=0; i<actionForm.getApCBPListSize(); i++) {
		    
		       ApCheckBatchPrintList actionList = actionForm.getApCBPByIndex(i);
		    	
               if (actionList.getPrint()) {
               	               	
               	   checkCodeList.add(actionList.getCheckCode());

               }
	           
	       }	
	       
	       if (checkCodeList.size() > 0) {
	       		       	   	
	       	   	actionForm.setCheckVoucherReport(Constants.STATUS_SUCCESS);	      
	       	   	actionForm.setCheckCodeList(checkCodeList);
	       	   	       	   	       	
	       }
	       
	        try {
            	
            	actionForm.clearApCBPList();
            	
            	ArrayList list = ejbCBP.getApChkByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModCheckDetails mdetails = (ApModCheckDetails)i.next();
      		
            		ApCheckBatchPrintList apCBPList = new ApCheckBatchPrintList(actionForm,
            		    mdetails.getChkCode(),
            		    mdetails.getChkType(),
            		    mdetails.getChkSplSupplierCode(),
            		    mdetails.getChkBaName(),
            		    Common.convertSQLDateToString(mdetails.getChkDate()),
            		    mdetails.getChkNumber(),
            		    mdetails.getChkDocumentNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getChkReleased()));
            		    
            		actionForm.saveApCBPList(apCBPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }	                       
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("apCheckBatchPrint"));

/*******************************************************
    -- Ap CBP Edit List Print Action --
 *******************************************************/

	  } else if (request.getParameter("editListButton") != null  &&
	    actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	     	
	     // get selected checks
	     
	    ArrayList checkCodeList = new ArrayList();
	      	        
	    int j = 0;
	                 
	    for(int i=0; i<actionForm.getApCBPListSize(); i++) {
	    
	       ApCheckBatchPrintList actionList = actionForm.getApCBPByIndex(i);
	    	
	        if (actionList.getPrint()) {
	        	               	
	        	   checkCodeList.add(actionList.getCheckCode());               	               	   	            

	        }
	       
	   }	
	   
	   if (checkCodeList.size() > 0) {
	   		       	   	
	   	   	actionForm.setCheckEditListReport(Constants.STATUS_SUCCESS);	      
	   	   	actionForm.setCheckCodeList(checkCodeList);	   	   	
	   	   	       	   	       	
	   }
	   
	    try {
	     	
	     	actionForm.clearApCBPList();
	     	
	     	ArrayList list = ejbCBP.getApChkByCriteria(actionForm.getCriteria(),
	     	    actionForm.getOrderBy(),
	     	    new Integer(actionForm.getLineCount()), 
	     	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	     	
	     	// check if prev should be disabled
	       if (actionForm.getLineCount() == 0) {
	        	
	          actionForm.setDisablePreviousButton(true);
	        	
	       } else {
	       	
	       	  actionForm.setDisablePreviousButton(false);
	       	
	       }
	       
	       // check if next should be disabled
	       if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	       	  
	       	  actionForm.setDisableNextButton(true);
	       	  
	       } else {
	       	  
	       	  actionForm.setDisableNextButton(false);
	       	  
	       	  //remove last record
	       	  list.remove(list.size() - 1);
	       	
	       }
	       
	     	Iterator i = list.iterator();
	     	
	     	while (i.hasNext()) {
	     		
	     		ApModCheckDetails mdetails = (ApModCheckDetails)i.next();
		
	     		ApCheckBatchPrintList apCBPList = new ApCheckBatchPrintList(actionForm,
	     		    mdetails.getChkCode(),
	     		    mdetails.getChkType(),
	     		    mdetails.getChkSplSupplierCode(),
	     		    mdetails.getChkBaName(),
	     		    Common.convertSQLDateToString(mdetails.getChkDate()),
	     		    mdetails.getChkNumber(),
	     		    mdetails.getChkDocumentNumber(),
	     		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
	     		    Common.convertByteToBoolean(mdetails.getChkReleased()));
	     		    
	     		actionForm.saveApCBPList(apCBPList);
	     		
	     	}
	
	     } catch (GlobalNoRecordFoundException ex) {
	        
	        // disable prev next buttons
	       actionForm.setDisableNextButton(true);
	        actionForm.setDisablePreviousButton(true);
	
	     } catch (EJBException ex) {
	
	        if (log.isInfoEnabled()) {
	
	           log.info("EJBException caught in ApCheckBatchPrintAction.execute(): " + ex.getMessage() +
	           " session: " + session.getId());
	           return mapping.findForward("cmnErrorPage"); 
	           
	        }
	
	     }	                       
	     
	     actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
	     return(mapping.findForward("apCheckBatchPrint"));

/*******************************************************
   -- Ap CBP Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearApCBPList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseSupplierPulldown()) {
            	
            		actionForm.clearSupplierCodeList();
            	
	            	list = ejbCBP.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierCodeList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
            	}

            	actionForm.clearBankAccountList();
            	
            	list = ejbCBP.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbCBP.getApOpenCbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
                        
            return(mapping.findForward("apCheckBatchPrint"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApCheckBatchPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}