package com.struts.ap.checkbatchprint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApCheckBatchPrintForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList(); 
   private String checkType = null;
   private ArrayList checkTypeList = new ArrayList();
   private boolean checkVoid = false;
   private String supplierCode = null;   
   private ArrayList supplierCodeList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String numberFrom = null;
   private String numberTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String released = null;
   private ArrayList releasedList = new ArrayList();
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private boolean useSupplierPulldown = true;
   private String pageState = new String();
   private ArrayList apCBPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();
   
   private String checkReport = null;   
   private String checkVoucherReport = null;
   private String checkEditListReport = null;
   private ArrayList checkCodeList = null;
   
   
   public String getCheckReport() {
   	
   	   return checkReport;
   }
   
   public void setCheckReport(String checkReport) {
   	
   	   this.checkReport = checkReport;
   	
   }
   
   public String getCheckVoucherReport() {
   	
   	   return checkVoucherReport;
   }
   
   public void setCheckVoucherReport(String checkVoucherReport) {
   	
   	   this.checkVoucherReport = checkVoucherReport;
   	
   }
   
   public String getCheckEditListReport() {
   	
   	   return checkEditListReport;
   }
   
   public void setCheckEditListReport(String checkEditListReport) {
   	
   	   this.checkEditListReport = checkEditListReport;
   	
   }
   
   public ArrayList getCheckCodeList() {   	  	
   	  return(checkCodeList);
   }
   
   public void setCheckCodeList(ArrayList checkCodeList) {
   	  this.checkCodeList = checkCodeList;
   }

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   
   public ApCheckBatchPrintList getApCBPByIndex(int index) {

      return((ApCheckBatchPrintList)apCBPList.get(index));

   }

   public Object[] getApCBPList() {

      return apCBPList.toArray();

   }

   public int getApCBPListSize() {

      return apCBPList.size();

   }

   public void saveApCBPList(Object newApCBPList) {

      apCBPList.add(newApCBPList);

   }

   public void clearApCBPList() {
   	
      apCBPList.clear();
      
   }

   public void setRowSelected(Object selectedApCBPList, boolean isEdit) {

      this.rowSelected = apCBPList.indexOf(selectedApCBPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getCheckType() {
   	
   	  return checkType;
   	  
   }
   
   public void setCheckType(String checkType) {
   	
   	  this.checkType = checkType;
   	  
   }                         

   public ArrayList getCheckTypeList() {

      return checkTypeList;

   }
   
   public boolean getCheckVoid() {
   	
   	  return checkVoid;
   	
   }
   
   public void setCheckVoid(boolean checkVoid) {
   	
   	  this.checkVoid = checkVoid;
   	
   }

   public String getSupplierCode() {

      return supplierCode;

   }

   public void setSupplierCode(String supplierCode) {

      this.supplierCode = supplierCode;

   }

   public ArrayList getSupplierCodeList() {

      return supplierCodeList;

   }

   public void setSupplierCodeList(String supplierCode) {

      supplierCodeList.add(supplierCode);

   }
   
   public void clearSupplierCodeList() {

      supplierCodeList.clear();
      supplierCodeList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getBankAccount() {

      return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

      this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

      return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

      bankAccountList.add(bankAccount);

   }
   
   public void clearBankAccountList() {

      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }

   public String getNumberFrom() {

      return numberFrom;

   }

   public void setNumberFrom(String numberFrom) {

      this.numberFrom = numberFrom;

   }                         

   public String getNumberTo() {

      return numberTo;

   }

   public void setNumberTo(String numberTo) {

      this.numberTo = numberTo;

   }

   public String getDocumentNumberFrom() {

      return documentNumberFrom;

   }

   public void setDocumentNumberFrom(String documentNumberFrom) {

      this.documentNumberFrom = documentNumberFrom;

   }                         

   public String getDocumentNumberTo() {

      return documentNumberTo;

   }

   public void setDocumentNumberTo(String documentNumberTo) {

      this.documentNumberTo = documentNumberTo;

   } 
 
   public String getReleased() {
   	
   	  return released;
   	  
   }
   
   public void setReleased(String released) {
   	
   	  this.released = released;
   	  
   }
   
   public ArrayList getReleasedList() {
   	
   	  return releasedList;
   
   }
   
   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }
   
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }       
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }  
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  batchName = Constants.GLOBAL_BLANK;
      checkTypeList.clear();
      checkTypeList.add(Constants.GLOBAL_BLANK);
      checkTypeList.add("PAYMENT");
      checkTypeList.add("DIRECT");
      if (checkType == null) checkType = Constants.GLOBAL_BLANK;          
      checkVoid = false;
   	  supplierCode = Constants.GLOBAL_BLANK;
   	  bankAccount = Constants.GLOBAL_BLANK;
   	  dateFrom = null;
   	  dateTo = null;
   	  numberFrom = null;
   	  numberTo = null;
   	  documentNumberFrom = null;
   	  documentNumberTo = null;
      releasedList.clear();
      releasedList.add(Constants.GLOBAL_BLANK);
      releasedList.add("YES");
      releasedList.add("NO");
      released = Constants.GLOBAL_BLANK;   	  
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      approvalStatus = "DRAFT";
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add("YES");
      postedList.add("NO");
      posted = Constants.GLOBAL_NO;
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("DOCUMENT NUMBER");
	      orderByList.add("BANK ACCOUNT");
	      orderByList.add("SUPPLIER CODE");
	      orderByList.add("CHECK NUMBER");
	      orderBy = Constants.GLOBAL_BLANK;   
	  
	  }     
	  
	  for (int i=0; i<apCBPList.size(); i++) {
	  	
	  	  ApCheckBatchPrintList actionList = (ApCheckBatchPrintList)apCBPList.get(i);
	  	  actionList.setPrint(false);
	  	
	  }
	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("checkBatchPrint.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("checkBatchPrint.error.maxRowsInvalid"));
		
		   }	 	  

           /*if (Common.validateRequired(checkType)) {

             errors.add("checkType",
               new ActionMessage("checkBatchPrint.error.checkTypeRequired"));

           }*/

      	
           if (!Common.validateDateFormat(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("checkBatchPrint.error.dateFromInvalid"));

           }         
         
           if (!Common.validateDateFormat(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("checkBatchPrint.error.dateToInvalid"));

           }                  

      } else if (request.getParameter("printCheckButton") != null || request.getParameter("printCVButton") != null) {
      	
      	 String selectedBankAccount = null;
      	
      	 for (int i=0; i<apCBPList.size(); i++) {
	  	
		  	 ApCheckBatchPrintList actionList = (ApCheckBatchPrintList)apCBPList.get(i);
		  	 
		  	 if (actionList.getPrint()) {
		  	 
		  	 
			  	 if (selectedBankAccount != null && !selectedBankAccount.equals(actionList.getBankAccount())) {
			  	 	
			  	 	errors.add("bankAccount",
	               		new ActionMessage("checkBatchPrint.error.bankAccountError"));
			  	 	
			  	 } else {
			  	 	 
			  	 	 selectedBankAccount = actionList.getBankAccount();
			  	 	
			  	 }
			  	 
			  }		  	 
	  	
	  	 }
      	
      	
      }
      
      return errors;

   }
}