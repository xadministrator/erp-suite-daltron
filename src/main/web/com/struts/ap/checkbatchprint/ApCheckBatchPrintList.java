package com.struts.ap.checkbatchprint;

import java.io.Serializable;

public class ApCheckBatchPrintList implements Serializable {

   private Integer checkCode = null;
   private String checkType = null;
   private String supplierCode = null;
   private String bankAccount = null;
   private String date = null;
   private String checkNumber = null;
   private String documentNumber = null;
   private String amount = null;
   private boolean released = false;

   private boolean print = false;
       
   private ApCheckBatchPrintForm parentBean;
    
   public ApCheckBatchPrintList(ApCheckBatchPrintForm parentBean,
	  Integer checkCode,
	  String checkType,  
	  String supplierCode,  
	  String bankAccount,  
	  String date,  
	  String checkNumber,
	  String documentNumber,  
	  String amount,  
	  boolean released) {

      this.parentBean = parentBean;
      this.checkCode = checkCode;
      this.checkType = checkType;
      this.supplierCode = supplierCode;
      this.bankAccount = bankAccount;
      this.date = date;
      this.checkNumber = checkNumber;
      this.documentNumber = documentNumber;
      this.amount = amount;
      this.released = released;
      
   }
   
   public Integer getCheckCode() {
   	
   	  return checkCode;
   	  
   }
   
   public String getCheckType() {
   	
   	  return checkType;
   	  
   }

   public String getSupplierCode() {

      return supplierCode;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	 
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public boolean getReleased() {
   	
   	  return released;
   	  
   }
   
   public boolean getPrint() {
   	
   	  return print;
   	
   }
   
   public void setPrint(boolean print) {
   	
   	  this.print = print;
   	
   }
      
}