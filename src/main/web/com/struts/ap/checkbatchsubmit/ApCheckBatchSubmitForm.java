package com.struts.ap.checkbatchsubmit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApCheckBatchSubmitForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList(); 
   private String supplierCode = null;
   private ArrayList supplierCodeList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String numberFrom = null;
   private String numberTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private boolean useSupplierPulldown = true;
   private String pageState = new String();
   private ArrayList apCBSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public ApCheckBatchSubmitList getApCBSByIndex(int index) {

      return((ApCheckBatchSubmitList)apCBSList.get(index));

   }

   public Object[] getApCBSList() {

      return apCBSList.toArray();

   }

   public int getApCBSListSize() {

      return apCBSList.size();

   }

   public void saveApCBSList(Object newApCBSList) {

      apCBSList.add(newApCBSList);

   }
   
   public void deleteApCBSList(int rowSelected) {

      apCBSList.remove(rowSelected);

   }

   public void clearApCBSList() {
   	
      apCBSList.clear();
      
   }

   public void setRowSelected(Object selectedApCBSList, boolean isEdit) {

      this.rowSelected = apCBSList.indexOf(selectedApCBSList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getSupplierCode() {

      return supplierCode;

   }

   public void setSupplierCode(String supplierCode) {

      this.supplierCode = supplierCode;

   }

   public ArrayList getSupplierCodeList() {

      return supplierCodeList;

   }

   public void setSupplierCodeList(String supplierCode) {

      supplierCodeList.add(supplierCode);

   }
   
   public void clearSupplierCodeList() {

      supplierCodeList.clear();
      supplierCodeList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getBankAccount() {

      return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

      this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

      return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

      bankAccountList.add(bankAccount);

   }
   
   public void clearBankAccountList() {

      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }

   public String getNumberFrom() {

      return numberFrom;

   }

   public void setNumberFrom(String numberFrom) {

      this.numberFrom = numberFrom;

   }                         

   public String getNumberTo() {

      return numberTo;

   }

   public void setNumberTo(String numberTo) {

      this.numberTo = numberTo;

   }

   public String getDocumentNumberFrom() {

      return documentNumberFrom;

   }

   public void setDocumentNumberFrom(String documentNumberFrom) {

      this.documentNumberFrom = documentNumberFrom;

   }                         

   public String getDocumentNumberTo() {

      return documentNumberTo;

   }

   public void setDocumentNumberTo(String documentNumberTo) {

      this.documentNumberTo = documentNumberTo;

   } 
 
   public String getCurrency() {
   	
   	  return currency;
   	  
   }
   
   public void setCurrency(String currency) {
   	
   	  this.currency = currency;
   	  
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   	     	
   }
          
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  batchName = Constants.GLOBAL_BLANK;
   	  supplierCode = Constants.GLOBAL_BLANK;
   	  bankAccount = Constants.GLOBAL_BLANK;
   	  currency = Constants.GLOBAL_BLANK;
   	  dateFrom = null;
   	  dateTo = null;
   	  numberFrom = null;
   	  numberTo = null;
   	  documentNumberFrom = null;
   	  documentNumberTo = null;
   	  
   	  for (int i=0; i<apCBSList.size(); i++) {
	  	
      	ApCheckBatchSubmitList actionList = (ApCheckBatchSubmitList)apCBSList.get(i);
      	
      		actionList.setSubmit(false);
	  	
      }
   	               
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("BANK ACCOUNT");
	      orderByList.add("SUPPLIER CODE");
	      orderByList.add("CHECK NUMBER");
	      orderByList.add("DOCUMENT NUMBER");
	      orderBy = "CHECK NUMBER";
	  
	  }     
	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("checkBatchSubmit.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("checkBatchSubmit.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("checkBatchSubmit.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("checkBatchSubmit.error.maxRowsInvalid"));
		
		   }
	 	  
      } 
      
      return errors;

   }
}