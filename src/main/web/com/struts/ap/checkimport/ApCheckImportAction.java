package com.struts.ap.checkimport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.ApCHKVoucherHasNoWTaxCodeException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.ApCheckImportController;
import com.ejb.txn.ApCheckImportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModAppliedVoucherDetails;
import com.util.ApModCheckDetails;
import com.util.EJBCommon;

public final class ApCheckImportAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
        
        
        try {
        	
/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArPosInterfaceUploadAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }

        } else {

            if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ApCheckImportForm actionForm = (ApCheckImportForm)form;
        
        if (request.getParameter("caller") != null) {
			
			actionForm.setCaller(request.getParameter("caller"));
			
		}
        
        String frParam = Common.getUserPermission(user, Constants.AP_CHECK_IMPORT_ID);

        if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

		        ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	            if (!fieldErrors.isEmpty()) {
	
	                saveErrors(request, new ActionMessages(fieldErrors));
	
	                return mapping.findForward("apCheckImport");
	                
	            }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ApCheckImportController EJB
*******************************************************/

        ApCheckImportControllerHome homeCI = null;
        ApCheckImportController ejbCI = null;

        try {

            homeCI = (ApCheckImportControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApCheckImportControllerEJB", ApCheckImportControllerHome.class);
             
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApCheckImportAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbCI = homeCI.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApCheckImportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
/*******************************************************
    Call ApCheckImportController EJB
    getGlFcPrecisionUnit
*******************************************************/

        short precisionUnit = 0;
        short quantityPrecisionUnit = 0;
        
        try {
        	
        	precisionUnit = ejbCI.getGlFcPrecisionUnit(user.getCmpCode());

        } catch(EJBException ex) {
        	
        	if (log.isInfoEnabled()) {
        		
        		log.info("EJBException caught in ApCheckImportAction.execute(): " + ex.getMessage() +
        				" session: " + session.getId());
        	}
        	
        	return(mapping.findForward("cmnErrorPage"));
        	
        }
                	
/*******************************************************
   -- Gl PIU Upload POS Interface Action --
*******************************************************/
        	
          if (request.getParameter("importButton") != null &&  
          		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {            

          	// get data
          	
          	ArrayList headerList = new ArrayList();
          	ArrayList detailList = new ArrayList();
          	
          	CSVParser csvParserHeader = new CSVParser(actionForm.getFilename1().getInputStream());
          	CSVParser csvParserDetails = new CSVParser(actionForm.getFilename2().getInputStream());
          	
          	String[][] header = csvParserHeader.getAllValues();
          	String[][] details = csvParserDetails.getAllValues();
          	
          	// header file 
          	
          	for (int i=1; i < header.length; i++) {
          		
          		ApModCheckDetails mDetails = new ApModCheckDetails();
          		
          		mDetails.setChkDate(Common.convertStringToSQLDate(header[i][0]));
          		mDetails.setChkCheckDate(Common.convertStringToSQLDate(header[i][0]));
          		mDetails.setChkSplName(header[i][1]);
          		mDetails.setChkAmount(Common.convertStringMoneyToDouble((header[i][3]), quantityPrecisionUnit));        			
          		mDetails.setChkDocumentNumber(header[i][2].trim());
          		mDetails.setChkCreatedBy(user.getUserName());
          		mDetails.setChkDateCreated(EJBCommon.getGcCurrentDateWoTime().getTime());
          		mDetails.setChkLastModifiedBy(user.getUserName());
          		mDetails.setChkDateLastModified(EJBCommon.getGcCurrentDateWoTime().getTime());
          		
          		headerList.add(mDetails);
          		
          	}
          	
          	// details file
          	
          	for (int i=1; i < details.length; i++) {
          		
          		ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();
          		
          		mdetails.setAvVpsChkDocumentNumber(details[i][0].trim());
          		mdetails.setAvVpsChkCheckNumber(details[i][2]);
          		mdetails.setAvApplyAmount(Common.convertStringMoneyToDouble((details[i][5]), precisionUnit));
          		mdetails.setAvVpsVouDocumentNumber(details[i][1].trim());
          		mdetails.setAvVpsChkBankAccount(details[i][3].trim());
          		mdetails.setAvDiscountAmount(Common.convertStringMoneyToDouble((details[i][4]), precisionUnit));
          		
          		detailList.add(mdetails);
          		
          	}
          	
          	Collections.sort(headerList, sortChkByDocumentNumber);
          	Collections.sort(detailList, sortAvByDocumentNumber);
          	
          	ArrayList list = new ArrayList();            	
          	Iterator hdrItr = headerList.iterator();
          	
          	String documentNumber = null;
          	
          	while(hdrItr.hasNext()) {
          		
          		ApModCheckDetails mdetails = (ApModCheckDetails)hdrItr.next();
          		
          		ArrayList avList = new ArrayList();
          		Iterator dtlItr = detailList.iterator();
          		String checkNumber = null;
          		
          		while (dtlItr.hasNext()) {
          			
          			ApModAppliedVoucherDetails mAvDetails = (ApModAppliedVoucherDetails)dtlItr.next();
          			
          			if(mAvDetails.getAvVpsChkDocumentNumber().equals(mdetails.getChkDocumentNumber())) {
    					
    					avList.add(mAvDetails);
    					
    				}

          		}
          		
          		mdetails.setChkAvList(avList);

          		list.add(mdetails);
          			
          	}
          	
          	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
          	String chkBatch = "IMPORT " + formatter.format(new java.util.Date());
          	
          	if (headerList.isEmpty() || detailList.isEmpty()) {
          		
          		errors.add(ActionMessages.GLOBAL_MESSAGE,
          				new ActionMessage("checkImport.error.noCheckToImport"));
          		
          	} else {
          		
          		try {
          			
          			ejbCI.importCheck(list, "PHP", chkBatch, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
          			
          		} catch (GlobalDocumentNumberNotUniqueException ex) {
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.documentNumberNotUnique", ex.getMessage()));
          			
          			
          		} catch (ApCHKCheckNumberNotUniqueException ex) {
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.checkNumberNotUnique", ex.getMessage()));
          			
          		} catch (ApCHKVoucherHasNoWTaxCodeException ex) {
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.invalidWTaxCodeCOA", ex.getMessage()));
          			
          		} catch (GlobalNoRecordFoundException ex) {
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.noRecordFound", ex.getMessage()));
          			
          		} catch (GlobalJournalNotBalanceException ex) {
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.amountNotBalance", ex.getMessage()));
          			
          		} catch (ApVOUOverapplicationNotAllowedException ex) {        	
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.overapplicationNotAllowed", ex.getMessage()));
          			
          		} catch (GlobalRecordInvalidException ex) {        	
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.recordMismatched", ex.getMessage()));
          			
          		} catch (GlobalTransactionAlreadyLockedException ex) {        	
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.transactionAlreadyLocked", ex.getMessage()));          		
          			
          		} catch (GlobalTransactionAlreadyPostedException ex) {        	
          			
          			errors.add(ActionMessages.GLOBAL_MESSAGE,
          					new ActionMessage("checkImport.error.voucherNotPosted", ex.getMessage()));          		
          			
          		} catch (EJBException ex) {
          			
          			if (log.isInfoEnabled()) {
          				
          				log.info("EJBException caught in ApCheckImportAction.execute(): " + ex.getMessage() +
          						" session: " + session.getId());
          				return mapping.findForward("cmnErrorPage"); 
          				
          			}
          			
          		}
          		
          	}
          	csvParserHeader.close();
          	csvParserHeader.close();
          	
/*******************************************************
    -- Ap VI Close Action --
*******************************************************/
          	
          } else if (request.getParameter("closeButton") != null) {
          	
          	return(mapping.findForward("cmnMain"));

/*******************************************************
    -- Gl PIU Load Action --
*******************************************************/
          	
          }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apCheckImport");

            }
            
            if ((request.getParameter("importButton") != null && 
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }

            actionForm.reset(mapping, request);
            
            return(mapping.findForward("apCheckImport"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	
      	  e.printStackTrace();

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApCheckImportAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
    
	private static Comparator sortChkByDocumentNumber = new Comparator() {
		
		public int compare(Object c1, Object c2) {
			
		    ApModCheckDetails check1 = (ApModCheckDetails) c1;
		    ApModCheckDetails check2 = (ApModCheckDetails) c2;
		    		    
			return check1.getChkDocumentNumber().compareTo(check2.getChkDocumentNumber());
			
		}
		
	};
	
	private static Comparator sortAvByDocumentNumber = new Comparator() {
		
		public int compare(Object v1, Object v2) {
		    
		    ApModAppliedVoucherDetails appliedVoucher1 = (ApModAppliedVoucherDetails) v1;
		    ApModAppliedVoucherDetails appliedVoucher2 = (ApModAppliedVoucherDetails) v2;
		    
			return appliedVoucher1.getAvVpsChkDocumentNumber().compareTo(appliedVoucher2.getAvVpsChkDocumentNumber());
			
		}
		
	};
}