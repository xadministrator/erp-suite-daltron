package com.struts.ap.checkimport;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApCheckImportForm extends ActionForm implements Serializable {
	
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	
	private String importButton = null;
	private String closeButton = null;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String caller = null;
	
	public void setUploadButton(String importButton) {
		
		this.importButton = importButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public FormFile getFilename1() {
		
		return filename1;
		
	}
	
	public void setFilename1(FormFile filename1) {
		
		this.filename1 = filename1;
		
	}
	
	public FormFile getFilename2() {
		
		return filename2;
		
	}
	
	public void setFilename2(FormFile filename2) {
		
		this.filename2 = filename2;
		
	}

	public String getCaller() {
		
		return caller;
		
	}
	
	public void setCaller(String caller) {
		
		this.caller = caller;		
		
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		filename1 = null;
		filename2 = null;
		importButton = null;
		closeButton = null;		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("importButton") != null) {
			
			if(Common.validateRequired(filename1.getFileName())){
				
				errors.add("filename1",
						new ActionMessage("checkImport.error.headerFileRequired"));
				
			} else if(!filename1.getFileName().substring(filename1.getFileName().indexOf(".") + 1, filename1.getFileName().length()).equalsIgnoreCase("CSV")){
				
				errors.add("filename1",
						new ActionMessage("checkImport.error.filenameInvalid"));
				
			}

			if(Common.validateRequired(filename2.getFileName())){
				
				errors.add("filename2",
						new ActionMessage("checkImport.error.detailsFileRequired"));
				
			} else if(!filename2.getFileName().substring(filename2.getFileName().indexOf(".") + 1, filename2.getFileName().length()).equalsIgnoreCase("CSV")){
				
				errors.add("filename2",
						new ActionMessage("checkImport.error.filenameInvalid"));
				
			}

		}
		
		return errors;
		
	}
}