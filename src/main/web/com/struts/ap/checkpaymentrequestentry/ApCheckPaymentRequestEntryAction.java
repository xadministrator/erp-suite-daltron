package com.struts.ap.checkpaymentrequestentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalReferenceNumberNotUniqueException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApCheckPaymentRequestEntryController;
import com.ejb.txn.ApCheckPaymentRequestEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModVoucherDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.ApTaxCodeDetails;
import com.util.ApVoucherDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApCheckPaymentRequestEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApCheckPaymentRequestEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApCheckPaymentRequestEntryForm actionForm = (ApCheckPaymentRequestEntryForm)form;

	      // reset report
	      
	     actionForm.setReport(null);
	     actionForm.setAttachment(null);
         
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
         	frParam = Common.getUserPermission(user, Constants.AP_CHECK_PAYMENT_REQUEST_ENTRY_ID);
         
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apCheckPaymentRequestEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ApCheckPaymentRequestEntryController EJB
*******************************************************/

         ApCheckPaymentRequestEntryControllerHome homeVOU = null;
         ApCheckPaymentRequestEntryController ejbVOU = null;

         try {
            
            homeVOU = (ApCheckPaymentRequestEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApCheckPaymentRequestEntryControllerEJB", ApCheckPaymentRequestEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApCheckPaymentRequestEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbVOU = homeVOU.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ApCheckPaymentRequestEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
                  
/*******************************************************
   Call ApCheckPaymentRequestEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/
         
         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         
         short precisionUnit = 0;
         short costPrecisionUnit = 0;
         short journalLineNumber = 0;
         short quantityPrecision = 0;
         Integer vouSize = null;
         boolean enableVoucherBatch = false; 
         boolean isInitialPrinting = false;
         boolean useSupplierPulldown = true;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/Check Payment Request/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
         
         try {
         	
            precisionUnit = ejbVOU.getGlFcPrecisionUnit(user.getCmpCode());
            costPrecisionUnit  = ejbVOU.getInvGpCostPrecisionUnit(user.getCmpCode());
        	journalLineNumber = ejbVOU.getAdPrfApJournalLineNumber(user.getCmpCode());
            enableVoucherBatch = Common.convertByteToBoolean(ejbVOU.getAdPrfEnableApVoucherBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableVoucherBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbVOU.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);          
            //quantityPrecision = ejbVOU.getInvGpQuantityPrecisionUnit(user.getCmpCode());
            quantityPrecision = (short)(10);
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Ap VOU Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ApVoucherDetails details = new ApVoucherDetails();
           
           details.setVouType("REQUEST");
           details.setVouCode(actionForm.getCheckPaymentRequestCode());           
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouReferenceNumber(actionForm.getReferenceNumber());
           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
           details.setVouAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getCheckPaymentRequestVoid()));
           details.setVouDescription(actionForm.getDescription());           
           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setVouPoNumber(actionForm.getPoNumber());           
           
           if (actionForm.getCheckPaymentRequestCode() == null) {
           	
           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());
	                      
           }
           
           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());
           
           ArrayList drList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getApVOUListSize(); i++) {
           	
           	   ApCheckPaymentRequestEntryList apVOUList = actionForm.getApVOUByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(apVOUList.getAccount()) &&
      	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
      	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(apVOUList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
           	   
           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apVOUList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());
           	   
           	   drList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           
           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.voucherNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apCheckPaymentRequestEntry"));
           	
           }
           
           // validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
	   	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
           	
           
           try {
           	    System.out.println("BATCH NAME="+actionForm.getBatchName());           	    
           	    Integer voucherCode = ejbVOU.saveApVouEntry(details, actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setCheckPaymentRequestCode(voucherCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.documentNumberNotUnique"));
                               	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.conversionDateNotExist")); 
           	    
           } catch (GlobalPaymentTermInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.paymentTermInvalid"));  
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyVoid"));                               	           
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.journalNotBalance"));
           	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
              	
          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("checkPaymentRequestEntry.error.branchAccountNumberInvalid"));
           	           	
           } catch (GlobalReferenceNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.referenceNumberNotUnique"));

           	    
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
    	    		
	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apCheckPaymentRequestEntry"));
	    		
	       } 
	       
        // save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
           
/*******************************************************
   -- Ap VOU Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ApVoucherDetails details = new ApVoucherDetails();
           
           details.setVouType("REQUEST");
           details.setVouCode(actionForm.getCheckPaymentRequestCode());           
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouReferenceNumber(actionForm.getReferenceNumber());
           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
           details.setVouAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getCheckPaymentRequestVoid()));
           details.setVouDescription(actionForm.getDescription());           
           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setVouPoNumber(actionForm.getPoNumber());           
           
           if (actionForm.getCheckPaymentRequestCode() == null) {
           	
           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());
	                      
           }
           
           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());
           
           ArrayList drList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getApVOUListSize(); i++) {
           	
           	   ApCheckPaymentRequestEntryList apVOUList = actionForm.getApVOUByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(apVOUList.getAccount()) &&
      	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
      	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(apVOUList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
           	   
           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apVOUList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());
           	   
           	   drList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           
           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.voucherNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apCheckPaymentRequestEntry"));
           	
           }
           
           // validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
	   	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("checkPaymentRequestEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("checkPaymentRequestEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apCheckPaymentRequestEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
           	
           
           try {
	           	           		
           	    Integer voucherCode = ejbVOU.saveApVouEntry(details, actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setCheckPaymentRequestCode(voucherCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.documentNumberNotUnique"));
                               	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.conversionDateNotExist")); 
           	    
           } catch (GlobalPaymentTermInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.paymentTermInvalid"));  
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyVoid"));                               	                             
                               
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.noApprovalApproverFound"));
                    
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.journalNotBalance"));
           	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
             	
         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("checkPaymentRequestEntry.error.branchAccountNumberInvalid"));
           	           	
           } catch (GlobalReferenceNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.referenceNumberNotUnique"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
    	    		
	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apCheckPaymentRequestEntry"));
	    		
	       } 
	       
// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   

       	   
/*******************************************************
   -- Ap CHK Generate Voucher --
*******************************************************/
         } else if (request.getParameter("generateVoucherButton") != null &&
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
        	 
        	 System.out.println(actionForm.getApprovalStatus() + " <== before if{} approval status");
        	 ArrayList drList = new ArrayList();
        	 if(Common.validateRequired(actionForm.getApprovalStatus())) {
                ApVoucherDetails details = new ApVoucherDetails();
               
                //details.setVouType("EXPENSES");  
                details.setVouCode(actionForm.getCheckPaymentRequestCode());           
                details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
                details.setVouDocumentNumber(actionForm.getDocumentNumber());
                details.setVouReferenceNumber(actionForm.getReferenceNumber());
                details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
                details.setVouAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));
                details.setVouVoid(Common.convertBooleanToByte(actionForm.getCheckPaymentRequestVoid()));
                details.setVouDescription(actionForm.getDescription());           
                details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                details.setVouPoNumber(actionForm.getPoNumber());           
                /*details.setVouApprovalStatus(null);
                actionForm.setApprovalStatus(null);
                details.setVouApprovalStatus(actionForm.getApprovalStatus());
                System.out.println(details.getVouCode() + " <== vou code?");
                System.out.println(actionForm.getApprovalStatus() + " <== approval status?");
                */
                if (actionForm.getCheckPaymentRequestCode() == null) {
                	
                   details.setVouCreatedBy(user.getUserName());
     	           details.setVouDateCreated(new java.util.Date());
     	                      
                }
                
                details.setVouLastModifiedBy(user.getUserName());
                details.setVouDateLastModified(new java.util.Date());
                
                //ArrayList drList = new ArrayList();
                int lineNumber = 0;  
                
                double TOTAL_DEBIT = 0;
                double TOTAL_CREDIT = 0;         
                           
                for (int i = 0; i<actionForm.getApVOUListSize(); i++) {
                	
                	   ApCheckPaymentRequestEntryList apVOUList = actionForm.getApVOUByIndex(i);           	   
                	              	   
                	   if (Common.validateRequired(apVOUList.getAccount()) &&
           	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
           	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;
                	   
                   byte isDebit = 0;
     		       double amount = 0d;
     	
     		       if (!Common.validateRequired(apVOUList.getDebitAmount())) {
     		       	
     		          isDebit = 1;
     		          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);
     		          
     		          TOTAL_DEBIT += amount;
     		          
     		       } else {
     		       	
     		          isDebit = 0;
     		          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit); 
     		          
     		          TOTAL_CREDIT += amount;
     		       }
     		       
     		       lineNumber++;

                	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
                	   
                	   mdetails.setDrLine((short)(lineNumber));
                	   mdetails.setDrClass(apVOUList.getDrClass());
                	   mdetails.setDrDebit(isDebit);
                	   mdetails.setDrAmount(amount);
                	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());
                	   
                	   drList.add(mdetails);
                	
                }
                
                TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
                TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
                
                if (TOTAL_DEBIT != TOTAL_CREDIT) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.voucherNotBalance"));
                         
                    saveErrors(request, new ActionMessages(errors));
                    return (mapping.findForward("apCheckPaymentRequestEntry"));
                	
                }
                System.out.println(actionForm.getCheckPaymentRequestCode() + " <== null?");
                try {
                	//Integer voucherCode = ejbVOU.generateApVoucher(details, actionForm.getCheckPaymentRequestCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	    
                	Integer voucherCode = ejbVOU.saveApVouEntry(details, actionForm.getPaymentTerm(),
            	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
            	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
            	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	        
            	    actionForm.setCheckPaymentRequestCode(voucherCode);
            	    System.out.println(actionForm.getCheckPaymentRequestCode() + " <== null after?");
                } catch (GlobalRecordAlreadyDeletedException ex) {
                	
                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.recordAlreadyDeleted"));
                	
                } catch (GlobalDocumentNumberNotUniqueException ex) {
                	
                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.documentNumberNotUnique"));
                                    	
                } catch (GlobalConversionDateNotExistException ex) {
                	
                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.conversionDateNotExist")); 
                	    
                } catch (GlobalPaymentTermInvalidException ex) {
                	
                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.paymentTermInvalid"));  
                          	
                } catch (GlobalTransactionAlreadyApprovedException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyApproved"));
                	
                } catch (GlobalTransactionAlreadyPendingException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPending"));
                	
                } catch (GlobalTransactionAlreadyPostedException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPosted"));
                	
                } catch (GlobalTransactionAlreadyVoidException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyVoid"));                               	           
                         
                } catch (GlobalNoApprovalRequesterFoundException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.noApprovalRequesterFound"));
                         
                } catch (GlobalNoApprovalApproverFoundException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.noApprovalApproverFound"));
                	   
                } catch (GlJREffectiveDateNoPeriodExistException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.effectiveDateNoPeriodExist"));
                         
                } catch (GlJREffectiveDatePeriodClosedException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.effectiveDatePeriodClosed"));
                         
                } catch (GlobalJournalNotBalanceException ex) {
                	
                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.journalNotBalance"));
                	   
                } catch (GlobalBranchAccountNumberInvalidException ex) {
                   	
               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("checkPaymentRequestEntry.error.branchAccountNumberInvalid"));
                	           	
                } catch (GlobalReferenceNumberNotUniqueException ex) {
                	
                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("checkPaymentRequestEntry.error.referenceNumberNotUnique"));

                	    
                } catch (EJBException ex) {
                	    if (log.isInfoEnabled()) {
                    	
                       log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
                          " session: " + session.getId());
                     }
                    
                    return(mapping.findForward("cmnErrorPage"));
                }
                
                if (!errors.isEmpty()) {
         	    		
     	    	   saveErrors(request, new ActionMessages(errors));
     	   		   return (mapping.findForward("apCheckPaymentRequestEntry"));
     	    		
     	       }
                  
        	 } 
        	 int lineNumber = 0;  
             
             double TOTAL_DEBIT = 0;
             double TOTAL_CREDIT = 0;         
                        
             for (int i = 0; i<actionForm.getApVOUListSize(); i++) {
             	
             	   ApCheckPaymentRequestEntryList apVOUList = actionForm.getApVOUByIndex(i);           	   
             	              	   
             	   if (Common.validateRequired(apVOUList.getAccount()) &&
        	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
        	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;
             	   
                byte isDebit = 0;
  		       double amount = 0d;
  	
  		       if (!Common.validateRequired(apVOUList.getDebitAmount())) {
  		       	
  		          isDebit = 1;
  		          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);
  		          
  		          TOTAL_DEBIT += amount;
  		          
  		       } else {
  		       	
  		          isDebit = 0;
  		          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit); 
  		          
  		          TOTAL_CREDIT += amount;
  		       }
  		       
  		       lineNumber++;

             	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
             	   
             	   mdetails.setDrLine((short)(lineNumber));
             	   mdetails.setDrClass(apVOUList.getDrClass());
             	   mdetails.setDrDebit(isDebit);
             	   mdetails.setDrAmount(amount);
             	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());
             	   
             	   drList.add(mdetails);
             	
             }
        	 try {
          		System.out.println(drList + "<== drList from action");
          		System.out.println("actionForm.getDescription()"+actionForm.getDescription());
          		vouSize = ejbVOU.generateApVoucher(actionForm.getDescription(), drList, actionForm.getCheckPaymentRequestCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
          		
          	} catch (EJBException ex) {
          		if (log.isInfoEnabled()) {
          			
          			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
          					" session: " + session.getId());
          		}
          		
          		return(mapping.findForward("cmnErrorPage"));
          	}
        	
        	 
        	 
/*******************************************************
	 -- Ap VOU Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("printButton") != null ) {

         	
         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApVoucherDetails details = new ApVoucherDetails();
	           
	           details.setVouType("REQUEST");
	           details.setVouCode(actionForm.getCheckPaymentRequestCode());           
	           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setVouDocumentNumber(actionForm.getDocumentNumber());
	           details.setVouReferenceNumber(actionForm.getReferenceNumber());
	           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
	           details.setVouAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));
	           details.setVouVoid(Common.convertBooleanToByte(actionForm.getCheckPaymentRequestVoid()));
	           details.setVouDescription(actionForm.getDescription());           
	           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
	           details.setVouPoNumber(actionForm.getPoNumber());
	           
	           if (actionForm.getCheckPaymentRequestCode() == null) {
	           	
	           	   details.setVouCreatedBy(user.getUserName());
		           details.setVouDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setVouLastModifiedBy(user.getUserName());
	           details.setVouDateLastModified(new java.util.Date());
	           
	           ArrayList drList = new ArrayList();
	           int lineNumber = 0;  
	           
	           double TOTAL_DEBIT = 0;
	           double TOTAL_CREDIT = 0;         
	                      
	           for (int i = 0; i<actionForm.getApVOUListSize(); i++) {
	           	
	           	   ApCheckPaymentRequestEntryList apVOUList = actionForm.getApVOUByIndex(i);           	   
	           	              	   
	           	   if (Common.validateRequired(apVOUList.getAccount()) &&
	      	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
	      	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;
	           	   
	           	   byte isDebit = 0;
			       double amount = 0d;
		
			       if (!Common.validateRequired(apVOUList.getDebitAmount())) {
			       	
			          isDebit = 1;
			          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);
			          
			          TOTAL_DEBIT += amount;
			          
			       } else {
			       	
			          isDebit = 0;
			          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit); 
			          
			          TOTAL_CREDIT += amount;
			       }
			       
			       lineNumber++;
	
	           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
	           	   
	           	   mdetails.setDrLine((short)(lineNumber));
	           	   mdetails.setDrClass(apVOUList.getDrClass());
	           	   mdetails.setDrDebit(isDebit);
	           	   mdetails.setDrAmount(amount);
	           	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());
	           	   
	           	   drList.add(mdetails);
	           	
	           }
	           
	           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
	           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
	           
	           if (TOTAL_DEBIT != TOTAL_CREDIT) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.voucherNotBalance"));
	                    
	               saveErrors(request, new ActionMessages(errors));
	               return (mapping.findForward("apCheckPaymentRequestEntry"));
	           	
	           }
	           
	           // validate attachment
           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	    	    	    	    	
	    	    	if (actionForm.getFilename1().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("checkPaymentRequestEntry.error.filename1NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename1Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename1().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename1SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apCheckPaymentRequestEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	    	   
	    	   if (!Common.validateRequired(filename2)) {                	       	    	
	    	    	    	    	
	    	    	if (actionForm.getFilename2().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("checkPaymentRequestEntry.error.filename2NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename2Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename2().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename2SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apCheckPaymentRequestEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	    	   
	    	   if (!Common.validateRequired(filename3)) {                	       	    	
	    	    	    	    	
	    	    	if (actionForm.getFilename3().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("checkPaymentRequestEntry.error.filename3NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename3Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename3().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename3SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apCheckPaymentRequestEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	    	   
	    	   if (!Common.validateRequired(filename4)) {                	       	    	
	    	    	    	    	
	    	    	if (actionForm.getFilename4().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("checkPaymentRequestEntry.error.filename4NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename4Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename4().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("checkPaymentRequestEntry.error.filename4SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apCheckPaymentRequestEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	           	
	           
	           try {		           	
	           	
	               Integer voucherCode = ejbVOU.saveApVouEntry(details, actionForm.getPaymentTerm(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
	           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	   actionForm.setCheckPaymentRequestCode(voucherCode);     
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.documentNumberNotUnique"));
	                               	
	           } catch (GlobalConversionDateNotExistException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.conversionDateNotExist"));    
	           	    
	           } catch (GlobalPaymentTermInvalidException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.paymentTermInvalid"));  
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.transactionAlreadyVoid"));	                    	           		           
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.noApprovalApproverFound"));
	                    
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	              	
	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("checkPaymentRequestEntry.error.branchAccountNumberInvalid"));
	           	           	
	           } catch (GlobalReferenceNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("checkPaymentRequestEntry.error.referenceNumberNotUnique"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {
    	    		
		    	   saveErrors(request, new ActionMessages(errors));
		   		   return (mapping.findForward("apCheckPaymentRequestEntry"));
		    		
		       } 
		       
		       // save attachment
	           
	           if (!Common.validateRequired(filename1)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	new File(attachmentPath).mkdirs();
	       	        	       	        		       	        		       	        	
	       	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + attachmentFileExtension);            	           	            	
	            	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename2)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	new File(attachmentPath).mkdirs();
	       	        	       	        		       	        		       	        	
	       	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + attachmentFileExtension);            	           	            	
	            	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	new File(attachmentPath).mkdirs();
	       	        	       	        		       	        		       	        	
	       	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + attachmentFileExtension);            	           	            	
	            	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	new File(attachmentPath).mkdirs();
	       	        	       	        		       	        		       	        	
	       	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + attachmentFileExtension);            	           	            	
	            	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	           
	           actionForm.setReport(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
                         	
	        }  else {
	        	
	        	actionForm.setReport(Constants.STATUS_SUCCESS);
	          	          	        
		        	return(mapping.findForward("apCheckPaymentRequestEntry"));
		        	
	        }       	   
       	   
         	
/*******************************************************
   -- AP VOU Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbVOU.deleteApVouEntry(actionForm.getCheckPaymentRequestCode(), user.getUserName(), user.getCmpCode());
           	    
           	    // delete attachment
           	    
           	    if (actionForm.getCheckPaymentRequestCode() != null) {
           	    	
					appProperties = MessageResources.getMessageResources("com.ApplicationResources");

		            attachmentPath = appProperties.getMessage("app.attachmentPath") + "ap/Check Payment Request/";
		            attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
					File file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();	

					}			

					file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();	

					}

					file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();	

					}

					file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();	

					}							
           	    	 
           	    }
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }            
       	          	                       
/*******************************************************
   -- Ap VOU Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ap VOU Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null ) {
         	
         	int listSize = actionForm.getApVOUListSize();
                                   
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	ApCheckPaymentRequestEntryList apVOUList = new ApCheckPaymentRequestEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);
	        	    
	        	apVOUList.setDrClassList(this.getDrClassList());
	        	
	        	actionForm.saveApVOUList(apVOUList);
	        	
	        }	        
	        
	        return(mapping.findForward("apCheckPaymentRequestEntry"));
	        
/*******************************************************
   -- Ap VOU Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null ) {
         	
         	for (int i = 0; i<actionForm.getApVOUListSize(); i++) {
           	
           	   ApCheckPaymentRequestEntryList apVOUList = actionForm.getApVOUByIndex(i);
           	   
           	   if (apVOUList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteApVOUList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getApVOUListSize(); i++) {
           	
           	   ApCheckPaymentRequestEntryList apVOUList = actionForm.getApVOUByIndex(i);
           	   
           	   apVOUList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("apCheckPaymentRequestEntry"));
	        
/*******************************************************
   -- Ap VOU VLI Add Lines Action --
*******************************************************/
/*
         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {
         	
         	int listSize = actionForm.getApVLIListSize();
                                   
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
            	ApCheckPaymentRequestLineItemList apVLIList = new ApCheckPaymentRequestLineItemList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
					"0.0", null, null);
	        	    
	        	apVLIList.setLocationList(actionForm.getLocationList());
	        	
	        	actionForm.saveApVLIList(apVLIList);
	        	
	        }	        
	        
	        return(mapping.findForward("apCheckPaymentRequestEntry"));
*/	        
/*******************************************************
   -- Ap VOU Delete Lines Action --
*******************************************************/
/*
         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("ITEMS")) {
         	
         	for (int i = 0; i<actionForm.getApVLIListSize(); i++) {
           	
           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);
           	   
           	   if (apVLIList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteApVLIList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getApVLIListSize(); i++) {
           	
           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);
           	   
           	   apVLIList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("apCheckPaymentRequestEntry"));	        
*/
/*******************************************************
   -- Ap VOU Supplier Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
         	
         	try {	
         		
         		ApModSupplierDetails spldetails = ejbVOU.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());
         		
         		actionForm.setPaymentTerm(spldetails.getSplPytName());
         		actionForm.setWithholdingTax(spldetails.getSplScWtcName());
         		actionForm.setSupplierName(spldetails.getSplName());
         		actionForm.setTaxCode(spldetails.getSplScTcName());
         		/*
         		if(actionForm.getType().equals("PO MATCHED") && actionForm.getType() != null){
         			
         			actionForm.clearApVPOList();
         			
         			String tCode = ejbVOU.getApNoneTc(user.getCmpCode());
         			
         			actionForm.setTaxCode("NONE");
         			
         			ArrayList list = new ArrayList();
         			
         			Iterator  i = null;
         			
         			list = ejbVOU.getApOpenPlBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)i.next();
         				
         				ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
         						actionForm, mdetails.getPlCode(),
								mdetails.getPlPoDocumentNumber(), mdetails.getPlPoReceivingPoNumber(),
								mdetails.getPlLocName(), mdetails.getPlIiName(),
								mdetails.getPlIiDescription(), 
								Common.convertDoubleToStringMoney(mdetails.getPlQuantity(), quantityPrecision),
								mdetails.getPlUomName(), 
								Common.convertDoubleToStringMoney(mdetails.getPlUnitCost(), costPrecisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlAmount(), costPrecisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount1(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount2(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount3(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount4(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlTotalDiscount(), precisionUnit));
         				
         				actionForm.saveApVPOList(apVPOList);
         			}
         			
         		} else {
         			
         			ApModSupplierDetails mdetails = ejbVOU.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());
         			
         			actionForm.clearApVOUList();
         			actionForm.setPaymentTerm(mdetails.getSplPytName());
         			actionForm.setTaxCode(mdetails.getSplScTcName());
         			actionForm.setWithholdingTax(mdetails.getSplScWtcName());
         			actionForm.setBillAmount(null);
         			actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
         			
         			// populate line item list
         			
         			if (actionForm.getType().equalsIgnoreCase("ITEMS") && actionForm.getType() != null) {
         				
         				if(mdetails.getSplLitName() != null && mdetails.getSplLitName().length() > 0) {
         					
         					actionForm.clearApVLIList();
         					
         					ArrayList list = ejbVOU.getInvLitByCstLitName(mdetails.getSplLitName(), user.getCmpCode());
         					
         					if (!list.isEmpty()) {
         						
         						Iterator i = list.iterator();
         						
         						while (i.hasNext()) {
         							
         							InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();
         							
         							ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm, null, 
         									Common.convertShortToString(liDetails.getLiLine()), liDetails.getLiLocName(), 
											liDetails.getLiIiName(), liDetails.getLiIiDescription(), "0", 
											liDetails.getLiUomName(), null, null, null, null, null, null, null, null);
         							
         							// populate location list
         							
         							apVLIList.setLocationList(actionForm.getLocationList());
         							
         							// populate unit field class
         							
         							ArrayList uomList = new ArrayList();
         							uomList = ejbVOU.getInvUomByIiName(apVLIList.getItemName(), user.getCmpCode());
         							
         							apVLIList.clearUnitList();
         							apVLIList.setUnitList(Constants.GLOBAL_BLANK);
         							
         							Iterator j = uomList.iterator();
         							while (j.hasNext()) {
         								
         								InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();
         								
         								apVLIList.setUnitList(mUomDetails.getUomName());
         								
         								if (mUomDetails.isDefault()) {
         									
         									apVLIList.setUnit(mUomDetails.getUomName());      				
         									
         								}      			
         								
         							}
         							
         							// populate unit cost field
         							
         							if (!Common.validateRequired(apVLIList.getItemName()) && !Common.validateRequired(apVLIList.getUnit())) {
         								
         								double unitCost = ejbVOU.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(), user.getCmpCode());
         								apVLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
         								
         							}
         							
         							// populate amount field
         							
         							if(!Common.validateRequired(apVLIList.getQuantity()) && !Common.validateRequired(apVLIList.getUnitCost())) {
         								
         								double amount = Common.convertStringMoneyToDouble(apVLIList.getQuantity(), costPrecisionUnit) * 
										Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), costPrecisionUnit);
         								apVLIList.setAmount(Common.convertDoubleToStringMoney(amount, costPrecisionUnit)); 
         								
         							}
         							
         							actionForm.saveApVLIList(apVLIList);
         							
         						}
         						
         					} 
         					
         				} else {
         					
         					actionForm.setTaxRate(spldetails.getSplScTcRate());
         					actionForm.setTaxType(spldetails.getSplScTcType());
         					
         					actionForm.clearApVLIList();
         					
         					for (int x = 1; x <= journalLineNumber; x++) {
         						
         						ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm,
         								null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
										"0.0", null, null);
         						
         						apVLIList.setLocationList(actionForm.getLocationList());
         						apVLIList.setUnitList(Constants.GLOBAL_BLANK);
         						apVLIList.setUnitList("Select Item First");
         						
         						actionForm.saveApVLIList(apVLIList);
         						
         					}
         					
         				}
         				
         			} else {
         				
         				actionForm.clearApVOUList();	
         				
         			}
         			
         		}*/  
         		actionForm.clearApVOUList();
         	} catch (GlobalNoRecordFoundException ex) {
             	
             	actionForm.clearApVOUList();
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
             
            return(mapping.findForward("apCheckPaymentRequestEntry"));

/*******************************************************
   -- Ap VOU Bill Amount Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {
         	
         	try {
         		         		
                 ArrayList list = ejbVOU.getApDrBySplSupplierCodeAndTcNameAndWtcNameAndVouBillAmount(actionForm.getSupplier(),
                     actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                     Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());                            		
                     
                 actionForm.clearApVOUList();
            		         		            		            		
 	             Iterator i = list.iterator();
		            
		         while (i.hasNext()) {
		            	
			           ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();
			           
				       String debitAmount = null;
				       String creditAmount = null;
				       
				       if (mDrDetails.getDrDebit() == 1) {
				       	
				          debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       } else {
				       	
				          creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       }
				       
				       ArrayList classList = new ArrayList();
				       classList.add(mDrDetails.getDrClass());
				       
				       ApCheckPaymentRequestEntryList apDrList = new ApCheckPaymentRequestEntryList(actionForm,
				          mDrDetails.getDrCode(),
				          Common.convertShortToString(mDrDetails.getDrLine()),
				          mDrDetails.getDrCoaAccountNumber(),
				          debitAmount, creditAmount, mDrDetails.getDrClass(),
				          mDrDetails.getDrCoaAccountDescription());
				          
				          
				      if (mDrDetails.getDrClass().equals(Constants.AP_DR_CLASS_PAYABLE)) {
				      	
				      	  actionForm.setAmountDue(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));
				      	
				      }
				      
				      actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));
				      actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));
				      
				      apDrList.setDrClassList(classList);
				      
				      actionForm.saveApVOUList(apDrList);
			     }	
			        			        			        
		         int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
		        
		         for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	ApCheckPaymentRequestEntryList apVOUList = new ApCheckPaymentRequestEntryList(actionForm,
		        	    null, String.valueOf(x), null, null, null, null, null);
		        	
		        	apVOUList.setDrClassList(this.getDrClassList());
		        	
		        	actionForm.saveApVOUList(apVOUList);
		        	
		         }				         
			                     
             } catch (GlobalNoRecordFoundException ex) {
           	    
           	    actionForm.clearApVOUList();
           	    
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("checkPaymentRequestEntry.error.billAmountNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  	
                        
            return(mapping.findForward("apCheckPaymentRequestEntry"));
            
/*******************************************************
   -- Ap VOU View Attachment Action --
*******************************************************/

         } else if(request.getParameter("viewAttachmentButton1") != null) {
 	    	
 	  	File file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + attachmentFileExtension );
 	  	File filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + attachmentFileExtensionPDF );
 	  	String filename = "";
 	  	
 	  	if (file.exists()){
 	  		filename = file.getName();
 	  	}else if (filePDF.exists()) {
 	  		filename = filePDF.getName();
 	  	}	    	  	
 	  	
			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));
	        
 	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		
 	  		fileExtension = attachmentFileExtension;
	    			
	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;          	    			
	    		
	    	}
 	  	System.out.println(fileExtension + " <== file extension?");
 	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + fileExtension);
 	  	
 	  	byte data[] = new byte[fis.available()];
	    	  
 	  	int ctr = 0;
 	  	int c = 0;
	      	
 	  	while ((c = fis.read()) != -1) {
 		  
	      		data[ctr] = (byte)c;
	      		ctr++;
	      		
 	  	}
 	  	
 	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");
		      	
		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
 	  	} else if (fileExtension == attachmentFileExtensionPDF ){
 	  		System.out.println("pdf");
 	  		
 	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
 	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
 	  	}
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apCheckPaymentRequestEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apCheckPaymentRequestEntryChild"));         
	         	
	        }
	         
	
	
	      } else if(request.getParameter("viewAttachmentButton2") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + attachmentFileExtensionPDF );
	    	  	String filename = "";
	    	  	
	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}	    	  	
	    	  	
	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
		    		
	    	  		fileExtension = attachmentFileExtension;
		    			
	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + fileExtension);
	    	  	
	    	  	byte data[] = new byte[fis.available()];
		    	  
	    	  	int ctr = 0;
	    	  	int c = 0;
		      	
	    	  	while ((c = fis.read()) != -1) {
	    		  
		      		data[ctr] = (byte)c;
		      		ctr++;
		      		
	    	  	}
	    	  	
	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
		      	
	      	
	    	  
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apCheckPaymentRequestEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apCheckPaymentRequestEntryChild"));         
	         	
	        }            
	
	
	      } else if(request.getParameter("viewAttachmentButton3") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + attachmentFileExtensionPDF );
	    	  String filename = "";
	    	  	
	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }	    	  	
	    	  	
	    	  System.out.println(filename + " <== filename?");
	    	  String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		  
	    		  fileExtension = attachmentFileExtension;
		    			
	    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	    	  }
	    	  System.out.println(fileExtension + " <== file extension?");
	    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + fileExtension);
	    	  	
	    	  byte data[] = new byte[fis.available()];
		    	  
	    	  int ctr = 0;
	    	  int c = 0;
		      	
	    	  while ((c = fis.read()) != -1) {
	    		  
	    		  data[ctr] = (byte)c;
	    		  ctr++;
		      		
	    	  }
	    	  	
	    	  if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  } else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  }
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apCheckPaymentRequestEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apCheckPaymentRequestEntryChild"));         
	         	
	        }           
	
	
	      } else if(request.getParameter("viewAttachmentButton4") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + attachmentFileExtensionPDF );
	    	  String filename = "";
	    	  	
	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }	    	  	
	    	  	
	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
		    		
	    	  		fileExtension = attachmentFileExtension;
		    			
	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + fileExtension);
	    	  	
	    	  	byte data[] = new byte[fis.available()];
		    	  
	    	  	int ctr = 0;
	    	  	int c = 0;
		      	
	    	  	while ((c = fis.read()) != -1) {
	    		  
		      		data[ctr] = (byte)c;
		      		ctr++;
		      		
	    	  	}
	    	  	
	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apCheckPaymentRequestEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apCheckPaymentRequestEntryChild"));         
	         	
	        }
	        
/*******************************************************
    -- Ap VOU Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apVLIList[" + 
        actionForm.getRowVLISelected() + "].isItemEntered"))) {
        
    	  ApCheckPaymentRequestLineItemList apVLIList = 
      		actionForm.getApVLIByIndex(actionForm.getRowVLISelected());      	    	
                      
      	try {
          	
      		// populate unit field class
      		
      		ArrayList uomList = new ArrayList();
      		uomList = ejbVOU.getInvUomByIiName(apVLIList.getItemName(), user.getCmpCode());
      		
      		apVLIList.clearUnitList();
      		apVLIList.setUnitList(Constants.GLOBAL_BLANK);
      		
  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {
      			
      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
      			
      			apVLIList.setUnitList(mUomDetails.getUomName());
      			
      			if (mUomDetails.isDefault()) {
      				
      				apVLIList.setUnit(mUomDetails.getUomName());      				
      				
      			}      			
      			
      		}
      		
      		// populate unit cost field
      		
      		if (!Common.validateRequired(apVLIList.getItemName()) && !Common.validateRequired(apVLIList.getUnit())) {
      		
	      		double unitCost = ejbVOU.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(), user.getCmpCode());
	      		apVLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
	      		
      		}
      		
    		apVLIList.setDiscount1("0.0");
    		apVLIList.setDiscount2("0.0");
    		apVLIList.setDiscount3("0.0");
    		apVLIList.setDiscount4("0.0");
         	apVLIList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));
      			      				        	
	        apVLIList.setQuantity(Common.convertDoubleToStringMoney(1d, quantityPrecision));
    		apVLIList.setAmount(apVLIList.getUnitCost());
      		      		      		
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("apCheckPaymentRequestEntry"));
         
/*******************************************************
    -- Ap VOU Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apVLIList[" + 
        actionForm.getRowVLISelected() + "].isUnitEntered"))) {
        
    	  ApCheckPaymentRequestLineItemList apVLIList = 
      		actionForm.getApVLIByIndex(actionForm.getRowVLISelected());
                      
      	try {
      		
            // populate unit cost field
      		
      		if (!Common.validateRequired(apVLIList.getLocation()) && !Common.validateRequired(apVLIList.getItemName())) {
      			      			      		
	      		double unitCost = ejbVOU.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(), user.getCmpCode());
	      		apVLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
	      		
	        }
	        
	        // populate amount field
      		double amount = 0;
      		
	        if(!Common.validateRequired(apVLIList.getQuantity()) && !Common.validateRequired(apVLIList.getUnitCost())) {
	        		        		        
	        	amount = Common.convertStringMoneyToDouble(apVLIList.getQuantity(), costPrecisionUnit) *
					Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), costPrecisionUnit);
	        	
	        	apVLIList.setAmount(Common.convertDoubleToStringMoney(amount, costPrecisionUnit)); 
	        	        
	        }
	        
      		// populate discount field
      		
      		if (!Common.validateRequired(apVLIList.getAmount()) && 
      			(!Common.validateRequired(apVLIList.getDiscount1()) ||
				!Common.validateRequired(apVLIList.getDiscount2()) ||
				!Common.validateRequired(apVLIList.getDiscount3()) ||
				!Common.validateRequired(apVLIList.getDiscount4()))) {
      				      			
      			if (actionForm.getTaxType().equals("INCLUSIVE")) {
      				
      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

      			}
      				      			
      			double discountRate1 = Common.convertStringMoneyToDouble(apVLIList.getDiscount1(), precisionUnit)/100;
      			double discountRate2 = Common.convertStringMoneyToDouble(apVLIList.getDiscount2(), precisionUnit)/100;
      			double discountRate3 = Common.convertStringMoneyToDouble(apVLIList.getDiscount3(), precisionUnit)/100;
      			double discountRate4 = Common.convertStringMoneyToDouble(apVLIList.getDiscount4(), precisionUnit)/100;
      			double totalDiscountAmount = 0d;

      			if (discountRate1 > 0) {
      				
      				double discountAmount = amount * discountRate1;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      				
      			}
      			
      			if (discountRate2 > 0) {
      				
      				double discountAmount = amount * discountRate2;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      				
      			}
      			
      			if (discountRate3 > 0) {
      				
      				double discountAmount = amount * discountRate3;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      				
      			}
      			
      			if (discountRate4 > 0) {
      				
      				double discountAmount = amount * discountRate4;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      				
      			}
      			
      			if (actionForm.getTaxType().equals("INCLUSIVE")) {
      				
      				amount += amount * (actionForm.getTaxRate() / 100);
      				
      			}
      			
      			apVLIList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
      			apVLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));
      			
      		}
      		       	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  	
                     
         return(mapping.findForward("apCheckPaymentRequestEntry"));
         
/*******************************************************
   -- Ap VOU Type Enter Action --
*******************************************************/
/*
         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {      		     	
	       
	        if (actionForm.getType().equals("ITEMS")) {
	        
	            actionForm.setBillAmount(null);
	           	actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApVOUList();
	           	actionForm.clearApVLIList();
	           	
	           	// Populate line when not forwarding
            	
            	for (int x = 1; x <= journalLineNumber; x++) {
            		
            		ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm, 
            				null, new Integer(x).toString(), null, null, null, null, null, 
							null, null, "0.0","0.0", "0.0", "0.0", null, null);	
            		
            		apVLIList.setLocationList(actionForm.getLocationList());
            		
            		apVLIList.setUnitList(Constants.GLOBAL_BLANK);
            		apVLIList.setUnitList("Select Item First");
            		
            		actionForm.saveApVLIList(apVLIList);
            		
            	}
	        } else if (actionForm.getType().equals("PO MATCHED")){
	        	
		        	if(actionForm.getSupplier() == null){
		        		
		        		actionForm.clearApVOUList();
			           	actionForm.clearApVLIList();
			           	actionForm.clearApVPOList();
			           	
		        	} else {
		        		
		        		actionForm.clearApVPOList();
		        		
		        		String tCode = ejbVOU.getApNoneTc(user.getCmpCode());
		         		
		         		actionForm.setTaxCode("NONE");
		         		 
		         		ArrayList list = new ArrayList();
		         		
		         		Iterator  i = null;
		         		
		         		list = ejbVOU.getApOpenPlBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		         		
		         		i = list.iterator();
		         		
		         		while (i.hasNext()) {
		         			
		         			ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)i.next();
		         			
		         			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
		         					actionForm, mdetails.getPlCode(),
									mdetails.getPlPoDocumentNumber(), mdetails.getPlPoReceivingPoNumber(),
									mdetails.getPlLocName(), mdetails.getPlIiName(),
									mdetails.getPlIiDescription(), 
									Common.convertDoubleToStringMoney(mdetails.getPlQuantity(), quantityPrecision),
									mdetails.getPlUomName(), 
									Common.convertDoubleToStringMoney(mdetails.getPlUnitCost(), costPrecisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlAmount(), costPrecisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlTotalDiscount(), precisionUnit));
		         			
		         			actionForm.saveApVPOList(apVPOList);
		         		}
		        		
		        	}
		        	
	        } else {
	        
	        	actionForm.setBillAmount(null);
	           	actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApVOUList();
	           	actionForm.clearApVLIList();
	        
	        }
	     
	        return(mapping.findForward("apCheckPaymentRequestEntry"));
*/	        
/*******************************************************
   -- Ap VOU Tax Code Enter Action --
*******************************************************/
	        
         } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {   
         	
         	try {

         		ApTaxCodeDetails details = ejbVOU.getApTcByTcName(actionForm.getTaxCode(), user.getCmpCode());
         		actionForm.setTaxRate(details.getTcRate());
         		actionForm.setTaxType(details.getTcType());
         		/*
         		if (actionForm.getType().equalsIgnoreCase("ITEMS")) {
         			
         			actionForm.clearApVLIList();
         			
         			for (int x = 1; x <= journalLineNumber; x++) {
         				
         				ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm,
         						null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
								"0.0", null, null);
         				
         				apVLIList.setLocationList(actionForm.getLocationList());
         				apVLIList.setUnitList(Constants.GLOBAL_BLANK);
         				apVLIList.setUnitList("Select Item First");
         				
         				actionForm.saveApVLIList(apVLIList);
         				
         			}
         			
         		}
         		*/
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage"); 
         			
         		}
         		
         	}  	
         	
         	return(mapping.findForward("apCheckPaymentRequestEntry"));

/*******************************************************
         	   -- Ap VOU Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {
        	 
        	 try {
        		 
        		 actionForm.setConversionRate(Common.convertDoubleToStringMoney(
        				 ejbVOU.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));
        		 
        	 } catch (GlobalConversionDateNotExistException ex) {
        		 
        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("checkPaymentRequestEntry.error.conversionDateNotExist"));
        		 
        	 } catch (EJBException ex) {
        		 
        		 if (log.isInfoEnabled()) {
        			 
        			 log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 
        		 }
        		 
        		 return(mapping.findForward("cmnErrorPage"));
        		 
        	 } 
        	 
        	 if (!errors.isEmpty()) {
        		 
        		 saveErrors(request, new ActionMessages(errors));
        		 
        	 }
        	 
        	 return(mapping.findForward("apCheckPaymentRequestEntry"));	              
        	 
/*******************************************************
   -- Ap VOU Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apCheckPaymentRequestEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbVOU.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();
            			
            			actionForm.setCurrencyList(mFcDetails.getFcName());
            			
            			if (mFcDetails.getFcSob() == 1) {
            				
            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());
            				
            			}            			
            		                			
            		}
            		
            	}
            	
            	actionForm.clearPaymentTermList();           	
            	
            	list = ejbVOU.getAdPytAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setPaymentTermList((String)i.next());
            			
            		}            		
            		
            		actionForm.setPaymentTerm("IMMEDIATE");
            		            		
            	}    
            	
            	actionForm.clearTaxCodeList();           	
            	
            	list = ejbVOU.getApTcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setTaxCodeList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            	
            	actionForm.clearWithholdingTaxList();           	
            	
            	list = ejbVOU.getApWtcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setWithholdingTaxList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setWithholdingTaxList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            	if(actionForm.getUseSupplierPulldown()) {
            		
	            	actionForm.clearSupplierList();           	
	            	
	            	list = ejbVOU.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbVOU.getApOpenVbAll(user.getUserDepartment() == null ? "" :user.getUserDepartment(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.clearLocationList();       	
            	
            	list = ejbVOU.getInvLocAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setLocationList((String)i.next());
            			
            		}
            		            		
            	} 
            	            	
            	if (request.getParameter("forwardBatch") != null) {
            		
            		actionForm.setBatchName(request.getParameter("batchName"));
            		actionForm.setType(null);
            		
            	}
            	
            	actionForm.clearApVOUList();
            	actionForm.clearApVLIList(); 
            	actionForm.clearApVPOList();
            	
            	if (request.getParameter("forward") != null || isInitialPrinting) {
            		
            		if (request.getParameter("forward") != null) {
            			            	    
            			actionForm.setCheckPaymentRequestCode(new Integer(request.getParameter("paymentRequestCode")));
            			
            		}
            		
            		ApModVoucherDetails mdetails = ejbVOU.getApVouByVouCode(actionForm.getCheckPaymentRequestCode(), user.getCmpCode());
            		
            		actionForm.setDescription(mdetails.getVouDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getVouDate()));
            		actionForm.setDocumentNumber(mdetails.getVouDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getVouReferenceNumber());
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getVouConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getVouConversionRate(), (short)40));
            		actionForm.setBillAmount(Common.convertDoubleToStringMoney(mdetails.getVouBillAmount(), precisionUnit));
            		actionForm.setAmountDue(Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit));
            		actionForm.setAmountPaid(Common.convertDoubleToStringMoney(mdetails.getVouAmountPaid(), precisionUnit));
            		actionForm.setCheckPaymentRequestVoid(Common.convertByteToBoolean(mdetails.getVouVoid()));
            		actionForm.setApprovalStatus(mdetails.getVouApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getVouReasonForRejection());
            		actionForm.setPosted(mdetails.getVouPosted() == 1 ? "YES" : "NO");
            		actionForm.setGenerated(mdetails.getVouGenerated() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getVouCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getVouDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getVouLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getVouDateLastModified()));            		
            		actionForm.setApprovedRejectedBy(mdetails.getVouApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getVouDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getVouPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getVouDatePosted()));
            		
            		if (!actionForm.getCurrencyList().contains(mdetails.getVouFcName())) {
            			
            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearCurrencyList();
            				
            			}
            			actionForm.setCurrencyList(mdetails.getVouFcName());
            			
            		}            		
            		actionForm.setCurrency(mdetails.getVouFcName());
            		
            		if (!actionForm.getTaxCodeList().contains(mdetails.getVouTcName())) {
            			
            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearTaxCodeList();
            				
            			}
            			actionForm.setTaxCodeList(mdetails.getVouTcName());
            			
            		}            		
            		actionForm.setTaxCode(mdetails.getVouTcName());
            		actionForm.setTaxType(mdetails.getVouTcType());
            		actionForm.setTaxRate(mdetails.getVouTcRate());
            		
            		if (!actionForm.getWithholdingTaxList().contains(mdetails.getVouWtcName())) {
            			
            			if (actionForm.getWithholdingTaxList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearWithholdingTaxList();
            				
            			}
            			actionForm.setWithholdingTaxList(mdetails.getVouWtcName());
            			
            		}            		
            		actionForm.setWithholdingTax(mdetails.getVouWtcName()); 
            		
            		if (!actionForm.getSupplierList().contains(mdetails.getVouSplSupplierCode())) {
            			
            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearSupplierList();
            				
            			}
            			actionForm.setSupplierList(mdetails.getVouSplSupplierCode());
            			
            		}            		
            		actionForm.setSupplier(mdetails.getVouSplSupplierCode());
            		actionForm.setSupplierName(mdetails.getVouSplName());
	                 
            		if (!actionForm.getPaymentTermList().contains(mdetails.getVouPytName())) {
            			
            			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearPaymentTermList();
            				
            			}
            			actionForm.setPaymentTermList(mdetails.getVouPytName());
            			
            		}            		
            		actionForm.setPaymentTerm(mdetails.getVouPytName());
            		
            		actionForm.setPoNumber(mdetails.getVouPoNumber());
            		
           		    if (!actionForm.getBatchNameList().contains(mdetails.getVouVbName())) {
           		    	
           		    	actionForm.setBatchNameList(mdetails.getVouVbName());
           		    	
           		    }           		    
            		actionForm.setBatchName(mdetails.getVouVbName());
            		/*            		
            		if (mdetails.getVouVliList() != null && !mdetails.getVouVliList().isEmpty()) {
            			
            			actionForm.setType("ITEMS");
            		
            		    list = mdetails.getVouVliList();            		
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails)i.next();
            				
            				ApCheckPaymentRequestLineItemList apVliList = new ApCheckPaymentRequestLineItemList(actionForm,
            						mVliDetails.getVliCode(),
									Common.convertShortToString(mVliDetails.getVliLine()),
									mVliDetails.getVliLocName(),
									mVliDetails.getVliIiName(),
									mVliDetails.getVliIiDescription(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliQuantity(), quantityPrecision),
									mVliDetails.getVliUomName(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliUnitCost(), costPrecisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliAmount(), costPrecisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliTotalDiscount(), precisionUnit),
									mVliDetails.getVliMisc());
            				
            				System.out.println("mVliDetails.getVliMisc(): " + mVliDetails.getVliMisc());
            				apVliList.setLocationList(actionForm.getLocationList());
            				
            				apVliList.clearUnitList();
			            	ArrayList unitList = ejbVOU.getInvUomByIiName(mVliDetails.getVliIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {
			            		
			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		apVliList.setUnitList(mUomDetails.getUomName()); 
			            		
			            	}	
            				
            				actionForm.saveApVLIList(apVliList);
            				
            				
            			}	
            			
            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
            			
            			for (int x = list.size() + 1; x <= remainingList; x++) {
            				
            				ApCheckPaymentRequestLineItemList apVLIList = new ApCheckPaymentRequestLineItemList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null, null, null, "0.0",
									"0.0", "0.0", "0.0", null, null);
            				
            				apVLIList.setLocationList(actionForm.getLocationList());
            				
            				apVLIList.setUnitList(Constants.GLOBAL_BLANK);
            				apVLIList.setUnitList("Select Item First");
            				
            				actionForm.saveApVLIList(apVLIList);
            				
            			}	
            			
            			this.setFormProperties(actionForm, user.getCompany());
            			            			
            			if (request.getParameter("child") == null) {
            				
            				return (mapping.findForward("apCheckPaymentRequestEntry"));         
            				
            			} else {
            				
            				return (mapping.findForward("apCheckPaymentRequestEntryChild"));         
            				
            			}
            		
            	  } else if (mdetails.getVouPlList() != null && !mdetails.getVouPlList().isEmpty()) {
            			
            			actionForm.setType("PO MATCHED");
            			
            			actionForm.clearApVPOList();
            			
            		    list = mdetails.getVouPlList();            		
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails)i.next();
            				
            				ApVoucherPurchaseOrderLineList apPlList = new ApVoucherPurchaseOrderLineList(actionForm,
            						mPlDetails.getPlCode(),
									mPlDetails.getPlPoDocumentNumber(),
									mPlDetails.getPlPoReceivingPoNumber(),
									mPlDetails.getPlLocName(),
									mPlDetails.getPlIiName(),
									mPlDetails.getPlIiDescription(),
									Common.convertDoubleToStringMoney(mPlDetails.getPlQuantity(), quantityPrecision),
									mPlDetails.getPlUomName(),
									Common.convertDoubleToStringMoney(mPlDetails.getPlUnitCost(), costPrecisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlAmount(), costPrecisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlTotalDiscount(), precisionUnit));
            				
            				apPlList.setIssueCheckbox(true);
            				
            				actionForm.saveApVPOList(apPlList);
            				
            			}	
            			
            			this.setFormProperties(actionForm, user.getCompany());
            			
            			if (actionForm.getEnableFields()) {
            				
            				//actionForm.clearApVPOList();            				            			
            				list = ejbVOU.getApOpenPlBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                     		
                     		i = list.iterator();
                     		
                     		while (i.hasNext()) {
                     			
                     			ApModPurchaseOrderLineDetails mPolDetails = (ApModPurchaseOrderLineDetails)i.next();
                     			
                     			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
                     					actionForm, mPolDetails.getPlCode(),
                     					mPolDetails.getPlPoDocumentNumber(), mPolDetails.getPlPoReceivingPoNumber(),
            							mPolDetails.getPlLocName(), mPolDetails.getPlIiName(),
            							mPolDetails.getPlIiDescription(), 
            							Common.convertDoubleToStringMoney(mPolDetails.getPlQuantity(), quantityPrecision),
            							mPolDetails.getPlUomName(), 
            							Common.convertDoubleToStringMoney(mPolDetails.getPlUnitCost(), costPrecisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlAmount(), costPrecisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount1(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount2(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount3(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount4(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlTotalDiscount(), precisionUnit));
                     			
                     			actionForm.saveApVPOList(apVPOList);
                     		}
            				
            			}
            			
            			if (request.getParameter("child") == null) {
            				
            				return (mapping.findForward("apCheckPaymentRequestEntry"));         
            				
            			} else {
            				
            				return (mapping.findForward("apCheckPaymentRequestEntryChild"));         
            				
            			}
            		
            		
            		} else {*/
            		
            		    //actionForm.setType("EXPENSES");
            			
            			list = mdetails.getVouDrList();              		
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();
            				
            				String debitAmount = null;
            				String creditAmount = null;
            				
            				if (mDrDetails.getDrDebit() == 1) {
            					
            					debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
            					
            				} else {
            					
            					creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
            					
            				}
            				
            				actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getVouTotalDebit(), precisionUnit));
            				actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getVouTotalCredit(), precisionUnit));
            				
            				ArrayList classList = new ArrayList();
            				classList.add(mDrDetails.getDrClass());
            				
            				ApCheckPaymentRequestEntryList apDrList = new ApCheckPaymentRequestEntryList(actionForm,
            						mDrDetails.getDrCode(),
									Common.convertShortToString(mDrDetails.getDrLine()),
									mDrDetails.getDrCoaAccountNumber(),
									debitAmount, creditAmount, mDrDetails.getDrClass(),
									mDrDetails.getDrCoaAccountDescription());
            				
            				apDrList.setDrClassList(classList);
            				
            				actionForm.saveApVOUList(apDrList);            				
            				
            			}	
            			
            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
            			
            			for (int x = list.size() + 1; x <= remainingList; x++) {
            				
            				ApCheckPaymentRequestEntryList apVOUList = new ApCheckPaymentRequestEntryList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null);
            				
            				apVOUList.setDrClassList(this.getDrClassList());
            				
            				actionForm.saveApVOUList(apVOUList);
            				
            			}
            			
            			this.setFormProperties(actionForm, user.getCompany());
            			
            			if (request.getParameter("child") == null) {
            				
            				return (mapping.findForward("apCheckPaymentRequestEntry"));         
            				
            			} else {
            				
            				return (mapping.findForward("apCheckPaymentRequestEntryChild"));         
            				
            			}
            			
            		}
            		
            	//}
            	
            	// Populate line when not forwarding
            	
            	if (user.getUserApps().contains("OMEGA INVENTORY")){ //&& (actionForm.getType() == null )){ || actionForm.getType().equals("ITEMS"))) {
            	
	            	for (int x = 1; x <= journalLineNumber; x++) {
	            		
	            		ApCheckPaymentRequestLineItemList apVLIList = new ApCheckPaymentRequestLineItemList(actionForm, 
	            				null, new Integer(x).toString(), null, null, null, null, null, 
								null, null, "0.0","0.0", "0.0", "0.0", null, null);	
	            		
	            		apVLIList.setLocationList(actionForm.getLocationList());
	            		
	            		apVLIList.setUnitList(Constants.GLOBAL_BLANK);
	            		apVLIList.setUnitList("Select Item First");
	            		
	            		actionForm.saveApVLIList(apVLIList);
	            		
	            	}
	            	
            	}/* else if (user.getUserApps().contains("OMEGA INVENTORY") && actionForm.getType().equals("PO MATCHED")){
            		
            		for (int x = 1; x <= journalLineNumber; x++) {
            			
            			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(actionForm, 
            					null, null, null, null, null, null, null, null, 
								null, null, "0.0","0.0", "0.0", "0.0", null);	
            			
            			apVPOList.setLocationList(actionForm.getLocationList());
            			
            			apVPOList.setUnitList(Constants.GLOBAL_BLANK);
            			apVPOList.setUnitList("Select Item First");
            			
            			actionForm.saveApVPOList(apVPOList);
            			
            		}
            	}*/
            	
            } catch(GlobalNoRecordFoundException ex) {
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("checkPaymentRequestEntry.error.voucherAlreadyDeleted"));
            	
            } catch(EJBException ex) {
            	
            	if (log.isInfoEnabled()) {
            		
            		log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
            				" session: " + session.getId());
            	}
               
               return(mapping.findForward("cmnErrorPage"));
               
            } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
            	                         	
               if (request.getParameter("saveSubmitButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                   try {
                   	
                   	   ArrayList list = ejbVOU.getAdApprovalNotifiedUsersByVouCode(actionForm.getCheckPaymentRequestCode(), user.getCmpCode());
                   	                      	   
                   	   if (list.isEmpty()) {
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));
                   	   	       
                   	   } else if (list.contains("DOCUMENT POSTED")) {
                   	   	
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,	
                	   	       new ActionMessage("messages.documentPosted"));     
                   	   	                      	   	   
                   	   } else {
                   	   	
                   	   	   Iterator i = list.iterator();
                   	   	   
                   	   	   String APPROVAL_USERS = "";
                   	   	   
                   	   	   while (i.hasNext()) {
                   	   	   	                   	   	   	                   	   	   	                      	   	   	   	
                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
                   	   	       
                   	   	       if (i.hasNext()) {
                   	   	       	
                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";
                   	   	       	
                   	   	       }
                   	   	                          	   	       
                   	   	   	   	                   	   	   	                      	   	   	
                   	   	   }
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
                   	   	
                   	   }
                   	   
                   	   saveMessages(request, messages);
                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                   	
                  	
                   } catch(EJBException ex) {
	     	
		               if (log.isInfoEnabled()) {
		               	
		                  log.info("EJBException caught in ApCheckPaymentRequestEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }
		               
		               return(mapping.findForward("cmnErrorPage"));
		               
		           } 
                   
               } else if ((request.getParameter("saveAsDraftButton") != null || request.getParameter("generateVoucherButton") != null) && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  if(request.getParameter("generateVoucherButton") != null) {
          			
          			messages.add(ActionMessages.GLOBAL_MESSAGE,	new ActionMessage("checkPaymentRequestEntry.prompt.voucherGenerated"));
              		saveMessages(request, messages);
          			
          		}
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);
            /*
            if (actionForm.getType() == null) {
      		   
               if (user.getUserApps().contains("OMEGA INVENTORY")) {
               
                   actionForm.setType("ITEMS");
                   
               } else {
               
                   actionForm.setType("EXPENSES");
               
               }
	          
            }      
			*/
            actionForm.setCheckPaymentRequestCode(null);          
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountPaid(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setConversionRate("1.000000");
            actionForm.setPosted("NO");
            actionForm.setGenerated("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            
                       
            this.setFormProperties(actionForm, user.getCompany());
            return(mapping.findForward("apCheckPaymentRequestEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ApCheckPaymentRequestEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ApCheckPaymentRequestEntryForm actionForm, String adCompany) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO") && !actionForm.getCheckPaymentRequestVoid()) {
				
				if (actionForm.getCheckPaymentRequestCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableVoucherVoid(false);
					actionForm.setShowGenerateVoucherButton(false);
														
				} else if (actionForm.getCheckPaymentRequestCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableVoucherVoid(true);	
					actionForm.setShowGenerateVoucherButton(false);
				    	
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableVoucherVoid(false);
					actionForm.setShowGenerateVoucherButton(false);					
				}
				
			} else if (actionForm.getPosted().equals("YES")) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setEnableVoucherVoid(false);
				actionForm.setShowGenerateVoucherButton(true);
				if(actionForm.getGenerated().equals("YES")){
				actionForm.setShowGenerateVoucherButton(false);
				}
												
			} else if (actionForm.getCheckPaymentRequestVoid()) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableVoucherVoid(false);
				actionForm.setShowGenerateVoucherButton(true);
				if(actionForm.getGenerated().equals("YES")){
				actionForm.setShowGenerateVoucherButton(false);
				}
			}
			
			
			
		} else {			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableVoucherVoid(false);
						
		}	
				
		// view attachment	
										
		if (actionForm.getCheckPaymentRequestCode() != null) {
			
			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/Check Payment Request/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
            
            
 			File file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-1" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton1(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton1(false);	
 				
 			}			
 			
 			file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-2" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton2(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton2(false);	
 				
 			}
 			
 			file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-3" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton3(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton3(false);	
 				
 			}
 			
 			file = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getCheckPaymentRequestCode() + "-4" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton4(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton4(false);	
 				
 			}							
			
		} else {
						
			actionForm.setShowViewAttachmentButton1(false);				
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);
			
		}	
		
		    
	}
	
	private ArrayList getDrClassList() {
		
		ArrayList classList = new ArrayList();
		
     	classList.add(Constants.AP_DR_CLASS_EXPENSE);
     	classList.add(Constants.AP_DR_CLASS_TAX);
     	classList.add(Constants.AP_DR_CLASS_WITHHOLDING_TAX);
     	classList.add(Constants.AP_DR_CLASS_OTHER);
     	
     	return classList;
		
	}
	
}