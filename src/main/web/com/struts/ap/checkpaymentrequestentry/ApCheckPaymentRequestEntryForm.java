package com.struts.ap.checkpaymentrequestentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;


public class ApCheckPaymentRequestEntryForm extends ActionForm implements Serializable {

   private Integer checkPaymentRequestCode = null;
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String supplier = null;
   private ArrayList supplierList = new ArrayList();   
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String paymentTerm = null;
   private ArrayList paymentTermList = new ArrayList();
   private String billAmount = null;   
   private String amountDue = null;
   private String amountPaid = null;
   private boolean checkPaymentRequestVoid = false;
   private String totalDebit = null;
   private String totalCredit = null;
   private String description = null;
   private String taxCode = null;
   private double taxRate = 0d;
   private String taxType = null;   
   private ArrayList taxCodeList = new ArrayList();
   private String withholdingTax = null;
   private ArrayList withholdingTaxList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String conversionDate = null;
   private String conversionRate = null;
   private String approvalStatus = null;
   private String reasonForRejection = null;
   private String posted = null;
   private String generated = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;   
   private String approvedRejectedBy = null;
   private String dateApprovedRejected = null;
   private String postedBy = null;
   private String datePosted = null;
   private String type = null;
   //private ArrayList typeList = new ArrayList();
   private String location = null;
   private ArrayList locationList = new ArrayList();
   private String poNumber = null;
   private String supplierName = null;
   
   private FormFile filename1 = null;
   private FormFile filename2 = null;
   private FormFile filename3 = null;
   private FormFile filename4 = null;
   
   private String functionalCurrency = null;
   
   
   private ArrayList apVOUList = new ArrayList();
   private ArrayList apVLIList = new ArrayList();
   private ArrayList apVPOList = new ArrayList();
   private int rowSelected = 0;
   private int rowVLISelected = 0;
   private int rowVPOSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showBatchName = false;
   private boolean enableVoucherVoid = false;   
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showDeleteButton = false;
   private boolean showSaveButton = false;
   private boolean showGenerateVoucherButton = false;
   private boolean showViewAttachmentButton1 = false;
   private boolean showViewAttachmentButton2 = false;
   private boolean showViewAttachmentButton3 = false;
   private boolean showViewAttachmentButton4 = false;
   private boolean useSupplierPulldown = true;
   
   private String isSupplierEntered  = null;
   private String isBillAmountEntered = null;
   private String isTypeEntered = null;
   private String isTaxCodeEntered = null;
   private String isConversionDateEntered = null;
   
   private String report = null;
   private String attachment = null;
   private String attachmentPDF = null;   
   public String getReport() {
   	
   	   return report;
   	
   }
   
   public void setReport(String report) {
   	
   	   this.report = report;
   	
   }
   
   public String getAttachment() {
   	
   	   return attachment;
   	
   }
   
   public void setAttachment(String attachment) {
   	
   	   this.attachment = attachment;
   	
   }
   
   public String getAttachmentPDF() {
	   	
   	   return attachmentPDF;
   	
   }
   
   public void setAttachmentPDF(String attachmentPDF) {
   	
   	   this.attachmentPDF = attachmentPDF;
   	
   }
       
   public int getRowSelected(){
      return rowSelected;
   }

   public ApCheckPaymentRequestEntryList getApVOUByIndex(int index){
      return((ApCheckPaymentRequestEntryList)apVOUList.get(index));
   }

   public Object[] getApVOUList(){
      return(apVOUList.toArray());
   }

   public int getApVOUListSize(){
      return(apVOUList.size());
   }

   public void saveApVOUList(Object newApVOUList){
      apVOUList.add(newApVOUList);
   }

   public void clearApVOUList(){
      apVOUList.clear();
   }

   public void setRowSelected(Object selectedApVOUList, boolean isEdit){
      this.rowSelected = apVOUList.indexOf(selectedApVOUList);
   }

   public void updateApVOURow(int rowSelected, Object newApVOUList){
      apVOUList.set(rowSelected, newApVOUList);
   }

   public void deleteApVOUList(int rowSelected){
      apVOUList.remove(rowSelected);
   }
   
   public int getRowVLISelected(){
      return rowVLISelected;
   }

   public ApCheckPaymentRequestLineItemList getApVLIByIndex(int index){
      return((ApCheckPaymentRequestLineItemList)apVLIList.get(index));
   }

   public Object[] getApVLIList(){
      return(apVLIList.toArray());
   }

   public int getApVLIListSize(){
      return(apVLIList.size());
   }

   public void saveApVLIList(Object newApVLIList){
      apVLIList.add(newApVLIList);
   }

   public void clearApVLIList(){
      apVLIList.clear();
   }

   public void setRowVLISelected(Object selectedApVLIList, boolean isEdit){
      this.rowVLISelected = apVLIList.indexOf(selectedApVLIList);
   }

   public void updateApVLIRow(int rowVLISelected, Object newApVLIList){
      apVLIList.set(rowVLISelected, newApVLIList);
   }

   public void deleteApVLIList(int rowVLISelected){
      apVLIList.remove(rowVLISelected);
   }
   
   public int getRowVPOSelected(){
   	  return rowVPOSelected;
   }
   /*
   public ApVoucherPurchaseOrderLineList getApVPOByIndex(int index){
	    return((ApVoucherPurchaseOrderLineList)apVPOList.get(index));
   }*/
	
	 public Object[] getApVPOList(){
	    return(apVPOList.toArray());
	 }
	
	 public int getApVPOListSize(){
	    return(apVPOList.size());
	 }
	
	 public void saveApVPOList(Object newApVPOList){
	    apVPOList.add(newApVPOList);
	 }
	
	 public void clearApVPOList(){
	    apVPOList.clear();
	 }
	
	 public void setRowVPOSelected(Object selectedApVPOList, boolean isEdit){
	    this.rowVPOSelected = apVPOList.indexOf(selectedApVPOList);
	 }
	
	 public void updateApVPORow(int rowVPOSelected, Object newApVPOList){
	    apVPOList.set(rowVPOSelected, newApVPOList);
	 }
	
	 public void deleteApVPOList(int rowVPOSelected){
	    apVPOList.remove(rowVPOSelected);
	 }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getCheckPaymentRequestCode() {
   	
   	  return checkPaymentRequestCode;
   	
   }
   
   public void setCheckPaymentRequestCode(Integer checkPaymentRequestCode) {
   	
   	  this.checkPaymentRequestCode = checkPaymentRequestCode;
   	
   }
   
   public String getBatchName() {
   	
   	  return batchName;
   	
   }
   
   public void setBatchName(String batchName) {
   	
   	  this.batchName = batchName;
   	
   }
   
   public ArrayList getBatchNameList() {
   	
   	  return batchNameList;
   	
   }
   
   public void setBatchNameList(String batchName) {
   	
   	  batchNameList.add(batchName);
   	
   }
   
   public void clearBatchNameList() {   	 
   	
   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getSupplier() {   	   	  
   	  	
   	  	return supplier;
   	     	     	
   }
   
   public void setSupplier(String supplier) {
   	
   	  this.supplier = supplier;
   	
   }
            
   public ArrayList getSupplierList() {
   	
   	  return supplierList;
   	
   }
   
   public void setSupplierList(String supplier) {
   	
   	  supplierList.add(supplier);
   	
   }
   public void clearSupplierList() {
   	
   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getDate() {
   	  
   	  return date;
   	   	
   }
   
   public void setDate(String date) {
   	
   	  this.date = date;
   	
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	
   }
   
   public void setDocumentNumber(String documentNumber) {
   	
   	  this.documentNumber = documentNumber;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	   this.referenceNumber = referenceNumber;
   	
   }
   
   public String getPaymentTerm() {
   	
   	  return paymentTerm;
   	
   }
   
   public void setPaymentTerm(String paymentTerm) {
   	
   	  this.paymentTerm = paymentTerm;
   	
   }
   
   public ArrayList getPaymentTermList() {
   	
   	  return paymentTermList;
   	
   }
   
   public void setPaymentTermList(String paymentTerm) {
   	
   	  paymentTermList.add(paymentTerm);
   	
   }
   
   public void clearPaymentTermList() {
   	
   	  paymentTermList.clear();
   	
   }
   
   public String getBillAmount() {
   	
   	  return billAmount;
   	
   }
   
   public void setBillAmount(String billAmount) {
   	
   	  this.billAmount = billAmount;
   	
   }  
   
   public String getAmountDue() {
   	
   	  return amountDue;
   	
   }
   
   public void setAmountDue(String amountDue) {
   	
   	  this.amountDue = amountDue;
   	
   } 
   
   public String getAmountPaid() {
   	
   	  return amountPaid;
   	
   }
   
   public void setAmountPaid(String amountPaid) {
   	
   	  this.amountPaid = amountPaid;
   	
   } 
   
   public boolean getCheckPaymentRequestVoid() {
   	
   	  return checkPaymentRequestVoid;
   	
   }
   
   public void setCheckPaymentRequestVoid(boolean checkPaymentRequestVoid) {
   	
   	  this.checkPaymentRequestVoid = checkPaymentRequestVoid;
   	
   }
   
   public String getTotalDebit() {
   	
   	  return totalDebit;
   	  
   }
   
   public void setTotalDebit(String totalDebit) {
   	
   	  this.totalDebit = totalDebit;
   	  
   }
   
   public String getTotalCredit() {
   	
   	  return totalCredit;
   	  
   }
   
   public void setTotalCredit(String totalCredit) {
   	
   	  this.totalCredit = totalCredit;
   	  
   }

   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public String getTaxCode() {
   	
   	   return taxCode;
   	
   }
   
   public void setTaxCode(String taxCode) {
   	
   	   this.taxCode = taxCode;
   	
   }
   
   public ArrayList getTaxCodeList() {
   	
   	   return taxCodeList;
   	
   }
   
   public void setTaxCodeList(String taxCode) {
   	
   	   taxCodeList.add(taxCode);
   	
   }
   
   public void clearTaxCodeList() {
   	
   	   taxCodeList.clear();
   	   taxCodeList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   
   public String getWithholdingTax() {
   	
   	   return withholdingTax;
   	
   }
   
   public void setWithholdingTax(String withholdingTax) {
   	
   	   this.withholdingTax = withholdingTax;
   	
   }
   
   public ArrayList getWithholdingTaxList() {
   	
   	   return withholdingTaxList;
   	
   }
   
   public void setWithholdingTaxList(String withholdingTax) {
   	
   	   withholdingTaxList.add(withholdingTax);
   	
   }
   
   public void clearWithholdingTaxList() {
   	
   	   withholdingTaxList.clear();
   	   withholdingTaxList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   
   public String getCurrency() {
   	
   	   return currency;
   	
   }
   
   public void setCurrency(String currency) {
   	
   	   this.currency = currency;
   	
   }
   
   public ArrayList getCurrencyList() {
   	
   	   return currencyList;
   	
   }
   
   public void setCurrencyList(String currency) {
   	
   	   currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	   currencyList.clear();
   	
   }
   
   public String getConversionDate() {
   	
   	   return conversionDate;
   	
   }
   
   public void setConversionDate(String conversionDate) {
   	
   	   this.conversionDate = conversionDate;
   	
   }
   
   public String getConversionRate() {
   	 
   	   return conversionRate;
   	
   }
   
   public void setConversionRate(String conversionRate) {
   	
   	   this.conversionRate = conversionRate;
   	
   }   
      
   public String getApprovalStatus() {
   	
   	   return approvalStatus;
   	
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	   this.approvalStatus = approvalStatus;
   	
   }
   
   public String getReasonForRejection() {
   	
   	   return reasonForRejection;
   	      	   
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	   this.reasonForRejection = reasonForRejection;
   	   
   }
   
   public String getPosted() {
   	
   	   return posted;
   	
   }
   
   public void setPosted(String posted) {
   	
   	   this.posted = posted;
   	
   }
   
   public String getGenerated() {
	   	
   	   return generated;
   	
   }
   
   public void setGenerated(String generated) {
   	
   	   this.generated = generated;
   	
   }

   public String getCreatedBy() {
   	
   	   return createdBy;
   	
   }
   
   public void setCreatedBy(String createdBy) {
   	
   	  this.createdBy = createdBy;
   	
   }
   
   public String getDateCreated() {
   	
   	   return dateCreated;
   	
   }
   
   public void setDateCreated(String dateCreated) {
   	
   	   this.dateCreated = dateCreated;
   	
   }   
   
   public String getLastModifiedBy() {
   	
   	  return lastModifiedBy;
   	
   }
   
   public void setLastModifiedBy(String lastModifiedBy) {
   	
   	  this.lastModifiedBy = lastModifiedBy;
   	
   }
   
   public String getDateLastModified() {
   	
   	  return dateLastModified;
   	
   }
   
   public void setDateLastModified(String dateLastModified) {
   	
   	  this.dateLastModified = dateLastModified;
   	
   }

   
   public String getApprovedRejectedBy() {
   	
   	  return approvedRejectedBy;
   	
   }
   
   public void setApprovedRejectedBy(String approvedRejectedBy) {
   	
   	  this.approvedRejectedBy = approvedRejectedBy;
   	
   }
   
   public String getDateApprovedRejected() {
   	
   	  return dateApprovedRejected;
   	
   }
   
   public void setDateApprovedRejected(String dateApprovedRejected) {
   	
   	  this.dateApprovedRejected = dateApprovedRejected;
   	
   }
   
   public String getPostedBy() {
   	
   	  return postedBy;
   	
   }
   
   public void setPostedBy(String postedBy) {
   	
   	  this.postedBy = postedBy;
   	
   }
   
   public String getDatePosted() {
   	
   	  return datePosted;
   	
   }
   
   public void setDatePosted(String datePosted) {
   	
   	  this.datePosted = datePosted;
   	
   }
   
   public FormFile getFilename1() {
   	
   	  return filename1;
   	
   }
   
   public void setFilename1(FormFile filename1) {
   	  
   	  this.filename1 = filename1;
   	
   }
   
   public FormFile getFilename2() {
   	
   	  return filename2;
   	
   }
   
   public void setFilename2(FormFile filename2) {
   
   	  this.filename2 = filename2;
   	
   }
   
   public FormFile getFilename3() {
   	
   	  return filename3;
   	
   }
   
   public void setFilename3(FormFile filename3) {
   	  
   	  this.filename3 = filename3;
   	
   }
   
   public FormFile getFilename4() {
   	
   	  return filename4;
   	
   }
   
   public void setFilename4(FormFile filename4) {
   	  
   	  this.filename4 = filename4;
   	
   }
   
   public String getFunctionalCurrency() {
   	
   	   return functionalCurrency;
   	
   }
   
   public void setFunctionalCurrency(String functionalCurrency) {
   	
   	   this.functionalCurrency = functionalCurrency;
   	
   }
   
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getEnableVoucherVoid() {
   	
   	   return enableVoucherVoid;
   	
   }
   
   public void setEnableVoucherVoid(boolean enableVoucherVoid) {
   	
   	   this.enableVoucherVoid = enableVoucherVoid;
   	
   }
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowDeleteButton() {
   	
   	   return showDeleteButton;
   	
   }
   
   public void setShowDeleteButton(boolean showDeleteButton) {
   	
   	   this.showDeleteButton = showDeleteButton;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
   
   public boolean getShowGenerateVoucherButton() {
	   	
   	   return showGenerateVoucherButton;
   	
   }
   
   public void setShowGenerateVoucherButton(boolean showGenerateVoucherButton) {
   	
   	   this.showGenerateVoucherButton = showGenerateVoucherButton;
   	
   }
      
   public boolean getShowViewAttachmentButton1() {
   	   
   	   return showViewAttachmentButton1;
   	
   }
   
   public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {
   	
   	   this.showViewAttachmentButton1 = showViewAttachmentButton1;
   	
   }
   
   public boolean getShowViewAttachmentButton2() {
   	   
   	   return showViewAttachmentButton2;
   	
   }
   
   public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {
   	
   	   this.showViewAttachmentButton2 = showViewAttachmentButton2;
   	
   }
   
   public boolean getShowViewAttachmentButton3() {
   	   
   	   return showViewAttachmentButton3;
   	
   }
   
   public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {
   	
   	   this.showViewAttachmentButton3 = showViewAttachmentButton3;
   	
   }
   
   public boolean getShowViewAttachmentButton4() {
   	   
   	   return showViewAttachmentButton4;
   	
   }
   
   public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {
   	
   	   this.showViewAttachmentButton4 = showViewAttachmentButton4;
   	
   }
            
   public String getIsSupplierEntered() {
   	
   	   return isSupplierEntered;
   	
   }
   
   public String getIsBillAmountEntered() {
   	
   	  return isBillAmountEntered;
   	
   }
   
   public String getIsTypeEntered() {
   	
   	  return isTypeEntered;
   	
   }
   
   public String getType() {
   	
   	   return type;
   	
   }
   
   public void setType(String type) {
   	
   	
   	   this.type = type;
   	
   }
   /*
   public ArrayList getTypeList() {
   	
   	   return typeList;
   	
   }
   */
   public String getLocation() {
   
	   return location;
		
   }
	
   public void setLocation(String location) {
   
	   this.location = location;
		
   }	
	
   public ArrayList getLocationList() {
   
	   return locationList;
		
   }
	
   public void setLocationList(String location) {
	
	   locationList.add(location);
		
   }
	
   public void clearLocationList() {
		
	   locationList.clear();
	   locationList.add(Constants.GLOBAL_BLANK);
		
   }
   
   public String getPoNumber() {
   	
   	   return poNumber;
   	   
   }
   
   public void setPoNumber(String poNumber) {
   	
   	   this.poNumber = poNumber;
   	   
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }

   public String getSupplierName() {
   	
   	return supplierName;
   	
   }
   
   public void setSupplierName(String supplierName) {
   	
   	this.supplierName = supplierName;
   	
   }
   
	public double getTaxRate() {
		
		return taxRate;
		
	}
	
	public void setTaxRate(double taxRate) {
		
		this.taxRate = taxRate;
		
	}
	
	public String getTaxType() {
		
		return taxType;
		
	}
	
	public void setTaxType(String taxType) {
		
		this.taxType = taxType;
		
	}
	
	public String getIsTaxCodeEntered() {
		
		return isTaxCodeEntered;
		
	}   
   	
   	public String getIsConversionDateEntered(){
   		
   		return isConversionDateEntered;
   		
   	}
   
   public void reset(ActionMapping mapping, HttpServletRequest request){ 
   	
       supplier = Constants.GLOBAL_BLANK;       
       taxCode = Constants.GLOBAL_BLANK;
       withholdingTax = Constants.GLOBAL_BLANK;       
	   date = null;
	   documentNumber = null;
	   referenceNumber = null;
	   poNumber = null;
	   billAmount = null;   
	   amountDue = null;
	   amountPaid = null;
	   checkPaymentRequestVoid = false;
	   description = null;
	   conversionDate = null;
	   conversionRate = null;
	   approvalStatus = null;
	   reasonForRejection = null;
	   posted = null;
	   generated = null;
	   filename1 = null;
	   filename2 = null;
	   filename3 = null;
	   filename4 = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;
	   isSupplierEntered = null;
	   isBillAmountEntered = null;
	   isTypeEntered = null;
	   /*
	   typeList.clear();
	   typeList.add("ITEMS");
	   typeList.add("EXPENSES");
	   typeList.add("PO MATCHED");
	   */
	   location = Constants.GLOBAL_BLANK;
	   supplierName = Constants.GLOBAL_BLANK;
       taxRate = 0d;
       taxType = Constants.GLOBAL_BLANK;
       isTaxCodeEntered = null;
	   isConversionDateEntered = null;
	   attachment = null;
	   /*
	   for (int i=0; i<apVLIList.size(); i++) {
	  	
	  	  ApVoucherLineItemList actionList = (ApVoucherLineItemList)apVLIList.get(i);
	  	  actionList.setIsItemEntered(null);
	  	  actionList.setIsUnitEntered(null);
	  	  
	  	
	  }
	   
	   for(int i=0; i<apVPOList.size(); i++){
	   		
	   	 ApVoucherPurchaseOrderLineList actionList = (ApVoucherPurchaseOrderLineList)apVPOList.get(i);
	   	 actionList.setIsItemEntered(null);
	   	 actionList.setIssueCheckbox(false);
			   
	   }
	   */      
   }
   
   public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
	   //ActionErrors errors = new ActionErrors();

	   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
	   String separator = "$";
	   
	   // Remove first $ character
	   misc = misc.substring(1);
	   
	   // Counter
	   int start = 0;
	   int nextIndex = misc.indexOf(separator, start);
	   int length = nextIndex - start;	
	   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
	   System.out.println("ctr :" + ctr);
	   
	   /*if(y==0)
		   return new ArrayList();*/
	   
	   ArrayList miscList = new ArrayList();

	   for(int x=0; x<ctr; x++) {
		   try {	        	

			   // Date
			   start = nextIndex + 1;
			   nextIndex = misc.indexOf(separator, start);
			   length = nextIndex - start;
			   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			   sdf.setLenient(false);*/
			   String checker = misc.substring(start, start + length);
			   if(checker!=""&& checker != " "){
				   miscList.add(checker);	 
			   }else{
				   miscList.add("null");
			   }
			   
			   //System.out.println(misc.substring(start, start + length));
		   } catch (Exception ex) {
			   ex.printStackTrace();
		   }

	   	   }
	   return miscList;
   }
   

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
         request.getParameter("saveAsDraftButton") != null ||
         request.getParameter("generateVoucherButton") != null ||
         request.getParameter("printButton") != null ||
		 request.getParameter("journalButton") != null) {
      	
      	if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
      	    showBatchName){
            errors.add("batchName",
               new ActionMessage("checkPaymentRequestEntry.error.batchNameRequired"));
         }
      	 
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("supplier",
               new ActionMessage("checkPaymentRequestEntry.error.supplierRequired"));
         }

      	 if(Common.validateRequired(description)) {
             errors.add("description",
                new ActionMessage("checkPaymentRequestEntry.error.descriptionIsRequired"));
          }
      	 
      	 if(Common.validateRequired(poNumber)) {
             errors.add("poNumber",
                new ActionMessage("checkPaymentRequestEntry.error.descriptionIsRequired"));
          }
      	 
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("checkPaymentRequestEntry.error.dateRequired"));
         }
         
		 if(!Common.validateDateFormat(date)){
	            errors.add("date", 
		       new ActionMessage("checkPaymentRequestEntry.error.dateInvalid"));
		 }
		          
		 System.out.println(date.lastIndexOf("/"));
      	 System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 if(year<1900){
      		 errors.add("date", 
      				 new ActionMessage("purchaseOrderEntry.error.dateInvalid"));
      	 }
      	 
         if(Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("paymentTerm",
               new ActionMessage("checkPaymentRequestEntry.error.paymentTermRequired"));
         }
         
         if(type.equals("EXPENSES") && Common.validateRequired(billAmount)){
            errors.add("billAmount",
               new ActionMessage("checkPaymentRequestEntry.error.billAmountRequired"));
         }
         
		 if(type.equals("EXPENSES") && !Common.validateMoneyFormat(billAmount)){
	            errors.add("billAmount", 
		       new ActionMessage("checkPaymentRequestEntry.error.billAmountInvalid"));
		 }
		 
		 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("taxCode",
               new ActionMessage("checkPaymentRequestEntry.error.taxCodeRequired"));
         }
         
         if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("withholdingTax",
               new ActionMessage("checkPaymentRequestEntry.error.withholdingTaxRequired"));
         }
         
         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("checkPaymentRequestEntry.error.currencyRequired"));
         }
         
	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("checkPaymentRequestEntry.error.conversionDateInvalid"));
         }
         
	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("checkPaymentRequestEntry.error.conversionRateInvalid"));
         }
         
         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("checkPaymentRequestEntry.error.conversionDateMustBeNull"));
	 	 }
	 	 
		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){ 
		    errors.add("conversionRate",
		       new ActionMessage("checkPaymentRequestEntry.error.conversionRateMustBeNull"));
		 }
		 
		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("checkPaymentRequestEntry.error.conversionRateOrDateMustBeEntered"));
		 }         
		 		 
		 if (type.equals("EXPENSES")) {
		 	
		 
	         int numberOfLines = 0;
	      	 
	      	 Iterator i = apVOUList.iterator();      	 
	      	 
	      	 while (i.hasNext()) {
	      	 	
	      		ApCheckPaymentRequestEntryList drList = (ApCheckPaymentRequestEntryList)i.next();      	 	 
	      	 	       	 	 
	      	 	 if (Common.validateRequired(drList.getAccount()) &&
	      	 	     Common.validateRequired(drList.getDebitAmount()) &&
	      	 	     Common.validateRequired(drList.getCreditAmount())) continue;
	      	 	     
	      	 	 numberOfLines++;
	      	 
		         if(Common.validateRequired(drList.getAccount())){
		            errors.add("account",
		               new ActionMessage("checkPaymentRequestEntry.error.accountRequired", drList.getLineNumber()));
		         }
			 	 if(!Common.validateMoneyFormat(drList.getDebitAmount())){
		            errors.add("debitAmount",
		               new ActionMessage("checkPaymentRequestEntry.error.debitAmountInvalid", drList.getLineNumber()));
		         }
		         if(!Common.validateMoneyFormat(drList.getCreditAmount())){
		            errors.add("creditAmount",
		               new ActionMessage("checkPaymentRequestEntry.error.creditAmountInvalid", drList.getLineNumber()));
		         }
				 if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
			            errors.add("journalLineAmounts",
			               new ActionMessage("checkPaymentRequestEntry.error.debitCreditAmountRequired", drList.getLineNumber()));
				 }
				 if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
			            errors.add("journalLineAmounts",
			               new ActionMessage("checkPaymentRequestEntry.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
			     }	    		     
			     
			          
		    }
		    
		    if (numberOfLines == 0) {
	         	
	         	errors.add("supplier",
	               new ActionMessage("checkPaymentRequestEntry.error.voucherMustHaveLine"));
	         	
	         }	
		    
		 }/* else if (type.equals("ITEMS")){
         
	         int numOfLines = 0;
	      	 
	      	 Iterator i = apVLIList.iterator();      	 
	      	 
	      	 while (i.hasNext()) {
	      	 	
	      	 	 ApVoucherLineItemList vliList = (ApVoucherLineItemList)i.next();      	 	 
				 
	      	 	 if (Common.validateRequired(vliList.getLocation()) &&
	      	 	     Common.validateRequired(vliList.getItemName()) &&
	      	 	     Common.validateRequired(vliList.getQuantity()) &&
	      	 	     Common.validateRequired(vliList.getUnit()) &&
	      	 	     Common.validateRequired(vliList.getUnitCost())) continue;
	      	 	     
	      	 	 numOfLines++;
	      	 	 
	      	 	try{
	      	 		String separator = "$";
	      	 		String misc = "";
	      		   // Remove first $ character
	      		   misc = vliList.getMisc().substring(1);
	      		   
	      		   // Counter
	      		   int start = 0;
	      		   int nextIndex = misc.indexOf(separator, start);
	      		   int length = nextIndex - start;	
	      		   int counter;
	      		   counter = Integer.parseInt(misc.substring(start, start + length));
	      		   
	      	 		ArrayList miscList = this.getExpiryDateStr(vliList.getMisc(), counter);
	      	 		System.out.println("rilList.getMisc() : " + vliList.getMisc());
	      	 		Iterator mi = miscList.iterator();
	     	 		
	      	 		int ctrError = 0;
	      	 		int ctr = 0;
	      	 		
	      	 		while(mi.hasNext()){
	      	 				String miscStr = (String)mi.next();
	      	 			    
	      	 				if(!Common.validateDateFormat(miscStr)){
	      	 					errors.add("date", 
	      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
	      	 					ctrError++;
	      	 				}
	      	 				
	      	 				System.out.println("miscStr: "+miscStr);
	      	 				if(miscStr=="null"){
	      	 					ctrError++;
	      	 				}
	      	 		}
	      	 		//if ctr==Error => No Date Will Apply
	      	 		//if ctr>error => Invalid Date
	      	 		System.out.println("CTR: "+  miscList.size());
	      	 		System.out.println("ctrError: "+  ctrError);
	      	 		System.out.println("counter: "+  counter);
	      	 			if(ctrError>0 && ctrError!=miscList.size()){
	      	 				errors.add("date", 
	      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
	      	 			}
	      	 			
	      	 		//if error==0 Add Expiry Date
	      	 		
	      	 		
	      	 	}catch(Exception ex){
	  	  	    	ex.printStackTrace();
	  	  	    }
	  	  	    
	      	 
		         if(Common.validateRequired(vliList.getLocation()) || vliList.getLocation().equals(Constants.GLOBAL_BLANK)){
		            errors.add("location",
		            	new ActionMessage("checkPaymentRequestEntry.error.locationRequired", vliList.getLineNumber()));
		         }
		         if(Common.validateRequired(vliList.getItemName()) || vliList.getItemName().equals(Constants.GLOBAL_BLANK)){
		            errors.add("itemName",
		            	new ActionMessage("checkPaymentRequestEntry.error.itemNameRequired", vliList.getLineNumber()));
		         }
		         if(Common.validateRequired(vliList.getQuantity())) {
		         	errors.add("quantity",
		         		new ActionMessage("checkPaymentRequestEntry.error.quantityRequired", vliList.getLineNumber()));
		         }
			 	 if(!Common.validateNumberFormat(vliList.getQuantity())){
		            errors.add("quantity",
		               new ActionMessage("checkPaymentRequestEntry.error.quantityInvalid", vliList.getLineNumber()));
		         }
			 	 if(!Common.validateRequired(vliList.getQuantity()) && Common.convertStringMoneyToDouble(vliList.getQuantity(), (short)3) <= 0) {
		         	errors.add("quantity",
		         		new ActionMessage("checkPaymentRequestEntry.error.negativeOrZeroQuantityNotAllowed", vliList.getLineNumber()));
		         }
			 	 if(Common.validateRequired(vliList.getUnit()) || vliList.getUnit().equals(Constants.GLOBAL_BLANK)){
		            errors.add("unit",
		            	new ActionMessage("checkPaymentRequestEntry.error.unitRequired", vliList.getLineNumber()));
		         }
			 	 if(Common.validateRequired(vliList.getUnitCost())){
		            errors.add("unitCost",
		               new ActionMessage("checkPaymentRequestEntry.error.unitCostRequired", vliList.getLineNumber()));
		         }
			 	 if(!Common.validateMoneyFormat(vliList.getUnitCost())){
		            errors.add("unitCost",
		               new ActionMessage("checkPaymentRequestEntry.error.unitCostInvalid", vliList.getLineNumber()));
		         }	        
	       
		    }
		    
		    if (numOfLines == 0) {
	         	
	         	errors.add("supplier",
	               new ActionMessage("checkPaymentRequestEntry.error.voucherMustHaveItemLine"));
	         	
	        }  	    
		    
		 } else if (type.equals("PO MATCHED")){ // PO MATCHED
		 		
		 		 int numOfLines = 0;
		 	
		      	 Iterator i = apVPOList.iterator();      	 
		      	 
		      	 while (i.hasNext()) {
		      	 	
		      	 	 ApVoucherPurchaseOrderLineList vplList = (ApVoucherPurchaseOrderLineList)i.next();      	 	 
		      	 	 
		      	 	 if(!vplList.getIssueCheckbox()) continue; 
		      	 	 
		      	 	 numOfLines++;
		      	 	 
		      	 	 
			    }  
		      	 
		      	if (numOfLines == 0) {
		      	 	
		      	 	errors.add("issueCheckBox",
		 	               new ActionMessage("checkPaymentRequestEntry.error.voucherMustHaveItemLine"));
		      	 	
		      	 }
			    
			 
		 }*/
	    	    	    	 
      } else if (!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
      	
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {      	   
            errors.add("supplier",
               new ActionMessage("checkPaymentRequestEntry.error.supplierRequired"));
         }      	       	       	 
      	      
      } else if (!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {
      	
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {     	 
            errors.add("supplier",
               new ActionMessage("checkPaymentRequestEntry.error.supplierRequired"));
         }      	        	 
      	
      	 if(Common.validateRequired(billAmount)){
            errors.add("billAmount",
               new ActionMessage("checkPaymentRequestEntry.error.billAmountRequired"));
         }
         
		 if(!Common.validateMoneyFormat(billAmount)){
	            errors.add("billAmount", 
		       new ActionMessage("checkPaymentRequestEntry.error.billAmountInvalid"));
		 }
		 
		 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("taxCode",
               new ActionMessage("checkPaymentRequestEntry.error.taxCodeRequired"));
         }
         
         if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("withholdingTax",
               new ActionMessage("checkPaymentRequestEntry.error.withholdingTaxRequired"));
         }
         
        
       	
      	
      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){
      	
      	if(!Common.validateDateFormat(conversionDate)){
      		
      		errors.add("conversionDate", new ActionMessage("checkPaymentRequestEntry.error.conversionDateInvalid"));
      		
      	}
      	
      }
      
      return(errors);	
   }
}
