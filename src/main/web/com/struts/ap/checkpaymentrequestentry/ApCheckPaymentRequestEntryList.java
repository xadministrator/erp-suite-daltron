package com.struts.ap.checkpaymentrequestentry;

import java.io.Serializable;
import java.util.ArrayList;

public class ApCheckPaymentRequestEntryList implements Serializable {

   private Integer distributionRecordCode = null;
   private String lineNumber = null;
   private String account = null;
   private String debitAmount = null;
   private String creditAmount = null;
   private String drClass = null;
   private ArrayList drClassList = new ArrayList();
   private String accountDescription = null;
   
   private boolean deleteCheckbox = false;
    
   private ApCheckPaymentRequestEntryForm parentBean;
    
   public ApCheckPaymentRequestEntryList(ApCheckPaymentRequestEntryForm parentBean,
      Integer distributionRecordCode,
      String lineNumber,
      String account,
      String debitAmount,
      String creditAmount,
      String drClass,
      String accountDescription){

      this.parentBean = parentBean;
      this.distributionRecordCode = distributionRecordCode;
      this.lineNumber = lineNumber;
      this.account = account;
      this.debitAmount = debitAmount;
      this.creditAmount = creditAmount;
      this.drClass = drClass;
      this.accountDescription = accountDescription;
   }
         
   public Integer getDistributionRecordCode(){
      return(distributionRecordCode);
   }

   public String getLineNumber(){
      return(lineNumber);
   }
   
   public void setLineNumber(String lineNumber) {
   	  this.lineNumber = lineNumber;
   }

   public String getAccount(){
      return(account);
   }
   
   public void setAccount(String account) {
   	  this.account = account;
   }

   public String getDebitAmount(){
      return(debitAmount);
   }
   
   public void setDebitAmount(String debitAmount) {
   	  this.debitAmount = debitAmount;
   }

   public String getCreditAmount(){
      return(creditAmount);
   }
   
   public void setCreditAmount(String creditAmount) {
   	  this.creditAmount = creditAmount;	
   }
   
   public String getDrClass(){
   	  return(drClass);
   }
   
   public void setDrClass(String drClass) {
   	
   	  this.drClass = drClass;
   	
   }
   
   public ArrayList getDrClassList() {
   	  
   	  return drClassList;
   	  
   }
   
   public void setDrClassList(ArrayList drClassList) {
   	
   	  this.drClassList = drClassList;
   	
   }
   
   public String getAccountDescription(){
   	  return(accountDescription);
   }
   
   public void setAccountDescription(String accountDescription) {
   	  this.accountDescription = accountDescription;
   }
   
   public boolean getDeleteCheckbox() {   	
   	  return deleteCheckbox;   	
   }
   
   public void setDeleteCheckbox(boolean deleteCheckbox) {
   	  this.deleteCheckbox = deleteCheckbox;
   }

}