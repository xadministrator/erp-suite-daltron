package com.struts.ap.checkpost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.txn.ApCheckPostController;
import com.ejb.txn.ApCheckPostControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModCheckDetails;

public final class ApCheckPostAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApCheckPostAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApCheckPostForm actionForm = (ApCheckPostForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_CHECK_POST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apCheckPost");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize ApCheckPostController EJB
*******************************************************/

         ApCheckPostControllerHome homeCP = null;
         ApCheckPostController ejbCP = null;

         try {

            homeCP = (ApCheckPostControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApCheckPostControllerEJB", ApCheckPostControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApCheckPostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCP = homeCP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApCheckPostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
  
 /*******************************************************
     Call ApCheckPostController EJB
     getGlFcPrecisionUnit
  *******************************************************/
         
         short precisionUnit = 0;
         boolean enableCheckBatch = false;
         boolean useSupplierPulldown = true;
         
         try { 
         	
            precisionUnit = ejbCP.getGlFcPrecisionUnit(user.getCmpCode());
            enableCheckBatch = Common.convertByteToBoolean(ejbCP.getAdPrfEnableApCheckBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableCheckBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbCP.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApCheckPostAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ap CP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apCheckPost"));
	     
/*******************************************************
   -- Ap CP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apCheckPost"));                         

/*******************************************************
   -- Ap CP Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ap CP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 

/*******************************************************
   -- Ap CP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	        	}	 
	        	
	        	criteria.put("checkVoid", new Byte(Common.convertBooleanToByte(actionForm.getCheckVoid())));
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}       	

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getNumberFrom())) {
	        		
	        		criteria.put("checkNumberFrom", actionForm.getNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("checkNumberTo", actionForm.getNumberTo());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        		        
                
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbCP.getApChkPostableByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ApCheckPostPrintAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	     	}
            
            try {
            	
            	actionForm.clearApCPList();
            	
            	ArrayList list = ejbCP.getApChkPostableByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModCheckDetails mdetails = (ApModCheckDetails)i.next();
      		
            		ApCheckPostList apCPList = new ApCheckPostList(actionForm,
            		    mdetails.getChkCode(),
            		    mdetails.getChkSplSupplierCode(),
            		    mdetails.getChkBaName(),
            		    Common.convertSQLDateToString(mdetails.getChkDate()),
            		    mdetails.getChkNumber(),
            		    mdetails.getChkDocumentNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getChkVoid()),
						mdetails.getChkType());
            		    
            		actionForm.saveApCPList(apCPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("checkPost.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckPostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apCheckPost");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("apCheckPost"));

/*******************************************************
   -- Ap CP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap CP Post Action --
*******************************************************/

         } else if (request.getParameter("postButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
              // get posted journals
                        
		    for(int i=0; i<actionForm.getApCPListSize(); i++) {
		    
		       ApCheckPostList actionList = actionForm.getApCPByIndex(i);
		    	
               if (actionList.getPost()) {
               	
               	     try {
               	     	
               	     	ejbCP.executeApChkPost(actionList.getCheckCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteApCPList(i);
               	     	i--;
               	     
               	     } catch (GlJREffectiveDateNoPeriodExistException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("checkPost.error.effectiveDateNoPeriod", actionList.getDocumentNumber()));
		                  
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("checkPost.error.effectiveDatePeriodClosed", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalJournalNotBalanceException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("checkPost.error.journalNotBalance", actionList.getDocumentNumber()));	
               	  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("checkPost.error.transactionAlreadyPosted", actionList.getDocumentNumber()));
		             
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("checkPost.error.recordAlreadyDeleted", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalTransactionAlreadyVoidPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("checkPost.error.transactionAlreadyVoidPosted", actionList.getDocumentNumber()));
		               
		             
		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		            	
		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("checkPost.error.noNegativeInventoryCostingCOA"));

		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ApCheckPostAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }		    
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apCheckPost");

            }	         
	        
	        try {
            	
            	actionForm.setLineCount(0);
            	actionForm.clearApCPList();
            	
            	ArrayList list = ejbCP.getApChkPostableByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModCheckDetails mdetails = (ApModCheckDetails)i.next();
      		
            		ApCheckPostList apCPList = new ApCheckPostList(actionForm,
            		    mdetails.getChkCode(),
            		    mdetails.getChkSplSupplierCode(),
            		    mdetails.getChkBaName(),
            		    Common.convertSQLDateToString(mdetails.getChkDate()),
            		    mdetails.getChkNumber(),
            		    mdetails.getChkDocumentNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getChkVoid()),
						mdetails.getChkType());
            		    
            		actionForm.saveApCPList(apCPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);               

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckPostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }                
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("apCheckPost"));
	        


/*******************************************************
   -- Ap CP Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apCheckPost");

            }            
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseSupplierPulldown()) {
            	
            		actionForm.clearSupplierCodeList();
            	
	            	list = ejbCP.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierCodeList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
            	}
	            	
            	actionForm.clearCurrencyList();
            	
            	list = ejbCP.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}            	

            	actionForm.clearBankAccountList();
            	
            	list = ejbCP.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbCP.getApOpenCbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	
            	actionForm.clearApCPList();
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApCheckPostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            
			actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
                        
            return(mapping.findForward("apCheckPost"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApCheckPostAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}