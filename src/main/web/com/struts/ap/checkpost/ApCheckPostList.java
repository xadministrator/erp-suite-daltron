package com.struts.ap.checkpost;

import java.io.Serializable;

public class ApCheckPostList implements Serializable {

   private Integer checkCode = null;
   private String supplierCode = null;
   private String bankAccount = null;
   private String date = null;
   private String checkNumber = null;
   private String documentNumber = null;
   private String amount = null;
   private boolean checkVoid = false;
   private String type = null;

   private boolean post = false;
       
   private ApCheckPostForm parentBean;
    
   public ApCheckPostList(ApCheckPostForm parentBean,
	  Integer checkCode,
	  String supplierCode,  
	  String bankAccount,  
	  String date,  
	  String checkNumber,
	  String documentNumber,  
	  String amount,  
	  boolean checkVoid,
	  String type) {

      this.parentBean = parentBean;
      this.checkCode = checkCode;
      this.supplierCode = supplierCode;
      this.bankAccount = bankAccount;
      this.date = date;
      this.checkNumber = checkNumber;
      this.documentNumber = documentNumber;
      this.amount = amount;
      this.checkVoid = checkVoid;
      this.type = type;
      
   }
   
   public Integer getCheckCode() {
   	
   	  return checkCode;
   	  
   }
   
   public String getSupplierCode() {

      return supplierCode;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	 
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public boolean getCheckVoid() {
   	
   	  return checkVoid;
   	  
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }
   
   public boolean getPost() {
   	
   	  return post;
   	
   }
   
   public void setPost(boolean post) {
   	
   	  this.post = post;
   	
   }
      
}