package com.struts.ap.debitmemoentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApDebitMemoEntryController;
import com.ejb.txn.ApDebitMemoEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModVoucherDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.ApVoucherDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApDebitMemoEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApDebitMemoEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApDebitMemoEntryForm actionForm = (ApDebitMemoEntryForm)form;

	     // reset report
	      
	     actionForm.setReport(null);
	     actionForm.setAttachment(null);
	     actionForm.setAttachmentPDF(null);
         
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
         	frParam = Common.getUserPermission(user, Constants.AP_DEBIT_MEMO_ENTRY_ID);
         
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
         
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apDebitMemoEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }
         
/*******************************************************
   Initialize ApDebitMemoEntryController EJB
*******************************************************/

         ApDebitMemoEntryControllerHome homeDM = null;
         ApDebitMemoEntryController ejbDM = null;

         try {
            
            homeDM = (ApDebitMemoEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApDebitMemoEntryControllerEJB", ApDebitMemoEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApDebitMemoEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbDM = homeDM.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ApDebitMemoEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();         
         
/*******************************************************
   Call ApDebitMemoEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         
         short precisionUnit = 0;
         short journalLineNumber = 0;
         boolean enableDebitMemoBatch = false;
         boolean isInitialPrinting = false;
         boolean useSupplierPulldown = true;
         short quantityPrecision = 0;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/Debit Memo/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
         String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
         String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
         try {
         	
            precisionUnit = ejbDM.getGlFcPrecisionUnit(user.getCmpCode());
            if (isAccessedByDevice(request.getHeader("user-agent"))) journalLineNumber=100;
            else journalLineNumber = ejbDM.getAdPrfApJournalLineNumber(user.getCmpCode());     
            enableDebitMemoBatch = Common.convertByteToBoolean(ejbDM.getAdPrfEnableApDebitMemoBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableDebitMemoBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbDM.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            quantityPrecision = ejbDM.getInvGpQuantityPrecisionUnit(user.getCmpCode());
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Ap DM Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("EXPENSES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApVoucherDetails details = new ApVoucherDetails();
           
           details.setVouType("EXPENSES");
           details.setVouCode(actionForm.getDebitMemoCode());           
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
           details.setVouDescription(actionForm.getDescription());           
                      
           if (actionForm.getDebitMemoCode() == null) {
           	
           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());
	                      
           }
           
           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());
           
           ArrayList drList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getApDMListSize(); i++) {
           	
           	   ApDebitMemoEntryList apDMList = actionForm.getApDMByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(apDMList.getAccount()) &&
      	 	       Common.validateRequired(apDMList.getDebitAmount()) &&
      	 	       Common.validateRequired(apDMList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(apDMList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apDMList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apDMList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
           	   
           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apDMList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apDMList.getAccount());
           	   
           	   drList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           

           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.debitMemoNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apDebitMemoEntry"));
           	
           }
           	
           // validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
   	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}

    	   
           try {
           	
           	    Integer debitMemoCode = ejbDM.saveApVouEntry(details, actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           	    actionForm.setDebitMemoCode(debitMemoCode);
           	    
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
                               	
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
                              
           } catch (ApVOUOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
           
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));
                               	
           } catch (GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.accountNumberInvalid"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.journalNotBalance"));           	             

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
 // save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
/*******************************************************
   -- Ap DM Save & Submit Action --
*******************************************************/

        } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("EXPENSES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApVoucherDetails details = new ApVoucherDetails();
           
           details.setVouType("EXPENSES");
           details.setVouCode(actionForm.getDebitMemoCode());           
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
           details.setVouDescription(actionForm.getDescription());           
                      
           if (actionForm.getDebitMemoCode() == null) {
           	
           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());
	                      
           }
           
           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());
           
           ArrayList drList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getApDMListSize(); i++) {
           	
           	   ApDebitMemoEntryList apDMList = actionForm.getApDMByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(apDMList.getAccount()) &&
      	 	       Common.validateRequired(apDMList.getDebitAmount()) &&
      	 	       Common.validateRequired(apDMList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(apDMList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apDMList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apDMList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
           	   
           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apDMList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apDMList.getAccount());
           	   
           	   drList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           

           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.debitMemoNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apDebitMemoEntry"));
           	
           }
           	
// validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
      	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}

           try {
           	
           	    Integer debitMemoCode = ejbDM.saveApVouEntry(details, actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setDebitMemoCode(debitMemoCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
                               	
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
                              
           } catch (ApVOUOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
           
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));                               	           
                  
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.journalNotBalance"));
           	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
               
               errors.add(ActionMessages.GLOBAL_MESSAGE, 
                    new ActionMessage("debitMemoEntry.error.branchAccountNumberInvalid"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
/*******************************************************
	 -- Ap DM Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("EXPENSES")) {

			if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApVoucherDetails details = new ApVoucherDetails();
	           
	           details.setVouType("EXPENSES");
	           details.setVouCode(actionForm.getDebitMemoCode());           
	           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setVouDocumentNumber(actionForm.getDocumentNumber());
	           details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
	           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	           details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
	           details.setVouDescription(actionForm.getDescription());           
	                      
	           if (actionForm.getDebitMemoCode() == null) {
	           	
	           	   details.setVouCreatedBy(user.getUserName());
		           details.setVouDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setVouLastModifiedBy(user.getUserName());
	           details.setVouDateLastModified(new java.util.Date());
	           
	           ArrayList drList = new ArrayList();
	           int lineNumber = 0;  
	           
	           double TOTAL_DEBIT = 0;
	           double TOTAL_CREDIT = 0;         
	                      
	           for (int i = 0; i<actionForm.getApDMListSize(); i++) {
	           	
	           	   ApDebitMemoEntryList apDMList = actionForm.getApDMByIndex(i);           	   
	           	              	   
	           	   if (Common.validateRequired(apDMList.getAccount()) &&
	      	 	       Common.validateRequired(apDMList.getDebitAmount()) &&
	      	 	       Common.validateRequired(apDMList.getCreditAmount())) continue;
	           	   
	           	   byte isDebit = 0;
			       double amount = 0d;
		
			       if (!Common.validateRequired(apDMList.getDebitAmount())) {
			       	
			          isDebit = 1;
			          amount = Common.convertStringMoneyToDouble(apDMList.getDebitAmount(), precisionUnit);
			          
			          TOTAL_DEBIT += amount;
			          
			       } else {
			       	
			          isDebit = 0;
			          amount = Common.convertStringMoneyToDouble(apDMList.getCreditAmount(), precisionUnit); 
			          
			          TOTAL_CREDIT += amount;
			       }
			       
			       lineNumber++;
	
	           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
	           	   
	           	   mdetails.setDrLine((short)(lineNumber));
	           	   mdetails.setDrClass(apDMList.getDrClass());
	           	   mdetails.setDrDebit(isDebit);
	           	   mdetails.setDrAmount(amount);
	           	   mdetails.setDrCoaAccountNumber(apDMList.getAccount());
	           	   
	           	   drList.add(mdetails);
	           	
	           }
	           
	           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
	           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
	           
	
	           if (TOTAL_DEBIT != TOTAL_CREDIT) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.debitMemoNotBalance"));
	                    
	               saveErrors(request, new ActionMessages(errors));
	               return (mapping.findForward("apDebitMemoEntry"));
	           	
	           }
	           	
//	         validate attachment
	           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	      	    	
		   	    	if (actionForm.getFilename1().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename1NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
		  	    	        	    	
		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename1Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename1().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename1SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename2)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename2().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename2NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename2Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename2().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename2SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename3)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename3().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename3NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename3Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename3().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename3SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename4)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename4().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename4NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename4Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename4().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename4SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   	}

	           try {
	           	
	           	    Integer debitMemoCode = ejbDM.saveApVouEntry(details, actionForm.getSupplier(), actionForm.getBatchName(),
	           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	    actionForm.setDebitMemoCode(debitMemoCode);    
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
	                               	
	           } catch (GlobalNoRecordFoundException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
	                              
	           } catch (ApVOUOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
	           
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));	            
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
	           	   
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    new ActionMessage("debitMemoEntry.error.branchAccountNumberInvalid"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
	           
	           if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("apDebitMemoEntry"));
	               
	           }
	           
	           actionForm.setReport(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
	           
	        // save attachment
	           
	           if (!Common.validateRequired(filename1)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension); 
		            	
	       	    		int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename2)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension); 
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension); 

		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension); 
	       	        	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
                         	
        }  else {
        	
        	   actionForm.setReport(Constants.STATUS_SUCCESS);
          	          	        
	           return(mapping.findForward("apDebitMemoEntry"));
	        	
        }

			
/*******************************************************
   -- Ap DLI Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApVoucherDetails details = new ApVoucherDetails();
           
           details.setVouType("ITEMS");
           details.setVouCode(actionForm.getDebitMemoCode());           
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
           details.setVouDescription(actionForm.getDescription());           
                      
           if (actionForm.getDebitMemoCode() == null) {
           	
           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());
	                      
           }
           
           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());
           
           ArrayList dliList = new ArrayList();

           for (int i = 0; i<actionForm.getApDLIListSize(); i++) {
           	
           	   ApDebitMemoLineItemList apDLIList = actionForm.getApDLIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(apDLIList.getLocation()) &&
           	   	   Common.validateRequired(apDLIList.getItemName()) &&
				   Common.validateRequired(apDLIList.getUnit()) &&
				   Common.validateRequired(apDLIList.getUnitCost()) &&
				   Common.validateRequired(apDLIList.getQuantity())) continue;

           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();
           	   
           	   mdetails.setVliLine(Common.convertStringToShort(apDLIList.getLineNumber()));
           	   mdetails.setVliIiName(apDLIList.getItemName());
           	   mdetails.setVliLocName(apDLIList.getLocation());
           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apDLIList.getQuantity(), quantityPrecision));
           	   mdetails.setVliUomName(apDLIList.getUnit());
           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apDLIList.getUnitCost(), precisionUnit));
           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apDLIList.getAmount(), precisionUnit));
           	   
           	   dliList.add(mdetails);
           	
           }
           
// validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
      	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}

    	   
           try {
           	
           	    Integer debitMemoCode = ejbDM.saveApVouVliEntry(details, actionForm.getSupplier(), actionForm.getBatchName(),
           	        dliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           	    actionForm.setDebitMemoCode(debitMemoCode);
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
                               	
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
                              
           } catch (ApVOUOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
           
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));
                               	
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
                    
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.journalNotBalance"));
           	   
           } catch (GlobalInventoryDateException ex) {
              	
	      	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	      			new ActionMessage("debitMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
	      	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
               
               errors.add(ActionMessages.GLOBAL_MESSAGE, 
                    new ActionMessage("debitMemoEntry.error.branchAccountNumberInvalid"));   	      	   

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.noNegativeInventoryCostingCOA"));
           	   
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }

/*******************************************************
-- Ap DLI Save & Submit Mobile Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton2") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ApVoucherDetails details = new ApVoucherDetails();
             
             details.setVouType("ITEMS");
             details.setVouCode(actionForm.getDebitMemoCode());           
             details.setVouDate(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
             details.setVouDocumentNumber(actionForm.getDocumentNumber());
             details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
             details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
             details.setVouDescription(actionForm.getDescription());           
                        
             if (actionForm.getDebitMemoCode() == null) {
             	
            	 details.setVouCreatedBy(user.getUserName());
            	 details.setVouDateCreated(new java.util.Date());
  	                      
             }
             
             details.setVouLastModifiedBy(user.getUserName());
             details.setVouDateLastModified(new java.util.Date());
             
             ArrayList dliList = new ArrayList();

             for (int i = 0; i<actionForm.getApDLIListSize(); i++) {
             	
             	   ApDebitMemoLineItemList apDLIList = actionForm.getApDLIByIndex(i);           	   

             	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();
             	   if (apDLIList.getPartNumber().equals("")) break;
             	   mdetails.setVliLine(Common.convertStringToShort(apDLIList.getLineNumber()));
             	   mdetails.setVliIiName(apDLIList.getPartNumber());
             	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apDLIList.getQuantity(), quantityPrecision));
             	   mdetails.setVliIiPartNumber(apDLIList.getPartNumber());
             	   mdetails.setVliMisc(apDLIList.getMisc());
             	   dliList.add(mdetails);
             	
             }
             
             try {
             	
             	    Integer debitMemoCode = ejbDM.saveApVouVliEntryMobile(details, actionForm.getSupplier(), actionForm.getBatchName(),
             	        dliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
             	
             	    actionForm.setDebitMemoCode(debitMemoCode);
             } catch (GlobalRecordAlreadyDeletedException ex) {
             	
             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
             	
             } catch (GlobalDocumentNumberNotUniqueException ex) {
             	
             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
                                 	
             } catch (GlobalNoRecordFoundException ex) {
             	
             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
                       	
             } catch (GlobalTransactionAlreadyApprovedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
             	
             } catch (GlobalTransactionAlreadyPendingException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
             	
             } catch (GlobalTransactionAlreadyPostedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
             	
             } catch (GlobalTransactionAlreadyVoidException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
                                
             } catch (ApVOUOverapplicationNotAllowedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
             
             } catch (GlobalTransactionAlreadyLockedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));
                                 	
             } catch (GlobalNoApprovalRequesterFoundException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
                      
             } catch (GlobalNoApprovalApproverFoundException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
                      
             } catch (GlobalInvItemLocationNotFoundException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.noItemLocationFound", ex.getMessage()));
             	   
             } catch (GlJREffectiveDateNoPeriodExistException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
                      
             } catch (GlJREffectiveDatePeriodClosedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
                      
             } catch (GlobalJournalNotBalanceException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.journalNotBalance"));
             	   
             } catch (GlobalInventoryDateException ex) {
                	
  	      	   errors.add(ActionMessages.GLOBAL_MESSAGE,
  	      			new ActionMessage("debitMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
  	      	   
             } catch (GlobalBranchAccountNumberInvalidException ex) {
                 
                 errors.add(ActionMessages.GLOBAL_MESSAGE, 
                      new ActionMessage("debitMemoEntry.error.branchAccountNumberInvalid"));   	      	   

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
            	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("debitMemoEntry.error.noNegativeInventoryCostingCOA"));
             	   
             } catch (EJBException ex) {
             	    if (log.isInfoEnabled()) {
                 	
                    log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                       " session: " + session.getId());
                  }
                 
                 return(mapping.findForward("cmnErrorPage"));
             }
        	 
/*******************************************************
   -- Ap DLI Save & Submit Action --
*******************************************************/

        } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApVoucherDetails details = new ApVoucherDetails();
           
           details.setVouType("ITEMS");
           details.setVouCode(actionForm.getDebitMemoCode());           
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
           details.setVouDescription(actionForm.getDescription());           
                      
           if (actionForm.getDebitMemoCode() == null) {
           	
           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());
	                      
           }
           
           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());
           
           ArrayList dliList = new ArrayList();

           for (int i = 0; i<actionForm.getApDLIListSize(); i++) {
           	
           	   ApDebitMemoLineItemList apDLIList = actionForm.getApDLIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(apDLIList.getLocation()) &&
           	   	   Common.validateRequired(apDLIList.getItemName()) &&
				   Common.validateRequired(apDLIList.getUnit()) &&
				   Common.validateRequired(apDLIList.getUnitCost()) &&
				   Common.validateRequired(apDLIList.getQuantity())) continue;

           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();
           	   
           	   mdetails.setVliLine(Common.convertStringToShort(apDLIList.getLineNumber()));
           	   mdetails.setVliIiName(apDLIList.getItemName());
           	   mdetails.setVliLocName(apDLIList.getLocation());
           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apDLIList.getQuantity(), quantityPrecision));
           	   mdetails.setVliUomName(apDLIList.getUnit());
           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apDLIList.getUnitCost(), precisionUnit));
           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apDLIList.getAmount(), precisionUnit));
           	   
           	   dliList.add(mdetails);
           	
           }
           
// validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
      	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("debitMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("debitMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDebitMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}

    	   
           try {
           	
           	    Integer debitMemoCode = ejbDM.saveApVouVliEntry(details, actionForm.getSupplier(), actionForm.getBatchName(),
           	        dliList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setDebitMemoCode(debitMemoCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
                               	
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
                              
           } catch (ApVOUOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
           
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));
                               	
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
                    
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.journalNotBalance"));
           	   
           } catch (GlobalInventoryDateException ex) {
             	
	      	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	      			new ActionMessage("debitMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
	      	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
               
               errors.add(ActionMessages.GLOBAL_MESSAGE, 
                    new ActionMessage("debitMemoEntry.error.branchAccountNumberInvalid"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("debitMemoEntry.error.noNegativeInventoryCostingCOA"));
           	   
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
/*******************************************************
	 -- Ap DLI Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("ITEMS")) {

			if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApVoucherDetails details = new ApVoucherDetails();
	           
	           details.setVouType("ITEMS");
	           details.setVouCode(actionForm.getDebitMemoCode());           
	           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setVouDocumentNumber(actionForm.getDocumentNumber());
	           details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
	           details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
	           details.setVouDescription(actionForm.getDescription());           
	                      
	           if (actionForm.getDebitMemoCode() == null) {
	           	
	           	   details.setVouCreatedBy(user.getUserName());
		           details.setVouDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setVouLastModifiedBy(user.getUserName());
	           details.setVouDateLastModified(new java.util.Date());
	           
	           ArrayList dliList = new ArrayList();

	           for (int i = 0; i<actionForm.getApDLIListSize(); i++) {
	           	
	           	   ApDebitMemoLineItemList apDLIList = actionForm.getApDLIByIndex(i);           	   
	           	              	   
	           	   if (Common.validateRequired(apDLIList.getLocation()) &&
	           	   	   Common.validateRequired(apDLIList.getItemName()) &&
					   Common.validateRequired(apDLIList.getUnit()) &&
					   Common.validateRequired(apDLIList.getUnitCost()) &&
					   Common.validateRequired(apDLIList.getQuantity())) continue;
	
	           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();
	           	   
	           	   mdetails.setVliLine(Common.convertStringToShort(apDLIList.getLineNumber()));
	           	   mdetails.setVliIiName(apDLIList.getItemName());
	           	   mdetails.setVliLocName(apDLIList.getLocation());
	           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apDLIList.getQuantity(), quantityPrecision));
	           	   mdetails.setVliUomName(apDLIList.getUnit());
	           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apDLIList.getUnitCost(), precisionUnit));
	           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apDLIList.getAmount(), precisionUnit));
	           	   
	           	   dliList.add(mdetails);
	           	
	           }
	           
//	         validate attachment
	           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	      	    	
		   	    	if (actionForm.getFilename1().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename1NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
		  	    	        	    	
		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename1Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename1().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename1SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename2)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename2().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename2NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename2Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename2().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename2SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename3)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename3().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename3NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename3Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename3().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename3SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename4)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename4().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename4NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename4Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename4().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename4SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   	}

	    	   
	           try {
	           	
	           	    Integer debitMemoCode = ejbDM.saveApVouVliEntry(details, actionForm.getSupplier(), actionForm.getBatchName(),
	           	        dliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	    actionForm.setDebitMemoCode(debitMemoCode);    
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
	                               	
	           } catch (GlobalNoRecordFoundException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
	                              
	           } catch (ApVOUOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
	           
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));
	                    	           	
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
	                    
	           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("debitMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	       
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalInventoryDateException ex) {
	              	
		      	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		      			new ActionMessage("debitMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
		      	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    new ActionMessage("debitMemoEntry.error.branchAccountNumberInvalid"));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("debitMemoEntry.error.noNegativeInventoryCostingCOA"));
	           	   
	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
	           
	           if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("apDebitMemoEntry"));
	               
	           }
	           
	           actionForm.setReport(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
	           
	        // save attachment
	           
	           if (!Common.validateRequired(filename1)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension); 
		            	
	       	    		int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename2)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension); 
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension); 

		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension); 
	       	        	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
                         	
        }  else {
        	
        	   actionForm.setReport(Constants.STATUS_SUCCESS);
          	          	        
	           return(mapping.findForward("apDebitMemoEntry"));
	        	
        }
			
			
			
			
/*******************************************************
	 -- Ap DLI Journal Action --
*******************************************************/             	
            	
         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("ITEMS")) {

		   if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApVoucherDetails details = new ApVoucherDetails();
	           
	           details.setVouType("ITEMS");
	           details.setVouCode(actionForm.getDebitMemoCode());           
	           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setVouDocumentNumber(actionForm.getDocumentNumber());
	           details.setVouDmVoucherNumber(actionForm.getVoucherNumber());
	           details.setVouVoid(Common.convertBooleanToByte(actionForm.getDebitMemoVoid()));
	           details.setVouDescription(actionForm.getDescription());           
	                      
	           if (actionForm.getDebitMemoCode() == null) {
	           	
	           	   details.setVouCreatedBy(user.getUserName());
		           details.setVouDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setVouLastModifiedBy(user.getUserName());
	           details.setVouDateLastModified(new java.util.Date());
	           
	           ArrayList dliList = new ArrayList();

	           for (int i = 0; i<actionForm.getApDLIListSize(); i++) {
	           	
	           	   ApDebitMemoLineItemList apDLIList = actionForm.getApDLIByIndex(i);           	   
	           	              	   
	           	   if (Common.validateRequired(apDLIList.getLocation()) &&
	           	   	   Common.validateRequired(apDLIList.getItemName()) &&
					   Common.validateRequired(apDLIList.getUnit()) &&
					   Common.validateRequired(apDLIList.getUnitCost()) &&
					   Common.validateRequired(apDLIList.getQuantity())) continue;
	
	           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();
	           	   
	           	   mdetails.setVliLine(Common.convertStringToShort(apDLIList.getLineNumber()));
	           	   mdetails.setVliIiName(apDLIList.getItemName());
	           	   mdetails.setVliLocName(apDLIList.getLocation());
	           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apDLIList.getQuantity(), quantityPrecision));
	           	   mdetails.setVliUomName(apDLIList.getUnit());
	           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apDLIList.getUnitCost(), precisionUnit));
	           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apDLIList.getAmount(), precisionUnit));
	           	   
	           	   dliList.add(mdetails);
	           	
	           }
	           
//	         validate attachment
	           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	      	    	
		   	    	if (actionForm.getFilename1().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename1NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
		  	    	        	    	
		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename1Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename1().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename1SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename2)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename2().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename2NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename2Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename2().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename2SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename3)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename3().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename3NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename3Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename3().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename3SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename4)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename4().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("debitMemoEntry.error.filename4NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename4Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename4().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("debitMemoEntry.error.filename4SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDebitMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   	}

	    	   
	           try {
	           	
	           	    Integer debitMemoCode = ejbDM.saveApVouVliEntry(details, actionForm.getSupplier(), actionForm.getBatchName(),
	           	        dliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	    actionForm.setDebitMemoCode(debitMemoCode);    
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.documentNumberNotUnique"));
	                               	
	           } catch (GlobalNoRecordFoundException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));           	                      	
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyVoid"));
	                              
	           } catch (ApVOUOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
	           
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.transactionAlreadyLocked"));
	                    	           	
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.noApprovalApproverFound"));
	                    
	           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("debitMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	       
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("debitMemoEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalInventoryDateException ex) {
	              	
		      	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		      			new ActionMessage("debitMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
		      	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    new ActionMessage("debitMemoEntry.error.branchAccountNumberInvalid"));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("debitMemoEntry.error.noNegativeInventoryCostingCOA"));
	           	   
	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
	           
	           if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("apDebitMemoEntry"));
	               
	           }
	           
	        // save attachment
	           
	           if (!Common.validateRequired(filename1)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension); 
		            	
	       	    		int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename2)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension); 
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension); 

		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension); 
	       	        	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	           
           }
		   

           
           String path = "/apJournal.do?forward=1" + 
		     "&transactionCode=" + actionForm.getDebitMemoCode() +
		     "&transaction=DEBIT MEMO" + 
		     "&transactionNumber=" + actionForm.getVoucherNumber() + 
		     "&transactionDate=" + actionForm.getDate() +
		     "&transactionEnableFields=" + actionForm.getEnableFields();

            return(new ActionForward(path));
            
/*******************************************************
   -- AP DM Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {
           	
           	    ejbDM.deleteApVouEntry(actionForm.getDebitMemoCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
                return(mapping.findForward("cmnErrorPage"));
            }                 
           
/*******************************************************
   -- Ap DM Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ap DM Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("EXPENSES")) {
         	
         	int listSize = actionForm.getApDMListSize();
                      
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	ApDebitMemoEntryList apDMList = new ApDebitMemoEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);
	        	    
	        	apDMList.setDrClassList(this.getDrClassList());
	        	
	        	actionForm.saveApDMList(apDMList);
	        	
	        }	        
	        
	        return(mapping.findForward("apDebitMemoEntry"));
	        
/*******************************************************
   -- Ap DM Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("EXPENSES")) {

         	for (int i = 0; i<actionForm.getApDMListSize(); i++) {
           	
           	   ApDebitMemoEntryList apDMList = actionForm.getApDMByIndex(i);
           	   
           	   if (apDMList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteApDMList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getApDMListSize(); i++) {
           	
           	   ApDebitMemoEntryList apDMList = actionForm.getApDMByIndex(i);
           	   
           	   apDMList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("apDebitMemoEntry"));
	        
/*******************************************************
   -- Ap DLI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	int listSize = actionForm.getApDLIListSize();
                                   
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	ApDebitMemoLineItemList apDLIList = new ApDebitMemoLineItemList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null, null, null, null, null);
	        	    
	        	apDLIList.setLocationList(actionForm.getLocationList());
	        	
	        	actionForm.saveApDLIList(apDLIList);
	        	
	        }	        
	        
	        return(mapping.findForward("apDebitMemoEntry"));
	        
/*******************************************************
   -- Ap DLI Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	for (int i = 0; i<actionForm.getApDLIListSize(); i++) {
           	
           	   ApDebitMemoLineItemList apDLIList = actionForm.getApDLIByIndex(i);
           	   
           	   if (apDLIList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteApDLIList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getApDLIListSize(); i++) {
           	
           	   ApDebitMemoLineItemList apDLIList = actionForm.getApDLIByIndex(i);
           	   
           	   apDLIList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("apDebitMemoEntry"));	        

/*******************************************************
   -- Ap DM Voucher Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isVoucherEntered"))) {

         	try {
             	
                 ArrayList list = ejbDM.getApDrByVouDmVoucherNumberAndVouBillAmountAndSplSupplierCodeBrCode(
                     actionForm.getVoucherNumber(),
                     Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit),
                     actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());                            		
                     
                 actionForm.clearApDMList();
            		         		            		            		
 	             Iterator i = list.iterator();
		            
		         while (i.hasNext()) {
		            	
			           ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();
			           
				       String debitAmount = null;
				       String creditAmount = null;
				       
				       if (mDrDetails.getDrDebit() == 1) {
				       	
				          debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       } else {
				       	
				          creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       }
				       
				       ArrayList classList = new ArrayList();
				       classList.add(mDrDetails.getDrClass());
				       
				       ApDebitMemoEntryList apDrList = new ApDebitMemoEntryList(actionForm,
				          mDrDetails.getDrCode(),
				          Common.convertShortToString(mDrDetails.getDrLine()),
				          mDrDetails.getDrCoaAccountNumber(),
				          debitAmount, creditAmount, mDrDetails.getDrClass(),
				          mDrDetails.getDrCoaAccountDescription());				          
				          				          
				      apDrList.setDrClassList(classList);
				      
				      actionForm.saveApDMList(apDrList);
			     }	
			        			        			        
		         int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
		        
		         for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	ApDebitMemoEntryList apDMList = new ApDebitMemoEntryList(actionForm,
		        	    null, String.valueOf(x), null, null, null, null, null);
		        	
		        	apDMList.setDrClassList(this.getDrClassList());
		        	
		        	actionForm.saveApDMList(apDMList);
		        	
		         }
		         
		         actionForm.setTotalDebit(actionForm.getAmount());
		         actionForm.setTotalCredit(actionForm.getAmount());
			                     
             } catch (GlobalNoRecordFoundException ex) {
             	
             	actionForm.clearApDMList();
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.noVoucherFound"));
                saveErrors(request, new ActionMessages(errors));
                
             } catch (ApVOUOverapplicationNotAllowedException ex) {
           	   
           	   actionForm.clearApDMList();
           	   
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.overapplicationNotAllowed"));
               saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  	
                        
            return(mapping.findForward("apDebitMemoEntry"));
            
            
/*******************************************************
-- Ap VOU View Attachment Action --
*******************************************************/

         } else if(request.getParameter("viewAttachmentButton1") != null) {
 	    	
 	  	File file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-1" + attachmentFileExtension );
 	  	File filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-1" + attachmentFileExtensionPDF );
 	  	String filename = "";
 	  	
 	  	if (file.exists()){
 	  		filename = file.getName();
 	  	}else if (filePDF.exists()) {
 	  		filename = filePDF.getName();
 	  	}	    	  	
 	  	
			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));
	        
 	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		
 	  		fileExtension = attachmentFileExtension;
	    			
	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;          	    			
	    		
	    	}
 	  	System.out.println(fileExtension + " <== file extension?");
 	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getDebitMemoCode() + "-1" + fileExtension);
 	  	
 	  	byte data[] = new byte[fis.available()];
	    	  
 	  	int ctr = 0;
 	  	int c = 0;
	      	
 	  	while ((c = fis.read()) != -1) {
 		  
	      		data[ctr] = (byte)c;
	      		ctr++;
	      		
 	  	}
 	  	
 	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");
		      	
		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
 	  	} else if (fileExtension == attachmentFileExtensionPDF ){
 	  		System.out.println("pdf");
 	  		
 	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
 	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
 	  	}
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apDebitMemoEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apDebitMemoEntryChild"));         
	         	
	        }
	         
	
	
	      } else if(request.getParameter("viewAttachmentButton2") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-2" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-2" + attachmentFileExtensionPDF );
	    	  	String filename = "";
	    	  	
	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}	    	  	
	    	  	
	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
		    		
	    	  		fileExtension = attachmentFileExtension;
		    			
	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getDebitMemoCode() + "-2" + fileExtension);
	    	  	
	    	  	byte data[] = new byte[fis.available()];
		    	  
	    	  	int ctr = 0;
	    	  	int c = 0;
		      	
	    	  	while ((c = fis.read()) != -1) {
	    		  
		      		data[ctr] = (byte)c;
		      		ctr++;
		      		
	    	  	}
	    	  	
	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
		      	
	      	
	    	  
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apDebitMemoEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apDebitMemoEntryChild"));         
	         	
	        }            
	
	
	      } else if(request.getParameter("viewAttachmentButton3") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-3" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-3" + attachmentFileExtensionPDF );
	    	  String filename = "";
	    	  	
	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }	    	  	
	    	  	
	    	  System.out.println(filename + " <== filename?");
	    	  String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		  
	    		  fileExtension = attachmentFileExtension;
		    			
	    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	    	  }
	    	  System.out.println(fileExtension + " <== file extension?");
	    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getDebitMemoCode() + "-3" + fileExtension);
	    	  	
	    	  byte data[] = new byte[fis.available()];
		    	  
	    	  int ctr = 0;
	    	  int c = 0;
		      	
	    	  while ((c = fis.read()) != -1) {
	    		  
	    		  data[ctr] = (byte)c;
	    		  ctr++;
		      		
	    	  }
	    	  	
	    	  if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  } else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  }
	      	
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apDebitMemoEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apDebitMemoEntryChild"));         
	         	
	        }           
	
	
	      } else if(request.getParameter("viewAttachmentButton4") != null) {
	      	
	    	  File file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-4" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-4" + attachmentFileExtensionPDF );
	    	  String filename = "";
	    	  	
	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }	    	  	
	    	  	
	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));
		        
	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
		    		
	    	  		fileExtension = attachmentFileExtension;
		    			
	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;          	    			
	  	    		
	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getDebitMemoCode() + "-4" + fileExtension);
	    	  	
	    	  	byte data[] = new byte[fis.available()];
		    	  
	    	  	int ctr = 0;
	    	  	int c = 0;
		      	
	    	  	while ((c = fis.read()) != -1) {
	    		  
		      		data[ctr] = (byte)c;
		      		ctr++;
		      		
	    	  	}
	    	  	
	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");
			      	
			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");
	    	  		
	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
	         if (request.getParameter("child") == null) {
			         	
	           return (mapping.findForward("apDebitMemoEntry"));         
	         	
	         } else {
	         	
	           return (mapping.findForward("apDebitMemoEntryChild"));         
	         	
	        }      

/*******************************************************
    -- Ap DLI Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apDLIList[" + 
        actionForm.getRowDLISelected() + "].isItemEntered"))) {

      	ApDebitMemoLineItemList apDLIList = 
      		actionForm.getApDLIByIndex(actionForm.getRowDLISelected());      	    	
                      
      	try {
          	
      		// populate unit field class
      		
      		ArrayList uomList = new ArrayList();
      		uomList = ejbDM.getInvUomByIiName(apDLIList.getItemName(), user.getCmpCode());
      		
      		apDLIList.clearUnitList();
      		apDLIList.setUnitList(Constants.GLOBAL_BLANK);
      		
  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {
      			
      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
      			
      			apDLIList.setUnitList(mUomDetails.getUomName());
      			
      			if (mUomDetails.isDefault()) {
      				
      				apDLIList.setUnit(mUomDetails.getUomName());      				
      				
      			}      			
      			
      		}
      		
      		// populate unit cost field
      		
      		if (!Common.validateRequired(apDLIList.getUnit()) && !Common.validateRequired(actionForm.getVoucherNumber()) &&      		
      			!Common.validateRequired(apDLIList.getLocation())) {
      	      			      		   
  		    	double unitCost = ejbDM.getInvIiUnitCostByIiNameAndUomName(actionForm.getVoucherNumber(), apDLIList.getItemName(), apDLIList.getLocation(), apDLIList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	      		apDLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
    	      		              		    	
      		} else {
      		
      			apDLIList.setUnitCost(Constants.GLOBAL_BLANK);
      		
      		}
      		
      		// populate amount field
      		
      		if(!Common.validateRequired(apDLIList.getQuantity()) && !Common.validateRequired(apDLIList.getUnitCost())) {
	        
	        	double amount = Common.convertStringMoneyToDouble(apDLIList.getQuantity(), precisionUnit) * Common.convertStringMoneyToDouble(apDLIList.getUnitCost(), precisionUnit);
	        	apDLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit)); 

	        }
	        
	      } catch (GlobalNoRecordFoundException ex) {
       	
	       	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                new ActionMessage("debitMemoEntry.error.noVoucherFound"));
	            saveErrors(request, new ActionMessages(errors));
      		      		      		      		
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("apDebitMemoEntry"));
         
/*******************************************************
    -- Ap DLI Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apDLIList[" + 
        actionForm.getRowDLISelected() + "].isUnitEntered"))) {

      	ApDebitMemoLineItemList apDLIList = 
      		actionForm.getApDLIByIndex(actionForm.getRowDLISelected());
                      
      	try {
      	
      	    // populate unit cost field
      		
      		if (!Common.validateRequired(apDLIList.getLocation()) && !Common.validateRequired(apDLIList.getItemName()) &&
      		    !Common.validateRequired(actionForm.getVoucherNumber())) {
      			      			
      		    double unitCost = ejbDM.getInvIiUnitCostByIiNameAndUomName(actionForm.getVoucherNumber(), apDLIList.getItemName(), apDLIList.getLocation(), apDLIList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      		    apDLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
	      		    	            	
	        } else {
	        
	        	apDLIList.setUnitCost(Constants.GLOBAL_BLANK);
	        
	        }
	        
	        // populate amount field
	        
	        if(!Common.validateRequired(apDLIList.getQuantity()) && !Common.validateRequired(apDLIList.getUnitCost())) {
	        
	        	double amount = Common.convertStringMoneyToDouble(apDLIList.getQuantity(), precisionUnit) * Common.convertStringMoneyToDouble(apDLIList.getUnitCost(), precisionUnit);
	        	apDLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit)); 
	        	        
	        }
	        
	      } catch (GlobalNoRecordFoundException ex) {
       	
	       	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                new ActionMessage("debitMemoEntry.error.noVoucherFound"));
	            saveErrors(request, new ActionMessages(errors));
      		       	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  	
                     
         return(mapping.findForward("apDebitMemoEntry"));
         
/*******************************************************
   -- Ap VLI Type Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {      		     	

	        if (actionForm.getType().equals("ITEMS")) {
	        
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApDMList();
	           	actionForm.clearApDLIList();
	           	
	           	// Populate line when not forwarding
            	
            	for (int x = 1; x <= journalLineNumber; x++) {
            		
            		ApDebitMemoLineItemList apDLIList = new ApDebitMemoLineItemList(actionForm, 
            				null, new Integer(x).toString(), null, null, null, null, null, 
							null, null, null, null);	
            		
            		apDLIList.setLocationList(actionForm.getLocationList());
            		
            		apDLIList.setUnitList(Constants.GLOBAL_BLANK);
            		apDLIList.setUnitList("Select Item First");
            		
            		actionForm.saveApDLIList(apDLIList);
            		
            	}
	        	
	        } else {
	        
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApDMList();
	           	actionForm.clearApDLIList();
	        
	        }
	     
	        return(mapping.findForward("apDebitMemoEntry"));            
 
/*******************************************************
   -- Ap DM Supplier Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isSupplierEntered"))) {      		     	

         	actionForm.setSupplierName(ejbDM.getApSplNameBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode()));

         	return(mapping.findForward("apDebitMemoEntry"));
	        

/*******************************************************
   -- Ap DM Load Action --
*******************************************************/

         }

         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apDebitMemoEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	            	
            	
            	if(actionForm.getUseSupplierPulldown()) {
            		
	            	actionForm.clearSupplierList();           	

	            	list = ejbDM.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
            				actionForm.setSupplierList((String) i.next());

	            		}
	            		            		
	            	}
	            	
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbDM.getApOpenVbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.clearLocationList();       	
            	
            	list = ejbDM.getInvLocAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setLocationList((String)i.next());
            			
            		}
            		            		
            	}
            	            	
            	if (request.getParameter("forwardBatch") != null) {
            		
            		actionForm.setBatchName(request.getParameter("batchName"));
            		actionForm.setType(null);
            		
            	}
            	
            	actionForm.clearApDMList();
            	actionForm.clearApDLIList();         	  	
            	
            	if (request.getParameter("forward") != null || isInitialPrinting) {
            		
            		if (request.getParameter("forward") != null) {
            			            	    
            			actionForm.setDebitMemoCode(new Integer(request.getParameter("debitMemoCode")));
            			
            		}
            		
            		ApModVoucherDetails mdetails = ejbDM.getApVouByVouCode(actionForm.getDebitMemoCode(), user.getCmpCode());
            		
            		actionForm.setDescription(mdetails.getVouDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getVouDate()));
            		actionForm.setDocumentNumber(mdetails.getVouDocumentNumber());
            		actionForm.setVoucherNumber(mdetails.getVouDmVoucherNumber());
            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getVouBillAmount(), precisionUnit));
            		actionForm.setDebitMemoVoid(Common.convertByteToBoolean(mdetails.getVouVoid()));
            		actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getVouTotalDebit(), precisionUnit));
            		actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getVouTotalCredit(), precisionUnit));
            		actionForm.setApprovalStatus(mdetails.getVouApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getVouReasonForRejection());
            		actionForm.setPosted(mdetails.getVouPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getVouCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getVouDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getVouLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getVouDateLastModified()));            		
            		actionForm.setApprovedRejectedBy(mdetails.getVouApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getVouDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getVouPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getVouDatePosted()));
            		
            		if (!actionForm.getSupplierList().contains(mdetails.getVouSplSupplierCode())) {
            			
            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearSupplierList();

            			}
            			
            			actionForm.setSupplierList(mdetails.getVouSplSupplierCode());

            		}            		
            		actionForm.setSupplier(mdetails.getVouSplSupplierCode());
            		actionForm.setSupplierName(mdetails.getVouSplName());
            		
            		if (!actionForm.getBatchNameList().contains(mdetails.getVouVbName())) {
           		    	actionForm.setBatchNameList(mdetails.getVouVbName());
           		    }           		    
            		actionForm.setBatchName(mdetails.getVouVbName());
            		
            		if (mdetails.getVouVliList() != null && !mdetails.getVouVliList().isEmpty()) {
            			
            			actionForm.setType("ITEMS");
            			
            		    list = mdetails.getVouVliList();            		
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails)i.next();
            				
            				ApDebitMemoLineItemList apDliList = new ApDebitMemoLineItemList(actionForm,
            						mVliDetails.getVliCode(),
									Common.convertShortToString(mVliDetails.getVliLine()),
									mVliDetails.getVliLocName(),
									mVliDetails.getVliIiName(),
									mVliDetails.getVliIiDescription(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliQuantity(), quantityPrecision),
									mVliDetails.getVliUomName(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliUnitCost(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliQuantity() *  mVliDetails.getVliUnitCost(), precisionUnit),
									mVliDetails.getVliIiPartNumber(),
									mVliDetails.getVliMisc());
            				
            				apDliList.setLocationList(actionForm.getLocationList());
            				
            				apDliList.clearUnitList();
			            	ArrayList unitList = ejbDM.getInvUomByIiName(mVliDetails.getVliIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {
			            		
			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		apDliList.setUnitList(mUomDetails.getUomName()); 
			            		
			            	}	
            				
            				actionForm.saveApDLIList(apDliList);
            				
            				
            			}	
            			
            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
            			
            			for (int x = list.size() + 1; x <= remainingList; x++) {
            				
            				ApDebitMemoLineItemList apDLIList = new ApDebitMemoLineItemList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null, null, null, null, null);
            				
            				apDLIList.setLocationList(actionForm.getLocationList());
            				
            				apDLIList.setUnitList(Constants.GLOBAL_BLANK);
            				apDLIList.setUnitList("Select Item First");
            				
            				actionForm.saveApDLIList(apDLIList);
            				
            			}	
            			
            			this.setFormProperties(actionForm, user.getCompany());
            			
            			if (request.getParameter("child") == null) {
            				
            				return (mapping.findForward("apDebitMemoEntry"));         
            				
            			} else {
            				
            				return (mapping.findForward("apDebitMemoEntryChild"));         
            				
            			}
            			            			            			
            		} else {
            		            			            		
            		    actionForm.setType("EXPENSES");
            		            		
	            		list = mdetails.getVouDrList();              		
	            		         		            		            		
			            i = list.iterator();
			            
			            while (i.hasNext()) {
			            	
				           ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();
				           
					       String debitAmount = null;
					       String creditAmount = null;
					       
					       if (mDrDetails.getDrDebit() == 1) {
					       	
					          debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
					          
					       } else {
					       	
					          creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
					          
					       }
					       
					       ArrayList classList = new ArrayList();
					       classList.add(mDrDetails.getDrClass());
					       
					       ApDebitMemoEntryList apDrList = new ApDebitMemoEntryList(actionForm,
					          mDrDetails.getDrCode(),
					          Common.convertShortToString(mDrDetails.getDrLine()),
					          mDrDetails.getDrCoaAccountNumber(),
					          debitAmount, creditAmount, mDrDetails.getDrClass(),
					          mDrDetails.getDrCoaAccountDescription());
					          
					      apDrList.setDrClassList(classList);
					      
					      actionForm.saveApDMList(apDrList);
					         
					      
				        }	
				        			        			        
				        int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
				        
				        for (int x = list.size() + 1; x <= remainingList; x++) {
				        	
				        	ApDebitMemoEntryList apDMList = new ApDebitMemoEntryList(actionForm,
				        	    null, String.valueOf(x), null, null, null, null, null);
				        	
				        	apDMList.setDrClassList(this.getDrClassList());
				        	
				        	actionForm.saveApDMList(apDMList);
				        	
				         }	
				         
				         this.setFormProperties(actionForm, user.getCompany());
				         
				         if (request.getParameter("child") == null) {
				         	
				         	return (mapping.findForward("apDebitMemoEntry"));         
				         	
				         } else {
				         	
				         	return (mapping.findForward("apDebitMemoEntryChild"));         
				         	
				         }
			         
			         }
            		
            	}
            	
            	// Populate line when not forwarding
            	
            	if (user.getUserApps().contains("OMEGA INVENTORY")) {
            	
	            	for (int x = 1; x <= journalLineNumber; x++) {
	            		
	            		ApDebitMemoLineItemList apDLIList = new ApDebitMemoLineItemList(actionForm, 
	            				null, new Integer(x).toString(), null, null, null, null, null, 
								null, null, null, null);	
	            		
	            		apDLIList.setLocationList(actionForm.getLocationList());
	            		
	            		apDLIList.setUnitList(Constants.GLOBAL_BLANK);
	            		apDLIList.setUnitList("Select Item First");
	            		
	            		actionForm.saveApDLIList(apDLIList);
	            		
	            	}
	            	
            	}
            		           			   	
            } catch(GlobalNoRecordFoundException ex) {
            	            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("debitMemoEntry.error.debitMemoAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveSubmitButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                   try {
                   	
                   	   ArrayList list = ejbDM.getAdApprovalNotifiedUsersByVouCode(actionForm.getDebitMemoCode(), user.getCmpCode());
                   	                      	   
                   	   if (list.isEmpty()) {
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));     
                   	   
                   	   } else if (list.contains("DOCUMENT POSTED")) {	
                   	   	
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,	
                	   	       new ActionMessage("messages.documentPosted"));
                   	   	   
                   	   } else {
                   	   	
                   	   	   Iterator i = list.iterator();
                   	   	   
                   	   	   String APPROVAL_USERS = "";
                   	   	   
                   	   	   while (i.hasNext()) {
                   	   	   	                   	   	   	                   	   	   	                      	   	   	   	
                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
                   	   	       
                   	   	       if (i.hasNext()) {
                   	   	       	
                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";
                   	   	       	
                   	   	       }
                   	   	                          	   	       
                   	   	   	   	                   	   	   	                      	   	   	
                   	   	   }
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
                   	   	
                   	   }
                   	   
                   	   saveMessages(request, messages);
                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                   	
                  	
                   } catch(EJBException ex) {
	     	
		               if (log.isInfoEnabled()) {
		               	
		                  log.info("EJBException caught in ApDebitMemoEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }
		               
		               return(mapping.findForward("cmnErrorPage"));
		               
		           } 
                   
               } else if (request.getParameter("saveAsDraftButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);
            
            if (actionForm.getType() == null) {
      		      	
	           if (user.getUserApps().contains("OMEGA INVENTORY")) {
               
                   actionForm.setType("ITEMS");
                   
               } else {
               
                   actionForm.setType("EXPENSES");
               
               }
	          
            }           

            actionForm.setDebitMemoCode(null);          
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            
            this.setFormProperties(actionForm, user.getCompany());
            
/*******************************************************
-- Check if the request was sent by a mobile device --
*******************************************************/
                        
            if (isAccessedByDevice(request.getHeader("user-agent"))) {
            	System.out.println("Mobile Device Detected.");
                return(mapping.findForward("apDebitMemoEntry2"));
            }
            else return(mapping.findForward("apDebitMemoEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ApDebitMemoEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ApDebitMemoEntryForm actionForm, String adCompany) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO") && !actionForm.getDebitMemoVoid()) {
				
				if (actionForm.getDebitMemoCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableDebitMemoVoid(false);
					
				} else if (actionForm.getDebitMemoCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableDebitMemoVoid(true);
				    
				    	
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableDebitMemoVoid(false);
					
				}
				
			} else if (actionForm.getPosted().equals("YES")) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setEnableDebitMemoVoid(false);
				
			} else if (actionForm.getDebitMemoVoid()) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableDebitMemoVoid(false);
				
			}
			
			
			
		} else {
			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setEnableDebitMemoVoid(false);
			
		}
		
//		 view attachment	
		if (actionForm.getDebitMemoCode() != null) {
			
			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/Debit Memo/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
            
            
 			File file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-1" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton1(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton1(false);	
 				
 			}			
 			
 			file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-2" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton2(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton2(false);	
 				
 			}
 			
 			file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-3" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton3(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton3(false);	
 				
 			}
 			
 			file = new File(attachmentPath + actionForm.getDebitMemoCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getDebitMemoCode() + "-4" + attachmentFileExtensionPDF);
 			
 			if (file.exists() || filePDF.exists()) {
 				
 				actionForm.setShowViewAttachmentButton4(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton4(false);	
 				
 			}							
			
		} else {
					
			actionForm.setShowViewAttachmentButton1(false);				
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);
			
		}
				
		    
	}
	
	private ArrayList getDrClassList() {
		
		ArrayList classList = new ArrayList();
		
     	classList.add(Constants.AP_DR_CLASS_EXPENSE);
     	classList.add(Constants.AP_DR_CLASS_TAX);
     	classList.add(Constants.AP_DR_CLASS_WITHHOLDING_TAX);
     	classList.add(Constants.AP_DR_CLASS_OTHER);
     	
     	return classList;
		
	}
	
	private Boolean isAccessedByDevice(String user_agent) {
		if (user_agent!=null) {
			if (user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows CE)")||
				user_agent.equals("Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)")||
				user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)")
			) return true;
			return false;
		}
		else return false;
	}
	
}