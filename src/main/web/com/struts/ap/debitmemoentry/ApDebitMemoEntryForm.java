package com.struts.ap.debitmemoentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;


public class ApDebitMemoEntryForm extends ActionForm implements Serializable {
	
	private Integer debitMemoCode = null;
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList();
	private String supplier = null;
	private ArrayList supplierList = new ArrayList();   
	private String date = null;
	private String documentNumber = null;
	private String voucherNumber = null;
	private String amount = null;   
	private boolean debitMemoVoid = false;
	private String totalDebit = null;
	private String totalCredit = null;
	private String description = null;
	private String approvalStatus = null;
	private String reasonForRejection = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String supplierName = null;

	private ArrayList apDMList = new ArrayList();
	private ArrayList apDLIList = new ArrayList();
	private int rowSelected = 0;
	private int rowDLISelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showBatchName = false;
	private boolean enableDebitMemoVoid = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	private boolean useSupplierPulldown = true;
	
	private String isVoucherEntered = null;
	private String isTypeEntered = null;
	private String isSupplierEntered = null;

	private String report = null;
	
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;
	
	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;
	
	private String attachment = null;
	private String attachmentPDF = null;
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {

		this.report = report;

	}

	public String getAttachmentPDF() {

		return attachmentPDF;

	}

	public void setAttachmentPDF(String attachmentPDF) {

		this.attachmentPDF = attachmentPDF;

	}

	public String getAttachment() {

		return attachment;

	}

	public void setAttachment(String attachment) {

		this.attachment = attachment;

	}

	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public ApDebitMemoEntryList getApDMByIndex(int index) {
		
		return((ApDebitMemoEntryList)apDMList.get(index));
		
	}
	
	public Object[] getApDMList() {
		
		return(apDMList.toArray());
	}
	
	public int getApDMListSize() {
		
		return(apDMList.size());
		
	}
	
	public void saveApDMList(Object newApDMList) {
		
		apDMList.add(newApDMList);
		
	}
	
	public void clearApDMList() {
		
		apDMList.clear();
		
	}
	
	public void setRowSelected(Object selectedApDMList, boolean isEdit) {
		
		this.rowSelected = apDMList.indexOf(selectedApDMList);
		
	}
	
	public void updateApDMRow(int rowSelected, Object newApDMList) {
		
		apDMList.set(rowSelected, newApDMList);
		
	}
	
	public void deleteApDMList(int rowSelected) {
		
		apDMList.remove(rowSelected);
		
	}
	
	public int getRowDLISelected() {
		
		return rowDLISelected;
		
	}
	
	public ApDebitMemoLineItemList getApDLIByIndex(int index) {
		
		return((ApDebitMemoLineItemList)apDLIList.get(index));
	}
	
	public Object[] getApDLIList() {
		
		return(apDLIList.toArray());
		
	}
	
	public int getApDLIListSize() {
		
		return(apDLIList.size());
		
	}
	
	public void saveApDLIList(Object newApDLIList){
		
		apDLIList.add(newApDLIList);
		
	}
	
	public void clearApDLIList(){
		
		apDLIList.clear();
		
	}
	
	public void setRowDLISelected(Object selectedApDLIList, boolean isEdit){
		
		this.rowDLISelected = apDLIList.indexOf(selectedApDLIList);
		
	}
	
	public void updateApDLIRow(int rowDLISelected, Object newApDLIList){
		
		apDLIList.set(rowDLISelected, newApDLIList);
		
	}
	
	public void deleteApDLIList(int rowDLISelected){
		
		apDLIList.remove(rowDLISelected);
		
	}
	
	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}
	
	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}
	
	public String getUserPermission(){
		return(userPermission);
	}
	
	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
	
	public Integer getDebitMemoCode() {
		
		return debitMemoCode;
		
	}
	
	public void setDebitMemoCode(Integer debitMemoCode) {
		
		this.debitMemoCode = debitMemoCode;
		
	}
	
	public String getBatchName() {
		
		return batchName;
		
	}
	
	public void setBatchName(String batchName) {
		
		this.batchName = batchName;
		
	}
	
	public ArrayList getBatchNameList() {
		
		return batchNameList;
		
	}
	
	public void setBatchNameList(String batchName) {
		
		batchNameList.add(batchName);
		
	}
	
	public void clearBatchNameList() {   	 
		
		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getSupplier() {
		
		return supplier;
		
	}
	
	public void setSupplier(String supplier) {
		
		this.supplier = supplier;
		
	}
	
	public ArrayList getSupplierList() {
		
		return supplierList;
		
	}
	
	public void setSupplierList(String supplier) {
		
		supplierList.add(supplier);
		
	}
	
	public void clearSupplierList() {
		
		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	public String getVoucherNumber() {
		
		return voucherNumber;
		
	}
	
	public void setVoucherNumber(String voucherNumber) {
		
		this.voucherNumber = voucherNumber;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}  
	
	public boolean getDebitMemoVoid() {
		
		return debitMemoVoid;
		
	}
	
	public void setDebitMemoVoid(boolean debitMemoVoid) {
		
		this.debitMemoVoid = debitMemoVoid;
		
	}
	
	public String getTotalDebit() {
	   	
	   	  return totalDebit;
	   
	   }
	   
	public void setTotalDebit(String totalDebit) {
   	
   	  	this.totalDebit = totalDebit;
   	  
	}
   
	public String getTotalCredit() {
	   	
	 	return totalCredit;
	   
	}
	
	public void setTotalCredit(String totalCredit) {
   	
   	  	this.totalCredit = totalCredit;
   	  
	}
   
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	public String getReasonForRejection() {
		
		return reasonForRejection;
		
	}
	
	public void setReasonForRejection(String reasonForRejection) {
		
		this.reasonForRejection = reasonForRejection;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}
	
	public String getDateCreated() {
		
		return dateCreated;
		
	}
	
	public void setDateCreated(String dateCreated) {
		
		this.dateCreated = dateCreated;
		
	}   
	
	public String getLastModifiedBy() {
		
		return lastModifiedBy;
		
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		
		this.lastModifiedBy = lastModifiedBy;
		
	}
	
	public String getDateLastModified() {
		
		return dateLastModified;
		
	}
	
	public void setDateLastModified(String dateLastModified) {
		
		this.dateLastModified = dateLastModified;
		
	}
	
	
	public String getApprovedRejectedBy() {
		
		return approvedRejectedBy;
		
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
		
	}
	
	public String getDateApprovedRejected() {
		
		return dateApprovedRejected;
		
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		
		this.dateApprovedRejected = dateApprovedRejected;
		
	}
	
	public String getPostedBy() {
		
		return postedBy;
		
	}
	
	public void setPostedBy(String postedBy) {
		
		this.postedBy = postedBy;
		
	}
	
	public String getDatePosted() {
		
		return datePosted;
		
	}
	
	public void setDatePosted(String datePosted) {
		
		this.datePosted = datePosted;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public boolean getShowBatchName() {
		
		return showBatchName;
		
	}
	
	public void setShowBatchName(boolean showBatchName) {
		
		this.showBatchName = showBatchName;
		
	}
	
	public boolean getEnableDebitMemoVoid() {
		
		return enableDebitMemoVoid;
		
	}
	
	public void setEnableDebitMemoVoid(boolean enableDebitMemoVoid) {
		
		this.enableDebitMemoVoid = enableDebitMemoVoid;
		
	}
	
	public boolean getShowAddLinesButton() {
		
		return showAddLinesButton;
		
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		
		this.showAddLinesButton = showAddLinesButton;
		
	}
	
	public boolean getShowDeleteLinesButton() {
		
		return showDeleteLinesButton;
		
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		
		this.showDeleteLinesButton = showDeleteLinesButton;
		
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getShowDeleteButton() {
	
		return showDeleteButton;
	
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
	
		this.showDeleteButton = showDeleteButton;
		
	}
		
	public String getIsVoucherEntered() {
		
		return isVoucherEntered;
		
	}
	
	public String getIsTypeEntered() {
		
		return isTypeEntered;
		
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}	
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(String location) {
		
		locationList.add(location);
		
	}
	
	public void clearLocationList() {
		
		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public boolean getUseSupplierPulldown() {
		
		return useSupplierPulldown;
		
	}
	
	public void setUseSupplierPulldown(boolean useSupplierPulldown) {
		
		this.useSupplierPulldown = useSupplierPulldown;
		
	}
	
	public String getSupplierName() {
		
		return supplierName;
		
	}
	
	public void setSupplierName(String supplierName) {
		
		this.supplierName = supplierName;
		
	}

	public String getIsSupplierEntered() {
		
		return isSupplierEntered;
		
	}

	public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {
		
		return filename3;
		
	}
	
	public void setFilename3(FormFile filename3) {
		
		this.filename3 = filename3;
		
	}
	
	public FormFile getFilename4() {
		
		return filename4;
		
	}
	
	public void setFilename4(FormFile filename4) {
		
		this.filename4 = filename4;
		
	}
	
	public boolean getShowViewAttachmentButton1() {
		
		return showViewAttachmentButton1;
		
	}
	
	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {
		
		this.showViewAttachmentButton1 = showViewAttachmentButton1;
		
	}
	
	public boolean getShowViewAttachmentButton2() {
		
		return showViewAttachmentButton2;
		
	}
	
	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {
		
		this.showViewAttachmentButton2 = showViewAttachmentButton2;
		
	}
	
	public boolean getShowViewAttachmentButton3() {
		
		return showViewAttachmentButton3;
		
	}
	
	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {
		
		this.showViewAttachmentButton3 = showViewAttachmentButton3;
		
	}
	
	public boolean getShowViewAttachmentButton4() {
		
		return showViewAttachmentButton4;
		
	}
	
	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {
		
		this.showViewAttachmentButton4 = showViewAttachmentButton4;
		
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request){      
		
		supplier = Constants.GLOBAL_BLANK;
		date = null;
		documentNumber = null;
		voucherNumber = null;
		amount = null;   
		debitMemoVoid = false;
		description = null;
		approvalStatus = null;
		reasonForRejection = null;
		posted = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;   
		approvedRejectedBy = null;
		dateApprovedRejected = null;
		postedBy = null;
		datePosted = null;
		isVoucherEntered = null;
		isTypeEntered = null;
		typeList.clear();
		typeList.add("ITEMS");
		typeList.add("EXPENSES");
		location = Constants.GLOBAL_BLANK;
		supplierName = Constants.GLOBAL_BLANK;
		isSupplierEntered = null;
		filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
		attachment = null;
		attachmentPDF = null;
		for (int i=0; i<apDLIList.size(); i++) {
			
			ApDebitMemoLineItemList actionList = (ApDebitMemoLineItemList)apDLIList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);
			
		}
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("saveSubmitButton") != null || 
		   request.getParameter("saveAsDraftButton") != null || 
		   request.getParameter("printButton") != null ||
		   request.getParameter("journalButton") != null) {
			
			if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showBatchName){
				errors.add("batchName",
						new ActionMessage("debitMemoEntry.error.batchNameRequired"));
			}
			
			if(Common.validateRequired(description)){
				errors.add("description",
						new ActionMessage("debitMemoEntry.error.descriptionIsRequired"));
			}
			
			if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("supplier",
						new ActionMessage("debitMemoEntry.error.supplierRequired"));
			}
			
			if(Common.validateRequired(date)){
				errors.add("date",
						new ActionMessage("debitMemoEntry.error.dateRequired"));
			}
			
			if(!Common.validateDateFormat(date)){
				errors.add("date", 
						new ActionMessage("debitMemoEntry.error.dateInvalid"));
			}
			
			System.out.println(date.lastIndexOf("/"));
			System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			if(year<1900){
				errors.add("date", 
						new ActionMessage("debitMemoEntry.error.dateInvalid"));
			}
			
			if(Common.validateRequired(voucherNumber)){
				errors.add("voucherNumber",
						new ActionMessage("debitMemoEntry.error.voucherNumberRequired"));
			}
			
			if(Common.validateRequired(amount) && type.equals("EXPENSES")){
				errors.add("amount",
						new ActionMessage("debitMemoEntry.error.amountRequired"));
			}
			
			if(!Common.validateMoneyFormat(amount) && type.equals("EXPENSES")){
				errors.add("amount", 
						new ActionMessage("debitMemoEntry.error.amountInvalid"));
			}							
			
			if (type.equals("EXPENSES")) {
				
				int numberOfLines = 0;
				
				Iterator i = apDMList.iterator();      	 
				
				while (i.hasNext()) {
					
					ApDebitMemoEntryList drList = (ApDebitMemoEntryList)i.next();      	 	 
					
					if (Common.validateRequired(drList.getAccount()) &&
							Common.validateRequired(drList.getDebitAmount()) &&
							Common.validateRequired(drList.getCreditAmount())) continue;
					
					numberOfLines++;
					
					if(Common.validateRequired(drList.getAccount())){
						errors.add("account",
								new ActionMessage("debitMemoEntry.error.accountRequired", drList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(drList.getDebitAmount())){
						errors.add("debitAmount",
								new ActionMessage("debitMemoEntry.error.debitAmountInvalid", drList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(drList.getCreditAmount())){
						errors.add("creditAmount",
								new ActionMessage("debitMemoEntry.error.creditAmountInvalid", drList.getLineNumber()));
					}
					if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
						errors.add("distributionRecordAmount",
								new ActionMessage("debitMemoEntry.error.debitCreditAmountRequired", drList.getLineNumber()));
					}
					if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
						errors.add("distributionRecordAmount",
								new ActionMessage("debitMemoEntry.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
					}	    		     
					
					
				}
				
				if (numberOfLines == 0) {
					
					errors.add("supplier",
							new ActionMessage("debitMemoEntry.error.debitMemoMustHaveLine"));
					
				}
				
			} else {
				
				int numOfLines = 0;
				
				Iterator i = apDLIList.iterator();      	 
				
				while (i.hasNext()) {
					
					ApDebitMemoLineItemList dliList = (ApDebitMemoLineItemList)i.next();      	 	 
					
					if (Common.validateRequired(dliList.getLocation()) &&
							Common.validateRequired(dliList.getItemName()) &&
							Common.validateRequired(dliList.getQuantity()) &&
							Common.validateRequired(dliList.getUnit()) &&
							Common.validateRequired(dliList.getUnitCost())) continue;
					
					numOfLines++;
					
					if(Common.validateRequired(dliList.getItemName()) || dliList.getItemName().equals(Constants.GLOBAL_BLANK)){
						errors.add("itemName",
								new ActionMessage("debitMemoEntry.error.itemNameRequired", dliList.getLineNumber()));
					}
					if(Common.validateRequired(dliList.getLocation()) || dliList.getLocation().equals(Constants.GLOBAL_BLANK)){
						errors.add("location",
								new ActionMessage("debitMemoEntry.error.locationRequired", dliList.getLineNumber()));
					}
					if(Common.validateRequired(dliList.getQuantity())) {
						errors.add("quantity",
								new ActionMessage("debitMemoEntry.error.quantityRequired", dliList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(dliList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("debitMemoEntry.error.quantityInvalid", dliList.getLineNumber()));
					}
					if(!Common.validateRequired(dliList.getQuantity()) && Common.convertStringMoneyToDouble(dliList.getQuantity(), (short)3) <= 0) {
		         		errors.add("quantity",
		         		 		new ActionMessage("debitMemoEntry.error.negativeOrZeroQuantityNotAllowed", dliList.getLineNumber()));
		            }
					if(Common.validateRequired(dliList.getUnit()) || dliList.getUnit().equals(Constants.GLOBAL_BLANK)){
						errors.add("unit",
								new ActionMessage("debitMemoEntry.error.unitRequired", dliList.getLineNumber()));
					}
					if(Common.validateRequired(dliList.getUnitCost())){
						errors.add("unitCost",
								new ActionMessage("debitMemoEntry.error.unitCostRequired", dliList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(dliList.getUnitCost())){
						errors.add("unitCost",
								new ActionMessage("debitMemoEntry.error.unitCostInvalid", dliList.getLineNumber()));
					}	        
					
				}
				
				if (numOfLines == 0) {
					
					errors.add("supplier",
							new ActionMessage("debitMemoEntry.error.debitMemoMustHaveItemLine"));
					
				}
				
			}	  	    
			
			
		} else if (!Common.validateRequired(request.getParameter("isVoucherEntered"))) {
			
			if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("supplier",
						new ActionMessage("debitMemoEntry.error.supplierRequired"));
			}
			
			if(Common.validateRequired(voucherNumber)){
				errors.add("voucherNumber",
						new ActionMessage("debitMemoEntry.error.voucherNumberRequired"));
			}
			
			if(Common.validateRequired(amount)){
				errors.add("amount",
						new ActionMessage("debitMemoEntry.error.amountRequired"));
			}
			
			if(!Common.validateMoneyFormat(amount)){
				errors.add("amount", 
						new ActionMessage("debitMemoEntry.error.amountInvalid"));
			}
			
			
		}
		
		return(errors);	
	}
}
