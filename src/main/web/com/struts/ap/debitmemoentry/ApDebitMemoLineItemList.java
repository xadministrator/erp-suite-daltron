package com.struts.ap.debitmemoentry;

import java.io.Serializable;
import java.util.ArrayList;

public class ApDebitMemoLineItemList implements Serializable {
	
	private Integer debitMemoLineItemCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String quantity = null;	
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitCost = null;
	private String amount = null;

	private boolean deleteCheckbox = false;
	
	private String isItemEntered = null;
	private String isUnitEntered = null;
	private String partNumber = null;
	private String misc = null;
	
	private ApDebitMemoEntryForm parentBean;
	
	public ApDebitMemoLineItemList(ApDebitMemoEntryForm parentBean,
			Integer debitMemoLineItemCode,
			String lineNumber, 
			String location,
			String itemName, 
			String itemDescription, 
			String quantity, 	   
			String unit,
			String unitCost,
			String amount,
			String partNumber,
			String misc){
		
		this.parentBean = parentBean;
		this.debitMemoLineItemCode = debitMemoLineItemCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.quantity = quantity;	  
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;
		this.partNumber = partNumber;
		this.misc = misc;
	}
	
	public Integer getDebitMemoLineItemCode(){
		
		return(debitMemoLineItemCode);
		
	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
			
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList() {
		
		unitList.clear();
		
	}
			
	public String getUnitCost() {
		
		return unitCost;		
		
	}
	
	public void setUnitCost(String unitCost) {
		
		this.unitCost = unitCost;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
	public boolean getDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public String getIsItemEntered() {
		
		return isItemEntered;
		
	}
	
	public void setIsItemEntered(String isItemEntered) {
		
		if (isItemEntered != null && isItemEntered.equals("true")) {
			
			parentBean.setRowDLISelected(this, false);
			
		}
		
		isItemEntered = null;
		
	}
	
	public String getIsUnitEntered() {
		
		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {	
		
		if (isUnitEntered != null && isUnitEntered.equals("true")) {
			
			parentBean.setRowDLISelected(this, false);
			
		}				
		
		isUnitEntered = null;
		
	}
	
	public String getPartNumber() {
		return partNumber;
	}
	
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	public String getMisc() {
		return misc;
	}
	
	public void setMisc(String misc) {
		this.misc = misc;
	}
	
}