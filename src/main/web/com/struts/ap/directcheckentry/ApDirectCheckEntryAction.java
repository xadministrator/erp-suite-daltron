package com.struts.ap.directcheckentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApDirectCheckEntryController;
import com.ejb.txn.ApDirectCheckEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ApCheckDetails;
import com.util.ApModCheckDetails;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.ApTaxCodeDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApDirectCheckEntryAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApDirectCheckEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ApDirectCheckEntryForm actionForm = (ApDirectCheckEntryForm)form;

         // reset report

         actionForm.setReport(null);
         actionForm.setReport2(null);
         actionForm.setAttachment(null);
         actionForm.setAttachmentPDF(null);

         String frParam = null;

         if (request.getParameter("child") == null) {

         	frParam = Common.getUserPermission(user, Constants.AP_DIRECT_CHECK_ENTRY_ID);

         } else {

         	frParam = Constants.FULL_ACCESS;

         }

         if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apDirectCheckEntry"));

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApDirectCheckEntryController EJB
*******************************************************/

         ApDirectCheckEntryControllerHome homeDC = null;
         ApDirectCheckEntryController ejbDC = null;
         
         GlDailyRateControllerHome homeFR = null;
         GlDailyRateController ejbFR = null;

         InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;

         try {

            homeDC = (ApDirectCheckEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApDirectCheckEntryControllerEJB", ApDirectCheckEntryControllerHome.class);
            
            homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
                    
            homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);

         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApDirectCheckEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {

            ejbDC = homeDC.create();
            
            ejbFR = homeFR.create();
            
            ejbII = homeII.create();

         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in ApDirectCheckEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ApDirectCheckEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

         short precisionUnit = 0;
         short journalLineNumber = 0;

         boolean enableCheckBatch = false;
         boolean isInitialPrinting = false;
         boolean useSupplierPulldown = true;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/Direct Check/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
         String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
         String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
         try {

            precisionUnit = ejbDC.getGlFcPrecisionUnit(user.getCmpCode());
            journalLineNumber = ejbDC.getAdPrfApJournalLineNumber(user.getCmpCode());
            enableCheckBatch = Common.convertByteToBoolean(ejbDC.getAdPrfEnableApCheckBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableCheckBatch);

            useSupplierPulldown = Common.convertByteToBoolean(ejbDC.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);


         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

/*******************************************************
   -- Ap DC Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("EXPENSES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 System.out.println("saveAsDraftButton");
           ApCheckDetails details = new ApCheckDetails();

           details.setChkCode(actionForm.getCheckCode());
           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
           details.setChkNumber(actionForm.getCheckNumber());
           details.setChkDocumentNumber(actionForm.getDocumentNumber());
           details.setChkReferenceNumber(actionForm.getReferenceNumber());
           details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
           details.setChkDescription(actionForm.getDescription());
           details.setChkInfoType(actionForm.getInfoType());
           details.setChkInfoType(actionForm.getInfoType());
           details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
           details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
           details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
           details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());
           details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setChkInvtInscribedStock(Common.convertBooleanToByte(actionForm.getInvtInscribedStock()));
           details.setChkInvtTreasuryBill(Common.convertBooleanToByte(actionForm.getInvtTreasuryBill()));
           details.setChkInvtNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtNextRunDate()));
           details.setChkInvtSettlementDate(Common.convertStringToSQLDate(actionForm.getInvtSettlementDate()));
           details.setChkInvtMaturityDate(Common.convertStringToSQLDate(actionForm.getInvtMaturityDate()));
           details.setChkInvtBidYield(Common.convertStringMoneyToDouble(actionForm.getInvtBidYield(), precisionUnit));
           details.setChkInvtCouponRate(Common.convertStringMoneyToDouble(actionForm.getInvtCouponRate(), precisionUnit));
           details.setChkInvtSettleAmount(Common.convertStringMoneyToDouble(actionForm.getInvtSettlementAmount(), precisionUnit));
           details.setChkInvtFaceValue(Common.convertStringMoneyToDouble(actionForm.getInvtFaceValue(), precisionUnit));
           details.setChkInvtPremiumAmount(Common.convertStringMoneyToDouble(actionForm.getInvtPremiumAmount(), precisionUnit));

           if (actionForm.getCheckCode() == null) {

           	   details.setChkCreatedBy(user.getUserName());
	           details.setChkDateCreated(new java.util.Date());

           }

           details.setChkLastModifiedBy(user.getUserName());
           details.setChkDateLastModified(new java.util.Date());
           details.setChkMemo(actionForm.getMemo());
           details.setChkSupplierName(actionForm.getSupplierName());

           ArrayList drList = new ArrayList();
           int lineNumber = 0;

           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;

           for (int i = 0; i<actionForm.getApDCListSize(); i++) {

           	   ApDirectCheckEntryList apDCList = actionForm.getApDCByIndex(i);

           	   if (Common.validateRequired(apDCList.getAccount()) &&
      	 	       Common.validateRequired(apDCList.getDebitAmount()) &&
      	 	       Common.validateRequired(apDCList.getCreditAmount())) continue;

           	   byte isDebit = 0;
		       double amount = 0d;

		       if (!Common.validateRequired(apDCList.getDebitAmount())) {

		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apDCList.getDebitAmount(), precisionUnit);

		          TOTAL_DEBIT += amount;

		       } else {

		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apDCList.getCreditAmount(), precisionUnit);

		          TOTAL_CREDIT += amount;
		       }

		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apDCList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apDCList.getAccount());

           	   drList.add(mdetails);

           }

           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           System.out.println(TOTAL_CREDIT+"-----"+TOTAL_CREDIT);
           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	System.out.println("NOT EQUAL");
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.checkNotBalance"));

               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apDirectCheckEntry"));

           }

           // validate attachment

           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;


           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   	}*/



           try {
           	System.out.println("saveApChkEntry----------->");
        	   Integer checkCode = ejbDC.saveApChkEntry(details, actionForm.getPaymentTerm(), actionForm.getBankAccount(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        	   actionForm.setCheckCode(checkCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

           } catch (ApCHKCheckNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

// save attachment

          /* if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }*/
/*******************************************************
   -- Ap DC Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&  actionForm.getType().equals("EXPENSES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApCheckDetails details = new ApCheckDetails();

           details.setChkCode(actionForm.getCheckCode());
           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
           details.setChkNumber(actionForm.getCheckNumber());
           details.setChkDocumentNumber(actionForm.getDocumentNumber());
           details.setChkReferenceNumber(actionForm.getReferenceNumber());
           details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
           details.setChkDescription(actionForm.getDescription());

           details.setChkInfoType(actionForm.getInfoType());
           details.setChkInfoType(actionForm.getInfoType());
           details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
           details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
           details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
           details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());
           details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setChkInvtInscribedStock(Common.convertBooleanToByte(actionForm.getInvtInscribedStock()));
           details.setChkInvtTreasuryBill(Common.convertBooleanToByte(actionForm.getInvtTreasuryBill()));
           details.setChkInvtNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtNextRunDate()));
           details.setChkInvtSettlementDate(Common.convertStringToSQLDate(actionForm.getInvtSettlementDate()));
           details.setChkInvtMaturityDate(Common.convertStringToSQLDate(actionForm.getInvtMaturityDate()));
           details.setChkInvtBidYield(Common.convertStringMoneyToDouble(actionForm.getInvtBidYield(), precisionUnit));
           details.setChkInvtCouponRate(Common.convertStringMoneyToDouble(actionForm.getInvtCouponRate(), precisionUnit));
           details.setChkInvtSettleAmount(Common.convertStringMoneyToDouble(actionForm.getInvtSettlementAmount(), precisionUnit));
           details.setChkInvtFaceValue(Common.convertStringMoneyToDouble(actionForm.getInvtFaceValue(), precisionUnit));
           details.setChkInvtPremiumAmount(Common.convertStringMoneyToDouble(actionForm.getInvtPremiumAmount(), precisionUnit));

           if (actionForm.getCheckCode() == null) {

           	   details.setChkCreatedBy(user.getUserName());
	           details.setChkDateCreated(new java.util.Date());

           }

           details.setChkLastModifiedBy(user.getUserName());
           details.setChkDateLastModified(new java.util.Date());
           details.setChkMemo(actionForm.getMemo());
           details.setChkSupplierName(actionForm.getSupplierName());

           ArrayList drList = new ArrayList();
           int lineNumber = 0;

           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;

           for (int i = 0; i<actionForm.getApDCListSize(); i++) {

           	   ApDirectCheckEntryList apDCList = actionForm.getApDCByIndex(i);

           	   if (Common.validateRequired(apDCList.getAccount()) &&
      	 	       Common.validateRequired(apDCList.getDebitAmount()) &&
      	 	       Common.validateRequired(apDCList.getCreditAmount())) continue;

           	   byte isDebit = 0;
		       double amount = 0d;

		       if (!Common.validateRequired(apDCList.getDebitAmount())) {

		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apDCList.getDebitAmount(), precisionUnit);

		          TOTAL_DEBIT += amount;

		       } else {

		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apDCList.getCreditAmount(), precisionUnit);

		          TOTAL_CREDIT += amount;
		       }

		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apDCList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apDCList.getAccount());

           	   drList.add(mdetails);

           }

           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);

           if (TOTAL_DEBIT != TOTAL_CREDIT) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.checkNotBalance"));

               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apDirectCheckEntry"));

           }

           // validate attachment

           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   	}*/

           try {

           	    Integer checkCode = ejbDC.saveApChkEntry(details, actionForm.getPaymentTerm(), actionForm.getBankAccount(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setCheckCode(checkCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

           } catch (ApCHKCheckNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

// save attachment

          /* if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }*/


/*******************************************************
	 -- Ap DC Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("EXPENSES")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApCheckDetails details = new ApCheckDetails();

	           details.setChkCode(actionForm.getCheckCode());
	           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
	           details.setChkNumber(actionForm.getCheckNumber());
	           details.setChkDocumentNumber(actionForm.getDocumentNumber());
	           details.setChkReferenceNumber(actionForm.getReferenceNumber());
	           details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
	           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
	           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
	           details.setChkDescription(actionForm.getDescription());
	           details.setChkInfoType(actionForm.getInfoType());
	           details.setChkInfoType(actionForm.getInfoType());
	           details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
	           details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
	           details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
	           details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());
               details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	           details.setChkInvtInscribedStock(Common.convertBooleanToByte(actionForm.getInvtInscribedStock()));
	           details.setChkInvtTreasuryBill(Common.convertBooleanToByte(actionForm.getInvtTreasuryBill()));
	           details.setChkInvtNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtNextRunDate()));
	           details.setChkInvtSettlementDate(Common.convertStringToSQLDate(actionForm.getInvtSettlementDate()));
	           details.setChkInvtMaturityDate(Common.convertStringToSQLDate(actionForm.getInvtMaturityDate()));
	           details.setChkInvtBidYield(Common.convertStringMoneyToDouble(actionForm.getInvtBidYield(), precisionUnit));
	           details.setChkInvtCouponRate(Common.convertStringMoneyToDouble(actionForm.getInvtCouponRate(), precisionUnit));
	           details.setChkInvtSettleAmount(Common.convertStringMoneyToDouble(actionForm.getInvtSettlementAmount(), precisionUnit));
	           details.setChkInvtFaceValue(Common.convertStringMoneyToDouble(actionForm.getInvtFaceValue(), precisionUnit));
	           details.setChkInvtPremiumAmount(Common.convertStringMoneyToDouble(actionForm.getInvtPremiumAmount(), precisionUnit));

	           if (actionForm.getCheckCode() == null) {

	           	   details.setChkCreatedBy(user.getUserName());
		           details.setChkDateCreated(new java.util.Date());

	           }

	           details.setChkLastModifiedBy(user.getUserName());
	           details.setChkDateLastModified(new java.util.Date());
	           details.setChkMemo(actionForm.getMemo());
	           details.setChkSupplierName(actionForm.getSupplierName());

	           ArrayList drList = new ArrayList();
	           int lineNumber = 0;

	           double TOTAL_DEBIT = 0;
	           double TOTAL_CREDIT = 0;

	           for (int i = 0; i<actionForm.getApDCListSize(); i++) {

	           	   ApDirectCheckEntryList apDCList = actionForm.getApDCByIndex(i);

	           	   if (Common.validateRequired(apDCList.getAccount()) &&
	      	 	       Common.validateRequired(apDCList.getDebitAmount()) &&
	      	 	       Common.validateRequired(apDCList.getCreditAmount())) continue;

	           	   byte isDebit = 0;
			       double amount = 0d;

			       if (!Common.validateRequired(apDCList.getDebitAmount())) {

			          isDebit = 1;
			          amount = Common.convertStringMoneyToDouble(apDCList.getDebitAmount(), precisionUnit);

			          TOTAL_DEBIT += amount;

			       } else {

			          isDebit = 0;
			          amount = Common.convertStringMoneyToDouble(apDCList.getCreditAmount(), precisionUnit);

			          TOTAL_CREDIT += amount;
			       }

			       lineNumber++;

	           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

	           	   mdetails.setDrLine((short)(lineNumber));
	           	   mdetails.setDrClass(apDCList.getDrClass());
	           	   mdetails.setDrDebit(isDebit);
	           	   mdetails.setDrAmount(amount);
	           	   mdetails.setDrCoaAccountNumber(apDCList.getAccount());

	           	   drList.add(mdetails);

	           }

	           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
	           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);

	           if (TOTAL_DEBIT != TOTAL_CREDIT) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.checkNotBalance"));

	               saveErrors(request, new ActionMessages(errors));
	               return (mapping.findForward("apDirectCheckEntry"));

	           }

	           // validate attachment

	           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	           if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDirectCheckEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDirectCheckEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDirectCheckEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apDirectCheckEntry"));

		   	    	}

		   	   	}*/
	           try {

	           	    Integer checkCode = ejbDC.saveApChkEntry(details, actionForm.getPaymentTerm(), actionForm.getBankAccount(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
	           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	    actionForm.setCheckCode(checkCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

	           } catch (ApCHKCheckNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

	           } catch (GlJREffectiveDateNoPeriodExistException ex) {

		       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

		       } catch (GlJREffectiveDatePeriodClosedException ex) {

		       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

		       } catch (GlobalJournalNotBalanceException ex) {

		       	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                new ActionMessage("directCheckEntry.error.journalNotBalance"));

		       } catch (GlobalBranchAccountNumberInvalidException ex) {

	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	        // save attachment

	           /*if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }    */
	       	if (!errors.isEmpty()) {

	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("apDirectCheckEntry"));

	           }

	           actionForm.setReport(Constants.STATUS_SUCCESS);

	           isInitialPrinting = true;

         } else {

            actionForm.setReport(Constants.STATUS_SUCCESS);

			   return(mapping.findForward("apDirectCheckEntry"));

			}

/*******************************************************
   -- Ap CV Print Action --
*******************************************************/

         } else if (request.getParameter("cvPrintButton") != null && actionForm.getType().equals("EXPENSES") &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 if(Common.validateRequired(actionForm.getApprovalStatus())) {

        		 ApCheckDetails details = new ApCheckDetails();

        		 details.setChkCode(actionForm.getCheckCode());
        		 details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
        		 details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
        		 details.setChkNumber(actionForm.getCheckNumber());
        		 details.setChkDocumentNumber(actionForm.getDocumentNumber());
        		 details.setChkReferenceNumber(actionForm.getReferenceNumber());
        		 details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
        		 details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
        		 details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
        		 details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
        		 details.setChkDescription(actionForm.getDescription());
        		 
        		 details.setChkInfoType(actionForm.getInfoType());
        		 details.setChkInfoType(actionForm.getInfoType());
        		 details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
        		 details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
        		 details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
        		 details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());
        		 details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
        		 details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

        		 details.setChkInvtInscribedStock(Common.convertBooleanToByte(actionForm.getInvtInscribedStock()));
  	           details.setChkInvtTreasuryBill(Common.convertBooleanToByte(actionForm.getInvtTreasuryBill()));
  	           details.setChkInvtNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtNextRunDate()));
  	           details.setChkInvtSettlementDate(Common.convertStringToSQLDate(actionForm.getInvtSettlementDate()));
  	           details.setChkInvtMaturityDate(Common.convertStringToSQLDate(actionForm.getInvtMaturityDate()));
  	           details.setChkInvtBidYield(Common.convertStringMoneyToDouble(actionForm.getInvtBidYield(), precisionUnit));
  	           details.setChkInvtCouponRate(Common.convertStringMoneyToDouble(actionForm.getInvtCouponRate(), precisionUnit));
  	           details.setChkInvtSettleAmount(Common.convertStringMoneyToDouble(actionForm.getInvtSettlementAmount(), precisionUnit));
  	           details.setChkInvtFaceValue(Common.convertStringMoneyToDouble(actionForm.getInvtFaceValue(), precisionUnit));
  	           details.setChkInvtPremiumAmount(Common.convertStringMoneyToDouble(actionForm.getInvtPremiumAmount(), precisionUnit));


        		 if (actionForm.getCheckCode() == null) {

        			 details.setChkCreatedBy(user.getUserName());
        			 details.setChkDateCreated(new java.util.Date());

        		 }

        		 details.setChkLastModifiedBy(user.getUserName());
        		 details.setChkDateLastModified(new java.util.Date());
        		 details.setChkMemo(actionForm.getMemo());
        		 details.setChkSupplierName(actionForm.getSupplierName());

        		 ArrayList drList = new ArrayList();
        		 int lineNumber = 0;

        		 double TOTAL_DEBIT = 0;
        		 double TOTAL_CREDIT = 0;

        		 for (int i = 0; i<actionForm.getApDCListSize(); i++) {

        			 ApDirectCheckEntryList apDCList = actionForm.getApDCByIndex(i);

        			 if (Common.validateRequired(apDCList.getAccount()) &&
        					 Common.validateRequired(apDCList.getDebitAmount()) &&
        					 Common.validateRequired(apDCList.getCreditAmount())) continue;

        			 byte isDebit = 0;
        			 double amount = 0d;

        			 if (!Common.validateRequired(apDCList.getDebitAmount())) {

        				 isDebit = 1;
        				 amount = Common.convertStringMoneyToDouble(apDCList.getDebitAmount(), precisionUnit);

        				 TOTAL_DEBIT += amount;

        			 } else {

        				 isDebit = 0;
        				 amount = Common.convertStringMoneyToDouble(apDCList.getCreditAmount(), precisionUnit);

        				 TOTAL_CREDIT += amount;
        			 }

        			 lineNumber++;

        			 ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

        			 mdetails.setDrLine((short)(lineNumber));
        			 mdetails.setDrClass(apDCList.getDrClass());
        			 mdetails.setDrDebit(isDebit);
        			 mdetails.setDrAmount(amount);
        			 mdetails.setDrCoaAccountNumber(apDCList.getAccount());

        			 drList.add(mdetails);

        		 }

        		 TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
        		 TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);

        		 if (TOTAL_DEBIT != TOTAL_CREDIT) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.checkNotBalance"));

        			 saveErrors(request, new ActionMessages(errors));
        			 return (mapping.findForward("apDirectCheckEntry"));

        		 }

        		 // validate attachment

        		  /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
        		  String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
        		  String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
        		  String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

        		  if (!Common.validateRequired(filename1)) {

      	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

      	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename1().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("apDirectCheckEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename2)) {

      	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename2().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("apDirectCheckEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename3)) {

      	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename3().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("apDirectCheckEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename4)) {

      	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename4().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("apDirectCheckEntry"));

      	   	    	}

      	   	   	}*/

        		 try {

        			 Integer checkCode = ejbDC.saveApChkEntry(details, actionForm.getPaymentTerm(), actionForm.getBankAccount(),
        					 actionForm.getTaxCode(), actionForm.getWithholdingTax(),
        					 actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
        					 drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        			 actionForm.setCheckCode(checkCode);

        		 } catch (GlobalRecordAlreadyDeletedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

        		 } catch (GlobalDocumentNumberNotUniqueException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

        		 } catch (ApCHKCheckNumberNotUniqueException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

        		 } catch (GlobalConversionDateNotExistException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

        		 } catch (GlobalTransactionAlreadyApprovedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

        		 } catch (GlobalTransactionAlreadyPendingException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

        		 } catch (GlobalTransactionAlreadyPostedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

        		 } catch (GlobalTransactionAlreadyVoidException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

        		 } catch (GlobalNoApprovalRequesterFoundException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

        		 } catch (GlobalNoApprovalApproverFoundException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

        		 } catch (GlJREffectiveDateNoPeriodExistException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

        		 } catch (GlJREffectiveDatePeriodClosedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

        		 } catch (GlobalJournalNotBalanceException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.journalNotBalance"));

        		 } catch (GlobalBranchAccountNumberInvalidException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

        		 } catch (EJBException ex) {
        			 if (log.isInfoEnabled()) {

        				 log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
        						 " session: " + session.getId());
        			 }

        			 return(mapping.findForward("cmnErrorPage"));
        		 }
        		// save attachment

                /* if (!Common.validateRequired(filename1)) {

             	        if (errors.isEmpty()) {

             	        	InputStream is = actionForm.getFilename1().getInputStream();

             	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

             	        	new File(attachmentPath).mkdirs();

             	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

             	    			fileExtension = attachmentFileExtension;

                 	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                 	    		fileExtension = attachmentFileExtensionPDF;

                 	    	}
             	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

             	    		int c;

      	            	while ((c = is.read()) != -1) {

      	            		fos.write((byte)c);

      	            	}

      	            	is.close();
      					fos.close();

             	        }

             	   }

             	   if (!Common.validateRequired(filename2)) {

             	        if (errors.isEmpty()) {

             	        	InputStream is = actionForm.getFilename2().getInputStream();

             	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

             	        	new File(attachmentPath).mkdirs();

             	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

             	    			fileExtension = attachmentFileExtension;

                 	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                 	    		fileExtension = attachmentFileExtensionPDF;

                 	    	}
             	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
      	            	int c;

      	            	while ((c = is.read()) != -1) {

      	            		fos.write((byte)c);

      	            	}

      	            	is.close();
      					fos.close();

             	        }

             	   }

             	   if (!Common.validateRequired(filename3)) {

             	        if (errors.isEmpty()) {

             	        	InputStream is = actionForm.getFilename3().getInputStream();

             	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

             	        	new File(attachmentPath).mkdirs();

             	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

             	    			fileExtension = attachmentFileExtension;

                 	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                 	    		fileExtension = attachmentFileExtensionPDF;

                 	    	}
             	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

      	            	int c;

      	            	while ((c = is.read()) != -1) {

      	            		fos.write((byte)c);

      	            	}

      	            	is.close();
      					fos.close();

             	        }

             	   }

             	   if (!Common.validateRequired(filename4)) {

             	        if (errors.isEmpty()) {

             	        	InputStream is = actionForm.getFilename4().getInputStream();

             	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

             	        	new File(attachmentPath).mkdirs();

             	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

             	    			fileExtension = attachmentFileExtension;

                 	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                 	    		fileExtension = attachmentFileExtensionPDF;

                 	    	}
             	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

      	            	int c;

      	            	while ((c = is.read()) != -1) {

      	            		fos.write((byte)c);

      	            	}

      	            	is.close();
      					fos.close();

             	        }

             	   }*/
        		 if (!errors.isEmpty()) {

        			 saveErrors(request, new ActionMessages(errors));
        			 return(mapping.findForward("apDirectCheckEntry"));

        		 }

        		 actionForm.setReport2(Constants.STATUS_SUCCESS);

        		 isInitialPrinting = true;

        	 } else {

  	           actionForm.setReport2(Constants.STATUS_SUCCESS);

			   return(mapping.findForward("apDirectCheckEntry"));

			}


/*******************************************************
    -- Ap DC CLI Save As Draft Action --
 *******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("ITEMS") &&
         	actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	ApCheckDetails details = new ApCheckDetails();

         	details.setChkCode(actionForm.getCheckCode());
         	details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
         	details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
         	details.setChkNumber(actionForm.getCheckNumber());
         	details.setChkDocumentNumber(actionForm.getDocumentNumber());
         	details.setChkReferenceNumber(actionForm.getReferenceNumber());
         	details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
         	details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
         	details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
         	details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
         	details.setChkDescription(actionForm.getDescription());
         	details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         	details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                 details.setChkInfoType(actionForm.getInfoType());
                details.setChkInfoType(actionForm.getInfoType());
                details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
                details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
                details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
                details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());


         	if (actionForm.getCheckCode() == null) {

         		details.setChkCreatedBy(user.getUserName());
         		details.setChkDateCreated(new java.util.Date());

         	}

         	details.setChkLastModifiedBy(user.getUserName());
         	details.setChkDateLastModified(new java.util.Date());
         	details.setChkMemo(actionForm.getMemo());
         	details.setChkSupplierName(actionForm.getSupplierName());

         	ArrayList vliList = new ArrayList();

         	for (int i = 0; i<actionForm.getApCLIListSize(); i++) {

         		ApDirectCheckLineItemList apCLIList = actionForm.getApCLIByIndex(i);

         		if (Common.validateRequired(apCLIList.getLocation()) &&
         				Common.validateRequired(apCLIList.getItemName()) &&
						Common.validateRequired(apCLIList.getUnit()) &&
						Common.validateRequired(apCLIList.getUnitCost()) &&
						Common.validateRequired(apCLIList.getQuantity())) continue;

         		ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

         		mdetails.setVliLine(Common.convertStringToShort(apCLIList.getLineNumber()));
         		mdetails.setVliIiName(apCLIList.getItemName());
         		mdetails.setVliLocName(apCLIList.getLocation());
         		mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apCLIList.getQuantity(), precisionUnit));
         		mdetails.setVliUomName(apCLIList.getUnit());
         		mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apCLIList.getUnitCost(), precisionUnit));
         		mdetails.setVliAmount(Common.convertStringMoneyToDouble(apCLIList.getAmount(), precisionUnit));
         		mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apCLIList.getDiscount1(), precisionUnit));
         		mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apCLIList.getDiscount2(), precisionUnit));
         		mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apCLIList.getDiscount3(), precisionUnit));
         		mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apCLIList.getDiscount4(), precisionUnit));
         		mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apCLIList.getTotalDiscount(), precisionUnit));
         		mdetails.setVliMisc(apCLIList.getMisc());

         		boolean isTraceMisc = ejbDC.getInvTraceMisc(apCLIList.getItemName(), user.getCmpCode());

  			    String misc=apCLIList.getMisc();



  	     	    if (isTraceMisc){
  	     		   mdetails.setIsTraceMisc(isTraceMisc);
  	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
  	     		   mdetails.setTagList(tagList);
  	     	    }


         		vliList.add(mdetails);

         	}

         	 // validate attachment

            /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   	}*/

         	try {

         		Integer checkCode = ejbDC.saveApChkVliEntry(details, actionForm.getBankAccount(),
         				actionForm.getTaxCode(), actionForm.getWithholdingTax(),
						actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
						vliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         		actionForm.setCheckCode(checkCode);
         	} catch (GlobalRecordAlreadyDeletedException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

         	} catch (GlobalDocumentNumberNotUniqueException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

         	} catch (ApCHKCheckNumberNotUniqueException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

         	} catch (GlobalTransactionAlreadyApprovedException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

         	} catch (GlobalTransactionAlreadyPendingException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

         	} catch (GlobalTransactionAlreadyPostedException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

         	} catch (GlobalTransactionAlreadyVoidException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

         	} catch (GlobalNoApprovalRequesterFoundException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

         	} catch (GlobalNoApprovalApproverFoundException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

         	} catch (GlobalInvItemLocationNotFoundException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.noItemLocationFound", ex.getMessage()));

            } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

            } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

            } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.journalNotBalance"));

            } catch (GlobalInventoryDateException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("directCheckEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

            } catch (GlobalBranchAccountNumberInvalidException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

            } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("directCheckEntry.error.noNegativeInventoryCostingCOA"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         		}

         		return(mapping.findForward("cmnErrorPage"));
         	}

         // save attachment

           /* if (!Common.validateRequired(filename1)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename1().getInputStream();

        	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

        	    		int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename2)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename2().getInputStream();

        	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename3)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename3().getInputStream();

        	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename4)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename4().getInputStream();

        	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }*/

/*******************************************************
   -- Ap DC CLI Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&  actionForm.getType().equals("ITEMS") &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	ApCheckDetails details = new ApCheckDetails();

         	details.setChkCode(actionForm.getCheckCode());
         	details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
         	details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
         	details.setChkNumber(actionForm.getCheckNumber());
         	details.setChkDocumentNumber(actionForm.getDocumentNumber());
         	details.setChkReferenceNumber(actionForm.getReferenceNumber());
         	details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
         	details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
         	details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
         	details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
         	details.setChkDescription(actionForm.getDescription());
         	details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         	details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                 details.setChkInfoType(actionForm.getInfoType());
                details.setChkInfoType(actionForm.getInfoType());
                details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
                details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
                details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
                details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());

         	if (actionForm.getCheckCode() == null) {

         		details.setChkCreatedBy(user.getUserName());
         		details.setChkDateCreated(new java.util.Date());

         	}

         	details.setChkLastModifiedBy(user.getUserName());
         	details.setChkDateLastModified(new java.util.Date());
         	details.setChkMemo(actionForm.getMemo());
         	details.setChkSupplierName(actionForm.getSupplierName());

         	ArrayList vliList = new ArrayList();

         	for (int i = 0; i<actionForm.getApCLIListSize(); i++) {

         		ApDirectCheckLineItemList apCLIList = actionForm.getApCLIByIndex(i);

         		if (Common.validateRequired(apCLIList.getLocation()) &&
         				Common.validateRequired(apCLIList.getItemName()) &&
						Common.validateRequired(apCLIList.getUnit()) &&
						Common.validateRequired(apCLIList.getUnitCost()) &&
						Common.validateRequired(apCLIList.getQuantity())) continue;

         		ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

         		mdetails.setVliLine(Common.convertStringToShort(apCLIList.getLineNumber()));
         		mdetails.setVliIiName(apCLIList.getItemName());
         		mdetails.setVliLocName(apCLIList.getLocation());
         		mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apCLIList.getQuantity(), precisionUnit));
         		mdetails.setVliUomName(apCLIList.getUnit());
         		mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apCLIList.getUnitCost(), precisionUnit));
         		mdetails.setVliAmount(Common.convertStringMoneyToDouble(apCLIList.getAmount(), precisionUnit));
         		mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apCLIList.getDiscount1(), precisionUnit));
         		mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apCLIList.getDiscount2(), precisionUnit));
         		mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apCLIList.getDiscount3(), precisionUnit));
         		mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apCLIList.getDiscount4(), precisionUnit));
         		mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apCLIList.getTotalDiscount(), precisionUnit));
         		mdetails.setVliMisc(apCLIList.getMisc());


         		boolean isTraceMisc = ejbDC.getInvTraceMisc(apCLIList.getItemName(), user.getCmpCode());

  			    String misc=apCLIList.getMisc();



  	     	    if (isTraceMisc){
  	     		   mdetails.setIsTraceMisc(isTraceMisc);
  	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
  	     		   mdetails.setTagList(tagList);
  	     	    }




         		vliList.add(mdetails);

         	}

//          validate attachment

            /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

            if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apDirectCheckEntry"));

	   	    	}

	   	   	}*/


         	try {

         		Integer checkCode = ejbDC.saveApChkVliEntry(details, actionForm.getBankAccount(),
         				actionForm.getTaxCode(), actionForm.getWithholdingTax(),
						actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
						vliList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         		actionForm.setCheckCode(checkCode);

         	} catch (GlobalRecordAlreadyDeletedException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

         	} catch (GlobalDocumentNumberNotUniqueException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

         	} catch (ApCHKCheckNumberNotUniqueException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

         	} catch (GlobalTransactionAlreadyApprovedException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

         	} catch (GlobalTransactionAlreadyPendingException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

         	} catch (GlobalTransactionAlreadyPostedException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

         	} catch (GlobalTransactionAlreadyVoidException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

         	} catch (GlobalNoApprovalRequesterFoundException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

         	} catch (GlobalNoApprovalApproverFoundException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

         	} catch (GlobalInvItemLocationNotFoundException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("directCheckEntry.error.noItemLocationFound", ex.getMessage()));

            } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

            } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

            } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.journalNotBalance"));

            } catch (GlobalInventoryDateException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("directCheckEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

            } catch (GlobalBranchAccountNumberInvalidException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

            } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("directCheckEntry.error.noNegativeInventoryCostingCOA"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         		}

         		return(mapping.findForward("cmnErrorPage"));
         	}
         // save attachment

           /* if (!Common.validateRequired(filename1)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename1().getInputStream();

        	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

        	    		int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename2)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename2().getInputStream();

        	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename3)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename3().getInputStream();

        	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename4)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename4().getInputStream();

        	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }
        	   */
/*******************************************************
 -- Ap DC CLI Print Action --
 *******************************************************/

         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("ITEMS")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

         		ApCheckDetails details = new ApCheckDetails();

         		details.setChkCode(actionForm.getCheckCode());
         		details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
         		details.setChkNumber(actionForm.getCheckNumber());
         		details.setChkDocumentNumber(actionForm.getDocumentNumber());
         		details.setChkReferenceNumber(actionForm.getReferenceNumber());
         		details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
         		details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
         		details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
         		details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
         		details.setChkDescription(actionForm.getDescription());
         		details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         		details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                         details.setChkInfoType(actionForm.getInfoType());
                        details.setChkInfoType(actionForm.getInfoType());
                        details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
                        details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
                        details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
                        details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());

         		if (actionForm.getCheckCode() == null) {

         			details.setChkCreatedBy(user.getUserName());
         			details.setChkDateCreated(new java.util.Date());

         		}

         		details.setChkLastModifiedBy(user.getUserName());
         		details.setChkDateLastModified(new java.util.Date());
         		details.setChkMemo(actionForm.getMemo());
         		details.setChkSupplierName(actionForm.getSupplierName());

         		ArrayList vliList = new ArrayList();

             	for (int i = 0; i<actionForm.getApCLIListSize(); i++) {

             		ApDirectCheckLineItemList apCLIList = actionForm.getApCLIByIndex(i);

             		if (Common.validateRequired(apCLIList.getLocation()) &&
             				Common.validateRequired(apCLIList.getItemName()) &&
    						Common.validateRequired(apCLIList.getUnit()) &&
    						Common.validateRequired(apCLIList.getUnitCost()) &&
    						Common.validateRequired(apCLIList.getQuantity())) continue;

             		ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

             		mdetails.setVliLine(Common.convertStringToShort(apCLIList.getLineNumber()));
             		mdetails.setVliIiName(apCLIList.getItemName());
             		mdetails.setVliLocName(apCLIList.getLocation());
             		mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apCLIList.getQuantity(), precisionUnit));
             		mdetails.setVliUomName(apCLIList.getUnit());
             		mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apCLIList.getUnitCost(), precisionUnit));
             		mdetails.setVliAmount(Common.convertStringMoneyToDouble(apCLIList.getAmount(), precisionUnit));
             		mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apCLIList.getDiscount1(), precisionUnit));
             		mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apCLIList.getDiscount2(), precisionUnit));
             		mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apCLIList.getDiscount3(), precisionUnit));
             		mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apCLIList.getDiscount4(), precisionUnit));
             		mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apCLIList.getTotalDiscount(), precisionUnit));
             		mdetails.setVliMisc(apCLIList.getMisc());


             		boolean isTraceMisc = ejbDC.getInvTraceMisc(apCLIList.getItemName(), user.getCmpCode());

      			    String misc=apCLIList.getMisc();



      	     	    if (isTraceMisc){
      	     		   mdetails.setIsTraceMisc(isTraceMisc);
      	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
      	     		   mdetails.setTagList(tagList);
      	     	    }

             		vliList.add(mdetails);

             	}

             	 // validate attachment

               /* String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

    	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

    	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename1().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename2)) {

    	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename2().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename3)) {

    	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename3().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename4)) {

    	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename4().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   	}
*/
         		try {

         			Integer checkCode = ejbDC.saveApChkVliEntry(details, actionForm.getBankAccount(),
         					actionForm.getTaxCode(), actionForm.getWithholdingTax(),
							actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
							vliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			actionForm.setCheckCode(checkCode);

         		} catch (GlobalRecordAlreadyDeletedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

         		} catch (GlobalDocumentNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

         		} catch (ApCHKCheckNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

         		} catch (GlobalConversionDateNotExistException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

         		} catch (GlobalTransactionAlreadyApprovedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

         		} catch (GlobalTransactionAlreadyPendingException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

         		} catch (GlobalTransactionAlreadyPostedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

         		} catch (GlobalTransactionAlreadyVoidException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

         		} catch (GlobalNoApprovalRequesterFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

         		} catch (GlobalNoApprovalApproverFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

         		} catch (GlobalInvItemLocationNotFoundException ex) {

             		errors.add(ActionMessages.GLOBAL_MESSAGE,
             				new ActionMessage("directCheckEntry.error.noItemLocationFound", ex.getMessage()));

                } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

                } catch (GlJREffectiveDatePeriodClosedException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

                } catch (GlobalJournalNotBalanceException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.journalNotBalance"));

                } catch (GlobalInventoryDateException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                   			new ActionMessage("directCheckEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                } catch (GlobalBranchAccountNumberInvalidException ex) {

	          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	          	            new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

 	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

 	              errors.add(ActionMessages.GLOBAL_MESSAGE,
 	                      new ActionMessage("directCheckEntry.error.noNegativeInventoryCostingCOA"));

         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {

         				log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}

         			return(mapping.findForward("cmnErrorPage"));
         		}
         	// save attachment

               /* if (!Common.validateRequired(filename1)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename1().getInputStream();

            	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

            	    		int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename2)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename2().getInputStream();

            	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename3)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename3().getInputStream();

            	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename4)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename4().getInputStream();

            	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }*/

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return(mapping.findForward("apDirectCheckEntry"));

         		}

         		actionForm.setReport(Constants.STATUS_SUCCESS);

         		isInitialPrinting = true;

         	} else {

         		actionForm.setReport(Constants.STATUS_SUCCESS);

         		return(mapping.findForward("apDirectCheckEntry"));

         	}

/*******************************************************
 -- Ap DC CLI CV Print Action --
 *******************************************************/

         } else if (request.getParameter("cvPrintButton") != null && actionForm.getType().equals("ITEMS")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

         		ApCheckDetails details = new ApCheckDetails();

         		details.setChkCode(actionForm.getCheckCode());
         		details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
         		details.setChkNumber(actionForm.getCheckNumber());
         		details.setChkDocumentNumber(actionForm.getDocumentNumber());
         		details.setChkReferenceNumber(actionForm.getReferenceNumber());
         		details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
         		details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
         		details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
         		details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
         		details.setChkDescription(actionForm.getDescription());
         		details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         		details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                         details.setChkInfoType(actionForm.getInfoType());
                        details.setChkInfoType(actionForm.getInfoType());
                        details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
                        details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
                        details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
                        details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());


         		if (actionForm.getCheckCode() == null) {

         			details.setChkCreatedBy(user.getUserName());
         			details.setChkDateCreated(new java.util.Date());

         		}

         		details.setChkLastModifiedBy(user.getUserName());
         		details.setChkDateLastModified(new java.util.Date());
         		details.setChkMemo(actionForm.getMemo());
         		details.setChkSupplierName(actionForm.getSupplierName());

         		ArrayList vliList = new ArrayList();

         		for (int i = 0; i<actionForm.getApCLIListSize(); i++) {

         			ApDirectCheckLineItemList apCLIList = actionForm.getApCLIByIndex(i);

         			if (Common.validateRequired(apCLIList.getLocation()) &&
         					Common.validateRequired(apCLIList.getItemName()) &&
							Common.validateRequired(apCLIList.getUnit()) &&
							Common.validateRequired(apCLIList.getUnitCost()) &&
							Common.validateRequired(apCLIList.getQuantity())) continue;

         			ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

         			mdetails.setVliLine(Common.convertStringToShort(apCLIList.getLineNumber()));
         			mdetails.setVliIiName(apCLIList.getItemName());
         			mdetails.setVliLocName(apCLIList.getLocation());
         			mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apCLIList.getQuantity(), precisionUnit));
         			mdetails.setVliUomName(apCLIList.getUnit());
         			mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apCLIList.getUnitCost(), precisionUnit));
         			mdetails.setVliAmount(Common.convertStringMoneyToDouble(apCLIList.getAmount(), precisionUnit));
             		mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apCLIList.getDiscount1(), precisionUnit));
             		mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apCLIList.getDiscount2(), precisionUnit));
             		mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apCLIList.getDiscount3(), precisionUnit));
             		mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apCLIList.getDiscount4(), precisionUnit));
             		mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apCLIList.getTotalDiscount(), precisionUnit));
             		mdetails.setVliMisc(apCLIList.getMisc());

             		boolean isTraceMisc = ejbDC.getInvTraceMisc(apCLIList.getItemName(), user.getCmpCode());

      			    String misc=apCLIList.getMisc();



      	     	    if (isTraceMisc){
      	     		   mdetails.setIsTraceMisc(isTraceMisc);
      	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
      	     		   mdetails.setTagList(tagList);
      	     	    }


         			vliList.add(mdetails);

         		}
//         	 validate attachment

                /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

    	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

    	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename1().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename2)) {

    	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename2().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename3)) {

    	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename3().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename4)) {

    	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename4().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   	}*/

         		try {

         			Integer checkCode = ejbDC.saveApChkVliEntry(details, actionForm.getBankAccount(),
         					actionForm.getTaxCode(), actionForm.getWithholdingTax(),
							actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
							vliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			actionForm.setCheckCode(checkCode);

         		} catch (GlobalRecordAlreadyDeletedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

         		} catch (GlobalDocumentNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

         		} catch (ApCHKCheckNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

         		} catch (GlobalConversionDateNotExistException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

         		} catch (GlobalTransactionAlreadyApprovedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

         		} catch (GlobalTransactionAlreadyPendingException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

         		} catch (GlobalTransactionAlreadyPostedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

         		} catch (GlobalTransactionAlreadyVoidException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

         		} catch (GlobalNoApprovalRequesterFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

         		} catch (GlobalNoApprovalApproverFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

         		} catch (GlobalInvItemLocationNotFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noItemLocationFound", ex.getMessage()));

         		} catch (GlJREffectiveDateNoPeriodExistException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

                } catch (GlJREffectiveDatePeriodClosedException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

                } catch (GlobalJournalNotBalanceException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.journalNotBalance"));

                } catch (GlobalInventoryDateException ex) {

                   	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   			new ActionMessage("directCheckEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                } catch (GlobalBranchAccountNumberInvalidException ex) {

	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

 	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

 	              errors.add(ActionMessages.GLOBAL_MESSAGE,
 	                      new ActionMessage("directCheckEntry.error.noNegativeInventoryCostingCOA"));

         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {

         				log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}

         			return(mapping.findForward("cmnErrorPage"));
         		}
         	// save attachment

                /*if (!Common.validateRequired(filename1)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename1().getInputStream();

            	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

            	    		int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename2)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename2().getInputStream();

            	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename3)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename3().getInputStream();

            	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename4)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename4().getInputStream();

            	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }*/

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return(mapping.findForward("apDirectCheckEntry"));

         		}

         		actionForm.setReport2(Constants.STATUS_SUCCESS);

         		isInitialPrinting = true;

         	} else {

         		actionForm.setReport2(Constants.STATUS_SUCCESS);

         		return(mapping.findForward("apDirectCheckEntry"));

         	}

/*******************************************************
    -- Ap DC CLI Journal Action --
 *******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("ITEMS")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

         		ApCheckDetails details = new ApCheckDetails();

         		details.setChkCode(actionForm.getCheckCode());
         		details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
         		details.setChkNumber(actionForm.getCheckNumber());
         		details.setChkDocumentNumber(actionForm.getDocumentNumber());
         		details.setChkReferenceNumber(actionForm.getReferenceNumber());
         		details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
         		details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
         		details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
         		details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
         		details.setChkDescription(actionForm.getDescription());
         		details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         		details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                         details.setChkInfoType(actionForm.getInfoType());
                        details.setChkInfoType(actionForm.getInfoType());
                        details.setChkInfoBioNumber(actionForm.getInfoBioNumber());
                        details.setChkInfoBioDescription(actionForm.getInfoBioDescription());
                        details.setChkInfoTypeStatus(actionForm.getInfoTypeStatus());
                        details.setChkInfoRequestStatus(actionForm.getInfoRequestStatus());

         		if (actionForm.getCheckCode() == null) {

         			details.setChkCreatedBy(user.getUserName());
         			details.setChkDateCreated(new java.util.Date());

         		}

         		details.setChkLastModifiedBy(user.getUserName());
         		details.setChkDateLastModified(new java.util.Date());
         		details.setChkMemo(actionForm.getMemo());
         		details.setChkSupplierName(actionForm.getSupplierName());

         		ArrayList vliList = new ArrayList();

         		for (int i = 0; i<actionForm.getApCLIListSize(); i++) {

         			ApDirectCheckLineItemList apCLIList = actionForm.getApCLIByIndex(i);

         			if (Common.validateRequired(apCLIList.getLocation()) &&
         					Common.validateRequired(apCLIList.getItemName()) &&
							Common.validateRequired(apCLIList.getUnit()) &&
							Common.validateRequired(apCLIList.getUnitCost()) &&
							Common.validateRequired(apCLIList.getQuantity())) continue;

         			ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

         			mdetails.setVliLine(Common.convertStringToShort(apCLIList.getLineNumber()));
         			mdetails.setVliIiName(apCLIList.getItemName());
         			mdetails.setVliLocName(apCLIList.getLocation());
         			mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apCLIList.getQuantity(), precisionUnit));
         			mdetails.setVliUomName(apCLIList.getUnit());
         			mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apCLIList.getUnitCost(), precisionUnit));
         			mdetails.setVliAmount(Common.convertStringMoneyToDouble(apCLIList.getAmount(), precisionUnit));
             		mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apCLIList.getDiscount1(), precisionUnit));
             		mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apCLIList.getDiscount2(), precisionUnit));
             		mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apCLIList.getDiscount3(), precisionUnit));
             		mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apCLIList.getDiscount4(), precisionUnit));
             		mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apCLIList.getTotalDiscount(), precisionUnit));
             		mdetails.setVliMisc(apCLIList.getMisc());

             		boolean isTraceMisc = ejbDC.getInvTraceMisc(apCLIList.getItemName(), user.getCmpCode());

      			    String misc=apCLIList.getMisc();



      	     	    if (isTraceMisc){
      	     		   mdetails.setIsTraceMisc(isTraceMisc);
      	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
      	     		   mdetails.setTagList(tagList);
      	     	    }


         			vliList.add(mdetails);

         		}
         		 // validate attachment

                /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

    	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename1NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

    	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename1Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename1().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename1SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename2)) {

    	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename2NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename2Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename2().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename2SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename3)) {

    	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename3NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename3Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename3().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename3SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename4)) {

    	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("directCheckEntry.error.filename4NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename4Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename4().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("directCheckEntry.error.filename4SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apDirectCheckEntry"));

    	   	    	}

    	   	   	}*/
         		try {

         			Integer checkCode = ejbDC.saveApChkVliEntry(details, actionForm.getBankAccount(),
         					actionForm.getTaxCode(), actionForm.getWithholdingTax(),
							actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
							vliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			actionForm.setCheckCode(checkCode);

         		} catch (GlobalRecordAlreadyDeletedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

         		} catch (GlobalDocumentNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

         		} catch (ApCHKCheckNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

         		} catch (GlobalConversionDateNotExistException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

         		} catch (GlobalTransactionAlreadyApprovedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

         		} catch (GlobalTransactionAlreadyPendingException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

         		} catch (GlobalTransactionAlreadyPostedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

         		} catch (GlobalTransactionAlreadyVoidException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

         		} catch (GlobalNoApprovalRequesterFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

         		} catch (GlobalNoApprovalApproverFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

         		} catch (GlobalInvItemLocationNotFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("directCheckEntry.error.noItemLocationFound", ex.getMessage()));

         		} catch (GlJREffectiveDateNoPeriodExistException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

                } catch (GlJREffectiveDatePeriodClosedException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

                } catch (GlobalJournalNotBalanceException ex) {

           	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("directCheckEntry.error.journalNotBalance"));

                } catch (GlobalInventoryDateException ex) {

                   	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   			new ActionMessage("directCheckEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                } catch (GlobalBranchAccountNumberInvalidException ex) {

	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

 	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

 	              errors.add(ActionMessages.GLOBAL_MESSAGE,
 	                      new ActionMessage("directCheckEntry.error.noNegativeInventoryCostingCOA"));

         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {

         				log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}

         			return(mapping.findForward("cmnErrorPage"));
         		}
         	// save attachment

                /*if (!Common.validateRequired(filename1)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename1().getInputStream();

            	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

            	    		int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename2)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename2().getInputStream();

            	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);
     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename3)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename3().getInputStream();

            	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename4)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename4().getInputStream();

            	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }*/

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return(mapping.findForward("apDirectCheckEntry"));

         		}

         	}

         	String path = "/apJournal.do?forward=1" +
		     "&transactionCode=" + actionForm.getCheckCode() +
		     "&transaction=DIRECT CHECK" +
		     "&transactionNumber=" + actionForm.getCheckNumber() +
		     "&transactionDate=" + actionForm.getDate() +
		     "&transactionEnableFields=" + actionForm.getEnableFields();

            return(new ActionForward(path));

/*******************************************************
   -- Ap DC Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbDC.deleteApChkEntry(actionForm.getCheckCode(), user.getUserName(), user.getCmpCode());
           	    // delete attachment

           	    if (actionForm.getCheckCode() != null) {

					appProperties = MessageResources.getMessageResources("com.ApplicationResources");

		            attachmentPath = appProperties.getMessage("app.attachmentPath") + "ap/check/";
		            attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
					File file = new File(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getCheckCode() + "-2" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getCheckCode() + "-3" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getCheckCode() + "-4" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

           	    }
            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

                return(mapping.findForward("cmnErrorPage"));
            }

/*******************************************************
   -- Ap DC Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap DC Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("EXPENSES")) {

         	int listSize = actionForm.getApDCListSize();

            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	ApDirectCheckEntryList apDCList = new ApDirectCheckEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);

	        	apDCList.setDrClassList(this.getDrClassList());

	        	actionForm.saveApDCList(apDCList);

	        }

	        return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
   -- Ap DC Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("EXPENSES")) {

         	for (int i = 0; i<actionForm.getApDCListSize(); i++) {

           	   ApDirectCheckEntryList apDCList = actionForm.getApDCByIndex(i);

           	   if (apDCList.getDeleteCheckbox()) {

           	   	   actionForm.deleteApDCList(i);
           	   	   i--;
           	   }

            }

            for (int i = 0; i<actionForm.getApDCListSize(); i++) {

           	   ApDirectCheckEntryList apDCList = actionForm.getApDCByIndex(i);

           	   apDCList.setLineNumber(String.valueOf(i+1));

            }

	        return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
 -- Ap DC CLI Add Lines Action --
 *******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	int listSize = actionForm.getApCLIListSize();

         	for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

         		ApDirectCheckLineItemList apCLIList = new ApDirectCheckLineItemList(actionForm,
         				null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
						"0.0", null, null);

         		apCLIList.setLocationList(actionForm.getLocationList());

         		actionForm.saveApCLIList(apCLIList);

         	}

         	return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
 -- Ap DC CLI Delete Lines Action --
 *******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	for (int i = 0; i<actionForm.getApCLIListSize(); i++) {

         		ApDirectCheckLineItemList apVLIList = actionForm.getApCLIByIndex(i);

         		if (apVLIList.getDeleteCheckbox()) {

         			actionForm.deleteApCLIList(i);
         			i--;
         		}

         	}

         	for (int i = 0; i<actionForm.getApCLIListSize(); i++) {

         		ApDirectCheckLineItemList apCLIList = actionForm.getApCLIByIndex(i);

         		apCLIList.setLineNumber(String.valueOf(i+1));

         	}

         	return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
   -- Ap DC Supplier Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {

         	try {

         		ApModSupplierDetails mdetails = ejbDC.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());

         		actionForm.clearApDCList();
         		actionForm.setBankAccount(mdetails.getSplStBaName());
         		actionForm.setTaxCode(mdetails.getSplScTcName());
         		actionForm.setWithholdingTax(mdetails.getSplScWtcName());
         		actionForm.setBillAmount(null);
         		actionForm.setAmount(null);
         		//actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
         		actionForm.setSupplierName(mdetails.getSplName());
         		actionForm.setTempSupplierName(mdetails.getSplName());

         		// populate line item list

         		if(actionForm.getType().equalsIgnoreCase("ITEMS") && actionForm.getType() != null) {

         			if(mdetails.getSplLitName() != null && mdetails.getSplLitName().length() > 0) {

         				actionForm.clearApCLIList();

         				ArrayList list = ejbDC.getInvLitByCstLitName(mdetails.getSplLitName(), user.getCmpCode());

         				if (!list.isEmpty()) {

         					Iterator i = list.iterator();

         					while (i.hasNext()) {

         						InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();

         						ApDirectCheckLineItemList apCLIList = new ApDirectCheckLineItemList(actionForm, null,
         								Common.convertShortToString(liDetails.getLiLine()), liDetails.getLiLocName(),
										liDetails.getLiIiName(), liDetails.getLiIiDescription(), "0",
										liDetails.getLiUomName(), null, null, null, null, null, null, null, null);

         						// populate location list

         						apCLIList.setLocationList(actionForm.getLocationList());

         						// populate unit field class

         						ArrayList uomList = new ArrayList();
         						uomList = ejbDC.getInvUomByIiName(apCLIList.getItemName(), user.getCmpCode());

         						apCLIList.clearUnitList();
         						apCLIList.setUnitList(Constants.GLOBAL_BLANK);

         						Iterator j = uomList.iterator();
         						while (j.hasNext()) {

         							InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();

         							apCLIList.setUnitList(mUomDetails.getUomName());

         							if (mUomDetails.isDefault()) {

         								apCLIList.setUnit(mUomDetails.getUomName());

         							}

         						}

         						// populate unit cost field

         						if (!Common.validateRequired(apCLIList.getItemName()) && !Common.validateRequired(apCLIList.getUnit())) {

         						//	double unitCost = ejbDC.getInvIiUnitCostByIiNameAndUomName(apCLIList.getItemName(), apCLIList.getUnit(), user.getCmpCode());
         							
         							double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apCLIList.getItemName(), apCLIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
               	
         							apCLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

         						}

         						// populate amount field

         						if(!Common.validateRequired(apCLIList.getQuantity()) && !Common.validateRequired(apCLIList.getUnitCost())) {

         							double amount = Common.convertStringMoneyToDouble(apCLIList.getQuantity(), precisionUnit) *
									Common.convertStringMoneyToDouble(apCLIList.getUnitCost(), precisionUnit);
         							apCLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

         						}

         						actionForm.saveApCLIList(apCLIList);

         					}

         				}

         			} else {

         				actionForm.clearApCLIList();

         				for (int x = 1; x <= journalLineNumber; x++) {

         					ApDirectCheckLineItemList apCLIList = new ApDirectCheckLineItemList(actionForm,
         							null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
									"0.0", null, null);

         					apCLIList.setLocationList(actionForm.getLocationList());
         					apCLIList.setUnitList(Constants.GLOBAL_BLANK);
         					apCLIList.setUnitList("Select Item First");

         					actionForm.saveApCLIList(apCLIList);

         				}

         			}

         		}

                        if(actionForm.getType().equalsIgnoreCase("EXPENSES") && actionForm.getType() != null) {

                        	System.out.println("mdetails.getSplScInvestment()="+mdetails.getSplScInvestment());
                        	System.out.println("mdetails.getSplScLoan()="+mdetails.getSplScLoan());
                            actionForm.setIsInvestment(mdetails.getSplScInvestment() == (byte)1 ? true : false);
                            //actionForm.setIsLoan(mdetails.getSplScLoan() == (byte)1 ? true : false);
                        }


         	} catch (GlobalNoRecordFoundException ex) {

             	actionForm.clearApDCList();

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }
            return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
   -- Ap DC Bill Amount Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {

         	try {

                 ArrayList list = ejbDC.getApDrBySplSupplierCodeAndTcNameAndWtcNameAndChkBillAmountAndBaName(actionForm.getSupplier(),
                     actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                     Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit),
                     actionForm.getBankAccount(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 actionForm.clearApDCList();

 	             Iterator i = list.iterator();

		         while (i.hasNext()) {

			           ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();

				       String debitAmount = null;
				       String creditAmount = null;

				       if (mDrDetails.getDrDebit() == 1) {

				          debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);

				       } else {

				          creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);

				       }

				       ArrayList classList = new ArrayList();
				       classList.add(mDrDetails.getDrClass());

				       ApDirectCheckEntryList apDrList = new ApDirectCheckEntryList(actionForm,
				          mDrDetails.getDrCode(),
				          Common.convertShortToString(mDrDetails.getDrLine()),
				          mDrDetails.getDrCoaAccountNumber(),
				          debitAmount, creditAmount, mDrDetails.getDrClass(),
				          mDrDetails.getDrCoaAccountDescription());


				      if (mDrDetails.getDrClass().equals(Constants.AP_DR_CLASS_CASH)) {

				      	  actionForm.setAmount(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));

				      }

				      actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));
					  actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));

				      apDrList.setDrClassList(classList);

				      actionForm.saveApDCList(apDrList);
			     }

		         int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

		         for (int x = list.size() + 1; x <= remainingList; x++) {

		        	ApDirectCheckEntryList apDCList = new ApDirectCheckEntryList(actionForm,
		        	    null, String.valueOf(x), null, null, null, null, null);

		        	apDCList.setDrClassList(this.getDrClassList());

		        	actionForm.saveApDCList(apDCList);

		         }

             } catch (GlobalNoRecordFoundException ex) {

           	    actionForm.clearApDCList();

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.billAmountNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
 	-- Ap VOU View Attachment Action --
*******************************************************/

        /* } else if(request.getParameter("viewAttachmentButton1") != null) {

 	  	File file = new File(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension );
 	  	File filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtensionPDF );
 	  	String filename = "";

 	  	if (file.exists()){
 	  		filename = file.getName();
 	  	}else if (filePDF.exists()) {
 	  		filename = filePDF.getName();
 	  	}

			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

 	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

 	  		fileExtension = attachmentFileExtension;

	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;

	    	}
 	  	System.out.println(fileExtension + " <== file extension?");
 	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-1" + fileExtension);

 	  	byte data[] = new byte[fis.available()];

 	  	int ctr = 0;
 	  	int c = 0;

 	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

 	  	}

 	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
 	  	} else if (fileExtension == attachmentFileExtensionPDF ){
 	  		System.out.println("pdf");

 	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
 	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
 	  	}

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apDirectCheckEntry"));

	         } else {

	           return (mapping.findForward("apDirectCheckEntryChild"));

	        }



	      } else if(request.getParameter("viewAttachmentButton2") != null) {

	    	  File file = new File(attachmentPath + actionForm.getCheckCode() + "-2" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-2" + attachmentFileExtensionPDF );
	    	  	String filename = "";

	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}




	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apDirectCheckEntry"));

	         } else {

	           return (mapping.findForward("apDirectCheckEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton3") != null) {

	    	  File file = new File(attachmentPath + actionForm.getCheckCode() + "-3" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-3" + attachmentFileExtensionPDF );
	    	  String filename = "";

	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }

	    	  System.out.println(filename + " <== filename?");
	    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    		  fileExtension = attachmentFileExtension;

	    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	    	  }
	    	  System.out.println(fileExtension + " <== file extension?");
	    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension);

	    	  byte data[] = new byte[fis.available()];

	    	  int ctr = 0;
	    	  int c = 0;

	    	  while ((c = fis.read()) != -1) {

	    		  data[ctr] = (byte)c;
	    		  ctr++;

	    	  }

	    	  if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  } else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  }

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apDirectCheckEntry"));

	         } else {

	           return (mapping.findForward("apDirectCheckEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton4") != null) {

	    	  File file = new File(attachmentPath + actionForm.getCheckCode() + "-4" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-4" + attachmentFileExtensionPDF );
	    	  String filename = "";

	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apDirectCheckEntry"));

	         } else {

	           return (mapping.findForward("apDirectCheckEntryChild"));

	        }*/

/*******************************************************
 	-- Ap VOU Item Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("apCLIList[" +
         		actionForm.getRowCLISelected() + "].isItemEntered"))) {

         	ApDirectCheckLineItemList apCLIList =
         		actionForm.getApCLIByIndex(actionForm.getRowCLISelected());

         	try {

         		// populate unit field class

         		ArrayList uomList = new ArrayList();
         		uomList = ejbDC.getInvUomByIiName(apCLIList.getItemName(), user.getCmpCode());

         		apCLIList.clearUnitList();
         		apCLIList.setUnitList(Constants.GLOBAL_BLANK);

         		Iterator i = uomList.iterator();
         		while (i.hasNext()) {

         			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

         			apCLIList.setUnitList(mUomDetails.getUomName());

         			if (mUomDetails.isDefault()) {

         				apCLIList.setUnit(mUomDetails.getUomName());

         			}

         		}


         		//TODO: populate taglist with empty values
         		apCLIList.clearTagList();


         		boolean isTraceMisc = ejbDC.getInvTraceMisc(apCLIList.getItemName(), user.getCmpCode());


         		apCLIList.setIsTraceMisc(isTraceMisc);
          		System.out.println(isTraceMisc + "<== trace misc item enter action");
          		if (isTraceMisc == true){
          			apCLIList.clearTagList();

          			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), apCLIList.getQuantity());


          			apCLIList.setMisc(misc);

          		}



         		// populate unit cost field

         		if (!Common.validateRequired(apCLIList.getItemName()) && !Common.validateRequired(apCLIList.getUnit())) {

         		//	double unitCost = ejbDC.getInvIiUnitCostByIiNameAndUomName(apCLIList.getItemName(), apCLIList.getUnit(), user.getCmpCode());
         			
         			    double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apCLIList.getItemName(), apCLIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                
         			
         			apCLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

         		}

	    		apCLIList.setDiscount1("0.0");
	    		apCLIList.setDiscount2("0.0");
	    		apCLIList.setDiscount3("0.0");
	    		apCLIList.setDiscount4("0.0");
	         	apCLIList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));

		        apCLIList.setQuantity(Common.convertDoubleToStringMoney(1d, precisionUnit));
	    		apCLIList.setAmount(apCLIList.getUnitCost());

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
    -- Ap VOU Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apCLIList[" +
        actionForm.getRowCLISelected() + "].isUnitEntered"))) {

      	ApDirectCheckLineItemList apCLIList =
      		actionForm.getApCLIByIndex(actionForm.getRowCLISelected());

      	try {

      	    // populate unit cost field

      		if (!Common.validateRequired(apCLIList.getLocation()) && !Common.validateRequired(apCLIList.getItemName())) {

	      		//double unitCost = ejbDC.getInvIiUnitCostByIiNameAndUomName(apCLIList.getItemName(), apCLIList.getUnit(), user.getCmpCode());
	      		
	      		   double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apCLIList.getItemName(), apCLIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                
	      		
	      		apCLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

	        }

	        // populate amount field
      		double amount = 0;
	        if(!Common.validateRequired(apCLIList.getQuantity()) && !Common.validateRequired(apCLIList.getUnitCost())) {

	        	amount = Common.convertStringMoneyToDouble(apCLIList.getQuantity(), precisionUnit) * Common.convertStringMoneyToDouble(apCLIList.getUnitCost(), precisionUnit);
	        	apCLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	        }

      		// populate discount field

      		if (!Common.validateRequired(apCLIList.getAmount()) &&
      			(!Common.validateRequired(apCLIList.getDiscount1()) ||
				!Common.validateRequired(apCLIList.getDiscount2()) ||
				!Common.validateRequired(apCLIList.getDiscount3()) ||
				!Common.validateRequired(apCLIList.getDiscount4()))) {

      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

      			}

      			double discountRate1 = Common.convertStringMoneyToDouble(apCLIList.getDiscount1(), precisionUnit)/100;
      			double discountRate2 = Common.convertStringMoneyToDouble(apCLIList.getDiscount2(), precisionUnit)/100;
      			double discountRate3 = Common.convertStringMoneyToDouble(apCLIList.getDiscount3(), precisionUnit)/100;
      			double discountRate4 = Common.convertStringMoneyToDouble(apCLIList.getDiscount4(), precisionUnit)/100;
      			double totalDiscountAmount = 0d;

      			if (discountRate1 > 0) {

      				double discountAmount = amount * discountRate1;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate2 > 0) {

      				double discountAmount = amount * discountRate2;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate3 > 0) {

      				double discountAmount = amount * discountRate3;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate4 > 0) {

      				double discountAmount = amount * discountRate4;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount += amount * (actionForm.getTaxRate() / 100);

      			}

      			apCLIList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
      			apCLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

      		}


          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
   -- Ap VOU Type Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {

	        if (actionForm.getType().equals("ITEMS")) {

	            actionForm.setBillAmount(null);
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApDCList();
	           	actionForm.clearApCLIList();

	           	// Populate line when not forwarding

            	for (int x = 1; x <= journalLineNumber; x++) {

            		ApDirectCheckLineItemList apCLIList = new ApDirectCheckLineItemList(actionForm,
            				null, new Integer(x).toString(), null, null, null, null, null,
							null, null, "0.0","0.0", "0.0", "0.0", null, null);

            		apCLIList.setLocationList(actionForm.getLocationList());

            		apCLIList.setUnitList(Constants.GLOBAL_BLANK);
            		apCLIList.setUnitList("Select Item First");

            		actionForm.saveApCLIList(apCLIList);

            	}

	        } else {

	        	actionForm.setBillAmount(null);
	        	actionForm.setAmount(null);
	           	//actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApDCList();
	           	actionForm.clearApCLIList();

	        }

	        return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
   -- Ap DC Tax Code Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {

         	try {

         		ApTaxCodeDetails details = ejbDC.getApTcByTcName(actionForm.getTaxCode(), user.getCmpCode());
         		actionForm.setTaxRate(details.getTcRate());
         		actionForm.setTaxType(details.getTcType());

         		if(actionForm.getType().equalsIgnoreCase("ITEMS")) {

         			actionForm.clearApCLIList();

         			for (int x = 1; x <= journalLineNumber; x++) {

         				ApDirectCheckLineItemList apCLIList = new ApDirectCheckLineItemList(actionForm,
         						null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
								"0.0", null, null);

         				apCLIList.setLocationList(actionForm.getLocationList());
         				apCLIList.setUnitList(Constants.GLOBAL_BLANK);
         				apCLIList.setUnitList("Select Item First");

         				actionForm.saveApCLIList(apCLIList);

         			}

         		}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apDirectCheckEntry"));

/*******************************************************
    -- Conversion Date Enter Action --
 *******************************************************/
  } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

  	try {

  		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
  				ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
  						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

  	} catch (GlobalConversionDateNotExistException ex) {

  		errors.add(ActionMessages.GLOBAL_MESSAGE,
  				new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

  	} catch (EJBException ex) {

  		if (log.isInfoEnabled()) {

  			log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
  					" session: " + session.getId());

  		}

  		return(mapping.findForward("cmnErrorPage"));

  	}

  	if (!errors.isEmpty()) {

  		saveErrors(request, new ActionMessages(errors));

  	}

  	return(mapping.findForward("apDirectCheckEntry"));



/*******************************************************
   -- Conversion Date Enter Action --
*******************************************************/
     } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

     	try {

     		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
     		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
     		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));

     	} catch (GlobalConversionDateNotExistException ex) {

     		errors.add(ActionMessages.GLOBAL_MESSAGE,
     				new ActionMessage("purchaseOrderEntry.error.conversionDateNotExist"));

     	} catch (EJBException ex) {

     		if (log.isInfoEnabled()) {

     			log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
     					" session: " + session.getId());

     		}

     		return(mapping.findForward("cmnErrorPage"));

     	}

     	if (!errors.isEmpty()) {

     		saveErrors(request, new ActionMessages(errors));

     	}

     	return(mapping.findForward("apDirectCheckEntry"));

     }

/*******************************************************
   -- Ap DC Load Action --
*******************************************************/

         
         if (frParam != null) {
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apDirectCheckEntry"));

            }
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }
            try {
            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearCurrencyList();

            	list = ejbDC.getGlFcAllWithDefault(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

            			actionForm.setCurrencyList(mFcDetails.getFcName());

            			if (mFcDetails.getFcSob() == 1) {

            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());

            			}

            		}

            	}

            	actionForm.clearBankAccountList();

            	list = ejbDC.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBankAccountList((String)i.next());

            		}

            	}


            	actionForm.clearUserList();

            	ArrayList userList = ejbDC.getAdUsrAll(user.getCmpCode());

            	if (userList == null || userList.size() == 0) {

            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator x = userList.iterator();

            		while (x.hasNext()) {

            			actionForm.setUserList((String)x.next());

            		}

            	}

            	actionForm.clearTaxCodeList();

            	list = ejbDC.getApTcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setTaxCodeList((String)i.next());

            		}

            	}


            	actionForm.clearWithholdingTaxList();

            	list = ejbDC.getApWtcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setWithholdingTaxList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setWithholdingTaxList((String)i.next());

            		}

            	}

            	if(actionForm.getUseSupplierPulldown()) {

	            	actionForm.clearSupplierList();

	            	list = ejbDC.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setSupplierList((String)i.next());

	            		}

	            	}

            	}

            	actionForm.clearBatchNameList();

            	list = ejbDC.getApOpenCbAll(user.getUserDepartment() == null ? "" :user.getUserDepartment(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBatchNameList((String)i.next());

            		}

            	}

            	actionForm.clearLocationList();

            	list = ejbDC.getInvLocAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setLocationList((String)i.next());

            		}

            	}


            	actionForm.clearPaymentTermList();

            	list = ejbDC.getAdPytAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setPaymentTermList((String)i.next());

            		}

            	}

            	if (request.getParameter("forwardBatch") != null) {

            		actionForm.setBatchName(request.getParameter("batchName"));
            		actionForm.setType(null);

            	}

            	actionForm.clearApDCList();
            	actionForm.clearApCLIList();

            	if (request.getParameter("forward") != null || isInitialPrinting) {

            		if (request.getParameter("forward") != null) {

            			actionForm.setCheckCode(new Integer(request.getParameter("checkCode")));

            		}

            		ApModCheckDetails mdetails = ejbDC.getApChkByChkCode(actionForm.getCheckCode(), user.getCmpCode());

            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getChkDate()));
            		actionForm.setCheckDate(Common.convertSQLDateToString(mdetails.getChkCheckDate()));
            		actionForm.setCheckNumber(mdetails.getChkNumber());
            		actionForm.setDocumentNumber(mdetails.getChkDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getChkReferenceNumber());
            		actionForm.setBillAmount(Common.convertDoubleToStringMoney(mdetails.getChkBillAmount(), precisionUnit));
            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit));
            		actionForm.setCrossCheck(Common.convertByteToBoolean(mdetails.getChkCrossCheck()));
            		actionForm.setCheckVoid(Common.convertByteToBoolean(mdetails.getChkVoid()));
            		actionForm.setDescription(mdetails.getChkDescription());

                        actionForm.setInfoType(mdetails.getChkInfoType());
                        actionForm.setInfoBioNumber(mdetails.getChkInfoBioNumber());
                        actionForm.setInfoBioDescription(mdetails.getChkInfoBioDescription());
                        actionForm.setInfoTypeStatus(mdetails.getChkInfoTypeStatus());
                        actionForm.setInfoRequestStatus(mdetails.getChkInfoRequestStatus());

            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getChkConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getChkConversionRate(), Constants.CONVERSION_RATE_PRECISION));
            		actionForm.setApprovalStatus(mdetails.getChkApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getChkReasonForRejection());
            		actionForm.setPosted(mdetails.getChkPosted() == 1 ? "YES" : "NO");
            		actionForm.setVoidApprovalStatus(mdetails.getChkVoidApprovalStatus());
            		actionForm.setVoidPosted(mdetails.getChkVoidPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getChkCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getChkDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getChkLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getChkDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getChkApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getChkDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getChkPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getChkDatePosted()));
            		actionForm.setMemo(mdetails.getChkMemo());

            		actionForm.setInvtInscribedStock(Common.convertByteToBoolean(mdetails.getChkInvtInscribedStock()));
            		actionForm.setInvtTreasuryBill(Common.convertByteToBoolean(mdetails.getChkInvtTreasuryBill()));
            		actionForm.setInvtNextRunDate(Common.convertSQLDateToString(mdetails.getChkInvtNextRunDate()));
            		actionForm.setInvtSettlementDate(Common.convertSQLDateToString(mdetails.getChkInvtSettlementDate()));
            		actionForm.setInvtMaturityDate(Common.convertSQLDateToString(mdetails.getChkInvtMaturityDate()));
            		actionForm.setInvtBidYield(Common.convertDoubleToStringMoney(mdetails.getChkInvtBidYield(), precisionUnit));
            		actionForm.setInvtCouponRate(Common.convertDoubleToStringMoney(mdetails.getChkInvtCouponRate(), precisionUnit));
            		actionForm.setInvtSettlementAmount(Common.convertDoubleToStringMoney(mdetails.getChkInvtSettleAmount(), precisionUnit));
            		actionForm.setInvtFaceValue(Common.convertDoubleToStringMoney(mdetails.getChkInvtFaceValue(), precisionUnit));
            		actionForm.setInvtPremiumAmount(Common.convertDoubleToStringMoney(mdetails.getChkInvtPremiumAmount(), precisionUnit));

            		actionForm.setIsInvestment(mdetails.getChkScInvestment()== (byte)1 ? true : false);
                    //actionForm.setIsLoan(mdetails.getChkScLoan() == (byte)1 ? true : false);

            		if (!actionForm.getCurrencyList().contains(mdetails.getChkFcName())) {

            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearCurrencyList();

            			}
            			actionForm.setCurrencyList(mdetails.getChkFcName());

            		}
            		actionForm.setCurrency(mdetails.getChkFcName());

            		if (!actionForm.getSupplierList().contains(mdetails.getChkSplSupplierCode())) {

            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearSupplierList();

            			}
            			actionForm.setSupplierList(mdetails.getChkSplSupplierCode());

            		}
            		actionForm.setSupplier(mdetails.getChkSplSupplierCode());
            		actionForm.setSupplierName(mdetails.getChkSplName());


            		if (!actionForm.getBankAccountList().contains(mdetails.getChkBaName())) {

            			if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearBankAccountList();

            			}
            			actionForm.setBankAccountList(mdetails.getChkBaName());

            		}
            		actionForm.setBankAccount(mdetails.getChkBaName());

            		if (!actionForm.getTaxCodeList().contains(mdetails.getChkTcName())) {

            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearTaxCodeList();

            			}
            			actionForm.setTaxCodeList(mdetails.getChkTcName());

            		}
            		actionForm.setTaxCode(mdetails.getChkTcName());
            		actionForm.setTaxType(mdetails.getChkTcType());
            		actionForm.setTaxRate(mdetails.getChkTcRate());

            		if (!actionForm.getWithholdingTaxList().contains(mdetails.getChkWtcName())) {

            			if (actionForm.getWithholdingTaxList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearWithholdingTaxList();

            			}
            			actionForm.setWithholdingTaxList(mdetails.getChkWtcName());

            		}
            		actionForm.setWithholdingTax(mdetails.getChkWtcName());

            		if (!actionForm.getBatchNameList().contains(mdetails.getChkCbName())) {

           		    	actionForm.setBatchNameList(mdetails.getChkCbName());

           		    }
            		actionForm.setBatchName(mdetails.getChkCbName());


            		if (!actionForm.getPaymentTermList().contains(mdetails.getChkPytName())) {

            			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearPaymentTermList();

            			}
            			actionForm.setPaymentTermList(mdetails.getChkPytName());

            		}
            		actionForm.setPaymentTerm(mdetails.getChkPytName());


            		if (mdetails.getChkVliList() != null && !mdetails.getChkVliList().isEmpty()) {

            		    actionForm.setType("ITEMS");

            		    list = mdetails.getChkVliList();

            			i = list.iterator();

            			while (i.hasNext()) {

            				ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails)i.next();

            				ApDirectCheckLineItemList apCliList = new ApDirectCheckLineItemList(actionForm,
            						mVliDetails.getVliCode(),
									Common.convertShortToString(mVliDetails.getVliLine()),
									mVliDetails.getVliLocName(),
									mVliDetails.getVliIiName(),
									mVliDetails.getVliIiDescription(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliQuantity(), precisionUnit),
									mVliDetails.getVliUomName(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliUnitCost(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliTotalDiscount(), precisionUnit),
									mVliDetails.getVliMisc());

            				apCliList.setLocationList(actionForm.getLocationList());




            				boolean isTraceMisc = ejbDC.getInvTraceMisc(mVliDetails.getVliIiName(), user.getCmpCode());
            				System.out.println("is tm: " + isTraceMisc);
            				apCliList.setIsTraceMisc(isTraceMisc);


            				if (mVliDetails.getIsTraceMic()){

            					ArrayList tagList = mVliDetails.getTagList();

            					//backward version compatibility
            					if(tagList.size()<=0) {
            						//old
            						apCliList.setMisc(mVliDetails.getVliMisc());
            					}else {
            						System.out.println(mVliDetails.getVliCode() + "<== getPlCode forwarded action");
        	        				//TODO:save tagList
        	        				String misc = Common.convertInvModTagListDetailsListToMisc(tagList, apCliList.getQuantity());

        	        			//	mVliDetails.setVliMisc(misc);
        	        				apCliList.setMisc(misc);
        	        				System.out.println(mVliDetails.getVliMisc() + "<== misc");
            					}


            				}









            				apCliList.clearUnitList();
			            	ArrayList unitList = ejbDC.getInvUomByIiName(mVliDetails.getVliIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {

			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		apCliList.setUnitList(mUomDetails.getUomName());

			            	}

            				actionForm.saveApCLIList(apCliList);


            			}

            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

            			for (int x = list.size() + 1; x <= remainingList; x++) {

            				ApDirectCheckLineItemList apCLIList = new ApDirectCheckLineItemList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0",
									"0.0", "0.0", null, null);

            				apCLIList.setLocationList(actionForm.getLocationList());

            				apCLIList.setUnitList(Constants.GLOBAL_BLANK);
            				apCLIList.setUnitList("Select Item First");

            				actionForm.saveApCLIList(apCLIList);

            			}

            			if(actionForm.getTempSupplierName() == null || actionForm.getTempSupplierName().length() == 0) {

            				actionForm.setTempSupplierName(actionForm.getSupplierName());

            			} else {

            				actionForm.setSupplierName(actionForm.getTempSupplierName());

            			}
            			this.setFormProperties(actionForm, user.getCompany());

            			if (request.getParameter("child") == null) {
            				return (mapping.findForward("apDirectCheckEntry"));

            			} else {
            				return (mapping.findForward("apDirectCheckEntryChild"));

            			}

            		} else {
            			actionForm.setType("EXPENSES");

            			list = mdetails.getChkDrList();

            			i = list.iterator();

            			while (i.hasNext()) {

            				ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();

            				String debitAmount = null;
            				String creditAmount = null;

            				if (mDrDetails.getDrDebit() == 1) {

            					debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);

            				} else {

            					creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);

            				}

            				System.out.println("debitAmount="+debitAmount);
            				System.out.println("creditAmount="+creditAmount);

            				ArrayList classList = new ArrayList();
            				classList.add(mDrDetails.getDrClass());

            				ApDirectCheckEntryList apDrList = new ApDirectCheckEntryList(actionForm,
            						mDrDetails.getDrCode(),
									Common.convertShortToString(mDrDetails.getDrLine()),
									mDrDetails.getDrCoaAccountNumber(),
									debitAmount, creditAmount, mDrDetails.getDrClass(),
									mDrDetails.getDrCoaAccountDescription());

            				apDrList.setDrClassList(classList);

            				actionForm.saveApDCList(apDrList);


            			}
            			actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getChkTotalDebit(), precisionUnit));
            			actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getChkTotalCredit(), precisionUnit));

            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

            			for (int x = list.size() + 1; x <= remainingList; x++) {

            				ApDirectCheckEntryList apDCList = new ApDirectCheckEntryList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null);

            				apDCList.setDrClassList(this.getDrClassList());

            				actionForm.saveApDCList(apDCList);

            			}
            			if(actionForm.getTempSupplierName() == null || actionForm.getTempSupplierName().length() == 0) {

            				actionForm.setTempSupplierName(actionForm.getSupplierName());

            			} else {

            				actionForm.setSupplierName(actionForm.getTempSupplierName());

            			}
            			this.setFormProperties(actionForm, user.getCompany());
            			if (request.getParameter("child") == null) {

            				return (mapping.findForward("apDirectCheckEntry"));

            			} else {

            				return (mapping.findForward("apDirectCheckEntryChild"));

            			}


            		}
            	}

            	// Populate line when not forwarding

            	if (user.getUserApps().contains("OMEGA INVENTORY")) {

	            	for (int x = 1; x <= journalLineNumber; x++) {

	            		ApDirectCheckLineItemList apCLIList = new ApDirectCheckLineItemList(actionForm,
	            				null, new Integer(x).toString(), null, null, null, null, null,
								null, null, "0.0","0.0", "0.0", "0.0", null, null);

	            		apCLIList.setLocationList(actionForm.getLocationList());

	            		apCLIList.setUnitList(Constants.GLOBAL_BLANK);
	            		apCLIList.setUnitList("Select Item First");

	            		actionForm.saveApCLIList(apCLIList);

	            	}

            	}

            } catch(GlobalNoRecordFoundException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

            } catch(EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }

               return(mapping.findForward("cmnErrorPage"));

           }



            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveSubmitButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   try {

                   	   ArrayList list = ejbDC.getAdApprovalNotifiedUsersByChkCode(actionForm.getCheckCode(), user.getCmpCode());

                   	   if (list.isEmpty()) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));

                   	   } else if (list.contains("DOCUMENT POSTED")) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                	   	       new ActionMessage("messages.documentPosted"));

                   	   } else {

                   	   	   Iterator i = list.iterator();

                   	   	   String APPROVAL_USERS = "";

                   	   	   while (i.hasNext()) {

                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

                   	   	       if (i.hasNext()) {

                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";

                   	   	       }


                   	   	   }

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

                   	   }

                   	   if (actionForm.getCheckVoid() && actionForm.getPosted().equals("NO")) {

                   	   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

                   	   } else {

	                   	   saveMessages(request, messages);
	                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

	                   }


                   } catch(EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in ApDirectCheckEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }

		               return(mapping.findForward("cmnErrorPage"));

		           }

               } else if (request.getParameter("saveAsDraftButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }

            }

            actionForm.reset(mapping, request);

            if (actionForm.getType() == null) {

	           if (user.getUserApps().contains("OMEGA INVENTORY")) {

                   actionForm.setType("ITEMS");

               } else {

                   actionForm.setType("EXPENSES");

               }

            }

            actionForm.setCheckCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCheckDate(Common.convertSQLDateToString(new java.util.Date()));
            //actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setConversionRate(Common.convertDoubleToStringMoney(1d, Constants.CONVERSION_RATE_PRECISION));
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            this.setFormProperties(actionForm, user.getCompany());

            System.out.println(actionForm.getIsInvestment() + " investment confirmation");
            return(mapping.findForward("apDirectCheckEntry"));

         } else {
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/
    	  e.printStackTrace();
         if (log.isInfoEnabled()) {

            log.info("Exception caught in ApDirectCheckAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }


	private void setFormProperties(ApDirectCheckEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {




			if (actionForm.getCheckVoid() && actionForm.getPosted().equals("NO")) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableCheckVoid(false);

			} else if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getCheckCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableCheckVoid(false);

				} else if (actionForm.getCheckCode() != null &&
				           Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableCheckVoid(true);


				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
				    actionForm.getApprovalStatus().equals("N/A")) {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableCheckVoid(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableCheckVoid(false);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(true);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableCheckVoid(true);

			}



		} else {


			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setEnableCheckVoid(false);

		}

		// view attachment

		/*if (actionForm.getCheckCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/Direct Check/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			File file = new File(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton1(true);

 			} else {

 				actionForm.setShowViewAttachmentButton1(false);

 			}

 			file = new File(attachmentPath + actionForm.getCheckCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-2" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton2(true);

 			} else {

 				actionForm.setShowViewAttachmentButton2(false);

 			}

 			file = new File(attachmentPath + actionForm.getCheckCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-3" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton3(true);

 			} else {

 				actionForm.setShowViewAttachmentButton3(false);

 			}

 			file = new File(attachmentPath + actionForm.getCheckCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getCheckCode() + "-4" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton4(true);

 			} else {

 				actionForm.setShowViewAttachmentButton4(false);

 			}

		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}*/

	}

	private ArrayList getDrClassList() {

		ArrayList classList = new ArrayList();

     	classList.add(Constants.AP_DR_CLASS_EXPENSE);
     	classList.add(Constants.AP_DR_CLASS_TAX);
     	classList.add(Constants.AP_DR_CLASS_WITHHOLDING_TAX);
     	classList.add(Constants.AP_DR_CLASS_OTHER);

     	return classList;

	}

}