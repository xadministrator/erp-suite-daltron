package com.struts.ap.directcheckentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;


public class ApDirectCheckEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
	private Integer checkCode = null;
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList();
	private String supplier = null;
	private ArrayList supplierList = new ArrayList();
	private String bankAccount = null;
	private ArrayList bankAccountList = new ArrayList();

	private String paymentTerm = null;
	private ArrayList paymentTermList = new ArrayList();

	private String date = null;
	private String checkDate = null;
	private String checkNumber = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String billAmount = null;
	private String amount = null;
	private boolean crossCheck = false;
	private boolean checkVoid = false;
	private String totalDebit = null;
	private String totalCredit = null;
	private String description = null;

        private String infoType = null;
        private String infoBioNumber = null;
        private String infoBioDescription = null;
        private String infoTypeStatus = null;
        private String infoRequestStatus = null;




	private String taxCode = null;
	private double taxRate = 0d;
	private String taxType = null;
	private ArrayList taxCodeList = new ArrayList();
	private String withholdingTax = null;
	private ArrayList withholdingTaxList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String conversionDate = null;
	private String conversionRate = null;
	private String approvalStatus = null;
	private String reasonForRejection = null;
	private String posted = null;
	private String voidApprovalStatus = null;
	private String voidPosted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String supplierName = null;
	private String functionalCurrency = null;

	private boolean isInvestment = false;

	private boolean invtInscribedStock = false;
	private boolean invtTreasuryBill = false;
	private String invtNextRunDate = null;
	private String invtSettlementDate = null;
	private String invtMaturityDate = null;
	private String invtBidYield = null;
	private String invtCouponRate = null;
	private String invtSettlementAmount = null;
	private String invtFaceValue = null;
	private String invtPremiumAmount = null;

	private ArrayList userList = new ArrayList();

	private ArrayList apDCList = new ArrayList();
	private ArrayList apCLIList = new ArrayList();
	private int rowSelected = 0;
	private int rowCLISelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showBatchName = false;
	private boolean enableCheckVoid = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveSubmitButton = false;
	private boolean showSaveAsDraftButton = false;
	private boolean showDeleteButton = false;
	private boolean useSupplierPulldown = false;

	private String isSupplierEntered  = null;
	private String isBillAmountEntered = null;
	private String isTypeEntered = null;
	private String isTaxCodeEntered = null;
	private String isConversionDateEntered = null;

	private String report = null;
	private String report2 = null;

	private String tempSupplierName = null;
	private String memo = null;

	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;

	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;

	private String attachment = null;
	private String attachmentPDF = null;



	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getReport2() {

		return report2;

	}

	public void setReport2(String report2) {

		this.report2 = report2;

	}

	public String getAttachmentPDF() {

		return attachmentPDF;

	}

	public void setAttachmentPDF(String attachmentPDF) {

		this.attachmentPDF = attachmentPDF;

	}

	public String getAttachment() {

		return attachment;

	}

	public void setAttachment(String attachment) {

		this.attachment = attachment;

	}

	public int getRowSelected(){
		return rowSelected;
	}

	public ApDirectCheckEntryList getApDCByIndex(int index){
		return((ApDirectCheckEntryList)apDCList.get(index));
	}

	public ArrayList getUserList() {

		return userList;

  }

  public void setUserList(String user) {

	   userList.add(user);

  }

  public void clearUserList() {

	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);

  }


	public Object[] getApDCList(){
		return(apDCList.toArray());
	}

	public int getApDCListSize(){
		return(apDCList.size());
	}

	public void saveApDCList(Object newApDCList){
		apDCList.add(newApDCList);
	}

	public void clearApDCList(){
		apDCList.clear();
	}

	public void setRowSelected(Object selectedApDCList, boolean isEdit){
		this.rowSelected = apDCList.indexOf(selectedApDCList);
	}

	public void updateApDCRow(int rowSelected, Object newApDCList){
		apDCList.set(rowSelected, newApDCList);
	}

	public void deleteApDCList(int rowSelected){
		apDCList.remove(rowSelected);
	}

	public int getRowCLISelected(){
		return rowCLISelected;
	}

	public ApDirectCheckLineItemList getApCLIByIndex(int index){
		return((ApDirectCheckLineItemList)apCLIList.get(index));
	}

	public Object[] getApCLIList(){
		return(apCLIList.toArray());
	}

	public int getApCLIListSize(){
		return(apCLIList.size());
	}

	public void saveApCLIList(Object newApCLIList){
		apCLIList.add(newApCLIList);
	}

	public void clearApCLIList(){
		apCLIList.clear();
	}

	public void setRowCLISelected(Object selectedApCLIList, boolean isEdit){
		this.rowCLISelected = apCLIList.indexOf(selectedApCLIList);
	}

	public void updateApCLIRow(int rowCLISelected, Object newApCLIList){
		apCLIList.set(rowCLISelected, newApCLIList);
	}

	public void deleteApCLIList(int rowCLISelected){
		apCLIList.remove(rowCLISelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}

public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

	public Integer getCheckCode() {

		return checkCode;

	}

	public void setCheckCode(Integer checkCode) {

		this.checkCode = checkCode;

	}

	public String getBatchName() {

		return batchName;

	}

	public void setBatchName(String batchName) {

		this.batchName = batchName;

	}

	public ArrayList getBatchNameList() {

		return batchNameList;

	}

	public void setBatchNameList(String batchName) {

		batchNameList.add(batchName);

	}

	public void clearBatchNameList() {

		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);

	}

	public String getSupplier() {

		return supplier;

	}

	public void setSupplier(String supplier) {

		this.supplier = supplier;

	}

	public ArrayList getSupplierList() {

		return supplierList;

	}

	public void setSupplierList(String supplier) {

		supplierList.add(supplier);

	}

	public void clearSupplierList() {

		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);

	}

	public String getBankAccount() {

		return bankAccount;

	}

	public void setBankAccount(String bankAccount) {

		this.bankAccount = bankAccount;

	}

	public ArrayList getBankAccountList() {

		return bankAccountList;

	}

	public void setBankAccountList(String bankAccount) {

		bankAccountList.add(bankAccount);

	}

	public void clearBankAccountList() {

		bankAccountList.clear();
		bankAccountList.add(Constants.GLOBAL_BLANK);

	}


	public String getPaymentTerm() {

	   	  return paymentTerm;

	   }

	   public void setPaymentTerm(String paymentTerm) {

	   	  this.paymentTerm = paymentTerm;

	   }

	   public ArrayList getPaymentTermList() {

	   	  return paymentTermList;

	   }

	   public void setPaymentTermList(String paymentTerm) {

	   	  paymentTermList.add(paymentTerm);

	   }

	   public void clearPaymentTermList() {

	   	  paymentTermList.clear();
	   	  paymentTermList.add(Constants.GLOBAL_BLANK);

	   }


	public String getDate() {

		return date;

	}

	public void setDate(String date) {

		this.date = date;

	}

	public String getCheckDate() {

		return checkDate;

	}

	public void setCheckDate(String checkDate) {

		this.checkDate = checkDate;

	}

	public String getCheckNumber() {

		return checkNumber;

	}

	public void setCheckNumber(String checkNumber) {

		this.checkNumber = checkNumber;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public void setDocumentNumber(String documentNumber) {

		this.documentNumber = documentNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public void setReferenceNumber(String referenceNumber) {

		this.referenceNumber = referenceNumber;

	}

	public String getBillAmount() {

		return billAmount;

	}

	public void setBillAmount(String billAmount) {

		this.billAmount = billAmount;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}

	public boolean getCrossCheck() {

		return crossCheck;

	}

	public void setCrossCheck(boolean crossCheck) {

		this.crossCheck = crossCheck;

	}

	public boolean getCheckVoid() {

		return checkVoid;

	}

	public void setCheckVoid(boolean checkVoid) {

		this.checkVoid = checkVoid;

	}

	public String getTotalDebit() {

		return totalDebit;

	}

	public void setTotalDebit(String totalDebit) {

		this.totalDebit = totalDebit;

	}

	public String getTotalCredit() {

		return totalCredit;

	}

	public void setTotalCredit(String totalCredit) {

		this.totalCredit = totalCredit;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

		this.description = description;

	}


	public String getInfoType() {

		return infoType;

	}

	public void setInfoType(String infoType) {

		this.infoType = infoType;

	}

        public String getInfoBioNumber() {

		return infoBioNumber;

	}

	public void setInfoBioNumber(String infoBioNumber) {

		this.infoBioNumber = infoBioNumber;

	}


        public String getInfoBioDescription() {

		return infoBioDescription;

	}

	public void setInfoBioDescription(String infoBioDescription) {

		this.infoBioDescription = infoBioDescription;

	}

        public String getInfoTypeStatus() {

		return infoTypeStatus;

	}

	public void setInfoTypeStatus(String infoTypeStatus) {

		this.infoTypeStatus = infoTypeStatus;

	}

        public String getInfoRequestStatus() {

		return infoRequestStatus;

	}

	public void setInfoRequestStatus(String infoRequestStatus) {

		this.infoRequestStatus = infoRequestStatus;

	}

	public String getTaxCode() {

		return taxCode;

	}

	public void setTaxCode(String taxCode) {

		this.taxCode = taxCode;

	}

	public ArrayList getTaxCodeList() {

		return taxCodeList;

	}

	public void setTaxCodeList(String taxCode) {

		taxCodeList.add(taxCode);

	}

	public void clearTaxCodeList() {

		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);

	}


	public String getWithholdingTax() {

		return withholdingTax;

	}

	public void setWithholdingTax(String withholdingTax) {

		this.withholdingTax = withholdingTax;

	}

	public ArrayList getWithholdingTaxList() {

		return withholdingTaxList;

	}

	public void setWithholdingTaxList(String withholdingTax) {

		withholdingTaxList.add(withholdingTax);

	}

	public void clearWithholdingTaxList() {

		withholdingTaxList.clear();
		withholdingTaxList.add(Constants.GLOBAL_BLANK);

	}


	public String getCurrency() {

		return currency;

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}

	public void setCurrencyList(String currency) {

		currencyList.add(currency);

	}

	public void clearCurrencyList() {

		currencyList.clear();

	}

	public String getConversionDate() {

		return conversionDate;

	}

	public void setConversionDate(String conversionDate) {

		this.conversionDate = conversionDate;

	}

	public String getConversionRate() {

		return conversionRate;

	}

	public void setConversionRate(String conversionRate) {

		this.conversionRate = conversionRate;

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public void setApprovalStatus(String approvalStatus) {

		this.approvalStatus = approvalStatus;

	}

	public String getReasonForRejection() {

		return reasonForRejection;

	}

	public void setReasonForRejection(String reasonForRejection) {

		this.reasonForRejection = reasonForRejection;

	}

	public String getPosted() {

		return posted;

	}

	public void setPosted(String posted) {

		this.posted = posted;

	}

	public String getVoidApprovalStatus() {

		return voidApprovalStatus;

	}

	public void setVoidApprovalStatus(String voidApprovalStatus) {

		this.voidApprovalStatus = voidApprovalStatus;

	}

	public String getVoidPosted() {

		return voidPosted;

	}

	public void setVoidPosted(String voidPosted) {

		this.voidPosted = voidPosted;

	}

	public String getCreatedBy() {

		return createdBy;

	}

	public void setCreatedBy(String createdBy) {

		this.createdBy = createdBy;

	}

	public String getDateCreated() {

		return dateCreated;

	}

	public void setDateCreated(String dateCreated) {

		this.dateCreated = dateCreated;

	}

	public String getLastModifiedBy() {

		return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

		this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

		return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

		this.dateLastModified = dateLastModified;

	}


	public String getApprovedRejectedBy() {

		return approvedRejectedBy;

	}

	public void setApprovedRejectedBy(String approvedRejectedBy) {

		this.approvedRejectedBy = approvedRejectedBy;

	}

	public String getDateApprovedRejected() {

		return dateApprovedRejected;

	}

	public void setDateApprovedRejected(String dateApprovedRejected) {

		this.dateApprovedRejected = dateApprovedRejected;

	}

	public String getPostedBy() {

		return postedBy;

	}

	public void setPostedBy(String postedBy) {

		this.postedBy = postedBy;

	}

	public String getDatePosted() {

		return datePosted;

	}

	public void setDatePosted(String datePosted) {

		this.datePosted = datePosted;

	}

	public String getFunctionalCurrency() {

		return functionalCurrency;

	}

	public void setFunctionalCurrency(String functionalCurrency) {

		this.functionalCurrency = functionalCurrency;

	}


public boolean getInvtInscribedStock() {

		return invtInscribedStock;

	}

	public void setInvtInscribedStock(boolean invtInscribedStock) {

		this.invtInscribedStock = invtInscribedStock;

	}

public boolean getInvtTreasuryBill() {

		return invtTreasuryBill;

	}

	public void setInvtTreasuryBill(boolean invtTreasuryBill) {

		this.invtTreasuryBill = invtTreasuryBill;

	}

	public String getInvtNextRunDate() {

		return invtNextRunDate;

	}

	public void setInvtNextRunDate(String invtNextRunDate) {

		this.invtNextRunDate = invtNextRunDate;

	}


	public String getInvtSettlementDate() {

		return invtSettlementDate;

	}

	public void setInvtSettlementDate(String invtSettlementDate) {

		this.invtSettlementDate = invtSettlementDate;

	}

	public String getInvtMaturityDate() {

		return invtMaturityDate;

	}

	public void setInvtMaturityDate(String invtMaturityDate) {

		this.invtMaturityDate = invtMaturityDate;

	}

public String getInvtBidYield() {

		return invtBidYield ;

	}

	public void setInvtBidYield(String invtBidYield ) {

		this.invtBidYield  = invtBidYield ;

	}

	public String getInvtCouponRate() {

		return invtCouponRate ;

	}

	public void setInvtCouponRate(String invtCouponRate ) {

		this.invtCouponRate  = invtCouponRate ;

	}

public String getInvtSettlementAmount() {

		return invtSettlementAmount ;

	}

	public void setInvtSettlementAmount(String invtSettlementAmount ) {

		this.invtSettlementAmount  = invtSettlementAmount ;

	}

public String getInvtFaceValue() {

		return invtFaceValue ;

	}

	public void setInvtFaceValue(String invtFaceValue ) {

		this.invtFaceValue  = invtFaceValue ;

	}

public String getInvtPremiumAmount() {

		return invtPremiumAmount ;

	}

	public void setInvtPremiumAmount(String invtPremiumAmount ) {

		this.invtPremiumAmount  = invtPremiumAmount ;

	}



	public boolean getIsInvestment(){

        return isInvestment;

    }

    public void setIsInvestment(boolean isInvestment){

        this.isInvestment = isInvestment;

    }






	public boolean getShowBatchName() {

		return showBatchName;

	}

	public void setShowBatchName(boolean showBatchName) {

		this.showBatchName = showBatchName;

	}

	public boolean getEnableFields() {

		return enableFields;

	}

	public void setEnableFields(boolean enableFields) {

		this.enableFields = enableFields;

	}

	public boolean getEnableCheckVoid() {

		return enableCheckVoid;

	}

	public void setEnableCheckVoid(boolean enableCheckVoid) {

		this.enableCheckVoid = enableCheckVoid;

	}

	public boolean getShowAddLinesButton() {

		return showAddLinesButton;

	}

	public void setShowAddLinesButton(boolean showAddLinesButton) {

		this.showAddLinesButton = showAddLinesButton;

	}

	public boolean getShowDeleteLinesButton() {

		return showDeleteLinesButton;

	}

	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

		this.showDeleteLinesButton = showDeleteLinesButton;

	}

	public boolean getShowSaveSubmitButton() {

		return showSaveSubmitButton;

	}

	public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {

		this.showSaveSubmitButton = showSaveSubmitButton;

	}

	public boolean getShowSaveAsDraftButton() {

		return showSaveAsDraftButton;

	}

	public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {

		this.showSaveAsDraftButton = showSaveAsDraftButton;

	}

	public boolean getShowDeleteButton() {

		return showDeleteButton;

	}

	public void setShowDeleteButton(boolean showDeleteButton) {

		this.showDeleteButton = showDeleteButton;

	}

	public String getIsSupplierEntered() {

		return isSupplierEntered;

	}

	public String getIsBillAmountEntered() {

		return isBillAmountEntered;

	}

	public String getIsTypeEntered() {

		return isTypeEntered;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {


		this.type = type;

	}

	public ArrayList getTypeList() {

		return typeList;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(String location) {

		locationList.add(location);

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getUseSupplierPulldown() {

		return useSupplierPulldown;

	}

	public void setUseSupplierPulldown(boolean useSupplierPulldown) {

		this.useSupplierPulldown = useSupplierPulldown;

	}

	public String getSupplierName() {

		return supplierName;

	}

	public void setSupplierName(String supplierName) {

		this.supplierName = supplierName;

	}


	public double getTaxRate() {

		return taxRate;

	}

	public void setTaxRate(double taxRate) {

		this.taxRate = taxRate;

	}

	public String getTaxType() {

		return taxType;

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public String getIsTaxCodeEntered() {

		return isTaxCodeEntered;

	}

	public String getIsConversionDateEntered(){

		return isConversionDateEntered;

	}

	public String getTempSupplierName() {

		return tempSupplierName;

	}

	public void setTempSupplierName(String tempSupplierName) {

		this.tempSupplierName = tempSupplierName;

	}

	public String getMemo() {

		return memo;

	}

	public void setMemo(String memo) {

		this.memo = memo;

	}

	public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {

		return filename3;

	}

	public void setFilename3(FormFile filename3) {

		this.filename3 = filename3;

	}

	public FormFile getFilename4() {

		return filename4;

	}

	public void setFilename4(FormFile filename4) {

		this.filename4 = filename4;

	}

	public boolean getShowViewAttachmentButton1() {

		return showViewAttachmentButton1;

	}

	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

		this.showViewAttachmentButton1 = showViewAttachmentButton1;

	}

	public boolean getShowViewAttachmentButton2() {

		return showViewAttachmentButton2;

	}

	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

		this.showViewAttachmentButton2 = showViewAttachmentButton2;

	}

	public boolean getShowViewAttachmentButton3() {

		return showViewAttachmentButton3;

	}

	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

		this.showViewAttachmentButton3 = showViewAttachmentButton3;

	}

	public boolean getShowViewAttachmentButton4() {

		return showViewAttachmentButton4;

	}

	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

		this.showViewAttachmentButton4 = showViewAttachmentButton4;

	}



	public void reset(ActionMapping mapping, HttpServletRequest request){

		supplier = Constants.GLOBAL_BLANK;
		taxCode = Constants.GLOBAL_BLANK;
		withholdingTax = Constants.GLOBAL_BLANK;
		date = null;
		checkDate = null;
		checkNumber = null;
		documentNumber = null;
		referenceNumber = null;
		billAmount = null;
		amount = null;
		crossCheck = false;
		checkVoid = false;
		description = null;
                infoType = null;
                infoBioNumber = null;
                infoBioDescription = null;
                infoTypeStatus = null;
                infoRequestStatus = null;
		conversionDate = null;
		conversionRate = null;
		approvalStatus = null;
		reasonForRejection = null;
		posted = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;
		approvedRejectedBy = null;
		dateApprovedRejected = null;
		postedBy = null;
		datePosted = null;
		isSupplierEntered = null;
		isBillAmountEntered = null;
		isTypeEntered = null;
		typeList.clear();
		typeList.add("ITEMS");
		typeList.add("EXPENSES");
		location = Constants.GLOBAL_BLANK;
		supplierName = Constants.GLOBAL_BLANK;
		taxRate = 0d;
		taxType = Constants.GLOBAL_BLANK;
		isTaxCodeEntered = null;
		isConversionDateEntered = null;
		tempSupplierName = null;
		memo = null;
		filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;

		invtInscribedStock = false;
		invtTreasuryBill = false;
		invtNextRunDate = null;
		invtSettlementDate = null;
		invtMaturityDate = null;
		invtBidYield = null;
		invtCouponRate = null;
		invtSettlementAmount = null;
		invtFaceValue = null;
		invtPremiumAmount = null;



		//showViewAttachmentButton1 = false;
		//showViewAttachmentButton2 = false;
		//showViewAttachmentButton3 = false;
		//showViewAttachmentButton4 = false;
		attachment = null;
		attachmentPDF = null;

               // isInvestor = false;
		for (int i=0; i<apCLIList.size(); i++) {

			ApDirectCheckLineItemList actionList = (ApDirectCheckLineItemList)apCLIList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);

		}

	}

	  public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		   //ActionErrors errors = new ActionErrors();

		   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		   String separator = "$";

		   // Remove first $ character
		   misc = misc.substring(1);

		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;
		   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		   System.out.println("ctr :" + ctr);

		   /*if(y==0)
			   return new ArrayList();*/

		   ArrayList miscList = new ArrayList();

		   for(int x=0; x<ctr; x++) {
			   try {

				   // Date
				   start = nextIndex + 1;
				   nextIndex = misc.indexOf(separator, start);
				   length = nextIndex - start;
				   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				   String checker = misc.substring(start, start + length);
				   if(checker!=""&& checker != " "){
					   miscList.add(checker);
				   }else{
					   miscList.add("null");
				   }

				   //System.out.println(misc.substring(start, start + length));
			   } catch (Exception ex) {
				   ex.printStackTrace();
			   }

		   	   }
		   return miscList;
	   }


	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("saveSubmitButton") != null ||
				request.getParameter("saveAsDraftButton") != null ||
				request.getParameter("cvPrintButton") != null ||
				request.getParameter("printButton") != null ||
				request.getParameter("journalButton") != null) {

			if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showBatchName){
				errors.add("batchName",
						new ActionMessage("directCheckEntry.error.batchNameRequired"));
			}

			if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("supplier",
						new ActionMessage("directCheckEntry.error.supplierRequired"));
			}

			if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("bankAccount",
						new ActionMessage("directCheckEntry.error.bankAccountRequired"));
			}

			if(Common.validateRequired(date)){
				errors.add("date",
						new ActionMessage("directCheckEntry.error.dateRequired"));
			}

			if(!Common.validateDateFormat(date)){
				errors.add("date",
						new ActionMessage("directCheckEntry.error.dateInvalid"));
			}

			System.out.println(date.lastIndexOf("/"));
			System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			if(year<1900){
				errors.add("date",
						new ActionMessage("directCheckEntry.error.dateInvalid"));
			}

			if(Common.validateRequired(checkDate)){
				errors.add("checkDate",
						new ActionMessage("directCheckEntry.error.checkDateRequired"));
			}

			if(!Common.validateDateFormat(checkDate)){
				errors.add("checkDate",
						new ActionMessage("directCheckEntry.error.checkDateInvalid"));
			}

			if(type.equals("EXPENSES") && Common.validateRequired(billAmount)){
				errors.add("billAmount",
						new ActionMessage("directCheckEntry.error.billAmountRequired"));
			}

			if(type.equals("EXPENSES") && !Common.validateMoneyFormat(billAmount)){
				errors.add("billAmount",
						new ActionMessage("directCheckEntry.error.billAmountInvalid"));
			}



			if(type.equals("EXPENSES") && Common.validateRequired(amount)){
				errors.add("billAmount",
						new ActionMessage("directCheckEntry.error.billAmountRequired"));
			}

			if(type.equals("EXPENSES") && !Common.validateMoneyFormat(amount)){
				errors.add("billAmount",
						new ActionMessage("directCheckEntry.error.billAmountInvalid"));
			}

			if(type.equals("EXPENSES") && Common.validateMoneyFormat(amount)){

				double amnt  = Common.convertStringMoneyToDouble(amount,Constants.MONEY_RATE_PRECISION);


				if(amnt <=0){
					errors.add("billAmount",
							new ActionMessage("directCheckEntry.error.billAmountCannotBeZero"));
				}

			}



			if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("taxCode",
						new ActionMessage("directCheckEntry.error.taxCodeRequired"));
			}

			if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("withholdingTax",
						new ActionMessage("directCheckEntry.error.withholdingTaxRequired"));
			}

			if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("currency",
						new ActionMessage("directCheckEntry.error.currencyRequired"));
			}

			if(!Common.validateDateFormat(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("directCheckEntry.error.conversionDateInvalid"));
			}

			if(!Common.validateMoneyRateFormat(conversionRate)){
				errors.add("conversionRate",
						new ActionMessage("directCheckEntry.error.conversionRateInvalid"));
			}

			if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("directCheckEntry.error.conversionDateMustBeNull"));
			}

			if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
					Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
				errors.add("conversionRate",
						new ActionMessage("directCheckEntry.error.conversionRateMustBeNull"));
			}

			if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
					Common.validateRequired(conversionDate)){
				errors.add("conversionRate",
						new ActionMessage("directCheckEntry.error.conversionRateOrDateMustBeEntered"));
			}

			if(type.equals("EXPENSES")) {

				int numberOfLines = 0;

				Iterator i = apDCList.iterator();

				while (i.hasNext()) {

					ApDirectCheckEntryList drList = (ApDirectCheckEntryList)i.next();

					if (Common.validateRequired(drList.getAccount()) &&
							Common.validateRequired(drList.getDebitAmount()) &&
							Common.validateRequired(drList.getCreditAmount())) continue;

					numberOfLines++;

					if(Common.validateRequired(drList.getAccount())){
						errors.add("account",
								new ActionMessage("directCheckEntry.error.accountRequired", drList.getLineNumber()));
					}


					if(!Common.validateMoneyFormat(drList.getDebitAmount() == null ? "0": drList.getDebitAmount())){
						errors.add("debitAmount",
								new ActionMessage("directCheckEntry.error.debitAmountInvalid", drList.getLineNumber()));
					}

					System.out.println("drList.getCreditAmount()="+drList.getCreditAmount());

					if(!Common.validateMoneyFormat( drList.getCreditAmount() == null ? "0": drList.getCreditAmount() )        ){
						errors.add("creditAmount",
								new ActionMessage("directCheckEntry.error.creditAmountInvalid", drList.getLineNumber()));
					}
					if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
						errors.add("journalLineAmounts",
								new ActionMessage("directCheckEntry.error.debitCreditAmountRequired", drList.getLineNumber()));
					}
					if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
						errors.add("journalLineAmounts",
								new ActionMessage("directCheckEntry.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
					}


				}

				if (numberOfLines == 0) {

					errors.add("supplier",
							new ActionMessage("directCheckEntry.error.checkMustHaveLine"));

				}



                                if(isInvestment){
                                  if((invtInscribedStock && invtTreasuryBill) || (!invtInscribedStock && !invtTreasuryBill))  {

                                    errors.add("investmentType", new ActionMessage("directCheckEntry.error.selectTypeInvestment"));


                                  }


                                  if(invtInscribedStock || invtTreasuryBill){

                                      if(!Common.validateDateFormat(invtNextRunDate)){
                                           errors.add("nextRunDate", new ActionMessage("directCheckEntry.error.dateInvalidNextRunDate"));
                                      }

                                      if(!Common.validateDateFormat(invtSettlementDate)){
                                           errors.add("settlementDate", new ActionMessage("directCheckEntry.error.dateInvalidSettlementDate"));
                                      }

                                      if(!Common.validateDateFormat(invtMaturityDate)){
                                           errors.add("maturityDate", new ActionMessage("directCheckEntry.error.dateInvalidMaturity"));
                                      }

                                      if(!Common.validateMoneyFormat(invtBidYield)){
                                           errors.add("bidYield", new ActionMessage("directCheckEntry.error.moneyInvalidFormatBidYield"));
                                      }

                                      if(!Common.validateMoneyFormat(invtCouponRate)){
                                           errors.add("couponRate", new ActionMessage("directCheckEntry.error.moneyInvalidFormatCouponRate"));
                                      }

                                      if(!Common.validateMoneyFormat(invtSettlementAmount)){
                                           errors.add("settlementAmount", new ActionMessage("directCheckEntry.error.moneyInvalidFormatSettlementAmount"));
                                      }

                                      if(!Common.validateMoneyFormat(invtFaceValue)){
                                           errors.add("faceValue", new ActionMessage("directCheckEntry.error.moneyInvalidFormatFaceValue"));
                                      }

                                      if(!Common.validateMoneyFormat(invtPremiumAmount)){
                                           errors.add("premiumAmount", new ActionMessage("directCheckEntry.error.moneyInvalidFormatPremiumAmount"));
                                      }

                                  }


                                }



			} else {

				int numberOfLines = 0;

				Iterator i = apCLIList.iterator();

				while (i.hasNext()) {

					ApDirectCheckLineItemList cliList = (ApDirectCheckLineItemList)i.next();

					if (Common.validateRequired(cliList.getLocation()) &&
							Common.validateRequired(cliList.getItemName()) &&
							Common.validateRequired(cliList.getQuantity()) &&
							Common.validateRequired(cliList.getUnit()) &&
							Common.validateRequired(cliList.getUnitCost())) continue;

					numberOfLines++;
					/*
					try{
		      	 		String separator = "$";
		      	 		String misc = "";
		      		   // Remove first $ character
		      		   misc = cliList.getMisc().substring(1);

		      		   // Counter
		      		   int start = 0;
		      		   int nextIndex = misc.indexOf(separator, start);
		      		   int length = nextIndex - start;
		      		   int counter;
		      		   counter = Integer.parseInt(misc.substring(start, start + length));

		      	 		ArrayList miscList = this.getExpiryDateStr(cliList.getMisc(), counter);
		      	 		System.out.println("rilList.getMisc() : " + cliList.getMisc());
		      	 		Iterator mi = miscList.iterator();

		      	 		int ctrError = 0;
		      	 		int ctr = 0;

		      	 		while(mi.hasNext()){
		      	 				String miscStr = (String)mi.next();

		      	 				if(!Common.validateDateFormat(miscStr)){
		      	 					errors.add("date",
		      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
		      	 					ctrError++;
		      	 				}

		      	 				System.out.println("miscStr: "+miscStr);
		      	 				if(miscStr=="null"){
		      	 					ctrError++;
		      	 				}
		      	 		}
		      	 		//if ctr==Error => No Date Will Apply
		      	 		//if ctr>error => Invalid Date
		      	 		System.out.println("CTR: "+  miscList.size());
		      	 		System.out.println("ctrError: "+  ctrError);
		      	 		System.out.println("counter: "+  counter);
		      	 			if(ctrError>0 && ctrError!=miscList.size()){
		      	 				errors.add("date",
		      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
		      	 			}

		      	 		//if error==0 Add Expiry Date


		      	 	}catch(Exception ex){
		  	  	    	ex.printStackTrace();
		  	  	    }*/


					if(Common.validateRequired(cliList.getLocation()) || cliList.getLocation().equals(Constants.GLOBAL_BLANK)){
						errors.add("location",
								new ActionMessage("directCheckEntry.error.locationRequired", cliList.getLineNumber()));
					}
					if(Common.validateRequired(cliList.getItemName()) || cliList.getItemName().equals(Constants.GLOBAL_BLANK)){
						errors.add("itemName",
								new ActionMessage("directCheckEntry.error.itemNameRequired", cliList.getLineNumber()));
					}
					if(Common.validateRequired(cliList.getQuantity())) {
						errors.add("quantity",
								new ActionMessage("directCheckEntry.error.quantityRequired", cliList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(cliList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("directCheckEntry.error.quantityInvalid", cliList.getLineNumber()));
					}
					if(!Common.validateRequired(cliList.getQuantity()) && Common.convertStringMoneyToDouble(cliList.getQuantity(), (short)3) <= 0) {
						errors.add("quantity",
								new ActionMessage("directCheckEntry.error.negativeOrZeroQuantityNotAllowed", cliList.getLineNumber()));
					}
					if(Common.validateRequired(cliList.getUnit()) || cliList.getUnit().equals(Constants.GLOBAL_BLANK)){
						errors.add("unit",
								new ActionMessage("directCheckEntry.error.unitRequired", cliList.getLineNumber()));
					}
					if(Common.validateRequired(cliList.getUnitCost())){
						errors.add("unitCost",
								new ActionMessage("directCheckEntry.error.unitCostRequired", cliList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(cliList.getUnitCost())){
						errors.add("unitCost",
								new ActionMessage("directCheckEntry.error.unitCostInvalid", cliList.getLineNumber()));
					}

				}

				if (numberOfLines == 0) {

					errors.add("supplier",
							new ActionMessage("directCheckEntry.error.checkMustHaveItemLine"));

				}

			}


		} else if (!Common.validateRequired(request.getParameter("isSupplierEntered"))) {

			if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("supplier",
						new ActionMessage("directCheckEntry.error.supplierRequired"));
			}

		} else if (!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {

			if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("supplier",
						new ActionMessage("directCheckEntry.error.supplierRequired"));
			}

			if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("bankAccount",
						new ActionMessage("directCheckEntry.error.bankAccountRequired"));
			}

			if(Common.validateRequired(billAmount)){
				errors.add("billAmount",
						new ActionMessage("directCheckEntry.error.billAmountRequired"));
			}

			System.out.println("billAmount="+billAmount);
			if(!Common.validateMoneyFormat(billAmount)){

                            System.out.println("invalid billAmount="+billAmount);
				errors.add("billAmount",
						new ActionMessage("directCheckEntry.error.billAmountInvalid"));
			}

			if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("taxCode",
						new ActionMessage("directCheckEntry.error.taxCodeRequired"));
			}

			if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("withholdingTax",
						new ActionMessage("directCheckEntry.error.withholdingTaxRequired"));
			}

		} else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

			if(!Common.validateDateFormat(conversionDate)){

				errors.add("conversionDate", new ActionMessage("directCheckEntry.error.conversionDateInvalid"));

			}

		}

		return(errors);
	}
}
