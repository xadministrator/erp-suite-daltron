package com.struts.ap.findcheck;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApFindCheckController;
import com.ejb.txn.ApFindCheckControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModCheckDetails;

public final class ApFindCheckAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApFindCheckAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApFindCheckForm actionForm = (ApFindCheckForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_FIND_CHECK_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apFindCheck");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize ApFindCheckController EJB
*******************************************************/

         ApFindCheckControllerHome homeFC = null;
         ApFindCheckController ejbFC = null;

         try {

            homeFC = (ApFindCheckControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApFindCheckControllerEJB", ApFindCheckControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApFindCheckAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFC = homeFC.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApFindCheckAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
 /*******************************************************
     Call ApFindCheckController EJB
     getGlFcPrecisionUnit
  *******************************************************/         
         short precisionUnit = 0;
         boolean enableCheckBatch = false;
         boolean useSupplierPulldown = true;
         String checkType = null;
         
         try { 
         	
            precisionUnit = ejbFC.getGlFcPrecisionUnit(user.getCmpCode());
            checkType = ejbFC.getAdPrfDefaultCheckType(user.getCmpCode());
            enableCheckBatch = Common.convertByteToBoolean(ejbFC.getAdPrfEnableApCheckBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableCheckBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbFC.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
           
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApFindCheckAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ap FC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apFindCheck"));
	     
/*******************************************************
   -- Ap FC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apFindCheck"));                         

/*******************************************************
    -- Ap FC First Action --
*******************************************************/ 

	     } else if (request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);	        
	        
/*******************************************************
   -- Ap FC Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ap FC Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Ap FC Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCheckType())) {
	        		
	        		criteria.put("checkType", actionForm.getCheckType());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	        	}	        	

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getNumberFrom())) {
	        		
	        		criteria.put("checkNumberFrom", actionForm.getNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getNumberTo())) {
	        		
	        		criteria.put("checkNumberTo", actionForm.getNumberTo());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReferenceNumberFrom())) {
	        		
	        		criteria.put("referenceNumberFrom", actionForm.getReferenceNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReferenceNumberTo())) {
	        		
	        		criteria.put("referenceNumberTo", actionForm.getReferenceNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	 
	        	if (!Common.validateRequired(actionForm.getReleased())) {
	        			        	   		        		
	        		criteria.put("released", actionForm.getReleased());
	        	
	            }
	            
	            if (actionForm.getCheckVoid()) {	        	
		        	   		        		
	        		criteria.put("checkVoid", new Byte((byte)1));
                
                }
                
                if (!actionForm.getCheckVoid()) {
                	
                	criteria.put("checkVoid", new Byte((byte)0));
                	
                }
	        	
	            if (!actionForm.getCheckPrinted()) {
	        		criteria.put("checkPrinted", new Byte((byte)0));
                
                }
	        
                
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFC.getApChkSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            try {
            	
            	actionForm.clearApFCList();
            	
            	ArrayList list = ejbFC.getApChkByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModCheckDetails mdetails = (ApModCheckDetails)i.next();
      		
            		ApFindCheckList apFCList = new ApFindCheckList(actionForm,
            		    mdetails.getChkCode(),
            		    mdetails.getChkType(),
            		    mdetails.getChkSplName(),
            		    mdetails.getChkBaName(),
            		    Common.convertSQLDateToString(mdetails.getChkDate()),
            		    mdetails.getChkNumber(),
            		    mdetails.getChkDocumentNumber(),
            		    mdetails.getChkReferenceNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getChkReleased()));
            			
            		actionForm.saveApFCList(apFCList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findCheck.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApFindCheckAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apFindCheck");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("apFindCheck"));

/*******************************************************
   -- Ap FC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap FC Open Action --
*******************************************************/

         } else if (request.getParameter("apFCList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             ApFindCheckList apFCList =
                actionForm.getApFCByIndex(actionForm.getRowSelected());
             
             String path = null;
             
             if (apFCList.getCheckType().equals("PAYMENT")) {
          
	  	         path = "/apPaymentEntry.do?forward=1" +
				     "&checkCode=" + apFCList.getCheckCode();
		      
		     } else { // direct check
		     	
		     	 path = "/apDirectCheckEntry.do?forward=1" +
				     "&checkCode=" + apFCList.getCheckCode();
				     
		     } 
		     
			 return(new ActionForward(path));

/*******************************************************
   -- Ap FC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearApFCList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseSupplierPulldown()) {
            		
	            	actionForm.clearSupplierCodeList();
	            	
	            	list = ejbFC.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierCodeList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
            	}

            	actionForm.clearBankAccountList();
            	
            	list = ejbFC.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbFC.getApOpenCbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApFindCheckAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            System.out.println("actionForm.getTableType()="+actionForm.getTableType());
            System.out.println("request.getParameter('findDirectDraft')="+request.getParameter("findDirectDraft"));
            System.out.println("request.getParameter('findDraft')="+request.getParameter("findDraft"));
            if (actionForm.getTableType() == null || request.getParameter("findDraft") != null || request.getParameter("findDirectDraft") != null || request.getParameter("findChecksForPrinting") != null) {
	        	

            	if (request.getParameter("findChecksForPrinting") != null) {
            	
	            	actionForm.setBatchName(Constants.GLOBAL_BLANK);
		        	actionForm.setSupplierCode(Constants.GLOBAL_BLANK);
		        	actionForm.setBankAccount(Constants.GLOBAL_BLANK);
		        	actionForm.setCheckVoid(false);
		        	actionForm.setCheckPrinted(false);
		        	actionForm.setDateFrom(null);
		        	actionForm.setDateTo(null);
		        	actionForm.setNumberFrom(null);
		        	actionForm.setNumberTo(null);
		        	actionForm.setDocumentNumberFrom(null);
		        	actionForm.setDocumentNumberTo(null);
		        	actionForm.setReferenceNumberFrom(null);
		        	actionForm.setReferenceNumberTo(null);
		        	actionForm.setReleased(Constants.GLOBAL_YES); 
		        	actionForm.setApprovalStatus("APPROVED");
		        	actionForm.setPosted(Constants.GLOBAL_YES);
		        	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
		        
		        	actionForm.setLineCount(0);
		            actionForm.setDisableNextButton(true);
		            actionForm.setDisablePreviousButton(true);
		            actionForm.setDisableFirstButton(true);
		            actionForm.setDisableLastButton(true);
		            actionForm.reset(mapping, request);
		            
		            actionForm.setCheckType(Constants.GLOBAL_BLANK);
		            
		            HashMap criteria = new HashMap();
		            criteria.put("checkVoid", new Byte((byte)0));
		            criteria.put("checkPrinted", new Byte((byte)0));
		            criteria.put("posted", actionForm.getPosted());
		            criteria.put("approvalStatus", actionForm.getApprovalStatus());
		            
		            actionForm.setCriteria(criteria);
		            
		        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        	
            	} else if (actionForm.getTableType() == null && request.getParameter("findDraft") != null) {
            	
	            	actionForm.setBatchName(Constants.GLOBAL_BLANK);
		        	actionForm.setSupplierCode(Constants.GLOBAL_BLANK);
		        	actionForm.setBankAccount(Constants.GLOBAL_BLANK);
		        	actionForm.setCheckVoid(false);
		        	actionForm.setCheckPrinted(true);
		        	actionForm.setDateFrom(null);
		        	actionForm.setDateTo(null);
		        	actionForm.setNumberFrom(null);
		        	actionForm.setNumberTo(null);
		        	actionForm.setDocumentNumberFrom(null);
		        	actionForm.setDocumentNumberTo(null);
		        	actionForm.setReferenceNumberFrom(null);
		        	actionForm.setReferenceNumberTo(null);
		        	actionForm.setReleased(Constants.GLOBAL_NO); 
		        	actionForm.setApprovalStatus("DRAFT");
		        	actionForm.setPosted(Constants.GLOBAL_NO);
		        	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
		        	actionForm.setCheckType("PAYMENT");
		        
		        	actionForm.setLineCount(0);
		            actionForm.setDisableNextButton(true);
		            actionForm.setDisablePreviousButton(true);
		            actionForm.setDisableFirstButton(true);
		            actionForm.setDisableLastButton(true);
		            actionForm.reset(mapping, request);
		            
		            
		            
		            HashMap criteria = new HashMap();
		            criteria.put("checkType", actionForm.getCheckType());
		            criteria.put("checkVoid", new Byte((byte)0));
		            criteria.put("posted", actionForm.getPosted());
		            criteria.put("approvalStatus", actionForm.getApprovalStatus());
		            
		            actionForm.setCriteria(criteria);
		            
		        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        	
            	}else if (actionForm.getTableType() == null && request.getParameter("findDirectDraft") != null) {
            		
	            	actionForm.setBatchName(Constants.GLOBAL_BLANK);
		        	actionForm.setSupplierCode(Constants.GLOBAL_BLANK);
		        	actionForm.setBankAccount(Constants.GLOBAL_BLANK);
		        	actionForm.setCheckVoid(false);
		        	actionForm.setCheckPrinted(true);
		        	actionForm.setDateFrom(null);
		        	actionForm.setDateTo(null);
		        	actionForm.setNumberFrom(null);
		        	actionForm.setNumberTo(null);
		        	actionForm.setDocumentNumberFrom(null);
		        	actionForm.setDocumentNumberTo(null);
		        	actionForm.setReferenceNumberFrom(null);
		        	actionForm.setReferenceNumberTo(null);
		        	actionForm.setReleased(Constants.GLOBAL_NO); 
		        	actionForm.setApprovalStatus("DRAFT");
		        	actionForm.setPosted(Constants.GLOBAL_NO);
		        	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
		        	actionForm.setCheckType("DIRECT");
		        
		        	actionForm.setLineCount(0);
		            actionForm.setDisableNextButton(true);
		            actionForm.setDisablePreviousButton(true);
		            actionForm.setDisableFirstButton(true);
		            actionForm.setDisableLastButton(true);
		            actionForm.reset(mapping, request);
		            
		            
		            
		            HashMap criteria = new HashMap();
		            criteria.put("checkType", actionForm.getCheckType());
		            System.out.println("actionForm.getCheckType()======"+actionForm.getCheckType());
		            criteria.put("checkVoid", new Byte((byte)0));
		            criteria.put("posted", actionForm.getPosted());
		            criteria.put("approvalStatus", actionForm.getApprovalStatus());
		            
		            actionForm.setCriteria(criteria);
		            
		        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        	
            	}
            	
	        	if(request.getParameter("findDraft") != null || request.getParameter("findDirectDraft") != null ||request.getParameter("findChecksForPrinting") != null) {
	            	
	            	return new ActionForward("/apFindCheck.do?goButton=1");
	            	
	            }
	          
            } else {
            	
            	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("checkVoid")) {
            		
            		Byte b = (Byte)c.get("checkVoid");
            		
            		actionForm.setCheckVoid(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	try {
                	
                	actionForm.clearApFCList();
                	
                	ArrayList newList = ejbFC.getApChkByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	newList.remove(newList.size() - 1);
    	           	
    	           }
    	           
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
                		ApModCheckDetails mdetails = (ApModCheckDetails)j.next();
          		
                		ApFindCheckList apFCList = new ApFindCheckList(actionForm,
                		    mdetails.getChkCode(),
                		    mdetails.getChkType(),
                		    mdetails.getChkSplName(),
                		    mdetails.getChkBaName(),
                		    Common.convertSQLDateToString(mdetails.getChkDate()),
                		    mdetails.getChkNumber(),
                		    mdetails.getChkDocumentNumber(),
                		    mdetails.getChkReferenceNumber(),
                		    Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit),
                		    Common.convertByteToBoolean(mdetails.getChkReleased()));
                		    
                		actionForm.saveApFCList(apFCList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev next buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findCheck.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in ApFindCheckAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }
                        
            return(mapping.findForward("apFindCheck"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApFindCheckAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}