package com.struts.ap.findcheck;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApFindCheckForm extends ActionForm implements Serializable {
   
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();	
   private String checkType = null;
   private ArrayList checkTypeList = new ArrayList();
   private boolean checkVoid = false;
   private boolean checkPrinted = true;
   private String supplierCode = null;   
   private ArrayList supplierCodeList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String numberFrom = null;
   private String numberTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   
   private String referenceNumberFrom = null;
   private String referenceNumberTo = null;
   
   private String released = null;
   private ArrayList releasedList = new ArrayList();
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private boolean useSupplierPulldown = true;
   private String pageState = new String();
   private ArrayList apFCList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ApFindCheckList getApFCByIndex(int index) {

      return((ApFindCheckList)apFCList.get(index));

   }

   public Object[] getApFCList() {

      return apFCList.toArray();

   }

   public int getApFCListSize() {

      return apFCList.size();

   }

   public void saveApFCList(Object newApFCList) {

      apFCList.add(newApFCList);

   }

   public void clearApFCList() {
   	
      apFCList.clear();
      
   }

   public void setRowSelected(Object selectedApFCList, boolean isEdit) {

      this.rowSelected = apFCList.indexOf(selectedApFCList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getCheckType() {
   	
   	  return checkType;
   	  
   }
   
   public void setCheckType(String checkType) {
   	
   	  this.checkType = checkType;
   	  
   }                         

   public ArrayList getCheckTypeList() {

      return checkTypeList;

   }
   
   public boolean getCheckVoid() {
   	
   	  return checkVoid;
   	
   }
   
   public void setCheckVoid(boolean checkVoid) {
   	
   	  this.checkVoid = checkVoid;
   	
   }
   
   public boolean getCheckPrinted() {
   	
   	  return checkPrinted;
   	
   }
   
   public void setCheckPrinted(boolean checkPrinted) {
   	
   	  this.checkPrinted = checkPrinted;
   	
   }

   public String getSupplierCode() {

      return supplierCode;

   }

   public void setSupplierCode(String supplierCode) {

      this.supplierCode = supplierCode;

   }

   public ArrayList getSupplierCodeList() {

      return supplierCodeList;

   }

   public void setSupplierCodeList(String supplierCode) {

      supplierCodeList.add(supplierCode);

   }
   
   public void clearSupplierCodeList() {

      supplierCodeList.clear();
      supplierCodeList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getBankAccount() {

      return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

      this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

      return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

      bankAccountList.add(bankAccount);

   }
   
   public void clearBankAccountList() {

      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }

   public String getNumberFrom() {

      return numberFrom;

   }

   public void setNumberFrom(String numberFrom) {

      this.numberFrom = numberFrom;

   }                         

   public String getNumberTo() {

      return numberTo;

   }

   public void setNumberTo(String numberTo) {

      this.numberTo = numberTo;

   }

   public String getDocumentNumberFrom() {

      return documentNumberFrom;

   }

   public void setDocumentNumberFrom(String documentNumberFrom) {

      this.documentNumberFrom = documentNumberFrom;

   }                         

   public String getDocumentNumberTo() {

      return documentNumberTo;

   }

   public void setDocumentNumberTo(String documentNumberTo) {

      this.documentNumberTo = documentNumberTo;

   } 
   
   
   
   public String getReferenceNumberFrom() {

      return referenceNumberFrom;

   }

   public void setReferenceNumberFrom(String referenceNumberFrom) {

      this.referenceNumberFrom = referenceNumberFrom;

   }                         

   public String getReferenceNumberTo() {

      return referenceNumberTo;

   }

   public void setReferenceNumberTo(String referenceNumberTo) {

      this.referenceNumberTo = referenceNumberTo;

   }
	   
 
   public String getReleased() {
   	
   	  return released;
   	  
   }
   
   public void setReleased(String released) {
   	
   	  this.released = released;
   	  
   }
   
   public ArrayList getReleasedList() {
   	
   	  return releasedList;
   
   }
   
   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }
   
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }       
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }  
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  
   	  checkVoid = false;
   	  checkPrinted=true;
   	  if (request.getParameter("findChecksForPrinting")!=null) checkPrinted = false;
   	  
      checkTypeList.clear();
      checkTypeList.add(Constants.GLOBAL_BLANK);
      checkTypeList.add(Constants.AP_FC_CHECK_TYPE_PAYMENT);
      checkTypeList.add(Constants.AP_FC_CHECK_TYPE_DIRECT);        
      
      releasedList.clear();
      releasedList.add(Constants.GLOBAL_BLANK);
      releasedList.add(Constants.GLOBAL_YES);
      releasedList.add(Constants.GLOBAL_NO);
        	  
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
           
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add(Constants.GLOBAL_YES);
      postedList.add(Constants.GLOBAL_NO);
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AP_FC_ORDER_BY_BANK_ACCOUNT);
	      orderByList.add(Constants.AP_FC_ORDER_BY_SUPPLIER_CODE);
	      orderByList.add(Constants.AP_FC_ORDER_BY_CHECK_NUMBER);
	      orderByList.add(Constants.AP_FC_ORDER_BY_DOCUMENT_NUMBER);
	      orderByList.add(Constants.AP_FC_ORDER_BY_REFERENCE_NUMBER);
	  
	  }     
	  
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

         if (!Common.validateDateFormat(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("findCheck.error.dateFromInvalid"));

         }         
         
         if (!Common.validateDateFormat(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("findCheck.error.dateToInvalid"));

         }                  

      }
      
      return errors;

   }
}