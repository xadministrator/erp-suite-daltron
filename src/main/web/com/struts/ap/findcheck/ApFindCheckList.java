package com.struts.ap.findcheck;

import java.io.Serializable;

public class ApFindCheckList implements Serializable {

   private Integer checkCode = null;
   private String checkType = null;
   private String supplierName = null;
   private String bankAccount = null;
   private String date = null;
   private String checkNumber = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String amount = null;
   private boolean released = false;
   
   private String openButton = null;
       
   private ApFindCheckForm parentBean;
    
   public ApFindCheckList(ApFindCheckForm parentBean,
	  Integer checkCode,
	  String checkType,  
	  String supplierName,  
	  String bankAccount,  
	  String date,  
	  String checkNumber,
	  String documentNumber,  
	  String referenceNumber,
	  String amount,  
	  boolean released) {

      this.parentBean = parentBean;
      this.checkCode = checkCode;
      this.checkType = checkType;
      this.supplierName = supplierName;
      this.bankAccount = bankAccount;
      this.date = date;
      this.checkNumber = checkNumber;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amount = amount;
      this.released = released;
      
   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }
   
   public Integer getCheckCode() {
   	
   	  return checkCode;
   	  
   }
   
   public String getCheckType() {
   	
   	  return checkType;
   	  
   }

   public String getSupplierName() {

      return supplierName;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	 
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
	   	
   	  return referenceNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public boolean getReleased() {
   	
   	  return released;
   	  
   }
      
}