package com.struts.ap.findcheckbatch;

import java.io.Serializable;

public class ApFindCheckBatchList implements Serializable {

   private Integer checkBatchCode = null;
   private String batchName = null;
   private String description = null;
   private String status = null;
   private String dateCreated = null;
   private String createdBy = null;

   private String openButton = null;
    
   private ApFindCheckBatchForm parentBean;
    
   public ApFindCheckBatchList(ApFindCheckBatchForm parentBean,
      Integer checkBatchCode,
      String batchName,
      String description,
      String status,
      String dateCreated,
      String createdBy){

      this.parentBean = parentBean;
      this.checkBatchCode = checkBatchCode;
      this.batchName = batchName;
      this.description = description;
      this.status = status;
      this.dateCreated = dateCreated;
      this.createdBy = createdBy;
      
   }

   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }

   public Integer getCheckBatchCode(){
      return(checkBatchCode);
   }

   public String getBatchName(){
      return(batchName);
   }

   public String getDescription(){
      return(description);
   }

   public String getStatus(){
      return(status);
   }
   
   public String getDateCreated(){
      return(dateCreated);
   }

   public String getCreatedBy(){
      return(createdBy);
   }
   
}
