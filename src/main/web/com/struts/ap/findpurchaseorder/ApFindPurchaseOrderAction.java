package com.struts.ap.findpurchaseorder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApFindPurchaseOrderController;
import com.ejb.txn.ApFindPurchaseOrderControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModPurchaseOrderDetails;

public final class ApFindPurchaseOrderAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 	Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApFindPurchaseOrderAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApFindPurchaseOrderForm actionForm = (ApFindPurchaseOrderForm)form;
			String frParam = null;
			
			if (request.getParameter("child") == null) {
		         
		         	frParam = Common.getUserPermission(user, Constants.AP_FIND_PURCHASE_ORDER_ID);
		         
		    } else {
		         	
		         	frParam = Constants.FULL_ACCESS;
		         	
		    }
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						if (request.getParameter("child") == null) {

							return mapping.findForward("apFindPurchaseOrder");
		                  
		                } else {
		                  	
		                  	return mapping.findForward("apFindPurchaseOrderChild");
		                  
		                }
												
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
 	Initialize ApFindPurchaseOrderController EJB
*******************************************************/
			
			ApFindPurchaseOrderControllerHome homeFPO = null;
			ApFindPurchaseOrderController ejbFPO = null;
			
			try {
				
				homeFPO = (ApFindPurchaseOrderControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ApFindPurchaseOrderControllerEJB", ApFindPurchaseOrderControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in ApFindPurchaseOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFPO = homeFPO.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in ApFindPurchaseOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();  

/*******************************************************
   Call ApFindPurchaseOrderController EJB
   getAdPrfApUseSupplierPulldown
*******************************************************/

			boolean useSupplierPulldown = true;
                        boolean enableVoucherBatch = true;
			
			try {
				
				useSupplierPulldown = Common.convertByteToBoolean(ejbFPO.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
				actionForm.setUseSupplierPulldown(useSupplierPulldown);
                                
                                enableVoucherBatch = Common.convertByteToBoolean(ejbFPO.getAdPrfEnableApPOBatch(user.getCmpCode()));
                                actionForm.setShowBatchName(enableVoucherBatch);
				
			} catch(EJBException ex) {
	         	
	            if (log.isInfoEnabled()) {
	            	
	               log.info("EJBException caught in ApFindPurchaseOrderAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	            }
	            
	            return(mapping.findForward("cmnErrorPage"));
	            
	         }
			
/*******************************************************
	 -- Ap FPO Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
												
				if (request.getParameter("child") == null) {
		        	
					return(mapping.findForward("apFindPurchaseOrder"));
		        	
		        } else {
		        	
		        	return mapping.findForward("apFindPurchaseOrderChild");
		        	
		        }
				
/*******************************************************
     -- Ap FPO Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				if (request.getParameter("child") == null) {
		        	
					return(mapping.findForward("apFindPurchaseOrder"));
		        	
		        } else {
		        	
		        	return mapping.findForward("apFindPurchaseOrderChild");
		        	
		        }                         
				
/*******************************************************
	-- Ap FPO First Action --
*******************************************************/ 
							
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);
				
/*******************************************************
	-- Ap FPO Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
	-- Ap FPO Next Action --
*******************************************************/ 
				
			}else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
			} 
			
/*******************************************************
	-- Ap FPO Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null || request.getParameter("forward") != null || 
					request.getParameter("forwardProc") != null || request.getParameter("findRejected") != null || request.getParameter("findReceivingRejected") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
                                        
                    if (!Common.validateRequired(actionForm.getBatchName()) && actionForm.getShowBatchName()) {
						
						criteria.put("batchName", actionForm.getBatchName());
						
					}
                                        
					if (!Common.validateRequired(actionForm.getSupplierCode())) {
						
						criteria.put("supplierCode", actionForm.getSupplierCode());
						
					}
					
					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
						
						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
						
						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getReferenceNumber())) {
						
						criteria.put("referenceNumber", actionForm.getReferenceNumber());
						
					}
					
					if (!Common.validateRequired(actionForm.getSerialNumber())) {
						
						criteria.put("serialNumber", actionForm.getSerialNumber().trim());
						
					}
					
					if (!Common.validateRequired(actionForm.getDateFrom())) {
						
						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getDateTo())) {
						
						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
						
					}
					
					if (!Common.validateRequired(actionForm.getCurrency())) {
						
						criteria.put("currency", actionForm.getCurrency());
						
					}
					
					if (!Common.validateRequired(actionForm.getType())) {
						
						criteria.put("type", actionForm.getType());
						
					}
					
					if(request.getParameter("child") == null) {
						
						if (!Common.validateRequired(actionForm.getApprovalStatus())) {
							
							criteria.put("approvalStatus", actionForm.getApprovalStatus());
							
						}
						
						if (!Common.validateRequired(actionForm.getPosted())) {
							
							criteria.put("posted", actionForm.getPosted());
							
						}
						
						if (actionForm.getReceiving()) {	        	
							
							criteria.put("receiving", new Byte((byte)1));
							
						}
						
						if (!actionForm.getReceiving()) {
							
							criteria.put("receiving", new Byte((byte)0));
							
						}
						
						if (actionForm.getPurchaseOrderVoid()) {	        	
							
							criteria.put("purchaseOrderVoid", new Byte((byte)1));
							
						}
						
						if (!actionForm.getPurchaseOrderVoid()) {
							
							criteria.put("purchaseOrderVoid", new Byte((byte)0));
							
						}
						
					} else {
						
						if(actionForm.getIsVoucher() == null) {

							System.out.println("pasok null receiving");
							criteria.put("receiving", new Byte((byte)0));
							
							
						}else {
							
							System.out.println("pasok receiving");
							criteria.put("receiving", new Byte((byte)1));
							/*
							if (actionForm.getReceiving()) {	        	
								
								criteria.put("receiving", new Byte((byte)1));
								
							}
							
							if (!actionForm.getReceiving()) {
								
								criteria.put("receiving", new Byte((byte)0));
								
							}
							*/
						}
						
						criteria.put("purchaseOrderVoid", new Byte((byte)0));
						criteria.put("posted", "YES");
						
					}		

					// save criteria                                       
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
					
				}	else if(request.getParameter("forward") != null){
					System.out.println("FORWARD-----------------------------------------"+request.getParameter("forward"));					  					    					    		                			              					        
					
					
					HashMap criteria = new HashMap();
					
        		   criteria.put("posted", Constants.GLOBAL_YES);
        		   actionForm.setPosted("YES");
        		   criteria.put("receiving", new Byte((byte)0));
        		   actionForm.setApprovalStatus(Constants.GLOBAL_BLANK);
        		   criteria.put("purchaseOrderVoid", new Byte((byte)0));
        		   if (request.getParameter("print") != null) {
	            		
	            		criteria.put("printed", new Byte((byte)0));
	            		
	            	}
	        	   actionForm.setLineCount(0);
	        	   actionForm.setCriteria(criteria);
	        	   actionForm.setOrderBy("DOCUMENT NUMBER");
	        	  
	            	
				
				
				
				} else if(request.getParameter("forwardProc") != null){
					
					HashMap criteria = new HashMap();

	        		   criteria.put("receiving", new Byte((byte)1));
	        		   
	        		   actionForm.setReceiving(true);
	        		   criteria.put("purchaseOrderVoid", new Byte((byte)0));
		        	   actionForm.setLineCount(0);
		        	   actionForm.setCriteria(criteria);
		        	   actionForm.setOrderBy("DOCUMENT NUMBER");
		        	   
				} else if(request.getParameter("findRejected") != null){
					
					HashMap criteria = new HashMap();
					
					criteria.put("receiving", new Byte((byte)0));
					actionForm.setReceiving(false);
					criteria.put("posted", Constants.GLOBAL_NO);
					actionForm.setPosted("NO");   
					criteria.put("approvalStatus", "REJECTED");
					actionForm.setApprovalStatus("REJECTED");
					criteria.put("purchaseOrderVoid", new Byte((byte)0));
	        	    actionForm.setLineCount(0);
	        	    actionForm.setCriteria(criteria);
	        	    actionForm.setOrderBy("DOCUMENT NUMBER");
	        	    
				} else if(request.getParameter("findReceivingRejected") != null){
					
					HashMap criteria = new HashMap();
					
					criteria.put("receiving", new Byte((byte)1));
					actionForm.setReceiving(true);
					criteria.put("posted", Constants.GLOBAL_NO);
					actionForm.setPosted("NO");   
					criteria.put("approvalStatus", "REJECTED");
					actionForm.setApprovalStatus("REJECTED");
					criteria.put("purchaseOrderVoid", new Byte((byte)0));
	        	    actionForm.setLineCount(0);
	        	    actionForm.setCriteria(criteria);
	        	    actionForm.setOrderBy("DOCUMENT NUMBER");
				}
				
				
				
				
				if(request.getParameter("lastButton") != null) {
					
					int size = ejbFPO.getApPoSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()),  user.getCmpCode()).intValue();
					
					if((size % Constants.GLOBAL_MAX_LINES) != 0) {
	            		
	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
	            		
	            	} else {
	            		
	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
	            		
	            	}
					
				}
				
				try {
					
					actionForm.clearApFPOList();
					
					ArrayList list = ejbFPO.getApPoByCriteria(actionForm.getCriteria(),
							new Integer(actionForm.getLineCount()), 
							new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), actionForm.getSelectedPoNumber() != null ? true : false, user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {
						
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						ApModPurchaseOrderDetails mdetails = (ApModPurchaseOrderDetails)i.next();
						
						ApFindPurchaseOrderList apFPOList = new ApFindPurchaseOrderList(actionForm,
								mdetails.getPoCode(),
								Common.convertByteToBoolean(mdetails.getPoReceiving()),
								mdetails.getPoSplSupplierCode(),
								Common.convertSQLDateToString(mdetails.getPoDate()),
								mdetails.getPoDocumentNumber(),
								mdetails.getPoReferenceNumber());
						
						actionForm.saveApFPOList(apFPOList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev, next, first & last buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findPurchaseOrder.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApFindPurchaseOrderAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
					if (request.getParameter("child") == null) {
						actionForm.setSelectedPoNumber(null);
						return(mapping.findForward("apFindPurchaseOrder"));
			        	
			        } else {
			            
			            actionForm.setSelectedPoNumber(request.getParameter("selectedPoNumber"));
			            actionForm.setIsVoucher(request.getParameter("selectedPoNumber"));
			        	return mapping.findForward("apFindPurchaseOrderChild");
			        	
			        }
					
				}
				
				
				actionForm.reset(mapping, request);
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}
				
				if (request.getParameter("child") == null) {
		        	
					return(mapping.findForward("apFindPurchaseOrder"));
		        	
		        } else {
		        	
		        	return mapping.findForward("apFindPurchaseOrderChild");
		        	
		        }
				
/*******************************************************
	-- Ap FPO Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
	-- Ap FPO Open Action --
*******************************************************/
				
			} else if (request.getParameter("apFPOList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				ApFindPurchaseOrderList apFPOList =
					actionForm.getApFPOByIndex(actionForm.getRowSelected());
				
				String path = null;
				
				if (apFPOList.getReceiving() == false) {
							    				
					path = "/apPurchaseOrderEntry.do?forward=1" +
					"&purchaseOrderCode=" + apFPOList.getPurchaseOrderCode();
					
				} else {
					
					path = "/apReceivingItemEntry.do?forward=1" +
					"&receivingItemCode=" + apFPOList.getPurchaseOrderCode();
					
				}
				
				return(new ActionForward(path));
				
/*******************************************************
	-- Ap FPO Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				actionForm.clearApFPOList();
				
	            ArrayList list = null;
	            Iterator i = null;
            
	            try {
	            	
	            	if(actionForm.getUseSupplierPulldown()) {
	            		
	            		actionForm.clearSupplierCodeList();           	
	            		
	            		list = ejbFPO.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            		
	            		if (list == null || list.size() == 0) {
	            			
	            			actionForm.setSupplierCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            			
	            		} else {
	            			
	            			i = list.iterator();
	            			
	            			while (i.hasNext()) {
	            				
	            				actionForm.setSupplierCodeList((String)i.next());
	            				
	            			}
	            			
	            		}
	            		
	            	}
	
	            	actionForm.clearCurrencyList();           	
	            	
	            	list = ejbFPO.getGlFcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setCurrencyList((String)i.next());
	            			
	            		}
	            		
	            	}
                        
                        
                        actionForm.clearBatchNameList();           	
                           
                        list = ejbFPO.getApOpenVbAll("PURCHASE ORDER", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                        if (list == null || list.size() == 0) {

                                actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

                        } else {

                                i = list.iterator();

                                while (i.hasNext()) {

                                    actionForm.setBatchNameList((String)i.next());

                                }

                        }

	            	
	            } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
				
				
				if (actionForm.getTableType() == null || request.getParameter("findDraft") != null
						
						) {	
                                        actionForm.setBatchName(Constants.GLOBAL_BLANK);
					actionForm.setSupplierCode(Constants.GLOBAL_BLANK);
					actionForm.setReceiving(false);
					actionForm.setPurchaseOrderVoid(false);
					actionForm.setType(Constants.GLOBAL_BLANK);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setReferenceNumber(null);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setCurrency(Constants.GLOBAL_BLANK);
					actionForm.setApprovalStatus("DRAFT");  
					actionForm.setPosted(Constants.GLOBAL_NO);
					actionForm.setOrderBy("DOCUMENT NUMBER");
				
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					HashMap criteria = new HashMap();
					criteria.put("approvalStatus", actionForm.getApprovalStatus());
					criteria.put("posted", actionForm.getPosted());
					criteria.put("receiving", new Byte((byte)0));
					criteria.put("purchaseOrderVoid", new Byte((byte)0));
					actionForm.setCriteria(criteria);
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
					if(request.getParameter("findDraft") != null) {
						
						return new ActionForward("/apFindPurchaseOrder.do?goButton=1");
						
					}
					
				} else {
					
					HashMap c = actionForm.getCriteria();
	            	
	            	if(c.containsKey("receiving")) {
	            		
	            		Byte b = (Byte)c.get("receiving");
	            		
	            		actionForm.setReceiving(Common.convertByteToBoolean(b.byteValue()));
	            		
	            	}
	            	
	            	if(c.containsKey("purchaseOrderVoid")) {
	            		
	            		Byte b = (Byte)c.get("purchaseOrderVoid");
	            		
	            		actionForm.setPurchaseOrderVoid(Common.convertByteToBoolean(b.byteValue()));
	            		
	            	}
					
					try {
							
						actionForm.clearApFPOList();
						
						ArrayList newList = ejbFPO.getApPoByCriteria(actionForm.getCriteria(),
									new Integer(actionForm.getLineCount()), 
									new Integer(Constants.GLOBAL_MAX_LINES + 1), 
									actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), actionForm.getSelectedPoNumber() != null ? true : false, user.getCmpCode());
					
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newList.remove(newList.size() - 1);
							
						}
						
						Iterator j = newList.iterator();
						while (j.hasNext()) {
							
							ApModPurchaseOrderDetails mdetails = (ApModPurchaseOrderDetails)j.next();
							
							ApFindPurchaseOrderList apFPOList = new ApFindPurchaseOrderList(actionForm,
									mdetails.getPoCode(),
									Common.convertByteToBoolean(mdetails.getPoReceiving()),
									mdetails.getPoSplSupplierCode(),
									Common.convertSQLDateToString(mdetails.getPoDate()),
									mdetails.getPoDocumentNumber(),
									mdetails.getPoReferenceNumber());
							
							actionForm.saveApFPOList(apFPOList);
							
						}
					
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
							
							actionForm.setDisableFirstButton(false);
							actionForm.setDisablePreviousButton(false);
							
						} else {
							
							actionForm.setDisableFirstButton(true);
							actionForm.setDisablePreviousButton(true);
							
						}
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findPurchaseOrder.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in ApFindPurchaseOrderAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
					
					
				}
				
				if (request.getParameter("child") == null) {
		        	
					return(mapping.findForward("apFindPurchaseOrder"));
		        	
		        } else {
		        			        	
					try {
						
		        		actionForm.setLineCount(0);
		        		
		        		HashMap criteria = new HashMap();
		        		
		        		if (request.getParameter("isVoucher")==null) {
		        			criteria.put(new String("receiving"), new Byte((byte)0));
		        		}else {
		        			criteria.put(new String("receiving"), new Byte((byte)1));
		        		}
		        		
		        		
		        		
		            	criteria.put(new String("purchaseOrderVoid"), new Byte((byte)0));
		            	criteria.put(new String("posted"), "YES");
		            	
		        		actionForm.setCriteria(criteria);						
						
						actionForm.clearApFPOList();
						
						ArrayList newList = ejbFPO.getApPoByCriteria(actionForm.getCriteria(),
								new Integer(actionForm.getLineCount()), 
								new Integer(Constants.GLOBAL_MAX_LINES + 1), 
								actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), actionForm.getSelectedPoNumber() != null ? true : false, user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);
							
						}
						
						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
							
							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);
							
							//remove last record
							newList.remove(newList.size() - 1);
							
						}
						
						i = newList.iterator();
						
						while (i.hasNext()) {
							
							ApModPurchaseOrderDetails mdetails = (ApModPurchaseOrderDetails)i.next();
							
							ApFindPurchaseOrderList apFPOList = new ApFindPurchaseOrderList(actionForm,
									mdetails.getPoCode(),
									Common.convertByteToBoolean(mdetails.getPoReceiving()),
									mdetails.getPoSplSupplierCode(),
									Common.convertSQLDateToString(mdetails.getPoDate()),
									mdetails.getPoDocumentNumber(),
									mdetails.getPoReferenceNumber());

							actionForm.saveApFPOList(apFPOList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);
						
						if(actionForm.getLineCount() > 0) {
						    
						    actionForm.setDisableFirstButton(false);
						    actionForm.setDisablePreviousButton(false);
						    
						} else {
						    
						    actionForm.setDisableFirstButton(true);
						    actionForm.setDisablePreviousButton(true);
						    
						}

					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in ApFindPurchaseOrderAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
					actionForm.setSelectedPoNumber(request.getParameter("selectedPoNumber"));
					actionForm.setIsVoucher(request.getParameter("isVoucher"));
	        		return mapping.findForward("apFindPurchaseOrderChild");
		        	
		        }							
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
 	System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in ApFindPurchaseOrderAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}