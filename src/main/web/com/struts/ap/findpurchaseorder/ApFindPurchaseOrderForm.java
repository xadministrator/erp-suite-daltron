package com.struts.ap.findpurchaseorder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApFindPurchaseOrderForm extends ActionForm implements Serializable {
	
	private String supplierCode = null;
	private ArrayList supplierCodeList = new ArrayList();
        private String batchName = null;
        private ArrayList batchNameList = new ArrayList();
	private boolean receiving = false;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private boolean purchaseOrderVoid = false;
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String referenceNumber = null;
	private String serialNumber= null;
	private String dateFrom = null;
	private String dateTo = null;
    private String approvalStatus = null;
    private ArrayList approvalStatusList = new ArrayList();
    private String currency = null;
    private ArrayList currencyList = new ArrayList();
    private String posted = null;
    private ArrayList postedList = new ArrayList();	
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   	
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList apFPOList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
        private boolean showBatchName = false;
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	private boolean useSupplierPulldown = true;
	
	private String selectedPoNumber = null;
	private String isVoucher = null;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public ApFindPurchaseOrderList getApFPOByIndex(int index) {
		
		return((ApFindPurchaseOrderList)apFPOList.get(index));
		
	}
	
	public Object[] getApFPOList() {
		
		return apFPOList.toArray();
		
	}
	
	public int getApFPOListSize() {
		
		return apFPOList.size();
		
	}
	
	public void saveApFPOList(Object newapFPOList) {
		
		apFPOList.add(newapFPOList);
		
	}
	
	public void clearApFPOList() {
		
		apFPOList.clear();
		
	}
	
	public void setRowSelected(Object selectedApFPOList, boolean isEdit) {
		
		this.rowSelected = apFPOList.indexOf(selectedApFPOList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
    public String getSupplierCode() {

      return supplierCode;

    }

   public void setSupplierCode(String supplierCode) {

      this.supplierCode = supplierCode;

   }

   public ArrayList getSupplierCodeList() {

      return supplierCodeList;

   }

   public void setSupplierCodeList(String supplierCode) {

      supplierCodeList.add(supplierCode);

   }

   public void clearSupplierCodeList() {

      supplierCodeList.clear();
      supplierCodeList.add(Constants.GLOBAL_BLANK);
   }
   
   
   
   public String getBatchName(){
       return batchName;
   }
   
   public void setBatchName(String batchName){
       this.batchName = batchName;
   }
   
    public ArrayList getBatchNameList() {

      return batchNameList;

   }

   public void setBatchNameList(String supplierCode) {

      batchNameList.add(supplierCode);

   }

   public void clearBatchNameList() {

      batchNameList.clear();
      batchNameList.add(Constants.GLOBAL_BLANK);
   }
   
   
   public boolean getReceiving() {
   	
   	  return receiving;
   	  
   }
   
   public void setReceiving(boolean receiving) {
   	
   	  this.receiving = receiving;
   	  
   }  
   
   public boolean getPurchaseOrderVoid() {
   	
   	  return purchaseOrderVoid;
   	  
   }
   
   public void setPurchaseOrderVoid(boolean purchaseOrderVoid) {
   	
   	  this.purchaseOrderVoid = purchaseOrderVoid;
   	  
   }
   
   public String getType() {
   	
   	  return type;
   	
   }
   
   public void setType(String type) {
   	
   	  this.type = type;
   	
   }
   
   public ArrayList getTypeList() {
   	
   	  return typeList;
   	
   }
	
	public String getDocumentNumberFrom() {
		
		return documentNumberFrom;
		
	}
	
	public void setDocumentNumberFrom(String documentNumberFrom) {
		
		this.documentNumberFrom = documentNumberFrom;
		
	}
	
	public String getDocumentNumberTo() {
		
		return documentNumberTo;
		
	}
	
	public void setDocumentNumberTo(String documentNumberTo) {
		
		this.documentNumberTo = documentNumberTo;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getSerialNumber() {
		
		return serialNumber;
		
	}
	
	public void setSerialNumber(String serialNumber) {
		
		this.serialNumber = serialNumber;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}		
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }

   public String getCurrency() {
   	
   	  return currency;
   
   }
   
   public void setCurrency(String currency) {
   
   	  this.currency = currency;
   
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   
   }
      
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }	
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
        
        
        
        public boolean getShowBatchName(){
            return showBatchName;
        }
        
        public void setShowBatchName(boolean showBatchName){
            this.showBatchName = showBatchName;
        }
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}
	
	public String getSelectedPoNumber() {
   	
   	  return selectedPoNumber;
   	
   }
   
   public void setSelectedPoNumber(String selectedPoNumber) {
   	
   	  this.selectedPoNumber = selectedPoNumber;
   	
   }
   
   
   public String getIsVoucher() {
	   	
   	  return isVoucher;
	   	
   }
   
   public void setIsVoucher(String isVoucher) {
   	
   	  this.isVoucher = isVoucher;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   	  return useSupplierPulldown;
   	  
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown){
   	
   	  this.useSupplierPulldown = useSupplierPulldown;
   	
   }
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		receiving = false;
		purchaseOrderVoid = false;
		
		typeList.clear();
 	    typeList.add(Constants.GLOBAL_BLANK);
 	    typeList.add("PO MATCHED");
 	    typeList.add("ITEMS");
 	    
		approvalStatusList.clear();
        approvalStatusList.add(Constants.GLOBAL_BLANK);
        approvalStatusList.add("DRAFT");
        approvalStatusList.add("N/A");
        approvalStatusList.add("PENDING");
        approvalStatusList.add("APPROVED");      
        approvalStatusList.add("REJECTED");
           
        postedList.clear();
        postedList.add(Constants.GLOBAL_BLANK);
        postedList.add(Constants.GLOBAL_YES);
        postedList.add(Constants.GLOBAL_NO);
        
		showDetailsButton = null;
		hideDetailsButton = null;
		
	    if (orderByList.isEmpty()) {
	    	
	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("SUPPLIER CODE");
	    	orderByList.add("DOCUMENT NUMBER");
	          
		  }		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
		
			if(useSupplierPulldown) {
				if (!Common.validateStringExists(supplierCodeList, supplierCode)) {
					
					errors.add("supplierCode",
							new ActionMessage("findPurchaseOrder.error.supplierCodeInvalid"));
					
				}
		    }
			
			if (!Common.validateDateFormat(dateFrom)) {
			    	
				errors.add("dateFrom",
					new ActionMessage("findPurchaseOrder.error.dateFromInvalid"));
			    		
			}         
			    		     					
			if (!Common.validateDateFormat(dateTo)) {
		    	
				errors.add("dateTo",
					new ActionMessage("findPurchaseOrder.error.dateToInvalid"));
		    		
			}
			
		}
		
		return errors;
		
	}
	
}