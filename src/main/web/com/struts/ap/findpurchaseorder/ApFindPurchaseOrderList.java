package com.struts.ap.findpurchaseorder;

import java.io.Serializable;

public class ApFindPurchaseOrderList implements Serializable {
	
	private Integer purchaseOrderCode = null;
	private boolean receiving = false;
	private String supplierCode = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	
	private String openButton = null;
	
	private ApFindPurchaseOrderForm parentBean;
	
	public ApFindPurchaseOrderList(ApFindPurchaseOrderForm parentBean,
			Integer purchaseOrderCode,
			boolean receiving,
			String supplierCode,
			String date,
			String documentNumber,
			String referenceNumber) {
		
		this.parentBean = parentBean;
		this.purchaseOrderCode = purchaseOrderCode;
		this.receiving = receiving;
		this.supplierCode = supplierCode;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getPurchaseOrderCode() {
		
		return purchaseOrderCode;
		
	}

	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public boolean getReceiving() {
		
		return receiving;
		
	}
	
	public String getSupplierCode() {
		
		return supplierCode;
		
	}

	public String getDate() {
		
		return date;
		
	}
	
}