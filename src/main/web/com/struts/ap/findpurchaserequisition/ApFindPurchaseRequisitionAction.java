package com.struts.ap.findpurchaserequisition;

import java.util.*;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalSupplierItemInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApFindPurchaseRequisitionController;
import com.ejb.txn.ApFindPurchaseRequisitionControllerHome;
import com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseRequisitionDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.ApPurchaseRequisitionDetails;

public final class ApFindPurchaseRequisitionAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 	Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApFindPurchaseRequisitionAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApFindPurchaseRequisitionForm actionForm = (ApFindPurchaseRequisitionForm)form;
			String frParam = null;
			
			frParam = Common.getUserPermission(user, Constants.AP_FIND_PURCHASE_REQUISITION_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("apFindPurchaseRequisition");
												
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
 	Initialize ApFindPurchaseRequisitionController EJB
*******************************************************/
			
			ApFindPurchaseRequisitionControllerHome homeFPR = null;
			ApFindPurchaseRequisitionController ejbFPR = null;
			
				
			try {
				
				homeFPR = (ApFindPurchaseRequisitionControllerHome)com.util.EJBHomeFactory.
					lookUpHome("ejb/ApFindPurchaseRequisitionControllerEJB", ApFindPurchaseRequisitionControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in ApFindPurchaseRequisitionAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbFPR = homeFPR.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in ApFindPurchaseRequisitionAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
 Call ApFindPurchaseRequisitionController EJB
 getGlFcPrecisionUnit
 *******************************************************/
			Integer poSize = null;
			short precisionUnit = 0;
			
			try {
				
				precisionUnit = ejbFPR.getGlFcPrecisionUnit(user.getCmpCode());
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in ApFindPurchaseRequisitionAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
/*******************************************************
	 -- Ap FPR Show Details Action --
*******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
												
				return(mapping.findForward("apFindPurchaseRequisition"));
				
/*******************************************************
     -- Ap FPR Hide Details Action --
*******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("apFindPurchaseRequisition"));                   
				
/*******************************************************
	-- Ap FPR First Action --
*******************************************************/ 
							
			} else if (request.getParameter("firstButton") != null){
				
				actionForm.setLineCount(0);
				
/*******************************************************
	-- Ap FPR Previous Action --
*******************************************************/ 
				
			} else if (request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
				
/*******************************************************
	-- Ap FPR Next Action --
*******************************************************/ 
				
			} else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
				
				
/*******************************************************
-- Ap PR Generate PR Action --
*******************************************************/   
			} else if(request.getParameter("generatePrButton") != null) {		
				
				ArrayList prSelectedList = new ArrayList();

				for(int i=0; i<actionForm.getApFPRListSize(); i++) {
						    
					ApFindPurchaseRequisitionList findprList = actionForm.getApFPRByIndex(i);
					System.out.println("CHECKBOX---->"+findprList.getPrCheckbox());	    	
					if(findprList.getPrCheckbox() == true){

					prSelectedList.add(findprList.getPurchaseRequisitionCode());
					
				  }	
					
				}
				
				               
				if(prSelectedList.size() > 0){              
				int prCode = ejbFPR.generateApPr(prSelectedList, new Integer(user.getCurrentBranch().getBrCode()),user.getUserName(), user.getCmpCode());
				}
						
/*******************************************************
-- Ap PR Generate PO Action --
*******************************************************/   
			} else if(request.getParameter("generatePoButton") != null) {
				         	      	       	
	         	//generate po
				
				for(int i=0; i<actionForm.getApFPRListSize(); i++) {
					ApFindPurchaseRequisitionList findprList = actionForm.getApFPRByIndex(i);
					
					if(findprList.getPrCheckbox() == true){
					ArrayList plList = null;
		         	try {
		         		
		         		ArrayList poList = ejbFPR.generateApPo(findprList.getPurchaseRequisitionCode(), user.getUserName(), findprList.getBranchCode(), user.getCmpCode());
		         		
		         		Iterator poListIter = poList.iterator();
		         		
		         		while (poListIter.hasNext()) {
			         		ApModPurchaseOrderDetails mdetails = ejbFPR.getApPoByPoCode((Integer)poListIter.next(), user.getCmpCode());
							
							
							plList = mdetails.getPoPlList();
			           		ejbFPR.saveApPoEntry(mdetails, mdetails.getPoPytName(),
			           				mdetails.getPoTcName(), mdetails.getPoFcName(), mdetails.getPoSplSupplierCode(), plList, false, 
			           				findprList.getBranchCode(), user.getCmpCode());
		         		}
		           		
		         	} catch (GlobalRecordAlreadyDeletedException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.recordAlreadyDeleted"));
		           		
		           	} catch (GlobalDocumentNumberNotUniqueException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.documentNumberNotUnique"));
		           		
		           	} catch (GlobalConversionDateNotExistException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.conversionDateNotExist")); 
		           		
		           	} catch (GlobalPaymentTermInvalidException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.paymentTermInvalid"));  
		           		
		           	} catch (GlobalTransactionAlreadyVoidException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyVoid"));
		           		
		           	} catch (GlobalInvItemLocationNotFoundException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.noItemLocationFound", ex.getMessage()));
		           		
		           	} catch (GlobalTransactionAlreadyApprovedException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyApproved"));
		           		
		           	} catch (GlobalTransactionAlreadyPendingException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPending"));
		           		
		           	} catch (GlobalTransactionAlreadyPostedException ex) {
		           		
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPosted"));
		           		
		           	} catch (GlobalNoApprovalRequesterFoundException ex) {
		              	
		            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                     new ActionMessage("purchaseOrderEntry.error.noApprovalRequesterFound"));
		                     
		            } catch (GlobalNoApprovalApproverFoundException ex) {
		            	
		            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                     new ActionMessage("purchaseOrderEntry.error.noApprovalApproverFound"));
		           		
		           	} catch (GlobalSupplierItemInvalidException ex) {
		                
		            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                     new ActionMessage("purchaseOrderEntry.error.supplierNotValidForItem", ex.getMessage()));	
		         		
		         	} catch (EJBException ex) {
		         		if (log.isInfoEnabled()) {
		         			
		         			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
		         					" session: " + session.getId());
		         		}
		         		
		         		return(mapping.findForward("cmnErrorPage"));
		         	}
				}
				}
/*******************************************************
-- Consolidate Ap PR Action --
*******************************************************/   
			} else if(request.getParameter("consolidateButton") != null) {

				ArrayList prSelectedList = new ArrayList();

				for(int i=0; i<actionForm.getApFPRListSize(); i++) {
						    
					ApFindPurchaseRequisitionList findprList = actionForm.getApFPRByIndex(i);
					System.out.println("CHECKBOX---->"+findprList.getPrCheckbox());	    	
					if(findprList.getPrCheckbox() == true){

					prSelectedList.add(findprList.getPurchaseRequisitionCode());
					
				  }	
					
				}
				
				               
				if(prSelectedList.size() > 0){              
				int prCode = ejbFPR.consolidateApPr(prSelectedList, new Integer(user.getCurrentBranch().getBrCode()),user.getUserName(), user.getCmpCode());
				}
				
}

/*******************************************************
	-- Ap FPR Go Action --
*******************************************************/
			
			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null || request.getParameter("forward") != null ||
					request.getParameter("forwardCnv") != null || request.getParameter("forwardRec") != null ||
					request.getParameter("forwardRej") != null || request.getParameter("forwardForCanvass") != null ||
					request.getParameter("findCanvassRejected") != null) {
				
				// create criteria 
				
			    if (request.getParameter("goButton") != null) {
			        
					if(request.getParameter("child") == null) {
					    					
						HashMap criteria = new HashMap();
						
						/*if(!Common.validateRequired(actionForm.getApprovalStatus())){
							
							criteria.put("user", actionForm.getUser());
							
							
						}
						
						if(!Common.validateRequired(actionForm.getDepartment())){
							
							criteria.put("department", actionForm.getDepartment());
							
						}*/
						
						if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
							
							criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
							
						}	        	
						
						if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
							
							criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
							
						}
						
						if (!Common.validateRequired(actionForm.getReferenceNumber())) {
							
							criteria.put("referenceNumber", actionForm.getReferenceNumber());
							
						}
	
						if (!Common.validateRequired(actionForm.getDateFrom())) {
							
							criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
							
						}	        	
						
						if (!Common.validateRequired(actionForm.getDateTo())) {
							
							criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
							
						}
						
						if (!Common.validateRequired(actionForm.getNextRunDateFrom())) {
			        		
			        		criteria.put("nextRunDateFrom", Common.convertStringToSQLDate(actionForm.getNextRunDateFrom()));
			        		
			        	}
			        	
			        	if (!Common.validateRequired(actionForm.getNextRunDateTo())) {
			        		
			        		criteria.put("nextRunDateTo", Common.convertStringToSQLDate(actionForm.getNextRunDateTo()));
			        		
			        	}
						
						if (!Common.validateRequired(actionForm.getApprovalStatus())) {
		        		
		        		   criteria.put("approvalStatus", actionForm.getApprovalStatus());
		        		
		        	    }
						
						
						if (!Common.validateRequired(actionForm.getCanvassApprovalStatus())) {
			        		
			        		   criteria.put("canvassApprovalStatus", actionForm.getCanvassApprovalStatus());
			        		
			        	}
		        	
		        	    if (!Common.validateRequired(actionForm.getCurrency())) {
		        		
		        		   criteria.put("currency", actionForm.getCurrency());
		        		
		        	    }
		        	    
		            	if (!Common.validateRequired(actionForm.getPosted())) {
		        		
		        		   criteria.put("posted", actionForm.getPosted());
		        		
		        	    }
		            	
		            	if (!Common.validateRequired(actionForm.getCanvassPosted())) {
			        		
			        		   criteria.put("canvassPosted", actionForm.getCanvassPosted());
			        		
			        	}
		        	    
		            	
		            	if (actionForm.getPurchaseRequisitionGenerated()) {	        	
	   		        		
			        		  criteria.put("purchaseRequisitionGenerated", new Byte((byte)1));
		                
		                }
		                
		                if (!actionForm.getPurchaseRequisitionGenerated()) {
		                	
		                	  criteria.put("purchaseRequisitionGenerated", new Byte((byte)0));
		                	
		                }
		                    
	                    if (actionForm.getPurchaseRequisitionVoid()) {	        	
			        	   		        		
		        		  criteria.put("purchaseRequisitionVoid", new Byte((byte)1));
	                
	                    }
	                
	                    if (!actionForm.getPurchaseRequisitionVoid()) {
	                	
	                	  criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
	                	
	                    }
	                    /*
	                    if (Common.getUserPermission(user, Constants.AP_CANVASS_ID) != null) {
		                    if (!Common.validateRequired(actionForm.getUser())) {
		                    	criteria.put("user", actionForm.getUser());
		                    }
	                    } else {
	                    	criteria.put("user", user.getUserName());
	                    }
	                    */

	                    if (!Common.validateRequired(actionForm.getUser())) {
	                    	
	                    	criteria.put("user", actionForm.getUser());
		                    	
	                    }
	                   	actionForm.setLineCount(0);
						actionForm.setCriteria(criteria);
						System.out.println("SAVE CRITERIA=="+actionForm.getCriteria());
					
					} 					
					
			    }  else if(request.getParameter("forward") != null){
					System.out.println("FORWARD-----------------------------------------"+request.getParameter("forward"));					  					    					    		                			              					        
					
						
						HashMap criteria = new HashMap();
						
	        		   criteria.put("posted", Constants.GLOBAL_YES);
	        		   actionForm.setPosted("YES");
	        		   
	        		   //criteria.put("approvalStatus", "APPROVED");
	        		   actionForm.setApprovalStatus(Constants.GLOBAL_BLANK);
	        		   
	        		   criteria.put("canvassPosted", Constants.GLOBAL_NO);
	        		   actionForm.setCanvassPosted("NO");
	        		   
	        		   criteria.put("canvassApprovalStatus", "DRAFT");
	        		   actionForm.setCanvassApprovalStatus("DRAFT");
	        		   
	        		   criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
	        		   criteria.put("purchaseRequisitionGenerated", new Byte((byte)0));
	        		   //actionForm.setUser(user.getUserName());
	        		   //criteria.put("user", user.getUserName());
		        	   actionForm.setLineCount(0);
		        	   actionForm.setCriteria(criteria);
		        	   actionForm.setOrderBy("DOCUMENT NUMBER");
		        	   
					
					
			    } else if(request.getParameter("forwardForCanvass") != null){
			    	
						   HashMap criteria = new HashMap();
							
		        		   criteria.put("posted", Constants.GLOBAL_YES);
		        		   actionForm.setPosted("YES");
		        		   
		        		   //criteria.put("approvalStatus", "APPROVED");
		        		   actionForm.setApprovalStatus(Constants.GLOBAL_BLANK);
		        		   
		        		   criteria.put("canvassPosted", Constants.GLOBAL_NO);
		        		   actionForm.setCanvassPosted("NO");
		        		   
		        		   criteria.put("canvassApprovalStatus", "DRAFT");
		        		   actionForm.setCanvassApprovalStatus("DRAFT");
		        		   
		        		   criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
		        		   criteria.put("purchaseRequisitionGenerated", new Byte((byte)0));
		        		   //actionForm.setUser(user.getUserName());
		        		   //criteria.put("user", user.getUserName());
			        	   actionForm.setLineCount(0);
			        	   actionForm.setCriteria(criteria);
			        	   actionForm.setOrderBy("DOCUMENT NUMBER");
				        	   
							
			    }	else if(request.getParameter("forwardCnv") != null){
					
					HashMap criteria = new HashMap();
					
	        		   criteria.put("posted", Constants.GLOBAL_YES);
	        		   actionForm.setPosted("YES");
	        		   
	        		   //criteria.put("approvalStatus", "APPROVED");
	        		   actionForm.setApprovalStatus(Constants.GLOBAL_BLANK);
	        		   
	        		   criteria.put("canvassPosted", Constants.GLOBAL_YES);
	        		   actionForm.setCanvassPosted("YES");
	        		   
	        		  // criteria.put("canvassApprovalStatus", "APPROVED");
	        		   actionForm.setCanvassApprovalStatus(Constants.GLOBAL_BLANK);
	        		   
	        		   
	        		   
	        		   criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
	        		   criteria.put("purchaseRequisitionGenerated", new Byte((byte)0));
	        		   
		        	   actionForm.setLineCount(0);
		        	   actionForm.setCriteria(criteria);
		        	   actionForm.setOrderBy("DOCUMENT NUMBER");
		        	   actionForm.setShowGeneratePrButton(false);
		        	   actionForm.setShowGeneratePoButton(true);
		        	   
				} else if(request.getParameter("forwardRec") != null){
					HashMap criteria = new HashMap();
						criteria.put("posted", Constants.GLOBAL_YES);
	        		   actionForm.setPosted("YES");
	        		   
	        		   //criteria.put("approvalStatus", "APPROVED");
	        		   actionForm.setApprovalStatus(Constants.GLOBAL_BLANK);
	        		   
	        		   criteria.put("nextRunDateFrom", Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	        		   actionForm.setNextRunDateFrom(Common.convertSQLDateToString(new java.util.Date()));
	        		   
	        		   criteria.put("nextRunDateTo",  Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	        		   actionForm.setNextRunDateTo(Common.convertSQLDateToString(new java.util.Date()));
	        		   
	        		   //criteria.put("canvassPosted", Constants.GLOBAL_NO);
	        		   //actionForm.setCanvassPosted("NO");
	        		   
	        		   //criteria.put("canvassApprovalStatus", "DRAFT");
	        		   //actionForm.setCanvassApprovalStatus("DRAFT");
	        		   
	        		   criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
	        		   criteria.put("purchaseRequisitionGenerated", new Byte((byte)0));
		        	   actionForm.setLineCount(0);
		        	   actionForm.setCriteria(criteria);
		        	   actionForm.setOrderBy("DOCUMENT NUMBER");
		        	   actionForm.setShowGeneratePrButton(true);
		        	   actionForm.setShowGeneratePoButton(false);
					
					
				} else if(request.getParameter("forwardRej") != null){
					HashMap criteria = new HashMap();
					criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
					actionForm.setApprovalStatus("REJECTED");
					criteria.put("approvalStatus", "REJECTED");
					actionForm.setUser(user.getUserName());
					criteria.put("user", user.getUserName());
					System.out.println("USER="+user.getUserName());
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					actionForm.setOrderBy("DOCUMENT NUMBER");
		        	   
				} else if(request.getParameter("findCanvassRejected") != null){
					System.out.println("findCanvassRejected-----------------");
					HashMap criteria = new HashMap();
					criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
					criteria.put("canvassApprovalStatus", "REJECTED");
					actionForm.setCanvassApprovalStatus("REJECTED");
					actionForm.setUser(user.getUserName());
					criteria.put("user", user.getUserName());
					
	        	    actionForm.setLineCount(0);
	        	    actionForm.setCriteria(criteria);
	        	    actionForm.setOrderBy("DOCUMENT NUMBER");
				}
				
			    if(request.getParameter("lastButton") != null) {
			    	
			    	int size = ejbFPR.getApPrSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
			    	
			    	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
			    		
			    		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
			    		
			    	} else {
			    		
			    		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
			    		
			    	}
			    	
			    }
			    
			    try {
			    	
			    	actionForm.clearApFPRList();
			    	System.out.println("ORDER BY ------>"+actionForm.getOrderBy());
			    	ArrayList list = null;
			    	
			    	if (request.getParameter("forwardForCanvass") != null || request.getParameter("forwardCnv") != null) {
			    		list = ejbFPR.getApPrByCriteria(actionForm.getCriteria(),
			    			new Integer(actionForm.getLineCount()), 
							new Integer(Constants.GLOBAL_MAX_LINES + 1), 
							actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), true, user.getCmpCode());
			    		
			    	} else {
			    		list = ejbFPR.getApPrByCriteria(actionForm.getCriteria(),
				    			new Integer(actionForm.getLineCount()), 
								new Integer(Constants.GLOBAL_MAX_LINES + 1), 
								actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), false, user.getCmpCode());
			    	}
			    	
			    	// check if prev should be disabled
			    	if (actionForm.getLineCount() == 0) {
			    		
			    		actionForm.setDisablePreviousButton(true);
			    		actionForm.setDisableFirstButton(true);
			    		
			    	} else {
			    		
			    		actionForm.setDisablePreviousButton(false);
			    		actionForm.setDisableFirstButton(false);
			    		
			    	}
			    	
			    	// check if next should be disabled
			    	if (list.size() <= Constants.GLOBAL_MAX_LINES) {
			    		
			    		actionForm.setDisableNextButton(true);
			    		actionForm.setDisableLastButton(true);
			    		
			    	} else {
			    		
			    		actionForm.setDisableNextButton(false);
			    		actionForm.setDisableLastButton(false);
			    		
			    		//remove last record
			    		list.remove(list.size() - 1);
			    		
			    	}
			    	
			    	Iterator i = list.iterator();
			    	
			    	while (i.hasNext()) {
			    		
			    		ApModPurchaseRequisitionDetails mdetails = (ApModPurchaseRequisitionDetails)i.next();
			    		
			    		ApFindPurchaseRequisitionList apFPRList = new ApFindPurchaseRequisitionList(actionForm, mdetails.getPrCode(), Common.convertSQLDateToString(mdetails.getPrDate()), mdetails.getPrNumber(), mdetails.getPrReferenceNumber(), mdetails.getPrAdBranchCode(), Common.convertDoubleToStringMoney(mdetails.getPrAmount(), precisionUnit),mdetails.getPrLastModifiedBy());
			    		
			    		actionForm.saveApFPRList(apFPRList);
			    		
			    	}
			    	
			    } catch (GlobalNoRecordFoundException ex) {
			    	
			    	// disable prev, next, first & last buttons
			    	actionForm.setDisableNextButton(true);
			    	actionForm.setDisablePreviousButton(true);
			    	actionForm.setDisableFirstButton(true);
			    	actionForm.setDisableLastButton(true);
			    	
			    	errors.add(ActionMessages.GLOBAL_MESSAGE,
			    			new ActionMessage("findPurchaseRequisition.error.noRecordFound"));
			    	
			    } catch (EJBException ex) {
			    	
			    	if (log.isInfoEnabled()) {
			    		
			    		log.info("EJBException caught in ApFindPurchaseRequisitionAction.execute(): " + ex.getMessage() +
			    				" session: " + session.getId());
			    		return mapping.findForward("cmnErrorPage"); 
			    		
			    	}
			    	
			    }
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
					return(mapping.findForward("apFindPurchaseRequisition"));
					
				}
				
				actionForm.reset(mapping, request);
				if (request.getParameter("forwardCnv") == null && request.getParameter("forwardRec") == null) {
					actionForm.setShowGeneratePrButton(false);
		        	   actionForm.setShowGeneratePoButton(false);
				}
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}
				
				return(mapping.findForward("apFindPurchaseRequisition"));
				
/*******************************************************
	-- Ap FPR Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
	-- Ap FPR Open Action --
*******************************************************/
				
			} else if (request.getParameter("apFPRList[" + 
					actionForm.getRowSelected() + "].openButton") != null) {
				
				ApFindPurchaseRequisitionList apFPRList =
					actionForm.getApFPRByIndex(actionForm.getRowSelected());
				
				String path = "/apPurchaseRequisitionEntry.do?forward=1" +
				"&purchaseRequisitionCode=" + apFPRList.getPurchaseRequisitionCode();
				
				return(new ActionForward(path));
				
/*******************************************************
	-- Ap FPR Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				actionForm.clearApFPRList();
				
	            ArrayList list = null;
	            Iterator i = null;
	            
	            ArrayList userlist = null;
	            Iterator x = null;
	            try {
	            	
	            	actionForm.clearCurrencyList();           	
	            	
	            	list = ejbFPR.getGlFcAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            			actionForm.setCurrencyList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
	            	actionForm.clearDepartmentList();      
	            	list = ejbFPR.getAdLvDEPARTMENT(user.getCmpCode());
	                i = list.iterator();

	                if (list == null || list.size() == 0) {  		
	                  	 actionForm.setDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);
	                } else {     		            		
	                  	 i = list.iterator();
	                  	 while (i.hasNext()) {
	       					    actionForm.setDepartmentList((String)i.next());
	                  	 }
	                }
	            	
	            	actionForm.clearUserList();           	
	            	
	            	userlist = ejbFPR.getAdUsrAll(user.getCmpCode());
	            	
	            	if (userlist == null || userlist.size() == 0) {
	            		
	            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		
	            		x = userlist.iterator();
	            		
	            		while (x.hasNext()) {
	            			
	            			actionForm.setUserList((String)x.next());
	            			
	            		}
	            		
	            	}
	            	
    	
	            	
	            } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in ApFindPurchaseRequisitionAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
				
				if (request.getParameter("goButton") != null) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
				if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {
					
					actionForm.setUser(null);
					actionForm.setPurchaseRequisitionVoid(false);
					actionForm.setPurchaseRequisitionGenerated(false);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setReferenceNumber(null);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setNextRunDateFrom(null);
		        	actionForm.setNextRunDateTo(null);
					actionForm.setCurrency(Constants.GLOBAL_BLANK);
					actionForm.setApprovalStatus("DRAFT");  
					actionForm.setPosted(Constants.GLOBAL_NO);
					actionForm.setCanvassApprovalStatus(Constants.GLOBAL_BLANK);  
					actionForm.setCanvassPosted(Constants.GLOBAL_BLANK);
					actionForm.setOrderBy("DOCUMENT NUMBER");
				
					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);
					
					HashMap criteria = new HashMap();
					criteria.put("approvalStatus", actionForm.getApprovalStatus());
					criteria.put("posted", actionForm.getPosted());
					criteria.put("purchaseRequisitionVoid", new Byte((byte)0));
					actionForm.setCriteria(criteria);
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
					if(request.getParameter("findDraft") != null) {
						
						return new ActionForward("/apFindPurchaseRequisition.do?goButton=1");
						
					}
					
				} else {
					if(request.getParameter("forward") != null){
						System.out.println("FORWARD");
					}
					System.out.println("FORWARD-----------------------------------------");
					HashMap c = actionForm.getCriteria();
	            	
	            	if(c.containsKey("purchaseRequisitionVoid")) {
	            		
	            		Byte b = (Byte)c.get("purchaseRequisitionVoid");
	            		
	            		actionForm.setPurchaseRequisitionVoid(Common.convertByteToBoolean(b.byteValue()));
	            		System.out.println("getPurchaseRequisitionVoid"+actionForm.getPurchaseRequisitionVoid());
	            	}
					
	            	try {
	            		
	            		actionForm.clearApFPRList();
	            		
	            		ArrayList newList = ejbFPR.getApPrByCriteria(actionForm.getCriteria(),
	            				new Integer(actionForm.getLineCount()), 
								new Integer(Constants.GLOBAL_MAX_LINES + 1), 
								actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), false, user.getCmpCode());
	            		
	            		// check if prev should be disabled
	            		if (actionForm.getLineCount() == 0) {
	            			
	            			actionForm.setDisablePreviousButton(true);
	            			actionForm.setDisableFirstButton(true);
	            			
	            		} else {
	            			
	            			actionForm.setDisablePreviousButton(false);
	            			actionForm.setDisableFirstButton(false);
	            			
	            		}
	            		
	            		// check if next should be disabled
	            		if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
	            			
	            			actionForm.setDisableNextButton(true);
	            			actionForm.setDisableLastButton(true);
	            			
	            		} else {
	            			
	            			actionForm.setDisableNextButton(false);
	            			actionForm.setDisableLastButton(false);
	            			
	            			//remove last record
	            			newList.remove(newList.size() - 1);
	            			
	            		}
	            		
	            		Iterator j = newList.iterator();
	            		while (j.hasNext()) {
	            			
	            			ApModPurchaseRequisitionDetails mdetails = (ApModPurchaseRequisitionDetails)j.next();
	            			
	            			ApFindPurchaseRequisitionList apFPRList = new ApFindPurchaseRequisitionList(actionForm,
	            					mdetails.getPrCode(),
									Common.convertSQLDateToString(mdetails.getPrDate()),
									mdetails.getPrNumber(),
									mdetails.getPrReferenceNumber(),
									mdetails.getPrAdBranchCode(),
									Common.convertDoubleToStringMoney(mdetails.getPrAmount(), precisionUnit),
									mdetails.getPrLastModifiedBy());
	            			
	            			actionForm.saveApFPRList(apFPRList);
	            			
	            		}
	            		
	            	} catch (GlobalNoRecordFoundException ex) {
	            		
	            		// disable prev next buttons
	            		actionForm.setDisableNextButton(true);
	            		actionForm.setDisableLastButton(true);
	            		
	            		if(actionForm.getLineCount() > 0) {
	            			
	            			actionForm.setDisableFirstButton(false);
	            			actionForm.setDisablePreviousButton(false);
	            			
	            		} else {
	            			
	            			actionForm.setDisableFirstButton(true);
	            			actionForm.setDisablePreviousButton(true);
	            			
	            		}
	            		
	            		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            				new ActionMessage("findPurchaseRequisition.error.noRecordFound"));
	            		
	            	} catch (EJBException ex) {
	            		
	            		if (log.isInfoEnabled()) {
	            			
	            			log.info("EJBException caught in ApFindPurchaseRequisitionAction.execute(): " + ex.getMessage() +
	            					" session: " + session.getId());
	            			return mapping.findForward("cmnErrorPage"); 
	            			
	            		}
	            		
	            	}
					
					
					
				}
				
				return(mapping.findForward("apFindPurchaseRequisition"));						
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
 	System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in ApFindPurchaseRequisitionAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}