package com.struts.ap.findpurchaserequisition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;


public class ApFindPurchaseRequisitionForm extends ActionForm implements Serializable {
	
	
	private boolean purchaseRequisitionVoid = false;
	private boolean purchaseRequisitionGenerated = false;
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String nextRunDateFrom = null;
	private String nextRunDateTo = null;
	private String referenceNumber = null;
	private String dateFrom = null;
	private String dateTo = null;
	
	private String user = null;
	private String department = null;
	private ArrayList userList = new ArrayList();
	private ArrayList departmentList = new ArrayList();
	
	private String approvalStatus = null;
	private ArrayList approvalStatusList = new ArrayList();
	private String canvassApprovalStatus = null;
	private ArrayList canvassApprovalStatusList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String posted = null;
	private ArrayList postedList = new ArrayList();
	private String canvassPosted = null;
	private ArrayList canvassPostedList = new ArrayList();	
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();   	
	
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList apFPRList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enablePoCheckbox = false;
	
	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	private boolean useSupplierPulldown = true;
	private boolean showGeneratePrButton = false;
	private boolean showGeneratePoButton = false;
	
	private String selectedPrNumber = null;
	
	private int lineCount = 0;
	
	private HashMap criteria = new HashMap();
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public ApFindPurchaseRequisitionList getApFPRByIndex(int index) {
		
		return((ApFindPurchaseRequisitionList)apFPRList.get(index));
		
	}
	
	public Object[] getApFPRList() {
		
		return apFPRList.toArray();
		
	}
	
	public int getApFPRListSize() {
		
		return apFPRList.size();
		
	}
	
	public void saveApFPRList(Object newapFPRList) {
		
		apFPRList.add(newapFPRList);
		
	}
	
	public void clearApFPRList() {
		
		apFPRList.clear();
		
	}
	
	public void setRowSelected(Object selectedApFPRList, boolean isEdit) {
		
		this.rowSelected = apFPRList.indexOf(selectedApFPRList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}   
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public boolean getPurchaseRequisitionVoid() {
		
		return purchaseRequisitionVoid;
		
	}
	
	public void setPurchaseRequisitionVoid(boolean purchaseRequisitionVoid) {
		
		this.purchaseRequisitionVoid = purchaseRequisitionVoid;
		
	}
	
	public boolean getPurchaseRequisitionGenerated() {
		
		return purchaseRequisitionGenerated;
		
	}
	
	public void setPurchaseRequisitionGenerated(boolean purchaseRequisitionGenerated) {
		
		this.purchaseRequisitionGenerated= purchaseRequisitionGenerated;
		
	}
	
	
	public String getDocumentNumberFrom() {
		
		return documentNumberFrom;
		
	}
	
	public void setDocumentNumberFrom(String documentNumberFrom) {
		
		this.documentNumberFrom = documentNumberFrom;
		
	}
	
	public String getDocumentNumberTo() {
		
		return documentNumberTo;
		
	}
	
	public String getNextRunDateFrom() {
	   	  return(nextRunDateFrom);	  
   }
   
   public void setNextRunDateFrom(String nextRunDateFrom) {
   	  this.nextRunDateFrom = nextRunDateFrom;
   }
   
   public String getNextRunDateTo() {
   	  return(nextRunDateTo);	  
   }
   
   public void setNextRunDateTo(String nextRunDateTo) {
   	  this.nextRunDateTo = nextRunDateTo;
   }
	   
	
	public void setDocumentNumberTo(String documentNumberTo) {
		
		this.documentNumberTo = documentNumberTo;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}		
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}
	
	
	public String getUser() {
		
		return user;
		
	}
	
	public void setUser(String user) {
		
		this.user = user;
		
	}
	
	public ArrayList getUserList() {
		
		return userList;
		
	}
	
	public void setUserList(String user) {
		
		userList.add(user);
		
	}
	
	public void clearUserList() {
		
		userList.clear();
		userList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getDepartment() {
		
		return department;
		
	}
	
	public void setDepartment(String department) {
		
		this.department = department;
		
	}
	
	public ArrayList getDepartmentList() {
		
		return departmentList;
		
	}
	
	public void setDepartmentList(String department) {
		
		departmentList.add(department);
		
	}
	
	public void clearDepartmentList() {
		
		departmentList.clear();
		departmentList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	
	public ArrayList getApprovalStatusList() {
		
		return approvalStatusList;
		
	}
	
	
	public String getCanvassApprovalStatus() {
		
		return canvassApprovalStatus;
		
	}
	
	public void setCanvassApprovalStatus(String canvassApprovalStatus) {
		
		this.canvassApprovalStatus = canvassApprovalStatus;
		
	}
	
	
	public ArrayList getCanvassApprovalStatusList() {
		
		return canvassApprovalStatusList;
		
	}
	
	public String getCurrency() {
		
		return currency;
		
	}
	
	public void setCurrency(String currency) {
		
		this.currency = currency;
		
	}
	
	public ArrayList getCurrencyList() {
		
		return currencyList;
		
	}
	
	public void setCurrencyList(String currency) {
		
		currencyList.add(currency);
		
	}
	
	public void clearCurrencyList() {
		
		currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public ArrayList getPostedList() {
		
		return postedList;
		
	}	
	
	
	public String getCanvassPosted() {
		
		return canvassPosted;
		
	}
	
	public void setCanvassPosted(String canvassPosted) {
		
		this.canvassPosted = canvassPosted;
		
	}
	
	public ArrayList getCanvassPostedList() {
		
		return canvassPostedList;
		
	}
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableFirstButton() {
		
		return disableFirstButton;
		
	}
	
	public void setDisableFirstButton(boolean disableFirstButton) {
		
		this.disableFirstButton = disableFirstButton;
		
	}
	
	public boolean getDisableLastButton() {
		
		return disableLastButton;
		
	}
	
	public void setDisableLastButton(boolean disableLastButton) {
		
		this.disableLastButton = disableLastButton;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}
	
	public String getSelectedPrNumber() {
		
		return selectedPrNumber;
		
	}
	
	public void setSelectedPrNumber(String selectedPrNumber) {
		
		this.selectedPrNumber = selectedPrNumber;
		
	}
	
	public boolean getEnablePoCheckbox() {
		
		return enablePoCheckbox;
		
	}
	
	public void setEnablePoCheckbox(boolean enablePoCheckbox) {
		
		this.enablePoCheckbox = enablePoCheckbox;
		
	}
	
	public boolean getShowGeneratePrButton() {
		
		return showGeneratePrButton;
		
	}
	
	public void setShowGeneratePrButton(boolean showGeneratePrButton) {
		
		this.showGeneratePrButton = showGeneratePrButton;
		
	}
		
	public boolean getShowGeneratePoButton() {
		
		return showGeneratePoButton;
		
	}
	
	public void setShowGeneratePoButton(boolean showGeneratePoButton) {
		
		this.showGeneratePoButton = showGeneratePoButton;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		HttpSession session = request.getSession();
		User activeUser = (User) session.getAttribute(Constants.USER_KEY);
		purchaseRequisitionVoid = false;
		purchaseRequisitionGenerated= false;
		
		approvalStatusList.clear();
		approvalStatusList.add(Constants.GLOBAL_BLANK);
		approvalStatusList.add("DRAFT");
		approvalStatusList.add("N/A");
		approvalStatusList.add("PENDING");
		approvalStatusList.add("APPROVED");      
		approvalStatusList.add("REJECTED");
		
		canvassApprovalStatusList.clear();
		canvassApprovalStatusList.add(Constants.GLOBAL_BLANK);
		canvassApprovalStatusList.add("DRAFT");
		canvassApprovalStatusList.add("N/A");
		canvassApprovalStatusList.add("PENDING");
		canvassApprovalStatusList.add("APPROVED");      
		canvassApprovalStatusList.add("REJECTED");
		
		user = activeUser.getUserName();
		
		postedList.clear();
		postedList.add(Constants.GLOBAL_BLANK);
		postedList.add(Constants.GLOBAL_YES);
		postedList.add(Constants.GLOBAL_NO);
		
		canvassPostedList.clear();
		canvassPostedList.add(Constants.GLOBAL_BLANK);
		canvassPostedList.add(Constants.GLOBAL_YES);
		canvassPostedList.add(Constants.GLOBAL_NO);
		
		showDetailsButton = null;
		hideDetailsButton = null;
		
		if (orderByList.isEmpty()) {
			
			orderByList.clear();
			orderByList.add(Constants.GLOBAL_BLANK);
			orderByList.add("DOCUMENT NUMBER");
			orderByList.add("NEXT RUN DATE");
			orderByList.add("DATE");
			
		}		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {
			
			if (!Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom",
						new ActionMessage("findPurchaseRequisition.error.dateFromInvalid"));
				
			}         
			
			if (!Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo",
						new ActionMessage("findPurchaseRequisition.error.dateToInvalid"));
				
			}
			
			if (!Common.validateDateFormat(nextRunDateFrom)) {
				
			     errors.add("nextRunDateFrom",
			        new ActionMessage("findRecurringJournal.error.nextRunDateFromInvalid"));
			
			  }         
			 
		 	  if (!Common.validateDateFormat(nextRunDateTo)) {
			
			     errors.add("nextRunDateTo",
			        new ActionMessage("findRecurringJournal.error.nextRunDateToInvalid"));
			
			  } 
			
		}
		
		return errors;
		
	}
	
}