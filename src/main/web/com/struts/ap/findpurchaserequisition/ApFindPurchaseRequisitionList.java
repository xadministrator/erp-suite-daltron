package com.struts.ap.findpurchaserequisition;

import java.io.Serializable;

public class ApFindPurchaseRequisitionList implements Serializable {
	
	private Integer purchaseRequisitionCode = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String branchCode = null;
	private String amount = null;
	private String user = null;
	
	private boolean prCheckbox = false;
	private String openButton = null;
	
	private ApFindPurchaseRequisitionForm parentBean;
	
	public ApFindPurchaseRequisitionList(ApFindPurchaseRequisitionForm parentBean,
			Integer purchaseRequisitionCode,
			String date,
			String documentNumber,
			String referenceNumber,
			String branchCode,
			String amount,
			String user
			) {
		
		this.parentBean = parentBean;
		this.purchaseRequisitionCode = purchaseRequisitionCode;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.branchCode = branchCode;
		this.amount = amount;
		this.user = amount;

	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getPurchaseRequisitionCode() {
		
		return purchaseRequisitionCode;
		
	}

	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public String getBranchCode() {
		
		return branchCode;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	public String getUser() {
		
		return user;
		
	}
	
	public void setUser(String user) {
		
		this.user = user;
		
	}
	
	public boolean getPrCheckbox() {
		
		return prCheckbox;
		
	}
	
	public void setPrCheckbox(boolean prCheckbox) {
		
		this.prCheckbox = prCheckbox;
		
	}
	
	
}