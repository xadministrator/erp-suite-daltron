package com.struts.ap.findrecurringvoucher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApFindRecurringVoucherForm extends ActionForm implements Serializable {

   private String name = null;
   private String nextRunDateFrom = null;
   private String nextRunDateTo = null;
   private String supplier = null;
   private ArrayList supplierList = new ArrayList(); 
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private ArrayList apFRVList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   private boolean useSupplierPulldown = true;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ApFindRecurringVoucherList getApFRVByIndex(int index){
      return((ApFindRecurringVoucherList)apFRVList.get(index));
   }

   public Object[] getApFRVList(){
      return(apFRVList.toArray());
   }

   public int getApFRVListSize(){
      return(apFRVList.size());
   }

   public void saveApFRVList(Object newApFRVList){
      apFRVList.add(newApFRVList);
   }

   public void clearApFRVList(){
      apFRVList.clear();
   }

   public void setRowSelected(Object selectedApFRVList, boolean isEdit){
      this.rowSelected = apFRVList.indexOf(selectedApFRVList);
   }

   public void updateApFRVRow(int rowSelected, Object newApFRVList){
      apFRVList.set(rowSelected, newApFRVList);
   }

   public void deleteApFRVList(int rowSelected){
      apFRVList.remove(rowSelected);
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   } 
   
   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getName(){
      return(name);
   }

   public void setName(String name){
      this.name = name;
   }
   
   public String getNextRunDateFrom() {
   	  return(nextRunDateFrom);	  
   }
   
   public void setNextRunDateFrom(String nextRunDateFrom) {
   	  this.nextRunDateFrom = nextRunDateFrom;
   }
   
   public String getNextRunDateTo() {
   	  return(nextRunDateTo);	  
   }
   
   public void setNextRunDateTo(String nextRunDateTo) {
   	  this.nextRunDateTo = nextRunDateTo;
   }   
      
   public String getSupplier() {
   
   	  return supplier;
   
   }
   
   public void setSupplier(String supplier) {
   	
   	  this.supplier = supplier;
   
   }
   
   public ArrayList getSupplierList() {
   
   	  return supplierList;
   
   }
   
   public void setSupplierList(String supplier) {
   
   	  supplierList.add(supplier);
   
   }
   
   public void clearSupplierList() {
   	 
   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);
   
   }   
      
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   } 
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }
      
   public void reset(ActionMapping mapping, HttpServletRequest request){
      
      if (orderByList.isEmpty()) {      
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AP_RVG_ORDER_BY_NAME);
	      orderByList.add(Constants.AP_RVG_ORDER_BY_NEXT_RUN_DATE);
	      orderByList.add(Constants.AP_RVG_ORDER_BY_SUPPLIER_CODE);
	                        
	  }
	  
      showDetailsButton = null;
	  hideDetailsButton = null;      	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
   	
       ActionErrors errors = new ActionErrors();
       
       if (request.getParameter("goButton") != null) {
      
	       if (!Common.validateDateFormat(nextRunDateFrom)) {
	
		     errors.add("nextRunDateFrom",
		        new ActionMessage("findRecurringJournal.error.nextRunDateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(nextRunDateTo)) {
		
		     errors.add("nextRunDateTo",
		        new ActionMessage("findRecurringJournal.error.nextRunDateToInvalid"));
		
		  } 
		  
	   }  	   
	   			        
      return(errors);
   }
}
