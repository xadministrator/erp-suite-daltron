package com.struts.ap.findrecurringvoucher;

import java.io.Serializable;

public class ApFindRecurringVoucherList implements Serializable {

   private Integer recurringVoucherCode = null;
   private String name = null;
   private String description = null;
   private String reminderSchedule = null;
   private String supplier = null;
   private String nextRunDate = null;
   private String lastRunDate = null;
   
   private String openButton = null;
    
   private ApFindRecurringVoucherForm parentBean;
    
   public ApFindRecurringVoucherList(ApFindRecurringVoucherForm parentBean,
      Integer recurringVoucherCode,
      String name,
      String description,
      String reminderSchedule,
      String supplier,
      String nextRunDate,
      String lastRunDate){

      this.parentBean = parentBean;
      this.recurringVoucherCode = recurringVoucherCode;
      this.name = name;
      this.description = description;
      this.reminderSchedule = reminderSchedule; 
      this.supplier = supplier; 
      this.nextRunDate = nextRunDate;
      this.lastRunDate = lastRunDate;
      
   }
   
   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }
   
   public Integer getRecurringVoucherCode(){
      return(recurringVoucherCode);
   }

   public String getName(){
      return(name);
   }
   
   public String getDescription(){
   	  return(description);
   }
      
   public String getReminderSchedule(){
      return(reminderSchedule);
   }
   
   public String getSupplier(){
      return(supplier);
   }

   public String getNextRunDate(){
      return(nextRunDate);
   }   

   public String getLastRunDate(){
   	  return(lastRunDate);
   }
      
}

