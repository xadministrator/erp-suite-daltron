package com.struts.ap.findsupplier;

import java.io.Serializable;


/**
 * 
 * @author Franco Antonio R. Roig
 * Created: 10/25/2005 5:25 PM
 * 
 */
public class ApBranchFindSupplierList implements Serializable {
	
	private String branchName = null;
	private Integer branchCode = null;
	private boolean branchCheckbox = false;
	private String branchPayableAccount = null;
	private String branchPayableDescription = null;
	private String branchExpenseAccount = null;
	private String branchExpenseDescription = null;
	
	private ApFindSupplierForm parentBean = null;
	
	public ApBranchFindSupplierList(ApFindSupplierForm parentBean, 
			String brName, Integer brCode) {
		
		this.parentBean = parentBean;
		this.branchName = brName;
		this.branchCode = brCode;
		
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBrCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
	public String getBranchPayableAccount() {
		return this.branchPayableAccount;
	}
	
	public void setBranchPayableAccount(String branchPayableAccount) {
		this.branchPayableAccount = branchPayableAccount;
	}
	
	public String getBranchPayableDescription() {
		return this.branchPayableDescription;
	}
	
	public void setBranchPayableDescription(String branchPayableDescription) {
		this.branchPayableDescription = branchPayableDescription;
	}
	
	public String getBranchExpenseAccount() {
		return this.branchExpenseAccount;
	}
	
	public void setBranchExpenseAccount(String branchExpenseAccount) {
		this.branchExpenseAccount = branchExpenseAccount;
	}
	
	public String getBranchExpenseDescription() {
		return this.branchExpenseDescription;
	}
	
	public void setBranchExpenseDescription(String branchExpenseDescription) {
		this.branchExpenseDescription = branchExpenseDescription;
	}
}

