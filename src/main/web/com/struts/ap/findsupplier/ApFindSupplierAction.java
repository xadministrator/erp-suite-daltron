package com.struts.ap.findsupplier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApFindSupplierController;
import com.ejb.txn.ApFindSupplierControllerHome;
import com.struts.util.Branch;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.ApModSupplierDetails;

public final class ApFindSupplierAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApFindSupplierAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApFindSupplierForm actionForm = (ApFindSupplierForm)form;
         
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
            frParam = Common.getUserPermission(user, Constants.AP_FIND_SUPPLIER_ID);
         	
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
        

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  
                  if (request.getParameter("child") == null) {

                  	return mapping.findForward("apFindSupplier");
                  	
                  } else {
                  
                    return mapping.findForward("apFindSupplierChild");
                  
                  }
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ApFindSupplierController EJB
*******************************************************/

         ApFindSupplierControllerHome homeFS = null;
         ApFindSupplierController ejbFS = null;
         ArrayList adBrnchList = new ArrayList();
         
         try {

            homeFS = (ApFindSupplierControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApFindSupplierControllerEJB", ApFindSupplierControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApFindSupplierAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFS = homeFS.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApFindSupplierAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
/*******************************************************
   -- Ap FS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        if (request.getParameter("child") == null) {

	          return mapping.findForward("apFindSupplier");
	          	
	        } else {
	          
	          return mapping.findForward("apFindSupplierChild");
	          
	        }
	     
/*******************************************************
   -- Ap FS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        if (request.getParameter("child") == null) {

	          return mapping.findForward("apFindSupplier");
	          	
	        } else {
	          
	          return mapping.findForward("apFindSupplierChild");
	          
	        }                         

/*******************************************************
    -- Ap FS First Action --
*******************************************************/ 

	     } else if (request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	              	
/*******************************************************
   -- Ap FS Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ap FS Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Ap FS Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("name", actionForm.getName());
	        		
	        	}	        	
	        	        	
	        	if (!Common.validateRequired(actionForm.getEmail())) {
	        		
	        		criteria.put("email", actionForm.getEmail());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSupplierType())) {
	        		
	        		criteria.put("supplierType", actionForm.getSupplierType());
	        		
	        	}   

	        	if (!Common.validateRequired(actionForm.getSupplierClass())) {
	        		
	        		criteria.put("supplierClass", actionForm.getSupplierClass());
	        		
	        	} 
	        	 
	        	if (actionForm.getEnable()) {
	        			        	   		        		
	        		criteria.put("enable", new Byte((byte)1));
	        	
	            }
	            
	        	if (actionForm.getDisable()) {
	        			            
	        		criteria.put("disable", new Byte((byte)1));
 
                }
                
                if (request.getParameter("child") == null) {
                
	                if  (!actionForm.getEnable() && !actionForm.getDisable()) {
	                	
	                	criteria.put("enable", new Byte((byte)1));
	                	criteria.put("disable", new Byte((byte)1));
	                	
	                }  
	                
	             } else {
	             
	                criteria.put("enable", new Byte((byte)1));	                
	             
	             }

                adBrnchList = new ArrayList();
                
	            Object[] apBrFSplList = actionForm.getApBFSplList();
	            
	            for(int j=0; j<apBrFSplList.length; j++) {
	                
	                ApBranchFindSupplierList apBFSplList = actionForm.getApBFSplByIndex(j);
	                
	                if(apBFSplList.getBranchCheckbox() == true) {
	                    
	                    AdBranchDetails details = new AdBranchDetails();	                    
	                    details.setBrCode(apBFSplList.getBranchCode());
	                    adBrnchList.add(details);
	                }
	            }
	            
	            if (request.getParameter("child") != null) {
	            	if (adBrnchList.isEmpty()) {
	            		            			
            			AdBranchDetails details = new AdBranchDetails();	                    
            			details.setBrCode(new Integer(user.getCurrentBranch().getBrCode()));
            			adBrnchList.add(details);
	            		
	            	}
	            }
	            
	        	// save criteria
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFS.getApSplSizeByCriteria(actionForm.getCriteria(), adBrnchList, user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            try {
            	
            	actionForm.clearApFSList();
            	
            	ArrayList list = ejbFS.getApSplByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), 
            	    adBrnchList, user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	              
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);	
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModSupplierDetails mdetails = (ApModSupplierDetails)i.next();
      		
            		ApFindSupplierList apFSList = new ApFindSupplierList(actionForm,
            		    mdetails.getSplCode(),
            		    mdetails.getSplSupplierCode(),
            		    mdetails.getSplName(),
            		    mdetails.getSplTin(),
            		    mdetails.getSplEmail(),
            		    mdetails.getSplStName(),
            		    mdetails.getSplScName(),
            		    mdetails.getSplPytName(),
            		    Common.convertByteToBoolean(mdetails.getSplEnable()));
            		    
            		actionForm.saveApFSList(apFSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findSupplier.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApFindSupplierAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               if (request.getParameter("child") == null) {

		          return mapping.findForward("apFindSupplier");
		          	
		        } else {
		          
		          return mapping.findForward("apFindSupplierChild");
		          
		        }

            }
                        
            if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            if (request.getParameter("child") == null) {

	          return mapping.findForward("apFindSupplier");
	          	
	        } else {
	        	           	          
	          return mapping.findForward("apFindSupplierChild");
	          
	        }

/*******************************************************
   -- Ap FS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap FS Open Action --
*******************************************************/

         } else if (request.getParameter("apFSList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {

         	ApFindSupplierList apFSList =
                actionForm.getApFSByIndex(actionForm.getRowSelected());

             String path = "/apSupplierEntry.do?forward=1" +
			     "&splCode=" + apFSList.getSplCode();

             actionForm.clearApBFSplList();
			   return(new ActionForward(path));

/*******************************************************
   -- Ap FS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearApFSList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            

            ArrayList list = null;
            Iterator i = null;

			// branch fields
			actionForm.clearApBFSplList();	        
			
			for (int j = 0; j < user.getBrnchCount(); j++) {

				 Branch branch = user.getBranch(j);
				 AdBranchDetails details = new AdBranchDetails();
				 details.setBrCode(new Integer(branch.getBrCode()));
				 details.setBrName(branch.getBrName());
				 ApBranchFindSupplierList apBFSplList = new ApBranchFindSupplierList(actionForm,
						details.getBrName(), details.getBrCode());
				 actionForm.saveApBFSplList(apBFSplList);
				 
			}
            
            try {
            	
            	
            	actionForm.clearSupplierTypeList();
            	
            	list = ejbFS.getApStAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setSupplierTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setSupplierTypeList((String)i.next());
            			
            		}
            		
            	}            	

            	actionForm.clearSupplierClassList();
            	
            	list = ejbFS.getApScAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setSupplierClassList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setSupplierClassList((String)i.next());
            			
            		}
            		
            	}
            
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApSupplierEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            
	        if (actionForm.getTableType() == null) {
	        	
	        	actionForm.setSupplierCode(null);
	        	actionForm.setName(null);
	        	actionForm.setSupplierType(Constants.GLOBAL_BLANK);
	        	actionForm.setSupplierClass(Constants.GLOBAL_BLANK);
	        	actionForm.setEnable(false);
	        	actionForm.setDisable(false);
	        	actionForm.setEmail(null);
	        	actionForm.setOrderBy(Constants.AP_FS_ORDER_BY_SUPPLIER_CODE);
	        
	            actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	            
	            HashMap criteria = new HashMap();
	            criteria.put("enable", new Byte((byte)0));
	            criteria.put("disable", new Byte((byte)0));
	            
	            adBrnchList = new ArrayList();
	            Object[] apBfsplList = actionForm.getApBFSplList();
	            
	            for(int j=0; j<apBfsplList.length; j++) {
	                
	                ApBranchFindSupplierList apBFSplList = actionForm.getApBFSplByIndex(j);
	                
	                if(apBFSplList.getBranchCheckbox() == true) {
	                    
	                    AdBranchDetails details = new AdBranchDetails();	                    
	                    details.setBrCode(apBFSplList.getBranchCode());
	                    adBrnchList.add(details);
	                }
	            }
	            
	            actionForm.setCriteria(criteria);
	            
	            actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            } else {
            	
            	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("enable")) {
            		
            		Byte b = (Byte)c.get("enable");
            		
            		actionForm.setEnable(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	if(c.containsKey("disable")) {
            		
            		Byte b = (Byte)c.get("disable");
            		
            		actionForm.setDisable(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	try {
                	
                	actionForm.clearApFSList();
                	
                	ArrayList newList = ejbFS.getApSplByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), 
						adBrnchList, user.getCmpCode());
                	
                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	  newList.remove(newList.size() - 1);
    	           	
    	           }
    	           
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
                		ApModSupplierDetails mdetails = (ApModSupplierDetails)j.next();
          		
                		ApFindSupplierList apFSList = new ApFindSupplierList(actionForm,
                		    mdetails.getSplCode(),
                		    mdetails.getSplSupplierCode(),
                		    mdetails.getSplName(),
                		    mdetails.getSplTin(),
                		    mdetails.getSplEmail(),
                		    mdetails.getSplStName(),
                		    mdetails.getSplScName(),
                		    mdetails.getSplPytName(),
                		    Common.convertByteToBoolean(mdetails.getSplEnable()));
                		    
                		actionForm.saveApFSList(apFSList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev next buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisablePreviousButton(true);
                   actionForm.setDisableFirstButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findSupplier.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in ApFindSupplierAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }
                        
            if (request.getParameter("child") == null) {

	          return mapping.findForward("apFindSupplier");
	          	
	        } else {
	          
	        	
	          try {
            	
	            	actionForm.setLineCount(0);
	            	
	            	HashMap criteria = new HashMap();
	            	criteria.put(new String("enable"), new Byte((byte)1));
	            	actionForm.setCriteria(criteria);
	        		
	        		actionForm.clearApFSList();

	        		adBrnchList = new ArrayList();

	        		// add the current branch to branchList
	        		
	            	if (adBrnchList.isEmpty()) {
	            		
            			AdBranchDetails details = new AdBranchDetails();	                    
            			details.setBrCode(new Integer(user.getCurrentBranch().getBrCode()));
            			adBrnchList.add(details);
	            		
	            	}
	            	
	            	ArrayList newList = ejbFS.getApSplByCriteria(actionForm.getCriteria(),
	            	    new Integer(actionForm.getLineCount()), 
	            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), 
						adBrnchList, user.getCmpCode());
	            	
		            actionForm.setDisablePreviousButton(true);
		            actionForm.setDisableFirstButton(true);
		           
		           // check if next should be disabled
		           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
		           	  
		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);
		           	  
		           } else {
		           	  
		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);
		           	  
		           	  //remove last record
		           	  newList.remove(newList.size() - 1);
		           	
		           }
		           
	               i = newList.iterator();
	            	
	               while (i.hasNext()) {
	            		
	            		ApModSupplierDetails mdetails = (ApModSupplierDetails)i.next();
	      		
	            		ApFindSupplierList apFSList = new ApFindSupplierList(actionForm,
	            		    mdetails.getSplCode(),
	            		    mdetails.getSplSupplierCode(),
	            		    mdetails.getSplName(),
	            		    mdetails.getSplTin(),
	            		    mdetails.getSplEmail(),
	            		    mdetails.getSplStName(),
	            		    mdetails.getSplScName(),
	            		    mdetails.getSplPytName(),
	            		    Common.convertByteToBoolean(mdetails.getSplEnable()));
	            		    
	            		actionForm.saveApFSList(apFSList);
	            		
	            	}
	
	            } catch (GlobalNoRecordFoundException ex) {
	               
	               // disable prev next buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisablePreviousButton(true);
	               actionForm.setDisableFirstButton(true);
	               actionForm.setDisableLastButton(true);
	
	          } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in ApFindSupplierAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	          }
	          
	          actionForm.setSelectedSupplierCode(request.getParameter("selectedSupplierCode"));
              actionForm.setSelectedSupplierName(request.getParameter("selectedSupplierName"));
              actionForm.setSelectedSupplierEntered(request.getParameter("selectedSupplierEntered"));
	          return mapping.findForward("apFindSupplierChild");
	          
	        }

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApFindSupplierAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}