package com.struts.ap.findsupplier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class ApFindSupplierForm extends ActionForm implements Serializable {

   private String supplierCode = null;
   private String name = null;
   private String supplierType = null;
   private ArrayList supplierTypeList = new ArrayList();
   private String supplierClass = null;
   private ArrayList supplierClassList = new ArrayList();
   private boolean enable = false;
   private boolean disable = false;
   private String email = null;   
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String pageState = new String();
   private ArrayList apFSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private String selectedSupplierCode = null;
   private String selectedSupplierName = null;
   private String selectedSupplierEntered = null;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   private ArrayList apBFSplList = new ArrayList();
   
   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ApFindSupplierList getApFSByIndex(int index) {

      return((ApFindSupplierList)apFSList.get(index));

   }

   public Object[] getApFSList() {

      return apFSList.toArray();

   }

   public int getApFSListSize() {

      return apFSList.size();

   }

   public void saveApFSList(Object newApFSList) {

      apFSList.add(newApFSList);

   }

   public void clearApFSList() {
   	
      apFSList.clear();
      
   }

   public void setRowSelected(Object selectedApFSList, boolean isEdit) {

      this.rowSelected = apFSList.indexOf(selectedApFSList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getSupplierCode() {

      return supplierCode;

   }

   public void setSupplierCode(String supplierCode) {

      this.supplierCode = supplierCode;

   }

   public String getName() {
   	
      return name;
   
   }

   public void setName(String name) {
   
      this.name = name;
   
   }
   
   public String getSupplierType() {

      return supplierType;

   }

   public void setSupplierType(String supplierType) {

      this.supplierType = supplierType;

   }                         

   public ArrayList getSupplierTypeList() {

      return supplierTypeList;

   }

   public void setSupplierTypeList(String supplierType) {

      supplierTypeList.add(supplierType);

   }
   
   public void clearSupplierTypeList() {

      supplierTypeList.clear();
      supplierTypeList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getSupplierClass() {

      return supplierClass;

   }

   public void setSupplierClass(String supplierClass) {

      this.supplierClass = supplierClass;

   }                         

   public ArrayList getSupplierClassList() {

      return supplierClassList;

   }

   public void setSupplierClassList(String supplierClass) {

      supplierClassList.add(supplierClass);

   }
   
   public void clearSupplierClassList() {

      supplierClassList.clear();
      supplierClassList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getEmail() {
   	
   	  return email;
   	  
   }
   
   public void setEmail(String email) {
   	
   	  this.email = email;
   	  
   }
 
   public boolean getEnable() {
   	
   	  return enable;
   	  
   }
   
   public void setEnable(boolean enable) {
   	
   	  this.enable = enable;
   	  
   }
   
   public boolean getDisable() {
   	
   	  return disable;
   	  
   }
   
   public void setDisable(boolean disable) {
   	
   	  this.disable = disable;
   	  
   } 
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public String getSelectedSupplierCode() {
   	
   	  return selectedSupplierCode;
   	
   }
   
   public void setSelectedSupplierCode(String selectedSupplierCode) {
   	
   	  this.selectedSupplierCode = selectedSupplierCode;
   	
   }
   
   public String getSelectedSupplierName() {
   	
   	  return selectedSupplierName;
   	
   }
   
   public void setSelectedSupplierName(String selectedSupplierName) {
   	
   	  this.selectedSupplierName = selectedSupplierName;
   	
   }
   
   public String getSelectedSupplierEntered() {
   	
   	  return selectedSupplierEntered;
   	
   }
   
   public void setSelectedSupplierEntered(String selectedSupplierEntered) {
   	
   	  this.selectedSupplierEntered = selectedSupplierEntered;
   	
   }

	public Object[] getApBFSplList(){

		return apBFSplList.toArray();
		
	}
	
	public ApBranchFindSupplierList getApBFSplByIndex(int index) {
		
		return ((ApBranchFindSupplierList)apBFSplList.get(index));
		
	}
	
	public int getApBFSplListSize(){
		
		return(apBFSplList.size());
		
	}
	
	public void saveApBFSplList(Object newApBFSplList){
		
		apBFSplList.add(newApBFSplList);   	  
		
	}
	
	public void clearApBFSplList(){
		
		apBFSplList.clear();
		
	}
	
	public void setApBFSplList(ArrayList apBFSplList) {
		
		this.apBFSplList = apBFSplList;
		
	}
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

	for (int i=0; i<apBFSplList.size(); i++) {
		
		ApBranchFindSupplierList actionList = (ApBranchFindSupplierList)apBFSplList.get(i);
		actionList.setBranchCheckbox(false);
		
	}

   	  enable = false;
   	  disable = false;
   	
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AP_FS_ORDER_BY_SUPPLIER_CODE);
	      orderByList.add(Constants.AP_FS_ORDER_BY_SUPPLIER_NAME);
	         
	  }     
	  
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

      }
      
      return errors;

   }
}