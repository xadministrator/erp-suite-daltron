package com.struts.ap.findsupplier;

import java.io.Serializable;

public class ApFindSupplierList implements Serializable {

   private Integer splCode = null;
   private String supplierCode = null;
   private String name = null;
   private String tinNumber = null;
   private String email = null;
   private String supplierType = null;
   private String supplierClass = null;
   private String paymentTerm = null;
   private boolean enable = false;

   private String openButton = null;
       
   private ApFindSupplierForm parentBean;
    
   public ApFindSupplierList(ApFindSupplierForm parentBean,
	  Integer splCode,  
	  String supplierCode,  
	  String name,  
	  String tinNumber,
	  String email,  
	  String supplierType,  
	  String supplierClass,
	  String paymentTerm,  
	  boolean enable) {

      this.parentBean = parentBean;
      this.splCode = splCode;
      this.supplierCode = supplierCode;
      this.name = name;
      this.tinNumber = tinNumber;
      this.email = email;
      this.supplierType = supplierType;
      this.supplierClass = supplierClass;
      this.paymentTerm = paymentTerm;
      this.enable = enable;
   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }
   
   public Integer getSplCode() {
   	
   	  return splCode;
   	  
   }

   public String getSupplierCode() {

      return supplierCode;

   }
   
   public String getName() {

      return name;

   }
      
   public String getTinNumber() {
   	
   	  return tinNumber;
   	 
   }

   public String getEmail() {
   	
   	  return email;
   	  
   }
   
   public String getSupplierType() {
   	
   	  return supplierType;
   	  
   }
   
   public String getSupplierClass() {
   	
   	  return supplierClass;
   	  
   }
   
   public String getPaymentTerm() {
   	
   	  return paymentTerm;
   	  
   }   
   
   public boolean getEnable() {
   	
   	  return enable;
   	  
   }
      
}