package com.struts.ap.findvoucher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApFindVoucherController;
import com.ejb.txn.ApFindVoucherControllerHome;
import com.ejb.txn.ApVoucherEntryController;
import com.ejb.txn.ApVoucherEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModVoucherDetails;
import com.util.ArModInvoiceDetails;

public final class ApFindVoucherAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApFindVoucherAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ApFindVoucherForm actionForm = (ApFindVoucherForm)form;

         String frParam = null;

         if (request.getParameter("child") == null) {

         	frParam = Common.getUserPermission(user, Constants.AP_FIND_VOUCHER_ID);

         } else {

         	frParam = Constants.FULL_ACCESS;

         }

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  if (request.getParameter("child") == null) {

                  	return mapping.findForward("apFindVoucher");

                  } else {

                  	return mapping.findForward("apFindVoucherChild");

                  }
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApFindVoucherController EJB
*******************************************************/

         ApFindVoucherControllerHome homeFV = null;
         ApFindVoucherController ejbFV = null;

         ApVoucherEntryControllerHome homeVOU = null;
         ApVoucherEntryController ejbVOU = null;

         try {

            homeFV = (ApFindVoucherControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApFindVoucherControllerEJB", ApFindVoucherControllerHome.class);

            homeVOU = (ApVoucherEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ApVoucherEntryControllerEJB", ApVoucherEntryControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApFindVoucherAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFV = homeFV.create();

            ejbVOU = homeVOU.create();

         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApFindVoucherAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
 /*******************************************************
     Call ApFindVoucherController EJB
     getGlFcPrecisionUnit
  *******************************************************/
         short precisionUnit = 0;
         boolean enableVoucherBatch = false;
         boolean useSupplierPulldown = true;

         try {

            precisionUnit = ejbFV.getGlFcPrecisionUnit(user.getCmpCode());
            enableVoucherBatch = Common.convertByteToBoolean(ejbFV.getAdPrfEnableApVoucherBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableVoucherBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbFV.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);


         } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApFindVoucherAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));
         }

/*******************************************************
   -- Ap FV Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_DETAILED);

	        if (request.getParameter("child") == null) {

	        	return mapping.findForward("apFindVoucher");

	        } else {

	        	return mapping.findForward("apFindVoucherChild");

	        }

/*******************************************************
   -- Ap FV Hide Details Action --
*******************************************************/

	     } else if (request.getParameter("hideDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	        if (request.getParameter("child") == null) {

	        	return mapping.findForward("apFindVoucher");

	        } else {

	        	return mapping.findForward("apFindVoucherChild");

	        }

/*******************************************************
    -- Ap FV First Action --
*******************************************************/

	     } else if(request.getParameter("firstButton") != null){

	     	actionForm.setLineCount(0);

/*******************************************************
   -- Ap FV Previous Action --
*******************************************************/

         } else if(request.getParameter("previousButton") != null){

         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);

/*******************************************************
   -- Ap FV Next Action --
*******************************************************/

         }else if(request.getParameter("nextButton") != null){

         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);




 /*******************************************************
     / -- Ap Delete All Action --
   /*******************************************************/

         } else if(request.getParameter("deleteAllButton") != null){

           actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
             if(actionForm.getCriteria().size()<=0){

             }
             ArrayList list = ejbFV.getApVouByCriteria(actionForm.getCriteria(),
             	    0,
             	    0,
             	    actionForm.getOrderBy(),
             	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());




             for(int x=0;x<list.size();x++){
            	 ApModVoucherDetails vouDetails = new ApModVoucherDetails();

            	 vouDetails = (ApModVoucherDetails)list.get(x);
            	 ejbVOU.deleteApVouEntry(vouDetails.getVouCode(), user.getUserName(), user.getCmpCode());
             }


         }

/*******************************************************
   -- Ap FV Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null || request.getParameter("findRejected") != null) {

             // create criteria

            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();

	        	if (!Common.validateRequired(actionForm.getBatchName())) {

	        		criteria.put("batchName", actionForm.getBatchName());

	        	}

	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {

	        		criteria.put("supplierCode", actionForm.getSupplierCode());

	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {

	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {

	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

	        	}

	        	if (!Common.validateRequired(actionForm.getDateTo())) {

	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

	        	}

	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {

	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());

	        	}

	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {

	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());

	        	}


	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {

	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());

	        	}

	        	if (!Common.validateRequired(actionForm.getCurrency())) {

	        		criteria.put("currency", actionForm.getCurrency());

	        	}

	        	if (!Common.validateRequired(actionForm.getPosted())) {

	        		criteria.put("posted", actionForm.getPosted());

	        	}

	        	if (!Common.validateRequired(actionForm.getPaymentStatus())) {

	        		criteria.put("paymentStatus", actionForm.getPaymentStatus());

	        	}

	        	if (actionForm.getDebitMemo()) {

	        		criteria.put("debitMemo", new Byte((byte)1));

                }

                if (!actionForm.getDebitMemo()) {

                	criteria.put("debitMemo", new Byte((byte)0));

                }

                if (actionForm.getVoucherVoid()) {

	        		criteria.put("voucherVoid", new Byte((byte)1));

                }

                if (!actionForm.getVoucherVoid()) {

                	criteria.put("voucherVoid", new Byte((byte)0));

                }

                if (!actionForm.getVoucherGenerated()) {

                	criteria.put("voucherGenerated", new Byte((byte)0));

                }

                if (actionForm.getPaymentRequest()) {

	        		criteria.put("paymentRequest", new Byte((byte)1));

                }

                if (!actionForm.getPaymentRequest()) {

                	criteria.put("paymentRequest", new Byte((byte)0));

                }

                if (!Common.validateRequired(actionForm.getUser())) {

                	criteria.put("user", actionForm.getUser());

                }

	        	//save criteria

	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);

	     	} else if(request.getParameter("findRejected") != null){

				HashMap criteria = new HashMap();

				criteria.put("paymentRequest", new Byte((byte)0));
				actionForm.setPaymentRequest(false);
				criteria.put("debitMemo", new Byte((byte)0));
				actionForm.setDebitMemo(false);
				criteria.put("posted", Constants.GLOBAL_NO);
				actionForm.setPosted("NO");
				criteria.put("approvalStatus", "REJECTED");
				actionForm.setApprovalStatus("REJECTED");
				criteria.put("voucherVoid", new Byte((byte)0));
        	    actionForm.setLineCount(0);
        	    actionForm.setCriteria(criteria);
        	    actionForm.setOrderBy("DOCUMENT NUMBER");
	     	}

            if(request.getParameter("lastButton") != null) {

            	int size = ejbFV.getApVouSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();

            	System.out.println("SIZE="+size);
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {

            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));

            	} else {

            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);

            	}




            }

            try {

            	actionForm.clearApFVList();

            	ArrayList list = ejbFV.getApVouByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()),
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
            	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {

	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);

	           } else {

	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);

	           }

	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {

	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);

	           } else {

	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);

	           	  //remove last record
	           	  list.remove(list.size() - 1);

	           }

            	Iterator i = list.iterator();

            	while (i.hasNext()) {

            		ApModVoucherDetails mdetails = (ApModVoucherDetails)i.next();

            		ApFindVoucherList apFVList = new ApFindVoucherList(actionForm,
            		    mdetails.getVouCode(),
            		    Common.convertByteToBoolean(mdetails.getVouDebitMemo()),
            		    mdetails.getVouSplName(),

            		    Common.convertSQLDateToString(mdetails.getVouDate()),
                        mdetails.getVouDocumentNumber(),
                        mdetails.getVouReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),
                        mdetails.getVouType(),
            		    Common.convertDoubleToStringMoney(mdetails.getVouAmountPaid(), precisionUnit));

            		actionForm.saveApFVList(apFVList);

            	}

            } catch (GlobalNoRecordFoundException ex) {

               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findVoucher.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApFindVoucherAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

               if (request.getParameter("child") == null) {

               		return mapping.findForward("apFindVoucher");

               } else {

               		return mapping.findForward("apFindVoucherChild");

               }

            }

            actionForm.reset(mapping, request);

            if (actionForm.getTableType() == null) {

	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	        }

	        if (request.getParameter("child") == null) {

	        	return mapping.findForward("apFindVoucher");

	        } else {

	        	return mapping.findForward("apFindVoucherChild");

	        }

/*******************************************************
   -- Ap FV Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap FV Open Action --
*******************************************************/

         } else if (request.getParameter("apFVList[" +
            actionForm.getRowSelected() + "].openButton") != null) {

             ApFindVoucherList apFVList =
                actionForm.getApFVByIndex(actionForm.getRowSelected());

             String path = null;

				System.out.println("is db: " + apFVList.getDebitMemo() );
				System.out.println("is payemnt req: " +actionForm.getPaymentRequest());



			if(apFVList.getVouType()!=null){
			
				 if(apFVList.getVouType().equals("REQUEST")) {
            	 	System.out.println("request");
            	 	path = "/apCheckPaymentRequestEntry.do?forward=1" +
					     "&paymentRequestCode=" + apFVList.getVoucherCode();
	             }  else if (!apFVList.getDebitMemo() && actionForm.getPaymentRequest() == false) {
					System.out.println("non db and non pr");
		  	        path = "/apVoucherEntry.do?forward=1" +
					     "&voucherCode=" + apFVList.getVoucherCode();
	
			     } else if (apFVList.getDebitMemo() && actionForm.getPaymentRequest() == false){
	
					System.out.println("debit memo");
			     	path = "/apDebitMemoEntry.do?forward=1" +
					     "&debitMemoCode=" + apFVList.getVoucherCode();
	
			     } else  {
			     	System.out.println("pr");
			    	 path = "/apCheckPaymentRequestEntry.do?forward=1" +
						     "&paymentRequestCode=" + apFVList.getVoucherCode();
	
			     }

			}else{
				if (!apFVList.getDebitMemo() && actionForm.getPaymentRequest() == false) {
				System.out.println("non db and non pr");
	  	        path = "/apVoucherEntry.do?forward=1" +
				     "&voucherCode=" + apFVList.getVoucherCode();

			     } else if (apFVList.getDebitMemo() && actionForm.getPaymentRequest() == false){
	
					System.out.println("debit memo");
			     	path = "/apDebitMemoEntry.do?forward=1" +
					     "&debitMemoCode=" + apFVList.getVoucherCode();
	
			     } else  {
			     	System.out.println("pr");
			    	 path = "/apCheckPaymentRequestEntry.do?forward=1" +
						     "&paymentRequestCode=" + apFVList.getVoucherCode();
	
			     }
			
			}

            

			 return(new ActionForward(path));

/*******************************************************
   -- Ap FV Load Action --
*******************************************************/

         }
         if (frParam != null) {

         	actionForm.clearApFVList();
            ArrayList list = null;
            Iterator i = null;

            try {

            	if(actionForm.getUseSupplierPulldown()) {

	            	actionForm.clearSupplierCodeList();

	            	list = ejbFV.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setSupplierCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setSupplierCodeList((String)i.next());

	            		}

	            	}

            	}

            	actionForm.clearCurrencyList();

            	list = ejbFV.getGlFcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setCurrencyList((String)i.next());

            		}

            	}

            	actionForm.clearBatchNameList();

            	list = ejbFV.getApOpenVbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBatchNameList((String)i.next());

            		}

            	}

            	actionForm.clearUserList();

            	ArrayList userlist = ejbFV.getAdUsrAll(user.getCmpCode());

            	if (userlist == null || userlist.size() == 0) {

            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = userlist.iterator();

            		while (i.hasNext()) {

            			actionForm.setUserList((String)i.next());

            		}

            	}

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("goButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            if (request.getParameter("findGeneration") != null) {

				System.out.println("1");
	        	actionForm.setBatchName(Constants.GLOBAL_BLANK);
	        	actionForm.setSupplierCode(Constants.GLOBAL_BLANK);
	        	actionForm.setDebitMemo(false);
	        	actionForm.setVoucherVoid(false);
	        	actionForm.setPaymentRequest(false);
	        	actionForm.setDateFrom(null);
	        	actionForm.setDateTo(null);
	        	actionForm.setDocumentNumberFrom(null);
	        	actionForm.setDocumentNumberTo(null);
	        	actionForm.setReferenceNumber(null);
	        	actionForm.setApprovalStatus("APPROVED");
	        	actionForm.setCurrency(Constants.GLOBAL_BLANK);
	        	actionForm.setPosted(Constants.GLOBAL_YES);
	        	actionForm.setPaymentStatus(Constants.GLOBAL_BLANK);
	        	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
	        	actionForm.reset(mapping, request);

	        	HashMap criteria = new HashMap();
	        	criteria.put("debitMemo", new Byte((byte)0));
	        	criteria.put("voucherVoid", new Byte((byte)0));
	        	criteria.put("paymentRequest", new Byte((byte)1));
	        	criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        	criteria.put("posted", actionForm.getPosted());
	        	criteria.put("voucherGenerated", new Byte((byte)0));


	        	if(request.getParameter("findGeneration") != null) {

	            	if(request.getParameter("debitMemo") != null) {

	            		return new ActionForward("/apFindVoucher.do?goButton=1&debitMemo=true");

	            	} else {

	            		actionForm.setVoucherGenerated(false);
	            		return new ActionForward("/apFindVoucher.do?goButton=1");

	            	}

	            }


	        } else if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {

				System.out.println("2");
	        	actionForm.setUser(null);
	        	actionForm.setBatchName(Constants.GLOBAL_BLANK);
	        	actionForm.setSupplierCode(Constants.GLOBAL_BLANK);
	        	actionForm.setDebitMemo(false);
	        	actionForm.setVoucherVoid(false);
	        	actionForm.setPaymentRequest(false);
	        	actionForm.setDateFrom(null);
	        	actionForm.setDateTo(null);
	        	actionForm.setDocumentNumberFrom(null);
	        	actionForm.setDocumentNumberTo(null);
	        	actionForm.setReferenceNumber(null);
	        	actionForm.setCurrency(Constants.GLOBAL_BLANK);
	        	actionForm.setApprovalStatus("DRAFT");
	        	actionForm.setPosted(Constants.GLOBAL_NO);
	        	actionForm.setPaymentStatus(Constants.GLOBAL_BLANK);
	        	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
	        	actionForm.reset(mapping, request);

	        	HashMap criteria = new HashMap();
	        	criteria.put("debitMemo", new Byte((byte)0));
	        	criteria.put("voucherVoid", new Byte((byte)0));
	        	criteria.put("paymentRequest", new Byte((byte)0));
	        	criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        	criteria.put("posted", actionForm.getPosted());

	        	actionForm.setCriteria(criteria);

	        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	        	if(request.getParameter("findDraft") != null) {

	            	if(request.getParameter("debitMemo") != null) {

	            		return new ActionForward("/apFindVoucher.do?goButton=1&debitMemo=true");

	            	} else {

	            		return new ActionForward("/apFindVoucher.do?goButton=1");

	            	}

	            }

	        } else if (request.getParameter("findProcessing") != null) {


				System.out.println("3");
	        	actionForm.setBatchName(Constants.GLOBAL_BLANK);
	        	actionForm.setSupplierCode(Constants.GLOBAL_BLANK);
	        	actionForm.setDebitMemo(false);
	        	actionForm.setVoucherVoid(false);
	        	actionForm.setPaymentRequest(false);
	        	actionForm.setDateFrom(null);
	        	actionForm.setDateTo(null);
	        	actionForm.setDocumentNumberFrom(null);
	        	actionForm.setDocumentNumberTo(null);
	        	actionForm.setReferenceNumber(null);
	        	actionForm.setApprovalStatus("DRAFT");
	        	actionForm.setCurrency(Constants.GLOBAL_BLANK);
	        	actionForm.setPosted(Constants.GLOBAL_YES);
	        	actionForm.setPaymentStatus(Constants.GLOBAL_BLANK);
	        	actionForm.setOrderBy(Constants.GLOBAL_BLANK);

	        	HashMap criteria = new HashMap();
	        	criteria.put("debitMemo", new Byte((byte)0));
	        	criteria.put("voucherVoid", new Byte((byte)0));
	        	criteria.put("paymentRequest", new Byte((byte)1));
	        	criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        	criteria.put("posted", actionForm.getPosted());



	        	if(request.getParameter("findProcessing") != null) {

	            	if(request.getParameter("debitMemo") != null) {

	            		return new ActionForward("/apFindVoucher.do?goButton=1&debitMemo=true");

	            	} else {

	            		return new ActionForward("/apFindVoucher.do?goButton=1");

	            	}

	            }

            } else {


				System.out.println("4");
            	HashMap c = actionForm.getCriteria();

            	if(c.containsKey("debitMemo")) {

            		Byte b = (Byte)c.get("debitMemo");

            		actionForm.setDebitMemo(Common.convertByteToBoolean(b.byteValue()));

            	}

            	if(c.containsKey("voucherVoid")) {

            		Byte b = (Byte)c.get("voucherVoid");

            		actionForm.setVoucherVoid(Common.convertByteToBoolean(b.byteValue()));

            	}

            	if(c.containsKey("paymentRequest")) {

            		Byte b = (Byte)c.get("paymentRequest");

            		actionForm.setPaymentRequest(Common.convertByteToBoolean(b.byteValue()));

            	}

            	try {

            		actionForm.clearApFVList();

                	ArrayList newList = ejbFV.getApVouByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()),
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
                	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {

    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);

    	           } else {

    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);

    	           }

    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);

    	           } else {

    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);

    	           	  //remove last record
    	           	newList.remove(newList.size() - 1);

    	           }

                	Iterator j = newList.iterator();

                	while (j.hasNext()) {

                		ApModVoucherDetails mdetails = (ApModVoucherDetails)j.next();

                		ApFindVoucherList apFVList = new ApFindVoucherList(actionForm,
                		    mdetails.getVouCode(),
                		    Common.convertByteToBoolean(mdetails.getVouDebitMemo()),
                		    mdetails.getVouSplName(),

                            Common.convertSQLDateToString(mdetails.getVouDate()),
                            mdetails.getVouDocumentNumber(),
                            mdetails.getVouReferenceNumber(),
                            Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),
                            mdetails.getVouType(),
                		    Common.convertDoubleToStringMoney(mdetails.getVouAmountPaid(), precisionUnit));

                		actionForm.saveApFVList(apFVList);

                	}

                } catch (GlobalNoRecordFoundException ex) {

                   // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);

                   if(actionForm.getLineCount() > 0) {

                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);

                   } else {

                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);

                   }

                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findVoucher.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in ApFindVoucherAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage");

                   }

                }

            }


	        if (request.getParameter("child") == null) {

	        	return mapping.findForward("apFindVoucher");

	        } else {

	        	try {

	        		actionForm.setLineCount(0);

	        		HashMap criteria = new HashMap();
	        		criteria.put(new String("debitMemo"), new Byte((byte)0));
	            	criteria.put(new String("voucherVoid"), new Byte((byte)0));
	            	criteria.put(new String("paymentRequest"), new Byte((byte)0));
	            	criteria.put(new String("posted"), "YES");
	            	criteria.put(new String("paymentStatus"), "UNPAID");
	        		actionForm.setCriteria(criteria);

	        		actionForm.clearApFVList();

	        		ArrayList newList = ejbFV.getApVouByCriteria(actionForm.getCriteria(),
	        				new Integer(actionForm.getLineCount()),
							new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	        		actionForm.setDisablePreviousButton(true);
	        		actionForm.setDisableFirstButton(true);

	        		// check if next should be disabled
	        		if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

	        			actionForm.setDisableNextButton(true);
	        			actionForm.setDisableLastButton(true);

	        		} else {

	        			actionForm.setDisableNextButton(false);
	        			actionForm.setDisableLastButton(false);

	        			//remove last record
	        			newList.remove(newList.size() - 1);

	        		}

	        		i = newList.iterator();

	        		while (i.hasNext()) {

	        			ApModVoucherDetails mdetails = (ApModVoucherDetails)i.next();

	        			ApFindVoucherList apFVList = new ApFindVoucherList(actionForm,
	        					mdetails.getVouCode(),
								Common.convertByteToBoolean(mdetails.getVouDebitMemo()),
								mdetails.getVouSplName(),

								Common.convertSQLDateToString(mdetails.getVouDate()),
								mdetails.getVouDocumentNumber(),
								mdetails.getVouReferenceNumber(),
								Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),
								mdetails.getVouType(),
								Common.convertDoubleToStringMoney(mdetails.getVouAmountPaid(), precisionUnit));

	        			actionForm.saveApFVList(apFVList);

	        		}

	        	} catch (GlobalNoRecordFoundException ex) {

	        		// disable prev, next, first & last buttons
	        		actionForm.setDisableNextButton(true);
	        		actionForm.setDisableLastButton(true);

	        		if(actionForm.getLineCount() > 0) {

	                   		actionForm.setDisableFirstButton(false);
	                   		actionForm.setDisablePreviousButton(false);

	                   } else {

	                   		actionForm.setDisableFirstButton(true);
	                   		actionForm.setDisablePreviousButton(true);

	                   }

	        	} catch (EJBException ex) {

	        		if (log.isInfoEnabled()) {

	        			log.info("EJBException caught in ApFindVoucherAction.execute(): " + ex.getMessage() +
	        					" session: " + session.getId());
	        			return mapping.findForward("cmnErrorPage");

	        		}

	        	}

	        	actionForm.setSelectedVoucherNumber(request.getParameter("selectedVoucherNumber"));
	        	return mapping.findForward("apFindVoucherChild");

	        }

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApFindVoucherAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}