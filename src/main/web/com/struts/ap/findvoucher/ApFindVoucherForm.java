package com.struts.ap.findvoucher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;


public class ApFindVoucherForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String supplierCode = null;
   private ArrayList supplierCodeList = new ArrayList();
   private boolean debitMemo = false;
   private boolean voucherVoid = false;
   private boolean voucherGenerated = true;
   private boolean paymentRequest = false;
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String referenceNumber = null;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String paymentStatus = null;
   private ArrayList paymentStatusList = new ArrayList();      
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String tableType = null;
   private String user = null;
	private ArrayList userList = new ArrayList();

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private boolean useSupplierPulldown =  true;
   private String pageState = new String();
   private ArrayList apFVList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private String selectedVoucherNumber = null;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ApFindVoucherList getApFVByIndex(int index) {

      return((ApFindVoucherList)apFVList.get(index));

   }

   public Object[] getApFVList() {

      return apFVList.toArray();

   }

   public int getApFVListSize() {

      return apFVList.size();

   }

   public void saveApFVList(Object newApFVList) {

      apFVList.add(newApFVList);

   }

   public void clearApFVList() {
      apFVList.clear();
   }

   public void setRowSelected(Object selectedApFVList, boolean isEdit) {

      this.rowSelected = apFVList.indexOf(selectedApFVList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showApFVRow(int rowSelected) {

   }

   public void updateApFVRow(int rowSelected, Object newApFVList) {

      apFVList.set(rowSelected, newApFVList);

   }

   public void deleteApFVList(int rowSelected) {

      apFVList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getSelectedVoucherNumber() {
   	
   	  return selectedVoucherNumber;
   	
   }
   
   public void setSelectedVoucherNumber(String selectedVoucherNumber) {
   	
   	  this.selectedVoucherNumber = selectedVoucherNumber;
   	
   }

   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getSupplierCode() {

      return supplierCode;

   }

   public void setSupplierCode(String supplierCode) {

      this.supplierCode = supplierCode;

   }

   public ArrayList getSupplierCodeList() {

      return supplierCodeList;

   }

   public void setSupplierCodeList(String supplierCode) {

      supplierCodeList.add(supplierCode);

   }

   public void clearSupplierCodeList() {

   	  supplierCodeList.clear();
      supplierCodeList.add(Constants.GLOBAL_BLANK);
   }
   
   public boolean getDebitMemo() {
   	
   	  return debitMemo;
   	  
   }
   
   public void setDebitMemo(boolean debitMemo) {
   	
   	  this.debitMemo = debitMemo;
   	  
   }  
   
   public boolean getVoucherVoid() {
   	
   	  return voucherVoid;
   	  
   }
   
   public void setVoucherVoid(boolean voucherVoid) {
   	
   	  this.voucherVoid = voucherVoid;
   	  
   }   
   
   public boolean getVoucherGenerated() {
   	
   	  return voucherGenerated;
   	  
   }
   
   public void setVoucherGenerated(boolean voucherGenerated) {
   	
   	  this.voucherGenerated = voucherGenerated;
   	  
   } 
   
   public boolean getPaymentRequest() {
	   	
	   	  return paymentRequest;
	   	  
   }
   
   public void setPaymentRequest(boolean paymentRequest) {
   	
   	  this.paymentRequest = paymentRequest;
   	  
   }
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	  
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   	  
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
   
   public String getDocumentNumberFrom() {
   	
   	  return documentNumberFrom;
   	  
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom) {
   	
   	  this.documentNumberFrom = documentNumberFrom;
   	  
   }
   
   public String getDocumentNumberTo() {
   	
   	  return documentNumberTo;
   	  
   }
   
   public void setDocumentNumberTo(String documentNumberTo) {
   	
   	  this.documentNumberTo = documentNumberTo;
   	  
   } 

   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	  this.referenceNumber = referenceNumber;
   	  
   }

   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }

   public String getCurrency() {
   	
   	  return currency;
   
   }
   
   public void setCurrency(String currency) {
   
   	  this.currency = currency;
   
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   
   }
      
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }
   
   public String getPaymentStatus() {
   	
   	  return paymentStatus;
   	  
   }
   
   public void setPaymentStatus(String paymentStatus) {
   	
   	  this.paymentStatus = paymentStatus;
   	  
   }
   
   public ArrayList getPaymentStatusList() {
   	
   	  return paymentStatusList;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   		
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   		
   		this.useSupplierPulldown = useSupplierPulldown;   		
		
   }
   
   public String getUser() {
		
		return user;
		
	}
	
	public void setUser(String user) {
		
		this.user = user;
		
	}
	
	public ArrayList getUserList() {
		
		return userList;
		
	}
	
	public void setUserList(String user) {
		
		userList.add(user);
		
	}
	
	public void clearUserList() {
		
		userList.clear();
		userList.add(Constants.GLOBAL_BLANK);
		
	}

   public void reset(ActionMapping mapping, HttpServletRequest request) {

	   HttpSession session = request.getSession();
	   
		User activeUser = (User) session.getAttribute(Constants.USER_KEY);
		user = activeUser.getUserName();
	   
	   
   	  debitMemo = false;
      voucherVoid = false;
      voucherGenerated = true;
      paymentRequest = false;	  
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
           
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add(Constants.GLOBAL_YES);
      postedList.add(Constants.GLOBAL_NO);
      
      paymentStatusList.clear();
      paymentStatusList.add(Constants.GLOBAL_BLANK);
      paymentStatusList.add(Constants.AP_FV_PAYMENT_STATUS_PAID);
      paymentStatusList.add(Constants.AP_FV_PAYMENT_STATUS_UNPAID);
        
      
      if (orderByList.isEmpty()) {
      	
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AP_FV_ORDER_BY_SUPPLIER_CODE);
	      orderByList.add(Constants.AP_FV_ORDER_BY_DOCUMENT_NUMBER);
	      
	  }

	  showDetailsButton = null;
	  hideDetailsButton = null;
	  

      if (request.getParameter("findGeneration")!=null) voucherGenerated=false;
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

      	 if(useSupplierPulldown) {
      	 	
	         if (!Common.validateStringExists(supplierCodeList, supplierCode)) {
	
	            errors.add("supplierCode",
	               new ActionMessage("findVoucher.error.supplierCodeInvalid"));
	
	         }
	         
      	 }
         
         if (!Common.validateDateFormat(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("findVoucher.error.dateFromInvalid"));

         }         
         
         if (!Common.validateDateFormat(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("findVoucher.error.dateToInvalid"));

         }                  
                  
      }
      
      return errors;

   }
   
}