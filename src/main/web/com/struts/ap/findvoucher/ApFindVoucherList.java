package com.struts.ap.findvoucher;

import java.io.Serializable;

public class ApFindVoucherList implements Serializable {

   private Integer voucherCode = null;
   private boolean debitMemo = false;
   private String vouType = null;
   private String supplierName = null;
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;  
   private String amountDue = null;
   private String amountPaid = null;
   
   private String openButton = null;
       
   private ApFindVoucherForm parentBean;
    
   public ApFindVoucherList(ApFindVoucherForm parentBean,
      Integer voucherCode,
      boolean debitMemo,
      
      String supplierName,
      String date,
      String documentNumber,
      String referenceNumber,
      String amountDue,
      String vouType,
      String amountPaid) {

      this.parentBean = parentBean;
      this.voucherCode = voucherCode;
      this.debitMemo = debitMemo;
      this.vouType = vouType;
      this.supplierName = supplierName;
      this.date = date;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amountDue = amountDue;
      this.amountPaid = amountPaid;
      
   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }

   public Integer getVoucherCode() {

      return voucherCode;

   }
   
   public boolean getDebitMemo() {
   	
   	  return debitMemo;
   	
   }
   
   public String getVouType() {
	   	
	   	  return vouType;
	   	
	   }

   public String getSupplierName() {

      return supplierName;
      
   }

   public String getDate() {
   
      return date;
      
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmountDue() {
   
      return amountDue;
   
   }

   public String getAmountPaid() {
   
      return amountPaid;
   
   }
      
   
}