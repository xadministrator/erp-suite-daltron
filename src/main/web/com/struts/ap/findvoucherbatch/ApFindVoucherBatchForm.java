package com.struts.ap.findvoucherbatch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApFindVoucherBatchForm extends ActionForm implements Serializable {

   private String batchName = null;
   private String status = null;
   private ArrayList statusList = new ArrayList();
   private String dateCreated = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
   private String department = null;
   private ArrayList departmentList = new ArrayList();
  
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private ArrayList apFVBList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ApFindVoucherBatchList getApFVBByIndex(int index){
      return((ApFindVoucherBatchList)apFVBList.get(index));
   }

   public Object[] getApFVBList(){
      return(apFVBList.toArray());
   }

   public int getApFVBListSize(){
      return(apFVBList.size());
   }

   public void saveApFVBList(Object newApFVBList){
      apFVBList.add(newApFVBList);
   }

   public void clearApFVBList(){
      apFVBList.clear();
   }

   public void setRowSelected(Object selectedApFVBList, boolean isEdit){
      this.rowSelected = apFVBList.indexOf(selectedApFVBList);
   }

   public void updateApFVBRow(int rowSelected, Object newApFVBList){
      apFVBList.set(rowSelected, newApFVBList);
   }

   public void deleteApFVBList(int rowSelected){
      apFVBList.remove(rowSelected);
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getBatchName(){
      return(batchName);
   }

   public void setBatchName(String batchName){
      this.batchName = batchName;
   }
      
   public String getStatus() {
   	  return(status);	  
   }
   
   public void setStatus(String status) {
   	  this.status = status;
   }
   
   public ArrayList getStatusList() {
   	  return(statusList);
   }
      
   public String getDateCreated() {
   	  return(dateCreated);
   }
   
   public void setDateCreated(String dateCreated){
   	  this.dateCreated = dateCreated;
   }   
            
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }
   
   public String getDepartment() {
		
		return department;
		
	}
	
	public void setDepartment(String department) {
		
		this.department = department;
		
	}                         
	
	public ArrayList getDepartmentList() {
		
		return departmentList;
		
	}
	
	public void setDepartmentList(String department) {
		
		departmentList.add(department);
		
	}
	
	public void clearDepartmentList() {
		
		departmentList.clear();
		departmentList.add(Constants.GLOBAL_BLANK);
		
	}
	
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request){
   	
      statusList.clear();
      statusList.add(Constants.GLOBAL_BLANK);
      statusList.add("OPEN");
      statusList.add("CLOSED");
      department = Constants.GLOBAL_BLANK;
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add("BATCH NAME");
	      orderByList.add("DATE CREATED");
	      
	  }
	  
	  showDetailsButton = null;
	  hideDetailsButton = null;      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
       ActionErrors errors = new ActionErrors();
      
       if (!Common.validateDateFormat(dateCreated)) {

	     errors.add("dateCreated",
	        new ActionMessage("findVoucherBatch.error.dateCreatedInvalid"));
	
	  }         
	 	       
      return(errors);
   }
}
