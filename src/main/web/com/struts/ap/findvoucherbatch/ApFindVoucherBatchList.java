package com.struts.ap.findvoucherbatch;

import java.io.Serializable;

public class ApFindVoucherBatchList implements Serializable {

   private Integer voucherBatchCode = null;
   private String batchName = null;
   private String description = null;
   private String status = null;
   private String dateCreated = null;
   private String createdBy = null;
   private String department = null;

   private String openButton = null;
    
   private ApFindVoucherBatchForm parentBean;
    
   public ApFindVoucherBatchList(ApFindVoucherBatchForm parentBean,
      Integer voucherBatchCode,
      String batchName,
      String description,
      String status,
      String dateCreated,
      String createdBy,
      String department){

      this.parentBean = parentBean;
      this.voucherBatchCode = voucherBatchCode;
      this.batchName = batchName;
      this.description = description;
      this.status = status;
      this.dateCreated = dateCreated;
      this.createdBy = createdBy;
      this.department = department;
      
   }

   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }

   public Integer getVoucherBatchCode(){
      return(voucherBatchCode);
   }

   public String getBatchName(){
      return(batchName);
   }

   public String getDescription(){
      return(description);
   }

   public String getStatus(){
      return(status);
   }
   
   public String getDateCreated(){
      return(dateCreated);
   }

   public String getCreatedBy(){
      return(createdBy);
   }
   
   public String getDepartment(){
      return(department);
   }

   
}
