package com.struts.ap.generatepurchaserequisition;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApGeneratePurchaseRequisitionController;
import com.ejb.txn.ApGeneratePurchaseRequisitionControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModBranchItemLocationDetails;
import com.util.ApModPurchaseRequisitionDetails;
import com.util.ApModPurchaseRequisitionLineDetails;

public final class ApGeneratePurchaseRequisitionAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form, HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
 Check if user has a session
 *******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApGeneratePurchaseRequisitionAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApGeneratePurchaseRequisitionForm actionForm = (ApGeneratePurchaseRequisitionForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.AP_GENERATE_PURCHASE_REQUISITION_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("apGeneratePurchaseRequisition");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
 Initialize ApGeneratePurchaseRequisitionController EJB
 *******************************************************/
			
	         ApGeneratePurchaseRequisitionControllerHome homeGPR = null;
	         ApGeneratePurchaseRequisitionController ejbGPR = null;

	         try {
	          
	         	homeGPR = (ApGeneratePurchaseRequisitionControllerHome)com.util.EJBHomeFactory.
	                lookUpHome("ejb/ApGeneratePurchaseRequisitionControllerEJB", ApGeneratePurchaseRequisitionControllerHome.class);

	         } catch (NamingException e) {
	         	
	         	e.printStackTrace();

	            if (log.isInfoEnabled()) {

	                log.info("NamingException caught in ApGeneratePurchaseRequisitionAction.execute(): " + e.getMessage() +
	               " session: " + session.getId());
	            }

	            return mapping.findForward("cmnErrorPage");

	         }

	         try {

	            ejbGPR = homeGPR.create();
	            
	         } catch (CreateException e) {

	            if (log.isInfoEnabled()) {

	                log.info("CreateException caught in ApGeneratePurchaseRequisitionAction.execute(): " + e.getMessage() +
	               " session: " + session.getId());

	            }
	            return mapping.findForward("cmnErrorPage");

	         }
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
 Call ApGeneratePurchaseRequisitionController EJB
 *******************************************************/
			
			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
			
			short quantityPrecision = 0; 
			
			try {
				
				quantityPrecision = ejbGPR.getInvGpQuantityPrecisionUnit(user.getCmpCode());
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in ApGeneratePurchaseRequisitionAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
/*******************************************************
 -- Ap GPR Previous Action --
 *******************************************************/ 
			
			if(request.getParameter("previousButton") != null) {
				
				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
	         	
	         	// check if prev should be disabled
	         	if (actionForm.getLineCount() == 0) {
	         		
	         		actionForm.setDisablePreviousButton(true);
	         		
	         	} else {
	         		
	         		actionForm.setDisablePreviousButton(false);
	         		
	         	}
	         	
	         	// check if next should be disabled
	         	if (actionForm.getApGPRListSize()  <= actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES) {
	         		
	         		actionForm.setDisableNextButton(true);
	         		
	         	} else {
	         		
	         		actionForm.setDisableNextButton(false);
	         		
	         	}
	         	
	         	return (mapping.findForward("apGeneratePurchaseRequisition"));      
				
/*******************************************************
 -- Ap GPR Next Action --
 *******************************************************/ 
				
			} else if(request.getParameter("nextButton") != null) {
				
				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
	         	
	         	// check if prev should be disabled
	         	if (actionForm.getLineCount() == 0) {
	         		
	         		actionForm.setDisablePreviousButton(true);
	         		
	         	} else {
	         		
	         		actionForm.setDisablePreviousButton(false);
	         		
	         	}
	         	
	         	// check if next should be disabled
	         	if (actionForm.getApGPRListSize() <= actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES) {
	         		
	         		actionForm.setDisableNextButton(true);
	         		
	         	} else {
	         		
	         		actionForm.setDisableNextButton(false);
	         		
	         	}
	         	
	         	return (mapping.findForward("apGeneratePurchaseRequisition"));  
				
/*******************************************************
   -- Ap GPR Go Action --
*******************************************************/

	         } if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
	            request.getParameter("previousButton") != null) {
	            
	             // create criteria 
	            
	            if (request.getParameter("goButton") != null) {
	            	
		        	HashMap criteria = new HashMap();            	
	                
		        	if (!Common.validateRequired(actionForm.getItemName())) {
		        		
		        		criteria.put("itemName", actionForm.getItemName());
		        		
		        	}
		        		        	
		        	if (!Common.validateRequired(actionForm.getItemClass())) {
		        		
		        		criteria.put("itemClass", actionForm.getItemClass());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getItemCategory())) {
		        		
		        		criteria.put("itemCategory", actionForm.getItemCategory());
		        		
		        	}	
		        			        	
		        	// save criteria
		        	
		        	actionForm.setLineCount(0);
		        	actionForm.setCriteria(criteria);
		        	
		     	} 
	            
	            try {
	            	
	            	actionForm.clearApGPRList();
	            	
	            	ArrayList branchNameSelectedList = new ArrayList();
		            
					for (int i=0; i < actionForm.getBranchNameSelectedList().length; i++) {
						
						branchNameSelectedList.add(actionForm.getBranchNameSelectedList()[i]);
						
					}
	            	
	            	ArrayList list = ejbGPR.getApGenPRLinesByCriteria(actionForm.getCriteria(),
	            			actionForm.getOrderBy(),
	            			branchNameSelectedList, user.getCmpCode());
	            	
	            	Iterator i = list.iterator();
	            	
	            	while (i.hasNext()) {
	            		
	            		AdModBranchItemLocationDetails mdetails = (AdModBranchItemLocationDetails)i.next();
	            		
	            		ApGeneratePurchaseRequisitionList gprList = new ApGeneratePurchaseRequisitionList(
	            				actionForm, mdetails.getBilIlIiName(), mdetails.getBilIlIiDescription(), mdetails.getBilBrName(), mdetails.getBilIlLocName(),
	            				Common.convertDoubleToStringMoney(mdetails.getBilReorderPoint(), quantityPrecision),
	            				Common.convertDoubleToStringMoney(mdetails.getBilReorderQuantity(), quantityPrecision),
	            				Common.convertDoubleToStringMoney(mdetails.getBilQuantityOnHand(), quantityPrecision),
	            				Common.convertDoubleToStringMoney(mdetails.getBilQuantity(), quantityPrecision)
	            				);
	            		
	            		actionForm.saveApGPRList(gprList);
	            		
	            	}
	            	
	            	// check if prev should be disabled
	            	if (actionForm.getLineCount() == 0) {
	            		
	            		actionForm.setDisablePreviousButton(true);
	            		
	            	} else {
	            		
	            		actionForm.setDisablePreviousButton(false);
	            		
	            	}
	            	
	            	// check if next should be disabled
	            	if (actionForm.getApGPRListSize() <= actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES) {
	            		
	            		actionForm.setDisableNextButton(true);
	            		
	            	} else {
	            		
	            		actionForm.setDisableNextButton(false);
	            		
	            	}
	            	
	            } catch (GlobalNoRecordFoundException ex) {
	            	
	               // disable prev next buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisablePreviousButton(true);
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  new ActionMessage("generatePurchaseRequisition.error.noRecordFound"));

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApGeneratePurchaseRequisitionAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }

	            }
	            
	            if (!errors.isEmpty()) {

	               saveErrors(request, new ActionMessages(errors));

	            }
	                        
	            return(mapping.findForward("apGeneratePurchaseRequisition")); 

/*******************************************************
 -- Ap GPR Close Action --
 *******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
 -- Ap GPR Generate Action --
 *******************************************************/
				
	         } else if (request.getParameter("generateButton") != null  &&
	         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	         	
	         	// generate purchase requisition
	         	
	         	ApModPurchaseRequisitionDetails mdetails = new ApModPurchaseRequisitionDetails();
	         	
	         	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
	         	mdetails.setPrDescription("GENERATED PR " + formatter.format(new java.util.Date()));
	         	mdetails.setPrDate(Common.getGcCurrentDateWoTime().getTime());
	         	mdetails.setPrConversionRate(1.000000);
	         	mdetails.setPrCreatedBy(user.getUserName());
	         	mdetails.setPrDateCreated(new Date());
	         	
	         	ArrayList prlList = new ArrayList();
	         	HashMap prlMap = new HashMap();
	         	
	         	short PRL_LN = 1;
	         	
	         	for(int i=0; i<actionForm.getApGPRListSize(); i++) {
	         		
	         		ApGeneratePurchaseRequisitionList actionList = actionForm.getApGPRByIndex(i);
	         		
	         		if (actionList.getGenerate()) {
	         			
	         			ApModPurchaseRequisitionLineDetails prlDetails = (ApModPurchaseRequisitionLineDetails)prlMap.get(
	         					actionList.getItemName() + "-" + actionList.getLocation());
	         			
	         			if (prlDetails == null) {
	         			
		         			prlDetails = new ApModPurchaseRequisitionLineDetails();
		         			
		         			prlDetails.setPrlLine(PRL_LN);
		         			prlDetails.setPrlQuantity(Common.convertStringMoneyToDouble(actionList.getQuantity(), (short)quantityPrecision));
		         			prlDetails.setPrlIlIiName(actionList.getItemName());
		         			prlDetails.setPrlIlLocName(actionList.getLocation());
							
		         			prlList.add(prlDetails);
		         			prlMap.put(actionList.getItemName() + "-" + actionList.getLocation(), prlDetails);
		         			
							PRL_LN++;
	         			} else {
	         				prlDetails.setPrlQuantity(prlDetails.getPrlQuantity() + Common.convertStringMoneyToDouble(actionList.getQuantity(), (short)quantityPrecision));
	         			}
						
	         		}
	         		
	         	}	
	         		         	
	         	//generate purchase requisition
	         	Integer prCode = null;
	         	
	         	try {
	         		
	         		prCode = ejbGPR.generateApPurchaseRequisition(mdetails, prlList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	         		
	         	} catch (EJBException ex) {
	         		if (log.isInfoEnabled()) {
	         			
	         			log.info("EJBException caught in ApGeneratePurchaseRequisitionAction.execute(): " + ex.getMessage() +
	         					" session: " + session.getId());
	         		}
	         		
	         		return(mapping.findForward("cmnErrorPage"));
	         	}
	         	
	         	//forward to purchase requisition entry screen	         	
	         	String path = "/apPurchaseRequisitionEntry.do?forward=1" +
 				"&purchaseRequisitionCode=" + String.valueOf(prCode.intValue());
	         	
	         	return(new ActionForward(path));
	         	
/*******************************************************
 -- Ap GPR Load Action --
 *******************************************************/
	         	
	         }
			
			if (frParam != null) {
				
				actionForm.clearApGPRList();
				ArrayList list = null;
				Iterator i = null;
				
	            try {
	            	    
	            	actionForm.clearItemCategoryList();
	            	
	            	list = ejbGPR.getAdLvInvItemCategoryAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setItemCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setItemCategoryList((String)i.next());
	            			
	            		}
	            		
	            	}     
	            	
	            	actionForm.clearBranchNameList();
	            	
	            	list = ejbGPR.getAdBrAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setBranchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		            		
	            	} else {
	            		
	            		i = list.iterator();
	            		
	            		while(i.hasNext()) {
	            			
	            			actionForm.setBranchNameList((String)i.next());
	            			
	            		}
	            	}
	            	           	
	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApGeneratePurchaseRequisitionAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }

	            }             
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} 
				
				actionForm.reset(mapping, request);
				actionForm.setLineCount(0);
				actionForm.setDisableNextButton(true);
				actionForm.setDisablePreviousButton(true);
				
				return(mapping.findForward("apGeneratePurchaseRequisition"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in ApGeneratePurchaseRequisitionAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			e.printStackTrace();
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
}