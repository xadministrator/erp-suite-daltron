package com.struts.ap.generatepurchaserequisition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Constants;

public class ApGeneratePurchaseRequisitionForm extends ActionForm implements Serializable {
	
	private String itemName = null;
	private String itemClass = null;
	private ArrayList itemClassList = new ArrayList();
	private String itemCategory = null;
	private ArrayList itemCategoryList = new ArrayList();
	private String[] branchNameSelectedList = new String[0];
	private ArrayList branchNameList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	
	private int lineCount = 0;
	   
	private HashMap criteria = new HashMap();
	private ArrayList apGPRList = new ArrayList();
	
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemClass() {
		
		return itemClass;
		
	}
	
	public void setItemClass(String itemClass) {
		
		this.itemClass = itemClass;
		
	}
	
	public ArrayList getItemClassList() {
	
		return itemClassList;
		
	}
	
	public void setItemClassList(String itemClass) {
		
		this.itemClassList.add(itemClass);
		
	}
	
	public void clearItemClassList() {
		
		itemClassList.clear();
		
	}
	
	public String getItemCategory() {
		
		return itemCategory;
		
	}
	
	public void setItemCategory(String itemCategory) {
		
		this.itemCategory = itemCategory;
		
	}
	
	public ArrayList getItemCategoryList() {
		
		return itemCategoryList;
		
	}
	
	public void setItemCategoryList(String itemCategory) {
		
		itemCategoryList.add(itemCategory);
		
	}
	
	public void clearItemCategoryList() {
		
		itemCategoryList.clear();
		itemCategoryList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public ArrayList getBranchNameList() {
		
		return branchNameList;
		
	}
	
	public void setBranchNameList(String branchName) {
	
		branchNameList.add(branchName);
		
	}
	
	public void clearBranchNameList() {
	
		branchNameList.clear();
		
	}
	
	public String[] getBranchNameSelectedList() {
		
		return branchNameSelectedList;
		
	}
	
	public void setBranchNameSelectedList(String[] branchNameSelectedList) {
		
		this.branchNameSelectedList = branchNameSelectedList;
		
	}
	
	public String getOrderBy() {
		
		return orderBy;
		
	}
	
	public void setOrderBy(String orderBy) {
		
		this.orderBy = orderBy;
		
	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public int getLineCount() {
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount) {
		
		this.lineCount = lineCount;
		
	}
	
	public HashMap getCriteria() {
		
		return criteria;
		
	}
	
	public void setCriteria(HashMap criteria) {
		
		this.criteria = criteria;
		
	}
	
	public Object[] getApGPRList(){

		return apGPRList.toArray();
		
	}
	
	public ApGeneratePurchaseRequisitionList getApGPRByIndex(int index) {
		
		return ((ApGeneratePurchaseRequisitionList)apGPRList.get(index));
		
	}
	
	public int getApGPRListSize(){
		
		return(apGPRList.size());
		
	}
	
	public void saveApGPRList(Object newApGPRList){
		
		apGPRList.add(newApGPRList);   	  
		
	}
	
	public void clearApGPRList(){
		
		apGPRList.clear();
		
	}
	
	public void setApGPRList(ArrayList apGPRList) {
		
		this.apGPRList = apGPRList;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		itemName = null;
		itemClass = null;
		itemCategory = null;
		orderBy = Constants.AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_NAME;
		
		itemClassList.clear();
		itemClassList.add(Constants.GLOBAL_BLANK);
		itemClassList.add(Constants.AP_GENERATE_PURCHASE_REQUISITION_ITEM_CLASS_STOCK);
		itemClassList.add(Constants.AP_GENERATE_PURCHASE_REQUISITION_ITEM_CLASS_ASSEMBLY);
		
		branchNameSelectedList = new String[0];
		
		orderByList.clear();
		orderByList.add(Constants.GLOBAL_BLANK);
		orderByList.add(Constants.AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_NAME);
		orderByList.add(Constants.AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_DESCRIPTION);
		orderByList.add(Constants.AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_CATEGORY);
		orderByList.add(Constants.AP_GENERATE_PURCHASE_REQUISITION_ORDER_BY_ITEM_CLASS);
				
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("generateButton") != null) {
			
			boolean checked = false;
			
			for(int i=0; i<apGPRList.size(); i++) {
				
				if(((ApGeneratePurchaseRequisitionList)apGPRList.get(i)).getGenerate() == true) {
					
					checked = true;
					break;
					
				}
				
			}
			
			if(checked == false) {
				
				errors.add("apGPRList",
			               new ActionMessage("generatePurchaseRequisition.error.prGenerationMustHaveAtLeastOneLine"));
				
			}

	    }
		
		return errors;
		
	}
	
}