package com.struts.ap.generatepurchaserequisition;

import java.io.Serializable;

public class ApGeneratePurchaseRequisitionList implements Serializable {
	
	public String itemName = null;
	public String itemDescription = null;
	public String branch = null;
	public String location = null;
	public String reorderPoint = null;
	public String reorderQuantity = null;
	public String quantityOnHand = null;
	public String quantity = null;
	public boolean generate = false;
	
	private ApGeneratePurchaseRequisitionForm parentBean;
	
	public ApGeneratePurchaseRequisitionList (ApGeneratePurchaseRequisitionForm parentBean, String itemName, 
			String itemDescription, String branch, String location, String reorderPoint, String reorderQuantity, String quantityOnHand, String quantity) {
		
		this.parentBean = parentBean;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.branch = branch;
		this.location = location;
		this.reorderPoint = reorderPoint;
		this.reorderQuantity = reorderQuantity;
		this.quantityOnHand = quantityOnHand;
		this.quantity = quantity;

	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getBranch() {
		
		return branch;
		
	}
	
	public void setBranch(String branch) {
		
		this.branch = branch;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public String getReorderPoint() {
		
		return reorderPoint;
		
	}
	
	public void setReorderPoint(String reorderPoint) {
		
		this.reorderPoint = reorderPoint;
		
	}
	
	public String getReorderQuantity() {
		
		return reorderQuantity;
		
	}
	
	public void setReorderQuantity(String reorderQuantity) {
		
		this.reorderQuantity = reorderQuantity;
		
	}
	
	public String getQuantityOnHand() {
		
		return quantityOnHand;
		
	}
	
	public void setQuantityOnHand(String quantityOnHand) {
		
		this.quantityOnHand = quantityOnHand;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	
	
	public boolean getGenerate() {
		
		return generate;
		
	}
	
	public void setGenerate(boolean generate) {
		
		this.generate = generate;
		
	}
	
}