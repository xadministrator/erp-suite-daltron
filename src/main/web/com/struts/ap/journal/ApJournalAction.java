package com.struts.ap.journal;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ApJournalController;
import com.ejb.txn.ApJournalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModDistributionRecordDetails;

public final class ApJournalAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApJournalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApJournalForm actionForm = (ApJournalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_JOURNAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apJournal");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApJournalController EJB
*******************************************************/

         ApJournalControllerHome homeJR = null;
         ApJournalController ejbJR = null;

         try {

            homeJR = (ApJournalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApJournalControllerEJB", ApJournalControllerHome.class);      
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbJR = homeJR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call ApJournalController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short voucherLineNumber = 0;
         
         try {
         	
            precisionUnit = ejbJR.getGlFcPrecisionUnit(user.getCmpCode());
            //precisionUnit = (short)(10);
            voucherLineNumber = ejbJR.getAdPrfApJournalLineNumber(user.getCmpCode());
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }	
      

/*******************************************************
   -- Ap JR Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
         	ArrayList drList = new ArrayList();
            int lineNumber = 0;  
            
            double TOTAL_DEBIT = 0;
            double TOTAL_CREDIT = 0;         
                       
            for (int i = 0; i < actionForm.getApJRListSize(); i++) {
            	
        	   ApJournalList apJRList = actionForm.getApJRByIndex(i);           	   
        	              	   
        	   if (Common.validateRequired(apJRList.getAccountNumber()) &&
        	   		Common.validateRequired(apJRList.getDebitAmount()) &&
        	   		Common.validateRequired(apJRList.getCreditAmount())) continue;
        	   
        	   byte isDebit = 0;
	           double amount = 0d;

		       if (!Common.validateRequired(apJRList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apJRList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apJRList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
	       
		       lineNumber++;

        	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
        	   
        	   mdetails.setDrLine((short)(lineNumber));
        	   mdetails.setDrClass(apJRList.getDrClass());
        	   mdetails.setDrDebit(isDebit);
        	   mdetails.setDrAmount(amount);
        	   mdetails.setDrCoaAccountNumber(apJRList.getAccountNumber());
        	   
        	   drList.add(mdetails);
            	
            }
            
            TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
            TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
            
            if (TOTAL_DEBIT != TOTAL_CREDIT) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("apJournal.error.journalNotBalance"));
                     
                saveErrors(request, new ActionMessages(errors));
                return (mapping.findForward("apJournal"));
            	
            }
            
            Integer primaryKey = null;
            
            if (actionForm.getPaymentCode() != null) {
            	
            	primaryKey = actionForm.getPaymentCode();
            	            	
            } else if (actionForm.getVoucherCode() != null) {
            	
            	primaryKey = actionForm.getVoucherCode();
            }
            
            try {
            	
            	ejbJR.saveApDrEntry(primaryKey, drList, actionForm.getTransaction(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            } catch (GlobalBranchAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("apJournal.error.branchAccountNumberInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

/*******************************************************
	-- Ap JR Add Lines Action --
*******************************************************/
	
	      } else if(request.getParameter("addLinesButton") != null) {
	      	
	      	int listSize = actionForm.getApJRListSize();
	                                
	         for (int x = listSize + 1; x <= listSize + voucherLineNumber; x++) {
	        	
	        	ApJournalList apJRList = new ApJournalList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);
	        	    
	        	apJRList.setDrClassList(this.getDrClassList());
	        	
	        	actionForm.saveApJRList(apJRList);
	        	
	        }	        
	        
	        return(mapping.findForward("apJournal"));
	
/*******************************************************
	 -- Ap JR Delete Lines Action --
*******************************************************/
	
	      } else if(request.getParameter("deleteLinesButton") != null) {
	      	
	      	for (int i = 0; i<actionForm.getApJRListSize(); i++) {
	        	
	        	   ApJournalList apJRList = actionForm.getApJRByIndex(i);
	        	   
	        	   if (apJRList.getDeleteCheckbox()) {
	        	   	
	        	   	   actionForm.deleteApJRList(i);
	        	   	   i--;
	        	   }
	        	   
	         }
	         
	         for (int i = 0; i<actionForm.getApJRListSize(); i++) {
	        	
	         	ApJournalList apJRList = actionForm.getApJRByIndex(i);
	        	   
	        	   apJRList.setLineNumber(String.valueOf(i+1));
	        	   
	         }
	        
	        return(mapping.findForward("apJournal"));	 
          
            
/*******************************************************
   -- Ap JR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {
            
            return(mapping.findForward("cmnMain"));
          
/*******************************************************
   -- Ap JR Back Action --
*******************************************************/

         } else if (request.getParameter("backButton") != null) {

            String path = null;
            
            if (actionForm.getTransaction().equals("PAYMENT")) {
            	
            	path = "/apPaymentEntry.do?forward=1&checkCode=" +
            	    actionForm.getPaymentCode();
            	    
            } else if (actionForm.getTransaction().equals("DIRECT CHECK")) {
            	
            	path = "/apDirectCheckEntry.do?forward=1&checkCode=" +
        	        actionForm.getPaymentCode();
            	            	    
            } else if (actionForm.getTransaction().equals("VOUCHER")) {
            	
            	path = "/apVoucherEntry.do?forward=1&voucherCode=" +
        	        actionForm.getVoucherCode();
        	        
        	} else if (actionForm.getTransaction().equals("DEBIT MEMO")) {
            	
            	path = "/apDebitMemoEntry.do?forward=1&debitMemoCode=" +
        	        actionForm.getVoucherCode();
            	
        	} else if (actionForm.getTransaction().equals("RECEIVING")) {
            	
            	path = "/apReceivingItemEntry.do?forward=1&receivingItemCode=" +
        	        actionForm.getVoucherCode();
            	
            }
            
            return(new ActionForward(path));
            

            
/*******************************************************
   -- Ap JR Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apJournal");

            }
            
            try {
            	
        		ArrayList list = new ArrayList();
        		Iterator i = null;
        		
        		actionForm.clearApJRList();

        		if ((request.getParameter("forward") != null && (request.getParameter("transaction").equals("PAYMENT") || request.getParameter("transaction").equals("DIRECT CHECK"))) ||
        		    actionForm.getTransaction().equals("PAYMENT") || actionForm.getTransaction().equals("DIRECT CHECK")){
        			
        			if (request.getParameter("transactionCode") != null) 
        			   actionForm.setPaymentCode(new Integer(request.getParameter("transactionCode")));
    				   actionForm.setVoucherCode(null);
        			
        			list = ejbJR.getApDrByChkCode(actionForm.getPaymentCode(), user.getCmpCode());
        			
	            			        			            			
        		} else if ((request.getParameter("forward") != null && (request.getParameter("transaction").equals("VOUCHER") || request.getParameter("transaction").equals("DEBIT MEMO"))) ||
            		actionForm.getTransaction().equals("VOUCHER") || actionForm.getTransaction().equals("DEBIT MEMO")) {
        			
        			if (request.getParameter("transactionCode") != null) 
        				actionForm.setVoucherCode(new Integer(request.getParameter("transactionCode")));
        				actionForm.setPaymentCode(null);

        			list = ejbJR.getApDrByVouCode(actionForm.getVoucherCode(), user.getCmpCode());
        			
	            			        			            			
        		} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("RECEIVING")) || actionForm.getTransaction().equals("RECEIVING")) {

            			if (request.getParameter("transactionCode") != null) 
            				actionForm.setVoucherCode(new Integer(request.getParameter("transactionCode")));
            				actionForm.setPaymentCode(null);

            			list = ejbJR.getApDrByPoCode(actionForm.getVoucherCode(), user.getCmpCode());
            			
    	            			        			            			
            	}
        		
        		if (request.getParameter("forward") != null) {

	      			actionForm.setNumber(request.getParameter("transactionNumber"));
	       			actionForm.setDate(request.getParameter("transactionDate"));
	       			actionForm.setEnableFields(request.getParameter("transactionEnableFields"));
	       			
	       		}

        		double totalDebit = 0;
        		double totalCredit = 0;
        		
        		i = list.iterator();        		       		
        			
    			while (i.hasNext()) {
    				
    				ApModDistributionRecordDetails mdetails = (ApModDistributionRecordDetails)i.next();
    				
    				String debitAmount = null;
    				String creditAmount = null;
    				
    				if (mdetails.getDrDebit() == 1) {
    					
    					debitAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalDebit = totalDebit + mdetails.getDrAmount();
    					
    				} else {
    					
    					creditAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalCredit = totalCredit + mdetails.getDrAmount();
    					
    				}
    				
    				ArrayList classList = new ArrayList();
    				classList.add(mdetails.getDrClass());
    				
    				ApJournalList apJRList = new ApJournalList(actionForm,
    						mdetails.getDrCode(), 
        				    Common.convertShortToString(mdetails.getDrLine()),
        				    mdetails.getDrCoaAccountNumber(),
        				    debitAmount,
        				    creditAmount,
        	    		    mdetails.getDrClass(),
        				    mdetails.getDrCoaAccountDescription());
    				
    				apJRList.setDrClassList(classList);
    				
    				actionForm.saveApJRList(apJRList);
    				        				
    			}
    			
    			actionForm.setTotalDebit(Common.convertDoubleToStringMoney(totalDebit, precisionUnit));
    			actionForm.setTotalCredit(Common.convertDoubleToStringMoney(totalCredit, precisionUnit));
    			
    			int remainingList = voucherLineNumber - (list.size() % voucherLineNumber) + list.size();
		        
		        for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	ApJournalList apJRList = new ApJournalList(actionForm,
			        	    null, String.valueOf(x), null, null, null, null, null);
			        	    
		        	apJRList.setDrClassList(this.getDrClassList());
			        	
			        actionForm.saveApJRList(apJRList);
			        			        	
		        }
            	
    			this.setFormProperties(actionForm);    	
    			
            } catch (GlobalNoRecordFoundException ex) {
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {//from update to saveButton

               if ((request.getParameter("saveButton") != null) && 
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
            return(mapping.findForward("apJournal"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApJournalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          
          e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

       }

    }
   
   private void setFormProperties(ApJournalForm actionForm) {
	
	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
		
		if (actionForm.getEnableFields().equals("true")) {
		
    		actionForm.setShowSaveButton(true);
			actionForm.setShowAddLinesButton(true);
			actionForm.setShowDeleteLinesButton(true);
		
		} else {
			
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			
		}
		
	} else {
		
		actionForm.setShowSaveButton(false);
		actionForm.setShowAddLinesButton(false);
		actionForm.setShowDeleteLinesButton(false);
		    		
	}

}
   
   private ArrayList getDrClassList() {
	
	ArrayList classList = new ArrayList();
	
 	classList.add("OTHER");
 	
 	return classList;
	
}
}