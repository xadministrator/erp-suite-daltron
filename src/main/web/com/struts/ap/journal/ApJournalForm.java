package com.struts.ap.journal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApJournalForm extends ActionForm implements Serializable {

   private Integer paymentCode = null;
   private Integer voucherCode = null;
   private String transaction = null;
   private String date = null;
   private String number = null;
   private String totalDebit = null;
   private String totalCredit = null;
   private String enableFields = null;
   private String backButton = null;
   private String closeButton = null;

   private ArrayList apJRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showSaveButton = false;

   public int getRowSelected() {

      return rowSelected;

   }

   public ApJournalList getApJRByIndex(int index) {

      return((ApJournalList)apJRList.get(index));

   }

   public Object[] getApJRList() {

      return apJRList.toArray();

   }

   public int getApJRListSize() {

      return apJRList.size();

   }

   public void saveApJRList(Object newApJRList) {

      apJRList.add(newApJRList);

   }

   public void clearApJRList() {
      apJRList.clear();
   }


   public void updateApJRRow(int rowSelected, Object newApJRList) {

      apJRList.set(rowSelected, newApJRList);

   }

   public void deleteApJRList(int rowSelected) {

      apJRList.remove(rowSelected);

   }


   public void setBackButton(String backButton) {

      this.backButton = backButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public Integer getPaymentCode() {

      return paymentCode;

   }

   public void setPaymentCode(Integer paymentCode) {

      this.paymentCode = paymentCode;

   }
   
   public Integer getVoucherCode() {
	
	  return voucherCode;
	
   }
   public void setVoucherCode(Integer voucherCode) {
	
	  this.voucherCode = voucherCode;
	
   }

   public String getTransaction() {

      return transaction;

   }

   public void setTransaction(String transaction) {

      this.transaction = transaction;

   }

   public String getDate() {

      return date;

   }

   public void setDate(String date) {

      this.date = date;

   }

   public String getNumber() {

      return number;

   }

   public void setNumber(String number) {

      this.number = number;

   }
   
   public String getTotalDebit() {
   	
   	  return totalDebit;
   	  
   }
   
   public void setTotalDebit(String totalDebit) {
   	
   	  this.totalDebit = totalDebit;
   	
   }
   
   public String getTotalCredit() {
   	
   	  return totalCredit;
   	  
   }
   
   public void setTotalCredit(String totalCredit) {
   	
   	  this.totalCredit = totalCredit;
   	
   }
   
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
   
   public String getEnableFields() {
   	
   	  return enableFields;
   	  
   }
   
   public void setEnableFields(String enableFields) {
   	
   	  this.enableFields = enableFields;
   	  
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      backButton = null;
      closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
   	
    ActionErrors errors = new ActionErrors();
    if (request.getParameter("saveButton") != null) {
    	 
    	int numberOfLines = 0;
   	 
   	 Iterator i = apJRList.iterator();      	 
   	 
   	 while (i.hasNext()) {
   	 	
   	 	 ApJournalList drList = (ApJournalList)i.next();      	 	 
   	 	       	 	 
   	 	 if (Common.validateRequired(drList.getAccountNumber()) &&
   	 	     Common.validateRequired(drList.getDebitAmount()) &&
   	 	     Common.validateRequired(drList.getCreditAmount())) continue;
   	 	     
   	 	 numberOfLines++;
   	 
	         if(Common.validateRequired(drList.getAccountNumber())){
	            errors.add("accountNumber",
	               new ActionMessage("apJournal.error.accountNumberRequired", drList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(drList.getDebitAmount())){
	            errors.add("debitAmount",
	               new ActionMessage("apJournal.error.debitAmountInvalid", drList.getLineNumber()));
	         }
	         if(!Common.validateMoneyFormat(drList.getCreditAmount())){
	            errors.add("creditAmount",
	               new ActionMessage("apJournal.error.creditAmountInvalid", drList.getLineNumber()));
	         }
			 if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("apJournal.error.debitCreditAmountRequired", drList.getLineNumber()));
			 }
			 if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("apJournal.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
		     }	    		     
		     
		          
	    }
	    
	    if (numberOfLines == 0) {
      	
      	errors.add("transaction",
            new ActionMessage("apJournal.error.journalMustHaveLine"));
      	
      }
    	
    }
    
    return errors;

   }
}