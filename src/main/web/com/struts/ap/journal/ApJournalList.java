package com.struts.ap.journal;

import java.io.Serializable;
import java.util.ArrayList;

public class ApJournalList implements Serializable {

   private Integer distributionRecordCode = null;
   private String lineNumber = null;
   private String accountNumber = null;
   private String description = null;
   private String drClass = null;
   private ArrayList drClassList = new ArrayList();
   private boolean reversal = false;
   private String debitAmount = null;
   private String creditAmount = null;
   private boolean deleteCheckbox = false;

   private ApJournalForm parentBean;
    
   public ApJournalList(ApJournalForm parentBean,
      Integer distributionRecordCode,
      String lineNumber,
      String accountNumber,
      String debitAmount,
      String creditAmount,
      String drClass,
      String description) {

      this.parentBean = parentBean;
      this.distributionRecordCode = distributionRecordCode;
      this.lineNumber = lineNumber;
      this.accountNumber = accountNumber;
      this.description = description;
      this.drClass = drClass;
      this.debitAmount = debitAmount;
      this.creditAmount = creditAmount;
   }

   public Integer getDistributionRecordCode() {

      return distributionRecordCode;

   }

   public String getLineNumber() {

      return lineNumber;

   }
   
   public void setLineNumber(String lineNumber) {
 	
	  this.lineNumber = lineNumber;
	  
   }

   public String getAccountNumber() {

      return accountNumber;

   }
   
   public void setAccountNumber(String accountNumber) {
   	
 	  this.accountNumber = accountNumber;
 	  
   }

   public String getDescription() {

      return description;

   }
   
   public void setAccountDescription(String description) {
   	
 	  this.description = description;
 	  
   }

   public String getDrClass() {

      return drClass;

   }
   
   public void setDrClass(String drClass) {
   	
   	  this.drClass = drClass;
   	
   }
   
   public String getDebitAmount() {

      return debitAmount;

   }
   
   public void setDebitAmount(String debitAmount) {
   	
 	  this.debitAmount = debitAmount;
 	  
 }
   
   public String getCreditAmount() {

      return creditAmount;

   }
   
   public void setCreditAmount(String creditAmount) {
   	
 	  this.creditAmount = creditAmount;
 	  
   }
   public ArrayList getDrClassList() {
 	  
 	  return drClassList;
 	  
 }
 
 public void setDrClassList(ArrayList drClassList) {
 	
 	  this.drClassList = drClassList;
 	
 }
   public boolean getDeleteCheckbox() {  
 	
 	  return deleteCheckbox;
 	  
   }
 
   public void setDeleteCheckbox(boolean deleteCheckbox) {
 	
 	  this.deleteCheckbox = deleteCheckbox;
 	  
   }

}