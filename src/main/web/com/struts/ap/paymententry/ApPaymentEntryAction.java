package com.struts.ap.paymententry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.ApCHKVoucherHasNoWTaxCodeException;
import com.ejb.exception.ApVOUOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApPaymentEntryController;
import com.ejb.txn.ApPaymentEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ApCheckDetails;
import com.util.ApModAppliedVoucherDetails;
import com.util.ApModCheckDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModVoucherPaymentScheduleDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;

public final class ApPaymentEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession(); 

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApPaymentEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
          ApPaymentEntryForm actionForm = (ApPaymentEntryForm)form;

	      // reset report
	      
	      actionForm.setReport(null);
	      actionForm.setReport2(null);
	      actionForm.setAttachment(null);

         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
         	frParam = Common.getUserPermission(user, Constants.AP_PAYMENT_ENTRY_ID);
         
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apPaymentEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ApPaymentEntryController EJB
*******************************************************/

         ApPaymentEntryControllerHome homeCHK = null;
         ApPaymentEntryController ejbCHK = null;
         
         GlDailyRateControllerHome homeFR = null;
         GlDailyRateController ejbFR = null;


         try {
            
            homeCHK = (ApPaymentEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApPaymentEntryControllerEJB", ApPaymentEntryControllerHome.class);
            
            homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApPaymentEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbCHK = homeCHK.create();
            
            ejbFR = homeFR.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ApPaymentEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call ApPaymentEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/
         
         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         
         short precisionUnit = 0;
         boolean enableCheckBatch = false;
         boolean isInitialPrinting = false;
         boolean useSupplierPulldown = true;
         String defaultCheckDate = null;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/payment/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
         
         try {
         	
            precisionUnit = ejbCHK.getGlFcPrecisionUnit(user.getCmpCode());
            enableCheckBatch = Common.convertByteToBoolean(ejbCHK.getAdPrfEnableApCheckBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableCheckBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbCHK.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            defaultCheckDate = ejbCHK.getAdPrfApDefaultCheckDate(user.getCmpCode());
            actionForm.setDefaultCheckDate(defaultCheckDate);
            actionForm.setPrecisionUnit(precisionUnit);
            
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
         
/*******************************************************
   -- Ap CHK Previous Action --
*******************************************************/ 
         
         if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - actionForm.getMaxRows());
         	
         	// check if prev should be disabled
         	if (actionForm.getLineCount() == 0) {
         		
         		actionForm.setDisablePreviousButton(true);
         		actionForm.setDisableFirstButton(true);
         		
         	} else {
         		
         		actionForm.setDisablePreviousButton(false);
         		actionForm.setDisableFirstButton(false);
         		
         	}
         	
         	// check if next should be disabled
         	if (actionForm.getApCHKListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {
         		
         		actionForm.setDisableNextButton(true);
         		actionForm.setDisableLastButton(true);
         		
         	} else {
         		
         		actionForm.setDisableNextButton(false);
         		actionForm.setDisableLastButton(false);
         		
         	}
         	
         	if (request.getParameter("child") == null) {
         		
         		return (mapping.findForward("apPaymentEntry"));         
         		
         	} else {
         		
         		return (mapping.findForward("apPaymentEntryChild"));         
         		
         	}
         	
/*******************************************************
   -- Ap CHK Next Action --
*******************************************************/ 
         	
         } else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + actionForm.getMaxRows());
         	
         	// check if prev should be disabled
         	if (actionForm.getLineCount() == 0) {
         		
         		actionForm.setDisablePreviousButton(true);
         		actionForm.setDisableFirstButton(true);
         		
         	} else {
         		
         		actionForm.setDisablePreviousButton(false);
         		actionForm.setDisableFirstButton(false);
         		
         	}
         	
         	// check if next should be disabled
         	if (actionForm.getApCHKListSize() <= actionForm.getLineCount() + actionForm.getMaxRows()) {
         		
         		actionForm.setDisableNextButton(true);
         		actionForm.setDisableLastButton(true);
         		
         	} else {
         		
         		actionForm.setDisableNextButton(false);
         		actionForm.setDisableLastButton(false);
         		
         	}
         	
         	
         	if (request.getParameter("child") == null) {
         		
         		return (mapping.findForward("apPaymentEntry"));         
         		
         	} else {
         		
         		return (mapping.findForward("apPaymentEntryChild"));         
         		
         	}

/*******************************************************
  -- Ap CHK First Action --
*******************************************************/ 

         } else if(request.getParameter("firstButton") != null){

        	 actionForm.setLineCount(0);

        	 // check if prev should be disabled
        	 if (actionForm.getLineCount() == 0) {

        		 actionForm.setDisablePreviousButton(true);
        		 actionForm.setDisableFirstButton(true);

        	 } else {

        		 actionForm.setDisablePreviousButton(false);
        		 actionForm.setDisableFirstButton(false);

        	 }

        	 // check if next should be disabled
        	 if (actionForm.getApCHKListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

        		 actionForm.setDisableNextButton(true);
        		 actionForm.setDisableLastButton(true);

        	 } else {

        		 actionForm.setDisableNextButton(false);
        		 actionForm.setDisableLastButton(false);

        	 }

        	 if (request.getParameter("child") == null) {

        		 return (mapping.findForward("apPaymentEntry"));         

        	 } else {

        		 return (mapping.findForward("apPaymentEntryChild"));         

        	 }     	

/*******************************************************
  -- Ap CHK Last Action --
*******************************************************/ 

         } else if(request.getParameter("lastButton") != null){

        	 int size = actionForm.getApCHKListSize();

        	 if((size % actionForm.getMaxRows()) != 0) {

        		 actionForm.setLineCount(size -(size % actionForm.getMaxRows()));

        	 } else {

        		 actionForm.setLineCount(size - actionForm.getMaxRows());

        	 }

        	 // check if prev should be disabled
        	 if (actionForm.getLineCount() == 0) {

        		 actionForm.setDisablePreviousButton(true);
        		 actionForm.setDisableFirstButton(true);

        	 } else {

        		 actionForm.setDisablePreviousButton(false);
        		 actionForm.setDisableFirstButton(false);

        	 }

        	 // check if next should be disabled
        	 if (actionForm.getApCHKListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

        		 actionForm.setDisableNextButton(true);
        		 actionForm.setDisableLastButton(true);

        	 } else {

        		 actionForm.setDisableNextButton(false);
        		 actionForm.setDisableLastButton(false);

        	 }

        	 if (request.getParameter("child") == null) {

        		 return (mapping.findForward("apPaymentEntry"));         

        	 } else {

        		 return (mapping.findForward("apPaymentEntryChild"));         

        	 }    
         	
         }         
         										     												 
/*******************************************************
   -- Ap CHK Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ApCheckDetails details = new ApCheckDetails();
           
           details.setChkCode(actionForm.getCheckCode());           
           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
           details.setChkNumber(actionForm.getCheckNumber());
           details.setChkDocumentNumber(actionForm.getDocumentNumber());
           details.setChkReferenceNumber(actionForm.getReferenceNumber());
           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
           details.setChkDescription(actionForm.getDescription());           
           details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
                      
           if (actionForm.getCheckCode() == null) {
           	
           	   details.setChkCreatedBy(user.getUserName());
	           details.setChkDateCreated(new java.util.Date());
	                      
           }
           
           details.setChkLastModifiedBy(user.getUserName());
           details.setChkDateLastModified(new java.util.Date());
           details.setChkMemo(actionForm.getMemo());
           details.setChkSupplierName(actionForm.getSupplierName());
           
           ArrayList vpsList = new ArrayList();
                      
           for (int i = 0; i<actionForm.getApCHKListSize(); i++) {
           	
           	   ApPaymentEntryList apCHKList = actionForm.getApCHKByIndex(i);           	   
           	              	   
           	   if (!apCHKList.getPayCheckbox()) continue;
           	   
           	   ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();
           	   
           	   mdetails.setAvVpsCode(apCHKList.getVoucherPaymentScheduleCode());
           	   mdetails.setAvApplyAmount(Common.convertStringMoneyToDouble(apCHKList.getApplyAmount(), precisionUnit));
           	   mdetails.setAvTaxWithheld(Common.convertStringMoneyToDouble(apCHKList.getTaxWithheld(), precisionUnit));
           	   mdetails.setAvDiscountAmount(Common.convertStringMoneyToDouble(apCHKList.getDiscount(), precisionUnit));
           	   mdetails.setAvAllocatedPaymentAmount(Common.convertStringMoneyToDouble(apCHKList.getAllocatedCheckAmount(), precisionUnit));
           	   
           	   vpsList.add(mdetails);
           	
           }
           
           // validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           
           if (!Common.validateRequired(filename1)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename1().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename1NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename1Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename1SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
           
           
           if (!Common.validateRequired(filename2)) {                	       	    	
    	    	
    	    	if (actionForm.getFilename2().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename2NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename2Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename2SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
 	   	   
           if (!Common.validateRequired(filename3)) {                	       	    	
    	    	
    	    	if (actionForm.getFilename3().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename3NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename3Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename3().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename3SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
           
           if (!Common.validateRequired(filename4)) {                	       	    	
    	    	
    	    	if (actionForm.getFilename4().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename4NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename4Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename4().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename4SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
           
           
                      
           try {
           	
           	    Integer checkCode = ejbCHK.saveApChkEntry(details, actionForm.getBankAccount(),
           	        actionForm.getCurrency(),
           	        actionForm.getSupplier(),
					actionForm.getBatchName(),
           	        vpsList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setCheckCode(checkCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.documentNumberNotUnique"));
           
           } catch (ApCHKCheckNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.checkNumberNotUnique"));
                               	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.conversionDateNotExist"));           	                      	
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyVoid"));
                    
           } catch (ApVOUOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.overapplicationNotAllowed", ex.getMessage()));
           	   
           } catch (ApCHKVoucherHasNoWTaxCodeException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.voucherHasNoWTaxCode", ex.getMessage()));
                    
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyLocked", ex.getMessage()));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.branchAccountNumberInvalid"));
           	           	           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
    	    		
	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apPaymentEntry"));
	    		
	       } 
	       
	       // save attachment
           
           if (!Common.validateRequired(filename1)) {     
    		                  	 
      	        if (errors.isEmpty()) {
      	        	       	        	 
      	        	InputStream is = actionForm.getFilename1().getInputStream();
      	        	
      	        	new File(attachmentPath).mkdirs();
      	        	       	        		       	        		       	        	
      	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);            	           	            	
           	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
      	        	
      	        }          	                
           	           	    	
      	   }
          
          
          if (!Common.validateRequired(filename2)) {     
	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
           
/*******************************************************
   -- Ap CHK Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ApCheckDetails details = new ApCheckDetails();
           
           details.setChkCode(actionForm.getCheckCode());           
           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
           details.setChkNumber(actionForm.getCheckNumber());
           details.setChkDocumentNumber(actionForm.getDocumentNumber());
           details.setChkReferenceNumber(actionForm.getReferenceNumber());
           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
           details.setChkDescription(actionForm.getDescription());           
           details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
                      
           if (actionForm.getCheckCode() == null) {
           	
           	   details.setChkCreatedBy(user.getUserName());
	           details.setChkDateCreated(new java.util.Date());
	                      
           }
           
           details.setChkLastModifiedBy(user.getUserName());
           details.setChkDateLastModified(new java.util.Date());
           details.setChkMemo(actionForm.getMemo());
           details.setChkSupplierName(actionForm.getSupplierName());
           
           ArrayList vpsList = new ArrayList();
                      
           for (int i = 0; i<actionForm.getApCHKListSize(); i++) {
           	
           	   ApPaymentEntryList apCHKList = actionForm.getApCHKByIndex(i);           	   
           	              	   
           	   if (!apCHKList.getPayCheckbox()) continue;
           	   
           	   ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();
           	   
           	   mdetails.setAvVpsCode(apCHKList.getVoucherPaymentScheduleCode());
           	   mdetails.setAvApplyAmount(Common.convertStringMoneyToDouble(apCHKList.getApplyAmount(), precisionUnit));
           	   mdetails.setAvTaxWithheld(Common.convertStringMoneyToDouble(apCHKList.getTaxWithheld(), precisionUnit));
           	   mdetails.setAvDiscountAmount(Common.convertStringMoneyToDouble(apCHKList.getDiscount(), precisionUnit));
           	   mdetails.setAvAllocatedPaymentAmount(Common.convertStringMoneyToDouble(apCHKList.getAllocatedCheckAmount(), precisionUnit));
           	   
           	   vpsList.add(mdetails);
           	
           }
           
           // validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           
           if (!Common.validateRequired(filename1)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename1().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename1NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename1Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename1SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
           
           
           if (!Common.validateRequired(filename2)) {                	       	    	
    	    	
    	    	if (actionForm.getFilename2().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename2NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename2Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename2SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
 	   	   
           if (!Common.validateRequired(filename3)) {                	       	    	
    	    	
    	    	if (actionForm.getFilename3().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename3NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename3Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename3().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename3SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
           
           if (!Common.validateRequired(filename4)) {                	       	    	
    	    	
    	    	if (actionForm.getFilename4().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("paymentEntry.error.filename4NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename4Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename4().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("paymentEntry.error.filename4SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPaymentEntry"));
    	    		
    	    	}    	    	
    	    	
    	   }
                      
           try {
           	
           	    Integer checkCode = ejbCHK.saveApChkEntry(details, actionForm.getBankAccount(),
           	        actionForm.getCurrency(),
           	        actionForm.getSupplier(),
           	        actionForm.getBatchName(),
           	        vpsList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setCheckCode(checkCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.documentNumberNotUnique"));
           
           } catch (ApCHKCheckNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.checkNumberNotUnique"));
                               	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.conversionDateNotExist"));           	                      	
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyVoid"));
                    
           } catch (ApVOUOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.overapplicationNotAllowed", ex.getMessage()));
                    
           } catch (ApCHKVoucherHasNoWTaxCodeException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.voucherHasNoWTaxCode", ex.getMessage()));
                    
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.transactionAlreadyLocked", ex.getMessage()));
           
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.noApprovalApproverFound"));
                    
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.journalNotBalance"));
           	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.branchAccountNumberInvalid"));
           	           	           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
    	    		
	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apPaymentEntry"));
	    		
	       } 
	       
	       // save attachment
           
           if (!Common.validateRequired(filename1)) {     
    		                  	 
      	        if (errors.isEmpty()) {
      	        	       	        	 
      	        	InputStream is = actionForm.getFilename1().getInputStream();
      	        	
      	        	new File(attachmentPath).mkdirs();
      	        	       	        		       	        		       	        	
      	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);            	           	            	
           	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
      	        	
      	        }          	                
           	           	    	
      	   }
          
          
          if (!Common.validateRequired(filename2)) {     
	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
/*******************************************************
	 -- Ap CHK Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("printButton") != null) {
         	
            if(Common.validateRequired(actionForm.getApprovalStatus())) {	
            
               ApCheckDetails details = new ApCheckDetails();
           
	           details.setChkCode(actionForm.getCheckCode());           
	           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
	           details.setChkNumber(actionForm.getCheckNumber());
	           details.setChkDocumentNumber(actionForm.getDocumentNumber());
	           details.setChkReferenceNumber(actionForm.getReferenceNumber());
	           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
	           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
	           details.setChkDescription(actionForm.getDescription());           
	           details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	                      
	           if (actionForm.getCheckCode() == null) {
	           	
	           	   details.setChkCreatedBy(user.getUserName());
		           details.setChkDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setChkLastModifiedBy(user.getUserName());
	           details.setChkDateLastModified(new java.util.Date());
	           details.setChkMemo(actionForm.getMemo());
	           details.setChkSupplierName(actionForm.getSupplierName());
	           
	           ArrayList vpsList = new ArrayList();
	                      
	           for (int i = 0; i<actionForm.getApCHKListSize(); i++) {
	           	
	           	   ApPaymentEntryList apCHKList = actionForm.getApCHKByIndex(i);           	   
	           	              	   
	           	   if (!apCHKList.getPayCheckbox()) continue;
	           	   
	           	   ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();
	           	   
	           	   mdetails.setAvVpsCode(apCHKList.getVoucherPaymentScheduleCode());
	           	   mdetails.setAvApplyAmount(Common.convertStringMoneyToDouble(apCHKList.getApplyAmount(), precisionUnit));
	           	   mdetails.setAvTaxWithheld(Common.convertStringMoneyToDouble(apCHKList.getTaxWithheld(), precisionUnit));
	           	   mdetails.setAvDiscountAmount(Common.convertStringMoneyToDouble(apCHKList.getDiscount(), precisionUnit));
	           	   mdetails.setAvAllocatedPaymentAmount(Common.convertStringMoneyToDouble(apCHKList.getAllocatedCheckAmount(), precisionUnit));
	           	   
	           	   vpsList.add(mdetails);
	           	
	           }
	           
	           // validate attachment
	           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
               String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
               String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
               
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	    	    	    	    	
	    	    	if (actionForm.getFilename1().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename1NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename1Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename1().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename1SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	           
	           
	           if (!Common.validateRequired(filename2)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename2().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename2NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename2Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename2().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename2SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
     	   	   
	           if (!Common.validateRequired(filename3)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename3().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename3NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename3Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename3().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename3SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	           
	           if (!Common.validateRequired(filename4)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename4().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename4NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename4Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename4().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename4SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	                      
	           try {
	           	
	           	    Integer checkCode = ejbCHK.saveApChkEntry(details, actionForm.getBankAccount(),
	           	        actionForm.getCurrency(),
	           	        actionForm.getSupplier(),
	           	     	actionForm.getBatchName(),
	           	        vpsList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	    actionForm.setCheckCode(checkCode);
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.documentNumberNotUnique"));
	           
	           } catch (ApCHKCheckNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.checkNumberNotUnique"));
	                               	
	           } catch (GlobalConversionDateNotExistException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.conversionDateNotExist"));           	                      	
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyVoid"));
	                    
	           } catch (ApVOUOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.overapplicationNotAllowed", ex.getMessage()));
	                    
	           } catch (ApCHKVoucherHasNoWTaxCodeException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.voucherHasNoWTaxCode", ex.getMessage()));
	                    
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyLocked", ex.getMessage()));
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.noApprovalApproverFound"));
	           
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.branchAccountNumberInvalid"));
	           	           	           	
	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
            
        	   if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("apPaymentEntry"));
	               
	           }

		       // save attachment
	           
        	   if (!Common.validateRequired(filename1)) {     
	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	new File(attachmentPath).mkdirs();
	       	        	       	        		       	        		       	        	
	       	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);            	           	            	
	            	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	           
	           
	           if (!Common.validateRequired(filename2)) {     
 	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension); 
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	

	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension); 

		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	

	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension); 
	       	        	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	           
	           actionForm.setReport(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
                             	
            } else {
            
  	           actionForm.setReport(Constants.STATUS_SUCCESS);		     
			
			   return(mapping.findForward("apPaymentEntry")); 
			   
			}
				
/*******************************************************
   -- Ap CV Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("cvPrintButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            if(Common.validateRequired(actionForm.getApprovalStatus())) {	
            
               ApCheckDetails details = new ApCheckDetails();
           
	           details.setChkCode(actionForm.getCheckCode());           
	           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
	           details.setChkNumber(actionForm.getCheckNumber());
	           details.setChkDocumentNumber(actionForm.getDocumentNumber());
	           details.setChkReferenceNumber(actionForm.getReferenceNumber());
	           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
	           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
	           details.setChkDescription(actionForm.getDescription());           
	           details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	                      
	           if (actionForm.getCheckCode() == null) {
	           	
	           	   details.setChkCreatedBy(user.getUserName());
		           details.setChkDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setChkLastModifiedBy(user.getUserName());
	           details.setChkDateLastModified(new java.util.Date());
	           details.setChkMemo(actionForm.getMemo());
	           details.setChkSupplierName(actionForm.getSupplierName());
	           
	           ArrayList vpsList = new ArrayList();
	                      
	           for (int i = 0; i<actionForm.getApCHKListSize(); i++) {
	           	
	           	   ApPaymentEntryList apCHKList = actionForm.getApCHKByIndex(i);           	   
	           	              	   
	           	   if (!apCHKList.getPayCheckbox()) continue;
	           	   
	           	   ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();
	           	   
	           	   mdetails.setAvVpsCode(apCHKList.getVoucherPaymentScheduleCode());
	           	   mdetails.setAvApplyAmount(Common.convertStringMoneyToDouble(apCHKList.getApplyAmount(), precisionUnit));
	           	   mdetails.setAvTaxWithheld(Common.convertStringMoneyToDouble(apCHKList.getTaxWithheld(), precisionUnit));
	           	   mdetails.setAvDiscountAmount(Common.convertStringMoneyToDouble(apCHKList.getDiscount(), precisionUnit));
	           	   mdetails.setAvAllocatedPaymentAmount(Common.convertStringMoneyToDouble(apCHKList.getAllocatedCheckAmount(), precisionUnit));
	           	   
	           	   vpsList.add(mdetails);
	           	
	           }
	           
	           // validate attachment
           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
               String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
               String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
               
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	    	    	    	    	
	    	    	if (actionForm.getFilename1().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename1NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename1Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename1().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename1SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	           
	           
	           if (!Common.validateRequired(filename2)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename2().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename2NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename2Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename2().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename2SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
     	   	   
	           if (!Common.validateRequired(filename3)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename3().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename3NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename3Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename3().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename3SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	           
	           if (!Common.validateRequired(filename4)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename4().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename4NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename4Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename4().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename4SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	                      
	           try {
	           	
	           	    Integer checkCode = ejbCHK.saveApChkEntry(details, actionForm.getBankAccount(),
	           	        actionForm.getCurrency(),
	           	        actionForm.getSupplier(),
	           	        actionForm.getBatchName(),
	           	        vpsList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	    actionForm.setCheckCode(checkCode);
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.documentNumberNotUnique"));
	           
	           } catch (ApCHKCheckNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.checkNumberNotUnique"));
	                               	
	           } catch (GlobalConversionDateNotExistException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.conversionDateNotExist"));           	                      	
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyVoid"));
	                    
	           } catch (ApVOUOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.overapplicationNotAllowed", ex.getMessage()));
	                    
	           } catch (ApCHKVoucherHasNoWTaxCodeException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.voucherHasNoWTaxCode", ex.getMessage()));
	                    
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyLocked", ex.getMessage()));
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.noApprovalApproverFound"));
	                    
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.branchAccountNumberInvalid"));
	           	           	           	
	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
            
        	   if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("apPaymentEntry"));
	               
	           }
	           
	           // save attachment
	           
        	   if (!Common.validateRequired(filename1)) {     
	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	new File(attachmentPath).mkdirs();
	       	        	       	        		       	        		       	        	
	       	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);            	           	            	
	            	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	           
	           
	           if (!Common.validateRequired(filename2)) {     
 	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension); 
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	

	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension); 

		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	

	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension); 
	       	        	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	           
	           actionForm.setReport2(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
                             	
            } else {
            
  	           actionForm.setReport2(Constants.STATUS_SUCCESS);		     
			
			   return(mapping.findForward("apPaymentEntry")); 
			   
			}
			
/*******************************************************
	 -- Ap CHK Journal Action --
*******************************************************/             	
            	
         } else if (request.getParameter("journalButton") != null) {
         	
            if(Common.validateRequired(actionForm.getApprovalStatus())) {	
            
               ApCheckDetails details = new ApCheckDetails();
           
	           details.setChkCode(actionForm.getCheckCode());           
	           details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getCheckDate()));
	           details.setChkNumber(actionForm.getCheckNumber());
	           details.setChkDocumentNumber(actionForm.getDocumentNumber());
	           details.setChkReferenceNumber(actionForm.getReferenceNumber());
	           details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	           details.setChkVoid(Common.convertBooleanToByte(actionForm.getCheckVoid()));
	           details.setChkCrossCheck(Common.convertBooleanToByte(actionForm.getCrossCheck()));
	           details.setChkDescription(actionForm.getDescription());           
	           details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	                      
	           if (actionForm.getCheckCode() == null) {
	           	
	           	   details.setChkCreatedBy(user.getUserName());
		           details.setChkDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setChkLastModifiedBy(user.getUserName());
	           details.setChkDateLastModified(new java.util.Date());
	           details.setChkMemo(actionForm.getMemo());
	           details.setChkSupplierName(actionForm.getSupplierName());
	           
	           ArrayList vpsList = new ArrayList();
	                      
	           for (int i = 0; i<actionForm.getApCHKListSize(); i++) {
	           	
	           	   ApPaymentEntryList apCHKList = actionForm.getApCHKByIndex(i);           	   
	           	              	   
	           	   if (!apCHKList.getPayCheckbox()) continue;
	           	   
	           	   ApModAppliedVoucherDetails mdetails = new ApModAppliedVoucherDetails();
	           	   
	           	   mdetails.setAvVpsCode(apCHKList.getVoucherPaymentScheduleCode());
	           	   mdetails.setAvApplyAmount(Common.convertStringMoneyToDouble(apCHKList.getApplyAmount(), precisionUnit));
	           	   mdetails.setAvTaxWithheld(Common.convertStringMoneyToDouble(apCHKList.getTaxWithheld(), precisionUnit));
	           	   mdetails.setAvDiscountAmount(Common.convertStringMoneyToDouble(apCHKList.getDiscount(), precisionUnit));
	           	   mdetails.setAvAllocatedPaymentAmount(Common.convertStringMoneyToDouble(apCHKList.getAllocatedCheckAmount(), precisionUnit));
	           	   
	           	   vpsList.add(mdetails);
	           	
	           }
	           
	           // validate attachment
           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
               String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
               String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
               
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	    	    	    	    	
	    	    	if (actionForm.getFilename1().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename1NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename1Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename1().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename1SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	           
	           
	           if (!Common.validateRequired(filename2)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename2().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename2NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename2Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename2().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename2SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
     	   	   
	           if (!Common.validateRequired(filename3)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename3().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename3NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename3Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename3().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename3SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
	           
	           if (!Common.validateRequired(filename4)) {                	       	    	
	    	    	
	    	    	if (actionForm.getFilename4().getFileSize() == 0) {
	    	    		
	    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	            			new ActionMessage("paymentEntry.error.filename4NotFound"));
	            			
	    	    	} else {
	    	    		
	    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	   	    	           	    	
	           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
	           	    	               	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename4Invalid"));	
	                    		
	           	    	}
	           	    	
	           	    	InputStream is = actionForm.getFilename4().getInputStream();
	           	    	           	    	
	           	    	if (is.available() > maxAttachmentFileSize) {
	           	    		
	           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    		new ActionMessage("paymentEntry.error.filename4SizeInvalid"));
	           	    		
	           	    	}
	           	    	
	           	    	is.close();	    		
	    	    		
	    	    	}
	    	    	
	    	    	if (!errors.isEmpty()) {
	    	    		
	    	    		saveErrors(request, new ActionMessages(errors));
	           			return (mapping.findForward("apPaymentEntry"));
	    	    		
	    	    	}    	    	
	    	    	
	    	   }
     	   	   
     	   	       
     	   	    	
	                      
	           try {
	           	
	           	    Integer checkCode = ejbCHK.saveApChkEntry(details, actionForm.getBankAccount(),
	           	        actionForm.getCurrency(),
	           	        actionForm.getSupplier(),
	           	        actionForm.getBatchName(),
	           	        vpsList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	    actionForm.setCheckCode(checkCode);
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.recordAlreadyDeleted"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.documentNumberNotUnique"));
	           
	           } catch (ApCHKCheckNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.checkNumberNotUnique"));
	                               	
	           } catch (GlobalConversionDateNotExistException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.conversionDateNotExist"));           	                      	
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyVoid"));
	                    
	           } catch (ApVOUOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.overapplicationNotAllowed", ex.getMessage()));
	                    
	           } catch (ApCHKVoucherHasNoWTaxCodeException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.voucherHasNoWTaxCode", ex.getMessage()));
	                    
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.transactionAlreadyLocked", ex.getMessage()));
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.noApprovalApproverFound"));
	                    
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.journalNotBalance"));
	           	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("paymentEntry.error.branchAccountNumberInvalid"));
	           	           	           	
	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
            
        	   if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("apPaymentEntry"));
	               
	           }
	           
	           // save attachment
	           
	           if (!Common.validateRequired(filename1)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	new File(attachmentPath).mkdirs();
	       	        	       	        		       	        		       	        	
	       	        	FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);            	           	            	
	            	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	           
	           
	           if (!Common.validateRequired(filename2)) {     
  	    		                  	 
 	       	        if (errors.isEmpty()) {
 	       	        	       	        	 
 	       	        	InputStream is = actionForm.getFilename2().getInputStream();
 	       	        	
 	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
 	       	        	
 	       	        	new File(attachmentPath).mkdirs();       	        	
 	       	        	
 	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-2" + fileExtension); 
 		            	int c;        
 		            	            	            	            	
 		            	while ((c = is.read()) != -1) {
 		            		
 		            		fos.write((byte)c);
 		            		
 		            	}
 		            	
 		            	is.close();
 						fos.close();
 	       	        	
 	       	        }          	                
 	            	           	    	
 	       	   }
 	       	   
 	       	   if (!Common.validateRequired(filename3)) {     
 	           	               	            	    		       	   		       	    		           	    		                  	 
 	       	        if (errors.isEmpty()) {
 	       	        	       	        	 
 	       	        	InputStream is = actionForm.getFilename3().getInputStream();
 	       	        	
 	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
 	       	        	
 	       	        	new File(attachmentPath).mkdirs();       	        	

 	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-3" + fileExtension); 

 		            	int c;        
 		            	            	            	            	
 		            	while ((c = is.read()) != -1) {
 		            		
 		            		fos.write((byte)c);
 		            		
 		            	}
 		            	
 		            	is.close();
 						fos.close();
 	       	        	
 	       	        }          	                
 	            	           	    	
 	       	   }
 	       	   
 	       	   if (!Common.validateRequired(filename4)) {     
 	           	               	            	    		       	   		       	    		           	    		                  	 
 	       	        if (errors.isEmpty()) {
 	       	        	       	        	 
 	       	        	InputStream is = actionForm.getFilename4().getInputStream();
 	       	        	
 	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
 	       	        	
 	       	        	new File(attachmentPath).mkdirs();       	        	

 	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCheckCode() + "-4" + fileExtension); 
 	       	        	
 		            	int c;        
 		            	            	            	            	
 		            	while ((c = is.read()) != -1) {
 		            		
 		            		fos.write((byte)c);
 		            		
 		            	}
 		            	
 		            	is.close();
 						fos.close();
 	       	        	
 	       	        }          	                
 	            	           	    	
 	       	   }
                             	
            } 
            
            String path = "/apJournal.do?forward=1" + 
		     "&transactionCode=" + actionForm.getCheckCode() +
		     "&transaction=PAYMENT" + 
		     "&transactionNumber=" + actionForm.getCheckNumber() + 
		     "&transactionDate=" + actionForm.getDate() +
		     "&transactionEnableFields=" + actionForm.getEnableFields();

            return(new ActionForward(path));
            
/*******************************************************
   -- Ap CHK Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbCHK.deleteApChkEntry(actionForm.getCheckCode(), user.getUserName(), user.getCmpCode());
           	    
           	    // delete attachment	
										
			if (actionForm.getCheckCode() != null) {
				
				appProperties = MessageResources.getMessageResources("com.ApplicationResources");
	
	            attachmentPath = appProperties.getMessage("app.attachmentPath") + "ap/payment/";
	            attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
				File file = new File(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);
				
				if (file.exists()) {
					
					file.delete();	
					
				}						
				
			}
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
                return(mapping.findForward("cmnErrorPage"));
            }            

/*******************************************************
   -- Ap CHK View Attachment Action --
*******************************************************/

         } else if(request.getParameter("viewAttachmentButton1") != null) {
         	
         	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);

         	
	       	 byte data[] = new byte[fis.available()];
	
	       	 int ctr = 0;
	       	 int c = 0;
	
	       	 while ((c = fis.read()) != -1) {
	
	       		 data[ctr] = (byte)c;
	       		 ctr++;
	
	       	 }
	       	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));
	
	       	 session.setAttribute(Constants.IMAGE_KEY, image);
	
	       	 actionForm.setAttachment(Constants.STATUS_SUCCESS);
	
	       	 if (request.getParameter("child") == null) {
	       		 return (mapping.findForward("apPaymentEntry"));         
	
	       	 } else {
	       		 return (mapping.findForward("apPaymentEntryChild"));         
	
	       	 }

        }  else if(request.getParameter("viewAttachmentButton2") != null) {



       	 FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-2" + attachmentFileExtension);

       	 byte data[] = new byte[fis.available()];

       	 int ctr = 0;
       	 int c = 0;

       	 while ((c = fis.read()) != -1) {

       		 data[ctr] = (byte)c;
       		 ctr++;

       	 }

       	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));

       	 session.setAttribute(Constants.IMAGE_KEY, image);

       	 actionForm.setAttachment(Constants.STATUS_SUCCESS);

       	 if (request.getParameter("child") == null) {

       		 return (mapping.findForward("apPaymentEntry"));         

       	 } else {

       		 return (mapping.findForward("apPaymentEntryChild"));         

       	 } 

        } else if(request.getParameter("viewAttachmentButton3") != null) {



       	 FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-3" + attachmentFileExtension);

       	 byte data[] = new byte[fis.available()];

       	 int ctr = 0;
       	 int c = 0;

       	 while ((c = fis.read()) != -1) {

       		 data[ctr] = (byte)c;
       		 ctr++;

       	 }

       	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));

       	 session.setAttribute(Constants.IMAGE_KEY, image);

       	 actionForm.setAttachment(Constants.STATUS_SUCCESS);

       	 if (request.getParameter("child") == null) {

       		 return (mapping.findForward("apPaymentEntry"));         

       	 } else {

       		 return (mapping.findForward("apPaymentEntryChild"));         

       	 }

        }  else if(request.getParameter("viewAttachmentButton4") != null) {



       	 FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCheckCode() + "-4" + attachmentFileExtension);

       	 byte data[] = new byte[fis.available()];

       	 int ctr = 0;
       	 int c = 0;

       	 while ((c = fis.read()) != -1) {

       		 data[ctr] = (byte)c;
       		 ctr++;

       	 }

       	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));

       	 session.setAttribute(Constants.IMAGE_KEY, image);

       	 actionForm.setAttachment(Constants.STATUS_SUCCESS);

       	 if (request.getParameter("child") == null) {

       		 return (mapping.findForward("apPaymentEntry"));         

       	 } else {

       		 return (mapping.findForward("apPaymentEntryChild"));         

       	 }

/*******************************************************
   -- Ap CHK Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ap CHK Supplier Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
         	
         	try {
             	System.out.println("supp is: " + actionForm.getSupplier());
                 ApModSupplierDetails mdetails = ejbCHK.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());
                 
                 actionForm.clearApCHKList();
                 actionForm.setBankAccount(mdetails.getSplStBaName());
                 actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
                 actionForm.setSupplierName(mdetails.getSplName());
          		 actionForm.setTempSupplierName(mdetails.getSplName());
          		 
          		 
             } catch (GlobalNoRecordFoundException ex) {
             	
             	actionForm.clearApCHKList();
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            try {
             	
                 ArrayList list = ejbCHK.getApVpsBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                 
                 Iterator i = list.iterator();
                 
                 while (i.hasNext()) {
                 	
                 	ApModVoucherPaymentScheduleDetails mdetails = (ApModVoucherPaymentScheduleDetails)i.next();
                 	
                 	ApPaymentEntryList vpsList = new ApPaymentEntryList(
                 		actionForm, mdetails.getVpsCode(), 
                 		mdetails.getVpsVouCode(),
                 		mdetails.getVpsVouDocumentNumber(),						
                 		Common.convertShortToString(mdetails.getVpsNumber()),
                 		Common.convertSQLDateToString(mdetails.getVpsDueDate()),
                 		mdetails.getVpsVouReferenceNumber(),
						mdetails.getVpsVouDescription(),
                 		mdetails.getVpsVouFcName(),
                 		Common.convertDoubleToStringMoney(mdetails.getVpsAmountDue(), precisionUnit),
                 		Common.convertDoubleToStringMoney(mdetails.getVpsAvApplyAmount(), precisionUnit),
                 		Common.convertDoubleToStringMoney(mdetails.getVpsAvTaxWithheld(), precisionUnit),
                 		Common.convertDoubleToStringMoney(mdetails.getVpsAvDiscountAmount(), precisionUnit),
                 		Common.convertDoubleToStringMoney(0d, precisionUnit));
                 	
                 	
                 	vpsList.setConversionDate(Common.convertSQLDateToString(mdetails.getVpsVouConversionDate()));
                 	System.out.println("----------------------mdetails.getVpsVouConversionRate()="+mdetails.getVpsVouConversionRate());
                 	vpsList.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getVpsVouConversionRate(),Constants.CONVERSION_RATE_PRECISION));

                 	actionForm.setCurrency(mdetails.getVpsVouFcName());
                 	actionForm.saveApCHKList(vpsList);
                 
                 }
                 
                 // check if prev should be disabled
                 if (actionForm.getLineCount() == 0) {
                 	
                 	actionForm.setDisablePreviousButton(true);
                 	actionForm.setDisableFirstButton(true);
                 	
                 } else {
                 	
                 	actionForm.setDisablePreviousButton(false);
                 	actionForm.setDisableFirstButton(false);
                 	
                 }
                 
                 // check if next should be disabled
                 if (actionForm.getApCHKListSize() <= actionForm.getLineCount() + actionForm.getMaxRows()) {
                 	
                 	actionForm.setDisableNextButton(true);
                 	actionForm.setDisableLastButton(true);
                 	
                 } else {
                 	
                 	actionForm.setDisableNextButton(false);
                 	actionForm.setDisableLastButton(false);
                 	
                 }
                 
             } catch (GlobalNoRecordFoundException ex) {
             	
             	actionForm.clearApCHKList();
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.noOpenVoucherFound"));
                saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 	
                        
            return(mapping.findForward("apPaymentEntry"));
            
            
/*******************************************************
    -- Conversion Date Enter Action --
 *******************************************************/
  } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

  	try {

  		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
  				ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
  						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

  	} catch (GlobalConversionDateNotExistException ex) {

  		errors.add(ActionMessages.GLOBAL_MESSAGE,
  				new ActionMessage("paymentEntry.error.conversionDateNotExist"));

  	} catch (EJBException ex) {

  		if (log.isInfoEnabled()) {

  			log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
  					" session: " + session.getId());

  		}

  		return(mapping.findForward("cmnErrorPage"));

  	}

  	if (!errors.isEmpty()) {

  		saveErrors(request, new ActionMessages(errors));

  	}

  	return(mapping.findForward("apPaymentEntry"));


            
/*******************************************************
   -- Ap CHK Conversion Date Enter Action --
*******************************************************/
      } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

      	try {

      		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
     		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
     		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));
     		
     		
      	} catch (GlobalConversionDateNotExistException ex) {
      		
      		errors.add(ActionMessages.GLOBAL_MESSAGE,
      				new ActionMessage("paymentEntry.error.conversionDateNotExist"));
      		
      	} catch (EJBException ex) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());
      			
      		}
      		
      		return(mapping.findForward("cmnErrorPage"));
      		
      	} 

      	if (!errors.isEmpty()) {
      		
      		saveErrors(request, new ActionMessages(errors));
      		
      	}
      	
      	return(mapping.findForward("apPaymentEntry"));


/*******************************************************
   -- Ap CHK Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apPaymentEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbCHK.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();
            			
            			actionForm.setCurrencyList(mFcDetails.getFcName());
            			
            			if (mFcDetails.getFcSob() == 1) {
            				
            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());
            				
            			}            			
            		                			
            		}
            		
            	}
 	
            	actionForm.clearBankAccountList();           	
            	
            	list = ejbCHK.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}            		
            		            		            		
            	}                	
            	 
            	if(actionForm.getUseSupplierPulldown()) {
            		
	            	actionForm.clearSupplierList();           	
	            	
	            	list = ejbCHK.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbCHK.getApOpenCbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	}
            	            	
            	if (request.getParameter("forwardBatch") != null) {
            		
            		actionForm.setBatchName(request.getParameter("batchName"));
            		
            	}
            	
            	actionForm.clearApCHKList();               	  	
            	
            	if (request.getParameter("forward") != null || isInitialPrinting) {
            		
            		if (request.getParameter("forward") != null) {
            			            	    
            			actionForm.setCheckCode(new Integer(request.getParameter("checkCode")));
            			
            		}
            		
            		ApModCheckDetails mdetails = ejbCHK.getApChkByChkCode(actionForm.getCheckCode(), user.getCmpCode());
            		
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getChkDate()));
            		actionForm.setCheckDate(Common.convertSQLDateToString(mdetails.getChkCheckDate()));
            		actionForm.setCheckNumber(mdetails.getChkNumber());
            		actionForm.setDocumentNumber(mdetails.getChkDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getChkReferenceNumber());
            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getChkAmount(), precisionUnit));
            		actionForm.setCheckVoid(Common.convertByteToBoolean(mdetails.getChkVoid()));
            		actionForm.setCrossCheck(Common.convertByteToBoolean(mdetails.getChkCrossCheck()));
            		actionForm.setDescription(mdetails.getChkDescription());
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getChkConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getChkConversionRate(), Constants.CONVERSION_RATE_PRECISION));
            		actionForm.setApprovalStatus(mdetails.getChkApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getChkReasonForRejection());
            		actionForm.setPosted(mdetails.getChkPosted() == 1 ? "YES" : "NO");
            		actionForm.setVoidApprovalStatus(mdetails.getChkVoidApprovalStatus());
            		actionForm.setVoidPosted(mdetails.getChkVoidPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getChkCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getChkDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getChkLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getChkDateLastModified()));            		
            		actionForm.setApprovedRejectedBy(mdetails.getChkApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getChkDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getChkPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getChkDatePosted()));
            		actionForm.setMemo(mdetails.getChkMemo());
            		
            		if (!actionForm.getCurrencyList().contains(mdetails.getChkFcName())) {
            			
            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearCurrencyList();
            				
            			}
            			actionForm.setCurrencyList(mdetails.getChkFcName());
            			
            		}
            		actionForm.setCurrency(mdetails.getChkFcName());
            		
            		if (!actionForm.getSupplierList().contains(mdetails.getChkSplSupplierCode())) {
            			
            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearSupplierList();
            				
            			}
            			actionForm.setSupplierList(mdetails.getChkSplSupplierCode());
            			
            		}
            		actionForm.setSupplier(mdetails.getChkSplSupplierCode());
            		actionForm.setSupplierName(mdetails.getChkSplName());
            		
            		if (!actionForm.getBankAccountList().contains(mdetails.getChkBaName())) {
            			
            			if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearBankAccountList();
            				
            			}
            			actionForm.setBankAccountList(mdetails.getChkBaName());
            			
            		}
            		actionForm.setBankAccount(mdetails.getChkBaName());
            		
            		if (!actionForm.getBatchNameList().contains(mdetails.getChkCbName())) {
            			
           		    	actionForm.setBatchNameList(mdetails.getChkCbName());
           		    	
           		    }           		    
            		actionForm.setBatchName(mdetails.getChkCbName());
            		            		            		
            		list = mdetails.getChkAvList();
            		         		            		            		
		            i = list.iterator();
		            		            
		            while (i.hasNext()) {
		            	
			           ApModAppliedVoucherDetails mAvDetails = (ApModAppliedVoucherDetails)i.next();
			           
				       ApPaymentEntryList vpsList = new ApPaymentEntryList(
                 		actionForm, mAvDetails.getAvVpsCode(), mAvDetails.getAvVpsVouCode(),
                 		mAvDetails.getAvVpsVouDocumentNumber(),
                 		Common.convertShortToString(mAvDetails.getAvVpsNumber()),
                 		Common.convertSQLDateToString(mAvDetails.getAvVpsDueDate()),
                 		mAvDetails.getAvVpsVouReferenceNumber(),
						mAvDetails.getAvVpsVouDescription(),
                 		mAvDetails.getAvVpsVouFcName(),
                 		Common.convertDoubleToStringMoney(mAvDetails.getAvVpsAmountDue(), precisionUnit),
                 		Common.convertDoubleToStringMoney(mAvDetails.getAvApplyAmount(), precisionUnit),
                 		Common.convertDoubleToStringMoney(mAvDetails.getAvTaxWithheld(), precisionUnit),
                 		Common.convertDoubleToStringMoney(mAvDetails.getAvDiscountAmount(), precisionUnit),
                 		Common.convertDoubleToStringMoney(mAvDetails.getAvAllocatedPaymentAmount(), precisionUnit));
                 		
                 		vpsList.setPayCheckbox(true);
				        
                 		vpsList.setConversionDate(mAvDetails.getAvVpsConversionDate());
                 		vpsList.setConversionRate(Common.convertDoubleToStringMoney(mAvDetails.getAvVpsConversionRate(), precisionUnit));
				        actionForm.saveApCHKList(vpsList); 
				        				        				      
			        }
		            
		            if(actionForm.getTempSupplierName() == null || actionForm.getTempSupplierName().length() == 0) {
		            	
        				actionForm.setTempSupplierName(actionForm.getSupplierName());
        				
        			} else {
        				
        				actionForm.setSupplierName(actionForm.getTempSupplierName());
        				
        			}
			        
			        this.setFormProperties(actionForm, user.getCompany());
			        
			        if (actionForm.getEnableFields()) {				        			        				        
			        			        			        
				        try {
	         	
			                 list = ejbCHK.getApVpsBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
			                 		                 		                 
			                 i = list.iterator();	                 
			                 
			                 while (i.hasNext()) {
			                 	
			                 	ApModVoucherPaymentScheduleDetails mVpsDetails = (ApModVoucherPaymentScheduleDetails)i.next();
			                 			                 			                 			                 			                 			                 			                 	
			                 	ApPaymentEntryList vpsList = new ApPaymentEntryList(
			                 		actionForm, mVpsDetails.getVpsCode(), mVpsDetails.getVpsVouCode(),
			                 		mVpsDetails.getVpsVouDocumentNumber(),
			                 		Common.convertShortToString(mVpsDetails.getVpsNumber()),
			                 		Common.convertSQLDateToString(mVpsDetails.getVpsDueDate()),
			                 		mVpsDetails.getVpsVouReferenceNumber(),
									mVpsDetails.getVpsVouDescription(),
			                 		mVpsDetails.getVpsVouFcName(),
			                 		Common.convertDoubleToStringMoney(mVpsDetails.getVpsAmountDue(), precisionUnit),
			                 		Common.convertDoubleToStringMoney(mVpsDetails.getVpsAvApplyAmount(), precisionUnit),
			                 		Common.convertDoubleToStringMoney(mVpsDetails.getVpsAvTaxWithheld(), precisionUnit),
			                 		Common.convertDoubleToStringMoney(mVpsDetails.getVpsAvDiscountAmount(), precisionUnit),
			                 		Common.convertDoubleToStringMoney(0d, precisionUnit));
			                 		
			                     actionForm.saveApCHKList(vpsList);
			                 	
			                 }
			                 
			             } catch (GlobalNoRecordFoundException ex) {
			             			           			             	
			             } 
			                    
			         }         
			        
			        actionForm.setLineCount(0);
		            actionForm.setMaxRows(5);
		            actionForm.setDisablePreviousButton(true);
		            actionForm.setDisableFirstButton(true);
		            
		         	// check if next should be disabled
		         	if (actionForm.getApCHKListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {
		         		
		         		actionForm.setDisableNextButton(true);
		         		actionForm.setDisableLastButton(true);
		         		
		         	} else {
		         		
		         		actionForm.setDisableNextButton(false);
		         		actionForm.setDisableLastButton(false);
		         		
		         	}
			         
			         if (request.getParameter("child") == null) {
			         	
			         	return (mapping.findForward("apPaymentEntry"));         
			         	
			         } else {
			         	
			         	return (mapping.findForward("apPaymentEntryChild"));         
			         	
			         }    
			         			         			         			            
            		
            	}            	                     	           			
            		           			   	
            } catch(GlobalNoRecordFoundException ex) {
            	            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("paymentEntry.error.checkAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveSubmitButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                   try {
                   	
                   	   ArrayList list = ejbCHK.getAdApprovalNotifiedUsersByChkCode(actionForm.getCheckCode(), user.getCmpCode());
                   	                      	   
                   	   if (list.isEmpty()) {
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));     
                   	   
                   	   } else if (list.contains("DOCUMENT POSTED")) {
                   	   	
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,	
                	   	       new ActionMessage("messages.documentPosted"));
                	   	                             	   	   
                   	   } else {
                   	   	
                   	   	   Iterator i = list.iterator();
                   	   	   
                   	   	   String APPROVAL_USERS = "";
                   	   	   
                   	   	   while (i.hasNext()) {
                   	   	   	                   	   	   	                   	   	   	                      	   	   	   	
                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
                   	   	       
                   	   	       if (i.hasNext()) {
                   	   	       	
                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";
                   	   	       	
                   	   	       }
                   	   	                          	   	       
                   	   	   	   	                   	   	   	                      	   	   	
                   	   	   }
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
                   	   	
                   	   }
                   	   
                   	   if (actionForm.getCheckVoid() && actionForm.getPosted().equals("NO")) {
                   	   	   
                   	   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                   	   	
                   	   } else {
                   	   	                   	   
	                   	   saveMessages(request, messages);
	                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
	                   	   
	                   }
                   	
                  	
                   } catch(EJBException ex) {
	     	
		               if (log.isInfoEnabled()) {
		               	
		                  log.info("EJBException caught in ApPaymentEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }
		               
		               return(mapping.findForward("cmnErrorPage"));
		               
		           } 
                   
               } else if (request.getParameter("saveAsDraftButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setCheckCode(null);          
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            
            if(defaultCheckDate.equals("CURRENT DATE")) {
            	
            	actionForm.setCheckDate(Common.convertSQLDateToString(new java.util.Date()));
            	
            }
            
            actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setConversionRate(Common.convertDoubleToStringMoney(1d, Constants.CONVERSION_RATE_PRECISION));
            actionForm.setPosted("NO");
            actionForm.setVoidPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(5);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.setDisableLastButton(true);
            actionForm.setDisableFirstButton(true);
            
            this.setFormProperties(actionForm, user.getCompany());
            return(mapping.findForward("apPaymentEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ApPaymentEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ApPaymentEntryForm actionForm, String adCompany) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getCheckVoid() && actionForm.getPosted().equals("NO")) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableCheckVoid(false);
			
			} else if (actionForm.getPosted().equals("NO")) {
				
				if (actionForm.getCheckCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableCheckVoid(false);
					
				} else if (actionForm.getCheckCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableCheckVoid(true);
				    
				    	
				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
				    actionForm.getApprovalStatus().equals("N/A")) {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableCheckVoid(true);
					
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableCheckVoid(false);
					
				}
				
			} else {
				
				actionForm.setEnableFields(false);				
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				
				if (actionForm.getVoidApprovalStatus() == null) {
					actionForm.setShowSaveSubmitButton(true);				
					actionForm.setEnableCheckVoid(true);
				} else {
					actionForm.setShowSaveSubmitButton(false);				
					actionForm.setEnableCheckVoid(false);					
				}
				
			}						
			
		} else {
			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableCheckVoid(false);
			
		}
				
		// view attachment	
										
		if (actionForm.getCheckCode() != null) {
			
			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/payment/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            
            
			File file = new File(attachmentPath + actionForm.getCheckCode() + "-1" + attachmentFileExtension);
 			
 			if (file.exists()) {
 				
 				actionForm.setShowViewAttachmentButton1(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton1(false);	
 				
 			}			
 			
 			file = new File(attachmentPath + actionForm.getCheckCode() + "-2" + attachmentFileExtension);
 			
 			if (file.exists()) {
 				
 				actionForm.setShowViewAttachmentButton2(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton2(false);	
 				
 			}
 			
 			file = new File(attachmentPath + actionForm.getCheckCode() + "-3" + attachmentFileExtension);
 			
 			if (file.exists()) {
 				
 				actionForm.setShowViewAttachmentButton3(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton3(false);	
 				
 			}
 			
 			file = new File(attachmentPath + actionForm.getCheckCode() + "-4" + attachmentFileExtension);
 			
 			if (file.exists()) {
 				
 				actionForm.setShowViewAttachmentButton4(true);	
 				
 			} else {
 				
 				actionForm.setShowViewAttachmentButton4(false);	
 				
 			}												
			
		} else {
						
			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);
			
		}	
		    
	}
		
}