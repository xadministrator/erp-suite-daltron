package com.struts.ap.paymententry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApPaymentEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
   private Integer checkCode = null;
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String supplier = null; 
   private ArrayList supplierList = new ArrayList();   
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();   
   private String date = null;
   private String checkDate = null;
   private String checkNumber = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String amount = null;   
   private boolean checkVoid = false;
   private boolean crossCheck = false;
   private String description = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();

   private String conversionDate = null;
   private String conversionRate = null;
   private String approvalStatus = null;
   private String reasonForRejection = null;
   private String posted = null;
   private String voidApprovalStatus = null;
   private String voidPosted = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;   
   private String approvedRejectedBy = null;
   private String dateApprovedRejected = null;
   private String postedBy = null;
   private String datePosted = null;
   
   private FormFile filename1 = null;
   private FormFile filename2 = null;
   private FormFile filename3 = null;
   private FormFile filename4 = null;
   
   private String functionalCurrency = null;
   
   
   private ArrayList apCHKList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showBatchName = false;
   private boolean enableCheckVoid = false;
   private boolean showSaveSubmitButton = false;
   private boolean showSaveAsDraftButton = false;
   private boolean showDeleteButton = false;
   private boolean showViewAttachmentButton1 = false;
   private boolean showViewAttachmentButton2 = false;
   private boolean showViewAttachmentButton3 = false;
   private boolean showViewAttachmentButton4 = false;
   private boolean useSupplierPulldown = true;
   
   private String isSupplierEntered  = null;
   private String defaultCheckDate = null;
   private String isConversionDateEntered = null;

   private String report = null;
   private String report2 = null;
   private String attachment = null;
  
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   private int lineCount = 0;
   private int maxRows = 0;
   
   private String supplierName = null;
   private String tempSupplierName = null;
   private String memo = null;
   
   public String getReport() {
   	
   	   return report;
   	
   }
   
   public void setReport(String report) {
   	
   	   this.report = report;
   	
   }
   
   public String getReport2() {
   	
   	   return report2;
   	
   }
   
   public void setReport2(String report2) {
   	
   	   this.report2 = report2;
   	
   }
   
   public String getAttachment() {
   	
   	   return attachment;
   	
   }
   
   public void setAttachment(String attachment) {
   	
   	   this.attachment = attachment;
   	
   }
        
   public int getRowSelected(){
      return rowSelected;
   }

   public ApPaymentEntryList getApCHKByIndex(int index){
      return((ApPaymentEntryList)apCHKList.get(index));
   }

   public Object[] getApCHKList(){
      return(apCHKList.toArray());
   }

   public int getApCHKListSize(){
      return(apCHKList.size());
   }

   public void saveApCHKList(Object newApCHKList){
      apCHKList.add(newApCHKList);
   }

   public void clearApCHKList(){
      apCHKList.clear();
   }

   public void setRowSelected(Object selectedApCHKList, boolean isEdit){
      this.rowSelected = apCHKList.indexOf(selectedApCHKList);
   }

   public void updateApCHKRow(int rowSelected, Object newApCHKList){
      apCHKList.set(rowSelected, newApCHKList);
   }

   public void deleteApCHKList(int rowSelected){
      apCHKList.remove(rowSelected);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public short getPrecisionUnit() {
		
		return precisionUnit;
		
	}
	
	public void setPrecisionUnit(short precisionUnit) {
		
		this.precisionUnit = precisionUnit;
		
	}
   
   public Integer getCheckCode() {
   	
   	  return checkCode;
   	
   }
   
   public void setCheckCode(Integer checkCode) {
   	
   	  this.checkCode = checkCode;
   	
   }
   
   public String getBatchName() {
   	
   	  return batchName;
   	
   }
   
   public void setBatchName(String batchName) {
   	
   	  this.batchName = batchName;
   	
   }
   
   public ArrayList getBatchNameList() {
   	
   	  return batchNameList;
   	
   }
   
   public void setBatchNameList(String batchName) {
   	
   	  batchNameList.add(batchName);
   	
   }
   
   public void clearBatchNameList() {   	 
   	
   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getSupplier() {
   	
   	  return supplier;
   	
   }
   
   public void setSupplier(String supplier) {
   	
   	  this.supplier = supplier;
   	
   }
   
   public ArrayList getSupplierList() {
   	
   	  return supplierList;
   	
   }
   
   public void setSupplierList(String supplier) {
   	
   	  supplierList.add(supplier);
   	
   }
   
   public void clearSupplierList() {
   	
   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getBankAccount() {
   	
   	  return bankAccount;
   	
   }
   
   public void setBankAccount(String bankAccount) {
   	
   	  this.bankAccount = bankAccount;
   	
   }
   
   public ArrayList getBankAccountList() {
   	
   	  return bankAccountList;
   	
   }
   
   public void setBankAccountList(String bankAccount) {
   	
   	  bankAccountList.add(bankAccount);
   	
   }
   
   public void clearBankAccountList() {
   	
   	  bankAccountList.clear();
   	  bankAccountList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getDate() {
   	  
   	  return date;
   	   	
   }
   
   public void setDate(String date) {
   	
   	  this.date = date;
   	
   }
   
   public String getCheckDate() {
 	  
 	  return checkDate;
 	   	
   }
 
   public void setCheckDate(String checkDate) {
 	
 	  this.checkDate = checkDate;
 	
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	
   }
   
   public void setCheckNumber(String checkNumber) {
   	
   	  this.checkNumber = checkNumber;
   	
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	
   }
   
   public void setDocumentNumber(String documentNumber) {
   	
   	  this.documentNumber = documentNumber;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	   this.referenceNumber = referenceNumber;
   	
   }
   
   public String getAmount() {
   	
   	  return amount;
   	
   }
   
   public void setAmount(String amount) {
   	
   	  this.amount = amount;
   	
   }   
     
   public boolean getCheckVoid() {
   	
   	  return checkVoid;
   	
   }
   
   public void setCheckVoid(boolean checkVoid) {
   	
   	  this.checkVoid = checkVoid;
   	
   }
   
   public boolean getCrossCheck() {
   	
   	  return crossCheck;
   	
   }
   
   public void setCrossCheck(boolean crossCheck) {
   	
   	  this.crossCheck = crossCheck;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }        
   
   public String getCurrency() {
   	
   	   return currency;
   	
   }
   
   public void setCurrency(String currency) {
   	
   	   this.currency = currency;
   	
   }
   
   public ArrayList getCurrencyList() {
   	
   	   return currencyList;
   	
   }
   
   public void setCurrencyList(String currency) {
   	
   	   currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	   currencyList.clear();
   	
   }
   
   
   public String getConversionDate() {
   	
   	   return conversionDate;
   	
   }
   
   public void setConversionDate(String conversionDate) {
   	
   	   this.conversionDate = conversionDate;
   	
   }
   
   public String getConversionRate() {
   	 
   	   return conversionRate;
   	
   }
   
   public void setConversionRate(String conversionRate) {
   	
   	   this.conversionRate = conversionRate;
   	
   }   
      
   public String getApprovalStatus() {
   	
   	   return approvalStatus;
   	
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	   this.approvalStatus = approvalStatus;
   	
   }
   
   public String getReasonForRejection() {
   	
   	   return reasonForRejection;
   	   
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	   this.reasonForRejection = reasonForRejection;
   	   
   }
   
   public String getPosted() {
   	
   	   return posted;
   	
   }
   
   public void setPosted(String posted) {
   	
   	   this.posted = posted;
   	
   }
   
   public String getVoidApprovalStatus() {
   	
   	   return voidApprovalStatus;
   	
   }
   
   public void setVoidApprovalStatus(String voidApprovalStatus) {
   	
   	   this.voidApprovalStatus = voidApprovalStatus;
   	
   }
   
   public String getVoidPosted() {
   	
   	   return voidPosted;
   	
   }
   
   public void setVoidPosted(String voidPosted) {
   	
   	   this.voidPosted = voidPosted;
   	
   }

   public String getCreatedBy() {
   	
   	   return createdBy;
   	
   }
   
   public void setCreatedBy(String createdBy) {
   	
   	  this.createdBy = createdBy;
   	
   }
   
   public String getDateCreated() {
   	
   	   return dateCreated;
   	
   }
   
   public void setDateCreated(String dateCreated) {
   	
   	   this.dateCreated = dateCreated;
   	
   }   
   
   public String getLastModifiedBy() {
   	
   	  return lastModifiedBy;
   	
   }
   
   public void setLastModifiedBy(String lastModifiedBy) {
   	
   	  this.lastModifiedBy = lastModifiedBy;
   	
   }
   
   public String getDateLastModified() {
   	
   	  return dateLastModified;
   	
   }
   
   public void setDateLastModified(String dateLastModified) {
   	
   	  this.dateLastModified = dateLastModified;
   	
   }

   
   public String getApprovedRejectedBy() {
   	
   	  return approvedRejectedBy;
   	
   }
   
   public void setApprovedRejectedBy(String approvedRejectedBy) {
   	
   	  this.approvedRejectedBy = approvedRejectedBy;
   	
   }
   
   public String getDateApprovedRejected() {
   	
   	  return dateApprovedRejected;
   	
   }
   
   public void setDateApprovedRejected(String dateApprovedRejected) {
   	
   	  this.dateApprovedRejected = dateApprovedRejected;
   	
   }
   
   public String getPostedBy() {
   	
   	  return postedBy;
   	
   }
   
   public void setPostedBy(String postedBy) {
   	
   	  this.postedBy = postedBy;
   	
   }
   
   public String getDatePosted() {
   	
   	  return datePosted;
   	
   }
   
   public void setDatePosted(String datePosted) {
   	
   	  this.datePosted = datePosted;
   	
   }
   
   public FormFile getFilename1() {
   	
   	  return filename1;
   	
   }
   
   public void setFilename1(FormFile filename1) {
   	  
   	  this.filename1 = filename1;
   	
   }
   
   
   public FormFile getFilename2() {
	   	
   	  return filename2;
   	
   }
   
   public void setFilename2(FormFile filename2) {
   	  
   	  this.filename2 = filename2;
   	
   }
   
   public FormFile getFilename3() {
	   	
   	  return filename3;
   	
   }
   
   public void setFilename3(FormFile filename3) {
   	  
   	  this.filename3 = filename3;
   	
   }
   
   public FormFile getFilename4() {
	   	
   	  return filename4;
   	
   }
   
   public void setFilename4(FormFile filename4) {
   	  
   	  this.filename4 = filename4;
   	
   }
   
   
	   
   public String getFunctionalCurrency() {
   	
   	   return functionalCurrency;
   	
   }
   
   public void setFunctionalCurrency(String functionalCurrency) {
   	
   	   this.functionalCurrency = functionalCurrency;
   	
   }
   
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getEnableCheckVoid() {
   	
   	   return enableCheckVoid;
   	
   }
   
   public void setEnableCheckVoid(boolean enableCheckVoid) {
   	
   	   this.enableCheckVoid = enableCheckVoid;
   	
   }
     
     
   public boolean getShowSaveSubmitButton() {
   	
   	   return showSaveSubmitButton;
   	
   }
   
   public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {
   	
   	   this.showSaveSubmitButton = showSaveSubmitButton;
   	
   }
   
   public boolean getShowSaveAsDraftButton() {
   	
   	   return showSaveAsDraftButton;
   	
   }
   
   public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {
   	
   	   this.showSaveAsDraftButton = showSaveAsDraftButton;
   	
   }
   
   public boolean getShowDeleteButton() {
   	
   	   return showDeleteButton;
   	
   }
   
   public void setShowDeleteButton(boolean showDeleteButton) {
   	
   	   this.showDeleteButton = showDeleteButton;	
   	
   }
      
   public boolean getShowViewAttachmentButton1() {
   	   
   	   return showViewAttachmentButton1;
   	
   }
   
   public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {
   	
   	   this.showViewAttachmentButton1 = showViewAttachmentButton1;
   	
   }
   
   public boolean getShowViewAttachmentButton2() {
   	   
   	   return showViewAttachmentButton2;
   	
   }
   
   public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {
   	
   	   this.showViewAttachmentButton2 = showViewAttachmentButton2;
   	
   }
   
   public boolean getShowViewAttachmentButton3() {
   	   
   	   return showViewAttachmentButton3;
   	
   }
   
   public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {
   	
   	   this.showViewAttachmentButton3 = showViewAttachmentButton3;
   	
   }
   
   public boolean getShowViewAttachmentButton4() {
   	   
   	   return showViewAttachmentButton4;
   	
   }
   
   public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {
   	
   	   this.showViewAttachmentButton4 = showViewAttachmentButton4;
   	
   }
   
   public String getIsSupplierEntered() {
   	
   	   return isSupplierEntered;
   	
   }
   
   public String getDefaultCheckDate() {
   	
   	  return defaultCheckDate;
   	
   }
   
   public void setDefaultCheckDate(String defaultCheckDate) {
   	
   	  this.defaultCheckDate = defaultCheckDate;
   	
   }
   
   public int getLineCount(){
   	
   	return lineCount;
   	
   }
   
   public void setLineCount(int lineCount){  	
   	
   	this.lineCount = lineCount;
   	
   }
   
   public int getMaxRows(){
   	
   	return maxRows;
   	
   }
   
   public void setMaxRows(int maxRows){  	
   	
   	this.maxRows = maxRows;
   	
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }

   public String getIsConversionDateEntered(){
   	
   		return isConversionDateEntered;
   	
   }

   public String getSupplierName() {

	   return supplierName;

   }

   public void setSupplierName(String supplierName) {

	   this.supplierName = supplierName;

   }
   
   public String getTempSupplierName() {

	   return tempSupplierName;

   }

   public void setTempSupplierName(String tempSupplierName) {

	   this.tempSupplierName = tempSupplierName;

   }
   
   public String getMemo() {
	   
	   return memo;
	   
   }
   
   public void setMemo(String memo) {
	   
	   this.memo = memo;
	   
   }
   
   public boolean getDisableFirstButton() {

	   return disableFirstButton;

   }

   public void setDisableFirstButton(boolean disableFirstButton) {

	   this.disableFirstButton = disableFirstButton;

   }

   public boolean getDisableLastButton() {

	   return disableLastButton;

   }

   public void setDisableLastButton(boolean disableLastButton) {

	   this.disableLastButton = disableLastButton;

   }
   
    public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       supplier = Constants.GLOBAL_BLANK;
       bankAccount = Constants.GLOBAL_BLANK;
	   date = null;
	   checkDate = null;
	   checkNumber = null;
	   documentNumber = null;
	   referenceNumber = null;
	   amount = null;   
	   checkVoid = false;
	   crossCheck = false;
	   description = null;
	   conversionDate = null;
	   conversionRate = null;
	   filename1 = null;
	   filename2 = null;
	   filename3 = null;
	   filename4 = null;
	   approvalStatus = null;
	   reasonForRejection = null;
	   posted = null;
	   voidApprovalStatus = null;
	   voidPosted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;
	   isSupplierEntered = null;
	   defaultCheckDate = null;
	   isConversionDateEntered = null;
	   supplierName = Constants.GLOBAL_BLANK;
	   tempSupplierName = null;
	   memo = null;
	   
	   // clear pay checkbox
	   
	   Iterator i = apCHKList.iterator();
	   
	   int ctr = 0;
	   
	   while (i.hasNext()) {
	   	
	   	   ApPaymentEntryList list = (ApPaymentEntryList)i.next();
	   	   
	   	   if (ctr >= lineCount && ctr <= lineCount + maxRows - 1) {
	   	   
	   	   	  list.setPayCheckbox(false);
	   	   	
	   	   }
	   	   
	   	   ctr++;
	   	   
	   }
	         
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null || 
         request.getParameter("cvPrintButton") != null ||
         request.getParameter("printButton") != null || 
         request.getParameter("journalButton") != null){
      	 
      	 if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
          	    showBatchName){
                errors.add("batchName",
                   new ActionMessage("paymentEntry.error.batchNameRequired"));
         }
      	
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("supplier",
               new ActionMessage("paymentEntry.error.supplierRequired"));
         }
         
         if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("bankAccount",
               new ActionMessage("paymentEntry.error.bankAccountRequired"));
         }
      	
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("paymentEntry.error.dateRequired"));
         }
         
      	 if(!Common.validateDateFormat(date)){
      		 errors.add("date", 
      				 new ActionMessage("paymentEntry.error.dateInvalid"));
      	 }

      	try{
			
			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			if(year<1900){
				errors.add("date", 
						new ActionMessage("paymentEntry.error.dateInvalid"));
			}
		} catch(Exception ex){
			
			errors.add("date", 
					new ActionMessage("paymentEntry.error.dateInvalid"));
			
		}
		 
		 if(Common.validateRequired(checkDate)){
            errors.add("checkDate",
               new ActionMessage("paymentEntry.error.checkDateRequired"));
         }
		 
		 if(!Common.validateDateFormat(checkDate)){
            errors.add("checkDate", 
	       new ActionMessage("paymentEntry.error.checkDateInvalid"));
	     }
		 
         if(Common.validateRequired(amount)){
            errors.add("amount",
               new ActionMessage("paymentEntry.error.amountRequired"));
         }
         
		 if(!Common.validateMoneyFormat(amount)){
	            errors.add("amount", 
		       new ActionMessage("paymentEntry.error.amountInvalid"));
		 }		 
		          
         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("paymentEntry.error.currencyRequired"));
         }
         
	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("paymentEntry.error.conversionDateInvalid"));
         }
         
	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("paymentEntry.error.conversionRateInvalid"));
         }
         
         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("paymentEntry.error.conversionDateMustBeNull"));
	 	 }
	 	 /*
		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){ 
		    errors.add("conversionRate",
		       new ActionMessage("paymentEntry.error.conversionRateMustBeNull"));
		 }
	*/
		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("paymentEntry.error.conversionRateOrDateMustBeEntered"));
		 }         
	 	         
         
         int numberOfLines = 0;
      	 
      	 Iterator i = apCHKList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 ApPaymentEntryList vpsList = (ApPaymentEntryList)i.next();      	 	 
      	 	       	 	 
      	 	 if (!vpsList.getPayCheckbox()) continue;
      	 	     
      	 	 numberOfLines++;
      	 
	         if(Common.validateRequired(vpsList.getApplyAmount())){
	            errors.add("applyAmount",
	               new ActionMessage("paymentEntry.error.applyAmountRequired", vpsList.getVoucherNumber() + "-" + vpsList.getInstallmentNumber()));
	         }
	         if(!Common.validateMoneyFormat(vpsList.getApplyAmount())){
	            errors.add("applyAmount",
	               new ActionMessage("paymentEntry.error.applyAmountInvalid", vpsList.getVoucherNumber() + "-" + vpsList.getInstallmentNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(vpsList.getDiscount())){
	            errors.add("discount",
	               new ActionMessage("paymentEntry.error.discountInvalid", vpsList.getVoucherNumber() + "-" + vpsList.getInstallmentNumber()));
	         }
	         if(!Common.validateMoneyFormat(vpsList.getTaxWithheld())){
	            errors.add("taxWithheld",
	               new ActionMessage("paymentEntry.error.taxWithheldInvalid", vpsList.getVoucherNumber() + "-" + vpsList.getInstallmentNumber()));
	         }
	         if(!Common.validateMoneyFormat(vpsList.getAllocatedCheckAmount())){
	            errors.add("allocatedCheckAmount",
	               new ActionMessage("paymentEntry.error.allocatedCheckAmountInvalid", vpsList.getVoucherNumber() + "-" + vpsList.getInstallmentNumber()));
	         }	
	         /*if((!currency.equals(functionalCurrency) || !vpsList.getCurrency().equals(functionalCurrency)) &&
	            (Common.validateRequired(vpsList.getAllocatedCheckAmount()) || Common.convertStringMoneyToDouble(vpsList.getAllocatedCheckAmount(), (short)6) == 0d)) {
	         	errors.add("currency",
	               new ActionMessage("paymentEntry.error.allocatedCheckAmountRequired", vpsList.getVoucherNumber() + "-" + vpsList.getInstallmentNumber()));
	         }	  
	         if((currency.equals(functionalCurrency) && vpsList.getCurrency().equals(functionalCurrency)) &&
	            (!Common.validateRequired(vpsList.getAllocatedCheckAmount()) && Common.convertStringMoneyToDouble(vpsList.getAllocatedCheckAmount(), (short)6) != 0d)) {
	         	errors.add("currency",
	               new ActionMessage("paymentEntry.error.allocatedCheckAmountMustNotBeEntered", vpsList.getVoucherNumber() + "-" + vpsList.getInstallmentNumber()));
	         }*/	

	         /* if vpsList.due date < date error
	         if(Common.convertStringToSQLDate(vpsList.getDueDate()).before(Common.convertStringToSQLDate(date))) {
	         
	         	errors.add("date",
	 	               new ActionMessage("paymentEntry.error.paymentDateMustBeLessThanDueDate", vpsList.getDueDate()));
	         	
	         }*/
		          
	    }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("supplier",
               new ActionMessage("paymentEntry.error.paymentMustHaveLine"));
         	
         }	  	    
	    	    	 
      } else if (!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
      	
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("supplier",
               new ActionMessage("paymentEntry.error.supplierRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){
      	
      	if(!Common.validateDateFormat(conversionDate)){
      		
      		errors.add("conversionDate", new ActionMessage("paymentEntry.error.conversionDateInvalid"));
      		
      	}
      	
      }     
      
      return(errors);	
   }
}
