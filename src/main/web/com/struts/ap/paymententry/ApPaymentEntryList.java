package com.struts.ap.paymententry;

import java.io.Serializable;

public class ApPaymentEntryList implements Serializable {

   private Integer voucherPaymentScheduleCode = null;
   private Integer voucherCode = null;
   private String voucherNumber = null;
   private String installmentNumber = null;
   private String dueDate = null;
   private String referenceNumber = null;
   private String voucherDescription = null;
   private String currency = null;
   private String amountDue = null;
   private String applyAmount = null;
   private String taxWithheld = null;
   private String allocatedCheckAmount = null;
   private String discount = null;
   private boolean payCheckbox = false;
   
   private String conversionDate = null;
   private String conversionRate = null;
   
   
   private ApPaymentEntryForm parentBean;
    
   public ApPaymentEntryList(ApPaymentEntryForm parentBean,
      Integer voucherPaymentScheduleCode,
	  Integer voucherCode,
      String voucherNumber,
      String installmentNumber,
      String dueDate,
      String referenceNumber,
	  String voucherDescription,
      String currency,
      String amountDue,
      String applyAmount,
      String taxWithheld,
      String discount,
      String allocatedCheckAmount
		   ){

      this.parentBean = parentBean;
      this.voucherPaymentScheduleCode = voucherPaymentScheduleCode;
      this.voucherCode = voucherCode;
      this.voucherNumber = voucherNumber;
      this.installmentNumber = installmentNumber;
      this.dueDate = dueDate;
      this.referenceNumber = referenceNumber;
      this.voucherDescription = voucherDescription;
      this.currency = currency;
      this.amountDue = amountDue;      
      this.applyAmount = applyAmount;
      this.taxWithheld = taxWithheld;
      this.discount = discount;
      this.allocatedCheckAmount = allocatedCheckAmount;
   }
         
   public Integer getVoucherPaymentScheduleCode() {
   	
      return voucherPaymentScheduleCode;
      
   }
   
   public Integer getVoucherCode() {
   	
      return voucherCode;
      
   }

   public String getVoucherNumber() {
   	
   	  return voucherNumber;
   	
   }
   
   public String getInstallmentNumber() {
   	
   	  return installmentNumber;
   	
   }
   
   public String getDueDate() {
   	  
   	  return dueDate;
   }
   
   
   public String getReferenceNumber() {
   	  
   	  return referenceNumber;
   	
   }
   
   public String getVoucherDescription() {
 	  
 	  return voucherDescription;
 	
   }
   
   public String getCurrency() {
   	 
   	  return currency;
   	
   }
   
   public String getAmountDue() {
   	 
   	   return amountDue;
   	
   }
   
   public String getApplyAmount() {
   	
   	   return applyAmount;
   	
   }
   
   public void setApplyAmount(String applyAmount) {
   	
   	   this.applyAmount = applyAmount;
   	
   }
   
   public String getTaxWithheld() {
   	
   	   return taxWithheld;
   	
   }
   
   public void setTaxWithheld(String taxWithheld) {
   	
   	   this.taxWithheld = taxWithheld;
   	
   }
   
   public String getDiscount() {
   	
   	   return discount;
   	
   }
   
   public void setDiscount(String discount) {
   	   
   	   this.discount = discount;
   	   
   }
      
   public String getAllocatedCheckAmount() {
   	
   	   return allocatedCheckAmount;
   	
   }
   
   public void setAllocatedCheckAmount(String allocatedCheckAmount) {
   	
   	   this.allocatedCheckAmount = allocatedCheckAmount;
   	
   }
   
   public boolean getPayCheckbox() {   	
   	  return payCheckbox;   	
   }
   
   public void setPayCheckbox(boolean payCheckbox) {
   	  this.payCheckbox = payCheckbox;
   }
   
   
   
   public String getConversionDate() {
	   	
   	   return conversionDate;
   	
   }
   
   public void setConversionDate(String conversionDate) {
   	
   	   this.conversionDate = conversionDate;
   	
   }
   
   public String getConversionRate() {
	   	
   	   return conversionRate;
   	
   }
   
   public void setConversionRate(String conversionRate) {
   	
   	   this.conversionRate = conversionRate;
   	
   }

}