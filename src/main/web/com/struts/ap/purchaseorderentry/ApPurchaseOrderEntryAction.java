package com.struts.ap.purchaseorderentry;

import com.ejb.exception.ApPONoBatchNameException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalSupplierItemInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.txn.ApPurchaseOrderEntryController;
import com.ejb.txn.ApPurchaseOrderEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.ap.receivingitementry.ApReceivingItemEntryTagList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModSupplierDetails;
import com.util.ApPurchaseOrderDetails;
import com.util.ApTaxCodeDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApPurchaseOrderEntryAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApPurchaseOrderEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ApPurchaseOrderEntryForm actionForm = (ApPurchaseOrderEntryForm)form;

         actionForm.setReport(null);
         actionForm.setAttachment(null);
         actionForm.setAttachmentPDF(null);
         actionForm.setAttachmentDownload(null);

	     String frParam = null;

         frParam = Common.getUserPermission(user, Constants.AP_PURCHASE_ORDER_ENTRY_ID);

         if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apPurchaseOrderEntry"));

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApPurchaseOrderEntryController EJB
*******************************************************/

         ApPurchaseOrderEntryControllerHome homePO = null;
         ApPurchaseOrderEntryController ejbPO = null;
    

         GlDailyRateControllerHome homeFR = null;
         GlDailyRateController ejbFR = null;
         
         InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;
         
         
         try {

            homePO = (ApPurchaseOrderEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApPurchaseOrderEntryControllerEJB", ApPurchaseOrderEntryControllerHome.class);

            homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
                    
           homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);

         } catch(NamingException e) {

         	if(log.isInfoEnabled()){
                log.info("NamingException caught in ApPurchaseOrderEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbPO = homePO.create();

            ejbFR = homeFR.create();
            
            ejbII = homeII.create();

         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in ApPurchaseOrderEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ApPurchaseOrderEntryController EJB
   getGlFcPrecisionUnit
   getInvGpQuantityPrecisionUnit
   getAdPrfApJournalLineNumber
   getAdPrfApUseSupplierPulldown
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

         short precisionUnit = 0;

         short journalLineNumber = 0;
         boolean isInitialPrinting = false;
         boolean useSupplierPulldown = true;
         boolean enablePOBatch = false;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/PO/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
         try {

         	precisionUnit = ejbPO.getGlFcPrecisionUnit(user.getCmpCode());
            journalLineNumber = ejbPO.getAdPrfApJournalLineNumber(user.getCmpCode());
            enablePOBatch = Common.convertByteToBoolean(ejbPO.getAdPrfEnableApPOBatch(user.getCmpCode()));

            System.out.println(enablePOBatch  +  " is enable");
            actionForm.setShowBatchName(enablePOBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbPO.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            actionForm.setPrecisionUnit(precisionUnit);

         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

/*******************************************************
   -- Ap PO Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

           details.setPoCode(actionForm.getPurchaseOrderCode());
           details.setPoDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setPoDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDeliveryPeriod()));
           details.setPoDocumentNumber(actionForm.getDocumentNumber());
           details.setPoReferenceNumber(actionForm.getReferenceNumber());
           details.setPoDescription(actionForm.getDescription());
           details.setPoVoid(Common.convertBooleanToByte(actionForm.getPurchaseOrderVoid()));
           details.setPoShipmentNumber(actionForm.getShipmentNumber());
           details.setPoBillTo(actionForm.getBillTo());
           details.setPoShipTo(actionForm.getShipTo());
           details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

           if (actionForm.getPurchaseOrderCode() == null) {

           	   details.setPoCreatedBy(user.getUserName());
	           details.setPoDateCreated(new java.util.Date());

           }




           details.setPoLastModifiedBy(user.getUserName());
           details.setPoDateLastModified(new java.util.Date());

           ArrayList plList = new ArrayList();

           for (int i = 0; i<actionForm.getApPOListSize(); i++) {

           	   ApPurchaseOrderEntryList apPOList = actionForm.getApPOByIndex(i);

           	   if (Common.validateRequired(apPOList.getLocation()) &&
           	   	   Common.validateRequired(apPOList.getItemName()) &&
				   Common.validateRequired(apPOList.getUnit()) &&
				   Common.validateRequired(apPOList.getUnitCost()) &&
				   Common.validateRequired(apPOList.getQuantity())) continue;

           	   ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();

           	   mdetails.setPlCode(apPOList.getPurchaseOrderLineCode());
           	   mdetails.setPlLine(Common.convertStringToShort(apPOList.getLineNumber()));
           	   mdetails.setPlIiName(apPOList.getItemName());
           	   mdetails.setPlIiDescription(apPOList.getItemDescription());
           	   mdetails.setPlLocName(apPOList.getLocation());
           	   mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apPOList.getQuantity(), precisionUnit));
           	   mdetails.setPlUomName(apPOList.getUnit());
           	   mdetails.setPlUnitCost(Common.convertStringMoneyToDouble(apPOList.getUnitCost(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())));
           	   mdetails.setPlQcNumber(apPOList.getQcNumber());


           	   mdetails.setPlQcExpiryDate(Common.convertStringToSQLDate(apPOList.getQcExpiryDate()));
           	   mdetails.setPlAmount(Common.convertStringMoneyToDouble(apPOList.getAmount(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())));
           	   mdetails.setPlDiscount1(Common.convertStringMoneyToDouble(apPOList.getDiscount1(), precisionUnit));
           	   mdetails.setPlDiscount2(Common.convertStringMoneyToDouble(apPOList.getDiscount2(), precisionUnit));
           	   mdetails.setPlDiscount3(Common.convertStringMoneyToDouble(apPOList.getDiscount3(), precisionUnit));
           	   mdetails.setPlDiscount4(Common.convertStringMoneyToDouble(apPOList.getDiscount4(), precisionUnit));
           	   mdetails.setPlTotalDiscount(Common.convertStringMoneyToDouble(apPOList.getTotalDiscount(), precisionUnit));

           	   boolean isTraceMisc = ejbPO.getInvTraceMisc(apPOList.getItemName(), user.getCmpCode());

			   String misc=apPOList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setTraceMisc((byte) 1);
	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
	     		   mdetails.setPlTagList(tagList);
	     	   }
           	   plList.add(mdetails);

           }

           try {
                if(actionForm.getShowBatchName()){
                    if(actionForm.getBatchName().equals(Constants.GLOBAL_NO_RECORD_FOUND) || actionForm.getBatchName()== null || actionForm.getBatchName().equals("")){
                        throw new ApPONoBatchNameException();
                    }

                    boolean notFound = true;
                    for(int x=0;x<actionForm.getBatchNameList().size();x++){
                        if(actionForm.getBatchName().equals(actionForm.getBatchNameList().get(x).toString())){
                            notFound = false;
                            break;
                        }
                    }

                    if(notFound){
                        throw new ApPONoBatchNameException();
                    }
                }
                Integer purchaseOrderCode = ejbPO.saveApPoEntry(details, actionForm.getBatchName(), actionForm.getPaymentTerm(),
                    actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getSupplier(), plList, true, actionForm.getValidate(),
                    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                actionForm.setPurchaseOrderCode(purchaseOrderCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.recordAlreadyDeleted"));

           } catch (ApPONoBatchNameException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noBatchName"));

           }catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyVoid"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPosted"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noApprovalApproverFound"));

           } catch (GlobalSupplierItemInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.supplierNotValidForItem", ex.getMessage()));

           } catch (GlobalRecordInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.recordInvalid"));

           }catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apPurchaseOrderEntry"));

	       }
           //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));


	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));



	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));



	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));



	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   	}

// save attachment

	   	 if (!Common.validateRequired(filename1)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename1().getInputStream();

    	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();


    	        	if(attachmentFileExtension==null) {
    	        		System.out.println("attachmentFileExtension is null");
    	        	}
    	        	if(fileExtension==null) {
    	        		System.out.println("fileExtension is null");

    	        	}
    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}


    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-1.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-1-" + filename1);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-1-" + filename1);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename2().getInputStream();

    	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();

    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}

    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-2.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-2-" + filename2);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-2-" + filename2);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename3().getInputStream();

    	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();

    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}


    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-3.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-3-" + filename3);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-3-" + filename3);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename4().getInputStream();

    	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();

    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}


    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-4.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-4-" + filename4);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-4-" + filename4);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }
/*******************************************************
   -- Ap PO Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           	ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

           	details.setPoCode(actionForm.getPurchaseOrderCode());
           	details.setPoDate(Common.convertStringToSQLDate(actionForm.getDate()));
           	details.setPoDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDeliveryPeriod()));
           	details.setPoDocumentNumber(actionForm.getDocumentNumber());
           	details.setPoReferenceNumber(actionForm.getReferenceNumber());
           	details.setPoDescription(actionForm.getDescription());
           	details.setPoVoid(Common.convertBooleanToByte(actionForm.getPurchaseOrderVoid()));
           	details.setPoShipmentNumber(actionForm.getShipmentNumber());
           	details.setPoBillTo(actionForm.getBillTo());
           	details.setPoShipTo(actionForm.getShipTo());
           	details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           	details.setPoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

           	if (actionForm.getPurchaseOrderCode() == null) {

           		details.setPoCreatedBy(user.getUserName());
           		details.setPoDateCreated(new java.util.Date());

           	}

           	details.setPoLastModifiedBy(user.getUserName());
           	details.setPoDateLastModified(new java.util.Date());

           	ArrayList plList = new ArrayList();

           	for (int i = 0; i<actionForm.getApPOListSize(); i++) {

           		ApPurchaseOrderEntryList apPOList = actionForm.getApPOByIndex(i);

           		if (Common.validateRequired(apPOList.getLocation()) &&
           				Common.validateRequired(apPOList.getItemName()) &&
						Common.validateRequired(apPOList.getUnit()) &&
						Common.validateRequired(apPOList.getUnitCost()) &&
						Common.validateRequired(apPOList.getQuantity())) continue;

           		ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();

           		mdetails.setPlCode(apPOList.getPurchaseOrderLineCode());
           		mdetails.setPlLine(Common.convertStringToShort(apPOList.getLineNumber()));
           		mdetails.setPlIiName(apPOList.getItemName());
           		mdetails.setPlIiDescription(apPOList.getItemDescription());
           		mdetails.setPlLocName(apPOList.getLocation());
           		mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apPOList.getQuantity(), precisionUnit));
           		mdetails.setPlUomName(apPOList.getUnit());
           		mdetails.setPlUnitCost(Common.convertStringMoneyToDouble(apPOList.getUnitCost(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())));
           		mdetails.setPlQcNumber(apPOList.getQcNumber());
           		mdetails.setPlQcExpiryDate(Common.convertStringToSQLDate(apPOList.getQcExpiryDate()));
           		mdetails.setPlAmount(Common.convertStringMoneyToDouble(apPOList.getAmount(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())));
           		mdetails.setPlDiscount1(Common.convertStringMoneyToDouble(apPOList.getDiscount1(), precisionUnit));
           		mdetails.setPlDiscount2(Common.convertStringMoneyToDouble(apPOList.getDiscount2(), precisionUnit));
           		mdetails.setPlDiscount3(Common.convertStringMoneyToDouble(apPOList.getDiscount3(), precisionUnit));
           		mdetails.setPlDiscount4(Common.convertStringMoneyToDouble(apPOList.getDiscount4(), precisionUnit));
           		mdetails.setPlTotalDiscount(Common.convertStringMoneyToDouble(apPOList.getTotalDiscount(), precisionUnit));





            	   //TODO:save and submit taglist
           	   boolean isTraceMisc = ejbPO.getInvTraceMisc(apPOList.getItemName(), user.getCmpCode());

			   String misc=apPOList.getMisc();

			   System.out.println("misc to submit: " + misc);

	     	   if (isTraceMisc){
	     		   mdetails.setTraceMisc((byte) 1);
	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
	     		   System.out.println("size taglist from action: " + tagList.size());

	     		   mdetails.setPlTagList(tagList);
	     	   }

           		plList.add(mdetails);

           	}
           	//validate attachment
            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

            if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   	}
           	try {


           		if(actionForm.getShowBatchName()){
                            if(actionForm.getBatchName().equals(Constants.GLOBAL_NO_RECORD_FOUND) || actionForm.getBatchName()== null || actionForm.getBatchName().equals("")){
                                throw new ApPONoBatchNameException();
                            }

                              boolean notFound = true;
                            for(int x=0;x<actionForm.getBatchNameList().size();x++){
                            if(actionForm.getBatchName().equals(actionForm.getBatchNameList().get(x).toString())){
                                notFound = false;
                                break;
                            }
                    }

                    if(notFound){
                        throw new ApPONoBatchNameException();
                    }
                        }
           		Integer purchaseOrderCode = ejbPO.saveApPoEntry(details, actionForm.getBatchName(), actionForm.getPaymentTerm(),
           				actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getSupplier(), plList, false, actionForm.getValidate(),
           				new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           		actionForm.setPurchaseOrderCode(purchaseOrderCode);

           	} catch (GlobalRecordAlreadyDeletedException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.recordAlreadyDeleted"));

           	} catch (ApPONoBatchNameException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noBatchName"));

                } catch (GlobalDocumentNumberNotUniqueException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.documentNumberNotUnique"));

           	} catch (GlobalConversionDateNotExistException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.conversionDateNotExist"));

           	} catch (GlobalPaymentTermInvalidException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.paymentTermInvalid"));

           	} catch (GlobalTransactionAlreadyVoidException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyVoid"));

           	} catch (GlobalInvItemLocationNotFoundException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.noItemLocationFound", ex.getMessage()));

           	} catch (GlobalTransactionAlreadyApprovedException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyApproved"));

           	} catch (GlobalTransactionAlreadyPendingException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPending"));

           	} catch (GlobalTransactionAlreadyPostedException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPosted"));

           	} catch (GlobalNoApprovalRequesterFoundException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("purchaseOrderEntry.error.noApprovalRequesterFound"));

            } catch (GlobalNoApprovalApproverFoundException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("purchaseOrderEntry.error.noApprovalApproverFound"));

           	} catch (GlobalSupplierItemInvalidException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("purchaseOrderEntry.error.supplierNotValidForItem", ex.getMessage()));

           	} catch (GlobalRecordInvalidException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("purchaseOrderEntry.error.recordInvalid"));

            } catch (EJBException ex) {
           		if (log.isInfoEnabled()) {

           			log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
           					" session: " + session.getId());
           		}

           		return(mapping.findForward("cmnErrorPage"));
           	}

           	if (!errors.isEmpty()) {

           		saveErrors(request, new ActionMessages(errors));
           		return (mapping.findForward("apPurchaseOrderEntry"));

           	}
         // save attachment

            if (!Common.validateRequired(filename1)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename1().getInputStream();

    	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();


    	        	if(attachmentFileExtension==null) {
    	        		System.out.println("attachmentFileExtension is null");
    	        	}
    	        	if(fileExtension==null) {
    	        		System.out.println("fileExtension is null");

    	        	}
    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}


    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-1.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-1-" + filename1);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-1-" + filename1);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename2().getInputStream();

    	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();

    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}

    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-2.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-2-" + filename2);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-2-" + filename2);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename3().getInputStream();

    	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();

    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}


    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-3.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-3-" + filename3);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-3-" + filename3);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename4().getInputStream();

    	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

    	        	new File(attachmentPath).mkdirs();

    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	    			fileExtension = attachmentFileExtension;

        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	    		fileExtension = attachmentFileExtensionPDF;

        	    	}


    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-4.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(actionForm.getPurchaseOrderCode() + "-4-" + filename4);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-4-" + filename4);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

/*******************************************************
	 -- Ap PO Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

           	ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

           	details.setPoCode(actionForm.getPurchaseOrderCode());
           	details.setPoDate(Common.convertStringToSQLDate(actionForm.getDate()));
           	details.setPoDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDeliveryPeriod()));
           	details.setPoDocumentNumber(actionForm.getDocumentNumber());
           	details.setPoReferenceNumber(actionForm.getReferenceNumber());
           	details.setPoDescription(actionForm.getDescription());
           	details.setPoVoid(Common.convertBooleanToByte(actionForm.getPurchaseOrderVoid()));
           	details.setPoShipmentNumber(actionForm.getShipmentNumber());
           	details.setPoBillTo(actionForm.getBillTo());
           	details.setPoShipTo(actionForm.getShipTo());

           	details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           	details.setPoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

           	if (actionForm.getPurchaseOrderCode() == null) {

           		details.setPoCreatedBy(user.getUserName());
           		details.setPoDateCreated(new java.util.Date());

           	}

           	details.setPoLastModifiedBy(user.getUserName());
           	details.setPoDateLastModified(new java.util.Date());

           	ArrayList plList = new ArrayList();

           	for (int i = 0; i<actionForm.getApPOListSize(); i++) {

           		ApPurchaseOrderEntryList apPOList = actionForm.getApPOByIndex(i);

           		if (Common.validateRequired(apPOList.getLocation()) &&
           				Common.validateRequired(apPOList.getItemName()) &&
						Common.validateRequired(apPOList.getUnit()) &&
						Common.validateRequired(apPOList.getUnitCost()) &&
						Common.validateRequired(apPOList.getQuantity())) continue;

           		ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();

           		mdetails.setPlCode(apPOList.getPurchaseOrderLineCode());
           		mdetails.setPlLine(Common.convertStringToShort(apPOList.getLineNumber()));
           		mdetails.setPlIiName(apPOList.getItemName());
           		mdetails.setPlIiDescription(apPOList.getItemDescription());
           		mdetails.setPlLocName(apPOList.getLocation());
           		mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apPOList.getQuantity(), precisionUnit));
           		mdetails.setPlUomName(apPOList.getUnit());
           		mdetails.setPlUnitCost(Common.convertStringMoneyToDouble(apPOList.getUnitCost(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())));
           		mdetails.setPlQcNumber(apPOList.getQcNumber());
           		mdetails.setPlQcExpiryDate(Common.convertStringToSQLDate(apPOList.getQcExpiryDate()));
           		mdetails.setPlAmount(Common.convertStringMoneyToDouble(apPOList.getAmount(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())));
           		mdetails.setPlDiscount1(Common.convertStringMoneyToDouble(apPOList.getDiscount1(), precisionUnit));
           		mdetails.setPlDiscount2(Common.convertStringMoneyToDouble(apPOList.getDiscount2(), precisionUnit));
           		mdetails.setPlDiscount3(Common.convertStringMoneyToDouble(apPOList.getDiscount3(), precisionUnit));
           		mdetails.setPlDiscount4(Common.convertStringMoneyToDouble(apPOList.getDiscount4(), precisionUnit));
           		mdetails.setPlTotalDiscount(Common.convertStringMoneyToDouble(apPOList.getTotalDiscount(), precisionUnit));

           	  boolean isTraceMisc = ejbPO.getInvTraceMisc(apPOList.getItemName(), user.getCmpCode());

			   String misc=apPOList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setTraceMisc((byte) 1);
	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
	     		   mdetails.setPlTagList(tagList);
	     	   }

           		plList.add(mdetails);

           	}
           	//validate attachment
            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

            if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("purchaseOrderEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("purchaseOrderEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apPurchaseOrderEntry"));

	   	    	}

	   	   	}

           try {

                if(actionForm.getShowBatchName()){
                    if(actionForm.getBatchName().equals(Constants.GLOBAL_NO_RECORD_FOUND) || actionForm.getBatchName()== null || actionForm.getBatchName().equals("")){
                        throw new ApPONoBatchNameException();
                    }

                      boolean notFound = true;
                    for(int x=0;x<actionForm.getBatchNameList().size();x++){
                        if(actionForm.getBatchName().equals(actionForm.getBatchNameList().get(x).toString())){
                            notFound = false;
                            break;
                        }
                    }

                    if(notFound){
                        throw new ApPONoBatchNameException();
                    }
                }
                Integer purchaseOrderCode = ejbPO.saveApPoEntry(details, actionForm.getBatchName(), actionForm.getPaymentTerm(),
                    actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getSupplier(), plList, true, actionForm.getValidate(),
                    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                actionForm.setPurchaseOrderCode(purchaseOrderCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.documentNumberNotUnique"));

           } catch (ApPONoBatchNameException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noBatchName"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyVoid"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.transactionAlreadyPosted"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noApprovalApproverFound"));

           } catch (GlobalSupplierItemInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.supplierNotValidForItem", ex.getMessage()));

           } catch (GlobalRecordInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.recordInvalid"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));

           }

	           if (!errors.isEmpty()) {

		    	   saveErrors(request, new ActionMessages(errors));
		   		   return (mapping.findForward("apPurchaseOrderEntry"));

		       }

		       actionForm.setReport(Constants.STATUS_SUCCESS);

	           isInitialPrinting = true;
	        // save attachment

	           if (!Common.validateRequired(filename1)) {

	    	        if (errors.isEmpty()) {

	    	        	InputStream is = actionForm.getFilename1().getInputStream();

	    	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	    	        	new File(attachmentPath).mkdirs();


	    	        	if(attachmentFileExtension==null) {
	    	        		System.out.println("attachmentFileExtension is null");
	    	        	}
	    	        	if(fileExtension==null) {
	    	        		System.out.println("fileExtension is null");

	    	        	}
	    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	    			fileExtension = attachmentFileExtension;

	        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	        	    		fileExtension = attachmentFileExtensionPDF;

	        	    	}


	    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-1.txt");
	    	        	FileWriter writer = new FileWriter(fileTest);
	    	            writer.write(actionForm.getPurchaseOrderCode() + "-1-" + filename1);
	    	            writer.close();

	    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-1-" + filename1);

	    	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	    	        }

	    	   }

	    	   if (!Common.validateRequired(filename2)) {

	    	        if (errors.isEmpty()) {

	    	        	InputStream is = actionForm.getFilename2().getInputStream();

	    	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	    	        	new File(attachmentPath).mkdirs();

	    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	    			fileExtension = attachmentFileExtension;

	        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	        	    		fileExtension = attachmentFileExtensionPDF;

	        	    	}

	    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-2.txt");
	    	        	FileWriter writer = new FileWriter(fileTest);
	    	            writer.write(actionForm.getPurchaseOrderCode() + "-2-" + filename2);
	    	            writer.close();

	    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-2-" + filename2);

	    	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	    	        }

	    	   }

	    	   if (!Common.validateRequired(filename3)) {

	    	        if (errors.isEmpty()) {

	    	        	InputStream is = actionForm.getFilename3().getInputStream();

	    	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	    	        	new File(attachmentPath).mkdirs();

	    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	    			fileExtension = attachmentFileExtension;

	        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	        	    		fileExtension = attachmentFileExtensionPDF;

	        	    	}


	    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-3.txt");
	    	        	FileWriter writer = new FileWriter(fileTest);
	    	            writer.write(actionForm.getPurchaseOrderCode() + "-3-" + filename3);
	    	            writer.close();

	    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-3-" + filename3);

	    	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	    	        }

	    	   }

	    	   if (!Common.validateRequired(filename4)) {

	    	        if (errors.isEmpty()) {

	    	        	InputStream is = actionForm.getFilename4().getInputStream();

	    	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	    	        	new File(attachmentPath).mkdirs();

	    	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	    			fileExtension = attachmentFileExtension;

	        	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	        	    		fileExtension = attachmentFileExtensionPDF;

	        	    	}


	    	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-4.txt");
	    	        	FileWriter writer = new FileWriter(fileTest);
	    	            writer.write(actionForm.getPurchaseOrderCode() + "-4-" + filename4);
	    	            writer.close();

	    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseOrderCode() + "-4-" + filename4);

	    	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	    	        }

	    	   }


	        }  else {

	        	actionForm.setReport(Constants.STATUS_SUCCESS);

		        	return(mapping.findForward("apPurchaseOrderEntry"));

	        }

/*******************************************************
   -- AP PO Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbPO.deleteApPoEntry(actionForm.getPurchaseOrderCode(), user.getUserName(), user.getCmpCode());

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());

                }

                return(mapping.findForward("cmnErrorPage"));

            }

/*******************************************************
   -- Ap PO Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap PO Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {

         	int listSize = actionForm.getApPOListSize();

            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	ApPurchaseOrderEntryList apPOList = new ApPurchaseOrderEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null, null, null, null, null, "0.0","0.0", "0.0", "0.0", "0.0", "0.0", null);

	        	apPOList.setLocationList(actionForm.getLocationList());

	        	actionForm.saveApPOList(apPOList);

	        }

	        return(mapping.findForward("apPurchaseOrderEntry"));

/*******************************************************
   -- Ap PO Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {

         	for (int i = 0; i<actionForm.getApPOListSize(); i++) {

           	   ApPurchaseOrderEntryList apPOList = actionForm.getApPOByIndex(i);

           	   if (apPOList.getDeleteCheckbox()) {

           	   	   actionForm.deleteApPOList(i);
           	   	   i--;
           	   }

            }

            for (int i = 0; i<actionForm.getApPOListSize(); i++) {

           	   ApPurchaseOrderEntryList apPOList = actionForm.getApPOByIndex(i);

           	   apPOList.setLineNumber(String.valueOf(i+1));

            }

	        return(mapping.findForward("apPurchaseOrderEntry"));

/*******************************************************
   -- Ap PO Supplier Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {

         	try {

                 ApModSupplierDetails mdetails = ejbPO.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());

                 actionForm.setPaymentTerm(mdetails.getSplPytName());
                 actionForm.setTaxCode(mdetails.getSplScTcName());
                 actionForm.setTaxRate(mdetails.getSplScTcRate());
                 actionForm.setTaxType(mdetails.getSplScTcType());
                 actionForm.setSupplierName(mdetails.getSplName());

                 actionForm.clearApPOList();

                 for (int x = 1; x <= journalLineNumber; x++) {

                 	ApPurchaseOrderEntryList apPOList = new ApPurchaseOrderEntryList(actionForm,
                 			null, new Integer(x).toString(), null, null, null, null, null,
							null, null, null, "0.0","0.0", "0.0", "0.0", "0.0", null, null, null);

                 	apPOList.setLocationList(actionForm.getLocationList());
                 	apPOList.setUnitList(Constants.GLOBAL_BLANK);
                 	apPOList.setUnitList("Select Item First");
                 	/*
                 	apPOList.clearTagList();
            		for (int y=0; y<=10; y++) {

            			ApPurchaseOrderEntryTagList apTagList = new ApPurchaseOrderEntryTagList(apPOList, "", "", "", "", "");

            			apPOList.saveTagList(apTagList);
            		}*/
                 	actionForm.saveApPOList(apPOList);

                 }

             } catch (GlobalNoRecordFoundException ex) {

             	actionForm.clearApPOList();

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

             return(mapping.findForward("apPurchaseOrderEntry"));

/*******************************************************
   -- Ap PO Item Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("apPOList[" +
         		actionForm.getRowSelected() + "].isItemEntered"))) {

         	ApPurchaseOrderEntryList apPOList =
         		actionForm.getApPOByIndex(actionForm.getRowSelected());

         	try {

         		// populate unit field class

         		ArrayList uomList = new ArrayList();
         		uomList = ejbPO.getInvUomByIiName(apPOList.getItemName(), user.getCmpCode());

         		apPOList.clearUnitList();
         		apPOList.setUnitList(Constants.GLOBAL_BLANK);

         		Iterator i = uomList.iterator();
         		while (i.hasNext()) {

         			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

         			apPOList.setUnitList(mUomDetails.getUomName());

         			if (mUomDetails.isDefault()) {

         				apPOList.setUnit(mUomDetails.getUomName());

         			}

         		}
         		//TODO: populate taglist with empty values
         		apPOList.clearTagList();


         		boolean isTraceMisc = ejbPO.getInvTraceMisc(apPOList.getItemName(), user.getCmpCode());


         		apPOList.setIsTraceMisc(isTraceMisc);
          		System.out.println(isTraceMisc + "<== trace misc item enter action");
          		if (isTraceMisc == true){
          			apPOList.clearTagList();

          			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), apPOList.getQuantity());


          			apPOList.setMisc(misc);

          		}

         		// populate unit cost field

         		if (!Common.validateRequired(apPOList.getItemName()) && !Common.validateRequired(apPOList.getUnit())) {
            
                    
         			double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apPOList.getItemName(), apPOList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
         			
         			unitCost = Common.currencyConversion(unitCost, Common.convertStringToDouble(actionForm.getConversionRate()), precisionUnit);
             
         			
         			apPOList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())));

         		}

	    		apPOList.setDiscount1("0.0");
	    		apPOList.setDiscount2("0.0");
	    		apPOList.setDiscount3("0.0");
	    		apPOList.setDiscount4("0.0");
	         	apPOList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));

		        apPOList.setQuantity(Common.convertDoubleToStringMoney(1d, precisionUnit));
	    		apPOList.setAmount(apPOList.getUnitCost());

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apPurchaseOrderEntry"));
/*******************************************************
    -- Ap PO View Attachment Action --
*******************************************************/

          } else if(request.getParameter("viewAttachmentButton1") != null) {

        	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-1.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals(attachmentFileExtension)) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}
	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apPurchaseOrderEntry"));

	         } else {

	           return (mapping.findForward("apPurchaseOrderEntryChild"));

	        }



	      } else if(request.getParameter("viewAttachmentButton2") != null) {

	    	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-2.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals(attachmentFileExtension)) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}


	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apPurchaseOrderEntry"));

	         } else {

	           return (mapping.findForward("apPurchaseOrderEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton3") != null) {

	    	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-3.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals(attachmentFileExtension)) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apPurchaseOrderEntry"));

	         } else {

	           return (mapping.findForward("apPurchaseOrderEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton4") != null) {

	    	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-4.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals(attachmentFileExtension)) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}
	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apPurchaseOrderEntry"));

	         } else {

	           return (mapping.findForward("apPurchaseOrderEntryChild"));

	        }
/*******************************************************
   -- Ap PO Unit Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("apPOList[" +
         		actionForm.getRowSelected() + "].isUnitEntered"))) {

         	ApPurchaseOrderEntryList apPOList =
         		actionForm.getApPOByIndex(actionForm.getRowSelected());

         	try {

         		// populate unit cost field

         		if (!Common.validateRequired(apPOList.getLocation()) && !Common.validateRequired(apPOList.getItemName())) {

         			//double unitCost = ejbPO.getInvIiUnitCostByIiNameAndUomName(apPOList.getItemName(), apPOList.getUnit(), user.getCmpCode());
         			
         			
         			double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apPOList.getItemName(), apPOList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
       
         			apPOList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));


         		}

         		// populate amount field
         		double amount = 0d;

         		if(!Common.validateRequired(apPOList.getQuantity()) && !Common.validateRequired(apPOList.getUnitCost())) {

         			amount = Common.convertStringMoneyToDouble(apPOList.getQuantity(), precisionUnit) *
						Common.convertStringMoneyToDouble(apPOList.getUnitCost(), precisionUnit);

         			apPOList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

         		}

	      		// populate discount field

	      		if (!Common.validateRequired(apPOList.getAmount()) &&
	      			(!Common.validateRequired(apPOList.getDiscount1()) ||
					!Common.validateRequired(apPOList.getDiscount2()) ||
					!Common.validateRequired(apPOList.getDiscount3()) ||
					!Common.validateRequired(apPOList.getDiscount4()))) {

	      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

	      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

	      			}

	      			double discountRate1 = Common.convertStringMoneyToDouble(apPOList.getDiscount1(), precisionUnit)/100;
	      			double discountRate2 = Common.convertStringMoneyToDouble(apPOList.getDiscount2(), precisionUnit)/100;
	      			double discountRate3 = Common.convertStringMoneyToDouble(apPOList.getDiscount3(), precisionUnit)/100;
	      			double discountRate4 = Common.convertStringMoneyToDouble(apPOList.getDiscount4(), precisionUnit)/100;
	      			double totalDiscountAmount = 0d;

	      			if (discountRate1 > 0) {

	      				double discountAmount = amount * discountRate1;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate2 > 0) {

	      				double discountAmount = amount * discountRate2;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate3 > 0) {

	      				double discountAmount = amount * discountRate3;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate4 > 0) {

	      				double discountAmount = amount * discountRate4;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

	      				amount += amount * (actionForm.getTaxRate() / 100);

	      			}

	      			apPOList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
	      			apPOList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	      		}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apPurchaseOrderEntry"));

/*******************************************************
   -- Ap PO Tax Code Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {

         	try {

         		ApTaxCodeDetails details = ejbPO.getApTcByTcName(actionForm.getTaxCode(), user.getCmpCode());
         		actionForm.setTaxRate(details.getTcRate());
         		actionForm.setTaxType(details.getTcType());

         		actionForm.clearApPOList();

         		for (int x = 1; x <= journalLineNumber; x++) {

         			ApPurchaseOrderEntryList apPOList = new ApPurchaseOrderEntryList(actionForm,
         					null, new Integer(x).toString(), null, null, null, null, null,
							null, null, null, "0.0","0.0", "0.0", "0.0", "0.0", null, null, null);

         			apPOList.setLocationList(actionForm.getLocationList());
         			apPOList.setUnitList(Constants.GLOBAL_BLANK);
         			apPOList.setUnitList("Select Item First");

         			actionForm.saveApPOList(apPOList);

         		}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apPurchaseOrderEntry"));



/*******************************************************
    -- Conversion Date Enter Action --
 *******************************************************/
      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

      	try {

      		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
      				ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
      						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

      	} catch (GlobalConversionDateNotExistException ex) {

      		errors.add(ActionMessages.GLOBAL_MESSAGE,
      				new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

      	} catch (EJBException ex) {

      		if (log.isInfoEnabled()) {

      			log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());

      		}

      		return(mapping.findForward("cmnErrorPage"));

      	}

      	if (!errors.isEmpty()) {

      		saveErrors(request, new ActionMessages(errors));

      	}

      	return(mapping.findForward("apPurchaseOrderEntry"));



/*******************************************************
   -- Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

         	try {

         		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
         		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));

      
                 for (int i = 0; i<actionForm.getApPOListSize(); i++) {
                   
                    ApPurchaseOrderEntryList apPOList = actionForm.getApPOByIndex(i);
                    
                    if(Common.validateRequired(apPOList.getItemName())){
                         continue;
                    }
                
                    double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apPOList.getItemName(), apPOList.getUnit(), Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                    
                    unitCost = Common.currencyConversion(unitCost, Common.convertStringToDouble(actionForm.getConversionRate()), precisionUnit);
                    
                    apPOList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit)); 
                    
                    apPOList.setAmount(Common.convertDoubleToStringMoney(unitCost *  Common.convertStringToDouble(apPOList.getQuantity()), precisionUnit) ); 
                
                
                
                 }
                
    





         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("purchaseOrderEntry.error.conversionDateNotExist"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));

         	}

         	return(mapping.findForward("apPurchaseOrderEntry"));

         }

/*******************************************************
   -- Ap PO Load Action --
*******************************************************/

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apPurchaseOrderEntry"));

            }

            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }

            try {

            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearCurrencyList();

            	list = ejbPO.getGlFcAllWithDefault(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

            			actionForm.setCurrencyList(mFcDetails.getFcName());

            			if (mFcDetails.getFcSob() == 1) {

            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());

            			}

            		}

            	}

            	actionForm.clearPaymentTermList();

            	list = ejbPO.getAdPytAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setPaymentTermList((String)i.next());

            		}

            		actionForm.setPaymentTerm("IMMEDIATE");

            	}

            	actionForm.clearTaxCodeList();

            	list = ejbPO.getApTcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setTaxCodeList((String)i.next());

            		}

            	}

            	actionForm.clearUserList();

            	ArrayList userList = ejbPO.getAdUsrAll(user.getCmpCode());

            	if (userList == null || userList.size() == 0) {

            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator x = userList.iterator();

            		while (x.hasNext()) {

            			actionForm.setUserList((String)x.next());

            		}

            	}
            	if(actionForm.getUseSupplierPulldown()) {

            		actionForm.clearSupplierList();

            		list = ejbPO.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		if (list == null || list.size() == 0) {

            			actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {

            			i = list.iterator();

            			while (i.hasNext()) {

            				actionForm.setSupplierList((String)i.next());

            			}

            		}

            	}



            	actionForm.clearBatchNameList();
            	System.out.println("DEPARTMENT---------->"+user.getUserDepartment());
            	list = ejbPO.getApOpenVbAll(user.getUserDepartment() == null ? "" :user.getUserDepartment(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBatchNameList((String)i.next());

            		}

            	}

            	actionForm.clearLocationList();

            	list = ejbPO.getInvLocAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setLocationList((String)i.next());

            		}

            	}

            	actionForm.clearApPOList();

            	if (request.getParameter("forward") != null || isInitialPrinting) {

            		if (request.getParameter("forward") != null) {

            			actionForm.setPurchaseOrderCode(new Integer(request.getParameter("purchaseOrderCode")));

            		}

            		ApModPurchaseOrderDetails mdetails = ejbPO.getApPoByPoCode(actionForm.getPurchaseOrderCode(), user.getCmpCode());

            		actionForm.setBatchName(mdetails.getPoBatchName());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getPoDate()));
            		actionForm.setDeliveryPeriod(Common.convertSQLDateToString(mdetails.getPoDeliveryPeriod()));
            		actionForm.setDocumentNumber(mdetails.getPoDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getPoReferenceNumber());
            		actionForm.setDescription(mdetails.getPoDescription());
            		actionForm.setPurchaseOrderVoid(Common.convertByteToBoolean(mdetails.getPoVoid()));
            		actionForm.setShipmentNumber(mdetails.getPoShipmentNumber());
            		actionForm.setBillTo(mdetails.getPoBillTo());
            		actionForm.setShipTo(mdetails.getPoShipTo());
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getPoConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getPoConversionRate(), Constants.CONVERSION_RATE_PRECISION));
            		actionForm.setApprovalStatus(mdetails.getPoApprovalStatus());
         			actionForm.setReasonForRejection(mdetails.getPoReasonForRejection());
         			actionForm.setPosted(mdetails.getPoPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getPoCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getPoDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getPoLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getPoDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getPoApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getPoDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getPoPostedBy());
         			actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getPoDatePosted()));

         			if(!actionForm.getCurrencyList().contains(mdetails.getPoFcName())) {

         				if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearCurrencyList();

            			}
         				actionForm.setCurrencyList(mdetails.getPoFcName());

         			}
            		actionForm.setCurrency(mdetails.getPoFcName());

            		if (!actionForm.getPaymentTermList().contains(mdetails.getPoPytName())) {

            			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearPaymentTermList();

            			}
            			actionForm.setPaymentTermList(mdetails.getPoPytName());

            		}
            		actionForm.setPaymentTerm(mdetails.getPoPytName());

            		if (!actionForm.getTaxCodeList().contains(mdetails.getPoTcName())) {

            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearTaxCodeList();

            			}
            			actionForm.setTaxCodeList(mdetails.getPoTcName());

            		}
            		actionForm.setTaxCode(mdetails.getPoTcName());
              		actionForm.setTaxType(mdetails.getPoTcType());
              		actionForm.setTaxRate(mdetails.getPoTcRate());

            		if (!actionForm.getSupplierList().contains(mdetails.getPoSplSupplierCode())) {

            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearSupplierList();

            			}
            			actionForm.setSupplierList(mdetails.getPoSplSupplierCode());

            		}
            		actionForm.setSupplier(mdetails.getPoSplSupplierCode());
            		actionForm.setSupplierName(mdetails.getPoSplName());

        		    list = mdetails.getPoPlList();

        			i = list.iterator();

        			while (i.hasNext()) {

        				ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails)i.next();

        				String fixedDiscountAmount = "0.00";

                        if((mPlDetails.getPlDiscount1() + mPlDetails.getPlDiscount2() +
                        		mPlDetails.getPlDiscount3() + mPlDetails.getPlDiscount4()) == 0)
                       	 fixedDiscountAmount = Common.convertDoubleToStringMoney(mPlDetails.getPlTotalDiscount(), precisionUnit);

        				ApPurchaseOrderEntryList apPlList = new ApPurchaseOrderEntryList(actionForm,
        						mPlDetails.getPlCode(),
								Common.convertShortToString(mPlDetails.getPlLine()),
								mPlDetails.getPlLocName(),
								mPlDetails.getPlIiName(),
								mPlDetails.getPlIiDescription(),
								Common.convertDoubleToStringMoney(mPlDetails.getPlQuantity(), precisionUnit),
								mPlDetails.getPlUomName(),
								Common.convertDoubleToStringMoney(mPlDetails.getPlUnitCost(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())),
								Common.convertDoubleToStringMoney(mPlDetails.getPlAmount(), ejbPO.getInvGpCostPrecisionUnit(user.getCmpCode())),
								mPlDetails.getPlQcNumber(),
								Common.convertSQLDateToString(mPlDetails.getPlQcExpiryDate()),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount1(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount2(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount3(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount4(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlTotalDiscount(), precisionUnit),
								fixedDiscountAmount, mPlDetails.getPlMisc());

        				apPlList.setLocationList(actionForm.getLocationList());
        				apPlList.clearTagList();


        				boolean isTraceMisc = ejbPO.getInvTraceMisc(mPlDetails.getPlIiName(), user.getCmpCode());
        				System.out.println("is tm: " + isTraceMisc);
        				apPlList.setIsTraceMisc(isTraceMisc);


        				if (mPlDetails.getTraceMisc()==1){



        					ArrayList tagList = mPlDetails.getPlTagList();




                        	if(tagList.size() > 0) {
                        		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mPlDetails.getPlQuantity()));


                        		apPlList.setMisc(misc);

                        	}else {
                          		if(mPlDetails.getPlMisc()==null) {

                          			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mPlDetails.getPlQuantity()));
                          			apPlList.setMisc(misc);
                          		}
                          	}




	        				System.out.println(mPlDetails.getPlCode() + "<== getPlCode forwarded action");
	        				//TODO:save tagList
	        				String misc = Common.convertInvModTagListDetailsListToMisc(tagList, apPlList.getQuantity());

	        				mPlDetails.setPlMisc(misc);
	        				apPlList.setMisc(mPlDetails.getPlMisc());
	        				System.out.println(mPlDetails.getPlMisc() + "<== misc");
        				}
        				apPlList.clearUnitList();

        				ArrayList unitList = ejbPO.getInvUomByIiName(mPlDetails.getPlIiName(), user.getCmpCode());

        				Iterator unitListIter = unitList.iterator();

        				while (unitListIter.hasNext()) {

		            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
		            		apPlList.setUnitList(mUomDetails.getUomName());

		            	}

        				actionForm.saveApPOList(apPlList);

        			}

        			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

        			for (int x = list.size() + 1; x <= remainingList; x++) {

        				ApPurchaseOrderEntryList apPOList = new ApPurchaseOrderEntryList(actionForm,
        						null, String.valueOf(x), null, null, null, null, null, null, null, null, null, "0.0","0.0", "0.0", "0.0", "0.0", "0.0", null);

        				apPOList.setLocationList(actionForm.getLocationList());

        				apPOList.setUnitList(Constants.GLOBAL_BLANK);
        				apPOList.setUnitList("Select Item First");

        				actionForm.saveApPOList(apPOList);

        			}

        			this.setFormProperties(actionForm, user.getCompany());

        			if (request.getParameter("child") == null) {

        				return (mapping.findForward("apPurchaseOrderEntry"));

        			} else {

        				return (mapping.findForward("apPurchaseOrderEntryChild"));

        			}

            	}

            	// Populate line when not forwarding

            	for (int x = 1; x <= journalLineNumber; x++) {

            		ApPurchaseOrderEntryList apPOList = new ApPurchaseOrderEntryList(actionForm,
            				null, new Integer(x).toString(), null, null, null, null, null,
							null, null, null, null, "0.0","0.0", "0.0", "0.0", "0.0", "0.0", null);

            		apPOList.setLocationList(actionForm.getLocationList());
            		apPOList.setUnitList(Constants.GLOBAL_BLANK);
            		apPOList.setUnitList("Select Item First");

            		actionForm.saveApPOList(apPOList);

            	}

            } catch(GlobalNoRecordFoundException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.purchaseOrderAlreadyDeleted"));

            } catch(EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }

               return(mapping.findForward("cmnErrorPage"));

            }

            if (!errors.isEmpty()) {

            	saveErrors(request, new ActionMessages(errors));

            } else {

            	if (request.getParameter("saveSubmitButton") != null &&
            			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            		try {

            			ArrayList list = ejbPO.getAdApprovalNotifiedUsersByPoCode(actionForm.getPurchaseOrderCode(), user.getCmpCode());

            			if (list.isEmpty()) {

            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentSentForPosting"));

            			} else if (list.contains("DOCUMENT POSTED")) {

            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentPosted"));

            			} else {

            				Iterator i = list.iterator();

            				String APPROVAL_USERS = "";

            				while (i.hasNext()) {

            					APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

            					if (i.hasNext()) {

            						APPROVAL_USERS = APPROVAL_USERS + ", ";

            					}


            				}

            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

            			}

            		} catch(EJBException ex) {

            			if (log.isInfoEnabled()) {

            				log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
            						" session: " + session.getId());
            			}

            			return(mapping.findForward("cmnErrorPage"));

            		}

            		saveMessages(request, messages);
            		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            	} else if (request.getParameter("saveAsDraftButton") != null &&
            			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            	}

            }

            actionForm.reset(mapping, request);

            actionForm.setPurchaseOrderCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setDeliveryPeriod(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setConversionRate(Common.convertDoubleToStringMoney(1d, Constants.CONVERSION_RATE_PRECISION));
            actionForm.setPosted("NO");
         	actionForm.setApprovalStatus(null);
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

            this.setFormProperties(actionForm, user.getCompany());

            return(mapping.findForward("apPurchaseOrderEntry"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

            log.info("Exception caught in ApPurchaseOrderEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }


	private void setFormProperties(ApPurchaseOrderEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getPurchaseOrderCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setEnablePurchaseOrderVoid(false);
					actionForm.setShowSaveButton(true);
					actionForm.setShowSaveDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);

				} else if (actionForm.getPurchaseOrderCode() != null && actionForm.getApprovalStatus() == null) {

					actionForm.setEnableFields(true);
					actionForm.setEnablePurchaseOrderVoid(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowSaveDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setEnablePurchaseOrderVoid(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowSaveDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);

				}

			} else if (!actionForm.getPurchaseOrderVoid() && !actionForm.getReceiving()) {

				actionForm.setEnableFields(false);
				actionForm.setEnablePurchaseOrderVoid(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowSaveDraftButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(false);


			} else {

				actionForm.setEnableFields(false);
				actionForm.setEnablePurchaseOrderVoid(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowSaveDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);

			}

		} else {

			actionForm.setEnableFields(false);
			actionForm.setEnablePurchaseOrderVoid(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowSaveDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);

		}
		//view attachment
		if (actionForm.getPurchaseOrderCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/PO/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

            if(new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-1.txt").exists()) {
 				actionForm.setShowViewAttachmentButton1(true);

 			}else {
 				actionForm.setShowViewAttachmentButton1(false);
 			}


 			if(new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-2.txt").exists()) {
 				actionForm.setShowViewAttachmentButton2(true);

 			}else {
 				actionForm.setShowViewAttachmentButton2(false);
 			}

 			if(new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-3.txt").exists()) {
 				actionForm.setShowViewAttachmentButton3(true);

 			}else {
 				actionForm.setShowViewAttachmentButton3(false);
 			}
 			if(new File(attachmentPath + actionForm.getPurchaseOrderCode() + "-4.txt").exists()) {
 				actionForm.setShowViewAttachmentButton4(true);

 			}else {
 				actionForm.setShowViewAttachmentButton4(false);
 			}


		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}

	}

}