package com.struts.ap.purchaseorderentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApPurchaseOrderEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
   private Integer purchaseOrderCode = null;
   private String supplier = null;
   private ArrayList supplierList = new ArrayList();
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String date = null;
   private String deliveryPeriod = null;
   private boolean showBatchName = false;
   private String documentNumber = null;
   private String shipmentNumber = null;
   private String referenceNumber = null;
   private String paymentTerm = null;
   private ArrayList paymentTermList = new ArrayList();
   private String description = null;
   private boolean purchaseOrderVoid = false;
   private boolean receiving = false;
   private boolean validate = false;
   private String billTo = null;
   private String shipTo =  null;
   private String taxCode = null;
   private double taxRate = 0d;
   private String taxType = null;
   private ArrayList taxCodeList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String conversionDate = null;
   private String conversionRate = null;
   private String approvalStatus = null;
   private String posted = null;
   private String reasonForRejection = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;
   private String approvedRejectedBy = null;
   private String dateApprovedRejected = null;
   private String postedBy = null;	
   private String datePosted = null;
   private String supplierName = null;

   private String location = null;
   private ArrayList locationList = new ArrayList();
   private String functionalCurrency = null;
   private ArrayList userList = new ArrayList();

   private FormFile filename1 = null;
   private FormFile filename2 = null;
   private FormFile filename3 = null;
   private FormFile filename4 = null;
   private String attachment = null;
   private String attachmentPDF = null;
   private String attachmentDownload = null;
   private boolean showViewAttachmentButton1 = false;
   private boolean showViewAttachmentButton2 = false;
   private boolean showViewAttachmentButton3 = false;
   private boolean showViewAttachmentButton4 = false;

   private String report = null;
   private ArrayList apPOList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean enablePurchaseOrderVoid = false;
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showDeleteButton = false;
   private boolean showSaveButton = false;
   private boolean showSaveDraftButton = false;
   private boolean useSupplierPulldown = true;

   private String isSupplierEntered  = null;
   private String isTaxCodeEntered = null;
   private String isConversionDateEntered = null;

   private String dateOfObligation = null;
   private String reasonForObligation = null;

   private String viewType = null;
   private ArrayList viewTypeList = new ArrayList();


   public String getDateOfObligation() {

	   return dateOfObligation;

   }

   public void setDateOfObligation(String dateOfObligation) {

	   this.dateOfObligation = dateOfObligation;

   }

   public String getReasonForObligation() {

	   return reasonForObligation;

   }

   public void setReasonForObligation(String reasonForObligation) {

	   this.reasonForObligation = reasonForObligation;

   }

   public String getViewType() {

		return viewType ;

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}

	public ArrayList getViewTypeList() {

		return viewTypeList;

	}

   public String getAttachment() {

   	   return attachment;

   }

   public void setAttachment(String attachment) {

   	   this.attachment = attachment;

   }

   public String getAttachmentPDF() {

   	   return attachmentPDF;

   }

   public void setAttachmentPDF(String attachmentPDF) {

   	   this.attachmentPDF = attachmentPDF;

   }

   public String getAttachmentDownload() {

   	   return attachmentDownload;

   }

   public void setAttachmentDownload(String attachmentDownload) {

   	   this.attachmentDownload = attachmentDownload;

   }
   public String getReport() {

   	   return report;

   }

   public void setReport(String report) {

   	   this.report = report;

   }

   public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

   public Integer getPurchaseOrderCode() {

   	  return purchaseOrderCode;

   }

   public void setPurchaseOrderCode(Integer purchaseOrderCode) {

   	  this.purchaseOrderCode = purchaseOrderCode;

   }

   public String getSupplier() {

   	  return supplier;

   }

   public void setSupplier(String supplier) {

   	  this.supplier = supplier;

   }

   public ArrayList getUserList() {

		return userList;

  }

  public void setUserList(String user) {

	   userList.add(user);

  }

  public void clearUserList() {

	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);

  }

   public ArrayList getSupplierList() {

   	  return supplierList;

   }

   public void setSupplierList(String supplier) {

   	  supplierList.add(supplier);

   }

   public void clearSupplierList() {

   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);

   }


   public String getBatchName(){
       return batchName;
   }

   public void setBatchName(String batchName){
       this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {

   	  return batchNameList;

   }

   public void setBatchNameList(String batchName) {

   	  batchNameList.add(batchName);

   }

   public void clearBatchNameList() {

   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);

   }


   public String getDate() {

   	  return date;

   }

   public void setDate(String date) {

   	  this.date = date;

   }


   public String getDeliveryPeriod() {

   	  return deliveryPeriod;

   }

   public void setDeliveryPeriod(String deliveryPeriod) {

   	  this.deliveryPeriod = deliveryPeriod;

   }


   public boolean getShowBatchName() {

   	   return showBatchName;

   }

   public void setShowBatchName(boolean showBatchName) {

   	   this.showBatchName = showBatchName;

   }
   public String getDocumentNumber() {

   	  return documentNumber;

   }

   public void setDocumentNumber(String documentNumber) {

	  this.documentNumber = documentNumber;

   }

   public String getShipmentNumber() {

	  return shipmentNumber;

   }

   public void setShipmentNumber(String shipmentNumber) {

	   this.shipmentNumber = shipmentNumber;

   }

   public boolean getShowViewAttachmentButton1() {

   	   return showViewAttachmentButton1;

   }

   public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

   	   this.showViewAttachmentButton1 = showViewAttachmentButton1;

   }

   public boolean getShowViewAttachmentButton2() {

   	   return showViewAttachmentButton2;

   }

   public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

   	   this.showViewAttachmentButton2 = showViewAttachmentButton2;

   }

   public boolean getShowViewAttachmentButton3() {

   	   return showViewAttachmentButton3;

   }

   public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

   	   this.showViewAttachmentButton3 = showViewAttachmentButton3;

   }

   public boolean getShowViewAttachmentButton4() {

   	   return showViewAttachmentButton4;

   }

   public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

   	   this.showViewAttachmentButton4 = showViewAttachmentButton4;

   }


   public String getReferenceNumber() {

   	  return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

   	  this.referenceNumber = referenceNumber;

   }

   public String getPaymentTerm() {

   	  return paymentTerm;

   }

   public void setPaymentTerm(String paymentTerm) {

   	  this.paymentTerm = paymentTerm;

   }

   public ArrayList getPaymentTermList() {

   	  return paymentTermList;

   }

   public void setPaymentTermList(String paymentTerm) {

   	  paymentTermList.add(paymentTerm);

   }

   public void clearPaymentTermList() {

   	  paymentTermList.clear();

   }

   public boolean getPurchaseOrderVoid() {

   	  return purchaseOrderVoid;

   }

   public void setPurchaseOrderVoid(boolean purchaseOrderVoid) {

   	  this.purchaseOrderVoid = purchaseOrderVoid;

   }

   public String getDescription() {

   	  return description;

   }

   public void setDescription(String description) {

   	  this.description = description;

   }

   public String getBillTo() {

   	  return billTo;

   }

   public void setBillTo(String billTo) {

	  this.billTo = billTo;

   }

   public String getShipTo() {

	  return shipTo;

   }

   public void setShipTo(String shipTo) {

	  this.shipTo = shipTo;

   }

   public String getTaxCode() {

   	   return taxCode;

   }

   public void setTaxCode(String taxCode) {

   	   this.taxCode = taxCode;

   }

   public ArrayList getTaxCodeList() {

   	   return taxCodeList;

   }

   public void setTaxCodeList(String taxCode) {

   	   taxCodeList.add(taxCode);

   }

   public void clearTaxCodeList() {

   	   taxCodeList.clear();
   	   taxCodeList.add(Constants.GLOBAL_BLANK);

   }

   public String getCurrency() {

   	   return currency;

   }

   public void setCurrency(String currency) {

   	   this.currency = currency;

   }

   public ArrayList getCurrencyList() {

   	   return currencyList;

   }

   public void setCurrencyList(String currency) {

   	   currencyList.add(currency);

   }

   public void clearCurrencyList() {

   	   currencyList.clear();

   }

   public String getConversionDate() {

   	   return conversionDate;

   }

   public void setConversionDate(String conversionDate) {

   	   this.conversionDate = conversionDate;

   }

   public String getConversionRate() {

   	   return conversionRate;

   }

   public void setConversionRate(String conversionRate) {

   	   this.conversionRate = conversionRate;

   }

   public String getApprovalStatus() {

   	  return approvalStatus;

   }

   public void setApprovalStatus(String approvalStatus) {

   	  this.approvalStatus = approvalStatus;

   }

   public String getPosted() {

   	  return posted;

   }

   public void setPosted(String posted) {

   	  this.posted = posted;

   }

   public String getReasonForRejection() {

   	  return reasonForRejection;

   }

   public void setReasonForRejection(String reasonForRejection) {

   	  this.reasonForRejection = reasonForRejection;

   }

   public String getCreatedBy() {

   	   return createdBy;

   }

   public void setCreatedBy(String createdBy) {

   	  this.createdBy = createdBy;

   }

   public String getDateCreated() {

   	   return dateCreated;

   }

   public void setDateCreated(String dateCreated) {

   	   this.dateCreated = dateCreated;

   }

   public String getLastModifiedBy() {

   	  return lastModifiedBy;

   }

   public void setLastModifiedBy(String lastModifiedBy) {

   	  this.lastModifiedBy = lastModifiedBy;

   }

   public String getDateLastModified() {

   	  return dateLastModified;

   }

   public void setDateLastModified(String dateLastModified) {

   	  this.dateLastModified = dateLastModified;

   }

   public String getApprovedRejectedBy() {

   	return approvedRejectedBy;

   }

   public void setApprovedRejectedBy(String approvedRejectedBy) {

   	this.approvedRejectedBy = approvedRejectedBy;

   }

   public String getDateApprovedRejected() {

   	return dateApprovedRejected;

   }

   public void setDateApprovedRejected(String dateApprovedRejected) {

   	this.dateApprovedRejected = dateApprovedRejected;

   }

   public String getPostedBy() {

   	  return postedBy;

   }

   public void setPostedBy(String postedBy) {

   	  this.postedBy = postedBy;

   }

   public String getDatePosted() {

   	  return datePosted;

   }

   public void setDatePosted(String datePosted) {

   	  this.datePosted = datePosted;

   }

   public String getLocation() {

   	  return location;

   }

   public void setLocation(String location) {

   	  this.location = location;

   }

   public ArrayList getLocationList() {

   	  return locationList;

   }

   public void setLocationList(String location) {

   	  locationList.add(location);

   }

   public void clearLocationList() {

   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);

   }

   public String getFunctionalCurrency() {

   	   return functionalCurrency;

   }

   public void setFunctionalCurrency(String functionalCurrency) {

   	   this.functionalCurrency = functionalCurrency;

   }

   public int getRowSelected(){

      return rowSelected;

   }
   public FormFile getFilename1() {

	   	  return filename1;

   }

   public void setFilename1(FormFile filename1) {

   	  this.filename1 = filename1;

   }

   public FormFile getFilename2() {

   	  return filename2;

   }

   public void setFilename2(FormFile filename2) {

   	  this.filename2 = filename2;

   }

   public FormFile getFilename3() {

   	  return filename3;

   }

   public void setFilename3(FormFile filename3) {

   	  this.filename3 = filename3;

   }

   public FormFile getFilename4() {

   	  return filename4;

   }

   public void setFilename4(FormFile filename4) {

   	  this.filename4 = filename4;

   }

   public void setRowSelected(Object selectedApPOList, boolean isEdit){

      this.rowSelected = apPOList.indexOf(selectedApPOList);

   }

   public Object[] getApPOList(){

      return(apPOList.toArray());

   }

   public void saveApPOList(Object newApPOList){

      apPOList.add(newApPOList);

   }

   public void deleteApPOList(int rowSelected){

      apPOList.remove(rowSelected);

   }

   public int getApPOListSize(){

      return(apPOList.size());

   }

   public void clearApPOList(){

      apPOList.clear();

   }

   public ApPurchaseOrderEntryList getApPOByIndex(int index){

   	  return((ApPurchaseOrderEntryList)apPOList.get(index));

   }

   public void updateApPORow(int rowSelected, Object newApPOList){

      apPOList.set(rowSelected, newApPOList);

   }

   public String getTxnStatus(){

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);

   }

   public void setTxnStatus(String txnStatus){

      this.txnStatus = txnStatus;

   }

   public String getUserPermission(){

      return(userPermission);

   }

   public void setUserPermission(String userPermission){

      this.userPermission = userPermission;

   }

   public boolean getEnableFields() {

   	   return enableFields;

   }

   public void setEnableFields(boolean enableFields) {

   	   this.enableFields = enableFields;

   }

   public boolean getEnablePurchaseOrderVoid() {

   	   return enablePurchaseOrderVoid;

   }

   public void setEnablePurchaseOrderVoid(boolean enablePurchaseOrderVoid) {

   	   this.enablePurchaseOrderVoid = enablePurchaseOrderVoid;

   }

   public boolean getShowAddLinesButton() {

   	   return showAddLinesButton;

   }

   public void setShowAddLinesButton(boolean showAddLinesButton) {

   	   this.showAddLinesButton = showAddLinesButton;

   }

   public boolean getShowDeleteLinesButton() {

   	   return showDeleteLinesButton;

   }

   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

   	   this.showDeleteLinesButton = showDeleteLinesButton;

   }

   public boolean getShowDeleteButton() {

   	   return showDeleteButton;

   }

   public void setShowDeleteButton(boolean showDeleteButton) {

   	   this.showDeleteButton = showDeleteButton;

   }

   public boolean getShowSaveButton() {

   	   return showSaveButton;

   }

   public void setShowSaveButton(boolean showSaveButton) {

   	   this.showSaveButton = showSaveButton;

   }

   public boolean getShowSaveDraftButton() {

   	   return showSaveDraftButton;

   }

   public void setShowSaveDraftButton(boolean showSaveDraftButton) {

   	   this.showSaveDraftButton = showSaveDraftButton;

   }

   public String getIsSupplierEntered() {

   	   return isSupplierEntered;

   }

   public boolean getUseSupplierPulldown() {

   	  return useSupplierPulldown;

   }

   public void setUseSupplierPulldown(boolean useSupplierPulldown) {

   	  this. useSupplierPulldown = useSupplierPulldown;

   }

   public boolean getReceiving() {

   	  return receiving;

   }

   public void setReceiving(boolean receiving) {

   	  this.receiving = receiving;

   }

   public boolean getValidate() {

   	  return validate;

   }

   public void setValidate(boolean validate) {

   	  this.validate = validate;

   }

	public String getSupplierName() {

		return supplierName;

	}

	public void setSupplierName(String supplierName) {

		this.supplierName = supplierName;

	}

	public double getTaxRate() {

		return taxRate;

	}

	public void setTaxRate(double taxRate) {

		this.taxRate = taxRate;

	}

	public String getTaxType() {

		return taxType;

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public String getIsTaxCodeEntered() {

		return isTaxCodeEntered;

	}

	public String getIsConversionDateEntered(){

		return isConversionDateEntered;

	}

   public void reset(ActionMapping mapping, HttpServletRequest request){

       supplier = Constants.GLOBAL_BLANK;
       date = null;
	   documentNumber = null;
	   shipmentNumber = null;
	   referenceNumber = null;
	   purchaseOrderVoid = false;
	   description = null;
	   billTo = null;
	   shipTo =  null;
	   taxCode = Constants.GLOBAL_BLANK;
	   conversionDate = null;
	   conversionRate = null;
	   approvalStatus = null;
	   posted = null;
	   reasonForRejection = null;
	   createdBy = null;
	   filename1 = null;
	   filename2 = null;
	   filename3 = null;
	   filename4 = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;
	   location = Constants.GLOBAL_BLANK;
	   isSupplierEntered = null;
	   receiving = false;
	   validate = false;
	   supplierName = Constants.GLOBAL_BLANK;
       taxRate = 0d;
       taxType = Constants.GLOBAL_BLANK;
       isTaxCodeEntered = null;
	   isConversionDateEntered = null;
	   attachment = null;
	   attachmentPDF = null;
	   dateOfObligation = null;
	   reasonForObligation = null;
	   viewTypeList.clear();
	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
	   viewType = Constants.REPORT_VIEW_TYPE_PDF;

	   for (int i=0; i<apPOList.size(); i++) {

	  	  ApPurchaseOrderEntryList actionList = (ApPurchaseOrderEntryList)apPOList.get(i);
	  	  actionList.setIsItemEntered(null);
	  	  actionList.setIsUnitEntered(null);

	  }

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null ||
      		request.getParameter("saveAsDraftButton") != null ||
   		 	request.getParameter("printButton") != null) {

       	 if(Common.validateRequired(description)){
             errors.add("description",
                new ActionMessage("purchaseOrderEntry.error.descriptionRequired"));
          }

      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
             errors.add("supplier",
                new ActionMessage("purchaseOrderEntry.error.supplierRequired"));
          }



         if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
      	    showBatchName){
            errors.add("batchName",
               new ActionMessage("purchaseOrderEntry.error.batchNameRequired"));
         }




      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("purchaseOrderEntry.error.dateRequired"));
         }

		 if(!Common.validateDateFormat(date)){
	            errors.add("date",
		       new ActionMessage("purchaseOrderEntry.error.dateInvalid"));
		 }

		 System.out.println(date.lastIndexOf("/"));
      	 System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 if(year<1900){
      		 errors.add("date",
      				 new ActionMessage("purchaseOrderEntry.error.dateInvalid"));
      	 }

		 if(Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("paymentTerm",
               new ActionMessage("purchaseOrderEntry.error.paymentTermRequired"));
         }

         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("purchaseOrderEntry.error.currencyRequired"));
         }

	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("purchaseOrderEntry.error.conversionDateInvalid"));
         }

	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("purchaseOrderEntry.error.conversionRateInvalid"));
         }

         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("purchaseOrderEntry.error.conversionDateMustBeNull"));
	 	 }

		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
		    errors.add("conversionRate",
		       new ActionMessage("purchaseOrderEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("purchaseOrderEntry.error.conversionRateOrDateMustBeEntered"));
		 }

		 int numOfLines = 0;

      	 Iterator i = apPOList.iterator();

      	 while (i.hasNext()) {

      	 	 ApPurchaseOrderEntryList plList = (ApPurchaseOrderEntryList)i.next();

      	 	 if (Common.validateRequired(plList.getItemName()) &&
      	 	 	 Common.validateRequired(plList.getLocation()) &&
      	 	     Common.validateRequired(plList.getQuantity()) &&
      	 	     Common.validateRequired(plList.getUnit()) &&
      	 	     Common.validateRequired(plList.getUnitCost())) continue;

      	 	 numOfLines++;

	         if(Common.validateRequired(plList.getItemName()) || plList.getItemName().equals(Constants.GLOBAL_BLANK)){
	            errors.add("itemName",
	            	new ActionMessage("purchaseOrderEntry.error.itemNameRequired", plList.getLineNumber()));
	         }
	         if(Common.validateRequired(plList.getLocation()) || plList.getLocation().equals(Constants.GLOBAL_BLANK)){
	            errors.add("location",
	            	new ActionMessage("purchaseOrderEntry.error.locationRequired", plList.getLineNumber()));
	         }
	         if(Common.validateRequired(plList.getQuantity())) {
	         	errors.add("quantity",
	         		new ActionMessage("purchaseOrderEntry.error.quantityRequired", plList.getLineNumber()));
	         }
		 	 if(!Common.validateNumberFormat(plList.getQuantity())){
	            errors.add("quantity",
	               new ActionMessage("purchaseOrderEntry.error.quantityInvalid", plList.getLineNumber()));
	         }
		 	 if(!Common.validateRequired(plList.getQuantity()) && Common.convertStringMoneyToDouble(plList.getQuantity(), (short)3) <= 0) {
	         	errors.add("quantity",
	         		new ActionMessage("purchaseOrderEntry.error.negativeOrZeroQuantityNotAllowed", plList.getLineNumber()));
	         }
		 	 if(Common.validateRequired(plList.getUnit()) || plList.getUnit().equals(Constants.GLOBAL_BLANK)){
	            errors.add("unit",
	            	new ActionMessage("purchaseOrderEntry.error.unitRequired", plList.getLineNumber()));
	         }
		 	 if(Common.validateRequired(plList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("purchaseOrderEntry.error.unitCostRequired", plList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(plList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("purchaseOrderEntry.error.unitCostInvalid", plList.getLineNumber()));
	         }

	    }

	    if (numOfLines == 0) {

         	errors.add("supplier",
               new ActionMessage("purchaseOrderEntry.error.purchaseOrderMustHaveItemLine"));

        }

      } else if (!Common.validateRequired(request.getParameter("isSupplierEntered"))) {

      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("supplier",
               new ActionMessage("purchaseOrderEntry.error.supplierRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {

      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("supplier",
               new ActionMessage("purchaseOrderEntry.error.supplierRequired"));
         }

		 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("taxCode",
               new ActionMessage("purchaseOrderEntry.error.taxCodeRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

      	if(!Common.validateDateFormat(conversionDate)){

      		errors.add("conversionDate", new ActionMessage("purchaseOrderEntry.error.conversionDateInvalid"));

      	}

      }

      return(errors);
   }
}
