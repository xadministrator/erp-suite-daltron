package com.struts.ap.purchaseorderentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.struts.ap.receivingitementry.ApReceivingItemEntryTagList;

public class ApPurchaseOrderEntryList implements Serializable {

	private Integer purchaseOrderLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String quantity = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitCost = null;
	private String amount = null;
	private String qcNumber = null;
	private String qcExpiryDate = null;
	private String itemDescription = null;
	private String discount1 = null;
	private String discount2 = null;
	private String discount3 = null;
	private String discount4 = null;
	private String totalDiscount = null;
	private String fixedDiscountAmount = null;
	private String misc = null;
	private boolean isTraceMisc = false;

	private ArrayList tagList = new ArrayList();

	private boolean deleteCheckbox = false;

	private String isItemEntered = null;
	private String isUnitEntered = null;

	private ApPurchaseOrderEntryForm parentBean;

	public ApPurchaseOrderEntryList(ApPurchaseOrderEntryForm parentBean,
			Integer purchaseOrderLineCode,
			String lineNumber,
			String location,
			String itemName,
			String itemDescription,
			String quantity,
			String unit,
			String unitCost,
			String amount,
			String qcNumber,
			String qcExpiryDate,
			String discount1,
			String discount2,
			String discount3,
			String discount4,
			String totalDiscount,
			String fixedDiscountAmount,
			String misc){

		this.parentBean = parentBean;
		this.purchaseOrderLineCode = purchaseOrderLineCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.quantity = quantity;
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;
		this.qcNumber = qcNumber;
		this.qcExpiryDate = qcExpiryDate;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.totalDiscount = totalDiscount;
		this.fixedDiscountAmount = fixedDiscountAmount;
		this.misc = misc;


	}

	public Integer getPurchaseOrderLineCode(){

		return(purchaseOrderLineCode);

	}

	public String getLineNumber() {

		return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

		this.lineNumber = lineNumber;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;

	}
	public String getItemDescription() {

		return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

		this.itemDescription = itemDescription;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(ArrayList locationList) {

		this.locationList = locationList;

	}

	public String getQuantity() {

		return quantity;

	}

	public void setQuantity(String quantity) {

		this.quantity = quantity;

	}

	public String getUnit() {

		return unit;

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}

	public ArrayList getUnitList() {

		return unitList;

	}

	public void setUnitList(String unit) {

		unitList.add(unit);

	}

	public void clearUnitList() {

		unitList.clear();

	}

	public ApPurchaseOrderEntryTagList getTagListByIndex(int index){
	      return((ApPurchaseOrderEntryTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}

	public String getUnitCost() {

		return unitCost;

	}

	public void setUnitCost(String unitCost) {

		this.unitCost = unitCost;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}


	public String getQcNumber() {

		return qcNumber;

	}

	public void setQcNumber(String qcNumber) {

		this.qcNumber = qcNumber;

	}

	public String getQcExpiryDate() {

		return qcExpiryDate;

	}

	public void setQcExpiryDate(String qcExpiryDate) {

		this.qcExpiryDate = qcExpiryDate;

	}



	public boolean getDeleteCheckbox() {

		return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

		this.deleteCheckbox = deleteCheckbox;

	}

	public boolean getIsTraceMisc() {

		return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

		this.isTraceMisc = isTraceMisc;

	}


	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isItemEntered = null;

	}

	public String getIsUnitEntered() {

		return isUnitEntered;

	}

	public void setIsUnitEntered(String isUnitEntered) {

		if (isUnitEntered != null && isUnitEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isUnitEntered = null;

	}

	public String getDiscount1() {

		return discount1;

	}

	public void setDiscount1(String discount1) {

		this.discount1 = discount1;

	}

	public String getDiscount2() {

		return discount2;

	}

	public void setDiscount2(String discount2) {

		this.discount2 = discount2;

	}

	public String getDiscount3() {

		return discount3;

	}

	public void setDiscount3(String discount3) {

		this.discount3 = discount3;

	}

	public String getDiscount4() {

		return discount4;

	}

	public void setDiscount4(String discount4) {

		this.discount4 = discount4;

	}

	
	public String getTotalDiscount() {

		return totalDiscount;

	}

	public void setTotalDiscount(String totalDiscount) {

		this.totalDiscount = totalDiscount;

	}

	public String getFixedDiscountAmount() {

		return fixedDiscountAmount;

	}

	public void setFixedDiscountAmount(String fixedDiscountAmount) {

		this.fixedDiscountAmount = fixedDiscountAmount;

	}

}