package com.struts.ap.purchaserequisitionentry;

import java.io.Serializable;

public class ApPurchaseRequisitionApproverList implements Serializable {
	
	private String approvedDate = null;
	private String approverName = null;
	private String status = null;

	private ApPurchaseRequisitionEntryForm parentBean;
	
	public ApPurchaseRequisitionApproverList(ApPurchaseRequisitionEntryForm parentBean,
			String approvedDate, String approverName, String status) {
		
		this.parentBean = parentBean;
		this.approvedDate = approvedDate;
		this.approverName = approverName;
		this.status = status;
		
	}

	public String getApprovedDate() {
		
		return approvedDate;
		
	}

	
	
	public String getApproverName() {
		
		return approverName;
		
	}
	
	public void setApproverName(String approverName) {
		
		this.approverName = approverName;
		
	}

	public String getStatus() {
		
		return status;
		
	}
	
	public void setStatus(String status) {
		
		this.status = status;
		
	}
	
	
	
}