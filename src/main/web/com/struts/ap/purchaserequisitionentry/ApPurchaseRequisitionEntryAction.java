package com.struts.ap.purchaserequisitionentry;

import java.io.BufferedReader;
//import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalInsufficientBudgetQuantityException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalSendEmailMessageException;
import com.ejb.txn.ApPurchaseRequisitionEntryController;
import com.ejb.txn.ApPurchaseRequisitionEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.ap.receivingitementry.ApReceivingItemEntryTagList;
import com.struts.util.Attachment;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.AdModApprovalQueueDetails;
import com.util.ApModPurchaseRequisitionDetails;
import com.util.ApModPurchaseRequisitionLineDetails;
import com.util.ApPurchaseRequisitionDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApPurchaseRequisitionEntryAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApPurchaseRequisitionEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ApPurchaseRequisitionEntryForm actionForm = (ApPurchaseRequisitionEntryForm)form;

         //reset reports

         actionForm.setReport(null);
         actionForm.setReport2(null);
         actionForm.setReportQuotation(null);
         System.out.println("NO USER");

         actionForm.setAttachment(null);
         actionForm.setAttachmentPDF(null);
         actionForm.setAttachmentDownload(null);

	     String frParam = null;

         frParam = Common.getUserPermission(user, Constants.AP_PURCHASE_REQUISITION_ENTRY_ID);

         if (frParam != null) {

         	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

         		ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

         		if (!fieldErrors.isEmpty()) {

         			saveErrors(request, new ActionMessages(fieldErrors));
         			return(mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         	}

         	actionForm.setUserPermission(frParam.trim());

         } else {

         	actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApPurchaseRequisitionEntryController EJB
*******************************************************/

         ApPurchaseRequisitionEntryControllerHome homePR = null;
         ApPurchaseRequisitionEntryController ejbPR = null;
        
         GlDailyRateControllerHome homeFR = null;
         GlDailyRateController ejbFR = null;
         
         InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;

         try {

            homePR = (ApPurchaseRequisitionEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApPurchaseRequisitionEntryControllerEJB", ApPurchaseRequisitionEntryControllerHome.class);

            homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
            
           
            homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);


         } catch(NamingException e) {

         	if(log.isInfoEnabled()){
                log.info("NamingException caught in ApPurchaseRequisitionEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));

         }

         try {

            ejbPR = homePR.create();

            ejbFR = homeFR.create();
            
            ejbII = homeII.create();

         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in ApPurchaseRequisitionEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ApPurchaseRequisitionEntryController EJB
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

         short precisionUnit = 0;

         short journalLineNumber = 0;
         boolean isInitialPrinting = false;
         boolean useSupplierPulldown = true;
         String defaultPrTax = null;
         String defaultPrCurrency = null;
         Integer poSize = null;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/PR/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
		 String attachmentFileExtensionData = appProperties.getMessage("app.attachmentFileExtensionData");

         try {

         	precisionUnit = ejbPR.getGlFcPrecisionUnit(user.getCmpCode());

            journalLineNumber = ejbPR.getAdPrfApJournalLineNumber(user.getCmpCode());
            defaultPrTax = ejbPR.getAdPrfApDefaultPrTax(user.getCmpCode());
            defaultPrCurrency = ejbPR.getAdPrfApDefaultPrCurrency(user.getCmpCode());
            actionForm.setShowPrCost(Common.convertByteToBoolean(ejbPR.getAdPrfApShowPrCost(user.getCmpCode())));
            actionForm.setPrecisionUnit(precisionUnit);
         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApPurchaseReqisitionEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

/*******************************************************
   -- Ap PR Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

           details.setPrCode(actionForm.getPurchaseRequisitionCode());
           details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setPrNumber(actionForm.getDocumentNumber());
           details.setPrReferenceNumber(actionForm.getReferenceNumber());
           details.setPrDescription(actionForm.getDescription());
           details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
           details.setPrTagStatus(actionForm.getTag());
           details.setPrType(actionForm.getPrType());
           details.setPrSchedule(actionForm.getReminderSchedule());
           details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
           details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

           if (actionForm.getPurchaseRequisitionCode() == null) {

           	   details.setPrCreatedBy(user.getUserName());
	           details.setPrDateCreated(new java.util.Date());

           }

           details.setPrLastModifiedBy(user.getUserName());
           details.setPrDateLastModified(new java.util.Date());

           ArrayList prlList = new ArrayList();

           for (int i = 0; i<actionForm.getApPRListSize(); i++) {

           	   ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

           	   if (Common.validateRequired(apPRList.getLocation()) &&
           	   	   Common.validateRequired(apPRList.getItemName()) &&
				   Common.validateRequired(apPRList.getUnit()) &&
				   Common.validateRequired(apPRList.getQuantity())) continue;

           	   ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();

           	   mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());
           	   mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));
           	   mdetails.setPrlIlIiName(apPRList.getItemName());
           	   mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
           	   mdetails.setPrlIlLocName(apPRList.getLocation());
           	   mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
           	   mdetails.setPrlUomName(apPRList.getUnit());
           	   mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));

           	   ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

           	   mdetails.setCnvList(mPrDetails.getCnvList());
           	   mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());
           	   mdetails.setPrlMisc(apPRList.getMisc());
           	   String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
           	   String misc=apPRList.getMisc();

           	   ArrayList tagList = new ArrayList();
           	   boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
        	   if (isTraceMisc == true){
        		   mdetails.setTraceMisc((byte) 1);
        		   String[] arrayMisc = misc.split("_");
               	   int qty= Common.convertStringToInt(arrayMisc[0]);

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split("@");
               	   propertyCode = arrayMisc[0].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split("<");
               	   serialNumber = arrayMisc[0].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split(">");
               	   specs = arrayMisc[0].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split(";");
               	   custodian = arrayMisc[0].split(",");
               	   //expiryDate = arrayMisc[1].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split("%");
               	   expiryDate = arrayMisc[0].split(",");
               	   tgDocumentNumber = arrayMisc[1].split(",");
               	   //if(apPRList.getTagListSize()>0){
            	   //TODO:save and submit taglist
            	   for (int y = 0; y<Common.convertStringToDouble(apPRList.getQuantity()); y++) {

            		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
            		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
            		   InvModTagListDetails tgDetails = new InvModTagListDetails();
            		   if (custodian[y].equals(" ")&&specs[y].equals(" ")
    	     				   &&expiryDate[y].equals(" ")&&propertyCode[y].equals(" ")
    	     				   &&serialNumber[y].equals(" ")){
    	        			   System.out.println("break");
    	        			   break;
    	     		   }

            		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
            		   tgDetails.setTgSpecs(specs[y].trim());
            		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
            		   tgDetails.setTgCustodian(custodian[y].trim());
            		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
            		   tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(actionForm.getDate()));
            		   tgDetails.setTgType("N/A");

            		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
            		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
            		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
            		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
            		   tagList.add(tgDetails);
            		   mdetails.setPrlTagList(tagList);
            	   }
            	   //}
        	   }

           	   prlList.add(mdetails);

           }
           //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

    	    		/*
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
           	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    //		new ActionMessage("purchaseRequisitionEntry.error.filename1Invalid"));

           	    	}
					*/
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPurchaseRequisitionEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

    	    		/*
    	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
           	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

           	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    //		new ActionMessage("purchaseRequisitionEntry.error.filename2Invalid"));

           	    	}
					*/
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPurchaseRequisitionEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
    	    		/*
    	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

           	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   // 		new ActionMessage("purchaseRequisitionEntry.error.filename3Invalid"));

           	    	}
					*/
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPurchaseRequisitionEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
    	    		/*
    	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

           	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    ///		new ActionMessage("purchaseRequisitionEntry.error.filename4Invalid"));

           	    	}
					*/
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("apPurchaseRequisitionEntry"));

    	    	}

    	   	}
           try {

           	    Integer purchaseRequisitionCode = ejbPR.saveApPrEntry(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(), prlList, true,
           	        new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));
           } catch (GlobalRecordInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.typeInvalid", ex.getMessage()));

           } catch (GlobalInsufficientBudgetQuantityException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.insufficientBudgetQuantity", ex.getMessage()));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apPurchaseRequisitionEntry"));

	       }

    	   // save attachment

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	if(attachmentFileExtension==null) {
       	        		System.out.println("attachmentFileExtension is null");
       	        	}
       	        	if(fileExtension==null) {
       	        		System.out.println("fileExtension is null");
       	        		fileExtension = attachmentFileExtensionData;
       	        	}
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

/*******************************************************
   -- Ap PR Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           	ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

           	details.setPrCode(actionForm.getPurchaseRequisitionCode());
           	details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
           	details.setPrNumber(actionForm.getDocumentNumber());
           	details.setPrReferenceNumber(actionForm.getReferenceNumber());
           	details.setPrDescription(actionForm.getDescription());
           	details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
            details.setPrTagStatus(actionForm.getTag());
            details.setPrType(actionForm.getPrType());
            details.setPrSchedule(actionForm.getReminderSchedule());
            details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
            details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));

           	details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           	details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

           	if (actionForm.getPurchaseRequisitionCode() == null) {

           		details.setPrCreatedBy(user.getUserName());
           		details.setPrDateCreated(new java.util.Date());

           	}

           	details.setPrLastModifiedBy(user.getUserName());
           	details.setPrDateLastModified(new java.util.Date());

           	ArrayList prlList = new ArrayList();

           	String lineNumber = null;
           	String itemName = null;
           	for (int i = 0; i<actionForm.getApPRListSize(); i++) {

           		ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

           		if (Common.validateRequired(apPRList.getLocation()) &&
           				Common.validateRequired(apPRList.getItemName()) &&
						Common.validateRequired(apPRList.getUnit()) &&
						Common.validateRequired(apPRList.getQuantity())) continue;

           		ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();
           		mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());

           		mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));

           		mdetails.setPrlIlIiName(apPRList.getItemName());
           		mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
           		mdetails.setPrlIlLocName(apPRList.getLocation());
           		mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
           		mdetails.setPrlUomName(apPRList.getUnit());
           		mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));
           		mdetails.setPrlBdgtQuantity(Common.convertStringMoneyToDouble(apPRList.getBudgetQuantity(), precisionUnit));

           		itemName = apPRList.getItemName();
           		lineNumber = apPRList.getLineNumber();

           		ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

           		mdetails.setCnvList(mPrDetails.getCnvList());
           		mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());

           		String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
        	   String misc=apPRList.getMisc();
        	   ArrayList tagList = new ArrayList();
           	   boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
        	   if (isTraceMisc == true){
        		   mdetails.setTraceMisc((byte) 1);
        		   String[] arrayMisc = misc.split("_");
               	   int qty= Common.convertStringToInt(arrayMisc[0]);

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split("@");
               	   propertyCode = arrayMisc[0].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split("<");
               	   serialNumber = arrayMisc[0].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split(">");
               	   specs = arrayMisc[0].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split(";");
               	   custodian = arrayMisc[0].split(",");
               	   //expiryDate = arrayMisc[1].split(",");

               	   misc = arrayMisc[1];
               	   arrayMisc = misc.split("%");
               	   expiryDate = arrayMisc[0].split(",");
               	   tgDocumentNumber = arrayMisc[1].split(",");
               	   //if(apPRList.getTagListSize()>0){
            	   //TODO:save and submit taglist
            	   for (int y = 0; y<Common.convertStringToDouble(apPRList.getQuantity()); y++) {

            		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
            		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
            		   InvModTagListDetails tgDetails = new InvModTagListDetails();
            		   if (custodian[y].equals(" ")&&specs[y].equals(" ")
    	     				   &&expiryDate[y].equals(" ")&&propertyCode[y].equals(" ")
    	     				   &&serialNumber[y].equals(" ")){
    	        			   System.out.println("break");
    	        			   break;
    	     		   }

            		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
            		   tgDetails.setTgSpecs(specs[y].trim());
            		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
            		   tgDetails.setTgCustodian(custodian[y].trim());
            		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
            		   tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(actionForm.getDate()));
            		   tgDetails.setTgType("N/A");

            		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
            		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
            		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
            		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
            		   tagList.add(tgDetails);
            		   mdetails.setPrlTagList(tagList);
            	   }
            	   //}
        	   }

           		prlList.add(mdetails);

           	}

	        //validate attachment
            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

            if (!Common.validateRequired(filename1)) {

     	    	if (actionForm.getFilename1().getFileSize() == 0) {

     	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
             			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

     	    	} else {

     	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
     	    		/*
     	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
           	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

           	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    //		new ActionMessage("purchaseRequisitionEntry.error.filename1Invalid"));

           	    	}
					*/
        	    	InputStream is = actionForm.getFilename1().getInputStream();

        	    	if (is.available() > maxAttachmentFileSize) {

        	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

        	    	}

        	    	is.close();

     	    	}

     	    	if (!errors.isEmpty()) {

     	    		saveErrors(request, new ActionMessages(errors));
            			return (mapping.findForward("apPurchaseRequisitionEntry"));

     	    	}

     	   }

     	   if (!Common.validateRequired(filename2)) {

     	    	if (actionForm.getFilename2().getFileSize() == 0) {

     	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
             			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

     	    	} else {

     	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
     	    		/*
     	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
           	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2Invalid"));

           	    	}
					*/
            	    	InputStream is = actionForm.getFilename2().getInputStream();

            	    	if (is.available() > maxAttachmentFileSize) {

            	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                     		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

            	    	}

            	    	is.close();

     	    	}

     	    	if (!errors.isEmpty()) {

     	    		saveErrors(request, new ActionMessages(errors));
            			return (mapping.findForward("apPurchaseRequisitionEntry"));

     	    	}

     	   }

     	   if (!Common.validateRequired(filename3)) {

     	    	if (actionForm.getFilename3().getFileSize() == 0) {

     	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
             			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

     	    	} else {

     	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
     	    		/*
     	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
           	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

           	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   // 		new ActionMessage("purchaseRequisitionEntry.error.filename3Invalid"));

           	    	}
					*/
        	    		InputStream is = actionForm.getFilename3().getInputStream();

            	    	if (is.available() > maxAttachmentFileSize) {

            	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                     		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

            	    	}

            	    	is.close();

     	    	}

     	    	if (!errors.isEmpty()) {

     	    		saveErrors(request, new ActionMessages(errors));
            			return (mapping.findForward("apPurchaseRequisitionEntry"));

     	    	}

     	   }

     	   if (!Common.validateRequired(filename4)) {

     	    	if (actionForm.getFilename4().getFileSize() == 0) {

     	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
             			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

     	    	} else {

     	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
     	    		/*
     	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
           	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

           	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   //		new ActionMessage("purchaseRequisitionEntry.error.filename4Invalid"));

           	    	}
					*/
            	    	InputStream is = actionForm.getFilename4().getInputStream();

            	    	if (is.available() > maxAttachmentFileSize) {

            	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                     		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

            	    	}

            	    	is.close();

     	    	}

     	    	if (!errors.isEmpty()) {

     	    		saveErrors(request, new ActionMessages(errors));
            			return (mapping.findForward("apPurchaseRequisitionEntry"));

     	    	}

     	   	}
           	try {

           		Integer purchaseRequisitionCode = ejbPR.saveApPrEntry(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(),
           				prlList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           		actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

           	} catch (GlobalRecordAlreadyDeletedException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

           	} catch (GlobalDocumentNumberNotUniqueException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

           	} catch (GlobalConversionDateNotExistException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

           	} catch (GlobalTransactionAlreadyVoidException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

           	} catch (GlobalInvItemLocationNotFoundException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));
           	} catch (GlobalRecordInvalidException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("purchaseRequisitionEntry.error.typeInvalid", ex.getMessage()));

           	} catch (GlobalInsufficientBudgetQuantityException ex) {

                   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("purchaseRequisitionEntry.error.insufficientBudgetQuantity", ex.getMessage()));

           	} catch (GlobalTransactionAlreadyApprovedException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

           	} catch (GlobalTransactionAlreadyPendingException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

           	} catch (GlobalTransactionAlreadyPostedException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

           	} catch (GlobalNoApprovalApproverFoundException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.noApprovalApproverFound"));

           	} catch (GlobalNoApprovalRequesterFoundException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.noApprovalRequesterFound"));

           	} catch (GlobalSendEmailMessageException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("purchaseRequisitionEntry.error.sendEmailMessageError"));

           	} catch (EJBException ex) {
           		if (log.isInfoEnabled()) {

           			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
           					" session: " + session.getId());
           		}

           		return(mapping.findForward("cmnErrorPage"));
           	}

           	if (!errors.isEmpty()) {

           		saveErrors(request, new ActionMessages(errors));
           		return (mapping.findForward("apPurchaseRequisitionEntry"));

           	}
           	// save attachment

           	if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	if(attachmentFileExtension==null) {
       	        		System.out.println("attachmentFileExtension is null");
       	        	}
       	        	if(fileExtension==null) {
       	        		System.out.println("fileExtension is null");
       	        		fileExtension = attachmentFileExtensionData;
       	        	}
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}else {
           	    		fileExtension = fileExtension + attachmentFileExtensionData;
           	    	}


       	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }


/**************************************************
    -- Ap Canvass Save & Submit Action --
 *******************************************************/

                  } else if (request.getParameter("saveCanvassButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                    	ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

                    	details.setPrCode(actionForm.getPurchaseRequisitionCode());
                    	details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
                    	details.setPrNumber(actionForm.getDocumentNumber());
                    	details.setPrReferenceNumber(actionForm.getReferenceNumber());
                    	details.setPrDescription(actionForm.getDescription());
                    	details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
                    	details.setPrTagStatus(actionForm.getTag());
                    	details.setPrType(actionForm.getPrType());
                    	details.setPrSchedule(actionForm.getReminderSchedule());
                        details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
                        details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));
                    	details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                    	details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                    	if (actionForm.getPurchaseRequisitionCode() == null) {

                    		details.setPrCreatedBy(user.getUserName());
                    		details.setPrDateCreated(new java.util.Date());

                    	}

                    	details.setPrLastModifiedBy(user.getUserName());
                    	details.setPrDateLastModified(new java.util.Date());

                    	ArrayList prlList = new ArrayList();

                    	for (int i = 0; i<actionForm.getApPRListSize(); i++) {

                    		ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

                    		if (Common.validateRequired(apPRList.getLocation()) &&
                    				Common.validateRequired(apPRList.getItemName()) &&
         						Common.validateRequired(apPRList.getUnit()) &&
         						Common.validateRequired(apPRList.getQuantity())) continue;

                    		ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();
                    		mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());
                    		mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));
                    		mdetails.setPrlIlIiName(apPRList.getItemName());
                    		mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
                    		mdetails.setPrlIlLocName(apPRList.getLocation());
                    		mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
                    		mdetails.setPrlUomName(apPRList.getUnit());
                    		mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));

                    		ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

                    		mdetails.setCnvList(mPrDetails.getCnvList());
                    		mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());
                    		String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
                    		String misc=apPRList.getMisc();
                    		ArrayList tagList = new ArrayList();
                        	   boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
                     	   if (isTraceMisc == true){
                     		   mdetails.setTraceMisc((byte) 1);
                     		  String[] arrayMisc = misc.split("_");
                          	   int qty= Common.convertStringToInt(arrayMisc[0]);

                          	   misc = arrayMisc[1];
                          	   arrayMisc = misc.split("@");
                          	   propertyCode = arrayMisc[0].split(",");

                          	   misc = arrayMisc[1];
                          	   arrayMisc = misc.split("<");
                          	   serialNumber = arrayMisc[0].split(",");

                          	   misc = arrayMisc[1];
                          	   arrayMisc = misc.split(">");
                          	   specs = arrayMisc[0].split(",");

                          	   misc = arrayMisc[1];
                          	   arrayMisc = misc.split(";");
                          	   custodian = arrayMisc[0].split(",");
                          	   //expiryDate = arrayMisc[1].split(",");

                          	   misc = arrayMisc[1];
                          	   arrayMisc = misc.split("%");
                          	   expiryDate = arrayMisc[0].split(",");
                          	   tgDocumentNumber = arrayMisc[1].split(",");
                            	   //if(apPRList.getTagListSize()>0){
                         	   //TODO:save and submit taglist
                         	   for (int y = 0; y<Common.convertStringToDouble(apPRList.getQuantity()); y++) {

                         		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
                         		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
                         		   InvModTagListDetails tgDetails = new InvModTagListDetails();
                         		   if (custodian[y].equals(" ")&&specs[y].equals(" ")
                 	     				   &&expiryDate[y].equals(" ")&&propertyCode[y].equals(" ")
                 	     				   &&serialNumber[y].equals(" ")){
                 	        			   System.out.println("break");
                 	        			   break;
                 	     		   }

                         		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
                         		   tgDetails.setTgSpecs(specs[y].trim());
                         		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
                         		   tgDetails.setTgCustodian(custodian[y].trim());
                         		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
                         		   tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(actionForm.getDate()));
                         		   tgDetails.setTgType("N/A");

                         		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
                         		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
                         		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
                         		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
                         		   tagList.add(tgDetails);
                         		   mdetails.setPrlTagList(tagList);
                         	   }
                         	   //}
                     	   }

                    		prlList.add(mdetails);

                    	}

                    	//validate attachment
                        String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                        String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                        String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                        String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                        if (!Common.validateRequired(filename1)) {

                 	    	if (actionForm.getFilename1().getFileSize() == 0) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

                 	    	} else {

                 	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
                 	    		/*
                 	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                       	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                       	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                                //	new ActionMessage("purchaseRequisitionEntry.error.filename1Invalid"));

                       	    	}
								*/
                        	    	InputStream is = actionForm.getFilename1().getInputStream();

                        	    	if (is.available() > maxAttachmentFileSize) {

                        	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                                 		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

                        	    	}

                        	    	is.close();

                 	    	}

                 	    	if (!errors.isEmpty()) {

                 	    		saveErrors(request, new ActionMessages(errors));
                        			return (mapping.findForward("apPurchaseRequisitionEntry"));

                 	    	}

                 	   }

                 	   if (!Common.validateRequired(filename2)) {

                 	    	if (actionForm.getFilename2().getFileSize() == 0) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

                 	    	} else {

                 	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
                 	    		/*
                 	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                       	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                       	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                                //		new ActionMessage("purchaseRequisitionEntry.error.filename2Invalid"));

                       	    	}
								*/
                        	    	InputStream is = actionForm.getFilename2().getInputStream();

                        	    	if (is.available() > maxAttachmentFileSize) {

                        	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                                 		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

                        	    	}

                        	    	is.close();

                 	    	}

                 	    	if (!errors.isEmpty()) {

                 	    		saveErrors(request, new ActionMessages(errors));
                        			return (mapping.findForward("apPurchaseRequisitionEntry"));

                 	    	}

                 	   }

                 	   if (!Common.validateRequired(filename3)) {

                 	    	if (actionForm.getFilename3().getFileSize() == 0) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

                 	    	} else {

                 	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
                 	    		/*
                 	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                       	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                       	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                                //		new ActionMessage("purchaseRequisitionEntry.error.filename3Invalid"));

                       	    	}
								*/
                        	    	InputStream is = actionForm.getFilename3().getInputStream();

                        	    	if (is.available() > maxAttachmentFileSize) {

                        	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                                 		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

                        	    	}

                        	    	is.close();

                 	    	}

                 	    	if (!errors.isEmpty()) {

                 	    		saveErrors(request, new ActionMessages(errors));
                        			return (mapping.findForward("apPurchaseRequisitionEntry"));

                 	    	}

                 	   }

                 	   if (!Common.validateRequired(filename4)) {

                 	    	if (actionForm.getFilename4().getFileSize() == 0) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

                 	    	} else {

                 	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
                 	    		/*
                 	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                       	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                       	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                                //		new ActionMessage("purchaseRequisitionEntry.error.filename4Invalid"));

                       	    	}
								*/
                        	    	InputStream is = actionForm.getFilename4().getInputStream();

                        	    	if (is.available() > maxAttachmentFileSize) {

                        	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                                 		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

                        	    	}

                        	    	is.close();

                 	    	}

                 	    	if (!errors.isEmpty()) {

                 	    		saveErrors(request, new ActionMessages(errors));
                        			return (mapping.findForward("apPurchaseRequisitionEntry"));

                 	    	}

                 	   	}

                    	try {

                    		Integer purchaseRequisitionCode = ejbPR.saveApPrCanvass(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(),
                    				prlList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                    		actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

                    	} catch (GlobalRecordAlreadyDeletedException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

                    	} catch (GlobalDocumentNumberNotUniqueException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

                    	} catch (GlobalConversionDateNotExistException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

                    	} catch (GlobalTransactionAlreadyVoidException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

                    	} catch (GlobalInvItemLocationNotFoundException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));

                    	} catch (GlobalTransactionAlreadyApprovedException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

                    	} catch (GlobalTransactionAlreadyPendingException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

                    	} catch (GlobalTransactionAlreadyPostedException ex) {

                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

                    	} catch (GlobalNoApprovalRequesterFoundException ex) {

                         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                                  new ActionMessage("canvassEntry.error.noApprovalRequesterFound"));

                         } catch (GlobalNoApprovalApproverFoundException ex) {

                         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                                  new ActionMessage("canvassEntry.error.noApprovalApproverFound"));


                         } catch (GlobalRecordInvalidException ex) {

                     		errors.add(ActionMessages.GLOBAL_MESSAGE,
                     				new ActionMessage("purchaseRequisitionEntry.error.recordInvalid", ex.getMessage()));

                     	} catch (EJBException ex) {
                    		if (log.isInfoEnabled()) {

                    			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
                    					" session: " + session.getId());
                    		}

                    		return(mapping.findForward("cmnErrorPage"));
                    	}

                    	if (!errors.isEmpty()) {

                    		saveErrors(request, new ActionMessages(errors));
                    		return (mapping.findForward("apPurchaseRequisitionEntry"));

                    	}
                    	// save attachment

                    	if (!Common.validateRequired(filename1)) {

                   	        if (errors.isEmpty()) {

                   	        	InputStream is = actionForm.getFilename1().getInputStream();

                   	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

                   	        	new File(attachmentPath).mkdirs();


                   	        	if(attachmentFileExtension==null) {
                   	        		System.out.println("attachmentFileExtension is null");
                   	        	}
                   	        	if(fileExtension==null) {
                   	        		System.out.println("fileExtension is null");
                   	        		fileExtension = attachmentFileExtensionData;
                   	        	}
                   	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                   	    			fileExtension = attachmentFileExtension;

                       	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                       	    		fileExtension = attachmentFileExtensionPDF;

                       	    	}else {
                       	    		fileExtension = fileExtension + attachmentFileExtensionData;
                       	    	}


                   	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt");
                   	        	FileWriter writer = new FileWriter(fileTest);
                   	            writer.write(actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);
                   	            writer.close();

                   	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);

                   	    		int c;

            	            	while ((c = is.read()) != -1) {

            	            		fos.write((byte)c);

            	            	}

            	            	is.close();
            					fos.close();

                   	        }

                   	   }

                   	   if (!Common.validateRequired(filename2)) {

                   	        if (errors.isEmpty()) {

                   	        	InputStream is = actionForm.getFilename2().getInputStream();

                   	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

                   	        	new File(attachmentPath).mkdirs();

                   	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                   	    			fileExtension = attachmentFileExtension;

                       	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                       	    		fileExtension = attachmentFileExtensionPDF;

                       	    	}else {
                       	    		fileExtension = fileExtension + attachmentFileExtensionData;
                       	    	}


                   	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt");
                   	        	FileWriter writer = new FileWriter(fileTest);
                   	            writer.write(actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);
                   	            writer.close();

                   	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);

                   	    		int c;

            	            	while ((c = is.read()) != -1) {

            	            		fos.write((byte)c);

            	            	}

            	            	is.close();
            					fos.close();

                   	        }

                   	   }

                   	   if (!Common.validateRequired(filename3)) {

                   	        if (errors.isEmpty()) {

                   	        	InputStream is = actionForm.getFilename3().getInputStream();

                   	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

                   	        	new File(attachmentPath).mkdirs();

                   	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                   	    			fileExtension = attachmentFileExtension;

                       	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                       	    		fileExtension = attachmentFileExtensionPDF;

                       	    	}else {
                       	    		fileExtension = fileExtension + attachmentFileExtensionData;
                       	    	}


                   	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt");
                   	        	FileWriter writer = new FileWriter(fileTest);
                   	            writer.write(actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);
                   	            writer.close();

                   	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);

                   	    		int c;

            	            	while ((c = is.read()) != -1) {

            	            		fos.write((byte)c);

            	            	}

            	            	is.close();
            					fos.close();

                   	        }

                   	   }

                   	   if (!Common.validateRequired(filename4)) {

                   	        if (errors.isEmpty()) {

                   	        	InputStream is = actionForm.getFilename4().getInputStream();

                   	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

                   	        	new File(attachmentPath).mkdirs();

                   	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                   	    			fileExtension = attachmentFileExtension;

                       	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                       	    		fileExtension = attachmentFileExtensionPDF;

                       	    	}else {
                       	    		fileExtension = fileExtension + attachmentFileExtensionData;
                       	    	}


                   	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt");
                   	        	FileWriter writer = new FileWriter(fileTest);
                   	            writer.write(actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);
                   	            writer.close();

                   	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);

                   	    		int c;

            	            	while ((c = is.read()) != -1) {

            	            		fos.write((byte)c);

            	            	}

            	            	is.close();
            					fos.close();

                   	        }

                   	   }

/*******************************************************
 -- Ap PR Print Action --
 *******************************************************/

         } else if (request.getParameter("printButton") != null) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

         		ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

         		details.setPrCode(actionForm.getPurchaseRequisitionCode());
         		details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setPrNumber(actionForm.getDocumentNumber());
         		details.setPrReferenceNumber(actionForm.getReferenceNumber());
         		details.setPrDescription(actionForm.getDescription());
         		details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
         		details.setPrTagStatus(actionForm.getTag());
         		details.setPrType(actionForm.getPrType());
         		details.setPrSchedule(actionForm.getReminderSchedule());
                details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
                details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));

         		details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         		details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

         		if (actionForm.getPurchaseRequisitionCode() == null) {

         			details.setPrCreatedBy(user.getUserName());
         			details.setPrDateCreated(new java.util.Date());

         		}

         		details.setPrLastModifiedBy(user.getUserName());
         		details.setPrDateLastModified(new java.util.Date());

         		ArrayList prlList = new ArrayList();

         		for (int i = 0; i<actionForm.getApPRListSize(); i++) {

         			ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

         			if (Common.validateRequired(apPRList.getLocation()) &&
         					Common.validateRequired(apPRList.getItemName()) &&
							Common.validateRequired(apPRList.getUnit()) &&
							Common.validateRequired(apPRList.getQuantity())) continue;

         			ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();
         			mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());
         			mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));
         			mdetails.setPrlIlIiName(apPRList.getItemName());
         			mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
         			mdetails.setPrlIlLocName(apPRList.getLocation());
         			mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
         			mdetails.setPrlUomName(apPRList.getUnit());
         			mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));
         			mdetails.setPrlBdgtQuantity(Common.convertStringMoneyToDouble(apPRList.getBudgetQuantity(), precisionUnit));
         			ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

         			mdetails.setCnvList(mPrDetails.getCnvList());
         			mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());
         			String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
         			String misc=apPRList.getMisc();
         			ArrayList tagList = new ArrayList();
                	   boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
             	   if (isTraceMisc == true){
             		   mdetails.setTraceMisc((byte) 1);
             		  String[] arrayMisc = misc.split("_");
                  	   int qty= Common.convertStringToInt(arrayMisc[0]);

                  	   misc = arrayMisc[1];
                  	   arrayMisc = misc.split("@");
                  	   propertyCode = arrayMisc[0].split(",");

                  	   misc = arrayMisc[1];
                  	   arrayMisc = misc.split("<");
                  	   serialNumber = arrayMisc[0].split(",");

                  	   misc = arrayMisc[1];
                  	   arrayMisc = misc.split(">");
                  	   specs = arrayMisc[0].split(",");

                  	   misc = arrayMisc[1];
                  	   arrayMisc = misc.split(";");
                  	   custodian = arrayMisc[0].split(",");
                  	   //expiryDate = arrayMisc[1].split(",");

                  	   misc = arrayMisc[1];
                  	   arrayMisc = misc.split("%");
                  	   expiryDate = arrayMisc[0].split(",");
                  	   tgDocumentNumber = arrayMisc[1].split(",");
                    	   //if(apPRList.getTagListSize()>0){
                 	   //TODO:save and submit taglist
                 	   for (int y = 0; y<Common.convertStringToDouble(apPRList.getQuantity()); y++) {

                 		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
                 		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
                 		   InvModTagListDetails tgDetails = new InvModTagListDetails();
                 		   if (custodian[y].equals(" ")&&specs[y].equals(" ")
         	     				   &&expiryDate[y].equals(" ")&&propertyCode[y].equals(" ")
         	     				   &&serialNumber[y].equals(" ")){
         	        			   System.out.println("break");
         	        			   break;
         	     		   }

                 		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
                 		   tgDetails.setTgSpecs(specs[y].trim());
                 		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
                 		   tgDetails.setTgCustodian(custodian[y].trim());
                 		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
                 		   tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(actionForm.getDate()));
	            		   tgDetails.setTgType("N/A");

                 		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
                 		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
                 		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
                 		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
                 		   tagList.add(tgDetails);
                 		   mdetails.setPrlTagList(tagList);
                 	   }
                 	   //}
             	   }

         			prlList.add(mdetails);

         		}
         		//validate attachment
                String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

         	    	if (actionForm.getFilename1().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

         	    	} else {

         	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                       // 		new ActionMessage("purchaseRequisitionEntry.error.filename1Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename1().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   }

         	   if (!Common.validateRequired(filename2)) {

         	    	if (actionForm.getFilename2().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

         	    	} else {

         	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                        //		new ActionMessage("purchaseRequisitionEntry.error.filename2Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename2().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   }

         	   if (!Common.validateRequired(filename3)) {

         	    	if (actionForm.getFilename3().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

         	    	} else {

         	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                        //		new ActionMessage("purchaseRequisitionEntry.error.filename3Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename3().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   }

         	   if (!Common.validateRequired(filename4)) {

         	    	if (actionForm.getFilename4().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

         	    	} else {

         	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                        //		new ActionMessage("purchaseRequisitionEntry.error.filename41Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename4().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   	}

         		try {

         			Integer purchaseRequisitionCode = ejbPR.saveApPrEntry(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(),
         					prlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

         		} catch (GlobalRecordAlreadyDeletedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

         		} catch (GlobalDocumentNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

         		} catch (GlobalConversionDateNotExistException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

         		} catch (GlobalTransactionAlreadyVoidException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

         		} catch (GlobalInvItemLocationNotFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));
         		} catch (GlobalRecordInvalidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("purchaseRequisitionEntry.error.typeInvalid", ex.getMessage()));

         		} catch (GlobalInsufficientBudgetQuantityException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("purchaseRequisitionEntry.error.insufficientBudgetQuantity", ex.getMessage()));

         		} catch (GlobalTransactionAlreadyApprovedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

         		} catch (GlobalTransactionAlreadyPendingException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

         		} catch (GlobalTransactionAlreadyPostedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

         		} catch (GlobalNoApprovalApproverFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalApproverFound"));

               	} catch (GlobalNoApprovalRequesterFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalRequesterFound"));

         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {

         				log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}

         			return(mapping.findForward("cmnErrorPage"));
         		}

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return (mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return (mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         		// save attachment

         		if (!Common.validateRequired(filename1)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename1().getInputStream();

           	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();


           	        	if(attachmentFileExtension==null) {
           	        		System.out.println("attachmentFileExtension is null");
           	        	}
           	        	if(fileExtension==null) {
           	        		System.out.println("fileExtension is null");
           	        		fileExtension = attachmentFileExtensionData;
           	        	}
           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

           	   if (!Common.validateRequired(filename2)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename2().getInputStream();

           	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();

           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

           	   if (!Common.validateRequired(filename3)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename3().getInputStream();

           	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();

           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

           	   if (!Common.validateRequired(filename4)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename4().getInputStream();

           	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();

           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

         		actionForm.setReport(Constants.STATUS_SUCCESS);

         		isInitialPrinting = true;


         	}  else {

         		actionForm.setReport(Constants.STATUS_SUCCESS);

         		return(mapping.findForward("apPurchaseRequisitionEntry"));

         	}

/*******************************************************
 -- Ap PR Print Quotation Action --
 *******************************************************/

         } else if (request.getParameter("printQuotationButton") != null) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

         		ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

         		details.setPrCode(actionForm.getPurchaseRequisitionCode());
         		details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setPrNumber(actionForm.getDocumentNumber());
         		details.setPrReferenceNumber(actionForm.getReferenceNumber());
         		details.setPrDescription(actionForm.getDescription());
         		details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
         		details.setPrTagStatus(actionForm.getTag());
         		details.setPrType(actionForm.getPrType());
         		details.setPrSchedule(actionForm.getReminderSchedule());
                details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
                details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));

         		details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         		details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

         		if (actionForm.getPurchaseRequisitionCode() == null) {

         			details.setPrCreatedBy(user.getUserName());
         			details.setPrDateCreated(new java.util.Date());

         		}

         		details.setPrLastModifiedBy(user.getUserName());
         		details.setPrDateLastModified(new java.util.Date());

         		ArrayList prlList = new ArrayList();

         		for (int i = 0; i<actionForm.getApPRListSize(); i++) {

         			ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

         			if (Common.validateRequired(apPRList.getLocation()) &&
         					Common.validateRequired(apPRList.getItemName()) &&
							Common.validateRequired(apPRList.getUnit()) &&
							Common.validateRequired(apPRList.getQuantity())) continue;

         			ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();
         			mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());
         			mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));
         			mdetails.setPrlIlIiName(apPRList.getItemName());
         			mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
         			mdetails.setPrlIlLocName(apPRList.getLocation());
         			mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
         			mdetails.setPrlUomName(apPRList.getUnit());
         			mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));
         			mdetails.setPrlBdgtQuantity(Common.convertStringMoneyToDouble(apPRList.getBudgetQuantity(), precisionUnit));
         			ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

         			mdetails.setCnvList(mPrDetails.getCnvList());
         			mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());
         			String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
            	   String misc=apPRList.getMisc();
            	   ArrayList tagList = new ArrayList();
               	   boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
            	   if (isTraceMisc == true){
            		   mdetails.setTraceMisc((byte) 1);
            		   String[] arrayMisc = misc.split("_");
                   	   int qty= Common.convertStringToInt(arrayMisc[0]);

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split("@");
                   	   propertyCode = arrayMisc[0].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split("<");
                   	   serialNumber = arrayMisc[0].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split(">");
                   	   specs = arrayMisc[0].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split(";");
                   	   custodian = arrayMisc[0].split(",");
                   	   //expiryDate = arrayMisc[1].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split("%");
                   	   expiryDate = arrayMisc[0].split(",");
                   	   tgDocumentNumber = arrayMisc[1].split(",");
                   	   //if(apPRList.getTagListSize()>0){
                	   //TODO:save and submit taglist
                	   for (int y = 0; y<Common.convertStringToDouble(apPRList.getQuantity()); y++) {

                		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
                		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
                		   InvModTagListDetails tgDetails = new InvModTagListDetails();
                		   if (custodian[y].equals(" ")&&specs[y].equals(" ")
        	     				   &&expiryDate[y].equals(" ")&&propertyCode[y].equals(" ")
        	     				   &&serialNumber[y].equals(" ")){
        	        			   System.out.println("break");
        	        			   break;
        	     		   }

                		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
                		   tgDetails.setTgSpecs(specs[y].trim());
                		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
                		   tgDetails.setTgCustodian(custodian[y].trim());
                		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
                		   tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(actionForm.getDate()));
	            		   tgDetails.setTgType("N/A");

                		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
                		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
                		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
                		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
                		   tagList.add(tgDetails);
                		   mdetails.setPrlTagList(tagList);
                	   }
                	   //}
            	   }

         			prlList.add(mdetails);

         		}


         		try {

         			Integer purchaseRequisitionCode = ejbPR.saveApPrEntry(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(),
         					prlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

         		} catch (GlobalRecordAlreadyDeletedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

         		} catch (GlobalDocumentNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

         		} catch (GlobalConversionDateNotExistException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

         		} catch (GlobalTransactionAlreadyVoidException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

         		} catch (GlobalInvItemLocationNotFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));
         		} catch (GlobalRecordInvalidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("purchaseRequisitionEntry.error.typeInvalid", ex.getMessage()));

         		} catch (GlobalInsufficientBudgetQuantityException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("purchaseRequisitionEntry.error.insufficientBudgetQuantity", ex.getMessage()));

         		} catch (GlobalTransactionAlreadyApprovedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

         		} catch (GlobalTransactionAlreadyPendingException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

         		} catch (GlobalTransactionAlreadyPostedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

         		} catch (GlobalNoApprovalApproverFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalApproverFound"));

               	} catch (GlobalNoApprovalRequesterFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalRequesterFound"));

         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {

         				log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}

         			return(mapping.findForward("cmnErrorPage"));
         		}

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return (mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return (mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         		actionForm.setReportQuotation(Constants.STATUS_SUCCESS);

         		isInitialPrinting = true;


         	}  else {

         		actionForm.setReportQuotation(Constants.STATUS_SUCCESS);

         		return(mapping.findForward("apPurchaseRequisitionEntry"));

         	}

/*******************************************************
 -- Ap PR Print CR Action --
 *******************************************************/

         } else if (request.getParameter("crPrintButton") != null) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

         		ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

         		details.setPrCode(actionForm.getPurchaseRequisitionCode());
         		details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setPrNumber(actionForm.getDocumentNumber());
         		details.setPrReferenceNumber(actionForm.getReferenceNumber());
         		details.setPrDescription(actionForm.getDescription());
         		details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
         		details.setPrTagStatus(actionForm.getTag());
         		details.setPrType(actionForm.getPrType());
         		details.setPrSchedule(actionForm.getReminderSchedule());
                details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
                details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));

         		details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         		details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

         		if (actionForm.getPurchaseRequisitionCode() == null) {

         			details.setPrCreatedBy(user.getUserName());
         			details.setPrDateCreated(new java.util.Date());

         		}

         		details.setPrLastModifiedBy(user.getUserName());
         		details.setPrDateLastModified(new java.util.Date());

         		ArrayList prlList = new ArrayList();

         		for (int i = 0; i<actionForm.getApPRListSize(); i++) {

         			ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

         			if (Common.validateRequired(apPRList.getLocation()) &&
         					Common.validateRequired(apPRList.getItemName()) &&
							Common.validateRequired(apPRList.getUnit()) &&
							Common.validateRequired(apPRList.getQuantity())) continue;

         			ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();
         			mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());
         			mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));
         			mdetails.setPrlIlIiName(apPRList.getItemName());
         			mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
         			mdetails.setPrlIlLocName(apPRList.getLocation());
         			mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
         			mdetails.setPrlUomName(apPRList.getUnit());
         			mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));
         			mdetails.setPrlBdgtQuantity(Common.convertStringMoneyToDouble(apPRList.getBudgetQuantity(), precisionUnit));
         			ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

         			mdetails.setCnvList(mPrDetails.getCnvList());
         			mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());

         			String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
            	   String misc=apPRList.getMisc();
            	   ArrayList tagList = new ArrayList();
               	   boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
            	   if (isTraceMisc == true){
            		   mdetails.setTraceMisc((byte) 1);
            		   String[] arrayMisc = misc.split("_");
                   	   int qty= Common.convertStringToInt(arrayMisc[0]);

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split("@");
                   	   propertyCode = arrayMisc[0].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split("<");
                   	   serialNumber = arrayMisc[0].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split(">");
                   	   specs = arrayMisc[0].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split(";");
                   	   custodian = arrayMisc[0].split(",");
                   	   //expiryDate = arrayMisc[1].split(",");

                   	   misc = arrayMisc[1];
                   	   arrayMisc = misc.split("%");
                   	   expiryDate = arrayMisc[0].split(",");
                   	   tgDocumentNumber = arrayMisc[1].split(",");
                   	   //if(apPRList.getTagListSize()>0){
                	   //TODO:save and submit taglist
                	   for (int y = 0; y<Common.convertStringToDouble(apPRList.getQuantity()); y++) {

                		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
                		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
                		   InvModTagListDetails tgDetails = new InvModTagListDetails();
                		   if (custodian[y].equals(" ")&&specs[y].equals(" ")
        	     				   &&expiryDate[y].equals(" ")&&propertyCode[y].equals(" ")
        	     				   &&serialNumber[y].equals(" ")){
        	        			   System.out.println("break");
        	        			   break;
        	     		   }

                		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
                		   tgDetails.setTgSpecs(specs[y].trim());
                		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
                		   tgDetails.setTgCustodian(custodian[y].trim());
                		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
                		   tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(actionForm.getDate()));
	            		   tgDetails.setTgType("N/A");

                		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
                		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
                		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
                		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
                		   tagList.add(tgDetails);
                		   mdetails.setPrlTagList(tagList);
                	   }
                	   //}
            	   }

         			prlList.add(mdetails);

         		}
         		//validate attachment
                String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

         	    	if (actionForm.getFilename1().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

         	    	} else {

         	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                        //		new ActionMessage("purchaseRequisitionEntry.error.filename1Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename1().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   }

         	   if (!Common.validateRequired(filename2)) {

         	    	if (actionForm.getFilename2().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

         	    	} else {

         	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                        //		new ActionMessage("purchaseRequisitionEntry.error.filename2Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename2().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   }

         	   if (!Common.validateRequired(filename3)) {

         	    	if (actionForm.getFilename3().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

         	    	} else {

         	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                        //		new ActionMessage("purchaseRequisitionEntry.error.filename3Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename3().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   }

         	   if (!Common.validateRequired(filename4)) {

         	    	if (actionForm.getFilename4().getFileSize() == 0) {

         	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

         	    	} else {

         	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
         	    		/*
         	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
               	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

               	    	//	errors.add(ActionMessages.GLOBAL_MESSAGE,
                        //		new ActionMessage("purchaseRequisitionEntry.error.filename4Invalid"));

               	    	}
						*/
                	    	InputStream is = actionForm.getFilename4().getInputStream();

                	    	if (is.available() > maxAttachmentFileSize) {

                	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                         		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

                	    	}

                	    	is.close();

         	    	}

         	    	if (!errors.isEmpty()) {

         	    		saveErrors(request, new ActionMessages(errors));
                			return (mapping.findForward("apPurchaseRequisitionEntry"));

         	    	}

         	   	}
         		try {

         			Integer purchaseRequisitionCode = ejbPR.saveApPrEntry(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(),
         					prlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

         		} catch (GlobalRecordAlreadyDeletedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

         		} catch (GlobalDocumentNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

         		} catch (GlobalConversionDateNotExistException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

         		} catch (GlobalTransactionAlreadyVoidException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

         		} catch (GlobalInvItemLocationNotFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));
         		} catch (GlobalRecordInvalidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("purchaseRequisitionEntry.error.typeInvalid", ex.getMessage()));

         		} catch (GlobalInsufficientBudgetQuantityException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("purchaseRequisitionEntry.error.insufficientBudgetQuantity", ex.getMessage()));

         		} catch (GlobalTransactionAlreadyApprovedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

         		} catch (GlobalTransactionAlreadyPendingException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

         		} catch (GlobalTransactionAlreadyPostedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

         		} catch (GlobalNoApprovalApproverFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalApproverFound"));

               	} catch (GlobalNoApprovalRequesterFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalRequesterFound"));

         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {

         				log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}

         			return(mapping.findForward("cmnErrorPage"));
         		}

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return (mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return (mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         		// save attachment

         		if (!Common.validateRequired(filename1)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename1().getInputStream();

           	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();


           	        	if(attachmentFileExtension==null) {
           	        		System.out.println("attachmentFileExtension is null");
           	        	}
           	        	if(fileExtension==null) {
           	        		System.out.println("fileExtension is null");
           	        		fileExtension = attachmentFileExtensionData;
           	        	}
           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1-" + filename1);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

           	   if (!Common.validateRequired(filename2)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename2().getInputStream();

           	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();

           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2-" + filename2);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

           	   if (!Common.validateRequired(filename3)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename3().getInputStream();

           	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();

           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3-" + filename3);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

           	   if (!Common.validateRequired(filename4)) {

           	        if (errors.isEmpty()) {

           	        	InputStream is = actionForm.getFilename4().getInputStream();

           	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

           	        	new File(attachmentPath).mkdirs();

           	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

           	    			fileExtension = attachmentFileExtension;

               	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
               	    		fileExtension = attachmentFileExtensionPDF;

               	    	}else {
               	    		fileExtension = fileExtension + attachmentFileExtensionData;
               	    	}


           	    		File fileTest = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt");
           	        	FileWriter writer = new FileWriter(fileTest);
           	            writer.write(actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);
           	            writer.close();

           	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4-" + filename4);

           	    		int c;

    	            	while ((c = is.read()) != -1) {

    	            		fos.write((byte)c);

    	            	}

    	            	is.close();
    					fos.close();

           	        }

           	   }

         		actionForm.setReport2(Constants.STATUS_SUCCESS);

         		isInitialPrinting = true;


         	}  else {

         		actionForm.setReport2(Constants.STATUS_SUCCESS);

         		return(mapping.findForward("apPurchaseRequisitionEntry"));

         	}

/*******************************************************
    -- Ap PR View Attachment Action --
 *******************************************************/

	      } else if(request.getParameter("viewAttachmentButton1") != null) {

	    	  //	File file = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1" + attachmentFileExtension );
	    	  //	File filePDF = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1" + attachmentFileExtensionPDF );
	    	 // 	File fileDownload = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1" + attachmentFileExtensionData);
	    	  	String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}
					System.out.println("attachment extension is : " + attachmentFileExtension);
		    	  	if (fileExtension.equals(".jpg")) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(".pdf") ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}




		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("apPurchaseRequisitionEntry"));

		         } else {

		           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

		        }



		      } else if(request.getParameter("viewAttachmentButton2") != null) {

		    	  String filename = "";

		    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt");


		    	  	if(fileRef.exists()) {
		    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

		    	  	  String st;
		    	  	  while ((st = br.readLine()) != null) {
		    	  		filename = st;
		    	  	  }
		    	  	}


		    	  	System.out.println("filepath:" + attachmentPath  + filename);
		    	  	File file = new File( attachmentPath  + filename );

		    	  	if(file.exists()) {

		    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  		System.out.println(fileExtension + " <== file extension?");
			    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

			    	  	byte data[] = new byte[fis.available()];

			    	  	int ctr = 0;
			    	  	int c = 0;

			    	  	while ((c = fis.read()) != -1) {

				      		data[ctr] = (byte)c;
				      		ctr++;

			    	  	}

			    	  	if (fileExtension.equals(attachmentFileExtension)) {
					      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
					      	System.out.println("jpg");

					      	session.setAttribute(Constants.IMAGE_KEY, image);
					      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
			    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
			    	  		System.out.println("pdf");

			    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
			    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
			    	  	} else {
			    	  		Object obj = data;

			    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
			    	  		session.setAttribute("DownloadFileName",filename);
			    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
			    	  	}






		    	  	}



		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("apPurchaseRequisitionEntry"));

		         } else {

		           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton3") != null) {

		    	  String filename = "";

		    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt");


		    	  	if(fileRef.exists()) {
		    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

		    	  	  String st;
		    	  	  while ((st = br.readLine()) != null) {
		    	  		filename = st;
		    	  	  }
		    	  	}


		    	  	System.out.println("filepath:" + attachmentPath  + filename);
		    	  	File file = new File( attachmentPath  + filename );

		    	  	if(file.exists()) {

		    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  		System.out.println(fileExtension + " <== file extension?");
			    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

			    	  	byte data[] = new byte[fis.available()];

			    	  	int ctr = 0;
			    	  	int c = 0;

			    	  	while ((c = fis.read()) != -1) {

				      		data[ctr] = (byte)c;
				      		ctr++;

			    	  	}

			    	  	if (fileExtension.equals(attachmentFileExtension)) {
					      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
					      	System.out.println("jpg");

					      	session.setAttribute(Constants.IMAGE_KEY, image);
					      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
			    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
			    	  		System.out.println("pdf");

			    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
			    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
			    	  	} else {
			    	  		Object obj = data;

			    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
			    	  		session.setAttribute("DownloadFileName",filename);
			    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
			    	  	}





		    	  	}

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("apPurchaseRequisitionEntry"));

		         } else {

		           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton4") != null) {

		    	  String filename = "";

		    	  	File fileRef = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt");


		    	  	if(fileRef.exists()) {
		    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

		    	  	  String st;
		    	  	  while ((st = br.readLine()) != null) {
		    	  		filename = st;
		    	  	  }
		    	  	}


		    	  	System.out.println("filepath:" + attachmentPath  + filename);
		    	  	File file = new File( attachmentPath  + filename );

		    	  	if(file.exists()) {

		    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  		System.out.println(fileExtension + " <== file extension?");
			    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

			    	  	byte data[] = new byte[fis.available()];

			    	  	int ctr = 0;
			    	  	int c = 0;

			    	  	while ((c = fis.read()) != -1) {

				      		data[ctr] = (byte)c;
				      		ctr++;

			    	  	}

			    	  	if (fileExtension.equals(attachmentFileExtension)) {
					      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
					      	System.out.println("jpg");

					      	session.setAttribute(Constants.IMAGE_KEY, image);
					      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
			    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
			    	  		System.out.println("pdf");

			    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
			    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
			    	  	} else {
			    	  		Object obj = data;

			    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
			    	  		session.setAttribute("DownloadFileName",filename);
			    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
			    	  	}






		    	  	}

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("apPurchaseRequisitionEntry"));

		         } else {

		           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

		        }

/*******************************************************
   -- AP PR Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbPR.deleteApPrEntry(actionForm.getPurchaseRequisitionCode(), user.getUserName(), user.getCmpCode());

           	    // delete attachment

           	    if (actionForm.getPurchaseRequisitionCode() != null) {


					appProperties = MessageResources.getMessageResources("com.ApplicationResources");

		            attachmentPath = appProperties.getMessage("app.attachmentPath") + "ap/PR/";
		            attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		            attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


					File file = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt");

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt");

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt");

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt");

					if (file.exists()) {

						file.delete();

					}

           	    }

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());

                }

                return(mapping.findForward("cmnErrorPage"));

            }

/*******************************************************
-- Ap PR Supplier Action --
*******************************************************/
         } else if(request.getParameter("apPRList[" +
                 actionForm.getRowSelected() + "].supplierButton") != null) {

          	if(Common.validateRequired(actionForm.getApprovalStatus())) {

          		ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

              	details.setPrCode(actionForm.getPurchaseRequisitionCode());
              	details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
              	details.setPrNumber(actionForm.getDocumentNumber());
              	details.setPrReferenceNumber(actionForm.getReferenceNumber());
              	details.setPrDescription(actionForm.getDescription());
              	details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
         		details.setPrType(actionForm.getPrType());
              	details.setPrTagStatus(actionForm.getTag());
              	details.setPrSchedule(actionForm.getReminderSchedule());
                details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
                details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));

              	details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
              	details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

              	if (actionForm.getPurchaseRequisitionCode() == null) {

              		details.setPrCreatedBy(user.getUserName());
              		details.setPrDateCreated(new java.util.Date());

              	}

              	details.setPrLastModifiedBy(user.getUserName());
              	details.setPrDateLastModified(new java.util.Date());

              	ArrayList prlList = new ArrayList();

              	for (int i = 0; i<actionForm.getApPRListSize(); i++) {

              		ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

              		if (Common.validateRequired(apPRList.getLocation()) &&
              				Common.validateRequired(apPRList.getItemName()) &&
     						Common.validateRequired(apPRList.getUnit()) &&
     						Common.validateRequired(apPRList.getQuantity())) continue;

              		ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();
              		mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());
              		mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));
              		mdetails.setPrlIlIiName(apPRList.getItemName());
              		mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
              		mdetails.setPrlIlLocName(apPRList.getLocation());
              		mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
              		mdetails.setPrlUomName(apPRList.getUnit());
              		mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));

              		ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

              		mdetails.setCnvList(mPrDetails.getCnvList());
              		mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());

              		 boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
	               	   if (isTraceMisc == true){
	               		   mdetails.setTraceMisc((byte) 1);
	               		   String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
	             	   String misc=apPRList.getMisc();
	             	   String[] arrayMisc = misc.split("_");
	             	   int qty= Common.convertStringToInt(arrayMisc[0]);

	             	   misc = arrayMisc[1];
	             	   arrayMisc = misc.split("@");
	             	   propertyCode = arrayMisc[0].split(",");

	             	   misc = arrayMisc[1];
	             	   arrayMisc = misc.split("<");
	             	   serialNumber = arrayMisc[0].split(",");

	             	   misc = arrayMisc[1];
	             	   arrayMisc = misc.split(">");
	             	   specs = arrayMisc[0].split(",");

	             	   misc = arrayMisc[1];
	             	   arrayMisc = misc.split(";");
	             	   custodian = arrayMisc[0].split(",");
	             	   //expiryDate = arrayMisc[1].split(",");

	             	   misc = arrayMisc[1];
	             	   arrayMisc = misc.split("%");
	             	   expiryDate = arrayMisc[0].split(",");
	             	   tgDocumentNumber = arrayMisc[1].split(",");
	             	   ArrayList tagList = new ArrayList();

		          	   //if(apPRList.getTagListSize()>0){
		          	   //TODO:save and submit taglist
			          	   for (int y = 0; y<apPRList.getTagListSize(); y++) {

			          		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
			          		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
			          		   InvModTagListDetails tgDetails = new InvModTagListDetails();
			          		   if (custodian[y].equals("")&&specs[y].equals("")
			       				   &&expiryDate[y].equals("")&&propertyCode[y].equals("")
			       				   &&serialNumber[y].equals("")){

			          			   break;
			          		   }

			          		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
			          		   tgDetails.setTgSpecs(specs[y].trim());
			          		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
			          		   tgDetails.setTgCustodian(custodian[y].trim());
			          		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));

			          		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
			          		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
			          		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
			          		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
			          		   tagList.add(tgDetails);
			          		   mdetails.setPrlTagList(tagList);
			          	   }
	               	   }



              		prlList.add(mdetails);

              	}

              	try {

              		Integer purchaseRequisitionCode = ejbPR.saveApPrEntry(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(), prlList, true,
              				new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

              		actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

              	} catch (GlobalRecordAlreadyDeletedException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

              	} catch (GlobalDocumentNumberNotUniqueException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

              	} catch (GlobalConversionDateNotExistException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

              	} catch (GlobalTransactionAlreadyVoidException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

              	} catch (GlobalInvItemLocationNotFoundException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));
              	} catch (GlobalRecordInvalidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("purchaseRequisitionEntry.error.typeInvalid", ex.getMessage()));

              	} catch (GlobalInsufficientBudgetQuantityException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("purchaseRequisitionEntry.error.insufficientBudgetQuantity", ex.getMessage()));

              	} catch (GlobalTransactionAlreadyApprovedException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

              	} catch (GlobalTransactionAlreadyPendingException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

              	} catch (GlobalTransactionAlreadyPostedException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              				new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

              	} catch (GlobalNoApprovalApproverFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalApproverFound"));

               	} catch (GlobalNoApprovalRequesterFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalRequesterFound"));

              	} catch (EJBException ex) {
              		if (log.isInfoEnabled()) {

              			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
              					" session: " + session.getId());
              		}

              		return(mapping.findForward("cmnErrorPage"));
              	}

              	if (!errors.isEmpty()) {

              		saveErrors(request, new ActionMessages(errors));
              		return (mapping.findForward("apPurchaseRequisitionEntry"));

              	}

          	}

          	//get purchase requisition line code

          	ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(actionForm.getRowSelected());

          	Integer prlCode = null;

          	try {

          		prlCode = ejbPR.getApPrlCodeByPrlLineAndPrCode(Short.parseShort(apPRList.getLineNumber()),
          				actionForm.getPurchaseRequisitionCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

          	} catch (EJBException ex) {
          		if (log.isInfoEnabled()) {

          			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
          					" session: " + session.getId());
          		}

          		return(mapping.findForward("cmnErrorPage"));
          	}

          	String path = "/apCanvass.do?forward=1" +
  				"&purchaseRequisitionLineCode=" + String.valueOf(prlCode.intValue()) +
  				"&purchaseRequisitionCode=" + String.valueOf(actionForm.getPurchaseRequisitionCode()) +
 				"&posted=" + actionForm.getPosted() + "&canvassApprovalStatus=" + actionForm.getCanvassApprovalStatus() +"&conversionRate="+actionForm.getConversionRate() +"&currency="+actionForm.getCurrency();

          	System.out.print(path);
          	return(new ActionForward(path));

/*******************************************************
 -- Ap PR Generate PO Action --
 *******************************************************/
         } else if(request.getParameter("generatePoButton") != null) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {
         		ApPurchaseRequisitionDetails details = new ApPurchaseRequisitionDetails();

         		details.setPrCode(actionForm.getPurchaseRequisitionCode());
         		details.setPrDate(Common.convertStringToSQLDate(actionForm.getDate()));
         		details.setPrNumber(actionForm.getDocumentNumber());
         		details.setPrReferenceNumber(actionForm.getReferenceNumber());
         		details.setPrDescription(actionForm.getDescription());
         		details.setPrVoid(Common.convertBooleanToByte(actionForm.getPurchaseRequisitionVoid()));
         		details.setPrTagStatus(actionForm.getTag());
         		details.setPrType(actionForm.getPrType());
         		details.setPrSchedule(actionForm.getReminderSchedule());
                details.setPrNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
                details.setPrDeliveryPeriod(Common.convertStringToSQLDate(actionForm.getDate()));

         		details.setPrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
         		details.setPrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

         		if (actionForm.getPurchaseRequisitionCode() == null) {

         			details.setPrCreatedBy(user.getUserName());
         			details.setPrDateCreated(new java.util.Date());

         		}

         		details.setPrLastModifiedBy(user.getUserName());
         		details.setPrDateLastModified(new java.util.Date());

         		ArrayList prlList = new ArrayList();

         		for (int i = 0; i<actionForm.getApPRListSize(); i++) {

         			ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

         			if (Common.validateRequired(apPRList.getLocation()) &&
         					Common.validateRequired(apPRList.getItemName()) &&
							Common.validateRequired(apPRList.getUnit()) &&
							Common.validateRequired(apPRList.getQuantity())) continue;

         			ApModPurchaseRequisitionLineDetails mdetails = new ApModPurchaseRequisitionLineDetails();
         			mdetails.setPrlCode(apPRList.getPurchaseRequisitionLineCode());
         			mdetails.setPrlLine(Common.convertStringToShort(apPRList.getLineNumber()));
         			mdetails.setPrlIlIiName(apPRList.getItemName());
         			mdetails.setPrlIlIiDescription(apPRList.getItemDescription());
         			mdetails.setPrlIlLocName(apPRList.getLocation());
         			mdetails.setPrlQuantity(Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit));
         			mdetails.setPrlUomName(apPRList.getUnit());
         			mdetails.setPrlAmount(Common.convertStringMoneyToDouble(apPRList.getAmount(), precisionUnit));

         			ApModPurchaseRequisitionLineDetails mPrDetails = ejbPR.getApCnvByPrlCode(mdetails, apPRList.getPurchaseRequisitionLineCode(), user.getCmpCode());

         			ArrayList list = mPrDetails.getCnvList();

         			Iterator listIter = list.iterator();

         			mdetails.setCnvList(mPrDetails.getCnvList());
         			mdetails.setRegenerateCanvass(mPrDetails.getRegenerateCanvass());

         			boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
              	   if (isTraceMisc == true){
              		   mdetails.setTraceMisc((byte) 1);

              		 String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
              	   String misc=apPRList.getMisc();
              	   String[] arrayMisc = misc.split("_");
              	   int qty= Common.convertStringToInt(arrayMisc[0]);

              	   misc = arrayMisc[1];
              	   arrayMisc = misc.split("@");
              	   propertyCode = arrayMisc[0].split(",");

              	   misc = arrayMisc[1];
              	   arrayMisc = misc.split("<");
              	   serialNumber = arrayMisc[0].split(",");

              	   misc = arrayMisc[1];
              	   arrayMisc = misc.split(">");
              	   specs = arrayMisc[0].split(",");

              	   misc = arrayMisc[1];
              	   arrayMisc = misc.split(";");
              	   custodian = arrayMisc[0].split(",");
              	   //expiryDate = arrayMisc[1].split(",");

              	   misc = arrayMisc[1];
              	   arrayMisc = misc.split("%");
              	   expiryDate = arrayMisc[0].split(",");
              	   tgDocumentNumber = arrayMisc[1].split(",");
              	   ArrayList tagList = new ArrayList();

	           	   //if(apPRList.getTagListSize()>0){
	           	   //TODO:save and submit taglist
	           	   for (int y = 0; y<apPRList.getTagListSize(); y++) {

	           		   //ApPurchaseRequisitionEntryTagList apRiLTgList =apPRList.getTagListByIndex(y);
	           		   //System.out.println("dumaan?"+apRiLTgList.getCustodian());
	           		   InvModTagListDetails tgDetails = new InvModTagListDetails();
	           		   if (custodian[y].equals("")&&specs[y].equals("")
	        				   &&expiryDate[y].equals("")&&propertyCode[y].equals("")
	        				   &&serialNumber[y].equals("")){

	           			   break;
	           		   }

	           		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
	           		   tgDetails.setTgSpecs(specs[y].trim());
	           		   tgDetails.setTgSerialNumber(serialNumber[y].trim());
	           		   tgDetails.setTgCustodian(custodian[y].trim());
	           		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));

	           		   System.out.println(tgDetails.getTgCustodian() + "<== getTgCustodian RI Entry Action");
	           		   System.out.println(tgDetails.getTgSpecs() + "<== getTgSpecs RI Entry Action");
	           		   System.out.println(tgDetails.getTgPropertyCode() + "<== getTgPropertyCode RI Entry Action");
	           		   System.out.println(tgDetails.getTgSerialNumber() + "<== getTgSerialNumber RI Entry Action");
	           		   tagList.add(tgDetails);
	           		   mdetails.setPrlTagList(tagList);
	           	   }
              	   }


         			prlList.add(mdetails);

         		}

         		try {

         			Integer purchaseRequisitionCode = ejbPR.saveApPrEntry(details, actionForm.getNotifiedUser1(), actionForm.getTaxCode(), actionForm.getCurrency(), prlList, false,
         					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			actionForm.setPurchaseRequisitionCode(purchaseRequisitionCode);

         		} catch (GlobalRecordAlreadyDeletedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

         		} catch (GlobalDocumentNumberNotUniqueException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.documentNumberNotUnique"));

         		} catch (GlobalConversionDateNotExistException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

         		} catch (GlobalTransactionAlreadyVoidException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyVoid"));

         		} catch (GlobalInvItemLocationNotFoundException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.noItemLocationFound", ex.getMessage()));
         		} catch (GlobalRecordInvalidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("purchaseRequisitionEntry.error.typeInvalid", ex.getMessage()));

         		} catch (GlobalInsufficientBudgetQuantityException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("purchaseRequisitionEntry.error.insufficientBudgetQuantity", ex.getMessage()));

         		} catch (GlobalTransactionAlreadyApprovedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyApproved"));

         		} catch (GlobalTransactionAlreadyPendingException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPending"));

         		} catch (GlobalTransactionAlreadyPostedException ex) {

         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("purchaseRequisitionEntry.error.transactionAlreadyPosted"));

         		} catch (GlobalNoApprovalApproverFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalApproverFound"));

               	} catch (GlobalNoApprovalRequesterFoundException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               				new ActionMessage("purchaseRequisitionEntry.error.noApprovalRequesterFound"));

         		} catch (EJBException ex) {
         			if (log.isInfoEnabled()) {

         				log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         						" session: " + session.getId());
         			}

         			return(mapping.findForward("cmnErrorPage"));
         		}

         		if (!errors.isEmpty()) {

         			saveErrors(request, new ActionMessages(errors));
         			return (mapping.findForward("apPurchaseRequisitionEntry"));

         		}

         	}

         	//generate po

         	try {

         		poSize = ejbPR.generateApPo(actionForm.getPurchaseRequisitionCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         	} catch (EJBException ex) {
         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         		}

         		return(mapping.findForward("cmnErrorPage"));
         	}

/*******************************************************
   -- Ap PR Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap PR Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {

         	int listSize = actionForm.getApPRListSize();

            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	ApPurchaseRequisitionEntryList apPRList = new ApPurchaseRequisitionEntryList(actionForm, null,
	        			new Integer(x).toString(), null, null, null, null, null, null, null, null);

	        	apPRList.setLocationList(actionForm.getLocationList());

	        	actionForm.saveApPRList(apPRList);

	        }

	        return(mapping.findForward("apPurchaseRequisitionEntry"));

/*******************************************************
   -- Ap PR Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {

         	for (int i = 0; i<actionForm.getApPRListSize(); i++) {

         		ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

           	   if (apPRList.getDeleteCheckbox()) {

           	   	   actionForm.deleteApPRList(i);
           	   	   i--;
           	   }

            }

         	for (int i = 0; i<actionForm.getApPRListSize(); i++) {

         		ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);

         		apPRList.setLineNumber(String.valueOf(i+1));

         	}

	        return(mapping.findForward("apPurchaseRequisitionEntry"));

/*******************************************************
   -- Ap PR Item Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("apPRList[" +
         		actionForm.getRowSelected() + "].isItemEntered"))) {

         	ApPurchaseRequisitionEntryList apPRList =
         		actionForm.getApPRByIndex(actionForm.getRowSelected());
         	System.out.println("Item Enter after lookup button");
         	try {
         		//TODO:
         		// populate unit field class

         		ArrayList uomList = new ArrayList();
         		uomList = ejbPR.getInvUomByIiName(apPRList.getItemName(), user.getCmpCode());

         		apPRList.clearUnitList();
         		apPRList.setUnitList(Constants.GLOBAL_BLANK);

         		Iterator i = uomList.iterator();
         		while (i.hasNext()) {

         			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

         			apPRList.setUnitList(mUomDetails.getUomName());

         			if (mUomDetails.isDefault()) {

         				apPRList.setUnit(mUomDetails.getUomName());

         			}

         			apPRList.setQuantity(Common.convertDoubleToStringMoney(1, precisionUnit));

         		}
         		apPRList.clearTagList();
         		boolean isTraceMisc = ejbPR.getInvTraceMisc(apPRList.getItemName(), user.getCmpCode());
         		String misc = apPRList.getQuantity() + "_";
         		String propertyCode = "";
         		String specs = "";
         		String serialNumber = "";
         		String custodian = "";
         		String expiryDate = "";
         		String tgDocumentNumber = "";
         		if (isTraceMisc == true){

         			apPRList.setIsTraceMisc(isTraceMisc);
         			System.out.println(apPRList.getTagListSize() + "<== apPRList.getTagListSize before");
	         		//create new blank tagList under ApPurchaseRequisitionEntryList

	         		//if(apPRList.getTagListSize()==0){
		         		for (int x=1; x<=20; x++) {

							//ApPurchaseRequisitionEntryTagList apTagList = new ApPurchaseRequisitionEntryTagList(apPRList, "", "", "", "", "");
							//apPRList.saveTagList(apTagList);
							if (expiryDate == null){
                   		   		expiryDate = "";
                   		   	}
                   		   	propertyCode += " ,";
                   		    specs += " ,";
                   		    serialNumber += " ,";
                   		    custodian += " ,";
                   		    expiryDate += " ,";
                   		    tgDocumentNumber += " ,";
						}
        			 //}
		         		misc = 	apPRList.getQuantity()+"_"+
    							propertyCode.substring(0,propertyCode.length()-1)+"@"+
    					   		serialNumber.substring(0,serialNumber.length()-1)+"<"+
    					   		specs.substring(0,specs.length()-1)+">"+
    					   		custodian.substring(0,custodian.length()-1)+";"+
    					   		expiryDate.substring(0,expiryDate.length()-1)+"%"+
    					   		tgDocumentNumber.substring(0,tgDocumentNumber.length()-1);

	         		System.out.println(apPRList.getTagListSize() + "<== apPRList.getTagListSize after");
         		}
         		apPRList.setMisc(misc);

         		if (!Common.validateRequired(apPRList.getItemName()) &&
         			!Common.validateRequired(apPRList.getLocation()) &&
					!Common.validateRequired(apPRList.getUnit())) {

	         		//populate amount field

	         		double quantity = Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit);
	         		//double unitCost = ejbPR.getInvIiUnitCostByIiNameAndUomName(apPRList.getItemName(), apPRList.getLocation(), apPRList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	         		
	         		Date currentDate =  Common.convertStringToSQLDate(actionForm.getDate());
	         		
	         		double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apPRList.getItemName(), apPRList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                    
                    unitCost = Common.currencyConversion(unitCost, Common.convertStringToDouble(actionForm.getConversionRate()), precisionUnit);
	         
	         	//	double unitCost = ejbPR.getAverageCost(apPRList.getItemName(), apPRList.getLocation(),apPRList.getUnit(),currentDate, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	         		apPRList.setAmount(Common.convertDoubleToStringMoney(Common.roundIt(unitCost * quantity, precisionUnit), precisionUnit));

         		}
         		// populate budget quantity field for item selected
         		double quantityForCurrentMonth = ejbPR.getInvTbForItemForCurrentMonth(apPRList.getItemName(), user.getUserName(), user.getCmpCode());
         		apPRList.setBudgetQuantity(Common.convertDoubleToStringMoney(quantityForCurrentMonth, precisionUnit));

         	} catch (EJBException ex) {


         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apPurchaseRequisitionEntry"));

/*******************************************************
 -- Ap PR Unit Enter Action --
 *******************************************************/

         } else if(!Common.validateRequired(request.getParameter("apPRList[" +
         		actionForm.getRowSelected() + "].isUnitEntered")) ||
				   !Common.validateRequired(request.getParameter("apPRList[" +
		         		actionForm.getRowSelected() + "].isLocationEntered"))) {

         	ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(actionForm.getRowSelected());
         	try {

         			if (!Common.validateRequired(apPRList.getItemName()) &&
             			!Common.validateRequired(apPRList.getLocation()) &&
    					!Common.validateRequired(apPRList.getUnit())) {
	        			//populate amount field
         				apPRList.setQuantity(Common.convertDoubleToStringMoney(1, precisionUnit));
         				double quantity = Common.convertStringMoneyToDouble(apPRList.getQuantity(), precisionUnit);
	             		double amount = ejbPR.getInvIiUnitCostByIiNameAndUomName(apPRList.getItemName(), apPRList.getLocation(), apPRList.getUnit(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	             		apPRList.setAmount(Common.convertDoubleToStringMoney(amount * quantity, precisionUnit));

         			}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apPurchaseRequisitionEntry"));



/*******************************************************
    -- Conversion Date Enter Action --
 *******************************************************/
      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

      	try {

      		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
      				ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
      						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

      	} catch (GlobalConversionDateNotExistException ex) {

      		errors.add(ActionMessages.GLOBAL_MESSAGE,
      				new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

      	} catch (EJBException ex) {

      		if (log.isInfoEnabled()) {

      			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());

      		}

      		return(mapping.findForward("cmnErrorPage"));

      	}

      	if (!errors.isEmpty()) {

      		saveErrors(request, new ActionMessages(errors));

      	}

      	return(mapping.findForward("apPurchaseRequisitionEntry"));


/*******************************************************
   -- Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

         	try {

         		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
         		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));
         		
         		 for (int i = 0; i<actionForm.getApPRListSize(); i++) {
                   
                    ApPurchaseRequisitionEntryList apPRList = actionForm.getApPRByIndex(i);
                    
                    if(Common.validateRequired(apPRList.getItemName())){
                         continue;
                    }
           
                    double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apPRList.getItemName(), apPRList.getUnit(), Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                    
                    unitCost = Common.currencyConversion(unitCost, Common.convertStringToDouble(actionForm.getConversionRate()), precisionUnit);
                    
         		    apPRList.setAmount(Common.convertDoubleToStringMoney(unitCost, precisionUnit)); 
         		
         		 }
         		
	

         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("purchaseRequisitionEntry.error.conversionDateNotExist"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));

         	}

         	return(mapping.findForward("apPurchaseRequisitionEntry"));

         }

/*******************************************************
   -- Ap PR Load Action --
*******************************************************/

         if (frParam != null) {


         	if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apPurchaseRequisitionEntry"));

            }

            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }

            try {

            	ArrayList list = null;
            	Iterator i = null;



            	actionForm.clearCurrencyList();

            	list = ejbPR.getGlFcAllWithDefault(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

            			actionForm.setCurrencyList(mFcDetails.getFcName());


            		}



            		actionForm.setCurrency(defaultPrCurrency);

            	}

            	actionForm.clearNotifiedUser1List();

            	list = ejbPR.getAdUsrAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setNotifiedUser1List(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			String userName = (String)i.next();
            			actionForm.setNotifiedUser1List(userName);

            		}

            	}



            	actionForm.clearTaxCodeList();

            	list = ejbPR.getApTcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			actionForm.setTaxCodeList((String)i.next());

            		}
            		actionForm.setTaxCode(defaultPrTax);

            	}

            	actionForm.clearUserList();

            	String userDepartment = ejbPR.getAdUsrDepartment(user.getUserName(), user.getCmpCode());
            	ArrayList userList = ejbPR.getAdUsrByDeptartmentAll(userDepartment, user.getCmpCode());

            	if (userList == null || userList.size() == 0) {

            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator x = userList.iterator();

            		while (x.hasNext()) {

            			actionForm.setUserList((String)x.next());

            		}

            	}


            	actionForm.clearLocationList();

            	list = ejbPR.getInvLocAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			actionForm.setLocationList((String)i.next());

            		}

            	}

            	actionForm.clearPrTypeList();

            	list = ejbPR.getAdPrType(user.getCmpCode());

            	System.out.println("action_form size="+list.size()+list.toString());

            	if (list == null || list.size() == 0) {

            		actionForm.setPrTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		System.out.println("YES");
            	} else {
            		System.out.println("NO");
            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setPrTypeList((String)i.next());

            		}

            	}
            	actionForm.clearApAPRList();
            	actionForm.clearApPRList();

            	if (request.getParameter("forward") != null || isInitialPrinting) {

            		if (request.getParameter("forward") != null) {

            			actionForm.setPurchaseRequisitionCode(new Integer(request.getParameter("purchaseRequisitionCode")));

            		}

            		ApModPurchaseRequisitionDetails mdetails = ejbPR.getApPrByPrCode(actionForm.getPurchaseRequisitionCode(), user.getCmpCode());
            		boolean brnchFound = false;
            		for(int j=0; j<user.getBrnchCount(); j++){
        				com.struts.util.Branch brnch = user.getBranch(j);
        				if(new Integer(brnch.getBrCode()).equals(mdetails.getPrBrCode())){
        					brnchFound = true;
        					user.setCurrentBranch(brnch);
        					session.setAttribute(Constants.USER_KEY, user);
        					break;
        				}
        			}

            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getPrDate()));
            		actionForm.setDocumentNumber(mdetails.getPrNumber());
            		actionForm.setReferenceNumber(mdetails.getPrReferenceNumber());
            		actionForm.setDescription(mdetails.getPrDescription());
            		actionForm.setPurchaseRequisitionVoid(Common.convertByteToBoolean(mdetails.getPrVoid()));
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getPrConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getPrConversionRate(), Constants.CONVERSION_RATE_PRECISION));
            		actionForm.setApprovalStatus(mdetails.getPrApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getPrReasonForRejection());
            		actionForm.setCanvassReasonForRejection(mdetails.getPrCanvassReasonForRejection());
            		actionForm.setPosted(mdetails.getPrPosted() == 1 ? "YES" : "NO");
            		actionForm.setGenerated(mdetails.getPrGenerated() == 1 ? "YES" : "NO");
            		actionForm.setCanvassApprovalStatus(mdetails.getPrCanvassApprovalStatus());
            		actionForm.setCanvassPosted(mdetails.getPrCanvassPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getPrCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getPrDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getPrLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getPrDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getPrApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getPrDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getPrPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getPrDatePosted()));
            		actionForm.setDeliveryPeriod(Common.convertSQLDateToString(mdetails.getPrDeliveryPeriod()));
            		actionForm.setCanvassApprovedRejectedBy(mdetails.getPrCanvassApprovedRejectedBy());
            		actionForm.setCanvassDateApprovedRejected(Common.convertSQLDateToString(mdetails.getPrCanvassDateApprovedRejected()));
            		actionForm.setCanvassPostedBy(mdetails.getPrCanvassPostedBy());
            		actionForm.setCanvassDatePosted(Common.convertSQLDateToString(mdetails.getPrCanvassDatePosted()));
            		actionForm.setTag(mdetails.getPrTagStatus());
            		actionForm.setPrType(mdetails.getPrType());
                    actionForm.setReminderSchedule(mdetails.getPrSchedule());
            		actionForm.setNextRunDate(Common.convertSQLDateToString(mdetails.getPrNextRunDate()));
            		actionForm.setLastRunDate(Common.convertSQLDateToString(mdetails.getPrLastRunDate()));
            		actionForm.setNotifiedUser1(mdetails.getPrAdNotifiedUser1());
            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getPrAmount(), precisionUnit));

            		if(!actionForm.getCurrencyList().contains(mdetails.getPrFcName())) {

            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearCurrencyList();

            			}

            			actionForm.setCurrencyList(mdetails.getPrFcName());

            		}
            		System.out.println("CURRENCY="+mdetails.getPrFcName());
            		actionForm.setCurrency(mdetails.getPrFcName());
            		//if (request.getParameter("conversionRate")!=null) actionForm.setConversionRate(request.getParameter("conversionRate"));
            		//if (request.getParameter("currency")!=null) actionForm.setCurrency(request.getParameter("currency"));
            		System.out.println("CURRENCY 2="+actionForm.getCurrency());
            		if (!actionForm.getTaxCodeList().contains(mdetails.getPrTcName())) {

            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearTaxCodeList();

            			}

            			actionForm.setTaxCodeList(mdetails.getPrTcName());

            		}
            		actionForm.setTaxCode(mdetails.getPrTcName());

            		list = mdetails.getPrAPRList();

            		i = list.iterator();

            		while (i.hasNext()) {

            			AdModApprovalQueueDetails mPrAPRDetails = (AdModApprovalQueueDetails)i.next();

            			ApPurchaseRequisitionApproverList apPrAPRList = new ApPurchaseRequisitionApproverList(actionForm,
            					Common.convertSQLDateAndTimeToString(mPrAPRDetails.getAqApprovedDate()), mPrAPRDetails.getAqApproverName(), mPrAPRDetails.getAqStatus());

            			actionForm.saveApAPRList(apPrAPRList);
            		}

            		list = mdetails.getPrPrlList();

            		i = list.iterator();

            		while (i.hasNext()) {

            			ApModPurchaseRequisitionLineDetails mPrlDetails = (ApModPurchaseRequisitionLineDetails)i.next();

            			ApPurchaseRequisitionEntryList apPrlList = new ApPurchaseRequisitionEntryList(actionForm,
            					mPrlDetails.getPrlCode(), Common.convertShortToString(mPrlDetails.getPrlLine()),
								mPrlDetails.getPrlIlIiName(), mPrlDetails.getPrlIlLocName(),
								Common.convertDoubleToStringMoney(mPrlDetails.getPrlQuantity(), precisionUnit),
								mPrlDetails.getPrlUomName(), mPrlDetails.getPrlIlIiDescription(),
								Common.convertDoubleToStringMoney(mPrlDetails.getPrlAmount(), precisionUnit),
								Common.convertDoubleToStringMoney(mPrlDetails.getPrlBdgtQuantity(), precisionUnit),
								mPrlDetails.getPrlMisc());

            			apPrlList.setLocationList(actionForm.getLocationList());

            			boolean isTraceMisc = ejbPR.getInvTraceMisc(mPrlDetails.getPrlIlIiName(), user.getCmpCode());
            			apPrlList.setIsTraceMisc(isTraceMisc);
            			apPrlList.clearTagList();
            			System.out.println(mPrlDetails.getPrlTagList() + "<== mPrlDetails.getPrlTagList");



        				//TODO:save tagList


        				if (mPrlDetails.getTraceMisc()==1){


        					ArrayList tagList = mPrlDetails.getPrlTagList();


                        	if(tagList.size() > 0) {
                        		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mPrlDetails.getPrlQuantity()));

                        		System.out.println(misc + "<== misc");
                        		apPrlList.setMisc(misc);

                        	}else {
                          		if(mPrlDetails.getPrlMisc()==null) {

                          			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mPrlDetails.getPrlQuantity()));
                          			apPrlList.setMisc(misc);
                          			System.out.println(misc + "<== misc");
                          		}
                          	}







                      //  	mPrlDetails.setPrlMisc(misc);
        				//	apPrlList.setMisc(mPrlDetails.getPrlMisc());





        				}

        				System.out.println(mPrlDetails.getPrlQuantity()+ "<== mPrlDetails.getPrlQuantity()");
            			apPrlList.setBudgetQuantity(Common.convertDoubleToStringMoney(ejbPR.getInvTbForItemForCurrentMonth(mPrlDetails.getPrlIlIiName(), user.getUserName(), user.getCmpCode()), precisionUnit));
            			apPrlList.clearUnitList();

            			ArrayList unitList = ejbPR.getInvUomByIiName(mPrlDetails.getPrlIlIiName(), user.getCmpCode());

            			Iterator unitListIter = unitList.iterator();

            			while (unitListIter.hasNext()) {

            				InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
            				apPrlList.setUnitList(mUomDetails.getUomName());

            			}



            			actionForm.saveApPRList(apPrlList);

            		}

            		int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

            		for (int x = list.size() + 1; x <= remainingList; x++) {

            			ApPurchaseRequisitionEntryList apPRList = new ApPurchaseRequisitionEntryList(actionForm,
            					null, String.valueOf(x), null, null, null, null, null, null, null, null);

            			apPRList.setLocationList(actionForm.getLocationList());

            			apPRList.setUnitList(Constants.GLOBAL_BLANK);
            			apPRList.setUnitList("Select Item First");

            			actionForm.saveApPRList(apPRList);

            		}

            		System.out.println("D@222222222");
            		this.setFormProperties(actionForm, user.getCompany());
					if (request.getParameter("child") == null) {

						return (mapping.findForward("apPurchaseRequisitionEntry"));

        			} else {
        				System.out.print("CHILD");
        				return (mapping.findForward("apPurchaseRequisitionEntryChild"));

        			}


            	}
//            	actionForm.clearPrTypeList();
//            	list = ejbPR.getAdPrType(user.getCmpCode());
//                i = list.iterator();
//
//                if (list == null || list.size() == 0) {
//                  	 actionForm.setPrTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
//                } else {
//                  	 i = list.iterator();
//                  	 while (i.hasNext()) {
//       					    actionForm.setPrTypeList((String)i.next());
//                  	 }
//                }
            	// Populate line when not forwarding



            	for (int x = 1; x <= journalLineNumber; x++) {

            		ApPurchaseRequisitionEntryList apPRList = new ApPurchaseRequisitionEntryList(actionForm, null,
            				new Integer(x).toString(), null, null, null, null, null, null, null, null);

            		apPRList.setLocationList(actionForm.getLocationList());
            		apPRList.setUnitList(Constants.GLOBAL_BLANK);
            		apPRList.setUnitList("Select Item First");
            		//apPRList.setBudgetQuantity(Common.convertDoubleToStringMoney(actionForm.getBudgetQuantity(), precisionUnit));
            		actionForm.saveApPRList(apPRList);

            		ApPurchaseRequisitionApproverList apAPRList = new ApPurchaseRequisitionApproverList(actionForm, null, "APPROVER", "STATUS");
            		actionForm.saveApAPRList(apAPRList);

            	}

            } catch(GlobalNoRecordFoundException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.recordAlreadyDeleted"));

            } catch(EJBException ex) {

            	if (log.isInfoEnabled()) {

            		log.info("EJBException caught in ApPurchaseRequisitionEntryAction.execute(): " + ex.getMessage() +
            				" session: " + session.getId());
            	}

            	return(mapping.findForward("cmnErrorPage"));

            }

            if (!errors.isEmpty()) {
            	saveErrors(request, new ActionMessages(errors));
            } else {
            	if (request.getParameter("saveSubmitButton") != null &&
            			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            		try {
            			ArrayList list = ejbPR.getAdApprovalNotifiedUsersByPrCode(actionForm.getPurchaseRequisitionCode(), user.getCmpCode());
            			if (list.isEmpty()) {
            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentSentForPosting"));
            			} else if (list.contains("DOCUMENT POSTED")) {
            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentPosted"));
            			} else {
            				Iterator i = list.iterator();
            				String APPROVAL_USERS = "";
            				while (i.hasNext()) {
            					APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
            					if (i.hasNext()) {
            						APPROVAL_USERS = APPROVAL_USERS + ", ";
            					}
            				}
            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));
            			}

            		} catch(EJBException ex) {

            		}

            		saveMessages(request, messages);
            		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            	}else if (request.getParameter("saveCanvassButton") != null &&
            			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            		try {
            			ArrayList list = ejbPR.getAdApprovalNotifiedUsersByPrCodeCanvass(actionForm.getPurchaseRequisitionCode(), user.getCmpCode());
            			if (list.isEmpty()) {
            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentSentForPosting"));
            			} else if (list.contains("DOCUMENT POSTED")) {
            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentPosted"));
            			} else {
            				Iterator i = list.iterator();
            				String APPROVAL_USERS = "";
            				while (i.hasNext()) {
            					APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
            					if (i.hasNext()) {
            						APPROVAL_USERS = APPROVAL_USERS + ", ";
            					}
            				}
            				messages.add(ActionMessages.GLOBAL_MESSAGE,
            						new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));
            			}

            		} catch(EJBException ex) {

            		}

            		saveMessages(request, messages);
            		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);



            	} else if ((request.getParameter("saveAsDraftButton") != null || request.getParameter("generatePoButton") != null) &&
            			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            		if(request.getParameter("generatePoButton") != null) {

            			messages.add(ActionMessages.GLOBAL_MESSAGE,	new ActionMessage("purchaseRequisitionEntry.prompt.purchaseOrdersGenerated", poSize));
                		saveMessages(request, messages);

            		}

            	}

            }

            actionForm.reset(mapping, request);

            actionForm.setPurchaseRequisitionCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            //actionForm.setDeliveryPeriod(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setConversionRate("1.000000");
            actionForm.setPosted("NO");
            actionForm.setGenerated("NO");
            actionForm.setApprovalStatus(null);

            actionForm.setCanvassPosted("NO");
            actionForm.setCanvassApprovalStatus(null);

            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

            System.out.print("WEEEE");
            this.setFormProperties(actionForm, user.getCompany());

            return(mapping.findForward("apPurchaseRequisitionEntry"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

            log.info("Exception caught in ApPurchaseRequisitionEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }

   private void setFormProperties(ApPurchaseRequisitionEntryForm actionForm, String adCompany) {


   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

   		if (actionForm.getPosted().equals("NO")) {
   			System.out.println("=======NOT POSTED");

   			if (actionForm.getPurchaseRequisitionCode() == null) {

   				System.out.println("=======NULL");
   				actionForm.setEnableFields(true);
   				actionForm.setEnablePurchaseRequisitionVoid(false);
   				actionForm.setShowSaveButton(true);
   				actionForm.setShowDeleteButton(false);
   				actionForm.setShowGeneratePoButton(false);
   				actionForm.setShowSaveCanvassButton(false);
   				actionForm.setShowAddLinesButton(true);
   				actionForm.setShowDeleteLinesButton(true);

   			} else if (actionForm.getPurchaseRequisitionCode() != null) {
   				System.out.println("=======NOT NULL");
   				actionForm.setEnableFields(true);
   				actionForm.setShowSaveButton(true);
   				actionForm.setShowDeleteButton(true);
   				actionForm.setShowGeneratePoButton(false);
   				actionForm.setShowSaveCanvassButton(false);


   				if (actionForm.getApprovalStatus() != null && actionForm.getApprovalStatus().equals("PENDING")){
   					System.out.println(actionForm.getApprovalStatus());
   					System.out.println("=======PENDING");
   					actionForm.setEnableFields(false);
   					actionForm.setShowSaveButton(false);
   	   				actionForm.setShowDeleteButton(false);
   	   				actionForm.setShowSaveCanvassButton(false);
   				}
   				actionForm.setEnablePurchaseRequisitionVoid(false);


   				actionForm.setShowAddLinesButton(true);
   				actionForm.setShowDeleteLinesButton(true);

   			} else {
   				System.out.println("=======ELSE");
   				actionForm.setEnableFields(false);
   				actionForm.setEnablePurchaseRequisitionVoid(false);
   				actionForm.setShowSaveButton(false);
   				actionForm.setShowDeleteButton(true);

   				actionForm.setShowGeneratePoButton(false);
   				actionForm.setShowSaveCanvassButton(true);

   				actionForm.setShowAddLinesButton(false);
   				actionForm.setShowDeleteLinesButton(false);

   			}




   		} else {


   			System.out.println("VOID"+actionForm.getPurchaseRequisitionVoid());
   			System.out.println("========POSTED");
   			System.out.println(actionForm.getCanvassApprovalStatus());

   			if(actionForm.getCanvassPosted().equals("NO") ){
   				System.out.println("=====CANVASS NOT POSTED"+actionForm.getCanvassPosted());
   				System.out.println("=====CANVASS APPRoVAL"+actionForm.getCanvassApprovalStatus());

   				actionForm.setShowGeneratePoButton(false);
   				actionForm.setShowSaveCanvassButton(true);
   				if(actionForm.getCanvassApprovalStatus() != null && actionForm.getCanvassApprovalStatus().equals("PENDING")){

   					actionForm.setShowSaveCanvassButton(false);
   				}

   			} else {

   				System.out.println("=====CANVASS POSTED");

   				if(actionForm.getGenerated().equals("NO")){
   					System.out.println("=====GENERATED NO");
   	   				actionForm.setShowGeneratePoButton(true);
   	   			}else if(actionForm.getGenerated().equals("YES")){
   	   			System.out.println("=====GENERATED YES");
   	   			actionForm.setShowGeneratePoButton(false);
   	   			}

   				actionForm.setShowSaveCanvassButton(false);


   			}



   			actionForm.setEnableFields(false);
   			actionForm.setEnablePurchaseRequisitionVoid(true);
   			if(actionForm.getPurchaseRequisitionVoid() == false){
   				actionForm.setShowSaveButton(true);
   			}else{
   				actionForm.setShowSaveButton(false);
   			}
   			actionForm.setShowDeleteButton(false);

   			actionForm.setShowAddLinesButton(false);
   			actionForm.setShowDeleteLinesButton(false);

   		}

   	} else {

   		actionForm.setEnableFields(false);
   		actionForm.setEnablePurchaseRequisitionVoid(false);
   		actionForm.setShowSaveButton(false);
   		actionForm.setShowDeleteButton(true);
   		actionForm.setShowGeneratePoButton(false);
   		actionForm.setShowSaveCanvassButton(false);
   		actionForm.setShowAddLinesButton(false);
   		actionForm.setShowDeleteLinesButton(false);

   	}
   	// view attachment

 		if (actionForm.getPurchaseRequisitionCode() != null) {

 			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

             String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/PR/";
             String attachmentFileExtensionData = appProperties.getMessage("app.attachmentFileExtensionData");
             String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
             String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			if(new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1.txt").exists()) {
 				actionForm.setShowViewAttachmentButton1(true);

 			}else {
 				actionForm.setShowViewAttachmentButton1(false);
 			}


 			if(new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-2.txt").exists()) {
 				actionForm.setShowViewAttachmentButton2(true);

 			}else {
 				actionForm.setShowViewAttachmentButton2(false);
 			}

 			if(new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-3.txt").exists()) {
 				actionForm.setShowViewAttachmentButton3(true);

 			}else {
 				actionForm.setShowViewAttachmentButton3(false);
 			}
 			if(new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-4.txt").exists()) {
 				actionForm.setShowViewAttachmentButton4(true);

 			}else {
 				actionForm.setShowViewAttachmentButton4(false);
 			}

 		} else {

 			actionForm.setShowViewAttachmentButton1(false);
 			actionForm.setShowViewAttachmentButton2(false);
 			actionForm.setShowViewAttachmentButton3(false);
 			actionForm.setShowViewAttachmentButton4(false);

 		}


   }

}