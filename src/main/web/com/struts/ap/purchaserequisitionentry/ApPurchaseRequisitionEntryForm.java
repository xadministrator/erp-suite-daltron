package com.struts.ap.purchaserequisitionentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApPurchaseRequisitionEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
	private Integer purchaseRequisitionCode = null;
	private String documentNumber = null;
	private String date = null;
	private String deliveryPeriod = null;
	private String referenceNumber = null;


	private String tag = null;
	private ArrayList tagTypeList = new ArrayList();

	private String prType = null;
	private ArrayList prTypeList = new ArrayList();

	private String misc1 = null;
	private ArrayList misc1List = new ArrayList();
	private String misc2 = null;
	private ArrayList misc2List = new ArrayList();
	private String misc3 = null;
	private ArrayList misc3List = new ArrayList();
	private String misc4 = null;
	private ArrayList misc4List = new ArrayList();
	private String misc5 = null;
	private ArrayList misc5List = new ArrayList();
	private String misc6 = null;
	private ArrayList misc6List = new ArrayList();


	private String amount = null;
	private double budgetQuantity = 0;
	private boolean totalBasedPo = false;
	private String description = null;
	private String taxCode = null;
	private ArrayList taxCodeList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String conversionDate = null;
	private String conversionRate = null;

	private String approvalStatus = null;
	private String posted = null;
	private String generated = null;

	private String canvassApprovalStatus = null;
	private String canvassPosted = null;

	private String reasonForRejection = null;
	private String canvassReasonForRejection = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;


	private String reminderSchedule = null;
    private ArrayList reminderScheduleList = new ArrayList();
    private String nextRunDate = null;
    private String lastRunDate = null;
    private String notifiedUser1 = null;
    private ArrayList notifiedUser1List = new ArrayList();


	private String canvassApprovedRejectedBy = null;
	private String canvassDateApprovedRejected = null;
	private String canvassPostedBy = null;
	private String canvassDatePosted = null;

	private boolean purchaseRequisitionVoid = false;

	private ArrayList apPRList = new ArrayList();
	private ArrayList apAPRList = new ArrayList();
	private ArrayList userList = new ArrayList();

	private boolean enableFields = false;
	private boolean enablePurchaseRequisitionVoid = false;
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();

	private String location = null;
	private ArrayList locationList = new ArrayList();

	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showDeleteButton = false;
	private boolean showSaveButton = false;
	private boolean showSaveDraftButton = false;
	private boolean showGeneratePoButton = false;
	private boolean showSaveCanvassButton = false;
	private boolean showPrCost = false;
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;
	private String attachment = null;
	private String attachmentPDF = null;
	private String attachmentDownload = null;
	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;
	private String isConversionDateEntered = null;

	private String report = null;
	private String report2 = null;
	private String reportQuotation = null;

	public String getAttachmentPDF() {

		return attachmentPDF;

	}

   	public void setAttachmentPDF(String attachmentPDF) {

   		this.attachmentPDF = attachmentPDF;

   	}

	public String getAttachment() {

		return attachment;

	}

   	public void setAttachment(String attachment) {

   		this.attachment = attachment;

   	}

   	public String getAttachmentDownload() {

		return attachmentDownload;

	}

   	public void setAttachmentDownload(String attachmentDownload) {

   		this.attachmentDownload = attachmentDownload;

   	}
   	public ArrayList getUserList() {

		return userList;

   	}

   	public void setUserList(String user) {

	   userList.add(user);

   	}

   	public void clearUserList() {

	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);

   	}
   	public FormFile getFilename1() {

	   	  return filename1;

	 }

	 public void setFilename1(FormFile filename1) {

	 	  this.filename1 = filename1;

	 }

	 public FormFile getFilename2() {

	 	  return filename2;

	 }

	 public void setFilename2(FormFile filename2) {

	 	  this.filename2 = filename2;

	 }

	 public FormFile getFilename3() {

	 	  return filename3;

	 }

	 public void setFilename3(FormFile filename3) {

	 	  this.filename3 = filename3;

	 }

	 public FormFile getFilename4() {

	 	  return filename4;

	 }

	 public void setFilename4(FormFile filename4) {

	 	  this.filename4 = filename4;

	 }
	 public boolean getShowViewAttachmentButton1() {

	   	   return showViewAttachmentButton1;

	 }

	 public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

		 this.showViewAttachmentButton1 = showViewAttachmentButton1;

	 }

	 public boolean getShowViewAttachmentButton2() {

		 return showViewAttachmentButton2;

	 }

	 public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

		 this.showViewAttachmentButton2 = showViewAttachmentButton2;

	 }

	 public boolean getShowViewAttachmentButton3() {

		 return showViewAttachmentButton3;

	 }

	 public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

		 this.showViewAttachmentButton3 = showViewAttachmentButton3;

	 }

	 public boolean getShowViewAttachmentButton4() {

		 return showViewAttachmentButton4;

	 }

	 public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

		 this.showViewAttachmentButton4 = showViewAttachmentButton4;

	 }

	public String getTag() {
	   	  return(tag);
	   }

	public void setTag(String tag) {
	   	  this.tag = tag;
	   }

	public ArrayList getTagTypeList() {
	   	  return(tagTypeList);
	   }


	public String getPrType() {
	   	  return(prType);
	   }

	public void setPrType(String prType) {
	   	  this.prType = prType;
	   }

	public ArrayList getPrTypeList() {
	   	  return(prTypeList);
	   }

	public void setPrTypeList(String prTypeList) {

		this.prTypeList.add(prTypeList);

	}
	public void clearPrTypeList() {

		prTypeList.clear();


	}


public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

	public Integer getPurchaseRequisitionCode() {

		return purchaseRequisitionCode;

	}

	public void setPurchaseRequisitionCode(Integer purchaseRequisitionCode) {

		this.purchaseRequisitionCode = purchaseRequisitionCode;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public void setDocumentNumber(String documentNumber) {

		this.documentNumber = documentNumber;

	}

	public double getBudgetQuantity() {

		return budgetQuantity;

	}

	public void setBudgetQuantity(double budgetQuantity) {

		this.budgetQuantity = budgetQuantity;

	}

	public boolean getTotalBasedPo() {

		return totalBasedPo;

	}

	public void setTotalBasedPo(boolean totalBasedPo) {

		this.totalBasedPo = totalBasedPo;

	}
	public String getDate() {

		return date;

	}

	public void setDate(String date) {

		this.date = date;

	}
	public String getDeliveryPeriod() {

		return deliveryPeriod;

	}

	public void setDeliveryPeriod(String deliveryPeriod) {

		this.deliveryPeriod = deliveryPeriod;

	}
	public String getReferenceNumber() {

		return referenceNumber;

	}

	public void setReferenceNumber(String referenceNumber) {

		this.referenceNumber = referenceNumber;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

		this.description = description;

	}

	public String getTaxCode() {

		return taxCode;

	}

	public void setTaxCode(String taxCode) {

		this.taxCode = taxCode;

	}

	public ArrayList getTaxCodeList() {

		return taxCodeList;

	}

	public void setTaxCodeList(String taxCode) {

		taxCodeList.add(taxCode);

	}

	public void clearTaxCodeList() {

		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);

	}

	public String getCurrency() {

		return currency;

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}

	public void setCurrencyList(String currency) {

		currencyList.add(currency);

	}

	public void clearCurrencyList() {

		currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);

	}

	public String getConversionDate() {

		return conversionDate;

	}

	public void setConversionDate(String conversionDate) {

		this.conversionDate = conversionDate;

	}

	public String getConversionRate() {

		return conversionRate;

	}

	public void setConversionRate(String conversionRate) {

		this.conversionRate = conversionRate;

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public void setApprovalStatus(String approvalStatus) {

		this.approvalStatus = approvalStatus;

	}









	public String getPosted() {

		return posted;

	}

	public void setPosted(String posted) {

		this.posted = posted;

	}

	public String getGenerated() {

		return generated;

	}

	public void setGenerated(String generated) {

		this.generated = generated;

	}









	public String getReasonForRejection() {

		return reasonForRejection;

	}

	public void setReasonForRejection(String reasonForRejection) {

		this.reasonForRejection = reasonForRejection;

	}

	public String getCanvassReasonForRejection() {

		return canvassReasonForRejection;

	}

	public void setCanvassReasonForRejection(String canvassReasonForRejection) {

		this.canvassReasonForRejection = canvassReasonForRejection;

	}



	public String getCreatedBy() {

		return createdBy;

	}

	public void setCreatedBy(String createdBy) {

		this.createdBy = createdBy;

	}

	public String getDateCreated() {

		return dateCreated;

	}

	public void setDateCreated(String dateCreated) {

		this.dateCreated = dateCreated;

	}

	public String getLastModifiedBy() {

		return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

		this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

		return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

		this.dateLastModified = dateLastModified;

	}


	public String getApprovedRejectedBy() {

		return approvedRejectedBy;

	}

	public void setApprovedRejectedBy(String approvedRejectedBy) {

		this.approvedRejectedBy = approvedRejectedBy;

	}






	public String getDateApprovedRejected() {

		return dateApprovedRejected;

	}

	public void setDateApprovedRejected(String dateApprovedRejected) {

		this.dateApprovedRejected = dateApprovedRejected;

	}


	public String getPostedBy() {

		return postedBy;

	}

	public void setPostedBy(String postedBy) {

		this.postedBy = postedBy;

	}




	public String getCanvassPosted() {

		return canvassPosted;

	}

	public void setCanvassPosted(String canvassPosted) {

		this.canvassPosted = canvassPosted;

	}


	public String getCanvassPostedBy() {

		return canvassPostedBy;

	}

	public void setCanvassPostedBy(String canvassPostedBy) {

		this.canvassPostedBy = canvassPostedBy;

	}

	public String getCanvassDatePosted() {

		return canvassDatePosted;

	}

	public void setCanvassDatePosted(String canvassDatePosted) {

	this.canvassDatePosted = canvassDatePosted;

	}



	public String getCanvassDateApprovedRejected() {

		return canvassDateApprovedRejected;

	}

	public void setCanvassDateApprovedRejected(String canvassDateApprovedRejected) {

		this.canvassDateApprovedRejected = canvassDateApprovedRejected;

	}

	public String getCanvassApprovedRejectedBy() {

		return canvassApprovedRejectedBy;

	}

	public void setCanvassApprovedRejectedBy(String canvassApprovedRejectedBy) {

		this.canvassApprovedRejectedBy = canvassApprovedRejectedBy;

	}


	public String getCanvassApprovalStatus() {

		return canvassApprovalStatus;

	}

	public void setCanvassApprovalStatus(String canvassApprovalStatus) {

		this.canvassApprovalStatus = canvassApprovalStatus;

	}

	public boolean getShowSaveCanvassButton() {

		return showSaveCanvassButton;

	}

	public void setShowSaveCanvassButton(boolean showSaveCanvassButton) {

		this.showSaveCanvassButton = showSaveCanvassButton;

	}




	public String getDatePosted() {

		return datePosted;

	}

	public void setDatePosted(String datePosted) {

		this.datePosted = datePosted;

	}

	public String getReminderSchedule() {

	   	   return reminderSchedule;

	   }

	   public void setReminderSchedule(String reminderSchedule) {

	   	   this.reminderSchedule = reminderSchedule;

	   }

	   public ArrayList getReminderScheduleList() {

	   	   return reminderScheduleList;

	   }

	   public String getNextRunDate() {

	   	   return nextRunDate;

	   }

	   public void setNextRunDate(String nextRunDate) {

	   	   this.nextRunDate = nextRunDate;

	   }

	   public String getLastRunDate() {

	   	   return lastRunDate;

	   }

	   public void setLastRunDate(String lastRunDate) {

	   	   this.lastRunDate = lastRunDate;

	   }

	   public String getNotifiedUser1() {

	   	  return notifiedUser1;

	   }

	   public void setNotifiedUser1(String notifiedUser1) {

	   	  this.notifiedUser1 = notifiedUser1;

	   }

	   public ArrayList getNotifiedUser1List() {

	   	   return notifiedUser1List;

	   }

	   public void setNotifiedUser1List(String notifiedUser1) {

	   	   notifiedUser1List.add(notifiedUser1);

	   }

	   public void clearNotifiedUser1List() {

	   	   notifiedUser1List.clear();
	   	   notifiedUser1List.add(Constants.GLOBAL_BLANK);

	   }







	public boolean getPurchaseRequisitionVoid() {

		return purchaseRequisitionVoid;

	}

	public void setPurchaseRequisitionVoid(boolean purchaseRequisitionVoid) {

		this.purchaseRequisitionVoid = purchaseRequisitionVoid;

	}

	public Object[] getApPRList(){

		return(apPRList.toArray());

	}

	public void saveApPRList(Object newApPRList){

		apPRList.add(newApPRList);

	}

	public void deleteApPRList(int rowSelected){

		apPRList.remove(rowSelected);

	}

	public int getApPRListSize(){

		return(apPRList.size());

	}

	public void clearApPRList(){

		apPRList.clear();

	}

	public ApPurchaseRequisitionEntryList getApPRByIndex(int index){

		return((ApPurchaseRequisitionEntryList)apPRList.get(index));

	}





	public Object[] getApAPRList(){

		return(apAPRList.toArray());

	}

	public void saveApAPRList(Object newApAPRList){

		apAPRList.add(newApAPRList);

	}

	public void deleteApAPRList(int rowSelected){

		apAPRList.remove(rowSelected);

	}

	public int getApAPRListSize(){

		return(apAPRList.size());

	}

	public void clearApAPRList(){

		apAPRList.clear();

	}

	public ApPurchaseRequisitionApproverList getApAPRByIndex(int index){

		return((ApPurchaseRequisitionApproverList)apAPRList.get(index));

	}


	public boolean getEnableFields() {

		return enableFields;

	}

	public void setEnableFields(boolean enableFields) {

		this.enableFields = enableFields;

	}

	public boolean getEnablePurchaseRequisitionVoid() {

		return enablePurchaseRequisitionVoid;

	}

	public void setEnablePurchaseRequisitionVoid(boolean enablePurchaseRequisitionVoid) {

		this.enablePurchaseRequisitionVoid = enablePurchaseRequisitionVoid;

	}

	public int getRowSelected(){

		return rowSelected;

	}

	public void setRowSelected(Object selectedApPRList, boolean isEdit){

		this.rowSelected = apPRList.indexOf(selectedApPRList);

	}

	public String getUserPermission() {

		return(userPermission);

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

	public String getTxnStatus(){

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);

	}

	public void setTxnStatus(String txnStatus){

		this.txnStatus = txnStatus;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(String location) {

		locationList.add(location);

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getShowAddLinesButton() {

		return showAddLinesButton;

	}

	public void setShowAddLinesButton(boolean showAddLinesButton) {

		this.showAddLinesButton = showAddLinesButton;

	}

	public boolean getShowDeleteLinesButton() {

		return showDeleteLinesButton;

	}

	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

		this.showDeleteLinesButton = showDeleteLinesButton;

	}

	public boolean getShowSaveButton() {

		return showSaveButton;

	}

	public void setShowSaveButton(boolean showSaveButton) {

		this.showSaveButton = showSaveButton;

	}

	public boolean getShowSaveDraftButton() {

	   	   return showSaveDraftButton;

	   }

	   public void setShowSaveDraftButton(boolean showSaveDraftButton) {

	   	   this.showSaveDraftButton = showSaveDraftButton;

	   }

	public boolean getShowGeneratePoButton() {

		return showGeneratePoButton;

	}

	public void setShowGeneratePoButton(boolean showGeneratePoButton) {

		this.showGeneratePoButton = showGeneratePoButton;

	}










	public boolean getShowDeleteButton() {

		return showDeleteButton;

	}

	public void setShowDeleteButton(boolean showDeleteButton) {

		this.showDeleteButton = showDeleteButton;

	}

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getReport2() {

		return report2;

	}

	public void setReport2(String report2) {

		this.report2 = report2;

	}

	public String getReportQuotation() {

		return reportQuotation;

	}

	public void setReportQuotation(String reportQuotation) {

		this.reportQuotation = reportQuotation;

	}

	public String getIsConversionDateEntered(){

		return isConversionDateEntered;

	}

	public boolean getShowPrCost() {

		return showPrCost;

	}

	public void setShowPrCost(boolean showPrCost) {

		this.showPrCost = showPrCost;

	}


	public String getMisc1() {

		return(misc1);

	}

	public void setMisc1(String misc1) {

		this.misc1 = misc1;

	}

	public ArrayList getMisc1List() {

		return(misc1List);

	}



	public void setMisc1List(String misc1) {

		misc1List.add(misc1);


	}

	public void clearMisc1List() {

		misc1List.clear();
		misc1List.add(Constants.GLOBAL_BLANK);
	}


	public String getMisc2() {

		return(misc2);

	}

	public void setMisc2(String misc2) {

		this.misc2 = misc2;

	}

	public ArrayList getMisc2List() {

		return(misc2List);

	}

	public void setMisc2List(String misc2) {

		misc2List.add(misc2);


	}

	public void clearMisc2List() {

		misc2List.clear();
		misc2List.add(Constants.GLOBAL_BLANK);

	}



	public String getMisc3() {

		return(misc3);

	}

	public void setMisc3(String misc3) {

		this.misc3 = misc3;

	}

	public ArrayList getMisc3List() {

		return(misc3List);

	}

	public void setMisc3List(String misc3) {

		misc3List.add(misc3);


	}

	public void clearMisc3List() {

		misc3List.clear();
		misc3List.add(Constants.GLOBAL_BLANK);
	}

	public String getMisc4() {

		return(misc4);

	}

	public void setMisc4(String misc4) {

		this.misc4 = misc4;

	}

	public ArrayList getMisc4List() {

		return(misc4List);

	}

	public void setMisc4List(String misc4) {

		misc4List.add(misc4);


	}

	public void clearMisc4List() {

		misc4List.clear();
		misc4List.add(Constants.GLOBAL_BLANK);
	}

	public String getMisc5() {

		return(misc5);

	}

	public void setMisc5(String misc5) {

		this.misc5 = misc5;

	}

	public ArrayList getMisc5List() {

		return(misc5List);

	}

	public void setMisc5List(String misc5) {

		misc5List.add(misc5);


	}

	public void clearMisc5List() {

		misc5List.clear();
		misc5List.add(Constants.GLOBAL_BLANK);
	}


	public String getMisc6() {

		return(misc6);

	}

	public void setMisc6(String misc6) {

		this.misc6 = misc6;

	}

	public ArrayList getMisc6List() {

		return(misc6List);

	}

	public void setMisc6List(String misc6) {

		misc6List.add(misc6);


	}

	public void clearMisc6List() {

		misc6List.clear();
		misc6List.add(Constants.GLOBAL_BLANK);
	}


	public void reset(ActionMapping mapping, HttpServletRequest request){

		documentNumber = null;
		date = null;
		deliveryPeriod = null;
		referenceNumber = null;
		amount = null;
		description = null;
		conversionDate = null;
		conversionRate = null;
		approvalStatus = null;
		posted = null;
		filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
		canvassApprovalStatus= null;
		canvassPosted = null;
		budgetQuantity = 0;
		totalBasedPo = false;
		reasonForRejection = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;

		approvedRejectedBy = null;
		dateApprovedRejected = null;
		postedBy = null;
		datePosted = null;

		canvassApprovedRejectedBy = null;
		canvassDateApprovedRejected = null;
		canvassPostedBy = null;
		canvassDatePosted = null;

		purchaseRequisitionVoid = false;
		isConversionDateEntered = null;
		tagTypeList.clear();
		tagTypeList.add("REGULAR");
		tagTypeList.add("URGENT");

		 reminderScheduleList.clear();
		 reminderScheduleList.add("NONE");
	     reminderScheduleList.add(Constants.AP_RV_SCHEDULE_DAILY);
	     reminderScheduleList.add(Constants.AP_RV_SCHEDULE_WEEKLY);
	     reminderScheduleList.add(Constants.AP_RV_SCHEDULE_SEMI_MONTHLY);
	     reminderScheduleList.add(Constants.AP_RV_SCHEDULE_MONTHLY);
	     reminderScheduleList.add(Constants.AP_RV_SCHEDULE_QUARTERLY);
	     reminderScheduleList.add(Constants.AP_RV_SCHEDULE_SEMI_ANNUALLY);
	     reminderScheduleList.add(Constants.AP_RV_SCHEDULE_ANNUALLY);
	     reminderSchedule = "NONE";
	     notifiedUser1 = Constants.GLOBAL_BLANK;

	     nextRunDate = null;
	     lastRunDate = null;

		for (int i=0; i<apPRList.size(); i++) {

		  	  ApPurchaseRequisitionEntryList actionList = (ApPurchaseRequisitionEntryList)apPRList.get(i);
		  	  actionList.setIsItemEntered(null);
		  	  actionList.setIsLocationEntered(null);
		  	  actionList.setIsUnitEntered(null);

		  }

		for (int i=0; i<apAPRList.size(); i++) {

		  	  ApPurchaseRequisitionApproverList actionList = (ApPurchaseRequisitionApproverList)apAPRList.get(i);
		  	  actionList.setApproverName(null);
		  	  actionList.setStatus(null);

		  }


	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){

		ActionErrors errors = new ActionErrors();

		if(request.getParameter("saveAsDraftButton") != null || request.getParameter("saveSubmitButton") != null ||
				request.getParameter("generatePoButton") != null || request.getParameter("saveCanvassButton") != null || request.getParameter("printButton") != null ||
				request.getParameter("crPrintButton") != null || request.getParameter("apPRList[" +
		                rowSelected + "].supplierButton") != null) {

			if(Common.validateRequired(description)) {
				errors.add("description",
						new ActionMessage("purchaseRequisitionEntry.error.descriptionRequired"));
			}

			if(Common.validateRequired(date)) {
				errors.add("date",
						new ActionMessage("purchaseRequisitionEntry.error.dateRequired"));
			}

			if(!Common.validateDateFormat(date)){
	            errors.add("date",
		       new ActionMessage("purchaseRequisitionEntry.error.dateInvalid"));
			}

			if(Common.validateRequired(deliveryPeriod)) {
				errors.add("deliveryPeriod",
						new ActionMessage("purchaseRequisitionEntry.error.deliveryPeriodRequired"));
			}

			if(!Common.validateDateFormat(deliveryPeriod)){
	            errors.add("deliveryPeriod",
		       new ActionMessage("purchaseRequisitionEntry.error.deliveryPeriodInvalid"));
			}

			if(Common.validateRequired(taxCode)) {
				errors.add("taxCode",
						new ActionMessage("purchaseRequisitionEntry.error.taxCodeRequired"));
			}

			if(Common.validateRequired(currency)) {
				errors.add("currency",
						new ActionMessage("purchaseRequisitionEntry.error.currencyRequired"));
			}

			if(!Common.validateDateFormat(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("purchaseRequisitionEntry.error.conversionDateInvalid"));
			}

			if(!Common.validateMoneyRateFormat(conversionRate)){
				errors.add("conversionRate",
						new ActionMessage("purchaseRequisitionEntry.error.conversionRateInvalid"));
			}

			int numOfLines = 0;

			Iterator i = apPRList.iterator();

			while (i.hasNext()) {

				ApPurchaseRequisitionEntryList prlList = (ApPurchaseRequisitionEntryList)i.next();

				if (Common.validateRequired(prlList.getItemName()) &&
						Common.validateRequired(prlList.getLocation()) &&
						Common.validateRequired(prlList.getQuantity()) &&
						Common.validateRequired(prlList.getUnit())) continue;

				numOfLines++;

				if(Common.validateRequired(prlList.getItemName()) || prlList.getItemName().equals(Constants.GLOBAL_BLANK)){
					errors.add("itemName",
							new ActionMessage("purchaseRequisitionEntry.error.itemNameRequired", prlList.getLineNumber()));
				}

				if(Common.validateRequired(prlList.getLocation()) || prlList.getLocation().equals(Constants.GLOBAL_BLANK)){
					errors.add("location",
							new ActionMessage("purchaseRequisitionEntry.error.locationRequired", prlList.getLineNumber()));
				}

				if(!Common.validateNumberFormat(prlList.getQuantity())){
					errors.add("quantity",
							new ActionMessage("purchaseRequisitionEntry.error.quantityInvalid", prlList.getLineNumber()));
				}

				if(!Common.validateRequired(prlList.getQuantity()) && Common.convertStringMoneyToDouble(prlList.getQuantity(), (short)3) <= 0) {
					errors.add("quantity",
							new ActionMessage("purchaseRequisitionEntry.error.negativeOrZeroQuantityNotAllowed", prlList.getLineNumber()));
				}

				if(Common.validateRequired(prlList.getUnit()) || prlList.getUnit().equals(Constants.GLOBAL_BLANK)){
					errors.add("unit",
							new ActionMessage("purchaseRequisitionEntry.error.unitRequired", prlList.getLineNumber()));
				}

			}

			if (numOfLines == 0) {

				errors.add("apPRList",
						new ActionMessage("purchaseRequisitionEntry.error.purchaseRequisitionMustHaveItemLine"));

			}

		} else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

			if(!Common.validateDateFormat(conversionDate)){

				errors.add("conversionDate", new ActionMessage("purchaseRequisitionEntry.error.conversionDateInvalid"));

			}

		}

		return errors;

	}



}