package com.struts.ap.purchaserequisitionentry;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryTagList;
import com.struts.util.Constants;

public class ApPurchaseRequisitionEntryList implements Serializable {

	private Integer purchaseRequisitionLineCode = null;
	private String lineNumber = null;
	private String itemName = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String quantity = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String itemDescription = null;
	private String amount = null;
	private String budgetQuantity = null;
	private String misc = null;

	private boolean deleteCheckbox = false;
	private boolean isTraceMisc = false;
	private String supplierButton = null;

	private String isItemEntered = null;
	private String isLocationEntered = null;
	private String isUnitEntered = null;
	private ArrayList tagList = new ArrayList();

	private ApPurchaseRequisitionEntryForm parentBean;

	public ApPurchaseRequisitionEntryList(ApPurchaseRequisitionEntryForm parentBean,
			Integer purchaseRequisitionLineCode, String lineNumber,	String itemName,
			String location, String quantity, String unit, String itemDescription, String amount, String budgetQuantity, String misc) {

		this.parentBean = parentBean;
		this.purchaseRequisitionLineCode = purchaseRequisitionLineCode;
		this.lineNumber = lineNumber;
		this.itemName = itemName;
		this.location = location;
		this.quantity = quantity;
		this.unit = unit;
		this.itemDescription = itemDescription;
		this.amount = amount;
		this.budgetQuantity = budgetQuantity;
		this.misc = misc;
	}

	public Integer getPurchaseRequisitionLineCode() {

		return purchaseRequisitionLineCode;

	}

	public void setPurchaseRequisitionLineCode(Integer purchaseRequisitionLineCode) {

		this.purchaseRequisitionLineCode = purchaseRequisitionLineCode;

	}

	public String getLineNumber() {

		return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

		this.lineNumber = lineNumber;

	}

	public String getBudgetQuantity() {

		return budgetQuantity;

	}

	public void setBudgetQuantity(String budgetQuantity) {

		this.budgetQuantity = budgetQuantity;

	}
	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;

	}


	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public ApPurchaseRequisitionEntryTagList getTagListByIndex(int index){
	      return((ApPurchaseRequisitionEntryTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(ArrayList locationList) {

		this.locationList = locationList;

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public String getQuantity() {

		return quantity;

	}

	public void setQuantity(String quantity) {

		this.quantity = quantity;

	}

	public String getUnit() {

		return unit;

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}

	public ArrayList getUnitList() {

		return unitList;

	}

	public void setUnitList(String unit) {

		unitList.add(unit);

	}

	public void clearUnitList() {

		unitList.clear();
		unitList.add(Constants.GLOBAL_BLANK);

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

		this.itemDescription = itemDescription;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;
	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isItemEntered = null;

	}

	public String getIsLocationEntered() {

		return isLocationEntered;

	}

	public void setIsLocationEntered(String isLocationEntered) {

		if (isLocationEntered != null && isLocationEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isLocationEntered = null;

	}

	public String getIsUnitEntered() {

		return isUnitEntered;

	}

	public void setIsUnitEntered(String isUnitEntered) {

		if (isUnitEntered != null && isUnitEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isUnitEntered = null;

	}

	public boolean getDeleteCheckbox() {

		return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

		this.deleteCheckbox = deleteCheckbox;

	}


	public boolean getIsTraceMisc() {

		return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

		this.isTraceMisc = isTraceMisc;

	}

	public void setSupplierButton(String supplierButton) {

		parentBean.setRowSelected(this, false);

	}

}