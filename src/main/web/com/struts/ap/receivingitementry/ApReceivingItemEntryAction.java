package com.struts.ap.receivingitementry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlPettyCashAccountNotFoundException;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ApRINoPurchaseOrderLinesFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvTagMissingException;
import com.ejb.exception.GlobalInvTagExistingException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.exception.GlobalReferenceNumberNotUniqueException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.InvTagSerialNumberAlreadyExistException;
import com.ejb.txn.ApReceivingItemEntryController;
import com.ejb.txn.ApReceivingItemEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.ar.invoiceentry.ArInvoiceJobOrderList;
import com.struts.inv.adjustmententry.InvAdjustmentEntryTagList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.util.AdModApprovalQueueDetails;
import com.util.ApModPurchaseOrderDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.InvModTagListDetails;
import com.util.InvModItemDetails;
import com.util.ApModSupplierDetails;
import com.util.ApPurchaseOrderDetails;
import com.util.ApTaxCodeDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApReceivingItemEntryAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApReceivingItemEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ApReceivingItemEntryForm actionForm = (ApReceivingItemEntryForm)form;

	      // reset report

	      actionForm.setReport(null);
	      actionForm.setAttachment(null);
	      actionForm.setAttachmentPDF(null);

         String frParam = null;

         frParam = Common.getUserPermission(user, Constants.AP_RECEIVING_ITEM_ID);

         if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apReceivingItemEntry"));

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApReceivingItemEntryController EJB
*******************************************************/

         ApReceivingItemEntryControllerHome homeRI = null;
         ApReceivingItemEntryController ejbRI = null;

         GlDailyRateControllerHome homeFR = null;
         GlDailyRateController ejbFR = null;
         
             InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;
         

         try {

            homeRI = (ApReceivingItemEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApReceivingItemEntryControllerEJB", ApReceivingItemEntryControllerHome.class);

            homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
                    
            homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);


         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApReceivingItemEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {

            ejbRI = homeRI.create();

            ejbFR = homeFR.create();

            ejbII = homeII.create();
         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in ApReceivingItemEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ApReceivingItemEntryController EJB
   getGlFcPrecisionUnit
   getInvGpQuantityPrecisionUnit
   getAdPrfApJournalLineNumber
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

         short precisionUnit = 0;

         short journalLineNumber = 0;


         boolean enableReceivingItemBatch = false;
         boolean isInitialPrinting = false;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/Receiving Item/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
         boolean useSupplierPulldown = true;

         try {


        	 precisionUnit = ejbRI.getGlFcPrecisionUnit(user.getCmpCode());;

        	if (isAccessedByDevice(request.getHeader("user-agent"))) journalLineNumber=100;
        	else journalLineNumber = ejbRI.getAdPrfApJournalLineNumber(user.getCmpCode());
            useSupplierPulldown = Common.convertByteToBoolean(ejbRI.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
             actionForm.setPrecisionUnit(precisionUnit);
         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

/*******************************************************
-- Ap RI Save and Submit Mobile Action --
*******************************************************/

         if (request.getParameter("saveSubmitButton2") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
             ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

             details.setPoCode(actionForm.getReceivingItemCode());
             details.setPoReceiving((byte)(1));
             details.setPoType(actionForm.getType());
             details.setPoDate(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
             details.setPoRcvPoNumber(actionForm.getPoNumber());
             details.setPoDocumentNumber(actionForm.getDocumentNumber());
             details.setPoReferenceNumber(actionForm.getReferenceNumber());
             details.setPoVoid(Common.convertBooleanToByte(actionForm.getReceivingItemVoid()));
             details.setPoDescription(actionForm.getDescription());
             details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setPoConversionRate(1.0);
             details.setPoDuties(Common.convertStringMoneyToDouble(actionForm.getDuties(), Constants.MONEY_RATE_PRECISION));
             details.setPoFreight(Common.convertStringMoneyToDouble(actionForm.getFreight(), Constants.MONEY_RATE_PRECISION));
             details.setPoEntryFee(Common.convertStringMoneyToDouble(actionForm.getEntryFee(), Constants.MONEY_RATE_PRECISION));
             details.setPoStorage(Common.convertStringMoneyToDouble(actionForm.getStorage(), Constants.MONEY_RATE_PRECISION));
             details.setPoWharfageHandling(Common.convertStringMoneyToDouble(actionForm.getWharfageHandling(), Constants.MONEY_RATE_PRECISION));

             if (actionForm.getReceivingItemCode() == null) {

            	 details.setPoCreatedBy(user.getUserName());
            	 details.setPoDateCreated(new java.util.Date());

             }

             details.setPoLastModifiedBy(user.getUserName());
             details.setPoDateLastModified(new java.util.Date());

             ArrayList plList = new ArrayList();
             for (int i = 0; i<actionForm.getApRILListSize(); i++) {

            	   ApReceivingItemEntryList apRILList= actionForm.getApRILByIndex(i);
            	   if ((apRILList.getPartNumber().equals(""))) continue;

            	   ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();
            	   apRILList.setItemName(apRILList.getPartNumber());
            	   mdetails.setPlPartNumber(apRILList.getPartNumber());
            	   mdetails.setPlIiName(apRILList.getItemName());
            	   mdetails.setPlMisc(apRILList.getMisc());
               	   mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apRILList.getReceived(), precisionUnit));

               	   boolean isTraceMisc = ejbRI.getInvTraceMisc(apRILList.getItemName(), user.getCmpCode());

	  			   String misc=apRILList.getMisc();



	  	     	   if (isTraceMisc){
	  	     		   mdetails.setTraceMisc((byte) 1);
	  	     	//	   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	  	     //		   mdetails.setPlTagList(tagList);
	  	     		   
	  	     		try {
		  	     		 ArrayList tagList=  this.convertInvTagListToArrayList(apRILList,actionForm.getDate(), "IN");
			     		  
			     		  System.out.println("taglist size: " + tagList.size());
		     		  mdetails.setPlTagList(tagList);
		  	     		} catch (InvTagSerialNumberAlreadyExistException ex) {
			        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));
		  	     		}
	  	     	   }



               	   plList.add(mdetails);
             }

             try {


            	    Integer purchaseOrderCode = ejbRI.saveApPoEntryMobile(details, actionForm.getPaymentTerm(),
            	        actionForm.getCurrency(), plList, true,
            	        new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	    actionForm.setReceivingItemCode(purchaseOrderCode);

           
             } catch (GlobalTransactionAlreadyVoidPostedException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.transactionAlreadyVoidPosted"));

             } catch (GlobalRecordAlreadyDeletedException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.paymentTermInvalid"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.transactionAlreadyVoid"));

             } catch (GlobalInvItemLocationNotFoundException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.noItemLocationFound", ex.getMessage()));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.journalNotBalance"));

             } catch (InvTagSerialNumberAlreadyExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));

            } catch (GlobalTransactionAlreadyLockedException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.transactionAlreadyLocked"));

             } catch (GlobalInventoryDateException ex) {

            		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("receivingItemEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

          		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("receivingItemEntry.error.branchAccountNumberInvalid"));

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.noNegativeInventoryCostingCOA"));

            } catch (GlobalMiscInfoIsRequiredException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

            } catch (EJBException ex) {
            	 if (log.isInfoEnabled()) {
                 		log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
            	 					" session: " + session.getId());
            	 }
                 return(mapping.findForward("cmnErrorPage"));
             }

             if (!errors.isEmpty()) {

  	    	   saveErrors(request, new ActionMessages(errors));
  	   		   return (mapping.findForward("apReceivingItemEntry2"));

  	       	 }


/*******************************************************
   -- Ap RI Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

           details.setPoCode(actionForm.getReceivingItemCode());
           details.setPoType(actionForm.getType());
           details.setPoDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setPoRcvPoNumber(actionForm.getPoNumber());
           details.setPoDocumentNumber(actionForm.getDocumentNumber());
           details.setPoReferenceNumber(actionForm.getReferenceNumber());
           details.setPoVoid(Common.convertBooleanToByte(actionForm.getReceivingItemVoid()));
           details.setPoShipmentNumber(actionForm.getShipmentNumber());
           details.setPoDescription(actionForm.getDescription());
           details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setPoTotalAmount(Common.convertStringMoneyToDouble(actionForm.getTotalAmount(), Constants.MONEY_RATE_PRECISION));
           details.setPoDuties(Common.convertStringMoneyToDouble(actionForm.getDuties(), Constants.MONEY_RATE_PRECISION));
           details.setPoFreight(Common.convertStringMoneyToDouble(actionForm.getFreight(), Constants.MONEY_RATE_PRECISION));
           details.setPoEntryFee(Common.convertStringMoneyToDouble(actionForm.getEntryFee(), Constants.MONEY_RATE_PRECISION));
           details.setPoStorage(Common.convertStringMoneyToDouble(actionForm.getStorage(), Constants.MONEY_RATE_PRECISION));
           details.setPoWharfageHandling(Common.convertStringMoneyToDouble(actionForm.getWharfageHandling(), Constants.MONEY_RATE_PRECISION));


           if (actionForm.getReceivingItemCode() == null) {

           	   details.setPoCreatedBy(user.getUserName());
	           details.setPoDateCreated(new java.util.Date());

           }

           details.setPoLastModifiedBy(user.getUserName());
           details.setPoDateLastModified(new java.util.Date());

           ArrayList plList = new ArrayList();

           for (int i = 0; i<actionForm.getApRILListSize(); i++) {

           	   ApReceivingItemEntryList apRILList = actionForm.getApRILByIndex(i);


           	   if (Common.validateRequired(apRILList.getItemName()) &&
				   Common.validateRequired(apRILList.getUnit()) &&
				   Common.validateRequired(apRILList.getUnitCost()) &&
				   Common.validateRequired(apRILList.getReceived())) continue;

           	   ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();

           	   mdetails.setPlCode(apRILList.getReceivingItemLineCode());
           	   mdetails.setPlLine(Common.convertStringToShort(apRILList.getLineNumber()));
           	   mdetails.setPlIiName(apRILList.getItemName());
           	   mdetails.setPlLocName(apRILList.getLocation());
           	   mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apRILList.getReceived(), precisionUnit));
           	   mdetails.setPlUomName(apRILList.getUnit());
           	   mdetails.setPlUnitCost(Common.convertStringMoneyToDouble(apRILList.getUnitCost(), precisionUnit));
           	   mdetails.setPlAmount(Common.convertStringMoneyToDouble(apRILList.getAmount(), precisionUnit));
           	   mdetails.setPlUnitCostLocal(Common.convertStringMoneyToDouble(apRILList.getUnitCostLocal(), precisionUnit));
        	   mdetails.setPlAmountLocal(Common.convertStringMoneyToDouble(apRILList.getAmountLocal(), precisionUnit));

           	   mdetails.setPlQcNumber(apRILList.getQcNumber());
           	   mdetails.setPlQcExpiryDate(Common.convertStringToSQLDate(apRILList.getQcExpiryDate()));
           	   mdetails.setPlConversionFactor(Common.convertStringMoneyToDouble(apRILList.getConversionFactor(), (short)8));
           	   mdetails.setPlPlCode(apRILList.getPlPlCode());
           	   mdetails.setPlDiscount1(Common.convertStringMoneyToDouble(apRILList.getDiscount1(), precisionUnit));
           	   mdetails.setPlDiscount2(Common.convertStringMoneyToDouble(apRILList.getDiscount2(), precisionUnit));
           	   mdetails.setPlDiscount3(Common.convertStringMoneyToDouble(apRILList.getDiscount3(), precisionUnit));
           	   mdetails.setPlDiscount4(Common.convertStringMoneyToDouble(apRILList.getDiscount4(), precisionUnit));
           	   mdetails.setPlTotalDiscount(Common.convertStringMoneyToDouble(apRILList.getTotalDiscount(), precisionUnit));
           	   mdetails.setPlMisc(apRILList.getMisc());

           	   boolean isTraceMisc = ejbRI.getInvTraceMisc(apRILList.getItemName(), user.getCmpCode());

			   String misc=apRILList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setTraceMisc((byte) 1);
	     		   
	     		  
	     		//   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	     		   
	     			try {
		  	     		 ArrayList tagList=  this.convertInvTagListToArrayList(apRILList,actionForm.getDate(), "IN");
			     		  
			     		  System.out.println("taglist size: " + tagList.size());
		     		  mdetails.setPlTagList(tagList);
	  	     		} catch (InvTagSerialNumberAlreadyExistException ex) {
		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));
	  	     		}
	     	   }



         	   plList.add(mdetails);

           }
           // validate attachment

           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   	}


           try {

           	    Integer purchaseOrderCode = ejbRI.saveApPoEntry(details, actionForm.getRecalculateJournal(), actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getSupplier(), plList, true,
           	        new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	    actionForm.setReceivingItemCode(purchaseOrderCode);

           } catch (GlobalTransactionAlreadyVoidPostedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyVoidPosted"));

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyVoid"));

           	}catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlobalInvTagMissingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.invTagInventoriableError", ex.getMessage()));

           }/*catch (GlobalInvTagExistingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.invTagNonInventoriableError", ex.getMessage()));

           }*/  catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.journalNotBalance"));

           /*} catch (AdPRFCoaGlPettyCashAccountNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.noPettyCashAccountFound"));*/

           } catch (GlobalTransactionAlreadyLockedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyLocked"));

           } catch (GlobalInventoryDateException ex) {

          		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("receivingItemEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

        		errors.add(ActionMessages.GLOBAL_MESSAGE,
        			new ActionMessage("receivingItemEntry.error.branchAccountNumberInvalid"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	   	errors.add(ActionMessages.GLOBAL_MESSAGE,
        			new ActionMessage("receivingItemEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
    			    new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

           } catch (GlobalRecordInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.recordInvalid", ex.getMessage()));

           } catch (GlobalReferenceNumberNotUniqueException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("receivingItemEntry.error.shipmentNumberDoesNotExist"));

           } catch (InvTagSerialNumberAlreadyExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));



           }catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apReceivingItemEntry"));

	       }
// save attachment

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

/*******************************************************
   -- Ap RI Save & Submit Action --
*******************************************************/

	} else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

           details.setPoCode(actionForm.getReceivingItemCode());
           details.setPoType(actionForm.getType());
           details.setPoDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setPoDocumentNumber(actionForm.getDocumentNumber());
           details.setPoReferenceNumber(actionForm.getReferenceNumber());
           details.setPoRcvPoNumber(actionForm.getPoNumber());
           details.setPoVoid(Common.convertBooleanToByte(actionForm.getReceivingItemVoid()));
           details.setPoShipmentNumber(actionForm.getShipmentNumber());
           details.setPoDescription(actionForm.getDescription());
           details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setPoTotalAmount(Common.convertStringMoneyToDouble(actionForm.getTotalAmount(), Constants.MONEY_RATE_PRECISION));
           details.setPoDuties(Common.convertStringMoneyToDouble(actionForm.getDuties(), Constants.MONEY_RATE_PRECISION));
           details.setPoFreight(Common.convertStringMoneyToDouble(actionForm.getFreight(), Constants.MONEY_RATE_PRECISION));
           details.setPoEntryFee(Common.convertStringMoneyToDouble(actionForm.getEntryFee(), Constants.MONEY_RATE_PRECISION));
           details.setPoStorage(Common.convertStringMoneyToDouble(actionForm.getStorage(), Constants.MONEY_RATE_PRECISION));
           details.setPoWharfageHandling(Common.convertStringMoneyToDouble(actionForm.getWharfageHandling(), Constants.MONEY_RATE_PRECISION));


           if (actionForm.getReceivingItemCode() == null) {

           	   details.setPoCreatedBy(user.getUserName());
	           details.setPoDateCreated(new java.util.Date());

           }

           details.setPoLastModifiedBy(user.getUserName());
           details.setPoDateLastModified(new java.util.Date());

           ArrayList plList = new ArrayList();

           for (int i = 0; i<actionForm.getApRILListSize(); i++) {

           	   ApReceivingItemEntryList apRILList = actionForm.getApRILByIndex(i);

           	   if (Common.validateRequired(apRILList.getItemName()) &&
				   Common.validateRequired(apRILList.getUnit()) &&
				   Common.validateRequired(apRILList.getUnitCost()) &&
				   Common.validateRequired(apRILList.getReceived())) continue;

           	   ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();

           	   mdetails.setPlCode(apRILList.getReceivingItemLineCode());
           	   mdetails.setPlLine(Common.convertStringToShort(apRILList.getLineNumber()));
           	   mdetails.setPlIiName(apRILList.getItemName());
           	   mdetails.setPlLocName(apRILList.getLocation());
           	   mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apRILList.getReceived(), precisionUnit));
           	   mdetails.setPlUomName(apRILList.getUnit());
           	   mdetails.setPlUnitCost(Common.convertStringMoneyToDouble(apRILList.getUnitCost(), precisionUnit));
           	   mdetails.setPlAmount(Common.convertStringMoneyToDouble(apRILList.getAmount(), precisionUnit));
           	   mdetails.setPlQcNumber(apRILList.getQcNumber());
           	   mdetails.setPlQcExpiryDate(Common.convertStringToSQLDate(apRILList.getQcExpiryDate()));
           	   mdetails.setPlConversionFactor(Common.convertStringMoneyToDouble(apRILList.getConversionFactor(), (short)8));
           	   mdetails.setPlPlCode(apRILList.getPlPlCode());
           	   mdetails.setPlDiscount1(Common.convertStringMoneyToDouble(apRILList.getDiscount1(), precisionUnit));
           	   mdetails.setPlDiscount2(Common.convertStringMoneyToDouble(apRILList.getDiscount2(), precisionUnit));
           	   mdetails.setPlDiscount3(Common.convertStringMoneyToDouble(apRILList.getDiscount3(), precisionUnit));
           	   mdetails.setPlDiscount4(Common.convertStringMoneyToDouble(apRILList.getDiscount4(), precisionUnit));
           	   mdetails.setPlTotalDiscount(Common.convertStringMoneyToDouble(apRILList.getTotalDiscount(), precisionUnit));
           	   mdetails.setPlMisc(apRILList.getMisc());

           	 boolean isTraceMisc = ejbRI.getInvTraceMisc(apRILList.getItemName(), user.getCmpCode());

			   String misc=apRILList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setTraceMisc((byte) 1);
	     	//	   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	     	//	   mdetails.setPlTagList(tagList);
	     		   
	     		   
	     			try {
		  	     		 ArrayList tagList=  this.convertInvTagListToArrayList(apRILList,actionForm.getDate(), "IN");
			     		  
			     		  System.out.println("taglist size: " + tagList.size());
		     		  mdetails.setPlTagList(tagList);
	  	     		} catch (InvTagSerialNumberAlreadyExistException ex) {
		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));
	  	     		}
	     	   }



         	   plList.add(mdetails);
           }
           // validate attachment

           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("receivingItemEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("receivingItemEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apReceivingItemEntry"));

	   	    	}

	   	   	}

           try {

           	    Integer purchaseOrderCode = ejbRI.saveApPoEntry(details,actionForm.getRecalculateJournal(),  actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getSupplier(), plList, false,
           	        new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setReceivingItemCode(purchaseOrderCode);

           } catch (GlobalTransactionAlreadyVoidPostedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyVoidPosted"));

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyVoid"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlobalInvTagMissingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.invTagInventoriableError", ex.getMessage()));

           }/*catch (GlobalInvTagExistingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.invTagNonInventoriableError", ex.getMessage()));

           } */catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.journalNotBalance"));

           /*} catch (AdPRFCoaGlPettyCashAccountNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.noPettyCashAccountFound"));*/

           } catch (InvTagSerialNumberAlreadyExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));

           } catch (GlobalTransactionAlreadyLockedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.transactionAlreadyLocked"));

           } catch (GlobalInventoryDateException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("receivingItemEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("receivingItemEntry.error.branchAccountNumberInvalid"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("receivingItemEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

           } catch (GlobalRecordInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.recordInvalid", ex.getMessage()));

           } catch (GlobalReferenceNumberNotUniqueException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("receivingItemEntry.error.shipmentNumberDoesNotExist"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("purchaseOrderEntry.error.noApprovalApproverFound"));

           }catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apReceivingItemEntry"));

	       }
// save attachment

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

/*******************************************************
	 -- Ap RI Print Action --
*******************************************************/

         } else if (request.getParameter("printInspectionButton") != null || request.getParameter("printReleaseButton") != null ||
        		 request.getParameter("printButton") != null || request.getParameter("printReceivingReportButton")!=null) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

	           details.setPoCode(actionForm.getReceivingItemCode());
	           details.setPoType(actionForm.getType());
	           details.setPoDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setPoDocumentNumber(actionForm.getDocumentNumber());
	           details.setPoReferenceNumber(actionForm.getReferenceNumber());
	           details.setPoRcvPoNumber(actionForm.getPoNumber());
	           details.setPoVoid(Common.convertBooleanToByte(actionForm.getReceivingItemVoid()));
	           details.setPoShipmentNumber(actionForm.getShipmentNumber());
	           details.setPoDescription(actionForm.getDescription());
	           details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setPoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	           details.setPoTotalAmount(Common.convertStringMoneyToDouble(actionForm.getTotalAmount(), Constants.MONEY_RATE_PRECISION));
	           details.setPoDuties(Common.convertStringMoneyToDouble(actionForm.getDuties(), Constants.MONEY_RATE_PRECISION));
	           details.setPoFreight(Common.convertStringMoneyToDouble(actionForm.getFreight(), Constants.MONEY_RATE_PRECISION));
	           details.setPoEntryFee(Common.convertStringMoneyToDouble(actionForm.getEntryFee(), Constants.MONEY_RATE_PRECISION));
	           details.setPoStorage(Common.convertStringMoneyToDouble(actionForm.getStorage(), Constants.MONEY_RATE_PRECISION));
	           details.setPoWharfageHandling(Common.convertStringMoneyToDouble(actionForm.getWharfageHandling(), Constants.MONEY_RATE_PRECISION));


	           if (actionForm.getReceivingItemCode() == null) {

	           	   details.setPoCreatedBy(user.getUserName());
		           details.setPoDateCreated(new java.util.Date());

	           }

	           details.setPoLastModifiedBy(user.getUserName());
	           details.setPoDateLastModified(new java.util.Date());

	           ArrayList plList = new ArrayList();

	           for (int i = 0; i<actionForm.getApRILListSize(); i++) {

	           	   ApReceivingItemEntryList apRILList = actionForm.getApRILByIndex(i);

	           	   if (Common.validateRequired(apRILList.getItemName()) &&
					   Common.validateRequired(apRILList.getUnit()) &&
					   Common.validateRequired(apRILList.getUnitCost()) &&
					   Common.validateRequired(apRILList.getReceived())) continue;

	           	   ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();

	           	   mdetails.setPlCode(apRILList.getReceivingItemLineCode());
	           	   mdetails.setPlLine(Common.convertStringToShort(apRILList.getLineNumber()));
	           	   mdetails.setPlIiName(apRILList.getItemName());
	           	   mdetails.setPlLocName(apRILList.getLocation());
	           	   mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apRILList.getReceived(), precisionUnit));
	           	   mdetails.setPlUomName(apRILList.getUnit());
	           	   mdetails.setPlUnitCost(Common.convertStringMoneyToDouble(apRILList.getUnitCost(), precisionUnit));
	           	   mdetails.setPlAmount(Common.convertStringMoneyToDouble(apRILList.getAmount(), precisionUnit));
	           	   mdetails.setPlQcNumber(apRILList.getQcNumber());
	           	mdetails.setPlQcExpiryDate(Common.convertStringToSQLDate(apRILList.getQcExpiryDate()));
	           	   mdetails.setPlConversionFactor(Common.convertStringMoneyToDouble(apRILList.getConversionFactor(), (short)8));
	           	   mdetails.setPlPlCode(apRILList.getPlPlCode());
	           	   mdetails.setPlDiscount1(Common.convertStringMoneyToDouble(apRILList.getDiscount1(), precisionUnit));
	           	   mdetails.setPlDiscount2(Common.convertStringMoneyToDouble(apRILList.getDiscount2(), precisionUnit));
	           	   mdetails.setPlDiscount3(Common.convertStringMoneyToDouble(apRILList.getDiscount3(), precisionUnit));
	           	   mdetails.setPlDiscount4(Common.convertStringMoneyToDouble(apRILList.getDiscount4(), precisionUnit));
	           	   mdetails.setPlTotalDiscount(Common.convertStringMoneyToDouble(apRILList.getTotalDiscount(), precisionUnit));
	           	   mdetails.setPlMisc(apRILList.getMisc());
	           	 boolean isTraceMisc = ejbRI.getInvTraceMisc(apRILList.getItemName(), user.getCmpCode());

	  			   String misc=apRILList.getMisc();



	  	     	   if (isTraceMisc){
	  	     		   mdetails.setTraceMisc((byte) 1);
	  	     		//   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	  	     		//   mdetails.setPlTagList(tagList);
	  	     		   
	  	     		   
	  	     		   
		  	     		try {
			  	     		 ArrayList tagList=  this.convertInvTagListToArrayList(apRILList,actionForm.getDate(), "IN");
				     		  
				     		  System.out.println("taglist size: " + tagList.size());
			     		  mdetails.setPlTagList(tagList);
		  	     		} catch (InvTagSerialNumberAlreadyExistException ex) {
			        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));
		  	     		}
	  	     		
	  	     	   
	  	     	   }


             	   plList.add(mdetails);

               }
	           // validate attachment

	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	           if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   	}

	           try {

	               Integer purchaseOrderCode = ejbRI.saveApPoEntry(details, actionForm.getRecalculateJournal(), actionForm.getPaymentTerm(),
	           	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getSupplier(),
						plList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	   actionForm.setReceivingItemCode(purchaseOrderCode);

	           } catch (InvTagSerialNumberAlreadyExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));
	           	   
	           	   
	           } catch (GlobalTransactionAlreadyVoidPostedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.transactionAlreadyVoidPosted"));

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.conversionDateNotExist"));

	           } catch (GlobalPaymentTermInvalidException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.paymentTermInvalid"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalInvItemLocationNotFoundException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.noItemLocationFound", ex.getMessage()));

           	   } catch (GlobalInvTagMissingException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.invTagInventoriableError", ex.getMessage()));

               }/*catch (GlobalInvTagExistingException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.invTagNonInventoriableError", ex.getMessage()));

               } */catch (GlJREffectiveDateNoPeriodExistException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.effectiveDateNoPeriodExist"));

               } catch (GlJREffectiveDatePeriodClosedException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.effectiveDatePeriodClosed"));

               } catch (GlobalJournalNotBalanceException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.journalNotBalance"));

               /*} catch (AdPRFCoaGlPettyCashAccountNotFoundException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.noPettyCashAccountFound"));*/

               } catch (GlobalTransactionAlreadyLockedException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.transactionAlreadyLocked"));

               } catch (GlobalInventoryDateException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("receivingItemEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

               } catch (GlobalBranchAccountNumberInvalidException ex) {

                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                           new ActionMessage("receivingItemEntry.error.branchAccountNumberInvalid"));

               } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("receivingItemEntry.error.noNegativeInventoryCostingCOA"));

	           } catch (GlobalMiscInfoIsRequiredException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	           } catch (GlobalRecordInvalidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.recordInvalid", ex.getMessage()));

	           } catch (GlobalReferenceNumberNotUniqueException ex) {

	                 errors.add(ActionMessages.GLOBAL_MESSAGE,
	                         new ActionMessage("receivingItemEntry.error.shipmentNumberDoesNotExist"));
	                 
	         

	           }catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {

		    	   saveErrors(request, new ActionMessages(errors));
		   		   return (mapping.findForward("apReceivingItemEntry"));

		       }

	           if(request.getParameter("printReceivingReportButton")!=null){
		    	   actionForm.setReceivingReport(true);
		       }
	           if(request.getParameter("printInspectionButton")!=null){
		    	   actionForm.setInspectedReport(true);
		       }
	           if(request.getParameter("printReleaseButton")!=null){
		    	   actionForm.setReleasedReport(true);
		       }

		       actionForm.setReport(Constants.STATUS_SUCCESS);

	           isInitialPrinting = true;

	        // save attachment

	           if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	        }  else {

	        	actionForm.setReport(Constants.STATUS_SUCCESS);

		        	return(mapping.findForward("apReceivingItemEntry"));

	        }

/*******************************************************
   -- Ap RI Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null) {

           if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApPurchaseOrderDetails details = new ApPurchaseOrderDetails();

	           details.setPoCode(actionForm.getReceivingItemCode());
	           details.setPoType(actionForm.getType());
	           details.setPoDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setPoDocumentNumber(actionForm.getDocumentNumber());
	           details.setPoReferenceNumber(actionForm.getReferenceNumber());
	           details.setPoRcvPoNumber(actionForm.getPoNumber());
	           details.setPoVoid(Common.convertBooleanToByte(actionForm.getReceivingItemVoid()));
	           details.setPoShipmentNumber(actionForm.getShipmentNumber());
	           details.setPoDescription(actionForm.getDescription());
	           details.setPoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setPoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	           details.setPoTotalAmount(Common.convertStringMoneyToDouble(actionForm.getTotalAmount(), Constants.MONEY_RATE_PRECISION));
	           details.setPoDuties(Common.convertStringMoneyToDouble(actionForm.getDuties(), Constants.MONEY_RATE_PRECISION));
	           details.setPoFreight(Common.convertStringMoneyToDouble(actionForm.getFreight(), Constants.MONEY_RATE_PRECISION));
	           details.setPoEntryFee(Common.convertStringMoneyToDouble(actionForm.getEntryFee(), Constants.MONEY_RATE_PRECISION));
	           details.setPoStorage(Common.convertStringMoneyToDouble(actionForm.getStorage(), Constants.MONEY_RATE_PRECISION));
	           details.setPoWharfageHandling(Common.convertStringMoneyToDouble(actionForm.getWharfageHandling(), Constants.MONEY_RATE_PRECISION));


	           if (actionForm.getReceivingItemCode() == null) {

	           	   details.setPoCreatedBy(user.getUserName());
		           details.setPoDateCreated(new java.util.Date());

	           }

	           details.setPoLastModifiedBy(user.getUserName());
	           details.setPoDateLastModified(new java.util.Date());

	           ArrayList plList = new ArrayList();

	           for (int i = 0; i<actionForm.getApRILListSize(); i++) {

	           	   ApReceivingItemEntryList apRILList = actionForm.getApRILByIndex(i);

	           	   if (Common.validateRequired(apRILList.getItemName()) &&
					   Common.validateRequired(apRILList.getUnit()) &&
					   Common.validateRequired(apRILList.getUnitCost()) &&
					   Common.validateRequired(apRILList.getReceived())) continue;

	           	   ApModPurchaseOrderLineDetails mdetails = new ApModPurchaseOrderLineDetails();

	           	   mdetails.setPlCode(apRILList.getReceivingItemLineCode());
	           	   mdetails.setPlLine(Common.convertStringToShort(apRILList.getLineNumber()));
	           	   mdetails.setPlIiName(apRILList.getItemName());
	           	   mdetails.setPlLocName(apRILList.getLocation());
	           	   mdetails.setPlQuantity(Common.convertStringMoneyToDouble(apRILList.getReceived(), precisionUnit));
	           	   mdetails.setPlUomName(apRILList.getUnit());
	           	   mdetails.setPlUnitCost(Common.convertStringMoneyToDouble(apRILList.getUnitCost(), precisionUnit));
	           	   mdetails.setPlAmount(Common.convertStringMoneyToDouble(apRILList.getAmount(), precisionUnit));
	           	   mdetails.setPlQcNumber(apRILList.getQcNumber());
	           	   mdetails.setPlQcExpiryDate(Common.convertStringToSQLDate(apRILList.getQcExpiryDate()));
	           	   mdetails.setPlConversionFactor(Common.convertStringMoneyToDouble(apRILList.getConversionFactor(), (short)8));
	           	   mdetails.setPlPlCode(apRILList.getPlPlCode());
	           	   mdetails.setPlDiscount1(Common.convertStringMoneyToDouble(apRILList.getDiscount1(), precisionUnit));
	           	   mdetails.setPlDiscount2(Common.convertStringMoneyToDouble(apRILList.getDiscount2(), precisionUnit));
	           	   mdetails.setPlDiscount3(Common.convertStringMoneyToDouble(apRILList.getDiscount3(), precisionUnit));
	           	   mdetails.setPlDiscount4(Common.convertStringMoneyToDouble(apRILList.getDiscount4(), precisionUnit));
	           	   mdetails.setPlTotalDiscount(Common.convertStringMoneyToDouble(apRILList.getTotalDiscount(), precisionUnit));
	           	   mdetails.setPlMisc(apRILList.getMisc());

	           	 boolean isTraceMisc = ejbRI.getInvTraceMisc(apRILList.getItemName(), user.getCmpCode());

	  			   String misc=apRILList.getMisc();



	  	     	   if (isTraceMisc){
	  	     		   mdetails.setTraceMisc((byte) 1);
	  	     		 //  ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	  	     	//	   mdetails.setPlTagList(tagList);
	  	     		   
	  	     		   
	  	     		try {
		  	     		 ArrayList tagList=  this.convertInvTagListToArrayList(apRILList,actionForm.getDate(), "IN");
			     		  
			     		  System.out.println("taglist size: " + tagList.size());
		     		  mdetails.setPlTagList(tagList);
	  	     		} catch (InvTagSerialNumberAlreadyExistException ex) {
		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			   new ActionMessage("receivingItemEntry.error.invTagSerialNumberExistError", ex.getMessage()));
	  	     		}
	  	     	   }



             	   plList.add(mdetails);

               }
	           // validate attachment

	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	           if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receivingItemEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("receivingItemEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apReceivingItemEntry"));

		   	    	}

		   	   	}

	           try {

	               Integer purchaseOrderCode = ejbRI.saveApPoEntry(details,actionForm.getRecalculateJournal(),  actionForm.getPaymentTerm(),
	           	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getSupplier(), plList, true,
	           	        new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	   actionForm.setReceivingItemCode(purchaseOrderCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.conversionDateNotExist"));

	           } catch (GlobalPaymentTermInvalidException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.paymentTermInvalid"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalInvItemLocationNotFoundException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.noItemLocationFound", ex.getMessage()));

           	   }  catch (GlobalInvTagMissingException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.invTagInventoriableError", ex.getMessage()));

               }/*catch (GlobalInvTagExistingException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.invTagNonInventoriableError", ex.getMessage()));

               }*/catch (GlJREffectiveDateNoPeriodExistException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.effectiveDateNoPeriodExist"));

               } catch (GlJREffectiveDatePeriodClosedException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.effectiveDatePeriodClosed"));

               } catch (GlobalJournalNotBalanceException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.journalNotBalance"));

               /*} catch (AdPRFCoaGlPettyCashAccountNotFoundException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.noPettyCashAccountFound"));*/

               } catch (GlobalTransactionAlreadyLockedException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("receivingItemEntry.error.transactionAlreadyLocked"));

               } catch (GlobalInventoryDateException ex) {

                  		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("receivingItemEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

               } catch (GlobalBranchAccountNumberInvalidException ex) {

                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("receivingItemEntry.error.branchAccountNumberInvalid"));

               } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("receivingItemEntry.error.noNegativeInventoryCostingCOA"));

	           } catch (GlobalMiscInfoIsRequiredException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	           } catch (GlobalRecordInvalidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.recordInvalid", ex.getMessage()));

	           } catch (GlobalReferenceNumberNotUniqueException ex) {

	                 errors.add(ActionMessages.GLOBAL_MESSAGE,
	                         new ActionMessage("receivingItemEntry.error.shipmentNumberDoesNotExist"));



	           }catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {

		    	   saveErrors(request, new ActionMessages(errors));
		   		   return (mapping.findForward("apReceivingItemEntry"));

		       }
	        // save attachment

	           if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceivingItemCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

       	   }

       	   String path = "/apJournal.do?forward=1" +
		     "&transactionCode=" + actionForm.getReceivingItemCode() +
		     "&transaction=RECEIVING" +
		     "&transactionNumber=" + actionForm.getReferenceNumber() +
		     "&transactionDate=" + actionForm.getDate() +
		     "&transactionEnableFields=" + actionForm.getEnableFields();

            return(new ActionForward(path));

/*******************************************************
   -- AP RI Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbRI.deleteApPoEntry(actionForm.getReceivingItemCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());

                }

                return(mapping.findForward("cmnErrorPage"));

            }

/*******************************************************
   -- Ap RI Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap RI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {

         	int listSize = actionForm.getApRILListSize();

            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	ApReceivingItemEntryList apRILList = new ApReceivingItemEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null, null, null, null, null, null, null, "QCXXXX", Common.convertSQLDateToString(new java.util.Date()), null, null, "0.0","0.0", "0.0", "0.0", null, "0.0", null, null);

	        	apRILList.setLocationList(actionForm.getLocationList());
	        	apRILList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
	        	actionForm.saveApRILList(apRILList);

	        }

	        return(mapping.findForward("apReceivingItemEntry"));


/*******************************************************
   -- Ap RI Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {

         	for (int i = 0; i<actionForm.getApRILListSize(); i++) {

           	   ApReceivingItemEntryList apRILList = actionForm.getApRILByIndex(i);

           	   if (apRILList.getDeleteCheckbox()) {

           	   	   actionForm.deleteApRILList(i);
           	   	   i--;
           	   }

            }

            for (int i = 0; i<actionForm.getApRILListSize(); i++) {

           	   ApReceivingItemEntryList apRILList = actionForm.getApRILByIndex(i);

           	   apRILList.setLineNumber(String.valueOf(i+1));

            }

	        return(mapping.findForward("apReceivingItemEntry"));

/*******************************************************
   -- Ap RI Supplier Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered")) && actionForm.getType().equals("ITEMS")) {

         	try {

                 ApModSupplierDetails mdetails = ejbRI.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());

                 actionForm.setPaymentTerm(mdetails.getSplPytName());
                 actionForm.setTaxCode(mdetails.getSplScTcName());
                 actionForm.setTaxType(mdetails.getSplScTcType());
                 actionForm.setTaxRate(mdetails.getSplScTcRate());
                 actionForm.setSupplierName(mdetails.getSplName());


                 actionForm.clearApRILList();

                 for (int x = 1; x <= journalLineNumber; x++) {

                 	ApReceivingItemEntryList apRILList = new ApReceivingItemEntryList(actionForm,
                 			null, new Integer(x).toString(), null, null, null, null, null,
							null, null, null, null, null, null, "QCXXXX", Common.convertSQLDateToString(new java.util.Date()), null,null, "0.0","0.0", "0.0", "0.0", null, null, null, null);

                 	apRILList.setLocationList(actionForm.getLocationList());
                 	apRILList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);

                 	apRILList.setUnitList(Constants.GLOBAL_BLANK);
                 	apRILList.setUnitList("Select Item First");

                 	actionForm.saveApRILList(apRILList);

                 }


             } catch (GlobalNoRecordFoundException ex) {

             	actionForm.clearApRILList();

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

             return(mapping.findForward("apReceivingItemEntry"));


/*******************************************************
   -- Ap RI Purchase Order Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isPurchaseOrderEntered"))) {

            if (actionForm.getType().equals("PO MATCHED")) {

	         	try {
	             	 System.out.println("isPurchaseOrderEntered");
	                 ApModPurchaseOrderDetails mdetails = ejbRI.getApPoByPoRcvPoNumberAndSplSupplierCodeAndAdBranch(
	                     actionForm.getPoNumber(), actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	                 actionForm.setPaymentTerm(mdetails.getPoPytName());
	                 actionForm.setTaxCode(mdetails.getPoTcName());
	                 actionForm.setTaxRate(mdetails.getPoTcRate());
	          		 actionForm.setTaxType(mdetails.getPoTcType());
	                 actionForm.setCurrency(mdetails.getPoFcName());
	                 actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getPoConversionDate()));
	                 actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getPoConversionRate(), Constants.CONVERSION_RATE_PRECISION));

	                 actionForm.setSupplierName(mdetails.getPoSplName());
	                 actionForm.setDescription(mdetails.getPoDescription());
	                 actionForm.setDuties("0");
	                 actionForm.setEntryFee("0");
	                 actionForm.setFreight("0");
	                 actionForm.setStorage("0");
	                 actionForm.setWharfageHandling("0");

	                 actionForm.clearApRILList();

	                 double totalAmount = 0d;
	 	             Iterator i = mdetails.getPoPlList().iterator();

			         while (i.hasNext()) {

				           ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails)i.next();
				           totalAmount += mPlDetails.getPlAmount();
					       ApReceivingItemEntryList apRilList = new ApReceivingItemEntryList(actionForm,
					       		null, Common.convertShortToString(mPlDetails.getPlLine()),
							    mPlDetails.getPlLocName(), mPlDetails.getPlIiName(),
								mPlDetails.getPlIiDescription(), Common.convertDoubleToStringMoney(mPlDetails.getPlRemaining(), precisionUnit),
							    Common.convertDoubleToStringMoney(mPlDetails.getPlQuantity(), precisionUnit),
							    mPlDetails.getPlUomName(),
							    Common.convertDoubleToStringMoney(mPlDetails.getPlUnitCost(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlAmount(), precisionUnit),
								"0",
								"0",
								"0",
								"QCXXXX",
								Common.convertSQLDateToString(new java.util.Date()),
								Common.convertDoubleToStringMoney(ejbRI.getInvUmcByIiNameAndUomName(mPlDetails.getPlIiName(), mPlDetails.getPlUomName(), user.getCmpCode()), (short)8),
								mPlDetails.getPlCode(),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount1(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount2(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount3(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount4(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlTotalDiscount(), precisionUnit),
								Common.convertDoubleToStringMoney(mPlDetails.getPlTotalDiscount(), precisionUnit),
								mPlDetails.getPlMisc(), mPlDetails.getPlPartNumber() );
					        	apRilList.setLocationList(actionForm.getLocationList());


					        apRilList.clearTagList();




					        String misc = mPlDetails.getPlQuantity() + "_";
				      		String propertyCode = "";
				   		 	String specs = "";
				   		 	String serialNumber = "";
				   		 	String custodian = "";
				   		 	String expiryDate = "";
				   		 	String tgDocumentNumber = "";

				   			boolean isTraceMisc = ejbRI.getInvTraceMisc(mPlDetails.getPlIiName(), user.getCmpCode());
				   			apRilList.setIsTraceMisc(isTraceMisc);
					        if (mPlDetails.getTraceMisc()==1){


					        	ArrayList tagList = mPlDetails.getPlTagList();





	        					if (tagList.size() == 0) {

	        						misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mPlDetails.getPlQuantity()));


	            				}else{
	            					System.out.println("existing taglist");


	            					misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mPlDetails.getPlQuantity()));


	            				}
					        }
					        apRilList.setMisc(misc);

					        actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));


	        				apRilList.clearUnitList();

	        				ArrayList unitList = ejbRI.getInvUomByIiName(mPlDetails.getPlIiName(), user.getCmpCode());

	        				Iterator unitListIter = unitList.iterator();

	        				while (unitListIter.hasNext()) {

			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		apRilList.setUnitList(mUomDetails.getUomName());

			            	}

					        actionForm.saveApRILList(apRilList);

				     }

	             } catch (GlobalNoRecordFoundException ex) {

	             	actionForm.clearApRILList();

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.noPurchaseOrderFound"));
	                saveErrors(request, new ActionMessages(errors));

			     } catch (ApRINoPurchaseOrderLinesFoundException ex) {

	             	actionForm.clearApRILList();

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.noPurchaseOrderLinesFound"));
	                saveErrors(request, new ActionMessages(errors));


	             } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }

            } else {

	            try {

	                 ApModSupplierDetails mdetails = ejbRI.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());

	                 actionForm.setPaymentTerm(mdetails.getSplPytName());
	                 actionForm.setTaxCode(mdetails.getSplScTcName());
	                 actionForm.setTaxType(mdetails.getSplScTcType());
	                 actionForm.setTaxRate(mdetails.getSplScTcRate());
	                 actionForm.setSupplierName(mdetails.getSplName());

	                 actionForm.clearApRILList();

	                 for (int x = 1; x <= journalLineNumber; x++) {

	                 	ApReceivingItemEntryList apRILList = new ApReceivingItemEntryList(actionForm,
	                 			null, new Integer(x).toString(), null, null, null, null, null,
								null, null, null, null, null, null, "QCXXXX", Common.convertSQLDateToString(new java.util.Date()), null, null, "0.0","0.0", "0.0", "0.0", null, null, null, null);

	                 	apRILList.setLocationList(actionForm.getLocationList());
	                 	apRILList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
	                 	apRILList.setUnitList(Constants.GLOBAL_BLANK);
	                 	apRILList.setUnitList("Select Item First");

	                 	apRILList.clearTagList();
	                 	/*
	            		for (int y=1; y<=10; y++) {

	            			ApReceivingItemEntryTagList apTagList = new ApReceivingItemEntryTagList(apRILList, "", "", "", "", "","");

	            			apRILList.saveTagList(apTagList);
	            		}
	                 	*/
	                 	actionForm.saveApRILList(apRILList);

	                 }

	             } catch (GlobalNoRecordFoundException ex) {

	             	actionForm.clearApRILList();

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("receivingItemEntry.error.supplierNoRecordFound"));
	                saveErrors(request, new ActionMessages(errors));

	             } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }

            }

            return(mapping.findForward("apReceivingItemEntry"));



/*******************************************************
    -- Ap RI Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apRILList[" +
        actionForm.getRowSelected() + "].isItemEntered"))) {

      	ApReceivingItemEntryList apRILList =
      		actionForm.getApRILByIndex(actionForm.getRowSelected());
            System.out.println("item enter action");
      	try {

      		// populate unit field class
      		System.out.println(apRILList.getTagListSize() + "<= getTagListSize item enter action");
      		ArrayList uomList = new ArrayList();
      		uomList = ejbRI.getInvUomByIiName(apRILList.getItemName(), user.getCmpCode());

      		apRILList.clearUnitList();
      		apRILList.setUnitList(Constants.GLOBAL_BLANK);

  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {

      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

      			apRILList.setUnitList(mUomDetails.getUomName());

      			if (mUomDetails.isDefault()) {

      				apRILList.setUnit(mUomDetails.getUomName());
      				apRILList.setConversionFactor(Common.convertDoubleToStringMoney(mUomDetails.getUomConversionFactor(), (short)8));

      			}

      		}
      		//TODO: populate taglist with empty values
      		boolean isTraceMisc = ejbRI.getInvTraceMisc(apRILList.getItemName(), user.getCmpCode());
      		apRILList.setIsTraceMisc(isTraceMisc);


      		System.out.println(isTraceMisc + "<== trace misc item enter action");
      		if (isTraceMisc == true){
      			apRILList.clearTagList();

      			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), apRILList.getReceived());


      			apRILList.setMisc(misc);


      		}

      		// populate unit cost field

      		if (!Common.validateRequired(apRILList.getItemName()) && !Common.validateRequired(apRILList.getUnit())) {

	      		//double unitCost = ejbRI.getInvIiUnitCostByIiNameAndUomName(apRILList.getItemName(), apRILList.getUnit(), user.getCmpCode());
	      	    double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apRILList.getItemName(), apRILList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                    
	      	
	      		apRILList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

      		}

      		apRILList.setDiscount1("0.0");
      		apRILList.setDiscount2("0.0");
      		apRILList.setDiscount3("0.0");
      		apRILList.setDiscount4("0.0");
      		apRILList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));

      		apRILList.setReceived("");
      		apRILList.setAmount(apRILList.getUnitCost());

          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("apReceivingItemEntry"));
/*******************************************************
     -- Ap VOU View Attachment Action --
*******************************************************/

	   } else if(request.getParameter("viewAttachmentButton1") != null) {

	  	File file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-1" + attachmentFileExtension );
	  	File filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-1" + attachmentFileExtensionPDF );
	  	String filename = "";

	  	if (file.exists()){
	  		filename = file.getName();
	  	}else if (filePDF.exists()) {
	  		filename = filePDF.getName();
	  	}


       String fileExtension = filename.substring(filename.lastIndexOf("."));

	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	  		fileExtension = attachmentFileExtension;

	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;

	    	}

	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceivingItemCode() + "-1" + fileExtension);

	  	byte data[] = new byte[fis.available()];

	  	int ctr = 0;
	  	int c = 0;

	  	while ((c = fis.read()) != -1) {

     		data[ctr] = (byte)c;
     		ctr++;

	  	}

	  	if (fileExtension == attachmentFileExtension) {
	      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));


	      	session.setAttribute(Constants.IMAGE_KEY, image);
	      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	  	} else if (fileExtension == attachmentFileExtensionPDF ){


	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	  	}

        if (request.getParameter("child") == null) {

          return (mapping.findForward("apReceivingItemEntry"));

        } else {

          return (mapping.findForward("apReceivingItemEntryChild"));

       }



     } else if(request.getParameter("viewAttachmentButton2") != null) {

   	  File file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-2" + attachmentFileExtension );
   	  	File filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-2" + attachmentFileExtensionPDF );
   	  	String filename = "";

   	  	if (file.exists()){
   	  		filename = file.getName();
   	  	}else if (filePDF.exists()) {
   	  		filename = filePDF.getName();
   	  	}


	        String fileExtension = filename.substring(filename.lastIndexOf("."));

   	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

   	  		fileExtension = attachmentFileExtension;

 	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
 	    		fileExtension = attachmentFileExtensionPDF;

 	    	}

   	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceivingItemCode() + "-2" + fileExtension);

   	  	byte data[] = new byte[fis.available()];

   	  	int ctr = 0;
   	  	int c = 0;

   	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

   	  	}

   	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));


		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
   	  	} else if (fileExtension == attachmentFileExtensionPDF ){


   	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
   	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
   	  	}




        if (request.getParameter("child") == null) {

          return (mapping.findForward("apReceivingItemEntry"));

        } else {

          return (mapping.findForward("apReceivingItemEntryChild"));

       }


     } else if(request.getParameter("viewAttachmentButton3") != null) {

   	  File file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-3" + attachmentFileExtension );
   	  File filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-3" + attachmentFileExtensionPDF );
   	  String filename = "";

   	  if (file.exists()){
   		  filename = file.getName();
   	  }else if (filePDF.exists()) {
   		  filename = filePDF.getName();
   	  }


   	  String fileExtension = filename.substring(filename.lastIndexOf("."));

   	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

   		  fileExtension = attachmentFileExtension;

   	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
 	    		fileExtension = attachmentFileExtensionPDF;

   	  }

   	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceivingItemCode() + "-3" + fileExtension);

   	  byte data[] = new byte[fis.available()];

   	  int ctr = 0;
   	  int c = 0;

   	  while ((c = fis.read()) != -1) {

   		  data[ctr] = (byte)c;
   		  ctr++;

   	  }

   	  if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));


		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
   	  } else if (fileExtension == attachmentFileExtensionPDF ){


   	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
   	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
   	  }

        if (request.getParameter("child") == null) {

          return (mapping.findForward("apReceivingItemEntry"));

        } else {

          return (mapping.findForward("apReceivingItemEntryChild"));

       }


     } else if(request.getParameter("viewAttachmentButton4") != null) {

   	  File file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-4" + attachmentFileExtension );
   	  File filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-4" + attachmentFileExtensionPDF );
   	  String filename = "";

   	  if (file.exists()){
   		  filename = file.getName();
   	  }else if (filePDF.exists()) {
   		  filename = filePDF.getName();
   	  }


	        String fileExtension = filename.substring(filename.lastIndexOf("."));

   	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

   	  		fileExtension = attachmentFileExtension;

 	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
 	    		fileExtension = attachmentFileExtensionPDF;

 	    	}

   	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceivingItemCode() + "-4" + fileExtension);

   	  	byte data[] = new byte[fis.available()];

   	  	int ctr = 0;
   	  	int c = 0;

   	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

   	  	}

   	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));


		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
   	  	} else if (fileExtension == attachmentFileExtensionPDF ){


   	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
   	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
   	  	}
        if (request.getParameter("child") == null) {

          return (mapping.findForward("apReceivingItemEntry"));

        } else {

          return (mapping.findForward("apReceivingItemEntryChild"));

       }
/*******************************************************
    -- Ap RI Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apRILList[" +
        actionForm.getRowSelected() + "].isUnitEntered"))) {

      	ApReceivingItemEntryList apRILList =
      		actionForm.getApRILByIndex(actionForm.getRowSelected());

      	try {

            // populate unit cost field

      		if (!Common.validateRequired(apRILList.getLocation()) && !Common.validateRequired(apRILList.getItemName())) {

	      	//	double unitCost = ejbRI.getInvIiUnitCostByIiNameAndUomName(apRILList.getItemName(), apRILList.getUnit(), user.getCmpCode());
	      	
	      	 double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apRILList.getItemName(), apRILList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
               
	      	
	      		apRILList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

	        }

	        // populate amount field
      		double amount = 0;

	        if(!Common.validateRequired(apRILList.getReceived()) && !Common.validateRequired(apRILList.getUnitCost())) {

	        	amount = Common.convertStringMoneyToDouble(apRILList.getReceived(), precisionUnit) *
					Common.convertStringMoneyToDouble(apRILList.getUnitCost(), precisionUnit);
	        	apRILList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	        }

      		// populate discount field

      		if (!Common.validateRequired(apRILList.getAmount()) &&
      			(!Common.validateRequired(apRILList.getDiscount1()) ||
				!Common.validateRequired(apRILList.getDiscount2()) ||
				!Common.validateRequired(apRILList.getDiscount3()) ||
				!Common.validateRequired(apRILList.getDiscount4()))) {

      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

      			}

      			double discountRate1 = Common.convertStringMoneyToDouble(apRILList.getDiscount1(), precisionUnit)/100;
      			double discountRate2 = Common.convertStringMoneyToDouble(apRILList.getDiscount2(), precisionUnit)/100;
      			double discountRate3 = Common.convertStringMoneyToDouble(apRILList.getDiscount3(), precisionUnit)/100;
      			double discountRate4 = Common.convertStringMoneyToDouble(apRILList.getDiscount4(), precisionUnit)/100;
      			double totalDiscountAmount = 0d;

      			if (discountRate1 > 0) {

      				double discountAmount = amount * discountRate1;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate2 > 0) {

      				double discountAmount = amount * discountRate2;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate3 > 0) {

      				double discountAmount = amount * discountRate3;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate4 > 0) {

      				double discountAmount = amount * discountRate4;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount += amount * (actionForm.getTaxRate() / 100);

      			}

      			apRILList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
      			apRILList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

      		}

      		// populate conversion field
      		if (!Common.validateRequired(apRILList.getConversionFactor())) {

	      		double conversionFactor = ejbRI.getInvUmcByIiNameAndUomName(apRILList.getItemName(), apRILList.getUnit(), user.getCmpCode());
	      		apRILList.setConversionFactor(Common.convertDoubleToStringMoney(conversionFactor, (short)8));

      		}

          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("apReceivingItemEntry"));

/*******************************************************
   -- Ap RI Type Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {

	        if (actionForm.getType().equals("ITEMS")) {

	           	actionForm.clearApRILList();

	           	// Populate line when not forwarding

            	for (int x = 1; x <= journalLineNumber; x++) {

            		ApReceivingItemEntryList apRilList = new ApReceivingItemEntryList(actionForm,
            				null, new Integer(x).toString(), null, null, null, null, null, null,
							null, null, null, null, null, "QCXXXX", Common.convertSQLDateToString(new java.util.Date()), null, null, "0.0","0.0", "0.0", "0.0", null, null, null, null);

            		apRilList.setLocationList(actionForm.getLocationList());
            		apRilList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
            		apRilList.setUnitList(Constants.GLOBAL_BLANK);
            		apRilList.setUnitList("Select Item First");



            		actionForm.saveApRILList(apRilList);

            	}

	        } else {

	        	actionForm.clearApRILList();

	        }

	        return(mapping.findForward("apReceivingItemEntry"));

/*******************************************************
   -- Ap RI Tax Code Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {

         	try {

         		ApTaxCodeDetails details = ejbRI.getApTcByTcName(actionForm.getTaxCode(), user.getCmpCode());
         		actionForm.setTaxRate(details.getTcRate());
         		actionForm.setTaxType(details.getTcType());

         		if (actionForm.getType().equalsIgnoreCase("ITEMS")) {

         			actionForm.clearApRILList();

         			for (int x = 1; x <= journalLineNumber; x++) {


         				ApReceivingItemEntryList apRILList = new ApReceivingItemEntryList(actionForm,
         						null, new Integer(x).toString(), null, null, null, null, null, null,
								null, null, null, null, null, "QCXXXX", Common.convertSQLDateToString(new java.util.Date()), null, null, "0.0","0.0", "0.0", "0.0", null, null, null, null);

         				apRILList.setLocationList(actionForm.getLocationList());
         				apRILList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
         				apRILList.setUnitList(Constants.GLOBAL_BLANK);
         				apRILList.setUnitList("Select Item First");

         				actionForm.saveApRILList(apRILList);

         			}

         		}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("apReceivingItemEntry"));



/*******************************************************
    -- Conversion Date Enter Action --
 *******************************************************/
  } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

  	try {

  		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
  				ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
  						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

  	} catch (GlobalConversionDateNotExistException ex) {

  		errors.add(ActionMessages.GLOBAL_MESSAGE,
  				new ActionMessage("receivingItemEntry.error.conversionDateNotExist"));

  	} catch (EJBException ex) {

  		if (log.isInfoEnabled()) {

  			log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
  					" session: " + session.getId());

  		}

  		return(mapping.findForward("cmnErrorPage"));

  	}

  	if (!errors.isEmpty()) {

  		saveErrors(request, new ActionMessages(errors));

  	}

  	return(mapping.findForward("apReceivingItemEntry"));

/*******************************************************
   -- Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

         	try {

         		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
         		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));

         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("receivingItemEntry.error.conversionDateNotExist"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));

         	}

         	return(mapping.findForward("apReceivingItemEntry"));


/*******************************************************
   -- Ap RI Load Action --
*******************************************************/

         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apReceivingItemEntry"));

            }

            if (request.getParameter("forward") == null && actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }

            try {

            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearCurrencyList();

            	list = ejbRI.getGlFcAllWithDefault(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

            			actionForm.setCurrencyList(mFcDetails.getFcName());

            			if (mFcDetails.getFcSob() == 1) {

            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());

            			}

            		}

            	}

            	actionForm.clearPaymentTermList();

            	list = ejbRI.getAdPytAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setPaymentTermList((String)i.next());

            		}

            		actionForm.setPaymentTerm("IMMEDIATE");

            	}

            	actionForm.clearTaxCodeList();

            	list = ejbRI.getApTcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setTaxCodeList((String)i.next());

            		}

            	}

            	actionForm.clearUserList();

            	ArrayList userList = ejbRI.getAdUsrAll(user.getCmpCode());

            	if (userList == null || userList.size() == 0) {

            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator x = userList.iterator();

            		while (x.hasNext()) {

            			actionForm.setUserList((String)x.next());

            		}

            	}


            	if(actionForm.getUseSupplierPulldown()) {

            		actionForm.clearSupplierList();

            		list = ejbRI.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		if (list == null || list.size() == 0) {

            			actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {

            			i = list.iterator();

            			while (i.hasNext()) {

            				actionForm.setSupplierList((String)i.next());

            			}

            		}

            	}

            	actionForm.clearLocationList();
            	list = ejbRI.getInvLocAll(user.getCmpCode());
            	if (list == null || list.size() == 0) {

            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setLocationList((String)i.next());

            		}

            	}


            	if (isAccessedByDevice(request.getHeader("user-agent"))) {

	            	actionForm.clearPoNumberList();

	            	list = ejbRI.getApRcvOpenPo(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setPoNumberList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            			actionForm.setPoNumberList((String)i.next());

	            		}
	            	}
            	}

            	actionForm.clearApRILList();

            	if (request.getParameter("forward") != null || isInitialPrinting) {

            		if (request.getParameter("forward") != null) {

            			actionForm.setReceivingItemCode(new Integer(request.getParameter("receivingItemCode")));
            		}

            		ApModPurchaseOrderDetails mdetails = ejbRI.getApPoByPoCode(actionForm.getReceivingItemCode(), user.getCmpCode());

            		actionForm.setDescription(mdetails.getPoDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getPoDate()));
            		actionForm.setType(mdetails.getPoType());
            		actionForm.setDocumentNumber(mdetails.getPoDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getPoReferenceNumber());
            		actionForm.setPoNumber(mdetails.getPoRcvPoNumber());
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getPoConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getPoConversionRate(), Constants.CONVERSION_RATE_PRECISION));
            		actionForm.setTotalAmount(Common.convertDoubleToStringMoney(mdetails.getPoTotalAmount(), precisionUnit));
            		actionForm.setReceivingItemVoid(Common.convertByteToBoolean(mdetails.getPoVoid()));
            		actionForm.setShipmentNumber(mdetails.getPoShipmentNumber());
            		actionForm.setApprovalStatus(mdetails.getPoApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getPoReasonForRejection());
            		actionForm.setPosted(mdetails.getPoPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getPoCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getPoDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getPoLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getPoDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getPoApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getPoDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getPoPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getPoDatePosted()));

            		actionForm.setFreight(Common.convertDoubleToStringMoney(mdetails.getPoFreight(), precisionUnit));
            		actionForm.setDuties(Common.convertDoubleToStringMoney(mdetails.getPoDuties(), precisionUnit));
            		actionForm.setEntryFee(Common.convertDoubleToStringMoney(mdetails.getPoEntryFee(), precisionUnit));
            		actionForm.setStorage(Common.convertDoubleToStringMoney(mdetails.getPoStorage(), precisionUnit));
            		actionForm.setWharfageHandling(Common.convertDoubleToStringMoney(mdetails.getPoWharfageHandling(), precisionUnit));

            		actionForm.setShowRecalculateJournal(true);
            		if (!actionForm.getCurrencyList().contains(mdetails.getPoFcName())) {

            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearCurrencyList();

            			}
            			actionForm.setCurrencyList(mdetails.getPoFcName());

            		}
            		actionForm.setCurrency(mdetails.getPoFcName());

            		if (!actionForm.getTaxCodeList().contains(mdetails.getPoTcName())) {

            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearTaxCodeList();

            			}
            			actionForm.setTaxCodeList(mdetails.getPoTcName());

            		}
            		actionForm.setTaxCode(mdetails.getPoTcName());

            		if (!actionForm.getSupplierList().contains(mdetails.getPoSplSupplierCode())) {

            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearSupplierList();

            			}
            			actionForm.setSupplierList(mdetails.getPoSplSupplierCode());

            		}
            		actionForm.setSupplier(mdetails.getPoSplSupplierCode());
	                 actionForm.setSupplierName(mdetails.getPoSplName());

            		if (!actionForm.getPaymentTermList().contains(mdetails.getPoPytName())) {

            			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearPaymentTermList();

            			}
            			actionForm.setPaymentTermList(mdetails.getPoPytName());

            		}
            		actionForm.setPaymentTerm(mdetails.getPoPytName());

					list = mdetails.getPoAPRList();

            		i = list.iterator();

            		while (i.hasNext()) {

            			AdModApprovalQueueDetails mPoAPRDetails = (AdModApprovalQueueDetails)i.next();

            			ApReceivingItemApproverList apPrAPRList = new ApReceivingItemApproverList(actionForm,
            					Common.convertSQLDateAndTimeToString(mPoAPRDetails.getAqApprovedDate()), mPoAPRDetails.getAqApproverName(), mPoAPRDetails.getAqStatus());

            			actionForm.saveApAPRList(apPrAPRList);
            		}

            	    list = mdetails.getPoPlList();

            			i = list.iterator();
            			double totalAmount = 0;
            			while (i.hasNext()) {

            				ApModPurchaseOrderLineDetails mRilDetails = (ApModPurchaseOrderLineDetails)i.next();
            				System.out.println("mRilDetails.getPlAmount()"+mRilDetails.getPlAmount());
            				totalAmount += mRilDetails.getPlAmount();
            				ApReceivingItemEntryList apRilList = new ApReceivingItemEntryList(actionForm,
            						mRilDetails.getPlCode(),
									Common.convertShortToString(mRilDetails.getPlLine()),
									mRilDetails.getPlLocName(),
									mRilDetails.getPlIiName(),
									mRilDetails.getPlIiDescription(),
									Common.convertDoubleToStringMoney(mRilDetails.getPlRemaining(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlQuantity(), precisionUnit),
									mRilDetails.getPlUomName(),
									Common.convertDoubleToStringMoney(mRilDetails.getPlUnitCost(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlUnitCostLocal(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlAmountLocal(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlAddonCost(), precisionUnit),

									mRilDetails.getPlQcNumber(),
									Common.convertSQLDateToString(mRilDetails.getPlQcExpiryDate()),
									Common.convertDoubleToStringMoney(mRilDetails.getPlConversionFactor(), (short)8),
									mRilDetails.getPlPlCode(),
									Common.convertDoubleToStringMoney(mRilDetails.getPlDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlTotalDiscount(), precisionUnit),
									Common.convertDoubleToStringMoney(mRilDetails.getPlTotalDiscount(), precisionUnit),
									mRilDetails.getPlMisc(), mRilDetails.getPlPartNumber());


            				apRilList.setLocationList(actionForm.getLocationList());
            				System.out.println("forwared b?");
            				System.out.println(mRilDetails.getPlTagList() + "getPlTagList forwared");

            				boolean isTraceMisc = ejbRI.getInvTraceMisc(mRilDetails.getPlIiName(), user.getCmpCode());

            				apRilList.setIsTraceMisc(isTraceMisc);

            				apRilList.clearTagList();
            				ArrayList tagList = mRilDetails.getPlTagList();

            				if (isTraceMisc){


            					 tagList = mRilDetails.getPlTagList();


            					 
            					 this.loadInvTagList(apRilList, tagList);
            					 
            					 
            					 
            				
            					 
            					 System.out.println("serial is: "+ apRilList.getSerialNumber());
            					 
            					 
                            	if(tagList.size() > 0) {
                            		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mRilDetails.getPlQuantity()));

                            		System.out.println(misc + "<== misc");
                            	//	mRilDetails.setPlMisc(misc);

                            		
                            		apRilList.setMisc(misc);

                            	}else {
                              		if(mRilDetails.getPlMisc()==null) {

                              			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mRilDetails.getPlQuantity()));
                              			apRilList.setMisc(misc);
                              			System.out.println(misc + "<== misc");
                              		}
                              	}




            					System.out.println(mRilDetails.getPlMisc() + "<== misc");
            				}



            				System.out.println(apRilList.getTagListSize() + "<== apRilList taglist");

            				apRilList.clearUnitList(); 
			            	ArrayList unitList = ejbRI.getInvUomByIiName(mRilDetails.getPlIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {

			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		apRilList.setUnitList(mUomDetails.getUomName());

			            	}

            				actionForm.saveApRILList(apRilList);


            			}

            			if (actionForm.getType().equals("ITEMS")) {

	            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

	            			for (int x = list.size() + 1; x <= remainingList; x++) {

	            				ApReceivingItemEntryList apRILList = new ApReceivingItemEntryList(actionForm,
	            						null, String.valueOf(x), null, null, null, null, null, null, null, null, null, null, null,
	            						"QCXXXX", Common.convertSQLDateToString(new java.util.Date()), null, null,
										"0.0","0.0", "0.0", "0.0", null, null, null, null);

	            				apRILList.setLocationList(actionForm.getLocationList());
	            				apRILList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
	            				apRILList.setUnitList(Constants.GLOBAL_BLANK);
	            				apRILList.setUnitList("Select Item First");

	            				actionForm.saveApRILList(apRILList);

	            			}

            			}

            			this.setFormProperties(actionForm, user.getCompany());
            			return (mapping.findForward("apReceivingItemEntry"));

            	}

            	// Populate line when not forwarding

            	if (user.getUserApps().contains("OMEGA INVENTORY")) {

	            	for (int x = 1; x <= journalLineNumber; x++) {

	            		ApReceivingItemEntryList apRILList = new ApReceivingItemEntryList(actionForm,
	            				null, new Integer(x).toString(), null, null, null, null, null, null, null,
	            				null, null, null, null, "QCXXXX", Common.convertSQLDateToString(new java.util.Date()), null, null, "0.0","0.0", "0.0", "0.0", null, null, null, null);

	            		apRILList.setLocationList(actionForm.getLocationList());
	            		apRILList.setLocation(actionForm.getLocationList().size() > 1 ? (String)actionForm.getLocationList().get(1) : Constants.GLOBAL_BLANK);
	            		apRILList.setUnitList(Constants.GLOBAL_BLANK);
	            		apRILList.setUnitList("Select Item First");
	            		actionForm.saveApRILList(apRILList);

	            	}

            	}



            } catch(GlobalNoRecordFoundException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receivingItemEntry.error.receivingItemAlreadyDeleted"));

            } catch(EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }

               return(mapping.findForward("cmnErrorPage"));

            }



            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveSubmitButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   try {

                   	   ArrayList list = ejbRI.getAdApprovalNotifiedUsersByPoCode(actionForm.getReceivingItemCode(), user.getCmpCode());

                   	   if (list.isEmpty()) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));

                   	   } else if (list.contains("DOCUMENT POSTED")) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                	   	       new ActionMessage("messages.documentPosted"));

                   	   } else {

                   	   	   Iterator i = list.iterator();

                   	   	   String APPROVAL_USERS = "";

                   	   	   while (i.hasNext()) {

                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

                   	   	       if (i.hasNext()) {

                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";

                   	   	       }


                   	   	   }

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

                   	   }

                   	   saveMessages(request, messages);
                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);


                   } catch(EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in ApReceivingItemEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }

		               return(mapping.findForward("cmnErrorPage"));

		           }

               } else if (request.getParameter("saveAsDraftButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }

            }

            actionForm.reset(mapping, request);

            if (actionForm.getType() == null) {

            	actionForm.setType("PO MATCHED");

            }

            actionForm.setReceivingItemCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setConversionRate(Common.convertDoubleToStringMoney(1d, Constants.CONVERSION_RATE_PRECISION));
            actionForm.setTotalAmount("0");
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

            this.setFormProperties(actionForm, user.getCompany());

/*******************************************************
-- Check if the request was sent by a mobile device --
*******************************************************/

            if (isAccessedByDevice(request.getHeader("user-agent"))) {
            	System.out.println("Mobile Device Detected. = " + request.getHeader("user-agent"));
                return(mapping.findForward("apReceivingItemEntry2"));
            }
            else return(mapping.findForward("apReceivingItemEntry"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

            log.info("Exception caught in ApReceivingItemEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }

	private void setFormProperties(ApReceivingItemEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getPosted().equals("NO") && !actionForm.getReceivingItemVoid()) {

				if (actionForm.getReceivingItemCode() == null) {
					System.out.println("getReceivingItemCode = null");
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableReceivingItemVoid(false);

				} else if (actionForm.getReceivingItemCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					System.out.println("getReceivingItemCode != null");
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableReceivingItemVoid(true);

				} else {
					System.out.println("else disable");
					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableReceivingItemVoid(false);

				}

			} else if (actionForm.getPosted().equals("YES")) {
				System.out.println("actionForm.getPosted().equals YES");
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setEnableReceivingItemVoid(false);

			} else if (actionForm.getReceivingItemVoid()) {
				System.out.println("actionForm.getReceivingItemVoid ");
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableReceivingItemVoid(false);

			}



		} else {
			System.out.println("all else disable");
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableReceivingItemVoid(false);

		}
		// view attachment

		if (actionForm.getReceivingItemCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/Receiving Item/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			File file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-1" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton1(true);

 			} else {

 				actionForm.setShowViewAttachmentButton1(false);

 			}

 			file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-2" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton2(true);

 			} else {

 				actionForm.setShowViewAttachmentButton2(false);

 			}

 			file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-3" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton3(true);

 			} else {

 				actionForm.setShowViewAttachmentButton3(false);

 			}

 			file = new File(attachmentPath + actionForm.getReceivingItemCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceivingItemCode() + "-4" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton4(true);

 			} else {

 				actionForm.setShowViewAttachmentButton4(false);

 			}

		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}

	}
	
	
	public ArrayList convertInvTagListToArrayList(ApReceivingItemEntryList apRilList, String transactionDate, String type) throws InvTagSerialNumberAlreadyExistException {
		
		ArrayList list = new ArrayList();
		String propertyCode = apRilList.getPropertyCode();
	 	String specs = apRilList.getSpecs();
	 	String serialNumber = apRilList.getSerialNumber();
	 	   String custodian = apRilList.getCustodian();
	 	   String expiryDate = apRilList.getExpiryDate();
	 	   
	 	   String[] tryit =  serialNumber.split(",");
	 	   
	 	   
	 	 String[] arrpropertyCode = propertyCode.split(",",-1);
	 	 String[] arrspecs = specs.split(",",-1);
	 	 String[] arrserialNumber =  serialNumber.split(",",-1);
	 	 String[] arrcustodian =  custodian.split(",",-1);
	 	 String[] arrexpiryDate = expiryDate.split(",",-1);
	 	   
	 	 int qty = propertyCode.split(",",-1).length;
	 	    
	 	 ArrayList tempArraySerial = new ArrayList();
	 	 
	 	 for(int x=0;x<qty;x++) {
	 		 
	 		 int lengthValue = arrpropertyCode[x].trim().length() + arrspecs[x].trim().length() + arrserialNumber[x].trim().length() + arrcustodian[x].trim().length() + arrexpiryDate[x].trim().length();
	 		 
	 		 if(lengthValue<=0) {
	 			 continue;
	 		 }
	 		 
	 		InvModTagListDetails tgDetails = new InvModTagListDetails();
	 		System.out.println("count :" + x); 
	 		tgDetails.setTgPropertyCode(arrpropertyCode[x]);
	 		tgDetails.setTgSpecs(arrspecs[x]);
	 		tgDetails.setTgSerialNumber(arrserialNumber[x]);
	 		tgDetails.setTgCustodian(arrcustodian[x]);
	 		tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(arrexpiryDate[x]));
	 		tgDetails.setTgTransactionDate(Common.convertStringToSQLDate(transactionDate));
	 		tgDetails.setTgType(type);
	 		list.add(tgDetails);
	 		
	 		tempArraySerial.add(arrserialNumber[x]);
	 		 
	 	 }
	 	 
	 	 
	 	 
	 	 //trace if something duplicate
	 	 for(int x=0;x<tempArraySerial.size();x++) {
	 		 
	 		 String serialComp1 = tempArraySerial.get(x).toString();
	 		 
	 		 for(int y=0;y<tempArraySerial.size();y++) {
	 			
	 			if(serialComp1.equals(tempArraySerial.get(y)) && y!=x) {
	 				throw new  InvTagSerialNumberAlreadyExistException(serialComp1);
	 			}
	 			 
	 		 }
	 		 
	 		 
	 	 }
	 	 
	 	 
	 	return list;
		
		
		
	}
	
	
	
	
	
	private void loadInvTagList(ApReceivingItemEntryList apRilList, ArrayList tagList) {
	 	   
	   	 
	 	   String propertyCode = "";
	 	   String specs = "";
	 	   String serialNumber = "";
	 	   String custodian = "";
	 	   String expiryDate = "";
	 	   
	 	   for(int x=0;x<tagList.size();x++) {
	    		
	   		 	InvModTagListDetails tagListDetails = (InvModTagListDetails)tagList.get(x);
	   		 
	   		 	if(x>0) {
	   		 		propertyCode += ",";
	   		 		specs += ",";
	   		 		serialNumber += ",";
	   		 		custodian += ",";
	   		 		expiryDate += ",";
	   		 		
	   		 	}
	   		
	   		 	if(!Common.validateRequired(tagListDetails.getTgPropertyCode())){
	   		 		propertyCode += tagListDetails.getTgPropertyCode();
	   		 	}
	   		    
	   		 	if(!Common.validateRequired(tagListDetails.getTgSpecs())){
	   		 		specs += tagListDetails.getTgSpecs();
	   		 	}
	   		 	
	   		 	if(!Common.validateRequired(tagListDetails.getTgSerialNumber())){
	   		 		serialNumber += tagListDetails.getTgSerialNumber();
	   		 	}
	   		 	
	   		 	if(!Common.validateRequired(tagListDetails.getTgCustodian())){
	   		 		custodian += tagListDetails.getTgCustodian();
	   		 	}
	   		 
	   		 	if(!Common.validateRequired(Common.convertSQLDateToString(tagListDetails.getTgExpiryDate()))){
	   		 		expiryDate += tagListDetails.getTgExpiryDate();
	   		 	}
	   		 	
	   		 
	   	   }
	 	   
	 	   System.out.println("s: " + serialNumber);
	 	 
	 	  apRilList.setPropertyCode(propertyCode);
	 	  apRilList.setSpecs(specs);
	 	  apRilList.setSerialNumber(serialNumber);
	 	  apRilList.setCustodian(custodian);
	 	  apRilList.setExpiryDate(expiryDate);
	 	  
	 	   
	    }
	    

	private ArrayList getDrClassList() {

		ArrayList classList = new ArrayList();

     	classList.add(Constants.AP_DR_CLASS_EXPENSE);
     	classList.add(Constants.AP_DR_CLASS_TAX);
     	classList.add(Constants.AP_DR_CLASS_WITHHOLDING_TAX);
     	classList.add(Constants.AP_DR_CLASS_OTHER);

     	return classList;

	}

	private Boolean isAccessedByDevice(String user_agent) {
		if (user_agent!=null) {
			if (user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows CE)")||
				user_agent.equals("Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)")||
				user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)")
			) return true;
			return false;
		}
		else return true;
	}

}