package com.struts.ap.receivingitementry;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;

public class ApReceivingItemEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
	private Integer receivingItemCode = null;
	private boolean receivingReport = false;
	private boolean inspectedReport = false;
	private boolean releasedReport = false;
	private String supplier = null;
	private ArrayList supplierList = new ArrayList();
	private ArrayList poNumberList = new ArrayList();
	private String date = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String documentNumber = null;
	private String totalAmount = null;
	private String shipmentNumber = null;
	private String referenceNumber = null;
	private String poNumber = null;
	private String paymentTerm = null;
	private ArrayList paymentTermList = new ArrayList();
	private boolean receivingItemVoid = false;
	private String description = null;
	private String taxCode = null;
	private double taxRate = 0d;
	private String taxType = null;
	private ArrayList taxCodeList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String conversionDate = null;
	private String conversionRate = null;
	private String approvalStatus = null;
	private String reasonForRejection = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();

	private String supplierName = null;
	private String functionalCurrency = null;
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;

	private String freight =null;
	private String duties = null;
	private String entryFee = null;
	private String storage = null;
	private String wharfageHandling = null;

	private ArrayList apAPRList = new ArrayList();
	private ArrayList apRILList = new ArrayList();
	private ArrayList userList = new ArrayList();
	private ArrayList tagList = new ArrayList();
	private String user = null;

	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean enableReceivingItemVoid = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showDeleteButton = false;
	private boolean showSaveButton = false;
	private boolean useSupplierPulldown = true;
	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;
	private boolean showRecalculateJournal = false;
	

	private String isSupplierEntered = null;
	private String isPoNumberEntered = null;
	private String isTaxCodeEntered = null;
	private String isConversionDateEntered = null;
	
	
	
	private boolean recalculateJournal = false;

	private String report = null;
	private String attachment = null;
	private String attachmentPDF = null;

	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();


	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getAttachmentPDF() {

		return attachmentPDF;

	}

	public void setAttachmentPDF(String attachmentPDF) {

		this.attachmentPDF = attachmentPDF;

	}

	public String getViewType() {

		return viewType;

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}

	public ArrayList getViewTypeList() {

		return viewTypeList;

	}

	public String getAttachment() {

		return attachment;

	}

	public void setAttachment(String attachment) {

		this.attachment = attachment;

	}

	public boolean getShowViewAttachmentButton1() {

		return showViewAttachmentButton1;

	}

	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

		this.showViewAttachmentButton1 = showViewAttachmentButton1;

	}

	public boolean getShowViewAttachmentButton2() {

		return showViewAttachmentButton2;

	}

	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

		this.showViewAttachmentButton2 = showViewAttachmentButton2;

	}

	public boolean getShowViewAttachmentButton3() {

		return showViewAttachmentButton3;

	}

	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

		this.showViewAttachmentButton3 = showViewAttachmentButton3;

	}

	public boolean getShowViewAttachmentButton4() {

		return showViewAttachmentButton4;

	}

	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

		this.showViewAttachmentButton4 = showViewAttachmentButton4;

	}
	
	
	public boolean getShowRecalculateJournal() {

		return showRecalculateJournal;

	}

	public void setShowRecalculateJournal(boolean showRecalculateJournal) {

		this.showRecalculateJournal = showRecalculateJournal;

	}

	public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {

		return filename3;

	}

	public void setFilename3(FormFile filename3) {

		this.filename3 = filename3;

	}

	public FormFile getFilename4() {

		return filename4;

	}

	public void setFilename4(FormFile filename4) {

		this.filename4 = filename4;

	}

	public int getRowSelected() {
		return rowSelected;
	}

	public ApReceivingItemEntryList getApRILByIndex(int index) {
		return ((ApReceivingItemEntryList) apRILList.get(index));
	}

	public Object[] getApRILList() {
		return (apRILList.toArray());
	}

	public int getApRILListSize() {
		return (apRILList.size());
	}

	public void saveApRILList(Object newApRILList) {
		apRILList.add(newApRILList);
	}

	public void clearApRILList() {
		apRILList.clear();
	}

	public ApReceivingItemEntryTagList getTagListByIndex(int index) {
		return ((ApReceivingItemEntryTagList) tagList.get(index));
	}

	public Object[] getTagList() {
		return (tagList.toArray());
	}

	public int getApRILTagListSize() {
		return (tagList.size());
	}

	public void saveApRILTagList(Object newApRILTagList) {
		tagList.add(newApRILTagList);
	}

	public void clearTagList() {
		tagList.clear();
	}

	public void setRowSelected(Object selectedApRILList, boolean isEdit) {
		this.rowSelected = apRILList.indexOf(selectedApRILList);
	}

	public void updateApRILRow(int rowSelected, Object newApRILList) {
		apRILList.set(rowSelected, newApRILList);
	}

	public void deleteApRILList(int rowSelected) {
		apRILList.remove(rowSelected);
	}

	public String getTxnStatus() {
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return (passTxnStatus);
	}

	public void setTxnStatus(String txnStatus) {
		this.txnStatus = txnStatus;
	}

	public String getUserPermission() {
		return (userPermission);
	}

	public void setUserPermission(String userPermission) {
		this.userPermission = userPermission;
	}

	public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

	public Integer getReceivingItemCode() {

		return receivingItemCode;

	}

	public void setReceivingItemCode(Integer receivingItemCode) {

		this.receivingItemCode = receivingItemCode;

	}

	public String getSupplier() {

		return supplier;

	}

	public void setSupplier(String supplier) {

		this.supplier = supplier;

	}

	public ArrayList getSupplierList() {

		return supplierList;

	}

	public void setSupplierList(String supplier) {

		supplierList.add(supplier);

	}

	public void clearSupplierList() {

		supplierList.clear();
		supplierList.add(Constants.GLOBAL_BLANK);

	}

	public ArrayList getUserList() {

		return userList;

	}

	public void setUserList(String user) {

		userList.add(user);

	}

	public void clearUserList() {

		userList.clear();
		userList.add(Constants.GLOBAL_BLANK);

	}

	public String getUser() {

		return user;

	}

	public void setUser(String user) {

		this.user = user;

	}

	public String getDate() {

		return date;

	}

	public void setDate(String date) {

		this.date = date;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {

		this.type = type;

	}

	public ArrayList getTypeList() {

		return typeList;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public void setDocumentNumber(String documentNumber) {

		this.documentNumber = documentNumber;

	}

	public String getTotalAmount() {

		return totalAmount;

	}

	public void setTotalAmount(String totalAmount) {

		this.totalAmount = totalAmount;

	}

	public String getShipmentNumber() {

		return shipmentNumber;

	}

	public void setShipmentNumber(String shipmentNumber) {

		this.shipmentNumber = shipmentNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public void setReferenceNumber(String referenceNumber) {

		this.referenceNumber = referenceNumber;

	}

	public String getPoNumber() {

		return poNumber;

	}

	public void setPoNumber(String poNumber) {

		this.poNumber = poNumber;

	}

	public ArrayList getPoNumberList() {

		return poNumberList;

	}

	public void setPoNumberList(String poNumber) {

		poNumberList.add(poNumber);

	}

	public void clearPoNumberList() {

		poNumberList.clear();
		poNumberList.add(Constants.GLOBAL_BLANK);

	}

	public String getPaymentTerm() {

		return paymentTerm;

	}

	public void setPaymentTerm(String paymentTerm) {

		this.paymentTerm = paymentTerm;

	}

	public ArrayList getPaymentTermList() {

		return paymentTermList;

	}

	public void setPaymentTermList(String paymentTerm) {

		paymentTermList.add(paymentTerm);

	}

	public void clearPaymentTermList() {

		paymentTermList.clear();

	}

	public boolean getReceivingItemVoid() {

		return receivingItemVoid;

	}

	public void setReceivingItemVoid(boolean receivingItemVoid) {

		this.receivingItemVoid = receivingItemVoid;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

		this.description = description;

	}

	public String getTaxCode() {

		return taxCode;

	}

	public void setTaxCode(String taxCode) {

		this.taxCode = taxCode;

	}

	public ArrayList getTaxCodeList() {

		return taxCodeList;

	}

	public void setTaxCodeList(String taxCode) {

		taxCodeList.add(taxCode);

	}

	public void clearTaxCodeList() {

		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);

	}

	public String getCurrency() {

		return currency;

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}

	public void setCurrencyList(String currency) {

		currencyList.add(currency);

	}

	public void clearCurrencyList() {

		currencyList.clear();

	}

	public String getConversionDate() {

		return conversionDate;

	}

	public void setConversionDate(String conversionDate) {

		this.conversionDate = conversionDate;

	}

	public String getConversionRate() {

		return conversionRate;

	}

	public void setConversionRate(String conversionRate) {

		this.conversionRate = conversionRate;

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public void setApprovalStatus(String approvalStatus) {

		this.approvalStatus = approvalStatus;

	}

	public String getReasonForRejection() {

		return reasonForRejection;

	}

	public void setReasonForRejection(String reasonForRejection) {

		this.reasonForRejection = reasonForRejection;

	}

	public String getPosted() {

		return posted;

	}

	public void setPosted(String posted) {

		this.posted = posted;

	}

	public String getCreatedBy() {

		return createdBy;

	}

	public void setCreatedBy(String createdBy) {

		this.createdBy = createdBy;

	}

	public String getDateCreated() {

		return dateCreated;

	}

	public void setDateCreated(String dateCreated) {

		this.dateCreated = dateCreated;

	}

	public String getLastModifiedBy() {

		return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

		this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

		return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

		this.dateLastModified = dateLastModified;

	}

	public String getApprovedRejectedBy() {

		return approvedRejectedBy;

	}

	public void setApprovedRejectedBy(String approvedRejectedBy) {

		this.approvedRejectedBy = approvedRejectedBy;

	}

	public String getDateApprovedRejected() {

		return dateApprovedRejected;

	}

	public void setDateApprovedRejected(String dateApprovedRejected) {

		this.dateApprovedRejected = dateApprovedRejected;

	}

	public String getPostedBy() {

		return postedBy;

	}

	public void setPostedBy(String postedBy) {

		this.postedBy = postedBy;

	}

	public String getDatePosted() {

		return datePosted;

	}

	public void setDatePosted(String datePosted) {

		this.datePosted = datePosted;

	}

	public String getFunctionalCurrency() {

		return functionalCurrency;

	}

	public void setFunctionalCurrency(String functionalCurrency) {

		this.functionalCurrency = functionalCurrency;

	}

	public boolean getEnableFields() {

		return enableFields;

	}

	public void setEnableFields(boolean enableFields) {

		this.enableFields = enableFields;

	}

	public boolean getEnableReceivingItemVoid() {

		return enableReceivingItemVoid;

	}

	public void setEnableReceivingItemVoid(boolean enableReceivingItemVoid) {

		this.enableReceivingItemVoid = enableReceivingItemVoid;

	}

	public boolean getShowAddLinesButton() {

		return showAddLinesButton;

	}

	public void setShowAddLinesButton(boolean showAddLinesButton) {

		this.showAddLinesButton = showAddLinesButton;

	}

	public boolean getShowDeleteLinesButton() {

		return showDeleteLinesButton;

	}

	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

		this.showDeleteLinesButton = showDeleteLinesButton;

	}

	public boolean getShowDeleteButton() {

		return showDeleteButton;

	}

	public void setShowDeleteButton(boolean showDeleteButton) {

		this.showDeleteButton = showDeleteButton;

	}

	public boolean getShowSaveButton() {

		return showSaveButton;

	}

	public void setShowSaveButton(boolean showSaveButton) {

		this.showSaveButton = showSaveButton;

	}

	public String getIsSupplierEntered() {

		return isSupplierEntered;

	}

	public String getIsPoNumberEntered() {

		return isPoNumberEntered;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(String location) {

		locationList.add(location);

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getReceivingReport() {

		return receivingReport;

	}

	public void setReceivingReport(boolean receivingReport) {

		this.receivingReport = receivingReport;

	}

	public boolean getInspectedReport() {

		return inspectedReport;

	}

	public void setInspectedReport(boolean inspectedReport) {

		this.inspectedReport = inspectedReport;

	}

	public boolean getReleasedReport() {

		return releasedReport;

	}

	public void setReleasedReport(boolean releasedReport) {

		this.releasedReport = releasedReport;

	}


	public String getFreight() {

		return freight;

	}

	public void setFreight(String freight) {

		this.freight = freight;

	}


	public String getDuties() {

		return duties;

	}

	public void setDuties(String duties) {

		this.duties = duties;

	}
	
	
	public String getEntryFee() {

		return entryFee;

	}

	public void setEntryFee(String entryFee) {

		this.entryFee = entryFee;

	}

	public String getStorage() {

		return storage;

	}

	public void setStorage(String storage) {

		this.storage = storage;

	}

	public String getWharfageHandling() {

		return wharfageHandling;

	}

	public void setWharfageHandling(String wharfageHandling) {

		this.wharfageHandling = wharfageHandling;

	}
	

	public boolean getUseSupplierPulldown() {

		return useSupplierPulldown;

	}

	public void setUseSupplierPulldown(boolean useSupplierPulldown) {

		this.useSupplierPulldown = useSupplierPulldown;

	}

	public String getSupplierName() {

		return supplierName;

	}

	public void setSupplierName(String supplierName) {

		this.supplierName = supplierName;

	}

	public double getTaxRate() {

		return taxRate;

	}

	public void setTaxRate(double taxRate) {

		this.taxRate = taxRate;

	}

	public String getTaxType() {

		return taxType;

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public String getIsTaxCodeEntered() {

		return isTaxCodeEntered;

	}

	public String getIsConversionDateEntered() {

		return isConversionDateEntered;

	}
	
	
	public boolean getRecalculateJournal() {

		return recalculateJournal;

	}

	public void setRecalculateJournal(boolean recalculateJournal) {

		this.recalculateJournal= recalculateJournal;

	}

	public Object[] getApAPRList(){

		return(apAPRList.toArray());

	}

	public void saveApAPRList(Object newApAPRList){

		apAPRList.add(newApAPRList);

	}

	public void deleteApAPRList(int rowSelected){

		apAPRList.remove(rowSelected);

	}

	public int getApAPRListSize(){

		return(apAPRList.size());

	}

	public void clearApAPRList(){

		apAPRList.clear();

	}

	public ApReceivingItemApproverList getApAPRByIndex(int index){

		return((ApReceivingItemApproverList)apAPRList.get(index));

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;

		supplier = Constants.GLOBAL_BLANK;
		taxCode = Constants.GLOBAL_BLANK;
		type = null;
		typeList.clear();
		typeList.add("PO MATCHED");
		typeList.add("ITEMS");
		date = null;
		poNumber = null;
		documentNumber = null;
		totalAmount = null;
		shipmentNumber = null;
		referenceNumber = null;
		receivingItemVoid = false;
		description = null;
		conversionDate = null;
		conversionRate = null;
		approvalStatus = null;
		reasonForRejection = null;
		posted = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;
		approvedRejectedBy = null;
		dateApprovedRejected = null;
		postedBy = null;
		datePosted = null;
		isSupplierEntered = null;
		location = Constants.GLOBAL_BLANK;
		receivingReport = false;
		inspectedReport = false;
		releasedReport = false;
		supplierName = Constants.GLOBAL_BLANK;
		taxRate = 0d;
		taxType = Constants.GLOBAL_BLANK;
		isTaxCodeEntered = null;
		isConversionDateEntered = null;
		freight = null;
		duties = null;
		entryFee = null;
		storage = null;
		wharfageHandling = null;
		filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
		attachment = null;
		attachmentPDF = null;
		recalculateJournal = false;

		for (int i = 0; i < apRILList.size(); i++) {

			ApReceivingItemEntryList actionList = (ApReceivingItemEntryList) apRILList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);
			/*
			 * for (int x=0; x<4; x++) { ApReceivingItemEntryTagList actionTagList = new
			 * ApReceivingItemEntryTagList(actionList, "1234", "Core I7:");
			 * actionList.saveTgList(actionTagList); }
			 */
		}


	for (int i=0; i<apAPRList.size(); i++) {

		  	  ApReceivingItemApproverList actionList = (ApReceivingItemApproverList)apAPRList.get(i);
		  	  actionList.setApproverName(null);
		  	  actionList.setStatus(null);

		  }
	}

	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		// ActionErrors errors = new ActionErrors();

		Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		String separator = "$";

		// Remove first $ character
		misc = misc.substring(1);

		// Counter
		int start = 0;
		int nextIndex = misc.indexOf(separator, start);
		int length = nextIndex - start;
		// y = new Integer(Integer.parseInt(misc.substring(start, start + length)));

		/*
		 * if(y==0) return new ArrayList();
		 */

		ArrayList miscList = new ArrayList();

		for (int x = 0; x < ctr; x++) {
			try {

				// Date
				start = nextIndex + 1;
				nextIndex = misc.indexOf(separator, start);
				length = nextIndex - start;
				/*
				 * SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				 * sdf.setLenient(false);
				 */
				String checker = misc.substring(start, start + length);
				if (checker != "" && checker != " ") {
					miscList.add(checker);
				} else {
					miscList.add("null");
				}

				// System.out.println(misc.substring(start, start + length));
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}
		return miscList;
	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveSubmitButton") != null || request.getParameter("saveAsDraftButton") != null
				|| request.getParameter("printButton") != null || request.getParameter("printReleaseButton") != null
				|| request.getParameter("printInspectionButton") != null
				|| request.getParameter("journalButton") != null)

		{

			if (Common.validateRequired(description)) {
				errors.add("description", new ActionMessage("receivingItemEntry.error.descriptionIsRequired"));
			}

			if (Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("supplier", new ActionMessage("receivingItemEntry.error.supplierRequired"));
			}

			if (Common.validateRequired(referenceNumber)) {
				errors.add("referenceNumber", new ActionMessage("receivingItemEntry.error.referenceNumberRequired"));
			}

			if (Common.validateRequired(poNumber) && type.equals("PO MATCHED")) {
				errors.add("poNumber", new ActionMessage("receivingItemEntry.error.poNumberRequired"));
			}

			if (Common.validateRequired(date)) {
				errors.add("date", new ActionMessage("receivingItemEntry.error.dateRequired"));
			}

			if (!Common.validateDateFormat(date)) {
				errors.add("date", new ActionMessage("receivingItemEntry.error.dateInvalid"));
			}

			int year = Integer.parseInt(date.substring(date.lastIndexOf("/") + 1, (date.lastIndexOf("/") + 5)));
			if (year < 1900) {
				errors.add("date", new ActionMessage("purchaseOrderEntry.error.dateInvalid"));
			}

			if (Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("paymentTerm", new ActionMessage("receivingItemEntry.error.paymentTermRequired"));
			}

			if (Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("taxCode", new ActionMessage("receivingItemEntry.error.taxCodeRequired"));
			}

			if (Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("currency", new ActionMessage("receivingItemEntry.error.currencyRequired"));
			}

			if (!Common.validateDateFormat(conversionDate)) {
				errors.add("conversionDate", new ActionMessage("receivingItemEntry.error.conversionDateInvalid"));
			}

			if (!Common.validateMoneyRateFormat(conversionRate)) {
				errors.add("conversionRate", new ActionMessage("receivingItemEntry.error.conversionRateInvalid"));
			}

			if (currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)) {
				errors.add("conversionDate", new ActionMessage("receivingItemEntry.error.conversionDateMustBeNull"));
			}

			if (currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate)
					&& Common.convertStringMoneyToDouble(conversionRate, (short) 6) != 1d) {
				errors.add("conversionRate", new ActionMessage("receivingItemEntry.error.conversionRateMustBeNull"));
			}

			if ((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate)
					&& Common.validateRequired(conversionDate)) {
				errors.add("conversionRate",
						new ActionMessage("receivingItemEntry.error.conversionRateOrDateMustBeEntered"));
			}

			int numOfLines = 0;

			Iterator i = apRILList.iterator();

			while (i.hasNext()) {

				ApReceivingItemEntryList rilList = (ApReceivingItemEntryList) i.next();

				if (Common.validateRequired(rilList.getItemName()) && Common.validateRequired(rilList.getReceived())
						&& Common.validateRequired(rilList.getUnit()) && Common.validateRequired(rilList.getUnitCost()))
					continue;

				numOfLines++;

				/*
				 * try{ String separator = "$"; String misc = ""; // Remove first $ character
				 * misc = rilList.getMisc().substring(1);
				 *
				 * // Counter int start = 0; int nextIndex = misc.indexOf(separator, start); int
				 * length = nextIndex - start; int counter; counter =
				 * Integer.parseInt(misc.substring(start, start + length));
				 *
				 * ArrayList miscList = this.getExpiryDateStr(rilList.getMisc(), counter);
				 * System.out.println("rilList.getMisc() : " + rilList.getMisc()); Iterator mi =
				 * miscList.iterator();
				 *
				 * int ctrError = 0; int ctr = 0;
				 *
				 * while(mi.hasNext()){ String miscStr = (String)mi.next();
				 *
				 * if(!Common.validateDateFormat(miscStr)){ errors.add("date", new
				 * ActionMessage("receivingItemEntry.error.expiryDateInvalid")); ctrError++; }
				 *
				 * System.out.println("miscStr: "+miscStr); if(miscStr=="null"){ ctrError++; } }
				 * //if ctr==Error => No Date Will Apply //if ctr>error => Invalid Date
				 * System.out.println("CTR: "+ miscList.size());
				 * System.out.println("ctrError: "+ ctrError); System.out.println("counter: "+
				 * counter); if(ctrError>0 && ctrError!=miscList.size()){ errors.add("date", new
				 * ActionMessage("receivingItemEntry.error.expiryDateNullInvalid")); }
				 *
				 * //if error==0 Add Expiry Date
				 *
				 *
				 * }catch(Exception ex){
				 *
				 * }
				 */

				if (Common.validateRequired(rilList.getLocation())
						|| rilList.getLocation().equals(Constants.GLOBAL_BLANK)) {
					errors.add("location",
							new ActionMessage("receivingItemEntry.error.locationRequired", rilList.getLineNumber()));
				}
				if (Common.validateRequired(rilList.getItemName())
						|| rilList.getItemName().equals(Constants.GLOBAL_BLANK)) {
					errors.add("itemName",
							new ActionMessage("receivingItemEntry.error.itemNameRequired", rilList.getLineNumber()));
				}

				/*
				 * if(Common.validateRequired(rilList.getQcNumber()) ||
				 * rilList.getQcNumber().equals(Constants.GLOBAL_BLANK)){ errors.add("qcNumber",
				 * new ActionMessage("receivingItemEntry.error.qcNumberRequired",
				 * rilList.getLineNumber())); }
				 *
				 *
				 * if(Common.validateRequired(rilList.getQcExpiryDate()) ||
				 * rilList.getQcExpiryDate().equals(Constants.GLOBAL_BLANK) ){
				 * errors.add("qcExpiryDate", new
				 * ActionMessage("receivingItemEntry.error.qcExpiryDateRequired",
				 * rilList.getLineNumber())); }
				 *
				 * if(!Common.validateDateFormat(rilList.getQcExpiryDate())){
				 * errors.add("qcExpiryDate", new
				 * ActionMessage("receivingItemEntry.error.qcExpiryDateInvalid",
				 * rilList.getLineNumber())); }
				 */

				if (Common.validateRequired(rilList.getReceived())) {
					errors.add("received",
							new ActionMessage("receivingItemEntry.error.receivedRequired", rilList.getLineNumber()));
				}
				if (!Common.validateNumberFormat(rilList.getReceived())) {
					errors.add("received",
							new ActionMessage("receivingItemEntry.error.receivedInvalid", rilList.getLineNumber()));
				}
				if (Common.validateRequired(rilList.getUnit()) || rilList.getUnit().equals(Constants.GLOBAL_BLANK)) {
					errors.add("unit",
							new ActionMessage("receivingItemEntry.error.unitRequired", rilList.getLineNumber()));
				}
				if (Common.validateRequired(rilList.getUnitCost())) {
					errors.add("unitCost",
							new ActionMessage("receivingItemEntry.error.unitCostRequired", rilList.getLineNumber()));
				}
				if (!Common.validateMoneyFormat(rilList.getUnitCost())) {
					System.out.println("UNIT COST TEST: " + rilList.getUnitCost());
					errors.add("unitCost",
							new ActionMessage("receivingItemEntry.error.unitCostInvalid", rilList.getLineNumber()));
				}

				// Items
				if (type.equals("ITEMS")) {

					if (!Common.validateRequired(rilList.getReceived())
							&& Common.convertStringMoneyToDouble(rilList.getReceived(), (short) 3) <= 0) {
						errors.add("received", new ActionMessage(
								"receivingItemEntry.error.negativeOrZeroQuantityNotAllowed", rilList.getLineNumber()));
					}

				}
				// PO Matched
				if (type.equals("PO MATCHED")) {

					if (!Common.validateRequired(rilList.getReceived())
							&& Common.convertStringMoneyToDouble(rilList.getReceived(), (short) 3) < 0) {
						errors.add("received", new ActionMessage("receivingItemEntry.error.negativeQuantityNotAllowed",
								rilList.getLineNumber()));
					}

					// 0 qty received
					if (!Common.validateRequired(rilList.getReceived())
							&& Common.convertStringMoneyToDouble(rilList.getReceived(), (short) 3) == 0) {
						numOfLines--;
					}

					if (approvalStatus.length() == 0 && Common.convertStringMoneyToDouble(rilList.getReceived(),
							(short) 3) > Common.convertStringMoneyToDouble(rilList.getRemaining(), (short) 3)) {
						errors.add("received",
								new ActionMessage(
										"receivingItemEntry.error.receivedQuantityGreaterThanRemainingQuantity",
										rilList.getLineNumber()));
					}

				}

			}

			if (numOfLines == 0) {

				errors.add("supplier", new ActionMessage("receivingItemEntry.error.receivingItemMustHaveLine"));

			}

		} else if (!Common.validateRequired(request.getParameter("isPurchaseOrderEntered"))) {

			if (Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("supplier", new ActionMessage("receivingItemEntry.error.supplierRequired"));
			}

			if (type.equals("PO MATCHED")) {

				if (Common.validateRequired(poNumber)) {
					errors.add("poNumber", new ActionMessage("receivingItemEntry.error.poNumberRequired"));
				}

			}

		} else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

			if (!Common.validateDateFormat(conversionDate)) {

				errors.add("conversionDate", new ActionMessage("receivingItemEntry.error.conversionDateInvalid"));

			}

		}

		return (errors);
	}
}
