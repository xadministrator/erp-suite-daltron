package com.struts.ap.receivingitementry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class ApReceivingItemEntryList implements Serializable {

	private Integer receivingItemLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String remaining = null;
	private String received = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private ArrayList tagList = new ArrayList();
	private String unitCost = null;
	private String amount = null;
	private String unitCostLocal = null;
	private String amountLocal = null;
	private String addonCost = null;
	
	private String qcNumber = null;
	private String qcExpiryDate = null;
	private String conversionFactor = null;
	private Integer plPlCode = null;
	private String discount1 = null;
	private String discount2 = null;
	private String discount3 = null;
	private String discount4 = null;
	private String totalDiscount = null;
	private String fixedDiscountAmount = null;
	private String misc = null;
	private String partNumber = null;
	private boolean isTraceMisc = false;
	private boolean deleteCheckbox = false;

	private String isItemEntered = null;
	private String isUnitEntered = null;
	
	private String propertyCode = "";
	private String serialNumber = "";
	private String specs = "";
	private String custodian = "";
	private String expiryDate = "";
	

	private ApReceivingItemEntryForm parentBean;

	public ApReceivingItemEntryList(ApReceivingItemEntryForm parentBean,
			Integer receivingItemLineCode,
			String lineNumber,
			String location,
			String itemName,
			String itemDescription,
			String remaining,
			String received,
			String unit,
			String unitCost,
			String amount,
			String unitCostLocal,
			String amountLocal,
			String addonCost,
			
			String qcNumber,
			String qcExpiryDate,
			String conversionFactor,
			Integer plPlCode,
			String discount1,
			String discount2,
			String discount3,
			String discount4,
			String totalDiscount,
			String fixedDiscountAmount,
			String misc,
			String partNumber
			/*ArrayList tagList*/){

		this.parentBean = parentBean;
		this.receivingItemLineCode = receivingItemLineCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.remaining = remaining;
		this.received = received;
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;
		this.unitCostLocal = unitCostLocal;
		this.amountLocal = amountLocal;
		this.addonCost = addonCost;
		
		this.qcNumber = qcNumber;
		this.qcExpiryDate =qcExpiryDate;
		this.conversionFactor = conversionFactor;
		this.plPlCode = plPlCode;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.totalDiscount = totalDiscount;
		this.fixedDiscountAmount = fixedDiscountAmount;
		this.misc = misc;
		this.partNumber =partNumber;
		//this.tagList = tagList;
	}

	public Integer getReceivingItemLineCode(){

		return(receivingItemLineCode);

	}

	public String getLineNumber() {

		return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

		this.lineNumber = lineNumber;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

		this.itemDescription = itemDescription;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(ArrayList locationList) {

		this.locationList = locationList;

	}

	public String getRemaining() {

		return remaining;

	}

	public void setRemaining(String remaining) {

		this.remaining = remaining;

	}

	public String getReceived() {

		return received;

	}

	public void setReceived(String received) {

		this.received = received;

	}

	public String getUnit() {

		return unit;

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}

	public ArrayList getUnitList() {

		return unitList;

	}

	public void setUnitList(String unit) {

		unitList.add(unit);

	}

	public ApReceivingItemEntryTagList getTagListByIndex(int index){
	      return((ApReceivingItemEntryTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}

	public void clearUnitList() {

		unitList.clear();

	}

	public String getUnitCost() {

		return unitCost;

	}

	public void setUnitCost(String unitCost) {

		this.unitCost = unitCost;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}
	
	
	
	public String getUnitCostLocal() {

		return unitCostLocal;

	}

	public void setUnitCostLocal(String unitCostLocal) {

		this.unitCostLocal = unitCostLocal;

	}

	public String getAmountLocal() {

		return amountLocal;

	}

	public void setAmountLocal(String amountLocal) {

		this.amountLocal = amountLocal;

	}
	
	public String getAddonCost() {

		return addonCost;

	}

	public void setAddonCost(String addonCost) {

		this.addonCost = addonCost;

	}
	
	
	
	
	
	

	public String getQcNumber() {

		return qcNumber;

	}

	public void setQcNumber(String qcNumber) {

		this.qcNumber = qcNumber;

	}

	public String getQcExpiryDate() {

		return qcExpiryDate;

	}

	public void setQcExpiryDate(String qcExpiryDate) {

		this.qcExpiryDate = qcExpiryDate;

	}

	public String getConversionFactor() {

		return conversionFactor;

	}

	public void setConversionFactor(String conversionFactor) {

		this.conversionFactor = conversionFactor;

	}


	public boolean getIsTraceMisc() {

		return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

		this.isTraceMisc = isTraceMisc;

	}



	public boolean getDeleteCheckbox() {

		return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

		this.deleteCheckbox = deleteCheckbox;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isItemEntered = null;

	}

	public String getIsUnitEntered() {

		return isUnitEntered;

	}

	public void setIsUnitEntered(String isUnitEntered) {

		if (isUnitEntered != null && isUnitEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isUnitEntered = null;

	}

	public Integer getPlPlCode() {

		return plPlCode;

	}

	public String getDiscount1() {

		return discount1;

	}

	public void setDiscount1(String discount1) {

		this.discount1 = discount1;

	}

	public String getDiscount2() {

		return discount2;

	}

	public void setDiscount2(String discount2) {

		this.discount2 = discount2;

	}

	public String getDiscount3() {

		return discount3;

	}

	public void setDiscount3(String discount3) {

		this.discount3 = discount3;

	}

	public String getDiscount4() {

		return discount4;

	}

	public void setDiscount4(String discount4) {

		this.discount4 = discount4;

	}

	public String getTotalDiscount() {

		return totalDiscount;

	}

	public void setTotalDiscount(String totalDiscount) {

		this.totalDiscount = totalDiscount;

	}


	public String getFixedDiscountAmount() {

		return fixedDiscountAmount;

	}

	public void setFixedDiscountAmount(String fixedDiscountAmount) {

		this.fixedDiscountAmount = fixedDiscountAmount;

	}

	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;

	}

	public String getPartNumber() {

		return partNumber;

	}

	public void setPartNumber(String partNumber) {

		this.partNumber = partNumber;
	}

	public String getPropertyCode() {
		return propertyCode;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public String getSpecs() {
		return specs;
	}

	public String getCustodian() {
		return custodian;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setPropertyCode(String propertyCode) {
		this.propertyCode = propertyCode;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public void setSpecs(String specs) {
		this.specs = specs;
	}

	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	
	
}