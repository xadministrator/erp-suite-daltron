package com.struts.ap.recurringvoucherentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.txn.ApRecurringVoucherEntryController;
import com.ejb.txn.ApRecurringVoucherEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModRecurringVoucherDetails;
import com.util.ApModSupplierDetails;
import com.util.ApRecurringVoucherDetails;
import com.util.GlModFunctionalCurrencyDetails;

public final class ApRecurringVoucherEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApRecurringVoucherEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApRecurringVoucherEntryForm actionForm = (ApRecurringVoucherEntryForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_RECURRING_VOUCHER_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apRecurringVoucherEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ApRecurringVoucherEntryController EJB
*******************************************************/

         ApRecurringVoucherEntryControllerHome homeRV = null;
         ApRecurringVoucherEntryController ejbRV = null;

         try {
            
            homeRV = (ApRecurringVoucherEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApRecurringVoucherEntryControllerEJB", ApRecurringVoucherEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApRecurringVoucherEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbRV = homeRV.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ApRecurringVoucherEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call ApRecurringVoucherEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short journalLineNumber = 0;
         boolean useSupplierPulldown = true;
         boolean enableVoucherBatch = false; 
         
         try {
         	
            precisionUnit = ejbRV.getGlFcPrecisionUnit(user.getCmpCode());
            journalLineNumber = ejbRV.getAdPrfApJournalLineNumber(user.getCmpCode());            
            useSupplierPulldown = Common.convertByteToBoolean(ejbRV.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            enableVoucherBatch = Common.convertByteToBoolean(ejbRV.getAdPrfEnableApVoucherBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableVoucherBatch);
            
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApRecurringVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Ap RV Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ApRecurringVoucherDetails details = new ApRecurringVoucherDetails();
           
           details.setRvCode(actionForm.getRecurringVoucherCode());
           details.setRvName(actionForm.getName());  
           details.setRvDescription(actionForm.getDescription()); 
           details.setRvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setRvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setRvAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));         
           details.setRvAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));	
           details.setRvSchedule(actionForm.getReminderSchedule());
           details.setRvNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
                      
           ArrayList drList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getApRVListSize(); i++) {
           	
           	   ApRecurringVoucherEntryList apRVList = actionForm.getApRVByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(apRVList.getAccount()) &&
      	 	       Common.validateRequired(apRVList.getDebitAmount()) &&
      	 	       Common.validateRequired(apRVList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(apRVList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apRVList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apRVList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
           	   
           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apRVList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apRVList.getAccount());
           	   
           	   drList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           
           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.recurringVoucherNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apRecurringVoucherEntry"));
           	
           }
           	
           
           try {
           	
           	    ejbRV.saveApRvEntry(details, actionForm.getNotifiedUser1(), 
	           	    actionForm.getNotifiedUser2(), actionForm.getNotifiedUser3(),
	           	    actionForm.getNotifiedUser4(), actionForm.getNotifiedUser5(),
           	        actionForm.getPaymentTerm(), actionForm.getTaxCode(), 
           	        actionForm.getWithholdingTax(), actionForm.getCurrency(), 
           	        actionForm.getSupplier(), drList, actionForm.getBatchName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.conversionDateNotExist"));           	                      	

           } catch (GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.branchAccountNumberInvalid"));
           	 
           } catch (GlobalPaymentTermInvalidException ex) {
           	
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.paymentTermInvalid", ex.getMessage()));
           	          	
           } catch (GlobalRecordInvalidException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("recurringVoucherEntry.error.recordInvalid", ex.getMessage()));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApRecurringVoucherEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
   -- Ap RV Delete Action --
*******************************************************/

         } else if (request.getParameter("deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	 
	     try { 
	     
	        ejbRV.deleteApRvEntry(actionForm.getRecurringVoucherCode(), user.getCmpCode());
	        
         } catch (GlobalRecordAlreadyDeletedException ex) {
         	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("recurringVoucherEntry.error.recordAlreadyDeleted"));
                   
         } catch (EJBException ex) {
         	
                if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in ApRecurringVoucherEntryAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                   
                }                
      	 } 
            	
           
/*******************************************************
   -- Ap RV Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ap RV Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {
         	
         	int listSize = actionForm.getApRVListSize();
                                   
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	ApRecurringVoucherEntryList apRVList = new ApRecurringVoucherEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);
	        	    
	        	apRVList.setDrClassList(this.getDrClassList());
	        	
	        	actionForm.saveApRVList(apRVList);
	        	
	        }	        
	        
	        return(mapping.findForward("apRecurringVoucherEntry"));
	        
/*******************************************************
   -- Ap RV Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getApRVListSize(); i++) {
           	
           	   ApRecurringVoucherEntryList apRVList = actionForm.getApRVByIndex(i);
           	   
           	   if (apRVList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteApRVList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getApRVListSize(); i++) {
           	
           	   ApRecurringVoucherEntryList apRVList = actionForm.getApRVByIndex(i);
           	   
           	   apRVList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("apRecurringVoucherEntry"));

/*******************************************************
   -- Ap RV Supplier Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
         	
         	try {
             	
                 ApModSupplierDetails mdetails = ejbRV.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());
                 
                 actionForm.clearApRVList();
                 actionForm.setPaymentTerm(mdetails.getSplPytName());
                 actionForm.setTaxCode(mdetails.getSplScTcName());
                 actionForm.setWithholdingTax(mdetails.getSplScWtcName());
                 actionForm.setAmount(null);
                 actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
                 
             } catch (GlobalNoRecordFoundException ex) {
             	
             	actionForm.clearApRVList();
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApRecurringVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  	
                        
            return(mapping.findForward("apRecurringVoucherEntry"));

/*******************************************************
   -- Ap RV Amount Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isAmountEntered"))) {
         	
         	try {
             	
                 ArrayList list = ejbRV.getApDrBySplSupplierCodeAndTcNameAndWtcNameAndRvAmount(actionForm.getSupplier(),
                     actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                     Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit), user.getCmpCode());                            		
                     
                 actionForm.clearApRVList();
            		         		            		            		
 	             Iterator i = list.iterator();
		            
		         while (i.hasNext()) {
		            	
			           ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();
			           
				       String debitAmount = null;
				       String creditAmount = null;
				       
				       if (mDrDetails.getDrDebit() == 1) {
				       	
				          debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       } else {
				       	
				          creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       }
				       
				       ArrayList classList = new ArrayList();
				       classList.add(mDrDetails.getDrClass());
				       
				       ApRecurringVoucherEntryList apDrList = new ApRecurringVoucherEntryList(actionForm,
				          mDrDetails.getDrCode(),
				          Common.convertShortToString(mDrDetails.getDrLine()),
				          mDrDetails.getDrCoaAccountNumber(),
				          debitAmount, creditAmount, mDrDetails.getDrClass(),
				          mDrDetails.getDrCoaAccountDescription());
				          
				          
				      if (mDrDetails.getDrClass().equals(Constants.AP_DR_CLASS_PAYABLE)) {
				      	
				      	  actionForm.setAmountDue(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));
				      	
				      }
				      
				      actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));
				      actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit));
				          
				      apDrList.setDrClassList(classList);
				      
				      actionForm.saveApRVList(apDrList);
			     }	
			        			        			        
		         int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
		        
		         for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	ApRecurringVoucherEntryList apRVList = new ApRecurringVoucherEntryList(actionForm,
		        	    null, String.valueOf(x), null, null, null, null, null);
		        	
		        	apRVList.setDrClassList(this.getDrClassList());
		        	
		        	actionForm.saveApRVList(apRVList);
		        	
		         }				         
			                     
             } catch (GlobalNoRecordFoundException ex) {
           	    
           	    actionForm.clearApRVList();
           	    
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.amountNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));
             	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApRecurringVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  	
                        
            return(mapping.findForward("apRecurringVoucherEntry"));
         	
/*******************************************************
   -- AP RV Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {
         	
         	try {
         		
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
         				ejbRV.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
         						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));
         		
         	} catch (GlobalConversionDateNotExistException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("recurringVoucherEntry.error.conversionDateNotExist"));
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ApRecurringVoucherEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			
         		}
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	} 
         	
         	if (!errors.isEmpty()) {
         		
         		saveErrors(request, new ActionMessages(errors));
         		
         	}
         	
         	return(mapping.findForward("apRecurringVoucherEntry"));

/*******************************************************
   -- Ap RV Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apRecurringVoucherEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearNotifiedUser1List();           	
            	actionForm.clearNotifiedUser2List();           	
            	actionForm.clearNotifiedUser3List();           	
            	actionForm.clearNotifiedUser4List();           	
            	actionForm.clearNotifiedUser5List();           	
            	
            	list = ejbRV.getAdUsrAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setNotifiedUser1List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser2List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser3List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser4List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser5List(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			String userName = (String)i.next();
            			actionForm.setNotifiedUser1List(userName);
            			actionForm.setNotifiedUser2List(userName);
            			actionForm.setNotifiedUser3List(userName);
            			actionForm.setNotifiedUser4List(userName);
            			actionForm.setNotifiedUser5List(userName);
            		                			
            		}
            		
            	}
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbRV.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();
            			
            			actionForm.setCurrencyList(mFcDetails.getFcName());
            			
            			if (mFcDetails.getFcSob() == 1) {
            				
            				actionForm.setCurrency(mFcDetails.getFcName());
            				
            			}            			
            		                			
            		}
            		
            	}
            	
            	actionForm.clearPaymentTermList();           	
            	
            	list = ejbRV.getAdPytAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setPaymentTermList((String)i.next());
            			
            		}            		
            		
            		actionForm.setPaymentTerm("IMMEDIATE");
            		            		
            	}    
            	
            	actionForm.clearTaxCodeList();           	
            	
            	list = ejbRV.getApTcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setTaxCodeList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            	
            	actionForm.clearWithholdingTaxList();           	
            	
            	list = ejbRV.getApWtcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setWithholdingTaxList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setWithholdingTaxList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbRV.getApOpenVbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	if(actionForm.getUseSupplierPulldown()) {
	            	actionForm.clearSupplierList();           	
	            	
	            	list = ejbRV.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierList((String)i.next());
	            			
	            		}
	            		            		
	            	}
	            	
            	}
            	
            	actionForm.clearApRVList();               	  	
            	
            	if (request.getParameter("forward") != null) {
            		
            	    actionForm.setRecurringVoucherCode(new Integer(request.getParameter("recurringVoucherCode")));

            		ApModRecurringVoucherDetails mdetails = ejbRV.getApRvByRvCode(actionForm.getRecurringVoucherCode(), user.getCmpCode());
            		
            		actionForm.setName(mdetails.getRvName());
            		actionForm.setDescription(mdetails.getRvDescription());
					actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getRvConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getRvConversionRate(), (short)40));
            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getRvAmount(), precisionUnit));
            		actionForm.setAmountDue(Common.convertDoubleToStringMoney(mdetails.getRvAmountDue(), precisionUnit));
                    actionForm.setReminderSchedule(mdetails.getRvSchedule());    		
            		actionForm.setNextRunDate(Common.convertSQLDateToString(mdetails.getRvNextRunDate()));
            		actionForm.setLastRunDate(Common.convertSQLDateToString(mdetails.getRvLastRunDate()));
            		actionForm.setNotifiedUser1(mdetails.getRvAdNotifiedUser1());
            		actionForm.setNotifiedUser2(mdetails.getRvAdNotifiedUser2());
            		actionForm.setNotifiedUser3(mdetails.getRvAdNotifiedUser3());
            		actionForm.setNotifiedUser4(mdetails.getRvAdNotifiedUser4());
            		actionForm.setNotifiedUser5(mdetails.getRvAdNotifiedUser5());
            		actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getRvTotalDebit(), precisionUnit));
            		actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getRvTotalCredit(), precisionUnit));
            		actionForm.setBatchName(mdetails.getRvVbName());
            		
            		if (!actionForm.getTaxCodeList().contains(mdetails.getRvTcName())) {
            			
            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearTaxCodeList();
            				
            			}
            			actionForm.setTaxCodeList(mdetails.getRvTcName());
            			
            		}
                    actionForm.setTaxCode(mdetails.getRvTcName());
                    
                    if (!actionForm.getPaymentTermList().contains(mdetails.getRvPytName())) {
                    	
                    	if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearPaymentTermList();
            				
            			}
                    	actionForm.setPaymentTermList(mdetails.getRvPytName());
                    	
                    }
                    actionForm.setPaymentTerm(mdetails.getRvPytName());
                    
                    if (!actionForm.getSupplierList().contains(mdetails.getRvSplSupplierCode())) {
                    	
                    	if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearSupplierList();
            				
            			}
                    	actionForm.setSupplierList(mdetails.getRvSplSupplierCode());
                    	
                    }
            		actionForm.setSupplier(mdetails.getRvSplSupplierCode());
            		
            		if (!actionForm.getWithholdingTaxList().contains(mdetails.getRvWtcName())) {
            			
            			if (actionForm.getWithholdingTaxList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearWithholdingTaxList();
            				
            			}
            			actionForm.setWithholdingTaxList(mdetails.getRvWtcName());
            			
            		}
            		actionForm.setWithholdingTax(mdetails.getRvWtcName());
            		
            		if (!actionForm.getCurrencyList().contains(mdetails.getRvFcName())) {
            			
            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearCurrencyList();
            				
            			}
            			actionForm.setCurrencyList(mdetails.getRvFcName());
            			
            		}
            		actionForm.setCurrency(mdetails.getRvFcName());
            		
            		list = mdetails.getRvDrList();              		
            		         		            		            		
		            i = list.iterator();
		            
		            while (i.hasNext()) {
		            	
			           ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();
			           
				       String debitAmount = null;
				       String creditAmount = null;
				       
				       if (mDrDetails.getDrDebit() == 1) {
				       	
				          debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       } else {
				       	
				          creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);				          
				          
				       }
				       
				       ArrayList classList = new ArrayList();
				       classList.add(mDrDetails.getDrClass());
				       
				       ApRecurringVoucherEntryList apDrList = new ApRecurringVoucherEntryList(actionForm,
				          mDrDetails.getDrCode(),
				          Common.convertShortToString(mDrDetails.getDrLine()),
				          mDrDetails.getDrCoaAccountNumber(),
				          debitAmount, creditAmount, mDrDetails.getDrClass(),
				          mDrDetails.getDrCoaAccountDescription());
				          
				      apDrList.setDrClassList(classList);
				      
				      actionForm.saveApRVList(apDrList);
				         
				      
			        }	
			        			        			        
			        int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
			        
			        for (int x = list.size() + 1; x <= remainingList; x++) {
			        	
			        	ApRecurringVoucherEntryList apRVList = new ApRecurringVoucherEntryList(actionForm,
			        	    null, String.valueOf(x), null, null, null, null, null);
			        	
			        	apRVList.setDrClassList(this.getDrClassList());
			        	
			        	actionForm.saveApRVList(apRVList);
			        	
			         }	
			         
			         this.setFormProperties(actionForm);
			         return (mapping.findForward("apRecurringVoucherEntry"));         
            		
            	}            	                     	           			
            		           			   	
            } catch(GlobalNoRecordFoundException ex) {
            	            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringVoucherEntry.error.recurringVoucherAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApRecurringVoucherEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setRecurringVoucherCode(null);          
            actionForm.setNextRunDate(Common.convertSQLDateToString(new java.util.Date()));
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("apRecurringVoucherEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ApRecurringVoucherEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ApRecurringVoucherEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getRecurringVoucherCode() != null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(true);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
		
			} else {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);

			}

		} else {
	
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);

		}
		
	}
	
	private ArrayList getDrClassList() {
		
		ArrayList classList = new ArrayList();
		
     	classList.add(Constants.AP_DR_CLASS_EXPENSE);
     	classList.add(Constants.AP_DR_CLASS_TAX);
     	classList.add(Constants.AP_DR_CLASS_WITHHOLDING_TAX);
     	classList.add(Constants.AP_DR_CLASS_OTHER);
     	
     	return classList;
		
	}
	
}