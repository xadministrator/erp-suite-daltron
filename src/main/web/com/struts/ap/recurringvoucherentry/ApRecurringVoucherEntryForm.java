package com.struts.ap.recurringvoucherentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApRecurringVoucherEntryForm extends ActionForm implements Serializable {

   private Integer recurringVoucherCode = null;
   private String name = null;
   private String description = null;   
   private String supplier = null;
   private ArrayList supplierList = new ArrayList();  
   private String paymentTerm = null;
   private ArrayList paymentTermList = new ArrayList();  
   private String amount = null;   
   private String amountDue = null;
   private String totalDebit = null;
   private String totalCredit = null;
   private String taxCode = null;
   private ArrayList taxCodeList = new ArrayList();
   private String withholdingTax = null;
   private ArrayList withholdingTaxList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String conversionDate = null;
   private String conversionRate = null;
   private String reminderSchedule = null;
   private ArrayList reminderScheduleList = new ArrayList();
   private String nextRunDate = null;
   private String lastRunDate = null;
   private String notifiedUser1 = null;
   private ArrayList notifiedUser1List = new ArrayList();
   private String notifiedUser2 = null;
   private ArrayList notifiedUser2List = new ArrayList();
   private String notifiedUser3 = null;
   private ArrayList notifiedUser3List = new ArrayList();
   private String notifiedUser4 = null;
   private ArrayList notifiedUser4List = new ArrayList();
   private String notifiedUser5 = null;
   private ArrayList notifiedUser5List = new ArrayList();  

   private String functionalCurrency = null;

   private ArrayList apRVList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showDeleteButton = false;
   private boolean showSaveButton = false;
   private boolean useSupplierPulldown = true;
   
   private String isSupplierEntered  = null;
   private String isAmountEntered = null;
   private String isConversionDateEntered = null;
   
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private boolean showBatchName = false;
   
   public int getRowSelected(){
      return rowSelected;
   }

   public ApRecurringVoucherEntryList getApRVByIndex(int index){
      return((ApRecurringVoucherEntryList)apRVList.get(index));
   }

   public Object[] getApRVList(){
      return(apRVList.toArray());
   }

   public int getApRVListSize(){
      return(apRVList.size());
   }

   public void saveApRVList(Object newApRVList){
      apRVList.add(newApRVList);
   }

   public void clearApRVList(){
      apRVList.clear();
   }

   public void setRowSelected(Object selectedApRVList, boolean isEdit){
      this.rowSelected = apRVList.indexOf(selectedApRVList);
   }

   public void updateApRVRow(int rowSelected, Object newApRVList){
      apRVList.set(rowSelected, newApRVList);
   }

   public void deleteApRVList(int rowSelected){
      apRVList.remove(rowSelected);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getRecurringVoucherCode() {
   	
   	  return recurringVoucherCode;
   	
   }
   
   public void setRecurringVoucherCode(Integer recurringVoucherCode) {
   	
   	  this.recurringVoucherCode = recurringVoucherCode;
   	
   }
   
   public String getName() {
   	
   	  return name;
   	  
   }
   
   public void setName(String name) {
   	
   	  this.name = name;
   	  
   }

   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public String getSupplier() {
   	
   	  return supplier;
   	
   }
   
   public void setSupplier(String supplier) {
   	
   	  this.supplier = supplier;
   	
   }
   
   public ArrayList getSupplierList() {
   	
   	  return supplierList;
   	
   }
   
   public void setSupplierList(String supplier) {
   	
   	  supplierList.add(supplier);
   	
   }
   
   public void clearSupplierList() {
   	
   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);
   	
   }

   public String getPaymentTerm() {
   	
   	  return paymentTerm;
   	
   }
   
   public void setPaymentTerm(String paymentTerm) {
   	
   	  this.paymentTerm = paymentTerm;
   	
   }
   
   public ArrayList getPaymentTermList() {
   	
   	  return paymentTermList;
   	
   }
   
   public void setPaymentTermList(String paymentTerm) {
   	
   	  paymentTermList.add(paymentTerm);
   	
   }
   
   public void clearPaymentTermList() {
   	
   	  paymentTermList.clear();
   	
   }
   
   public String getAmount() {
   	
   	  return amount;
   	
   }
   
   public void setAmount(String amount) {
   	
   	  this.amount = amount;
   	
   }  
   
   public String getAmountDue() {
   	
   	  return amountDue;
   	
   }
   
   public void setAmountDue(String amountDue) {
   	
   	  this.amountDue = amountDue;
   	
   } 
   
   public String getTotalDebit() {
   	
   	  return totalDebit;
   	  
   }
   
   public void setTotalDebit(String totalDebit) {
   	
   	  this.totalDebit = totalDebit;
   	  
   }
   
   public String getTotalCredit() {
   	
   	  return totalCredit;
   	  
   }
   
   public void setTotalCredit(String totalCredit) {
   	
   	  this.totalCredit = totalCredit;
   	  
   }
   
   public String getTaxCode() {
   	
   	   return taxCode;
   	
   }
   
   public void setTaxCode(String taxCode) {
   	
   	   this.taxCode = taxCode;
   	
   }
   
   public ArrayList getTaxCodeList() {
   	
   	   return taxCodeList;
   	
   }
   
   public void setTaxCodeList(String taxCode) {
   	
   	   taxCodeList.add(taxCode);
   	
   }
   
   public void clearTaxCodeList() {
   	
   	   taxCodeList.clear();
   	   taxCodeList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   
   public String getWithholdingTax() {
   	
   	   return withholdingTax;
   	
   }
   
   public void setWithholdingTax(String withholdingTax) {
   	
   	   this.withholdingTax = withholdingTax;
   	
   }
   
   public ArrayList getWithholdingTaxList() {
   	
   	   return withholdingTaxList;
   	
   }
   
   public void setWithholdingTaxList(String withholdingTax) {
   	
   	   withholdingTaxList.add(withholdingTax);
   	
   }
   
   public void clearWithholdingTaxList() {
   	
   	   withholdingTaxList.clear();
   	   withholdingTaxList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   
   public String getCurrency() {
   	
   	   return currency;
   	
   }
   
   public void setCurrency(String currency) {
   	
   	   this.currency = currency;
   	
   }
   
   public ArrayList getCurrencyList() {
   	
   	   return currencyList;
   	
   }
   
   public void setCurrencyList(String currency) {
   	
   	   currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	   currencyList.clear();
   	
   }
   
   public String getConversionDate() {
   	
   	   return conversionDate;
   	
   }
   
   public void setConversionDate(String conversionDate) {
   	
   	   this.conversionDate = conversionDate;
   	
   }
   
   public String getConversionRate() {
   	 
   	   return conversionRate;
   	
   }

   public void setConversionRate(String conversionRate) {
   	
   	   this.conversionRate = conversionRate;
   	
   }
   
   public String getReminderSchedule() {
   	
   	   return reminderSchedule;
   	
   }
   
   public void setReminderSchedule(String reminderSchedule) {
   	
   	   this.reminderSchedule = reminderSchedule;
   	
   }
   
   public ArrayList getReminderScheduleList() {
   	
   	   return reminderScheduleList;
   	
   }
   
   public String getNextRunDate() {
   	
   	   return nextRunDate;
   	
   }
   
   public void setNextRunDate(String nextRunDate) {
   	
   	   this.nextRunDate = nextRunDate;
   	
   }
   
   public String getLastRunDate() {
   	
   	   return lastRunDate;
   	
   }
   
   public void setLastRunDate(String lastRunDate) {
   	
   	   this.lastRunDate = lastRunDate;
   	
   }
   
   public String getNotifiedUser1() {
   	
   	  return notifiedUser1;
   	
   }
   
   public void setNotifiedUser1(String notifiedUser1) {
   	
   	  this.notifiedUser1 = notifiedUser1;
   	
   }
   
   public ArrayList getNotifiedUser1List() {
   	
   	   return notifiedUser1List;
   	
   }
   
   public void setNotifiedUser1List(String notifiedUser1) {
   	
   	   notifiedUser1List.add(notifiedUser1);
   	
   }
   
   public void clearNotifiedUser1List() {
   	
   	   notifiedUser1List.clear();
   	   notifiedUser1List.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getNotifiedUser2() {
   	
   	  return notifiedUser2;
   	
   }
   
   public void setNotifiedUser2(String notifiedUser2) {
   	
   	  this.notifiedUser2 = notifiedUser2;
   	
   }
   
   public ArrayList getNotifiedUser2List() {
   	
   	   return notifiedUser2List;
   	
   }
   
   public void setNotifiedUser2List(String notifiedUser2) {
   	
   	   notifiedUser2List.add(notifiedUser2);
   	
   }
   
   public void clearNotifiedUser2List() {
   	
   	   notifiedUser2List.clear();
   	   notifiedUser2List.add(Constants.GLOBAL_BLANK);
   	
   }   
   
   public String getNotifiedUser3() {
   	
   	  return notifiedUser3;
   	
   }
   
   public void setNotifiedUser3(String notifiedUser3) {
   	
   	  this.notifiedUser3 = notifiedUser3;
   	
   }
   
   public ArrayList getNotifiedUser3List() {
   	
   	   return notifiedUser3List;
   	
   }
   
   public void setNotifiedUser3List(String notifiedUser3) {
   	
   	   notifiedUser3List.add(notifiedUser3);
   	
   }
   
   public void clearNotifiedUser3List() {
   	
   	   notifiedUser3List.clear();
   	   notifiedUser3List.add(Constants.GLOBAL_BLANK);
   	
   } 

   public String getNotifiedUser4() {
   	
   	  return notifiedUser4;
   	
   }
   
   public void setNotifiedUser4(String notifiedUser4) {
   	
   	  this.notifiedUser4 = notifiedUser4;
   	
   }
   
   public ArrayList getNotifiedUser4List() {
   	
   	   return notifiedUser4List;
   	
   }
   
   public void setNotifiedUser4List(String notifiedUser4) {
   	
   	   notifiedUser4List.add(notifiedUser4);
   	
   }
   
   public void clearNotifiedUser4List() {
   	
   	   notifiedUser4List.clear();
   	   notifiedUser4List.add(Constants.GLOBAL_BLANK);
   	
   }  
   
   public String getNotifiedUser5() {
   	
   	  return notifiedUser5;
   	
   }
   
   public void setNotifiedUser5(String notifiedUser5) {
   	
   	  this.notifiedUser5 = notifiedUser5;
   	
   }
   
   public ArrayList getNotifiedUser5List() {
   	
   	   return notifiedUser5List;
   	
   }
   
   public void setNotifiedUser5List(String notifiedUser5) {
   	
   	   notifiedUser5List.add(notifiedUser5);
   	
   }
   
   public void clearNotifiedUser5List() {
   	
   	   notifiedUser5List.clear();
   	   notifiedUser5List.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }   
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
   
   public boolean getShowDeleteButton() {
   	
   	   return showDeleteButton;
   	
   }
   
   public void setShowDeleteButton(boolean showDeleteButton) {
   	
   	   this.showDeleteButton = showDeleteButton;
   	
   }
   
   public String getIsSupplierEntered() {
   	
   	   return isSupplierEntered;
   	
   }
   
   public String getIsAmountEntered() {
   	
   	  return isAmountEntered;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }

   public String getIsConversionDateEntered(){
   	
   	return isConversionDateEntered;
   	
   }
   
   public String getBatchName() {
   	
   	  return batchName;
   	
   }
   
   public void setBatchName(String batchName) {
   	
   	  this.batchName = batchName;
   	
   }
   
   public ArrayList getBatchNameList() {
   	
   	  return batchNameList;
   	
   }
   
   public void setBatchNameList(String batchName) {
   	
   	  batchNameList.add(batchName);
   	
   }
   
   public void clearBatchNameList() {   	 
   	
   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       name = null;
       description = null;
       supplier = Constants.GLOBAL_BLANK;
       paymentTerm = Constants.GLOBAL_BLANK;
	   amount = null;   
	   amountDue = null;   
	   totalDebit = null;
	   totalCredit = null;
       taxCode = Constants.GLOBAL_BLANK;
       withholdingTax = Constants.GLOBAL_BLANK;
       currency = Constants.GLOBAL_BLANK;
	   conversionDate = null;
	   conversionRate = "1.00000";
       reminderScheduleList.clear();
       reminderScheduleList.add(Constants.AP_RV_SCHEDULE_DAILY);
       reminderScheduleList.add(Constants.AP_RV_SCHEDULE_WEEKLY);
       reminderScheduleList.add(Constants.AP_RV_SCHEDULE_SEMI_MONTHLY);
       reminderScheduleList.add(Constants.AP_RV_SCHEDULE_MONTHLY);
       reminderScheduleList.add(Constants.AP_RV_SCHEDULE_QUARTERLY);
       reminderScheduleList.add(Constants.AP_RV_SCHEDULE_SEMI_ANNUALLY);
       reminderScheduleList.add(Constants.AP_RV_SCHEDULE_ANNUALLY);
       reminderSchedule = Constants.AP_RV_SCHEDULE_MONTHLY;
       notifiedUser1 = Constants.GLOBAL_BLANK;
       notifiedUser2 = Constants.GLOBAL_BLANK;
       notifiedUser3 = Constants.GLOBAL_BLANK;
       notifiedUser4 = Constants.GLOBAL_BLANK;
       notifiedUser5 = Constants.GLOBAL_BLANK;	   
       nextRunDate = null;
       lastRunDate = null;
	   isSupplierEntered = null;
	   isAmountEntered = null;
	   isConversionDateEntered = null;	  
	   batchName = null;
	         
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null){

      	 if(Common.validateRequired(name)){
            errors.add("name",
               new ActionMessage("recurringVoucherEntry.error.nameRequired"));
         }
         
      	 if(Common.validateRequired(reminderSchedule)){
            errors.add("reminderSchedule",
               new ActionMessage("recurringVoucherEntry.error.reminderScheduleRequired"));
         }         
      	 
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("supplier",
               new ActionMessage("recurringVoucherEntry.error.supplierRequired"));
         }

         if(Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("paymentTerm",
               new ActionMessage("recurringVoucherEntry.error.paymentTermRequired"));
         }
         
         if(Common.validateRequired(amount)){
            errors.add("amount",
               new ActionMessage("recurringVoucherEntry.error.amountRequired"));
         }
         
		 if(!Common.validateMoneyFormat(amount)){
	            errors.add("amount", 
		       new ActionMessage("recurringVoucherEntry.error.amountInvalid"));
		 }
		 
		 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("taxCode",
               new ActionMessage("recurringVoucherEntry.error.taxCodeRequired"));
         }
         
         if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("withholdingTax",
               new ActionMessage("recurringVoucherEntry.error.withholdingTaxRequired"));
         }
         
         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("recurringVoucherEntry.error.currencyRequired"));
         }
         
	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("recurringVoucherEntry.error.conversionDateInvalid"));
         }
         
	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("recurringVoucherEntry.error.conversionRateInvalid"));
         }
         
         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("recurringVoucherEntry.error.conversionDateMustBeNull"));
	 	 }
	 	 
		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){ 
		    errors.add("conversionRate",
		       new ActionMessage("recurringVoucherEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("recurringVoucherEntry.error.conversionRateOrDateMustBeEntered"));
		 }         


         if (Common.validateRequired(notifiedUser1)) {
         	errors.add("notifiedUser1",
               new ActionMessage("recurringVoucherEntry.error.notifiedUser1Required"));
         	         	
         }
         
         if (Common.validateRequired(nextRunDate)) {
         	errors.add("nextRunDate",
               new ActionMessage("recurringVoucherEntry.error.nextRunDateRequired"));
         	         	
         }
         
         if (!Common.validateDateFormat(nextRunDate)) {
         	errors.add("nextRunDate",
               new ActionMessage("recurringVoucherEntry.error.nextRunDateInvalid"));
         } 
         
         if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
          	    showBatchName){
                errors.add("batchName",
                   new ActionMessage("recurringVoucherEntry.error.batchNameRequired"));
         }
	 	         
         
         int numberOfLines = 0;
      	 
      	 Iterator i = apRVList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 ApRecurringVoucherEntryList drList = (ApRecurringVoucherEntryList)i.next();      	 	 
      	 	       	 	 
      	 	 if (Common.validateRequired(drList.getAccount()) &&
      	 	     Common.validateRequired(drList.getDebitAmount()) &&
      	 	     Common.validateRequired(drList.getCreditAmount())) continue;
      	 	     
      	 	 numberOfLines++;
      	 
	         if(Common.validateRequired(drList.getAccount())){
	            errors.add("account",
	               new ActionMessage("recurringVoucherEntry.error.accountRequired", drList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(drList.getDebitAmount())){
	            errors.add("debitAmount",
	               new ActionMessage("recurringVoucherEntry.error.debitAmountInvalid", drList.getLineNumber()));
	         }
	         if(!Common.validateMoneyFormat(drList.getCreditAmount())){
	            errors.add("creditAmount",
	               new ActionMessage("recurringVoucherEntry.error.creditAmountInvalid", drList.getLineNumber()));
	         }
			 if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("recurringVoucherEntry.error.debitCreditAmountRequired", drList.getLineNumber()));
			 }
			 if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("recurringVoucherEntry.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
		     }	    		     
		     
		          
	    }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("supplier",
               new ActionMessage("recurringVoucherEntry.error.recurringVoucherMustHaveLine"));
         	
         }	  	    
	    
	    	    	 
      } else if (!Common.validateRequired(request.getParameter("isSupplierEntered"))) {
      	
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("supplier",
               new ActionMessage("recurringVoucherEntry.error.supplierRequired"));
         }
      	      
      } else if (!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {
      	
      	 if(Common.validateRequired(supplier) || supplier.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("supplier",
               new ActionMessage("recurringVoucherEntry.error.supplierRequired"));
         }
      	
      	 if(Common.validateRequired(amount)){
            errors.add("amount",
               new ActionMessage("recurringVoucherEntry.error.amountRequired"));
         }
         
		 if(!Common.validateMoneyFormat(amount)){
	            errors.add("amount", 
		       new ActionMessage("recurringVoucherEntry.error.amountInvalid"));
		 }
		 
		 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("taxCode",
               new ActionMessage("recurringVoucherEntry.error.taxCodeRequired"));
         }
         
         if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("withholdingTax",
               new ActionMessage("recurringVoucherEntry.error.withholdingTaxRequired"));
         }
      	
      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){
      	
      	if(!Common.validateDateFormat(conversionDate)){
      		
      		errors.add("conversionDate", new ActionMessage("recurringVoucherEntry.error.conversionDateInvalid"));
      		
      	}
      	
      }
      
      return(errors);	
   }
}
