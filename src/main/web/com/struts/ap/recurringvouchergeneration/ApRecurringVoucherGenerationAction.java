package com.struts.ap.recurringvouchergeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAmountInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.ApRecurringVoucherGenerationController;
import com.ejb.txn.ApRecurringVoucherGenerationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModRecurringVoucherDetails;

public final class ApRecurringVoucherGenerationAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApRecurringVoucherGenerationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApRecurringVoucherGenerationForm actionForm = (ApRecurringVoucherGenerationForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_RECURRING_VOUCHER_GENERATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apRecurringVoucherGeneration");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApRecurringVoucherGenerationController EJB
*******************************************************/

         ApRecurringVoucherGenerationControllerHome homeRVG = null;
         ApRecurringVoucherGenerationController ejbRVG = null;

         try {
          
            homeRVG = (ApRecurringVoucherGenerationControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApRecurringVoucherGenerationControllerEJB", ApRecurringVoucherGenerationControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApRecurringVoucherGenerationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbRVG = homeRVG.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApRecurringVoucherGenerationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call ApVoucherEntryController EJB     
*******************************************************/

         short precisionUnit = (short)4;
         boolean useSupplierPulldown = true;
         
         try { 
         	
            //precisionUnit = ejbRVG.getGlFcPrecisionUnit(user.getCmpCode());
            useSupplierPulldown = Common.convertByteToBoolean(ejbRVG.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
		    actionForm.setUseSupplierPulldown(useSupplierPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApRecurringVoucherGenerationAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }
         
/*******************************************************
   -- Ap RVG Previous Action --
*******************************************************/ 

         if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ap RVG Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 
         
/*******************************************************
   -- Ap RVG Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null ||
            request.getParameter("forward") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("name", actionForm.getName());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getNextRunDateFrom())) {
	        		
	        		criteria.put("nextRunDateFrom", Common.convertStringToSQLDate(actionForm.getNextRunDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getNextRunDateTo())) {
	        		
	        		criteria.put("nextRunDateTo", Common.convertStringToSQLDate(actionForm.getNextRunDateTo()));
	        		
	        	}	        		        	
	        		        	
	        	if (!Common.validateRequired(actionForm.getSupplier())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplier());
	        		
	        	}	        	
	        	      	
	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	} else if (request.getParameter("forward") != null) {
	     		
	     		HashMap criteria = new HashMap();
	     		
	     		criteria.put("nextRunDateTo", Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	     		
	     		// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	     		
	     	}
            
            try {
            	
            	actionForm.clearApRVGList();
            	
            	ArrayList list = ejbRVG.getApRvByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
            	    Common.convertStringMoneyToDouble(actionForm.getInterestRate(), precisionUnit),
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	String nullString = null;
            	
            	while (i.hasNext()) {
            		
            		ApModRecurringVoucherDetails mdetails = (ApModRecurringVoucherDetails)i.next();
            		
            		
            		ApRecurringVoucherGenerationList rvgList = new ApRecurringVoucherGenerationList(
            			actionForm,
            			mdetails.getRvCode(),
            			mdetails.getRvName(),
            			Common.convertSQLDateToString(mdetails.getRvNewNextRunDate()),
            			nullString,
            			Common.convertSQLDateToString(mdetails.getRvNextRunDate()),
            			mdetails.getRvSplSupplierCode(),
            			mdetails.getRvSchedule(),
            			Common.convertSQLDateToString(mdetails.getRvLastRunDate()),
            			Common.convertDoubleToStringMoney(mdetails.getRvInterestRate(), precisionUnit));
            			
            	   actionForm.saveApRVGList(rvgList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("recurringVoucherGeneration.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApRecurringVoucherGenerationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apRecurringVoucherGeneration");

            }
                        
            actionForm.reset(mapping, request);            	                               
            
            return(mapping.findForward("apRecurringVoucherGeneration")); 

/*******************************************************
   -- Ap RVG Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap RVG Generate Action --
*******************************************************/

         } else if (request.getParameter("generateButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	

  	         // get posted journals
  	        
		    for(int i=0; i<actionForm.getApRVGListSize(); i++) {
		    
		       ApRecurringVoucherGenerationList actionList = actionForm.getApRVGByIndex(i);
		    	
               if (actionList.getGenerate()) {
               	
               	     try {
               	     	               	     	
               	     	ejbRVG.executeApRvGeneration(actionList.getRecurringVoucherCode(), 
               	     	   Common.convertStringToSQLDate(actionList.getNextRunDate()), 
               	     	   actionList.getDocumentNumber(),
               	     	   Common.convertStringToSQLDate(actionList.getVoucherDate()), 
               	     	   Common.convertStringMoneyToDouble(actionList.getInterestRate(), (short)4),
               	     	   user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());               	     	
               	     	
               	     	actionForm.deleteApRVGList(i);
               	        i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("recurringVoucherGeneration.error.recordAlreadyDeleted", actionList.getName()));
  
		             } catch (GlobalDocumentNumberNotUniqueException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("recurringVoucherGeneration.error.documentNumberNotUnique", actionList.getName()));

		             } catch (GlobalAmountInvalidException ex) {
     		               
			               errors.add(ActionMessages.GLOBAL_MESSAGE,
			                  new ActionMessage("recurringVoucherGeneration.error.noInvestment", new String(actionList.getName() + " - " + actionList.getSupplier())));

		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ApRecurringVoucherGenerationAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 		                           
	            
	           }
	           
	       }		      
		
/*******************************************************
   -- Ap RVG Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apRecurringVoucherGeneration");

            }
            
            actionForm.clearApRVGList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseSupplierPulldown()) {
            		
	            	actionForm.clearSupplierList();           	
	            	
	            	list = ejbRVG.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
            	}
            	           	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApRecurringVoucherGenerationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }             
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("generateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            actionForm.reset(mapping, request);
            actionForm.setLineCount(0);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            	        
            return(mapping.findForward("apRecurringVoucherGeneration"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApRecurringVoucherGenerationAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();

          return mapping.findForward("cmnErrorPage");

       }

    }
}