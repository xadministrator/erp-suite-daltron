package com.struts.ap.recurringvouchergeneration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApRecurringVoucherGenerationForm extends ActionForm implements Serializable {

   private String name = null;
   private String nextRunDateFrom = null;
   private String nextRunDateTo = null;
   private String supplier = null;
   private ArrayList supplierList = new ArrayList(); 
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
   private String interestRate = null;
    
   private ArrayList apRVGList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean useSupplierPulldown = true;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {
   	
      return rowSelected;
   
   }
   
   public int getLineCount() {
   
   	  return lineCount;   	 
   
   }
   
   public void setLineCount(int lineCount) {  
   	
   	  this.lineCount = lineCount;
   
   }

   public ApRecurringVoucherGenerationList getApRVGByIndex(int index) {
   
      return((ApRecurringVoucherGenerationList)apRVGList.get(index));
   
   }

   public Object[] getApRVGList() {
   	
      return(apRVGList.toArray());
   
   }

   public int getApRVGListSize() {
   
      return(apRVGList.size());
   
   }

   public void saveApRVGList(Object newApRVGList) {
   	
      apRVGList.add(newApRVGList);
   
   }

   public void clearApRVGList() {
   
      apRVGList.clear();
   
   }

   public void setRowSelected(Object selectedApRVGList, boolean isEdit) {
   
      this.rowSelected = apRVGList.indexOf(selectedApRVGList);
  
   }

   public void updateApRVGRow(int rowSelected, Object newApRVGList) {
   	
      apRVGList.set(rowSelected, newApRVGList);
   
   }

   public void deleteApRVGList(int rowSelected) {
   
      apRVGList.remove(rowSelected);
   
   }
   
   public String getTxnStatus() {
   	
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   
   }

   public void setTxnStatus(String txnStatus) {
   
      this.txnStatus = txnStatus;
   
   }

   public String getUserPermission() {
   	
      return(userPermission);
   
   }

   public void setUserPermission(String userPermission) {
   
      this.userPermission = userPermission;
   
   }
   
   public String getName() {
   	
      return name;
   
   }

   public void setName(String name) {
   	
      this.name = name;
   
   }
   
   public String getNextRunDateFrom() {
   
   	  return nextRunDateFrom;	  
   
   }
   
   public void setNextRunDateFrom(String nextRunDateFrom) {
   	
   	  this.nextRunDateFrom = nextRunDateFrom;
   
   }
   
   public String getNextRunDateTo() {
   
   	  return nextRunDateTo;	  
   
   }
   
   public void setNextRunDateTo(String nextRunDateTo) {
   	
   	  this.nextRunDateTo = nextRunDateTo;
   
   }   
      
   public String getSupplier() {
   
   	  return supplier;
   
   }
   
   public void setSupplier(String supplier) {
   	
   	  this.supplier = supplier;
   
   }
   
   public ArrayList getSupplierList() {
   
   	  return supplierList;
   
   }
   
   public void setSupplierList(String supplier) {
   
   	  supplierList.add(supplier);
   
   }
   
   public void clearSupplierList() {
   	 
   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);
   
   }   
      
   public String getOrderBy() {
   	
   	  return orderBy;
   
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   
   }
   
   public ArrayList getOrderByList() {
   
   	  return orderByList;
   
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   	
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }
   
   public String getInterestRate() {
	   
	   return interestRate;
	   
   }
   
   public void setInterestRate(String interestRate) {
	   
	   this.interestRate = interestRate;
   }
      
   public void reset(ActionMapping mapping, HttpServletRequest request){
      name = null;
      nextRunDateFrom = null;
      nextRunDateTo = null;
      supplier = Constants.GLOBAL_BLANK;      
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AP_RVG_ORDER_BY_NAME);
	      orderByList.add(Constants.AP_RVG_ORDER_BY_NEXT_RUN_DATE);
	      orderByList.add(Constants.AP_RVG_ORDER_BY_SUPPLIER_CODE);
	      orderBy = Constants.GLOBAL_BLANK;                  
	      
	  }
      
      previousButton = null;
      nextButton = null;	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
   	
       ActionErrors errors = new ActionErrors();
       
       if (request.getParameter("goButton") != null) {
      
	       if (!Common.validateDateFormat(nextRunDateFrom)) {
	
		     errors.add("nextRunDateFrom",
		        new ActionMessage("recurringVoucherGeneration.error.nextRunDateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(nextRunDateTo)) {
		
		     errors.add("nextRunDateTo",
		        new ActionMessage("recurringVoucherGeneration.error.nextRunDateToInvalid"));
		
		  } 
	 	  
	 	  if (!Common.validateMoneyFormat(interestRate)) {

	 		  errors.add("interestRate",
	 				  new ActionMessage("recurringVoucherGeneration.error.interestRateInvalid"));

	 	  } 
		  
	   }  
	   
	   if (request.getParameter("generateButton") != null) {
	   
		   Iterator i = apRVGList.iterator();      	 
	      	 
	      	 while (i.hasNext()) {
	      	 	
	      	 	 ApRecurringVoucherGenerationList rvgList = (ApRecurringVoucherGenerationList)i.next();      	 	 
	      	 	       	 	 
	      	 	 if (!rvgList.getGenerate()) continue;
	      	 	     	      	 
		         if(Common.validateRequired(rvgList.getNextRunDate())){
		            errors.add("nextRunDate",
		               new ActionMessage("recurringVoucherGeneration.error.nextRunDateRequired", rvgList.getName()));
		         }
			 	 if(!Common.validateDateFormat(rvgList.getNextRunDate())){
		            errors.add("nextRunDate",
		               new ActionMessage("recurringVoucherGeneration.error.nextRunDateInvalid", rvgList.getName()));
		         }  
		         if(Common.validateRequired(rvgList.getVoucherDate())){
		            errors.add("journalDate",
		               new ActionMessage("recurringVoucherGeneration.error.voucherDateRequired", rvgList.getName()));
		         }
			 	 if(!Common.validateDateFormat(rvgList.getVoucherDate())){
		            errors.add("journalDate",
		               new ActionMessage("recurringVoucherGeneration.error.voucherDateInvalid", rvgList.getName()));
		         } 		     
			 	 if (!Common.validateMoneyFormat(interestRate)) {
			 		 errors.add("interestRate",
	 				   new ActionMessage("recurringVoucherGeneration.error.interestRateInvalid", rvgList.getName()));
			 	 } 
			          
		    }               	 
		    
      }
			        
      return(errors);
   }
}
