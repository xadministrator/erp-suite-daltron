package com.struts.ap.recurringvouchergeneration;

import java.io.Serializable;

public class ApRecurringVoucherGenerationList implements Serializable {

   private Integer recurringVoucherCode = null;
   private String name = null;
   private String nextRunDate = null;
   private String documentNumber = null;
   private String voucherDate = null;
   private String supplier = null;
   private String reminderSchedule = null;
   private String lastRunDate = null;
   private String interestRate = null;
   
   private boolean generate = false;
    
   private ApRecurringVoucherGenerationForm parentBean;
    
   public ApRecurringVoucherGenerationList(ApRecurringVoucherGenerationForm parentBean,
      Integer recurringVoucherCode,
      String name,
      String nextRunDate,
      String documentNumber,
      String voucherDate,
      String supplier,
      String reminderSchedule,
      String lastRunDate, 
      String interestRate){

      this.parentBean = parentBean;
      this.recurringVoucherCode = recurringVoucherCode;
      this.name = name;
      this.nextRunDate = nextRunDate;
      this.documentNumber = documentNumber;
      this.voucherDate = voucherDate;
      this.supplier = supplier;
      this.reminderSchedule = reminderSchedule;
      this.lastRunDate = lastRunDate;
      this.interestRate = interestRate;

   }
   
   public Integer getRecurringVoucherCode(){
      return(recurringVoucherCode);
   }

   public String getName(){
      return(name);
   }

   public String getNextRunDate(){
      return(nextRunDate);
   }
   
   public void setNextRunDate(String nextRunDate) {
   	  this.nextRunDate = nextRunDate;   	
   }
   
   public String getDocumentNumber(){
      return(documentNumber);
   }
   
   public void setDocumentNumber(String documentNumber) {
   	  this.documentNumber = documentNumber;
   }

   public String getVoucherDate(){
      return(voucherDate);
   }
   
   public void setVoucherDate(String voucherDate) {
   	  this.voucherDate = voucherDate;
   }
   
   public String getReminderSchedule(){
      return(reminderSchedule);
   }
   
   public String getLastRunDate(){
   	  return(lastRunDate);
   }

   public String getSupplier() {
   	  return(supplier);
   }
   
   public boolean getGenerate() {
   	  return(generate);
   }

   public void setGenerate(boolean generate){
   	  this.generate = generate;   	
   }
   
   public String getInterestRate() {
	   return(interestRate);
   }
   
   public void setInterestRate(String interestRate) {
	   this.interestRate = interestRate;
   }
   
}

