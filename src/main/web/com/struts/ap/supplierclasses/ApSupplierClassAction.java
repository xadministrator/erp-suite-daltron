package com.struts.ap.supplierclasses;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ApSCCoaGlExpenseAccountNotFoundException;
import com.ejb.exception.ApSCCoaGlPayableAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ApSupplierClassController;
import com.ejb.txn.ApSupplierClassControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModSupplierClassDetails;

public final class ApSupplierClassAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();

      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApSupplierClassAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }

        ApSupplierClassForm actionForm = (ApSupplierClassForm)form;

        String frParam = Common.getUserPermission(user, Constants.AP_SUPPLIER_CLASS_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("apSupplierClasses");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ApSupplierClassController EJB
*******************************************************/

        ApSupplierClassControllerHome homeSC = null;
        ApSupplierClassController ejbSC = null;

        try {

            homeSC = (ApSupplierClassControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApSupplierClassControllerEJB", ApSupplierClassControllerHome.class);

        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApSupplierClassAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbSC = homeSC.create();

        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApSupplierClassAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }

            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Ap SC Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_DETAILED);

	        return(mapping.findForward("apSupplierClasses"));

/*******************************************************
   -- Ap SC Hide Details Action --
*******************************************************/

	    } else if (request.getParameter("hideDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	        return(mapping.findForward("apSupplierClasses"));

/*******************************************************
   -- Ap SC Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ApModSupplierClassDetails mdetails = new ApModSupplierClassDetails();
            mdetails.setScName(actionForm.getName());
            mdetails.setScDescription(actionForm.getDescription());
            mdetails.setScCoaGlPayableAccountNumber(actionForm.getPayableAccount());
            mdetails.setScCoaGlExpenseAccountNumber(actionForm.getExpenseAccount());
            mdetails.setScInvestorBonusRate(Common.convertStringToDouble(actionForm.getInvestorBonusRate()));
            mdetails.setScInvestorInterestRate(Common.convertStringToDouble(actionForm.getInvestorInterestRate()));
            mdetails.setScEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            mdetails.setScLedger(Common.convertBooleanToByte(actionForm.getLedger()));
            mdetails.setScIsInvestment(Common.convertBooleanToByte(actionForm.getIsInvestment()));
            mdetails.setScIsLoan(Common.convertBooleanToByte(actionForm.getIsLoan()));
            mdetails.setScIsVatReliefVoucherItem(Common.convertBooleanToByte(actionForm.getIsVatReliefVoucherItem()));
            mdetails.setScLastModifiedBy(user.getUserName());
            mdetails.setScDateLastModified(new java.util.Date());



            try {

            	ejbSC.addApScEntry(mdetails, actionForm.getTaxCode(), actionForm.getWithholdingTaxCode(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("supplierClass.error.recordAlreadyExist"));

            } catch (ApSCCoaGlPayableAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("supplierClass.error.payableAccountNotFound"));

            } catch (ApSCCoaGlExpenseAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("supplierClass.error.expenseAccountNotFound"));

            } catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in ApSupplierClassAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	return mapping.findForward("cmnErrorPage");

               	}

            }

/*******************************************************
   -- Ap SC Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap SC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


            ApSupplierClassList apSCList =
	            actionForm.getApSCByIndex(actionForm.getRowSelected());


            ApModSupplierClassDetails mdetails = new ApModSupplierClassDetails();
            mdetails.setScCode(apSCList.getSupplierClassCode());
            mdetails.setScName(actionForm.getName());
            mdetails.setScDescription(actionForm.getDescription());
            mdetails.setScCoaGlPayableAccountNumber(actionForm.getPayableAccount());
            mdetails.setScCoaGlExpenseAccountNumber(actionForm.getExpenseAccount());
            mdetails.setScInvestorBonusRate(Common.convertStringToDouble(actionForm.getInvestorBonusRate()));
            mdetails.setScInvestorInterestRate(Common.convertStringToDouble(actionForm.getInvestorInterestRate()));
            mdetails.setScEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            mdetails.setScLedger(Common.convertBooleanToByte(actionForm.getLedger()));
            mdetails.setScIsInvestment(Common.convertBooleanToByte(actionForm.getIsInvestment()));
            mdetails.setScIsLoan(Common.convertBooleanToByte(actionForm.getIsLoan()));
            mdetails.setScIsVatReliefVoucherItem(Common.convertBooleanToByte(actionForm.getIsLoan()));
            mdetails.setScLastModifiedBy(user.getUserName());
            mdetails.setScDateLastModified(new java.util.Date());

            try {

            	ejbSC.updateApScEntry(mdetails, actionForm.getTaxCode(), actionForm.getWithholdingTaxCode(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("supplierClass.error.recordAlreadyExist"));

            } catch (ApSCCoaGlPayableAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("supplierClass.error.payableAccountNotFound"));

            } catch (ApSCCoaGlExpenseAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("supplierClass.error.expenseAccountNotFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApSupplierClassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

/*******************************************************
   -- Ap SC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ap SC Edit Action --
*******************************************************/

         } else if (request.getParameter("apSCList[" +
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showApSCRow(actionForm.getRowSelected());

            return mapping.findForward("apSupplierClasses");

/*******************************************************
   -- Ap SC Delete Action --
*******************************************************/

         } else if (request.getParameter("apSCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ApSupplierClassList apSCList =
              actionForm.getApSCByIndex(actionForm.getRowSelected());

            try {

	            ejbSC.deleteApScEntry(apSCList.getSupplierClassCode(), user.getCmpCode());

	        } catch (GlobalRecordAlreadyAssignedException ex) {

	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("supplierClass.error.deleteSupplierClassAlreadyAssigned"));

	        } catch (GlobalRecordAlreadyDeletedException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("supplierClass.error.supplierClassAlreadyDeleted"));

            } catch(EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApSupplierClassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }

               return(mapping.findForward("cmnErrorPage"));

            }

/*******************************************************
   -- Ap SC Load Action --
*******************************************************/

         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apSupplierClasses");

            }

            ArrayList list = null;
            Iterator i = null;

	        try {

               	actionForm.clearApSCList();

               	actionForm.clearTaxCodeList();

               	list = ejbSC.getApTcAll(user.getCmpCode());

               	if (list == null || list.size() == 0) {

                	actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

               	} else {

            		i = list.iterator();

                  	while (i.hasNext()) {

            	    	actionForm.setTaxCodeList((String)i.next());

            	  	}

               	}

               	actionForm.clearWithholdingTaxCodeList();

               	list = ejbSC.getApWtcAll(user.getCmpCode());

               	if (list == null || list.size() == 0) {

                   	actionForm.setWithholdingTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

               	} else {

               	i = list.iterator();

                  	while (i.hasNext()) {

            	    	actionForm.setWithholdingTaxCodeList((String)i.next());

            	  	}

               	}

	           	list = ejbSC.getApScAll(user.getCmpCode());

	           	i = list.iterator();

	           	while(i.hasNext()) {

	              	ApModSupplierClassDetails mdetails = (ApModSupplierClassDetails)i.next();

	              	ApSupplierClassList apSCList = new ApSupplierClassList(actionForm,
	            		mdetails.getScCode(),
	            	    mdetails.getScName(),
	            	    mdetails.getScDescription(),
	            	    mdetails.getScTcName(),
	            	    mdetails.getScWtcName(),
	            	    mdetails.getScCoaGlPayableAccountNumber(),
	            	    mdetails.getScCoaGlPayableAccountDescription(),
                        mdetails.getScCoaGlExpenseAccountNumber().toString(),
                        mdetails.getScCoaGlExpenseAccountDescription().toString(),
                        Double.toString(mdetails.getScInvestorBonusRate()),
                        Double.toString(mdetails.getScInvestorInterestRate()),
                        Common.convertByteToBoolean(mdetails.getScEnable()),
                        Common.convertByteToBoolean(mdetails.getScLedger()),
                        Common.convertByteToBoolean(mdetails.getScIsInvestment()),
                        Common.convertByteToBoolean(mdetails.getScIsLoan()),
                        Common.convertByteToBoolean(mdetails.getScIsVatReliefVoucherItem()),

                        mdetails.getScLastModifiedBy(),
                        Common.convertSQLDateToString(mdetails.getScDateLastModified())
                        );


	              	actionForm.saveApSCList(apSCList);

	           	}

	        } catch (GlobalNoRecordFoundException ex) {

	        } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApSupplierClassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveButton") != null ||
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);

	        if (actionForm.getTableType() == null) {

	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

            }

            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);

            return(mapping.findForward("apSupplierClasses"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ApSupplierClassAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }

}