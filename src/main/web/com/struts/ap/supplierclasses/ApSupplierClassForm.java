package com.struts.ap.supplierclasses;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApSupplierClassForm extends ActionForm implements Serializable {

	private Integer supplierClassCode = null;
	private String name = null;
	private String description = null;
	private String taxCode = null;
	private ArrayList taxCodeList = new ArrayList();
	private String withholdingTaxCode = null;
	private ArrayList withholdingTaxCodeList = new ArrayList();
	private String payableAccount = null;
	private String payableAccountDescription = null;
	private String expenseAccount =  null;
	private String expenseAccountDescription = null;
	private String investorBonusRate = "0";
	private String investorInterestRate = "0";
	private boolean enable = false;
	private boolean ledger = false;
	private boolean isInvestment = false;
	private boolean isLoan = false;
	private boolean isVatReliefVoucherItem = false;
	private String lastModifiedBy = null;
	private String dateLastModified = null;

	private String tableType = null;
	private String showDetailsButton = null;
	private String hideDetailsButton = null;
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList apSCList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();

	public int getRowSelected() {

	    return rowSelected;

	}

	public ApSupplierClassList getApSCByIndex(int index) {

	    return((ApSupplierClassList)apSCList.get(index));

	}

	public Object[] getApSCList() {

	    return apSCList.toArray();

	}

	public int getApSCListSize() {

	    return apSCList.size();

	}

	public void saveApSCList(Object newApSCList) {

	    apSCList.add(newApSCList);

	}

	public void clearApSCList() {

	    apSCList.clear();

	}

	public void setRowSelected(Object selectedApSCList, boolean isEdit) {

	    this.rowSelected = apSCList.indexOf(selectedApSCList);

	    if (isEdit) {

	        this.pageState = Constants.PAGE_STATE_EDIT;

	    }

	}

	public void showApSCRow(int rowSelected) {

		this.name = ((ApSupplierClassList)apSCList.get(rowSelected)).getName();
		this.description = ((ApSupplierClassList)apSCList.get(rowSelected)).getDescription();

		if (!this.taxCodeList.contains(((ApSupplierClassList)apSCList.get(rowSelected)).getTaxCode())) {

			if (this.taxCodeList.contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

				this.taxCodeList.clear();
				this.taxCodeList.add(Constants.GLOBAL_BLANK);

			}
			this.taxCodeList.add(((ApSupplierClassList)apSCList.get(rowSelected)).getTaxCode());

		}
		this.taxCode = ((ApSupplierClassList)apSCList.get(rowSelected)).getTaxCode();

		if (!this.withholdingTaxCodeList.contains(((ApSupplierClassList)apSCList.get(rowSelected)).getWithholdingTaxCode())) {

			if (this.withholdingTaxCodeList.contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

				this.withholdingTaxCodeList.clear();
				this.withholdingTaxCodeList.add(Constants.GLOBAL_BLANK);

			}
			this.withholdingTaxCodeList.add(((ApSupplierClassList)apSCList.get(rowSelected)).getWithholdingTaxCode());

		}
		this.withholdingTaxCode = ((ApSupplierClassList)apSCList.get(rowSelected)).getWithholdingTaxCode();

        this.payableAccount = ((ApSupplierClassList)apSCList.get(rowSelected)).getPayableAccount();
        this.payableAccountDescription = ((ApSupplierClassList)apSCList.get(rowSelected)).getPayableAccountDescription();
        this.expenseAccount = ((ApSupplierClassList)apSCList.get(rowSelected)).getExpenseAccount();
        this.expenseAccountDescription = ((ApSupplierClassList)apSCList.get(rowSelected)).getExpenseAccountDescription();
        this.investorBonusRate = ((ApSupplierClassList)apSCList.get(rowSelected)).getInvestorBonusRate();
        this.investorInterestRate = ((ApSupplierClassList)apSCList.get(rowSelected)).getInvestorInterestRate();
        this.enable = ((ApSupplierClassList)apSCList.get(rowSelected)).getEnable();
        this.ledger = ((ApSupplierClassList)apSCList.get(rowSelected)).getLedger();
        this.isInvestment = ((ApSupplierClassList)apSCList.get(rowSelected)).getIsInvestment();
        this.isLoan = ((ApSupplierClassList)apSCList.get(rowSelected)).getIsLoan();
        this.isVatReliefVoucherItem = ((ApSupplierClassList)apSCList.get(rowSelected)).getIsVatReliefVoucherItem();
        this.lastModifiedBy = ((ApSupplierClassList)apSCList.get(rowSelected)).getLastModifiedBy();
        this.dateLastModified = ((ApSupplierClassList)apSCList.get(rowSelected)).getDateLastModified();



	}

	public void updateApSCRow(int rowSelected, Object newApSCList) {

	    apSCList.set(rowSelected, newApSCList);

	}

	public void deleteApSCList(int rowSelected) {

	    apSCList.remove(rowSelected);

	}

	public void setUpdateButton(String updateButton) {

	    this.updateButton = updateButton;

	}

	public void setCancelButton(String cancelButton) {

	    this.cancelButton = cancelButton;

	}

	public void setSaveButton(String saveButton) {

	    this.saveButton = saveButton;

	}

	public void setCloseButton(String closeButton) {

	    this.closeButton = closeButton;

	}

	public void setShowDetailsButton(String showDetailsButton) {

	    this.showDetailsButton = showDetailsButton;

	}

	public void setHideDetailsButton(String hideDetailsButton) {

	    this.hideDetailsButton = hideDetailsButton;

	}

	public void setPageState(String pageState) {

	    this.pageState = pageState;

	}

	public String getPageState() {

	    return pageState;

	}

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;

	}

	public void setTxnStatus(String txnStatus) {

	  	this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

	  	return userPermission;

	}

	public void setUserPermission(String userPermission) {

	  	this.userPermission = userPermission;

	}

	public Integer getSupplierClassCode() {

		return supplierClassCode;

	}

	public void setSupplierClassCode(Integer supplierClassCode) {

		this.supplierClassCode = supplierClassCode;

	}

	public String getName() {

	  	return name;

	}

	public void setName(String name) {

	  	this.name = name;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

	  	this.description = description;

	}

    public String getTaxCode() {

    	return taxCode;

    }

    public void setTaxCode(String taxCode) {

    	this.taxCode = taxCode;

    }

    public ArrayList getTaxCodeList() {

    	return taxCodeList;

    }

    public void setTaxCodeList(String taxCode) {

    	taxCodeList.add(taxCode);

    }

    public void clearTaxCodeList() {

    	taxCodeList.clear();
    	taxCodeList.add(Constants.GLOBAL_BLANK);

    }

    public String getWithholdingTaxCode() {

    	return withholdingTaxCode;

    }

    public void setWithholdingTaxCode(String withholdingTaxCode) {

    	this.withholdingTaxCode = withholdingTaxCode;

    }

    public ArrayList getWithholdingTaxCodeList() {

    	return withholdingTaxCodeList;

    }

    public void setWithholdingTaxCodeList(String withholdingTaxCode) {

    	withholdingTaxCodeList.add(withholdingTaxCode);

    }

    public void clearWithholdingTaxCodeList() {

    	withholdingTaxCodeList.clear();
    	withholdingTaxCodeList.add(Constants.GLOBAL_BLANK);

    }

    public String getPayableAccount() {

    	return payableAccount;

    }

    public void setPayableAccount(String payableAccount) {

    	this.payableAccount = payableAccount;

    }

    public String getPayableAccountDescription() {

    	return payableAccountDescription;

    }

    public void setPayableAccountDescription(String payableAccountDescription) {

    	this.payableAccountDescription = payableAccountDescription;

    }

    public String getExpenseAccount() {

    	return expenseAccount;

    }

    public void setExpenseAccount(String expenseAccount) {

    	this.expenseAccount = expenseAccount;

    }

    public String getExpenseAccountDescription() {

    	return expenseAccountDescription;

    }

    public void setExpenseAccountDescription(String expenseAccountDescription) {

    	this.expenseAccountDescription = expenseAccountDescription;

    }

    public String getInvestorBonusRate(){
        return investorBonusRate;
    }

    public void setInvestorBonusRate(String investorBonusRate){
        this.investorBonusRate = investorBonusRate;
    }

    public String getInvestorInterestRate(){
        return investorInterestRate;
    }

    public void setInvestorInterestRate(String investorInterestRate){
        this.investorInterestRate = investorInterestRate;
    }

    public boolean getEnable() {

    	return enable;

    }

    public void setEnable(boolean enable) {

    	this.enable = enable;

    }

public boolean getLedger() {

    	return ledger;

    }

    public void setLedger(boolean ledger) {

    	this.ledger = ledger;

    }


	public boolean getIsInvestment() {

    	return isInvestment;

    }

    public void setIsInvestment(boolean isInvestment) {

    	this.isInvestment = isInvestment;

    }

    public boolean getIsLoan() {

    	return isLoan;

    }

    public void setIsLoan(boolean isLoan) {

    	this.isLoan = isLoan;

    }

    public boolean getIsVatReliefVoucherItem() {

    	return isVatReliefVoucherItem;

    }

    public void setIsVatReliefVoucherItem(boolean isVatReliefVoucherItem) {

    	this.isVatReliefVoucherItem = isVatReliefVoucherItem;

    }


public String getLastModifiedBy() {

		return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

		this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

		return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

		this.dateLastModified = dateLastModified;

	}


	public String getTableType() {

	  	return(tableType);

	}

	public void setTableType(String tableType) {

	  	this.tableType = tableType;

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		name = null;
		description = null;
        taxCode = null;
        withholdingTaxCode = null;
        payableAccount = null;
        payableAccountDescription = null;
        expenseAccount = null;
        expenseAccountDescription = null;
		enable = false;
		ledger = false;
		isInvestment = false;
		isLoan = false;
		isVatReliefVoucherItem = false;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;
        investorBonusRate = "0";
        investorInterestRate = "0";
        lastModifiedBy = null;
		dateLastModified = null;

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

			if (Common.validateRequired(name)) {

				errors.add("name",
			   		new ActionMessage("supplierClass.error.nameRequired"));

			}

			if (Common.validateRequired(taxCode)) {

				errors.add("taxCode",
			   		new ActionMessage("supplierClass.error.taxCodeRequired"));

			}

			if (Common.validateRequired(withholdingTaxCode)) {

				errors.add("withholdingTaxCode",
			   		new ActionMessage("supplierClass.error.withholdingTaxCodeRequired"));

			}

			if (Common.validateRequired(payableAccount)) {

				errors.add("payableAccount",
			   		new ActionMessage("supplierClass.error.payableAccountRequired"));

			}

                        if (!Common.validateMoneyRateFormat(investorBonusRate)){
                            errors.add("investorBonusRate",
			   		new ActionMessage("supplierClass.error.investorBonusRateFormat"));
                        }

                        if (!Common.validateNumberFormat(investorInterestRate)){
                            errors.add("investorInterestRate",
			   		new ActionMessage("supplierClass.error.investorInterestRateFormat"));
                        }


	  	}

	  	return errors;

	}

}