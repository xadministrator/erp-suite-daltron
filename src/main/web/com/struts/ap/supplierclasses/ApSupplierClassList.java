package com.struts.ap.supplierclasses;

import java.io.Serializable;

public class ApSupplierClassList implements Serializable {

    private Integer supplierClassCode = null;
    private String name = null;
    private String description = null;
    private String taxCode = null;
    private String withholdingTaxCode = null;
    private String payableAccount = null;
    private String payableAccountDescription = null;
    private String expenseAccount = null;
    private String expenseAccountDescription = null;
    private String investorBonusRate = "0";
    private String investorInterestRate = "0";
    private boolean enable = false;
    private boolean ledger = false;
    private boolean isInvestment = false;
    private boolean isLoan= false;
    private boolean isVatReliefVoucherItem = false;

    private String lastModifiedBy = null;
	private String dateLastModified = null;

	private String editButton = null;
	private String deleteButton = null;

	private ApSupplierClassForm parentBean;

	public ApSupplierClassList(ApSupplierClassForm parentBean,
		Integer supplierClassCode,
        String name,
        String description,
        String taxCode,
        String withholdingTaxCode,
        String payableAccount,
        String payableAccountDescription,
        String expenseAccount,
        String expenseAccountDescription,
        String investorBonusRate,
        String investorInterestRate,
		boolean enable, boolean ledger, boolean isInvestment, boolean isLoan, boolean isVatReliefVoucherItem,
		String lastModifiedBy,
		String dateLastModified
			) {

		this.parentBean = parentBean;
        this.supplierClassCode = supplierClassCode;
        this.name = name;
        this.description = description;
        this.taxCode = taxCode;
        this.withholdingTaxCode = withholdingTaxCode;
        this.payableAccount = payableAccount;
        this.payableAccountDescription = payableAccountDescription;
        this.expenseAccount = expenseAccount;
        this.expenseAccountDescription = expenseAccountDescription;
        this.investorBonusRate = investorBonusRate;
        this.investorInterestRate = investorInterestRate;
		this.enable = enable;
		this.ledger = ledger;
		this.isInvestment = isInvestment;
		this.isLoan = isLoan;
		this.isVatReliefVoucherItem = isVatReliefVoucherItem;
		this.lastModifiedBy = lastModifiedBy;
		this.dateLastModified = dateLastModified;

	}

	public void setEditButton(String editButton) {

	    parentBean.setRowSelected(this, true);

	}

	public void setDeleteButton(String deleteButton) {

	    parentBean.setRowSelected(this, true);

	}

	public Integer getSupplierClassCode() {

	    return supplierClassCode;

	}

	public String getName() {

		return name;

    }

    public String getDescription() {

    	return description;

    }

    public String getTaxCode() {

    	return taxCode;

    }

    public String getWithholdingTaxCode() {

    	return withholdingTaxCode;

    }

    public String getPayableAccount() {

    	return payableAccount;

    }

    public String getPayableAccountDescription() {

    	return payableAccountDescription;

    }

    public String getExpenseAccount() {

    	return expenseAccount;

    }

    public String getExpenseAccountDescription() {

    	return expenseAccountDescription;

    }

    public String getInvestorBonusRate(){
        return investorBonusRate;
    }

    public String getInvestorInterestRate(){
        return investorInterestRate;
    }
    public boolean getEnable() {

    	return enable;

    }
    public boolean getLedger() {

    	return ledger;

    }

    public boolean getIsInvestment() {

    	return isInvestment;

    }

    public boolean getIsLoan() {

	return isLoan;

    }

    public boolean getIsVatReliefVoucherItem() {
    	return isVatReliefVoucherItem;
    }

    public String getLastModifiedBy() {

		return lastModifiedBy;

	}

	public String getDateLastModified() {

		return dateLastModified;

	}



}