package com.struts.ap.supplierentry;

import java.io.Serializable;


/**
 * 
 * @author Franco Antonio R. Roig
 * Created: 10/25/2005 5:25 PM
 * 
 */
public class ApBranchSupplierList implements Serializable {
	
	private String brName = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;
	private String branchPayableAccount = null;
	private String branchPayableDescription = null;
	private String branchExpenseAccount = null;
	private String branchExpenseDescription = null;
	
	private ApSupplierEntryForm parentBean;
	
	public ApBranchSupplierList(ApSupplierEntryForm parentBean, 
			String brName, Integer brCode) {
		
		this.parentBean = parentBean;
		this.brName = brName;
		this.brCode = brCode;
		
	}

	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
	public String getBranchPayableAccount() {
		return this.branchPayableAccount;
	}
	
	public void setBranchPayableAccount(String branchPayableAccount) {
		this.branchPayableAccount = branchPayableAccount;
	}
	
	public String getBranchPayableDescription() {
		return this.branchPayableDescription;
	}
	
	public void setBranchPayableDescription(String branchPayableDescription) {
		this.branchPayableDescription = branchPayableDescription;
	}
	
	public String getBranchExpenseAccount() {
		return this.branchExpenseAccount;
	}
	
	public void setBranchExpenseAccount(String branchExpenseAccount) {
		this.branchExpenseAccount = branchExpenseAccount;
	}
	
	public String getBranchExpenseDescription() {
		return this.branchExpenseDescription;
	}
	
	public void setBranchExpenseDescription(String branchExpenseDescription) {
		this.branchExpenseDescription = branchExpenseDescription;
	}
}

