package com.struts.ap.supplierentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ApSCCoaGlExpenseAccountNotFoundException;
import com.ejb.exception.ApSCCoaGlPayableAccountNotFoundException;
import com.ejb.exception.GlobalNameAndAddressAlreadyExistsException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ApSupplierEntryController;
import com.ejb.txn.ApSupplierEntryControllerHome;
import com.struts.util.Branch;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchSupplierDetails;
import com.util.AdResponsibilityDetails;
import com.util.ApModSupplierClassDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModSupplierTypeDetails;

public final class ApSupplierEntryAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		HttpSession session = request.getSession();
		
		try {

/*******************************************************
	 Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ApSupplierEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ApSupplierEntryForm actionForm = (ApSupplierEntryForm)form;
			
			String ceParam = Common.getUserPermission(user, Constants.AP_SUPPLIER_ENTRY_ID);
			
			if (ceParam != null) {
				
				if (ceParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("apSupplierEntry");
					}
					
				}
				
				actionForm.setUserPermission(ceParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}

/*******************************************************
	 Initialize ApSupplierEntryController EJB
*******************************************************/
			
			ApSupplierEntryControllerHome homeSE = null;                  
			ApSupplierEntryController ejbSE = null;         
			
			try {
				
				homeSE = (ApSupplierEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ApSupplierEntryControllerEJB", ApSupplierEntryControllerHome.class);
				
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in ApSupplierEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbSE = homeSE.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in ApSupplierEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();	

/*******************************************************
	 adPrfApAutoGenerateSupplierCode
	 getPrfApNextSupplierCode
*******************************************************/
			
			boolean adPrfApAutoGenerateSupplierCode = false;
			String nextSupplierCode = null;
			boolean useCustomerPulldown = true;
			
			try {
				
				useCustomerPulldown = Common.convertByteToBoolean(ejbSE.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
				actionForm.setUseCustomerPulldown(useCustomerPulldown);
				
				adPrfApAutoGenerateSupplierCode = Common.convertByteToBoolean(
						ejbSE.getPrfApAutoGenerateSupplierCode(user.getCmpCode()));
				actionForm.setAutoGenerateSupplierCode(adPrfApAutoGenerateSupplierCode);
				
				if(adPrfApAutoGenerateSupplierCode && request.getParameter("forward") == null) {
					
					actionForm.setIsNew(true);
					nextSupplierCode = ejbSE.getPrfApNextSupplierCode(user.getCmpCode());
					
				}
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in ApSupplierEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}

/*******************************************************
 -- Ap SE Save Action --
*******************************************************/
		
			if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				ApModSupplierDetails details = new ApModSupplierDetails();
				details.setSplCode(actionForm.getSplCode());
				details.setSplSupplierCode(actionForm.getSupplierCode());
                                details.setSplAccountNumber(actionForm.getAccountNumber());
				details.setSplName(actionForm.getSupplierName());
				details.setSplAddress(actionForm.getAddress());
				details.setSplCity(actionForm.getCity());
				details.setSplStateProvince(actionForm.getStateProvince());
				details.setSplPostalCode(actionForm.getPostalCode()); 
				details.setSplCountry(actionForm.getCountry()); 
				details.setSplContact(actionForm.getContact());
				details.setSplPhone(actionForm.getPhone());
				details.setSplFax(actionForm.getFax());
				details.setSplAlternatePhone(actionForm.getAlternatePhone()); 
				details.setSplAlternateContact(actionForm.getAlternateContact());
				details.setSplEmail(actionForm.getEmail());
				details.setSplEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				details.setSplTin(actionForm.getTinNumber());  
				details.setSplRemarks(actionForm.getRemarks());
				
				// For Branch Setting
				ArrayList branchList = new ArrayList();
				
				try {
					
					for(int i=0; i<actionForm.getApBSplListSize(); i++) {
						ApBranchSupplierList bsplList = actionForm.getApBSplByIndex(i);
						
						if(bsplList.getBranchCheckbox() == true) {
							
							// check branch's payable account if null or not
							if (bsplList.getBranchPayableAccount() == null || bsplList.getBranchPayableAccount().length() < 1)
								bsplList.setBranchPayableAccount(actionForm.getPayableAccount());
							
							// check branch's expense account if null or not
							if (bsplList.getBranchExpenseAccount() == null || bsplList.getBranchExpenseAccount().length() < 1)
								bsplList.setBranchExpenseAccount(actionForm.getExpenseAccount());

							AdModBranchSupplierDetails mdetails = new AdModBranchSupplierDetails();
							mdetails.setBSplBranchCode(bsplList.getBrCode());
							mdetails.setBSplBranchName(bsplList.getBrName());
							mdetails.setBSplPayableAccountNumber(bsplList.getBranchPayableAccount());
							mdetails.setBSplPayableAccountDesc(bsplList.getBranchPayableDescription());
							mdetails.setBSplExpenseAccountNumber(bsplList.getBranchExpenseAccount());
							mdetails.setBSplExpenseAccountDesc(bsplList.getBranchExpenseDescription());

							branchList.add(mdetails);
						}
					}
					
					// get user's current selected responsibility name
					AdResponsibilityDetails adRsDetails = ejbSE.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
					
					if (actionForm.getSupplierType().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
						actionForm.setSupplierType(null);
					}            	            	
					System.out.println("EXPENSE : " + actionForm.getExpenseAccount());
					ejbSE.saveApSplEntry(details, actionForm.getSupplierType(), 
							actionForm.getPaymentTerm(), actionForm.getSupplierClass(),
							actionForm.getPayableAccount(), actionForm.getExpenseAccount(),
							actionForm.getBankAccount(), adRsDetails.getRsName(), branchList, actionForm.getTemplateName(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("supplierEntry.error.supplierCodeAlreadyExist"));    
					
				} catch (GlobalNameAndAddressAlreadyExistsException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("supplierEntry.error.supplierNameAndAddressAlreadyExist"));
					
				} catch (ApSCCoaGlPayableAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("supplierEntry.error.payableAccountNotFound"));
					
				} catch (ApSCCoaGlExpenseAccountNotFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("supplierEntry.error.expenseAccountNotFound"));

				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApSupplierEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if(adPrfApAutoGenerateSupplierCode) {
					
					nextSupplierCode = ejbSE.getPrfApNextSupplierCode(user.getCmpCode());
					actionForm.setIsNew(true);
					
				}
				
/*******************************************************
 -- Ap SPL Supplier Class Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("isSupplierClassEntered"))) {
				
				try {
					
					ApModSupplierClassDetails mdetails = ejbSE.getApScByScName(actionForm.getSupplierClass(), user.getCmpCode());
					
					actionForm.setPayableAccount(mdetails.getScCoaGlPayableAccountNumber());
					actionForm.setPayableDescription(mdetails.getScCoaGlPayableAccountDescription());
					actionForm.setExpenseAccount(mdetails.getScCoaGlExpenseAccountNumber());
					actionForm.setExpenseDescription(mdetails.getScCoaGlExpenseAccountDescription());
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApSupplierEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}  	
				
				return(mapping.findForward("apSupplierEntry"));
				
/*******************************************************
 -- Ap SPL Supplier Type Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("isSupplierTypeEntered"))) {
				
				try {
					
					if (!actionForm.getSupplierType().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
						
						ApModSupplierTypeDetails mdetails = ejbSE.getApStByStName(actionForm.getSupplierType(), user.getCmpCode());	                                  
						actionForm.setBankAccount(mdetails.getStBaName());
						
					}
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApSupplierEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}  	
				
				return(mapping.findForward("apSupplierEntry"));
				
/*******************************************************
 -- Ap SE Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
 -- Ap SE Load Action --
*******************************************************/
				
			}

			if (ceParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("apSupplierEntry");
					
				}
				
				
				if (request.getParameter("forward") == null &&
						actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
					saveErrors(request, new ActionMessages(errors));
					
					return mapping.findForward("cmnMain");	
					
				}

				actionForm.reset(mapping, request);
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
					
					
					actionForm.clearSupplierTypeList();
					
					list = ejbSE.getApStAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setSupplierTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setSupplierTypeList((String)i.next());
							
						}
						
					}
					

					actionForm.clearPaymentTermList();
					
					list = ejbSE.getApPytAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setPaymentTermList((String)i.next());
							
						}
						
						actionForm.setPaymentTerm("IMMEDIATE");
						
					}
					
					
					actionForm.clearSupplierClassList();
					
					list = ejbSE.getApScAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setSupplierClassList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setSupplierClassList((String)i.next());
							
						}
						
					} 
					
					actionForm.clearBankAccountList();
					
					list = ejbSE.getAdBaAll(user.getCmpCode());
					
					if (list == null || list.size() == 0) {
						
						actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
						
					} else {
						
						i = list.iterator();
						
						while (i.hasNext()) {
							
							actionForm.setBankAccountList((String)i.next());
							
						}
						
					} 

					// branch fields
					actionForm.clearApBFSplList();	        
					
					for (int j = 0; j < user.getBrnchCount(); j++) {

						 Branch branch = user.getBranch(j);
						 AdBranchDetails details = new AdBranchDetails();
						 details.setBrCode(new Integer(branch.getBrCode()));
						 details.setBrName(branch.getBrName());
						 ApBranchSupplierList apBSplList = new ApBranchSupplierList(actionForm,
								details.getBrName(), details.getBrCode());
						 
						 if ( request.getParameter("forward") == null )
						 	apBSplList.setBranchCheckbox(true);
						 
						 actionForm.saveApBSplList(apBSplList);
						 
					}
					
					actionForm.clearTemplateList();
	            	
	            	list = ejbSE.getInvLitAll(user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setTemplateList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setTemplateList((String)i.next());
	            			
	            		}
	            		
	            	} 
					
					

					if (request.getParameter("forward") != null) {

						ApModSupplierDetails splDetails = ejbSE.getApSplBySplCode(
								new Integer(request.getParameter("splCode")), user.getCmpCode());
						
						actionForm.setSplCode(splDetails.getSplCode());
						actionForm.setSupplierCode(splDetails.getSplSupplierCode());
						actionForm.setAccountNumber(splDetails.getSplAccountNumber());
                                                actionForm.setSupplierName(splDetails.getSplName());
						actionForm.setAddress(splDetails.getSplAddress());
						actionForm.setCity(splDetails.getSplCity());
						actionForm.setStateProvince(splDetails.getSplStateProvince());
						actionForm.setPostalCode(splDetails.getSplPostalCode()); 
						actionForm.setCountry(splDetails.getSplCountry()); 
						actionForm.setContact(splDetails.getSplContact());
						actionForm.setPhone(splDetails.getSplPhone());
						actionForm.setFax(splDetails.getSplFax());
						actionForm.setAlternatePhone(splDetails.getSplAlternatePhone()); 
						actionForm.setAlternateContact(splDetails.getSplAlternateContact());
						actionForm.setEmail(splDetails.getSplEmail());
						actionForm.setTinNumber(splDetails.getSplTin());
						actionForm.setEnable(Common.convertByteToBoolean(splDetails.getSplEnable()));
						actionForm.setRemarks(splDetails.getSplRemarks());
						
						if (!actionForm.getSupplierTypeList().contains(splDetails.getSplStName())
								&& splDetails.getSplStName() != null) {
							
							if (actionForm.getSupplierTypeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
								
								actionForm.clearSupplierTypeList();
								
							}							
							actionForm.setSupplierTypeList(splDetails.getSplStName());
							
						}					
						actionForm.setSupplierType(splDetails.getSplStName());
						
						if (!actionForm.getPaymentTermList().contains(splDetails.getSplPytName())
								&& splDetails.getSplPytName() != null) {
							
							if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
								
								actionForm.clearPaymentTermList();
								
							}				    
							actionForm.setPaymentTermList(splDetails.getSplPytName());
							
						}				    
						actionForm.setPaymentTerm(splDetails.getSplPytName());
						
						
						
						
						if (!actionForm.getSupplierClassList().contains(splDetails.getSplScName())
								&& splDetails.getSplScName() != null) {
							
							if (actionForm.getSupplierClassList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
								
								actionForm.clearSupplierClassList();
								
							}				    
							actionForm.setSupplierClassList(splDetails.getSplScName());
							
						}				    
						actionForm.setSupplierClass(splDetails.getSplScName());
						
						if (!actionForm.getBankAccountList().contains(splDetails.getSplBaName())) {
							
							if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
								
								actionForm.clearBankAccountList();
								
							}	   
							actionForm.setBankAccountList(splDetails.getSplBaName());
							
						}				    
						actionForm.setBankAccount(splDetails.getSplBaName());
						
						actionForm.setPayableAccount(splDetails.getSplCoaGlPayableAccountNumber());
						actionForm.setExpenseAccount(splDetails.getSplCoaGlExpenseAccountNumber());
						actionForm.setPayableDescription(splDetails.getSplCoaGlPayableAccountDescription());
						actionForm.setExpenseDescription(splDetails.getSplCoaGlExpenseAccountDescription());
						actionForm.setTemplateName(splDetails.getSplLitName());
						
						actionForm.setIsNew(false);
						
						// set branch lists for this supplier
						list = null;
						i = null;
						
						Integer splCode = new Integer( request.getParameter("splCode") ); 
						
						ArrayList branchSuppliers = ejbSE.getAdBrSplBySplCode(splCode, user.getCmpCode()); 
						
						ArrayList branchList = new ArrayList();
						
						actionForm.clearApBFSplList();
						
						// all branches that user has access to
						for (int k = 0; k < user.getBrnchCount(); k++) {

							 Branch branch = user.getBranch(k);
							 AdBranchDetails details = new AdBranchDetails();
							 details.setBrCode(new Integer(branch.getBrCode()));
							 details.setBrName(branch.getBrName());
							 branchList.add(details);
						}

						i = branchList.iterator();
						
						while(i.hasNext()) {
							AdBranchDetails details = (AdBranchDetails)i.next();
							
							ApBranchSupplierList apBSplList = new ApBranchSupplierList(actionForm,
									details.getBrName(), details.getBrCode());

							Iterator j = branchSuppliers.iterator();
							while ( j.hasNext() ) {
								AdModBranchSupplierDetails mdetails = (AdModBranchSupplierDetails)j.next();
								
								// if this branch is mapped to the selected supplier
								if ( mdetails.getBSplBranchCode().equals(details.getBrCode()) ) {
									apBSplList.setBranchCheckbox(true);
									apBSplList.setBranchPayableAccount(mdetails.getBSplPayableAccountNumber());
									apBSplList.setBranchPayableDescription(mdetails.getBSplPayableAccountDesc());
									apBSplList.setBranchExpenseAccount(mdetails.getBSplExpenseAccountNumber());
									apBSplList.setBranchExpenseDescription(mdetails.getBSplExpenseAccountDesc());
									break;
								}
							}
							
							actionForm.saveApBSplList(apBSplList);
						}
						return(mapping.findForward("apSupplierEntry"));  
						
					}          		                	            	
					
					actionForm.setSplCode(null);           
					actionForm.setEnable(true);
					actionForm.setNextSupplierCode(nextSupplierCode);
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("supplierEntry.error.noRecordFound")); 
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ApSupplierEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}           
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if ((request.getParameter("saveButton") != null &&
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
				}
				
				return(mapping.findForward("apSupplierEntry"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(ArithmeticException e) {
			
/*******************************************************
 System Failed: Forward to error page 
*******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in ApSupplierEntryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
	}
}