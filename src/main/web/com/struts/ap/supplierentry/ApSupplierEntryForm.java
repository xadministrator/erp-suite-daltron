package com.struts.ap.supplierentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApSupplierEntryForm extends ActionForm implements Serializable {
	
	private Integer splCode = null;
	private String supplierCode = null;
    private String accountNumber = null;
	private String nextSupplierCode = null;
	private String supplierName = null;
	private String supplierType = null;
	private ArrayList supplierTypeList = new ArrayList();
	
	
	private boolean useCustomerPulldown = true;
	
	private String supplierClass = null;
	private ArrayList supplierClassList = new ArrayList();    
	private String paymentTerm = null;
	private ArrayList paymentTermList = new ArrayList();
	private boolean enable = false;      
	private String address = null;
	private String city = null;
	private String stateProvince = null;
	private String postalCode = null;
	private String country = null;   
	private String contact = null;
	private String phone = null;
	private String fax = null;
	private String alternatePhone = null;
	private String alternateContact = null;
	private String email = null;
	private String tinNumber = null;   
	private String bankAccount = null;
	private ArrayList bankAccountList = new ArrayList();
	private String payableAccount = null;
	private String expenseAccount = null;
	private String payableDescription = null;
	private String expenseDescription = null;
	private String templateName = null;
	private ArrayList templateList = new ArrayList();
	private String remarks = null;
	
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String isSupplierClassEntered = null;
	private String isSupplierTypeEntered = null;   
	private boolean autoGenerateSupplierCode = false;
	private boolean isNew = false;
	
	private ArrayList apBSplList = new ArrayList();
	
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getSplCode() {
		
		return splCode;
		
	}
	
	public void setSplCode(Integer splCode) {
		
		this.splCode = splCode;
		
	}
	
	public String getSupplierCode() {
		
		return supplierCode;
		
	}
	
	public void setSupplierCode(String supplierCode) {
		
		this.supplierCode = supplierCode;
		
	}
        
        
        public String getAccountNumber() {
		
		return accountNumber;
		
	}
	
	public void setAccountNumber(String accountNumber) {
		
		this.accountNumber = accountNumber;
		
	}
	
	public String getNextSupplierCode() {
		
		return nextSupplierCode;
		
	}
	
	public void setNextSupplierCode(String nextSupplierCode) {
		
		this.nextSupplierCode = nextSupplierCode;
		
	}
	
	public String getSupplierName() {
		
		return supplierName;
		
	}
	
	public void setSupplierName(String supplierName) {
		
		this.supplierName = supplierName;
		
	}
	
	
	public String getSupplierType() {
		
		return supplierType;
		
	}
	
	public void setSupplierType(String supplierType) {
		
		this.supplierType = supplierType;
		
	}                         
	
	public ArrayList getSupplierTypeList() {
		
		return supplierTypeList;
		
	}
	
	public void setSupplierTypeList(String supplierType) {
		
		supplierTypeList.add(supplierType);
		
	}
	
	public void clearSupplierTypeList() {
		
		supplierTypeList.clear();
		supplierTypeList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	


	public boolean getUseCustomerPulldown() {
	   	
	   	  return useCustomerPulldown;
	   	  
	   }
	   
	   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
	   	
	   	  this.useCustomerPulldown = useCustomerPulldown;
	   	  
	   }
	   
	
	public String getSupplierClass() {
		
		return supplierClass;
		
	}
	
	public void setSupplierClass(String supplierClass) {
		
		this.supplierClass = supplierClass;
		
	}                         
	
	public ArrayList getSupplierClassList() {
		
		return supplierClassList;
		
	}
	
	public void setSupplierClassList(String supplierClass) {
		
		supplierClassList.add(supplierClass);
		
	}
	
	public void clearSupplierClassList() {
		
		supplierClassList.clear();
		supplierClassList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getPaymentTerm() {
		
		return paymentTerm;
		
	}
	
	public void setPaymentTerm(String paymentTerm) {
		
		this.paymentTerm = paymentTerm;
		
	}                         
	
	public ArrayList getPaymentTermList() {
		
		return paymentTermList;
		
	}
	
	public void setPaymentTermList(String paymentTerm) {
		
		paymentTermList.add(paymentTerm);
		
	}       
	
	public void clearPaymentTermList() {
		
		paymentTermList.clear();
		paymentTermList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public boolean getEnable() {
		
		return enable;
		
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		
	}
	
	public String getAddress() {
		
		return address;
		
	}
	
	public void setAddress(String address) {
		
		this.address = address;
		
	} 
	
	public String getCity() {
		
		return city;
		
	}
	
	public void setCity(String city) {
		
		this.city = city;
		
	}   
	
	public String getStateProvince() {
		
		return stateProvince;
		
	}
	
	public void setStateProvince(String stateProvince) {
		
		this.stateProvince = stateProvince;
		
	}  
	
	public String getPostalCode() {
		
		return postalCode;
		
	}
	
	public void setPostalCode(String postalCode) {
		
		this.postalCode = postalCode;
		
	}   
	
	public String getCountry() {
		
		return country;
		
	}
	
	public void setCountry(String country) {
		
		this.country = country;
		
	}     
	
	public String getContact() {
		
		return contact;
		
	}
	
	public void setContact(String contact) {
		
		this.contact = contact;
		
	}  
	
	public String getPhone() {
		
		return phone;
		
	}
	
	public void setPhone(String phone) {
		
		this.phone = phone;
		
	}
	
	public String getFax() {
		
		return fax;
		
	}
	
	public void setFax(String fax) {
		
		this.fax = fax;
		
	}  
	
	public String getAlternatePhone() {
		
		return alternatePhone;
		
	}
	
	public void setAlternatePhone(String alternatePhone) {
		
		this.alternatePhone = alternatePhone;
		
	}
	
	public String getAlternateContact() {
		
		return alternateContact;
		
	}
	
	public void setAlternateContact(String alternateContact) {
		
		this.alternateContact = alternateContact;
		
	}
	
	public String getEmail() {
		
		return email;
		
	}
	
	public void setEmail(String email) {
		
		this.email = email;
		
	}
	
	public String getTinNumber() {
		
		return tinNumber;
		
	}
	
	public void setTinNumber(String tinNumber) {
		
		this.tinNumber = tinNumber;
		
	}
	
	public String getBankAccount() {
		
		return bankAccount;   	
		
	}
	
	public void setBankAccount(String bankAccount) {
		
		this.bankAccount = bankAccount;
		
	}
	
	public ArrayList getBankAccountList() {
		
		return bankAccountList;
		
	}
	
	public void setBankAccountList(String bankAccount) {
		
		bankAccountList.add(bankAccount);
		
	}
	
	public void clearBankAccountList() {
		
		bankAccountList.clear();
		bankAccountList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getPayableAccount() {
		
		return payableAccount;   	
		
	}
	
	public void setPayableAccount(String payableAccount) {
		
		this.payableAccount = payableAccount;
		
	}
	
	public String getExpenseAccount() {
		
		return expenseAccount;   	
		
	}
	
	public void setExpenseAccount(String expenseAccount) {
		
		this.expenseAccount = expenseAccount;
		
	}
	
	public String getPayableDescription() {
		
		return payableDescription;   	
		
	}
	
	public void setPayableDescription(String payableDescription) {
		
		this.payableDescription = payableDescription;
		
	}
	
	public String getExpenseDescription() {
		
		return expenseDescription;   	
		
	}
	
	public void setExpenseDescription(String expenseDescription) {
		
		this.expenseDescription = expenseDescription;
		
	}
	
	public String getIsSupplierClassEntered() {
		
		return isSupplierClassEntered;   	
		
	}
	
	public void setIsSupplierClassEntered(String isSupplierClassEntered) {
		
		this.isSupplierClassEntered = isSupplierClassEntered;
		
	}
	
	public String getIsSupplierTypeEntered() {
		
		return isSupplierTypeEntered;   	
		
	}
	
	public void setIsSupplierTypeEntered(String isSupplierTypeEntered) {
		
		this.isSupplierTypeEntered = isSupplierTypeEntered;
		
	}
	
	public boolean getAutoGenerateSupplierCode() {
		
		return autoGenerateSupplierCode;
		
	}
	
	public void setAutoGenerateSupplierCode(boolean autoGenerateSupplierCode) {
		
		this.autoGenerateSupplierCode = autoGenerateSupplierCode;
		
	}
	
	public boolean getIsNew() {
		
		return isNew;
		
	}
	
	public void setIsNew(boolean isNew) {
		
		this.isNew = isNew;
		
	}
	
	public Object[] getApBSplList(){
		
		return apBSplList.toArray();
		
	}
	
	public ApBranchSupplierList getApBSplByIndex(int index) {
		
		return ((ApBranchSupplierList)apBSplList.get(index));
		
	}
	
	public int getApBSplListSize(){
		
		return(apBSplList.size());
		
	}
	
	public void saveApBSplList(Object newApBSplList){
		
		apBSplList.add(newApBSplList);   	  
		
	}
	
	public void clearApBFSplList(){
		
		apBSplList.clear();
		
	}
	
	public void setApBSplList(ArrayList apBSplList) {
		
		this.apBSplList = apBSplList;
		
	}
	
	public String getTemplateName() {
		
		return templateName;
		
	}
	
	public void setTemplateName(String templateName) {
		
		this.templateName = templateName;
		
	}
	
	public ArrayList getTemplateList() {
		
		return templateList;
		
	}
	
	public void setTemplateList(String template) {
		
		templateList.add(template);
		
	}
	
	public void clearTemplateList() {
		
		templateList.clear();
		templateList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getRemarks() {
		
		return remarks;
		
	}
	
	public void setRemarks(String remarks) {
		
		this.remarks = remarks;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<apBSplList.size(); i++) {
			
			ApBranchSupplierList actionList = (ApBranchSupplierList)apBSplList.get(i);
			actionList.setBranchCheckbox(false);
			
		}
		
		supplierCode = null;
                accountNumber = null;
		supplierName = null;
		supplierType = Constants.GLOBAL_BLANK;
		supplierClass = Constants.GLOBAL_BLANK;
		paymentTerm = Constants.GLOBAL_BLANK;
		bankAccount = Constants.GLOBAL_BLANK;
		enable = false;
		address = null;
		city = null;
		stateProvince = null;
		postalCode = null;
		country = null;   	  
		contact = null;
		phone = null;
		fax = null;
		alternatePhone = null;
		alternateContact = null;
		email = null;
		tinNumber = null;     
		payableAccount = null;   	  
		expenseAccount = null;
		payableDescription = null;
		expenseDescription = null;
		isSupplierClassEntered = null;
		isSupplierTypeEntered = null;
		templateName = Constants.GLOBAL_BLANK;
		remarks = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("saveButton") != null) {         
			
			if (Common.validateRequired(supplierCode)) {
				
				if (autoGenerateSupplierCode == false) {
					
					errors.add("supplierCode", 
							new ActionMessage("supplierEntry.error.supplierCodeRequired"));
					
				}
			}
			
			if (Common.validateRequired(supplierName)) {
				
				errors.add("supplierName",
						new ActionMessage("supplierEntry.error.supplierNameRequired"));
				
			}
			
			if (Common.validateRequired(supplierClass) || supplierClass.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				errors.add("supplierClass",
						new ActionMessage("supplierEntry.error.supplierClassRequired"));
				
			}         
			
			
			if (Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				errors.add("paymentTerm",
						new ActionMessage("supplierEntry.error.paymentTermRequired"));
				
			}
			
			if (!Common.validateStringExists(supplierTypeList, supplierType)) {
				
				errors.add("supplierType",
						new ActionMessage("supplierEntry.error.supplierTypeInvalid"));
				
			}         
			
			if (!Common.validateStringExists(paymentTermList, paymentTerm)) {
				
				errors.add("paymentTerm",
						new ActionMessage("supplierEntry.error.paymentTermInvalid"));
				
			}        
			
			if (!Common.validateStringExists(supplierClassList, supplierClass)) {
				
				errors.add("supplierClass",
						new ActionMessage("supplierEntry.error.supplierClassInvalid"));
				
			}
			
			if (Common.validateRequired(bankAccount)) {
				
				errors.add("bankAccount",
						new ActionMessage("supplierEntry.error.bankAccountRequired"));
				
			}
			
			if (Common.validateRequired(payableAccount)) {
				
				errors.add("payableAccount",
						new ActionMessage("supplierEntry.error.payableAccountRequired"));
				
			}
			
			if (Common.validateRequired(expenseAccount)) {
				
				errors.add("expenseAccount",
						new ActionMessage("supplierEntry.error.expenseAccountRequired"));
				
			}
			
			
		}
		
		return errors;
	}
}