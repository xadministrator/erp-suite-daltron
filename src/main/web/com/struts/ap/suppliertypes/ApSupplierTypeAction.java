package com.struts.ap.suppliertypes;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ApSupplierTypeController;
import com.ejb.txn.ApSupplierTypeControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModSupplierTypeDetails;
import com.util.ApSupplierTypeDetails;

public final class ApSupplierTypeAction extends Action {
	
    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
      	
      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApSupplierTypeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ApSupplierTypeForm actionForm = (ApSupplierTypeForm)form;

        String frParam = Common.getUserPermission(user, Constants.AP_SUPPLIER_TYPE_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("apSupplierTypes");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ApSupplierTypeController EJB
*******************************************************/

        ApSupplierTypeControllerHome homeST = null;
        ApSupplierTypeController ejbST = null;

        try {

            homeST = (ApSupplierTypeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApSupplierTypeControllerEJB", ApSupplierTypeControllerHome.class);
            
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApSupplierTypeAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbST = homeST.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApSupplierTypeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ap ST Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apSupplierTypes"));
	     
/*******************************************************
   -- Ap ST Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apSupplierTypes"));                  
         
/*******************************************************
   -- Ap ST Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ApSupplierTypeDetails details = new ApSupplierTypeDetails();
            details.setStName(actionForm.getName());
            details.setStDescription(actionForm.getDescription());
            details.setStEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            
            try {
            	
            	ejbST.addApStEntry(details, actionForm.getBankAccount(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("supplierType.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in ApSupplierTypeAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	return mapping.findForward("cmnErrorPage"); 
                  
               	}

            }

/*******************************************************
   -- Ap ST Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap ST Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            ApSupplierTypeList apSTList =
	            actionForm.getApSTByIndex(actionForm.getRowSelected());
            
            
            ApSupplierTypeDetails details = new ApSupplierTypeDetails();
            details.setStCode(apSTList.getSupplierTypeCode());
            details.setStName(actionForm.getName());
            details.setStDescription(actionForm.getDescription());
            details.setStEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            
            try {
            	
            	ejbST.updateApStEntry(details, actionForm.getBankAccount(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("supplierType.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApSupplierTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ap ST Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ap ST Edit Action --
*******************************************************/

         } else if (request.getParameter("apSTList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showApSTRow(actionForm.getRowSelected());
            
            return mapping.findForward("apSupplierTypes");
            
/*******************************************************
   -- Ap ST Delete Action --
*******************************************************/

         } else if (request.getParameter("apSTList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ApSupplierTypeList apSTList =
              actionForm.getApSTByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbST.deleteApStEntry(apSTList.getSupplierTypeCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("supplierType.error.deleteSupplierTypeAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("supplierType.error.supplierTypeAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApSupplierTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ap ST Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apSupplierTypes");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	        	
               	actionForm.clearApSTList();	        	    	

               	actionForm.clearBankAccountList();           	
            	
               	list = ejbST.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
               	if (list == null || list.size() == 0) {
            		
                	actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
               	} else {
            		           		            		
            		i = list.iterator();
            		
                  	while (i.hasNext()) {
            			
            	    	actionForm.setBankAccountList((String)i.next());
            			
            	  	}
            		            		
               	} 	           
                              	           
	           	list = ejbST.getApStAll(user.getCmpCode()); 
	           
	           	i = list.iterator();
	            
	           	while(i.hasNext()) {
        	            			            	
	              	ApModSupplierTypeDetails mdetails = (ApModSupplierTypeDetails)i.next();
	            	
	              	ApSupplierTypeList apSTList = new ApSupplierTypeList(actionForm,
	            		mdetails.getStCode(),
	            	    mdetails.getStName(),
	            	    mdetails.getStDescription(),
	            	    mdetails.getStBaName(),
                        Common.convertByteToBoolean(mdetails.getStEnable()));                            
	            	 	            	    
	              	actionForm.saveApSTList(apSTList);
	            	
	           	}
	                          
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApSupplierTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("apSupplierTypes"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ApSupplierTypeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}