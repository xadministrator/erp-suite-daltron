package com.struts.ap.suppliertypes;

import java.io.Serializable;

public class ApSupplierTypeList implements Serializable {

	private Integer supplierTypeCode = null;
	private String name = null;
	private String description = null;
	private String bankAccount = null;
	private boolean enable = false;

	private String editButton = null;
	private String deleteButton = null;
	
	private ApSupplierTypeForm parentBean;
    
	public ApSupplierTypeList(ApSupplierTypeForm parentBean,
		Integer supplierTypeCode,
		String name,
		String description,
		String bankAccount,
		boolean enable) {
	
		this.parentBean = parentBean;
		this.supplierTypeCode = supplierTypeCode;
		this.name = name;
		this.description = description;
		this.bankAccount = bankAccount;
		this.enable = enable;
		
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getSupplierTypeCode() {
	
	    return supplierTypeCode;
	
	}
	
	public String getName() {
		
		return name;
		
    }
    
    public String getDescription() {
    	
    	return description;
    	
    }
    
    public String getBankAccount() {
    	
    	return bankAccount;
    	
    }
    
    public boolean getEnable() {
    	
    	return enable;
    	
    }
        	    	
}