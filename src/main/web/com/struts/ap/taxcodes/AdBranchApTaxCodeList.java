package com.struts.ap.taxcodes;

import java.io.Serializable;

public class AdBranchApTaxCodeList implements Serializable {
	
	
	private Integer branchCode = null;
	private String branchName = null;
	
	private boolean branchCheckbox = false;	
	private String branchTaxCodeAccount = null;
	private String branchTaxCodeAccountDescription = null;

	private ApTaxCodeForm parentBean;
	
	public AdBranchApTaxCodeList(ApTaxCodeForm parentBean,
			Integer branchCode,
			String branchName,
			String branchTaxCodeAccount,
			String branchTaxCodeAccountDescription

			) {
		
		this.parentBean = parentBean;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.branchTaxCodeAccount = branchTaxCodeAccount;
		this.branchTaxCodeAccountDescription = branchTaxCodeAccountDescription;

		
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBranchCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	

	public String getBranchTaxCodeAccount() {
		
		return this.branchTaxCodeAccount;
		
	}
	
	public void setBranchTaxCodeAccount(String branchTaxCodeAccount) {
		
		this.branchTaxCodeAccount = branchTaxCodeAccount;
		
	}
	
	public String getBranchTaxCodeAccountDescription() {
		
		return this.branchTaxCodeAccountDescription;
		
	}
	
	public void setBranchTaxCodeAccountDescription(String branchTaxCodeAccountDescription) {
		
		this.branchTaxCodeAccountDescription = branchTaxCodeAccountDescription;
		
	}
	
	
	
}