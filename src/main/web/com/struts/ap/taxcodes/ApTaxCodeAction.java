package com.struts.ap.taxcodes;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ApTaxCodeController;
import com.ejb.txn.ApTaxCodeControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchApTaxCodeDetails;
import com.util.AdResponsibilityDetails;
import com.util.ApModTaxCodeDetails;
import com.util.ApTaxCodeDetails;
import com.util.ArModTaxCodeDetails;

public final class ApTaxCodeAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      //try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApTaxCodeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApTaxCodeForm actionForm = (ApTaxCodeForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_TAX_CODE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apTaxCodes");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApTaxCodeController EJB
*******************************************************/

         ApTaxCodeControllerHome homeTC = null;
         ApTaxCodeController ejbTC = null;

         try {

            homeTC = (ApTaxCodeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApTaxCodeControllerEJB", ApTaxCodeControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbTC = homeTC.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ap TC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apTaxCodes"));
	     
/*******************************************************
   -- Ap TC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apTaxCodes"));                  
         
/*******************************************************
   -- Ap TC Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ApTaxCodeDetails details = new ApTaxCodeDetails();
            details.setTcName(actionForm.getTaxName());
            details.setTcDescription(actionForm.getDescription());
            details.setTcType(actionForm.getType());                                    
            details.setTcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setTcEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            
            try {
            	
            	ejbTC.addApTcEntry(details, actionForm.getTaxAccount(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("apTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("apTaxCode.error.accountNumberInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ap TC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap TC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
        	 ApTaxCodeList apTCList =
                     ((ApTaxCodeForm)form).getApTCByIndex(
                     ((ApTaxCodeForm) form).getRowSelected());
            
           
            ApModTaxCodeDetails details = new ApModTaxCodeDetails();
            
            details.setTcCode(apTCList.getTaxCode());
            details.setTcName(actionForm.getTaxName());
            details.setTcDescription(actionForm.getDescription());
            details.setTcType(actionForm.getType());
            details.setTcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setTcEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
         // For Branch Setting
            ArrayList branchList = new ArrayList();
                     
                     for (int i = 0; i<actionForm.getApBTCListSize(); i++) {
                     	
                    	 AdBranchApTaxCodeList arBtcList = actionForm.getApBTCByIndex(i);           	   
                     	
                    	
                     	if (arBtcList.getBranchCheckbox() == true ){ 
                     		
                     		// checks if branches glcoa values are null
                     		
                     		if ( Common.validateRequired(arBtcList.getBranchTaxCodeAccount()) ||
                     				Common.validateRequired(arBtcList.getBranchTaxCodeAccountDescription())) {

                     			arBtcList.setBranchTaxCodeAccount(actionForm.getTaxAccount());
                    

                     		}
                     		
                     		
                     		
                     		AdModBranchApTaxCodeDetails brTcDetails = new AdModBranchApTaxCodeDetails();
                     		brTcDetails.setBtcAdCompany(user.getCmpCode());
                     		brTcDetails.setBtcBranchCode(arBtcList.getBranchCode()  );
                     		brTcDetails.setBtcBranchName(arBtcList.getBranchName() );
                     		brTcDetails.setBtcTaxCodeAccountNumber(arBtcList.getBranchTaxCodeAccount());
                     		brTcDetails.setBtcTaxCodeAccountDescription(arBtcList.getBranchTaxCodeAccountDescription());
                     		
                     				
                     		
                         	branchList.add(brTcDetails);

                     	}
                     	
                     	
                     }
                     
            AdResponsibilityDetails mdetails  = ejbTC.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
             
            
            try {
            	
            	ejbTC.updateApTcEntry(details, actionForm.getTaxAccount(), mdetails.getRsName(),branchList, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("apTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("apTaxCode.error.accountNumberInvalid"));
               
            } catch (GlobalRecordAlreadyAssignedException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("apTaxCode.error.updateTaxCodeAlreadyAssigned"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ap TC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ap TC Edit Action --
*******************************************************/

         } else if (request.getParameter("apTCList[" + 
        		 ((ApTaxCodeForm)form).getRowSelected() + "].editButton") != null &&
                 ((ApTaxCodeForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

        	 actionForm.reset(mapping, request);

        	 
     	 	Object[] apTCList = actionForm.getApTCList();
				
				// get list of branches
	        	ArrayList branchList = new ArrayList();
	        	
	        	AdResponsibilityDetails details = ejbTC.getAdRsByRsCode(new Integer(user.getCurrentResCode()));

	        	try {                                  
	                 
	        		branchList = ejbTC.getAdBrTcAll(((ApTaxCodeList)apTCList[actionForm.getRowSelected()]).getTaxCode(),details.getRsName(), user.getCmpCode());
	                 
	                 
	             } catch(GlobalNoRecordFoundException ex) {
	                 
	                 
	             }
	        	
            actionForm.showApTCRow(branchList,actionForm.getRowSelected());
            
            return mapping.findForward("apTaxCodes");
            
/*******************************************************
   -- Ap TC Delete Action --
*******************************************************/

         } else if (request.getParameter("apTCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ApTaxCodeList apTCList =
              actionForm.getApTCByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbTC.deleteApTcEntry(apTCList.getTaxCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("apTaxCode.error.deleteTaxCodeAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("apTaxCode.error.taxCodeAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ap TC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apTaxCodes");

            }
            
            if (request.getParameter("forward") == null &&
                    actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                    	                	
                    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                    saveErrors(request, new ActionMessages(errors));
                  
                    return mapping.findForward("cmnMain");	
                    
                }
            System.out.println("----------------1");
            actionForm.reset(mapping, request);
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
 	           
 	           
 	           
 	           
 	           
 	           actionForm.clearApBTCList();
                
                list = ejbTC.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
                
                i = list.iterator();
                
                while(i.hasNext()) {
                
                    AdBranchDetails details = (AdBranchDetails)i.next();
                    
                    
                    AdBranchApTaxCodeList arBTCList = new AdBranchApTaxCodeList(actionForm,
                        details.getBrCode(),
                        details.getBrName(),
                        null,
                        null
                        );
                    
                    
                    
                    System.out.println("----------------2");
                    if ( request.getParameter("forward") == null )
                 	   arBTCList.setBranchCheckbox(true);
                                 
                    actionForm.saveApBTCList(arBTCList);
                    
                }
 	            
 	        } catch (GlobalNoRecordFoundException ex) {
 	        	
 	        } catch (EJBException ex) {
                 
                if (log.isInfoEnabled()) {

                   log.info("xxxxxxxxxxxEJBException caught in ApTaxCodeAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                   return mapping.findForward("cmnErrorPage"); 
                   
                }

             }  
            
                      	                        	                          	            	
	        try {
	    	
               actionForm.clearApTCList();	        
	           
	           list = ejbTC.getApTcAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              ApModTaxCodeDetails mdetails = (ApModTaxCodeDetails)i.next();
	            	
	              ApTaxCodeList apTCList = new ApTaxCodeList(actionForm,
	            	    mdetails.getTcCode(),
	            	    mdetails.getTcName(),
	            	    mdetails.getTcDescription(),
	            	    mdetails.getTcType(),
	            	    Common.convertDoubleToStringMoney(mdetails.getTcRate(), Constants.MONEY_RATE_PRECISION),
                        mdetails.getTcCoaGlTaxAccountNumber(),
                        mdetails.getTcCoaGlTaxDescription(),
                        Common.convertByteToBoolean(mdetails.getTcEnable()));                            
	            	 	            	    
	              
	              actionForm.saveApTCList(apTCList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

            	if((request.getParameter("saveButton") != null || 
                        request.getParameter("updateButton") != null || 
                        request.getParameter("apTCList[" + 
                        ((ApTaxCodeForm) form).getRowSelected() + "].deleteButton") != null) && 
                        ((ApTaxCodeForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                        ((ApTaxCodeForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
                    }
            }

            if (((ApTaxCodeForm)form).getTableType() == null) {
                
                ((ApTaxCodeForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
                
            }                
            System.out.println("----------------6");
            ((ApTaxCodeForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            return(mapping.findForward("apTaxCodes"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      /*} catch(Exception e) {

*//*******************************************************
   System Failed: Forward to error page 
*******************************************************//*

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ApTaxCodeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }*/

   }
   
}