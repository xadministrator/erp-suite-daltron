package com.struts.ap.taxcodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdModBranchApTaxCodeDetails;
import com.util.Debug;


public class ApTaxCodeForm extends ActionForm implements Serializable {

	private Integer taxCode = null;
	private String taxName = null;
	private String description = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String rate = null;
	private String taxAccount = null;
	private String accountDescription = null;
	private boolean enable = false;
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList apTCList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	
	private ArrayList apBTCList = new ArrayList();
	public Object[] getApBTCList(){
	       
	       return apBTCList.toArray();
	       
	   }
	   
	   public AdBranchApTaxCodeList getApBTCByIndex(int index){
	       
	       return ((AdBranchApTaxCodeList)apBTCList.get(index));
	       
	   }
	   
	   public int getApBTCListSize(){
	       
	       return(apBTCList.size());
	       
	   }
	   
	   public void saveApBTCList(Object newApBTCList){
	       
	       apBTCList.add(newApBTCList);   	  
	       
	   }
	   
	   public void clearApBTCList(){
	       
	       apBTCList.clear();
	       
	   }
	   
	   public void setApBTCList(ArrayList apBTCList) {
	       
	       this.apBTCList = apBTCList;
	       
	   }
	   
	public int getRowSelected() {
	
	    return rowSelected;
	
	}
	
	public ApTaxCodeList getApTCByIndex(int index) {
	
	    return((ApTaxCodeList)apTCList.get(index));
	
	}
	
	public Object[] getApTCList() {
	
	    return apTCList.toArray();
	
	}
	
	public int getApTCListSize() {
	
	    return apTCList.size();
	
	}
	
	public void saveApTCList(Object newApTCList) {
	
	    apTCList.add(newApTCList);
	
	}
	
	public void clearApTCList() {
		
	    apTCList.clear();
	  
	}
	
	public void setRowSelected(Object selectedApTCList, boolean isEdit) {
	
	    this.rowSelected = apTCList.indexOf(selectedApTCList);
	
	    if (isEdit) {
	
	        this.pageState = Constants.PAGE_STATE_EDIT;
	
	    }
	
	}
	
	public void showApTCRow(ArrayList branchList, int rowSelected) {
		
		Debug.print("ApTaxCodeForm showApTCRow");
		
		this.taxName = ((ApTaxCodeList)apTCList.get(rowSelected)).getTaxName();
		this.description = ((ApTaxCodeList)apTCList.get(rowSelected)).getDescription();
		this.type = ((ApTaxCodeList)apTCList.get(rowSelected)).getType();
		this.rate = ((ApTaxCodeList)apTCList.get(rowSelected)).getRate();
		this.taxAccount = ((ApTaxCodeList)apTCList.get(rowSelected)).getTaxAccount();
		this.accountDescription = ((ApTaxCodeList)apTCList.get(rowSelected)).getAccountDescription();      
		this.enable = ((ApTaxCodeList)apTCList.get(rowSelected)).getEnable();
		
		
		Object[] obj = this.getApBTCList();
	       
	       this.clearApBTCList();
	       
	       for(int i=0; i<obj.length; i++) {
	           
	    	   AdBranchApTaxCodeList btcList = (AdBranchApTaxCodeList)obj[i];
	           
	           
	           if(branchList != null) {
	               
	               Iterator brListIter = branchList.iterator();
	               
	               while(brListIter.hasNext()) {
	            	   
	            	   AdModBranchApTaxCodeDetails brTcDetails = (AdModBranchApTaxCodeDetails)brListIter.next();
	                  
	                   if(brTcDetails.getBtcBranchCode().equals(btcList.getBranchCode())) {
	                	   
	                	   btcList.setBranchCheckbox(true);  
	                	   btcList.setBranchTaxCodeAccount(brTcDetails.getBtcTaxCodeAccountNumber());
	                	   btcList.setBranchTaxCodeAccountDescription(brTcDetails.getBtcTaxCodeAccountDescription());
	                	  
	                       
	                       break;
	                       
	                   }
	                   
	               }
	           }
	           
	           this.apBTCList.add(btcList);
	           
	       }
	   
	}
	
	public void updateApTCRow(int rowSelected, Object newApTCList) {
	
	    apTCList.set(rowSelected, newApTCList);
	
	}
	
	public void deleteApTCList(int rowSelected) {
	
	    apTCList.remove(rowSelected);
	
	}
	
	public void setUpdateButton(String updateButton) {
	
	    this.updateButton = updateButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
	
	    this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	    this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	    this.closeButton = closeButton;
	
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
	    this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
	    this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
	
	    this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	    return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getTaxCode() {
		
		return taxCode;
		
	}
	
	public void setTaxCode(Integer taxCode) {
		
		this.taxCode = taxCode;
		
	}
	
	public String getTaxName() {
	
	  	return taxName;
	
	}
	
	public void setTaxName(String taxName) {
	
	  	this.taxName = taxName;
	
	}
	
	public String getDescription() {
	
		return description;
	
	}
	
	public void setDescription(String description) {
	
	  	this.description = description;
	
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	  
	public String getRate() {
	
	  	return rate;
	
	}
	
	public void setRate(String rate) {
	
	  	this.rate = rate;
	
	}
	
	public String getTaxAccount() {
	
	  	return taxAccount;
	
	}
	
	public void setTaxAccount(String taxAccount) {
	
	  	this.taxAccount = taxAccount;
	
	}
	
	public String getAccountDescription() {
	
	  	return accountDescription;
	
	}
	
	public void setAccountDescription(String accountDescription) {
	
	  	this.accountDescription = accountDescription;
	
	}   
	
	public boolean getEnable() {
		
	  	return enable;
	  
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		  
	}
	
	public String getTableType() {
		
	  	return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
	  	this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		Debug.print("ApTaxCodeForm reset");
		
	       System.out.println("apBTCList.size()="+apBTCList.size());
	       for (int i=0; i<apBTCList.size(); i++) {
	           
	    	   AdBranchApTaxCodeList list = (AdBranchApTaxCodeList)apBTCList.get(i);
	           
	           
	           list.setBranchCheckbox(false);
	           list.setBranchTaxCodeAccount(null);
	           list.setBranchTaxCodeAccountDescription(null);

	       } 
	
		taxName = null;
		description = null;
		typeList.clear();
		typeList.add(Constants.AP_TAX_CODE_TYPE_NONE);
		typeList.add(Constants.AP_TAX_CODE_TYPE_EXEMPT);
		typeList.add(Constants.AP_TAX_CODE_TYPE_INCLUSIVE);
		typeList.add(Constants.AP_TAX_CODE_TYPE_EXCLUSIVE);
		typeList.add(Constants.AP_TAX_CODE_TYPE_ZERO_RATED);
		type = Constants.AP_TAX_CODE_TYPE_NONE;
		rate = null;
		taxAccount = null;
		accountDescription = null;
		enable = false;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;      
	
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
			if (Common.validateRequired(taxName)) {
			
				errors.add("taxName",
			   		new ActionMessage("apTaxCode.error.taxNameRequired"));
			
			}
			
			if (type.equals(Constants.AP_TAX_CODE_TYPE_INCLUSIVE) || type.equals(Constants.AP_TAX_CODE_TYPE_EXCLUSIVE) || type.equals(Constants.AP_TAX_CODE_TYPE_ZERO_RATED)) {
			
				if (Common.validateRequired(taxAccount)) {
				
					errors.add("taxAccount",
				   		new ActionMessage("apTaxCode.error.taxAccountRequired"));
				
				}
				
		    }
			
			if (!Common.validateMoneyFormat(rate)) {
			
				errors.add("rate",
			   		new ActionMessage("apTaxCode.error.rateInvalid"));
			
			}
			
			if ((type.equals(Constants.AP_TAX_CODE_TYPE_NONE) ||
			     type.equals(Constants.AP_TAX_CODE_TYPE_EXEMPT) ||
			     type.equals(Constants.AP_TAX_CODE_TYPE_ZERO_RATED)) &&
			    (Common.validateMoneyFormat(rate)) && Common.convertStringMoneyToDouble(rate, (short)6) > 0d) {       
			    
				errors.add("rate",
			   		new ActionMessage("apTaxCode.error.rateMustBeZero"));
			   		
			}			    
	     
	  	}
	     
	  	return errors;
	
	}
	
}