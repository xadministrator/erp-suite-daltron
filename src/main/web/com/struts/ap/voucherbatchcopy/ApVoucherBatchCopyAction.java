package com.struts.ap.voucherbatchcopy;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalBatchCopyInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.ApVoucherBatchCopyController;
import com.ejb.txn.ApVoucherBatchCopyControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModVoucherDetails;

public final class ApVoucherBatchCopyAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApVoucherBatchCopyAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApVoucherBatchCopyForm actionForm = (ApVoucherBatchCopyForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.AP_VOUCHER_BATCH_COPY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apVoucherBatchCopy"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ApVoucherBatchCopyController EJB
*******************************************************/

         ApVoucherBatchCopyControllerHome homeVBC = null;
         ApVoucherBatchCopyController ejbVBC = null;

         try {
            
            homeVBC = (ApVoucherBatchCopyControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApVoucherBatchCopyControllerEJB", ApVoucherBatchCopyControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApVoucherBatchCopyAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbVBC = homeVBC.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ApVoucherBatchCopyAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call ApVoucherBatchCopy EJB
     getGlFcPrecisionUnit
  *******************************************************/
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbVBC.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlJournalBatchCopyAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ap VBC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apVoucherBatchCopy"));
	     
/*******************************************************
   -- Ap VBC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apVoucherBatchCopy"));  
	        
	     }
         
/*******************************************************
   -- Ap VBC Copy Action --
*******************************************************/

         if (request.getParameter("copyButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           
           ArrayList list = new ArrayList();
           
           for (int i=0; i<actionForm.getApVBCListSize(); i++) {
           
              ApVoucherBatchCopyList actionList = actionForm.getApVBCByIndex(i);
              
              if (actionList.getCopy()) list.add(actionList.getVoucherCode());
           
           }

           try {
           	
           	    ejbVBC.executeApVouBatchCopy(list, actionForm.getBatchTo(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	           	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchCopy.error.transactionBatchClose")); 
                    
           } catch (GlobalBatchCopyInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchCopy.error.batchCopyInvalid")); 
           
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApVoucherBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }

/*******************************************************
   -- Ap VBC Batch From Entered Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isBatchFromEntered"))) {
         
            try {
            
                actionForm.clearApVBCList();
            
                ArrayList list = ejbVBC.getApVouByVbName(actionForm.getBatchFrom(), user.getCmpCode());
                                                
                Iterator i = list.iterator();
                
                while (i.hasNext()) {
                                   
                   ApModVoucherDetails mdetails = (ApModVoucherDetails)i.next();
            		
            		ApVoucherBatchCopyList actionList = new ApVoucherBatchCopyList(actionForm,
            		    mdetails.getVouCode(),
            		    Common.convertByteToBoolean(mdetails.getVouDebitMemo()),
            		    mdetails.getVouSplSupplierCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getVouDate()),
                        mdetails.getVouDocumentNumber(),
                        mdetails.getVouReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),                            
            		    Common.convertDoubleToStringMoney(mdetails.getVouAmountPaid(), precisionUnit));
                       
                   actionForm.saveApVBCList(actionList);
                                                     
                }
                
            } catch (GlobalNoRecordFoundException ex) {
            
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchCopy.error.noVoucherFound"));                    
            
            } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApVoucherBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apVoucherBatchCopy"));
               
           }
           
           return(mapping.findForward("apVoucherBatchCopy"));
                     
/*******************************************************
   -- Ap VBC Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ap VBC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apVoucherBatchCopy"));
               
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearApVBCList();
            	actionForm.clearBatchFromList();
            	actionForm.clearBatchToList();
            	
            	list = ejbVBC.getApOpenVbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchFromList(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setBatchToList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			String batchName = (String)i.next();
            			
            		    actionForm.setBatchFromList(batchName);
            		    actionForm.setBatchToList(batchName);
            			
            		}
            		            		
            	}
            	            	
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApVoucherBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("copyButton") != null) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               
               }
            }

            actionForm.reset(mapping, request);
            
            if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }            
            
            return(mapping.findForward("apVoucherBatchCopy"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ApVoucherBatchCopyAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }
}