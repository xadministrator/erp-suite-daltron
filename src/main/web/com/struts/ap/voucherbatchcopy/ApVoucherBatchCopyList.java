package com.struts.ap.voucherbatchcopy;

import java.io.Serializable;

public class ApVoucherBatchCopyList implements Serializable {

   private Integer voucherCode = null;
   private boolean debitMemo = false;
   private String supplierCode = null;
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;  
   private String amountDue = null;
   private String amountPaid = null;
   
   private boolean copy = false;
       
   private ApVoucherBatchCopyForm parentBean;
    
   public ApVoucherBatchCopyList(ApVoucherBatchCopyForm parentBean,
      Integer voucherCode,
      boolean debitMemo,
      String supplierCode,
      String date,
      String documentNumber,
      String referenceNumber,
      String amountDue,
      String amountPaid) {

      this.parentBean = parentBean;
      this.voucherCode = voucherCode;
      this.debitMemo = debitMemo;
      this.supplierCode = supplierCode;
      this.date = date;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amountDue = amountDue;
      this.amountPaid = amountPaid;
      
   }

   public Integer getVoucherCode() {

      return voucherCode;

   }
   
   public boolean getDebitMemo() {
   	
   	  return debitMemo;
   	
   }

   public String getSupplierCode() {

      return supplierCode;
      
   }

   public String getDate() {
   
      return date;
      
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmountDue() {
   
      return amountDue;
   
   }

   public String getAmountPaid() {
   
      return amountPaid;
   
   }
   
   public boolean getCopy() {
   	
   	  return copy;
   	
   }
   
   public void setCopy(boolean copy) {
   	
   	  this.copy = copy;
   	
   }
      
   
}