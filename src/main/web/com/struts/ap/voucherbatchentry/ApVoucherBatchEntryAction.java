package com.struts.ap.voucherbatchentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.ApVoucherBatchEntryController;
import com.ejb.txn.ApVoucherBatchEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApVoucherBatchDetails;

public final class ApVoucherBatchEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ApVoucherBatchEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ApVoucherBatchEntryForm actionForm = (ApVoucherBatchEntryForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.AP_VOUCHER_BATCH_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apVoucherBatchEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ApVoucherBatchEntryController EJB
*******************************************************/

         ApVoucherBatchEntryControllerHome homeVB = null;
         ApVoucherBatchEntryController ejbVB = null;

         try {
            
            homeVB = (ApVoucherBatchEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApVoucherBatchEntryControllerEJB", ApVoucherBatchEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApVoucherBatchEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbVB = homeVB.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ApVoucherBatchEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ap VB Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ApVoucherBatchDetails details = new ApVoucherBatchDetails();
           
           details.setVbCode(actionForm.getVoucherBatchCode());
           details.setVbName(actionForm.getBatchName());
           details.setVbDescription(actionForm.getDescription());
           details.setVbDepartment(actionForm.getDepartment());
           details.setVbStatus(actionForm.getStatus());
           details.setVbType(actionForm.getType());
           
           if (actionForm.getVoucherBatchCode() == null) {
           	
           	   details.setVbCreatedBy(user.getUserName());
	           details.setVbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbVB.saveApVbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchEntry.error.transactionBatchClose")); 
           
           } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchEntry.error.saveRecordAlreadyAssigned"));          	                      	
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApVoucherBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
    -- Ap VB Invoice Action --
*******************************************************/

         } else if (request.getParameter("voucherButton") != null &&
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                   	           	
                   ApVoucherBatchDetails details = new ApVoucherBatchDetails();
                   
                   details.setVbCode(actionForm.getVoucherBatchCode());
                   details.setVbName(actionForm.getBatchName());
                   details.setVbDescription(actionForm.getDescription());
                   details.setVbDepartment(actionForm.getDepartment());
                   details.setVbStatus(actionForm.getStatus());
                   details.setVbType(actionForm.getType());
                   
                   if (actionForm.getVoucherBatchCode() == null) {
                   	
                   	   details.setVbCreatedBy(user.getUserName());
        	           details.setVbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
        	                      
                   }
                   
                   
                   try {
                   	
                   	    ejbVB.saveApVbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                   	
                   } catch (GlobalRecordAlreadyExistException ex) {
                   	
                   	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("voucherBatchEntry.error.recordAlreadyExist"));
                   	
                   } catch (GlobalRecordAlreadyDeletedException ex) {
                   	
                   	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("voucherBatchEntry.error.recordAlreadyDeleted"));
                                       	
                   } catch (GlobalTransactionBatchCloseException ex) {
                   	
                   	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("voucherBatchEntry.error.transactionBatchClose")); 
                   
                   } catch (GlobalRecordAlreadyAssignedException ex) {
                   	
                   	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("voucherBatchEntry.error.saveRecordAlreadyAssigned"));          	                      	
                   	
                   } catch (EJBException ex) {
                   	    if (log.isInfoEnabled()) {
                       	
                          log.info("EJBException caught in ApVoucherBatchEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                        }
                       
                       return(mapping.findForward("cmnErrorPage"));
                   }           

                   
                   if (errors.isEmpty()) {
                   	   
                   	   String path = null;
                   	
                   	   if (actionForm.getType().equals("VOUCHER")) {
                   	   	
	                   	 path = "/apVoucherEntry.do?forwardBatch=1" +
					         "&batchName=" + actionForm.getBatchName();	
                   	                      	   	
                   	   } else {
                   	
                   	   	 path = "/apDebitMemoEntry.do?forwardBatch=1" +
				         	  "&batchName=" + actionForm.getBatchName();                   	   	 
                   	   	
                   	   }
                   	                      	   
                   	   return(new ActionForward(path));
                   	 
                   }

/*******************************************************
   -- Ap VB Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbVB.deleteApVbEntry(actionForm.getVoucherBatchCode(), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchEntry.error.recordAlreadyDeleted"));
                    
            } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchEntry.error.deleteRecordAlreadyAssigned"));
                    
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApVoucherBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- Ap VB Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ap VB Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apVoucherBatchEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            
            
            try {
            	
            	ArrayList list = null;
				Iterator i = null;

				actionForm.clearDepartmentList();
				   
				   list = ejbVB.getAdLvDEPARTMENT(user.getCmpCode());
				       i = list.iterator();
				       
						if (list == null || list.size() == 0) {  		
							actionForm.setDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);
						} else {     		            		
							i = list.iterator();
							while (i.hasNext()) {
							    actionForm.setDepartmentList((String)i.next());
							}
						}
					
            	
            	if (request.getParameter("forward") != null) {
            		            			            	    
            		actionForm.setVoucherBatchCode(new Integer(request.getParameter("voucherBatchCode")));            		            		
            		
            		ApVoucherBatchDetails details = ejbVB.getApVbByVbCode(actionForm.getVoucherBatchCode(), user.getCmpCode());
            		
            		actionForm.setBatchName(details.getVbName());
            		actionForm.setDescription(details.getVbDescription());
            		actionForm.setCreatedBy(details.getVbCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(details.getVbDateCreated()));
            		if (!actionForm.getDepartmentList().contains(details.getVbDepartment())
							&& details.getVbDepartment() != null) {
						
						if (actionForm.getDepartmentList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
							
							actionForm.clearDepartmentList();
							
						}							
						actionForm.setDepartmentList(details.getVbDepartment());
						
					}					
					actionForm.setDepartment(details.getVbDepartment());
            		actionForm.setStatus(details.getVbStatus());            		
            		actionForm.setType(details.getVbType());
            					         
			        this.setFormProperties(actionForm);
			         
			        return (mapping.findForward("apVoucherBatchEntry"));   			         
			         
            		
            	}            	
	                    		           			   	
            } catch(GlobalNoRecordFoundException ex) {            	            	
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherBatchEntry.error.voucherBatchAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApVoucherBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("saveButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setVoucherBatchCode(null);          
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setStatus("OPEN");
            actionForm.setType("INVOICE");
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("apVoucherBatchEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ApVoucherBatchEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ApVoucherBatchEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
							
			if (actionForm.getVoucherBatchCode() == null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(false);
				
			} else {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(true);
				
			}									
						
		} else {
						
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			
			
		}
				    
	}
	
}