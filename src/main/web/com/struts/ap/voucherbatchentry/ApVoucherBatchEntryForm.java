package com.struts.ap.voucherbatchentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApVoucherBatchEntryForm extends ActionForm implements Serializable {

   private Integer voucherBatchCode = null;
   private String batchName = null;
   private String description = null;
   private String dateCreated = null;
   private String createdBy = null;
   private String department = null;
   private ArrayList departmentList = new ArrayList();
   private String status = null;
   private ArrayList statusList = new ArrayList();
   private String type = null;
   private ArrayList typeList = new ArrayList();
      
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showSaveButton = false;
   private boolean showDeleteButton = false;
   private int apVPOListSize = 0;
   private ArrayList apVPOList = new ArrayList();
   
   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getVoucherBatchCode() {
   	
   	  return voucherBatchCode;
   	
   }
   
   public void setVoucherBatchCode(Integer voucherBatchCode) {
   	
   	  this.voucherBatchCode = voucherBatchCode;
   	
   }
   
   public String getBatchName() {
   	  
   	  return batchName;
   	
   }
   
   public void setBatchName(String batchName) {
   	
   	  this.batchName = batchName;
   	
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public String getDateCreated() {
   	  
   	  return dateCreated;
   	   	
   }
   
   public void setDateCreated(String dateCreated) {
   	
   	  this.dateCreated = dateCreated;
   	
   }
   
   public String getCreatedBy() {
   	
   	  return createdBy;
   	
   }
   
   public void setCreatedBy(String createdBy) {
   	
   	  this.createdBy = createdBy;
   	
   }
   
   public String getDepartment() {
		
		return department;
		
	}
	
	public void setDepartment(String department) {
		
		this.department = department;
		
	}                         
	
	public ArrayList getDepartmentList() {
		
		return departmentList;
		
	}
	
	public void setDepartmentList(String department) {
		
		departmentList.add(department);
		
	}
	
	public void clearDepartmentList() {
		
		departmentList.clear();
		departmentList.add(Constants.GLOBAL_BLANK);
		
	}
   
   public String getStatus() {
   	
   	  return status;
   	
   }
   
   public void setStatus(String status) {
   	
   	  this.status = status;
   	
   }
   
   public ArrayList getStatusList() {
   	
   	  return statusList;
   	
   }
   
   public String getType() {
   	
   	  return type;
   	
   }
   
   public void setType(String type) {
   	
   	  this.type = type;
   	
   }
   
   public ArrayList getTypeList() {
   	
   	  return typeList;
   	
   }
     
   
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
      
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
      
   public boolean getShowDeleteButton() {
   	
   	   return showDeleteButton;
   	
   }
   
   public void setShowDeleteButton(boolean showDeleteButton) {
   	
   	   this.showDeleteButton = showDeleteButton;
   	
   }  
   
   
   public Object[] apVPOList() {
		
		return apVPOList.toArray();
		
	}
   
   public int getApVPOListSize() {
		
	   apVPOListSize = apVPOList.size();
		return apVPOList.size();
		
	}
	
	public void saveApVPOList(Object newApVPOList) {
		
		apVPOList.add(newApVPOList);
		
	}
	
            	   	            
   public void reset(ActionMapping mapping, HttpServletRequest request){      
       
	   batchName = null;
	   description = null;
	   dateCreated = null;
	   createdBy = null;
	   department = Constants.GLOBAL_BLANK;
	   statusList.clear();
	   statusList.add("OPEN");
	   statusList.add("CLOSED");
	   typeList.clear();
	   typeList.add("VOUCHER");
	   typeList.add("DEBIT MEMO");
           typeList.add("PURCHASE ORDER");
	   	         
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
      ActionErrors errors = new ActionErrors();
            
      if(request.getParameter("saveButton") != null || request.getParameter("voucherButton") != null){
      	
      	 if(Common.validateRequired(batchName)){
            errors.add("batchName",
               new ActionMessage("voucherBatchEntry.error.batchNameRequired"));
         }         	   	    	    	 
      }
      
      return(errors);	
   }
}
