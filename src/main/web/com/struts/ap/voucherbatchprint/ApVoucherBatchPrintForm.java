package com.struts.ap.voucherbatchprint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApVoucherBatchPrintForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();  
   private String supplierCode = null;
   private ArrayList supplierCodeList = new ArrayList();
   private boolean debitMemo = false;
   private boolean voucherVoid = false;
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String referenceNumber = null;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String paymentStatus = null;
   private ArrayList paymentStatusList = new ArrayList();      
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private boolean useSupplierPulldown = true;
   private String pageState = new String();
   private ArrayList apVBPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();
   
   private String voucherReport = null;   
   private String debitMemoReport = null;
   private String voucherEditListReport = null;
   private ArrayList voucherCodeList = null;
   
   
   public String getVoucherReport() {
   	
   	   return voucherReport;
   	
   }
   
   public void setVoucherReport(String voucherReport) {
   	
   	   this.voucherReport = voucherReport;
   	
   }
   
   public String getDebitMemoReport() {
   	
   	   return debitMemoReport;
   	
   }
   
   public void setDebitMemoReport(String debitMemoReport) {
   	
   	   this.debitMemoReport = debitMemoReport;
   	
   }
   
   public String getVoucherEditListReport() {
   	
   	   return voucherEditListReport;
   	
   }
   
   public void setVoucherEditListReport(String voucherEditListReport) {
   	
   	   this.voucherEditListReport = voucherEditListReport;
   	
   }
   
   public ArrayList getVoucherCodeList() {   	  	
   	  return(voucherCodeList);
   }
   
   public void setVoucherCodeList(ArrayList voucherCodeList) {
   	  this.voucherCodeList = voucherCodeList;
   }

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public ApVoucherBatchPrintList getApVBPByIndex(int index) {

      return((ApVoucherBatchPrintList)apVBPList.get(index));

   }

   public Object[] getApVBPList() {

      return apVBPList.toArray();

   }

   public int getApVBPListSize() {

      return apVBPList.size();

   }

   public void saveApVBPList(Object newApVBPList) {

      apVBPList.add(newApVBPList);

   }

   public void clearApVBPList() {
      apVBPList.clear();
   }

   public void setRowSelected(Object selectedApVBPList, boolean isEdit) {

      this.rowSelected = apVBPList.indexOf(selectedApVBPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showApVBPRow(int rowSelected) {

   }

   public void updateApVBPRow(int rowSelected, Object newApVBPList) {

      apVBPList.set(rowSelected, newApVBPList);

   }

   public void deleteApVBPList(int rowSelected) {

      apVBPList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getSupplierCode() {

      return supplierCode;

   }

   public void setSupplierCode(String supplierCode) {

      this.supplierCode = supplierCode;

   }

   public ArrayList getSupplierCodeList() {

      return supplierCodeList;

   }

   public void setSupplierCodeList(String supplierCode) {

      supplierCodeList.add(supplierCode);

   }

   public void clearSupplierCodeList() {

      supplierCodeList.clear();
      supplierCodeList.add(Constants.GLOBAL_BLANK);
   }
   
   public boolean getDebitMemo() {
   	
   	  return debitMemo;
   	  
   }
   
   public void setDebitMemo(boolean debitMemo) {
   	
   	  this.debitMemo = debitMemo;
   	  
   }  
   
   public boolean getVoucherVoid() {
   	
   	  return voucherVoid;
   	  
   }
   
   public void setVoucherVoid(boolean voucherVoid) {
   	
   	  this.voucherVoid = voucherVoid;
   	  
   }  
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	  
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   	  
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
   
   public String getDocumentNumberFrom() {
   	
   	  return documentNumberFrom;
   	  
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom) {
   	
   	  this.documentNumberFrom = documentNumberFrom;
   	  
   }
   
   public String getDocumentNumberTo() {
   	
   	  return documentNumberTo;
   	  
   }
   
   public void setDocumentNumberTo(String documentNumberTo) {
   	
   	  this.documentNumberTo = documentNumberTo;
   	  
   } 

   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	  this.referenceNumber = referenceNumber;
   	  
   }

   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }

   public String getCurrency() {
   	
   	  return currency;
   
   }
   
   public void setCurrency(String currency) {
   
   	  this.currency = currency;
   
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   
   }
      
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }
   
   public String getPaymentStatus() {
   	
   	  return paymentStatus;
   	  
   }
   
   public void setPaymentStatus(String paymentStatus) {
   	
   	  this.paymentStatus = paymentStatus;
   	  
   }
   
   public ArrayList getPaymentStatusList() {
   	
   	  return paymentStatusList;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   } 

   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseSupplierPulldown() {
   	
   		return useSupplierPulldown;
   		
   }
   
   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
   		
   		this.useSupplierPulldown = useSupplierPulldown;
   		
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  batchName = Constants.GLOBAL_BLANK;
      supplierCode = Constants.GLOBAL_BLANK;
      debitMemo = false;
      voucherVoid = false;
      dateFrom = null;
      dateTo = null;
      documentNumberFrom = null;
      documentNumberTo = null;
      referenceNumber = null;
      currency = Constants.GLOBAL_BLANK;
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      approvalStatus = "DRAFT";    
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add(Constants.GLOBAL_YES);
      postedList.add(Constants.GLOBAL_NO);
      posted = Constants.GLOBAL_NO;
      paymentStatusList.clear();
      paymentStatusList.add(Constants.GLOBAL_BLANK);
      paymentStatusList.add("PAID");
      paymentStatusList.add("UNPAID");
      paymentStatus = Constants.GLOBAL_BLANK;     
      
      if (orderByList.isEmpty()) {
      	
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("SUPPLIER CODE");
	      orderByList.add("DOCUMENT NUMBER");
	      orderBy = Constants.GLOBAL_BLANK;
	      
	  }
	  
	  for (int i=0; i<apVBPList.size(); i++) {
	  	
	  	  ApVoucherBatchPrintList actionList = (ApVoucherBatchPrintList)apVBPList.get(i);
	  	  actionList.setPrint(false);
	  	
	  }

      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("voucherBatchPrint.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("voucherBatchPrint.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("voucherBatchPrint.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("voucherBatchPrint.error.maxRowsInvalid"));
		
		   }
	 	   
	 	   if(useSupplierPulldown) {
	 	   	
	           if (!Common.validateStringExists(supplierCodeList, supplierCode)) {
	
	             errors.add("supplierCode",
	                new ActionMessage("voucherBatchPrint.error.supplierCodeInvalid"));
	
	           }
	 	   }
                           
      }
      
      return errors;

   }
   
}