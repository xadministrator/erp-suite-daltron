package com.struts.ap.voucherbatchsubmit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApVoucherBatchSubmitController;
import com.ejb.txn.ApVoucherBatchSubmitControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModVoucherDetails;

public final class ApVoucherBatchSubmitAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApVoucherBatchSubmitAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApVoucherBatchSubmitForm actionForm = (ApVoucherBatchSubmitForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_VOUCHER_BATCH_SUBMIT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apVoucherBatchSubmit");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApVoucherBatchSubmitController EJB
*******************************************************/

         ApVoucherBatchSubmitControllerHome homeVBS = null;
         ApVoucherBatchSubmitController ejbVBS = null;

         try {
          
            homeVBS = (ApVoucherBatchSubmitControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApVoucherBatchSubmitControllerEJB", ApVoucherBatchSubmitControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApVoucherBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbVBS = homeVBS.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApVoucherBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call ApVoucherBatchSubmitController EJB
     getGlFcPrecisionUnit
  *******************************************************/

         short precisionUnit = 0;
         boolean enableVoucherBatch = false; 
         boolean useSupplierPulldown = true;
         
         try { 
         	
            precisionUnit = ejbVBS.getGlFcPrecisionUnit(user.getCmpCode());
            enableVoucherBatch = Common.convertByteToBoolean(ejbVBS.getAdPrfEnableApVoucherBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableVoucherBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbVBS.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApVoucherBatchSubmitAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ap VBS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apVoucherBatchSubmit"));
	     
/*******************************************************
   -- Ap VBS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apVoucherBatchSubmit"));         

/*******************************************************
   -- Ap VBS Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ap VBS Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Ap VBS Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();  
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
                
	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	        	
	        	if (actionForm.getDebitMemo()) {	        	
		        	   		        		
	        		criteria.put("debitMemo", new Byte((byte)1));
                
                }
                
                if (!actionForm.getDebitMemo()) {
                	
                	criteria.put("debitMemo", new Byte((byte)0));
                	
                }
                	 
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbVBS.getApVouByCriteria(actionForm.getCriteria(),                	    
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ApVoucherBatchSubmitAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	     	}
            
            try {
            	
            	actionForm.clearApVBSList();
            	
            	ArrayList list = ejbVBS.getApVouByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModVoucherDetails mdetails = (ApModVoucherDetails)i.next();
            		
            		ApVoucherBatchSubmitList apVBSList = new ApVoucherBatchSubmitList(actionForm,
            		    mdetails.getVouCode(),
            		    mdetails.getVouSplSupplierCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getVouDate()),
                        mdetails.getVouDocumentNumber(),
                        mdetails.getVouReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),
						mdetails.getVouType());
            		    
            		actionForm.saveApVBSList(apVBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("voucherBatchSubmit.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apVoucherBatchSubmit");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("apVoucherBatchSubmit")); 

/*******************************************************
   -- Ap VBS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap VBS Submit Action --
*******************************************************/

         } else if (request.getParameter("submitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get posted journals
                        
		    for(int i=0; i<actionForm.getApVBSListSize(); i++) {
		    
		       ApVoucherBatchSubmitList actionList = actionForm.getApVBSByIndex(i);
		    	
               if (actionList.getSubmit()) {
               	
               	     try {

               	     	ejbVBS.executeApVouBatchSubmit(actionList.getVoucherCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteApVBSList(i);
               	     	i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherBatchSubmit.error.recordAlreadyDeleted", actionList.getDocumentNumber()));
		             
		             } catch (GlobalTransactionAlreadyApprovedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherBatchSubmit.error.transactionAlreadyApproved", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPendingException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherBatchSubmit.error.transactionAlreadyPending", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherBatchSubmit.error.transactionAlreadyPosted", actionList.getDocumentNumber()));
		             
		             } catch (GlobalNoApprovalRequesterFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherBatchSubmit.error.noApprovalRequesterFound", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalNoApprovalApproverFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherBatchSubmit.error.noApprovalApproverFound", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalTransactionAlreadyVoidException ex) {
           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("voucherBatchSubmit.error.transactionAlreadyVoid", actionList.getDocumentNumber()));
		               
		             } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("voucherBatchSubmit.error.effectiveDateNoPeriodExist", actionList.getDocumentNumber()));
		                    
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("voucherBatchSubmit.error.effectiveDatePeriodClosed", actionList.getDocumentNumber()));
		                    
		             } catch (GlobalJournalNotBalanceException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("voucherBatchSubmit.error.journalNotBalance", actionList.getDocumentNumber()));
		           	   
		             } catch (GlobalInventoryDateException ex) {
	                       	
                   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   			new ActionMessage("voucherBatchSubmit.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));                     	     	

		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		            	
		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("voucherBatchSubmit.error.noNegativeInventoryCostingCOA"));

		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ApVoucherBatchSubmitAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }	
		    
		    if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apVoucherBatchSubmit");

            }
	        
	        try {
	        	
	        	actionForm.setLineCount(0);
            	
            	actionForm.clearApVBSList();
            	
            	ArrayList list = ejbVBS.getApVouByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModVoucherDetails mdetails = (ApModVoucherDetails)i.next();
            		
            		ApVoucherBatchSubmitList apVBSList = new ApVoucherBatchSubmitList(actionForm,
            		    mdetails.getVouCode(),
            		    mdetails.getVouSplSupplierCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getVouDate()),
                        mdetails.getVouDocumentNumber(),
                        mdetails.getVouReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),
						mdetails.getVouType());
            		    
            		actionForm.saveApVBSList(apVBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }                                 
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("apVoucherBatchSubmit")); 
	        	     

/*******************************************************
   -- Ap VBS Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apVoucherBatchSubmit");

            }
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseSupplierPulldown()) {

            		actionForm.clearSupplierCodeList();           	
	            	
	            	list = ejbVBS.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierCodeList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
            	}

            	actionForm.clearCurrencyList();           	
            	
            	list = ejbVBS.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbVBS.getApOpenVbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearApVBSList();
                        	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("apVoucherBatchSubmit"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApVoucherBatchSubmitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}