package com.struts.ap.voucherbatchsubmit;

import java.io.Serializable;

public class ApVoucherBatchSubmitList implements Serializable {

   private Integer voucherCode = null;
   private String supplierCode = null;
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;  
   private String amountDue = null;
   private String type = null;
   
   private boolean submit = false;
       
   private ApVoucherBatchSubmitForm parentBean;
    
   public ApVoucherBatchSubmitList(ApVoucherBatchSubmitForm parentBean,
      Integer voucherCode,
      String supplierCode,
      String date,
      String documentNumber,
      String referenceNumber,
      String amountDue,
	  String type) {

      this.parentBean = parentBean;
      this.voucherCode = voucherCode;
      this.supplierCode = supplierCode;
      this.date = date;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amountDue = amountDue;
      this.type = type;
      
   }

   public Integer getVoucherCode() {

      return voucherCode;

   }

   public String getSupplierCode() {

      return supplierCode;
      
   }

   public String getDate() {
   
      return date;
      
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmountDue() {
   
      return amountDue;
   
   }
   
   public String getType() {
    
       return type;
    
    }

   public boolean getSubmit() {
   	
   	  return submit;
   	
   }
   
   public void setSubmit(boolean submit) {
   	
   	  this.submit = submit;
   	
   }
      
   
}