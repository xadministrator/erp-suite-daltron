package com.struts.ap.voucherentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalReferenceNumberNotUniqueException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApVoucherEntryController;
import com.ejb.txn.ApVoucherEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ApModDistributionRecordDetails;
import com.util.ApModPurchaseOrderLineDetails;
import com.util.ApModSupplierDetails;
import com.util.ApModVoucherDetails;
import com.util.ApModVoucherLineItemDetails;
import com.util.ApTaxCodeDetails;
import com.util.ApVoucherDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ApVoucherEntryAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApVoucherEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ApVoucherEntryForm actionForm = (ApVoucherEntryForm)form;

	      // reset report

	     actionForm.setReport(null);
	     actionForm.setAttachment(null);
	     actionForm.setAttachmentPDF(null);

         String frParam = null;

         if (request.getParameter("child") == null) {

         	frParam = Common.getUserPermission(user, Constants.AP_VOUCHER_ENTRY_ID);

         } else {

         	frParam = Constants.FULL_ACCESS;

         }

         if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("apVoucherEntry"));

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApVoucherEntryController EJB
*******************************************************/

         ApVoucherEntryControllerHome homeVOU = null;
         ApVoucherEntryController ejbVOU = null;

         GlDailyRateControllerHome homeFR = null;
         GlDailyRateController ejbFR = null;
         
         InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;

         try {

            homeVOU = (ApVoucherEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApVoucherEntryControllerEJB", ApVoucherEntryControllerHome.class);

            homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
                    
            homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);


         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ApVoucherEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {

            ejbVOU = homeVOU.create();

            ejbFR = homeFR.create();
            
              ejbII = homeII.create();

         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in ApVoucherEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ApVoucherEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

         short precisionUnit = 0;
         short journalLineNumber = 0;
         boolean enableVoucherBatch = false;
         boolean isInitialPrinting = false;
         boolean useSupplierPulldown = true;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ap/Voucher/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

         try {

            precisionUnit = ejbVOU.getGlFcPrecisionUnit(user.getCmpCode());


        	journalLineNumber = ejbVOU.getAdPrfApJournalLineNumber(user.getCmpCode());
            enableVoucherBatch = Common.convertByteToBoolean(ejbVOU.getAdPrfEnableApVoucherBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableVoucherBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbVOU.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            actionForm.setPrecisionUnit(precisionUnit);



         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

/*******************************************************
   -- Ap VOU Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("EXPENSES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApVoucherDetails details = new ApVoucherDetails();

           details.setVouType("EXPENSES");
           details.setVouCode(actionForm.getVoucherCode());
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouReferenceNumber(actionForm.getReferenceNumber());
           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
           details.setVouAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
           details.setVouDescription(actionForm.getDescription());
           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setVouPoNumber(actionForm.getPoNumber());

           details.setVouLoan(Common.convertBooleanToByte(actionForm.getLoan()));



           if (actionForm.getVoucherCode() == null) {

           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());

           }

           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());

           ArrayList drList = new ArrayList();
           int lineNumber = 0;

           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;

           for (int i = 0; i<actionForm.getApVOUListSize(); i++) {

           	   ApVoucherEntryList apVOUList = actionForm.getApVOUByIndex(i);

           	   if (Common.validateRequired(apVOUList.getAccount()) &&
      	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
      	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;

           	   byte isDebit = 0;
		       double amount = 0d;

		       if (!Common.validateRequired(apVOUList.getDebitAmount())) {

		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);

		          TOTAL_DEBIT += amount;

		       } else {

		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit);

		          TOTAL_CREDIT += amount;
		       }

		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apVOUList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());

           	   drList.add(mdetails);

           }

           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);

           if (TOTAL_DEBIT != TOTAL_CREDIT) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.voucherNotBalance"));

               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apVoucherEntry"));

           }

           // validate attachment

           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

           	}

           	if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

           	}

           	if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   	}


           try {

           	    Integer voucherCode = ejbVOU.saveApVouEntry(details, actionForm.getPaymentTerm(), actionForm.getPaymentTerm2(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setVoucherCode(voucherCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

           } catch (GlobalReferenceNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));


           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apVoucherEntry"));

	       }

           // save attachment

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }


/*******************************************************
   -- Ap VOU Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("EXPENSES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApVoucherDetails details = new ApVoucherDetails();

           details.setVouType("EXPENSES");
           details.setVouCode(actionForm.getVoucherCode());
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouReferenceNumber(actionForm.getReferenceNumber());
           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
           details.setVouAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
           details.setVouDescription(actionForm.getDescription());
           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setVouPoNumber(actionForm.getPoNumber());

           details.setVouLoan(Common.convertBooleanToByte(actionForm.getLoan()));

           if (actionForm.getVoucherCode() == null) {

           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());

           }

           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());

           ArrayList drList = new ArrayList();
           int lineNumber = 0;

           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;

           for (int i = 0; i<actionForm.getApVOUListSize(); i++) {

           	   ApVoucherEntryList apVOUList = actionForm.getApVOUByIndex(i);

           	   if (Common.validateRequired(apVOUList.getAccount()) &&
      	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
      	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;

           	   byte isDebit = 0;
		       double amount = 0d;

		       if (!Common.validateRequired(apVOUList.getDebitAmount())) {

		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);

		          TOTAL_DEBIT += amount;

		       } else {

		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit);

		          TOTAL_CREDIT += amount;
		       }

		       lineNumber++;

           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(apVOUList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());

           	   drList.add(mdetails);

           }

           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);

           if (TOTAL_DEBIT != TOTAL_CREDIT) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.voucherNotBalance"));

               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("apVoucherEntry"));

           }

           // validate attachment

           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

          	}

          	if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

          	}

          	if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   	}


           try {

           	    Integer voucherCode = ejbVOU.saveApVouEntry(details, actionForm.getPaymentTerm(), actionForm.getPaymentTerm2(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        drList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setVoucherCode(voucherCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

           } catch (GlobalReferenceNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apVoucherEntry"));

	       }

// save attachment

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

/*******************************************************
	 -- Ap VOU Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("EXPENSES")) {


         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApVoucherDetails details = new ApVoucherDetails();

	           details.setVouType("EXPENSES");
	           details.setVouCode(actionForm.getVoucherCode());
	           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setVouDocumentNumber(actionForm.getDocumentNumber());
	           details.setVouReferenceNumber(actionForm.getReferenceNumber());
	           details.setVouBillAmount(Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit));
	           details.setVouAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmountDue(), precisionUnit));
	           details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
	           details.setVouDescription(actionForm.getDescription());
	           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	           details.setVouPoNumber(actionForm.getPoNumber());

	           details.setVouLoan(Common.convertBooleanToByte(actionForm.getLoan()));

	           if (actionForm.getVoucherCode() == null) {

	           	   details.setVouCreatedBy(user.getUserName());
		           details.setVouDateCreated(new java.util.Date());

	           }

	           details.setVouLastModifiedBy(user.getUserName());
	           details.setVouDateLastModified(new java.util.Date());

	           ArrayList drList = new ArrayList();
	           int lineNumber = 0;

	           double TOTAL_DEBIT = 0;
	           double TOTAL_CREDIT = 0;


	           for (int i = 0; i<actionForm.getApVOUListSize(); i++) {

	           	   ApVoucherEntryList apVOUList = actionForm.getApVOUByIndex(i);

	           	   if (Common.validateRequired(apVOUList.getAccount()) &&
	      	 	       Common.validateRequired(apVOUList.getDebitAmount()) &&
	      	 	       Common.validateRequired(apVOUList.getCreditAmount())) continue;

	           	   byte isDebit = 0;
			       double amount = 0d;

			       if (!Common.validateRequired(apVOUList.getDebitAmount())) {

			          isDebit = 1;
			          amount = Common.convertStringMoneyToDouble(apVOUList.getDebitAmount(), precisionUnit);

			          TOTAL_DEBIT += amount;

			       } else {

			          isDebit = 0;
			          amount = Common.convertStringMoneyToDouble(apVOUList.getCreditAmount(), precisionUnit);

			          TOTAL_CREDIT += amount;
			       }

			       lineNumber++;

	           	   ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

	           	   mdetails.setDrLine((short)(lineNumber));
	           	   mdetails.setDrClass(apVOUList.getDrClass());
	           	   mdetails.setDrDebit(isDebit);
	           	   mdetails.setDrAmount(amount);
	           	   mdetails.setDrCoaAccountNumber(apVOUList.getAccount());

	           	   drList.add(mdetails);

	           }

	           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
	           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);

	           if (TOTAL_DEBIT != TOTAL_CREDIT) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.voucherNotBalance"));

	               saveErrors(request, new ActionMessages(errors));
	               return (mapping.findForward("apVoucherEntry"));

	           }

	           // validate attachment

	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	           if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

	          	}

	          	if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

	          	}

	          	if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

		   	   	}


	           try {

	               Integer voucherCode = ejbVOU.saveApVouEntry(details, actionForm.getPaymentTerm(), actionForm.getPaymentTerm2(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
	           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	   actionForm.setVoucherCode(voucherCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.conversionDateNotExist"));

	           } catch (GlobalPaymentTermInvalidException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.paymentTermInvalid"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

	           } catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.journalNotBalance"));

	           } catch (GlobalBranchAccountNumberInvalidException ex) {

	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

	           } catch (GlobalReferenceNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {

		    	   saveErrors(request, new ActionMessages(errors));
		   		   return (mapping.findForward("apVoucherEntry"));

		       }

	        // save attachment

	           if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	           actionForm.setReport(Constants.STATUS_SUCCESS);

	           isInitialPrinting = true;

	        }  else {

	        	actionForm.setReport(Constants.STATUS_SUCCESS);

		        	return(mapping.findForward("apVoucherEntry"));

	        }

/*******************************************************
   -- Ap VLI Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 System.out.println("-----------------------saveAsDraftButton");
           ApVoucherDetails details = new ApVoucherDetails();

           details.setVouType("ITEMS");
           details.setVouCode(actionForm.getVoucherCode());
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouReferenceNumber(actionForm.getReferenceNumber());
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
           details.setVouDescription(actionForm.getDescription());
           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setVouPoNumber(actionForm.getPoNumber());

           details.setVouLoan(Common.convertBooleanToByte(actionForm.getLoan()));

           if (actionForm.getVoucherCode() == null) {

           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());

           }

           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());

           ArrayList vliList = new ArrayList();

           for (int i = 0; i<actionForm.getApVLIListSize(); i++) {

           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);

           	   if (Common.validateRequired(apVLIList.getLocation()) &&
           	   	   Common.validateRequired(apVLIList.getItemName()) &&
				   Common.validateRequired(apVLIList.getUnit()) &&
				   Common.validateRequired(apVLIList.getUnitCost()) &&
				   Common.validateRequired(apVLIList.getQuantity())) continue;

           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

           	   mdetails.setVliLine(Common.convertStringToShort(apVLIList.getLineNumber()));
           	   mdetails.setVliIiName(apVLIList.getItemName());
           	   mdetails.setVliLocName(apVLIList.getLocation());
           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apVLIList.getQuantity(), precisionUnit));
           	   mdetails.setVliUomName(apVLIList.getUnit());
           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), precisionUnit));
           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apVLIList.getAmount(), precisionUnit));
           	   mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apVLIList.getDiscount1(), precisionUnit));
           	   mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apVLIList.getDiscount2(), precisionUnit));
           	   mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apVLIList.getDiscount3(), precisionUnit));
           	   mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apVLIList.getDiscount4(), precisionUnit));
           	   mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apVLIList.getTotalDiscount(), precisionUnit));
           	   mdetails.setVliMisc(apVLIList.getMisc());

           	   mdetails.setVliTax(Common.convertBooleanToByte(apVLIList.getIsTax()));
           	   mdetails.setVliIsVatRelief(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode())));
           	   mdetails.setVliIsProject(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode())));


           	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

           		   mdetails.setVliSplName(apVLIList.getSupplierName());
           		   mdetails.setVliSplTin(apVLIList.getSupplierTin());
           		   mdetails.setVliSplAddress(apVLIList.getSupplierAddress());

           	   }
           	   System.out.println("------------------------isVatRelief="+ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()));
           	   System.out.println("------------------------isProject="+ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()));

           	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

           		   mdetails.setVliProjectCode(apVLIList.getProjectCode());
           		   mdetails.setVliProjectTypeCode(apVLIList.getProjectTypeCode().equals("<TYPE>") ? null: apVLIList.getProjectTypeCode());
           		   mdetails.setVliProjectPhaseName(apVLIList.getProjectPhaseName().equals("<PHASE>") ? null: apVLIList.getProjectPhaseName());

	           	}



           	   boolean isTraceMisc = ejbVOU.getInvTraceMisc(apVLIList.getItemName(), user.getCmpCode());

			   String misc=apVLIList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setIsTraceMisc(true);
	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
	     		   mdetails.setTagList(tagList);
	     	   }

           	   vliList.add(mdetails);

           }

           // validate attachment

           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

         	}

         	if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

         	}

         	if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   	}


           try {

           	    Integer voucherCode = ejbVOU.saveApVouVliEntry(details, actionForm.getPaymentTerm(), actionForm.getPaymentTerm2(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        vliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setVoucherCode(voucherCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.journalNotBalance"));

           } catch (GlobalInventoryDateException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
              	    new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

           } catch (GlobalReferenceNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("voucherEntry.error.noNegativeInventoryCostingCOA"));

           }catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apVoucherEntry"));

	       }

// save attachment

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

/*******************************************************
   -- Ap VLI Save & Submit Action --
*******************************************************/

	} else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ApVoucherDetails details = new ApVoucherDetails();

           details.setVouType("ITEMS");
           details.setVouCode(actionForm.getVoucherCode());
           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setVouDocumentNumber(actionForm.getDocumentNumber());
           details.setVouReferenceNumber(actionForm.getReferenceNumber());
           details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
           details.setVouDescription(actionForm.getDescription());
           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
           details.setVouPoNumber(actionForm.getPoNumber());

           details.setVouLoan(Common.convertBooleanToByte(actionForm.getLoan()));

           if (actionForm.getVoucherCode() == null) {

           	   details.setVouCreatedBy(user.getUserName());
	           details.setVouDateCreated(new java.util.Date());

           }

           details.setVouLastModifiedBy(user.getUserName());
           details.setVouDateLastModified(new java.util.Date());

           ArrayList vliList = new ArrayList();

           for (int i = 0; i<actionForm.getApVLIListSize(); i++) {

           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);

           	   if (Common.validateRequired(apVLIList.getLocation()) &&
           	   	   Common.validateRequired(apVLIList.getItemName()) &&
				   Common.validateRequired(apVLIList.getUnit()) &&
				   Common.validateRequired(apVLIList.getUnitCost()) &&
				   Common.validateRequired(apVLIList.getQuantity())) continue;

           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

           	   mdetails.setVliLine(Common.convertStringToShort(apVLIList.getLineNumber()));
           	   mdetails.setVliIiName(apVLIList.getItemName());
           	   mdetails.setVliLocName(apVLIList.getLocation());
           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apVLIList.getQuantity(), precisionUnit));
           	   mdetails.setVliUomName(apVLIList.getUnit());
           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), precisionUnit));
           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apVLIList.getAmount(), precisionUnit));
           	   mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apVLIList.getDiscount1(), precisionUnit));
           	   mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apVLIList.getDiscount2(), precisionUnit));
           	   mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apVLIList.getDiscount3(), precisionUnit));
           	   mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apVLIList.getDiscount4(), precisionUnit));
           	   mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apVLIList.getTotalDiscount(), precisionUnit));
           	   mdetails.setVliMisc(apVLIList.getMisc());

           	   mdetails.setVliTax(Common.convertBooleanToByte(apVLIList.getIsTax()));
        	   mdetails.setVliIsVatRelief(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode())));
        	   mdetails.setVliIsProject(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode())));


        	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

        		   mdetails.setVliSplName(apVLIList.getSupplierName());
        		   mdetails.setVliSplTin(apVLIList.getSupplierTin());
        		   mdetails.setVliSplAddress(apVLIList.getSupplierAddress());

        	   }
        	   System.out.println("------------------------isVatRelief="+ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()));
        	   System.out.println("------------------------isProject="+ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()));

        	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

        		   mdetails.setVliProjectCode(apVLIList.getProjectCode());
        		   mdetails.setVliProjectTypeCode(apVLIList.getProjectTypeCode().equals("<TYPE>") ? null: apVLIList.getProjectTypeCode());
        		   mdetails.setVliProjectPhaseName(apVLIList.getProjectPhaseName().equals("<PHASE>") ? null: apVLIList.getProjectPhaseName());

	           	}

        	   boolean isTraceMisc = ejbVOU.getInvTraceMisc(apVLIList.getItemName(), user.getCmpCode());

			   String misc=apVLIList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setIsTraceMisc(true);
	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
	     		   mdetails.setTagList(tagList);
	     	   }

           	   vliList.add(mdetails);

           }

           // validate attachment

           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

         	}

         	if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

         	}

         	if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   	}


           try {

           	    Integer voucherCode = ejbVOU.saveApVouVliEntry(details, actionForm.getPaymentTerm(), actionForm.getPaymentTerm2(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
           	        vliList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setVoucherCode(voucherCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.conversionDateNotExist"));

           } catch (GlobalPaymentTermInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.paymentTermInvalid"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.journalNotBalance"));

           } catch (GlobalInventoryDateException ex) {

      		   errors.add(ActionMessages.GLOBAL_MESSAGE,
      			    new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

           } catch (GlobalReferenceNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

           }catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("voucherEntry.error.noNegativeInventoryCostingCOA"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

           if (!errors.isEmpty()) {

	    	   saveErrors(request, new ActionMessages(errors));
	   		   return (mapping.findForward("apVoucherEntry"));

	       }

// save attachment

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

/*******************************************************
	 -- Ap VLI Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("ITEMS")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApVoucherDetails details = new ApVoucherDetails();

	           details.setVouType("ITEMS");
	           details.setVouCode(actionForm.getVoucherCode());
	           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setVouDocumentNumber(actionForm.getDocumentNumber());
	           details.setVouReferenceNumber(actionForm.getReferenceNumber());
	           details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
	           details.setVouDescription(actionForm.getDescription());
	           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	           details.setVouPoNumber(actionForm.getPoNumber());

	           details.setVouLoan(Common.convertBooleanToByte(actionForm.getLoan()));

	           if (actionForm.getVoucherCode() == null) {

	           	   details.setVouCreatedBy(user.getUserName());
		           details.setVouDateCreated(new java.util.Date());

	           }

	           details.setVouLastModifiedBy(user.getUserName());
	           details.setVouDateLastModified(new java.util.Date());

	           ArrayList vliList = new ArrayList();

	           for (int i = 0; i<actionForm.getApVLIListSize(); i++) {

           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);

           	   if (Common.validateRequired(apVLIList.getLocation()) &&
           	   	   Common.validateRequired(apVLIList.getItemName()) &&
				   Common.validateRequired(apVLIList.getUnit()) &&
				   Common.validateRequired(apVLIList.getUnitCost()) &&
				   Common.validateRequired(apVLIList.getQuantity())) continue;

           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

           	   mdetails.setVliLine(Common.convertStringToShort(apVLIList.getLineNumber()));
           	   mdetails.setVliIiName(apVLIList.getItemName());
           	   mdetails.setVliLocName(apVLIList.getLocation());
           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apVLIList.getQuantity(), precisionUnit));
           	   mdetails.setVliUomName(apVLIList.getUnit());
           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), precisionUnit));
           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apVLIList.getAmount(), precisionUnit));
           	   mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apVLIList.getDiscount1(), precisionUnit));
           	   mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apVLIList.getDiscount2(), precisionUnit));
           	   mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apVLIList.getDiscount3(), precisionUnit));
           	   mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apVLIList.getDiscount4(), precisionUnit));
           	   mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apVLIList.getTotalDiscount(), precisionUnit));
           	   mdetails.setVliMisc(apVLIList.getMisc());

           	   mdetails.setVliTax(Common.convertBooleanToByte(apVLIList.getIsTax()));
        	   mdetails.setVliIsVatRelief(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode())));
        	   mdetails.setVliIsProject(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode())));


        	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

        		   mdetails.setVliSplName(apVLIList.getSupplierName());
        		   mdetails.setVliSplTin(apVLIList.getSupplierTin());
        		   mdetails.setVliSplAddress(apVLIList.getSupplierAddress());

        	   }
        	   System.out.println("------------------------isVatRelief="+ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()));
        	   System.out.println("------------------------isProject="+ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()));

        	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

        		   mdetails.setVliProjectCode(apVLIList.getProjectCode());
        		   mdetails.setVliProjectTypeCode(apVLIList.getProjectTypeCode().equals("<TYPE>") ? null: apVLIList.getProjectTypeCode());
        		   mdetails.setVliProjectPhaseName(apVLIList.getProjectPhaseName().equals("<PHASE>") ? null: apVLIList.getProjectPhaseName());

	           	}


        	   boolean isTraceMisc = ejbVOU.getInvTraceMisc(apVLIList.getItemName(), user.getCmpCode());

			   String misc=apVLIList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setIsTraceMisc(true);
	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
	     		   mdetails.setTagList(tagList);
	     	   }


           	   vliList.add(mdetails);

               }

	           // validate attachment

	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	           if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

	          	}

	          	if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

	          	}

	          	if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("voucherEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("apVoucherEntry"));

		   	    	}

		   	   	}


	           try {

	               Integer voucherCode = ejbVOU.saveApVouVliEntry(details, actionForm.getPaymentTerm(), actionForm.getPaymentTerm2(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
	           	        vliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	   actionForm.setVoucherCode(voucherCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.conversionDateNotExist"));

	           } catch (GlobalPaymentTermInvalidException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.paymentTermInvalid"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

	           } catch (GlobalInvItemLocationNotFoundException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.noItemLocationFound", ex.getMessage()));

           	   } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

               } catch (GlJREffectiveDatePeriodClosedException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

               } catch (GlobalJournalNotBalanceException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.journalNotBalance"));

               } catch (GlobalInventoryDateException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

               } catch (GlobalBranchAccountNumberInvalidException ex) {

	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

               } catch (GlobalReferenceNumberNotUniqueException ex) {

               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("voucherEntry.error.noNegativeInventoryCostingCOA"));

	           }catch (GlobalMiscInfoIsRequiredException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

               } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {

		    	   saveErrors(request, new ActionMessages(errors));
		   		   return (mapping.findForward("apVoucherEntry"));

		       }

	        // save attachment

	           if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	           actionForm.setReport(Constants.STATUS_SUCCESS);

	           isInitialPrinting = true;

	        }  else {

	        	actionForm.setReport(Constants.STATUS_SUCCESS);

		        	return(mapping.findForward("apVoucherEntry"));

	        }

/*******************************************************
   -- Ap VLI Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("ITEMS")) {

           if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ApVoucherDetails details = new ApVoucherDetails();

	           details.setVouType("ITEMS");
	           details.setVouCode(actionForm.getVoucherCode());
	           details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setVouDocumentNumber(actionForm.getDocumentNumber());
	           details.setVouReferenceNumber(actionForm.getReferenceNumber());
	           details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
	           details.setVouDescription(actionForm.getDescription());
	           details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
	           details.setVouPoNumber(actionForm.getPoNumber());

	           details.setVouLoan(Common.convertBooleanToByte(actionForm.getLoan()));

	           if (actionForm.getVoucherCode() == null) {

	           	   details.setVouCreatedBy(user.getUserName());
		           details.setVouDateCreated(new java.util.Date());

	           }

	           details.setVouLastModifiedBy(user.getUserName());
	           details.setVouDateLastModified(new java.util.Date());

	           ArrayList vliList = new ArrayList();

	           for (int i = 0; i<actionForm.getApVLIListSize(); i++) {

           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);

           	   if (Common.validateRequired(apVLIList.getLocation()) &&
           	   	   Common.validateRequired(apVLIList.getItemName()) &&
				   Common.validateRequired(apVLIList.getUnit()) &&
				   Common.validateRequired(apVLIList.getUnitCost()) &&
				   Common.validateRequired(apVLIList.getQuantity())) continue;

           	   ApModVoucherLineItemDetails mdetails = new ApModVoucherLineItemDetails();

           	   mdetails.setVliLine(Common.convertStringToShort(apVLIList.getLineNumber()));
           	   mdetails.setVliIiName(apVLIList.getItemName());
           	   mdetails.setVliLocName(apVLIList.getLocation());
           	   mdetails.setVliQuantity(Common.convertStringMoneyToDouble(apVLIList.getQuantity(), precisionUnit));
           	   mdetails.setVliUomName(apVLIList.getUnit());
           	   mdetails.setVliUnitCost(Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), precisionUnit));
           	   mdetails.setVliAmount(Common.convertStringMoneyToDouble(apVLIList.getAmount(), precisionUnit));
           	   mdetails.setVliDiscount1(Common.convertStringMoneyToDouble(apVLIList.getDiscount1(), precisionUnit));
           	   mdetails.setVliDiscount2(Common.convertStringMoneyToDouble(apVLIList.getDiscount2(), precisionUnit));
           	   mdetails.setVliDiscount3(Common.convertStringMoneyToDouble(apVLIList.getDiscount3(), precisionUnit));
           	   mdetails.setVliDiscount4(Common.convertStringMoneyToDouble(apVLIList.getDiscount4(), precisionUnit));
           	   mdetails.setVliTotalDiscount(Common.convertStringMoneyToDouble(apVLIList.getTotalDiscount(), precisionUnit));
           	   mdetails.setVliMisc(apVLIList.getMisc());

           	   mdetails.setVliTax(Common.convertBooleanToByte(apVLIList.getIsTax()));
        	   mdetails.setVliIsVatRelief(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode())));
        	   mdetails.setVliIsProject(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode())));


        	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

        		   mdetails.setVliSplName(apVLIList.getSupplierName());
        		   mdetails.setVliSplTin(apVLIList.getSupplierTin());
        		   mdetails.setVliSplAddress(apVLIList.getSupplierAddress());

        	   }
        	   System.out.println("------------------------isVatRelief="+ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()));
        	   System.out.println("------------------------isProject="+ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()));

        	   if(Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()))) {

        		   mdetails.setVliProjectCode(apVLIList.getProjectCode());
        		   mdetails.setVliProjectTypeCode(apVLIList.getProjectTypeCode().equals("<TYPE>") ? null: apVLIList.getProjectTypeCode());
        		   mdetails.setVliProjectPhaseName(apVLIList.getProjectPhaseName().equals("<PHASE>") ? null: apVLIList.getProjectPhaseName());

	           	}

        	   boolean isTraceMisc = ejbVOU.getInvTraceMisc(apVLIList.getItemName(), user.getCmpCode());

			   String misc=apVLIList.getMisc();



	     	   if (isTraceMisc){
	     		   mdetails.setIsTraceMisc(true);
	     		   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
	     		   mdetails.setTagList(tagList);
	     	   }


           	   vliList.add(mdetails);

               }

	           try {

	               Integer voucherCode = ejbVOU.saveApVouVliEntry(details, actionForm.getPaymentTerm(), actionForm.getPaymentTerm2(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
	           	        vliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	   actionForm.setVoucherCode(voucherCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.conversionDateNotExist"));

	           } catch (GlobalPaymentTermInvalidException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.paymentTermInvalid"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

	           } catch (GlobalInvItemLocationNotFoundException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.noItemLocationFound", ex.getMessage()));

           	   } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

               } catch (GlJREffectiveDatePeriodClosedException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

               } catch (GlobalJournalNotBalanceException ex) {

           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.journalNotBalance"));

               } catch (GlobalInventoryDateException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

               } catch (GlobalBranchAccountNumberInvalidException ex) {

	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

               } catch (GlobalReferenceNumberNotUniqueException ex) {

               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("voucherEntry.error.noNegativeInventoryCostingCOA"));

	           }catch (GlobalMiscInfoIsRequiredException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

               } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {

		    	   saveErrors(request, new ActionMessages(errors));
		   		   return (mapping.findForward("apVoucherEntry"));

		       }



       	   }

       	   String path = "/apJournal.do?forward=1" +
		     "&transactionCode=" + actionForm.getVoucherCode() +
		     "&transaction=VOUCHER" +
		     "&transactionNumber=" + actionForm.getReferenceNumber() +
		     "&transactionDate=" + actionForm.getDate() +
		     "&transactionEnableFields=" + actionForm.getEnableFields();

            return(new ActionForward(path));

/*******************************************************
    -- Ap VPO Save As Draft Action --
*******************************************************/

          } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("PO MATCHED") &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ApVoucherDetails details = new ApVoucherDetails();

            details.setVouType("PO MATCHED");
            details.setVouCode(actionForm.getVoucherCode());
            details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setVouDocumentNumber(actionForm.getDocumentNumber());
            details.setVouReferenceNumber(actionForm.getReferenceNumber());
            details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
            details.setVouDescription(actionForm.getDescription());
            details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
            details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
            details.setVouPoNumber(actionForm.getPoNumber());

            if (actionForm.getVoucherCode() == null) {

            	   details.setVouCreatedBy(user.getUserName());
 	           details.setVouDateCreated(new java.util.Date());

            }

            details.setVouLastModifiedBy(user.getUserName());
            details.setVouDateLastModified(new java.util.Date());

            ArrayList plList = new ArrayList();

            for (int i = 0; i<actionForm.getApVPOListSize(); i++) {

            	   ApVoucherPurchaseOrderLineList apVPOList = actionForm.getApVPOByIndex(i);

            	   if(!apVPOList.getIssueCheckbox()) continue;


            	   boolean isTraceMisc = ejbVOU.getInvTraceMisc(apVPOList.getItemName(), user.getCmpCode());

    			   String misc=apVPOList.getMisc();



    	     	   if (isTraceMisc){

    	     		   apVPOList.setIsTraceMisc(true);


    	     	//	   mdetails.setIsTraceMisc(true);
    	     	//	   ArrayList tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "N/A");
    	     	//	   mdetails.setTagList(tagList);
    	     	   }

            	   plList.add(apVPOList.getVoucherPurchaseOrderLineCode());


            }

            // validate attachment

            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

          	}

          	if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

          	}

          	if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("voucherEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("apVoucherEntry"));

	   	    	}

	   	   	}


            try {

	            	Integer voucherCode = ejbVOU.saveApVouPlEntry(details, actionForm.getPaymentTerm(),
            	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
            	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
            	        plList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	    actionForm.setVoucherCode(voucherCode);

            } catch (GlobalRecordAlreadyDeletedException ex) {

            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

            } catch (GlobalDocumentNumberNotUniqueException ex) {

            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

            } catch (GlobalConversionDateNotExistException ex) {

            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.conversionDateNotExist"));

            } catch (GlobalPaymentTermInvalidException ex) {

            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.paymentTermInvalid"));

            } catch (GlobalTransactionAlreadyApprovedException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

            } catch (GlobalTransactionAlreadyPendingException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

            } catch (GlobalTransactionAlreadyPostedException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

            } catch (GlobalTransactionAlreadyVoidException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

            } catch (GlobalNoApprovalRequesterFoundException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

            } catch (GlobalNoApprovalApproverFoundException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

            } catch (GlJREffectiveDateNoPeriodExistException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

            } catch (GlJREffectiveDatePeriodClosedException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

            } catch (GlobalJournalNotBalanceException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.journalNotBalance"));

            } catch (GlobalInventoryDateException ex) {

	      	   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	      			 new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

            } catch (GlobalBranchAccountNumberInvalidException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

            } catch (GlobalReferenceNumberNotUniqueException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

            } catch (EJBException ex) {
            	if (log.isInfoEnabled()) {

            		log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
            				" session: " + session.getId());
            	}

            	return(mapping.findForward("cmnErrorPage"));
            }

            if (!errors.isEmpty()) {

 	    	   saveErrors(request, new ActionMessages(errors));
 	   		   return (mapping.findForward("apVoucherEntry"));

 	       }

         // save attachment

            if (!Common.validateRequired(filename1)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename1().getInputStream();

        	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

        	    		int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename2)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename2().getInputStream();

        	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename3)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename3().getInputStream();

        	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

        	   if (!Common.validateRequired(filename4)) {

        	        if (errors.isEmpty()) {

        	        	InputStream is = actionForm.getFilename4().getInputStream();

        	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

        	        	new File(attachmentPath).mkdirs();

        	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

        	    			fileExtension = attachmentFileExtension;

            	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
            	    		fileExtension = attachmentFileExtensionPDF;

            	    	}
        	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

 	            	int c;

 	            	while ((c = is.read()) != -1) {

 	            		fos.write((byte)c);

 	            	}

 	            	is.close();
 					fos.close();

        	        }

        	   }

 /*******************************************************
    -- Ap VPO Save & Submit Action --
 *******************************************************/

          } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("PO MATCHED") &&
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                ApVoucherDetails details = new ApVoucherDetails();

                details.setVouType("PO MATCHED");
                details.setVouCode(actionForm.getVoucherCode());
                details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
                details.setVouDocumentNumber(actionForm.getDocumentNumber());
                details.setVouReferenceNumber(actionForm.getReferenceNumber());
                details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
                details.setVouDescription(actionForm.getDescription());
                details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
                details.setVouPoNumber(actionForm.getPoNumber());

                if (actionForm.getVoucherCode() == null) {

                	   details.setVouCreatedBy(user.getUserName());
     	           details.setVouDateCreated(new java.util.Date());

                }

                details.setVouLastModifiedBy(user.getUserName());
                details.setVouDateLastModified(new java.util.Date());

                ArrayList plList = new ArrayList();

                for (int i = 0; i<actionForm.getApVPOListSize(); i++) {

                	   ApVoucherPurchaseOrderLineList apVPOList = actionForm.getApVPOByIndex(i);

                	   if(!apVPOList.getIssueCheckbox()) continue;

                	   plList.add(apVPOList.getVoucherPurchaseOrderLineCode());

                }

                // validate attachment

                String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

    	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("voucherEntry.error.filename1NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

    	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename1().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apVoucherEntry"));

    	   	    	}

              	}

              	if (!Common.validateRequired(filename2)) {

    	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("voucherEntry.error.filename2NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename2().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apVoucherEntry"));

    	   	    	}

              	}

              	if (!Common.validateRequired(filename3)) {

    	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("voucherEntry.error.filename3NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename3().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apVoucherEntry"));

    	   	    	}

    	   	   }

    	   	   if (!Common.validateRequired(filename4)) {

    	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           			new ActionMessage("voucherEntry.error.filename4NotFound"));

    	   	    	} else {

    	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

    	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
    	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

    	          	    	}

    	          	    	InputStream is = actionForm.getFilename4().getInputStream();

    	          	    	if (is.available() > maxAttachmentFileSize) {

    	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

    	          	    	}

    	          	    	is.close();

    	   	    	}

    	   	    	if (!errors.isEmpty()) {

    	   	    		saveErrors(request, new ActionMessages(errors));
    	          			return (mapping.findForward("apVoucherEntry"));

    	   	    	}

    	   	   	}


                try {

                	    Integer voucherCode = ejbVOU.saveApVouPlEntry(details, actionForm.getPaymentTerm(),
                	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                	        actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
                	        plList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                	    actionForm.setVoucherCode(voucherCode);

                } catch (GlobalRecordAlreadyDeletedException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

                } catch (GlobalDocumentNumberNotUniqueException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

                } catch (GlobalConversionDateNotExistException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.conversionDateNotExist"));

                } catch (GlobalPaymentTermInvalidException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.paymentTermInvalid"));

                } catch (GlobalTransactionAlreadyApprovedException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

                } catch (GlobalTransactionAlreadyPendingException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

                } catch (GlobalTransactionAlreadyPostedException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

                } catch (GlobalTransactionAlreadyVoidException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

                } catch (GlobalNoApprovalRequesterFoundException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

                } catch (GlobalNoApprovalApproverFoundException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

                } catch (GlJREffectiveDateNoPeriodExistException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

                } catch (GlJREffectiveDatePeriodClosedException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

                } catch (GlobalJournalNotBalanceException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("voucherEntry.error.journalNotBalance"));

                } catch (GlobalInventoryDateException ex) {

               	       errors.add(ActionMessages.GLOBAL_MESSAGE,
              			 new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                } catch (GlobalBranchAccountNumberInvalidException ex) {

		          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

                } catch (GlobalReferenceNumberNotUniqueException ex) {

                	errors.add(ActionMessages.GLOBAL_MESSAGE,
                			new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

                } catch (EJBException ex) {
                	if (log.isInfoEnabled()) {

                		log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                				" session: " + session.getId());
                	}

                	return(mapping.findForward("cmnErrorPage"));
                }

                if (!errors.isEmpty()) {

                	saveErrors(request, new ActionMessages(errors));
                	return (mapping.findForward("apVoucherEntry"));

                }

             // save attachment

                if (!Common.validateRequired(filename1)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename1().getInputStream();

            	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

            	    		int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename2)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename2().getInputStream();

            	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename3)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename3().getInputStream();

            	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename4)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename4().getInputStream();

            	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }


/*******************************************************
    -- Ap VPO Print Action --
*******************************************************/
             } else if (request.getParameter("printButton") != null && actionForm.getType().equals("PO MATCHED")) {

             	if(Common.validateRequired(actionForm.getApprovalStatus())) {

             		ApVoucherDetails details = new ApVoucherDetails();

             		details.setVouType("PO MATCHED");
             		details.setVouCode(actionForm.getVoucherCode());
             		details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
             		details.setVouDocumentNumber(actionForm.getDocumentNumber());
                    details.setVouReferenceNumber(actionForm.getReferenceNumber());
             		details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
             		details.setVouDescription(actionForm.getDescription());
             		details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             		details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
             		details.setVouPoNumber(actionForm.getPoNumber());

             		if (actionForm.getVoucherCode() == null) {

             			details.setVouCreatedBy(user.getUserName());
             			details.setVouDateCreated(new java.util.Date());

             		}

             		details.setVouLastModifiedBy(user.getUserName());
             		details.setVouDateLastModified(new java.util.Date());

             		ArrayList plList = new ArrayList();

             		for (int i = 0; i<actionForm.getApVPOListSize(); i++) {

             			ApVoucherPurchaseOrderLineList apVPOList = actionForm.getApVPOByIndex(i);

             			if(!apVPOList.getIssueCheckbox()) continue;

             			plList.add(apVPOList.getVoucherPurchaseOrderLineCode());

             		}
             		// validate attachment

             		String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
             		String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
             		String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
             		String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

             		if (!Common.validateRequired(filename1)) {

        	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

        	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	           			new ActionMessage("voucherEntry.error.filename1NotFound"));

        	   	    	} else {

        	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

        	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
        	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename1Invalid"));

        	          	    	}

        	          	    	InputStream is = actionForm.getFilename1().getInputStream();

        	          	    	if (is.available() > maxAttachmentFileSize) {

        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename1SizeInvalid"));

        	          	    	}

        	          	    	is.close();

        	   	    	}

        	   	    	if (!errors.isEmpty()) {

        	   	    		saveErrors(request, new ActionMessages(errors));
        	          			return (mapping.findForward("apVoucherEntry"));

        	   	    	}

                  	}

                  	if (!Common.validateRequired(filename2)) {

        	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

        	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	           			new ActionMessage("voucherEntry.error.filename2NotFound"));

        	   	    	} else {

        	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

        	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
        	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename2Invalid"));

        	          	    	}

        	          	    	InputStream is = actionForm.getFilename2().getInputStream();

        	          	    	if (is.available() > maxAttachmentFileSize) {

        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename2SizeInvalid"));

        	          	    	}

        	          	    	is.close();

        	   	    	}

        	   	    	if (!errors.isEmpty()) {

        	   	    		saveErrors(request, new ActionMessages(errors));
        	          			return (mapping.findForward("apVoucherEntry"));

        	   	    	}

                  	}

                  	if (!Common.validateRequired(filename3)) {

        	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

        	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	           			new ActionMessage("voucherEntry.error.filename3NotFound"));

        	   	    	} else {

        	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

        	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
        	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename3Invalid"));

        	          	    	}

        	          	    	InputStream is = actionForm.getFilename3().getInputStream();

        	          	    	if (is.available() > maxAttachmentFileSize) {

        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename3SizeInvalid"));

        	          	    	}

        	          	    	is.close();

        	   	    	}

        	   	    	if (!errors.isEmpty()) {

        	   	    		saveErrors(request, new ActionMessages(errors));
        	          			return (mapping.findForward("apVoucherEntry"));

        	   	    	}

        	   	   }

        	   	   if (!Common.validateRequired(filename4)) {

        	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

        	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	           			new ActionMessage("voucherEntry.error.filename4NotFound"));

        	   	    	} else {

        	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

        	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
        	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename4Invalid"));

        	          	    	}

        	          	    	InputStream is = actionForm.getFilename4().getInputStream();

        	          	    	if (is.available() > maxAttachmentFileSize) {

        	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
        	                   		new ActionMessage("voucherEntry.error.filename4SizeInvalid"));

        	          	    	}

        	          	    	is.close();

        	   	    	}

        	   	    	if (!errors.isEmpty()) {

        	   	    		saveErrors(request, new ActionMessages(errors));
        	          			return (mapping.findForward("apVoucherEntry"));

        	   	    	}

        	   	   	}


             		try {

             			Integer voucherCode = ejbVOU.saveApVouPlEntry(details, actionForm.getPaymentTerm(),
             					actionForm.getTaxCode(), actionForm.getWithholdingTax(),
								actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
								plList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

             			actionForm.setVoucherCode(voucherCode);

             		} catch (GlobalRecordAlreadyDeletedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

             		} catch (GlobalDocumentNumberNotUniqueException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

             		} catch (GlobalConversionDateNotExistException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.conversionDateNotExist"));

             		} catch (GlobalPaymentTermInvalidException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.paymentTermInvalid"));

             		} catch (GlobalTransactionAlreadyApprovedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

             		} catch (GlobalTransactionAlreadyPendingException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

             		} catch (GlobalTransactionAlreadyPostedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

             		} catch (GlobalTransactionAlreadyVoidException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

             		} catch (GlobalNoApprovalRequesterFoundException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

             		} catch (GlobalNoApprovalApproverFoundException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

             		}  catch (GlJREffectiveDateNoPeriodExistException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

             		} catch (GlJREffectiveDatePeriodClosedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

             		} catch (GlobalJournalNotBalanceException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.journalNotBalance"));

             		 } catch (GlobalInventoryDateException ex) {

                   	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                   			    new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             		} catch (GlobalBranchAccountNumberInvalidException ex) {

    	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             		} catch (GlobalReferenceNumberNotUniqueException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

             		} catch (EJBException ex) {
             			if (log.isInfoEnabled()) {

             				log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
             						" session: " + session.getId());
             			}

             			return(mapping.findForward("cmnErrorPage"));
             		}

             		if (!errors.isEmpty()) {

             			saveErrors(request, new ActionMessages(errors));
             			return (mapping.findForward("apVoucherEntry"));

             		}

             	// save attachment

                    if (!Common.validateRequired(filename1)) {

                	        if (errors.isEmpty()) {

                	        	InputStream is = actionForm.getFilename1().getInputStream();

                	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

                	        	new File(attachmentPath).mkdirs();

                	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                	    			fileExtension = attachmentFileExtension;

                    	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                    	    		fileExtension = attachmentFileExtensionPDF;

                    	    	}
                	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

                	    		int c;

         	            	while ((c = is.read()) != -1) {

         	            		fos.write((byte)c);

         	            	}

         	            	is.close();
         					fos.close();

                	        }

                	   }

                	   if (!Common.validateRequired(filename2)) {

                	        if (errors.isEmpty()) {

                	        	InputStream is = actionForm.getFilename2().getInputStream();

                	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

                	        	new File(attachmentPath).mkdirs();

                	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                	    			fileExtension = attachmentFileExtension;

                    	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                    	    		fileExtension = attachmentFileExtensionPDF;

                    	    	}
                	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);
         	            	int c;

         	            	while ((c = is.read()) != -1) {

         	            		fos.write((byte)c);

         	            	}

         	            	is.close();
         					fos.close();

                	        }

                	   }

                	   if (!Common.validateRequired(filename3)) {

                	        if (errors.isEmpty()) {

                	        	InputStream is = actionForm.getFilename3().getInputStream();

                	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

                	        	new File(attachmentPath).mkdirs();

                	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                	    			fileExtension = attachmentFileExtension;

                    	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                    	    		fileExtension = attachmentFileExtensionPDF;

                    	    	}
                	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

         	            	int c;

         	            	while ((c = is.read()) != -1) {

         	            		fos.write((byte)c);

         	            	}

         	            	is.close();
         					fos.close();

                	        }

                	   }

                	   if (!Common.validateRequired(filename4)) {

                	        if (errors.isEmpty()) {

                	        	InputStream is = actionForm.getFilename4().getInputStream();

                	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

                	        	new File(attachmentPath).mkdirs();

                	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

                	    			fileExtension = attachmentFileExtension;

                    	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                    	    		fileExtension = attachmentFileExtensionPDF;

                    	    	}
                	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

         	            	int c;

         	            	while ((c = is.read()) != -1) {

         	            		fos.write((byte)c);

         	            	}

         	            	is.close();
         					fos.close();

                	        }

                	   }
             		actionForm.setReport(Constants.STATUS_SUCCESS);

             		isInitialPrinting = true;

             	}  else {

             		actionForm.setReport(Constants.STATUS_SUCCESS);

             		return(mapping.findForward("apVoucherEntry"));

             	}

/*******************************************************
 -- Ap VPO Journal Action --
*******************************************************/
             } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("PO MATCHED")) {

             	if(Common.validateRequired(actionForm.getApprovalStatus())) {
             		ApVoucherDetails details = new ApVoucherDetails();

             		details.setVouType("PO MATCHED");
             		details.setVouCode(actionForm.getVoucherCode());
             		details.setVouDate(Common.convertStringToSQLDate(actionForm.getDate()));
             		details.setVouDocumentNumber(actionForm.getDocumentNumber());
                    details.setVouReferenceNumber(actionForm.getReferenceNumber());
             		details.setVouVoid(Common.convertBooleanToByte(actionForm.getVoucherVoid()));
             		details.setVouDescription(actionForm.getDescription());
             		details.setVouConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             		details.setVouConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));
             		details.setVouPoNumber(actionForm.getPoNumber());

             		if (actionForm.getVoucherCode() == null) {

             			details.setVouCreatedBy(user.getUserName());
             			details.setVouDateCreated(new java.util.Date());

             		}

             		details.setVouLastModifiedBy(user.getUserName());
             		details.setVouDateLastModified(new java.util.Date());

             		ArrayList plList = new ArrayList();

                    for (int i = 0; i<actionForm.getApVPOListSize(); i++) {

                    	   ApVoucherPurchaseOrderLineList apVPOList = actionForm.getApVPOByIndex(i);

                    	   if(!apVPOList.getIssueCheckbox()) continue;

                    	   plList.add(apVPOList.getVoucherPurchaseOrderLineCode());

                    }



             		try {

             			Integer voucherCode = ejbVOU.saveApVouPlEntry(details, actionForm.getPaymentTerm(),
             					actionForm.getTaxCode(), actionForm.getWithholdingTax(),
								actionForm.getCurrency(), actionForm.getSupplier(), actionForm.getBatchName(),
								plList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

             			actionForm.setVoucherCode(voucherCode);

             		} catch (GlobalRecordAlreadyDeletedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

             		} catch (GlobalDocumentNumberNotUniqueException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.documentNumberNotUnique"));

             		} catch (GlobalConversionDateNotExistException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.conversionDateNotExist"));

             		} catch (GlobalPaymentTermInvalidException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.paymentTermInvalid"));

             		} catch (GlobalTransactionAlreadyApprovedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyApproved"));

             		} catch (GlobalTransactionAlreadyPendingException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyPending"));

             		} catch (GlobalTransactionAlreadyPostedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyPosted"));

             		} catch (GlobalTransactionAlreadyVoidException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.transactionAlreadyVoid"));

             		} catch (GlobalNoApprovalRequesterFoundException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.noApprovalRequesterFound"));

             		} catch (GlobalNoApprovalApproverFoundException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.noApprovalApproverFound"));

             		} catch (GlJREffectiveDateNoPeriodExistException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.effectiveDateNoPeriodExist"));

             		} catch (GlJREffectiveDatePeriodClosedException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.effectiveDatePeriodClosed"));

             		} catch (GlobalJournalNotBalanceException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.journalNotBalance"));

             		 } catch (GlobalInventoryDateException ex) {

                   	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                   			    new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             		} catch (GlobalBranchAccountNumberInvalidException ex) {

    	          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
    	                   new ActionMessage("voucherEntry.error.branchAccountNumberInvalid"));

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             		} catch (GlobalReferenceNumberNotUniqueException ex) {

             			errors.add(ActionMessages.GLOBAL_MESSAGE,
             					new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));

             		} catch (EJBException ex) {
             			if (log.isInfoEnabled()) {

             				log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
             						" session: " + session.getId());
             			}

             			return(mapping.findForward("cmnErrorPage"));
             		}

             		if (!errors.isEmpty()) {

             			saveErrors(request, new ActionMessages(errors));
             			return (mapping.findForward("apVoucherEntry"));

             		}



             	}

             	String path = "/apJournal.do?forward=1" +
				"&transactionCode=" + actionForm.getVoucherCode() +
				"&transaction=VOUCHER" +
				"&transactionNumber=" + actionForm.getReferenceNumber() +
				"&transactionDate=" + actionForm.getDate() +
				"&transactionEnableFields=" + actionForm.getEnableFields();

             	return(new ActionForward(path));

/*******************************************************
   -- AP VOU Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbVOU.deleteApVouEntry(actionForm.getVoucherCode(), user.getUserName(), user.getCmpCode());

           	    // delete attachment

           	    if (actionForm.getVoucherCode() != null) {

					appProperties = MessageResources.getMessageResources("com.ApplicationResources");

		            attachmentPath = appProperties.getMessage("app.attachmentPath") + "ap/Voucher/";
		            attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
					File file = new File(attachmentPath + actionForm.getVoucherCode() + "-1" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getVoucherCode() + "-2" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getVoucherCode() + "-3" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

					file = new File(attachmentPath + actionForm.getVoucherCode() + "-4" + attachmentFileExtension);

					if (file.exists()) {

						file.delete();

					}

           	    }

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());

                }

                return(mapping.findForward("cmnErrorPage"));

            }

/*******************************************************
   -- Ap VOU Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap VOU Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("EXPENSES")) {

         	int listSize = actionForm.getApVOUListSize();

            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	ApVoucherEntryList apVOUList = new ApVoucherEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);

	        	apVOUList.setDrClassList(this.getDrClassList());

	        	actionForm.saveApVOUList(apVOUList);

	        }

	        return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("EXPENSES")) {

         	for (int i = 0; i<actionForm.getApVOUListSize(); i++) {

           	   ApVoucherEntryList apVOUList = actionForm.getApVOUByIndex(i);

           	   if (apVOUList.getDeleteCheckbox()) {

           	   	   actionForm.deleteApVOUList(i);
           	   	   i--;
           	   }

            }

            for (int i = 0; i<actionForm.getApVOUListSize(); i++) {

           	   ApVoucherEntryList apVOUList = actionForm.getApVOUByIndex(i);

           	   apVOUList.setLineNumber(String.valueOf(i+1));

            }

	        return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU VLI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	int listSize = actionForm.getApVLIListSize();

            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
					"0.0", null, null,
					false, false, false, null, null, null , null, null, null
	        			);

	        	apVLIList.setLocationList(actionForm.getLocationList());

	        	actionForm.saveApVLIList(apVLIList);

	        }

	        return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	for (int i = 0; i<actionForm.getApVLIListSize(); i++) {

           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);

           	   if (apVLIList.getDeleteCheckbox()) {

           	   	   actionForm.deleteApVLIList(i);
           	   	   i--;
           	   }

            }

            for (int i = 0; i<actionForm.getApVLIListSize(); i++) {

           	   ApVoucherLineItemList apVLIList = actionForm.getApVLIByIndex(i);

           	   apVLIList.setLineNumber(String.valueOf(i+1));

            }

	        return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU Supplier Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {

         	try {

         		ApModSupplierDetails spldetails = ejbVOU.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());

         		actionForm.setPaymentTerm(spldetails.getSplPytName());
         		actionForm.setWithholdingTax(spldetails.getSplScWtcName());
         		actionForm.setSupplierName(spldetails.getSplName());
         		actionForm.setIsLoan(spldetails.getSplScLoan() == (byte)1 ? true :false);

         		if(actionForm.getType().equals("PO MATCHED") && actionForm.getType() != null){

         			actionForm.clearApVPOList();

         			String tCode = ejbVOU.getApNoneTc(user.getCmpCode());

         			actionForm.setTaxCode("NONE");

         			ArrayList list = new ArrayList();

         			Iterator  i = null;

         			list = ejbVOU.getApOpenPlBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         			i = list.iterator();

         			while (i.hasNext()) {

         				ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)i.next();
         				//mdetails.getPl
         				ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
         						actionForm, mdetails.getPlCode(),
								mdetails.getPlPoDocumentNumber(), mdetails.getPlPoReceivingPoNumber(),
								mdetails.getPlLocName(), mdetails.getPlIiName(),
								mdetails.getPlIiDescription(),
								Common.convertDoubleToStringMoney(mdetails.getPlPoAmount(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlPoQuantity(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlQuantity(), precisionUnit),
								mdetails.getPlUomName(),
								Common.convertDoubleToStringMoney(mdetails.getPlUnitCost(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlAmount(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount1(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount2(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount3(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlDiscount4(), precisionUnit),
								Common.convertDoubleToStringMoney(mdetails.getPlTotalDiscount(), precisionUnit),
								mdetails.getPlMisc());

         				apVPOList.setPoConversionRate(Common.convertDoubleToStringMoney(mdetails.getPlPoConversionRate(),Constants.MONEY_RATE_PRECISION));
         				apVPOList.setPoConversionDate(Common.convertSQLDateToString(mdetails.getPlPoConversionDate()));

         				actionForm.saveApVPOList(apVPOList);
         				actionForm.setCurrency(mdetails.getPlCurrency());

         			}

         		} else {

         			ApModSupplierDetails mdetails = ejbVOU.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());

         			actionForm.clearApVOUList();
         			actionForm.setPaymentTerm(mdetails.getSplPytName());
         			actionForm.setTaxCode(mdetails.getSplScTcName());
         			actionForm.setWithholdingTax(mdetails.getSplScWtcName());
         			actionForm.setBillAmount(null);
         			actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));

         			// populate line item list

         			if (actionForm.getType().equalsIgnoreCase("ITEMS") && actionForm.getType() != null) {

         				if(mdetails.getSplLitName() != null && mdetails.getSplLitName().length() > 0) {

         					actionForm.clearApVLIList();

         					ArrayList list = ejbVOU.getInvLitByCstLitName(mdetails.getSplLitName(), user.getCmpCode());

         					if (!list.isEmpty()) {

         						Iterator i = list.iterator();

         						while (i.hasNext()) {

         							InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();

         							ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm, null,
         									Common.convertShortToString(liDetails.getLiLine()), liDetails.getLiLocName(),
											liDetails.getLiIiName(), liDetails.getLiIiDescription(), "0",
											liDetails.getLiUomName(), null, null, null, null, null, null, null, null,
											false, false, false, null, null,null, null, null, null);

         							// populate location list

         							apVLIList.setLocationList(actionForm.getLocationList());

         							// populate unit field class

         							ArrayList uomList = new ArrayList();
         							uomList = ejbVOU.getInvUomByIiName(apVLIList.getItemName(), user.getCmpCode());

         							apVLIList.clearUnitList();
         							apVLIList.setUnitList(Constants.GLOBAL_BLANK);

         							Iterator j = uomList.iterator();
         							while (j.hasNext()) {

         								InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();

         								apVLIList.setUnitList(mUomDetails.getUomName());

         								if (mUomDetails.isDefault()) {

         									apVLIList.setUnit(mUomDetails.getUomName());

         								}

         							}

         							// populate unit cost field

         							if (!Common.validateRequired(apVLIList.getItemName()) && !Common.validateRequired(apVLIList.getUnit())) {

         								//double unitCost = ejbVOU.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(), user.getCmpCode());
         								
         								double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
               
         								apVLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

         							}

         							// populate amount field

         							if(!Common.validateRequired(apVLIList.getQuantity()) && !Common.validateRequired(apVLIList.getUnitCost())) {

         								double amount = Common.convertStringMoneyToDouble(apVLIList.getQuantity(), precisionUnit) *
										Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), precisionUnit);
         								apVLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

         							}

         							actionForm.saveApVLIList(apVLIList);

         						}

         					}

         				} else {

         					actionForm.setTaxRate(spldetails.getSplScTcRate());
         					actionForm.setTaxType(spldetails.getSplScTcType());

         					actionForm.clearApVLIList();

         					for (int x = 1; x <= journalLineNumber; x++) {

         						ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm,
         								null, String.valueOf(x), null, null, null, null, null, null, null, "0.0","0.0", "0.0",
										"0.0", null, null,
										false, false, false, null ,null , null, null, null, null
         								);

         						apVLIList.setLocationList(actionForm.getLocationList());
         						apVLIList.setUnitList(Constants.GLOBAL_BLANK);
         						apVLIList.setUnitList("Select Item First");

         						actionForm.saveApVLIList(apVLIList);

         					}

         				}

         			} else {

         				actionForm.clearApVOUList();

         			}

         		}



         	} catch (GlobalNoRecordFoundException ex) {

             	actionForm.clearApVOUList();

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.supplierNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            return(mapping.findForward("apVoucherEntry"));

    /*******************************************************
    -- Ap VOU PO Enter Action --
 *******************************************************/

          } else if(!Common.validateRequired(request.getParameter("isPurchaseOrderEntered"))) {

          	try {
          		String SPL_SPPLR_CODE = "";
          		if(actionForm.getType().equals("PO MATCHED") && actionForm.getType() != null) {
          			
          			ArrayList list = new ArrayList();
          			list = ejbVOU.getApOpenPlByPoDcmntNmbr(actionForm.getPoNumber(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
          			
          			actionForm.clearApVPOList();

          			String tCode = ejbVOU.getApNoneTc(user.getCmpCode());

          			actionForm.setTaxCode("NONE");

          	
          			if(list.size()<=0) {
          				throw new GlobalNoRecordFoundException(actionForm.getPoNumber());
          			}
          			Iterator  i = null;

          		

          			i = list.iterator();

          			while (i.hasNext()) {
          				
          				ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)i.next();
          				//mdetails.getPl
          				ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
          						actionForm, mdetails.getPlCode(),
 								mdetails.getPlPoDocumentNumber(), mdetails.getPlPoReceivingPoNumber(),
 								mdetails.getPlLocName(), mdetails.getPlIiName(),
 								mdetails.getPlIiDescription(),
 								Common.convertDoubleToStringMoney(mdetails.getPlPoAmount(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlPoQuantity(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlQuantity(), precisionUnit),
 								mdetails.getPlUomName(),
 								Common.convertDoubleToStringMoney(mdetails.getPlUnitCost(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlAmount(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlDiscount1(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlDiscount2(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlDiscount3(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlDiscount4(), precisionUnit),
 								Common.convertDoubleToStringMoney(mdetails.getPlTotalDiscount(), precisionUnit),
 								mdetails.getPlMisc());

          				apVPOList.setPoConversionRate(Common.convertDoubleToStringMoney(mdetails.getPlPoConversionRate(),Constants.MONEY_RATE_PRECISION));
          				apVPOList.setPoConversionDate(Common.convertSQLDateToString(mdetails.getPlPoConversionDate()));
          				System.out.println("po number: " + mdetails.getPlPoReceivingPoNumber());
          				actionForm.saveApVPOList(apVPOList);
          				actionForm.setCurrency(mdetails.getPlCurrency());
          				actionForm.setPoNumber(mdetails.getPlPoReceivingPoNumber());
          				
          				actionForm.setSupplier(mdetails.getPlPoSupplierCode());

          			}
          			
          			
          		}
          	
          		ApModSupplierDetails spldetails = ejbVOU.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());
          		
          		
          		

          		

          		actionForm.setPaymentTerm(spldetails.getSplPytName());
          		actionForm.setWithholdingTax(spldetails.getSplScWtcName());
          		actionForm.setSupplierName(spldetails.getSplName());
          		actionForm.setIsLoan(spldetails.getSplScLoan() == (byte)1 ? true :false);

          	


          	} catch (GlobalNoRecordFoundException ex) {

              	actionForm.clearApVOUList();

            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("voucherEntry.error.poNotFound",ex.getMessage()));
                 saveErrors(request, new ActionMessages(errors));

              } catch (EJBException ex) {

                if (log.isInfoEnabled()) {

                   log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                   return mapping.findForward("cmnErrorPage");

                }

             }

             return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU Bill Amount Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {

         	try {
         		 System.out.println("actionForm.getBillAmount()="+actionForm.getBillAmount());
                 ArrayList list = ejbVOU.getApDrBySplSupplierCodeAndTcNameAndWtcNameAndVouBillAmount(actionForm.getSupplier(),
                     actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                     Common.convertStringMoneyToDouble(actionForm.getBillAmount(), precisionUnit), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


                 actionForm.clearApVOUList();

 	             Iterator i = list.iterator();

		         while (i.hasNext()) {

			           ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();

				       String debitAmount = null;
				       String creditAmount = null;

				       double DR_AMOUNT = Common.roundIt(mDrDetails.getDrAmount() * Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION), precisionUnit);

				       if (mDrDetails.getDrDebit() == 1) {

				          debitAmount = Common.convertDoubleToStringMoney(DR_AMOUNT, precisionUnit);

				       } else {

				          creditAmount = Common.convertDoubleToStringMoney(DR_AMOUNT, precisionUnit);

				       }

				       ArrayList classList = new ArrayList();
				       classList.add(mDrDetails.getDrClass());

				       ApVoucherEntryList apDrList = new ApVoucherEntryList(actionForm,
				          mDrDetails.getDrCode(),
				          Common.convertShortToString(mDrDetails.getDrLine()),
				          mDrDetails.getDrCoaAccountNumber(),
				          debitAmount, creditAmount, mDrDetails.getDrClass(),
				          mDrDetails.getDrCoaAccountDescription());


				      if (mDrDetails.getDrClass().equals(Constants.AP_DR_CLASS_PAYABLE)) {

				      	  actionForm.setAmountDue(Common.convertDoubleToStringMoney(DR_AMOUNT, precisionUnit));

				      }

				      actionForm.setTotalDebit(Common.convertDoubleToStringMoney(DR_AMOUNT, precisionUnit));
				      actionForm.setTotalCredit(Common.convertDoubleToStringMoney(DR_AMOUNT, precisionUnit));

				      apDrList.setDrClassList(classList);

				      actionForm.saveApVOUList(apDrList);
			     }

		         int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

		         for (int x = list.size() + 1; x <= remainingList; x++) {

		        	ApVoucherEntryList apVOUList = new ApVoucherEntryList(actionForm,
		        	    null, String.valueOf(x), null, null, null, null, null);

		        	apVOUList.setDrClassList(this.getDrClassList());

		        	actionForm.saveApVOUList(apVOUList);

		         }

             } catch (GlobalNoRecordFoundException ex) {

           	    actionForm.clearApVOUList();

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("voucherEntry.error.billAmountNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU View Attachment Action --
*******************************************************/

         } else if(request.getParameter("viewAttachmentButton1") != null) {

 	  	File file = new File(attachmentPath + actionForm.getVoucherCode() + "-1" + attachmentFileExtension );
 	  	File filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-1" + attachmentFileExtensionPDF );
 	  	String filename = "";

 	  	if (file.exists()){
 	  		filename = file.getName();
 	  	}else if (filePDF.exists()) {
 	  		filename = filePDF.getName();
 	  	}

			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

 	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

 	  		fileExtension = attachmentFileExtension;

	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;

	    	}
 	  	System.out.println(fileExtension + " <== file extension?");
 	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getVoucherCode() + "-1" + fileExtension);

 	  	byte data[] = new byte[fis.available()];

 	  	int ctr = 0;
 	  	int c = 0;

 	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

 	  	}

 	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
 	  	} else if (fileExtension == attachmentFileExtensionPDF ){
 	  		System.out.println("pdf");

 	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
 	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
 	  	}

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apVoucherEntry"));

	         } else {

	           return (mapping.findForward("apVoucherEntryChild"));

	        }



	      } else if(request.getParameter("viewAttachmentButton2") != null) {

	    	  File file = new File(attachmentPath + actionForm.getVoucherCode() + "-2" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-2" + attachmentFileExtensionPDF );
	    	  	String filename = "";

	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getVoucherCode() + "-2" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}




	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apVoucherEntry"));

	         } else {

	           return (mapping.findForward("apVoucherEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton3") != null) {

	    	  File file = new File(attachmentPath + actionForm.getVoucherCode() + "-3" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-3" + attachmentFileExtensionPDF );
	    	  String filename = "";

	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }

	    	  System.out.println(filename + " <== filename?");
	    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    		  fileExtension = attachmentFileExtension;

	    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	    	  }
	    	  System.out.println(fileExtension + " <== file extension?");
	    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getVoucherCode() + "-3" + fileExtension);

	    	  byte data[] = new byte[fis.available()];

	    	  int ctr = 0;
	    	  int c = 0;

	    	  while ((c = fis.read()) != -1) {

	    		  data[ctr] = (byte)c;
	    		  ctr++;

	    	  }

	    	  if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  } else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  }

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apVoucherEntry"));

	         } else {

	           return (mapping.findForward("apVoucherEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton4") != null) {

	    	  File file = new File(attachmentPath + actionForm.getVoucherCode() + "-4" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-4" + attachmentFileExtensionPDF );
	    	  String filename = "";

	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getVoucherCode() + "-4" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("apVoucherEntry"));

	         } else {

	           return (mapping.findForward("apVoucherEntryChild"));

	        }

/*******************************************************
    -- Ap VOU Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apVLIList[" +
        actionForm.getRowVLISelected() + "].isItemEntered"))) {


      	ApVoucherLineItemList apVLIList =
      		actionForm.getApVLIByIndex(actionForm.getRowVLISelected());


      	try {

      		 if(Common.validateRequired(actionForm.getSupplier()) || actionForm.getSupplier().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
                 errors.add("supplier",
                    new ActionMessage("voucherEntry.error.supplierRequired"));

                 apVLIList.setItemName("");
                 apVLIList.setLocation("");
                 apVLIList.setItemDescription("");
                 saveErrors(request, new ActionMessages(errors));
                 return(mapping.findForward("apVoucherEntry"));
              }

      		apVLIList.setIsTax(false);
    		apVLIList.setIsVatRelief(false);
    		apVLIList.setIsProject(false);

      		// populate unit field class

      		ArrayList uomList = new ArrayList();
      		uomList = ejbVOU.getInvUomByIiName(apVLIList.getItemName(), user.getCmpCode());

      		apVLIList.clearUnitList();
      		apVLIList.setUnitList(Constants.GLOBAL_BLANK);

  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {

      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

      			apVLIList.setUnitList(mUomDetails.getUomName());

      			if (mUomDetails.isDefault()) {

      				apVLIList.setUnit(mUomDetails.getUomName());

      			}

      		}



      		//TODO: populate taglist with empty values
      		apVLIList.clearTagList();


     		boolean isTraceMisc = ejbVOU.getInvTraceMisc(apVLIList.getItemName(), user.getCmpCode());


     		apVLIList.setIsTraceMisc(isTraceMisc);
      		System.out.println(isTraceMisc + "<== trace misc item enter action");
      		if (isTraceMisc == true){
      			apVLIList.clearTagList();

      			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), apVLIList.getQuantity());


      			apVLIList.setMisc(misc);

      		}


      		// populate unit cost field

      		if (!Common.validateRequired(apVLIList.getItemName()) && !Common.validateRequired(apVLIList.getUnit())) {

	      		//double unitCost = ejbVOU.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(), user.getCmpCode());
	      		
	      		double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                    
	      		
	      		apVLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));
	      		apVLIList.setIsTax(Common.convertByteToBoolean(ejbVOU.getInvIiIsTaxByIiName(apVLIList.getItemName(), user.getCmpCode())));
      		}

    		apVLIList.setDiscount1("0.0");
    		apVLIList.setDiscount2("0.0");
    		apVLIList.setDiscount3("0.0");
    		apVLIList.setDiscount4("0.0");
         	apVLIList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));

	        apVLIList.setQuantity(Common.convertDoubleToStringMoney(1d, precisionUnit));
    		apVLIList.setAmount(apVLIList.getUnitCost());

    		//ApModSupplierDetails mdetails = ejbVOU.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());


    		if( Common.convertByteToBoolean(ejbVOU.getInvIiIsVatReliefByIiName(apVLIList.getItemName(), user.getCmpCode()))){


    			apVLIList.setIsVatRelief(true);
    			apVLIList.setSupplierTin("<TIN>");
    			apVLIList.setSupplierAddress("<ADDRESS>");
    			apVLIList.setSupplierName("<SUPPLIER NAME>");
    			System.out.println("isProject Entered-------------------="+apVLIList.getIsVatRelief());
    		}


    		if( Common.convertByteToBoolean(ejbVOU.getInvIiIsProjectByIiName(apVLIList.getItemName(), user.getCmpCode()))){

    			apVLIList.setIsProject(true);
    			apVLIList.clearProjectNameList();
    			apVLIList.setProjectCodeList("<PROJECT CODE>");

    			ArrayList projectCodeList = ejbVOU.getPmPrjctCodeAll(user.getCmpCode());

    			for(int x=0;x<projectCodeList.size();x++) {
    				String prjctCode = projectCodeList.get(x).toString();
    				apVLIList.setProjectCodeList(prjctCode);
    			}
    			apVLIList.setProjectCode("<PROJECT CODE>");

    			apVLIList.clearProjectTypeCodeList();
    			apVLIList.setProjectTypeCodeList("<TYPE>");
    			apVLIList.setProjectTypeCode("<TYPE>");

    			apVLIList.clearProjectPhaseNameList();
    			apVLIList.setProjectPhaseNameList("<PHASE>");
    			apVLIList.setProjectPhaseName("<PHASE>");

    		}else {
    			apVLIList.setIsVatRelief(false);
    			apVLIList.setSupplierTin("");
    			apVLIList.setSupplierAddress("");
    			apVLIList.setSupplierName("");

    		}




          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("apVoucherEntry"));


/*******************************************************
     -- Ap VOU Project Code Enter Action --
*******************************************************/

       } else if(!Common.validateRequired(request.getParameter("apVLIList[" +
         actionForm.getRowVLISelected() + "].isProjectCodeEntered"))) {

	    ApVoucherLineItemList apVLIList =
	      		actionForm.getApVLIByIndex(actionForm.getRowVLISelected());
    	 try {

    		 String projectCode = apVLIList.getProjectCode();
    		 String projectTypeCode = apVLIList.getProjectTypeCode();


    		 if(!projectCode.equals("<PROJECT CODE>")) {

    			apVLIList.clearProjectTypeCodeList();
	 			apVLIList.setProjectTypeCodeList("<TYPE>");
	 			ArrayList projectTypeCodeList = ejbVOU.getPmOpenPttByPrjProjectCode(projectCode , user.getCmpCode());

	 			for(int y=0;y<projectTypeCodeList.size();y++) {

	 				String prjctTypCode = projectTypeCodeList.get(y).toString();
	 				System.out.println("p type : " + prjctTypCode);
	 				apVLIList.setProjectTypeCodeList(prjctTypCode);
	 			}
	 			apVLIList.setProjectTypeCode("<TYPE>");

    		 }


    		 System.out.println("prjcode: " + projectCode + "   prjcTyp:" + projectTypeCode);
    		 if(!projectCode.equals("<PROJECT CODE>") && !projectTypeCode.equals("<TYPE>")) {
    			 System.out.println("pasok 1");
    			 apVLIList.clearProjectPhaseNameList();
    			 apVLIList.setProjectPhaseNameList("<PHASE>");
    			 ArrayList projectPhaseNameList = ejbVOU.getPmPrjctPhsAllByPrjctCodeAndPrjctTypCode(projectCode, projectTypeCode, user.getCmpCode());

    			 for(int x=0;x<projectPhaseNameList.size();x++) {
    				 String projectPhaseName = projectPhaseNameList.get(x).toString();
    				 System.out.println("pasok 1:" + projectPhaseName );
    				 apVLIList.setProjectPhaseNameList(projectPhaseName);
    			 }

    		 }



    	 } catch (EJBException ex) {

             if (log.isInfoEnabled()) {

                log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                " session: " + session.getId());
                return mapping.findForward("cmnErrorPage");

             }

          }

          return(mapping.findForward("apVoucherEntry"));



/*******************************************************
     -- Ap VOU Project Type Code Enter Action --
*******************************************************/

       } else if(!Common.validateRequired(request.getParameter("apVLIList[" +
         actionForm.getRowVLISelected() + "].isProjectTypeCodeEntered"))) {
    	    ApVoucherLineItemList apVLIList =
    	      		actionForm.getApVLIByIndex(actionForm.getRowVLISelected());
    	   try {

      		 String projectCode = apVLIList.getProjectCode();
      		 String projectTypeCode = apVLIList.getProjectTypeCode();

      		System.out.println("prjcode: " + projectCode + "   prjcTyp:" + projectTypeCode);
      		 if(!projectCode.equals("<PROJECT CODE>") && !projectTypeCode.equals("<TYPE>")) {

      			 System.out.println("pasok 2");
      			 apVLIList.clearProjectPhaseNameList();
      			 apVLIList.setProjectPhaseNameList("<PHASE>");
      			 ArrayList projectPhaseNameList = ejbVOU.getPmPrjctPhsAllByPrjctCodeAndPrjctTypCode(projectCode, projectTypeCode, user.getCmpCode());

      			 for(int x=0;x<projectPhaseNameList.size();x++) {
      				 String projectPhaseName = projectPhaseNameList.get(x).toString();
      				System.out.println("pasok 2:" + projectPhaseName );
      				 apVLIList.setProjectPhaseNameList(projectPhaseName);
      			 }

      		 }



      	 } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            return(mapping.findForward("apVoucherEntry"));
/*******************************************************
    -- Ap VOU Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("apVLIList[" +
        actionForm.getRowVLISelected() + "].isUnitEntered"))) {

      	ApVoucherLineItemList apVLIList =
      		actionForm.getApVLIByIndex(actionForm.getRowVLISelected());

      	try {

            // populate unit cost field

      		if (!Common.validateRequired(apVLIList.getLocation()) && !Common.validateRequired(apVLIList.getItemName())) {

	      	//	double unitCost = ejbVOU.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(), user.getCmpCode());
	      		
	      		
	      		double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(apVLIList.getItemName(), apVLIList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
               
	      		apVLIList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, precisionUnit));

	        }

	        // populate amount field
      		double amount = 0;

	        if(!Common.validateRequired(apVLIList.getQuantity()) && !Common.validateRequired(apVLIList.getUnitCost())) {

	        	amount = Common.convertStringMoneyToDouble(apVLIList.getQuantity(), precisionUnit) *
					Common.convertStringMoneyToDouble(apVLIList.getUnitCost(), precisionUnit);

	        	apVLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	        }

      		// populate discount field

      		if (!Common.validateRequired(apVLIList.getAmount()) &&
      			(!Common.validateRequired(apVLIList.getDiscount1()) ||
				!Common.validateRequired(apVLIList.getDiscount2()) ||
				!Common.validateRequired(apVLIList.getDiscount3()) ||
				!Common.validateRequired(apVLIList.getDiscount4()))) {

      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

      			}

      			double discountRate1 = Common.convertStringMoneyToDouble(apVLIList.getDiscount1(), precisionUnit)/100;
      			double discountRate2 = Common.convertStringMoneyToDouble(apVLIList.getDiscount2(), precisionUnit)/100;
      			double discountRate3 = Common.convertStringMoneyToDouble(apVLIList.getDiscount3(), precisionUnit)/100;
      			double discountRate4 = Common.convertStringMoneyToDouble(apVLIList.getDiscount4(), precisionUnit)/100;
      			double totalDiscountAmount = 0d;

      			if (discountRate1 > 0) {

      				double discountAmount = amount * discountRate1;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate2 > 0) {

      				double discountAmount = amount * discountRate2;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate3 > 0) {

      				double discountAmount = amount * discountRate3;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (discountRate4 > 0) {

      				double discountAmount = amount * discountRate4;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;

      			}

      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount += amount * (actionForm.getTaxRate() / 100);

      			}

      			apVLIList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
      			apVLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

      		}

          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU Type Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {

	        if (actionForm.getType().equals("ITEMS")) {

	            actionForm.setBillAmount(null);
	           	actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApVOUList();
	           	actionForm.clearApVLIList();

	           	// Populate line when not forwarding

            	for (int x = 1; x <= journalLineNumber; x++) {

            		ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm,
            				null, new Integer(x).toString(), null, null, null, null, null,
							null, null, "0.0","0.0", "0.0", "0.0", null, null,
							false, false, false, null ,null , null, null, null, null);

            		apVLIList.setLocationList(actionForm.getLocationList());

            		apVLIList.setUnitList(Constants.GLOBAL_BLANK);
            		apVLIList.setUnitList("Select Item First");

            		actionForm.saveApVLIList(apVLIList);

            	}
	        } else if (actionForm.getType().equals("PO MATCHED")){

		        	if(actionForm.getSupplier().equals(Constants.GLOBAL_BLANK)){

		        		actionForm.clearApVOUList();
			           	actionForm.clearApVLIList();
			           	actionForm.clearApVPOList();

			           	ArrayList list = new ArrayList();

		         		Iterator  i = null;

		         		list = ejbVOU.getApOpenPl(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

		         		i = list.iterator();

		         		while (i.hasNext()) {

		         			ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)i.next();

		         			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
		         					actionForm, mdetails.getPlCode(),
									mdetails.getPlPoDocumentNumber(), mdetails.getPlPoReceivingPoNumber(),
									mdetails.getPlLocName(), mdetails.getPlIiName(),
									mdetails.getPlIiDescription(),
									Common.convertDoubleToStringMoney(mdetails.getPlPoAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlPoQuantity(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlQuantity(), precisionUnit),
									mdetails.getPlUomName(),
									Common.convertDoubleToStringMoney(mdetails.getPlUnitCost(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlTotalDiscount(), precisionUnit),
									mdetails.getPlMisc());

		         			actionForm.saveApVPOList(apVPOList);
		         		}


		        	} else {

		        		actionForm.clearApVPOList();

		        		String tCode = ejbVOU.getApNoneTc(user.getCmpCode());

		         		actionForm.setTaxCode("NONE");

		         		ArrayList list = new ArrayList();

		         		Iterator  i = null;

		         		list = ejbVOU.getApOpenPlBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

		         		i = list.iterator();

		         		while (i.hasNext()) {

		         			ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)i.next();

		         			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
		         					actionForm, mdetails.getPlCode(),
									mdetails.getPlPoDocumentNumber(), mdetails.getPlPoReceivingPoNumber(),
									mdetails.getPlLocName(), mdetails.getPlIiName(),
									mdetails.getPlIiDescription(),
									Common.convertDoubleToStringMoney(mdetails.getPlPoAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlPoQuantity(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlQuantity(), precisionUnit),
									mdetails.getPlUomName(),
									Common.convertDoubleToStringMoney(mdetails.getPlUnitCost(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mdetails.getPlTotalDiscount(), precisionUnit),
									mdetails.getPlMisc());

		         			actionForm.saveApVPOList(apVPOList);
		         		}

		        	}

	        } else {

	        	actionForm.setBillAmount(null);
	           	actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearApVOUList();
	           	actionForm.clearApVLIList();

	        }

	        return(mapping.findForward("apVoucherEntry"));



/*******************************************************
    -- Conversion Date Enter Action --
 *******************************************************/
  } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

try {

	actionForm.setConversionRate(Common.convertDoubleToStringMoney(
			ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
					Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

} catch (GlobalConversionDateNotExistException ex) {

	errors.add(ActionMessages.GLOBAL_MESSAGE,
			new ActionMessage("voucherEntry.error.conversionDateNotExist"));

} catch (EJBException ex) {

	if (log.isInfoEnabled()) {

		log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
				" session: " + session.getId());

	}

	return(mapping.findForward("cmnErrorPage"));

}

if (!errors.isEmpty()) {

	saveErrors(request, new ActionMessages(errors));

}

return(mapping.findForward("apVoucherEntry"));

/*******************************************************
    -- Conversion Date Enter Action --
 *******************************************************/
      } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

      	try {

      		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
      		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
      		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));

      	} catch (GlobalConversionDateNotExistException ex) {

      		errors.add(ActionMessages.GLOBAL_MESSAGE,
      				new ActionMessage("voucherEntry.error.conversionDateNotExist"));

      	} catch (EJBException ex) {

      		if (log.isInfoEnabled()) {

      			log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());

      		}

      		return(mapping.findForward("cmnErrorPage"));

      	}

      	if (!errors.isEmpty()) {

      		saveErrors(request, new ActionMessages(errors));

      	}

      	return(mapping.findForward("apVoucherEntry"));

/*******************************************************
         	   -- Ap VOU Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

        	 try {

        		 actionForm.setConversionRate(Common.convertDoubleToStringMoney(
        				 ejbVOU.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("voucherEntry.error.conversionDateNotExist"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());

        		 }

        		 return(mapping.findForward("cmnErrorPage"));

        	 }

        	 if (!errors.isEmpty()) {

        		 saveErrors(request, new ActionMessages(errors));

        	 }

        	 return(mapping.findForward("apVoucherEntry"));

/*******************************************************
   -- Ap VOU Load Action --
*******************************************************/

         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("apVoucherEntry"));

            }

            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }

            try {

            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearCurrencyList();

            	list = ejbVOU.getGlFcAllWithDefault(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

            			actionForm.setCurrencyList(mFcDetails.getFcName());

            			if (mFcDetails.getFcSob() == 1) {
            				System.out.println("SOME CHANGES HERE CURR: " + mFcDetails.getFcName());
            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());

            			}

            		}

            	}



            	actionForm.clearPaymentTermList();

            	list = ejbVOU.getAdPytAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setPaymentTermList((String)i.next());

            		}

            		actionForm.setPaymentTerm("IMMEDIATE");

            	}

            	actionForm.clearUserList();

            	ArrayList userList = ejbVOU.getAdUsrAll(user.getCmpCode());

            	if (userList == null || userList.size() == 0) {

            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator x = userList.iterator();

            		while (x.hasNext()) {

            			actionForm.setUserList((String)x.next());

            		}

            	}

            	actionForm.clearPaymentTerm2List();

            	list = ejbVOU.getAdPytAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setPaymentTerm2List(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setPaymentTerm2List((String)i.next());

            		}

            		actionForm.setPaymentTerm2(Constants.GLOBAL_BLANK);

            	}

            	actionForm.clearTaxCodeList();

            	list = ejbVOU.getApTcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setTaxCodeList((String)i.next());

            		}

            	}


            	actionForm.clearWithholdingTaxList();

            	list = ejbVOU.getApWtcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setWithholdingTaxList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setWithholdingTaxList((String)i.next());

            		}

            	}

            	if(actionForm.getUseSupplierPulldown()) {

	            	actionForm.clearSupplierList();

	            	list = ejbVOU.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setSupplierList((String)i.next());

	            		}

	            	}

            	}


            	actionForm.clearBatchNameList();
            	System.out.println("DEPARTMENT---------->"+user.getUserDepartment());
            	list = ejbVOU.getApOpenVbAll(user.getUserDepartment() == null ? "" :user.getUserDepartment(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBatchNameList((String)i.next());

            		}

            	}

            	actionForm.clearLocationList();

            	list = ejbVOU.getInvLocAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setLocationList((String)i.next());

            		}

            	}

            	if (request.getParameter("forwardBatch") != null) {

            		actionForm.setBatchName(request.getParameter("batchName"));
            		actionForm.setType(null);

            	}

            	actionForm.clearApVOUList();
            	actionForm.clearApVLIList();
            	actionForm.clearApVPOList();

            	if (request.getParameter("forward") != null || isInitialPrinting) {

            		if (request.getParameter("forward") != null) {

            			actionForm.setVoucherCode(new Integer(request.getParameter("voucherCode")));

            		}

            		ApModVoucherDetails mdetails = ejbVOU.getApVouByVouCode(actionForm.getVoucherCode(), user.getCmpCode());

            		actionForm.setDescription(mdetails.getVouDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getVouDate()));
            		actionForm.setDocumentNumber(mdetails.getVouDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getVouReferenceNumber());
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getVouConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getVouConversionRate(), Constants.CONVERSION_RATE_PRECISION));
            		actionForm.setBillAmount(Common.convertDoubleToStringMoney(mdetails.getVouBillAmount(), precisionUnit));
            		actionForm.setAmountDue(Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit));
            		actionForm.setAmountPaid(Common.convertDoubleToStringMoney(mdetails.getVouAmountPaid(), precisionUnit));
            		actionForm.setVoucherVoid(Common.convertByteToBoolean(mdetails.getVouVoid()));
            		actionForm.setApprovalStatus(mdetails.getVouApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getVouReasonForRejection());
            		actionForm.setPosted(mdetails.getVouPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getVouCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getVouDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getVouLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getVouDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getVouApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getVouDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getVouPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getVouDatePosted()));

            		actionForm.setPaymentTerm2(mdetails.getVouPytName2());
            		actionForm.setLoan(mdetails.getVouLoan() == (byte)1 ? true : false);
            		actionForm.setLoanGenerated(mdetails.getVouLoanGenerated() == (byte)1 ? true : false);
            		actionForm.setIsLoan(mdetails.getVouScLoan() == (byte)1 ? true : false);

            		System.out.println("mdetails.getVouScLoan()="+mdetails.getVouScLoan());
            		System.out.println("mdetails.getVouLoan()="+mdetails.getVouLoan());

            		if (!actionForm.getCurrencyList().contains(mdetails.getVouFcName())) {

            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearCurrencyList();

            			}
            			actionForm.setCurrencyList(mdetails.getVouFcName());

            		}
            		actionForm.setCurrency(mdetails.getVouFcName());

            		if (!actionForm.getTaxCodeList().contains(mdetails.getVouTcName())) {

            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearTaxCodeList();

            			}
            			actionForm.setTaxCodeList(mdetails.getVouTcName());

            		}
            		actionForm.setTaxCode(mdetails.getVouTcName());
            		actionForm.setTaxType(mdetails.getVouTcType());
            		actionForm.setTaxRate(mdetails.getVouTcRate());

            		if (!actionForm.getWithholdingTaxList().contains(mdetails.getVouWtcName())) {

            			if (actionForm.getWithholdingTaxList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearWithholdingTaxList();

            			}
            			actionForm.setWithholdingTaxList(mdetails.getVouWtcName());

            		}
            		actionForm.setWithholdingTax(mdetails.getVouWtcName());

            		if (!actionForm.getSupplierList().contains(mdetails.getVouSplSupplierCode())) {

            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearSupplierList();

            			}
            			actionForm.setSupplierList(mdetails.getVouSplSupplierCode());

            		}
            		actionForm.setSupplier(mdetails.getVouSplSupplierCode());
            		actionForm.setSupplierName(mdetails.getVouSplName());

            		if (!actionForm.getPaymentTermList().contains(mdetails.getVouPytName())) {

            			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearPaymentTermList();

            			}
            			actionForm.setPaymentTermList(mdetails.getVouPytName());

            		}
            		actionForm.setPaymentTerm(mdetails.getVouPytName());

            		actionForm.setPoNumber(mdetails.getVouPoNumber());

           		    if (!actionForm.getBatchNameList().contains(mdetails.getVouVbName())) {

           		    	actionForm.setBatchNameList(mdetails.getVouVbName());

           		    }
            		actionForm.setBatchName(mdetails.getVouVbName());

            		if (mdetails.getVouVliList() != null && !mdetails.getVouVliList().isEmpty()) {

            			actionForm.setType("ITEMS");

            		    list = mdetails.getVouVliList();

            			i = list.iterator();

            			while (i.hasNext()) {

            				ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails)i.next();

            				ApVoucherLineItemList apVliList = new ApVoucherLineItemList(actionForm,
            						mVliDetails.getVliCode(),
									Common.convertShortToString(mVliDetails.getVliLine()),
									mVliDetails.getVliLocName(),
									mVliDetails.getVliIiName(),
									mVliDetails.getVliIiDescription(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliQuantity(), precisionUnit),
									mVliDetails.getVliUomName(),
									Common.convertDoubleToStringMoney(mVliDetails.getVliUnitCost(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mVliDetails.getVliTotalDiscount(), precisionUnit),
									mVliDetails.getVliMisc(),
									mVliDetails.getVliIsVatRelief(),
									Common.convertByteToBoolean(mVliDetails.getVliTax()),
									mVliDetails.getVliIsProject(),
									mVliDetails.getVliSplName(),
									mVliDetails.getVliSplTin(),
									mVliDetails.getVliSplAddress(),
									mVliDetails.getVliProjectCode(),
									mVliDetails.getVliProjectTypeCode(),
									mVliDetails.getVliProjecPhaseName()

            						);

            				System.out.println("mVliDetails.getVliMisc(): " + mVliDetails.getVliMisc());
            				apVliList.setLocationList(actionForm.getLocationList());



            				boolean isTraceMisc = ejbVOU.getInvTraceMisc(mVliDetails.getVliIiName(), user.getCmpCode());

            				apVliList.setIsTraceMisc(isTraceMisc);

            				apVliList.clearTagList();


            				if (mVliDetails.getIsTraceMic()){


            					ArrayList tagList = mVliDetails.getTagList();


                            	if(tagList.size() > 0) {
                            		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mVliDetails.getVliQuantity()));

                            		System.out.println(misc + "<== misc");
                            	//	mRilDetails.setPlMisc(misc);

                            		apVliList.setMisc(misc);

                            	}else {
                              		if(mVliDetails.getVliMisc()==null) {

                              			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mVliDetails.getVliQuantity()));
                              			apVliList.setMisc(misc);
                              			System.out.println(misc + "<== misc");
                              		}
                              	}




            					System.out.println(mVliDetails.getVliMisc() + "<== misc");
            				}








            				apVliList.clearUnitList();
			            	ArrayList unitList = ejbVOU.getInvUomByIiName(mVliDetails.getVliIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {

			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		apVliList.setUnitList(mUomDetails.getUomName());

			            	}



			            	if(mVliDetails.getVliIsProject() ) {
			            		String projectCode = mVliDetails.getVliProjectCode();
			            		String projectTypeCode = mVliDetails.getVliProjectTypeCode();
			            		String projectPhaseName = mVliDetails.getVliProjecPhaseName();

			            		apVliList.clearProjectNameList();
			            		apVliList.setProjectCodeList("<PROJECT CODE>");
			        			ArrayList projectCodeList = ejbVOU.getPmPrjctCodeAll(user.getCmpCode());

			        			for(int x=0;x<projectCodeList.size();x++) {
			        				String prjctCode = projectCodeList.get(x).toString();
			        				apVliList.setProjectCodeList(prjctCode);
			        			}


			        			apVliList.clearProjectTypeCodeList();
			        			apVliList.setProjectTypeCodeList("<TYPE>");
			        			ArrayList projectTypeCodeList = ejbVOU.getPmOpenPttByPrjProjectCode(projectCode, user.getCmpCode());

			        			for(int y=0;y<projectTypeCodeList.size();y++) {

			        				String prjctTypCode = projectTypeCodeList.get(y).toString();

			        				apVliList.setProjectTypeCodeList(prjctTypCode);
			        			}




			        			apVliList.clearProjectPhaseNameList();
			        			apVliList.setProjectPhaseNameList("<PHASE>");

			        			ArrayList projectPhaseNameList = ejbVOU.getPmPrjctPhsAllByPrjctCodeAndPrjctTypCode(projectCode, projectTypeCode, user.getCmpCode());

			        			for(int y=0;y<projectPhaseNameList.size();y++) {

			        				String prjctPhsNm = projectPhaseNameList.get(y).toString();

			        				apVliList.setProjectPhaseNameList(prjctPhsNm);
			        			}


			            	}

            				actionForm.saveApVLIList(apVliList);


            			}

            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

            			for (int x = list.size() + 1; x <= remainingList; x++) {

            				ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null, null, null, "0.0",
									"0.0", "0.0", "0.0", null, null,
									false, false, false, null,null,null,null, null, null);

            				apVLIList.setLocationList(actionForm.getLocationList());

            				apVLIList.setUnitList(Constants.GLOBAL_BLANK);
            				apVLIList.setUnitList("Select Item First");

            				actionForm.saveApVLIList(apVLIList);

            			}

            			this.setFormProperties(actionForm, user.getCompany());

            			if (request.getParameter("child") == null) {

            				return (mapping.findForward("apVoucherEntry"));

            			} else {

            				return (mapping.findForward("apVoucherEntryChild"));

            			}

            		}else if (mdetails.getVouPlList() != null && !mdetails.getVouPlList().isEmpty()) {

            			actionForm.setType("PO MATCHED");

            			actionForm.clearApVPOList();

            		    list = mdetails.getVouPlList();

            			i = list.iterator();

            			while (i.hasNext()) {

            				ApModPurchaseOrderLineDetails mPlDetails = (ApModPurchaseOrderLineDetails)i.next();

            				ApVoucherPurchaseOrderLineList apPlList = new ApVoucherPurchaseOrderLineList(actionForm,
            						mPlDetails.getPlCode(),
									mPlDetails.getPlPoDocumentNumber(),
									mPlDetails.getPlPoReceivingPoNumber(),
									mPlDetails.getPlLocName(),
									mPlDetails.getPlIiName(),
									mPlDetails.getPlIiDescription(),
									Common.convertDoubleToStringMoney(mPlDetails.getPlPoAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlPoQuantity(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlQuantity(), precisionUnit),
									mPlDetails.getPlUomName(),
									Common.convertDoubleToStringMoney(mPlDetails.getPlUnitCost(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount1(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount2(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount3(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlDiscount4(), precisionUnit),
									Common.convertDoubleToStringMoney(mPlDetails.getPlTotalDiscount(), precisionUnit),
									mPlDetails.getPlMisc());

            				
            				
            				apPlList.setPoConversionRate(Common.convertDoubleToStringMoney(mPlDetails.getPlPoConversionRate(),Constants.MONEY_RATE_PRECISION));
            				apPlList.setPoConversionDate(Common.convertSQLDateToString(mPlDetails.getPlPoConversionDate()));
              				
              			
              				actionForm.setCurrency(mPlDetails.getPlCurrency());
              				actionForm.setPoNumber(mPlDetails.getPlPoReceivingPoNumber());
              				
              				
            				apPlList.setIssueCheckbox(true);

            				boolean isTraceMisc = ejbVOU.getInvTraceMisc(mPlDetails.getPlIiName(), user.getCmpCode());

            				apPlList.setIsTraceMisc(isTraceMisc);

            				apPlList.clearTagList();
            				String misc = "";

            				if (isTraceMisc){




            					ArrayList tagList = mPlDetails.getPlTagList();

            					if (tagList.size() == 0 && tagList.isEmpty()) {

            						misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mPlDetails.getPlPoQuantity()));



	            				}else {
	            					System.out.println("existing taglist?");
	            					//TODO:save tagList

	            					misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mPlDetails.getPlPoQuantity()));




	            				}
            					mPlDetails.setPlMisc(misc);
            					apPlList.setMisc(mPlDetails.getPlMisc());

            					System.out.println(mPlDetails.getPlMisc() + "<== misc");
            				}




            				actionForm.saveApVPOList(apPlList);

            			}

            			this.setFormProperties(actionForm, user.getCompany());

            			if (actionForm.getEnableFields()) {

            				//actionForm.clearApVPOList();
            				list = ejbVOU.getApOpenPlBySplSupplierCode(actionForm.getSupplier(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     		i = list.iterator();

                     		while (i.hasNext()) {

                     			ApModPurchaseOrderLineDetails mPolDetails = (ApModPurchaseOrderLineDetails)i.next();

                     			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
                     					actionForm, mPolDetails.getPlCode(),
                     					mPolDetails.getPlPoDocumentNumber(), mPolDetails.getPlPoReceivingPoNumber(),
            							mPolDetails.getPlLocName(), mPolDetails.getPlIiName(),
            							mPolDetails.getPlIiDescription(),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlPoAmount(), precisionUnit),
    									Common.convertDoubleToStringMoney(mPolDetails.getPlPoQuantity(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlQuantity(), precisionUnit),
            							mPolDetails.getPlUomName(),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlUnitCost(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlAmount(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount1(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount2(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount3(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlDiscount4(), precisionUnit),
            							Common.convertDoubleToStringMoney(mPolDetails.getPlTotalDiscount(), precisionUnit),
            							mPolDetails.getPlMisc());



                     			apVPOList.setPoConversionRate(Common.convertDoubleToStringMoney(mPolDetails.getPlPoConversionRate(),Constants.MONEY_RATE_PRECISION));
							    apVPOList.setPoConversionDate(Common.convertSQLDateToString(mPolDetails.getPlPoConversionDate()));
							    System.out.println("po number: " + mPolDetails.getPlPoReceivingPoNumber());
							    



                     			boolean isTraceMisc = ejbVOU.getInvTraceMisc(mPolDetails.getPlIiName(), user.getCmpCode());

                     			apVPOList.setIsTraceMisc(isTraceMisc);

                     			apVPOList.clearTagList();
                				String misc = "";

                				if (isTraceMisc){




                					ArrayList tagList = mPolDetails.getPlTagList();

                					if (tagList.size() == 0 && tagList.isEmpty()) {

                						misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mPolDetails.getPlPoQuantity()));



    	            				}else {
    	            					System.out.println("existing taglist?");
    	            					//TODO:save tagList

    	            					misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mPolDetails.getPlPoQuantity()));




    	            				}
                					mPolDetails.setPlMisc(misc);
                					apVPOList.setMisc(mPolDetails.getPlMisc());

                					System.out.println(mPolDetails.getPlMisc() + "<== misc");
                				}






                     			actionForm.saveApVPOList(apVPOList);
                     		}

            			}

            			if (request.getParameter("child") == null) {

            				return (mapping.findForward("apVoucherEntry"));

            			} else {

            				return (mapping.findForward("apVoucherEntryChild"));

            			}


            		} else {

            		    actionForm.setType("EXPENSES");

            			list = mdetails.getVouDrList();

            			i = list.iterator();

            			while (i.hasNext()) {

            				ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();

            				String debitAmount = null;
            				String creditAmount = null;

            				if (mDrDetails.getDrDebit() == 1) {

            					debitAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);

            				} else {

            					creditAmount = Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit);

            				}

            				actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getVouTotalDebit(), precisionUnit));
            				actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getVouTotalCredit(), precisionUnit));

            				ArrayList classList = new ArrayList();
            				classList.add(mDrDetails.getDrClass());

            				ApVoucherEntryList apDrList = new ApVoucherEntryList(actionForm,
            						mDrDetails.getDrCode(),
									Common.convertShortToString(mDrDetails.getDrLine()),
									mDrDetails.getDrCoaAccountNumber(),
									debitAmount, creditAmount, mDrDetails.getDrClass(),
									mDrDetails.getDrCoaAccountDescription());

            				apDrList.setDrClassList(classList);

            				actionForm.saveApVOUList(apDrList);

            			}

            			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

            			for (int x = list.size() + 1; x <= remainingList; x++) {

            				ApVoucherEntryList apVOUList = new ApVoucherEntryList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null);

            				apVOUList.setDrClassList(this.getDrClassList());

            				actionForm.saveApVOUList(apVOUList);

            			}

            			this.setFormProperties(actionForm, user.getCompany());

            			if (request.getParameter("child") == null) {

            				return (mapping.findForward("apVoucherEntry"));

            			} else {

            				return (mapping.findForward("apVoucherEntryChild"));

            			}

            		}

            	}

            	// Populate line when not forwarding

            	if (user.getUserApps().contains("OMEGA INVENTORY") && (actionForm.getType() == null || actionForm.getType().equals("ITEMS"))) {

	            	for (int x = 1; x <= journalLineNumber; x++) {

	            		ApVoucherLineItemList apVLIList = new ApVoucherLineItemList(actionForm,
	            				null, new Integer(x).toString(), null, null, null, null, null,
								null, null, "0.0","0.0", "0.0", "0.0", null, null,
								false, false, false, null,null,null, null, null, null);

	            		apVLIList.setLocationList(actionForm.getLocationList());

	            		apVLIList.setUnitList(Constants.GLOBAL_BLANK);
	            		apVLIList.setUnitList("Select Item First");

	            		actionForm.saveApVLIList(apVLIList);

	            	}

            	} else if (user.getUserApps().contains("OMEGA INVENTORY") && actionForm.getType().equals("PO MATCHED")){

            		for (int x = 1; x <= journalLineNumber; x++) {

            			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(actionForm,
            					null, null, null, null, null, null,"0.0","0.0", null, null,
								null, null, "0.0","0.0", "0.0", "0.0", null, null);

            			apVPOList.setLocationList(actionForm.getLocationList());

            			apVPOList.setUnitList(Constants.GLOBAL_BLANK);
            			apVPOList.setUnitList("Select Item First");

            			actionForm.saveApVPOList(apVPOList);

            		}
            	}

            } catch(GlobalNoRecordFoundException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("voucherEntry.error.voucherAlreadyDeleted"));

            } catch(EJBException ex) {

            	if (log.isInfoEnabled()) {

            		log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
            				" session: " + session.getId());
            	}

               return(mapping.findForward("cmnErrorPage"));

            }



            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveSubmitButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   try {

                   	   ArrayList list = ejbVOU.getAdApprovalNotifiedUsersByVouCode(actionForm.getVoucherCode(), user.getCmpCode());

                   	   if (list.isEmpty()) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));

                   	   } else if (list.contains("DOCUMENT POSTED")) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                	   	       new ActionMessage("messages.documentPosted"));

                   	   } else {

                   	   	   Iterator i = list.iterator();

                   	   	   String APPROVAL_USERS = "";

                   	   	   while (i.hasNext()) {

                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

                   	   	       if (i.hasNext()) {

                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";

                   	   	       }


                   	   	   }

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

                   	   }

                   	   saveMessages(request, messages);
                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);


                   } catch(EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }

		               return(mapping.findForward("cmnErrorPage"));

		           }

               } else if (request.getParameter("saveAsDraftButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }

            }
            System.out.println("RESET 1");
            actionForm.reset(mapping, request);

            if (actionForm.getType() == null) {

               if (user.getUserApps().contains("OMEGA INVENTORY")) {

                   actionForm.setType("PO MATCHED");

               } else {

                   actionForm.setType("EXPENSES");

               }

            }

            if (actionForm.getType().equals("PO MATCHED")) {

            	actionForm.clearApVOUList();
	           	actionForm.clearApVLIList();
	           	actionForm.clearApVPOList();

	           	ArrayList list = new ArrayList();

         		Iterator  i = null;

         		list = ejbVOU.getApOpenPl(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         		i = list.iterator();

         		while (i.hasNext()) {

         			ApModPurchaseOrderLineDetails mdetails = (ApModPurchaseOrderLineDetails)i.next();

         			ApVoucherPurchaseOrderLineList apVPOList = new ApVoucherPurchaseOrderLineList(
         					actionForm, mdetails.getPlCode(),
							mdetails.getPlPoDocumentNumber(), mdetails.getPlPoReceivingPoNumber(),
							mdetails.getPlLocName(), mdetails.getPlIiName(),
							mdetails.getPlIiDescription(),
							Common.convertDoubleToStringMoney(mdetails.getPlPoAmount(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlPoQuantity(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlQuantity(), precisionUnit),
							mdetails.getPlUomName(),
							Common.convertDoubleToStringMoney(mdetails.getPlUnitCost(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlAmount(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlDiscount1(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlDiscount2(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlDiscount3(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlDiscount4(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getPlTotalDiscount(), precisionUnit),
							mdetails.getPlMisc());

         			actionForm.saveApVPOList(apVPOList);
         		}
            }

            actionForm.setVoucherCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountPaid(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setConversionRate(Common.convertDoubleToStringMoney(1d, Constants.CONVERSION_RATE_PRECISION));
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

            this.setFormProperties(actionForm, user.getCompany());
            return(mapping.findForward("apVoucherEntry"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

            log.info("Exception caught in ApVoucherEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }


	private void setFormProperties(ApVoucherEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getPosted().equals("NO") && !actionForm.getVoucherVoid()) {

				if (actionForm.getVoucherCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableVoucherVoid(false);

				} else if (actionForm.getVoucherCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableVoucherVoid(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableVoucherVoid(false);

				}

			} else if (actionForm.getPosted().equals("YES")) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setEnableVoucherVoid(false);

			} else if (actionForm.getVoucherVoid()) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableVoucherVoid(false);

			}



		} else {

			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableVoucherVoid(false);

		}

		// view attachment

		if (actionForm.getVoucherCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ap/Voucher/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			File file = new File(attachmentPath + actionForm.getVoucherCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-1" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton1(true);

 			} else {

 				actionForm.setShowViewAttachmentButton1(false);

 			}

 			file = new File(attachmentPath + actionForm.getVoucherCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-2" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton2(true);

 			} else {

 				actionForm.setShowViewAttachmentButton2(false);

 			}

 			file = new File(attachmentPath + actionForm.getVoucherCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-3" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton3(true);

 			} else {

 				actionForm.setShowViewAttachmentButton3(false);

 			}

 			file = new File(attachmentPath + actionForm.getVoucherCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getVoucherCode() + "-4" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton4(true);

 			} else {

 				actionForm.setShowViewAttachmentButton4(false);

 			}

		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}


	}

	private ArrayList getDrClassList() {

		ArrayList classList = new ArrayList();

     	classList.add(Constants.AP_DR_CLASS_EXPENSE);
     	classList.add(Constants.AP_DR_CLASS_TAX);
     	classList.add(Constants.AP_DR_CLASS_WITHHOLDING_TAX);
     	classList.add(Constants.AP_DR_CLASS_OTHER);

     	return classList;

	}

}