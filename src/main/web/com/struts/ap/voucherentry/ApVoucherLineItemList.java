package com.struts.ap.voucherentry;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.ap.directcheckentry.ApDirectCheckLineItemTagList;

public class ApVoucherLineItemList implements Serializable {

	private Integer voucherLineItemCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String quantity = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitCost = null;
	private String amount = null;
	private String discount1 = null;
	private String discount2 = null;
	private String discount3 = null;
	private String discount4 = null;
	private String totalDiscount = null;
	private String misc = null;

	private boolean isVatRelief = false;
	private boolean isTax = false;
	private boolean isProject = false;

	private boolean isTraceMisc = false;

	private ArrayList tagList = new ArrayList();

	private String supplierName = null;
	private String supplierTin = null;
	private String supplierAddress = null;

	private String projectCode = null;
	private ArrayList projectCodeList = new ArrayList();
	private String projectTypeCode = null;
	private ArrayList projectTypeCodeList = new ArrayList();
	private String projectPhaseName = null;
	private ArrayList projectPhaseNameList = new ArrayList();


	private boolean deleteCheckbox = false;

	private String isItemEntered = null;
	private String isUnitEntered = null;
	private String isProjectCodeEntered = null;
	private String isProjectTypeCodeEntered = null;



	private ApVoucherEntryForm parentBean;

	public ApVoucherLineItemList(ApVoucherEntryForm parentBean,
			Integer voucherLineItemCode,
			String lineNumber,
			String location,
			String itemName,
			String itemDescription,
			String quantity,
			String unit,
			String unitCost,
			String amount,
			String discount1,
			String discount2,
			String discount3,
			String discount4,
			String totalDiscount,
			String misc,
			boolean isVatRelief,
			boolean isTax,
			boolean isProject,
			String supplierName,
			String supplierTin,
			String supplierAddress,
			String projectCode,
			String projectTypeCode,
			String projectPhaseName

			){

		this.parentBean = parentBean;
		this.voucherLineItemCode = voucherLineItemCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.quantity = quantity;
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.totalDiscount = totalDiscount;
		this.misc = misc;
		this.isVatRelief = isVatRelief;
		this.isTax = isTax;
		this.isProject = isProject;
		this.supplierName = supplierName;
		this.supplierTin = supplierTin;
		this.supplierAddress = supplierAddress;
		this.projectCode= projectCode;
		this.projectTypeCode = projectTypeCode;
		this.projectPhaseName = projectPhaseName;

	}

	public Integer getVoucherLineItemCode(){

		return(voucherLineItemCode);

	}

	public String getLineNumber() {

		return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

		this.lineNumber = lineNumber;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

		this.itemDescription = itemDescription;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(ArrayList locationList) {

		this.locationList = locationList;

	}

	public String getQuantity() {

		return quantity;

	}

	public void setQuantity(String quantity) {

		this.quantity = quantity;

	}

	public String getUnit() {

		return unit;

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}

	public ArrayList getUnitList() {

		return unitList;

	}

	public void setUnitList(String unit) {

		unitList.add(unit);

	}

	public void clearUnitList() {

		unitList.clear();

	}

	public String getUnitCost() {

		return unitCost;

	}

	public void setUnitCost(String unitCost) {

		this.unitCost = unitCost;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}

	public boolean getDeleteCheckbox() {

		return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

		this.deleteCheckbox = deleteCheckbox;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowVLISelected(this, false);

		}

		isItemEntered = null;

	}

	public String getIsUnitEntered() {

		return isUnitEntered;

	}

	public void setIsUnitEntered(String isUnitEntered) {

		if (isUnitEntered != null && isUnitEntered.equals("true")) {

			parentBean.setRowVLISelected(this, false);

		}

		isUnitEntered = null;

	}


	public String getIsProjectCodeEntered() {

		return isProjectCodeEntered;

	}

	public void setIsProjectCodeEntered(String isProjectCodeEntered) {

		if (isProjectCodeEntered != null && isProjectCodeEntered.equals("true")) {

			parentBean.setRowVLISelected(this, false);

		}

		isProjectCodeEntered = null;

	}

	public String getIsProjectTypeCodeEntered() {

		return isProjectTypeCodeEntered;

	}

	public void setIsProjectTypeCodeEntered(String isProjectTypeCodeEntered) {

		if (isProjectTypeCodeEntered != null && isProjectTypeCodeEntered.equals("true")) {

			parentBean.setRowVLISelected(this, false);

		}

		isProjectTypeCodeEntered = null;

	}

	public String getDiscount1() {

		return discount1;

	}

	public void setDiscount1(String discount1) {

		this.discount1 = discount1;

	}

	public String getDiscount2() {

		return discount2;

	}

	public void setDiscount2(String discount2) {

		this.discount2 = discount2;

	}

	public String getDiscount3() {

		return discount3;

	}

	public void setDiscount3(String discount3) {

		this.discount3 = discount3;

	}

	public String getDiscount4() {

		return discount4;

	}

	public void setDiscount4(String discount4) {

		this.discount4 = discount4;

	}

	public String getTotalDiscount() {

		return totalDiscount;

	}

	public void setTotalDiscount(String totalDiscount) {

		this.totalDiscount = totalDiscount;

	}

	public String getMisc() {
		System.out.println("misc" + misc);
		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("misc" + misc);

	}


	public boolean getIsVatRelief(){

	    return isVatRelief;

	}

	public void setIsVatRelief(boolean isVatRelief){

	    this.isVatRelief = isVatRelief;

	}

	public boolean getIsTax() {
		return isTax;
	}

	public void setIsTax(boolean isTax) {
		this.isTax = isTax;
	}

	public boolean getIsTraceMisc() {
		return isTraceMisc;
	}

	public void setIsTraceMisc(boolean isTraceMisc) {
		this.isTraceMisc = isTraceMisc;
	}



	public boolean getIsProject() {
		return isProject;
	}




	public void setIsProject(boolean isProject) {
		this.isProject = isProject;
	}



	public ApVoucherLineItemTagList getTagListByIndex(int index){
	      return((ApVoucherLineItemTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}


public String getSupplierName() {

		return supplierName;

	}

	public void setSupplierName(String supplierName) {

		this.supplierName = supplierName;

	}

public String getSupplierTin() {

		return supplierTin;

	}

	public void setSupplierTin(String supplierTin) {

		this.supplierTin = supplierTin;

	}

	public String getSupplierAddress() {

		return supplierAddress;

	}

	public void setSupplierAddress(String supplierAddress) {

		this.supplierAddress = supplierAddress;

	}

	public String getProjectCode() {

		return projectCode;

	}

	public void setProjectCode(String projectCode) {

		this.projectCode = projectCode;

	}

	public ArrayList getProjectCodeList() {

		return projectCodeList;

	}

	public void setProjectCodeList(String projectName) {

		projectCodeList.add(projectName);

	}

	public void clearProjectNameList() {

		projectCodeList.clear();

	}


	public String getProjectTypeCode() {

		return projectTypeCode;

	}

	public void setProjectTypeCode(String projectTypeCode) {

		this.projectTypeCode = projectTypeCode;

	}

	public ArrayList getProjectTypeCodeList() {

		return projectTypeCodeList;

	}

	public void setProjectTypeCodeList(String projectTypeCode) {

		projectTypeCodeList.add(projectTypeCode);

	}

	public void clearProjectTypeCodeList() {

		projectTypeCodeList.clear();

	}


	public String getProjectPhaseName() {

		return projectPhaseName;

	}

	public void setProjectPhaseName(String projectPhaseName) {

		this.projectPhaseName = projectPhaseName;

	}

	public ArrayList getProjectPhaseNameList() {

		return projectPhaseNameList;

	}

	public void setProjectPhaseNameList(String projectPhaseName) {

		projectPhaseNameList.add(projectPhaseName);

	}

	public void clearProjectPhaseNameList() {

		projectPhaseNameList.clear();

	}




}