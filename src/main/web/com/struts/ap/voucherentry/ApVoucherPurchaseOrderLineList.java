package com.struts.ap.voucherentry;

import java.io.Serializable;
import java.util.ArrayList;

public class ApVoucherPurchaseOrderLineList implements Serializable {

	private Integer voucherPurchaseOrderLineCode = null;
	private String docNumber = null;
	private String poNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String poQuantity = null;
	private String poAmount = null;
	private String quantity = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitCost = null;
	private String amount = null;
	private String discount1 = null;
	private String discount2 = null;
	private String discount3 = null;
	private String discount4 = null;
	private String totalDiscount = null;

	private boolean isTraceMisc = false;
	private boolean issueCheckbox = false;
	private boolean allPoCheckbox = false;
	private boolean allCheckbox = false;

	private ArrayList tagList = new ArrayList();

	private String isItemEntered = null;
	private String isUnitEntered = null;

	private String poConversionRate;
	private String poConversionDate;
	private ApVoucherEntryForm parentBean;

	private String misc = null;

	public ApVoucherPurchaseOrderLineList(ApVoucherEntryForm parentBean,
			Integer voucherPurchaseOrderLineCode,
			String docNumber,
			String poNumber,
			String location,
			String itemName,
			String itemDescription,
			String poAmount,
			String poQuantity,
			String quantity,
			String unit,
			String unitCost,
			String amount,
			String discount1,
			String discount2,
			String discount3,
			String discount4,
			String totalDiscount,
			String misc){

		this.parentBean = parentBean;
		this.voucherPurchaseOrderLineCode = voucherPurchaseOrderLineCode;
		this.docNumber = docNumber;
		this.poNumber = poNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.poQuantity = poQuantity;
		this.poAmount = poAmount;
		this.quantity = quantity;
		this.unit = unit;
		this.unitCost = unitCost;
		this.amount = amount;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.totalDiscount = totalDiscount;
		this.misc = misc;

	}

	public Integer getVoucherPurchaseOrderLineCode(){

		return (voucherPurchaseOrderLineCode);

	}

	public String getDocNumber() {

		return docNumber;

	}

	public void setDocNumber(String docNumber) {

		this.docNumber = docNumber;

	}

	public String getPoNumber() {

		return poNumber;

	}

	public void setPoNumber(String poNumber) {

		this.poNumber = poNumber;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

		this.itemDescription = itemDescription;

	}


	public String getPoQuantity() {

		return poQuantity;

	}

	public void setPoQuantity(String poQuantity) {

		this.poQuantity = poQuantity;

	}


	public String getPoAmount() {

		return poAmount;

	}

	public void setPoAmount(String poAmount) {

		this.poAmount = poAmount;

	}


	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(ArrayList locationList) {

		this.locationList = locationList;

	}

	public String getQuantity() {

		return quantity;

	}

	public void setQuantity(String quantity) {

		this.quantity = quantity;

	}

	public String getUnit() {

		return unit;

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}

	public ArrayList getUnitList() {

		return unitList;

	}

	public void setUnitList(String unit) {

		unitList.add(unit);

	}

	public void clearUnitList() {

		unitList.clear();

	}

	public String getUnitCost() {

		return unitCost;

	}

	public void setUnitCost(String unitCost) {

		this.unitCost = unitCost;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}


	public boolean getIsTraceMisc() {

		return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

		this.isTraceMisc = isTraceMisc;

	}


	public boolean getIssueCheckbox() {

		return issueCheckbox;

	}

	public void setIssueCheckbox(boolean issueCheckbox) {

		this.issueCheckbox = issueCheckbox;

	}

	public boolean getAllPoCheckbox() {

		return allPoCheckbox;

	}

	public void setAllPoCheckbox(boolean allPoCheckbox) {

		this.allPoCheckbox = allPoCheckbox;

	}

	public boolean getAllCheckbox() {

		return allCheckbox;

	}

	public void setAllCheckbox(boolean allCheckbox) {

		this.allCheckbox = allCheckbox;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowVPOSelected(this, false);

		}

		isItemEntered = null;

	}

	public void setDiscount1(String discount1) {

		this.discount1 = discount1;

	}

	public String getDiscount2() {

		return discount2;

	}

	public void setDiscount2(String discount2) {

		this.discount2 = discount2;

	}

	public String getDiscount3() {

		return discount3;

	}

	public void setDiscount3(String discount3) {

		this.discount3 = discount3;

	}

	public String getDiscount4() {

		return discount4;

	}

	public void setDiscount4(String discount4) {

		this.discount4 = discount4;

	}

	public String getTotalDiscount() {

		return totalDiscount;

	}

	public void setTotalDiscount(String totalDiscount) {

		this.totalDiscount = totalDiscount;

	}

	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;

	}

	public String getPoConversionRate() {
		return poConversionRate;
	}

	public void setPoConversionRate(String poConversionRate) {
		this.poConversionRate = poConversionRate;
	}

	public String getPoConversionDate() {
		return poConversionDate;
	}

	public void setPoConversionDate(String poConversionDate) {
		this.poConversionDate = poConversionDate;
	}


	public ApVoucherPurchaseOrderLineTagList getTagListByIndex(int index){
	      return((ApVoucherPurchaseOrderLineTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}

}