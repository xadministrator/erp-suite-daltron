package com.struts.ap.voucherimport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalReferenceNumberNotUniqueException;
import com.ejb.txn.ApVoucherImportController;
import com.ejb.txn.ApVoucherImportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModVoucherDetails;
import com.util.ApModVoucherLineItemDetails;

public final class ApVoucherImportAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
        
        
        try {
        	
/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApVoucherImportAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }

        } else {

            if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ApVoucherImportForm actionForm = (ApVoucherImportForm)form;
        
        String frParam = Common.getUserPermission(user, Constants.AP_VOUCHER_IMPORT_ID);

        if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

		        ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	            if (!fieldErrors.isEmpty()) {
	
	                saveErrors(request, new ActionMessages(fieldErrors));
	
	                return mapping.findForward("apVoucherImport");
	                
	            }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ApVoucherImportController EJB
*******************************************************/

        ApVoucherImportControllerHome homeVI = null;
        ApVoucherImportController ejbVI = null;

        try {

            homeVI = (ApVoucherImportControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApVoucherImportControllerEJB", ApVoucherImportControllerHome.class);
             
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApVoucherImportAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbVI = homeVI.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApVoucherImportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
/*******************************************************
    Call ApVoucherImportController EJB
    getGlFcPrecisionUnit
    getInvGpQuantityPrecisionUnit
*******************************************************/

        short precisionUnit = 0;
        short quantityPrecisionUnit = 0;
        
        try {
        	
        	precisionUnit = ejbVI.getGlFcPrecisionUnit(user.getCmpCode());
        	quantityPrecisionUnit = ejbVI.getInvGpQuantityPrecisionUnit(user.getCmpCode());
        	precisionUnit = (short)(10);
        	quantityPrecisionUnit = (short)(10);
        	
        } catch(EJBException ex) {
        	
        	if (log.isInfoEnabled()) {
        		
        		log.info("EJBException caught in ApVoucherImportAction.execute(): " + ex.getMessage() +
        				" session: " + session.getId());
        	}
        	
        	return(mapping.findForward("cmnErrorPage"));
        	
        }
                	
/*******************************************************
   -- Ap VI Import Action --
*******************************************************/
        	
          if (request.getParameter("importButton") != null &&  
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {            
            
            ArrayList headerList = new ArrayList();
        	ArrayList detailsList = new ArrayList();
        	
        	CSVParser csvParser1 = new CSVParser(actionForm.getHeaderFile().getInputStream());
        	CSVParser csvParser2 = new CSVParser(actionForm.getDetailsFile().getInputStream());
        	
        	String[][] header = csvParser1.getAllValues();
        	String[][] details = csvParser2.getAllValues();
        	
        	LinkedHashSet set = new LinkedHashSet();
        	
        	for (int i=1; i < header.length; i++) {
        		
        		ApModVoucherDetails mdetails = new ApModVoucherDetails();
                
        		mdetails.setVouCode(null);
        		mdetails.setVouSplName(header[i][1].trim());
        		mdetails.setVouDate(Common.convertStringToSQLDate(header[i][0].trim()));
                mdetails.setVouDocumentNumber(header[i][2].trim());
                mdetails.setVouReferenceNumber(header[i][2].trim());
                mdetails.setVouVoid(Common.convertBooleanToByte(false));
                mdetails.setVouDescription(header[i][3].trim());           
                mdetails.setVouConversionDate(null);
                mdetails.setVouConversionRate(Common.convertStringMoneyToDouble("1.000000", Constants.MONEY_RATE_PRECISION));
                mdetails.setVouPoNumber(null);  
                mdetails.setVouCreatedBy(user.getUserName());
  	           	mdetails.setVouDateCreated(new java.util.Date());
  	           	mdetails.setVouLastModifiedBy(user.getUserName());
  	           	mdetails.setVouDateLastModified(new java.util.Date());
  	           	mdetails.setVouAmountDue(Common.convertStringMoneyToDouble(header[i][4].trim(), precisionUnit));
  	           	
  	           	headerList.add(mdetails);
  	           	
        	}
        	
        	for (int i=1; i < details.length; i++) {
        		
        		ApModVoucherLineItemDetails mVliDetails = new ApModVoucherLineItemDetails();
        		
        		mVliDetails.setVliVouDocumentNumber(details[i][0].trim());
        		mVliDetails.setVliIiName(details[i][1].trim());
        		mVliDetails.setVliQuantity(Common.convertStringMoneyToDouble(details[i][3].trim(), quantityPrecisionUnit));           	   
        		mVliDetails.setVliUnitCost(Common.convertStringMoneyToDouble(details[i][4].trim(), precisionUnit));
        		mVliDetails.setVliAmount(Common.convertStringMoneyToDouble(details[i][5].trim(), precisionUnit));
        		
        		detailsList.add(mVliDetails);
        		
        	}
        	
        	Collections.sort(headerList, sortVouByDocumentNumber);
    		Collections.sort(detailsList, sortVliByDocumentNumber);
        	
    		ArrayList list = new ArrayList();
    		
    		Iterator hdrItr = headerList.iterator();
    		
    		while(hdrItr.hasNext()) {
    			
    			ApModVoucherDetails mdetails = (ApModVoucherDetails)hdrItr.next();
    			
    			short lineNumber = 0;
    			
    			ArrayList vliList = new ArrayList();
    			
    			Iterator dtlItr = detailsList.iterator();
    			
    			while (dtlItr.hasNext()) {
    				
    				ApModVoucherLineItemDetails mVliDetails = (ApModVoucherLineItemDetails)dtlItr.next();
    				
    				if(mVliDetails.getVliVouDocumentNumber().equals(mdetails.getVouDocumentNumber())) {
    					
    					mVliDetails.setVliLine(lineNumber++);
    					vliList.add(mVliDetails);
    					
    				}
    				
    			}
    			
    			mdetails.setVouVliList(vliList);
    			
    			list.add(mdetails);
    			
    		}
    		
    		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
    		String VB_NM = "IMPORT " + formatter.format(new java.util.Date());
    		
    		if (headerList.isEmpty() || detailsList.isEmpty()) {
    			
    			errors.add(ActionMessages.GLOBAL_MESSAGE,
    					new ActionMessage("voucherImport.error.noVoucherToImport"));
    			
    		} else {
    			
    			try {
    				
    				ejbVI.importVoucher(list, "IMMEDIATE", "VAT INCLUSIVE", "NONE", "PHP", VB_NM,
    						new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    				
    			} catch (GlobalDocumentNumberNotUniqueException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("voucherImport.error.documentNumberNotUnique", ex.getMessage()));
    				
    			} catch (GlobalPaymentTermInvalidException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("voucherEntry.error.paymentTermInvalid"));  
    				
    			} catch (GlobalInvItemLocationNotFoundException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("voucherImport.error.noItemLocationFound", ex.getMessage()));
    				
    			} catch (GlobalReferenceNumberNotUniqueException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("voucherEntry.error.referenceNumberNotUnique"));
    				
    			} catch (GlobalNoRecordFoundException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("voucherImport.error.supplierNotExist", ex.getMessage()));
    				
    			} catch (GlobalJournalNotBalanceException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("voucherImport.error.amountNotBalance", ex.getMessage()));
    				
    			} catch (EJBException ex) {
    				if (log.isInfoEnabled()) {
    					
    					log.info("EJBException caught in ApVoucherImportAction.execute(): " + ex.getMessage() +
    							" session: " + session.getId());
    				}
    				
    				return(mapping.findForward("cmnErrorPage"));
    			}
    			
    		}
    		csvParser1.close();
        	csvParser2.close();
          
/*******************************************************
   -- Ap VI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap VI Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {
            	
            	if ((request.getParameter("importButton") != null) && 
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            		
            		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            		
            	}
            	
            }
            
            actionForm.reset(mapping, request);
            
            return(mapping.findForward("apVoucherImport"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	
      	  e.printStackTrace();

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApVoucherImportAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
    
	private static Comparator sortVouByDocumentNumber = new Comparator() {
		
		public int compare(Object r1, Object r2) {
			
		    ApModVoucherDetails voucher1 = (ApModVoucherDetails) r1;
		    ApModVoucherDetails voucher2 = (ApModVoucherDetails) r2;
		    		    
			return voucher1.getVouDocumentNumber().compareTo(voucher2.getVouDocumentNumber());
			
		}
		
	};
	
	private static Comparator sortVliByDocumentNumber = new Comparator() {
		
		public int compare(Object r1, Object r2) {
		    
			ApModVoucherLineItemDetails voucher1 = (ApModVoucherLineItemDetails) r1;
			ApModVoucherLineItemDetails voucher2 = (ApModVoucherLineItemDetails) r2;
		    
			return voucher1.getVliVouDocumentNumber().compareTo(voucher2.getVliVouDocumentNumber());
			
		}
		
	};
}