package com.struts.ap.voucherimport;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ApVoucherImportForm extends ActionForm implements Serializable {
	
	private FormFile headerFile = null;
	private FormFile detailsFile = null;
	private String importButton = null;
	private String closeButton = null;
	
	private String pageState = new String();
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public void setImportButton(String importButton) {
		
		this.importButton = importButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public FormFile getHeaderFile() {
		
		return headerFile;
		
	}
	
	public void setHeaderFile(FormFile headerFile) {
		
		this.headerFile = headerFile;
		
	}
	
	public FormFile getDetailsFile() {
		
		return detailsFile;
		
	}
	
	public void setDetailsFile(FormFile detailsFile) {
		
		this.detailsFile = detailsFile;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		headerFile = null;
		detailsFile = null;
		importButton = null;
		closeButton = null;		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("importButton") != null) {
			
			if(Common.validateRequired(headerFile.getFileName())){
				errors.add("headerFile",
						new ActionMessage("voucherImport.error.headerFileRequired"));
			}
			
			if(Common.validateRequired(detailsFile.getFileName())){
				errors.add("detailsFile",
						new ActionMessage("voucherImport.error.detailsFileRequired"));
			}
			
			if(!headerFile.getFileName().substring(headerFile.getFileName().indexOf(".") + 1, headerFile.getFileName().length()).equalsIgnoreCase("CSV")){
				errors.add("headerFile",
						new ActionMessage("voucherImport.error.filenameInvalid"));
			}
			
			if(!detailsFile.getFileName().substring(detailsFile.getFileName().indexOf(".") + 1, detailsFile.getFileName().length()).equalsIgnoreCase("CSV")){
				errors.add("detailsFile",
						new ActionMessage("voucherImport.error.filenameInvalid"));
			}
			
		}
		
		return errors;
		
	}
}