package com.struts.ap.voucherpost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApVoucherPostController;
import com.ejb.txn.ApVoucherPostControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModVoucherDetails;

public final class ApVoucherPostAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApVoucherPostAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApVoucherPostForm actionForm = (ApVoucherPostForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_VOUCHER_POST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apVoucherPost");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApVoucherPostController EJB
*******************************************************/

         ApVoucherPostControllerHome homeVP = null;
         ApVoucherPostController ejbVP = null;

         try {
          
            homeVP = (ApVoucherPostControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApVoucherPostControllerEJB", ApVoucherPostControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApVoucherPostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbVP = homeVP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApVoucherPostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call ApVoucherPostController EJB
     getGlFcPrecisionUnit
  *******************************************************/

         short precisionUnit = 0;
         boolean enableVoucherBatch = false;
         boolean useSupplierPulldown = true;
         
         try { 
         	
            precisionUnit = ejbVP.getGlFcPrecisionUnit(user.getCmpCode());
            precisionUnit = (short)(2);
            enableVoucherBatch = Common.convertByteToBoolean(ejbVP.getAdPrfEnableApVoucherBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableVoucherBatch);
            useSupplierPulldown = Common.convertByteToBoolean(ejbVP.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ApVoucherPostAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ap VP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apVoucherPost"));
	     
/*******************************************************
   -- Ap VP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apVoucherPost"));         

/*******************************************************
   -- Ap VP Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ap VP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Ap VP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();   
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}	        	
                
	        	if (!Common.validateRequired(actionForm.getSupplierCode())) {
	        		
	        		criteria.put("supplierCode", actionForm.getSupplierCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	

	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	        	
	        	if (actionForm.getDebitMemo()) {	        	
		        	   		        		
	        		criteria.put("debitMemo", new Byte((byte)1));
                
                }
                
                if (!actionForm.getDebitMemo()) {
                	
                	criteria.put("debitMemo", new Byte((byte)0));
                	
                }
                	 
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbVP.getApVouPostableByCriteria(actionForm.getCriteria(),                	    
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ApVoucherPostAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	     	}
            
            try {
            	
            	actionForm.clearApVPList();
            	
            	ArrayList list = ejbVP.getApVouPostableByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModVoucherDetails mdetails = (ApModVoucherDetails)i.next();
            		
            		ApVoucherPostList apVPList = new ApVoucherPostList(actionForm,
            		    mdetails.getVouCode(),
            		    mdetails.getVouSplSupplierCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getVouDate()),
                        mdetails.getVouDocumentNumber(),
                        mdetails.getVouReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),
						mdetails.getVouType());
            		    
            		actionForm.saveApVPList(apVPList);
            		
            	}
            	
            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("voucherPost.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherPostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apVoucherPost");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("apVoucherPost")); 

/*******************************************************
   -- Ap VP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap VP Post Action --
*******************************************************/

         } else if (request.getParameter("postButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get posted journals
                        
		    for(int i=0; i<actionForm.getApVPListSize(); i++) {
		    
		       ApVoucherPostList actionList = actionForm.getApVPByIndex(i);
		    	
               if (actionList.getPost()) {
               	
               	     try {
               	     	
               	     	ejbVP.executeApVouPost(actionList.getVoucherCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteApVPList(i);
               	     	i--;
               	     	
               	     } catch (GlJREffectiveDateNoPeriodExistException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherPost.error.effectiveDateNoPeriod", actionList.getDocumentNumber()));
		                  
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherPost.error.effectiveDatePeriodClosed", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalJournalNotBalanceException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherPost.error.journalNotBalance", actionList.getDocumentNumber()));
		                  		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherPost.error.transactionAlreadyPosted", actionList.getDocumentNumber()));
		             
		             } catch (GlobalTransactionAlreadyVoidException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherPost.error.transactionAlreadyVoid", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("voucherPost.error.recordAlreadyDeleted", actionList.getDocumentNumber()));
		               
		             
		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		            	
		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("voucherPost.error.noNegativeInventoryCostingCOA"));

		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ApVoucherPostAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }	
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apVoucherPost");

            }
	        
	        try {
            	
            	
				actionForm.setLineCount(0);
            	actionForm.clearApVPList();
            	
            	ArrayList list = ejbVP.getApVouPostableByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ApModVoucherDetails mdetails = (ApModVoucherDetails)i.next();
            		
            		ApVoucherPostList apVPList = new ApVoucherPostList(actionForm,
            		    mdetails.getVouCode(),
            		    mdetails.getVouSplSupplierCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getVouDate()),
                        mdetails.getVouDocumentNumber(),
                        mdetails.getVouReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getVouAmountDue(), precisionUnit),
						mdetails.getVouType());
            		    
            		actionForm.saveApVPList(apVPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherPostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }	                       
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("apVoucherPost")); 	     

/*******************************************************
   -- Ap VP Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apVoucherPost");

            }
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseSupplierPulldown()) {
            	
            		actionForm.clearSupplierCodeList();           	
            	
	            	list = ejbVP.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	if (list == null || list.size() == 0) {
	            		
	            		actionForm.setSupplierCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
	            		
	            	} else {
	            		           		            		
	            		i = list.iterator();
	            		
	            		while (i.hasNext()) {
	            			
	            		    actionForm.setSupplierCodeList((String)i.next());
	            			
	            		}
	            		
	            	}
	            	
            	}

            	actionForm.clearCurrencyList();           	
            	
            	list = ejbVP.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbVP.getApOpenVbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearApVPList();
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("apVoucherPost"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ApVoucherPostAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}