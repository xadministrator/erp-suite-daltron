package com.struts.ap.withholdingtaxcodes;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ApWithholdingTaxCodeController;
import com.ejb.txn.ApWithholdingTaxCodeControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApModWithholdingTaxCodeDetails;
import com.util.ApWithholdingTaxCodeDetails;

public final class ApWithholdingTaxCodeAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ApWithholdingTaxCodeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ApWithholdingTaxCodeForm actionForm = (ApWithholdingTaxCodeForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_WITHHOLDING_TAX_CODE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("apWithholdingTaxCodes");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ApWithholdingTaxCodeController EJB
*******************************************************/

         ApWithholdingTaxCodeControllerHome homeWTC = null;
         ApWithholdingTaxCodeController ejbWTC = null;

         try {

            homeWTC = (ApWithholdingTaxCodeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ApWithholdingTaxCodeControllerEJB", ApWithholdingTaxCodeControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ApWithholdingTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbWTC = homeWTC.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ApWithholdingTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ap WTC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("apWithholdingTaxCodes"));
	     
/*******************************************************
   -- Ap WTC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("apWithholdingTaxCodes"));                  
         
/*******************************************************
   -- Ap WTC Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ApWithholdingTaxCodeDetails details = new ApWithholdingTaxCodeDetails();
            details.setWtcName(actionForm.getName());
            details.setWtcDescription(actionForm.getDescription());
            details.setWtcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setWtcEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            
            try {
            	
            	ejbWTC.addApWtcEntry(details, actionForm.getAccount(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("withholdingTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("withholdingTaxCode.error.accountNumberInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ap WTC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap WTC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            ApWithholdingTaxCodeList apWTCList =
	            actionForm.getApWTCByIndex(actionForm.getRowSelected());
            
            
            ApWithholdingTaxCodeDetails details = new ApWithholdingTaxCodeDetails();
            details.setWtcCode(apWTCList.getWithholdingTaxCode());
            details.setWtcName(actionForm.getName());
            details.setWtcDescription(actionForm.getDescription());
            details.setWtcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setWtcEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            try {
            	
            	ejbWTC.updateApWtcEntry(details, actionForm.getAccount(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("withholdingTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("withholdingTaxCode.error.accountNumberInvalid"));            
               
            } catch (GlobalRecordAlreadyAssignedException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("withholdingTaxCode.error.updateWithholdingTaxCodeAlreadyAssigned"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ap WTC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ap WTC Edit Action --
*******************************************************/

         } else if (request.getParameter("apWTCList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showApWTCRow(actionForm.getRowSelected());
            
            return mapping.findForward("apWithholdingTaxCodes");
            
/*******************************************************
   -- Ap WTC Delete Action --
*******************************************************/

         } else if (request.getParameter("apWTCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ApWithholdingTaxCodeList apWTCList =
              actionForm.getApWTCByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbWTC.deleteApWtcEntry(apWTCList.getWithholdingTaxCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("withholdingTaxCode.error.deleteWithholdingTaxCodeAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("withholdingTaxCode.error.withholdingTaxCodeAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ap WTC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("apWithholdingTaxCodes");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               actionForm.clearApWTCList();	        
	           
	           list = ejbWTC.getApWtcAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              ApModWithholdingTaxCodeDetails mdetails = (ApModWithholdingTaxCodeDetails)i.next();
	            	
	              ApWithholdingTaxCodeList apWTCList = new ApWithholdingTaxCodeList(actionForm,
	            	    mdetails.getWtcCode(),
	            	    mdetails.getWtcName(),
	            	    mdetails.getWtcDescription(),
	            	    Common.convertDoubleToStringMoney(mdetails.getWtcRate(), Constants.MONEY_RATE_PRECISION),
                        mdetails.getWtcCoaGlWithholdingTaxAccountNumber(),
                        mdetails.getWtcCoaGlWithholdingTaxAccountDescription(),
                        Common.convertByteToBoolean(mdetails.getWtcEnable()));                            
	            	 	            	    
	              actionForm.saveApWTCList(apWTCList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ApWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("apWithholdingTaxCodes"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ApWithholdingTaxCodeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}