package com.struts.ap.withholdingtaxcodes;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ApWithholdingTaxCodeForm extends ActionForm implements Serializable {

	private Integer withholdingTaxCode = null;
	private String name = null;
	private String description = null;
	private String rate = null;
	private String account = null;
	private String accountDescription = null;
	private boolean enable = false;
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList apWTCList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
	
	    return rowSelected;
	
	}
	
	public ApWithholdingTaxCodeList getApWTCByIndex(int index) {
	
	    return((ApWithholdingTaxCodeList)apWTCList.get(index));
	
	}
	
	public Object[] getApWTCList() {
	
	    return apWTCList.toArray();
	
	}
	
	public int getApWTCListSize() {
	
	    return apWTCList.size();
	
	}
	
	public void saveApWTCList(Object newApWTCList) {
	
	    apWTCList.add(newApWTCList);
	
	}
	
	public void clearApWTCList() {
		
	    apWTCList.clear();
	  
	}
	
	public void setRowSelected(Object selectedApWTCList, boolean isEdit) {
	
	    this.rowSelected = apWTCList.indexOf(selectedApWTCList);
	
	    if (isEdit) {
	
	        this.pageState = Constants.PAGE_STATE_EDIT;
	
	    }
	
	}
	
	public void showApWTCRow(int rowSelected) {
		
		this.name = ((ApWithholdingTaxCodeList)apWTCList.get(rowSelected)).getName();
		this.description = ((ApWithholdingTaxCodeList)apWTCList.get(rowSelected)).getDescription();
		this.rate = ((ApWithholdingTaxCodeList)apWTCList.get(rowSelected)).getRate();
		this.account = ((ApWithholdingTaxCodeList)apWTCList.get(rowSelected)).getAccount();
		this.accountDescription = ((ApWithholdingTaxCodeList)apWTCList.get(rowSelected)).getAccountDescription();      
		this.enable = ((ApWithholdingTaxCodeList)apWTCList.get(rowSelected)).getEnable(); 
	   
	}
	
	public void updateApWTCRow(int rowSelected, Object newApWTCList) {
	
	    apWTCList.set(rowSelected, newApWTCList);
	
	}
	
	public void deleteApWTCList(int rowSelected) {
	
	    apWTCList.remove(rowSelected);
	
	}
	
	public void setUpdateButton(String updateButton) {
	
	    this.updateButton = updateButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
	
	    this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	    this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	    this.closeButton = closeButton;
	
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
	    this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
	    this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
	
	    this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	    return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getWithholdingTaxCode() {
		
		return withholdingTaxCode;
		
	}
	
	public void setWithholdingTaxCode(Integer withholdingTaxCode) {
		
		this.withholdingTaxCode = withholdingTaxCode;
		
	}
	
	public String getName() {
	
	  	return name;
	
	}
	
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	public String getDescription() {
	
		return description;
	
	}
	
	public void setDescription(String description) {
	
	  	this.description = description;
	
	}
	
	public String getRate() {
	
	  	return rate;
	
	}
	
	public void setRate(String rate) {
	
	  	this.rate = rate;
	
	}
	
	public String getAccount() {
		
		return account;
		
	}
	
	public void setAccount(String account) {
		
		this.account = account;
	
	}

	public String getAccountDescription() {
		
		return accountDescription;
		
	}
	
	public void setAccountDescription(String accountDescription) {
		
		this.accountDescription = accountDescription;
		
	}
	
	public boolean getEnable() {
		
	  	return enable;
	  
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		  
	}
	
	public String getTableType() {
		
	  	return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
	  	this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
	
		name = null;
		description = null;
		rate = null;
		account = null;
		accountDescription = null;
		enable = false;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;      
	
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
			if (Common.validateRequired(name)) {
			
				errors.add("name",
			   		new ActionMessage("withholdingTaxCode.error.nameRequired"));
			
			}
			
			if ((Common.validateMoneyFormat(rate)) && Common.convertStringMoneyToDouble(rate, (short)6) > 0d) {
			
				if (Common.validateRequired(account)) {
				
					errors.add("account",
				   		new ActionMessage("withholdingTaxCode.error.accountRequired"));
				
				}
				
		    }
			
			if (!Common.validateMoneyFormat(rate)) {
			
				errors.add("rate",
			   		new ActionMessage("withholdingTaxCode.error.rateInvalid"));
			
			}
			
	  	}
	     
	  	return errors;
	
	}
	
}