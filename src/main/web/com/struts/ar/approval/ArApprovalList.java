package com.struts.ar.approval;

import java.io.Serializable;

public class ArApprovalList implements Serializable {


   private Integer approvalCode = null;
   private Integer documentCode = null;
   private String document = null;   
   private String date = null;
   private String customerCode = null;   
   private String documentNumber = null;
   private String amount = null;
   private String documentType = null;
   private String reasonForRejection = null;
   private String memo = null;
   private String type;
   private String customerName = null;
   
   private boolean approve = false;
   private boolean reject = false;
          
   private ArApprovalForm parentBean;
    
   public ArApprovalList(ArApprovalForm parentBean,
   	  Integer approvalCode,
      Integer documentCode,
      String document,
      String date,
      String customerCode,
      String documentNumber,
      String amount,
      String documentType,
	  String type,
	  String customerName) {

	  this.approvalCode = approvalCode;
      this.parentBean = parentBean;
      this.documentCode = documentCode;
      this.document = document;
      this.date = date;
      this.customerCode = customerCode;
      this.documentNumber = documentNumber;
      this.amount = amount;
      this.documentType = documentType;
      this.type = type;
      this.customerName = customerName;
      
   }


	public Integer getApprovalCode() {

      return approvalCode;

   }
   
   public void setApprovalCode(Integer approvalCode) {

     this.approvalCode = approvalCode;

   }

   public Integer getDocumentCode() {

      return documentCode;

   }
   
   public String getDocument() {
   	
   	  return document;
   	
   }
   
   public String getDate() {
   
      return date;
      
   }

   public String getCustomerCode() {

      return customerCode;
      
   }
      
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getAmount() {
   
      return amount;
   
   }
   
   public String getDocumentType() {
   	
   	  return documentType;
   	
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }

   public boolean getApprove() {
   	
   	  return approve;
   	
   }
   
   public void setApprove(boolean approve) {
   	
   	  this.approve = approve;
   	
   }
   
   public boolean getReject() {
   	
   	  return reject;
   	
   }
   
   public void setReject(boolean reject) {
   	
   	  this.reject = reject;
   	  
   }
   
   public String getReasonForRejection() {
   	
   	  return reasonForRejection;
   	 
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
   
   public String getMemo() {
	   	
   	  return memo;
   	 
   }
   
   public void setMemo(String memo) {
   	
   	  this.memo = memo;
   	  
   }
   
   public String getCustomerName() {
	   
	   return customerName;
	   
   }
   
   public void setCustomerName(String customerName) {
	   
	   this.customerName = customerName;
	   
   }
         
}