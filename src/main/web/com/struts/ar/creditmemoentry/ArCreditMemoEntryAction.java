package com.struts.ar.creditmemoentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.txn.ArApprovalController;
import com.ejb.txn.ArApprovalControllerHome;
import com.ejb.txn.ArCreditMemoEntryController;
import com.ejb.txn.ArCreditMemoEntryControllerHome;
import com.struts.util.ApproverList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.AdModApprovalQueueDetails;
import com.util.ArInvoiceDetails;
import com.util.ArModDistributionRecordDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ArCreditMemoEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArCreditMemoEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArCreditMemoEntryForm actionForm = (ArCreditMemoEntryForm)form;

	      // reset report
	      
	      actionForm.setReport(null);
	      actionForm.setAttachment(null);
	      actionForm.setAttachmentPDF(null);
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
         	frParam = Common.getUserPermission(user, Constants.AR_CREDIT_MEMO_ENTRY_ID);
         
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arCreditMemoEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ArCreditMemoEntryController EJB
*******************************************************/

         ArCreditMemoEntryControllerHome homeCM = null;
         ArCreditMemoEntryController ejbCM = null;

		 ArApprovalControllerHome homeAA = null;
		 ArApprovalController ejbAA = null;

         try {
            
            homeCM = (ArCreditMemoEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArCreditMemoEntryControllerEJB", ArCreditMemoEntryControllerHome.class);
                
        	homeAA = (ArApprovalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArApprovalControllerEJB", ArApprovalControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArCreditMemoEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbCM = homeCM.create();
            
            ejbAA = homeAA.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ArCreditMemoEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call ArCreditMemoEntryController EJB
   getGlFcPrecisionUnit
   getAdPrfArInvoiceLineNumber
   getAdPrfEnableArCreditMemoBatch
   getInvGpprecisionUnitUnit
   getAdPrfEnableInvShift
   getAdPrfArUseCustomerPulldown
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         
         short precisionUnit = 0;
         short invoiceLineNumber = 0;
         boolean enableCreditMemoBatch = false;
         boolean isInitialPrinting = false;
         
         boolean enableShift = false;
         boolean useCustomerPulldown = true;
         
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ar/Credit Memo/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
         String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
         String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
         try {
         	
            precisionUnit = ejbCM.getGlFcPrecisionUnit(user.getCmpCode());
            invoiceLineNumber = ejbCM.getAdPrfArInvoiceLineNumber(user.getCmpCode());  
            enableCreditMemoBatch = Common.convertByteToBoolean(ejbCM.getAdPrfEnableArCreditMemoBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableCreditMemoBatch);
       
            enableShift = Common.convertByteToBoolean(ejbCM.getAdPrfEnableInvShift(user.getCmpCode()));
            actionForm.setShowShift(enableShift);
            useCustomerPulldown = Common.convertByteToBoolean(ejbCM.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            
            for(int i = 0; i < actionForm.getArCLIListSize(); i++) {
                
                ArCreditMemoLineItemList arCLIList = actionForm.getArCLIByIndex(i);
                
                if(arCLIList.getItemName() != null && arCLIList.getItemName().length() > 0) {
                    
                    boolean isAssemblyItem = ejbCM.getInvItemClassByIiName(arCLIList.getItemName(), user.getCmpCode()).equals("Assembly");
                    arCLIList.setIsAssemblyItem(isAssemblyItem);
                    
                }
            }
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
												     												 
/*******************************************************
   -- Ar CM Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("MEMO LINES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArInvoiceDetails details = new ArInvoiceDetails();
           
           details.setInvCode(actionForm.getCreditMemoCode());    
           details.setInvType(actionForm.getType());
           details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setInvNumber(actionForm.getCmNumber());
           details.setInvCmInvoiceNumber(actionForm.getInvoiceNumber());
           details.setInvCmReferenceNumber(actionForm.getCmReferenceNumber());
           details.setInvAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setInvVoid(Common.convertBooleanToByte(actionForm.getCreditMemoVoid()));
           details.setInvDescription(actionForm.getDescription());    
           details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));

           if (actionForm.getCreditMemoCode() == null) {
           	
           	   details.setInvCreatedBy(user.getUserName());
	           details.setInvDateCreated(new java.util.Date());
	                      
           }
           
           details.setInvLastModifiedBy(user.getUserName());
           details.setInvDateLastModified(new java.util.Date());
           
                      
           ArrayList drList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getArCMListSize(); i++) {
           	
           	   ArCreditMemoEntryList arCMList = actionForm.getArCMByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arCMList.getAccount()) &&
      	 	       Common.validateRequired(arCMList.getDebitAmount()) &&
      	 	       Common.validateRequired(arCMList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(arCMList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(arCMList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(arCMList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();
           	   
           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(arCMList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(arCMList.getAccount());
           	   
           	   drList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           

           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.creditMemoNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("arCreditMemoEntry"));
           	
           }
           
//         validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
    	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
           
           try {
           	
        	   Integer creditMemoCode = ejbCM.saveArInvEntry(details, actionForm.getCustomer(), actionForm.getBatchName(),
           	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
        	   actionForm.setCreditMemoCode(creditMemoCode);
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
                    
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.documentNumberNotUnique"));
                               	                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyVoid"));
           
           } catch (ArINVOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
           	   
           } catch (GlobalAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.accountNumberInvalid", ex.getMessage()));
                               
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyLocked"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.journalNotBalance"));
                               	           	           	
           } catch (GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.branchAccountNumberInvalid"));
           	  
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
        // save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
/*******************************************************
   -- Ar CM Save & Submit Action --
*******************************************************/

        } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("MEMO LINES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArInvoiceDetails details = new ArInvoiceDetails();
           
         
           details.setInvCode(actionForm.getCreditMemoCode()); 
           details.setInvType(actionForm.getType());
           details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setInvNumber(actionForm.getCmNumber());
           details.setInvCmInvoiceNumber(actionForm.getInvoiceNumber());
           details.setInvCmReferenceNumber(actionForm.getCmReferenceNumber());
           details.setInvAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setInvVoid(Common.convertBooleanToByte(actionForm.getCreditMemoVoid()));
           details.setInvDescription(actionForm.getDescription());         
           details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                      
           if (actionForm.getCreditMemoCode() == null) {
           	
           	   details.setInvCreatedBy(user.getUserName());
	           details.setInvDateCreated(new java.util.Date());
	                      
           }
           
           details.setInvLastModifiedBy(user.getUserName());
           details.setInvDateLastModified(new java.util.Date());
           
           // create Distribution List
           
           ArrayList drList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getArCMListSize(); i++) {
           	
           	   ArCreditMemoEntryList arCMList = actionForm.getArCMByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arCMList.getAccount()) &&
      	 	       Common.validateRequired(arCMList.getDebitAmount()) &&
      	 	       Common.validateRequired(arCMList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(arCMList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(arCMList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(arCMList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();
           	   
           	   mdetails.setDrLine((short)(lineNumber));
           	   mdetails.setDrClass(arCMList.getDrClass());
           	   mdetails.setDrDebit(isDebit);
           	   mdetails.setDrAmount(amount);
           	   mdetails.setDrCoaAccountNumber(arCMList.getAccount());
           	   
           	   drList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           

           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.creditMemoNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("arCreditMemoEntry"));
           	
           }                            	
           
//         validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
   	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
    	   
           try {
           	
           	    Integer creditMemoCode = ejbCM.saveArInvEntry(details, actionForm.getCustomer(), actionForm.getBatchName(),
           	        drList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setCreditMemoCode(creditMemoCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
                    
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.documentNumberNotUnique"));
                               	                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyVoid"));
           
           } catch (ArINVOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
           
           } catch (GlobalAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.accountNumberInvalid", ex.getMessage()));
           
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyLocked"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.journalNotBalance"));
           	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.branchAccountNumberInvalid"));
           	  
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }

// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
/*******************************************************
	 -- Ar CM Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("MEMO LINES")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {         	

	           ArInvoiceDetails details = new ArInvoiceDetails();
	           
	           details.setInvCode(actionForm.getCreditMemoCode()); 
	           details.setInvType(actionForm.getType());         
	           details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setInvNumber(actionForm.getCmNumber());
	           details.setInvCmInvoiceNumber(actionForm.getInvoiceNumber());
	           details.setInvCmReferenceNumber(actionForm.getCmReferenceNumber());
	           details.setInvAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	           details.setInvVoid(Common.convertBooleanToByte(actionForm.getCreditMemoVoid()));
	           details.setInvDescription(actionForm.getDescription());           
	           details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));           
	           
	           if (actionForm.getCreditMemoCode() == null) {
	           	
	           	   details.setInvCreatedBy(user.getUserName());
		           details.setInvDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setInvLastModifiedBy(user.getUserName());
	           details.setInvDateLastModified(new java.util.Date());
	           
	           ArrayList drList = new ArrayList();
	           int lineNumber = 0;  
	           
	           double TOTAL_DEBIT = 0;
	           double TOTAL_CREDIT = 0;         
	                      
	           for (int i = 0; i<actionForm.getArCMListSize(); i++) {
	           	
	           	   ArCreditMemoEntryList arCMList = actionForm.getArCMByIndex(i);           	   
	           	              	   
	           	   if (Common.validateRequired(arCMList.getAccount()) &&
	      	 	       Common.validateRequired(arCMList.getDebitAmount()) &&
	      	 	       Common.validateRequired(arCMList.getCreditAmount())) continue;
	           	   
	           	   byte isDebit = 0;
			       double amount = 0d;
		
			       if (!Common.validateRequired(arCMList.getDebitAmount())) {
			       	
			          isDebit = 1;
			          amount = Common.convertStringMoneyToDouble(arCMList.getDebitAmount(), precisionUnit);
			          
			          TOTAL_DEBIT += amount;
			          
			       } else {
			       	
			          isDebit = 0;
			          amount = Common.convertStringMoneyToDouble(arCMList.getCreditAmount(), precisionUnit); 
			          
			          TOTAL_CREDIT += amount;
			       }
			       
			       lineNumber++;

	           	   ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();
	           	   
	           	   mdetails.setDrLine((short)(lineNumber));
	           	   mdetails.setDrClass(arCMList.getDrClass());
	           	   mdetails.setDrDebit(isDebit);
	           	   mdetails.setDrAmount(amount);
	           	   mdetails.setDrCoaAccountNumber(arCMList.getAccount());
	           	   
	           	   drList.add(mdetails);
	           	
	           }
	           
	           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
	           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
	           

	           if (TOTAL_DEBIT != TOTAL_CREDIT) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.creditMemoNotBalance"));
	                    
	               saveErrors(request, new ActionMessages(errors));
	               return (mapping.findForward("arCreditMemoEntry"));
	           	
	           }             
	           
//	         validate attachment
	           
	           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
	           
	           if (!Common.validateRequired(filename1)) {                	       	    	
	    	    	
		   	    	if (actionForm.getFilename1().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("creditMemoEntry.error.filename1NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
		  	    	        	    	
		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename1Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename1().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename1SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arCreditMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename2)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename2().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("creditMemoEntry.error.filename2NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename2Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename2().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename2SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arCreditMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename3)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename3().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("creditMemoEntry.error.filename3NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename3Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename3().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename3SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arCreditMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   }
		   	   
		   	   if (!Common.validateRequired(filename4)) {                	       	    	
		   	    	    	    	
		   	    	if (actionForm.getFilename4().getFileSize() == 0) {
		   	    		
		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("creditMemoEntry.error.filename4NotFound"));
		           			
		   	    	} else {
		   	    		
		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
		  	    	           	    	
		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
		          	    	               	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename4Invalid"));	
		                   		
		          	    	}
		          	    	
		          	    	InputStream is = actionForm.getFilename4().getInputStream();
		          	    	           	    	
		          	    	if (is.available() > maxAttachmentFileSize) {
		          	    		
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("creditMemoEntry.error.filename4SizeInvalid"));
		          	    		
		          	    	}
		          	    	
		          	    	is.close();	    		
		   	    		
		   	    	}
		   	    	
		   	    	if (!errors.isEmpty()) {
		   	    		
		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arCreditMemoEntry"));
		   	    		
		   	    	}    	    	
		   	    	
		   	   	}
	    	   
	           
	           try {
	           	
	           	   Integer creditMemo =  ejbCM.saveArInvEntry(details, actionForm.getCustomer(), actionForm.getBatchName(), drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	
	           	   actionForm.setCreditMemoCode(creditMemo);
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
	                    
	           } catch (GlobalNoRecordFoundException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.documentNumberNotUnique"));
	                               	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyVoid"));
	           
	           } catch (ArINVOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
	                    
	           } catch (GlobalAccountNumberInvalidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.accountNumberInvalid", ex.getMessage()));
	                       
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyLocked"));
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noApprovalApproverFound"));
	           	   
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.journalNotBalance"));
	           
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.branchAccountNumberInvalid"));
	           	   
	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("arCreditMemoEntry"));
	               
	           }
	           
	           actionForm.setReport(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
	           
	        // save attachment
	           
	           if (!Common.validateRequired(filename1)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-1" + fileExtension); 
		            	
	       	    		int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename2)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-2" + fileExtension); 
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-3" + fileExtension); 

		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-4" + fileExtension); 
	       	        	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
                         	
        }  else {
        	
        	actionForm.setReport(Constants.STATUS_SUCCESS);
          	          	        
	        	return(mapping.findForward("arCreditMemoEntry"));
	        	
        }
         	

         	
/*******************************************************
   -- Ar CLI Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && (actionForm.getType().equals("ITEMS") || actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED")) &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArInvoiceDetails details = new ArInvoiceDetails();
           
           details.setInvCode(actionForm.getCreditMemoCode());  
           details.setInvType(actionForm.getType());
           details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setInvNumber(actionForm.getCmNumber());
           details.setInvCmInvoiceNumber(actionForm.getInvoiceNumber());
           details.setInvCmReferenceNumber(actionForm.getCmReferenceNumber());
           details.setInvAmountDue(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setInvVoid(Common.convertBooleanToByte(actionForm.getCreditMemoVoid()));
           details.setInvDescription(actionForm.getDescription()); 
           details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
           details.setInvPartialCm(Common.convertBooleanToByte(actionForm.getPartialCm()));            
           if (actionForm.getCreditMemoCode() == null) {
           	
           	   details.setInvCreatedBy(user.getUserName());
	           details.setInvDateCreated(new java.util.Date());
	                      
           }
           
           details.setInvLastModifiedBy(user.getUserName());
           details.setInvDateLastModified(new java.util.Date());
           
           
           
           
           if(!actionForm.getPartialCm()) {
        	   actionForm.clearArCLIList();
        	    String taxCodeName = "";
    			String wtaxCodeName = "";
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
        		if (actionForm.getType().equals("ITEMS")) {
       	        System.out.println("populate items");
       	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
   	           	// Populate line when not forwarding
        			ArCreditMemoLineItemList arCLIList;
              	
        		
        			
        			
               	ArrayList list = ejbCM.getArInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
               	
               	System.out.println("size list" + list.size());
               	if(list.size()<=0){
               		throw new GlobalNoRecordFoundException();
               	}
               
               	for (int x = 0; x < list.size(); x++) {
               		int lineCount = x + 1;
               		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
               		
               		
               		actionForm.setTaxCode(mdetails.getIliTaxCodeName());
               		actionForm.setWithholdingTaxCode(mdetails.getIliWTaxCodeName());
               		
               		System.out.println("unit:" + mdetails.getIliUomName());
               		System.out.println("loc:" + mdetails.getIliLocName());
               		arCLIList = new ArCreditMemoLineItemList(
               				actionForm, 
               				mdetails.getIliCode(),
               				new Integer(lineCount).toString(), 
               				mdetails.getIliLocName(), 
               				mdetails.getIliIiName(), 
               				mdetails.getIliIiDescription(), 
               				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
               				, mdetails.getIliUomName(),	 
               				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
               				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
               						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
               		
               		if(mdetails.getIliIiClass().equals("Assembly")){
               			arCLIList.setIsAssemblyItem(true);
               		}else{
               			arCLIList.setIsAssemblyItem(false);
               		}
               		
               		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
         
               		
               		//unit entered list
               		ArrayList uomList = new ArrayList();
               		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
               		
                 		arCLIList.clearUnitList();
                 		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
                 		
                 		arCLIList.setLocationList(actionForm.getLocationList());
       	        	
             			Iterator i = uomList.iterator();
                 		while (i.hasNext()) {
                 			
                 			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
                 			
                 			arCLIList.setUnitList(mUomDetails.getUomName());
                 			
                 			if (mUomDetails.isDefault()) {
                 				
                 				arCLIList.setUnit(mUomDetails.getUomName());      					
                 			}      			
                 			
                 		}
                 		actionForm.saveArCLIList(arCLIList);
               	}
               	
   	        	
   	        } else if (actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED")) {
       	        System.out.println("populate items");
       	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
   	           	// Populate line when not forwarding
        			ArCreditMemoLineItemList arCLIList;
              	
               	ArrayList list = new ArrayList();
               	
               	if(actionForm.getType().equals("SO MATCHED")) {
               	 	list = ejbCM.getArSoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
               	}else if(actionForm.getType().equals("JO MATCHED")) {
               	 	list = ejbCM.getArJoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
               	}
              
               	
               	System.out.println("size list" + list.size());
               	if(list.size()<=0){
               		throw new GlobalNoRecordFoundException();
               	}
               
               	for (int x = 0; x < list.size(); x++) {
               		int lineCount = x + 1;
               		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
               		System.out.println("unit:" + mdetails.getIliUomName());
               		System.out.println("loc:" + mdetails.getIliLocName());
               		arCLIList = new ArCreditMemoLineItemList(
               				actionForm, 
               				mdetails.getIliCode(),
               				new Integer(lineCount).toString(), 
               				mdetails.getIliLocName(), 
               				mdetails.getIliIiName(), 
               				mdetails.getIliIiDescription(), 
               				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
               				, mdetails.getIliUomName(),	 
               				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
               				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
               						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
               		
               		if(mdetails.getIliIiClass().equals("Assembly")){
               			arCLIList.setIsAssemblyItem(true);
               		}else{
               			arCLIList.setIsAssemblyItem(false);
               		}
               		
               		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
         
               		
               		//unit entered list
               		ArrayList uomList = new ArrayList();
               		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
               		
                 		arCLIList.clearUnitList();
                 		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
                 		
                 		arCLIList.setLocationList(actionForm.getLocationList());
       	        	
             			Iterator i = uomList.iterator();
                 		while (i.hasNext()) {
                 			
                 			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
                 			
                 			arCLIList.setUnitList(mUomDetails.getUomName());
                 			
                 			if (mUomDetails.isDefault()) {
                 				
                 				arCLIList.setUnit(mUomDetails.getUomName());      					
                 			}      			
                 			
                 		}
                 		actionForm.saveArCLIList(arCLIList);
               	}
               	
   	        	
   	        }
           }
           
       
           
           ArrayList iliList = new ArrayList(); 
                                 
           for (int i = 0; i<actionForm.getArCLIListSize(); i++) {
           	
           	   ArCreditMemoLineItemList arCLIList = actionForm.getArCLIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arCLIList.getLocation()) &&
           	   	   Common.validateRequired(arCLIList.getItemName()) &&
				   Common.validateRequired(arCLIList.getUnit()) &&
				   Common.validateRequired(arCLIList.getUnitPrice()) &&
				   Common.validateRequired(arCLIList.getQuantity())) continue;
           	   
           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();
           	   
           	   System.out.println("action in amount is : " + arCLIList.getAmount());
           	   
           	   mdetails.setIliLine(Common.convertStringToShort(arCLIList.getLineNumber()));
           	   mdetails.setIliIiName(arCLIList.getItemName());
           	   mdetails.setIliLocName(arCLIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arCLIList.getQuantity(), precisionUnit));
           	   mdetails.setIliUomName(arCLIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arCLIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arCLIList.getAmount(), precisionUnit));
           	   mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arCLIList.getAutoBuildCheckbox()));
        	   mdetails.setIliMisc(arCLIList.getMisc());
        		mdetails.setIliTax(Common.convertBooleanToByte(arCLIList.getTaxable()));
        	   if(arCLIList.getIsAssemblyItem() == true) {        	   
        	   		mdetails.setIliIiClass("Assembly");        	   	
        	   } else {        	   	
        	   		mdetails.setIliIiClass("Stock");        	   	
        	   }
        
           	   iliList.add(mdetails);
           	
           }
           
//         validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
   	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
    	   
    	   
           try {
           	
        	   if(details.getInvType().equals("ITEMS")) {
        		   Integer creditMemoCode =ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(),
                  	        iliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        		    
              	    actionForm.setCreditMemoCode(creditMemoCode);
        	   }
        	   if(details.getInvType().equals("SO MATCHED") || details.getInvType().equals("JO MATCHED")) {
        		   Integer creditMemoCode =ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(),
                  	        iliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        		    
              	    actionForm.setCreditMemoCode(creditMemoCode);
        	   }
           
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
                    
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.documentNumberNotUnique"));
                               	                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyVoid"));
           
           } catch (ArINVOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
                    
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyLocked"));
                    
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.journalNotBalance"));
           
           } catch (GlobalInventoryDateException ex) {
              	
          		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("creditMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));           	    

           } catch (GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.branchAccountNumberInvalid"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("creditMemoEntry.error.noNegativeInventoryCostingCOA"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   
       	   
       	   
       	   
       	   
       	   
       	   
       	   
       	   
       	   
/*******************************************************
   -- Ar CLI Save & Submit Action --
*******************************************************/

        } else if (request.getParameter("saveSubmitButton") != null && (actionForm.getType().equals("ITEMS") || actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED"))&&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArInvoiceDetails details = new ArInvoiceDetails();
           
           details.setInvCode(actionForm.getCreditMemoCode());  
           details.setInvType(actionForm.getType());
           details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setInvNumber(actionForm.getCmNumber());
           details.setInvCmInvoiceNumber(actionForm.getInvoiceNumber());
           details.setInvCmReferenceNumber(actionForm.getCmReferenceNumber());
           details.setInvVoid(Common.convertBooleanToByte(actionForm.getCreditMemoVoid()));
           details.setInvDescription(actionForm.getDescription());   
           details.setInvLvShift(actionForm.getShift());
           details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
           details.setInvPartialCm(Common.convertBooleanToByte(actionForm.getPartialCm()));         
           if (actionForm.getCreditMemoCode() == null) {
           	
           	   details.setInvCreatedBy(user.getUserName());
	           details.setInvDateCreated(new java.util.Date());
	                      
           }
           
           details.setInvLastModifiedBy(user.getUserName());
           details.setInvDateLastModified(new java.util.Date());
           
           
           if(!actionForm.getPartialCm()) {
        	   actionForm.clearArCLIList();
        	   String taxCodeName = "";
    			String wtaxCodeName = "";
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
        		if (actionForm.getType().equals("ITEMS")) {
       	        System.out.println("populate items");
       	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
   	           	// Populate line when not forwarding
        			ArCreditMemoLineItemList arCLIList;
              	
        		
        			
        			
               	ArrayList list = ejbCM.getArInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
               	
               	System.out.println("size list" + list.size());
               	if(list.size()<=0){
               		throw new GlobalNoRecordFoundException();
               	}
               
               	for (int x = 0; x < list.size(); x++) {
               		int lineCount = x + 1;
               		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
               		
               		
               		actionForm.setTaxCode(mdetails.getIliTaxCodeName());
               		actionForm.setWithholdingTaxCode(mdetails.getIliWTaxCodeName());
               		
               		System.out.println("unit:" + mdetails.getIliUomName());
               		System.out.println("loc:" + mdetails.getIliLocName());
               		arCLIList = new ArCreditMemoLineItemList(
               				actionForm, 
               				mdetails.getIliCode(),
               				new Integer(lineCount).toString(), 
               				mdetails.getIliLocName(), 
               				mdetails.getIliIiName(), 
               				mdetails.getIliIiDescription(), 
               				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
               				, mdetails.getIliUomName(),	 
               				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
               				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
               						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
               		
               		if(mdetails.getIliIiClass().equals("Assembly")){
               			arCLIList.setIsAssemblyItem(true);
               		}else{
               			arCLIList.setIsAssemblyItem(false);
               		}
               		
               		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
         
               		
               		//unit entered list
               		ArrayList uomList = new ArrayList();
               		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
               		
                 		arCLIList.clearUnitList();
                 		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
                 		
                 		arCLIList.setLocationList(actionForm.getLocationList());
       	        	
             			Iterator i = uomList.iterator();
                 		while (i.hasNext()) {
                 			
                 			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
                 			
                 			arCLIList.setUnitList(mUomDetails.getUomName());
                 			
                 			if (mUomDetails.isDefault()) {
                 				
                 				arCLIList.setUnit(mUomDetails.getUomName());      					
                 			}      			
                 			
                 		}
                 		actionForm.saveArCLIList(arCLIList);
               	}
               	
   	        	
   	        } else if (actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED")) {
       	        System.out.println("populate items");
       	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
   	           	// Populate line when not forwarding
        			ArCreditMemoLineItemList arCLIList;
              	
               	ArrayList list = new ArrayList();
               	
               	if(actionForm.getType().equals("SO MATCHED")) {
               		
               	   	list = ejbCM.getArSoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
               	}else if(actionForm.getType().equals("JO MATCHED")) {
               	   list=	ejbCM.getArJoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
               	}
            
               	
               	System.out.println("size list" + list.size());
               	if(list.size()<=0){
               		throw new GlobalNoRecordFoundException();
               	}
               
               	for (int x = 0; x < list.size(); x++) {
               		int lineCount = x + 1;
               		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
               		System.out.println("unit:" + mdetails.getIliUomName());
               		System.out.println("loc:" + mdetails.getIliLocName());
               		arCLIList = new ArCreditMemoLineItemList(
               				actionForm, 
               				mdetails.getIliCode(),
               				new Integer(lineCount).toString(), 
               				mdetails.getIliLocName(), 
               				mdetails.getIliIiName(), 
               				mdetails.getIliIiDescription(), 
               				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
               				, mdetails.getIliUomName(),	 
               				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
               				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
               						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
               		
               		if(mdetails.getIliIiClass().equals("Assembly")){
               			arCLIList.setIsAssemblyItem(true);
               		}else{
               			arCLIList.setIsAssemblyItem(false);
               		}
               		
               		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
         
               		
               		//unit entered list
               		ArrayList uomList = new ArrayList();
               		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
               		
                 		arCLIList.clearUnitList();
                 		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
                 		
                 		arCLIList.setLocationList(actionForm.getLocationList());
       	        	
             			Iterator i = uomList.iterator();
                 		while (i.hasNext()) {
                 			
                 			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
                 			
                 			arCLIList.setUnitList(mUomDetails.getUomName());
                 			
                 			if (mUomDetails.isDefault()) {
                 				
                 				arCLIList.setUnit(mUomDetails.getUomName());      					
                 			}      			
                 			
                 		}
                 		actionForm.saveArCLIList(arCLIList);
               	}
               	
   	        	
   	        }
        	   
           }
           
           
           
           
           
           
           ArrayList iliList = new ArrayList(); 
                                 
           for (int i = 0; i<actionForm.getArCLIListSize(); i++) {
           	
           	   ArCreditMemoLineItemList arCLIList = actionForm.getArCLIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arCLIList.getLocation()) &&
           	   	   Common.validateRequired(arCLIList.getItemName()) &&
				   Common.validateRequired(arCLIList.getUnit()) &&
				   Common.validateRequired(arCLIList.getUnitPrice()) &&
				   Common.validateRequired(arCLIList.getQuantity())) continue;
           	   
           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();
           	   
           	   mdetails.setIliLine(Common.convertStringToShort(arCLIList.getLineNumber()));
           	   mdetails.setIliIiName(arCLIList.getItemName());
           	   mdetails.setIliLocName(arCLIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arCLIList.getQuantity(), precisionUnit));
           	   mdetails.setIliUomName(arCLIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arCLIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arCLIList.getAmount(), precisionUnit));
	           mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arCLIList.getAutoBuildCheckbox()));
	           mdetails.setIliMisc(arCLIList.getMisc());
	       	mdetails.setIliTax(Common.convertBooleanToByte(arCLIList.getTaxable()));
	     	   if(arCLIList.getIsAssemblyItem() == true) {        	   
	     	   		mdetails.setIliIiClass("Assembly");        	   	
	     	   } else {        	   	
	     	   		mdetails.setIliIiClass("Stock");        	   	
	     	   }
           	   iliList.add(mdetails);
           	
           }                     	
           
//         validate attachment
           
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
   	    	
	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename1NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	  	    	        	    	
	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename1().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename1SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename2)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename2NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename2().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename2SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename3)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename3NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename3().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename3SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   }
	   	   
	   	   if (!Common.validateRequired(filename4)) {                	       	    	
	   	    	    	    	
	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
	   	    		
	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("creditMemoEntry.error.filename4NotFound"));
	           			
	   	    	} else {
	   	    		
	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	  	    	           	    	
	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
	          	    	               	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4Invalid"));	
	                   		
	          	    	}
	          	    	
	          	    	InputStream is = actionForm.getFilename4().getInputStream();
	          	    	           	    	
	          	    	if (is.available() > maxAttachmentFileSize) {
	          	    		
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("creditMemoEntry.error.filename4SizeInvalid"));
	          	    		
	          	    	}
	          	    	
	          	    	is.close();	    		
	   	    		
	   	    	}
	   	    	
	   	    	if (!errors.isEmpty()) {
	   	    		
	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arCreditMemoEntry"));
	   	    		
	   	    	}    	    	
	   	    	
	   	   	}
    	   
    	   
           try {
           	
        	   if(details.getInvType().equals("ITEMS")) {
        		   Integer creditMemoCode = ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(),
                  	        iliList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        		   actionForm.setCreditMemoCode(creditMemoCode);
        	   }
        	   
        	   if(details.getInvType().equals("SO MATCHED") || details.getInvType().equals("JO MATCHED")) {
        		   Integer creditMemoCode = ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(),
                  	        iliList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        		   actionForm.setCreditMemoCode(creditMemoCode);
        	   }
           	  
           	        
           	 
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
                    
           } catch (GlobalNoRecordFoundException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.documentNumberNotUnique"));
                               	                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyVoid"));
           
           } catch (ArINVOverapplicationNotAllowedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
                    
           } catch (GlobalTransactionAlreadyLockedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.transactionAlreadyLocked"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.journalNotBalance"));
           
           } catch (GlobalInventoryDateException ex) {
              	
          		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("creditMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));           	    
           	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.branchAccountNumberInvalid"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
          	
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("creditMemoEntry.error.noNegativeInventoryCostingCOA"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }

// save attachment
           
           if (!Common.validateRequired(filename1)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename1().getInputStream();
       	        	
       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-1" + fileExtension); 
	            	
       	    		int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename2)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename2().getInputStream();
       	        	
       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-2" + fileExtension); 
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename3)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename3().getInputStream();
       	        	
       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-3" + fileExtension); 

	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
       	   
       	   if (!Common.validateRequired(filename4)) {     
           	               	            	    		       	   		       	    		           	    		                  	 
       	        if (errors.isEmpty()) {
       	        	       	        	 
       	        	InputStream is = actionForm.getFilename4().getInputStream();
       	        	
       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
       	        	
       	        	new File(attachmentPath).mkdirs();       	        	
       	        	
       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
       	    			
       	    			fileExtension = attachmentFileExtension;
       	    			
           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;          	    			
           	    		
           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-4" + fileExtension); 
       	        	
	            	int c;        
	            	            	            	            	
	            	while ((c = is.read()) != -1) {
	            		
	            		fos.write((byte)c);
	            		
	            	}
	            	
	            	is.close();
					fos.close();
       	        	
       	        }          	                
            	           	    	
       	   }
           
/*******************************************************
	 -- Ar CLI Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("printButton") != null && (actionForm.getType().equals("ITEMS") ||actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED"))) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {         	

	           ArInvoiceDetails details = new ArInvoiceDetails();
	           
	           details.setInvCode(actionForm.getCreditMemoCode()); 
	           details.setInvType(actionForm.getType());
	           details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setInvNumber(actionForm.getCmNumber());
	           details.setInvCmInvoiceNumber(actionForm.getInvoiceNumber());
	           details.setInvCmReferenceNumber(actionForm.getCmReferenceNumber());
	           details.setInvVoid(Common.convertBooleanToByte(actionForm.getCreditMemoVoid()));
	           details.setInvDescription(actionForm.getDescription());  
	           details.setInvLvShift(actionForm.getShift());
	           details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));           
	           details.setInvPartialCm(Common.convertBooleanToByte(actionForm.getPartialCm()));  
	           if (actionForm.getCreditMemoCode() == null) {
	           	
	           	   details.setInvCreatedBy(user.getUserName());
		           details.setInvDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setInvLastModifiedBy(user.getUserName());
	           details.setInvDateLastModified(new java.util.Date());
	           
	           if(!actionForm.getPartialCm()) {
	        	   actionForm.clearArCLIList();
	        	   
	        	   String taxCodeName = "";
	     			String wtaxCodeName = "";
		           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	         		if (actionForm.getType().equals("ITEMS")) {
	        	        System.out.println("populate items");
	        	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	    	           	// Populate line when not forwarding
	         			ArCreditMemoLineItemList arCLIList;
	               	
	         		
	         			
	         			
	                	ArrayList list = ejbCM.getArInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
	                	
	                	System.out.println("size list" + list.size());
	                	if(list.size()<=0){
	                		throw new GlobalNoRecordFoundException();
	                	}
	                
	                	for (int x = 0; x < list.size(); x++) {
	                		int lineCount = x + 1;
	                		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
	                		
	                		
	                		actionForm.setTaxCode(mdetails.getIliTaxCodeName());
	                		actionForm.setWithholdingTaxCode(mdetails.getIliWTaxCodeName());
	                		
	                		System.out.println("unit:" + mdetails.getIliUomName());
	                		System.out.println("loc:" + mdetails.getIliLocName());
	                		arCLIList = new ArCreditMemoLineItemList(
	                				actionForm, 
	                				mdetails.getIliCode(),
	                				new Integer(lineCount).toString(), 
	                				mdetails.getIliLocName(), 
	                				mdetails.getIliIiName(), 
	                				mdetails.getIliIiDescription(), 
	                				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
	                				, mdetails.getIliUomName(),	 
	                				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
	                				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
	                						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
	                		
	                		if(mdetails.getIliIiClass().equals("Assembly")){
	                			arCLIList.setIsAssemblyItem(true);
	                		}else{
	                			arCLIList.setIsAssemblyItem(false);
	                		}
	                		
	                		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
	          
	                		
	                		//unit entered list
	                		ArrayList uomList = new ArrayList();
	                		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
	                		
	                  		arCLIList.clearUnitList();
	                  		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
	                  		
	                  		arCLIList.setLocationList(actionForm.getLocationList());
	        	        	
	              			Iterator i = uomList.iterator();
	                  		while (i.hasNext()) {
	                  			
	                  			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
	                  			
	                  			arCLIList.setUnitList(mUomDetails.getUomName());
	                  			
	                  			if (mUomDetails.isDefault()) {
	                  				
	                  				arCLIList.setUnit(mUomDetails.getUomName());      					
	                  			}      			
	                  			
	                  		}
	                  		actionForm.saveArCLIList(arCLIList);
	                	}
	                	
	    	        	
	    	        } else if (actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED")) {
	        	        System.out.println("populate items");
	        	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	    	           	// Populate line when not forwarding
	         			ArCreditMemoLineItemList arCLIList;
	               	
	                	ArrayList list = new ArrayList();
	                	
	                	if(actionForm.getType().equals("SO MATCHED")) {
	                		list=  ejbCM.getArSoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
	                	}else if(actionForm.getType().equals("JO MATCHED")) {
	                		list = ejbCM.getArJoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
	                	}
	                	
	                	System.out.println("size list" + list.size());
	                	if(list.size()<=0){
	                		throw new GlobalNoRecordFoundException();
	                	}
	                
	                	for (int x = 0; x < list.size(); x++) {
	                		int lineCount = x + 1;
	                		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
	                		System.out.println("unit:" + mdetails.getIliUomName());
	                		System.out.println("loc:" + mdetails.getIliLocName());
	                		arCLIList = new ArCreditMemoLineItemList(
	                				actionForm, 
	                				mdetails.getIliCode(),
	                				new Integer(lineCount).toString(), 
	                				mdetails.getIliLocName(), 
	                				mdetails.getIliIiName(), 
	                				mdetails.getIliIiDescription(), 
	                				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
	                				, mdetails.getIliUomName(),	 
	                				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
	                				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
	                						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
	                		
	                		if(mdetails.getIliIiClass().equals("Assembly")){
	                			arCLIList.setIsAssemblyItem(true);
	                		}else{
	                			arCLIList.setIsAssemblyItem(false);
	                		}
	                		
	                		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
	          
	                		
	                		//unit entered list
	                		ArrayList uomList = new ArrayList();
	                		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
	                		
	                  		arCLIList.clearUnitList();
	                  		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
	                  		
	                  		arCLIList.setLocationList(actionForm.getLocationList());
	        	        	
	              			Iterator i = uomList.iterator();
	                  		while (i.hasNext()) {
	                  			
	                  			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
	                  			
	                  			arCLIList.setUnitList(mUomDetails.getUomName());
	                  			
	                  			if (mUomDetails.isDefault()) {
	                  				
	                  				arCLIList.setUnit(mUomDetails.getUomName());      					
	                  			}      			
	                  			
	                  		}
	                  		actionForm.saveArCLIList(arCLIList);
	                	}
	                	
	    	        	
	    	        }
	        	   
	           }
	           
	           
	           
	           ArrayList iliList = new ArrayList(); 
                                 
               for (int i = 0; i<actionForm.getArCLIListSize(); i++) {
           	
           	   ArCreditMemoLineItemList arCLIList = actionForm.getArCLIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arCLIList.getLocation()) &&
           	   	   Common.validateRequired(arCLIList.getItemName()) &&
				   Common.validateRequired(arCLIList.getUnit()) &&
				   Common.validateRequired(arCLIList.getUnitPrice()) &&
				   Common.validateRequired(arCLIList.getQuantity())) continue;
           	   
           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();
           	   
           	   mdetails.setIliLine(Common.convertStringToShort(arCLIList.getLineNumber()));
           	   mdetails.setIliIiName(arCLIList.getItemName());
           	   mdetails.setIliLocName(arCLIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arCLIList.getQuantity(), precisionUnit));
           	   mdetails.setIliUomName(arCLIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arCLIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arCLIList.getAmount(), precisionUnit));
           	   mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arCLIList.getAutoBuildCheckbox()));
           	mdetails.setIliMisc(arCLIList.getMisc());
        	mdetails.setIliTax(Common.convertBooleanToByte(arCLIList.getTaxable()));
	     	   if(arCLIList.getIsAssemblyItem() == true) {        	   
	     	   		mdetails.setIliIiClass("Assembly");        	   	
	     	   } else {        	   	
	     	   		mdetails.setIliIiClass("Stock");        	   	
	     	   }
	     	   
           	   iliList.add(mdetails);
           	
           }                     	
	           
//             validate attachment
               
               String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
               String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
               String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
               String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
               
               if (!Common.validateRequired(filename1)) {                	       	    	
       	    	
   	   	    	if (actionForm.getFilename1().getFileSize() == 0) {
   	   	    		
   	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	           			new ActionMessage("creditMemoEntry.error.filename1NotFound"));
   	           			
   	   	    	} else {
   	   	    		
   	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
   	  	    	        	    	
   	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
   	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {           	    	    	           	    	    
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename1Invalid"));	
   	                   		
   	          	    	}
   	          	    	
   	          	    	InputStream is = actionForm.getFilename1().getInputStream();
   	          	    	           	    	
   	          	    	if (is.available() > maxAttachmentFileSize) {
   	          	    		
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename1SizeInvalid"));
   	          	    		
   	          	    	}
   	          	    	
   	          	    	is.close();	    		
   	   	    		
   	   	    	}
   	   	    	
   	   	    	if (!errors.isEmpty()) {
   	   	    		
   	   	    		saveErrors(request, new ActionMessages(errors));
   	          			return (mapping.findForward("arCreditMemoEntry"));
   	   	    		
   	   	    	}    	    	
   	   	    	
   	   	   }
   	   	   
   	   	   if (!Common.validateRequired(filename2)) {                	       	    	
   	   	    	    	    	
   	   	    	if (actionForm.getFilename2().getFileSize() == 0) {
   	   	    		
   	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	           			new ActionMessage("creditMemoEntry.error.filename2NotFound"));
   	           			
   	   	    	} else {
   	   	    		
   	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
   	  	    	           	    	
   	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
   	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
   	          	    	               	    		
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename2Invalid"));	
   	                   		
   	          	    	}
   	          	    	
   	          	    	InputStream is = actionForm.getFilename2().getInputStream();
   	          	    	           	    	
   	          	    	if (is.available() > maxAttachmentFileSize) {
   	          	    		
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename2SizeInvalid"));
   	          	    		
   	          	    	}
   	          	    	
   	          	    	is.close();	    		
   	   	    		
   	   	    	}
   	   	    	
   	   	    	if (!errors.isEmpty()) {
   	   	    		
   	   	    		saveErrors(request, new ActionMessages(errors));
   	          			return (mapping.findForward("arCreditMemoEntry"));
   	   	    		
   	   	    	}    	    	
   	   	    	
   	   	   }
   	   	   
   	   	   if (!Common.validateRequired(filename3)) {                	       	    	
   	   	    	    	    	
   	   	    	if (actionForm.getFilename3().getFileSize() == 0) {
   	   	    		
   	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	           			new ActionMessage("creditMemoEntry.error.filename3NotFound"));
   	           			
   	   	    	} else {
   	   	    		
   	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
   	  	    	           	    	
   	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
   	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
   	          	    	               	    		
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename3Invalid"));	
   	                   		
   	          	    	}
   	          	    	
   	          	    	InputStream is = actionForm.getFilename3().getInputStream();
   	          	    	           	    	
   	          	    	if (is.available() > maxAttachmentFileSize) {
   	          	    		
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename3SizeInvalid"));
   	          	    		
   	          	    	}
   	          	    	
   	          	    	is.close();	    		
   	   	    		
   	   	    	}
   	   	    	
   	   	    	if (!errors.isEmpty()) {
   	   	    		
   	   	    		saveErrors(request, new ActionMessages(errors));
   	          			return (mapping.findForward("arCreditMemoEntry"));
   	   	    		
   	   	    	}    	    	
   	   	    	
   	   	   }
   	   	   
   	   	   if (!Common.validateRequired(filename4)) {                	       	    	
   	   	    	    	    	
   	   	    	if (actionForm.getFilename4().getFileSize() == 0) {
   	   	    		
   	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	           			new ActionMessage("creditMemoEntry.error.filename4NotFound"));
   	           			
   	   	    	} else {
   	   	    		
   	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
   	  	    	           	    	
   	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
   	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {           	    	    	           	    	    
   	          	    	               	    		
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename4Invalid"));	
   	                   		
   	          	    	}
   	          	    	
   	          	    	InputStream is = actionForm.getFilename4().getInputStream();
   	          	    	           	    	
   	          	    	if (is.available() > maxAttachmentFileSize) {
   	          	    		
   	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
   	                   		new ActionMessage("creditMemoEntry.error.filename4SizeInvalid"));
   	          	    		
   	          	    	}
   	          	    	
   	          	    	is.close();	    		
   	   	    		
   	   	    	}
   	   	    	
   	   	    	if (!errors.isEmpty()) {
   	   	    		
   	   	    		saveErrors(request, new ActionMessages(errors));
   	          			return (mapping.findForward("arCreditMemoEntry"));
   	   	    		
   	   	    	}    	    	
   	   	    	
   	   	   	}
        	   
        	   
	           try {
	           	
	        	   if(details.getInvType().equals("ITEMS")){
	        		   Integer creditMemo =  ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(), iliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	   	           	
		           	   actionForm.setCreditMemoCode(creditMemo);
	        	   }
	        	   
	        	   if(details.getInvType().equals("SO MATCHED") || details.getInvType().equals("JO MATCHED")){
	        		   Integer creditMemo =  ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(), iliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		   	           	
		           	   actionForm.setCreditMemoCode(creditMemo);
	        	   }
	        	   
	           	 
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
	                    
	           } catch (GlobalNoRecordFoundException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.documentNumberNotUnique"));
	                               	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyVoid"));
	           
	           } catch (ArINVOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
	                    
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyLocked"));
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noApprovalApproverFound"));
	           	   
	           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("creditMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	       
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.journalNotBalance"));
	           
	           } catch (GlobalInventoryDateException ex) {
		           	
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("creditMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate"));
		           		
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.branchAccountNumberInvalid"));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("creditMemoEntry.error.noNegativeInventoryCostingCOA"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }

	           if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("arCreditMemoEntry"));
	               
	           }
	           
	           actionForm.setReport(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
	           
	        // save attachment
	           
	           if (!Common.validateRequired(filename1)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename1().getInputStream();
	       	        	
	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-1" + fileExtension); 
		            	
	       	    		int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename2)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename2().getInputStream();
	       	        	
	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-2" + fileExtension); 
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename3)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename3().getInputStream();
	       	        	
	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-3" + fileExtension); 

		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
	       	   
	       	   if (!Common.validateRequired(filename4)) {     
	           	               	            	    		       	   		       	    		           	    		                  	 
	       	        if (errors.isEmpty()) {
	       	        	       	        	 
	       	        	InputStream is = actionForm.getFilename4().getInputStream();
	       	        	
	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));
	       	        	
	       	        	new File(attachmentPath).mkdirs();       	        	
	       	        	
	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	       	    			
	       	    			fileExtension = attachmentFileExtension;
	       	    			
	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;          	    			
	           	    		
	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getCreditMemoCode() + "-4" + fileExtension); 
	       	        	
		            	int c;        
		            	            	            	            	
		            	while ((c = is.read()) != -1) {
		            		
		            		fos.write((byte)c);
		            		
		            	}
		            	
		            	is.close();
						fos.close();
	       	        	
	       	        }          	                
	            	           	    	
	       	   }
                         	
        }  else {
        	
        	actionForm.setReport(Constants.STATUS_SUCCESS);
          	          	        
	        	return(mapping.findForward("arCreditMemoEntry"));
	        	
        }       
         	
         	
         	
         	

        
/*******************************************************
   -- Ar CM Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ar CM Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {
         	         	         	
         	int listSize = actionForm.getArCMListSize();
         	int lineNumber = 0;
                                   
            for (int x = listSize + 1; x <= listSize + invoiceLineNumber; x++) {
	        	
	        	ArCreditMemoEntryList arCMList = new ArCreditMemoEntryList(actionForm, 
	        	    null, String.valueOf(++lineNumber), null, null, null, null, null);	       
	        		        	    
	        	arCMList.setDrClassList(this.getDrClassList());
	        	
	        	actionForm.saveArCMList(arCMList);
	        	
	        }	
	        
	        for (int i = 0; i<actionForm.getArCMListSize(); i++) {
           	
           	   ArCreditMemoEntryList arCMList = actionForm.getArCMByIndex(i);
           	   
           	   arCMList.setLineNumber(String.valueOf(i+1));
           	   
            }        
	        
	        return(mapping.findForward("arCreditMemoEntry"));
	        
/*******************************************************
   -- Ar CM Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {
         	
         	for (int i = 0; i<actionForm.getArCMListSize(); i++) {
           	
           	   ArCreditMemoEntryList arCMList = actionForm.getArCMByIndex(i);
           	   
           	   if (arCMList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteArCMList(i);
           	   	   i--;
           	   }
           	   
            }
            	        
	        return(mapping.findForward("arCreditMemoEntry"));
	        
/*******************************************************
   -- Ar CLI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {
         	         	         	
         	int listSize = actionForm.getArCLIListSize();
         	int lineNumber = 0;
                                   
            for (int x = listSize + 1; x <= listSize + invoiceLineNumber; x++) {
	        	
	        	ArCreditMemoLineItemList arCLIList = new ArCreditMemoLineItemList(actionForm, 
	        	    null, String.valueOf(++lineNumber), null, null, null, null, null, null, null, false, false, null,false);
	        	    	        	    	       	        		        	    
	        	arCLIList.setLocationList(actionForm.getLocationList());
	        	
	        	actionForm.saveArCLIList(arCLIList);
	        	
	        }	
	        
	        for (int i = 0; i<actionForm.getArCLIListSize(); i++) {
           	
           	   ArCreditMemoLineItemList arCLIList = actionForm.getArCLIByIndex(i);
           	   
           	   arCLIList.setLineNumber(String.valueOf(i+1));
           	   
            }        
	        
	        return(mapping.findForward("arCreditMemoEntry"));
	        
/*******************************************************
   -- Ar CLI Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && (actionForm.getType().equals("ITEMS") || actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED") )) {
         	
         	for (int i = 0; i<actionForm.getArCLIListSize(); i++) {
           	
           	   ArCreditMemoLineItemList arCLIList = actionForm.getArCLIByIndex(i);
           	   
           	   if (arCLIList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteArCLIList(i);
           	   	   i--;
           	   }
           	   
            }
            	        
	        return(mapping.findForward("arCreditMemoEntry"));	   
	        
/*******************************************************
   -- Ar Credit Memo Enter Action --
*******************************************************/
	        
         } else if(!Common.validateRequired(request.getParameter("isInvoiceEntered"))) {
         	System.out.println("is invoice entered: " + actionForm.getInvoiceNumber());
         	try {
         		String customerCode = ejbCM.getArCstCustomerCodeByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());

         		actionForm.setCustomer(customerCode);
         		
	           	actionForm.clearArCMList();
	           	actionForm.clearArCLIList();
	           	
	           
	         
	           	ArModInvoiceDetails invDetails = ejbCM.getArInvByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
	           	
	           	System.out.println("type is " + invDetails.getInvType());
	          
	           	actionForm.setType(invDetails.getInvType());
	           	

		/*
	        	ArrayList getlist = ejbCM.getArInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
            	
	           	if(getlist.size()<=0){
	           		System.out.println("memo lines");
	           		
	           	//	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            	}else{
            		System.out.println("items");
            		actionForm.setType("ITEMS");
            		actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            	}
            	
           		      	  */    
	           	
	           	String taxCodeName = "";
     			String wtaxCodeName = "";
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
         		if (actionForm.getType().equals("ITEMS")) {
        	        System.out.println("populate items");
        	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
    	           	// Populate line when not forwarding
         			ArCreditMemoLineItemList arCLIList;
       
                	ArrayList list = ejbCM.getArInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
                	
                	System.out.println("size list" + list.size());
                	if(list.size()<=0){
                		throw new GlobalNoRecordFoundException();
                	}
                
                	for (int x = 0; x < list.size(); x++) {
                		int lineCount = x + 1;
                		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
                		
                		
                		actionForm.setTaxCode(mdetails.getIliTaxCodeName());
                		actionForm.setWithholdingTaxCode(mdetails.getIliWTaxCodeName());
                		
                		System.out.println("unit:" + mdetails.getIliUomName());
                		System.out.println("loc:" + mdetails.getIliLocName());
                		arCLIList = new ArCreditMemoLineItemList(
                				actionForm, 
                				mdetails.getIliCode(),
                				new Integer(lineCount).toString(), 
                				mdetails.getIliLocName(), 
                				mdetails.getIliIiName(), 
                				mdetails.getIliIiDescription(), 
                				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
                				, mdetails.getIliUomName(),	 
                				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
                				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
                						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
                		
                		if(mdetails.getIliIiClass().equals("Assembly")){
                			arCLIList.setIsAssemblyItem(true);
                		}else{
                			arCLIList.setIsAssemblyItem(false);
                		}
                		
                		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
          
                		
                		//unit entered list
                		ArrayList uomList = new ArrayList();
                		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
                		
                  		arCLIList.clearUnitList();
                  		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
                  		
                  		arCLIList.setLocationList(actionForm.getLocationList());
        	        	
              			Iterator i = uomList.iterator();
                  		while (i.hasNext()) {
                  			
                  			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
                  			
                  			arCLIList.setUnitList(mUomDetails.getUomName());
                  			
                  			if (mUomDetails.isDefault()) {
                  				
                  				arCLIList.setUnit(mUomDetails.getUomName());      					
                  			}      			
                  			
                  		}
                  		actionForm.saveArCLIList(arCLIList);
                	}
                	
    	        	
    	        } else if (actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED")) {
        	        System.out.println("populate items");
        	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
    	           	// Populate line when not forwarding
         			ArCreditMemoLineItemList arCLIList;
               	
                	ArrayList list = new ArrayList();
                	
                	if(actionForm.getType().equals("SO MATCHED")) {
                		list = ejbCM.getArSoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
                	}else if(actionForm.getType().equals("JO MATCHED")) {
                		list = ejbCM.getArJoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
                	}
                	
                	System.out.println("size list" + list.size());
                	if(list.size()<=0){
                		throw new GlobalNoRecordFoundException();
                	}
                
                	for (int x = 0; x < list.size(); x++) {
                		int lineCount = x + 1;
                		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
                		System.out.println("unit:" + mdetails.getIliUomName());
                		System.out.println("loc:" + mdetails.getIliLocName());
                		arCLIList = new ArCreditMemoLineItemList(
                				actionForm, 
                				mdetails.getIliCode(),
                				new Integer(lineCount).toString(), 
                				mdetails.getIliLocName(), 
                				mdetails.getIliIiName(), 
                				mdetails.getIliIiDescription(), 
                				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
                				, mdetails.getIliUomName(),	 
                				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
                				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
                						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
                		
                		if(mdetails.getIliIiClass().equals("Assembly")){
                			arCLIList.setIsAssemblyItem(true);
                		}else{
                			arCLIList.setIsAssemblyItem(false);
                		}
                		
                		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
          
                		
                		//unit entered list
                		ArrayList uomList = new ArrayList();
                		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
                		
                  		arCLIList.clearUnitList();
                  		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
                  		
                  		arCLIList.setLocationList(actionForm.getLocationList());
        	        	
              			Iterator i = uomList.iterator();
                  		while (i.hasNext()) {
                  			
                  			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
                  			
                  			arCLIList.setUnitList(mUomDetails.getUomName());
                  			
                  			if (mUomDetails.isDefault()) {
                  				
                  				arCLIList.setUnit(mUomDetails.getUomName());      					
                  			}      			
                  			
                  		}
                  		actionForm.saveArCLIList(arCLIList);
                	}
                	
    	        	
    	        }  else {
    	        
	
  					
    	           	
    	        	ArrayList list = ejbCM.getArDrByInvCmInvoiceNumberAndInvBillAmountAndCstCustomerCode(
             				actionForm.getInvoiceNumber(),
    						Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit),
    						customerCode, user.getCmpCode());       
					
					double amountRemaining = ejbCM.getArInvoiceAmountDueByInvoiceNumber(actionForm.getInvoiceNumber(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
             		
             		
             		Iterator i = list.iterator();
             		
             		while (i.hasNext()) {
             			
             			ArModDistributionRecordDetails mDrDetails = (ArModDistributionRecordDetails)i.next();
             			
             			String debitAmount = null;
             			String creditAmount = null;
             			
             			if (mDrDetails.getDrDebit() == 1) {
             				
             				debitAmount = Common.convertDoubleToStringMoney(amountRemaining, precisionUnit);				          
             				
             			} else {
             				
             				creditAmount = Common.convertDoubleToStringMoney(amountRemaining, precisionUnit);				          
             				
             			}
             			
             			ArrayList classList = new ArrayList();
             			classList.add(mDrDetails.getDrClass());
             			
             			ArCreditMemoEntryList arDrList = new ArCreditMemoEntryList(actionForm,
             					mDrDetails.getDrCode(),
    							Common.convertShortToString(mDrDetails.getDrLine()),
    							mDrDetails.getDrCoaAccountNumber(),
    							debitAmount, creditAmount, mDrDetails.getDrClass(),
    							mDrDetails.getDrCoaAccountDescription());				          
             			
             			arDrList.setDrClassList(classList);
             			
             			actionForm.saveArCMList(arDrList);
             		}
             		
             		actionForm.setDate(Common.convertSQLDateToString(ejbCM.getArInvoiceDateByInvoiceNumber(actionForm.getInvoiceNumber(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode())));
             		
             		
             		actionForm.setAmount(Common.convertDoubleToStringMoney(amountRemaining, precisionUnit));
             		
             		int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();
             		
             		for (int x = list.size() + 1; x <= remainingList; x++) {
             			
             			ArCreditMemoEntryList arCMList = new ArCreditMemoEntryList(actionForm,
             					null, String.valueOf(x), null, null, null, null, null);
             			
             			arCMList.setDrClassList(this.getDrClassList());
             			
             			actionForm.saveArCMList(arCMList);
             			
             		}	
    	        }
    	     
         		actionForm.setDate(Common.convertSQLDateToString(ejbCM.getArInvoiceDateByInvoiceNumber(actionForm.getInvoiceNumber(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode())));
         		
         					         
         		
         	} catch (GlobalNoRecordFoundException ex) {
         		
         		actionForm.clearArCMList();
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
         		saveErrors(request, new ActionMessages(errors));
         		
         	} catch (ArINVOverapplicationNotAllowedException ex) {
         		
         		actionForm.clearArCMList();
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
         		saveErrors(request, new ActionMessages(errors));
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage"); 
         			
         		}
         		
         	}  	
         	
         	return(mapping.findForward("arCreditMemoEntry"));
			
/*******************************************************
   -- Ar CLI Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbCM.deleteArInvEntry(actionForm.getCreditMemoCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }			
			
/*******************************************************
   -- Ar CLI Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && (actionForm.getType().equals("ITEMS") || actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED"))) {
         	
         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ArInvoiceDetails details = new ArInvoiceDetails();
	           
	           details.setInvCode(actionForm.getCreditMemoCode()); 
	           details.setInvType(actionForm.getType());
	           details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setInvNumber(actionForm.getCmNumber());
	           details.setInvCmInvoiceNumber(actionForm.getInvoiceNumber());
	           details.setInvCmReferenceNumber(actionForm.getCmReferenceNumber());
	           details.setInvVoid(Common.convertBooleanToByte(actionForm.getCreditMemoVoid()));
	           details.setInvDescription(actionForm.getDescription());     
	           details.setInvLvShift(actionForm.getShift());
	           details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
	           details.setInvPartialCm(Common.convertBooleanToByte(actionForm.getPartialCm()));  
	           if (actionForm.getCreditMemoCode() == null) {
	           	
	           	   details.setInvCreatedBy(user.getUserName());
		           details.setInvDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setInvLastModifiedBy(user.getUserName());
	           details.setInvDateLastModified(new java.util.Date());
	           
	           
	           
	           
	           
	           if(!actionForm.getPartialCm()) {
	        	   
	        	   
	        	   actionForm.clearArCLIList();
	        	   String taxCodeName = "";
	     			String wtaxCodeName = "";
		           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	         		if (actionForm.getType().equals("ITEMS")) {
	        	        System.out.println("populate items");
	        	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	    	           	// Populate line when not forwarding
	         			ArCreditMemoLineItemList arCLIList;
	               	
	         		
	         			
	         			
	                	ArrayList list = ejbCM.getArInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
	                	
	                	System.out.println("size list" + list.size());
	                	if(list.size()<=0){
	                		throw new GlobalNoRecordFoundException();
	                	}
	                
	                	for (int x = 0; x < list.size(); x++) {
	                		int lineCount = x + 1;
	                		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
	                		
	                		
	                		actionForm.setTaxCode(mdetails.getIliTaxCodeName());
	                		actionForm.setWithholdingTaxCode(mdetails.getIliWTaxCodeName());
	                		
	                		System.out.println("unit:" + mdetails.getIliUomName());
	                		System.out.println("loc:" + mdetails.getIliLocName());
	                		arCLIList = new ArCreditMemoLineItemList(
	                				actionForm, 
	                				mdetails.getIliCode(),
	                				new Integer(lineCount).toString(), 
	                				mdetails.getIliLocName(), 
	                				mdetails.getIliIiName(), 
	                				mdetails.getIliIiDescription(), 
	                				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
	                				, mdetails.getIliUomName(),	 
	                				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
	                				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
	                						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
	                		
	                		if(mdetails.getIliIiClass().equals("Assembly")){
	                			arCLIList.setIsAssemblyItem(true);
	                		}else{
	                			arCLIList.setIsAssemblyItem(false);
	                		}
	                		
	                		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
	          
	                		
	                		//unit entered list
	                		ArrayList uomList = new ArrayList();
	                		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
	                		
	                  		arCLIList.clearUnitList();
	                  		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
	                  		
	                  		arCLIList.setLocationList(actionForm.getLocationList());
	        	        	
	              			Iterator i = uomList.iterator();
	                  		while (i.hasNext()) {
	                  			
	                  			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
	                  			
	                  			arCLIList.setUnitList(mUomDetails.getUomName());
	                  			
	                  			if (mUomDetails.isDefault()) {
	                  				
	                  				arCLIList.setUnit(mUomDetails.getUomName());      					
	                  			}      			
	                  			
	                  		}
	                  		actionForm.saveArCLIList(arCLIList);
	                	}
	                	
	    	        	
	    	        } else if (actionForm.getType().equals("SO MATCHED") || actionForm.getType().equals("JO MATCHED")) {
	        	        System.out.println("populate items");
	        	        actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	    	           	// Populate line when not forwarding
	         			ArCreditMemoLineItemList arCLIList;
	               	
	                	ArrayList list = new ArrayList();
	                	
	                	if(actionForm.getType().equals("SO MATCHED")) {
	                		list = ejbCM.getArSoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
	                	}else if(actionForm.getType().equals("JO MATCHED")) {
	                		list = ejbCM.getArJoInvLnItmByArInvNmbr(actionForm.getInvoiceNumber(),new Integer(user.getCurrentBranch().getBrCode()),user.getCmpCode());
	                	}
	                	
	                	System.out.println("size list" + list.size());
	                	if(list.size()<=0){
	                		throw new GlobalNoRecordFoundException();
	                	}
	                
	                	for (int x = 0; x < list.size(); x++) {
	                		int lineCount = x + 1;
	                		ArModInvoiceLineItemDetails mdetails = (ArModInvoiceLineItemDetails)list.get(x);
	                		System.out.println("unit:" + mdetails.getIliUomName());
	                		System.out.println("loc:" + mdetails.getIliLocName());
	                		arCLIList = new ArCreditMemoLineItemList(
	                				actionForm, 
	                				mdetails.getIliCode(),
	                				new Integer(lineCount).toString(), 
	                				mdetails.getIliLocName(), 
	                				mdetails.getIliIiName(), 
	                				mdetails.getIliIiDescription(), 
	                				Common.convertDoubleToStringMoney(mdetails.getIliQuantity(), precisionUnit)
	                				, mdetails.getIliUomName(),	 
	                				Common.convertDoubleToStringMoney(mdetails.getIliUnitPrice(), precisionUnit),  
	                				Common.convertDoubleToStringMoney(mdetails.getIliAmount(), 
	                						precisionUnit), false, false, mdetails.getIliMisc(), Common.convertByteToBoolean(mdetails.getIliTax()));	
	                		
	                		if(mdetails.getIliIiClass().equals("Assembly")){
	                			arCLIList.setIsAssemblyItem(true);
	                		}else{
	                			arCLIList.setIsAssemblyItem(false);
	                		}
	                		
	                		arCLIList.setAutoBuildCheckbox(Common.convertByteToBoolean(mdetails.getIliEnableAutoBuild()));
	          
	                		
	                		//unit entered list
	                		ArrayList uomList = new ArrayList();
	                		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
	                		
	                  		arCLIList.clearUnitList();
	                  		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
	                  		
	                  		arCLIList.setLocationList(actionForm.getLocationList());
	        	        	
	              			Iterator i = uomList.iterator();
	                  		while (i.hasNext()) {
	                  			
	                  			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
	                  			
	                  			arCLIList.setUnitList(mUomDetails.getUomName());
	                  			
	                  			if (mUomDetails.isDefault()) {
	                  				
	                  				arCLIList.setUnit(mUomDetails.getUomName());      					
	                  			}      			
	                  			
	                  		}
	                  		actionForm.saveArCLIList(arCLIList);
	                	}
	                	
	    	        	
	    	        }
	           }
	           
	           
	           
	           
	           
	           
	           
	           
	           
	           
	           
	           
	           
	           
	           
	           ArrayList iliList = new ArrayList(); 
                                 
               for (int i = 0; i<actionForm.getArCLIListSize(); i++) {
           	
           	   ArCreditMemoLineItemList arCLIList = actionForm.getArCLIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arCLIList.getLocation()) &&
           	   	   Common.validateRequired(arCLIList.getItemName()) &&
				   Common.validateRequired(arCLIList.getUnit()) &&
				   Common.validateRequired(arCLIList.getUnitPrice()) &&
				   Common.validateRequired(arCLIList.getQuantity())) continue;
           	   
           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();
           	   
           	   mdetails.setIliLine(Common.convertStringToShort(arCLIList.getLineNumber()));
           	   mdetails.setIliIiName(arCLIList.getItemName());
           	   mdetails.setIliLocName(arCLIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arCLIList.getQuantity(), precisionUnit));
           	   mdetails.setIliUomName(arCLIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arCLIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arCLIList.getAmount(), precisionUnit));
           	   mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arCLIList.getAutoBuildCheckbox()));
           	mdetails.setIliMisc(arCLIList.getMisc());
           	mdetails.setIliTax(Common.convertBooleanToByte(arCLIList.getTaxable()));
	     	   if(arCLIList.getIsAssemblyItem() == true) {        	   
	     	   		mdetails.setIliIiClass("Assembly");        	   	
	     	   } else {        	   	
	     	   		mdetails.setIliIiClass("Stock");        	   	
	     	   }
           	   
           	   iliList.add(mdetails);
           	
               }                     	
	           
	           try {
	           	
	        	   if(details.getInvType().equals("ITEMS")) {
	        		   Integer creditMemo  = ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(), iliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                          
		           	    actionForm.setCreditMemoCode(creditMemo);
	        		   
	        	   }
	        	   
	        	   if(details.getInvType().equals("SO MATCHED") || details.getInvType().equals("JO MATCHED")) {
	        		   Integer creditMemo  = ejbCM.saveArInvIliEntry(details, actionForm.getCustomer(), actionForm.getBatchName(), iliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                          
		           	    actionForm.setCreditMemoCode(creditMemo);
	        		   
	        	   }
	           	 
	           	     
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.recordAlreadyDeleted"));
	                    
	           } catch (GlobalNoRecordFoundException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noInvoiceFound"));
	           	
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.documentNumberNotUnique"));
	                               	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyVoid"));
	           
	           } catch (ArINVOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.overapplicationNotAllowed"));
	                    
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.transactionAlreadyLocked"));
	                    
	           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noApprovalRequesterFound"));
	                    
	           } catch (GlobalNoApprovalApproverFoundException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.noApprovalApproverFound"));
	                    
	           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("creditMemoEntry.error.noItemLocationFound", ex.getMessage()));
           	       
	           } catch (GlJREffectiveDateNoPeriodExistException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.effectiveDateNoPeriodExist"));
	                    
	           } catch (GlJREffectiveDatePeriodClosedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.effectiveDatePeriodClosed"));
	                    
	           } catch (GlobalJournalNotBalanceException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.journalNotBalance"));
	           
	           } catch (GlobalInventoryDateException ex) {
		           	
		           		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("creditMemoEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate"));
	           	   
	           } catch (GlobalBranchAccountNumberInvalidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("creditMemoEntry.error.branchAccountNumberInvalid"));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
	          	
	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("creditMemoEntry.error.noNegativeInventoryCostingCOA"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
	           
	           if (!errors.isEmpty()) {
        	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("arCreditMemoEntry"));
	               
	           }	           
         	
         	}

  	        String path = "/arJournal.do?forward=1" + 
			     "&transactionCode=" + actionForm.getCreditMemoCode() +
			     "&transaction=CREDIT MEMO" + 
			     "&transactionNumber=" + actionForm.getInvoiceNumber() + 
			     "&transactionDate=" + actionForm.getDate() +
                 "&transactionEnableFields=" + actionForm.getEnableFields();
 
			return(new ActionForward(path));						

/*******************************************************
-- Ar View Attachment Action --
*******************************************************/

        } else if(request.getParameter("viewAttachmentButton1") != null) {
	    	
	  	File file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-1" + attachmentFileExtension );
	  	File filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-1" + attachmentFileExtensionPDF );
	  	String filename = "";
	  	
	  	if (file.exists()){
	  		filename = file.getName();
	  	}else if (filePDF.exists()) {
	  		filename = filePDF.getName();
	  	}	    	  	
	  	
			System.out.println(filename + " <== filename?");
        String fileExtension = filename.substring(filename.lastIndexOf("."));
        
	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
    		
	  		fileExtension = attachmentFileExtension;
    			
	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;          	    			
	    		
	    	}
	  	System.out.println(fileExtension + " <== file extension?");
	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCreditMemoCode() + "-1" + fileExtension);
	  	
	  	byte data[] = new byte[fis.available()];
    	  
	  	int ctr = 0;
	  	int c = 0;
      	
	  	while ((c = fis.read()) != -1) {
		  
      		data[ctr] = (byte)c;
      		ctr++;
      		
	  	}
	  	
	  	if (fileExtension == attachmentFileExtension) {
	      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
	      	System.out.println("jpg");
	      	
	      	session.setAttribute(Constants.IMAGE_KEY, image);
	      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	  		System.out.println("pdf");
	  		
	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	  	}
      	
         if (request.getParameter("child") == null) {
		         	
           return (mapping.findForward("arCreditMemoEntry"));         
         	
         } else {
         	
           return (mapping.findForward("arCreditMemoEntryChild"));         
         	
        }
         


      } else if(request.getParameter("viewAttachmentButton2") != null) {
      	
    	  File file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-2" + attachmentFileExtension );
    	  	File filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-2" + attachmentFileExtensionPDF );
    	  	String filename = "";
    	  	
    	  	if (file.exists()){
    	  		filename = file.getName();
    	  	}else if (filePDF.exists()) {
    	  		filename = filePDF.getName();
    	  	}	    	  	
    	  	
 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));
	        
    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		
    	  		fileExtension = attachmentFileExtension;
	    			
  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;          	    			
  	    		
  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCreditMemoCode() + "-2" + fileExtension);
    	  	
    	  	byte data[] = new byte[fis.available()];
	    	  
    	  	int ctr = 0;
    	  	int c = 0;
	      	
    	  	while ((c = fis.read()) != -1) {
    		  
	      		data[ctr] = (byte)c;
	      		ctr++;
	      		
    	  	}
    	  	
    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");
		      	
		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");
    	  		
    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}
	      	
      	
    	  
      	
         if (request.getParameter("child") == null) {
		         	
           return (mapping.findForward("arCreditMemoEntry"));         
         	
         } else {
         	
           return (mapping.findForward("arCreditMemoEntryChild"));         
         	
        }            


      } else if(request.getParameter("viewAttachmentButton3") != null) {
      	
    	  File file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-3" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-3" + attachmentFileExtensionPDF );
    	  String filename = "";
    	  	
    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }	    	  	
    	  	
    	  System.out.println(filename + " <== filename?");
    	  String fileExtension = filename.substring(filename.lastIndexOf("."));
	        
    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
    		  
    		  fileExtension = attachmentFileExtension;
	    			
    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;          	    			
  	    		
    	  }
    	  System.out.println(fileExtension + " <== file extension?");
    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCreditMemoCode() + "-3" + fileExtension);
    	  	
    	  byte data[] = new byte[fis.available()];
	    	  
    	  int ctr = 0;
    	  int c = 0;
	      	
    	  while ((c = fis.read()) != -1) {
    		  
    		  data[ctr] = (byte)c;
    		  ctr++;
	      		
    	  }
    	  	
    	  if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");
		      	
		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  } else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");
    	  		
    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  }
      	
         if (request.getParameter("child") == null) {
		         	
           return (mapping.findForward("arCreditMemoEntry"));         
         	
         } else {
         	
           return (mapping.findForward("arCreditMemoEntryChild"));         
         	
        }           


      } else if(request.getParameter("viewAttachmentButton4") != null) {
      	
    	  File file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-4" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-4" + attachmentFileExtensionPDF );
    	  String filename = "";
    	  	
    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }	    	  	
    	  	
 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));
	        
    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){
	    		
    	  		fileExtension = attachmentFileExtension;
	    			
  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;          	    			
  	    		
  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getCreditMemoCode() + "-4" + fileExtension);
    	  	
    	  	byte data[] = new byte[fis.available()];
	    	  
    	  	int ctr = 0;
    	  	int c = 0;
	      	
    	  	while ((c = fis.read()) != -1) {
    		  
	      		data[ctr] = (byte)c;
	      		ctr++;
	      		
    	  	}
    	  	
    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");
		      	
		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");
    	  		
    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}
         if (request.getParameter("child") == null) {
		         	
           return (mapping.findForward("arCreditMemoEntry"));         
         	
         } else {
         	
           return (mapping.findForward("arCreditMemoEntryChild"));         
         	
        }


/*******************************************************
    -- Ap CLI Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("arCLIList[" + 
        actionForm.getRowCLISelected() + "].isItemEntered"))) {
        
      	ArCreditMemoLineItemList arCLIList = 
      		actionForm.getArCLIByIndex(actionForm.getRowCLISelected());      	    	
        
      	boolean isAssemblyItem = ejbCM.getInvItemClassByIiName(arCLIList.getItemName(), user.getCmpCode()).equals("Assembly");
      	
      	arCLIList.setIsAssemblyItem(isAssemblyItem);
      	
      	if(ejbCM.getInvAutoBuildEnabledByIiName(arCLIList.getItemName(), user.getCmpCode())) {      	
      		arCLIList.setAutoBuildCheckbox(true);      		
      	} else {      		
      		arCLIList.setAutoBuildCheckbox(false);      		
      	}
      	
      	try {
          	
      		// populate unit field class
      		
      		ArrayList uomList = new ArrayList();
      		uomList = ejbCM.getInvUomByIiName(arCLIList.getItemName(), user.getCmpCode());
      		
      		arCLIList.clearUnitList();
      		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
      		
  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {
      			
      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
      			
      			arCLIList.setUnitList(mUomDetails.getUomName());
      			
      			if (mUomDetails.isDefault()) {
      				
      				arCLIList.setUnit(mUomDetails.getUomName());      				
      				
      			}      			
      			
      		}
      		
      		// populate unit cost field
      		
      		if (!Common.validateRequired(arCLIList.getItemName()) && !Common.validateRequired(arCLIList.getUnit())) {
      		
	      		double unitPrice = ejbCM.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(),arCLIList.getItemName(), arCLIList.getUnit(), user.getCmpCode());
	      		arCLIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
	      		
      		}
      		
      		// populate amount field
      		
      		if(!Common.validateRequired(arCLIList.getQuantity()) && !Common.validateRequired(arCLIList.getUnitPrice())) {
	        
	        	double amount = Common.convertStringMoneyToDouble(arCLIList.getQuantity(), precisionUnit) * Common.convertStringMoneyToDouble(arCLIList.getUnitPrice(), precisionUnit);
	        	arCLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit)); 
	        	        
	        }
      		      		      		      		
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("arCreditMemoEntry"));
         
/*******************************************************
    -- Ar CLI Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("arCLIList[" + 
        actionForm.getRowCLISelected() + "].isUnitEntered"))) {
        
      	ArCreditMemoLineItemList arCLIList = 
      		actionForm.getArCLIByIndex(actionForm.getRowCLISelected());
                      
      	try {
      	
      	    // populate unit cost field
      		
      		if (!Common.validateRequired(arCLIList.getLocation()) && !Common.validateRequired(arCLIList.getItemName())) {
      			      			      		
	      		double unitPrice = ejbCM.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arCLIList.getItemName(), arCLIList.getUnit(), user.getCmpCode());
	      		arCLIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
	      		
	        }
	        
	        // populate amount field
	        
	        if(!Common.validateRequired(arCLIList.getQuantity()) && !Common.validateRequired(arCLIList.getUnitPrice())) {
	        
	        	double amount = Common.convertStringMoneyToDouble(arCLIList.getQuantity(), precisionUnit) * Common.convertStringMoneyToDouble(arCLIList.getUnitPrice(), precisionUnit);
	        	arCLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit)); 
	        	        
	        }
      		       	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  	
                     
         return(mapping.findForward("arCreditMemoEntry"));
         
/*******************************************************
   -- Ar CL Type Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {      		     	
	       
	        if (actionForm.getType().equals("ITEMS")) {
	        
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearArCMList();
	           	actionForm.clearArCLIList();
	           	actionForm.setInvoiceNumber("");
	           	actionForm.setCustomer("");
	           	
	           	// Populate line when not forwarding
            	
            	for (int x = 1; x <= invoiceLineNumber; x++) {
            		
            		ArCreditMemoLineItemList arCLIList = new ArCreditMemoLineItemList(actionForm, 
            				null, new Integer(x).toString(), null, null, null, null, null, 
							null, null, false, false, null,false);	
            		
            		arCLIList.setLocationList(actionForm.getLocationList());
            		
            		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
            		arCLIList.setUnitList("Select Item First");
            		
            		actionForm.saveArCLIList(arCLIList);
            		
            	}
	        	
	        } else if (actionForm.getType().equals("SO MATCHED")) {
	        
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearArCMList();
	           	actionForm.clearArCLIList();
	           	actionForm.setInvoiceNumber("");
	           	actionForm.setCustomer("");
	           	
	           	// Populate line when not forwarding
            	
            	for (int x = 1; x <= invoiceLineNumber; x++) {
            		
            		ArCreditMemoLineItemList arCLIList = new ArCreditMemoLineItemList(actionForm, 
            				null, new Integer(x).toString(), null, null, null, null, null, 
							null, null, false, false, null,false);	
            		
            		arCLIList.setLocationList(actionForm.getLocationList());
            		
            		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
            		arCLIList.setUnitList("Select Item First");
            		
            		actionForm.saveArCLIList(arCLIList);
            		
            	}
	        	
	        } else if (actionForm.getType().equals("JO MATCHED")) {
	        
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearArCMList();
	           	actionForm.clearArCLIList();
	           	actionForm.setInvoiceNumber("");
	           	actionForm.setCustomer("");
	           	
	           	// Populate line when not forwarding
            	
            	for (int x = 1; x <= invoiceLineNumber; x++) {
            		
            		ArCreditMemoLineItemList arCLIList = new ArCreditMemoLineItemList(actionForm, 
            				null, new Integer(x).toString(), null, null, null, null, null, 
							null, null, false, false, null,false);	
            		
            		arCLIList.setLocationList(actionForm.getLocationList());
            		
            		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
            		arCLIList.setUnitList("Select Item First");
            		
            		actionForm.saveArCLIList(arCLIList);
            		
            	}
	        	
	        } else {
	        
	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearArCMList();
	           	actionForm.clearArCLIList();
	        	actionForm.setInvoiceNumber("");
	           	actionForm.setCustomer("");
	           	
	           	for (int x = 1; x <= invoiceLineNumber; x++) {
		        	
		        	ArCreditMemoEntryList arCMList = new ArCreditMemoEntryList(actionForm, 
		        	    null, new Integer(x).toString(), null, null, null, null, null);
		        	
		        	arCMList.setDrClassList(this.getDrClassList());
		        	
		        	actionForm.saveArCMList(arCMList);
		        	
		         }
	        
	        }
	     
	        return(mapping.findForward("arCreditMemoEntry"));            
/*******************************************************
   -- Ar CM Customer Enter Action --
 *******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {      		     	

         	actionForm.setCustomerName(ejbCM.getArCstNameByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode()));
         	
	        return(mapping.findForward("arCreditMemoEntry"));
	        
/*******************************************************
   -- Ar CM Load Action --
*******************************************************/

         }

         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arCreditMemoEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	if(actionForm.getUseCustomerPulldown()){
                	
                	actionForm.clearCustomerList(); 

            		list = ejbCM.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {

            				actionForm.setCustomerList((String)i.next());

            			}
            			
            		} 
            	
            	}
            	            	            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbCM.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.clearLocationList();       	
            	
            	list = ejbCM.getInvLocAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setLocationList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.clearShiftList();           	
            	
            	list = ejbCM.getAdLvInvShiftAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setShiftList((String)i.next());
            			
            		}
            		            		
            	}
            	            	
            	if (request.getParameter("forwardBatch") != null) {
            		
            		actionForm.setBatchName(request.getParameter("batchName"));
            		actionForm.setType(null);
            		
            	}
            	
            	actionForm.clearArCMList();
            	actionForm.clearArCLIList();    
            	actionForm.clearApproverList();       	  	
            	
            	if (request.getParameter("forward") != null || isInitialPrinting) {
            		System.out.println("forward entering");
            		if (request.getParameter("forward") != null) {
            			            	    
            			actionForm.setCreditMemoCode(new Integer(request.getParameter("creditMemoCode")));
            			
            		}
            		System.out.println("pasok 1 " + actionForm.getCreditMemoCode());
            		ArModInvoiceDetails mdetails = ejbCM.getArInvByInvCode(actionForm.getCreditMemoCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		System.out.println("pasok 2");
            		actionForm.setType(mdetails.getInvType());
            		actionForm.setDescription(mdetails.getInvDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getInvDate()));
            		actionForm.setInvoiceNumber(mdetails.getInvCmInvoiceNumber());
            		actionForm.setCmNumber(mdetails.getInvNumber());
            		actionForm.setCmReferenceNumber(mdetails.getInvCmReferenceNumber());
            		System.out.println("amount due is: " +Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit));
            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit));
            		actionForm.setCreditMemoVoid(Common.convertByteToBoolean(mdetails.getInvVoid()));
            		actionForm.setApprovalStatus(mdetails.getInvApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getInvReasonForRejection());
            		actionForm.setPosted(mdetails.getInvPosted() == 1 ? "YES" : "NO");
            		actionForm.setVoidApprovalStatus(mdetails.getInvVoidApprovalStatus());
            		actionForm.setVoidPosted(mdetails.getInvVoidPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getInvCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getInvDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getInvLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getInvDateLastModified()));            		
            		actionForm.setApprovedRejectedBy(mdetails.getInvApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getInvDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getInvPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getInvDatePosted()));
            		actionForm.setSubjectToCommission(Common.convertByteToBoolean(mdetails.getInvSubjectToCommission()));
            		actionForm.setTaxCode(mdetails.getInvTaxCodeName());
            		actionForm.setWithholdingTaxCode(mdetails.getInvWTaxCodeName());
            		actionForm.setPartialCm(Common.convertByteToBoolean(mdetails.getInvPartialCm()));
            		System.out.println("pasok 3");
            		if (!actionForm.getCustomerList().contains(mdetails.getInvCstCustomerCode())) {
            			
            			if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearCustomerList();

            			}
            			actionForm.setCustomerList(mdetails.getInvCstCustomerCode());

            		}     
            		System.out.println("pasok 3");
            		actionForm.setCustomer(mdetails.getInvCstCustomerCode());
            		actionForm.setCustomerName(mdetails.getInvCstName());
            		System.out.println(mdetails.getInvCstCustomerCode() + " customer:");
            		actionForm.setShift(mdetails.getInvLvShift());
            		
            		if (!actionForm.getBatchNameList().contains(mdetails.getInvIbName())) {
            			
           		    	actionForm.setBatchNameList(mdetails.getInvIbName());
           		    	
           		    }           		    
            		actionForm.setBatchName(mdetails.getInvIbName());
            		
            		
            		
            		
            		list = ejbAA.getArApproverList("AR CREDIT MEMO", mdetails.getInvCode(), user.getCmpCode());

            		
            		i = list.iterator();

            		while (i.hasNext()) {

            			AdModApprovalQueueDetails mPrAPRDetails = (AdModApprovalQueueDetails)i.next();

						ApproverList approverList = new ApproverList(actionForm,
            					Common.convertSQLDateAndTimeToString(mPrAPRDetails.getAqApprovedDate()), mPrAPRDetails.getAqApproverName(), mPrAPRDetails.getAqStatus());
					
            			actionForm.saveApproverList(approverList);
            		}
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		if (mdetails.getInvIliList() != null && !mdetails.getInvIliList().isEmpty()) {
            		
            			System.out.println("entering item");
            		    actionForm.setType("ITEMS");
            		    actionForm.setType(mdetails.getInvType());
            		    list = mdetails.getInvIliList();            		
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails)i.next();
            				
            				boolean isAssemblyItem = ejbCM.getInvItemClassByIiName(mIliDetails.getIliIiName(),user.getCmpCode()).equals("Assembly");
            				
            				ArCreditMemoLineItemList arCliList = new ArCreditMemoLineItemList(actionForm,
            						mIliDetails.getIliCode(),
									Common.convertShortToString(mIliDetails.getIliLine()),
									mIliDetails.getIliLocName(),
									mIliDetails.getIliIiName(),
									mIliDetails.getIliIiDescription(),
									Common.convertDoubleToStringMoney(mIliDetails.getIliQuantity(), precisionUnit),
									mIliDetails.getIliUomName(),
									Common.convertDoubleToStringMoney(mIliDetails.getIliUnitPrice(), precisionUnit),
									Common.convertDoubleToStringMoney(mIliDetails.getIliQuantity() *  mIliDetails.getIliUnitPrice(), precisionUnit),
            						isAssemblyItem, Common.convertByteToBoolean(mIliDetails.getIliEnableAutoBuild()),
            						mIliDetails.getIliMisc(),
            						Common.convertByteToBoolean(mIliDetails.getIliTax()));
            				
            				arCliList.setLocationList(actionForm.getLocationList());
            				
            				
            		
            				
            				
            				arCliList.clearUnitList();
			            	ArrayList unitList = ejbCM.getInvUomByIiName(mIliDetails.getIliIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {
			            		
			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		arCliList.setUnitList(mUomDetails.getUomName()); 
			            		
			            	}	
            				
            				actionForm.saveArCLIList(arCliList);
            				
            				
            			}	
            			
            			int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();
            			
            			for (int x = list.size() + 1; x <= remainingList; x++) {
            				
            				ArCreditMemoLineItemList arCLIList = new ArCreditMemoLineItemList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null, null, null, false, false, null,false);
            				
            				arCLIList.setLocationList(actionForm.getLocationList());
            				
            				arCLIList.setUnitList(Constants.GLOBAL_BLANK);
            				arCLIList.setUnitList("Select Item First");
            				
            				actionForm.saveArCLIList(arCLIList);
            				
            			}	
            			
            			this.setFormProperties(actionForm, user.getCompany());
            			
            			if (request.getParameter("child") == null) {
            				
            				return (mapping.findForward("arCreditMemoEntry"));         
            				
            			} else {
            				
            				return (mapping.findForward("arCreditMemoEntryChild"));         
            				
            			}
            			            			            			
            		} else {
            			System.out.println("entering memo lines");
            		    actionForm.setType("MEMO LINES");
            		
	            		list = mdetails.getInvDrList();              		
	            		         		            		            		
			            i = list.iterator();
			            
			            int lineNumber = 0;
			            
			            while (i.hasNext()) {
			            	
				           ArModDistributionRecordDetails mDrDetails = (ArModDistributionRecordDetails)i.next();
				           				       
				           ArrayList classList = new ArrayList();
					       classList.add(mDrDetails.getDrClass());
					       
					       ArCreditMemoEntryList arDrList = new ArCreditMemoEntryList(actionForm,
					              mDrDetails.getDrCode(),
							      Common.convertShortToString(mDrDetails.getDrLine()),
							      mDrDetails.getDrCoaAccountNumber(),
							      mDrDetails.getDrDebit() == 1 ? Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit) : null,
							      mDrDetails.getDrDebit() == 0 ? Common.convertDoubleToStringMoney(mDrDetails.getDrAmount(), precisionUnit) : null,
							      mDrDetails.getDrClass(),
							      mDrDetails.getDrCoaAccountDescription());
					          
					       arDrList.setDrClassList(classList);
					      
					       
					  
					       
					      actionForm.saveArCMList(arDrList);
					         
				        }
			            
			            actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getInvTotalDebit(), precisionUnit));
			            actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getInvTotalCredit(), precisionUnit));
			            
			            int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();
				        
				        for (int x = list.size() + 1; x <= remainingList; x++) {
				        	
				        	ArCreditMemoEntryList arCMList = new ArCreditMemoEntryList(actionForm, 
				        	    null, String.valueOf(x), null, null, null, null, null);
				        	
				        	arCMList.setDrClassList(this.getDrClassList());
				        	
				        	actionForm.saveArCMList(arCMList);
				        	
				         }	
				         
				         this.setFormProperties(actionForm, user.getCompany());
				         
				         if (request.getParameter("child") == null) {
				         
				         	return (mapping.findForward("arCreditMemoEntry"));         
				         	
				         } else {
				         	
				         	return (mapping.findForward("arCreditMemoEntryChild"));         
				         	
				         }
				         
			         }
    
            		
            	}     
            	
            	// Populate line when not forwarding
            	
            	if (user.getUserApps().contains("OMEGA INVENTORY") && (actionForm.getType() == null || actionForm.getType().equals("ITEMS"))) {
            	
	            	for (int x = 1; x <= invoiceLineNumber; x++) {
	            		
	            		ArCreditMemoLineItemList arCLIList = new ArCreditMemoLineItemList(actionForm, 
	            				null, new Integer(x).toString(), null, null, null, null, null, 
								null, null, false, false, null,false);	
	            		
	            		arCLIList.setLocationList(actionForm.getLocationList());
	            		
	            		arCLIList.setUnitList(Constants.GLOBAL_BLANK);
	            		arCLIList.setUnitList("Select Item First");
	            		
	            		actionForm.saveArCLIList(arCLIList);
	            		
	            	}       	  
	            	
            	} else {
            		
            		for (int x = 1; x <= invoiceLineNumber; x++) {
    		        	
    		        	ArCreditMemoEntryList arCMList = new ArCreditMemoEntryList(actionForm, 
    		        	    null, new Integer(x).toString(), null, null, null, null, null);
    		        	
    		        	arCMList.setDrClassList(this.getDrClassList());
    		        	
    		        	actionForm.saveArCMList(arCMList);
    		        	
    		         }
            		
            	}
	  	                            	           			            		           			   
            } catch(GlobalNoRecordFoundException ex) {
            	            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("creditMemoEntry.error.creditMemoAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 
	               
            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveSubmitButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                   try {
                   	
                   	   ArrayList list = ejbCM.getAdApprovalNotifiedUsersByInvCode(actionForm.getCreditMemoCode(), user.getCmpCode());
                   	                      	   
                   	   if (list.isEmpty()) {
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));     
                   	   
                   	   } else if (list.contains("DOCUMENT POSTED")) {
                   	   	
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,	
                	   	       new ActionMessage("messages.documentPosted"));  
                   	   } else {
                   	   	
                   	   	   Iterator i = list.iterator();
                   	   	   
                   	   	   String APPROVAL_USERS = "";
                   	   	   
                   	   	   while (i.hasNext()) {
                   	   	   	                   	   	   	                   	   	   	                      	   	   	   	
                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
                   	   	       
                   	   	       if (i.hasNext()) {
                   	   	       	
                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";
                   	   	       	
                   	   	       }
                   	   	                          	   	       
                   	   	   	   	                   	   	   	                      	   	   	
                   	   	   }
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
                   	   	
                   	   }
                   	   
                   	   saveMessages(request, messages);
                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                   	
                  	
                   } catch(EJBException ex) {
	     	
		               if (log.isInfoEnabled()) {
		               	
		                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }
		               
		               return(mapping.findForward("cmnErrorPage"));
		               
		           } 
                   
               } else if (request.getParameter("saveAsDraftButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);
            
            if (actionForm.getType() == null) {
      		      	
	           if (user.getUserApps().contains("OMEGA INVENTORY")) {
               
                   actionForm.setType("ITEMS");
                   
               } else {
               
                   actionForm.setType("MEMO LINES");
               
               }
	          
            } 

            actionForm.setCreditMemoCode(null);          
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            
            this.setFormProperties(actionForm, user.getCompany());
            return(mapping.findForward("arCreditMemoEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ArCreditMemoEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ArCreditMemoEntryForm actionForm , String adCompany) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getCreditMemoVoid() && actionForm.getPosted().equals("NO") ) {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableCreditMemoVoid(false);
				actionForm.setShowJournalButton(true);
				
				
				
			} else if (actionForm.getPosted().equals("NO")) {
				
				if (actionForm.getCreditMemoCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);

					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableCreditMemoVoid(false);
					actionForm.setShowJournalButton(true);

				} else if (actionForm.getCreditMemoCode() != null &&
						Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
	
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableCreditMemoVoid(true);
					actionForm.setShowJournalButton(true);


				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
						actionForm.getApprovalStatus().equals("N/A")) {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableCreditMemoVoid(true);
					actionForm.setShowJournalButton(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableCreditMemoVoid(false);
					actionForm.setShowJournalButton(true);
					
				}
					
			} else {
				
					actionForm.setEnableFields(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowJournalButton(true);
	
					
					System.out.println("actionForm.getVoidApprovalStatus()="+actionForm.getVoidApprovalStatus());
					if (actionForm.getVoidApprovalStatus() == null) {
						actionForm.setShowSaveSubmitButton(true);				
						actionForm.setEnableCreditMemoVoid(true);
					} else {
						actionForm.setShowSaveSubmitButton(false);				
						actionForm.setEnableCreditMemoVoid(false);					
					}
				
			}
			

			
		} else {
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableCreditMemoVoid(false);
			actionForm.setShowJournalButton(true);
			

		}
		
		//view attachment	
		
		if (actionForm.getCreditMemoCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

			String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ar/Credit Memo/";
			String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
			 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
             
             
	 			File file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-1" + attachmentFileExtension);
	 			File filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-1" + attachmentFileExtensionPDF);
	 			
	 			if (file.exists() || filePDF.exists()) {
	 				
	 				actionForm.setShowViewAttachmentButton1(true);	
	 				
	 			} else {
	 				
	 				actionForm.setShowViewAttachmentButton1(false);	
	 				
	 			}			
	 			
	 			file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-2" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-2" + attachmentFileExtensionPDF);
	 			
	 			if (file.exists() || filePDF.exists()) {
	 				
	 				actionForm.setShowViewAttachmentButton2(true);	
	 				
	 			} else {
	 				
	 				actionForm.setShowViewAttachmentButton2(false);	
	 				
	 			}
	 			
	 			file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-3" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-3" + attachmentFileExtensionPDF);
	 			
	 			if (file.exists() || filePDF.exists()) {
	 				
	 				actionForm.setShowViewAttachmentButton3(true);	
	 				
	 			} else {
	 				
	 				actionForm.setShowViewAttachmentButton3(false);	
	 				
	 			}
	 			
	 			file = new File(attachmentPath + actionForm.getCreditMemoCode() + "-4" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getCreditMemoCode() + "-4" + attachmentFileExtensionPDF);
	 			
	 			if (file.exists() || filePDF.exists()) {
	 				
	 				actionForm.setShowViewAttachmentButton4(true);	
	 				
	 			} else {
	 				
	 				actionForm.setShowViewAttachmentButton4(false);	
	 				
	 			}			
		} else {

			actionForm.setShowViewAttachmentButton1(false);				
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}
				    
	}
	
	private ArrayList getDrClassList() {
		
		ArrayList classList = new ArrayList();
		
     	classList.add("REVENUE");
     	classList.add("TAX");
     	classList.add("W-TAX");
     	classList.add("OTHER");
     	
     	return classList;
		
	}
		
}