package com.struts.ar.creditmemoentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;


public class ArCreditMemoEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
   private Integer creditMemoCode = null;
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String customer = null;
   private ArrayList customerList = new ArrayList();
   private String date = null;
   private String invoiceNumber = null;
   private String taxCode=null;
   private String withholdingTaxCode = null;
   private String cmNumber = null;
   private String cmReferenceNumber = null;
   private String srrNumber = null;
   private String amount = null;
   private boolean creditMemoVoid = false;
   private String totalDebit = null;
   private String totalCredit = null;
   private String description = null;
   private String approvalStatus = null;
   private String reasonForRejection = null;
   private String posted = null;
   private String voidPosted = null;
   private String voidApprovalStatus = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;
   private String approvedRejectedBy = null;
   private String dateApprovedRejected = null;
   private String postedBy = null;
   private String datePosted = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String location = null;
   private ArrayList locationList = new ArrayList();
   private String shift = null;
   private ArrayList shiftList = new ArrayList();
   private String functionalCurrency = null;
   private String customerName = null;

   private ArrayList arCMList = new ArrayList();
   private ArrayList arCLIList = new ArrayList();
   private int rowSelected = 0;
   private int rowCLISelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showBatchName = false;
   private boolean enableCreditMemoVoid = false;
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showSaveSubmitButton = false;
   private boolean showSaveAsDraftButton = false;
   private boolean showDeleteButton = false;
   private boolean showJournalButton = false;
   private boolean showShift = false;
   private boolean useCustomerPulldown = true;

   private String isCustomerEntered  = null;
   private String isTypeEntered = null;

   private boolean subjectToCommission = false;

   private String report = null;

   private FormFile filename1 = null;
   private FormFile filename2 = null;
   private FormFile filename3 = null;
   private FormFile filename4 = null;

   private boolean showViewAttachmentButton1 = false;
   private boolean showViewAttachmentButton2 = false;
   private boolean showViewAttachmentButton3 = false;
   private boolean showViewAttachmentButton4 = false;

   private String attachment = null;
   private String attachmentPDF = null;
   
   private boolean partialCm = false;
   
   private ArrayList approverList = new ArrayList();
   


   public String getReport() {

   	   return report;

   }

   public void setReport(String report) {

	   this.report = report;

   }

   public String getAttachment() {

	   return attachment;

   }

   public void setAttachment(String attachment) {

	   this.attachment = attachment;

   }

   public String getAttachmentPDF() {

	   return attachmentPDF;

   }

   public void setAttachmentPDF(String attachmentPDF) {

	   this.attachmentPDF = attachmentPDF;

   }
   
   
   public boolean getPartialCm() {

	   return partialCm;

   }

   public void setPartialCm(boolean partialCm) {

	   this.partialCm = partialCm;

   }

   public int getRowSelected(){
      return rowSelected;
   }

   public ArCreditMemoEntryList getArCMByIndex(int index){
      return((ArCreditMemoEntryList)arCMList.get(index));
   }

   public Object[] getArCMList(){
      return(arCMList.toArray());
   }

   public int getArCMListSize(){
      return(arCMList.size());
   }

   public void saveArCMList(Object newArCMList){
      arCMList.add(newArCMList);
   }

   public void clearArCMList(){
      arCMList.clear();
   }

   public void setRowSelected(Object selectedArCMList, boolean isEdit){
      this.rowSelected = arCMList.indexOf(selectedArCMList);
   }

   public void updateArCMRow(int rowSelected, Object newArCMList){
      arCMList.set(rowSelected, newArCMList);
   }

   public void deleteArCMList(int rowSelected){
      arCMList.remove(rowSelected);
   }

   public int getRowCLISelected(){
      return rowCLISelected;
   }

   public ArCreditMemoLineItemList getArCLIByIndex(int index){
      return((ArCreditMemoLineItemList)arCLIList.get(index));
   }

   public Object[] getArCLIList(){
      return(arCLIList.toArray());
   }

   public int getArCLIListSize(){
      return(arCLIList.size());
   }

   public void saveArCLIList(Object newArCLIList){
      arCLIList.add(newArCLIList);
   }

   public void clearArCLIList(){
      arCLIList.clear();
   }

   public void setRowCLISelected(Object selectedArCLIList, boolean isEdit){
      this.rowCLISelected = arCLIList.indexOf(selectedArCLIList);
   }

   public void updateArCLIRow(int rowCLISelected, Object newArCLIList){
      arCLIList.set(rowCLISelected, newArCLIList);
   }

   public void deleteArCLIList(int rowCLISelected){
      arCLIList.remove(rowCLISelected);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

   public Integer getCreditMemoCode() {

   	  return creditMemoCode;

   }

   public void setCreditMemoCode(Integer creditMemoCode) {

   	  this.creditMemoCode = creditMemoCode;

   }

   public String getBatchName() {

   	  return batchName;

   }

   public void setBatchName(String batchName) {

   	  this.batchName = batchName;

   }

   public ArrayList getBatchNameList() {

   	  return batchNameList;

   }

   public void setBatchNameList(String batchName) {

   	  batchNameList.add(batchName);

   }

   public void clearBatchNameList() {

   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);

   }

   public String getCustomer() {

   	  return customer;

   }

   public void setCustomer(String customer) {

   	  this.customer = customer;

   }

   public ArrayList getCustomerList() {

   	  return customerList;

   }

   public void setCustomerList(String customer) {

   	  customerList.add(customer);

   }

   public void clearCustomerList() {

   	  customerList.clear();
   	  customerList.add(Constants.GLOBAL_BLANK);

   }

   public String getDate() {

   	  return date;

   }

   public void setDate(String date) {

   	  this.date = date;

   }

   public String getInvoiceNumber() {

   	  return invoiceNumber;

   }

   public void setInvoiceNumber(String invoiceNumber) {

   	  this.invoiceNumber = invoiceNumber;

   }
   
   public String getTaxCode() {

   	  return taxCode;

   }

   public void setTaxCode(String taxCode) {

   	  this.taxCode = taxCode;

   }

   public String getWithholdingTaxCode() {

   	  return withholdingTaxCode;

   }

   public void setWithholdingTaxCode(String withholdingTaxCode) {

   	  this.withholdingTaxCode = withholdingTaxCode;

   }

   public String getCmNumber() {

   	  return cmNumber;

   }

   public void setCmNumber(String cmNumber) {

   	   this.cmNumber = cmNumber;

   }


   public String getCmReferenceNumber() {

	   	  return cmReferenceNumber;

   }

   public void setCmReferenceNumber(String cmReferenceNumber) {

   	   this.cmReferenceNumber = cmReferenceNumber;

   }

   public String getSrrNumber() {

	   	  return srrNumber;

   }

   public void setSrrNumber(String srrNumber) {

   	   this.srrNumber = srrNumber;

   }


   public String getAmount() {

   	  return amount;

   }

   public void setAmount(String amount) {

   	  this.amount = amount;

   }

   public boolean getCreditMemoVoid() {

   	  return creditMemoVoid;

   }

   public void setCreditMemoVoid(boolean creditMemoVoid) {

   	  this.creditMemoVoid = creditMemoVoid;

   }

   public String getTotalDebit() {

   	  return totalDebit;

   }

   public void setTotalDebit(String totalDebit) {

   	  this.totalDebit = totalDebit;

   }

   public void setTotalCredit(String totalCredit) {

   	  this.totalCredit = totalCredit;

   }

   public String getTotalCredit() {

   	  return totalCredit;

   }

   public String getDescription() {

   	  return description;

   }

   public void setDescription(String description) {

   	  this.description = description;

   }

   public String getApprovalStatus() {

   	   return approvalStatus;

   }

   public void setApprovalStatus(String approvalStatus) {

   	   this.approvalStatus = approvalStatus;

   }

   public String getReasonForRejection() {

   	   return reasonForRejection;

   }

   public void setReasonForRejection(String reasonForRejection) {

   	   this.reasonForRejection = reasonForRejection;

   }

   public String getPosted() {

   	   return posted;

   }

   public void setPosted(String posted) {

   	   this.posted = posted;

   }

   public String getVoidPosted() {

   	   return voidPosted;

   }

   public void setVoidPosted(String voidPosted) {

   	   this.voidPosted = voidPosted;

   }

   public String getVoidApprovalStatus() {

   	   return voidApprovalStatus;

   }

   public void setVoidApprovalStatus(String voidApprovalStatus) {

   	   this.voidApprovalStatus = voidApprovalStatus;

   }

   public String getCreatedBy() {

   	   return createdBy;

   }

   public void setCreatedBy(String createdBy) {

   	  this.createdBy = createdBy;

   }

   public String getDateCreated() {

   	   return dateCreated;

   }

   public void setDateCreated(String dateCreated) {

   	   this.dateCreated = dateCreated;

   }

   public String getLastModifiedBy() {

   	  return lastModifiedBy;

   }

   public void setLastModifiedBy(String lastModifiedBy) {

   	  this.lastModifiedBy = lastModifiedBy;

   }

   public String getDateLastModified() {

   	  return dateLastModified;

   }

   public void setDateLastModified(String dateLastModified) {

   	  this.dateLastModified = dateLastModified;

   }


   public String getApprovedRejectedBy() {

   	  return approvedRejectedBy;

   }

   public void setApprovedRejectedBy(String approvedRejectedBy) {

   	  this.approvedRejectedBy = approvedRejectedBy;

   }

   public String getDateApprovedRejected() {

   	  return dateApprovedRejected;

   }

   public void setDateApprovedRejected(String dateApprovedRejected) {

   	  this.dateApprovedRejected = dateApprovedRejected;

   }

   public String getPostedBy() {

   	  return postedBy;

   }

   public void setPostedBy(String postedBy) {

   	  this.postedBy = postedBy;

   }

   public String getDatePosted() {

   	  return datePosted;

   }

   public void setDatePosted(String datePosted) {

   	  this.datePosted = datePosted;

   }

   public String getFunctionalCurrency() {

   	   return functionalCurrency;

   }

   public void setFunctionalCurrency(String functionalCurrency) {

   	   this.functionalCurrency = functionalCurrency;

   }

   public boolean getEnableFields() {

   	   return enableFields;

   }

   public void setEnableFields(boolean enableFields) {

   	   this.enableFields = enableFields;

   }

   public boolean getShowBatchName() {

   	   return showBatchName;

   }

   public void setShowBatchName(boolean showBatchName) {

   	   this.showBatchName = showBatchName;

   }

   public boolean getEnableCreditMemoVoid() {

   	   return enableCreditMemoVoid;

   }

   public void setEnableCreditMemoVoid(boolean enableCreditMemoVoid) {

   	   this.enableCreditMemoVoid = enableCreditMemoVoid;

   }

   public boolean getShowAddLinesButton() {

   	   return showAddLinesButton;

   }

   public void setShowAddLinesButton(boolean showAddLinesButton) {

   	   this.showAddLinesButton = showAddLinesButton;

   }

   public boolean getShowDeleteLinesButton() {

   	   return showDeleteLinesButton;

   }

   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

   	   this.showDeleteLinesButton = showDeleteLinesButton;

   }

   public boolean getShowSaveSubmitButton() {

   	   return showSaveSubmitButton;

   }

   public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {

   	   this.showSaveSubmitButton = showSaveSubmitButton;

   }


   public boolean getShowSaveAsDraftButton() {

   	   return showSaveAsDraftButton;

   }

   public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {

   	   this.showSaveAsDraftButton = showSaveAsDraftButton;

   }

   public boolean getShowDeleteButton() {

   	   return showDeleteButton;

   }

   public void setShowDeleteButton(boolean showDeleteButton) {

   	   this.showDeleteButton = showDeleteButton;

   }

   public boolean getShowJournalButton() {

   	   return showJournalButton;

   }

   public void setShowJournalButton(boolean showJournalButton) {

   	   this.showJournalButton = showJournalButton;

   }

   public boolean getShowShift() {

   	   return showShift;

   }

   public void setShowShift(boolean showShift) {

   	   this.showShift = showShift;

   }


   public String getIsTypeEntered() {

		return isTypeEntered;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {


		this.type = type;

	}

	public ArrayList getTypeList() {

		return typeList;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(String location) {

		locationList.add(location);

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public String getShift() {

		return shift;

	}

	public void setShift(String shift) {

		this.shift = shift;

	}

	public ArrayList getShiftList() {

		return shiftList;

	}

	public void setShiftList(String shift) {

		shiftList.add(shift);

	}

	public void clearShiftList() {

		shiftList.clear();
		shiftList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getUseCustomerPulldown() {

		return useCustomerPulldown;

	}

	public void setUseCustomerPulldown(boolean useCustomerPulldown) {

		this.useCustomerPulldown = useCustomerPulldown;

	}

	public boolean getSubjectToCommission() {

		return subjectToCommission;

	}

	public void setSubjectToCommission(boolean subjectToCommission) {

		this.subjectToCommission = subjectToCommission;

	}

    public String getCustomerName() {

    	return customerName;

    }

    public void setCustomerName(String customerName) {

    	this.customerName = customerName;

    }

    public String getIsCustomerEntered() {

		return isCustomerEntered;

	}

public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {

		return filename3;

	}

	public void setFilename3(FormFile filename3) {

		this.filename3 = filename3;

	}

	public FormFile getFilename4() {

		return filename4;

	}

	public void setFilename4(FormFile filename4) {

		this.filename4 = filename4;

	}

	public boolean getShowViewAttachmentButton1() {

		return showViewAttachmentButton1;

	}

	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

		this.showViewAttachmentButton1 = showViewAttachmentButton1;

	}

	public boolean getShowViewAttachmentButton2() {

		return showViewAttachmentButton2;

	}

	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

		this.showViewAttachmentButton2 = showViewAttachmentButton2;

	}

	public boolean getShowViewAttachmentButton3() {

		return showViewAttachmentButton3;

	}

	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

		this.showViewAttachmentButton3 = showViewAttachmentButton3;

	}

	public boolean getShowViewAttachmentButton4() {

		return showViewAttachmentButton4;

	}

	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

		this.showViewAttachmentButton4 = showViewAttachmentButton4;

	}


		public Object[] getApproverList(){

		return(approverList.toArray());

	}

	public void saveApproverList(Object newApprover){

		approverList.add(newApprover);

	}

	public void deleteApproverList(int rowSelected){

		approverList.remove(rowSelected);

	}

	public int getApproverListSize(){

		return(approverList.size());

	}

	public void clearApproverList(){

		approverList.clear();

	}


   public void reset(ActionMapping mapping, HttpServletRequest request){

       customer = Constants.GLOBAL_BLANK;
	   date = null;
	   invoiceNumber = null;
	   cmNumber = null;
	   cmReferenceNumber = null;
	   srrNumber = null;
	  // amount = null;
	   creditMemoVoid = false;
	   totalDebit = null;
	   totalCredit = null;
	   description = null;
	   approvalStatus = null;
	   reasonForRejection = null;
	   posted = null;
	   voidPosted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;
	   isTypeEntered = null;
	   typeList.clear();
	   typeList.add("ITEMS");
	   typeList.add("SO MATCHED");
	   typeList.add("JO MATCHED");
	   typeList.add("MEMO LINES");
	   location = Constants.GLOBAL_BLANK;
	   shift = Constants.GLOBAL_BLANK;
	   subjectToCommission = false;
	   customerName = Constants.GLOBAL_BLANK;
	   isCustomerEntered = null;
	   filename1 = null;
	   filename2 = null;
	   filename3 = null;
	   filename4 = null;
	   partialCm = false;

	   taxCode = null;
	   withholdingTaxCode = null;
	   for (int i=0; i<arCLIList.size(); i++) {

			ArCreditMemoLineItemList actionList = (ArCreditMemoLineItemList)arCLIList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);
			actionList.setAutoBuildCheckbox(false);
		  	actionList.setIsAssemblyItem(false);

		}


   }

   public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
	   //ActionErrors errors = new ActionErrors();

	   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
	   String separator = "$";

	   // Remove first $ character
	   misc = misc.substring(1);

	   // Counter
	   int start = 0;
	   int nextIndex = misc.indexOf(separator, start);
	   int length = nextIndex - start;
	   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
	   System.out.println("ctr :" + ctr);

	   /*if(y==0)
		   return new ArrayList();*/

	   ArrayList miscList = new ArrayList();

	   for(int x=0; x<ctr; x++) {
		   try {

			   // Date
			   start = nextIndex + 1;
			   nextIndex = misc.indexOf(separator, start);
			   length = nextIndex - start;
			   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			   sdf.setLenient(false);*/
			   String checker = misc.substring(start, start + length);
			   if(checker!=""&& checker != " "){
				   miscList.add(checker);
			   }else{
				   miscList.add("null");
			   }

			   //System.out.println(misc.substring(start, start + length));
		   } catch (Exception ex) {
			   ex.printStackTrace();
		   }

	   	   }
	   return miscList;
   }


   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null ||
	     request.getParameter("saveAsDraftButton") != null || request.getParameter("printButton") != null ||
         request.getParameter("journalButton") != null){

      	 if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
          	    showBatchName){
                errors.add("batchName",
                   new ActionMessage("creditMemoEntry.error.batchNameRequired"));
         }

      	 if((Common.validateRequired(shift) || shift.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
          	    showShift && type.equals("ITEMS")){
                errors.add("shift",
                   new ActionMessage("invoiceEntry.error.shiftRequired"));
         }

      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("creditMemoEntry.error.customerRequired"));
         }

      	 if(Common.validateRequired(date)){
            errors.add("date",
            		new ActionMessage("creditMemoEntry.error.dateRequired"));
      	 }

      	 if(!Common.validateDateFormat(date)){
      		 errors.add("date",
      				 new ActionMessage("creditMemoEntry.error.dateInvalid"));
      	 }

      	 System.out.println(date.lastIndexOf("/"));
      	 System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 if(year<1900){
      		 errors.add("date",
      				 new ActionMessage("creditMemoEntry.error.dateInvalid"));
      	 }

		 if(Common.validateRequired(invoiceNumber)){
            errors.add("invoiceNumber",
               new ActionMessage("creditMemoEntry.error.invoiceNumberRequired"));
         }

         if (type.equals("MEMO LINES")) {

			int numberOfLines = 0;

			Iterator i = arCMList.iterator();

			while (i.hasNext()) {

				ArCreditMemoEntryList drList = (ArCreditMemoEntryList)i.next();

				if (Common.validateRequired(drList.getAccount()) &&
						Common.validateRequired(drList.getDebitAmount()) &&
						Common.validateRequired(drList.getCreditAmount())) continue;

				numberOfLines++;

				if(Common.validateRequired(drList.getAccount())){
					errors.add("account",
							new ActionMessage("creditMemoEntry.error.accountRequired", drList.getLineNumber()));
				}

				if(!Common.validateMoneyFormat(drList.getDebitAmount()== null ? "0": drList.getDebitAmount())){
					errors.add("debitAmount",
							new ActionMessage("creditMemoEntry.error.debitAmountInvalid", drList.getLineNumber()));
				}

				if(!Common.validateMoneyFormat(drList.getCreditAmount()== null ? "0": drList.getCreditAmount())){
					errors.add("creditAmount",
							new ActionMessage("creditMemoEntry.error.creditAmountInvalid", drList.getLineNumber()));
				}
				if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
					errors.add("distributionRecordAmount",
							new ActionMessage("creditMemoEntry.error.debitCreditAmountRequired", drList.getLineNumber()));
				}
				if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
					errors.add("distributionRecordAmount",
							new ActionMessage("creditMemoEntry.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
				}

			}

			if (numberOfLines == 0) {

				errors.add("customer",
						new ActionMessage("creditMemoEntry.error.creditMemoMustHaveLine"));

			}

		} else {

	     	int numOfLines = 0;

				Iterator i = arCLIList.iterator();

				while (i.hasNext()) {

					ArCreditMemoLineItemList cliList = (ArCreditMemoLineItemList)i.next();

					if (Common.validateRequired(cliList.getLocation()) &&
							Common.validateRequired(cliList.getItemName()) &&
							Common.validateRequired(cliList.getQuantity()) &&
							Common.validateRequired(cliList.getUnit()) &&
							Common.validateRequired(cliList.getUnitPrice())) continue;

					numOfLines++;
					/*
					try{
		      	 		String separator = "$";
		      	 		String misc = "";
		      		   // Remove first $ character
		      		   misc = cliList.getMisc().substring(1);

		      		   // Counter
		      		   int start = 0;
		      		   int nextIndex = misc.indexOf(separator, start);
		      		   int length = nextIndex - start;
		      		   int counter;
		      		   counter = Integer.parseInt(misc.substring(start, start + length));

		      	 		ArrayList miscList = this.getExpiryDateStr(cliList.getMisc(), counter);
		      	 		System.out.println("rilList.getMisc() : " + cliList.getMisc());
		      	 		Iterator mi = miscList.iterator();

		      	 		int ctrError = 0;
		      	 		int ctr = 0;

		      	 		while(mi.hasNext()){
		      	 				String miscStr = (String)mi.next();

		      	 				if(!Common.validateDateFormat(miscStr)){
		      	 					errors.add("date",
		      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
		      	 					ctrError++;
		      	 				}

		      	 				System.out.println("miscStr: "+miscStr);
		      	 				if(miscStr=="null"){
		      	 					ctrError++;
		      	 				}
		      	 		}
		      	 		//if ctr==Error => No Date Will Apply
		      	 		//if ctr>error => Invalid Date
		      	 		System.out.println("CTR: "+  miscList.size());
		      	 		System.out.println("ctrError: "+  ctrError);
		      	 		System.out.println("counter: "+  counter);
		      	 			if(ctrError>0 && ctrError!=miscList.size()){
		      	 				errors.add("date",
		      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
		      	 			}

		      	 		//if error==0 Add Expiry Date


		      	 	}catch(Exception ex){
		  	  	    	ex.printStackTrace();
		  	  	    }*/

					if(Common.validateRequired(cliList.getItemName()) || cliList.getItemName().equals(Constants.GLOBAL_BLANK)){
						errors.add("itemName",
								new ActionMessage("creditMemoEntry.error.itemNameRequired", cliList.getLineNumber()));
					}
					if(Common.validateRequired(cliList.getLocation()) || cliList.getLocation().equals(Constants.GLOBAL_BLANK)){
						errors.add("location",
								new ActionMessage("creditMemoEntry.error.locationRequired", cliList.getLineNumber()));
					}
					if(Common.validateRequired(cliList.getQuantity())) {
						errors.add("quantity",
								new ActionMessage("creditMemoEntry.error.quantityRequired", cliList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(cliList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("creditMemoEntry.error.quantityInvalid", cliList.getLineNumber()));
					}
					if(!Common.validateRequired(cliList.getQuantity()) && Common.convertStringMoneyToDouble(cliList.getQuantity(), (short)3) <= 0) {
		         		errors.add("quantity",
		         		 		new ActionMessage("creditMemoEntry.error.negativeOrZeroQuantityNotAllowed", cliList.getLineNumber()));
		            }
					if(Common.validateRequired(cliList.getUnit()) || cliList.getUnit().equals(Constants.GLOBAL_BLANK)){
						errors.add("unit",
								new ActionMessage("creditMemoEntry.error.unitRequired", cliList.getLineNumber()));
					}
					if(Common.validateRequired(cliList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("creditMemoEntry.error.unitPriceRequired", cliList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(cliList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("creditMemoEntry.error.unitPriceInvalid", cliList.getLineNumber()));
					}

				}

				if (numOfLines == 0) {

					errors.add("customer",
							new ActionMessage("creditMemoEntry.error.creditMemoMustHaveLine"));

				}

	     }

      }

      return(errors);
   }
}
