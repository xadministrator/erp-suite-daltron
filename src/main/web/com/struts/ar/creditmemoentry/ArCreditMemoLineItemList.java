package com.struts.ar.creditmemoentry;

import java.io.Serializable;
import java.util.ArrayList;

public class ArCreditMemoLineItemList implements Serializable {
	
	private Integer creditMemoLineItemCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String quantity = null;	
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private String unitPrice = null;
	private String amount = null;

	private boolean deleteCheckbox = false;
	private boolean autoBuildCheckbox = false;
	
	private String isItemEntered = null;
	private String isUnitEntered = null;
	private boolean isAssemblyItem = false;
	
	private boolean taxable = false;
	
	private String misc = null;
	
	private ArCreditMemoEntryForm parentBean;
	
	public ArCreditMemoLineItemList(ArCreditMemoEntryForm parentBean,
			Integer creditMemoLineItemCode,
			String lineNumber, 
			String location,
			String itemName, 
			String itemDescription, 
			String quantity, 	   
			String unit,
			String unitPrice,
			String amount,
			boolean isAssemblyItem,
			boolean autoBuildCheckbox,
			String misc,
			boolean taxable){
		
		this.parentBean = parentBean;
		this.creditMemoLineItemCode = creditMemoLineItemCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.quantity = quantity;	  
		this.unit = unit;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.isAssemblyItem = isAssemblyItem;
		this.autoBuildCheckbox = autoBuildCheckbox;
		this.misc = misc;
		this.taxable = taxable;
	}
	
	public Integer getDebitMemoLineItemCode(){
		
		return(creditMemoLineItemCode);
		
	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
			
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public ArrayList getLocationList() {
		
		return locationList;
		
	}
	
	public void setLocationList(ArrayList locationList) {
		
		this.locationList = locationList;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	public String getUnit() {
		
		return unit;
		
	}
	
	public void setUnit(String unit) {
		
		this.unit = unit;
		
	}
	
	public ArrayList getUnitList() {
		
		return unitList;
		
	}
	
	public void setUnitList(String unit) {
		
		unitList.add(unit);
		
	}
	
	public void clearUnitList() {
		
		unitList.clear();
		
	}
			
	public String getUnitPrice() {
		
		return unitPrice;		
		
	}
	
	public void setUnitPrice(String unitPrice) {
		
		this.unitPrice = unitPrice;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
	public boolean getDeleteCheckbox() {
		
		return deleteCheckbox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckbox) {
		
		this.deleteCheckbox = deleteCheckbox;
		
	}
	
	public boolean getIsAssemblyItem() {
		
		return isAssemblyItem;
		
	}
	
	public void setIsAssemblyItem(boolean isAssemblyItem) {
		
		this.isAssemblyItem = isAssemblyItem;
		
	}
	
	public boolean getAutoBuildCheckbox() {
		
		return autoBuildCheckbox;
		
	}
	
	public void setAutoBuildCheckbox(boolean autoBuildCheckbox) {
		
		this.autoBuildCheckbox = autoBuildCheckbox;
		
	}
	
	public String getIsItemEntered() {
		
		return isItemEntered;
		
	}
	
	public void setIsItemEntered(String isItemEntered) {
		
		if (isItemEntered != null && isItemEntered.equals("true")) {
			
			parentBean.setRowCLISelected(this, false);
			
		}
		
		isItemEntered = null;
		
	}
	
	public String getIsUnitEntered() {
		
		return isUnitEntered;
		
	}
	
	public void setIsUnitEntered(String isUnitEntered) {	
		
		if (isUnitEntered != null && isUnitEntered.equals("true")) {
			
			parentBean.setRowCLISelected(this, false);
			
		}				
		
		isUnitEntered = null;
		
	}
	
	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;
		System.out.println("MISC" + misc);

	}
	
	
	public boolean getTaxable() {

		return taxable;

	}

	public void setTaxable(boolean taxable) {

		this.taxable = taxable;


	}
	
}