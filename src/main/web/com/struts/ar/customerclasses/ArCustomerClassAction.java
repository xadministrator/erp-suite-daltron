package com.struts.ar.customerclasses;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ArCCCoaGlEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedPenaltyAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArCustomerClassController;
import com.ejb.txn.ArCustomerClassControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModCustomerClassDetails;

public final class ArCustomerClassAction extends Action {
	
    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
      	
      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArCustomerClassAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ArCustomerClassForm actionForm = (ArCustomerClassForm)form;

        String frParam = Common.getUserPermission(user, Constants.AR_CUSTOMER_CLASS_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("arCustomerClasses");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ArCustomerClassController EJB
*******************************************************/

        ArCustomerClassControllerHome homeCC = null;
        ArCustomerClassController ejbCC = null;

        try {

            homeCC = (ArCustomerClassControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArCustomerClassControllerEJB", ArCustomerClassControllerHome.class);
            
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArCustomerClassAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbCC = homeCC.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArCustomerClassAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();


/*******************************************************
   Call ArCustomerClassController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         
         try {
         	
            precisionUnit = ejbCC.getGlFcPrecisionUnit(user.getCmpCode());        
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArCustomerClassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         
/*******************************************************
   -- Ar CC Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arCustomerClasses"));
	     
/*******************************************************
   -- Ar CC Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arCustomerClasses"));                  
         
/*******************************************************
   -- Ar CC Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArModCustomerClassDetails mdetails = new ArModCustomerClassDetails();
            mdetails.setCcName(actionForm.getName());
            mdetails.setCcDescription(actionForm.getDescription());
            mdetails.setCcNextCustomerCode(actionForm.getNextCustomerCode());
            mdetails.setCcCustomerBatch(actionForm.getCustomerBatch());
            mdetails.setCcDealPrice(actionForm.getDealPrice());
            mdetails.setCcMonthlyInterestRate(Common.convertStringMoneyToDouble(actionForm.getMonthlyInterestRate(),precisionUnit));
            mdetails.setCcGlCoaReceivableAccountNumber(actionForm.getReceivableAccount());
            mdetails.setCcGlCoaRevenueAccountNumber(actionForm.getRevenueAccount());
            mdetails.setCcGlCoaUnEarnedInterestAccountNumber(actionForm.getUnEarnedInterestAccount());
            mdetails.setCcGlCoaEarnedInterestAccountNumber(actionForm.getEarnedInterestAccount());
            mdetails.setCcGlCoaUnEarnedPenaltyAccountNumber(actionForm.getUnEarnedPenaltyAccount());
            mdetails.setCcGlCoaEarnedPenaltyAccountNumber(actionForm.getEarnedPenaltyAccount());
            mdetails.setCcCreditLimit(Common.convertStringMoneyToDouble(actionForm.getCreditLimit(),precisionUnit));
            
            mdetails.setCcEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            mdetails.setCcEnableRebate(Common.convertBooleanToByte(actionForm.getEnableRebate())); 
            mdetails.setCcAutoComputeInterest(Common.convertBooleanToByte(actionForm.getAutoComputeInterest()));
            mdetails.setCcAutoComputePenalty(Common.convertBooleanToByte(actionForm.getAutoComputePenalty()));
           
            try {
            	
            	ejbCC.addArCcEntry(mdetails, actionForm.getTaxCode(), actionForm.getWithholdingTaxCode(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("customerClass.error.recordAlreadyExist"));
                  	
            } catch (ArCCCoaGlReceivableAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.receivableAccountNotFound"));

            } catch (ArCCCoaGlRevenueAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.revenueAccountNotFound"));                  	                  	

            } catch (ArCCCoaGlUnEarnedInterestAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.unEarnedInterestAccountNotFound")); 
               	
            } catch (ArCCCoaGlEarnedInterestAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.earnedInterestAccountNotFound"));                  	                  	

             
            } catch (ArCCCoaGlUnEarnedPenaltyAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.unEarnedPenaltyAccountNotFound")); 
               	
            } catch (ArCCCoaGlEarnedPenaltyAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.earnedPenaltyAccountNotFound"));                  	                  	

               	
            } catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in ArCustomerClassAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	return mapping.findForward("cmnErrorPage"); 
                  
               	}

            }
            
/*******************************************************
   -- Ar CC Memo Line Class Action --
*******************************************************/
            
        } else if (request.getParameter("memoLineClassButton") != null) {

            String path = "/arStandardMemoLineClass.do?forward=1" +
	           "&customerClassName=" + actionForm.getName() + "&isCustomerClassEntered=1"; 
	           

            return(new ActionForward(path));
            
          

/*******************************************************
   -- Ar CC Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar CC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            ArCustomerClassList arCCList =
	            actionForm.getArCCByIndex(actionForm.getRowSelected());
            
            
            ArModCustomerClassDetails mdetails = new ArModCustomerClassDetails();
            mdetails.setCcCode(arCCList.getCustomerClassCode());
            mdetails.setCcName(actionForm.getName());
            mdetails.setCcDescription(actionForm.getDescription());
            mdetails.setCcNextCustomerCode(actionForm.getNextCustomerCode());
            mdetails.setCcCustomerBatch(actionForm.getCustomerBatch());
            mdetails.setCcDealPrice(actionForm.getDealPrice());
            mdetails.setCcMonthlyInterestRate(Common.convertStringMoneyToDouble(actionForm.getMonthlyInterestRate(),precisionUnit));
                                                  
            mdetails.setCcGlCoaReceivableAccountNumber(actionForm.getReceivableAccount());
            mdetails.setCcGlCoaRevenueAccountNumber(actionForm.getRevenueAccount());
            mdetails.setCcGlCoaUnEarnedInterestAccountNumber(actionForm.getUnEarnedInterestAccount());
            mdetails.setCcGlCoaEarnedInterestAccountNumber(actionForm.getEarnedInterestAccount());
            mdetails.setCcGlCoaUnEarnedPenaltyAccountNumber(actionForm.getUnEarnedPenaltyAccount());
            mdetails.setCcGlCoaEarnedPenaltyAccountNumber(actionForm.getEarnedPenaltyAccount());
            mdetails.setCcCreditLimit(Common.convertStringMoneyToDouble(actionForm.getCreditLimit(),precisionUnit));
            
            mdetails.setCcEnable(Common.convertBooleanToByte(actionForm.getEnable())); 
            mdetails.setCcEnableRebate(Common.convertBooleanToByte(actionForm.getEnableRebate())); 
            System.out.println("actionForm.getAutoComputeInterest()="+actionForm.getAutoComputeInterest());
            System.out.println("actionForm.getAutoComputePenalty()="+actionForm.getAutoComputePenalty());
            mdetails.setCcAutoComputeInterest(Common.convertBooleanToByte(actionForm.getAutoComputeInterest()));
            mdetails.setCcAutoComputePenalty(Common.convertBooleanToByte(actionForm.getAutoComputePenalty()));
            mdetails.setCcEnable(Common.convertBooleanToByte(actionForm.getEnable())); 
            try {
            	
            	ejbCC.updateArCcEntry(mdetails, actionForm.getTaxCode(), actionForm.getWithholdingTaxCode(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("customerClass.error.recordAlreadyExist"));
                  	
            } catch (ArCCCoaGlReceivableAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.receivableAccountNotFound"));

            } catch (ArCCCoaGlRevenueAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.revenueAccountNotFound")); 
               	
            } catch (ArCCCoaGlUnEarnedInterestAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.unEarnedInterestAccountNotFound")); 
               	
            } catch (ArCCCoaGlEarnedInterestAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.earnedInterestAccountNotFound"));                  	                  	

             
            } catch (ArCCCoaGlUnEarnedPenaltyAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.unEarnedPenaltyAccountNotFound")); 
               	
            } catch (ArCCCoaGlEarnedPenaltyAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("customerClass.error.earnedPenaltyAccountNotFound"));                  	                  	


            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerClassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ar CC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ar CC Edit Action --
*******************************************************/

         } else if (request.getParameter("arCCList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showArCCRow(actionForm.getRowSelected());
            
            return mapping.findForward("arCustomerClasses");
            
/*******************************************************
   -- Ar CC Delete Action --
*******************************************************/

         } else if (request.getParameter("arCCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ArCustomerClassList arCCList =
              actionForm.getArCCByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbCC.deleteArCcEntry(arCCList.getCustomerClassCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("customerClass.error.deleteCustomerClassAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("customerClass.error.customerClassAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCustomerClassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar CC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arCustomerClasses");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               	actionForm.clearArCCList();	        
                              	           
               	actionForm.clearTaxCodeList();           	
            	
               	list = ejbCC.getArTcAll(user.getCmpCode());
            	
               	if (list == null || list.size() == 0) {
            		
                	actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
               	} else {
            		           		            		
            		i = list.iterator();
            		
                  	while (i.hasNext()) {
            			
            	    	actionForm.setTaxCodeList((String)i.next());
            			
            	  	}
            		            		
               	} 	           
               
               	actionForm.clearWithholdingTaxCodeList();           	
            	
               	list = ejbCC.getArWtcAll(user.getCmpCode());
            	
               	if (list == null || list.size() == 0) {
            		
                   	actionForm.setWithholdingTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
               	} else {
            		           		            		
               	i = list.iterator();
            		
                  	while (i.hasNext()) {
            			
            	    	actionForm.setWithholdingTaxCodeList((String)i.next());
            			
            	  	}
            		            		
               	}
               	
               	
               	actionForm.clearCustomerBatchList();           	
         		
         		list = ejbCC.getAdLvCustomerBatchAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setCustomerBatchList((String)i.next());
         				
         			}
         			
         		}
         		
         		actionForm.clearDealPriceList();    
            	
        		list = ejbCC.getAdLvInvPriceLevelAll(user.getCmpCode());
        		
        		if (list == null || list.size() == 0) {
        			
        			actionForm.setDealPriceList(Constants.GLOBAL_NO_RECORD_FOUND);
        			
        		} else {
        			
        			i = list.iterator();
        			
        			while (i.hasNext()) {
        				
        				actionForm.setDealPriceList((String)i.next());
        				
        			}
        			
        		}
               	
               	
               	
               	actionForm.setEnableRevenueAccount(ejbCC.getArCcGlCoaRevenueAccountEnable(user.getCmpCode())); 	                          
               	               	
	           	list = ejbCC.getArCcAll(user.getCmpCode()); 
	           
	           	i = list.iterator();
	            
	           	while(i.hasNext()) {
        	            			            	
	              	ArModCustomerClassDetails mdetails = (ArModCustomerClassDetails)i.next();
	            	
	              	ArCustomerClassList arCCList = new ArCustomerClassList(actionForm,
	            		mdetails.getCcCode(),
	            	    mdetails.getCcName(),
	            	    mdetails.getCcDescription(),
	            	    mdetails.getCcNextCustomerCode(),
	            	    mdetails.getCcCustomerBatch(),
	            	    mdetails.getCcDealPrice(),
	            	    Common.convertDoubleToStringMoney(mdetails.getCcMonthlyInterestRate(),precisionUnit),
	            	    
	            	    mdetails.getCcTcName(),
	            	    mdetails.getCcWtcName(),
                        mdetails.getCcGlCoaReceivableAccountNumber(),
                        mdetails.getCcGlCoaReceivableAccountDescription(),
                        mdetails.getCcGlCoaRevenueAccountNumber(),
                        mdetails.getCcGlCoaRevenueAccountDescription(),
                        mdetails.getCcGlCoaUnEarnedInterestAccountNumber(),
                        mdetails.getCcGlCoaUnEarnedInterestAccountDescription(),
                        mdetails.getCcGlCoaEarnedInterestAccountNumber(),
                        mdetails.getCcGlCoaEarnedInterestAccountDescription(),
                        mdetails.getCcGlCoaUnEarnedPenaltyAccountNumber(),
                        mdetails.getCcGlCoaUnEarnedPenaltyAccountDescription(),
                        mdetails.getCcGlCoaEarnedPenaltyAccountNumber(),
                        mdetails.getCcGlCoaEarnedPenaltyAccountDescription(),
                        
                        Common.convertByteToBoolean(mdetails.getCcEnable()),
                        Common.convertByteToBoolean(mdetails.getCcEnableRebate()),
                        Common.convertByteToBoolean(mdetails.getCcAutoComputeInterest()),
                        Common.convertByteToBoolean(mdetails.getCcAutoComputePenalty()),
                        Common.convertDoubleToStringMoney(mdetails.getCcCreditLimit(),precisionUnit)); 
	              	 	            	    
	              	actionForm.saveArCCList(arCCList);
	            	
	           	}
	           	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerClassAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            actionForm.setEnableRebate(false);
            actionForm.setAutoComputeInterest(false);
            actionForm.setAutoComputePenalty(false);
            actionForm.setMonthlyInterestRate("0.00");
            actionForm.setCreditLimit("0.00");
            
            return(mapping.findForward("arCustomerClasses"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ArCustomerClassAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}