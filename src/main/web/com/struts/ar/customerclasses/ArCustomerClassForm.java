package com.struts.ar.customerclasses;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArCustomerClassForm extends ActionForm implements Serializable {

	private Integer customerClassCode = null;
	private String name = null;
	private String description = null;
	private String nextCustomerCode = null;
	private String monthlyInterestRate = null;
	private String taxCode = null;
	private ArrayList taxCodeList = new ArrayList();
	private String withholdingTaxCode = null;
	private ArrayList withholdingTaxCodeList = new ArrayList();
	private String receivableAccount =  null;
	private String receivableAccountDescription = null;
	private String revenueAccount =  null;
	private String revenueAccountDescription = null;
	private boolean enableRevenueAccount = false;
	
	private boolean enable = false;
	private boolean enableRebate = false;
	private boolean autoComputeInterest = false;
	private boolean autoComputePenalty = false;
	private String creditLimit = null;
	
	
	private String unEarnedInterestAccount = null;
        private String unEarnedInterestAccountDescription = null;
        private String earnedInterestAccount = null;
        private String earnedInterestAccountDescription = null;

        private String unEarnedPenaltyAccount = null;
        private String unEarnedPenaltyAccountDescription = null;
        private String earnedPenaltyAccount = null;
        private String earnedPenaltyAccountDescription = null;

        private String customerBatch = null;
        private ArrayList customerBatchList = new ArrayList();

        private String dealPrice = null;
        private ArrayList dealPriceList = new ArrayList();

	
	private String tableType = null;
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
        private String memoLineClassButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList arCCList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
	
	    return rowSelected;
	
	}
	
	public ArCustomerClassList getArCCByIndex(int index) {
	
	    return((ArCustomerClassList)arCCList.get(index));
	
	}
	
	public Object[] getArCCList() {
	
	    return arCCList.toArray();
	
	}
	
	public int getArCCListSize() {
	
	    return arCCList.size();
	
	}
	
	public void saveArCCList(Object newArCCList) {
	
	    arCCList.add(newArCCList);
	
	}
	
	public void clearArCCList() {
		
	    arCCList.clear();
	  
	}
	
	public void setRowSelected(Object selectedArCCList, boolean isEdit) {
	
	    this.rowSelected = arCCList.indexOf(selectedArCCList);
	
	    if (isEdit) {
	
	        this.pageState = Constants.PAGE_STATE_EDIT;
	
	    }
	
	}
	
	public void showArCCRow(int rowSelected) {
		
		this.name = ((ArCustomerClassList)arCCList.get(rowSelected)).getName();
		this.description = ((ArCustomerClassList)arCCList.get(rowSelected)).getDescription();
		this.nextCustomerCode = ((ArCustomerClassList)arCCList.get(rowSelected)).getNextCustomerCode();
		this.customerBatch = ((ArCustomerClassList)arCCList.get(rowSelected)).getCustomerBatch();
		this.dealPrice = ((ArCustomerClassList)arCCList.get(rowSelected)).getDealPrice();
		this.monthlyInterestRate = ((ArCustomerClassList)arCCList.get(rowSelected)).getMonthlyInterestRate();
		
		if (!this.taxCodeList.contains(((ArCustomerClassList)arCCList.get(rowSelected)).getTaxCode())) {
			
			if (this.taxCodeList.contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				this.taxCodeList.clear();
				this.taxCodeList.add(Constants.GLOBAL_BLANK);
				
			}
			this.taxCodeList.add(((ArCustomerClassList)arCCList.get(rowSelected)).getTaxCode());
			
		}		
		this.taxCode = ((ArCustomerClassList)arCCList.get(rowSelected)).getTaxCode();
		
		if (!this.withholdingTaxCodeList.contains(((ArCustomerClassList)arCCList.get(rowSelected)).getWithholdingTaxCode())) {
			
			if (this.withholdingTaxCodeList.contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				this.withholdingTaxCodeList.clear();
				this.withholdingTaxCodeList.add(Constants.GLOBAL_BLANK);
				
			}
			this.withholdingTaxCodeList.add(((ArCustomerClassList)arCCList.get(rowSelected)).getWithholdingTaxCode());
			
		}		
		this.withholdingTaxCode = ((ArCustomerClassList)arCCList.get(rowSelected)).getWithholdingTaxCode();
		
        this.receivableAccount = ((ArCustomerClassList)arCCList.get(rowSelected)).getReceivableAccount();
        this.receivableAccountDescription = ((ArCustomerClassList)arCCList.get(rowSelected)).getReceivableAccountDescription();
		this.revenueAccount = ((ArCustomerClassList)arCCList.get(rowSelected)).getRevenueAccount();
		this.revenueAccountDescription = ((ArCustomerClassList)arCCList.get(rowSelected)).getRevenueAccountDescription();
		
		this.unEarnedInterestAccount =  ((ArCustomerClassList)arCCList.get(rowSelected)).getUnEarnedInterestAccount();
		this.unEarnedInterestAccountDescription =  ((ArCustomerClassList)arCCList.get(rowSelected)).getUnEarnedInterestAccountDescription();
		this.earnedInterestAccount =  ((ArCustomerClassList)arCCList.get(rowSelected)).getEarnedInterestAccount();
		this.earnedInterestAccountDescription =  ((ArCustomerClassList)arCCList.get(rowSelected)).getEarnedInterestAccountDescription();
		
		this.unEarnedPenaltyAccount =  ((ArCustomerClassList)arCCList.get(rowSelected)).getUnEarnedPenaltyAccount();
		this.unEarnedPenaltyAccountDescription =  ((ArCustomerClassList)arCCList.get(rowSelected)).getUnEarnedPenaltyAccountDescription();
		this.earnedPenaltyAccount =  ((ArCustomerClassList)arCCList.get(rowSelected)).getEarnedPenaltyAccount();
		this.earnedPenaltyAccountDescription =  ((ArCustomerClassList)arCCList.get(rowSelected)).getEarnedPenaltyAccountDescription();
		
		
		this.enable = ((ArCustomerClassList)arCCList.get(rowSelected)).getEnable(); 
		this.enableRebate = ((ArCustomerClassList)arCCList.get(rowSelected)).getEnableRebate();
		this.autoComputeInterest = ((ArCustomerClassList)arCCList.get(rowSelected)).getAutoComputeInterest();
		this.autoComputePenalty = ((ArCustomerClassList)arCCList.get(rowSelected)).getAutoComputePenalty();
		
		
	
		
	    this.creditLimit = ((ArCustomerClassList)arCCList.get(rowSelected)).getCreditLimit();
	}
	
	public void updateArCCRow(int rowSelected, Object newArCCList) {
	
	    arCCList.set(rowSelected, newArCCList);
	
	}
	
	public void deleteArCCList(int rowSelected) {
	
	    arCCList.remove(rowSelected);
	
	}
        
	public void setUpdateButton(String updateButton) {
	
	    this.updateButton = updateButton;
	
	}
        
        
	public void setMemoLineClassButton(String memoLineClassButton) {
	
	    this.memoLineClassButton = memoLineClassButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
	
	    this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	    this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	    this.closeButton = closeButton;
	
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
	    this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
	    this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
	
	    this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	    return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getCustomerClassCode() {
		
		return customerClassCode;
		
	}
	
	public void setCustomerClassCode(Integer customerClassCode) {
		
		this.customerClassCode = customerClassCode;
		
	}
	
	public String getName() {
	
	  	return name;
	
	}
	
	public void setName(String name) {
	
	  	this.name = name;
	
	}
	
	public String getDescription() {
	
		return description;
	
	}
	
	public void setDescription(String description) {
	
	  	this.description = description;
	
	}
	
	public String getNextCustomerCode() {
		
		return nextCustomerCode;
	
	}
	
	public void setNextCustomerCode(String nextCustomerCode) {
	
	  	this.nextCustomerCode = nextCustomerCode;
	
	}
	
	
	public String getMonthlyInterestRate() {
		
		return monthlyInterestRate;
	
	}
	
	public void setMonthlyInterestRate(String monthlyInterestRate) {
	
	  	this.monthlyInterestRate = monthlyInterestRate;
	
	}
	
	
    public String getTaxCode() {
    	
    	return taxCode;
    	
    }
    
    public void setTaxCode(String taxCode) {
    	
    	this.taxCode = taxCode;
    	
    }
    
    public ArrayList getTaxCodeList() {
    	
    	return taxCodeList;
    	
    }
    
    public void setTaxCodeList(String taxCode) {
    	
    	taxCodeList.add(taxCode);
    	
    }
    
    public void clearTaxCodeList() {
    	
    	taxCodeList.clear();
    	taxCodeList.add(Constants.GLOBAL_BLANK);
    	
    }
    
    public String getWithholdingTaxCode() {
    	
    	return withholdingTaxCode;
    	
    }
    
    public void setWithholdingTaxCode(String withholdingTaxCode) {
    	
    	this.withholdingTaxCode = withholdingTaxCode;
    	
    }
    
    public ArrayList getWithholdingTaxCodeList() {
    	
    	return withholdingTaxCodeList;
    	
    }
    
    public void setWithholdingTaxCodeList(String withholdingTaxCode) {
    	
    	withholdingTaxCodeList.add(withholdingTaxCode);
    	
    }
    
    public void clearWithholdingTaxCodeList() {
    	
    	withholdingTaxCodeList.clear();
    	withholdingTaxCodeList.add(Constants.GLOBAL_BLANK);
    	
    }
    
    public String getReceivableAccount() {
    	
    	return receivableAccount;
    	
    }
    
    public void setReceivableAccount(String receivableAccount) {
    	
    	this.receivableAccount = receivableAccount;
    	
    }
    
    public String getReceivableAccountDescription() {
    	
    	return receivableAccountDescription;
    	
    }
    
    public void setReceivableAccountDescription(String receivableAccountDescription) {
    	
    	this.receivableAccountDescription = receivableAccountDescription;
    	
    }
    
    public String getRevenueAccount() {
    	
    	return revenueAccount;
    	
    }
    
    public void setRevenueAccount(String revenueAccount) {
    	
    	this.revenueAccount = revenueAccount;
    	
    }
    
    public String getRevenueAccountDescription() {
    	
    	return revenueAccountDescription;
    	
    }
    
    public void setRevenueAccountDescription(String revenueAccountDescription) {
    	
    	this.revenueAccountDescription = revenueAccountDescription;
    	
    }    
    
    public boolean getEnable() {
    	
    	return enable;
    	
    }
    
    public void setEnable(boolean enable) {
    	
    	this.enable = enable;
    	
    }
    
 public boolean getEnableRebate() {
    	
    	return enableRebate;
    	
    }
 
    
    public void setEnableRebate(boolean enableRebate) {
    	
    	this.enableRebate = enableRebate;
    	
    }
    
    
    
    public boolean getAutoComputeInterest() {
    	
    	return autoComputeInterest;
    	
    }
    
    
    public void setAutoComputeInterest(boolean autoComputeInterest) {
    	
    	this.autoComputeInterest = autoComputeInterest;
    	
    }
    
    public boolean getAutoComputePenalty() {
    	
    	return autoComputePenalty;
    	
    }

    
    public void setAutoComputePenalty(boolean autoComputePenalty) {
    	
    	this.autoComputePenalty = autoComputePenalty;
    	
    }
    
    
    
    public boolean getEnableRevenueAccount() {
    	
    	return enableRevenueAccount;
    	
    }
    
    public void setEnableRevenueAccount(boolean enableRevenueAccount) {
    	
    	this.enableRevenueAccount = enableRevenueAccount;
    	
    }
    
    
    
    
    
    
    
    
    
    
public String getUnEarnedInterestAccount() {
        
        return unEarnedInterestAccount;   	
        
    }
    
    public void setUnEarnedInterestAccount(String unEarnedInterestAccount) {
        
        this.unEarnedInterestAccount = unEarnedInterestAccount;
        
    }
    
    public String getUnEarnedInterestAccountDescription() {
        
        return unEarnedInterestAccountDescription;   	
        
    }
    
    public void setUnEarnedInterestAccountDescription(String unEarnedInterestAccountDescription) {
        
        this.unEarnedInterestAccountDescription = unEarnedInterestAccountDescription;
        
    }
    
    
    public String getEarnedInterestAccount() {
        
        return earnedInterestAccount;   	
        
    }
    
    public void setEarnedInterestAccount(String earnedInterestAccount) {
        
        this.earnedInterestAccount = earnedInterestAccount;
        
    }
    
    public String getEarnedInterestAccountDescription() {
        
        return earnedInterestAccountDescription;   	
        
    }
    
    public void setEarnedInterestAccountDescription(String earnedInterestAccountDescription) {
        
        this.earnedInterestAccountDescription = earnedInterestAccountDescription;
        
    }
    
    
public String getUnEarnedPenaltyAccount() {
        
        return unEarnedPenaltyAccount;   	
        
    }
    
    public void setUnEarnedPenaltyAccount(String unEarnedPenaltyAccount) {
        
        this.unEarnedPenaltyAccount = unEarnedPenaltyAccount;
        
    }
    
    public String getUnEarnedPenaltyAccountDescription() {
        
        return unEarnedPenaltyAccountDescription;   	
        
    }
    
    public void setUnEarnedPenaltyAccountDescription(String unEarnedPenaltyAccountDescription) {
        
        this.unEarnedPenaltyAccountDescription = unEarnedPenaltyAccountDescription;
        
    }
    
    
    public String getEarnedPenaltyAccount() {
        
        return earnedPenaltyAccount;   	
        
    }
    
    public void setEarnedPenaltyAccount(String earnedPenaltyAccount) {
        
        this.earnedPenaltyAccount = earnedPenaltyAccount;
        
    }
    
    public String getEarnedPenaltyAccountDescription() {
        
        return earnedPenaltyAccountDescription;   	
        
    }
    
    public void setEarnedPenaltyDescription(String earnedPenaltyAccountDescription) {
        
        this.earnedPenaltyAccountDescription = earnedPenaltyAccountDescription;
        
    }
    
    
    
    
public String getCustomerBatch() {
       	
        return customerBatch;
        
     }

     public void setCustomerBatch(String customerBatch) {
     	
        this.customerBatch = customerBatch;
        
     }

     public ArrayList getCustomerBatchList() {
     	
        return customerBatchList;
        
     }

     public void setCustomerBatchList(String customerBatch) {
     	
        customerBatchList.add(customerBatch);
        
     }
     
     public void clearCustomerBatchList() {
     	
  	   customerBatchList.clear();
  	   customerBatchList.add(Constants.GLOBAL_BLANK);
        
     }








public String getDealPrice() {
        
        return dealPrice;
        
    }
    
    public void setDealPrice(String dealPrice) {
        
        this.dealPrice = dealPrice;
        
    }
    
    public ArrayList getDealPriceList() {
        
        return dealPriceList;
        
    }
    
    public void setDealPriceList(String dealPrice) {
        
        dealPriceList.add(dealPrice);
        
    }
    
    public void clearDealPriceList() {
        
        dealPriceList.clear();
        dealPriceList.add(Constants.GLOBAL_BLANK);
        
    }
    
    
    
    
	
	public String getTableType() {
		
	  	return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
	  	this.tableType = tableType;
		
	}
	public String getCreditLimit() {
		
	  	return(creditLimit);
		
	}
	
	public void setCreditLimit(String creditLimit) {
		
	  	this.creditLimit = creditLimit;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
	
		name = null;
		description = null;
		nextCustomerCode = null;
		customerBatch = null;
		dealPrice = null;
		monthlyInterestRate = null;
		
        taxCode = Constants.GLOBAL_BLANK;
        withholdingTaxCode = Constants.GLOBAL_BLANK;
        receivableAccount = null;
        revenueAccount = null;
        receivableAccountDescription = null;
        revenueAccountDescription = null;
        
        
        unEarnedInterestAccount = null;
        earnedInterestAccount = null;
        unEarnedPenaltyAccount = null;
        earnedPenaltyAccount = null;

        unEarnedInterestAccountDescription = null;
        earnedInterestAccountDescription = null;
        unEarnedPenaltyAccountDescription = null;
        earnedPenaltyAccountDescription = null;
        
        dealPrice = null;
        customerBatch = null;
        
		enable = false;
		enableRebate = false;
		autoComputeInterest = false;
		autoComputePenalty = false;
		
		showDetailsButton = null;
		hideDetailsButton = null;   
		creditLimit = null;
	
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
			if (Common.validateRequired(name)) {
			
				errors.add("name",
			   		new ActionMessage("customerClass.error.nameRequired"));
			
			}
			
			if (Common.validateRequired(taxCode)) {
			
				errors.add("taxCode",
			   		new ActionMessage("customerClass.error.taxCodeRequired"));
			
			}
			
			if (Common.validateRequired(withholdingTaxCode)) {
			
				errors.add("withholdingTaxCode",
			   		new ActionMessage("customerClass.error.withholdingTaxCodeRequired"));
			
			}			
						
			if (Common.validateRequired(receivableAccount)) {
			
				errors.add("receivableAccount",
			   		new ActionMessage("customerClass.error.receivableAccountRequired"));
			
			}									

			if (Common.validateRequired(revenueAccount) && (enableRevenueAccount == true)) {
			
				errors.add("revenueAccount",
			   		new ActionMessage("customerClass.error.revenueAccountRequired"));
			
			}
                        
                        
                 if(autoComputeInterest){
        
   

                    if (Common.validateRequired(unEarnedInterestAccount)) {

                    errors.add("unEarnedInterestAccount",
                        new ActionMessage("customerClass.error.unEarnedInterestAccount"));

                }

                if (Common.validateRequired(earnedInterestAccount)) {

                    errors.add("earnedInterestAccount",
                            new ActionMessage("customerClass.error.earnedInterestAccount"));

                }


            }

            if(autoComputePenalty){

                if (Common.validateRequired(unEarnedPenaltyAccount)) {

                errors.add("unEarnedPenaltyAccount",
                        new ActionMessage("customerClass.error.unEarnedPenaltyAccount"));

                }

                if (Common.validateRequired(earnedPenaltyAccount)) {

                    errors.add("earnedPenaltyAccount",
                            new ActionMessage("customerClass.error.earnedPenaltyAccount"));

                }
            }
													
	  	}
	     
	  	return errors;
	
	}
	
}