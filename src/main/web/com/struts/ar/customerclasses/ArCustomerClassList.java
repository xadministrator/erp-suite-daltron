package com.struts.ar.customerclasses;

import java.io.Serializable;

public class ArCustomerClassList implements Serializable {

    private Integer customerClassCode = null;
    private String name = null;
    private String description = null;
    private String nextCustomerCode = null;
    private String customerBatch = null;
    private String dealPrice = null;
    private String monthlyInterestRate = null;
    private String taxCode = null;
    private String withholdingTaxCode = null;
    private String receivableAccount = null;
    private String receivableAccountDescription = null;
    private String revenueAccount = null;
    private String revenueAccountDescription = null;
    
    private String unEarnedInterestAccount = null;
    private String unEarnedInterestAccountDescription = null;
    private String earnedInterestAccount = null;
    private String earnedInterestAccountDescription = null;
    
    private String unEarnedPenaltyAccount = null;
    private String unEarnedPenaltyAccountDescription = null;
    private String earnedPenaltyAccount = null;
    private String earnedPenaltyAccountDescription = null;
    
    private boolean enable = false;
    private boolean enableRebate = false;
	private boolean autoComputeInterest = false;
	private boolean autoComputePenalty = false;
	
    private String creditLimit = null;

	private String editButton = null;
	private String deleteButton = null;
	
	private ArCustomerClassForm parentBean;
    
	public ArCustomerClassList(ArCustomerClassForm parentBean,
		Integer customerClassCode,
        String name,
        String description,
        String nextCustomerCode,
        String customerBatch,
        String dealPrice,
        String monthlyInterestRate,
        
        String taxCode,
        String withholdingTaxCode,
        String receivableAccount,
        String receivableAccountDescription,
        String revenueAccount,
        String revenueAccountDescription,
        String unEarnedInterestAccount,
        String unEarnedInterestAccountDescription,
        String earnedInterestAccount,
        String earnedInterestAccountDescription,
        String unEarnedPenaltyAccount,
        String unEarnedPenaltyAccountDescription,
        String earnedPenaltyAccount,
        String earnedPenaltyAccountDescription,
		boolean enable,
		boolean enableRebate,
		boolean autoComputeInterest,
		boolean autoComputePenalty,
		String creditLimit) {
	
		this.parentBean = parentBean;
        this.customerClassCode = customerClassCode;
        this.name = name;
        this.description = description;
        this.nextCustomerCode = nextCustomerCode;
        this.customerBatch = customerBatch;
        this.dealPrice = dealPrice;
        this.monthlyInterestRate = monthlyInterestRate;
        
        this.taxCode = taxCode;
        this.withholdingTaxCode = withholdingTaxCode;
        this.receivableAccount = receivableAccount;
        this.receivableAccountDescription = receivableAccountDescription;
        this.revenueAccount = revenueAccount;
        this.revenueAccountDescription = revenueAccountDescription;
        
        this.unEarnedInterestAccount = unEarnedInterestAccount;
        this.unEarnedInterestAccountDescription = unEarnedInterestAccountDescription;
        this.earnedInterestAccount = earnedInterestAccount;
        this.earnedInterestAccountDescription = earnedInterestAccountDescription;
        
        
        this.unEarnedPenaltyAccount = unEarnedPenaltyAccount;
        this.unEarnedPenaltyAccountDescription = unEarnedPenaltyAccountDescription;
        this.earnedPenaltyAccount = earnedPenaltyAccount;
        this.earnedPenaltyAccountDescription = earnedPenaltyAccountDescription;
        
		this.enable = enable;
		this.enableRebate = enableRebate;
		this.autoComputeInterest = autoComputeInterest;
		this.autoComputePenalty = autoComputePenalty;
		this.creditLimit = creditLimit;
		
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getCustomerClassCode() {
	
	    return customerClassCode;
	
	}
	
	public String getName() {
		
		return name;
		
    }
    
    public String getDescription() {
    	
    	return description;
    	
    }
    
public String getNextCustomerCode() {
    	
    	return nextCustomerCode;
    	
    }
    
    
public String getCustomerBatch() {
    	
    	return customerBatch;
    	
    }

public String getDealPrice() {
	
	return dealPrice;
	
}

public String getMonthlyInterestRate() {
	
	return monthlyInterestRate;
	
}
        
    public String getTaxCode() {
    	
    	return taxCode;
    	
    }
    
    public String getWithholdingTaxCode() {
    	
    	return withholdingTaxCode;
    	
    }
    
    public String getReceivableAccount() {
    	
    	return receivableAccount;
    	
    }
    
    public String getReceivableAccountDescription() {
    	
    	return receivableAccountDescription;
    	
    }
    
    public String getRevenueAccount() {
    	
    	return revenueAccount;
    	
    }
    
    public String getRevenueAccountDescription() {
    	
    	return revenueAccountDescription;
    	
    }
    
    
    
public String getUnEarnedInterestAccount() {
        
        return unEarnedInterestAccount;   	
        
    }
    

    
    public String getUnEarnedInterestAccountDescription() {
        
        return unEarnedInterestAccountDescription;   	
        
    }
    

    
    
    public String getEarnedInterestAccount() {
        
        return earnedInterestAccount;   	
        
    }
    

    
    public String getEarnedInterestAccountDescription() {
        
        return earnedInterestAccountDescription;   	
        
    }
    

    
    
public String getUnEarnedPenaltyAccount() {
        
        return unEarnedPenaltyAccount;   	
        
    }
    

    
    public String getUnEarnedPenaltyAccountDescription() {
        
        return unEarnedPenaltyAccountDescription;   	
        
    }
    

    
    
    public String getEarnedPenaltyAccount() {
        
        return earnedPenaltyAccount;   	
        
    }
    

    
    public String getEarnedPenaltyAccountDescription() {
        
        return earnedPenaltyAccountDescription;   	
        
    }
    

    
    public boolean getEnable() {
    	
    	return enable;
    	
    }
    
public boolean getEnableRebate() {
    	
    	return enableRebate;
    	
    }

public boolean getAutoComputeInterest() {
	
	return autoComputeInterest;
	
}
    
public boolean getAutoComputePenalty() {
	
	return autoComputePenalty;
	
}

    public String getCreditLimit() {
    	
    	return creditLimit;
    	
    }
        	    	
}