package com.struts.ar.customerentry;

import java.io.Serializable;


/**
 * 
 * @author Farrah S. Garing
 * Created: 10/26/2005 2:10 PM
 * 
 */
public class ArBranchCustomerList implements Serializable {
	
	
	private String branchCode = null;
	private String branchName = null;
	private String branchReceivableAccount = null;
	private String branchReceivableDescription = null;
	private String branchRevenueAccount = null;
	private String branchRevenueDescription = null;
	private String branchUnEarnedInterestAccount = null;
	private String branchUnEarnedInterestDescription = null;
	private String branchEarnedInterestAccount = null;
	private String branchEarnedInterestDescription = null;
	
	private String branchUnEarnedPenaltyAccount = null;
	private String branchUnEarnedPenaltyDescription = null;
	private String branchEarnedPenaltyAccount = null;
	private String branchEarnedPenaltyDescription = null;
	
	private boolean branchCheckbox = false;
	
	private ArCustomerEntryForm parentBean;
	
	public ArBranchCustomerList(ArCustomerEntryForm parentBean, String branchCode,
			String branchName) {
		
		this.parentBean = parentBean;
		this.branchCode = branchCode;
		this.branchName = branchName;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}

	public String getBranchCode() {
		
		return this.branchCode;
		
	}
	
	public void setBranchCode(String branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public String getBranchName() {
		
		return this.branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}	
	
	public String getBranchReceivableAccount() {
		
		return this.branchReceivableAccount;
		
	}
	
	public void setBranchReceivableAccount(String branchReceivableAccount) {
		
		this.branchReceivableAccount = branchReceivableAccount;
		
	}
	
	public String getBranchReceivableDescription() {
		
		return this.branchReceivableDescription;
		
	}
	
	public void setBranchReceivableDescription(String branchReceivableDescription) {
		
		this.branchReceivableDescription = branchReceivableDescription;
		
	}
	
	public String getBranchRevenueAccount() {
		
		return this.branchRevenueAccount;
		
	}
	
	public void setBranchRevenueAccount(String branchRevenueAccount) {
		
		this.branchRevenueAccount = branchRevenueAccount;
		
	}
	
	public String getBranchRevenueDescription() {
		
		return this.branchRevenueDescription;
		
	}
	
	public void setBranchRevenueDescription(String branchRevenueDescription) {
		
		this.branchRevenueDescription = branchRevenueDescription;
		
	}
	
	
	
	
public String getBranchUnEarnedInterestAccount() {
		
		return this.branchUnEarnedInterestAccount;
		
	}
	
	public void setBranchUnEarnedInterestAccount(String branchUnEarnedInterestAccount) {
		
		this.branchUnEarnedInterestAccount = branchUnEarnedInterestAccount;
		
	}
	
	public String getBranchUnEarnedInterestDescription() {
		
		return this.branchUnEarnedInterestDescription;
		
	}
	
	public void setBranchUnEarnedInterestDescription(String branchUnEarnedInterestDescription) {
		
		this.branchUnEarnedInterestDescription = branchUnEarnedInterestDescription;
		
	}
	
	
	
public String getBranchEarnedInterestAccount() {
		
		return this.branchEarnedInterestAccount;
		
	}
	
	public void setBranchEarnedInterestAccount(String branchEarnedInterestAccount) {
		
		this.branchEarnedInterestAccount = branchEarnedInterestAccount;
		
	}
	
	public String getBranchEarnedInterestDescription() {
		
		return this.branchEarnedInterestDescription;
		
	}
	
	public void setBranchEarnedInterestDescription(String branchEarnedInterestDescription) {
		
		this.branchEarnedInterestDescription = branchEarnedInterestDescription;
		
	}
	
	
public String getBranchUnEarnedPenaltyAccount() {
		
		return this.branchUnEarnedPenaltyAccount;
		
	}
	
	public void setBranchUnEarnedPenaltyAccount(String branchUnEarnedPenaltyAccount) {
		
		this.branchUnEarnedPenaltyAccount = branchUnEarnedPenaltyAccount;
		
	}
	
	public String getBranchUnEarnedPenaltyDescription() {
		
		return this.branchUnEarnedPenaltyDescription;
		
	}
	
	public void setBranchUnEarnedPenaltyDescription(String branchUnEarnedPenaltyDescription) {
		
		this.branchUnEarnedPenaltyDescription = branchUnEarnedPenaltyDescription;
		
	}
	
	
	
public String getBranchEarnedPenaltyAccount() {
		
		return this.branchEarnedPenaltyAccount;
		
	}
	
	public void setBranchEarnedPenaltyAccount(String branchEarnedPenaltyAccount) {
		
		this.branchEarnedPenaltyAccount = branchEarnedPenaltyAccount;
		
	}
	
	public String getBranchEarnedPenaltyDescription() {
		
		return this.branchEarnedPenaltyDescription;
		
	}
	
	public void setBranchEarnedPenaltyDescription(String branchEarnedPenaltyDescription) {
		
		this.branchEarnedPenaltyDescription = branchEarnedPenaltyDescription;
		
	}
	
}//ArBranchCustomerList

