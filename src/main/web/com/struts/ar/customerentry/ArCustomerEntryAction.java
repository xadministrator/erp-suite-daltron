package com.struts.ar.customerentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ArCCCoaGlReceivableAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlRevenueAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlUnEarnedInterestAccountNotFoundException;
import com.ejb.exception.ArCCCoaGlEarnedInterestAccountNotFoundException;
import com.ejb.exception.GlobalNameAndAddressAlreadyExistsException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArCustomerEntryController;
import com.ejb.txn.ArCustomerEntryControllerHome;
import com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryForm;
import com.struts.ar.customerentry.ArBranchCustomerList;
import com.struts.ar.customerentry.ArCustomerEntryForm;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchCustomerDetails;
import com.util.ArModCustomerClassDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModCustomerTypeDetails;

public final class ArCustomerEntryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArCustomerEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArCustomerEntryForm actionForm = (ArCustomerEntryForm)form;
         
         String ceParam = null;

         
         if (request.getParameter("child") == null) {
             
        	 ceParam = Common.getUserPermission(user, Constants.AR_CUSTOMER_ENTRY_ID);
          
          } else {
          	
        	  ceParam = Constants.FULL_ACCESS;
          	
          }
         
         
         if (ceParam != null) {

	      if (ceParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arCustomerEntry");
               }

            }

            actionForm.setUserPermission(ceParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArCustomerEntryController EJB
*******************************************************/

         ArCustomerEntryControllerHome homeCE = null;
         ArCustomerEntryController ejbCE = null;

         try {

            homeCE = (ArCustomerEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArCustomerEntryControllerEJB", ArCustomerEntryControllerHome.class);

            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArCustomerEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCE = homeCE.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArCustomerEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
     adPrfArAutoGenerateCustomerCode
     getPrfArNextCustomerCode
*******************************************************/
         
         boolean adPrfArAutoGenerateCustomerCode = false;
         String nextCustomerCode = null;
         
         /*try {
             
             adPrfArAutoGenerateCustomerCode = Common.convertByteToBoolean(
                     						   ejbCE.getPrfArAutoGenerateCustomerCode(user.getCmpCode()));
             actionForm.setAutoGenerateCustomerCode(adPrfArAutoGenerateCustomerCode);
             
             if(adPrfArAutoGenerateCustomerCode && request.getParameter("forward") == null) {
             	
             	nextCustomerCode = ejbCE.getPrfArNextCustomerCode(user.getCmpCode());
             	actionForm.setIsNew(true);
             	
             }
            
         } catch(EJBException ex) {
          	
             if (log.isInfoEnabled()) {
             	
                log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
             }
             
             return(mapping.findForward("cmnErrorPage"));
             
          }*/
         
/*******************************************************
   Call ArCustomerEntryController EJB
   getArCstGlCoaRevenueAccountEnable
*******************************************************/
         
         try {
         	
            actionForm.setEnableRevenueAccount(ejbCE.getArCstGlCoaRevenueAccountEnable(user.getCmpCode()));        
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
         
/*******************************************************
   Call ArCustomerEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/
         
         short precisionUnit = 0;
         
         try {
         	
         	precisionUnit = ejbCE.getGlFcPrecisionUnit(user.getCmpCode());
         	
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
         	
         	
         }

/*******************************************************
   -- Ar CE Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
            (actionForm.getUserPermission().equals(Constants.FULL_ACCESS) || actionForm.getUserPermission().equals(Constants.SPECIAL))) {
            
         	ArModCustomerDetails details = new ArModCustomerDetails();
            details.setCstCode(actionForm.getCstCode());
            
            
            
            details.setCstLastModifiedBy(user.getUserName());
            details.setCstDateLastModified(new java.util.Date());
            
            
			details.setCstCustomerCode(actionForm.getCustomerCode());
			details.setCstName(actionForm.getCustomerName());
			details.setCstDescription(actionForm.getDescription());
			details.setCstPaymentMethod(actionForm.getPaymentMethod());
			details.setCstCreditLimit(Common.convertStringMoneyToDouble(actionForm.getCreditLimit(), precisionUnit));
			details.setCstSquareMeter(Common.convertStringMoneyToDouble(actionForm.getSquareMeter(), precisionUnit));
			details.setCstNumbersParking(Common.convertStringMoneyToDouble(actionForm.getNumbersParking(), precisionUnit));
			details.setCstMonthlyInterestRate(Common.convertStringMoneyToDouble(actionForm.getMonthlyInterestRate(), precisionUnit));
			details.setCstParkingID(actionForm.getParkingID());
			details.setCstRealPropertyTaxRate(Common.convertStringMoneyToDouble(actionForm.getRealPropertyTaxRate(), precisionUnit));
			details.setCstAssociationDuesRate(Common.convertStringMoneyToDouble(actionForm.getAssociationDuesRate(), precisionUnit));
			
			details.setCstWordPressCustomerID(actionForm.getWpCustomerID());
			details.setCstAddress(actionForm.getAddress());
			details.setCstCity(actionForm.getCity());
			details.setCstStateProvince(actionForm.getStateProvince());
			details.setCstPostalCode(actionForm.getPostalCode()); 
			details.setCstCountry(actionForm.getCountry()); 
			details.setCstContact(actionForm.getContact());
			details.setCstEmployeeID(actionForm.getEmployeeID());
			details.setCstAccountNumber(actionForm.getAccountNumber());
			details.setCstPhone(actionForm.getPhone());
			details.setCstFax(actionForm.getFax());
			details.setCstAlternatePhone(actionForm.getAlternatePhone()); 
			details.setCstAlternateContact(actionForm.getAlternateContact());
			details.setCstEmail(actionForm.getEmail());
			details.setCstBillToAddress(actionForm.getBillToAddress());
			details.setCstBillToContact(actionForm.getBillToContact());
			details.setCstBillToAltContact(actionForm.getBillToAltContact());
			details.setCstBillToPhone(actionForm.getBillToPhone());
			details.setCstBillingHeader(actionForm.getBillingHeader());
			details.setCstBillingFooter(actionForm.getBillingFooter());
			details.setCstBillingHeader2(actionForm.getBillingHeader2());
			details.setCstBillingFooter2(actionForm.getBillingFooter2());
			details.setCstBillingHeader3(actionForm.getBillingHeader3());
			details.setCstBillingFooter3(actionForm.getBillingFooter3());
			details.setCstBillingSignatory(actionForm.getBillingSignatory());
			details.setCstSignatoryTitle(actionForm.getSignatoryTitle());
			details.setCstShipToAddress(actionForm.getShipToAddress());
			details.setCstShipToContact(actionForm.getShipToContact());
			details.setCstShipToAltContact(actionForm.getShipToAltContact());
			details.setCstShipToPhone(actionForm.getShipToPhone());
			details.setCstTin(actionForm.getTinNumber());      
			
			details.setCstEnable(Common.convertBooleanToByte(actionForm.getEnable()));
			details.setCstEnableRetailCashier(Common.convertBooleanToByte(actionForm.getEnableRetailCashier()));
			details.setCstEnableRebate(Common.convertBooleanToByte(actionForm.getEnableRebate()));
			details.setCstAutoComputeInterest(Common.convertBooleanToByte(actionForm.getAutoComputeInterest()));
			details.setCstAutoComputePenalty(Common.convertBooleanToByte(actionForm.getAutoComputePenalty())); 
			details.setCstBirthday(Common.convertStringToSQLDate(actionForm.getBirthday()));
			details.setCstDealPrice(actionForm.getDealPrice());
			details.setCstMobilePhone(actionForm.getMobilePhone());
			details.setCstAlternateMobilePhone(actionForm.getAlternateMobilePhone());
			details.setCstArea(actionForm.getArea());
			details.setCstEntryDate(Common.convertStringToSQLDate(actionForm.getEntryDate()));
			details.setCstEffectivityDays(Common.convertStringToShort(actionForm.getEffectivityDays()));
			details.setCstAdLvRegion(actionForm.getRegion());
			details.setCstMemo(actionForm.getMemo());
			details.setCstCustomerBatch(actionForm.getCustomerBatch());
			details.setCstCustomerDepartment(actionForm.getCustomerDepartment());
			details.setCstSlpSalespersonCode2(actionForm.getSalesperson2());
	
			if (actionForm.getCstCode() == null) {
                
                details.setCstCreatedBy(user.getUserName());
                details.setCstDateCreated(new java.util.Date());
                
            }
			
			
			// For Branch Setting
			ArrayList bCstList = new ArrayList();
            
            for (int i = 0; i<actionForm.getBcstListSize(); i++) {
            	
            	ArBranchCustomerList arBcstList = actionForm.getBcstByIndex(i);           	   
            	
            	
            	if (arBcstList.getBranchCheckbox() == true ){ 
            		
            		// checks if branches glcoa values are null
            		
            		if ( Common.validateRequired(arBcstList.getBranchReceivableAccount()) ||
            				Common.validateRequired(arBcstList.getBranchReceivableDescription())) {

            			arBcstList.setBranchReceivableAccount(actionForm.getReceivableAccount());
            			arBcstList.setBranchReceivableDescription(actionForm.getReceivableDescription());

            		}
            		
            		if (Common.validateRequired(arBcstList.getBranchRevenueAccount()) ||
            				Common.validateRequired(arBcstList.getBranchRevenueDescription())) {
            			
            			arBcstList.setBranchRevenueAccount(actionForm.getRevenueAccount());
            			arBcstList.setBranchRevenueDescription(actionForm.getRevenueDescription());
            			
            		}
            		
            		
            		if (Common.validateRequired(arBcstList.getBranchUnEarnedInterestAccount()) ||
            				Common.validateRequired(arBcstList.getBranchUnEarnedInterestDescription())) {
            			
            			arBcstList.setBranchUnEarnedInterestAccount(actionForm.getUnEarnedInterestAccount());
            			arBcstList.setBranchUnEarnedInterestDescription(actionForm.getUnEarnedInterestDescription());
            			
            		}
            		
            		if (Common.validateRequired(arBcstList.getBranchEarnedInterestAccount()) ||
            				Common.validateRequired(arBcstList.getBranchEarnedInterestDescription())) {
            			
            			arBcstList.setBranchEarnedInterestAccount(actionForm.getEarnedInterestAccount());
            			arBcstList.setBranchEarnedInterestDescription(actionForm.getEarnedInterestDescription());
            			
            		}
            		
            		if (Common.validateRequired(arBcstList.getBranchUnEarnedPenaltyAccount()) ||
            				Common.validateRequired(arBcstList.getBranchUnEarnedPenaltyDescription())) {
            			
            			arBcstList.setBranchUnEarnedPenaltyAccount(actionForm.getUnEarnedPenaltyAccount());
            			arBcstList.setBranchUnEarnedPenaltyDescription(actionForm.getUnEarnedPenaltyDescription());
            			
            		}
            		
            		if (Common.validateRequired(arBcstList.getBranchEarnedPenaltyAccount()) ||
            				Common.validateRequired(arBcstList.getBranchEarnedPenaltyDescription())) {
            			
            			arBcstList.setBranchEarnedPenaltyAccount(actionForm.getEarnedPenaltyAccount());
            			arBcstList.setBranchEarnedPenaltyDescription(actionForm.getEarnedPenaltyDescription());
            			
            		}
            		
                	AdModBranchCustomerDetails mdetails = new AdModBranchCustomerDetails();
                	
                	mdetails.setBcstBranchCode(arBcstList.getBranchCode());
                	mdetails.setBcstBranchName(arBcstList.getBranchName());
                	
                	mdetails.setBcstReceivableAccountNumber(arBcstList.getBranchReceivableAccount());
                	mdetails.setBcstReceivableAccountDescription(arBcstList.getBranchReceivableDescription());
                	
                	mdetails.setBcstRevenueAccountNumber(arBcstList.getBranchRevenueAccount());
                	mdetails.setBcstRevenueAccountDescription(arBcstList.getBranchRevenueDescription());
                	
                	mdetails.setBcstUnEarnedInterestAccountNumber(arBcstList.getBranchUnEarnedInterestAccount());
                	mdetails.setBcstUnEarnedInterestAccountDescription(arBcstList.getBranchUnEarnedInterestDescription());
                	
                	mdetails.setBcstEarnedInterestAccountNumber(arBcstList.getBranchEarnedInterestAccount());
                	mdetails.setBcstEarnedInterestAccountDescription(arBcstList.getBranchEarnedInterestDescription());
                	
                	
                	mdetails.setBcstUnEarnedPenaltyAccountNumber(arBcstList.getBranchUnEarnedPenaltyAccount());
                	mdetails.setBcstUnEarnedPenaltyAccountDescription(arBcstList.getBranchUnEarnedPenaltyDescription());
                	
                	mdetails.setBcstEarnedPenaltyAccountNumber(arBcstList.getBranchEarnedPenaltyAccount());
                	mdetails.setBcstEarnedPenaltyAccountDescription(arBcstList.getBranchEarnedPenaltyDescription());
                	
                	bCstList.add(mdetails);

            	}
            	
            	details.setBcstList(bCstList);
            	
            }

            try {

            	if (actionForm.getCustomerType().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            		actionForm.setCustomerType(null);
            	}
            	
            	// validate email
    			boolean adPrfValidateCustomerEmail = false;	        
    			
    			try {
    				
    				adPrfValidateCustomerEmail = Common.convertByteToBoolean(
    						ejbCE.getPrfValidateCustomerEmail(user.getCmpCode()));
    				
    			} catch(EJBException ex) {
    				
    				if (log.isInfoEnabled()) {
    					
    					log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
    							" session: " + session.getId());
    				}
    				
    				return(mapping.findForward("cmnErrorPage"));
    				
    			}
    			
    			boolean validEmail = true;
    			if(adPrfValidateCustomerEmail)
    			{
    				String email = details.getCstEmail();
    				
    				int index = email.indexOf("@");
    				
    				if(index<1)
    					validEmail = false;
    				
    				email = email.substring(index + 1);
    				index = email.indexOf(".");
    				    				
    				if(index<1)
    					validEmail = false;
    				
    				email = email.substring(index + 1);    							
    				
    				if(email.equals(""))
    					validEmail = false;
    				
    				
    				if(!validEmail){    					
    					errors.add(ActionMessages.GLOBAL_MESSAGE,
    			                  new ActionMessage("customerEntry.error.emailRequired"));    					
    				}    					
    			}
    			System.out.println("---------Action Form--------------" + details.getCstWordPressCustomerID());
    			if(validEmail){
    				
    				Integer cstCode = ejbCE.saveArCstEntry(details, actionForm.getCustomerType(), 
    						actionForm.getPaymentTerm(), actionForm.getSupplier() ,actionForm.getCustomerClass(),
    						actionForm.getReceivableAccount(), actionForm.getRevenueAccount(),
    						actionForm.getUnEarnedInterestAccount(),actionForm.getEarnedInterestAccount(),
    						actionForm.getUnEarnedPenaltyAccount(),actionForm.getEarnedPenaltyAccount(),
    						actionForm.getBankAccount(), new Integer(user.getCurrentResCode()), actionForm.getSalesperson(), 
    						actionForm.getSalesperson2(), actionForm.getTemplateName(),
    						actionForm.getDeployedBranchName(),
    						actionForm.getEmployeeNumber(),
    						null,
    						true,
    						new Integer(user.getCurrentBranch().getBrCode()),
    						user.getCmpCode());
    				
    				actionForm.setCstCode(cstCode);
    			}
                
            } catch (GlobalRecordAlreadyExistException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("customerEntry.error.customerCodeAlreadyExist"));
               
            } catch (GlobalNoApprovalApproverFoundException ex) {
           		
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("customerEntry.error.noApprovalApproverFound"));
           		
           	} catch (GlobalNoApprovalRequesterFoundException ex) {
           		
           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           				new ActionMessage("customerEntry.error.noApprovalRequesterFound"));
               
            } catch (GlobalNameAndAddressAlreadyExistsException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("customerEntry.error.customerNameAndAddressAlreadyExist"));
               
            } catch (ArCCCoaGlReceivableAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("customerEntry.error.receivableAccountNotFound"));
                

                
            } catch (ArCCCoaGlRevenueAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("customerEntry.error.revenueAccountNotFound"));
                
                
                
            } catch (ArCCCoaGlUnEarnedInterestAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("customerEntry.error.unEarnedInterestAccountNotFound"));
                
            } catch (ArCCCoaGlEarnedInterestAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("customerEntry.error.earnedInterestAccountNotFound"));
                  
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            /*if(adPrfArAutoGenerateCustomerCode) {
             	
            	nextCustomerCode = ejbCE.getPrfArNextCustomerCode(user.getCmpCode());
             	actionForm.setIsNew(true);
             	
            }*/

/*******************************************************
   -- Ar CST Customer Class Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isCustomerClassEntered"))) {
         	
         	try {
             	
                 ArModCustomerClassDetails mdetails = ejbCE.getArCcByCcName(actionForm.getCustomerClass(), user.getCmpCode());
                                  
                 actionForm.setReceivableAccount(mdetails.getCcGlCoaReceivableAccountNumber());
                 actionForm.setReceivableDescription(mdetails.getCcGlCoaReceivableAccountDescription());
                 actionForm.setRevenueAccount(mdetails.getCcGlCoaRevenueAccountNumber());
                 actionForm.setRevenueDescription(mdetails.getCcGlCoaRevenueAccountDescription());
                 
                 actionForm.setUnEarnedInterestAccount(mdetails.getCcGlCoaUnEarnedInterestAccountNumber());
				    actionForm.setUnEarnedInterestDescription(mdetails.getCcGlCoaUnEarnedInterestAccountDescription());
				    
				    actionForm.setEarnedInterestAccount(mdetails.getCcGlCoaEarnedInterestAccountNumber());
				    actionForm.setEarnedInterestDescription(mdetails.getCcGlCoaEarnedInterestAccountDescription());
				 
				    actionForm.setUnEarnedPenaltyAccount(mdetails.getCcGlCoaUnEarnedPenaltyAccountNumber());
				    actionForm.setUnEarnedPenaltyDescription(mdetails.getCcGlCoaUnEarnedPenaltyAccountDescription());
				    
				    actionForm.setEarnedPenaltyAccount(mdetails.getCcGlCoaEarnedPenaltyAccountNumber());
				    actionForm.setEarnedPenaltyDescription(mdetails.getCcGlCoaEarnedPenaltyAccountDescription());
				 
				    
				    
			         try {
			             
			             adPrfArAutoGenerateCustomerCode = Common.convertByteToBoolean(
			                     						   ejbCE.getPrfArAutoGenerateCustomerCode(user.getCmpCode()));
			             actionForm.setAutoGenerateCustomerCode(adPrfArAutoGenerateCustomerCode);
			             
			             if(adPrfArAutoGenerateCustomerCode && request.getParameter("forward") == null) {
			             	
			            	 nextCustomerCode = mdetails.getCcNextCustomerCode();
			             	actionForm.setNextCustomerCode(nextCustomerCode);
			             	actionForm.setIsNew(true);
			             	
			             }
			            
			         } catch (Exception ex){
			        	 
			         }
			         
			         
				    actionForm.setCustomerBatch(mdetails.getCcCustomerBatch());
				    actionForm.setDealPrice(mdetails.getCcDealPrice());
				    actionForm.setMonthlyInterestRate(Common.convertDoubleToStringMoney(mdetails.getCcMonthlyInterestRate(), precisionUnit));
				    actionForm.setCreditLimit(Common.convertDoubleToStringMoney(mdetails.getCcCreditLimit(), precisionUnit));
                 	actionForm.setEnableRebate(Common.convertByteToBoolean(mdetails.getCcEnableRebate()));
					actionForm.setAutoComputeInterest(Common.convertByteToBoolean(mdetails.getCcAutoComputeInterest()));
					actionForm.setAutoComputePenalty(Common.convertByteToBoolean(mdetails.getCcAutoComputePenalty()));
					
                 
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
             
            return(mapping.findForward("arCustomerEntry"));
            
/*******************************************************
   -- Ar CST Customer Type Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isCustomerTypeEntered"))) {
         	
         	try {
             	  
         		 if (!actionForm.getCustomerType().equals(Constants.GLOBAL_NO_RECORD_FOUND)) { 
         		 	
         		 	ArModCustomerTypeDetails mdetails = ejbCE.getArCtByCtName(actionForm.getCustomerType(), user.getCmpCode());                                 
         		 	actionForm.setBankAccount(mdetails.getCtBaName());
         		 	
         		 }
                              	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  	
                        
            return(mapping.findForward("arCustomerEntry"));

/*******************************************************
   -- Ar CE Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
        

/*******************************************************
   -- Ar CE Load Action --
*******************************************************/

         }
         
         if (ceParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arCustomerEntry");

            }
            
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            actionForm.reset(mapping, request);
            

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	
            	actionForm.clearCustomerTypeList();
            	
            	list = ejbCE.getArCtAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCustomerTypeList((String)i.next());
            			
            		}
            		
            	}            	
            	
            	actionForm.clearPaymentTermList();
            	
            	list = ejbCE.getAdPytAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setPaymentTermList((String)i.next());
            			
            		}
            		
            		actionForm.setPaymentTerm("IMMEDIATE");
            		
            	}
            	
            	
            	if(actionForm.getUseSupplierPulldown()) {

					actionForm.clearSupplierList();           	

					list = ejbCE.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							actionForm.setSupplierList((String)i.next());

						}

					} 

				}
				
				
				
            	

            	actionForm.clearCustomerClassList();
            	
            	list = ejbCE.getArCcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setArea("");
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCustomerClassList((String)i.next());
            			
            		}
            		
            	} 
            	
            	actionForm.clearCustomerBatchList();           	
         		
         		list = ejbCE.getAdLvCustomerBatchAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setCustomerBatchList((String)i.next());
         				
         			}
         			
         		}
         		
         		
         		actionForm.clearCustomerDepartmentList();           	
         		
         		list = ejbCE.getAdLvCustomerDepartmentAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setCustomerDepartmentList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setCustomerDepartmentList((String)i.next());
         				
         			}
         			
         		}
            	
            	actionForm.clearBankAccountList();
            	
            	list = ejbCE.getAdBaAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	} 

            	actionForm.clearSalespersonList();    
            	
        		list = ejbCE.getArSlpAll(user.getCmpCode());
        		
        		if (list == null || list.size() == 0) {
        			
        			actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
        			
        		} else {
        			
        			i = list.iterator();
        			
        			while (i.hasNext()) {
        				
        				actionForm.setSalespersonList((String)i.next());
        				
        			}
        			
        		} 
        		
            	actionForm.clearSalesperson2List();    
            	
        		list = ejbCE.getArSlpAll(user.getCmpCode());
        		
        		if (list == null || list.size() == 0) {
        			
        			actionForm.setSalesperson2List(Constants.GLOBAL_NO_RECORD_FOUND);
        			
        		} else {
        			
        			i = list.iterator();
        			
        			while (i.hasNext()) {
        				
        				actionForm.setSalesperson2List((String)i.next());
        				
        			}
        			
        		} 
        		
        		
        		actionForm.clearDealPriceList();    
            	
        		list = ejbCE.getAdLvInvPriceLevelAll(user.getCmpCode());
        		
        		if (list == null || list.size() == 0) {
        			
        			actionForm.setDealPriceList(Constants.GLOBAL_NO_RECORD_FOUND);
        			
        		} else {
        			
        			i = list.iterator();
        			
        			while (i.hasNext()) {
        				
        				actionForm.setDealPriceList((String)i.next());
        				
        			}
        			
        		} 
        		
        		actionForm.clearRegionList();           	
            	
            	list = ejbCE.getAdLvArRegionAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setRegionList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {

            		    actionForm.setRegionList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	
            	
            	actionForm.clearDeployedBranchNameList();           	
                
                list = ejbCE.getHrOpenDbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                
                if (list == null || list.size() == 0) {
                    
                    actionForm.setDeployedBranchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
                    
                } else {
                    
                    i = list.iterator();
                    
                    while (i.hasNext()) {
                        
                        actionForm.setDeployedBranchNameList((String)i.next());
                        
                    }
                    
                }
                
                
                actionForm.clearEmployeeNumberList();           	
                
                list = ejbCE.getHrOpenEmpAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                
                if (list == null || list.size() == 0) {
                    
                    actionForm.setEmployeeNumberList(Constants.GLOBAL_NO_RECORD_FOUND);
                    
                } else {
                    
                    i = list.iterator();
                    
                    while (i.hasNext()) {
                        
                        actionForm.setEmployeeNumberList((String)i.next());
                        
                    }
                    
                }

            	// branch fields
            	
				list = null;
				i = null;
			
				actionForm.clearBcstList();	        
				
				list = ejbCE.getAdBrResAll(user.getCurrentResCode(), user.getCmpCode()); 
				
				i = list.iterator();
				
				while(i.hasNext()) {
					
					AdBranchDetails details = (AdBranchDetails)i.next();

					ArBranchCustomerList arBcstList = new ArBranchCustomerList(actionForm,
							details.getBrBranchCode(), details.getBrName());
					
					if ( request.getParameter("forward") == null )
						arBcstList.setBranchCheckbox(true);
					
					actionForm.saveBcstList(arBcstList);
					
				}
				
				actionForm.clearTemplateList();
            	
            	list = ejbCE.getInvLitAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setTemplateList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setTemplateList((String)i.next());
            			
            		}
            		
            	} 
            	
            	if (request.getParameter("forward") != null) {
            		
            		if (request.getParameter("forward") != null) {
            			
            			actionForm.setCstCode(new Integer(request.getParameter("cstCode")));
            			
            		}
            		
            		ArModCustomerDetails cstDetails = ejbCE.getArCstByCstCode(
            				actionForm.getCstCode(), new Integer(user.getCurrentResCode()),
						user.getCmpCode());
            		
					
					actionForm.setCustomerCode(cstDetails.getCstCustomerCode());
					actionForm.setCustomerName(cstDetails.getCstName());
					actionForm.setDescription(cstDetails.getCstDescription());
					actionForm.setPaymentMethod(cstDetails.getCstPaymentMethod());
					actionForm.setCreditLimit(Common.convertDoubleToStringMoney(cstDetails.getCstCreditLimit(), precisionUnit));
					actionForm.setSquareMeter(Common.convertDoubleToStringMoney(cstDetails.getCstSquareMeter(), precisionUnit));
					actionForm.setNumbersParking(Common.convertDoubleToStringMoney(cstDetails.getCstNumbersParking(), precisionUnit));
					actionForm.setMonthlyInterestRate(Common.convertDoubleToStringMoney(cstDetails.getCstMonthlyInterestRate(), precisionUnit));
					actionForm.setParkingID(cstDetails.getCstParkingID());
					actionForm.setWpCustomerID(cstDetails.getCstWordPressCustomerID());
					actionForm.setRealPropertyTaxRate(Common.convertDoubleToStringMoney(cstDetails.getCstRealPropertyTaxRate(), precisionUnit));
					actionForm.setAssociationDuesRate(Common.convertDoubleToStringMoney(cstDetails.getCstAssociationDuesRate(), precisionUnit));
					
					actionForm.setAddress(cstDetails.getCstAddress());
					actionForm.setCity(cstDetails.getCstCity());
					actionForm.setStateProvince(cstDetails.getCstStateProvince());
					actionForm.setPostalCode(cstDetails.getCstPostalCode()); 
					actionForm.setCountry(cstDetails.getCstCountry()); 
					actionForm.setContact(cstDetails.getCstContact());
					actionForm.setEmployeeID(cstDetails.getCstEmployeeID());
					actionForm.setAccountNumber(cstDetails.getCstAccountNumber());
					actionForm.setPhone(cstDetails.getCstPhone());
					actionForm.setFax(cstDetails.getCstFax());
					actionForm.setAlternatePhone(cstDetails.getCstAlternatePhone()); 
					actionForm.setAlternateContact(cstDetails.getCstAlternateContact());
					actionForm.setEmail(cstDetails.getCstEmail());
					actionForm.setBillToAddress(cstDetails.getCstBillToAddress());
					actionForm.setBillToContact(cstDetails.getCstBillToContact());
					actionForm.setBillToAltContact(cstDetails.getCstBillToAltContact());
					actionForm.setBillToPhone(cstDetails.getCstBillToPhone());
					actionForm.setBillingHeader(cstDetails.getCstBillingHeader());
					actionForm.setBillingFooter(cstDetails.getCstBillingFooter());
					actionForm.setBillingHeader2(cstDetails.getCstBillingHeader2());
					actionForm.setBillingFooter2(cstDetails.getCstBillingFooter2());
					actionForm.setBillingHeader3(cstDetails.getCstBillingHeader3());
					actionForm.setBillingFooter3(cstDetails.getCstBillingFooter3());
					actionForm.setBillingSignatory(cstDetails.getCstBillingSignatory());
					actionForm.setSignatoryTitle(cstDetails.getCstSignatoryTitle());
					actionForm.setShipToAddress(cstDetails.getCstShipToAddress());
					actionForm.setShipToContact(cstDetails.getCstShipToContact());
					actionForm.setShipToAltContact(cstDetails.getCstShipToAltContact());
					actionForm.setShipToPhone(cstDetails.getCstShipToPhone());
					actionForm.setTinNumber(cstDetails.getCstTin());
					actionForm.setEnable(Common.convertByteToBoolean(cstDetails.getCstEnable()));
					actionForm.setEnableRetailCashier(Common.convertByteToBoolean(cstDetails.getCstEnableRetailCashier()));
					actionForm.setEnableRebate(Common.convertByteToBoolean(cstDetails.getCstEnableRebate()));
					
					actionForm.setAutoComputeInterest(Common.convertByteToBoolean(cstDetails.getCstAutoComputeInterest()));
					actionForm.setAutoComputePenalty(Common.convertByteToBoolean(cstDetails.getCstAutoComputePenalty()));
					actionForm.setBirthday(Common.convertSQLDateToString(cstDetails.getCstBirthday()));
					actionForm.setDealPrice(cstDetails.getCstDealPrice());
					actionForm.setMobilePhone(cstDetails.getCstMobilePhone());
					actionForm.setAlternateMobilePhone(cstDetails.getCstAlternateMobilePhone());
					actionForm.setArea(cstDetails.getCstArea());
					actionForm.setEntryDate(Common.convertSQLDateToString(cstDetails.getCstEntryDate()));
					actionForm.setEffectivityDays(Common.convertShortToString(cstDetails.getCstEffectivityDays()));
					actionForm.setRegion(cstDetails.getCstAdLvRegion());
					actionForm.setMemo(cstDetails.getCstMemo());
					actionForm.setCustomerBatch(cstDetails.getCstCustomerBatch());
					actionForm.setCustomerDepartment(cstDetails.getCstCustomerDepartment());
					
					actionForm.setApprovalStatus(cstDetails.getCstApprovalStatus());
                    actionForm.setReasonForRejection(cstDetails.getCstReasonForRejection());
                    actionForm.setPosted(cstDetails.getCstPosted() == 1 ? "YES" : "NO");
                    actionForm.setCreatedBy(cstDetails.getCstCreatedBy());
                    actionForm.setDateCreated(Common.convertSQLDateToString(cstDetails.getCstDateCreated()));
                    actionForm.setLastModifiedBy(cstDetails.getCstLastModifiedBy());
                    actionForm.setDateLastModified(Common.convertSQLDateToString(cstDetails.getCstDateLastModified()));            		
                    actionForm.setApprovedRejectedBy(cstDetails.getCstApprovedRejectedBy());
                    actionForm.setDateApprovedRejected(Common.convertSQLDateToString(cstDetails.getCstDateApprovedRejected()));
                    actionForm.setPostedBy(cstDetails.getCstPostedBy());
                    actionForm.setDatePosted(Common.convertSQLDateToString(cstDetails.getCstDatePosted()));
                    
				    
					if (!actionForm.getCustomerTypeList().contains(cstDetails.getCstCtName()) 
						&& cstDetails.getCstCtName() != null) {
						
						if (actionForm.getCustomerTypeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
							
							actionForm.clearCustomerTypeList();
							
						}						
						actionForm.setCustomerTypeList(cstDetails.getCstCtName());	
						
					}					
					actionForm.setCustomerType(cstDetails.getCstCtName());
					
					

					if (!actionForm.getSupplierList().contains(cstDetails.getCstSupplierCode())
								&& cstDetails.getCstSupplierCode() != null) {
							
							if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
								
								actionForm.clearSupplierList();
								
							}				    
							actionForm.setSupplierList(cstDetails.getCstSupplierCode());
						
						}				    
						actionForm.setSupplier(cstDetails.getCstSupplierCode());
					
					
					
					
					if (!actionForm.getPaymentTermList().contains(cstDetails.getCstPytName())
						&& cstDetails.getCstPytName() != null) {
						
						if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
							
							actionForm.clearPaymentTermList();
							
						}						
						actionForm.setPaymentTermList(cstDetails.getCstPytName());
						
					}					
				    actionForm.setPaymentTerm(cstDetails.getCstPytName());
				    
				    if (!actionForm.getCustomerClassList().contains(cstDetails.getCstCcName()) 
				    	&& cstDetails.getCstCcName() != null) {
				    	
				    	if (actionForm.getCustomerClassList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
				    		
				    		actionForm.clearCustomerClassList();
				    		
				    	}				    	
				    	actionForm.setCustomerClassList(cstDetails.getCstCcName());
				    	
				    }				    
				    actionForm.setCustomerClass(cstDetails.getCstCcName());
				    
				    if (!actionForm.getBankAccountList().contains(cstDetails.getCstBaName())) {
				    	
				    	if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
   							
   							actionForm.clearBankAccountList();
   							
   						}	   
				    	actionForm.setBankAccountList(cstDetails.getCstBaName());
				    	
				    }				    
				    actionForm.setBankAccount(cstDetails.getCstBaName());
				    
            		if (!actionForm.getSalespersonList().contains(cstDetails.getCstSlpSalespersonCode())) {
            			
            			if (actionForm.getSalespersonList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearSalespersonList();
            				
            			}
            			actionForm.setSalespersonList(cstDetails.getCstSlpSalespersonCode());
            			
            		}
            		
            		
            		if (!actionForm.getDeployedBranchNameList().contains(cstDetails.getCstHrDeployedBranchName())) {
                        
                        actionForm.setDeployedBranchNameList(cstDetails.getCstHrDeployedBranchName());
                        
                    }                 		
                    actionForm.setDeployedBranchName(cstDetails.getCstHrDeployedBranchName());
                    
                    
                    if (!actionForm.getEmployeeNumberList().contains(cstDetails.getCstHrEmployeeNumber())) {
                        
                        actionForm.setEmployeeNumberList(cstDetails.getCstHrEmployeeNumber());
                        
                    }                 		
                    actionForm.setEmployeeNumber(cstDetails.getCstHrEmployeeNumber());
                    actionForm.setBioNumber(cstDetails.getCstHrBioNumber());
                    actionForm.setManagingBranch(cstDetails.getCstHrManagingBranch());
                    actionForm.setCurrentJobPosition(cstDetails.getCstHrCurrentJobPosition());
                    
                    
                    
            		actionForm.setSalesperson(cstDetails.getCstSlpSalespersonCode());
            		actionForm.setSalesperson2(cstDetails.getCstSlpSalespersonCode2());
            		
            		System.out.println("action : " + cstDetails.getCstSlpSalespersonCode2());
				    
				    actionForm.setReceivableAccount(cstDetails.getCstGlCoaReceivableAccountNumber());
				    actionForm.setReceivableDescription(cstDetails.getCstGlCoaReceivableAccountDescription());
				    actionForm.setRevenueAccount(cstDetails.getCstGlCoaRevenueAccountNumber());
				    actionForm.setRevenueDescription(cstDetails.getCstGlCoaRevenueAccountDescription());
				    
				    actionForm.setUnEarnedInterestAccount(cstDetails.getCstGlCoaUnEarnedInterestAccountNumber());
				    actionForm.setUnEarnedInterestDescription(cstDetails.getCstGlCoaUnEarnedInterestAccountDescription());
				    
				    actionForm.setEarnedInterestAccount(cstDetails.getCstGlCoaEarnedInterestAccountNumber());
				    actionForm.setEarnedInterestDescription(cstDetails.getCstGlCoaEarnedInterestAccountDescription());
				    
				    
				    actionForm.setUnEarnedPenaltyAccount(cstDetails.getCstGlCoaUnEarnedPenaltyAccountNumber());
				    actionForm.setUnEarnedPenaltyDescription(cstDetails.getCstGlCoaUnEarnedPenaltyAccountDescription());
				    
				    actionForm.setEarnedPenaltyAccount(cstDetails.getCstGlCoaEarnedPenaltyAccountNumber());
				    actionForm.setEarnedPenaltyDescription(cstDetails.getCstGlCoaEarnedPenaltyAccountDescription());
				    
				    
				    actionForm.setTemplateName(cstDetails.getCstLitName());
				    
	            	// branch fields
				
					ArrayList branchCustomers = new ArrayList();
				    
				    for(int j=0; j < actionForm.getBcstListSize(); j++) {
						
						ArBranchCustomerList arBcstList = (ArBranchCustomerList)actionForm.getBcstByIndex(j);
						
						ArrayList mBcstDetails = cstDetails.getBcstList();
						Iterator k = mBcstDetails.iterator();
						
						while (k.hasNext()) {
						
							AdModBranchCustomerDetails mBcstDetail = (AdModBranchCustomerDetails) k.next();

							if(arBcstList.getBranchName().equals(mBcstDetail.getBcstBranchName())) {
								
								arBcstList.setBranchCode(mBcstDetail.getBcstBranchCode());
								arBcstList.setBranchName(mBcstDetail.getBcstBranchName());
								arBcstList.setBranchCheckbox(true);
								arBcstList.setBranchReceivableAccount(mBcstDetail.getBcstReceivableAccountNumber());
								arBcstList.setBranchReceivableDescription(mBcstDetail.getBcstReceivableAccountDescription());
								
								arBcstList.setBranchRevenueAccount(mBcstDetail.getBcstRevenueAccountNumber());
								arBcstList.setBranchRevenueDescription(mBcstDetail.getBcstRevenueAccountDescription());
								
								arBcstList.setBranchUnEarnedInterestAccount(mBcstDetail.getBcstUnEarnedInterestAccountNumber());
								arBcstList.setBranchUnEarnedInterestDescription(mBcstDetail.getBcstUnEarnedInterestAccountDescription());
								
								arBcstList.setBranchEarnedInterestAccount(mBcstDetail.getBcstEarnedInterestAccountNumber());
								arBcstList.setBranchEarnedInterestDescription(mBcstDetail.getBcstEarnedInterestAccountDescription());
								
								arBcstList.setBranchUnEarnedPenaltyAccount(mBcstDetail.getBcstUnEarnedPenaltyAccountNumber());
								arBcstList.setBranchUnEarnedPenaltyDescription(mBcstDetail.getBcstUnEarnedPenaltyAccountDescription());
								
								arBcstList.setBranchEarnedPenaltyAccount(mBcstDetail.getBcstEarnedPenaltyAccountNumber());
								arBcstList.setBranchEarnedPenaltyDescription(mBcstDetail.getBcstEarnedPenaltyAccountDescription());
								
								break;
								
							}
							
						}
						
						branchCustomers.add(arBcstList);
						
					}
					
					actionForm.setBcstList(branchCustomers);
					actionForm.setIsNew(false);
					
					this.setFormProperties(actionForm, user.getCompany());
				    
				
				    if (request.getParameter("child") == null) {
                        
                        return (mapping.findForward("arCustomerEntry"));         
                        
                    } else {
                        
                        return (mapping.findForward("arCustomerEntryChild"));         
                        
                    }
				    
			    }
            	
            	
	            
            } catch (GlobalNoRecordFoundException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("customerEntry.error.noRecordFound")); 
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }           
            

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if ((request.getParameter("saveButton") != null &&
                   (actionForm.getUserPermission().equals(Constants.FULL_ACCESS) || actionForm.getUserPermission().equals(Constants.SPECIAL)))) {


            	   System.out.println("actionForm.getCstCode()="+actionForm.getCstCode());
           		
           		try {
           			ArrayList listUser = ejbCE.getAdApprovalNotifiedUsersByCstCode(actionForm.getCstCode(), user.getCmpCode());
           			if (listUser.isEmpty()) {
           				
           				messages.add(ActionMessages.GLOBAL_MESSAGE,	
           						new ActionMessage("messages.documentPosted"));
           			} else {
           				Iterator x = listUser.iterator();
           				String APPROVAL_USERS = "";
           				while (x.hasNext()) {
           					APPROVAL_USERS = APPROVAL_USERS + (String)x.next();
           					if (x.hasNext()) {	
           						APPROVAL_USERS = APPROVAL_USERS + ", ";	
           					}
           				}
           				messages.add(ActionMessages.GLOBAL_MESSAGE,
           						new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
           			}	
           		
           		} catch(EJBException ex) {
           			
           		}
           		
           		saveMessages(request, messages);
           		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
           		
           		

               }
            	
            	
            	
            }
            
            actionForm.reset(mapping, request);
            
            actionForm.setCstCode(null);
        	actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setMonthlyInterestRate(Common.convertDoubleToStringMoney(0d, precisionUnit));
            
            actionForm.setSquareMeter(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setNumbersParking(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setRealPropertyTaxRate(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAssociationDuesRate(Common.convertDoubleToStringMoney(0d, precisionUnit)); 
            actionForm.setNextCustomerCode(nextCustomerCode);
            actionForm.setEnable(true);
            actionForm.setEnableRetailCashier(false);
            actionForm.setEnableRebate(false);
            actionForm.setAutoComputeInterest(false);
            actionForm.setAutoComputePenalty(false);
            
            actionForm.setEntryDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setEffectivityDays("0");
            
            this.setFormProperties(actionForm, user.getCompany());
            
            if (request.getParameter("child") == null) {
				
				return (mapping.findForward("arCustomerEntry"));          
				
			} else {
				
				return (mapping.findForward("arCustomerEntryChild"));         
				
			}
          

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(ArithmeticException e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArCustomerEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

      }
   }
   
   
   private void setFormProperties(ArCustomerEntryForm actionForm, String adCompany) {
	   if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	   		
		   	if(actionForm.getApprovalStatus() != null && actionForm.getApprovalStatus().equals("PENDING")){
		   		System.out.println("PENDING");
	   			actionForm.setShowSaveButton(false);
	   		} else {
	   			System.out.println("FALSE");
	   			actionForm.setShowSaveButton(true);
	   		}
   }
}
   
}