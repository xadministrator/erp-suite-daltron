package com.struts.ar.customerentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArCustomerEntryForm extends ActionForm implements Serializable {

    private Integer cstCode = null;
    private String customerCode = null;
    private String customerName = null;
    private String description = null;
    private String creditLimit = null;
    private String customerType = null;
    private ArrayList customerTypeList = new ArrayList();
    private String customerClass = null;
    private ArrayList customerClassList = new ArrayList();

    private String supplier = null;
	private ArrayList supplierList = new ArrayList();
	private boolean useSupplierPulldown = true;


    private String paymentTerm = null;
    private ArrayList paymentTermList = new ArrayList();
    private String paymentMethod = null;
    private ArrayList paymentMethodList = new ArrayList();


    //HRIS
    private String deployedBranchName = null;
	private ArrayList deployedBranchNameList = new ArrayList();

	private String employeeNumber = null;
	private ArrayList employeeNumberList = new ArrayList();

	private String bioNumber = null;
	private String managingBranch = null;
	private String currentJobPosition = null;



    private String address = null;
    private String city = null;
    private String stateProvince = null;
    private String postalCode = null;
    private String country = null;
    private String contact = null;
    private String employeeID = null;
    private String accountNumber = null;
    private String phone = null;
    private String fax = null;
    private String alternatePhone = null;
    private String alternateContact = null;
    private String email = null;
    private String billToAddress = null;
    private String billToContact = null;
    private String billToAltContact = null;
    private String billToPhone = null;
    private String billingHeader = null;
    private String billingFooter = null;
    private String billingHeader2 = null;
    private String billingFooter2 = null;
    private String billingHeader3 = null;
    private String billingFooter3 = null;
    private String billingSignatory = null;
    private String signatoryTitle = null;
    private String shipToAddress = null;
    private String shipToContact = null;
    private String shipToAltContact = null;
    private String shipToPhone = null;
    private String tinNumber = null;
    private String bankAccount = null;
    private ArrayList bankAccountList = new ArrayList();
    private String receivableAccount = null;
    private String receivableDescription = null;
    private String revenueAccount = null;
    private String revenueDescription = null;

    private String unEarnedInterestAccount = null;
    private String unEarnedInterestDescription = null;
    private String earnedInterestAccount = null;
    private String earnedInterestDescription = null;

    private String unEarnedPenaltyAccount = null;
    private String unEarnedPenaltyDescription = null;
    private String earnedPenaltyAccount = null;
    private String earnedPenaltyDescription = null;

    private boolean enable = false;
    private boolean enableRetailCashier = false;
    private boolean enableRebate = false;
    private boolean enablePayroll = false;
    private boolean autoComputeInterest = false;
    private boolean autoComputePenalty = false;
    private String nextCustomerCode = null;
    private boolean isNew = false;

    private String userPermission = new String();
    private String txnStatus = new String();

    private String isCustomerClassEntered = null;
    private String isCustomerTypeEntered = null;
    private boolean enableRevenueAccount = false;
    private boolean autoGenerateCustomerCode = false;

    private ArrayList arBcstList = new ArrayList();

    private String birthday = null;

    private String mobilePhone = null;
    private String alternateMobilePhone = null;
    private String salesperson = null;
    private String salesperson2 = null;
    private ArrayList salespersonList = new ArrayList();
    private ArrayList salesperson2List = new ArrayList();
    private String templateName = null;
    private ArrayList templateList = new ArrayList();
    private String area = null;
    private String squareMeter;
    private String numbersParking;
    private String monthlyInterestRate;
    private String parkingID;
    private String associationDuesRate;
    private String realPropertyTaxRate;
    private String wpCustomerID;
    private String entryDate = null;
    private String effectivityDays = null;
    private String region = null;
    private ArrayList regionList = new ArrayList();
    private String memo = null;

    private String approvalStatus = null;
	private String reasonForRejection = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private boolean showSaveButton = false;

    private String customerBatch = null;
    private ArrayList customerBatchList = new ArrayList();

    private String customerDepartment = null;
    private ArrayList customerDepartmentList = new ArrayList();

    private String dealPrice = null;
    private ArrayList dealPriceList = new ArrayList();



    public boolean getShowSaveButton() {

		return showSaveButton;

	}

	public void setShowSaveButton(boolean showSaveButton) {

		this.showSaveButton = showSaveButton;

	}

    public String getTxnStatus() {

        String passTxnStatus = txnStatus;
        txnStatus = Constants.GLOBAL_BLANK;
        return passTxnStatus;
    }

    public void setTxnStatus(String txnStatus) {

        this.txnStatus = txnStatus;

    }

    public String getUserPermission() {

        return userPermission;

    }

    public void setUserPermission(String userPermission) {

        this.userPermission = userPermission;

    }

    public Integer getCstCode() {

        return cstCode;

    }

    public void setCstCode(Integer cstCode) {

        this.cstCode = cstCode;

    }

    public String getCustomerCode() {

        return customerCode;

    }

    public void setCustomerCode(String customerCode) {

        this.customerCode = customerCode;

    }

    public String getCustomerName() {

        return customerName;

    }

    public void setCustomerName(String customerName) {

        this.customerName = customerName;

    }

    public String getDescription() {

        return description;

    }

    public void setDescription(String description) {

        this.description = description;

    }

    public String getCreditLimit() {

        return creditLimit;

    }

    public void setCreditLimit(String creditLimit) {

        this.creditLimit = creditLimit;

    }

    public String getCustomerType() {

        return customerType;

    }

    public void setCustomerType(String customerType) {

        this.customerType = customerType;

    }

    public ArrayList getCustomerTypeList() {

        return customerTypeList;

    }

    public void setCustomerTypeList(String customerType) {

        customerTypeList.add(customerType);

    }

    public void clearCustomerTypeList() {

        customerTypeList.clear();
        customerTypeList.add(Constants.GLOBAL_BLANK);

    }

    public String getCustomerClass() {

        return customerClass;

    }

    public void setCustomerClass(String customerClass) {

        this.customerClass = customerClass;

    }

    public ArrayList getCustomerClassList() {

        return customerClassList;

    }

    public void setCustomerClassList(String customerClass) {

        customerClassList.add(customerClass);

    }

    public void clearCustomerClassList() {

        customerClassList.clear();
        customerClassList.add(Constants.GLOBAL_BLANK);

    }








    public String getSupplier() {

            return supplier;

     }

     public void setSupplier(String supplier) {

            this.supplier = supplier;

     }

     public ArrayList getSupplierList() {

            return supplierList;

     }

     public void setSupplierList(String supplier) {

            supplierList.add(supplier);

     }

     public void clearSupplierList() {

            supplierList.clear();
            supplierList.add(Constants.GLOBAL_BLANK);

     }


  public boolean getUseSupplierPulldown() {

            return useSupplierPulldown;

     }

     public void setUseSupplierPulldown(boolean useSupplierPulldown) {

            this.useSupplierPulldown = useSupplierPulldown;

     }

    public String getPaymentTerm() {

        return paymentTerm;

    }

    public void setPaymentTerm(String paymentTerm) {

        this.paymentTerm = paymentTerm;

    }

    public ArrayList getPaymentTermList() {

        return paymentTermList;

    }

    public void setPaymentTermList(String paymentTerm) {

        paymentTermList.add(paymentTerm);

    }

    public void clearPaymentTermList() {

        paymentTermList.clear();
        paymentTermList.add(Constants.GLOBAL_BLANK);

    }

    public String getPaymentMethod() {

        return paymentMethod;

    }

    public void setPaymentMethod(String paymentMethod) {

        this.paymentMethod = paymentMethod;

    }

    public ArrayList getPaymentMethodList() {

        return paymentMethodList;

    }


public String getDeployedBranchName() {

		return deployedBranchName;

	}

	public void setDeployedBranchName(String deployedBranchName) {

		this.deployedBranchName = deployedBranchName;

	}

	public ArrayList getDeployedBranchNameList() {

		return deployedBranchNameList;

	}

	public void setDeployedBranchNameList(String deployedBranchName) {

		deployedBranchNameList.add(deployedBranchName);

	}

	public void clearDeployedBranchNameList() {

		deployedBranchNameList.clear();
		deployedBranchNameList.add(Constants.GLOBAL_BLANK);

	}



public String getEmployeeNumber() {

		return employeeNumber;

	}

	public void setEmployeeNumber(String employeeNumber) {

		this.employeeNumber = employeeNumber;

	}

	public ArrayList getEmployeeNumberList() {

		return employeeNumberList;

	}

	public void setEmployeeNumberList(String employeeNumber) {

		employeeNumberList.add(employeeNumber);

	}

	public void clearEmployeeNumberList() {

		employeeNumberList.clear();
		employeeNumberList.add(Constants.GLOBAL_BLANK);

	}


public String getBioNumber() {

		return bioNumber;

	}

	public void setBioNumber(String bioNumber) {

		this.bioNumber = bioNumber;

	}


public String getManagingBranch() {

		return managingBranch;

	}

	public void setManagingBranch(String managingBranch) {

		this.managingBranch = managingBranch;

	}

public String getCurrentJobPosition() {

		return currentJobPosition;

	}

	public void setCurrentJobPosition(String currentJobPosition) {

		this.currentJobPosition = currentJobPosition;

	}



    public String getAddress() {

        return address;

    }

    public void setAddress(String address) {

        this.address = address;

    }

    public String getCity() {

        return city;

    }

    public void setCity(String city) {

        this.city = city;

    }

    public String getStateProvince() {

        return stateProvince;

    }

    public void setStateProvince(String stateProvince) {

        this.stateProvince = stateProvince;

    }

    public String getPostalCode() {

        return postalCode;

    }

    public void setPostalCode(String postalCode) {

        this.postalCode = postalCode;

    }

    public String getCountry() {

        return country;

    }

    public void setCountry(String country) {

        this.country = country;

    }

    public String getContact() {

        return contact;

    }

    public void setContact(String contact) {

        this.contact = contact;

    }

public String getEmployeeID() {

        return employeeID;

    }

    public void setEmployeeID(String employeeID) {

        this.employeeID = employeeID;

    }

public String getAccountNumber() {

        return accountNumber;

    }

    public void setAccountNumber(String accountNumber) {

        this.accountNumber = accountNumber;

    }

    public String getPhone() {

        return phone;

    }

    public void setPhone(String phone) {

        this.phone = phone;

    }

    public String getFax() {

        return fax;

    }

    public void setFax(String fax) {

        this.fax = fax;

    }

    public String getAlternatePhone() {

        return alternatePhone;

    }

    public void setAlternatePhone(String alternatePhone) {

        this.alternatePhone = alternatePhone;

    }

    public String getAlternateContact() {

        return alternateContact;

    }

    public void setAlternateContact(String alternateContact) {

        this.alternateContact = alternateContact;

    }

    public String getEmail() {

        return email;

    }

    public void setEmail(String email) {

        this.email = email;

    }

    public String getBillToAddress() {

        return billToAddress;

    }

    public void setBillToAddress(String billToAddress) {

        this.billToAddress = billToAddress;

    }

    public String getBillToContact() {

        return billToContact;

    }

    public void setBillToContact(String billToContact) {

        this.billToContact = billToContact;

    }

    public String getBillToAltContact() {

        return billToAltContact;

    }

    public void setBillToAltContact(String billToAltContact) {

        this.billToAltContact = billToAltContact;

    }

    public String getBillToPhone() {

        return billToPhone;

    }

    public void setBillToPhone(String billToPhone) {

        this.billToPhone = billToPhone;

    }

    public String getBillingHeader() {

        return billingHeader;

    }

    public void setBillingHeader(String billingHeader) {

        this.billingHeader = billingHeader;

    }

    public String getBillingFooter() {

        return billingFooter;

    }

    public void setBillingFooter(String billingFooter) {

        this.billingFooter = billingFooter;

    }
    public String getBillingHeader2() {

        return billingHeader2;

    }
    public void setBillingHeader2(String billingHeader2) {

        this.billingHeader2 = billingHeader2;

    }

    public String getBillingFooter2() {

        return billingFooter2;

    }

    public void setBillingFooter2(String billingFooter2) {

        this.billingFooter2 = billingFooter2;

    }

    public String getBillingHeader3() {

        return billingHeader3;

    }
    public void setBillingHeader3(String billingHeader3) {

        this.billingHeader3 = billingHeader3;

    }

    public String getBillingFooter3() {

        return billingFooter3;

    }

    public void setBillingFooter3(String billingFooter3) {

        this.billingFooter3 = billingFooter3;

    }


    public String getBillingSignatory() {

        return billingSignatory;

    }

    public void setBillingSignatory(String billingSignatory) {

        this.billingSignatory = billingSignatory;

    }

    public String getSignatoryTitle() {

        return signatoryTitle;

    }

    public void setSignatoryTitle(String signatoryTitle) {

        this.signatoryTitle = signatoryTitle;

    }

    public String getShipToAddress() {

        return shipToAddress;

    }

    public void setShipToAddress(String shipToAddress) {

        this.shipToAddress = shipToAddress;

    }

    public String getShipToContact() {

        return shipToContact;

    }

    public void setShipToContact(String shipToContact) {

        this.shipToContact = shipToContact;

    }

    public String getShipToAltContact() {

        return shipToAltContact;

    }

    public void setShipToAltContact(String shipToAltContact) {

        this.shipToAltContact = shipToAltContact;

    }

    public String getShipToPhone() {

        return shipToPhone;

    }

    public void setShipToPhone(String shipToPhone) {

        this.shipToPhone = shipToPhone;

    }

    public String getTinNumber() {

        return tinNumber;

    }

    public void setTinNumber(String tinNumber) {

        this.tinNumber = tinNumber;

    }


    public boolean getEnable() {

        return enable;

    }

    public void setEnable(boolean enable) {

        this.enable = enable;

    }


public boolean getEnableRetailCashier() {

        return enableRetailCashier;

    }

    public void setEnableRetailCashier(boolean enableRetailCashier) {

        this.enableRetailCashier = enableRetailCashier;

    }

public boolean getEnableRebate() {

        return enableRebate;

    }

    public void setEnableRebate(boolean enableRebate) {

        this.enableRebate = enableRebate;

    }

public boolean getEnablePayroll() {

        return enablePayroll;

    }

    public void setEnablePayroll(boolean enablePayroll) {

        this.enablePayroll = enablePayroll;

    }


public boolean getAutoComputeInterest() {

        return autoComputeInterest;

    }

    public void setAutoComputeInterest(boolean autoComputeInterest) {

        this.autoComputeInterest = autoComputeInterest;

    }



public boolean getAutoComputePenalty() {

        return autoComputePenalty;

    }

    public void setAutoComputePenalty(boolean autoComputePenalty) {

        this.autoComputePenalty = autoComputePenalty;

    }

    public String getNextCustomerCode() {

        return nextCustomerCode;

    }

    public void setNextCustomerCode(String nextCustomerCode) {

        this.nextCustomerCode = nextCustomerCode;

    }

    public boolean getIsNew() {

        return isNew;

    }

    public void setIsNew(boolean isNew) {

        this.isNew = isNew;

    }

    public String getBankAccount() {

        return bankAccount;

    }

    public void setBankAccount(String bankAccount) {

        this.bankAccount = bankAccount;

    }

    public ArrayList getBankAccountList() {

        return bankAccountList;

    }

    public void setBankAccountList(String bankAccount) {

        bankAccountList.add(bankAccount);

    }

    public void clearBankAccountList() {

        bankAccountList.clear();
        bankAccountList.add(Constants.GLOBAL_BLANK);

    }

    public String getReceivableAccount() {

        return receivableAccount;

    }

    public void setReceivableAccount(String receivableAccount) {

        this.receivableAccount = receivableAccount;

    }

    public String getReceivableDescription() {

        return receivableDescription;

    }

    public void setReceivableDescription(String receivableDescription) {

        this.receivableDescription = receivableDescription;

    }

    public String getRevenueAccount() {

        return revenueAccount;

    }

    public void setRevenueAccount(String revenueAccount) {

        this.revenueAccount = revenueAccount;

    }

    public String getRevenueDescription() {

        return revenueDescription;

    }

    public void setRevenueDescription(String revenueDescription) {

        this.revenueDescription = revenueDescription;

    }



public String getUnEarnedInterestAccount() {

        return unEarnedInterestAccount;

    }

    public void setUnEarnedInterestAccount(String unEarnedInterestAccount) {

        this.unEarnedInterestAccount = unEarnedInterestAccount;

    }

    public String getUnEarnedInterestDescription() {

        return unEarnedInterestDescription;

    }

    public void setUnEarnedInterestDescription(String unEarnedInterestDescription) {

        this.unEarnedInterestDescription = unEarnedInterestDescription;

    }


    public String getEarnedInterestAccount() {

        return earnedInterestAccount;

    }

    public void setEarnedInterestAccount(String earnedInterestAccount) {

        this.earnedInterestAccount = earnedInterestAccount;

    }

    public String getEarnedInterestDescription() {

        return earnedInterestDescription;

    }

    public void setEarnedInterestDescription(String earnedInterestDescription) {

        this.earnedInterestDescription = earnedInterestDescription;

    }


public String getUnEarnedPenaltyAccount() {

        return unEarnedPenaltyAccount;

    }

    public void setUnEarnedPenaltyAccount(String unEarnedPenaltyAccount) {

        this.unEarnedPenaltyAccount = unEarnedPenaltyAccount;

    }

    public String getUnEarnedPenaltyDescription() {

        return unEarnedPenaltyDescription;

    }

    public void setUnEarnedPenaltyDescription(String unEarnedPenaltyDescription) {

        this.unEarnedPenaltyDescription = unEarnedPenaltyDescription;

    }


    public String getEarnedPenaltyAccount() {

        return earnedPenaltyAccount;

    }

    public void setEarnedPenaltyAccount(String earnedPenaltyAccount) {

        this.earnedPenaltyAccount = earnedPenaltyAccount;

    }

    public String getEarnedPenaltyDescription() {

        return earnedPenaltyDescription;

    }

    public void setEarnedPenaltyDescription(String earnedPenaltyDescription) {

        this.earnedPenaltyDescription = earnedPenaltyDescription;

    }




    public String getIsCustomerClassEntered() {

        return isCustomerClassEntered;

    }

    public void setIsCustomerClassEntered(String isCustomerClassEntered) {

        this.isCustomerClassEntered = isCustomerClassEntered;

    }

    public String getIsCustomerTypeEntered() {

        return isCustomerTypeEntered;

    }

    public void setIsCustomerTypeEntered(String isCustomerTypeEntered) {

        this.isCustomerTypeEntered = isCustomerTypeEntered;

    }

    public boolean getEnableRevenueAccount() {

        return enableRevenueAccount;

    }

    public void setEnableRevenueAccount(boolean enableRevenueAccount) {

        this.enableRevenueAccount = enableRevenueAccount;

    }

    public boolean getAutoGenerateCustomerCode() {

        return autoGenerateCustomerCode;

    }

    public void setAutoGenerateCustomerCode(boolean autoGenerateCustomerCode) {

        this.autoGenerateCustomerCode = autoGenerateCustomerCode;

    }

    public Object[] getBcstList(){

        return arBcstList.toArray();

    }

    public ArBranchCustomerList getBcstByIndex(int index) {

        return ((ArBranchCustomerList)arBcstList.get(index));

    }

    public int getBcstListSize(){

        return(arBcstList.size());

    }

    public void saveBcstList(Object newArBcstList){

        arBcstList.add(newArBcstList);

    }

    public void clearBcstList(){

        arBcstList.clear();

    }

    public void setBcstList(ArrayList arBcstList) {

        this.arBcstList = arBcstList;

    }

    public String getBirthday() {

        return birthday;

    }

    public void setBirthday(String birthday) {

        this.birthday = birthday;

    }

    public String getDealPrice() {

        return dealPrice;

    }

    public void setDealPrice(String dealPrice) {

        this.dealPrice = dealPrice;

    }

    public ArrayList getDealPriceList() {

        return dealPriceList;

    }

    public void setDealPriceList(String dealPrice) {

        dealPriceList.add(dealPrice);

    }

    public void clearDealPriceList() {

        dealPriceList.clear();
        dealPriceList.add(Constants.GLOBAL_BLANK);

    }

    public String getMobilePhone() {

        return mobilePhone;

    }

    public void setMobilePhone(String mobilePhone) {

        this.mobilePhone = mobilePhone;

    }

    public String getAlternateMobilePhone() {

        return alternateMobilePhone;

    }

    public void setAlternateMobilePhone(String alternateMobilePhone) {

        this.alternateMobilePhone = alternateMobilePhone;

    }

    public String getSalesperson() {

        return salesperson;

    }

    public void setSalesperson(String salesperson) {

        this.salesperson = salesperson;

    }

    public String getSalesperson2() {

        return salesperson2;

    }

    public void setSalesperson2(String salesperson2) {

        this.salesperson2 = salesperson2;

    }


    public ArrayList getSalespersonList() {

        return salespersonList;

    }

    public void setSalespersonList(String salesperson) {

        salespersonList.add(salesperson);

    }

    public void clearSalespersonList() {

        salespersonList.clear();
        salespersonList.add(Constants.GLOBAL_BLANK);

    }

    public ArrayList getSalesperson2List() {

        return salesperson2List;

    }

    public void setSalesperson2List(String salesperson2) {

        salesperson2List.add(salesperson2);

    }

    public void clearSalesperson2List() {

        salesperson2List.clear();
        salesperson2List.add(Constants.GLOBAL_BLANK);

    }


    public String getTemplateName() {

        return templateName;

    }

    public void setTemplateName(String templateName) {

        this.templateName = templateName;

    }

    public ArrayList getTemplateList() {

        return templateList;

    }

    public void setTemplateList(String template) {


        templateList.add(template);

    }

    public void clearTemplateList() {

        templateList.clear();
        templateList.add(Constants.GLOBAL_BLANK);

    }

    public String getArea() {

        return area;

    }

    public void setArea(String area) {


        this.area = area;

    }

    public String getSquareMeter() {

        return squareMeter;

    }

    public void setSquareMeter(String squareMeter) {


        this.squareMeter = squareMeter;

    }

    public String getNumbersParking() {

        return numbersParking;

    }

    public void setNumbersParking(String numbersParking) {


        this.numbersParking = numbersParking;

    }

    public String getMonthlyInterestRate() {

        return monthlyInterestRate;

    }

    public void setMonthlyInterestRate(String monthlyInterestRate) {


        this.monthlyInterestRate = monthlyInterestRate;

    }


    public String getParkingID() {

        return parkingID;

    }

    public void setParkingID(String parkingID) {


        this.parkingID = parkingID;

    }

    public String getAssociationDuesRate() {

        return associationDuesRate;

    }

    public void setAssociationDuesRate(String associationDuesRate) {


        this.associationDuesRate = associationDuesRate;

    }


    public String getRealPropertyTaxRate() {

        return realPropertyTaxRate;

    }

    public void setRealPropertyTaxRate(String realPropertyTaxRate) {


        this.realPropertyTaxRate = realPropertyTaxRate;

    }


    public String getWpCustomerID() {

        return wpCustomerID;

    }

    public void setWpCustomerID(String wpCustomerID) {


        this.wpCustomerID = wpCustomerID;

    }

    public String getEntryDate() {

        return entryDate;

    }

    public void setEntryDate(String entryDate) {


        this.entryDate = entryDate;

    }

    public String getEffectivityDays() {

        return effectivityDays;

    }

    public void setEffectivityDays(String effectivityDays) {


        this.effectivityDays = effectivityDays;

    }

    public String getRegion() {

        return region;

    }

    public void setRegion(String region) {


        this.region = region;

    }

    public ArrayList getRegionList() {

        return regionList;

    }

    public void setRegionList(String region) {

        regionList.add(region);

    }

    public void clearRegionList() {

        regionList.clear();
        regionList.add(Constants.GLOBAL_BLANK);

    }

    public String getMemo() {

    	return memo;

    }

    public void setMemo(String memo) {

    	this.memo = memo;

    }

    public String getCustomerBatch() {

        return customerBatch;

     }

     public void setCustomerBatch(String customerBatch) {

        this.customerBatch = customerBatch;

     }

     public ArrayList getCustomerBatchList() {

        return customerBatchList;

     }

     public void setCustomerBatchList(String customerBatch) {

        customerBatchList.add(customerBatch);

     }

     public void clearCustomerBatchList() {

  	   customerBatchList.clear();
  	   customerBatchList.add(Constants.GLOBAL_BLANK);

     }


     public String getCustomerDepartment() {

         return customerDepartment;

      }

      public void setCustomerDepartment(String customerDepartment) {

         this.customerDepartment = customerDepartment;

      }

      public ArrayList getCustomerDepartmentList() {

         return customerDepartmentList;

      }

      public void setCustomerDepartmentList(String customerDepartment) {

         customerDepartmentList.add(customerDepartment);

      }

      public void clearCustomerDepartmentList() {

   	   customerDepartmentList.clear();
   	   customerDepartmentList.add(Constants.GLOBAL_BLANK);

      }


     public String getApprovalStatus() {

 		return approvalStatus;

 	}

 	public void setApprovalStatus(String approvalStatus) {

 		this.approvalStatus = approvalStatus;

 	}

 	public String getReasonForRejection() {

 		return reasonForRejection;

 	}

 	public void setReasonForRejection(String reasonForRejection) {

 		this.reasonForRejection = reasonForRejection;

 	}

 	public String getPosted() {

 		return posted;

 	}

 	public void setPosted(String posted) {

 		this.posted = posted;

 	}

 	public String getCreatedBy() {

 		return createdBy;

 	}

 	public void setCreatedBy(String createdBy) {

 		this.createdBy = createdBy;

 	}

 	public String getDateCreated() {

 		return dateCreated;

 	}

 	public void setDateCreated(String dateCreated) {

 		this.dateCreated = dateCreated;

 	}

 	public String getLastModifiedBy() {

 		return lastModifiedBy;

 	}

 	public void setLastModifiedBy(String lastModifiedBy) {

 		this.lastModifiedBy = lastModifiedBy;

 	}

 	public String getDateLastModified() {

 		return dateLastModified;

 	}

 	public void setDateLastModified(String dateLastModified) {

 		this.dateLastModified = dateLastModified;

 	}


 	public String getApprovedRejectedBy() {

 		return approvedRejectedBy;

 	}

 	public void setApprovedRejectedBy(String approvedRejectedBy) {

 		this.approvedRejectedBy = approvedRejectedBy;

 	}

 	public String getDateApprovedRejected() {

 		return dateApprovedRejected;

 	}

 	public void setDateApprovedRejected(String dateApprovedRejected) {

 		this.dateApprovedRejected = dateApprovedRejected;

 	}

 	public String getPostedBy() {

 		return postedBy;

 	}

 	public void setPostedBy(String postedBy) {

 		this.postedBy = postedBy;

 	}

 	public String getDatePosted() {

 		return datePosted;

 	}

 	public void setDatePosted(String datePosted) {

 		this.datePosted = datePosted;

 	}

    public void reset(ActionMapping mapping, HttpServletRequest request) {

        customerCode = null;
        customerName = null;
        description = null;
        creditLimit = null;
        squareMeter = null;
        numbersParking = null;
        monthlyInterestRate = null;
        parkingID = null;
        realPropertyTaxRate = null;
        associationDuesRate = null;
        wpCustomerID=null;
        customerType = Constants.GLOBAL_BLANK;
        customerClass = Constants.GLOBAL_BLANK;
        paymentTerm = Constants.GLOBAL_BLANK;
        bankAccount = Constants.GLOBAL_BLANK;
        paymentMethodList.clear();
        paymentMethodList.add(Constants.GLOBAL_BLANK);
        paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CASH);
        paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CHECK);
        paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CREDIT_CARD);
        paymentMethod = Constants.GLOBAL_BLANK;

        deployedBranchName = null;
        employeeNumber = null;
		bioNumber = null;
		managingBranch = null;
		currentJobPosition = null;

        address = null;
        city = null;
        stateProvince = null;
        postalCode = null;
        country = null;
        contact = null;
        employeeID = null;
        phone = null;
        fax = null;
        alternatePhone = null;
        alternateContact = null;
        email = null;
        billToAddress = null;
        billToContact = null;
        billToAltContact = null;
        billToPhone = null;
        billingHeader = null;
        billingFooter2 = null;
        billingHeader2 = null;
        billingFooter3 = null;
        billingHeader3 = null;
        billingFooter = null;
        billingSignatory = null;
        signatoryTitle = null;
        shipToAddress = null;
        shipToContact = null;
        shipToAltContact = null;
        shipToPhone = null;
        tinNumber = null;
        enable = false;
        enableRetailCashier = false;
        enableRebate = false;
        enablePayroll =false;

        autoComputeInterest = false;
        autoComputePenalty = false;
        receivableAccount = null;
        receivableDescription = null;
        revenueAccount = null;
        revenueDescription = null;
        unEarnedInterestAccount = null;
        unEarnedInterestDescription = null;
        earnedInterestAccount = null;
        earnedInterestDescription = null;

        unEarnedPenaltyAccount = null;
        unEarnedPenaltyDescription = null;
        earnedPenaltyAccount = null;
        earnedPenaltyDescription = null;

        isCustomerClassEntered = null;
        isCustomerTypeEntered = null;
        birthday = null;
        dealPrice = null;
        mobilePhone = null;
        alternateMobilePhone = null;
        salesperson = null;
        salesperson2 = null;
        templateName = Constants.GLOBAL_BLANK;
        region = Constants.GLOBAL_BLANK;
        memo = null;

        for (int i=0; i<arBcstList.size(); i++) {

            ArBranchCustomerList actionList = (ArBranchCustomerList)arBcstList.get(i);
            actionList.setBranchCheckbox(false);

        }

    }

    public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();

        if (request.getParameter("saveButton") != null) {

            if (Common.validateRequired(customerCode)) {

                if (autoGenerateCustomerCode == false) {

                    errors.add("customerCode",
                            new ActionMessage("customerEntry.error.customerCodeRequired"));

                }

            }

            if (Common.validateRequired(customerName)) {

                errors.add("customerName",
                        new ActionMessage("customerEntry.error.customerNameRequired"));

            }

            if (Common.validateRequired(customerClass) || customerClass.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

                errors.add("customerClass",
                        new ActionMessage("customerEntry.error.customerClassRequired"));

            }

            if (Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

                errors.add("paymentTerm",
                        new ActionMessage("customerEntry.error.paymentTermRequired"));

            }

            if (!Common.validateStringExists(customerTypeList, customerType)) {

                errors.add("customerType",
                        new ActionMessage("customerEntry.error.customerTypeInvalid"));

            }

            if (!Common.validateStringExists(paymentTermList, paymentTerm)) {

                errors.add("paymentTerm",
                        new ActionMessage("customerEntry.error.paymentTermInvalid"));

            }

            if (!Common.validateStringExists(customerClassList, customerClass)) {

                errors.add("customerClass",
                        new ActionMessage("customerEntry.error.customerClassInvalid"));

            }

            if (Common.validateRequired(bankAccount)) {

                errors.add("bankAccount",
                        new ActionMessage("customerEntry.error.bankAccountRequired"));

            }

            if (Common.validateRequired(receivableAccount)) {

                errors.add("receivableAccount",
                        new ActionMessage("customerEntry.error.receivableAccountRequired"));

            }

            if (Common.validateRequired(revenueAccount) && enableRevenueAccount) {

                errors.add("revenueAccount",
                        new ActionMessage("customerEntry.error.revenueAccountRequired"));

            }

            if(!Common.validateDateFormat(birthday)){
                errors.add("birthday",
                        new ActionMessage("customerEntry.error.birthdayInvalid"));
            }
            //autoComputeInterest = false;
            //autoComputePenalty = false;

            if(autoComputeInterest){


            	/*
                if (Common.validateRequired(unEarnedInterestAccount)) {

                errors.add("unEarnedInterestAccount",
                        new ActionMessage("customerEntry.error.unEarnedInterestAccount"));

                }
            	 */
                if (Common.validateRequired(earnedInterestAccount)) {

                    errors.add("earnedInterestAccount",
                            new ActionMessage("customerEntry.error.earnedInterestAccount"));

                }


            }

            if(autoComputePenalty){
            	/*
                if (Common.validateRequired(unEarnedPenaltyAccount)) {

                errors.add("unEarnedPenaltyAccount",
                        new ActionMessage("customerEntry.error.unEarnedPenaltyAccount"));

                }
        	    */
                if (Common.validateRequired(earnedPenaltyAccount)) {

                    errors.add("earnedPenaltyAccount",
                            new ActionMessage("customerEntry.error.earnedPenaltyAccount"));

                }
            }

        }

        return errors;
    }
}