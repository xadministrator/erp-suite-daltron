package com.struts.ar.customertypes;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArCustomerTypeController;
import com.ejb.txn.ArCustomerTypeControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArCustomerTypeDetails;
import com.util.ArModCustomerTypeDetails;

public final class ArCustomerTypeAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
      	
      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArCustomerTypeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ArCustomerTypeForm actionForm = (ArCustomerTypeForm)form;
        
        String frParam = Common.getUserPermission(user, Constants.AR_CUSTOMER_TYPE_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("arCustomerTypes");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ArCustomerTypeController EJB
*******************************************************/

        ArCustomerTypeControllerHome homeCT = null;
        ArCustomerTypeController ejbCT = null;

        try {

            homeCT = (ArCustomerTypeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArCustomerTypeControllerEJB", ArCustomerTypeControllerHome.class);
            
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArCustomerTypeAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbCT = homeCT.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArCustomerTypeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ar  CT Show Details Action --
*******************************************************/

        if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arCustomerTypes"));
	     
/*******************************************************
   -- Ar  CT Hide Details Action --
*******************************************************/	     
	     
	    } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arCustomerTypes"));                  
         
/*******************************************************
   -- Ar  CT Save Action --
*******************************************************/

        } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArCustomerTypeDetails details = new ArCustomerTypeDetails();
            details.setCtName(actionForm.getName());
            details.setCtDescription(actionForm.getDescription());
            details.setCtEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            
            try {
            	
            	ejbCT.addArCtEntry(details, actionForm.getBankAccount(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("customerType.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in ArCustomerTypeAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	return mapping.findForward("cmnErrorPage"); 
                  
               	}

            }

/*******************************************************
   -- Ar  CT Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar  CT Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            ArCustomerTypeList arCTList =
	            actionForm.getArCTByIndex(actionForm.getRowSelected());
            
            
            ArCustomerTypeDetails details = new ArCustomerTypeDetails();
            details.setCtCode(arCTList.getCustomerTypeCode());
            details.setCtName(actionForm.getName());
            details.setCtDescription(actionForm.getDescription());
            details.setCtEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            
            try {
            	
            	ejbCT.updateArCtEntry(details, actionForm.getBankAccount(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("customerType.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ar  CT Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ar  CT Edit Action --
*******************************************************/

         } else if (request.getParameter("arCTList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showArCTRow(actionForm.getRowSelected());
            
            return mapping.findForward("arCustomerTypes");
            
/*******************************************************
   -- Ar  CT Delete Action --
*******************************************************/

         } else if (request.getParameter("arCTList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ArCustomerTypeList arCTList =
              actionForm.getArCTByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbCT.deleteArCtEntry(arCTList.getCustomerTypeCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("customerType.error.deleteCustomerTypeAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("customerType.error.customerTypeAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArCustomerTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar  CT Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arCustomerTypes");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               	actionForm.clearArCTList();	        

               	actionForm.clearBankAccountList();           	
            	
               	list = ejbCT.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
               	if (list == null || list.size() == 0) {
            		
                	actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
               	} else {
            		           		            		
            		i = list.iterator();
            		
                  	while (i.hasNext()) {
            			
            	    	actionForm.setBankAccountList((String)i.next());
            			
            	  	}
            		            		
               	} 	           
                              	           
	           	list = ejbCT.getArCtAll(user.getCmpCode()); 
	           
	           	i = list.iterator();
	            
	           	while(i.hasNext()) {
        	            			            	
	              	ArModCustomerTypeDetails mdetails = (ArModCustomerTypeDetails)i.next();
	            	
	              	ArCustomerTypeList arCTList = new ArCustomerTypeList(actionForm,
	            		mdetails.getCtCode(),
	            	    mdetails.getCtName(),
	            	    mdetails.getCtDescription(),
	            	    mdetails.getCtBaName(),
                        Common.convertByteToBoolean(mdetails.getCtEnable()));                            
	            	 	            	    
	              	actionForm.saveArCTList(arCTList);
	            	
	           	}
	                          
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArCustomerTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("arCustomerTypes"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ArCustomerTypeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}