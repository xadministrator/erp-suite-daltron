package com.struts.ar.customertypes;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArCustomerTypeForm extends ActionForm implements Serializable {

	private Integer customerTypeCode = null;
	private String name = null;
	private String description = null;
    private String bankAccount = null;
    private ArrayList bankAccountList = new ArrayList();
	private boolean enable = false;
	
	private String tableType = null;
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList arCTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
	
	    return rowSelected;
	
	}
	
	public ArCustomerTypeList getArCTByIndex(int index) {
	
	    return((ArCustomerTypeList)arCTList.get(index));
	
	}
	
	public Object[] getArCTList() {
	
	    return arCTList.toArray();
	
	}
	
	public int getArCTListSize() {
	
	    return arCTList.size();
	
	}
	
	public void saveArCTList(Object newArCTList) {
	
	    arCTList.add(newArCTList);
	
	}
	
	public void clearArCTList() {
		
	    arCTList.clear();
	  
	}
	
	public void setRowSelected(Object selectedArCTList, boolean isEdit) {
	
	    this.rowSelected = arCTList.indexOf(selectedArCTList);
	
	    if (isEdit) {
	
	        this.pageState = Constants.PAGE_STATE_EDIT;
	
	    }
	
	}
	
	public void showArCTRow(int rowSelected) {
		
		this.name = ((ArCustomerTypeList)arCTList.get(rowSelected)).getName();
		this.description = ((ArCustomerTypeList)arCTList.get(rowSelected)).getDescription();
		
		if (!this.bankAccountList.contains(((ArCustomerTypeList)arCTList.get(rowSelected)).getBankAccount())) {
			
			if (this.bankAccountList.contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				this.bankAccountList.clear();
				this.bankAccountList.add(Constants.GLOBAL_BLANK);
				
			}
			this.bankAccountList.add(((ArCustomerTypeList)arCTList.get(rowSelected)).getBankAccount());
			
		}		
        this.bankAccount = ((ArCustomerTypeList)arCTList.get(rowSelected)).getBankAccount();
        
		this.enable = ((ArCustomerTypeList)arCTList.get(rowSelected)).getEnable(); 
	   
	}
	
	public void updateArCTRow(int rowSelected, Object newArCTList) {
	
	    arCTList.set(rowSelected, newArCTList);
	
	}
	
	public void deleteArCTList(int rowSelected) {
	
	    arCTList.remove(rowSelected);
	
	}
	
	public void setUpdateButton(String updateButton) {
	
	    this.updateButton = updateButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
	
	    this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	    this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	    this.closeButton = closeButton;
	
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
	    this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
	    this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
	
	    this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	    return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getCustomerTypeCode() {
		
		return customerTypeCode;
		
	}
	
	public void setCustomerTypeCode(Integer customerTypeCode) {
		
		this.customerTypeCode = customerTypeCode;
		
	}
	
	public String getName() {
	
	  	return name;
	
	}
	
	public void setName(String name) {
	
	  	this.name = name;
	
	}
	
	public String getDescription() {
	
		return description;
	
	}
	
	public void setDescription(String description) {
	
	  	this.description = description;
	
	}
	
	public String getBankAccount() {
		
		return bankAccount;
		
	}
	
	public void setBankAccount(String bankAccount) {
		
		this.bankAccount = bankAccount;
		
	}
	
	public ArrayList getBankAccountList() {
		
		return bankAccountList;
		
	}
	
	public void setBankAccountList(String bankAccount) {
		
		bankAccountList.add(bankAccount);
		
	}
	
	public void clearBankAccountList() {
		
		bankAccountList.clear();
		bankAccountList.add(Constants.GLOBAL_BLANK);
		
	}
    
    public boolean getEnable() {
    	
    	return enable;
    	
    }
    
    public void setEnable(boolean enable) {
    	
    	this.enable = enable;
    	
    }
	
	public String getTableType() {
		
	  	return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
	  	this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
	
		name = null;
		description = null;
    	bankAccount = null;
		enable = false;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;      
	
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
			if (Common.validateRequired(name)) {
			
				errors.add("name",
			   		new ActionMessage("customerType.error.nameRequired"));
			
			}
			
			if (Common.validateRequired(bankAccount)) {
			
				errors.add("bankAccount",
			   		new ActionMessage("customerType.error.bankAccountRequired"));
			
			}
			
	  	}
	     
	  	return errors;
	
	}
	
}