package com.struts.ar.customertypes;

import java.io.Serializable;

public class ArCustomerTypeList implements Serializable {

	private Integer customerTypeCode = null;
	private String name = null;
	private String description = null;
	private String bankAccount = null;
	private boolean enable = false;

	private String editButton = null;
	private String deleteButton = null;
	
	private ArCustomerTypeForm parentBean;
    
	public ArCustomerTypeList(ArCustomerTypeForm parentBean,
		Integer customerTypeCode,
		String name,
		String description,
		String bankAccount,
		boolean enable) {
	
		this.parentBean = parentBean;
		this.customerTypeCode = customerTypeCode;
		this.name = name;
		this.description = description;
		this.bankAccount = bankAccount;
		this.enable = enable;
		
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getCustomerTypeCode() {
		
		return customerTypeCode;
		
	}
	
	public String getName() {
		
		return name;
		
    }
    
    public String getDescription() {
    	
    	return description;
    	
    }
    
    public String getBankAccount() {
    	
    	return bankAccount;
    	
    }
    
    public boolean getEnable() {
    	
    	return enable;
    	
    }
        	    	
}