package com.struts.ar.delivery;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArDeliveryController;
import com.ejb.txn.ArDeliveryControllerHome;
import com.ejb.txn.ArJobOrderEntryController;
import com.ejb.txn.ArJobOrderEntryControllerHome;
import com.struts.ar.joborderassignment.ArJobOrderAssignmentList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Responsibility;
import com.struts.util.User;
import com.util.ArModDeliveryDetails;
import com.util.ArModJobOrderAssignmentDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArPersonelDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ArDeliveryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArDeliveryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
            
     

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArDeliveryForm actionForm = (ArDeliveryForm)form;
         
         actionForm.setReport(null);
         
         String frParam = Common.getUserPermission(user, Constants.AR_DELIVERY_ID);

         if (frParam != null) {
         	
         	if (frParam.trim().equals(Constants.FULL_ACCESS)) {
         		
         		ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
         		if (!fieldErrors.isEmpty()) {
         			
         			saveErrors(request, new ActionMessages(fieldErrors));
         			
         			return mapping.findForward("arDelivery");
         		}
         		
         	}
         	
         	actionForm.setUserPermission(frParam.trim());
         	
         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArDeliveryController EJB
*******************************************************/

         ArDeliveryControllerHome homeDV = null;
         ArDeliveryController ejbDV = null;

         int deliverLineNumber =4;
       

         try {

        	 homeDV = (ArDeliveryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArDeliveryControllerEJB", ArDeliveryControllerHome.class);      
            
          
        
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArDeliveryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {
        	 ejbDV = homeDV.create();
          
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArDeliveryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
         boolean isInitialPrinting = false;
    
/*******************************************************
   -- Ar DV Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null) {
         	
         	ArrayList list = new ArrayList();
         	System.out.println("pasok");
         	
         
         	for(int i=0; i<actionForm.getArDVListSize(); i++) {
         		
         		ArDeliveryList arDVList = (ArDeliveryList)actionForm.getArDVByIndex(i);
         		
         		if(Common.validateRequired(arDVList.getDvContainer()))continue;
         		
         		
         		
         		
         		
         		
         		ArModDeliveryDetails mdetails = new ArModDeliveryDetails();
         		
         		
         		mdetails.setDvCode(Common.validateRequired(arDVList.getDvCode())?null:Integer.parseInt(arDVList.getDvCode()));
         		
         		mdetails.setDvContainer(arDVList.getDvContainer());
         		mdetails.setDvDeliveryNumber(arDVList.getDvDeliveryNumber());
         		mdetails.setDvBookingTime(arDVList.getDvBookingTime());
         		mdetails.setDvTabsFeePullOut(arDVList.getDvTabsFeePullOut());
         		mdetails.setDvReleasedDate(arDVList.getDvReleasedDate());
         		mdetails.setDvDeliveredDate(arDVList.getDvDeliveredDate());
         		mdetails.setDvDeliveredTo(arDVList.getDvDeliveredTo());
         		mdetails.setDvPlateNo(arDVList.getDvPlateNo());
         		mdetails.setDvDriverName(arDVList.getDvDriverName());
         		mdetails.setDvEmptyReturnDate(arDVList.getDvEmptyReturnDate());
         		mdetails.setDvEmptyReturnTo(arDVList.getDvEmptyReturnTo());
         		mdetails.setDvTabsFeeReturn(arDVList.getDvTabsFeeReturn());
         		mdetails.setDvStatus(arDVList.getDvStatus());
         		mdetails.setDvRemarks(arDVList.getDvRemarks());
  
         		mdetails.setSoTransactionStatus(actionForm.getTransactionStatus());
                list.add(mdetails);
                
              
         	}
         	
         
         	
         	try {
         		
         		//save ar job order assignment
         		ejbDV.saveArDelivery(list, actionForm.getSoDocumentNumber(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         		
         		
         		
         		
         		
         		
         		
         		actionForm.clearArDVList();
         		Integer salesOrderCode = Integer.parseInt(actionForm.getSoCode());
         		
         		
         		
         		
    			ArrayList listDetails = ejbDV.getDeliveryBySoCode(salesOrderCode,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    			
    		
    			Iterator iDv = listDetails.iterator();
    			
    			while (iDv.hasNext()) {
    				
    				
    				ArModDeliveryDetails details = (ArModDeliveryDetails)iDv.next();
    				
    			//	actionForm.setSoCode(salesOrderCode);
    		//		actionForm.setSoDocumentNumber(details.getSoDocumentNumber());
    			//	actionForm.setSoReferenceNumber(details.getSoReferenceNumber());
    			//	actionForm.setCstCustomerCode(details.getCstName());
    				
    				ArDeliveryList arDVList = new ArDeliveryList(actionForm, details.getDvCode().toString(), details.getDvContainer(), details.getDvDeliveryNumber(), details.getDvBookingTime(), details.getDvTabsFeePullOut(), details.getDvReleasedDate(),
    						details.getDvDeliveredDate(), details.getDvDeliveredTo(), details.getDvPlateNo(), details.getDvDriverName(), details.getDvEmptyReturnDate(), details.getDvEmptyReturnTo(),
    						details.getDvTabsFeeReturn(), details.getDvStatus(), details.getDvRemarks());
    				
    				
    				arDVList.setDvStatusList(actionForm.getDeliveryStatusList());
    				
    				actionForm.saveArDVList(arDVList);
    				
    				
    				
    			}
         		
         		
         
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ArDeliveryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage"); 
         			
         		}
         		
         	}  
         	       	
/*******************************************************
 -- Ar JA Add Lines Action --
 *******************************************************/
         	
         } else if(request.getParameter("addLinesButton") != null) {
         	
         	int listSize = actionForm.getArDVListSize();
         	
         	for (int x = listSize + 1; x <= listSize + deliverLineNumber; x++) {
         		
         		ArDeliveryList arDVList = new ArDeliveryList(actionForm, null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
         		
         		arDVList.setDvStatusList(actionForm.getDeliveryStatusList());
         		
         		actionForm.saveArDVList(arDVList);
         		
         	}	        
         	
         	return(mapping.findForward("arDelivery"));
         	
/*******************************************************
 -- Ar DV Delete  Action --
 *******************************************************/
         	
         } else if(request.getParameter("deleteButton") != null) {
         	
        
          	 System.out.println("pasok");
          	
          	 for (int i = 0; i<actionForm.getArDVListSize(); i++) {
           		
        		 ArDeliveryList arDVList = actionForm.getArDVByIndex(i);
          		
          		if (arDVList.getSelectCheckbox() && arDVList.getDvCode() == null ) {
          			
          			actionForm.deleteArDVList(i);
          			i--;
          		}
          		
          	}
          	for(int i=0; i<actionForm.getArDVListSize(); i++) {
          		
          		ArDeliveryList arDVList = (ArDeliveryList)actionForm.getArDVByIndex(i);
          		
          		
          		
          		if(!arDVList.getSelectCheckbox() ) {
          			System.out.println("why?");
          			
          			System.out.println("code:" + arDVList.getDvCode());
          			System.out.println("chkbx:" + arDVList.getSelectCheckbox());
          			continue;
          		}
          		
          		
          		ArModDeliveryDetails mdetails = new ArModDeliveryDetails();
          		
          		
          		mdetails.setDvCode(Common.validateRequired(arDVList.getDvCode())?null:Integer.parseInt(arDVList.getDvCode()));
          		
          		mdetails.setDvContainer(arDVList.getDvContainer());
          		mdetails.setDvDeliveryNumber(arDVList.getDvDeliveryNumber());
          		mdetails.setDvBookingTime(arDVList.getDvBookingTime());
          		mdetails.setDvTabsFeePullOut(arDVList.getDvTabsFeePullOut());
          		mdetails.setDvReleasedDate(arDVList.getDvReleasedDate());
          		mdetails.setDvDeliveredDate(arDVList.getDvDeliveredDate());
          		mdetails.setDvDeliveredTo(arDVList.getDvDeliveredTo());
          		mdetails.setDvPlateNo(arDVList.getDvPlateNo());
          		mdetails.setDvDriverName(arDVList.getDvDriverName());
          		mdetails.setDvEmptyReturnDate(arDVList.getDvEmptyReturnDate());
          		mdetails.setDvEmptyReturnTo(arDVList.getDvEmptyReturnTo());
          		mdetails.setDvTabsFeeReturn(arDVList.getDvTabsFeeReturn());
          		mdetails.setDvStatus(arDVList.getDvStatus());
          		mdetails.setDvRemarks(arDVList.getDvRemarks());
          		mdetails.setSoTransactionStatus(actionForm.getTransactionStatus());
   
          		try {
          			ejbDV.deleteDeliveryByDvCode(Integer.parseInt(arDVList.getDvCode()));
          			actionForm.deleteArDVList(i);
          			i--;
          		}catch(Exception ex) {
          			continue;
          		}
          		
          		
                 
               
          	}
        	 
        	
        	 
        	 
         	
         	
         	return(mapping.findForward("arDelivery"));	 
 

/*******************************************************
 -- Ar DV Close Action --
 *******************************************************/
         	
         } else if (request.getParameter("closeButton") != null) {
            
            return(mapping.findForward("cmnMain"));
          
/*******************************************************
   -- Ar DV Back Action --
*******************************************************/

     	} else if (request.getParameter("backButton") != null) {
     		
     		String path = null;
     		System.out.println("back so code:" + actionForm.getSoCode());
     		path = "/arSalesOrderEntry.do?forward=1&salesOrderCode=" + actionForm.getSoCode() ;
     		
     		return(new ActionForward(path));
     		

     		
/*******************************************************
   -- Ar DV Print Action --
*******************************************************/

         } else if (request.getParameter("arDVList[" +
            actionForm.getRowSelected() + "].printButton") != null) {
        	 
        	 
        	 System.out.println("selected is  : " +actionForm.getRowSelected() );
        	 
        	 System.out.println("so code is  : " +actionForm.getSoCode() );
        	 ArDeliveryList arDVList = actionForm.getArDVByIndex(actionForm.getRowSelected());
        	 
        	 actionForm.setReportType("SINGLE");
        	 actionForm.setReportDvCode(arDVList.getDvCode());
        	 actionForm.setReportSoCode(actionForm.getSoCode());
        	 actionForm.setViewType(actionForm.getViewType());
        	 
     		
     		  actionForm.setReport(Constants.STATUS_SUCCESS);
     		 return(mapping.findForward("arDelivery"));
           //   isInitialPrinting = true;
     		
/*******************************************************
   -- Ar DV Print All Action --
*******************************************************/

     	} else if (request.getParameter("printAllButton") != null) {
     		System.out.println("print all button");
     		actionForm.setReportType("ALL dv");
     		System.out.println("so code is  : " +actionForm.getSoCode() );

        	 actionForm.setReportSoCode(actionForm.getSoCode());
     		  actionForm.setReport(Constants.STATUS_SUCCESS);
     		 actionForm.setViewType(actionForm.getViewType());
     		return(mapping.findForward("arDelivery"));
             // isInitialPrinting = true;
/*******************************************************
 -- Ar JA Load Action --
 *******************************************************/
     		
     	}
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arDelivery");

            }
            
        
            
            try {
            	
        		ArrayList list = new ArrayList();
        		ArrayList prList = new ArrayList();
        		Iterator i = null;
        		Iterator y = null;
        		double amount=0;
        		double qty=0;
        		
        		 //get transaction status list
              	actionForm.clearTransactionStatusList();

              	list = ejbDV.getAdLvTransactionStatusAll(user.getCmpCode());

              	i = list.iterator();

              	actionForm.setTransactionStatusList(Constants.GLOBAL_BLANK);
          		while (i.hasNext()) {

          		    actionForm.setTransactionStatusList((String)i.next());

          		}

            	
        		
        		if (request.getParameter("forward") != null || isInitialPrinting) {
        			
        			System.out.println("YES");
        			actionForm.clearArJAList();
        			
        			
        			
        			String salesOrderCode = request.getParameter("salesOrderCode")!=null?request.getParameter("salesOrderCode"):actionForm.getSoCode(); 
        			
        			
        			System.out.println("soCode" + request.getParameter("salesOrderCode"));
        			
    				ArModSalesOrderDetails soDetails = ejbDV.getArSalesOrderByCode(Integer.parseInt(salesOrderCode));
    				
    				
    				
    				
    				actionForm.setSoCode(salesOrderCode);
    				actionForm.setSoDocumentNumber(soDetails.getSoDocumentNumber());
    				actionForm.setSoReferenceNumber(soDetails.getSoReferenceNumber());
    				actionForm.setCstCustomerCode(soDetails.getSoCstCustomerCode());
    				actionForm.setTransactionStatus(soDetails.getSoTransactionStatus());
        			
        			ArrayList listDetails = ejbDV.getDeliveryBySoCode(Integer.parseInt(salesOrderCode),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        			
        		
        			Iterator iDv = listDetails.iterator();
        			
        			while (iDv.hasNext()) {
        				
        				
        				ArModDeliveryDetails details = (ArModDeliveryDetails)iDv.next();
        				
        				actionForm.setSoCode(salesOrderCode);
        				actionForm.setSoDocumentNumber(details.getSoDocumentNumber());
        				actionForm.setSoReferenceNumber(details.getSoReferenceNumber());
        				actionForm.setCstCustomerCode(details.getCstName());
        				
        				ArDeliveryList arDVList = new ArDeliveryList(actionForm, details.getDvCode().toString(), details.getDvContainer(), details.getDvDeliveryNumber(), details.getDvBookingTime(), details.getDvTabsFeePullOut(), details.getDvReleasedDate(),
        						details.getDvDeliveredDate(), details.getDvDeliveredTo(), details.getDvPlateNo(), details.getDvDriverName(), details.getDvEmptyReturnDate(), details.getDvEmptyReturnTo(),
        						details.getDvTabsFeeReturn(), details.getDvStatus(), details.getDvRemarks());
        				
        				
        				arDVList.setDvStatusList(actionForm.getDeliveryStatusList());
        				
        				actionForm.saveArDVList(arDVList);
        				
        				
        				
        			}

			       
	       			
	       		}
        	
        	
        		
    			this.setFormProperties(actionForm);   
    			
    			
    			
    		 	  for(int x=0;x<user.getResCount();x++) {
    	        	  
    	        	  Responsibility res = user.getRes(x);
    	        	  
    	        	  System.out.println("res is -" + res.getRSName()	);
    	       
    	        	  if(res.equals("Transport")) {
    	        		  actionForm.setShowAddLinesButton(false);
        	  			  actionForm.setShowDeleteLinesButton(false);	
    	        	  }
    	        
    	              
    	          }
    			
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArDeliveryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {//from update to saveButton
            	
              if (request.getParameter("saveButton") != null) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
          
            }
            
            return(mapping.findForward("arDelivery"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArDeliveryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          
          e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

       }

    }

   
   private void setFormProperties(ArDeliveryForm actionForm) {
   	
   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
   		
   		
   		
   		
   			actionForm.setEnableFields(true);
   		
			actionForm.setShowSaveButton(true);
			actionForm.setShowAddLinesButton(true);
			actionForm.setShowDeleteLinesButton(true);	
   		
   		
   	}
   	
   }

}