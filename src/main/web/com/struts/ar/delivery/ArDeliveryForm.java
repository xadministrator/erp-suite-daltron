package com.struts.ar.delivery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.EJBCommon;

public class ArDeliveryForm extends ActionForm implements Serializable {
	
	private String soCode = null;
	private String soDocumentNumber = null;
	private String soReferenceNumber = null;
	private String cstCustomerCode = null;
	private String soShippingLine = null;
	private String soPort = null;

	private String txnStatus = null;
	private String userPermission = new String();
	private int rowSelected = 0;
	private String pageState = new String();

	
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();
	
	private boolean enableFields = false;

	private String backButton = null;
	private String closeButton = null;
	private boolean showSaveButton = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	
	private ArrayList arDVList = new ArrayList();
	private int arDVListSize = 0;
	private ArrayList selectedArDVList = new ArrayList();
	
	private String deliveryStatus = null;
    private ArrayList deliveryStatusList = new ArrayList();
	
    
    private String transactionStatus = null;
    private ArrayList transactionStatusList = new ArrayList();
    
    private String report = null;
    private String reportDvCode = null;
    private String reportSoCode = null;
    private String reportType = null;
	
	public String getSoCode() {
		return soCode;
	}
	
	public void setSoCode(String soCode) {
		this.soCode =soCode;
	}
	
	public String getSoDocumentNumber() {
		return soDocumentNumber;
	}
	
	public void setSoDocumentNumber(String soDocumentNumber) {
		this.soDocumentNumber =soDocumentNumber;
	}
	
	public String getSoReferenceNumber() {
		return soReferenceNumber;
	}
	
	public void setSoReferenceNumber(String soReferenceNumber) {
		this.soReferenceNumber =soReferenceNumber;
	}
	
	public String getCstCustomerCode() {
		return cstCustomerCode;
	}
	
	public void setCstCustomerCode(String cstCustomerCode) {
		this.cstCustomerCode =cstCustomerCode;
	}
	
	
	
	public String getSoShippingLine() {
		return soShippingLine;
	}
	
	public void setSoShippingLine(String soShippingLine) {
		this.soShippingLine =soShippingLine;
	}
	
	public String getSoPort() {
		return soPort;
	}
	
	public void setSoPort(String soPort) {
		this.soPort =soPort;
	}
	
	
	public ArDeliveryList getArDVByIndex(int index) {
		
		return((ArDeliveryList)arDVList.get(index));
		
	}
	
	public Object[] getArDVList() {
		
		return arDVList.toArray();
		
	}
	
	public int getArDVListSize() {
		
		arDVListSize = arDVList.size();
		return arDVList.size();
		
	}
	
	public void saveArDVList(Object newArDVList) {
		
		arDVList.add(newArDVList);
		
	}
	
	public void deleteArJAList(int rowSelected){
		
		arDVList.remove(rowSelected);
		
	}
	
	public void clearArJAList() {
		
		arDVList.clear();
		
	}
	
	public String getViewType() {

		return viewType ;

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}




	public ArrayList getViewTypeList() {

		return viewTypeList;

	}

	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public void setRowSelected(Object selectedArDVList, boolean isEdit) {
		
		this.rowSelected = arDVList.indexOf(selectedArDVList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	

	
	
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	
	
	public void setBackButton(String backButton) {
		
		this.backButton = backButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getShowAddLinesButton() {
		
		return showAddLinesButton;
		
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		
		this.showAddLinesButton = showAddLinesButton;
		
	}
	
	public boolean getShowDeleteLinesButton() {
		
		return showDeleteLinesButton;
		
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		
		this.showDeleteLinesButton = showDeleteLinesButton;
		
	}
	
	
	public void deleteArDVList(int rowSelected){
		
		arDVList.remove(rowSelected);
		
	}
	
	
	public void clearArDVList() {
		
		arDVList.clear();
		
	}
	
	public String getDeliveryStatus() {

   	  	return deliveryStatus;

	}

	public void setDeliveryStatus(String deliveryStatus) {

   	  	this.deliveryStatus = deliveryStatus;

	}
	
	
	public ArrayList getDeliveryStatusList() {
		return deliveryStatusList;
	}

	public void setDeliveryStatusList(String deliveryStatus) {
		deliveryStatusList.add(deliveryStatus);
	}

	public void clearDeliveryStatusList() {
		deliveryStatusList.clear();
	}

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}
	
	
	public String getReportDvCode() {

		return reportDvCode;

	}

	public void setReportDvCode(String reportDvCode) {

		this.reportDvCode = reportDvCode;

	}
	
	
	public String getReportSoCode() {

		return reportSoCode;

	}

	public void setReportSoCode(String reportSoCode) {

		this.reportSoCode = reportSoCode;

	}
	
	
	public String getReportType() {

		return reportType;

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}
	
	
	public String getTransactionStatus() {

   	  	return transactionStatus;

	}

	public void setTransactionStatus(String transactionStatus) {

   	  	this.transactionStatus = transactionStatus;

	}
	
	
	public ArrayList getTransactionStatusList() {
		return transactionStatusList;
	}

	public void setTransactionStatusList(String transactionStatus) {
		transactionStatusList.add(transactionStatus);
	}

	public void clearTransactionStatusList() {
		transactionStatusList.clear();
	}

	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		
		deliveryStatusList.clear();
		deliveryStatusList.add(Constants.GLOBAL_BLANK);
		deliveryStatusList.add("READY TO DELIVER");
		deliveryStatusList.add("DELIVERED");
		deliveryStatusList.add("CANCELLED");
		
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		
		backButton = null;
		closeButton = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveButton") != null) {
			
		
			
			Iterator i = arDVList.iterator();
			
			while(i.hasNext()) {
				
				ArDeliveryList dvList = (ArDeliveryList)i.next();
				
			
					
					System.out.println("validating");
					
					
				
					if(Common.validateRequired(dvList.getDvContainer())){
						
						
						 continue;
					}else {
						if(Common.validateRequired(dvList.getDvDeliveredTo())){
							
							
							  errors.add("deliveredToRequired",
			                            new ActionMessage("delivery.error.deliveredToRequired"));
							
						}
					}
					
					
				
					
				
			
				
				
				
				
				
			
				
			}
		
			
		}
		
		return errors;
		
	}
	
}