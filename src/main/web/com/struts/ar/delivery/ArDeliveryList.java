package com.struts.ar.delivery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import com.struts.util.Constants;

public class ArDeliveryList implements Serializable {
	
	private String dvCode = null;
	private String dvContainer = null;
	private String dvDeliveryNumber = null;
	private String dvBookingTime = null;
	private String dvTabsFeePullOut = null;
	private String dvReleasedDate = null;
	private String dvDeliveredDate = null;
	private String dvDeliveredTo = null;
	private String dvPlateNo = null;
	private String dvDriverName = null;
	private String dvEmptyReturnDate = null;
	private String dvEmptyReturnTo = null;
	private String dvTabsFeeReturn = null;
	private String dvStatus = null;
	private ArrayList dvStatusList = new ArrayList();
	private String dvRemarks = null;
	private String printButton = null;
	
	private boolean selectCheckBox = false;
	
	private ArDeliveryForm parentBean = null;
	
	public ArDeliveryList(ArDeliveryForm parentBean, String dvCode, String dvContainer, String dvDeliveryNumber, String dvBookingTime, String dvTabsFeePullOut, String dvReleasedDate, String dvDeliveredDate,
			String dvDeliveredTo, String dvPlateNo, String dvDriverName, String dvEmptyReturnDate, String dvEmptyReturnTo, String dvTabsFeeReturn , String dvStatus, String dvRemarks
			) {
		this.parentBean = parentBean;
		this.dvCode = dvCode;
		this.dvContainer = dvContainer;
		this.dvDeliveryNumber = dvDeliveryNumber;
		this.dvBookingTime = dvBookingTime;
		this.dvTabsFeePullOut = dvTabsFeePullOut;
		this.dvReleasedDate = dvReleasedDate;
		this.dvDeliveredDate = dvDeliveredDate;
		this.dvDeliveredTo = dvDeliveredTo;
		this.dvPlateNo = dvPlateNo;
		this.dvDriverName = dvDriverName;
		this.dvEmptyReturnDate = dvEmptyReturnDate;
		this.dvEmptyReturnTo = dvEmptyReturnTo;
		this.dvTabsFeeReturn = dvTabsFeeReturn;
		this.dvStatus = dvStatus;
		this.dvRemarks = dvRemarks;
		
	}

	public String getDvCode() {
		return dvCode;
	}

	public String getDvContainer() {
		return dvContainer;
	}
	
	public String getDvDeliveryNumber() {
		return dvDeliveryNumber;
	}

	public String getDvBookingTime() {
		return dvBookingTime;
	}

	public String getDvTabsFeePullOut() {
		return dvTabsFeePullOut;
	}

	public String getDvReleasedDate() {
		return dvReleasedDate;
	}

	public String getDvDeliveredDate() {
		return dvDeliveredDate;
	}

	public String getDvDeliveredTo() {
		return dvDeliveredTo;
	}

	public String getDvPlateNo() {
		return dvPlateNo;
	}

	public String getDvDriverName() {
		return dvDriverName;
	}

	public String getDvEmptyReturnDate() {
		return dvEmptyReturnDate;
	}

	public String getDvEmptyReturnTo() {
		return dvEmptyReturnTo;
	}

	public String getDvTabsFeeReturn() {
		return dvTabsFeeReturn;
	}

	public String getDvStatus() {
		return dvStatus;
	}

	public String getDvRemarks() {
		return dvRemarks;
	}

	public void setDvCode(String dvCode) {
		this.dvCode = dvCode;
	}

	public void setDvContainer(String dvContainer) {
		this.dvContainer = dvContainer;
	}

	public void setDvDeliveryNumber(String dvDeliveryNumber) {
		this.dvDeliveryNumber = dvDeliveryNumber;
	}
	
	public void setDvBookingTime(String dvBookingTime) {
		this.dvBookingTime = dvBookingTime;
	}

	public void setDvTabsFeePullOut(String dvTabsFeePullOut) {
		this.dvTabsFeePullOut = dvTabsFeePullOut;
	}

	public void setDvReleasedDate(String dvReleasedDate) {
		this.dvReleasedDate = dvReleasedDate;
	}

	public void setDvDeliveredDate(String dvDeliveredDate) {
		this.dvDeliveredDate = dvDeliveredDate;
	}

	public void setDvDeliveredTo(String dvDeliveredTo) {
		this.dvDeliveredTo = dvDeliveredTo;
	}

	public void setDvPlateNo(String dvPlateNo) {
		this.dvPlateNo = dvPlateNo;
	}

	public void setDvDriverName(String dvDriverName) {
		this.dvDriverName = dvDriverName;
	}

	public void setDvEmptyReturnDate(String dvEmptyReturnDate) {
		this.dvEmptyReturnDate = dvEmptyReturnDate;
	}

	public void setDvEmptyReturnTo(String dvEmptyReturnTo) {
		this.dvEmptyReturnTo = dvEmptyReturnTo;
	}

	public void setDvTabsFeeReturn(String dvTabsFeeReturn) {
		this.dvTabsFeeReturn = dvTabsFeeReturn;
	}

	public void setDvStatus(String dvStatus) {
		this.dvStatus = dvStatus;
	}

	public void setDvRemarks(String dvRemarks) {
		this.dvRemarks = dvRemarks;
	}
	
	
	public boolean getSelectCheckbox() {
		
		return selectCheckBox;
		
	}
	
	public void setSelectCheckbox(boolean selectCheckBox) {
		
		this.selectCheckBox = selectCheckBox;
		
	}
	
	
	public ArrayList getDvStatusList() {

		return dvStatusList;

	}

	public void setDvStatusList(ArrayList dvStatusList) {

		this.dvStatusList = dvStatusList;

	}
	
   public void setPrintButton(String printButton) {

	      parentBean.setRowSelected(this, false);

   }
}