package com.struts.ar.findcustomer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArFindCustomerController;
import com.ejb.txn.ArFindCustomerControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchCustomerDetails;
import com.util.ArModCustomerDetails;

public final class ArFindCustomerAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArFindCustomerAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArFindCustomerForm actionForm = (ArFindCustomerForm)form;
         
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
            frParam = Common.getUserPermission(user, Constants.AR_FIND_CUSTOMER_ID);
         	
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  if (request.getParameter("child") == null) {

                  	return mapping.findForward("arFindCustomer");
                  	
                  } else {
                  
                    return mapping.findForward("arFindCustomerChild");
                  
                  }
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize ArFindCustomerController EJB
*******************************************************/

         ArFindCustomerControllerHome homeFC = null;
         ArFindCustomerController ejbFC = null;

         try {

            homeFC = (ArFindCustomerControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArFindCustomerControllerEJB", ArFindCustomerControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArFindCustomerAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFC = homeFC.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArFindCustomerAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
/*******************************************************
   -- Ar FC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindCustomer");
	          	
	        } else {
	          
	            return mapping.findForward("arFindCustomerChild");
	          
	        }
	     
/*******************************************************
   -- Ar FC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindCustomer");
	          	
	        } else {
	          
	            return mapping.findForward("arFindCustomerChild");
	          
	        }                         

/*******************************************************
    -- Ar FC First Action --
*******************************************************/ 

	     } else if (request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	        
/*******************************************************
   -- Ar FC Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ar FC Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Ar FC Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null || request.getParameter("findRejected") != null)  {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("name", actionForm.getName());
	        		
	        	}	  
	        	
	        	if (!Common.validateRequired(actionForm.getEmployeeId())) {
	        		
	        		criteria.put("employeeId", actionForm.getEmployeeId());
	        		
	        	}
	        	
	        	        	
	        	if (!Common.validateRequired(actionForm.getEmail())) {
	        		
	        		criteria.put("email", actionForm.getEmail());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerType())) {
	        		
	        		criteria.put("customerType", actionForm.getCustomerType());
	        		
	        	}   

	        	if (!Common.validateRequired(actionForm.getCustomerClass())) {
	        		
	        		criteria.put("customerClass", actionForm.getCustomerClass());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerBatch())) {
	        		
	        		criteria.put("customerBatch", actionForm.getCustomerBatch());
	        		
	        	}
	        	 
	        	if (actionForm.getEnable()) {
	        			        	   		        		
	        		criteria.put("enable", new Byte((byte)1));
	        	
                        }
	            
	        	if (actionForm.getDisable()) {
	        			            
	        		criteria.put("disable", new Byte((byte)1));
 
                         }
                        
                        if(request.getParameter("enablePayroll")!=null){
                            criteria.put("enablePayroll", new Byte((byte)1));
                        }else{
                            criteria.put("enablePayroll", new Byte((byte)0));
                        }
                        
                        
                        
                        
                
                if  (!actionForm.getEnable() && !actionForm.getDisable()) {
                	
                	criteria.put("enable", new Byte((byte)1));
                	criteria.put("disable", new Byte((byte)1));
                	
                }  
                
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	} else if(request.getParameter("findRejected") != null){
				
				HashMap criteria = new HashMap();
				

				criteria.put("approvalStatus", "REJECTED");
				actionForm.setApprovalStatus("REJECTED");
			
        	    actionForm.setLineCount(0);
        	    actionForm.setCriteria(criteria);
        	    actionForm.setOrderBy("CUSTOMER CODE");
	     	}
            
            // get all AdFindCustomerBranchLists
        	
        	ArrayList branchList = new ArrayList();
        	
        	if(request.getParameter("child") == null) {
        		
        		for(int i=0; i<actionForm.getBcstListSize(); i++) {
            		
            		ArFindCustomerBranchList brList = (ArFindCustomerBranchList)actionForm.getBcstByIndex(i);
            		
            		if(brList.getBranchCheckbox() == true) {
            			
            			AdModBranchCustomerDetails mdetails = new AdModBranchCustomerDetails();
            			mdetails.setBcstBranchCode(brList.getBranchCode());
            			branchList.add(mdetails);
            			
            		}
            		
            	}
        		
        	} else {
        		
        		AdModBranchCustomerDetails mdetails = new AdModBranchCustomerDetails();
    			
    			mdetails.setBcstBranchCode(user.getCurrentBranch().getBrBranchCode());
    			branchList.add(mdetails);
        		
        	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFC.getArCstSizeByCriteria(actionForm.getCriteria(), branchList, user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }

            try {
            	
            	actionForm.clearArFCList();
            	
            	ArrayList list = ejbFC.getArCstByCriteria(actionForm.getCriteria(), branchList,
            			new Integer(actionForm.getLineCount()), 
						new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), user.getCmpCode());
            	
            	// check if prev should be disabled
            	if (actionForm.getLineCount() == 0) {
            		
            		actionForm.setDisablePreviousButton(true);
            		actionForm.setDisableFirstButton(true);
            		
            	} else {
            		
            		actionForm.setDisablePreviousButton(false);
            		actionForm.setDisableFirstButton(false);
            		
            	}
            	
            	// check if next should be disabled
            	if (list.size() <= Constants.GLOBAL_MAX_LINES) {
            		
            		actionForm.setDisableNextButton(true);
            		actionForm.setDisableLastButton(true);
            		
            	} else {
            		
            		actionForm.setDisableNextButton(false);
            		actionForm.setDisableLastButton(false);
            		
            		//remove last record
            		list.remove(list.size() - 1);
            		
            	}
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModCustomerDetails mdetails = (ArModCustomerDetails)i.next();
					
            		ArFindCustomerList arFCList = new ArFindCustomerList(actionForm,
            				mdetails.getCstCode(),
							mdetails.getCstCustomerCode(),
							mdetails.getCstName(),
							mdetails.getCstEmployeeID(),
							mdetails.getCstAddress(),
							mdetails.getCstCcName(),
							mdetails.getCstSlpName(),
							mdetails.getCstTin(),
							mdetails.getCstEmail(),
							mdetails.getCstCtName(),
							mdetails.getCstCcName(),
							mdetails.getCstPytName(),
							Common.convertByteToBoolean(mdetails.getCstEnable()));
            		
            		actionForm.saveArFCList(arFCList);
            		
            	}
            	
            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findCustomer.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindCustomerAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               if (request.getParameter("child") == null) {

		          	return mapping.findForward("arFindCustomer");
		          	
		        } else {
		          
		            return mapping.findForward("arFindCustomerChild");
		          
		        }

            }
                        
            //actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindCustomer");
	          	
	        } else {
	          
	            return mapping.findForward("arFindCustomerChild");
	          
	        }

/*******************************************************
   -- Ar FC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar FC Open Action --
*******************************************************/

         } else if (request.getParameter("arFCList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             ArFindCustomerList arFCList =
                actionForm.getArFCByIndex(actionForm.getRowSelected());
          
  	         String path = "/arCustomerEntry.do?forward=1" +
			     "&cstCode=" + arFCList.getCstCode();
		      
			   return(new ActionForward(path));

/*******************************************************
   -- Ar FC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearArFCList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	
            	actionForm.clearCustomerTypeList();
            	
            	list = ejbFC.getArCtAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCustomerTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			actionForm.setCustomerTypeList((String)i.next());
            			
            		}
            		
            	}            	
            	
            	actionForm.clearCustomerClassList();
            	
            	list = ejbFC.getArCcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCustomerClassList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			actionForm.setCustomerClassList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearCustomerBatchList();           	
         		
         		list = ejbFC.getAdLvCustomerBatchAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setCustomerBatchList((String)i.next());
         				
         			}
         			
         		} 
            	
            	actionForm.clearBcstList();	        
            	
            	list = ejbFC.getAdBrResAll(user.getCurrentResCode(), user.getCmpCode()); 
            	
            	i = list.iterator();
            	
            	while(i.hasNext()) {
            		
            		AdBranchDetails details = (AdBranchDetails)i.next();
            		
            		ArFindCustomerBranchList arBcstList = new ArFindCustomerBranchList(actionForm,
            				details.getBrBranchCode(), details.getBrName());
            		
            		actionForm.saveBcstList(arBcstList);
            		
            	}
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindCustomerAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            
	        if (actionForm.getTableType() == null) {
	        	actionForm.setCustomerCode(null);
	        	actionForm.setName(null);
	        	actionForm.setEmployeeId(null);
	        	actionForm.setCustomerType(Constants.GLOBAL_BLANK);
	        	actionForm.setCustomerClass(Constants.GLOBAL_BLANK);
	        	actionForm.setEmail(null);
	        	actionForm.setEnable(false);
	        	actionForm.setDisable(false);
	        	actionForm.setOrderBy(Constants.AR_FC_ORDER_BY_CUSTOMER_CODE);
	        
	        	actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	            
	            HashMap criteria = new HashMap();
	            criteria.put("enable", new Byte((byte)0));
            	criteria.put("disable", new Byte((byte)0));
	            
            	actionForm.setCriteria(criteria);
            	
	        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            } else {
            	
            	// get all InvFindCustomerBranchLists
            	
            	ArrayList branchList = new ArrayList();
            	
            	for(int j =0; j < actionForm.getBcstListSize(); j++) {
            		
            		ArFindCustomerBranchList brList = (ArFindCustomerBranchList)actionForm.getBcstByIndex(j);
            		
            		if(brList.getBranchCheckbox() == true) {
            			
            			AdModBranchCustomerDetails mdetails = new AdModBranchCustomerDetails();
            			mdetails.setBcstBranchCode(brList.getBranchCode());
            			
            			branchList.add(mdetails);
            			
            		}
            		
            	}
            	
            	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("enable")) {
            		
            		Byte b = (Byte)c.get("enable");
            		
            		actionForm.setEnable(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	if(c.containsKey("disable")) {
            		
            		Byte b = (Byte)c.get("disable");
            		
            		actionForm.setDisable(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	try {
                	
                	actionForm.clearArFCList();
                	
                	branchList = new ArrayList();
                	
                	ArrayList newList = ejbFC.getArCstByCriteria(actionForm.getCriteria(), branchList,
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), user.getCmpCode());
                	
                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);	
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);	
    	           	  
    	           	  //remove last record
    	           	  newList.remove(newList.size() - 1);
    	           	
    	           }
    	           
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
	            		ArModCustomerDetails mdetails = (ArModCustomerDetails)j.next();

                		ArFindCustomerList arFCList = new ArFindCustomerList(actionForm,
                		    mdetails.getCstCode(),
                		    mdetails.getCstCustomerCode(),
                		    mdetails.getCstName(),
                		    mdetails.getCstEmployeeID(),
                		    mdetails.getCstAddress(),
                		    mdetails.getCstArea(),
                		    mdetails.getCstSlpName(),
                		    mdetails.getCstTin(),
                		    mdetails.getCstEmail(),
                		    mdetails.getCstCtName(),
                		    mdetails.getCstCcName(),
                		    mdetails.getCstPytName(),
                		    Common.convertByteToBoolean(mdetails.getCstEnable()));
                		    
                		actionForm.saveArFCList(arFCList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findCustomer.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in ArFindCustomerAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }

            	
            }
                        
            if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindCustomer");
	          	
	        } else {
	        	                    		
	        	try {
	        		
	        		actionForm.reset(mapping, request);
	        		
	        		actionForm.setLineCount(0);
	        		
	        		HashMap criteria = new HashMap();
	        		criteria.put(new String("enable"), new Byte((byte)1));
	        		actionForm.setCriteria(criteria);
	        		
	        		actionForm.setEnable(true);
	        		
	        		actionForm.clearArFCList();
	        		
	        		ArrayList branchList = new ArrayList();
	        		
        			AdModBranchCustomerDetails mBrDetails = new AdModBranchCustomerDetails();
        			
        			mBrDetails.setBcstBranchCode(user.getCurrentBranch().getBrBranchCode());
        			branchList.add(mBrDetails);
	        			
	        		ArrayList newList = ejbFC.getArCstByCriteria(actionForm.getCriteria(), branchList,
	        				new Integer(actionForm.getLineCount()), 
							new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), user.getCmpCode());
	        		
	        		actionForm.setDisablePreviousButton(true);
	        		actionForm.setDisableFirstButton(true);
	        		
	        		// check if next should be disabled
	        		if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
	        			
	        			actionForm.setDisableNextButton(true);
	        			actionForm.setDisableLastButton(true);
	        			
	        		} else {
	        			
	        			actionForm.setDisableNextButton(false);
	        			actionForm.setDisableLastButton(false);	
	        			
	        			//remove last record
	        			newList.remove(newList.size() - 1);
	        			
	        		}
	        		
	        		i = newList.iterator();
	        		
	        		while (i.hasNext()) {
	        			
	        			ArModCustomerDetails mdetails = (ArModCustomerDetails)i.next();
	        			
	        			ArFindCustomerList arFCList = new ArFindCustomerList(actionForm,
	        					mdetails.getCstCode(),
								mdetails.getCstCustomerCode(),
								mdetails.getCstName(),
								mdetails.getCstEmployeeID(),
								mdetails.getCstAddress(),
								mdetails.getCstArea(),
								mdetails.getCstSlpName(),
								mdetails.getCstTin(),
								mdetails.getCstEmail(),
								mdetails.getCstCtName(),
								mdetails.getCstCcName(),
								mdetails.getCstPytName(),
								Common.convertByteToBoolean(mdetails.getCstEnable()));
	        			
	        			actionForm.saveArFCList(arFCList);
	        			
	        		}
	        		
	        	} catch (GlobalNoRecordFoundException ex) {
	               
	               // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	
	            } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in ArFindCustomerAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
	            
	            actionForm.setSelectedCustomerCode(request.getParameter("selectedCustomerCode"));
                actionForm.setSelectedCustomerName(request.getParameter("selectedCustomerName"));
                actionForm.setSelectedCustomerEntered(request.getParameter("selectedCustomerEntered"));
	          
	            return mapping.findForward("arFindCustomerChild");
	          
	        }

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

      	e.printStackTrace();  
      	if (log.isInfoEnabled()) {

             log.info("Exception caught in ArFindCustomerAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          
          return mapping.findForward("cmnErrorPage");

       }

    }
}