package com.struts.ar.findcustomer;

import java.io.Serializable;


/**
 * 
 * @author Farrah S. Garing
 * Created: 10/27/2005 5:57 PM
 * 
 */
public class ArFindCustomerBranchList implements Serializable {
	
	
	private String branchCode = null;
	private String branchName = null;
	
	private boolean branchCheckbox = false;
	
	private ArFindCustomerForm parentBean;
	
	public ArFindCustomerBranchList(ArFindCustomerForm parentBean, String branchCode,
			String branchName) {
		
		this.parentBean = parentBean;
		this.branchCode = branchCode;
		this.branchName = branchName;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}

	public String getBranchCode() {
		
		return this.branchCode;
		
	}
	
	public void setBranchCode(String branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public String getBranchName() {
		
		return this.branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}	
	
	
}//ArFindCustomerBranchList

