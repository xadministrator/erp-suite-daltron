package com.struts.ar.findcustomer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class ArFindCustomerForm extends ActionForm implements Serializable {

   private String customerCode = null;
   private String name = null;
   private String employeeId = null;
   private String address = null;
   private String area = null;
   private String salesPerson = null;
   private String customerType = null;
   private ArrayList customerTypeList = new ArrayList();
   private String customerClass = null;
   private ArrayList customerClassList = new ArrayList();
   private String customerBatch = null;
   private ArrayList customerBatchList = new ArrayList();
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   
   private boolean enable = false;
   private boolean disable = false;
   private String email = null;   
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String pageState = new String();
   private ArrayList arFCList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private String selectedCustomerCode = null;
   private String selectedCustomerName = null;
   private String selectedCustomerEntered = null;
   private String selectedLocation = null;
   private String sourceType = null;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();
   
   private ArrayList arBcstList = new ArrayList();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ArFindCustomerList getArFCByIndex(int index) {

      return((ArFindCustomerList)arFCList.get(index));

   }

   public Object[] getArFCList() {

      return arFCList.toArray();

   }

   public int getArFCListSize() {

      return arFCList.size();

   }

   public void saveArFCList(Object newArFCList) {

      arFCList.add(newArFCList);

   }

   public void clearArFCList() {
   	
      arFCList.clear();
      
   }

   public void setRowSelected(Object selectedArFCList, boolean isEdit) {

      this.rowSelected = arFCList.indexOf(selectedArFCList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public String getName() {
   	
      return name;
   
   }

   public void setName(String name) {
   
      this.name = name;
   
   }
   
   public String getEmployeeId() {
	   	
      return employeeId;
   
   }

   public void setEmployeeId(String employeeId) {
   
      this.employeeId = employeeId;
   
   }
   
   public String getAddress() {
	   	
	      return address;
	   
	   }

   public void setAddress(String address) {
   
      this.address = address;
   
   }
   
   public String getArea() {
	   	
	      return area;
	   
	   }

	public void setArea(String area) {
	
	   this.area = area;
	
	}
	
	public String getSalesPerson() {
	   	
	      return salesPerson;
	   
	   }

	public void setSalesPerson(String salesPerson) {
	
	   this.salesPerson = salesPerson;
	
	}
   
   public String getCustomerType() {

      return customerType;

   }

   public void setCustomerType(String customerType) {

      this.customerType = customerType;

   }                         

   public ArrayList getCustomerTypeList() {

      return customerTypeList;

   }

   public void setCustomerTypeList(String customerType) {

      customerTypeList.add(customerType);

   }
   
   public void clearCustomerTypeList() {

      customerTypeList.clear();
      customerTypeList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getCustomerClass() {

      return customerClass;

   }

   public void setCustomerClass(String customerClass) {

      this.customerClass = customerClass;

   }                         

   public ArrayList getCustomerClassList() {

      return customerClassList;

   }

   public void setCustomerClassList(String customerClass) {

      customerClassList.add(customerClass);

   }
   
   public void clearCustomerClassList() {

      customerClassList.clear();
      customerClassList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getCustomerBatch() {
	   	
	      return customerBatch;
	      
	   }

	   public void setCustomerBatch(String customerBatch) {
	   	
	      this.customerBatch = customerBatch;
	      
	   }

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }
	   
	   

public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }
   
   public String getEmail() {
   	
   	  return email;
   	  
   }
   
   public void setEmail(String email) {
   	
   	  this.email = email;
   	  
   }
 
   public boolean getEnable() {
   	
   	  return enable;
   	  
   }
   
   public void setEnable(boolean enable) {
   	
   	  this.enable = enable;
   	  
   }
   
   public boolean getDisable() {
   	
   	  return disable;
   	  
   }
   
   public void setDisable(boolean disable) {
   	
   	  this.disable = disable;
   	  
   } 
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public String getSelectedCustomerCode() {
   	
   	  return selectedCustomerCode;
   	
   }
   
   public void setSelectedCustomerCode(String selectedCustomerCode) {
   	
   	  this.selectedCustomerCode = selectedCustomerCode;
   	
   }
   
   public String getSelectedCustomerName() {
   	
   	  return selectedCustomerName;
   	
   }
   
   public void setSelectedCustomerName(String selectedCustomerName) {
   	
   	  this.selectedCustomerName = selectedCustomerName;
   	
   }
   
   public String getSelectedCustomerEntered() {
   	
   	  return selectedCustomerEntered;
   	
   }
   
   public void setSelectedCustomerEntered(String selectedCustomerEntered) {
   	
   	  this.selectedCustomerEntered = selectedCustomerEntered;
   	
   }
   
   public String getSelectedLocation() {
   	
   	  return selectedLocation;
   	
   }
   
   public void setSelectedLocation(String selectedLocation) {
   	
   	  this.selectedLocation = selectedLocation;
   	
   }
   
   
    public String getSourceType() {
   	
   	  return sourceType;
   	
   }
   
   public void setSourceType(String sourceType) {
   	
   	  this.sourceType = sourceType;
   	
   }
   
	public Object[] getBcstList(){
		
		return arBcstList.toArray();
		
	}
	
	public ArFindCustomerBranchList getBcstByIndex(int index) {
		
		return ((ArFindCustomerBranchList)arBcstList.get(index));
		
	}
	
	public int getBcstListSize(){
		
		return(arBcstList.size());
		
	}
	
	public void saveBcstList(Object newArBcstList){
		
		arBcstList.add(newArBcstList);   	  
		
	}
	
	public void clearBcstList(){
		
		arBcstList.clear();
		
	}
	
	public void setBcstList(ArrayList arBcstList) {
		
		this.arBcstList = arBcstList;
		
	}   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  enable = false;
      disable = false;
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AR_FC_ORDER_BY_CUSTOMER_CODE);
	      orderByList.add(Constants.AR_FC_ORDER_BY_CUSTOMER_NAME);
	  
	  }     
	  
      
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      
      showDetailsButton = null;
	  hideDetailsButton = null;
	  
 	  for (int i=0; i<arBcstList.size(); i++) {
 	  	
 	  	ArFindCustomerBranchList actionList = (ArFindCustomerBranchList)arBcstList.get(i);
 	  	actionList.setBranchCheckbox(false);
 	  	
 	  } 

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

      }
      
      return errors;

   }
}