package com.struts.ar.findcustomer;

import java.io.Serializable;

public class ArFindCustomerList implements Serializable {

   private Integer cstCode = null;
   private String customerCode = null;
   private String name = null;
   private String employeeId = null;
   private String address = null;
   private String area = null;
   private String salesPerson = null;
   private String tinNumber = null;
   private String email = null;
   private String customerType = null;
   private String customerClass = null;
   private String paymentTerm = null;
   private boolean enable = false;

   private String openButton = null;
       
   private ArFindCustomerForm parentBean;
    
   public ArFindCustomerList(ArFindCustomerForm parentBean,
	  Integer cstCode,  
	  String customerCode,  
	  String name,
	  String employeeId,
	  String address,
	  String area,
	  String salesPerson,
	  String tinNumber,
	  String email,  
	  String customerType,  
	  String customerClass,
	  String paymentTerm,  
	  boolean enable) {

      this.parentBean = parentBean;
      this.cstCode = cstCode;
      this.customerCode = customerCode;
      this.name = name;
      this.employeeId = employeeId;
      this.address = address;
      this.area = area;
      this.salesPerson = salesPerson;
      this.tinNumber = tinNumber;
      this.email = email;
      this.customerType = customerType;
      this.customerClass = customerClass;
      this.paymentTerm = paymentTerm;
      this.enable = enable;
   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }
   
   public Integer getCstCode() {
   	
   	  return cstCode;
   	  
   }

   public String getCustomerCode() {

      return customerCode;

   }
   
   public String getName() {

      return name;

   }
   
   public String getEmployeeId() {

      return employeeId;

   }
   
   
   
   public String getAddress() {

	      return address;

	}

   public String getArea() {

	      return area;

	}
   public String getSalesPerson() {

	      return salesPerson;

	}
      
   public String getTinNumber() {
   	
   	  return tinNumber;
   	 
   }

   public String getEmail() {
   	
   	  return email;
   	  
   }
   
   public String getCustomerType() {
   	
   	  return customerType;
   	  
   }
   
   public String getCustomerClass() {
   	
   	  return customerClass;
   	  
   }
   
   public String getPaymentTerm() {
   	
   	  return paymentTerm;
   	  
   }   
   
   public boolean getEnable() {
   	
   	  return enable;
   	  
   }
      
}