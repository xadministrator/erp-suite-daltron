package com.struts.ar.finddelivery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArFindDeliveryController;
import com.ejb.txn.ArFindDeliveryControllerHome;
import com.ejb.txn.ArFindSalesOrderController;
import com.ejb.txn.ArFindSalesOrderControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModDeliveryDetails;
import com.util.ArModSalesOrderDetails;

public class ArFindDeliveryAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		HttpSession session = request.getSession();

		try {

/*******************************************************
 	Check if user has a session
*******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("ArFindDeliveryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}

			} else {

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			ArFindDeliveryForm actionForm = (ArFindDeliveryForm)form;
			String frParam = null;
			boolean isChild = false;

			if (request.getParameter("child") == null) {

		         	frParam = Common.getUserPermission(user, Constants.AR_FIND_DELIVERY_ID);

		    } else {

		         	frParam = Constants.FULL_ACCESS;
		         	isChild = true;
		    }



			if (frParam != null) {

				if (frParam.trim().equals(Constants.FULL_ACCESS)) {

					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));
						return mapping.findForward("arFindDelivery");

					}

				}

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}

/*******************************************************
 	Initialize ArFindDeliveryController EJB
*******************************************************/

		
			ArFindDeliveryControllerHome homeFDV = null;
			ArFindDeliveryController ejbFDV = null;

			try {

				homeFDV = (ArFindDeliveryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArFindDeliveryControllerEJB", ArFindDeliveryControllerHome.class);

			} catch (NamingException e) {

				if (log.isInfoEnabled()) {

					log.info("NamingException caught in ArFindDeliveryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}

				return mapping.findForward("cmnErrorPage");

			}

			try {

				ejbFDV = homeFDV.create();

			} catch (CreateException e) {

				if (log.isInfoEnabled()) {

					log.info("CreateException caught in ArFindDeliveryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}
				return mapping.findForward("cmnErrorPage");

			}

			ActionErrors errors = new ActionErrors();

/*******************************************************
   Call ArFindDeliveryController EJB
   getAdPrfArUseCustomerPulldown
*******************************************************/

			boolean useCustomerPulldown = true;

			try {

				useCustomerPulldown = Common.convertByteToBoolean(ejbFDV.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
				actionForm.setUseCustomerPulldown(useCustomerPulldown);

			} catch(EJBException ex) {

		        if (log.isInfoEnabled()) {

		           log.info("EJBException caught in ArFindDeliveryAction.execute(): " + ex.getMessage() +
		              " session: " + session.getId());
		        }

		        return(mapping.findForward("cmnErrorPage"));

		     }

/*******************************************************
	-- Ar FDV Go Action --
*******************************************************/

			if (request.getParameter("goButton") != null) {

				// create criteria

				if (request.getParameter("goButton") != null) {

					HashMap criteria = new HashMap();

					
					

					if (!Common.validateRequired(actionForm.getReferenceNumber())) {

						criteria.put("referenceNumber", actionForm.getReferenceNumber());

					}

					
					if (!Common.validateRequired(actionForm.getDeliveryNumberFrom())) {

						criteria.put("deliveryNumberFrom", actionForm.getDeliveryNumberFrom());

					}

					if (!Common.validateRequired(actionForm.getDeliveryNumberTo())) {

						criteria.put("deliveryNumberTo", actionForm.getDeliveryNumberTo());

					}
					
				
					
					
					
					if (!Common.validateRequired(actionForm.getCustomerCode())) {

	        		   criteria.put("customerCode", actionForm.getCustomerCode());

	        	    }
					
					if (!Common.validateRequired(actionForm.getContainer())) {

	        		   criteria.put("container", actionForm.getContainer());

	        	    }


					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {

						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());

					}

					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {

						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());

					}
					
					
					
					if (!Common.validateRequired(actionForm.getTransactionStatus())) {

						criteria.put("transactionStatus", actionForm.getTransactionStatus());

					}

					
					
					
					criteria.put("salesOrderVoid",Common.convertBooleanToByte(actionForm.getSalesOrderVoid()));
					
					
					if (!Common.validateRequired(actionForm.getPosted())) {

						criteria.put("posted", actionForm.getPosted());

					}

					
	           

					// save criteria

					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);

				}

				

				try {

					actionForm.clearArFDVList();

					ArrayList list = ejbFDV.getArDvByCriteria(actionForm.getCriteria(), isChild,
							new Integer(actionForm.getLineCount()),
							new Integer(Constants.GLOBAL_MAX_LINES + 1),
							actionForm.getOrderBy(),
							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					
				

					Iterator i = list.iterator();

					while (i.hasNext()) {

						ArModDeliveryDetails mdetails = (ArModDeliveryDetails)i.next();

						ArFindDeliveryList apFDVList = new ArFindDeliveryList(actionForm,
								mdetails.getDvCode(),
								mdetails.getSoCode(),
								mdetails.getCstName(),
								mdetails.getSoDocumentNumber(),
								mdetails.getSoReferenceNumber(),
								mdetails.getDvContainer(),
								mdetails.getDvDeliveryNumber(),
								mdetails.getDvStatus()
								);

						actionForm.saveArFDVList(apFDVList);

					}

				} catch (GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findDelivery.error.noRecordFound"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ApFindDeliveryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

					if (request.getParameter("child") == null) {

					    return(mapping.findForward("arFindDelivery"));

					} else {

					    return(mapping.findForward("arFindDeliveryChild"));

					}

				}

				actionForm.reset(mapping, request);

				if (actionForm.getTableType() == null) {

					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

				}

				if (request.getParameter("child") == null) {

				    return(mapping.findForward("arFindDelivery"));

				} else {

				    return(mapping.findForward("arFindDeliveryChild"));

				}

/*******************************************************
	-- Ar FDV Close Action --
*******************************************************/

			} else if (request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

/*******************************************************
	-- Ar FDV Open Action --
*******************************************************/

			} else if (request.getParameter("arFDVList[" +
					actionForm.getRowSelected() + "].openButton") != null) {

				ArFindDeliveryList arFDVList =
					actionForm.getArFDVByIndex(actionForm.getRowSelected());
				System.out.println("sdd open to salesOrder");
				String path = null;

				path = "/arDelivery.do?forward=1" +
				"&salesOrderCode=" + arFDVList.getSalesOrderCode();

				return(new ActionForward(path));

			}

/*******************************************************
	-- Ar FDV Load Action --
*******************************************************/

			if (frParam != null) {
				actionForm.clearArFDVList();
				 System.out.println("pasok 1");
	            ArrayList list = null;
	            Iterator i = null;

	            try {

	            	if(actionForm.getUseCustomerPulldown()) {

	            		actionForm.clearCustomerCodeList();

	            		list = ejbFDV.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	            		if (list == null || list.size() == 0) {

	            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

	            		} else {

	            			i = list.iterator();

	            			while (i.hasNext()) {

	            				actionForm.setCustomerCodeList((String)i.next());

	            			}

	            		}

	            	}

	            

            		
            		actionForm.clearTransactionStatusList();
            		actionForm.setTransactionStatusList(Constants.GLOBAL_BLANK);
	            	list = ejbFDV.getAdLvTransactionStatusAll(user.getCmpCode());

	            
	            	
	            	i = list.iterator();

            		while (i.hasNext()) {
            			 System.out.println("pasok trans add");
            		    actionForm.setTransactionStatusList((String)i.next());

            		}

            	
            		
            		
            		
            		


	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArFindDeliveryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }
	            
	            
	        //	String salesOrderCode = request.getParameter("salesOrderCode")!=null?request.getParameter("salesOrderCode"):actionForm.getSoCode(); 
    			
    			
    		//	System.out.println("soCode" + request.getParameter("salesOrderCode"));
    			
			//	ArModSalesOrderDetails soDetails = ejbDV.getArSalesOrderByCode(Integer.parseInt(salesOrderCode));
				
				

				if (request.getParameter("goButton") != null) {

					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

				}


			//	if (actionForm.getTableType() == null) {

				
					
					actionForm.setCustomerCode(Constants.GLOBAL_BLANK);
					actionForm.setSalesOrderVoid(false);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setReferenceNumber(null);

					actionForm.setPosted(Constants.GLOBAL_NO);
					actionForm.setOrderBy("DOCUMENT NUMBER");

					actionForm.setLineCount(0);
					 System.out.println("pasok reset 1");
					actionForm.reset(mapping, request);

					HashMap criteria = new HashMap();
				
				//	criteria.put("customerCode", actionForm.getCustomerCode());
					criteria.put("posted", "");
					criteria.put("salesOrderVoid", new Byte((byte)0));
					criteria.put("transactionStatus", actionForm.getTransactionStatus());
					actionForm.setCriteria(criteria);

					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED); 

			//	} else {
					 System.out.println("pasok not reset");
					HashMap c = actionForm.getCriteria();

	            	if(c.containsKey("salesOrderVoid")) {

	            		Byte b = (Byte)c.get("salesOrderVoid");

	            		actionForm.setSalesOrderVoid(Common.convertByteToBoolean(b.byteValue()));

	            	}

					try {

						actionForm.clearArFDVList();
						System.out.println("pasok search");
						ArrayList newList = ejbFDV.getArDvByCriteria(actionForm.getCriteria(), isChild,
									new Integer(actionForm.getLineCount()),
									new Integer(Constants.GLOBAL_MAX_LINES + 1),
									actionForm.getOrderBy(),
									new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						

						Iterator j = newList.iterator();
						while (j.hasNext()) {

							ArModDeliveryDetails mdetails = (ArModDeliveryDetails)j.next();

							ArFindDeliveryList arFDVList = new ArFindDeliveryList(actionForm,
									mdetails.getDvCode(),
									mdetails.getSoCode(),
									mdetails.getCstName(),
									mdetails.getSoDocumentNumber(),
									mdetails.getSoReferenceNumber(),
									mdetails.getDvContainer(),
									mdetails.getDvDeliveryNumber(),
									mdetails.getDvStatus());
							 
							actionForm.saveArFDVList(arFDVList);

						}

					} catch (GlobalNoRecordFoundException ex) {

					
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findSalesOrder.error.noRecordFound"));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArFindDeliveryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}



			//	}

			

				if (request.getParameter("child") == null) {

					return(mapping.findForward("arFindDelivery"));

		        } else {

					try {

		        		actionForm.setLineCount(0);

		        	//	HashMap criteria = new HashMap();


		            	criteria.put(new String("salesOrderVoid"), new Byte((byte)0));

		            	//if(request.getParameter("cmFind")==null) {
		            //		criteria.put(new String("posted"), "YES");
		           // 	}

		        		actionForm.setCriteria(criteria);

						actionForm.clearArFDVList();
		
						ArrayList newList = ejbFDV.getArDvByCriteria(actionForm.getCriteria(), isChild,
								new Integer(actionForm.getLineCount()),
								new Integer(Constants.GLOBAL_MAX_LINES + 1),
								actionForm.getOrderBy(),
								new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						// check if prev should be disabled
						
						System.out.println("pasok 1");
								

						i = newList.iterator();

						while (i.hasNext()) {

							ArModDeliveryDetails mdetails = (ArModDeliveryDetails)i.next();

							ArFindDeliveryList arFDVList = new ArFindDeliveryList(actionForm,
									mdetails.getDvCode(),
									mdetails.getSoCode(),
									mdetails.getCstName(),
									mdetails.getSoDocumentNumber(),
									mdetails.getSoReferenceNumber(),
									mdetails.getDvContainer(),
									mdetails.getDvDeliveryNumber(),
									mdetails.getDvStatus());

							actionForm.saveArFDVList(arFDVList);

						}

					} catch (GlobalNoRecordFoundException ex) {

						

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArFindDeliveryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}

					actionForm.setSelectedDvNumber(request.getParameter("selectedDvNumber"));
	        		return mapping.findForward("arFindDeliveryChild");

		        }

			} else {  

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}

	} catch(Exception e) {

/*******************************************************
 	System Failed: Forward to error page
*******************************************************/

			if (log.isInfoEnabled()) {

				log.info("Exception caught in ApFindDeliveryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}

			return mapping.findForward("cmnErrorPage");

		}

	}

}
