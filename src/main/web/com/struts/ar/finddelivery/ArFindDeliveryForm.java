package com.struts.ar.finddelivery;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArFindDeliveryForm extends ActionForm implements java.io.Serializable {

	
	private String deliveryNumberFrom = null;
	private String deliveryNumberTo = null;
    private String customerCode = null;
	private ArrayList customerCodeList = new ArrayList();
	private boolean salesOrderVoid = false;

	private String container = null;
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String referenceNumber = null;

	private String transactionStatus = null;
	
	private ArrayList transactionStatusList = new ArrayList();

    private String posted = null;
    private ArrayList postedList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();

	private String tableType = null;


	private String pageState = new String();
	private ArrayList arFDVList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();


	
	private boolean useCustomerPulldown = true;


	private String selectedDvNumber = null;

	private int lineCount = 0;

	private HashMap criteria = new HashMap();

	

	
	
	public int getRowSelected() {

		return rowSelected;

	}

	public int getLineCount() {

		return lineCount;

	}

	public void setLineCount(int lineCount) {

		this.lineCount = lineCount;

	}

	public ArFindDeliveryList getArFDVByIndex(int index) {

		return((ArFindDeliveryList)arFDVList.get(index));

	}

	public Object[] getArFDVList() {

		return arFDVList.toArray();

	}

	public int getArFDVListSize() {

		return arFDVList.size();

	}

	public void saveArFDVList(Object newArFDVList) {

		arFDVList.add(newArFDVList);

	}

	public void clearArFDVList() {

		arFDVList.clear();

	}

	public void setRowSelected(Object selectedArFDVList, boolean isEdit) {

		this.rowSelected = arFDVList.indexOf(selectedArFDVList);

		if (isEdit) {

			this.pageState = Constants.PAGE_STATE_EDIT;

		}

	}

	
	public String getContainer() {
		return container;
	}
	
	public void setContainer(String container) {
		this.container = container;
	}
	
	
	public void setPageState(String pageState) {

		this.pageState = pageState;

	}

	public String getPageState() {

		return pageState;

	}

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}

	public void setTxnStatus(String txnStatus) {

		this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

		return userPermission;

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

    public String getCustomerCode() {

      return customerCode;

    }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }

   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
   }

   public boolean getSalesOrderVoid() {

   	  return salesOrderVoid;

   }

   public void setSalesOrderVoid(boolean salesOrderVoid) {

   	  this.salesOrderVoid = salesOrderVoid;

   }

   
   
	public String getDeliveryNumberFrom() {

		return deliveryNumberFrom;

	}



	public String getDeliveryNumberTo() {

		return deliveryNumberTo;

	}


 
	
	
   
	public String getDocumentNumberFrom() {

		return documentNumberFrom;

	}
	
	 
	public void setDocumentNumberFrom(String documentNumberFrom) {

		this. documentNumberFrom = documentNumberFrom;

	}




	public String getDocumentNumberTo() {

		return documentNumberTo;

	}

	public void setDocumentNumberTo(String documentNumberTo) {

		this. documentNumberTo = documentNumberTo;

	}


	public String getReferenceNumber() {

		return referenceNumber;

	}

	public void setReferenceNumber(String referenceNumber) {

		this.referenceNumber = referenceNumber;

	}




   public String getPosted() {

   	  return posted;

   }

   public void setPosted(String posted) {

   	  this.posted = posted;

   }

   public ArrayList getPostedList() {

   	  return postedList;

   }

	public String getOrderBy() {

		return orderBy;

	}

	public void setOrderBy(String orderBy) {

		this.orderBy = orderBy;

	}

	public ArrayList getOrderByList() {

		return orderByList;

	}

	
	public HashMap getCriteria() {

		return criteria;

	}

	public void setCriteria(HashMap criteria) {

		this.criteria = criteria;

	}

	public String getTableType() {

		return(tableType);

	}

	public void setTableType(String tableType) {

		this.tableType = tableType;

	}

	public String getSelectedDvNumber() {

   	  return selectedDvNumber;

   }

   public void setSelectedDvNumber(String selectedDvNumber) {

   	  this.selectedDvNumber = selectedDvNumber;

   }

   public boolean getUseCustomerPulldown() {

   	  return useCustomerPulldown;

   }

   public void setUseCustomerPulldown(boolean useCustomerPulldown){

   	  this.useCustomerPulldown = useCustomerPulldown;

   }
   
   

	public String getTransactionStatus() {

  	  	return transactionStatus;

	}

	public void setTransactionStatus(String transactionStatus) {

  	  	this.transactionStatus = transactionStatus;

	}
	
	
	public ArrayList getTransactionStatusList() {
		return transactionStatusList;
	}

	public void setTransactionStatusList(String transactionStatus) {
		transactionStatusList.add(transactionStatus);
	}

	public void clearTransactionStatusList() {
		transactionStatusList.clear();
	}


	public void reset(ActionMapping mapping, HttpServletRequest request) {

		salesOrderVoid = false;

		

        postedList.clear();
        postedList.add(Constants.GLOBAL_BLANK);
        postedList.add(Constants.GLOBAL_YES);
        postedList.add(Constants.GLOBAL_NO);

	

	    if (orderByList.isEmpty()) {

	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("SUPPLIER CODE");
	    	orderByList.add("DOCUMENT NUMBER");
	    	orderByList.add("REFERENCE NUMBER");
		  }

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {

			if(useCustomerPulldown) {
				if (!Common.validateStringExists(customerCodeList, customerCode)) {

					errors.add("customerCode",
							new ActionMessage("findSalesOrder.error.customerCodeInvalid"));

				}
		    }


			
		}

		return errors;

	}

}
