package com.struts.ar.finddelivery;


public class ArFindDeliveryList implements java.io.Serializable {

	
	private Integer deliveryCode = null;
	private Integer salesOrderCode = null;
	private String customerCode = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	
	private String container = null;
	private String deliveryNumber = null;
	private String status = null;
	
	private String openButton = null;

	private ArFindDeliveryForm parentBean;

	public ArFindDeliveryList(ArFindDeliveryForm parentBean,
			Integer deliveryCode,
			Integer salesOrderCode,
			String customerCode,
			String documentNumber,
			String referenceNumber,
			String container,
			String deliveryNumber,
			String status) {

		this.parentBean = parentBean;
		this.deliveryCode = deliveryCode;
		this.salesOrderCode = salesOrderCode;
		this.customerCode = customerCode;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.container = container;
		this.deliveryNumber = deliveryNumber;
		this.status = status;
	}

	public void setOpenButton(String openButton) {

		parentBean.setRowSelected(this, false);

	}
	
	public Integer getDeliveryCode() {

		return deliveryCode;

	}

	public Integer getSalesOrderCode() {

		return salesOrderCode;

	}
	


	public String getDocumentNumber() {

		return documentNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}


	public String getCustomerCode() {

		return customerCode;

	}

	public String getContainer() {

		return container;

	}

	public String getDeliveryNumber() {

		return deliveryNumber;

	}
	
	public String getStatus() {

		return status;

	}

}
