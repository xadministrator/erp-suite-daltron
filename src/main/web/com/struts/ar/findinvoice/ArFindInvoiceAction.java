package com.struts.ar.findinvoice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.txn.ArCreditMemoEntryController;
import com.ejb.txn.ArCreditMemoEntryControllerHome;
import com.ejb.txn.ArFindInvoiceController;
import com.ejb.txn.ArFindInvoiceControllerHome;
import com.ejb.txn.ArInvoiceEntryController;
import com.ejb.txn.ArInvoiceEntryControllerHome;
import com.struts.ar.creditmemoentry.ArCreditMemoEntryList;
import com.struts.ar.receiptentry.ArReceiptEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArInvoiceDetails;
import com.util.ArModDistributionRecordDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;

public final class ArFindInvoiceAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArFindInvoiceAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ArFindInvoiceForm actionForm = (ArFindInvoiceForm)form;

         String frParam = null;

         if (request.getParameter("child") == null) {

         	frParam = Common.getUserPermission(user, Constants.AR_FIND_INVOICE_ID);

         } else {

         	frParam = Constants.FULL_ACCESS;

         }

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  if(request.getParameter("child") == null) {

                  	return mapping.findForward("arFindInvoice");

                  } else {

                  	return mapping.findForward("arFindInvoiceChild");

                  }

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         ActionMessages messages = new ActionMessages();
/*******************************************************
   Initialize ArFindInvoiceController EJB
*******************************************************/

         ArFindInvoiceControllerHome homeFI = null;
         ArFindInvoiceController ejbFI = null;

         ArInvoiceEntryControllerHome homeINVC = null;
         ArInvoiceEntryController ejbINVC = null;

         ArCreditMemoEntryControllerHome homeCM = null;
         ArCreditMemoEntryController ejbCM = null;

         try {

            homeFI = (ArFindInvoiceControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArFindInvoiceControllerEJB", ArFindInvoiceControllerHome.class);
            homeINVC = (ArInvoiceEntryControllerHome)com.util.EJBHomeFactory.
    				lookUpHome("ejb/ArInvoiceEntryControllerEJB", ArInvoiceEntryControllerHome.class);
            homeCM = (ArCreditMemoEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArCreditMemoEntryControllerEJB", ArCreditMemoEntryControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArFindInvoiceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFI = homeFI.create();
            ejbINVC = homeINVC.create();
            ejbCM = homeCM.create();

         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArFindInvoiceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

 /*******************************************************
     Call ArFindInvoiceController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableArInvoiceBatch
     getAdPrfEnableInvShift
     getAdPrfArUseCustomerPulldown
  *******************************************************/

         short precisionUnit = 0;
         boolean enableInvoiceBatch = false;
         boolean enableShift = false;
         boolean useCustomerPulldown = true;

         try {

            precisionUnit = ejbFI.getGlFcPrecisionUnit(user.getCmpCode());
            enableInvoiceBatch = Common.convertByteToBoolean(ejbFI.getAdPrfEnableArInvoiceBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableInvoiceBatch);
            enableShift = Common.convertByteToBoolean(ejbFI.getAdPrfEnableInvShift(user.getCmpCode()));
            actionForm.setShowShift(enableShift);
            useCustomerPulldown = Common.convertByteToBoolean(ejbFI.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);



         } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArFindInvoiceAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));
         }

/*******************************************************
   -- Ar FI Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_DETAILED);

	        if(request.getParameter("child") == null) {

	        	return mapping.findForward("arFindInvoice");

	        } else {

	        	return mapping.findForward("arFindInvoiceChild");

	        }

/*******************************************************
   -- Ar FI Hide Details Action --
*******************************************************/

	     } else if (request.getParameter("hideDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	        if(request.getParameter("child") == null) {

	        	return mapping.findForward("arFindInvoice");

	        } else {

	        	return mapping.findForward("arFindInvoiceChild");

	        }

/*******************************************************
    -- Ar FI First Action --
*******************************************************/

	     } else if(request.getParameter("firstButton") != null){

	     	actionForm.setLineCount(0);

/*******************************************************
   -- Ar FI Previous Action --
*******************************************************/

         } else if(request.getParameter("previousButton") != null){

         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);



/*******************************************************
   -- Ar FI Next Action --
*******************************************************/

         }else if(request.getParameter("nextButton") != null){

         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);


         }

/*******************************************************
  / -- Ar Delete All Action --
/*******************************************************/

          else if(request.getParameter("deleteAllButton") != null){

            actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
              if(actionForm.getCriteria().size()<=0){

              }
              ArrayList list = ejbFI.getArInvByCriteria(actionForm.getCriteria(),
              	    0,
              	    0,
              	    actionForm.getOrderBy(),
              	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());



              for(int x=0;x<list.size();x++){
            	  ArModInvoiceDetails invDetails = new ArModInvoiceDetails();
            	  invDetails = (ArModInvoiceDetails)list.get(x);
            	  ejbINVC.deleteArInvEntry(invDetails.getInvCode(), user.getUserName(), user.getCmpCode());
              }


          }

/*******************************************************
   -- Ar FI Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("lastButton")!=null) {

             // create criteria

            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();
	        	if (!Common.validateRequired(actionForm.getInvoiceType())) {
	        		criteria.put("invoiceType", actionForm.getInvoiceType());
	        	}


	        	if (!Common.validateRequired(actionForm.getBatchName())) {

	        		criteria.put("batchName", actionForm.getBatchName());

	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerBatch())) {

	        		criteria.put("customerBatch", actionForm.getCustomerBatch());

	        	}

	        	if (!Common.validateRequired(actionForm.getShift())) {

	        		criteria.put("shift", actionForm.getShift());

	        	}

	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {

	        		criteria.put("customerCode", actionForm.getCustomerCode());

	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {

	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSerialNumber())) {

	        		criteria.put("serialNumber", actionForm.getSerialNumber().trim());
	        	}

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {

	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

	        	}

	        	if (!Common.validateRequired(actionForm.getDateTo())) {

	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

	        	}

	        	if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {

	        		criteria.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());

	        	}

	        	if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {

	        		criteria.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());

	        	}


	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {

	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());

	        	}

	        	if (!Common.validateRequired(actionForm.getCurrency())) {

	        		criteria.put("currency", actionForm.getCurrency());

	        	}

	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		System.out.println("posted pasok  : " + actionForm.getPosted());
	        		criteria.put("posted", actionForm.getPosted());

	        	}

	        	if (!Common.validateRequired(actionForm.getPaymentStatus())) {

	        		criteria.put("paymentStatus", actionForm.getPaymentStatus());

	        	}

	        	if (actionForm.getCreditMemo()) {

	        		criteria.put("creditMemo", new Byte((byte)1));

                }

                if (!actionForm.getCreditMemo()) {

                	criteria.put("creditMemo", new Byte((byte)0));

                }

                if (actionForm.getInterest()) {

                	criteria.put("interest", new Byte((byte)1));

                }

                if (!actionForm.getInterest()) {

                	criteria.put("interest", new Byte((byte)0));

                }

                if (actionForm.getInvoiceVoid()) {

	        		criteria.put("invoiceVoid", new Byte((byte)1));

                }

                if (!actionForm.getInvoiceVoid()) {

                	criteria.put("invoiceVoid", new Byte((byte)0));

                }

	        	// save criteria

	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);

	     	}

            if(request.getParameter("lastButton") != null) {



            	int size = ejbFI.getArInvSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	System.out.println("max line is : " + size);
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {

            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));

            	} else {

            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);

            	}

            }

            try {

            	actionForm.clearArFIList();
            	System.out.println("CLEAR LIST---------------------------");
            	ArrayList list = ejbFI.getArInvByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()),
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
            	    actionForm.getOrderBy(),
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	System.out.println("actionForm.getLineCount()="+actionForm.getLineCount());
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {

	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);

	           } else {

	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);

	           }


	           System.out.println("list.size()="+list.size());
	           System.out.println("Constants.GLOBAL_MAX_LINES="+Constants.GLOBAL_MAX_LINES);
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {

	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);

	           } else {

	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);

	           	  //remove last record
	           	  list.remove(list.size() - 1);

	           }

            	Iterator i = list.iterator();

            	while (i.hasNext()) {

            		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();


            		System.out.println("Search code is: " + mdetails.getInvCode());

            		ArFindInvoiceList arFIList = new ArFindInvoiceList(actionForm,
            		    mdetails.getInvCode(),
            		    Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
            		    mdetails.getInvCstCustomerCode(),
            		    mdetails.getInvCstName(),
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        mdetails.getInvType(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),
            		    Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit),"0",mdetails.getInvArBatchName(),Common.convertByteToBoolean(mdetails.getInvPosted()));

            		actionForm.saveArFIList(arFIList);

            	}

            } catch (GlobalNoRecordFoundException ex) {
               System.out.println("1 GlobalNoRecordFoundException ex");
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findInvoice.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindInvoiceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

               if (request.getParameter("child") == null) {

               		return mapping.findForward("arFindInvoice");

               } else {

               		return mapping.findForward("arFindInvoiceChild");

               }

            }


	        if (actionForm.getTableType() == null) {

	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

            }

	        if (request.getParameter("child") == null) {

	        	return mapping.findForward("arFindInvoice");

	        } else {

	        	return mapping.findForward("arFindInvoiceChild");

	        }
/*******************************************************
	-- Ar FI Credit Memo Button --
*******************************************************/

	     } else if (request.getParameter("creditMemoButton") != null) {



	    	 ArrayList errorLogList = new ArrayList();
	    	 ArrayList postedInvCode = new ArrayList();
	    	 int noOfSuccess = 0;
	    	 int noOfError = 0;
				for (int i = 0; i<actionForm.getArFIListSize(); i++) {

					ArFindInvoiceList arFindInvoice = actionForm.getArFIByIndex(i);

					if(arFindInvoice.getSelectCreditMemo()){
						System.out.println("entering credit memo");
						 ArInvoiceDetails details = new ArInvoiceDetails();
						 details.setInvCode(null);
					     details.setInvDate(Common.convertStringToSQLDate(arFindInvoice.getDate()));
					     details.setInvNumber(null);
					     details.setInvCmInvoiceNumber(arFindInvoice.getInvoiceNumber());
					     details.setInvCmReferenceNumber(arFindInvoice.getInvoiceNumber());
					     details.setInvAmountDue(Common.convertStringMoneyToDouble(arFindInvoice.getCreditMemoAmount(), precisionUnit));
					     details.setInvVoid(Common.convertBooleanToByte(false));
					     details.setInvDescription("");
					     details.setInvSubjectToCommission(Common.convertBooleanToByte(false));
					     details.setInvCreatedBy(user.getUserName());
					     details.setInvDateCreated(new java.util.Date());
					     details.setInvLastModifiedBy(user.getUserName());
						 details.setInvDateLastModified(new java.util.Date());








						 if(!Common.validateMoneyFormat( arFindInvoice.getCreditMemoAmount()== null ? "0":  arFindInvoice.getCreditMemoAmount())	){
							 errorLogList.add( arFindInvoice.getInvoiceNumber() + " = Invalid Credit Amount Format	");
					         noOfError++;
					         continue;
						 }else{
							 if(Common.convertStringMoneyToDouble(arFindInvoice.getCreditMemoAmount(), precisionUnit)<=0){

								 errorLogList.add( arFindInvoice.getInvoiceNumber() + " = Credit Amount cannot be zero");
						         noOfError++;
						         continue;
							 }
						 }



						 if((Common.validateRequired(arFindInvoice.getInvoiceBatch()) || arFindInvoice.getInvoiceBatch().equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
									actionForm.getShowBatchName()){
							 errorLogList.add( arFindInvoice.getInvoiceNumber() + " = Invalid Batch Name");
					        	noOfError++;
					        	continue;
						 }



						 ArModInvoiceDetails arInvDetails=null;
					        try{



					        	System.out.println("invoice is :" + arFindInvoice.getInvoiceCode() + " in " + i);
					        	arInvDetails = ejbINVC.getArInvByInvCode(arFindInvoice.getInvoiceCode(),  user.getCmpCode());
					        	System.out.println("passed getArInvByInvCode");
					        //	System.out.println(" size memoline: " + arInvDetails.getInvIlList().size());


					        }
					        catch(Exception ex){
					        	errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Invoice not exist or already deleted");
					        	noOfError++;
					        	continue;
					        }


					    //	System.out.println(arInvDetails.getInvIliList().size() + " invoicel line item size");
						if(arInvDetails.getInvIliList()!=null){
							//item
						    System.out.println("item entered");
					        ArrayList invIliList =  new ArrayList();


					        for (int x = 0; x<arInvDetails.getInvIliList().size(); x++) {

					        	ArModInvoiceLineItemDetails arInvLnItm = null;

				        		 arInvLnItm = (ArModInvoiceLineItemDetails)arInvDetails.getInvIliList().get(x);

				        		 boolean isAssemblyItem = ejbCM.getInvItemClassByIiName(arInvLnItm.getIliIiName(), user.getCmpCode()).equals("Assembly");

				        		 if(isAssemblyItem == true) {
				        			 System.out.println("Assembly");
						        	   arInvLnItm.setIliIiClass("Assembly");
					        	    } else {
					        	    	 System.out.println("Stock");
					        		   arInvLnItm.setIliIiClass("Stock");
					        	    }
				        		 invIliList.add(arInvLnItm);



					        }


						    try {

				           	    Integer creditMemoCode = ejbCM.saveArInvIliEntry(details, arFindInvoice.getCustomerCode(), arFindInvoice.getInvoiceBatch(),
				           	    		invIliList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

				           	  noOfSuccess++;

				           	 postedInvCode.add(creditMemoCode);

				           } catch (GlobalRecordAlreadyDeletedException ex) {
				           		errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Record Already Deleted");
				        	 //  messages.add(ActionMessages.GLOBAL_MESSAGE,
								//		new ActionMessage("app.creditMemo", ""));
				        	   noOfError++;

				           } catch (GlobalNoRecordFoundException ex) {

				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Invoice not Found");
				        	   noOfError++;
				           } catch (GlobalDocumentNumberNotUniqueException ex) {

				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Document Number not Unique");
				        	   noOfError++;
				           } catch (GlobalTransactionAlreadyApprovedException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already Approved");

				           } catch (GlobalTransactionAlreadyPendingException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already Pendin");

				           } catch (GlobalTransactionAlreadyPostedException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already Posted");

				           } catch (GlobalTransactionAlreadyVoidPostedException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already void");

				           } catch (ArINVOverapplicationNotAllowedException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Amount must be less than Voucher's Amount Due");


				           } catch (GlobalTransactionAlreadyLockedException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Voucher entered has been used by another debit memo or check payment.  Please post the said transactions to release the lock");

				           } catch (GlobalNoApprovalRequesterFoundException ex) {
				        	   noOfError++;
				        	   //Your user name has no amount limit found.  Please contact your administrator to configure the approval setup.
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Your user name has no amount limit found.  Please contact your administrator to configure the approval setup");

				           } catch (GlobalNoApprovalApproverFoundException ex) {
				           		//Your entered amount has no approver found.  Please contact your administrator to configure the approval setup
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Your entered amount has no approver found.  Please contact your administrator to configure the approval setup.");
				        	   noOfError++;
				           } catch (GlobalInvItemLocationNotFoundException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = No item location found");

				           } catch (GlJREffectiveDateNoPeriodExistException ex) {
				           	//Date is Invalid.  Period is not found.
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Date is Invalid.  Period is not found");
				        	   noOfError++;
				           } catch (GlJREffectiveDatePeriodClosedException ex) {
				           	//Date is Invalid. Period is not open.
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Date is Invalid. Period is not open");
				        	   noOfError++;
				           } catch (GlobalJournalNotBalanceException ex) {
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Journal not balance");
				        	   noOfError++;
				           } catch (GlobalInventoryDateException ex) {
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Date inventory error");
				           } catch (GlobalBranchAccountNumberInvalidException ex) {
				           	//Account Generated is Invalid for the Current Branch.
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Account Generated is Invalid for the Current Branch");
				        	   noOfError++;
				           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
				          	//Negative Inventory Costing Chart Of Account is not set. Please select a COA at the Ad Preference module
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Negative Inventory Costing Chart Of Account is not set. Please select a COA at the Ad Preference module");
				        	   noOfError++;
				           } catch (EJBException ex) {
				           	   System.out.println(ex.toString());
				        	   noOfError++;
				        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Contact administrator");

				           }

						}else if(arInvDetails.getInvIlList()!=null){
				//memo line
							System.out.println("Entering credit memo in memo line");
							try {
							    ArrayList drList = new ArrayList();
							    drList = ejbCM.getArDrByInvCmInvoiceNumberAndInvBillAmountAndCstCustomerCode(
							    		arFindInvoice.getInvoiceNumber(),
										Common.convertStringMoneyToDouble(arFindInvoice.getCreditMemoAmount(), precisionUnit),
										arFindInvoice.getCustomerCode(), user.getCmpCode());

								 System.out.println("3--");
								 Integer creditMemoCode = ejbCM.saveArInvEntry(details, arFindInvoice.getCustomerCode(), arFindInvoice.getInvoiceBatch(),
						           	        drList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
								 System.out.println("4--");

					           	 noOfSuccess++;
					           	 postedInvCode.add(creditMemoCode);
							 } catch (GlobalRecordAlreadyDeletedException ex) {
					           		errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Record Already Deleted");

					           		noOfError++;

							 } catch (GlobalNoRecordFoundException ex) {
					        	   noOfError++;

					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Invoice not Found");

							 } catch (GlobalDocumentNumberNotUniqueException ex) {
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Document Number not Unique");

							 } catch (GlobalTransactionAlreadyApprovedException ex) {
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already Approved");

							 } catch (GlobalTransactionAlreadyPendingException ex) {
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already Pending");

							 } catch (GlobalTransactionAlreadyPostedException ex) {
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already Posted");

							 } catch (GlobalTransactionAlreadyVoidPostedException ex) {
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Transaction Already Void Posted");

							 } catch (ArINVOverapplicationNotAllowedException ex) {
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Amount must be less than Voucher's Amount Due");


							 } catch (GlobalTransactionAlreadyLockedException ex) {
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Voucher entered has been used by another debit memo or check payment.  Please post the said transactions to release the lock");

							 } catch (GlobalNoApprovalRequesterFoundException ex) {

					        	   //Your user name has no amount limit found.  Please contact your administrator to configure the approval setup.
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Your user name has no amount limit found.  Please contact your administrator to configure the approval setup");

							 } catch (GlobalNoApprovalApproverFoundException ex) {
					           		//Your entered amount has no approver found.  Please contact your administrator to configure the approval setup
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Your entered amount has no approver found.  Please contact your administrator to configure the approval setup");


							 } catch (GlJREffectiveDateNoPeriodExistException ex) {
					           	//Date is Invalid.  Period is not found.
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Date is Invalid.  Period is not found");

							 } catch (GlJREffectiveDatePeriodClosedException ex) {
					           	//Date is Invalid. Period is not open.
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Date is Invalid. Period is not open");

							 } catch (GlobalJournalNotBalanceException ex) {
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Journal not balance");

							 } catch (GlobalBranchAccountNumberInvalidException ex) {
					           	//Account Generated is Invalid for the Current Branch.
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Account Generated is Invalid for the Current Branch");

					        	   noOfError++;
							 } catch (EJBException ex) {

					        	   System.out.println(ex.toString());
					        	   noOfError++;
					        	   errorLogList.add(arFindInvoice.getInvoiceNumber() + " = Contact administrator");

							 }



						}

					}

				}



				//

				int noOfSendPosting = 0;
				int noOfPosted = 0;
				int noOfSentForApproval =0;

				 try {

					 for(int x=0;x<postedInvCode.size();x++){
						 ArrayList list = ejbCM.getAdApprovalNotifiedUsersByInvCode((Integer)postedInvCode.get(x), user.getCmpCode());

	                 	   if (list.isEmpty()) {

	                 	   	   noOfSendPosting++;

	                 	   } else if (list.contains("DOCUMENT POSTED")) {

	                 		  noOfPosted++;
	                 	   } else {

	                 		  noOfSentForApproval++;
	                 	   }

	                 	   saveMessages(request, messages);
	                 	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

					 }



                 } catch(EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in ArCreditMemoEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }

		               return(mapping.findForward("cmnErrorPage"));

		           }




				messages.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("app.creditMemo", "POSTED: " + noOfPosted ));
				messages.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("app.creditMemo", "FOR POSTING: " + noOfSendPosting ));
				messages.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("app.creditMemo", "FOR APPROVAL: " + noOfSentForApproval ));
				messages.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("app.creditMemo", "FAIL: " + noOfError ));


				for(int x=0;x<errorLogList.size();x++){


					messages.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("app.creditMemo", "ERROR INV: " + errorLogList.get(x).toString()));
				}

				saveMessages(request, messages);



/*******************************************************
   -- Ar FI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar FI Open Action --
*******************************************************/

         } else if (request.getParameter("arFIList[" +
            actionForm.getRowSelected() + "].openButton") != null) {

             ArFindInvoiceList arFIList =
                actionForm.getArFIByIndex(actionForm.getRowSelected());

             String path = null;

             if (!arFIList.getCreditMemo()) {

                path = "/arInvoiceEntry.do?forward=1" +
                             "&invoiceCode=" + arFIList.getInvoiceCode();

             } else {
                System.out.println("credit memo forward");
                path = "/arCreditMemoEntry.do?forward=1" +
                             "&creditMemoCode=" + arFIList.getInvoiceCode();
                System.out.println(path);
             }

			 return(new ActionForward(path));

/*******************************************************
   -- Ar FI Load Action --
*******************************************************/

         }
         if (frParam != null) {

        	 actionForm.clearArFIList();
            ArrayList list = null;
            Iterator i = null;

            try {

            	if(actionForm.getUseCustomerPulldown()) {

                	actionForm.clearCustomerCodeList();

            		list = ejbFI.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		if (list == null || list.size() == 0) {

            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {

            			i = list.iterator();

            			while (i.hasNext()) {

            				actionForm.setCustomerCodeList((String)i.next());

            			}

            		}

            	}

            	actionForm.clearCurrencyList();

            	list = ejbFI.getGlFcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setCurrencyList((String)i.next());

            		}

            	}

            	actionForm.clearBatchNameList();

            	list = ejbFI.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBatchNameList((String)i.next());

            		}

            	}


            	actionForm.clearCustomerBatchList();

         		list = ejbFI.getAdLvCustomerBatchAll(user.getCmpCode());

         		if (list == null || list.size() == 0) {

         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);

         		} else {

         			i = list.iterator();

         			while (i.hasNext()) {

         				actionForm.setCustomerBatchList((String)i.next());

         			}

         		}





            	actionForm.clearCmBatchNameList();

            	list = ejbCM.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCmBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setCmBatchNameList((String)i.next());

            		}

            	}




            	actionForm.clearShiftList();

            	list = ejbFI.getAdLvInvShiftAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setShiftList((String)i.next());

            		}

            	}

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindInvoiceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("goButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }


            }

            if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {



            	if(request.getParameter("findType")!=null){

            		String invType = request.getParameter("findType").toString();

            		System.out.println("invType="+invType);

            		if(invType.equals("ITEMS")){
            			actionForm.setInvoiceType("ITEMS");
            		}else{
            			actionForm.setInvoiceType("MEMO LINES");
            		}


            	}else{
            		actionForm.setInvoiceType(Constants.GLOBAL_BLANK);
            	}

	        	actionForm.setBatchName(Constants.GLOBAL_BLANK);
	        	actionForm.setCustomerCode(Constants.GLOBAL_BLANK);
	        	actionForm.setCreditMemo(false);
	        	actionForm.setInvoiceVoid(false);
	        	actionForm.setDateFrom(null);
	        	actionForm.setDateTo(null);
	        	actionForm.setInvoiceNumberFrom(null);
	        	actionForm.setInvoiceNumberTo(null);
	        	actionForm.setReferenceNumber(null);
	        	actionForm.setApprovalStatus("DRAFT");
	        	actionForm.setCurrency(Constants.GLOBAL_BLANK);
	            actionForm.setPosted(Constants.GLOBAL_NO);
	            actionForm.setPaymentStatus(Constants.GLOBAL_BLANK);
	            actionForm.setOrderBy(Constants.GLOBAL_BLANK);

	            actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);


	            HashMap criteria = new HashMap();
	            criteria.put("invoiceType", actionForm.getInvoiceType());
	            criteria.put("posted", actionForm.getPosted());
	            criteria.put("approvalStatus", actionForm.getApprovalStatus());
	            criteria.put("invoiceVoid", new Byte((byte)0));
	            criteria.put("creditMemo", new Byte((byte)0));

	            actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	            if(request.getParameter("findDraft") != null) {

	            	if(request.getParameter("creditMemo") != null) {

	            		actionForm.setCreditMemo(true);
	            		criteria.put("creditMemo", new Byte((byte)1));
	            		return new ActionForward("/arFindInvoice.do?goButton=1&creditMemo=true");

	            	} else {

	            		return new ActionForward("/arFindInvoice.do?goButton=1");

	            	}

	            }

	            actionForm.setCriteria(criteria);


            } else {

            	HashMap c = actionForm.getCriteria();

            	if(c.containsKey("invoiceVoid")) {

            		Byte b = (Byte)c.get("invoiceVoid");

            		actionForm.setInvoiceVoid(Common.convertByteToBoolean(b.byteValue()));

            	}

            	if(c.containsKey("creditMemo")) {

            		Byte b = (Byte)c.get("creditMemo");

            		actionForm.setCreditMemo(Common.convertByteToBoolean(b.byteValue()));
            	}

            	try {

            	//	actionForm.clearArFIList();
                	System.out.println("CLEAR LIST 3 ------------------------------------------");
                	ArrayList newList = ejbFI.getArInvByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()),
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
                	    actionForm.getOrderBy(),
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {

    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);

    	           } else {

    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);

    	           }

    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);

    	           } else {

    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);

    	           	  //remove last record
    	           	newList.remove(newList.size() - 1);

    	           }

                	Iterator j = newList.iterator();

                	while (j.hasNext()) {

                		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)j.next();

                		ArFindInvoiceList arFIList = new ArFindInvoiceList(actionForm,
                		    mdetails.getInvCode(),
                		    Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
                		    mdetails.getInvCstCustomerCode(),
                		    mdetails.getInvCstName(),
                            Common.convertSQLDateToString(mdetails.getInvDate()),
                            mdetails.getInvNumber(),
                            mdetails.getInvReferenceNumber(),
                            mdetails.getInvType(),
                            Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),
                		    Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit),"0",mdetails.getInvArBatchName(),Common.convertByteToBoolean(mdetails.getInvPosted()));

                		actionForm.saveArFIList(arFIList);

                	}

                } catch (GlobalNoRecordFoundException ex) {

                   // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);

                   if(actionForm.getLineCount() > 0) {

                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);

                   } else {

                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);

                   }

                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findInvoice.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in ArFindInvoiceAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage");

                   }

                }

            }

	        if (request.getParameter("child") == null) {

	        	return mapping.findForward("arFindInvoice");

	        } else {

	        	try {

	        		actionForm.setLineCount(0);

	        		HashMap criteria = new HashMap();
	        		criteria.put(new String("creditMemo"), new Byte((byte)0));
	            	criteria.put(new String("invoiceVoid"), new Byte((byte)0));
	            	criteria.put(new String("posted"), "YES");
	            	criteria.put(new String("paymentStatus"), "UNPAID");
	        		actionForm.setCriteria(criteria);

	            	actionForm.clearArFIList();
	            	System.out.println("CLEAR LIST 4 ----------------");
	            	ArrayList newlist = ejbFI.getArInvByCriteria(actionForm.getCriteria(),
	            	    new Integer(actionForm.getLineCount()),
	            	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
	            	    actionForm.getOrderBy(),
	            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	            	// check if prev should be disabled
		           if (actionForm.getLineCount() == 0) {

		              actionForm.setDisablePreviousButton(true);
		              actionForm.setDisableFirstButton(true);

		           } else {

		           	  actionForm.setDisablePreviousButton(false);
		           	  actionForm.setDisableFirstButton(false);

		           }

		           // check if next should be disabled
		           if (newlist.size() <= Constants.GLOBAL_MAX_LINES) {

		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);

		           } else {

		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);

		           	  //remove last record
		           	newlist.remove(newlist.size() - 1);

		           }

		           i = newlist.iterator();

		           while (i.hasNext()) {

		           	ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();

		           	ArFindInvoiceList arFIList = new ArFindInvoiceList(actionForm,
		           			mdetails.getInvCode(),
							Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
							mdetails.getInvCstCustomerCode(),
							mdetails.getInvCstName(),
							Common.convertSQLDateToString(mdetails.getInvDate()),
							mdetails.getInvNumber(),
							mdetails.getInvReferenceNumber(),
							mdetails.getInvType(),
							Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),
							Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit),"0",mdetails.getInvArBatchName(),Common.convertByteToBoolean(mdetails.getInvPosted()));

		           	actionForm.saveArFIList(arFIList);

		           }

	            } catch (GlobalNoRecordFoundException ex) {

	               // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);

	               if(actionForm.getLineCount() > 0) {

                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);

                   } else {

                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);

                   }

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArFindInvoiceAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }

	        	actionForm.setSelectedInvoiceNumber(request.getParameter("selectedInvoiceNumber"));
	        	actionForm.setSelectedInvoiceType(request.getParameter("selectedInvoiceType"));
	        	return mapping.findForward("arFindInvoiceChild");

	        }

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArFindInvoiceAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}