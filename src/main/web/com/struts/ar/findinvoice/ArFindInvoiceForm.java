package com.struts.ar.findinvoice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArFindInvoiceForm extends ActionForm implements Serializable {


   private String invoiceType = null;
   private ArrayList invoiceTypeList = new ArrayList();
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private ArrayList cmBatchNameList = new ArrayList();
   private String shift = null;
   private ArrayList shiftList = new ArrayList();
   private String customerCode = null;
   private ArrayList customerCodeList = new ArrayList();
   private boolean creditMemo = false;
   private boolean interest = false;
   private boolean invoiceVoid = false;
   private String dateFrom = null;
   private String dateTo = null;
   private String invoiceNumberFrom = null;
   private String invoiceNumberTo = null;
   private String referenceNumber = null;
   private String serialNumber = null;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String paymentStatus = null;
   private ArrayList paymentStatusList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String customerBatch = null;
   private ArrayList customerBatchList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;

   private boolean showBatchName = false;
   private boolean showShift = false;
   private String pageState = new String();
   private ArrayList arFIList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   private String deleteAllButton = null;

   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   private boolean disableDeleteAllButton = false;

   private boolean useCustomerPulldown = true;

   private String selectedInvoiceNumber = null;
   private String selectedInvoiceType = null;
   private String selectedInvoiceEntered = null;
   private String typeOfSearch = null;

   private int lineCount = 0;

   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }

   public int getLineCount(){
   	  return lineCount;
   }

   public void setLineCount(int lineCount){
   	  this.lineCount = lineCount;
   }

   public ArFindInvoiceList getArFIByIndex(int index) {

      return((ArFindInvoiceList)arFIList.get(index));

   }

   public Object[] getArFIList() {

      return arFIList.toArray();

   }

   public int getArFIListSize() {

      return arFIList.size();

   }

   public void saveArFIList(Object newArFIList) {

      arFIList.add(newArFIList);

   }

   public void clearArFIList() {
      arFIList.clear();
   }

   public void setRowSelected(Object selectedArFIList, boolean isEdit) {

      this.rowSelected = arFIList.indexOf(selectedArFIList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showArFIRow(int rowSelected) {

   }

   public void updateArFIRow(int rowSelected, Object newArFIList) {

      arFIList.set(rowSelected, newArFIList);

   }

   public void deleteArFIList(int rowSelected) {

      arFIList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {

	  this.showDetailsButton = showDetailsButton;

   }

   public void setHideDetailsButton(String hideDetailsButton) {

      this.hideDetailsButton = hideDetailsButton;

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getSelectedInvoiceNumber() {

   	  return(selectedInvoiceNumber);

   }

   public void setSelectedInvoiceNumber(String selectedInvoiceNumber) {

   	  this.selectedInvoiceNumber = selectedInvoiceNumber;

   }

   public String getSelectedInvoiceType() {

   	  return(selectedInvoiceType);

   }

   public void setSelectedInvoiceType(String selectedInvoiceType) {

   	  this.selectedInvoiceType = selectedInvoiceType;

   }

   public String getSelectedInvoiceEntered() {

   	  return(selectedInvoiceEntered);

   }

   public void setSelectedInvoiceEntered(String selectedInvoiceEntered) {

   	  this.selectedInvoiceEntered = selectedInvoiceEntered;

   }

   public String getTypeOfSearch() {

   	  return(typeOfSearch);

   }

   public void setTypeOfSearch(String typeOfSearch) {

   	  this.typeOfSearch = typeOfSearch;

   }

   public String getInvoiceType() {

	  return(invoiceType);

   }

   public void setInvoiceType(String invoiceType){

	  this.invoiceType = invoiceType;

   }

   public ArrayList getInvoiceTypeList(){
	   return invoiceTypeList;
   }

   public void setInvoiceTypeList(String invoiceType){
	   invoiceTypeList.add(invoiceType);
   }

   public void clearInvoiceTypeList(){
	   invoiceTypeList.clear();
	   invoiceTypeList.add(Constants.GLOBAL_BLANK);
   }

   public String getBatchName() {

	  return(batchName);

   }

   public void setBatchName(String batchName){

	  this.batchName = batchName;

   }

   public ArrayList getBatchNameList() {

	  return(batchNameList);

   }

   public void setBatchNameList(String batchName){

	  batchNameList.add(batchName);

   }

   public void clearBatchNameList(){

	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);

   }


   public ArrayList getCmBatchNameList(){
	   return(cmBatchNameList);
   }

   public void setCmBatchNameList(String batchName){
	   cmBatchNameList.add(batchName);
   }

   public void clearCmBatchNameList(){
	   cmBatchNameList.clear();
	   cmBatchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getShift() {

	  return(shift);

   }

   public void setShift(String shift){

	  this.shift = shift;

   }

   public ArrayList getShiftList() {

	  return(shiftList);

   }

   public void setShiftList(String shift){

	  shiftList.add(shift);

   }

   public void clearShiftList(){

	  shiftList.clear();
	  shiftList.add(Constants.GLOBAL_BLANK);

   }

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }

   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);

   }

   public boolean getCreditMemo() {

   	  return creditMemo;

   }

   public void setCreditMemo(boolean creditMemo) {

   	  this.creditMemo = creditMemo;

   }

   public boolean getInterest(){
	   return interest;
   }

   public void setInterest(boolean interest){

	   this.interest = interest;
   }

   public boolean getInvoiceVoid() {

   	  return invoiceVoid;

   }

   public void setInvoiceVoid(boolean invoiceVoid) {

   	  this.invoiceVoid = invoiceVoid;

   }

   public String getDateFrom() {

   	  return dateFrom;

   }

   public void setDateFrom(String dateFrom) {

   	  this.dateFrom = dateFrom;

   }

   public String getDateTo() {

   	  return dateTo;

   }

   public void setDateTo(String dateTo) {

   	  this.dateTo = dateTo;

   }

   public String getInvoiceNumberFrom() {

   	  return invoiceNumberFrom;

   }

   public void setInvoiceNumberFrom(String invoiceNumberFrom) {

   	  this.invoiceNumberFrom = invoiceNumberFrom;

   }

   public String getInvoiceNumberTo() {

   	  return invoiceNumberTo;

   }

   public void setInvoiceNumberTo(String invoiceNumberTo) {

   	  this.invoiceNumberTo = invoiceNumberTo;

   }

   public String getReferenceNumber() {

   	  return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

   	  this.referenceNumber = referenceNumber;

   }

	public String getSerialNumber() {

   	  return serialNumber;

   }

   public void setSerialNumber(String serialNumber) {

   	  this.serialNumber = serialNumber;

   }
   public String getApprovalStatus() {

   	  return approvalStatus;

   }

   public void setApprovalStatus(String approvalStatus) {

   	  this.approvalStatus = approvalStatus;

   }

   public ArrayList getApprovalStatusList() {

   	  return approvalStatusList;

   }

   public String getCurrency() {

   	  return currency;

   }

   public void setCurrency(String currency) {

   	  this.currency = currency;

   }

   public ArrayList getCurrencyList() {

   	  return currencyList;

   }

   public void setCurrencyList(String currency) {

   	  currencyList.add(currency);

   }

   public void clearCurrencyList() {

   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);

   }

   public String getPosted() {

   	  return posted;

   }

   public void setPosted(String posted) {

   	  this.posted = posted;

   }

   public ArrayList getPostedList() {

   	  return postedList;

   }

   public String getPaymentStatus() {

   	  return paymentStatus;

   }

   public void setPaymentStatus(String paymentStatus) {

   	  this.paymentStatus = paymentStatus;

   }

   public ArrayList getPaymentStatusList() {

   	  return paymentStatusList;

   }

   public String getOrderBy() {

   	  return orderBy;

   }

   public void setOrderBy(String orderBy) {

   	  this.orderBy = orderBy;

   }

   public ArrayList getOrderByList() {

   	  return orderByList;

   }

   public String getCustomerBatch() {

	      return customerBatch;

	   }

	   public void setCustomerBatch(String customerBatch) {

	      this.customerBatch = customerBatch;

	   }

	   public ArrayList getCustomerBatchList() {

	      return customerBatchList;

	   }

	   public void setCustomerBatchList(String customerBatch) {

	      customerBatchList.add(customerBatch);

	   }

	   public void clearCustomerBatchList() {

		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);

	   }


   public boolean getDisablePreviousButton() {

   	  return disablePreviousButton;

   }

   public void setDisablePreviousButton(boolean disablePreviousButton) {

   	  this.disablePreviousButton = disablePreviousButton;

   }

   public boolean getDisableNextButton() {

      return disableNextButton;

   }

   public void setDisableNextButton(boolean disableNextButton) {

   	  this.disableNextButton = disableNextButton;

   }

   public boolean getDisableFirstButton() {

   	  return disableFirstButton;

   }

   public void setDisableFirstButton(boolean disableFirstButton) {

   	  this.disableFirstButton = disableFirstButton;

   }

   public boolean getDisableLastButton() {

   	  return disableLastButton;

   }

   public void setDisableLastButton(boolean disableLastButton) {

   	  this.disableLastButton = disableLastButton;

   }


   public boolean getDisableDeleteAllButton(){
	   return disableDeleteAllButton;
   }

   public void setDisableDeleteAllButton(boolean disableDeleteAllButton){
	   this.disableDeleteAllButton = disableDeleteAllButton;
   }

   public HashMap getCriteria() {

   	   return criteria;

   }

   public void setCriteria(HashMap criteria) {

   	   this.criteria = criteria;

   }

   public String getTableType() {

      return(tableType);

   }

   public void setTableType(String tableType) {

      this.tableType = tableType;

   }

   public boolean getShowBatchName() {

   	   return showBatchName;

   }

   public void setShowBatchName(boolean showBatchName) {

   	   this.showBatchName = showBatchName;

   }

   public boolean getShowShift() {

   	   return showShift;

   }

   public void setShowShift(boolean showShift) {

   	   this.showShift = showShift;

   }

   public boolean getUseCustomerPulldown() {

   	  return useCustomerPulldown;

   }

   public void setUseCustomerPulldown(boolean useCustomerPulldown) {

   	  this.useCustomerPulldown = useCustomerPulldown;

   }




   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  creditMemo = false;
   	  invoiceVoid = false;
   	  interest = false;

   	  invoiceTypeList.clear();
   	  invoiceTypeList.add(Constants.GLOBAL_BLANK);
   	  invoiceTypeList.add("ITEMS");
   	  invoiceTypeList.add("MEMO LINES");
   	  invoiceTypeList.add("SO MATCHED");
   	  invoiceTypeList.add("JO MATCHED");

      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");
      approvalStatusList.add("REJECTED");

      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add(Constants.GLOBAL_YES);
      postedList.add(Constants.GLOBAL_NO);

      paymentStatusList.clear();
      paymentStatusList.add(Constants.GLOBAL_BLANK);
      paymentStatusList.add(Constants.AR_FI_PAYMENT_STATUS_PAID);
      paymentStatusList.add(Constants.AR_FI_PAYMENT_STATUS_UNPAID);


      if (orderByList.isEmpty()) {

	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AR_FI_ORDER_BY_CUSTOMER_CODE);
	      orderByList.add(Constants.AR_FI_ORDER_BY_INVOICE_NUMBER);


	  }

      showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

      	if(useCustomerPulldown) {
      		if (!Common.validateStringExists(customerCodeList, customerCode)) {

      			errors.add("customerCode",
      					new ActionMessage("findInvoice.error.customerCodeInvalid"));

      		}
      	}

         if (!Common.validateDateFormat(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("findInvoice.error.dateFromInvalid"));

         }

         if (!Common.validateDateFormat(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("findInvoice.error.dateToInvalid"));

         }

      }

      return errors;

   }

}