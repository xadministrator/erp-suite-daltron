package com.struts.ar.findinvoice;

import java.io.Serializable;

public class ArFindInvoiceList implements Serializable {

   private Integer invoiceCode = null;
   private boolean creditMemo = false;

   private String customerCode = null;
   private String customerName = null;
   private String date = null;
   private String invoiceNumber = null;
   private String referenceNumber = null;  
   private String amountDue = null;
   private String amountPaid = null;
   private String invoiceType = null;
  
  
   private boolean ifPosted = false;

   
   private String creditMemoAmount = null;
   private String invoiceBatch = null;
   private boolean SelectCreditMemo = false;
   
   private String openButton = null;
       
   private ArFindInvoiceForm parentBean;
    
   public ArFindInvoiceList(ArFindInvoiceForm parentBean,
      Integer invoiceCode,
      boolean creditMemo,
      String customerCode,
      String customerName,
      String date,
      String invoiceNumber,
      String referenceNumber,
      String invoiceType,
      String amountDue,
      String amountPaid,
      String creditMemoAmount,
      String invoiceBatch,
      boolean ifPosted) {

      this.parentBean = parentBean;
      this.invoiceCode = invoiceCode;
      this.creditMemo = creditMemo;
      this.customerCode = customerCode;
      this.customerName = customerName;
      this.date = date;
      this.invoiceNumber = invoiceNumber;
      this.referenceNumber = referenceNumber;
      this.invoiceType = invoiceType;
      this.amountDue = amountDue;
      this.amountPaid = amountPaid;
      this.creditMemoAmount = creditMemoAmount;
      this.invoiceBatch = invoiceBatch;
      this.ifPosted = ifPosted;

   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }

   public Integer getInvoiceCode() {

      return invoiceCode;

   }
   
   public boolean getCreditMemo() {
   	
   	  return creditMemo;
   	
   }

   
   public String getCustomerCode() {
	   return customerCode;
   }
   
   public void setCustomerCode(String customerCode){
	   this.customerCode = customerCode;
   }
   
   public String getCustomerName() {

      return customerName;
      
   }

   public String getDate() {
   
      return date;
      
   }
   
   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmountDue() {
   
      return amountDue;
   
   }

   public String getAmountPaid() {
   
      return amountPaid;
   
   }
   
   public String getInvoiceType(){
	   return invoiceType;
   }
   
   public void setInvoiceType(String invoiceType){
	   this.invoiceType = invoiceType;
   }
   
   public String getCreditMemoAmount(){
	   return creditMemoAmount;
   }
   
   public void setCreditMemoAmount(String creditMemoAmount){
	   this.creditMemoAmount = creditMemoAmount;
   }
   
   public String getInvoiceBatch(){
	   return invoiceBatch;
   }
   
   public void setInvoiceBatch(String invoiceBatch){
	   this.invoiceBatch = invoiceBatch;
   }
   
   public boolean getSelectCreditMemo(){
	   
	   return SelectCreditMemo;
   }
   
   public void setSelectCreditMemo(boolean SelectCreditMemo){
	   
	   this.SelectCreditMemo = SelectCreditMemo;
   }
   
   
   public boolean getIfPosted(){
	   return ifPosted;
   }
   
   public void setIfPosted(boolean ifPosted){
	   this.ifPosted = ifPosted;
   }
   

  
      
   
}