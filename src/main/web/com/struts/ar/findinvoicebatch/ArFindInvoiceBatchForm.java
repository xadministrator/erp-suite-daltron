package com.struts.ar.findinvoicebatch;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArFindInvoiceBatchForm extends ActionForm implements Serializable {

   private String batchName = null;
   private String status = null;
   private ArrayList statusList = new ArrayList();
   private String dateCreated = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
  
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private ArrayList ArFIBList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ArFindInvoiceBatchList getArFIBByIndex(int index){
      return((ArFindInvoiceBatchList)ArFIBList.get(index));
   }

   public Object[] getArFIBList(){
      return(ArFIBList.toArray());
   }

   public int getArFIBListSize(){
      return(ArFIBList.size());
   }

   public void saveArFIBList(Object newArFIBList){
      ArFIBList.add(newArFIBList);
   }

   public void clearArFIBList(){
      ArFIBList.clear();
   }

   public void setRowSelected(Object selectedArFIBList, boolean isEdit){
      this.rowSelected = ArFIBList.indexOf(selectedArFIBList);
   }

   public void updateArFIBRow(int rowSelected, Object newArFIBList){
      ArFIBList.set(rowSelected, newArFIBList);
   }

   public void deleteArFIBList(int rowSelected){
      ArFIBList.remove(rowSelected);
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getBatchName(){
      return(batchName);
   }

   public void setBatchName(String batchName){
      this.batchName = batchName;
   }
      
   public String getStatus() {
   	  return(status);	  
   }
   
   public void setStatus(String status) {
   	  this.status = status;
   }
   
   public ArrayList getStatusList() {
   	  return(statusList);
   }
      
   public String getDateCreated() {
   	  return(dateCreated);
   }
   
   public void setDateCreated(String dateCreated){
   	  this.dateCreated = dateCreated;
   }   
            
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request){
   	
      statusList.clear();
      statusList.add(Constants.GLOBAL_BLANK);
      statusList.add("OPEN");
      statusList.add("CLOSED");
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add("BATCH NAME");
	      orderByList.add("DATE CREATED");
	      
	  }
	  
	  showDetailsButton = null;
	  hideDetailsButton = null;      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
       ActionErrors errors = new ActionErrors();
      
       if (!Common.validateDateFormat(dateCreated)) {

	     errors.add("dateCreated",
	        new ActionMessage("findInvoiceBatch.error.dateCreatedInvalid"));
	
	  }         
	 	       
      return(errors);
   }
}
