package com.struts.ar.findinvoicebatch;

import java.io.Serializable;

public class ArFindInvoiceBatchList implements Serializable {

   private Integer invoiceBatchCode = null;
   private String batchName = null;
   private String description = null;
   private String status = null;
   private String dateCreated = null;
   private String createdBy = null;

   private String openButton = null;
    
   private ArFindInvoiceBatchForm parentBean;
    
   public ArFindInvoiceBatchList(ArFindInvoiceBatchForm parentBean,
      Integer invoiceBatchCode,
      String batchName,
      String description,
      String status,
      String dateCreated,
      String createdBy){

      this.parentBean = parentBean;
      this.invoiceBatchCode = invoiceBatchCode;
      this.batchName = batchName;
      this.description = description;
      this.status = status;
      this.dateCreated = dateCreated;
      this.createdBy = createdBy;
      
   }

   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }

   public Integer getInvoiceBatchCode(){
      return(invoiceBatchCode);
   }

   public String getBatchName(){
      return(batchName);
   }

   public String getDescription(){
      return(description);
   }

   public String getStatus(){
      return(status);
   }
   
   public String getDateCreated(){
      return(dateCreated);
   }

   public String getCreatedBy(){
      return(createdBy);
   }
   
}
