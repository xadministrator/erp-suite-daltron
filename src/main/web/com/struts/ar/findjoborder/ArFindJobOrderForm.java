package com.struts.ar.findjoborder;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArFindJobOrderForm extends ActionForm implements java.io.Serializable {

    private String customerCode = null;
    
	private ArrayList customerCodeList = new ArrayList();
	private String jobOrderType = null;
    private ArrayList jobOrderTypeList = new ArrayList();
	private boolean jobOrderVoid = false;
	private String documentNumberFrom = null;
	private String documentNumberTo = null;
	private String referenceNumber = null;
	private ArrayList transactionTypeList = new ArrayList();
	private String transactionType = null;
	private String technician = null;
	private ArrayList technicianList = new ArrayList();
	private String jobOrderStatus = null;
	private ArrayList jobOrderStatusList= new ArrayList();
	private String dateFrom = null;
	private String dateTo = null;
    private String approvalStatus = null;
    private ArrayList approvalStatusList = new ArrayList();
    private String currency = null;
    private ArrayList currencyList = new ArrayList();
    private String posted = null;
    private ArrayList postedList = new ArrayList();
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();

	private String tableType = null;

	private String showDetailsButton = null;
	private String hideDetailsButton = null;
	private String pageState = new String();
	private ArrayList arFJOList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();

	private String nextButton = null;
	private String previousButton = null;
	private String firstButton = null;
	private String lastButton = null;

	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableFirstButton = false;
	private boolean disableLastButton = false;
	private boolean useCustomerPulldown = true;
	private String approvalDateFrom = null;
	private String approvalDateTo = null;

	private String selectedJoNumber = null;

	private int lineCount = 0;

	private HashMap criteria = new HashMap();

	public int getRowSelected() {

		return rowSelected;

	}

	public int getLineCount() {

		return lineCount;

	}

	public void setLineCount(int lineCount) {

		this.lineCount = lineCount;

	}

	public ArFindJobOrderList getArFJOByIndex(int index) {

		return((ArFindJobOrderList)arFJOList.get(index));

	}

	public Object[] getArFJOList() {

		return arFJOList.toArray();

	}

	public int getArFJOListSize() {

		return arFJOList.size();

	}

	public void saveArFJOList(Object newArFJOList) {

		arFJOList.add(newArFJOList);

	}

	public void clearArFJOList() {

		arFJOList.clear();

	}

	public void setRowSelected(Object selectedArFJOList, boolean isEdit) {

		this.rowSelected = arFJOList.indexOf(selectedArFJOList);

		if (isEdit) {

			this.pageState = Constants.PAGE_STATE_EDIT;

		}

	}

	public void setShowDetailsButton(String showDetailsButton) {

		this.showDetailsButton = showDetailsButton;

	}

	public void setHideDetailsButton(String hideDetailsButton) {

		this.hideDetailsButton = hideDetailsButton;

	}

	public void setPageState(String pageState) {

		this.pageState = pageState;

	}

	public String getPageState() {

		return pageState;

	}

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}

	public void setTxnStatus(String txnStatus) {

		this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

		return userPermission;

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

    public String getCustomerCode() {

      return customerCode;

    }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }

   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
   }

   
   public String getJobOrderType() {
	   return jobOrderType;
   }
   
   public void setJobOrderType(String jobOrderType) {
	   this.jobOrderType = jobOrderType;
   }
   
   public ArrayList getJobOrderTypeList() {

      return jobOrderTypeList;

   }

   public void setJobOrderTypeList(String jobOrderType) {

	   jobOrderTypeList.add(jobOrderType);

   }

   public void clearJobOrderTypeList() {

	   jobOrderTypeList.clear();
	   jobOrderTypeList.add(Constants.GLOBAL_BLANK);
   }
   
   
   public boolean getJobOrderVoid() {

   	  return jobOrderVoid;

   }

   public void setJobOrderVoid(boolean jobOrderVoid) {

   	  this.jobOrderVoid = jobOrderVoid;

   }

	public String getDocumentNumberFrom() {

		return documentNumberFrom;

	}

	public void setDocumentNumberFrom(String documentNumberFrom) {

		this.documentNumberFrom = documentNumberFrom;

	}

	public String getDocumentNumberTo() {

		return documentNumberTo;

	}

	public void setDocumentNumberTo(String documentNumberTo) {

		this.documentNumberTo = documentNumberTo;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public void setReferenceNumber(String referenceNumber) {

		this.referenceNumber = referenceNumber;

	}


	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}


	public ArrayList getTransactionTypeList() {

		return transactionTypeList;

	}

  	public void setTransactionTypeList(String transactionType) {

  		transactionTypeList.add(transactionType);

  	}

  	public void clearTransactionTypeList() {

  		transactionTypeList.clear();
  		transactionTypeList.add(Constants.GLOBAL_BLANK);

  	}
  	
  	
  	public String getTechnician() {
		return technician;
	}

	public void setTechnician(String technician) {
		this.technician = technician;
	}


	public ArrayList getTechnicianList() {

		return technicianList;

	}

  	public void setTechnicianList(String technician) {

  		technicianList.add(technician);

  	}

  	public void clearTechnicianList() {

  		technicianList.clear();
  		technicianList.add(Constants.GLOBAL_BLANK);

  	}
  	
  	
  	public String getJobOrderStatus() {
  		return jobOrderStatus;
  	}
  	
  	public void setJobOrderStatus(String jobOrderStatus) {
  		this.jobOrderStatus = jobOrderStatus;
  	}
  	
  	
  	public ArrayList getJobOrderStatusList() {

		return jobOrderStatusList;

	}

  	public void setJobOrderStatusList(String jobOrderStatus) {

  		jobOrderStatusList.add(jobOrderStatus);

  	}

  	public void clearJobOrderStatusList() {

  		jobOrderStatusList.clear();
  		jobOrderStatusList.add(Constants.GLOBAL_BLANK);

  	}
  	

	public String getDateFrom() {

		return dateFrom;

	}

	public void setDateFrom(String dateFrom) {

		this.dateFrom = dateFrom;

	}

	public String getDateTo() {

		return dateTo;

	}

	public void setDateTo(String dateTo) {

		this.dateTo = dateTo;

	}

	public String getApprovalDateFrom() {

		return approvalDateFrom;

	}

	public void setApprovalDateFrom(String approvalDateFrom) {

		this.approvalDateFrom = approvalDateFrom;

	}

	public String getApprovalDateTo() {

		return approvalDateTo;

	}

	public void setApprovalDateTo(String approvalDateTo) {

		this.approvalDateTo = approvalDateTo;

	}

   public String getApprovalStatus() {

   	  return approvalStatus;

   }

   public void setApprovalStatus(String approvalStatus) {

   	  this.approvalStatus = approvalStatus;

   }

   public ArrayList getApprovalStatusList() {

   	  return approvalStatusList;

   }

   public String getCurrency() {

   	  return currency;

   }

   public void setCurrency(String currency) {

   	  this.currency = currency;

   }

   public ArrayList getCurrencyList() {

   	  return currencyList;

   }

   public void setCurrencyList(String currency) {

   	  currencyList.add(currency);

   }

   public void clearCurrencyList() {

   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);

   }

   public String getPosted() {

   	  return posted;

   }

   public void setPosted(String posted) {

   	  this.posted = posted;

   }

   public ArrayList getPostedList() {

   	  return postedList;

   }

	public String getOrderBy() {

		return orderBy;

	}

	public void setOrderBy(String orderBy) {

		this.orderBy = orderBy;

	}

	public ArrayList getOrderByList() {

		return orderByList;

	}

	public boolean getDisablePreviousButton() {

		return disablePreviousButton;

	}

	public void setDisablePreviousButton(boolean disablePreviousButton) {

		this.disablePreviousButton = disablePreviousButton;

	}

	public boolean getDisableNextButton() {

		return disableNextButton;

	}

	public void setDisableNextButton(boolean disableNextButton) {

		this.disableNextButton = disableNextButton;

	}

	public boolean getDisableFirstButton() {

		return disableFirstButton;

	}

	public void setDisableFirstButton(boolean disableFirstButton) {

		this.disableFirstButton = disableFirstButton;

	}

	public boolean getDisableLastButton() {

		return disableLastButton;

	}

	public void setDisableLastButton(boolean disableLastButton) {

		this.disableLastButton = disableLastButton;

	}

	public HashMap getCriteria() {

		return criteria;

	}

	public void setCriteria(HashMap criteria) {

		this.criteria = criteria;

	}

	public String getTableType() {

		return(tableType);

	}

	public void setTableType(String tableType) {

		this.tableType = tableType;

	}

	public String getSelectedJoNumber() {

   	  return selectedJoNumber;

   }

   public void setSelectedJoNumber(String selectedJoNumber) {

   	  this.selectedJoNumber = selectedJoNumber;

   }

   public boolean getUseCustomerPulldown() {

   	  return useCustomerPulldown;

   }

   public void setUseCustomerPulldown(boolean useCustomerPulldown){

   	  this.useCustomerPulldown = useCustomerPulldown;

   }

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		jobOrderVoid = false;
		jobOrderType = null;
		approvalStatusList.clear();
        approvalStatusList.add(Constants.GLOBAL_BLANK);
        approvalStatusList.add("DRAFT");
        approvalStatusList.add("N/A");
        approvalStatusList.add("PENDING");
        approvalStatusList.add("APPROVED");
        approvalStatusList.add("REJECTED");

        postedList.clear();
        postedList.add(Constants.GLOBAL_BLANK);
        postedList.add(Constants.GLOBAL_YES);
        postedList.add(Constants.GLOBAL_NO);

		showDetailsButton = null;
		hideDetailsButton = null;

	    if (orderByList.isEmpty()) {

	    	orderByList.clear();
	    	orderByList.add(Constants.GLOBAL_BLANK);
	    	orderByList.add("SUPPLIER CODE");
	    	orderByList.add("DOCUMENT NUMBER");

		  }

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		if (request.getParameter("goButton") != null) {

			if(useCustomerPulldown) {
				if (!Common.validateStringExists(customerCodeList, customerCode)) {

					errors.add("customerCode",
							new ActionMessage("findJobOrder.error.customerCodeInvalid"));

				}
		    }

			if (!Common.validateDateFormat(dateFrom)) {

				errors.add("dateFrom",
					new ActionMessage("findJobOrder.error.dateFromInvalid"));

			}

			if (!Common.validateDateFormat(dateTo)) {

				errors.add("dateTo",
					new ActionMessage("findJobOrder.error.dateToInvalid"));

			}

			if (!Common.validateDateFormat(approvalDateFrom)) {

				errors.add("approvalDateFrom",
					new ActionMessage("findJobOrder.error.approvalDateFromInvalid"));

			}

			if (!Common.validateDateFormat(approvalDateTo)) {

				errors.add("approvalDateTo",
					new ActionMessage("findJobOrder.error.approvalDateToInvalid"));

			}

		}

		return errors;

	}

}
