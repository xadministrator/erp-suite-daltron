package com.struts.ar.findjoborder;


public class ArFindJobOrderList implements java.io.Serializable {

    private Integer jobOrderCode = null;
    private String jobOrderType = null;
	private String customerCode = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String transactionType = null;
	private String customerName = null;

	private String openButton = null;

	private ArFindJobOrderForm parentBean;

	public ArFindJobOrderList(ArFindJobOrderForm parentBean,
			Integer jobOrderCode,
			String jobOrderType,
			String customerCode,
			String date,
			String documentNumber,
			String referenceNumber,
			String transactionType,
			String customerName) {

		this.parentBean = parentBean;
		this.jobOrderCode = jobOrderCode;
		this.jobOrderType = jobOrderType;
		this.customerCode = customerCode;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.transactionType = transactionType;
		this.customerName = customerName;

	}

	public void setOpenButton(String openButton) {

		parentBean.setRowSelected(this, false);

	}

	public Integer getJobOrderCode() {

		return jobOrderCode;

	}
	
	public String getJobOrderType() {

		return jobOrderType;

	}

	public String getDocumentNumber() {

		return documentNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public String getTransactionType() {

		return transactionType;

	}

	public String getCustomerCode() {

		return customerCode;

	}

	public String getDate() {

		return date;

	}

	public String getCustomerName() {

		return customerName;

	}

}
