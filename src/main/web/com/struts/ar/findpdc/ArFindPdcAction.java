package com.struts.ar.findpdc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArFindPdcController;
import com.ejb.txn.ArFindPdcControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModPdcDetails;

public final class ArFindPdcAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArFindPdcAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArFindPdcForm actionForm = (ArFindPdcForm)form;
         
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         	
         	frParam = Common.getUserPermission(user, Constants.AR_FIND_PDC_ID);
         	
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  
                  if(request.getParameter("child") == null) {
                  
                  	return mapping.findForward("arFindPdc");
                  	
                  } else {
                  	
                  	return mapping.findForward("arFindPdcChild");
                  	
                  }
                  
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArFindPdcController EJB
*******************************************************/

         ArFindPdcControllerHome homeFP = null;
         ArFindPdcController ejbFP = null;

         try {
          
            homeFP = (ArFindPdcControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArFindPdcControllerEJB", ArFindPdcControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArFindPdcAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFP = homeFP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArFindPdcAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call ArFindPdcController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableInvShift
     getAdPrfArUseCustomerPulldown
  *******************************************************/

         short precisionUnit = 0;
         boolean enableShift = false;
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbFP.getGlFcPrecisionUnit(user.getCmpCode());           
            enableShift = Common.convertByteToBoolean(ejbFP.getAdPrfEnableInvShift(user.getCmpCode()));
            actionForm.setShowShift(enableShift);
            useCustomerPulldown = Common.convertByteToBoolean(ejbFP.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArFindPdcAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ar FP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        if(request.getParameter("child") == null) {
	        	
	        	return mapping.findForward("arFindPdc");
	        	
	        } else {
	        	
	        	return mapping.findForward("arFindPdcChild");
	        	
	        }
	     
/*******************************************************
   -- Ar FP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        if(request.getParameter("child") == null) {
	        	
	        	return mapping.findForward("arFindPdc");
	        	
	        } else {
	        	
	        	return mapping.findForward("arFindPdcChild");
	        	
	        }         

	        
/*******************************************************
   -- Ar FP First Action --
*******************************************************/ 

	     } else if(request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	     	
/*******************************************************
   -- Ar FP Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ar FP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 
         
/*******************************************************
   -- Ar FP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getShift())) {
	        		
	        		criteria.put("shift", actionForm.getShift());
	        		
	        	}
	
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        		
	        	}	

	        	if (!Common.validateRequired(actionForm.getCheckNumber())) {
	        		
	        		criteria.put("checkNumber", actionForm.getCheckNumber());
	        		
	        	}		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateReceivedFrom())) {
	        		
	        		criteria.put("dateReceivedFrom", Common.convertStringToSQLDate(actionForm.getDateReceivedFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateReceivedTo())) {
	        		
	        		criteria.put("dateReceivedTo", Common.convertStringToSQLDate(actionForm.getDateReceivedTo()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getMaturityDateFrom())) {
	        		
	        		criteria.put("maturityDateFrom", Common.convertStringToSQLDate(actionForm.getMaturityDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getMaturityDateTo())) {
	        		
	        		criteria.put("maturityDateTo", Common.convertStringToSQLDate(actionForm.getMaturityDateTo()));
	        		
	        	}	        		        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getStatus())) {
	        		
	        		criteria.put("status", actionForm.getStatus());
	        		
	        	}
	        	
                if (actionForm.getCancelled()) {	        	
		        	   		        		
	        		criteria.put("cancelled", new Byte((byte)1));
                
                }
                
                if (!actionForm.getCancelled()) {
                	
                	criteria.put("cancelled", new Byte((byte)0));
                	
                }
                	 
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFP.getArPdcSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            try {
            	
            	actionForm.clearArFPList();
            	
            	ArrayList list = ejbFP.getArPdcByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
            	    actionForm.getOrderBy(), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModPdcDetails mdetails = (ArModPdcDetails)i.next();
            		
            		ArFindPdcList arFPList = new ArFindPdcList(actionForm,
            		    mdetails.getPdcCode(),
            		    mdetails.getPdcCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getPdcDateReceived()),
                        Common.convertSQLDateToString(mdetails.getPdcMaturityDate()),
                        mdetails.getPdcCheckNumber(),
                        mdetails.getPdcReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getPdcAmount(), precisionUnit));
            		    
            		actionForm.saveArFPList(arFPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findPdc.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindPdcAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
               if (request.getParameter("child") == null) {
               	
               		return mapping.findForward("arFindPdc");
               	
               } else {
               	
               		return mapping.findForward("arFindPdcChild");
               	
               }

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
	        if (request.getParameter("child") == null) {
	        	
	        	return mapping.findForward("arFindPdc");
	        	
	        } else {
	        	
	        	return mapping.findForward("arFindPdcChild");
	        	
	        } 

/*******************************************************
   -- Ar FP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar FP Open Action --
*******************************************************/

         } else if (request.getParameter("arFPList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             ArFindPdcList arFPList =
                actionForm.getArFPByIndex(actionForm.getRowSelected());
                
             String path = null;               
                          	
	  	        path = "/arPdcEntry.do?forward=1" +
				     "&pdcCode=" + arFPList.getPdcCode();

			 return(new ActionForward(path));

/*******************************************************
   -- Ar FP Load Action --
*******************************************************/

         }
         if (frParam != null) {

            actionForm.clearArFPList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseCustomerPulldown()) {
            	    
                	actionForm.clearCustomerCodeList();           	
                	
            		list = ejbFP.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerCodeList((String)i.next());
            				
            			}
            			
            		}
            	
            	}
            	
            	actionForm.clearShiftList();           	
            	
            	list = ejbFP.getAdLvInvShiftAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setShiftList((String)i.next());
            			
            		}
            		
            	}
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindPdcAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("goButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            
            
	        if (actionForm.getTableType() == null) {
	        	
	        	actionForm.setCustomerCode(Constants.GLOBAL_BLANK);
	        	actionForm.setStatus(Constants.GLOBAL_BLANK);   
	        	actionForm.setCancelled(false);
	        	actionForm.setDateReceivedFrom(null);
	        	actionForm.setDateReceivedTo(null);
	        	actionForm.setMaturityDateFrom(null);
	        	actionForm.setMaturityDateTo(null);
	        	actionForm.setReferenceNumber(null);
	        	actionForm.setCheckNumber(null);
	        	actionForm.setPosted(Constants.GLOBAL_NO);
	        	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
	        	
	        	actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	        	
	            HashMap criteria = new HashMap();
	            criteria.put("cancelled", new Byte((byte)0));
	            criteria.put("posted", actionForm.getPosted());
	            
	            actionForm.setCriteria(criteria);
	            
	        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            } else {
            	
            	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("cancelled")) {
            		
            		Byte b = (Byte)c.get("cancelled");
            		
            		actionForm.setCancelled(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	try {
                	
            		actionForm.clearArFPList();
                	
                	ArrayList newList = ejbFP.getArPdcByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
                	    actionForm.getOrderBy(), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	newList.remove(newList.size() - 1);
    	           	
    	           }
                	
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
                		ArModPdcDetails mdetails = (ArModPdcDetails)j.next();
                		
                		ArFindPdcList arFPList = new ArFindPdcList(actionForm,
                		    mdetails.getPdcCode(),
                		    mdetails.getPdcCstCustomerCode(),            		                		    
                            Common.convertSQLDateToString(mdetails.getPdcDateReceived()),
                            Common.convertSQLDateToString(mdetails.getPdcMaturityDate()),
                            mdetails.getPdcCheckNumber(),
                            mdetails.getPdcReferenceNumber(),
                            Common.convertDoubleToStringMoney(mdetails.getPdcAmount(), precisionUnit));
                		    
                		actionForm.saveArFPList(arFPList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findPdc.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in ArFindPdcAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }
	        
	        if (request.getParameter("child") == null) {
	        	
	        	return mapping.findForward("arFindPdc");
	        	
	        } else {
	        	
	        	try {
	        		
	        		actionForm.setLineCount(0);
	        		
	        		HashMap criteria = new HashMap();
	            	criteria.put(new String("cancelled"), new Byte((byte)0));
	            	criteria.put(new String("posted"), "YES");
	            	criteria.put(new String("status"), "OPEN");
	        		actionForm.setCriteria(criteria);
	            	
	            	actionForm.clearArFPList();
	            	
	            	ArrayList newlist = ejbFP.getArPdcByCriteria(actionForm.getCriteria(),
	            	    new Integer(actionForm.getLineCount()), 
	            	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
	            	    actionForm.getOrderBy(), 
	            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	
	            	// check if prev should be disabled
		           if (actionForm.getLineCount() == 0) {
		            	
		              actionForm.setDisablePreviousButton(true);
		              actionForm.setDisableFirstButton(true);
		            	
		           } else {
		           	
		           	  actionForm.setDisablePreviousButton(false);
		           	  actionForm.setDisableFirstButton(false);
		           	
		           }
		           
		           // check if next should be disabled
		           if (newlist.size() <= Constants.GLOBAL_MAX_LINES) {
		           	  
		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);
		           	  
		           } else {
		           	  
		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);
		           	  
		           	  //remove last record
		           	newlist.remove(newlist.size() - 1);
		           	
		           }
	            	
		           i = newlist.iterator();
		           
		           while (i.hasNext()) {
		           	
		           	ArModPdcDetails mdetails = (ArModPdcDetails)i.next();
		           	
		           	ArFindPdcList arFPList = new ArFindPdcList(actionForm,
		           			mdetails.getPdcCode(),
							mdetails.getPdcCstCustomerCode(),            		                		    
							Common.convertSQLDateToString(mdetails.getPdcDateReceived()),
							Common.convertSQLDateToString(mdetails.getPdcMaturityDate()),							
							mdetails.getPdcCheckNumber(),
							mdetails.getPdcReferenceNumber(),                            
							Common.convertDoubleToStringMoney(mdetails.getPdcAmount(), precisionUnit));
		           	
		           	actionForm.saveArFPList(arFPList);
		           	
		           }

	            } catch (GlobalNoRecordFoundException ex) {
	               
	               // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArFindPdcAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }

	            }
	        	
	        	actionForm.setSelectedInvoiceNumber(request.getParameter("selectedInvoiceNumber"));
	        	return mapping.findForward("arFindPdcChild");
	        	
	        }

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArFindPdcAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}