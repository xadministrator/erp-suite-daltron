package com.struts.ar.findpdc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArFindPdcForm extends ActionForm implements Serializable {

   private String status = null;
   private ArrayList statusList = new ArrayList();
   private String shift = null;
   private ArrayList shiftList = new ArrayList();
   private String customerCode = null;
   private ArrayList customerCodeList = new ArrayList();
   private boolean cancelled = false;
   private String dateReceivedFrom = null;
   private String dateReceivedTo = null;
   private String maturityDateFrom = null;
   private String maturityDateTo = null;
   private String checkNumber = null;   
   private String referenceNumber = null;
   private String posted = null;
   private ArrayList postedList = new ArrayList();      
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	 
   private boolean showShift = false;
   private String pageState = new String();
   private ArrayList arFPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   private boolean useCustomerPulldown = true;
   
   private String selectedInvoiceNumber = null;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ArFindPdcList getArFPByIndex(int index) {

      return((ArFindPdcList)arFPList.get(index));

   }

   public Object[] getArFPList() {

      return arFPList.toArray();

   }

   public int getArFPListSize() {

      return arFPList.size();

   }

   public void saveArFPList(Object newArFPList) {

      arFPList.add(newArFPList);

   }

   public void clearArFPList() {
      arFPList.clear();
   }

   public void setRowSelected(Object selectedArFPList, boolean isEdit) {

      this.rowSelected = arFPList.indexOf(selectedArFPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showArFPRow(int rowSelected) {

   }

   public void updateArFPRow(int rowSelected, Object newArFPList) {

      arFPList.set(rowSelected, newArFPList);

   }

   public void deleteArFPList(int rowSelected) {

      arFPList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getSelectedInvoiceNumber() {
   	
   	  return(selectedInvoiceNumber);
   	
   }
   
   public void setSelectedInvoiceNumber(String selectedInvoiceNumber) {
   	
   	  this.selectedInvoiceNumber = selectedInvoiceNumber;
   	
   }

   public String getStatus() {
   	
	  return(status);
	  
   }

   public void setStatus(String status){
   	
	  this.status = status;
	  
   }

   public ArrayList getStatusList() {
   	
	  return(statusList);
	  
   }

   public String getShift() {
   	
	  return(shift);
	  
   }

   public void setShift(String shift){
   	
	  this.shift = shift;
	  
   }

   public ArrayList getShiftList() {
   	
	  return(shiftList);
	  
   }

   public void setShiftList(String shift){
   	
	  shiftList.add(shift);
	  
   }

   public void clearShiftList(){
   	
	  shiftList.clear();
	  shiftList.add(Constants.GLOBAL_BLANK);
	  
   }

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }

   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
   }
   
   public boolean getCancelled() {
   	
   	  return cancelled;
   	  
   }
   
   public void setCancelled(boolean cancelled) {
   	
   	  this.cancelled = cancelled;
   	  
   } 
   
   public String getDateReceivedFrom() {
   	
   	  return dateReceivedFrom;
   	  
   }
   
   public void setDateReceivedFrom(String dateReceivedFrom) {
   	
   	  this.dateReceivedFrom = dateReceivedFrom;
   	  
   }
   
   public String getDateReceivedTo() {
   	
   	  return dateReceivedTo;
   	  
   }
   
   public void setDateReceivedTo(String dateReceivedTo) {
   	
   	  this.dateReceivedTo = dateReceivedTo;
   	  
   }
   
   public String getMaturityDateFrom() {
   	
   	  return maturityDateFrom;
   	  
   }
   
   public void setMaturityDateFrom(String maturityDateFrom) {
   	
   	  this.maturityDateFrom = maturityDateFrom;
   	  
   }
   
   public String getMaturityDateTo() {
   	
   	  return maturityDateTo;
   	  
   }
   
   public void setMaturityDateTo(String maturityDateTo) {
   	
   	  this.maturityDateTo = maturityDateTo;
   	  
   } 

   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	  this.referenceNumber = referenceNumber;
   	  
   }

   public String getCheckNumber() {
   	
   	  return checkNumber;
   
   }
   
   public void setCheckNumber(String checkNumber) {
   	
   	  this.checkNumber = checkNumber;
   
   }
   
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }  
   
   public boolean getShowShift() {
   	
   	   return showShift;
   	
   }
   
   public void setShowShift(boolean showShift) {
   	
   	   this.showShift = showShift;
   	
   }

   public boolean getUseCustomerPulldown() {
   	
   	  return useCustomerPulldown;
   	  
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	  this.useCustomerPulldown = useCustomerPulldown;
   	  
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
      
   	  cancelled = false;
   	  
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add(Constants.GLOBAL_YES);
      postedList.add(Constants.GLOBAL_NO);
      
      statusList.clear();
      statusList.add(Constants.GLOBAL_BLANK);
      statusList.add("OPEN");
      statusList.add("MATURED");
      statusList.add("CLEARED");
        
      if (orderByList.isEmpty()) {
      	
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("CUSTOMER CODE");
	      orderByList.add("CHECK NUMBER");
	     
	  }

	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

      	if(useCustomerPulldown) {
      		if (!Common.validateStringExists(customerCodeList, customerCode)) {
      			
      			errors.add("customerCode",
      					new ActionMessage("findPdc.error.customerCodeInvalid"));
      			
      		}
      	}
         
         if (!Common.validateDateFormat(dateReceivedFrom)) {

            errors.add("dateReceivedFrom",
               new ActionMessage("findPdc.error.dateReceivedFromInvalid"));

         }         
         
         if (!Common.validateDateFormat(dateReceivedTo)) {

            errors.add("dateReceivedTo",
               new ActionMessage("findPdc.error.dateReceivedToInvalid"));

         }                  
         
         if (!Common.validateDateFormat(maturityDateFrom)) {

            errors.add("maturityDateFrom",
               new ActionMessage("findPdc.error.maturityDateFromInvalid"));

         }         
         
         if (!Common.validateDateFormat(maturityDateTo)) {

            errors.add("maturityDateTo",
               new ActionMessage("findPdc.error.maturityDateToInvalid"));

         }                           
                  
      }
      
      return errors;

   }
   
}