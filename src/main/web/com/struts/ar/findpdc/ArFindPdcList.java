package com.struts.ar.findpdc;

import java.io.Serializable;

public class ArFindPdcList implements Serializable {

   private Integer pdcCode = null;
   private String customerCode = null;
   private String dateReceived = null;
   private String maturityDate = null;
   private String checkNumber = null;
   private String referenceNumber = null;  
   private String amount = null;

   private String openButton = null;
       
   private ArFindPdcForm parentBean;
    
   public ArFindPdcList(ArFindPdcForm parentBean,
      Integer pdcCode,
      String customerCode,
      String dateReceived,
      String maturityDate,
      String checkNumber,
      String referenceNumber,
      String amount) {

      this.parentBean = parentBean;
      this.pdcCode = pdcCode;
      this.customerCode = customerCode;
      this.dateReceived = dateReceived;
      this.maturityDate = maturityDate;
      this.checkNumber = checkNumber;
      this.referenceNumber = referenceNumber;
      this.amount = amount;

   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }

   public Integer getPdcCode() {

      return pdcCode;

   }

   public String getCustomerCode() {

      return customerCode;
      
   }

   public String getDateReceived() {
   
      return dateReceived;
      
   }
   
   public String getMaturityDate() {
   	
   	  return maturityDate;
   	
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmount() {
   
      return amount;
   
   }

}