package com.struts.ar.findpersonel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArFindPersonelController;
import com.ejb.txn.ArFindPersonelControllerHome;
import com.ejb.txn.ArFindPersonelControllerHome;


import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModPersonelDetails;



public final class ArFindPersonelAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArFindPersonelAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArFindPersonelForm actionForm = (ArFindPersonelForm)form;
         
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
            frParam = Common.getUserPermission(user, Constants.AR_FIND_CUSTOMER_ID);
         	
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  if (request.getParameter("child") == null) {

                  	return mapping.findForward("arFindPersonel");
                  	
                  } else {
                  
                    return mapping.findForward("arFindPersonelChild");
                  
                  }
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
/*******************************************************
   Initialize arFindPersonelController EJB
*******************************************************/

         ArFindPersonelControllerHome homeFP = null;
         ArFindPersonelController ejbFP = null;

         try {

            homeFP = (ArFindPersonelControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArFindPersonelControllerEJB", ArFindPersonelControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArFindPersonelAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

        	 ejbFP = homeFP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArFindPersonelAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
/*******************************************************
   -- Ar FP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	   
	     
	        if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindPersonel");
	          	
	        } else {
	          
	            return mapping.findForward("arFindPersonelChild");
	          
	        }
	     
/*******************************************************
   -- Ar FP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	       
	        
	        if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindPersonel");
	          	
	        } else {
	          
	            return mapping.findForward("arFindPersonelChild");
	          
	        }                         

/*******************************************************
    -- Ar FP First Action --
*******************************************************/ 

	     } else if (request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	        
/*******************************************************
   -- Ar FP Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ar FP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Ar FP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null || request.getParameter("findRejected") != null)  {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getIdNumber())) {
	        		
	        		criteria.put("idNumber", actionForm.getIdNumber());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("personelName", actionForm.getName());
	        		
	        	}	  
	        	
	        	if (!Common.validateRequired(actionForm.getPersonelType())) {
	        		
	        		criteria.put("personelType", actionForm.getPersonelType());
	        		
	        	}   

	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	} 
            
           
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFP.getArPeSizeByCriteria(actionForm.getCriteria(), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }

            try {
            	
            	actionForm.clearArFPList();
            	
            	ArrayList list = ejbFP.getArPeByCriteria(actionForm.getCriteria(), 
            			new Integer(actionForm.getLineCount()), 
						new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), user.getCmpCode());
            	
            	// check if prev should be disabled
            	if (actionForm.getLineCount() == 0) {
            		
            		actionForm.setDisablePreviousButton(true);
            		actionForm.setDisableFirstButton(true);
            		
            	} else {
            		
            		actionForm.setDisablePreviousButton(false);
            		actionForm.setDisableFirstButton(false);
            		
            	}
            	
            	// check if next should be disabled
            	if (list.size() <= Constants.GLOBAL_MAX_LINES) {
            		
            		actionForm.setDisableNextButton(true);
            		actionForm.setDisableLastButton(true);
            		
            	} else {
            		
            		actionForm.setDisableNextButton(false);
            		actionForm.setDisableLastButton(false);
            		
            		//remove last record
            		list.remove(list.size() - 1);
            		
            	}
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModPersonelDetails mdetails  = (ArModPersonelDetails)i.next();
            			
            		ArFindPersonelList arFPList = new ArFindPersonelList(actionForm,
            				mdetails.getPeCode(),
            				mdetails.getPeIdNumber(),
            				mdetails.getPeName(),
            				mdetails.getPeDescription(),
            				mdetails.getPeAddress(),
            				mdetails.getPePtName()
            				);
            		
            		actionForm.saveArFPList(arFPList);
            		
            	}
            	
            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findCustomer.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindPersonelAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               if (request.getParameter("child") == null) {

		          	return mapping.findForward("arFindPersonel");
		          	
		        } else {
		          
		            return mapping.findForward("arFindPersonelChild");
		          
		        }

            }
                        
          	                       
            
            if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindPersonel");
	          	
	        } else {
	          
	            return mapping.findForward("arFindPersonelChild");
	          
	        }

/*******************************************************
   -- Ar FP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar FP Open Action --
*******************************************************/

         } else if (request.getParameter("arFPList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
        	 
        	 System.out.println("rowselect:" + actionForm.getRowSelected());
             ArFindPersonelList arFPList =
                actionForm.getArFPByIndex(actionForm.getRowSelected());
          
  	         String path = "/arPersonel.do?forward=1" +
			     "&peCode=" + arFPList.getPeCode();
		      
			   return(new ActionForward(path));

/*******************************************************
   -- Ar FP Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
        	 
            actionForm.clearArFPList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	
            	actionForm.clearPersonelTypeList();;
            	
            	list = ejbFP.getArPtNmAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPersonelType(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			actionForm.setPersonelTypeList((String)i.next());
            			
            		}
            		
            	}            	
            	
            
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindPersonelAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            
            
        	actionForm.setIdNumber(null);
        	actionForm.setName(null);
        	actionForm.setDescription(null);
        	actionForm.setPersonelType(null);
        	actionForm.setOrderBy(Constants.AR_FP_ORDER_BY_ID_NUMBER);
        
        	actionForm.setLineCount(0);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.setDisableFirstButton(true);
            actionForm.setDisableLastButton(true);
            actionForm.reset(mapping, request);
            
            HashMap criteria = new HashMap();
            criteria.put("enable", new Byte((byte)0));
        	criteria.put("disable", new Byte((byte)0));
            
        	actionForm.setCriteria(criteria);
            
            
            
                        
            if (request.getParameter("child") == null) {

	          	return mapping.findForward("arFindPersonel");
	          	
	        } else {
	        	                    		
	        	try {
	        		
	        		actionForm.reset(mapping, request);
	        		
	        		actionForm.setLineCount(0);
	        		
	        		criteria = new HashMap();
	        	//	criteria.put(new String("enable"), new Byte((byte)1));
	        		actionForm.setCriteria(criteria);
	        		
	 
	        		
	        		actionForm.clearArFPList();
	        		
	      
	        		System.out.println("OFFSET IS: " + actionForm.getLineCount());
	        		System.out.println("LIMIT IS: " + Constants.GLOBAL_MAX_LINES + 1);
	        		ArrayList newList = ejbFP.getArPeByCriteria(actionForm.getCriteria(), new Integer(actionForm.getLineCount()), 
							new Integer(Constants.GLOBAL_MAX_LINES + 1), actionForm.getOrderBy(), user.getCmpCode());
	        		
	        		actionForm.setDisablePreviousButton(true);
	        		actionForm.setDisableFirstButton(true);
	        		
	        		// check if next should be disabled
	        		if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
	        			
	        			actionForm.setDisableNextButton(true);
	        			actionForm.setDisableLastButton(true);
	        			
	        		} else {
	        			
	        			actionForm.setDisableNextButton(false);
	        			actionForm.setDisableLastButton(false);	
	        			
	        			//remove last record
	        			newList.remove(newList.size() - 1);
	        			
	        		}
	        		
	        		i = newList.iterator();
	        		
	        		while (i.hasNext()) {
	        			
	        			
	        			ArModPersonelDetails mdetails = (ArModPersonelDetails)i.next();
	 
	        			
	        			ArFindPersonelList arFPList = new ArFindPersonelList(actionForm,
	        					mdetails.getPeCode(),
	        					mdetails.getPeIdNumber(),
	        					mdetails.getPeName(),
	        					mdetails.getPeDescription(),
	        					mdetails.getPeAddress(),
	        					mdetails.getPePtName()
	        					
	        				);
	        			
	        			actionForm.saveArFPList(arFPList);
	        			
	        		}
	        		
	        	} catch (GlobalNoRecordFoundException ex) {
	               
	               // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	
	            } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in ArFindPersonelAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
	            
	            actionForm.setSelectedIdNumber(request.getParameter("selectedIdNumber"));
                actionForm.setSelectedName(request.getParameter("selectedName"));
                actionForm.setSelectedPersonelEntered(request.getParameter("selectedPersonelEntered"));
	          
	            return mapping.findForward("arFindPersonelChild");
	          
	        }

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

      	e.printStackTrace();  
      	if (log.isInfoEnabled()) {

             log.info("Exception caught in ArFindPersonelAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          
          return mapping.findForward("cmnErrorPage");

       }

    }
}