package com.struts.ar.findpersonel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class ArFindPersonelForm extends ActionForm implements Serializable {

   private String idNumber = null;
   private String name = null;
   private String description = null;
   private String address = null;
  
   private String personelType = null;
   private ArrayList personelTypeList = new ArrayList();



   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String pageState = new String();
   private ArrayList arFPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private String selectedIdNumber = null;
   private String selectedName = null;
   private String selectedPersonelEntered = null;
   private String selectedLocation = null;
   private String sourceType = null;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();
 
   
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ArFindPersonelList getArFPByIndex(int index) {

      return((ArFindPersonelList)arFPList.get(index));

   }

   public Object[] getArFPList() {

      return arFPList.toArray();

   }

   public int getArFPListSize() {

      return arFPList.size();

   }

   public void saveArFPList(Object newArFPList) {

	   arFPList.add(newArFPList);

   }

   public void clearArFPList() {
   	
      arFPList.clear();
      
   }

   public void setRowSelected(Object selectedArFPList, boolean isEdit) {

      this.rowSelected = arFPList.indexOf(selectedArFPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getIdNumber() {

      return idNumber;

   }

   public void setIdNumber(String idNumber) {

      this.idNumber = idNumber;

   }

   public String getName() {
   	
      return name;
   
   }

   public void setName(String name) {
   
      this.name = name;
   
   }
   
  
   
   public String getAddress() {
	   	
	      return address;
	   
	   }

   public void setAddress(String address) {
   
      this.address = address;
   
   }
   
   public String getDescription() {
	   	
	      return description;
	   
	   }

	public void setDescription(String description) {
	
	   this.description = description;
	
	}
   
   public String getPersonelType() {

      return personelType;

   }

   public void setPersonelType(String personelType) {

      this.personelType = personelType;

   }                         

   public ArrayList getPersonelTypeList() {

      return personelTypeList;

   }

   public void setPersonelTypeList(String personelType) {

	   personelTypeList.add(personelType);

   }
   
   public void clearPersonelTypeList() {

	   personelTypeList.clear();
	   personelTypeList.add(Constants.GLOBAL_BLANK);
      
   }

  
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
  
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
  
   public String getSelectedIdNumber() {
   	
   	  return selectedIdNumber;
   	
   }
   
   public void setSelectedIdNumber(String selectedIdNumber) {
   	
   	  this.selectedIdNumber = selectedIdNumber;
   	
   }
   
   public String getSelectedName() {
   	
   	  return selectedName;
   	
   }
   
   public void setSelectedName(String selectedName) {
   	
   	  this.selectedName = selectedName;
   	
   }
   
   public String getSelectedPersonelEntered() {
   	
   	  return selectedPersonelEntered;
   	
   }
   
   public void setSelectedPersonelEntered(String selectedPersonelEntered) {
   	
   	  this.selectedPersonelEntered = selectedPersonelEntered;
   	
   }
   
   public String getSelectedLocation() {
   	
   	  return selectedLocation;
   	
   }
   
   public void setSelectedLocation(String selectedLocation) {
   	
   	  this.selectedLocation = selectedLocation;
   	
   }
   
   
    public String getSourceType() {
   	
   	  return sourceType;
   	
   }
   
   public void setSourceType(String sourceType) {
   	
   	  this.sourceType = sourceType;
   	
   }
   
   public String getOrderBy() {
	   return orderBy;
   }
   
   
   public void setOrderBy(String orderBy) {
	   this.orderBy = orderBy;
   }
	
   
   public ArrayList getOrderByList() {
	   	
	   	  return orderByList;
	   	  
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AR_FP_ORDER_BY_ID_NUMBER);
	      orderByList.add(Constants.AR_FP_ORDER_BY_PERSONEL_NAME);
	  
	  }     
	  

      
      showDetailsButton = null;
	  hideDetailsButton = null;
	  
 	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

      }
      
      return errors;

   }
}