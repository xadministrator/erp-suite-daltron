package com.struts.ar.findpersonel;

import java.io.Serializable;

public class ArFindPersonelList implements Serializable {

   private Integer peCode = null;
   private String idNumber = null;
   private String name = null;
   private String description = null;
   private String address = null;
   private String personelType = null;



   private String openButton = null;
       
   private ArFindPersonelForm parentBean;
    
   public ArFindPersonelList(ArFindPersonelForm parentBean,
	  Integer peCode,  
	  String idNumber,  
	  String name,
	  String description,
	  String address,
	  String personelType) {

      this.parentBean = parentBean;
      this.peCode = peCode;
      this.idNumber = idNumber;
      this.name = name;
      this.description = description;
      this.address = address;
      this.personelType = personelType;
      
   }

	public Integer getPeCode() {
		return peCode;
	}
	
	public String getIdNumber() {
		return idNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getPersonelType() {
		return personelType;
	}
	
	public String getOpenButton() {
		return openButton;
	}
	
	public void setPeCode(Integer peCode) {
		this.peCode = peCode;
	}
	
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public void setPersonelType(String personelType) {
		this.personelType = personelType;
	}
	
	public void setOpenButton(String openButton) {
		
		parentBean.setRowSelected(this, false);

	}

   
      
}