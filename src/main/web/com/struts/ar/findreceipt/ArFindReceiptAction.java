package com.struts.ar.findreceipt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArFindReceiptController;
import com.ejb.txn.ArFindReceiptControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModReceiptDetails;

public final class ArFindReceiptAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Receipt if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArFindReceiptAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArFindReceiptForm actionForm = (ArFindReceiptForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_FIND_RECEIPT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arFindReceipt");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize ArFindReceiptController EJB
*******************************************************/

         ArFindReceiptControllerHome homeFR = null;
         ArFindReceiptController ejbFR = null;

         try {

            homeFR = (ArFindReceiptControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArFindReceiptControllerEJB", ArFindReceiptControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArFindReceiptAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFR = homeFR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArFindReceiptAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
 /*******************************************************
     Call ArFindReceiptController EJB
     getGlFcPrecisionUnit
     getArPrfDefaultReceiptType
     getAdPrfEnableArReceiptBatch
     getAdPrfEnableInvShift
     getAdPrfArUseCustomerPulldown
  *******************************************************/

         short precisionUnit = 0;
         boolean enableReceiptBatch = false;
         boolean enableShift = false;
         String receiptType = null;
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbFR.getGlFcPrecisionUnit(user.getCmpCode());
            receiptType = ejbFR.getArPrfDefaultReceiptType(user.getCmpCode());
            enableReceiptBatch = Common.convertByteToBoolean(ejbFR.getAdPrfEnableArReceiptBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableReceiptBatch);
            enableShift = Common.convertByteToBoolean(ejbFR.getAdPrfEnableInvShift(user.getCmpCode()));
            actionForm.setShowShift(enableShift);
            useCustomerPulldown = Common.convertByteToBoolean(ejbFR.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
           
                        	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArFindReceiptAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ar FR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arFindReceipt"));
	     
/*******************************************************
   -- Ar FR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arFindReceipt"));                         

/*******************************************************
    -- Ar FR First Action --
*******************************************************/ 
	        
	     } else if (request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);	        
	        
/*******************************************************
   -- Ar FR Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ar FR Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Ar FR Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {

            // create criteria 
            
            if (request.getParameter("goButton") != null) {

	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getShift())) {
	        		
	        		criteria.put("shift", actionForm.getShift());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReceiptType())) {
	        		
	        		criteria.put("receiptType", actionForm.getReceiptType());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}	        	

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReceiptNumberFrom())) {
	        		
	        		criteria.put("receiptNumberFrom", actionForm.getReceiptNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptNumberTo())) {
	        		
	        		criteria.put("receiptNumberTo", actionForm.getReceiptNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptReferenceNumberFrom())) {
	        		
	        		criteria.put("receiptReferenceNumberFrom", actionForm.getReceiptReferenceNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptReferenceNumberTo())) {
	        		
	        		criteria.put("receiptReferenceNumberTo", actionForm.getReceiptReferenceNumberTo());
	        		
	        	}
	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	
	        	if (actionForm.getReceiptVoid()) {	        	
		        	   		        		
	        		criteria.put("receiptVoid", new Byte((byte)1));
                
                }
                
                if (!actionForm.getReceiptVoid()) {
                	
                	criteria.put("receiptVoid", new Byte((byte)0));
                	
                }
	        
                
                if (actionForm.getComputeRatio()) {	        	
		        		
	        		criteria.put("computeRatio", "YES");
                
                }
                
                if (!actionForm.getComputeRatio()) {
                	
                	criteria.put("computeRatio", "NO");
                	
                }
                
	        	// save criteria 
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	System.out.println("size" + actionForm.getCriteria());
            	/*int size = ejbFR.getArRctByCriteria(actionForm.getCriteria(), actionForm.getOrderBy(), actionForm.getLineCount(), 
            			new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()), 
            			user.getCmpCode()).intValue();*/
            	int size = ejbFR.getArRctSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
                
            	//System.out.println(size);//String ORDER_BY, Integer OFFSET, Integer LIMIT
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		System.out.println("AR Receipt action line 342");
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		System.out.println("AR Receipt action line 346");
            	}
            	
            }
            
            try {
            	
            	actionForm.clearArFRList();
            	System.out.println("AR Receipt action line 354");
            	ArrayList list = ejbFR.getArRctByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	        	   System.out.println("AR Receipt action line 363");
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	        	   System.out.println("AR Receipt action line 368");
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	        	   System.out.println("AR Receipt action line 376");
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	        	   System.out.println("AR Receipt action line 381");
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
            		System.out.println("AR Receipt action line 395");
            		ArFindReceiptList arFRList = new ArFindReceiptList(actionForm,
            		    mdetails.getRctCode(),
            		    mdetails.getRctType(),
            		    mdetails.getRctCstName(),
            		    mdetails.getRctBaName(),
            		    Common.convertSQLDateToString(mdetails.getRctDate()),
            		    mdetails.getRctNumber(),
            		    mdetails.getRctReferenceNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit),
            		    Common.convertDoubleToStringMoney(mdetails.getRctCreditedBalance(), precisionUnit),
            		    new Double(mdetails.getRctRatio()));
            		    
            		actionForm.saveArFRList(arFRList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findReceipt.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindReceiptAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arFindReceipt");

            }
                    
            if(user.getCompany().toLowerCase().equals("tinderbox")){
            	actionForm.setToDefaultCurrentDate(true);
            }else{
            	actionForm.setToDefaultCurrentDate(false);
            }
            
            actionForm.reset(mapping, request);
            actionForm.setComputeRatio(false);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("arFindReceipt"));

/*******************************************************
   -- Ar FR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar FR Open Action --
*******************************************************/

         } else if (request.getParameter("arFRList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             ArFindReceiptList arFRList =
                actionForm.getArFRByIndex(actionForm.getRowSelected());
                
             String path = null;
             
             if (arFRList.getReceiptType().equals("COLLECTION")) {
          
	  	         path = "/arReceiptEntry.do?forward=1" +
				     "&receiptCode=" + arFRList.getReceiptCode();
		      
		     } else { // misc receipt
		     	
		     	 path = "/arMiscReceiptEntry.do?forward=1" +
				     "&receiptCode=" + arFRList.getReceiptCode();
				     
		     } 
		     
			 return(new ActionForward(path));

/*******************************************************
   -- Ar FR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearArFRList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseCustomerPulldown()) {

                	actionForm.clearCustomerCodeList();
                	
            		list = ejbFR.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerCodeList((String)i.next());
            				
            			}
            			
            		}            	
            		
            	}

            	actionForm.clearBankAccountList();
            	
            	list = ejbFR.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbFR.getArOpenRbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearShiftList();
            	
            	list = ejbFR.getAdLvInvShiftAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            	
            		actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);
            	
            	} else {
            	
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            		
            			actionForm.setShiftList((String)i.next());
            		
            		}
            	
            	}
            	
            	// check to see if COLLECTION and/or MISC is checked on the current user/responsbility
            	// if so, add COLLECTION or MISC
            	
            	actionForm.clearReceiptTypeList();
            	if(Common.getUserPermission(user, Constants.AR_RECEIPT_ENTRY_ID) != null)
            	{
						actionForm.setReceiptTypeList("COLLECTION");
                }
            	if(Common.getUserPermission(user, Constants.AR_MISC_RECEIPT_ENTRY_ID) != null)
                {
						actionForm.setReceiptTypeList("MISC");
                }
					
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArFindReceiptAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            
            
            if (receiptType.equals("LAST USED") && actionForm.getReceiptType() == null) {
            	
            	actionForm.setReceiptType("COLLECTION");
            	
            } else if (receiptType.equals("COLLECTION") || receiptType.equals("MISC")) {
            
            	actionForm.setReceiptType(receiptType);
            	
            }
	        
            if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {

            	actionForm.setBatchName(Constants.GLOBAL_BLANK);
            	actionForm.setCustomerCode(Constants.GLOBAL_BLANK);
            	actionForm.setBankAccount(Constants.GLOBAL_BLANK);
            	actionForm.setReceiptVoid(false);
            	actionForm.setDateFrom(null);
            	actionForm.setDateTo(null);
            	actionForm.setReceiptNumberFrom(null);
            	actionForm.setReceiptNumberTo(null); 
            	actionForm.setPosted(Constants.GLOBAL_NO);
            	actionForm.setApprovalStatus("DRAFT");
            	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
            	
            	actionForm.setLineCount(0);
                actionForm.setDisableNextButton(true);
                actionForm.setDisablePreviousButton(true);
                actionForm.setDisableFirstButton(true);
                actionForm.setDisableLastButton(true);
                
                if(user.getCompany().toLowerCase().equals("tinderbox")){
                	actionForm.setToDefaultCurrentDate(true);
                }else{
                	actionForm.setToDefaultCurrentDate(false);
                }
                actionForm.reset(mapping, request);
            	
                actionForm.setComputeRatio(true);
                
                HashMap criteria = new HashMap();
                criteria.put("receiptType", actionForm.getReceiptType());
                criteria.put("receiptVoid", new Byte((byte)0));
                criteria.put("posted", actionForm.getPosted());
                criteria.put("approvalStatus", actionForm.getApprovalStatus());                
                
                actionForm.setCriteria(criteria);
                
            	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
            	
            	if(request.getParameter("findDraft") != null) {
	            	
	            	return new ActionForward("/arFindReceipt.do?goButton=1");
	            	
	            }
	          
            } else {

            	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("receiptVoid")) {
            		
            		Byte b = (Byte)c.get("receiptVoid");
            		
            		actionForm.setReceiptVoid(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	try {
                	
                	actionForm.clearArFRList();
                	
                	ArrayList newList = ejbFR.getArRctByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1),
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
                	// check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	newList.remove(newList.size() - 1);
    	           	
    	           }
    	           
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
                		ArModReceiptDetails mdetails = (ArModReceiptDetails)j.next();
          		
                		ArFindReceiptList arFRList = new ArFindReceiptList(actionForm,
                		    mdetails.getRctCode(),
                		    mdetails.getRctType(),
                		    mdetails.getRctCstName(),
                		    mdetails.getRctBaName(),
                		    Common.convertSQLDateToString(mdetails.getRctDate()),
                		    mdetails.getRctNumber(),
                		    mdetails.getRctReferenceNumber(),
                		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit),
                		    Common.convertDoubleToStringMoney(mdetails.getRctCreditedBalance(), precisionUnit),
                		    new Double(mdetails.getRctRatio()));
                		    
                		actionForm.saveArFRList(arFRList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findReceipt.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in ArFindReceiptAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }
                        
            return(mapping.findForward("arFindReceipt"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArFindReceiptAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}