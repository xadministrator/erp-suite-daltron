package com.struts.ar.findreceipt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArFindReceiptForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String shift = null;
   private ArrayList shiftList = new ArrayList();
   private String receiptType = null;
   private ArrayList receiptTypeList = new ArrayList();
   private boolean receiptVoid = false;
   private String customerCode = null;
   private ArrayList customerCodeList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String receiptNumberFrom = null;
   private String receiptNumberTo = null;
   private String receiptReferenceNumberFrom = null;
   private String receiptReferenceNumberTo = null;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;
   private boolean computeRatio;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private boolean showShift = false;
   private String pageState = new String();
   private ArrayList arFRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   private boolean useCustomerPulldown = true;
   
   private boolean toDefaultCurrentDate = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public ArFindReceiptList getArFRByIndex(int index) {

      return((ArFindReceiptList)arFRList.get(index));

   }

   public Object[] getArFRList() {

      return arFRList.toArray();

   }

   public int getArFRListSize() {

      return arFRList.size();

   }

   public void saveArFRList(Object newArFRList) {

      arFRList.add(newArFRList);

   }

   public void clearArFRList() {
   	
      arFRList.clear();
      
   }

   public void setRowSelected(Object selectedArFRList, boolean isEdit) {

      this.rowSelected = arFRList.indexOf(selectedArFRList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
   	
	  return(batchName);
	  
   }

   public void setBatchName(String batchName){
   	
	  this.batchName = batchName;
	  
   }

   public ArrayList getBatchNameList() {
   	
	  return(batchNameList);
	  
   }

   public void setBatchNameList(String batchName){
   	
	  batchNameList.add(batchName);
	  
   }

   public void clearBatchNameList(){
   	
	  batchNameList.clear();	  
	  batchNameList.add(Constants.GLOBAL_BLANK);
	  
   }
   
   public String getShift() {
   	
   	  return shift;
   	
   }
   
   public void setShift(String shift) {
   	
   	  this.shift = shift;
   	
   }
   
   public ArrayList getShiftList() {
   	
   	  return shiftList;
   	
   }
   
   public void setShiftList(String shift) {
   	
   	  shiftList.add(shift);
   	
   }
   
   public void clearShiftList() {   	 
   	
   	  shiftList.clear();
   	  shiftList.add(Constants.GLOBAL_BLANK);
   	
   }

   public String getReceiptType() {
   	
   	  return receiptType;
   	  
   }
   
   public void setReceiptType(String receiptType) {
   	
   	  this.receiptType = receiptType;
   	  
   }
   
   public ArrayList getReceiptTypeList() {

      return receiptTypeList;

   }
   
   public void setReceiptTypeList(String receiptTypeList) {
	   
	   this.receiptTypeList.add(receiptTypeList);
	   
   }
   
   public void clearReceiptTypeList() {
	   
	   receiptTypeList.clear();
	   receiptTypeList.add(Constants.GLOBAL_BLANK);
   }
   
   public boolean getReceiptVoid() {

      return receiptVoid;

   }

   public void setReceiptVoid(boolean receiptVoid) {

      this.receiptVoid = receiptVoid;

   }

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }
   
   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getBankAccount() {

      return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

      this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

      return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

      bankAccountList.add(bankAccount);

   }
   
   public void clearBankAccountList() {

      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }

   public String getReceiptNumberFrom() {

      return receiptNumberFrom;

   }

   public void setReceiptNumberFrom(String receiptNumberFrom) {

      this.receiptNumberFrom = receiptNumberFrom;

   }                         

   public String getReceiptNumberTo() {

      return receiptNumberTo;

   }

   public void setReceiptNumberTo(String receiptNumberTo) {

      this.receiptNumberTo = receiptNumberTo;

   }
   
   
   
   public String getReceiptReferenceNumberFrom() {

	      return receiptReferenceNumberFrom;

	   }

	   public void setReceiptReferenceNumberFrom(String receiptReferenceNumberFrom) {

	      this.receiptReferenceNumberFrom = receiptReferenceNumberFrom;

	   }                         

	   public String getReceiptReferenceNumberTo() {

	      return receiptReferenceNumberTo;

	   }

	   public void setReceiptReferenceNumberTo(String receiptReferenceNumberTo) {

	      this.receiptReferenceNumberTo = receiptReferenceNumberTo;

	   }

   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }
   
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }       
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getShowShift() {
   	
   	   return showShift;
   	
   }
   
   public void setShowShift(boolean showShift) {
   	
   	   this.showShift = showShift;
   	
   }
   
   public boolean getUseCustomerPulldown() {
   	
   	  return useCustomerPulldown;
   	  
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	  this.useCustomerPulldown = useCustomerPulldown;
   	  
   }
   
   public boolean getComputeRatio() {
	   
	   return computeRatio;
	   
   }

   public void setComputeRatio(boolean computeRatio) {
	   
	   this.computeRatio = computeRatio;
	   
   }
   
   public boolean getToDefaultCurrentDate() {
	
	   return toDefaultCurrentDate;
	   
   }

   public void setToDefaultCurrentDate(boolean toDefaultCurrentDate) {
	   
	   this.toDefaultCurrentDate = toDefaultCurrentDate;
	   
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  receiptVoid = false;
   	  
      //receiptTypeList.clear();
      //receiptTypeList.add(Constants.AR_FR_RECEIPT_TYPE_COLLECTION);
      //receiptTypeList.add(Constants.AR_FR_RECEIPT_TYPE_MISC);       
      
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
          
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add(Constants.GLOBAL_YES);
      postedList.add(Constants.GLOBAL_NO);
      
      if(toDefaultCurrentDate){
    	  dateFrom = Common.convertSQLDateToString(new Date());
          dateTo = Common.convertSQLDateToString(new Date());
      }else{
    	  dateFrom = null;
          dateTo = null;
      }
      
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AR_FR_ORDER_BY_BANK_ACCOUNT);
	      orderByList.add(Constants.AR_FR_ORDER_BY_CUSTOMER_CODE);
	      orderByList.add(Constants.AR_FR_ORDER_BY_RECEIPT_NUMBER);
	         
	  }     
	  
      showDetailsButton = null;
	  hideDetailsButton = null;
	  computeRatio = false;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

         if (Common.validateRequired(receiptType)) {

            errors.add("receiptType",
               new ActionMessage("findReceipt.error.receiptTypeRequired"));

         }

      	
         if (!Common.validateDateFormat(dateFrom)) {

            errors.add("dateFrom",
               new ActionMessage("findReceipt.error.dateFromInvalid"));

         }         
         
         if (!Common.validateDateFormat(dateTo)) {

            errors.add("dateTo",
               new ActionMessage("findReceipt.error.dateToInvalid"));

         }                  

      }
      
      return errors;

   }
}