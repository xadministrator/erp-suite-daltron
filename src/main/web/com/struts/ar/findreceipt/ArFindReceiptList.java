package com.struts.ar.findreceipt;

import java.io.Serializable;

public class ArFindReceiptList implements Serializable {

   private Integer receiptCode = null;
   private String receiptType = null;
   private String customerName = null;
   private String bankAccount = null;
   private String date = null;
   private String receiptNumber = null;
   private String receiptReferenceNumber = null;
   private String amount = null;
   private String creditedBalance = null;
   private Double ratio = null;

   private String openButton = null;
       
   private ArFindReceiptForm parentBean;
    
   public ArFindReceiptList(ArFindReceiptForm parentBean,
	  Integer receiptCode,
	  String receiptType,  
	  String customerName,  
	  String bankAccount,  
	  String date,  
	  String receiptNumber,
	  String receiptReferenceNumber,
	  String amount,
	  String creditedBalance,
	  Double ratio) {

      this.parentBean = parentBean;
      this.receiptCode = receiptCode;
      this.receiptType = receiptType;
      this.customerName = customerName;
      this.bankAccount = bankAccount;
      this.date = date;
      this.receiptNumber = receiptNumber;
      this.receiptReferenceNumber = receiptReferenceNumber;
      this.amount = amount;
      this.creditedBalance = creditedBalance;
      this.ratio = ratio;
      
   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }
   
   public Integer getReceiptCode() {
   	
   	  return receiptCode;
   	  
   }
   
   public String getReceiptType() {
   	
   	  return receiptType;
   	  
   }

   public String getCustomerName() {

      return customerName;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getReceiptNumber() {
   	
   	  return receiptNumber;
   	 
   }
   
   public String getReceiptReferenceNumber() {
	   	
	   	  return receiptReferenceNumber;
	   	 
	   }

   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public String getCreditedBalance() {
	   	
   	  return creditedBalance;
   	  
   }
   
   public Double getRatio() {
	   
	   return ratio;
	   
   }
      
}