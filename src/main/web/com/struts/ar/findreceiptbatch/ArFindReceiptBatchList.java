package com.struts.ar.findreceiptbatch;

import java.io.Serializable;

public class ArFindReceiptBatchList implements Serializable {

   private Integer receiptBatchCode = null;
   private String batchName = null;
   private String description = null;
   private String status = null;
   private String dateCreated = null;
   private String createdBy = null;

   private String openButton = null;
    
   private ArFindReceiptBatchForm parentBean;
    
   public ArFindReceiptBatchList(ArFindReceiptBatchForm parentBean,
      Integer receiptBatchCode,
      String batchName,
      String description,
      String status,
      String dateCreated,
      String createdBy){

      this.parentBean = parentBean;
      this.receiptBatchCode = receiptBatchCode;
      this.batchName = batchName;
      this.description = description;
      this.status = status;
      this.dateCreated = dateCreated;
      this.createdBy = createdBy;
      
   }

   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }

   public Integer getReceiptBatchCode(){
      return(receiptBatchCode);
   }

   public String getBatchName(){
      return(batchName);
   }

   public String getDescription(){
      return(description);
   }

   public String getStatus(){
      return(status);
   }
   
   public String getDateCreated(){
      return(dateCreated);
   }

   public String getCreatedBy(){
      return(createdBy);
   }
   
}
