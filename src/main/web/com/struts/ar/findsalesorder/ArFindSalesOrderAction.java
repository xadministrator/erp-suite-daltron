package com.struts.ar.findsalesorder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArFindSalesOrderController;
import com.ejb.txn.ArFindSalesOrderControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModSalesOrderDetails;

public class ArFindSalesOrderAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		HttpSession session = request.getSession();

		try {

/*******************************************************
 	Check if user has a session
*******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("ArFindSalesOrderAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}

			} else {

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			ArFindSalesOrderForm actionForm = (ArFindSalesOrderForm)form;
			String frParam = null;
			boolean isChild = false;

			if (request.getParameter("child") == null) {

		         	frParam = Common.getUserPermission(user, Constants.AR_FIND_SALES_ORDER_ID);

		    } else {

		         	frParam = Constants.FULL_ACCESS;
		         	isChild = true;
		    }



			if (frParam != null) {

				if (frParam.trim().equals(Constants.FULL_ACCESS)) {

					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));
						return mapping.findForward("arFindSalesOrder");

					}

				}

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}

/*******************************************************
 	Initialize ArFindSalesOrderController EJB
*******************************************************/

			ArFindSalesOrderControllerHome homeFSO = null;
			ArFindSalesOrderController ejbFSO = null;

			try {

				homeFSO = (ArFindSalesOrderControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArFindSalesOrderControllerEJB", ArFindSalesOrderControllerHome.class);

			} catch (NamingException e) {

				if (log.isInfoEnabled()) {

					log.info("NamingException caught in ArFindSalesOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}

				return mapping.findForward("cmnErrorPage");

			}

			try {

				ejbFSO = homeFSO.create();

			} catch (CreateException e) {

				if (log.isInfoEnabled()) {

					log.info("CreateException caught in ArFindSalesOrderAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}
				return mapping.findForward("cmnErrorPage");

			}

			ActionErrors errors = new ActionErrors();

/*******************************************************
   Call ArFindSalesOrderController EJB
   getAdPrfArUseCustomerPulldown
*******************************************************/

			boolean useCustomerPulldown = true;

			try {

				useCustomerPulldown = Common.convertByteToBoolean(ejbFSO.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
				actionForm.setUseCustomerPulldown(useCustomerPulldown);

			} catch(EJBException ex) {

		        if (log.isInfoEnabled()) {

		           log.info("EJBException caught in ArFindSalesOrderAction.execute(): " + ex.getMessage() +
		              " session: " + session.getId());
		        }

		        return(mapping.findForward("cmnErrorPage"));

		     }

/*******************************************************
	 -- Ar FSO Show Details Action --
*******************************************************/

			if (request.getParameter("showDetailsButton") != null) {

				actionForm.setTableType(Constants.GLOBAL_DETAILED);

				if (request.getParameter("child") == null) {

					return(mapping.findForward("arFindSalesOrder"));

				} else {

					return(mapping.findForward("arFindSalesOrderChild"));

				}

/*******************************************************
     -- Ar FSO Hide Details Action --
*******************************************************/

			} else if (request.getParameter("hideDetailsButton") != null) {

				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

				if (request.getParameter("child") == null) {

					return(mapping.findForward("arFindSalesOrder"));

				} else {

					return(mapping.findForward("arFindSalesOrderChild"));

				}

/*******************************************************
 -- Ar FSO First Action --
 *******************************************************/

			} else if(request.getParameter("firstButton") != null) {

				actionForm.setLineCount(0);

/*******************************************************
	-- Ar FSO Previous Action --
*******************************************************/

			} else if (request.getParameter("previousButton") != null){

				actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);

/*******************************************************
	-- Ar FSO Next Action --
*******************************************************/

			}else if(request.getParameter("nextButton") != null){

				actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);

			}

/*******************************************************
	-- Ar FSO Go Action --
*******************************************************/

			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
					request.getParameter("lastButton") != null) {

				// create criteria

				if (request.getParameter("goButton") != null) {

					HashMap criteria = new HashMap();

					if (!Common.validateRequired(actionForm.getCustomerCode())) {

	        		   criteria.put("customerCode", actionForm.getCustomerCode());

	        	    }

					if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {

						criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());

					}

					if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {

						criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());

					}

					if (!Common.validateRequired(actionForm.getReferenceNumber())) {

						criteria.put("referenceNumber", actionForm.getReferenceNumber());

					}
					
					System.out.println("doc type: 1" + actionForm.getDocumentType());
					if (!Common.validateRequired(actionForm.getDocumentType())) {

						criteria.put("documentType", actionForm.getDocumentType());

					}
					

					System.out.println("trans type: 1" + actionForm.getTransactionType());
					if (!Common.validateRequired(actionForm.getTransactionType())) {

						criteria.put("transactionType", actionForm.getTransactionType());

					}

					if (!Common.validateRequired(actionForm.getDateFrom())) {

						criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));

					}

					if (!Common.validateRequired(actionForm.getDateTo())) {

						criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));

					}

					if (!Common.validateRequired(actionForm.getApprovalDateFrom())) {

						criteria.put("approvalDateFrom", Common.convertStringToSQLDate(actionForm.getApprovalDateFrom()));

					}

					if (!Common.validateRequired(actionForm.getApprovalDateTo())) {

						criteria.put("approvalDateTo", Common.convertStringToSQLDate(actionForm.getApprovalDateTo()));

					}

					if (!Common.validateRequired(actionForm.getCurrency())) {

	        		   criteria.put("currency", actionForm.getCurrency());

	        	    }

	            	if (request.getParameter("child") == null) {

	            		if (!Common.validateRequired(actionForm.getApprovalStatus())) {

	            			criteria.put("approvalStatus", actionForm.getApprovalStatus());

	            		}

	            		if (actionForm.getSalesOrderVoid()) {

	            			criteria.put("salesOrderVoid", new Byte((byte)1));

	            		}

	            		if (!actionForm.getSalesOrderVoid()) {

	            			criteria.put("salesOrderVoid", new Byte((byte)0));

	            		}

	            		if (!Common.validateRequired(actionForm.getPosted())) {

	            			criteria.put("posted", actionForm.getPosted());

	            		}

	            	} else {

    					criteria.put("salesOrderVoid", new Byte((byte)0));
		            	criteria.put("posted", "YES");

    				}

					// save criteria

					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);

				}

				if (request.getParameter("lastButton") != null) {

	            	int size = ejbFSO.getArSoSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();

	            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {

	            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));

	            	} else {

	            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);

	            	}

	            }

				try {

					actionForm.clearArFSOList();

					ArrayList list = ejbFSO.getArSoByCriteria(actionForm.getCriteria(), isChild,
							new Integer(actionForm.getLineCount()),
							new Integer(Constants.GLOBAL_MAX_LINES + 1),
							actionForm.getOrderBy(),
							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {

						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);

					} else {

						actionForm.setDisablePreviousButton(false);
						actionForm.setDisableFirstButton(false);

					}

					// check if next should be disabled
					if (list.size() <= Constants.GLOBAL_MAX_LINES) {

						actionForm.setDisableNextButton(true);
						actionForm.setDisableLastButton(true);

					} else {

						actionForm.setDisableNextButton(false);
						actionForm.setDisableLastButton(false);

						//remove last record
						list.remove(list.size() - 1);

					}

					Iterator i = list.iterator();

					while (i.hasNext()) {

						ArModSalesOrderDetails mdetails = (ArModSalesOrderDetails)i.next();

						ArFindSalesOrderList apFSOList = new ArFindSalesOrderList(actionForm,
								mdetails.getSoCode(),
								mdetails.getSoDocumentType(),
								mdetails.getSoCstCustomerCode(),
								Common.convertSQLDateToString(mdetails.getSoDate()),
								mdetails.getSoDocumentNumber(),
								mdetails.getSoReferenceNumber(), mdetails.getSoTransactionType(), mdetails.getSoCstName());

						actionForm.saveArFSOList(apFSOList);

					}

				} catch (GlobalNoRecordFoundException ex) {

					// disable prev, next, first & last buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("findSalesOrder.error.noRecordFound"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ApFindSalesOrderAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

					if (request.getParameter("child") == null) {

					    return(mapping.findForward("arFindSalesOrder"));

					} else {

					    return(mapping.findForward("arFindSalesOrderChild"));

					}

				}

				actionForm.reset(mapping, request);

				if (actionForm.getTableType() == null) {

					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

				}

				if (request.getParameter("child") == null) {

				    return(mapping.findForward("arFindSalesOrder"));

				} else {

				    return(mapping.findForward("arFindSalesOrderChild"));

				}

/*******************************************************
	-- Ar FSO Close Action --
*******************************************************/

			} else if (request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

/*******************************************************
	-- Ar FSO Open Action --
*******************************************************/

			} else if (request.getParameter("arFSOList[" +
					actionForm.getRowSelected() + "].openButton") != null) {

				ArFindSalesOrderList arFSOList =
					actionForm.getArFSOByIndex(actionForm.getRowSelected());

				String path = null;

				path = "/arSalesOrderEntry.do?forward=1" +
				"&salesOrderCode=" + arFSOList.getSalesOrderCode();

				return(new ActionForward(path));

			}

/*******************************************************
	-- Ar FSO Load Action --
*******************************************************/

			if (frParam != null) {
				actionForm.clearArFSOList();

	            ArrayList list = null;
	            Iterator i = null;

	            try {

	            	if(actionForm.getUseCustomerPulldown()) {

	            		actionForm.clearCustomerCodeList();

	            		list = ejbFSO.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	            		if (list == null || list.size() == 0) {

	            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

	            		} else {

	            			i = list.iterator();

	            			while (i.hasNext()) {

	            				actionForm.setCustomerCodeList((String)i.next());

	            			}

	            		}

	            	}

	            	actionForm.clearCurrencyList();

	            	list = ejbFSO.getGlFcAll(user.getCmpCode());

	            	if (list == null || list.size() == 0) {

	            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

	            	} else {

	            		i = list.iterator();

	            		while (i.hasNext()) {

	            		    actionForm.setCurrencyList((String)i.next());

	            		}

	            	}

	            	actionForm.clearTransactionTypeList();

	            	list = ejbFSO.getArSoTransactionTypeList(user.getCmpCode());

	            	i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setTransactionTypeList((String)i.next());

            		}

            		
            		

            		actionForm.clearDocumentTypeList();

	            	list = ejbFSO.getArSoDocumentTypeList(user.getCmpCode());

	            	i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setDocumentTypeList((String)i.next());

            		}

            		


	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArFindSalesOrderAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }

				if (request.getParameter("goButton") != null) {

					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

				}


				if (actionForm.getTableType() == null) {

					actionForm.setCustomerCode(Constants.GLOBAL_BLANK);
					actionForm.setSalesOrderVoid(false);
					actionForm.setDocumentType(Constants.GLOBAL_BLANK);
					actionForm.setDocumentNumberFrom(null);
					actionForm.setDocumentNumberTo(null);
					actionForm.setReferenceNumber(null);
					actionForm.setTransactionType(Constants.GLOBAL_BLANK);
					actionForm.setDateFrom(null);
					actionForm.setDateTo(null);
					actionForm.setApprovalDateFrom(null);
					actionForm.setApprovalDateTo(null);
					actionForm.setCurrency(Constants.GLOBAL_BLANK);
					actionForm.setApprovalStatus("DRAFT");
					actionForm.setPosted(Constants.GLOBAL_NO);
					actionForm.setOrderBy("DOCUMENT NUMBER");

					actionForm.setLineCount(0);
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);
					actionForm.setDisableLastButton(true);
					actionForm.reset(mapping, request);

					HashMap criteria = new HashMap();
					criteria.put("approvalStatus", actionForm.getApprovalStatus());
					criteria.put("posted", actionForm.getPosted());
					criteria.put("salesOrderVoid", new Byte((byte)0));
					actionForm.setCriteria(criteria);

					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

				} else {

					HashMap c = actionForm.getCriteria();

	            	if(c.containsKey("salesOrderVoid")) {

	            		Byte b = (Byte)c.get("salesOrderVoid");

	            		actionForm.setSalesOrderVoid(Common.convertByteToBoolean(b.byteValue()));

	            	}

					try {

						actionForm.clearArFSOList();

						ArrayList newList = ejbFSO.getArSoByCriteria(actionForm.getCriteria(), isChild,
									new Integer(actionForm.getLineCount()),
									new Integer(Constants.GLOBAL_MAX_LINES + 1),
									actionForm.getOrderBy(),
									new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {

							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);

						} else {

							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);

						}

						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);

						} else {

							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);

							//remove last record
							newList.remove(newList.size() - 1);

						}

						Iterator j = newList.iterator();
						while (j.hasNext()) {

							ArModSalesOrderDetails mdetails = (ArModSalesOrderDetails)j.next();

							ArFindSalesOrderList arFSOList = new ArFindSalesOrderList(actionForm,
									mdetails.getSoCode(),
									mdetails.getSoDocumentType(),
									mdetails.getSoCstCustomerCode(),
									Common.convertSQLDateToString(mdetails.getSoDate()),
									mdetails.getSoDocumentNumber(),
									mdetails.getSoReferenceNumber(), mdetails.getSoTransactionType(),  mdetails.getSoCstName());

							actionForm.saveArFSOList(arFSOList);

						}

					} catch (GlobalNoRecordFoundException ex) {

						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						actionForm.setDisableLastButton(true);
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("findSalesOrder.error.noRecordFound"));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArFindSalesOrderAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}



				}

				if (request.getParameter("findSalesOrderProcessing")!=null) {

					actionForm.clearArFSOList();
					i = ejbFSO.getArSoForProcessingByBrCode(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).iterator();

					while (i.hasNext()) {

						ArModSalesOrderDetails mdetails = (ArModSalesOrderDetails)i.next();

						ArFindSalesOrderList arFSOList = new ArFindSalesOrderList(actionForm,
								mdetails.getSoCode(),
								mdetails.getSoDocumentType(),
								mdetails.getSoCstCustomerCode(),
								Common.convertSQLDateToString(mdetails.getSoDate()),
								mdetails.getSoDocumentNumber(),
								mdetails.getSoReferenceNumber(), mdetails.getSoTransactionType(), mdetails.getSoCstName());

						actionForm.saveArFSOList(arFSOList);

					}

					return(mapping.findForward("arFindSalesOrder"));

				}

				if (request.getParameter("child") == null) {

					return(mapping.findForward("arFindSalesOrder"));

		        } else {

					try {

		        		actionForm.setLineCount(0);

		        		HashMap criteria = new HashMap();


		            	criteria.put(new String("salesOrderVoid"), new Byte((byte)0));

		            	if(request.getParameter("cmFind")==null) {
		            		criteria.put(new String("posted"), "YES");
		            	}

		        		actionForm.setCriteria(criteria);

						actionForm.clearArFSOList();
		
						ArrayList newList = ejbFSO.getArSoByCriteria(actionForm.getCriteria(), isChild,
								new Integer(actionForm.getLineCount()),
								new Integer(Constants.GLOBAL_MAX_LINES + 1),
								actionForm.getOrderBy(),
								new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						// check if prev should be disabled
						
						System.out.println("pasok 1");
						if (actionForm.getLineCount() == 0) {

							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);

						} else {

							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);

						}

						// check if next should be disabled
						if (newList.size() <= Constants.GLOBAL_MAX_LINES) {

							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);

						} else {

							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);

							//remove last record
							System.out.println("pasok 2");
							if(list.size()>0) {
								newList.remove(list.size() - 1);
							}
							
							System.out.println("pasok 3");

						}

						i = newList.iterator();

						while (i.hasNext()) {

							ArModSalesOrderDetails mdetails = (ArModSalesOrderDetails)i.next();

							ArFindSalesOrderList arFSOList = new ArFindSalesOrderList(actionForm,
									mdetails.getSoCode(),
									mdetails.getSoDocumentType(),
									mdetails.getSoCstCustomerCode(),
									Common.convertSQLDateToString(mdetails.getSoDate()),
									mdetails.getSoDocumentNumber(),
									mdetails.getSoReferenceNumber(), mdetails.getSoTransactionType(), mdetails.getSoCstName());

							actionForm.saveArFSOList(arFSOList);

						}

					} catch (GlobalNoRecordFoundException ex) {

						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);
						actionForm.setDisableLastButton(true);

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArFindSalesOrderAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}

					actionForm.setSelectedSoNumber(request.getParameter("selectedSoNumber"));
	        		return mapping.findForward("arFindSalesOrderChild");

		        }

			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}

	} catch(Exception e) {

/*******************************************************
 	System Failed: Forward to error page
*******************************************************/

			if (log.isInfoEnabled()) {

				log.info("Exception caught in ApFindSalesOrderAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}

			return mapping.findForward("cmnErrorPage");

		}

	}

}
