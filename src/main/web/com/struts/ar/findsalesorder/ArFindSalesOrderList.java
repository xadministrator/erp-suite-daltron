package com.struts.ar.findsalesorder;


public class ArFindSalesOrderList implements java.io.Serializable {

    private Integer salesOrderCode = null;
    private String documentType = null;
	private String customerCode = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String transactionType = null;
	private String customerName = null;

	private String openButton = null;

	private ArFindSalesOrderForm parentBean;

	public ArFindSalesOrderList(ArFindSalesOrderForm parentBean,
			Integer salesOrderCode,
			String documentType,
			String customerCode,
			String date,
			String documentNumber,
			String referenceNumber,
			String transactionType,
			String customerName) {

		this.parentBean = parentBean;
		this.documentType = documentType;
		this.salesOrderCode = salesOrderCode;
		this.customerCode = customerCode;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.transactionType = transactionType;
		this.customerName = customerName;

	}

	public void setOpenButton(String openButton) {

		parentBean.setRowSelected(this, false);

	}

	public Integer getSalesOrderCode() {

		return salesOrderCode;

	}
	
	public String getDocumentType() {
		return documentType;
	}
	

	public String getDocumentNumber() {

		return documentNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public String getTransactionType() {

		return transactionType;

	}

	public String getCustomerCode() {

		return customerCode;

	}

	public String getDate() {

		return date;

	}

	public String getCustomerName() {

		return customerName;

	}

}
