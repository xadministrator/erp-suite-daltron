package com.struts.ar.invoicebatchcopy;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalBatchCopyInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.ArInvoiceBatchCopyController;
import com.ejb.txn.ArInvoiceBatchCopyControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModInvoiceDetails;

public final class ArInvoiceBatchCopyAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArInvoiceBatchCopyAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArInvoiceBatchCopyForm actionForm = (ArInvoiceBatchCopyForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.AR_INVOICE_BATCH_COPY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arInvoiceBatchCopy"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ArInvoiceBatchCopyController EJB
*******************************************************/

         ArInvoiceBatchCopyControllerHome homeIBC = null;
         ArInvoiceBatchCopyController ejbIBC = null;

         try {
            
            homeIBC = (ArInvoiceBatchCopyControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArInvoiceBatchCopyControllerEJB", ArInvoiceBatchCopyControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArInvoiceBatchCopyAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbIBC = homeIBC.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ArInvoiceBatchCopyAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
     Call ArInvoiceBatchCopy EJB
     getGlFcPrecisionUnit
  *******************************************************/
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbIBC.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArInvoiceBatchCopyAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ar IBC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arInvoiceBatchCopy"));
	     
/*******************************************************
   -- Ar IBC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arInvoiceBatchCopy"));  
	        
	     }
         
/*******************************************************
   -- Ar IBC Copy Action --
*******************************************************/

         if (request.getParameter("copyButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           
           ArrayList list = new ArrayList();
           
           for (int i=0; i<actionForm.getArIBCListSize(); i++) {
           
              ArInvoiceBatchCopyList actionList = actionForm.getArIBCByIndex(i);
              
              if (actionList.getCopy()) list.add(actionList.getInvoiceCode());
           
           }
           
           try {
           	
           	    ejbIBC.executeArInvcBatchCopy(list, actionForm.getBatchTo(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	           	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchCopy.error.transactionBatchClose"));
                    
           } catch (GlobalBatchCopyInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchCopy.error.batchCopyInvalid"));  
           
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
   -- Ar IBC Batch From Entered Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isBatchFromEntered"))) {
         
            try {
            
                actionForm.clearArIBCList();
            
                ArrayList list = ejbIBC.getArInvByIbName(actionForm.getBatchFrom(), user.getCmpCode());
                                                
                Iterator i = list.iterator();
                
                while (i.hasNext()) {
                                   
                   ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
            		
            		ArInvoiceBatchCopyList actionList = new ArInvoiceBatchCopyList(actionForm,
            		    mdetails.getInvCode(),
            		    Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
            		    mdetails.getInvCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),                            
            		    Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit));
                       
                   actionForm.saveArIBCList(actionList);
                                                     
                }
                
            } catch (GlobalNoRecordFoundException ex) {
            
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchCopy.error.noInvoiceFound"));                    
            
            } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arInvoiceBatchCopy"));
               
           }
           
           return(mapping.findForward("arInvoiceBatchCopy"));
           
/*******************************************************
   -- Ar IBC Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ar IBC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arInvoiceBatchCopy"));
               
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearArIBCList();
            	actionForm.clearBatchFromList();
            	actionForm.clearBatchToList();
            	
            	list = ejbIBC.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchFromList(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setBatchToList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			String batchName = (String)i.next();
            			
            		    actionForm.setBatchFromList(batchName);
            		    actionForm.setBatchToList(batchName);
            			
            		}
            		            		
            	}
            	            	
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("copyButton") != null) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               
               }
            }

            actionForm.reset(mapping, request);
            
            if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }  
            
            return(mapping.findForward("arInvoiceBatchCopy"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ArInvoiceBatchCopyAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }
}