package com.struts.ar.invoicebatchcopy;

import java.io.Serializable;

public class ArInvoiceBatchCopyList implements Serializable {

   private Integer invoiceCode = null;
   private boolean creditMemo = false;
   private String customerCode = null;
   private String date = null;
   private String invoiceNumber = null;
   private String referenceNumber = null;  
   private String amountDue = null;
   private String amountPaid = null;
   
   private boolean copy = false;
       
   private ArInvoiceBatchCopyForm parentBean;
    
   public ArInvoiceBatchCopyList(ArInvoiceBatchCopyForm parentBean,
      Integer invoiceCode,
      boolean creditMemo,
      String customerCode,
      String date,
      String invoiceNumber,
      String referenceNumber,
      String amountDue,
      String amountPaid) {

      this.parentBean = parentBean;
      this.invoiceCode = invoiceCode;
      this.creditMemo = creditMemo;
      this.customerCode = customerCode;
      this.date = date;
      this.invoiceNumber = invoiceNumber;
      this.referenceNumber = referenceNumber;
      this.amountDue = amountDue;
      this.amountPaid = amountPaid;
      
   }

   public Integer getInvoiceCode() {

      return invoiceCode;

   }
   
   public boolean getCreditMemo() {
   	
   	  return creditMemo;
   	
   }

   public String getCustomerCode() {

      return customerCode;
      
   }

   public String getDate() {
   
      return date;
      
   }
   
   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmountDue() {
   
      return amountDue;
   
   }

   public String getAmountPaid() {
   
      return amountPaid;
   
   }
   
   public boolean getCopy() {
   	
   	  return copy;
   	
   }
   
   public void setCopy(boolean copy) {
   	
   	  this.copy = copy;
   	
   }
   
}