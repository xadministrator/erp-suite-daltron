package com.struts.ar.invoicebatchentry;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.ArInvoiceBatchEntryController;
import com.ejb.txn.ArInvoiceBatchEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArInvoiceBatchDetails;

public final class ArInvoiceBatchEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArInvoiceBatchEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArInvoiceBatchEntryForm actionForm = (ArInvoiceBatchEntryForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.AR_INVOICE_BATCH_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arInvoiceBatchEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ArInvoiceBatchEntryController EJB
*******************************************************/

         ArInvoiceBatchEntryControllerHome homeIB = null;
         ArInvoiceBatchEntryController ejbIB = null;

         try {
            
            homeIB = (ArInvoiceBatchEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArInvoiceBatchEntryControllerEJB", ArInvoiceBatchEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArInvoiceBatchEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbIB = homeIB.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ArInvoiceBatchEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- AR IB Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArInvoiceBatchDetails details = new ArInvoiceBatchDetails();
           
           details.setIbCode(actionForm.getInvoiceBatchCode());
           details.setIbName(actionForm.getBatchName());
           details.setIbDescription(actionForm.getDescription());
           details.setIbStatus(actionForm.getStatus());
           details.setIbType(actionForm.getType());
           
           if (actionForm.getInvoiceBatchCode() == null) {
           	
           	   details.setIbCreatedBy(user.getUserName());
	           details.setIbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbIB.saveArIbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.transactionBatchClose"));           	                      	
                    
           } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.saveRecordAlreadyAssigned"));            
           
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }           
           
/*******************************************************
   -- AR IB Invoice Action --
*******************************************************/

         } else if (request.getParameter("invoiceButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArInvoiceBatchDetails details = new ArInvoiceBatchDetails();
           
           details.setIbCode(actionForm.getInvoiceBatchCode());
           details.setIbName(actionForm.getBatchName());
           details.setIbDescription(actionForm.getDescription());
           details.setIbStatus(actionForm.getStatus());
           details.setIbType(actionForm.getType());
           
           if (actionForm.getInvoiceBatchCode() == null) {
           	
           	   details.setIbCreatedBy(user.getUserName());
	           details.setIbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbIB.saveArIbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.transactionBatchClose"));           	                      	
                    
           } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.saveRecordAlreadyAssigned"));            
           
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }     
           
           if (errors.isEmpty()) {
     	   
	     	   String path = null;
	     	
	     	   if (actionForm.getType().equals("INVOICE")) {
	     	   	
	         	 path = "/arInvoiceEntry.do?forwardBatch=1" +
				         "&batchName=" + actionForm.getBatchName();	
	     	                      	   	
	     	   } else {
	     	
	     	   	 path = "/arCreditMemoEntry.do?forwardBatch=1" +
			         	  "&batchName=" + actionForm.getBatchName();                   	   	 
	     	   	
	     	   }
	     	                      	   
	     	   return(new ActionForward(path));
	     	 
	        }

           
/*******************************************************
   -- AR IB Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbIB.deleteArIbEntry(actionForm.getInvoiceBatchCode(), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.recordAlreadyDeleted"));
                    
            } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.deleteRecordAlreadyAssigned"));
                    
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- AR IB Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- AR IB Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arInvoiceBatchEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {            	
            	
            	if (request.getParameter("forward") != null) {
            		            			            	    
            		actionForm.setInvoiceBatchCode(new Integer(request.getParameter("invoiceBatchCode")));            		            		
            		
            		ArInvoiceBatchDetails details = ejbIB.getArIbByIbCode(actionForm.getInvoiceBatchCode(), user.getCmpCode());
            		
            		actionForm.setBatchName(details.getIbName());
            		actionForm.setDescription(details.getIbDescription());
            		actionForm.setCreatedBy(details.getIbCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(details.getIbDateCreated()));
            		actionForm.setStatus(details.getIbStatus());
            		actionForm.setType(details.getIbType());            		
            					         
			        this.setFormProperties(actionForm);
			         
			        return (mapping.findForward("arInvoiceBatchEntry"));   			         
			         
            		
            	}            	
	                    		           			   	
            } catch(GlobalNoRecordFoundException ex) {            	            	
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invoiceBatchEntry.error.invoiceBatchAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("saveButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setInvoiceBatchCode(null);          
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setStatus("OPEN");
            actionForm.setType("INVOICE");
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("arInvoiceBatchEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ArInvoiceBatchEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ArInvoiceBatchEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
							
			if (actionForm.getInvoiceBatchCode() == null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(false);
				
			} else {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(true);
				
			}									
						
		} else {
						
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			
			
		}
				    
	}
	
}