package com.struts.ar.invoicebatchprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArInvoiceBatchPrintController;
import com.ejb.txn.ArInvoiceBatchPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModInvoiceDetails;

public final class ArInvoiceBatchPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArInvoiceBatchPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArInvoiceBatchPrintForm actionForm = (ArInvoiceBatchPrintForm)form;
         
         actionForm.setInvoiceReport(null);
         actionForm.setCreditMemoReport(null);
         actionForm.setInvoiceEditListReport(null);
         actionForm.setDeliveryReceiptReport(null);
         actionForm.setInvoiceCodeList(null);
         
         String frParam = Common.getUserPermission(user, Constants.AR_INVOICE_BATCH_PRINT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arInvoiceBatchPrint");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArInvoiceBatchPrintController EJB
*******************************************************/

         ArInvoiceBatchPrintControllerHome homeIBP = null;
         ArInvoiceBatchPrintController ejbIBP = null;

         try {
          
            homeIBP = (ArInvoiceBatchPrintControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArInvoiceBatchPrintControllerEJB", ArInvoiceBatchPrintControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArInvoiceBatchPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbIBP = homeIBP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArInvoiceBatchPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

 /*******************************************************
     Call ArInvoiceBatchPrintController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableArInvoiceBatch
     getAdPrfArUseCustomerPulldown
  *******************************************************/
         
         short precisionUnit = 0;
         boolean enableInvoiceBatch = false; 
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbIBP.getGlFcPrecisionUnit(user.getCmpCode());
            enableInvoiceBatch = Common.convertByteToBoolean(ejbIBP.getAdPrfEnableArInvoiceBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableInvoiceBatch);
            useCustomerPulldown = Common.convertByteToBoolean(ejbIBP.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArInvoiceBatchPrintAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ar IBP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arInvoiceBatchPrint"));
	     
/*******************************************************
   -- Ar IBP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arInvoiceBatchPrint"));         

/*******************************************************
   -- Ar IBP Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ar IBP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Ar IBP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("refresh") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();   
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	} 
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {
	        		
	        		criteria.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {
	        		
	        		criteria.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());
	        		
	        	}
	        	

	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getPaymentStatus())) {
	        		
	        		criteria.put("paymentStatus", actionForm.getPaymentStatus());
	        		
	        	}
	        	
	        	if (actionForm.getCreditMemo()) {	        	
		        	   		        		
	        		criteria.put("creditMemo", new Byte((byte)1));
                
                }
                
                if (!actionForm.getCreditMemo()) {
                	
                	criteria.put("creditMemo", new Byte((byte)0));
                	
                }
                
                if (actionForm.getInvoiceVoid()) {	        	
		        	   		        		
	        		criteria.put("invoiceVoid", new Byte((byte)1));
                
                }
                
                if (!actionForm.getInvoiceVoid()) {
                	
                	criteria.put("invoiceVoid", new Byte((byte)0));
                	
                }
                	 
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbIBP.getArInvByCriteria(actionForm.getCriteria(),                	    
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE), 
                	    actionForm.getOrderBy(), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ArInvoiceBatchPrintAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	        	
	     	}
            
            try {
            	
            	actionForm.clearArIBPList();
            	
            	ArrayList list = ejbIBP.getArInvByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
            		
            		ArInvoiceBatchPrintList arIBPList = new ArInvoiceBatchPrintList(actionForm,
            		    mdetails.getInvCode(),
            		    Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
            		    mdetails.getInvCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),                            
            		    Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit));
            		    
            		actionForm.saveArIBPList(arIBPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("invoiceBatchPrint.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            System.out.println(actionForm.getCreditMemo());
            
            if (actionForm.getCreditMemo())
            	actionForm.setIsCreditMemo(true);
            else
            	actionForm.setIsCreditMemo(false);

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoiceBatchPrint");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("arInvoiceBatchPrint")); 

/*******************************************************
   -- Ar IBP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar IBP Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
  	         // get selected invoices
  	         
  	        ArrayList invoiceCodeList = new ArrayList();
  	        
  	        boolean isCreditMemo = false;
  	        
  	        int j = 0;
                        
		    for(int i=0; i<actionForm.getArIBPListSize(); i++) {
		    
		       ArInvoiceBatchPrintList actionList = actionForm.getArIBPByIndex(i);
		    	
               if (actionList.getPrint()) {
               	
               	   isCreditMemo = actionList.getCreditMemo();
               	   invoiceCodeList.add(actionList.getInvoiceCode());
               	   
               }
	           
	       }	
	       
	       if (invoiceCodeList.size() > 0) {
	       	
	       	   if (!isCreditMemo) {
	       	   	
	       	   	  actionForm.setInvoiceReport(Constants.STATUS_SUCCESS);	      
	       	   	
	       	   } else {
	       	   	
	       	   	  actionForm.setCreditMemoReport(Constants.STATUS_SUCCESS);	      
	       	   	
	       	   }       	
	       	   
	       	   actionForm.setInvoiceCodeList(invoiceCodeList);
	       	   	       	   	       	
	       }
	       
	       
	       try {
            	
            	actionForm.clearArIBPList();
            	
            	ArrayList list = ejbIBP.getArInvByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
            		
            		ArInvoiceBatchPrintList arIBPList = new ArInvoiceBatchPrintList(actionForm,
            		    mdetails.getInvCode(),
            		    Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
            		    mdetails.getInvCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),                            
            		    Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit));
            		    
            		actionForm.saveArIBPList(arIBPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }                       
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("arInvoiceBatchPrint")); 

            
/*******************************************************
    -- Ar IBP Edit List Action --
 *******************************************************/

      } else if (request.getParameter("editListButton") != null  &&
        actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         // get selected invoices
         
        ArrayList invoiceCodeList = new ArrayList();

        int j = 0;
                     
	    for(int i=0; i<actionForm.getArIBPListSize(); i++) {
	    
	       ArInvoiceBatchPrintList actionList = actionForm.getArIBPByIndex(i);
	    	
            if (actionList.getPrint()) {

            	invoiceCodeList.add(actionList.getInvoiceCode());
            	
            }
           
       }	
       
       if (invoiceCodeList.size() > 0) {

       	   actionForm.setInvoiceEditListReport(Constants.STATUS_SUCCESS);	      
       	   actionForm.setInvoiceCodeList(invoiceCodeList);
       	   	       	   	       	
       }
       
       
       try {
         	
         	actionForm.clearArIBPList();
         	
         	ArrayList list = ejbIBP.getArInvByCriteria(actionForm.getCriteria(),
         	    new Integer(actionForm.getLineCount()), 
         	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
         	    actionForm.getOrderBy(), 
         	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         	
         	// check if prev should be disabled
           if (actionForm.getLineCount() == 0) {
            	
              actionForm.setDisablePreviousButton(true);
            	
           } else {
           	
           	  actionForm.setDisablePreviousButton(false);
           	
           }
           
           // check if next should be disabled
           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
           	  
           	  actionForm.setDisableNextButton(true);
           	  
           } else {
           	  
           	  actionForm.setDisableNextButton(false);
           	  
           	  //remove last record
           	  list.remove(list.size() - 1);
           	
           }
         	
         	Iterator i = list.iterator();
         	
         	while (i.hasNext()) {
         		
         		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
         		
         		ArInvoiceBatchPrintList arIBPList = new ArInvoiceBatchPrintList(actionForm,
         		    mdetails.getInvCode(),
         		    Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
         		    mdetails.getInvCstCustomerCode(),            		                		    
                     Common.convertSQLDateToString(mdetails.getInvDate()),
                     mdetails.getInvNumber(),
                     mdetails.getInvReferenceNumber(),
                     Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),                            
         		    Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit));
         		    
         		actionForm.saveArIBPList(arIBPList);
         		
         	}

         } catch (GlobalNoRecordFoundException ex) {
            
            // disable prev next buttons
	       actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);

         } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArInvoiceBatchPrintAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }                       
         
         actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
         return(mapping.findForward("arInvoiceBatchPrint"));
            
/*******************************************************
   -- Ar IBP Print DR Action --
*******************************************************/

      } else if (request.getParameter("printDrButton") != null  &&
      		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
      	
      	// get selected invoices
      	
      	ArrayList invoiceCodeList = new ArrayList();
      	
      	int j = 0;
      	
      	for(int i=0; i<actionForm.getArIBPListSize(); i++) {
      		
      		ArInvoiceBatchPrintList actionList = actionForm.getArIBPByIndex(i);
      		
      		if (actionList.getPrint()) {
      			
      			invoiceCodeList.add(actionList.getInvoiceCode());
      			
      		}
      		
      	}	
      	
      	if (invoiceCodeList.size() > 0) {
      		
      		actionForm.setDeliveryReceiptReport(Constants.STATUS_SUCCESS);	      
      		actionForm.setInvoiceCodeList(invoiceCodeList);
      		
      	}

      	try {
      		
      		actionForm.clearArIBPList();
      		
      		ArrayList list = ejbIBP.getArInvByCriteria(actionForm.getCriteria(),
      				new Integer(actionForm.getLineCount()), 
					new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
					actionForm.getOrderBy(), 
					new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
      		
      		// check if prev should be disabled
      		if (actionForm.getLineCount() == 0) {
      			
      			actionForm.setDisablePreviousButton(true);
      			
      		} else {
      			
      			actionForm.setDisablePreviousButton(false);
      			
      		}
      		
      		// check if next should be disabled
      		if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
      			
      			actionForm.setDisableNextButton(true);
      			
      		} else {
      			
      			actionForm.setDisableNextButton(false);
      			
      			//remove last record
      			list.remove(list.size() - 1);
      			
      		}
      		
      		Iterator i = list.iterator();
      		
      		while (i.hasNext()) {
      			
      			ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
      			
      			ArInvoiceBatchPrintList arIBPList = new ArInvoiceBatchPrintList(actionForm,
      					mdetails.getInvCode(),
						Common.convertByteToBoolean(mdetails.getInvCreditMemo()),
						mdetails.getInvCstCustomerCode(),            		                		    
						Common.convertSQLDateToString(mdetails.getInvDate()),
						mdetails.getInvNumber(),
						mdetails.getInvReferenceNumber(),
						Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),                            
						Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit));
      			
      			actionForm.saveArIBPList(arIBPList);
      			
      		}
      		
      	} catch (GlobalNoRecordFoundException ex) {
      		
      		// disable prev next buttons
      		actionForm.setDisableNextButton(true);
      		actionForm.setDisablePreviousButton(true);
      		
      	} catch (EJBException ex) {
      		
      		if (log.isInfoEnabled()) {
      			
      			log.info("EJBException caught in ArInvoiceBatchPrintAction.execute(): " + ex.getMessage() +
      					" session: " + session.getId());
      			return mapping.findForward("cmnErrorPage"); 
      			
      		}
      		
      	}                       
      	
      	actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
      	return(mapping.findForward("arInvoiceBatchPrint"));
         
/*******************************************************
   -- Ar IBP Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoiceBatchPrint");

            }
            
            actionForm.clearArIBPList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseCustomerPulldown()) {
            	    
                	actionForm.clearCustomerCodeList();         
                	
            		list = ejbIBP.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerCodeList((String)i.next());
            				
            			}
            			
            		}
            		
            	}
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbIBP.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbIBP.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
                        
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("arInvoiceBatchPrint"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArInvoiceBatchPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}