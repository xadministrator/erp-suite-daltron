package com.struts.ar.invoicebatchprint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArInvoiceBatchPrintForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList(); 
   private String customerCode = null;
   private ArrayList customerCodeList = new ArrayList();
   private boolean creditMemo = false;
   private boolean invoiceVoid = false;
   private String dateFrom = null;
   private String dateTo = null;
   private String invoiceNumberFrom = null;
   private String invoiceNumberTo = null;
   private String referenceNumber = null;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String paymentStatus = null;
   private ArrayList paymentStatusList = new ArrayList();      
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private String pageState = new String();
   private ArrayList arIBPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean useCustomerPulldown = true;
   private boolean isCreditMemo = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();
   
   private String invoiceReport = null;   
   private String creditMemoReport = null;
   private String invoiceEditListReport = null;
   private String deliveryReceiptReport = null;
   private ArrayList invoiceCodeList = null;
   
   public String getInvoiceReport() {
   	
   	   return invoiceReport;
   	
   }
   
   public void setInvoiceReport(String invoiceReport) {
   	
   	   this.invoiceReport = invoiceReport;
   	
   }
   
   public String getCreditMemoReport() {
   	
   	   return creditMemoReport;
   	
   }
   
   public void setCreditMemoReport(String creditMemoReport) {
   	
   	   this.creditMemoReport = creditMemoReport;
   	
   }
   
   public String getInvoiceEditListReport() {
   	
   	   return invoiceEditListReport;
   	
   }
   
   public void setInvoiceEditListReport(String invoiceEditListReport) {
   	
   	   this.invoiceEditListReport = invoiceEditListReport;
   	
   }
   
   public ArrayList getInvoiceCodeList() {   	  	
   	  return(invoiceCodeList);
   }
   
   public void setInvoiceCodeList(ArrayList invoiceCodeList) {
   	  this.invoiceCodeList = invoiceCodeList;
   }


   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public ArInvoiceBatchPrintList getArIBPByIndex(int index) {

      return((ArInvoiceBatchPrintList)arIBPList.get(index));

   }

   public Object[] getArIBPList() {

      return arIBPList.toArray();

   }

   public int getArIBPListSize() {

      return arIBPList.size();

   }

   public void saveArIBPList(Object newArIBPList) {

      arIBPList.add(newArIBPList);

   }

   public void clearArIBPList() {
      arIBPList.clear();
   }

   public void setRowSelected(Object selectedArIBPList, boolean isEdit) {

      this.rowSelected = arIBPList.indexOf(selectedArIBPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showArIBPRow(int rowSelected) {

   }

   public void updateArIBPRow(int rowSelected, Object newArIBPList) {

      arIBPList.set(rowSelected, newArIBPList);

   }

   public void deleteArIBPList(int rowSelected) {

      arIBPList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }

   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
   }
   
   public boolean getCreditMemo() {
   	
   	  return creditMemo;
   	  
   }
   
   public void setCreditMemo(boolean creditMemo) {
   	
   	  this.creditMemo = creditMemo;
   	  
   }  
   
   public boolean getInvoiceVoid() {
   	
   	  return invoiceVoid;
   	  
   }
   
   public void setInvoiceVoid(boolean invoiceVoid) {
   	
   	  this.invoiceVoid = invoiceVoid;
   	  
   } 
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	  
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   	  
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
   
   public String getInvoiceNumberFrom() {
   	
   	  return invoiceNumberFrom;
   	  
   }
   
   public void setInvoiceNumberFrom(String invoiceNumberFrom) {
   	
   	  this.invoiceNumberFrom = invoiceNumberFrom;
   	  
   }
   
   public String getInvoiceNumberTo() {
   	
   	  return invoiceNumberTo;
   	  
   }
   
   public void setInvoiceNumberTo(String invoiceNumberTo) {
   	
   	  this.invoiceNumberTo = invoiceNumberTo;
   	  
   } 

   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	  this.referenceNumber = referenceNumber;
   	  
   }

   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }

   public String getCurrency() {
   	
   	  return currency;
   
   }
   
   public void setCurrency(String currency) {
   
   	  this.currency = currency;
   
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   
   }
      
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }
   
   public String getPaymentStatus() {
   	
   	  return paymentStatus;
   	  
   }
   
   public void setPaymentStatus(String paymentStatus) {
   	
   	  this.paymentStatus = paymentStatus;
   	  
   }
   
   public ArrayList getPaymentStatusList() {
   	
   	  return paymentStatusList;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }

   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseCustomerPulldown() {
   	
   	  return useCustomerPulldown;
   	  
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	  this.useCustomerPulldown = useCustomerPulldown;
   	  
   }

   public String getDeliveryReceiptReport() {
   	
   	   return deliveryReceiptReport;
   	
   }
   
   public void setDeliveryReceiptReport(String deliveryReceiptReport) {
   	
   	   this.deliveryReceiptReport = deliveryReceiptReport;
   	
   }
   
   public boolean getIsCreditMemo() {
    
       return isCreditMemo;
    
    }
    
    public void setIsCreditMemo(boolean isCreditMemo) {
    	
    	  this.isCreditMemo = isCreditMemo;
    	
    }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  batchName = Constants.GLOBAL_BLANK;
      customerCode = Constants.GLOBAL_BLANK;
      creditMemo = false;
      invoiceVoid = false;
      dateFrom = null;
      dateTo = null;
      invoiceNumberFrom = null;
      invoiceNumberTo = null;
      referenceNumber = null;
      currency = Constants.GLOBAL_BLANK;
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      approvalStatus = "DRAFT";     
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add("YES");
      postedList.add("NO");
      posted = Constants.GLOBAL_NO;
      paymentStatusList.clear();
      paymentStatusList.add(Constants.GLOBAL_BLANK);
      paymentStatusList.add("PAID");
      paymentStatusList.add("UNPAID");
      paymentStatus = Constants.GLOBAL_BLANK;     
      
      if (orderByList.isEmpty()) {
      	
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("CUSTOMER CODE");
	      orderByList.add("INVOICE NUMBER");
	      orderBy = Constants.GLOBAL_BLANK;
	      
	  }
	  
	  for (int i=0; i<arIBPList.size(); i++) {
	  	
	  	  ArInvoiceBatchPrintList actionList = (ArInvoiceBatchPrintList)arIBPList.get(i);
	  	  actionList.setPrint(false);
	  	
	  }

      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("invoiceBatchPrint.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("invoiceBatchPrint.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("invoiceBatchPrint.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("invoiceBatchPrint.error.maxRowsInvalid"));
		
		   }
	 	  
	 	   if(useCustomerPulldown) {
	 	   	if (!Common.validateStringExists(customerCodeList, customerCode)) {
	 	   		
	 	   		errors.add("customerCode",
	 	   				new ActionMessage("invoiceBatchPrint.error.customerCodeInvalid"));
	 	   		
	 	   	}                        
	 	   }
                  
      }

      if (request.getParameter("printDrButton") != null) {
      	
      	boolean isAllCreditMemo = true;
      	
      	for (int i=0; i<arIBPList.size(); i++) {
      	
  	  	  ArInvoiceBatchPrintList actionList = (ArInvoiceBatchPrintList)arIBPList.get(i);
  	  	  
  	  	  if (actionList.getCreditMemo() == false) {
  	  	  	
  	  	  	isAllCreditMemo = false;
  	  	  	break;
  	  	  	
  	  	  }
 	   		
      	}
      	
      	if (isAllCreditMemo == true && arIBPList.size() != 0) {

      		errors.add("printDr",
      				new ActionMessage("invoiceBatchPrint.error.deliveryReceiptNotApplicable"));
      		
      	}

      	
      }
      
      return errors;

   }
   
}