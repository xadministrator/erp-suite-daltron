package com.struts.ar.invoicebatchsubmit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ArInvoiceBatchSubmitController;
import com.ejb.txn.ArInvoiceBatchSubmitControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModInvoiceDetails;

public final class ArInvoiceBatchSubmitAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArInvoiceBatchSubmitAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArInvoiceBatchSubmitForm actionForm = (ArInvoiceBatchSubmitForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_INVOICE_BATCH_SUBMIT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arInvoiceBatchSubmit");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArInvoiceBatchSubmitController EJB
*******************************************************/

         ArInvoiceBatchSubmitControllerHome homeIBS = null;
         ArInvoiceBatchSubmitController ejbIBS = null;

         try {
          
            homeIBS = (ArInvoiceBatchSubmitControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArInvoiceBatchSubmitControllerEJB", ArInvoiceBatchSubmitControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArInvoiceBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbIBS = homeIBS.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArInvoiceBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
 /*******************************************************
     Call ArInvoiceBatchSubmitController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableArInvoiceBatch
     getAdPrfArUseCustomerPulldown
  *******************************************************/
         
         short precisionUnit = 0;
         boolean enableInvoiceBatch = false; 
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbIBS.getGlFcPrecisionUnit(user.getCmpCode());
            enableInvoiceBatch = Common.convertByteToBoolean(ejbIBS.getAdPrfEnableArInvoiceBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableInvoiceBatch);
            useCustomerPulldown = Common.convertByteToBoolean(ejbIBS.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArInvoiceBatchSubmitAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ar IBS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arInvoiceBatchSubmit"));
	     
/*******************************************************
   -- Ar IBS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arInvoiceBatchSubmit"));         

/*******************************************************
   -- Ar IBS Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ar IBS Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Ar IBS Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap(); 
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerBatch())) {
	        		
	        		criteria.put("customerBatch", actionForm.getCustomerBatch());
	        		
	        	}
                
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {
	        		
	        		criteria.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {
	        		
	        		criteria.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	        	
	        	if (actionForm.getCreditMemo()) {	        	
		        	   		        		
	        		criteria.put("creditMemo", new Byte((byte)1));
                
                }
                
                if (!actionForm.getCreditMemo()) {
                	
                	criteria.put("creditMemo", new Byte((byte)0));
                	
                }
                	 
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbIBS.getArInvByCriteria(actionForm.getCriteria(),                	    
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ArInvoiceBatchSubmitAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	        	
	     	}
            
            try {
            	
            	actionForm.clearArIBSList();
            	
            	ArrayList list = ejbIBS.getArInvByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
            		
            		ArInvoiceBatchSubmitList arIBSList = new ArInvoiceBatchSubmitList(actionForm,
            		    mdetails.getInvCode(),
            		    mdetails.getInvCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),
						mdetails.getInvType());
            		    
            		actionForm.saveArIBSList(arIBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("invoiceBatchSubmit.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoiceBatchSubmit");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("arInvoiceBatchSubmit")); 

/*******************************************************
   -- Ar IBS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar IBS Submit Action --
*******************************************************/

         } else if (request.getParameter("submitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get submit invoices
                        
		    for(int i=0; i<actionForm.getArIBSListSize(); i++) {
		    
		       ArInvoiceBatchSubmitList actionList = actionForm.getArIBSByIndex(i);
		    	
               if (actionList.getSubmit()) {
               	
               	     try {
               	     	
               	     	ejbIBS.executeArInvBatchSubmit(actionList.getInvoiceCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteArIBSList(i);
               	     	i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoiceBatchSubmit.error.recordAlreadyDeleted", actionList.getInvoiceNumber()));
		             
		             } catch (GlobalTransactionAlreadyApprovedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoiceBatchSubmit.error.transactionAlreadyApproved", actionList.getInvoiceNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPendingException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoiceBatchSubmit.error.transactionAlreadyPending", actionList.getInvoiceNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoiceBatchSubmit.error.transactionAlreadyPosted", actionList.getInvoiceNumber()));
		             
		             } catch (GlobalNoApprovalRequesterFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoiceBatchSubmit.error.noApprovalRequesterFound", actionList.getInvoiceNumber()));
		                  
		             } catch (GlobalNoApprovalApproverFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoiceBatchSubmit.error.noApprovalApproverFound", actionList.getInvoiceNumber()));
		               
		             } catch (GlobalTransactionAlreadyVoidException ex) {
		               	
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("invoiceEntry.error.transactionAlreadyVoid", actionList.getInvoiceNumber()));		                       
		              
		             } catch (GlJREffectiveDateNoPeriodExistException ex) {
		               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invoiceBatchSubmit.error.effectiveDateNoPeriodExist", actionList.getInvoiceNumber()));
	                        
	                 } catch (GlJREffectiveDatePeriodClosedException ex) {
	               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invoiceBatchSubmit.error.effectiveDatePeriodClosed", actionList.getInvoiceNumber()));
	                        
	                 } catch (GlobalJournalNotBalanceException ex) {
	               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invoiceBatchSubmit.error.journalNotBalance", actionList.getInvoiceNumber()));
	               	   	               	   
	                 } catch (GlobalInventoryDateException ex) {
	                       	
                   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   			new ActionMessage("invoiceBatchSubmit.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
		                
	                 } catch (GlobalBranchAccountNumberInvalidException ex) {
	                 	
	                 	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                 			new ActionMessage("invoiceBatchSubmit.error.branchAccountNumberInvalid", ex.getMessage()));

		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		            	
		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("invoiceBatchSubmit.error.noNegativeInventoryCostingCOA"));

	                 } catch (EJBException ex) {
	                 	
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ApVoucherBatchSubmitAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }		
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoiceBatchSubmit");

            } 
	        	        
	        try {
	        
	            actionForm.setLineCount(0);
            	
            	actionForm.clearArIBSList();
            	
            	ArrayList list = ejbIBS.getArInvByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	          	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
            		
            		ArInvoiceBatchSubmitList arIBSList = new ArInvoiceBatchSubmitList(actionForm,
            		    mdetails.getInvCode(),
            		    mdetails.getInvCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),
						mdetails.getInvType());
            		    
            		actionForm.saveArIBSList(arIBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }                 
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("arInvoiceBatchSubmit")); 
	          

/*******************************************************
   -- Ar IBS Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoiceBatchSubmit");

            }
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseCustomerPulldown()) {
            	    
                	actionForm.clearCustomerCodeList();      
                	
            		list = ejbIBS.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerCodeList((String)i.next());
            				
            			}
            			
            		}
            	
            	}

            	actionForm.clearCurrencyList();           	
            	
            	list = ejbIBS.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearCustomerBatchList();           	
         		
         		list = ejbIBS.getAdLvCustomerBatchAll(user.getCmpCode());
         		
         		if (list == null || list.size() == 0) {
         			
         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
         			
         		} else {
         			
         			i = list.iterator();
         			
         			while (i.hasNext()) {
         				
         				actionForm.setCustomerBatchList((String)i.next());
         				
         			}
         			
         		} 
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbIBS.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	
            	actionForm.clearArIBSList();
            	            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 

            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("arInvoiceBatchSubmit"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArInvoiceBatchSubmitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}