package com.struts.ar.invoicebatchsubmit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArInvoiceBatchSubmitForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList(); 
   private String customerBatch = null;
   private ArrayList customerBatchList = new ArrayList();

   private String customerCode = null;
   private ArrayList customerCodeList = new ArrayList();
   private boolean creditMemo = false;
   private String dateFrom = null;
   private String dateTo = null;
   private String invoiceNumberFrom = null;
   private String invoiceNumberTo = null;
   private String referenceNumber = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String tableType = null;
   
   private int rowILISelected = 0;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;	  
   private boolean showBatchName = false;
   private String pageState = new String();
   private ArrayList arIBSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean useCustomerPulldown = true;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public ArInvoiceBatchSubmitList getArIBSByIndex(int index) {

      return((ArInvoiceBatchSubmitList)arIBSList.get(index));

   }

   public Object[] getArIBSList() {

      return arIBSList.toArray();

   }

   public int getArIBSListSize() {

      return arIBSList.size();

   }

   public void saveArIBSList(Object newArIBSList) {

      arIBSList.add(newArIBSList);

   }

   public void clearArIBSList() {
      arIBSList.clear();
   }

   public void setRowSelected(Object selectedArIBSList, boolean isEdit) {

      this.rowSelected = arIBSList.indexOf(selectedArIBSList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }
   
   public void setRowILISelected(Object selectedArILIList, boolean isEdit){
   	
    this.rowILISelected = arIBSList.indexOf(selectedArILIList);
    
 }

   public void showArIBSRow(int rowSelected) {

   }

   public void updateArIBSRow(int rowSelected, Object newArIBSList) {

      arIBSList.set(rowSelected, newArIBSList);

   }

   public void deleteArIBSList(int rowSelected) {

      arIBSList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getCustomerBatch() {
	   	
	      return customerBatch;
	      
	   }

	   public void setCustomerBatch(String customerBatch) {
	   	
	      this.customerBatch = customerBatch;
	      
	   }

	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }

   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
   }
   
   public boolean getCreditMemo() {
   	
   	  return creditMemo;
   	  
   }
   
   public void setCreditMemo(boolean creditMemo) {
   	
   	  this.creditMemo = creditMemo;
   	  
   }  
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	  
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   	  
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
   
   public String getInvoiceNumberFrom() {
   	
   	  return invoiceNumberFrom;
   	  
   }
   
   public void setInvoiceNumberFrom(String invoiceNumberFrom) {
   	
   	  this.invoiceNumberFrom = invoiceNumberFrom;
   	  
   }
   
   public String getInvoiceNumberTo() {
   	
   	  return invoiceNumberTo;
   	  
   }
   
   public void setInvoiceNumberTo(String invoiceNumberTo) {
   	
   	  this.invoiceNumberTo = invoiceNumberTo;
   	  
   } 

   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	  this.referenceNumber = referenceNumber;
   	  
   }

   public String getCurrency() {
   	
   	  return currency;
   
   }
   
   public void setCurrency(String currency) {
   
   	  this.currency = currency;
   
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   
   }
        
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public int getRowILISelected(){
   	
    return rowILISelected;
 
   }

   public boolean getUseCustomerPulldown() {
   	
   	return useCustomerPulldown;
   	
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	this.useCustomerPulldown = useCustomerPulldown;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	  batchName = Constants.GLOBAL_BLANK;
      customerCode = Constants.GLOBAL_BLANK;
      creditMemo = false;
      dateFrom = null;
      dateTo = null;
      invoiceNumberFrom = null;
      invoiceNumberTo = null;
      referenceNumber = null;
      currency = Constants.GLOBAL_BLANK;  
      
      for (int i=0; i<arIBSList.size(); i++) {
	  	
      	ArInvoiceBatchSubmitList actionList = (ArInvoiceBatchSubmitList)arIBSList.get(i);
      	
      		actionList.setSubmit(false);
	  	
      }

      if (orderByList.isEmpty()) { 
            
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("CUSTOMER CODE");
	      orderByList.add("INVOICE NUMBER");
	      orderBy = Constants.GLOBAL_BLANK;
	      
      }

      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("invoiceBatchSubmit.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("invoiceBatchSubmit.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("invoiceBatchSubmit.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("invoiceBatchSubmit.error.maxRowsInvalid"));
		
		   }	 	  
	 	   
	 	   if(useCustomerPulldown){
	 	   	if (!Common.validateStringExists(customerCodeList, customerCode)) {
	 	   		
	 	   		errors.add("customerCode",
	 	   				new ActionMessage("invoiceBatchSubmit.error.customerCodeInvalid"));
	 	   		
	 	   	}
	 	   }
      	
      	
      } 
      
      return errors;

   }
   
}