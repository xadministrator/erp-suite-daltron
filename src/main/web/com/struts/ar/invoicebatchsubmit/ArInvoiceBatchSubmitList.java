package com.struts.ar.invoicebatchsubmit;

import java.io.Serializable;

public class ArInvoiceBatchSubmitList implements Serializable {

   private Integer invoiceCode = null;
   private String customerCode = null;
   private String date = null;
   private String invoiceNumber = null;
   private String referenceNumber = null;  
   private String amountDue = null;
   private String type = null;
   
   private boolean submit = false;
       
   private ArInvoiceBatchSubmitForm parentBean;
    
   public ArInvoiceBatchSubmitList(ArInvoiceBatchSubmitForm parentBean,
      Integer invoiceCode,
      String customerCode,
      String date,
      String invoiceNumber,
      String referenceNumber,
      String amountDue,
	  String type) {

      this.parentBean = parentBean;
      this.invoiceCode = invoiceCode;
      this.customerCode = customerCode;
      this.date = date;
      this.invoiceNumber = invoiceNumber;
      this.referenceNumber = referenceNumber;
      this.amountDue = amountDue;
      this.type = type;
      
   }

   public Integer getInvoiceCode() {

      return invoiceCode;

   }
   
   public String getCustomerCode() {

      return customerCode;
      
   }

   public String getDate() {
   
      return date;
      
   }
   
   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmountDue() {
   
      return amountDue;
   
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }

   public boolean getSubmit() {
   	
   	  return submit;
   	
   }
   
   public void setSubmit(boolean submit) {
   	
   	  this.submit = submit;
   	
   }
   
}