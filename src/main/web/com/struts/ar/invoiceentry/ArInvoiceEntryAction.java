 package com.struts.ar.invoiceentry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.ArINVNoSalesOrderLinesFoundException;
import com.ejb.exception.ArInvDuplicateUploadNumberException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.AdDocumentSequenceAssignmentController;
import com.ejb.txn.AdDocumentSequenceAssignmentControllerHome;
import com.ejb.txn.AdLogController;
import com.ejb.txn.AdLogControllerHome;
import com.ejb.txn.ArInvoiceEntryController;
import com.ejb.txn.ArInvoiceEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.ejb.txn.InvRepItemCostingController;
import com.ejb.txn.InvRepItemCostingControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.ReportParameter;
import com.struts.util.Responsibility;
import com.struts.util.User;
import com.util.ArInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModJobOrderDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.ArSalespersonDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.ArTaxCodeDetails;
import com.util.CmAdjustmentDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ArInvoiceEntryAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArInvoiceEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ArInvoiceEntryForm actionForm = (ArInvoiceEntryForm)form;

	      // reset report

	      actionForm.setReport(null);
	      actionForm.setReport2(null);
	      actionForm.setReport3(null);
	      actionForm.setAttachment(null);
	      actionForm.setAttachmentPDF(null);
	      actionForm.setAttachmentDownload(null);



         String frParam = null;

         if (request.getParameter("child") == null) {

         	frParam = Common.getUserPermission(user, Constants.AR_INVOICE_ENTRY_ID);

         } else {

         	frParam = Constants.FULL_ACCESS;

         }

         if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               if (!fieldErrors.isEmpty()) {


            	   System.out.println("ERROR TEST: " + fieldErrors.toString());
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arInvoiceEntry"));

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArInvoiceEntryController EJB
*******************************************************/
       
         ArInvoiceEntryControllerHome homeINV = null;
         ArInvoiceEntryController ejbINV = null;

         InvRepItemCostingControllerHome homeRIC = null;
         InvRepItemCostingController ejbRIC = null;
         
         AdLogControllerHome homeLOG = null;
         AdLogController ejbLOG = null;
         
         GlDailyRateControllerHome homeFR = null;
         GlDailyRateController ejbFR = null;
         
      
        AdDocumentSequenceAssignmentControllerHome homeDSA = null;
        AdDocumentSequenceAssignmentController ejbDSA = null;



         try {

            homeINV = (ArInvoiceEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArInvoiceEntryControllerEJB", ArInvoiceEntryControllerHome.class);
            homeRIC = (InvRepItemCostingControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/InvRepItemCostingControllerEJB", InvRepItemCostingControllerHome.class);
            homeLOG = (AdLogControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/AdLogControllerEJB", AdLogControllerHome.class);
            homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
            homeDSA = (AdDocumentSequenceAssignmentControllerHome)com.util.EJBHomeFactory.
                         lookUpHome("ejb/AdDocumentSequenceAssignmentControllerEJB", AdDocumentSequenceAssignmentControllerHome.class);
           
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArInvoiceEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {

            ejbINV = homeINV.create();
            ejbRIC = homeRIC.create();
            ejbLOG = homeLOG.create();
            ejbFR = homeFR.create();
            ejbDSA = homeDSA.create();

         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in ArInvoiceEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ArInvoiceEntryController EJB
   getGlFcPrecisionUnit
   getAdPrfArInvoiceLineNumber
   getAdPrfEnableArInvoiceBatch
   getInvGpQuantityPrecisionUnit
   getAdPrfEnableInvShift
   getAdPrfArUseCustomerPulldown
   getAdPrfArEnablePaymentTerm
   getAdPrfArDisableSalesPrice
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

         short precisionUnit = 0;
         short invoiceLineNumber = 0;

         boolean enableInvoiceBatch = false;
         boolean isInitialPrinting = false;
         boolean enableShift = false;
         boolean useCustomerPulldown = true;
         boolean enablePaymentTerm = true;
         boolean disableSalesPrice = true;
         boolean invoiceSalespersonRequired = false;

         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ar/Invoice/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
         String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
         String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

         try {

             precisionUnit = ejbINV.getGlFcPrecisionUnit(user.getCmpCode());
             invoiceLineNumber = ejbINV.getAdPrfArInvoiceLineNumber(user.getCmpCode());
             enableInvoiceBatch = Common.convertByteToBoolean(ejbINV.getAdPrfEnableArInvoiceBatch(user.getCmpCode()));
             invoiceSalespersonRequired = Common.convertByteToBoolean(ejbINV.getAdPrfInvoiceSalespersonRequired(user.getCmpCode()));
             actionForm.setShowBatchName(enableInvoiceBatch);
             actionForm.setPrecisionUnit(precisionUnit);
             enableShift = Common.convertByteToBoolean(ejbINV.getAdPrfEnableInvShift(user.getCmpCode()));
             actionForm.setShowShift(enableShift);
             useCustomerPulldown = Common.convertByteToBoolean(ejbINV.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
             actionForm.setUseCustomerPulldown(useCustomerPulldown);
             enablePaymentTerm = Common.convertByteToBoolean(ejbINV.getAdPrfArEnablePaymentTerm(user.getCmpCode()));
             actionForm.setEnablePaymentTerm(enablePaymentTerm);
             disableSalesPrice = Common.convertByteToBoolean(ejbINV.getAdPrfArDisableSalesPrice(user.getCmpCode()));
             actionForm.setDisableSalesPrice(disableSalesPrice);
             actionForm.setSalespersonRequired(invoiceSalespersonRequired);



             for(int i = 0; i < actionForm.getArILIListSize(); i++) {

                 ArInvoiceLineItemList arILIList = actionForm.getArILIByIndex(i);

                 if(arILIList.getItemName() != null && arILIList.getItemName().length() > 0) {

                     boolean isAssemblyItem = ejbINV.getInvIiClassByIiName(arILIList.getItemName(), user.getCmpCode()).equals("Assembly");
                     arILIList.setIsAssemblyItem(isAssemblyItem);

                 }
             }


         } catch(EJBException ex) {

             if (log.isInfoEnabled()) {

                 log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                         " session: " + session.getId());
             }

             return(mapping.findForward("cmnErrorPage"));

         }





/*******************************************************
   -- Ar INV Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("MEMO LINES") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArModInvoiceDetails details = new ArModInvoiceDetails();

             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingHeader3(actionForm.getBillingHeader3());
             details.setInvBillingFooter3(actionForm.getBillingFooter3());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToContact(actionForm.getShipToContact());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));

             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());

             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

             ArrayList ilList = new ArrayList();

             for (int i = 0; i<actionForm.getArINVListSize(); i++) {

                 ArInvoiceEntryList arINVList = actionForm.getArINVByIndex(i);

                 if (Common.validateRequired(arINVList.getMemoLine()) &&
                         Common.validateRequired(arINVList.getDescription()) &&
                         Common.validateRequired(arINVList.getQuantity()) &&
                         Common.validateRequired(arINVList.getUnitPrice()) &&
                         Common.validateRequired(arINVList.getAmount())) continue;

                 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

                 mdetails.setIlSmlName(arINVList.getMemoLine());
                 mdetails.setIlDescription(arINVList.getDescription());
                 mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arINVList.getQuantity(), precisionUnit));
                 mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arINVList.getUnitPrice(), precisionUnit));
                 mdetails.setIlAmount(Common.convertStringMoneyToDouble(arINVList.getAmount(), precisionUnit));
                 mdetails.setIlTax(Common.convertBooleanToByte(arINVList.getTaxable()));

                 ilList.add(mdetails);

             }

             try {

            	 Integer invoiceCode = ejbINV.saveArInvEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         ilList, true, actionForm.getSalesperson(),
                         actionForm.getProjectCode(),
                         actionForm.getProjectTypeCode(),
                         actionForm.getProjectPhaseName(),
                         actionForm.getContractTermName(),
                         new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());



            	 actionForm.setInvoiceCode(invoiceCode);

            	   ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - MEMO LINE",  new java.util.Date(),user.getUserName(),"SAVE AS DRAFT", actionForm.getLogEntry(),  user.getCmpCode());
                 	
             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid"));


             } catch (ArInvDuplicateUploadNumberException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));


             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }

       




/*******************************************************
   -- Ar INV Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("MEMO LINES") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArModInvoiceDetails details = new ArModInvoiceDetails();

             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingHeader3(actionForm.getBillingHeader3());
             details.setInvBillingFooter3(actionForm.getBillingFooter3());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));

             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());

             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));


             ArrayList ilList = new ArrayList();

             for (int i = 0; i<actionForm.getArINVListSize(); i++) {

                 ArInvoiceEntryList arINVList = actionForm.getArINVByIndex(i);

                 if (Common.validateRequired(arINVList.getMemoLine()) &&
                         Common.validateRequired(arINVList.getDescription()) &&
                         Common.validateRequired(arINVList.getQuantity()) &&
                         Common.validateRequired(arINVList.getUnitPrice()) &&
                         Common.validateRequired(arINVList.getAmount())) continue;

                 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

                 mdetails.setIlSmlName(arINVList.getMemoLine());
                 mdetails.setIlDescription(arINVList.getDescription());
                 mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arINVList.getQuantity(), precisionUnit));
                 mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arINVList.getUnitPrice(), precisionUnit));
                 mdetails.setIlAmount(Common.convertStringMoneyToDouble(arINVList.getAmount(), precisionUnit));
                 mdetails.setIlTax(Common.convertBooleanToByte(arINVList.getTaxable()));

                 ilList.add(mdetails);

             }

             try {

                 Integer invoiceCode = ejbINV.saveArInvEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         ilList, false, actionForm.getSalesperson(),
                         actionForm.getProjectCode(),
                         actionForm.getProjectTypeCode(),
                         actionForm.getProjectPhaseName(),
                         actionForm.getContractTermName(),
                         new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                actionForm.setInvoiceCode(invoiceCode);
                 
                 ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - MEMO LINE",  new java.util.Date(),user.getUserName(),"SAVE AND SUBMIT", actionForm.getLogEntry(),  user.getCmpCode());
                 

             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid"));

             } catch (ArInvDuplicateUploadNumberException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }

        
/*******************************************************
	 -- Ar INV Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("MEMO LINES")) {
		
             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArModInvoiceDetails details = new ArModInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingHeader3(actionForm.getBillingHeader3());
                 details.setInvBillingFooter3(actionForm.getBillingFooter3());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());

                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));

                 if (actionForm.getInvoiceCode() == null) {

               //      details.setInvCreatedBy(user.getUserName());;
                     details.setInvCreatedBy(user.getUserDescription());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());
                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));


                 ArrayList ilList = new ArrayList();

                 for (int i = 0; i<actionForm.getArINVListSize(); i++) {

                     ArInvoiceEntryList arINVList = actionForm.getArINVByIndex(i);

                     if (Common.validateRequired(arINVList.getMemoLine()) &&
                             Common.validateRequired(arINVList.getDescription()) &&
                             Common.validateRequired(arINVList.getQuantity()) &&
                             Common.validateRequired(arINVList.getUnitPrice()) &&
                             Common.validateRequired(arINVList.getAmount())) continue;

                     ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

                     mdetails.setIlSmlName(arINVList.getMemoLine());
                     mdetails.setIlDescription(arINVList.getDescription());
                     mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arINVList.getQuantity(), precisionUnit));
                     mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arINVList.getUnitPrice(), precisionUnit));
                     mdetails.setIlAmount(Common.convertStringMoneyToDouble(arINVList.getAmount(), precisionUnit));
                     mdetails.setIlTax(Common.convertBooleanToByte(arINVList.getTaxable()));

                     ilList.add(mdetails);

                 }

//               validate attachment

                 /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                 String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                 String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                 String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                 if (!Common.validateRequired(filename1)) {

      	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename1NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

      	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename1Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename1().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename1SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename2)) {

      	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename2NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename2Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename2().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename2SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename3)) {

      	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename3NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename3Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename3().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename3SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename4)) {

      	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename4NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename4Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename4().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename4SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   	}*/
                 try {

                     Integer invoiceCode = ejbINV.saveArInvEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             ilList, true, actionForm.getSalesperson(),
                             actionForm.getProjectCode(),
                             actionForm.getProjectTypeCode(),
                             actionForm.getProjectPhaseName(),
                             actionForm.getContractTermName(),
                             new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     actionForm.setInvoiceCode(invoiceCode);
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - MEMO LINE",  new java.util.Date(),user.getUserName(),"PRINT", actionForm.getLogEntry(),  user.getCmpCode());
                     

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));



                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid"));

                 } catch (ArInvDuplicateUploadNumberException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

                 } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }

              // save attachment

  	          /* if (!Common.validateRequired(filename1)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename1().getInputStream();

  	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-1" + fileExtension);

  	       	    		int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }

  	       	   if (!Common.validateRequired(filename2)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename2().getInputStream();

  	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-2" + fileExtension);
  		            	int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }

  	       	   if (!Common.validateRequired(filename3)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename3().getInputStream();

  	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-3" + fileExtension);

  		            	int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }

  	       	   if (!Common.validateRequired(filename4)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename4().getInputStream();

  	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-4" + fileExtension);

  		            	int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }

                 if (!errors.isEmpty()) {

                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("arInvoiceEntry"));

                 }*/

                 actionForm.setReport(Constants.STATUS_SUCCESS);

                 isInitialPrinting = true;

             }  else {

                 actionForm.setReport(Constants.STATUS_SUCCESS);

                 return(mapping.findForward("arInvoiceEntry"));

             }

/*******************************************************
   -- Ar INV Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("MEMO LINES")) {

             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArModInvoiceDetails details = new ArModInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingHeader3(actionForm.getBillingHeader3());
                 details.setInvBillingFooter3(actionForm.getBillingFooter3());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());
                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));


                 if (actionForm.getInvoiceCode() == null) {

                     details.setInvCreatedBy(user.getUserName());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());

                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));


                 ArrayList ilList = new ArrayList();

                 for (int i = 0; i<actionForm.getArINVListSize(); i++) {

                     ArInvoiceEntryList arINVList = actionForm.getArINVByIndex(i);

                     if (Common.validateRequired(arINVList.getMemoLine()) &&
                             Common.validateRequired(arINVList.getDescription()) &&
                             Common.validateRequired(arINVList.getQuantity()) &&
                             Common.validateRequired(arINVList.getUnitPrice()) &&
                             Common.validateRequired(arINVList.getAmount())) continue;

                     ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

                     mdetails.setIlSmlName(arINVList.getMemoLine());
                     mdetails.setIlDescription(arINVList.getDescription());
                     mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arINVList.getQuantity(), precisionUnit));
                     mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arINVList.getUnitPrice(), precisionUnit));
                     mdetails.setIlAmount(Common.convertStringMoneyToDouble(arINVList.getAmount(), precisionUnit));
                     mdetails.setIlTax(Common.convertBooleanToByte(arINVList.getTaxable()));

                     ilList.add(mdetails);

                 }

//               validate attachment

                 /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                 String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                 String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                 String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                 if (!Common.validateRequired(filename1)) {

      	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename1NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

      	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename1Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename1().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename1SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename2)) {

      	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename2NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename2Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename2().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename2SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename3)) {

      	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename3NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename3Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename3().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename3SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   }

      	   	   if (!Common.validateRequired(filename4)) {

      	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

      	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	           			new ActionMessage("invoiceEntry.error.filename4NotFound"));

      	   	    	} else {

      	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

      	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename4Invalid"));

      	          	    	}

      	          	    	InputStream is = actionForm.getFilename4().getInputStream();

      	          	    	if (is.available() > maxAttachmentFileSize) {

      	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
      	                   		new ActionMessage("invoiceEntry.error.filename4SizeInvalid"));

      	          	    	}

      	          	    	is.close();

      	   	    	}

      	   	    	if (!errors.isEmpty()) {

      	   	    		saveErrors(request, new ActionMessages(errors));
      	          			return (mapping.findForward("arInvoiceEntry"));

      	   	    	}

      	   	   	}*/
                 try {

                     Integer invoiceCode = ejbINV.saveArInvEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             ilList, true, actionForm.getSalesperson(),
                             actionForm.getProjectCode(),
                             actionForm.getProjectTypeCode(),
                             actionForm.getProjectPhaseName(),
                             actionForm.getContractTermName(),

                             new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     actionForm.setInvoiceCode(invoiceCode);
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - MEMO LINE",  new java.util.Date(),user.getUserName(),"JOURNAL", actionForm.getLogEntry(),  user.getCmpCode());
                     

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));

                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid"));

                 } catch (ArInvDuplicateUploadNumberException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

                 } catch (EJBException ex) {

                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }

              // save attachment

  	          /* if (!Common.validateRequired(filename1)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename1().getInputStream();

  	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-1" + fileExtension);

  	       	    		int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }

  	       	   if (!Common.validateRequired(filename2)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename2().getInputStream();

  	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-2" + fileExtension);
  		            	int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }

  	       	   if (!Common.validateRequired(filename3)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename3().getInputStream();

  	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-3" + fileExtension);

  		            	int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }

  	       	   if (!Common.validateRequired(filename4)) {

  	       	        if (errors.isEmpty()) {

  	       	        	InputStream is = actionForm.getFilename4().getInputStream();

  	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

  	       	        	new File(attachmentPath).mkdirs();

  	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

  	       	    			fileExtension = attachmentFileExtension;

  	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	           	    		fileExtension = attachmentFileExtensionPDF;

  	           	    	}
  	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getInvoiceCode() + "-4" + fileExtension);

  		            	int c;

  		            	while ((c = is.read()) != -1) {

  		            		fos.write((byte)c);

  		            	}

  		            	is.close();
  						fos.close();

  	       	        }

  	       	   }


                 if (!errors.isEmpty()) {

                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("arInvoiceEntry"));

                 }*/

             }

             String path = "/arJournal.do?forward=1" +
             "&transactionCode=" + actionForm.getInvoiceCode() +
             "&transaction=INVOICE" +
             "&transactionNumber=" + actionForm.getInvoiceNumber() +
             "&transactionDate=" + actionForm.getDate() +
             "&transactionEnableFields=" + actionForm.getEnableFields();

             return(new ActionForward(path));

/*******************************************************
   -- Ar INV ILI Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("ITEMS") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 ArInvoiceDetails details = new ArInvoiceDetails();
             System.out.println("actionForm.getInvoiceCode())="+actionForm.getInvoiceCode());
             System.out.println("actionForm.getInvoiceNumber())="+actionForm.getInvoiceNumber());

             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingHeader3(actionForm.getBillingHeader3());
             details.setInvBillingFooter3(actionForm.getBillingFooter3());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToContact(actionForm.getShipToContact());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvLvShift(actionForm.getShift());
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));



             System.out.println("actionForm.getRecieveDate() :" +actionForm.getRecieveDate());
             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());
             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

             ArrayList iliList = new ArrayList();

             for (int i = 0; i<actionForm.getArILIListSize(); i++) {

                 ArInvoiceLineItemList arILIList = actionForm.getArILIByIndex(i);

                 if (Common.validateRequired(arILIList.getLocation()) &&
                         Common.validateRequired(arILIList.getItemName()) &&
                         Common.validateRequired(arILIList.getUnit()) &&
                         Common.validateRequired(arILIList.getUnitPrice()) &&
                         Common.validateRequired(arILIList.getQuantity())) continue;

                 if (Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) <= 0) continue;

                 ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

                 mdetails.setIliLine(Common.convertStringToShort(arILIList.getLineNumber()));
                 mdetails.setIliIiName(arILIList.getItemName());
                 mdetails.setIliLocName(arILIList.getLocation());
                 mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit));
                 mdetails.setIliUomName(arILIList.getUnit());
                 mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit));
                 mdetails.setIliAmount(Common.convertStringMoneyToDouble(arILIList.getAmount(), precisionUnit));
                 mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arILIList.getAutoBuildCheckbox()));
                 mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arILIList.getDiscount1(), precisionUnit));
                 mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arILIList.getDiscount2(), precisionUnit));
                 mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arILIList.getDiscount3(), precisionUnit));
                 mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arILIList.getDiscount4(), precisionUnit));
                 mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arILIList.getTotalDiscount(), precisionUnit));

                 mdetails.setIliTax(Common.convertBooleanToByte(arILIList.getTax().equals("Y")?true:false));
                 System.out.println("arILIList.tax(): "+arILIList.getTax());


                 ArrayList tagList = new ArrayList();
             	   //TODO:save and submit taglist
             	   boolean isTraceMisc = ejbINV.getArTraceMisc(arILIList.getItemName(), user.getCmpCode());
  	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
  	           	String misc = arILIList.getMisc();

          	   if (isTraceMisc){


          		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
          		   mdetails.setIliTagList(tagList);

          		   if(tagList.size()<=0) {
          		     mdetails.setIliMisc(null);
          		   }

          	   }



                 iliList.add(mdetails);

             }

              //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}
			Integer invCode = 0;
             try {
            	 
            	 
            	 System.out.println("inv code is : " + details.getInvCode());

                 Integer invoiceCode = ejbINV.saveArInvIliEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         iliList, true, actionForm.getSalesperson(), actionForm.getDeployedBranchName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

				invCode = invoiceCode;
                 ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - ITEM",  new java.util.Date(),user.getUserName(),"SAVE AS DRAFT", actionForm.getLogEntry(),  user.getCmpCode());
                 
                 
             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlobalInvItemLocationNotFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalInventoryDateException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

             } catch (GlobalRecordInvalidException ex) {

	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	            	  new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

             } catch (GlobalMiscInfoIsRequiredException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

             } catch (ArInvDuplicateUploadNumberException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("invoiceEntry.error.duplicateUploadNumber", ex.getMessage()));

             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }
             
             
                // save attachment
		

           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	

					
					
					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

/*******************************************************
   -- Ar INV Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("ITEMS") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArModInvoiceDetails details = new ArModInvoiceDetails();

             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingHeader3(actionForm.getBillingHeader3());
             details.setInvBillingFooter3(actionForm.getBillingFooter3());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToContact(actionForm.getShipToContact());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());
             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

             ArrayList iliList = new ArrayList();

             for (int i = 0; i<actionForm.getArILIListSize(); i++) {

                 ArInvoiceLineItemList arILIList = actionForm.getArILIByIndex(i);

                 if (Common.validateRequired(arILIList.getLocation()) &&
                         Common.validateRequired(arILIList.getItemName()) &&
                         Common.validateRequired(arILIList.getUnit()) &&
                         Common.validateRequired(arILIList.getUnitPrice()) &&
                         Common.validateRequired(arILIList.getQuantity())) continue;

                 if (Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) <= 0) continue;

                 ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

                 mdetails.setIliLine(Common.convertStringToShort(arILIList.getLineNumber()));
                 mdetails.setIliIiName(arILIList.getItemName());
                 mdetails.setIliLocName(arILIList.getLocation());
                 mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit));
                 mdetails.setIliUomName(arILIList.getUnit());
                 mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit));
                 mdetails.setIliAmount(Common.convertStringMoneyToDouble(arILIList.getAmount(), precisionUnit));
                 mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arILIList.getAutoBuildCheckbox()));
                 mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arILIList.getDiscount1(), precisionUnit));
                 mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arILIList.getDiscount2(), precisionUnit));
                 mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arILIList.getDiscount3(), precisionUnit));
                 mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arILIList.getDiscount4(), precisionUnit));
                 mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arILIList.getTotalDiscount(), precisionUnit));
                 mdetails.setIliMisc(arILIList.getMisc());
                 mdetails.setIliTax(Common.convertBooleanToByte(arILIList.getTax().equals("Y")?true:false));



                 ArrayList tagList = new ArrayList();
             	   //TODO:save and submit taglist
             	   boolean isTraceMisc = ejbINV.getArTraceMisc(arILIList.getItemName(), user.getCmpCode());
  	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
  				String misc= arILIList.getMisc();


           	   if (isTraceMisc){


           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
           		   mdetails.setIliTagList(tagList);

           		  if(tagList.size()<=0) {
           		     mdetails.setIliMisc(null);
           		   }

           	   }


                 iliList.add(mdetails);

             }
 //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}

             try {

                 Integer invoiceCode = ejbINV.saveArInvIliEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         iliList, false, actionForm.getSalesperson(), actionForm.getDeployedBranchName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 actionForm.setInvoiceCode(invoiceCode);
                 
                 ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - ITEM",  new java.util.Date(),user.getUserName(),"SAVE AND SUBMIT", actionForm.getLogEntry(),  user.getCmpCode());
                 

             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlobalInvItemLocationNotFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalInventoryDateException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

             } catch (GlobalRecordInvalidException ex) {

	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	            	  new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

             } catch (GlobalMiscInfoIsRequiredException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

             } catch (ArInvDuplicateUploadNumberException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("invoiceEntry.error.duplicateUploadNumber", ex.getMessage()));

             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }

			
          // save attachment
		Integer invCode = actionForm.getInvoiceCode();


           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	

					
					
					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }
/*******************************************************
	 -- Ar INV Print Action --
*******************************************************/

         } else if ((request.getParameter("printButton") != null || request.getParameter("drPrintButton") != null) && actionForm.getType().equals("ITEMS")) {

             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArModInvoiceDetails details = new ArModInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingHeader3(actionForm.getBillingHeader3());
                 details.setInvBillingFooter3(actionForm.getBillingFooter3());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());
                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvLvShift(actionForm.getShift());
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
                 if (actionForm.getInvoiceCode() == null) {

                     details.setInvCreatedBy(user.getUserName());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());
                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                 ArrayList iliList = new ArrayList();

                 for (int i = 0; i<actionForm.getArILIListSize(); i++) {

                     ArInvoiceLineItemList arILIList = actionForm.getArILIByIndex(i);

                     if (Common.validateRequired(arILIList.getLocation()) &&
                             Common.validateRequired(arILIList.getItemName()) &&
                             Common.validateRequired(arILIList.getUnit()) &&
                             Common.validateRequired(arILIList.getUnitPrice()) &&
                             Common.validateRequired(arILIList.getQuantity())) continue;

                     if (Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) <= 0) continue;

                     ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

                     mdetails.setIliLine(Common.convertStringToShort(arILIList.getLineNumber()));
                     mdetails.setIliIiName(arILIList.getItemName());
                     mdetails.setIliLocName(arILIList.getLocation());
                     mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit));
                     mdetails.setIliUomName(arILIList.getUnit());
                     mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit));
                     mdetails.setIliAmount(Common.convertStringMoneyToDouble(arILIList.getAmount(), precisionUnit));
                     mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arILIList.getAutoBuildCheckbox()));
                     mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arILIList.getDiscount1(), precisionUnit));
                     mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arILIList.getDiscount2(), precisionUnit));
                     mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arILIList.getDiscount3(), precisionUnit));
                     mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arILIList.getDiscount4(), precisionUnit));
                     mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arILIList.getTotalDiscount(), precisionUnit));
                     mdetails.setIliMisc(arILIList.getMisc());
                     mdetails.setIliTax(Common.convertBooleanToByte(arILIList.getTax().equals("Y")?true:false));



                     ArrayList tagList = new ArrayList();
                 	   //TODO:save and submit taglist
                 	   boolean isTraceMisc = ejbINV.getArTraceMisc(arILIList.getItemName(), user.getCmpCode());
      	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
      				String misc= arILIList.getMisc();


               	   if (isTraceMisc){


               		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
               		   mdetails.setIliTagList(tagList);

               		  if(tagList.size()<=0) {
               		     mdetails.setIliMisc(null);
               		   }

               	   }




                     iliList.add(mdetails);

                 }


                 //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}

                 try {

                     Integer invoiceCode = ejbINV.saveArInvIliEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             iliList, true, actionForm.getSalesperson(), actionForm.getDeployedBranchName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     actionForm.setInvoiceCode(invoiceCode);
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - ITEM",  new java.util.Date(),user.getUserName(),"PRINT", actionForm.getLogEntry(),  user.getCmpCode());
                     

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));


                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));


                 }catch (GlobalRecordInvalidException ex) {

                	  errors.add(ActionMessages.GLOBAL_MESSAGE,
        	             new ActionMessage("invoiceEntry.error.insufficientStocksPrint", ex.getMessage()));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlobalInvItemLocationNotFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));

                 } catch (GlobalInventoryDateException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (GlobalMiscInfoIsRequiredException ex) {

		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

                 } catch (ArInvDuplicateUploadNumberException ex) {

		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		        			   new ActionMessage("invoiceEntry.error.duplicateUploadNumber", ex.getMessage()));

                 } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }


                // save attachment
		Integer invCode = actionForm.getInvoiceCode();


           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	

					
					
					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }
       	   
       	   
       	   		if(request.getParameter("drPrintButton") != null){
       	   		  	actionForm.setReport2(Constants.STATUS_SUCCESS);

                 	
       	   		}else{
       	   		 	 actionForm.setReport(Constants.STATUS_SUCCESS);

                 
       	   		
       	   		}
       	   			isInitialPrinting = true;
               

             }  else {

                
       	   		if(request.getParameter("drPrintButton") != null){
       	   		  	actionForm.setReport2(Constants.STATUS_SUCCESS);

                 	
       	   		}else{
       	   		 	 actionForm.setReport(Constants.STATUS_SUCCESS);

                 
       	   		
       	   		}

                 return(mapping.findForward("arInvoiceEntry"));
					
             }

/*******************************************************
   -- Ar INV ILI Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("ITEMS")) {

             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArModInvoiceDetails details = new ArModInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingHeader3(actionForm.getBillingHeader3());
                 details.setInvBillingFooter3(actionForm.getBillingFooter3());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());
                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvLvShift(actionForm.getShift());
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
                 if (actionForm.getInvoiceCode() == null) {

                     details.setInvCreatedBy(user.getUserName());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());
                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                 ArrayList iliList = new ArrayList();

                 for (int i = 0; i<actionForm.getArILIListSize(); i++) {

                     ArInvoiceLineItemList arILIList = actionForm.getArILIByIndex(i);

                     if (Common.validateRequired(arILIList.getLocation()) &&
                             Common.validateRequired(arILIList.getItemName()) &&
                             Common.validateRequired(arILIList.getUnit()) &&
                             Common.validateRequired(arILIList.getUnitPrice()) &&
                             Common.validateRequired(arILIList.getQuantity())) continue;

                     if (Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) <= 0) continue;

                     ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

                     mdetails.setIliLine(Common.convertStringToShort(arILIList.getLineNumber()));
                     mdetails.setIliIiName(arILIList.getItemName());
                     mdetails.setIliLocName(arILIList.getLocation());
                     mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit));
                     mdetails.setIliUomName(arILIList.getUnit());
                     mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit));
                     mdetails.setIliAmount(Common.convertStringMoneyToDouble(arILIList.getAmount(), precisionUnit));
                     mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arILIList.getAutoBuildCheckbox()));
                     mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arILIList.getDiscount1(), precisionUnit));
                     mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arILIList.getDiscount2(), precisionUnit));
                     mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arILIList.getDiscount3(), precisionUnit));
                     mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arILIList.getDiscount4(), precisionUnit));
                     mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arILIList.getTotalDiscount(), precisionUnit));
                     mdetails.setIliMisc(arILIList.getMisc());
                     mdetails.setIliTax(Common.convertBooleanToByte(arILIList.getTax().equals("Y")?true:false));



                     ArrayList tagList = new ArrayList();
                 	   //TODO:save and submit taglist
                 	   boolean isTraceMisc = ejbINV.getArTraceMisc(arILIList.getItemName(), user.getCmpCode());
      	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
      				String misc= arILIList.getMisc();


               	   if (isTraceMisc){


               		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
               		   mdetails.setIliTagList(tagList);

               		  if(tagList.size()<=0) {
               		     mdetails.setIliMisc(null);
               		   }

               	   }

                     iliList.add(mdetails);

                 }

 //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}

                 try {

                     Integer invoiceCode = ejbINV.saveArInvIliEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             iliList, true, actionForm.getSalesperson(), actionForm.getDeployedBranchName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                    actionForm.setInvoiceCode(invoiceCode);
                     
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - ITEM",  new java.util.Date(),user.getUserName(),"JOURNAL", actionForm.getLogEntry(),  user.getCmpCode());
                    

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalRecordInvalidException ex) {

  	               errors.add(ActionMessages.GLOBAL_MESSAGE,
  	            	  new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));


                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlobalInvItemLocationNotFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));

                 } catch (GlobalInventoryDateException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (ArInvDuplicateUploadNumberException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

                 } catch (GlobalMiscInfoIsRequiredException ex) {

		        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

                 } catch (EJBException ex) {

                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }


              // save attachment
		Integer invCode = actionForm.getInvoiceCode();


           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	

					
					
					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }


                 if (!errors.isEmpty()) {

                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("arInvoiceEntry"));

                 }

             }

             String path = "/arJournal.do?forward=1" +
             "&transactionCode=" + actionForm.getInvoiceCode() +
             "&transaction=INVOICE" +
             "&transactionNumber=" + actionForm.getInvoiceNumber() +
             "&transactionDate=" + actionForm.getDate() +
             "&transactionEnableFields=" + actionForm.getEnableFields();

             return(new ActionForward(path));

/*******************************************************
    -- Ar INV SIL Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("SO MATCHED") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArInvoiceDetails details = new ArInvoiceDetails();
             
             System.out.println("inv code is: "  + actionForm.getInvoiceCode());
             
             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvSoNumber(actionForm.getSoNumber());
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToContact(actionForm.getShipToContact());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvLvShift(actionForm.getShift());
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());
             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

             ArrayList solList = new ArrayList();

             for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

                 ArInvoiceSalesOrderList arSOLList = actionForm.getArSOLByIndex(i);

                 if(!arSOLList.getIssueCheckbox()) continue;

                 if (Common.validateRequired(arSOLList.getLocation()) &&
                         Common.validateRequired(arSOLList.getItemName()) &&
                         Common.validateRequired(arSOLList.getUnit()) &&
                         Common.validateRequired(arSOLList.getUnitPrice()) &&
                         Common.validateRequired(arSOLList.getQuantity())) continue;

                 ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

                 mdetails.setSolCode(arSOLList.getInvoiceSalesOrderLineCode());
                 mdetails.setSolLine(Common.convertStringToShort(arSOLList.getLineNumber()));
                 mdetails.setSolIiName(arSOLList.getItemName());
                 mdetails.setSolLocName(arSOLList.getLocation());
                 mdetails.setSolQuantityDelivered(Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit));
                 mdetails.setSolUomName(arSOLList.getUnit());
                 mdetails.setSolUnitPrice(Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit));
                 mdetails.setSolAmount(Common.convertStringMoneyToDouble(arSOLList.getAmount(), precisionUnit));
                 mdetails.setSolDiscount1(Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit));
                 mdetails.setSolDiscount2(Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit));
                 mdetails.setSolDiscount3(Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit));
                 mdetails.setSolDiscount4(Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit));
                 mdetails.setSolTotalDiscount(Common.convertStringMoneyToDouble(arSOLList.getTotalDiscount(), precisionUnit));

                 mdetails.setSolTax(Common.convertBooleanToByte(arSOLList.getTax().equals("Y")?true:false));


                 ArrayList tagList = new ArrayList();
             	   //TODO:save and submit taglist
             	   boolean isTraceMisc = ejbINV.getArTraceMisc(arSOLList.getItemName(), user.getCmpCode());
  	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;

  				String misc= arSOLList.getMisc();

  				System.out.println("misc is: " + misc);
           	   if (isTraceMisc){


           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
           		   mdetails.setSolTagList(tagList);

           		  if(tagList.size()<=0) {
           		     mdetails.setSolMisc(null);
           		   }

           	   }




                 solList.add(mdetails);

             }

 //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}

             try {

                 Integer invoiceCode = ejbINV.saveArInvSolEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         solList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                 actionForm.setInvoiceCode(invoiceCode);
                 
                 
                 ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - SO MATCHED",  new java.util.Date(),user.getUserName(),"SAVE AS DRAFT", actionForm.getLogEntry(),  user.getCmpCode());
                
             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlobalInvItemLocationNotFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalTransactionAlreadyLockedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

             } catch (GlobalInventoryDateException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

             } catch (ArInvDuplicateUploadNumberException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }


           // save attachment
		Integer invCode = actionForm.getInvoiceCode();


           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	

					
					
					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }
/*******************************************************
    -- Ar INV SIL Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("SO MATCHED") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArInvoiceDetails details = new ArInvoiceDetails();

             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvSoNumber(actionForm.getSoNumber());
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToContact(actionForm.getShipToContact());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvLvShift(actionForm.getShift());
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());
             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

             ArrayList solList = new ArrayList();

             for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

                 ArInvoiceSalesOrderList arSOLList = actionForm.getArSOLByIndex(i);

                 if(!arSOLList.getIssueCheckbox()) continue;

                 if (Common.validateRequired(arSOLList.getLocation()) &&
                         Common.validateRequired(arSOLList.getItemName()) &&
                         Common.validateRequired(arSOLList.getUnit()) &&
                         Common.validateRequired(arSOLList.getUnitPrice()) &&
                         Common.validateRequired(arSOLList.getQuantity())) continue;

                 ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

                 mdetails.setSolCode(arSOLList.getInvoiceSalesOrderLineCode());
                 mdetails.setSolLine(Common.convertStringToShort(arSOLList.getLineNumber()));
                 mdetails.setSolIiName(arSOLList.getItemName());
                 mdetails.setSolLocName(arSOLList.getLocation());
                 mdetails.setSolQuantityDelivered(Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit));
                 mdetails.setSolUomName(arSOLList.getUnit());
                 mdetails.setSolUnitPrice(Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit));
                 mdetails.setSolAmount(Common.convertStringMoneyToDouble(arSOLList.getAmount(), precisionUnit));
                 mdetails.setSolDiscount1(Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit));
                 mdetails.setSolDiscount2(Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit));
                 mdetails.setSolDiscount3(Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit));
                 mdetails.setSolDiscount4(Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit));
                 mdetails.setSolTotalDiscount(Common.convertStringMoneyToDouble(arSOLList.getTotalDiscount(), precisionUnit));
                 mdetails.setSolTax(Common.convertBooleanToByte(arSOLList.getTax().equals("Y")?true:false));

                 ArrayList tagList = new ArrayList();
           	   //TODO:save and submit taglist
           	   boolean isTraceMisc = ejbINV.getArTraceMisc(arSOLList.getItemName(), user.getCmpCode());
	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
				String misc= arSOLList.getMisc();


	           	   if (isTraceMisc){


	           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	           		   mdetails.setSolTagList(tagList);

	           		  if(tagList.size()<=0) {
	           		     mdetails.setSolMisc(null);
	           		   }

	           	   }




                 solList.add(mdetails);

             }


            //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}

             try {

                 Integer invoiceCode = ejbINV.saveArInvSolEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         solList, false, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                actionForm.setInvoiceCode(invoiceCode);
                 
                 ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - SO MATCHED",  new java.util.Date(),user.getUserName(),"SAVE AND SUBMIT", actionForm.getLogEntry(),  user.getCmpCode());
                 

             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlobalInvItemLocationNotFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

		
             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalTransactionAlreadyLockedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

             } catch (GlobalInventoryDateException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

             } catch (ArInvDuplicateUploadNumberException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));
 
          } catch (GlobalRecordInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
  	            	  new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }

   // save attachment
		Integer invCode = actionForm.getInvoiceCode();


           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	

					
					
					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }
/*******************************************************
   	 -- Ar INV SIL Print Action --
*******************************************************/

         } else if ((request.getParameter("printButton") != null || request.getParameter("drPrintButton") != null) && actionForm.getType().equals("SO MATCHED")) {

        	 System.out.println("PRINT BUTTON SO MATCHED");
             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArInvoiceDetails details = new ArInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvSoNumber(actionForm.getSoNumber());
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());
                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvLvShift(actionForm.getShift());
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
                 if (actionForm.getInvoiceCode() == null) {

                     details.setInvCreatedBy(user.getUserName());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());
                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                 ArrayList solList = new ArrayList();

                 for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

                     ArInvoiceSalesOrderList arSOLList = actionForm.getArSOLByIndex(i);

                     if(!arSOLList.getIssueCheckbox()) continue;

                     if (Common.validateRequired(arSOLList.getLocation()) &&
                             Common.validateRequired(arSOLList.getItemName()) &&
                             Common.validateRequired(arSOLList.getUnit()) &&
                             Common.validateRequired(arSOLList.getUnitPrice()) &&
                             Common.validateRequired(arSOLList.getQuantity())) continue;

                     ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

                     mdetails.setSolCode(arSOLList.getInvoiceSalesOrderLineCode());
                     mdetails.setSolLine(Common.convertStringToShort(arSOLList.getLineNumber()));
                     mdetails.setSolIiName(arSOLList.getItemName());
                     mdetails.setSolLocName(arSOLList.getLocation());
                     mdetails.setSolQuantityDelivered(Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit));
                     mdetails.setSolUomName(arSOLList.getUnit());
                     mdetails.setSolUnitPrice(Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit));
                     mdetails.setSolAmount(Common.convertStringMoneyToDouble(arSOLList.getAmount(), precisionUnit));
                     mdetails.setSolDiscount1(Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit));
                     mdetails.setSolDiscount2(Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit));
                     mdetails.setSolDiscount3(Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit));
                     mdetails.setSolDiscount4(Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit));
                     mdetails.setSolTotalDiscount(Common.convertStringMoneyToDouble(arSOLList.getTotalDiscount(), precisionUnit));
                     mdetails.setSolTax(Common.convertBooleanToByte(arSOLList.getTax().equals("Y")?true:false));

                     ArrayList tagList = new ArrayList();
               	   //TODO:save and submit taglist
               	   boolean isTraceMisc = ejbINV.getArTraceMisc(arSOLList.getItemName(), user.getCmpCode());
    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
    				String misc= arSOLList.getMisc();

    	           	   if (isTraceMisc){


    	           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
    	           		   mdetails.setSolTagList(tagList);

    	           		  if(tagList.size()<=0) {
    	           		     mdetails.setSolMisc(null);
    	           		   }

    	           	   }



                     solList.add(mdetails);

                 }


               //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}
                 try {

                     Integer invoiceCode = ejbINV.saveArInvSolEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             solList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     actionForm.setInvoiceCode(invoiceCode);
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - SO MATCHED",  new java.util.Date(),user.getUserName(),"PRINT", actionForm.getLogEntry(),  user.getCmpCode());
                     

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlobalInvItemLocationNotFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));

                 } catch (GlobalTransactionAlreadyLockedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

                 } catch (GlobalInventoryDateException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (ArInvDuplicateUploadNumberException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

                  } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }

   // save attachment
		Integer invCode = actionForm.getInvoiceCode();


           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

                 
       	   		if(request.getParameter("drPrintButton") != null){
       	   		  	actionForm.setReport2(Constants.STATUS_SUCCESS);

                 	
       	   		}else{
       	   		 	 actionForm.setReport(Constants.STATUS_SUCCESS);

                 
       	   		
       	   		}

                 isInitialPrinting = true;

             }  else {

                 
       	   		if(request.getParameter("drPrintButton") != null){
       	   		  	actionForm.setReport2(Constants.STATUS_SUCCESS);

                 	
       	   		}else{
       	   		 	 actionForm.setReport(Constants.STATUS_SUCCESS);

                 
       	   		
       	   		}

                 return(mapping.findForward("arInvoiceEntry"));

             }


/*******************************************************
   -- Ar INV SIL Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("SO MATCHED")) {

             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArInvoiceDetails details = new ArInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvSoNumber(actionForm.getSoNumber());
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());
                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvLvShift(actionForm.getShift());
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 if (actionForm.getInvoiceCode() == null) {

                     details.setInvCreatedBy(user.getUserName());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());
                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                 ArrayList solList = new ArrayList();

                 for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

                     ArInvoiceSalesOrderList arSOLList = actionForm.getArSOLByIndex(i);

                     if(!arSOLList.getIssueCheckbox()) continue;

                     if (Common.validateRequired(arSOLList.getLocation()) &&
                             Common.validateRequired(arSOLList.getItemName()) &&
                             Common.validateRequired(arSOLList.getUnit()) &&
                             Common.validateRequired(arSOLList.getUnitPrice()) &&
                             Common.validateRequired(arSOLList.getQuantity())) continue;

                     ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

                     mdetails.setSolCode(arSOLList.getInvoiceSalesOrderLineCode());
                     mdetails.setSolLine(Common.convertStringToShort(arSOLList.getLineNumber()));
                     mdetails.setSolIiName(arSOLList.getItemName());
                     mdetails.setSolLocName(arSOLList.getLocation());
                     mdetails.setSolQuantityDelivered(Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit));
                     mdetails.setSolUomName(arSOLList.getUnit());
                     mdetails.setSolUnitPrice(Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit));
                     mdetails.setSolAmount(Common.convertStringMoneyToDouble(arSOLList.getAmount(), precisionUnit));
                     mdetails.setSolDiscount1(Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit));
                     mdetails.setSolDiscount2(Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit));
                     mdetails.setSolDiscount3(Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit));
                     mdetails.setSolDiscount4(Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit));
                     mdetails.setSolTotalDiscount(Common.convertStringMoneyToDouble(arSOLList.getTotalDiscount(), precisionUnit));
                     mdetails.setSolMisc(arSOLList.getMisc());
                     mdetails.setSolTax(Common.convertBooleanToByte(arSOLList.getTax().equals("Y")?true:false));

                     ArrayList tagList = new ArrayList();
               	   //TODO:save and submit taglist
               	   boolean isTraceMisc = ejbINV.getArTraceMisc(arSOLList.getItemName(), user.getCmpCode());
    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
    				String misc= arSOLList.getMisc();


    	           	   if (isTraceMisc){


    	           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
    	           		   mdetails.setSolTagList(tagList);

    	           		 if(tagList.size()<=0) {
    	           		     mdetails.setSolMisc(null);
    	           		   }

    	           	   }



                     solList.add(mdetails);

                 }

 //validate attachment
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

    	    	if (actionForm.getFilename1().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename1NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename1SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	    	if (actionForm.getFilename2().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename2NotFound"));

    	    	} else {

    	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename2SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	    	if (actionForm.getFilename3().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename3NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename3().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename3SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	    	if (actionForm.getFilename4().getFileSize() == 0) {

    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("purchaseRequisitionEntry.error.filename4NotFound"));

    	    	} else {

    	    		
           	    	InputStream is = actionForm.getFilename4().getInputStream();

           	    	if (is.available() > maxAttachmentFileSize) {

           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("purchaseRequisitionEntry.error.filename4SizeInvalid"));

           	    	}

           	    	is.close();

    	    	}

    	    	if (!errors.isEmpty()) {

    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("arInvoiceEntry"));

    	    	}

    	   	}
                 try {

                     Integer invoiceCode = ejbINV.saveArInvSolEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             solList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     actionForm.setInvoiceCode(invoiceCode);
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - SO MATCHED",  new java.util.Date(),user.getUserName(),"JOURNAL", actionForm.getLogEntry(),  user.getCmpCode());
                     

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlobalInvItemLocationNotFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));

                 } catch (GlobalTransactionAlreadyLockedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

                 } catch (GlobalInventoryDateException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (ArInvDuplicateUploadNumberException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

                  } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }


            // save attachment
		Integer invCode = actionForm.getInvoiceCode();


           if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {
       	        
       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();


       	        	

					
					
					
       	    		File fileTest = new File(attachmentPath + invCode + "-1.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode+ "-1-" + filename1);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-1-" + filename1);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();
					
       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode+ "-2.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-2-" + filename2);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode+ "-2-" + filename2);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();



       	    		File fileTest = new File(attachmentPath + invCode + "-3.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-3-" + filename3);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-3-" + filename3);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		

       	    		File fileTest = new File(attachmentPath + invCode + "-4.txt");
       	        	FileWriter writer = new FileWriter(fileTest);
       	            writer.write(invCode + "-4-" + filename4);
       	            writer.close();

       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + invCode + "-4-" + filename4);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }
                 if (!errors.isEmpty()) {

                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("arInvoiceEntry"));

                 }
             }

             String path = "/arJournal.do?forward=1" +
             "&transactionCode=" + actionForm.getInvoiceCode() +
             "&transaction=INVOICE" +
             "&transactionNumber=" + actionForm.getInvoiceNumber() +
             "&transactionDate=" + actionForm.getDate() +
             "&transactionEnableFields=" + actionForm.getEnableFields();

             return(new ActionForward(path));
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             

/*******************************************************
    -- Ar INV JO Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("JO MATCHED") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArInvoiceDetails details = new ArInvoiceDetails();

           	 System.out.println(actionForm.getInvoiceCode() + " is the code");
             
             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvJoNumber(actionForm.getJoNumber());
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToContact(actionForm.getShipToContact());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvLvShift(actionForm.getShift());
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());
             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

             ArrayList joList = new ArrayList();

             for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

                 ArInvoiceJobOrderList arJOList = actionForm.getArJOLByIndex(i);

                 if(!arJOList.getIssueCheckbox()) continue;

                 if (Common.validateRequired(arJOList.getLocation()) &&
                         Common.validateRequired(arJOList.getItemName()) &&
                         Common.validateRequired(arJOList.getUnit()) &&
                         Common.validateRequired(arJOList.getUnitPrice()) &&
                         Common.validateRequired(arJOList.getQuantity())) continue;

              
                 
                 ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();

                 mdetails.setJolCode(arJOList.getInvoiceJobOrderLineCode());
                 mdetails.setJolLine(Common.convertStringToShort(arJOList.getLineNumber()));
                 mdetails.setJolIiName(arJOList.getItemName());
                 mdetails.setJolLocName(arJOList.getLocation());
                 mdetails.setJolQuantityDelivered(Common.convertStringMoneyToDouble(arJOList.getQuantity(), precisionUnit));
                 mdetails.setJolUomName(arJOList.getUnit());
                 mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOList.getUnitPrice(), precisionUnit));
                 mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOList.getAmount(), precisionUnit));
                 mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOList.getDiscount1(), precisionUnit));
                 mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOList.getDiscount2(), precisionUnit));
                 mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOList.getDiscount3(), precisionUnit));
                 mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOList.getDiscount4(), precisionUnit));
                 mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOList.getTotalDiscount(), precisionUnit));

                 mdetails.setJolTax(Common.convertBooleanToByte(arJOList.getTax().equals("Y")?true:false));


                 ArrayList tagList = new ArrayList();
             	   //TODO:save and submit taglist
             	   boolean isTraceMisc = ejbINV.getArTraceMisc(arJOList.getItemName(), user.getCmpCode());
  	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;

  				String misc= arJOList.getMisc();

  				System.out.println("misc is: " + misc);
           	   if (isTraceMisc){


           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
           		   mdetails.setJolTagList(tagList);

           		  if(tagList.size()<=0) {
           		     mdetails.setJolMisc(null);
           		   }

           	   }




           	   joList.add(mdetails);

             }

             

             try {

         
                 Integer invoiceCode = ejbINV.saveArInvJolEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         joList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                 actionForm.setInvoiceCode(invoiceCode);
                 
                 ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - JO MATCHED",  new java.util.Date(),user.getUserName(),"SAVE AS DRAFT", actionForm.getLogEntry(),  user.getCmpCode());
                 
             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlobalInvItemLocationNotFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalTransactionAlreadyLockedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

             } catch (GlobalInventoryDateException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

             } catch (ArInvDuplicateUploadNumberException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }


        
/*******************************************************
    -- Ar INV SIL Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("JO MATCHED") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArInvoiceDetails details = new ArInvoiceDetails();

             details.setInvCode(actionForm.getInvoiceCode());
             details.setInvDocumentType(actionForm.getDocumentType());
       
             details.setInvType(actionForm.getType());
             details.setInvMemo(actionForm.getMemo());
             details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
             details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setInvNumber(actionForm.getInvoiceNumber());
             details.setInvReferenceNumber(actionForm.getReferenceNumber());
             details.setInvUploadNumber(actionForm.getUploadNumber());
             details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
             details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
             details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
             details.setInvJoNumber(actionForm.getJoNumber());
             details.setInvDescription(actionForm.getDescription());
             details.setInvBillToAddress(actionForm.getBillTo());
             details.setInvBillToContact(actionForm.getBillToContact());
             details.setInvBillToAltContact(actionForm.getBillToAltContact());
             details.setInvBillToPhone(actionForm.getBillToPhone());
             details.setInvBillingHeader(actionForm.getBillingHeader());
             details.setInvBillingFooter(actionForm.getBillingFooter());
             details.setInvBillingHeader2(actionForm.getBillingHeader2());
             details.setInvBillingFooter2(actionForm.getBillingFooter2());
             details.setInvBillingSignatory(actionForm.getBillingSignatory());
             details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
             details.setInvShipToAddress(actionForm.getShipTo());
             details.setInvShipToContact(actionForm.getShipToContact());
             details.setInvShipToAltContact(actionForm.getShipToAltContact());
             details.setInvShipToPhone(actionForm.getShipToPhone());
             details.setInvLvFreight(actionForm.getFreight());
             details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
             details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
             details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
             details.setInvLvShift(actionForm.getShift());
             details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
             details.setInvClientPO(actionForm.getClientPO());
             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
             if (actionForm.getInvoiceCode() == null) {

                 details.setInvCreatedBy(user.getUserName());
                 details.setInvDateCreated(new java.util.Date());

             }

             details.setInvLastModifiedBy(user.getUserName());
             details.setInvDateLastModified(new java.util.Date());
             details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

             ArrayList jolList = new ArrayList();

             for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

                 ArInvoiceJobOrderList arJOLList = actionForm.getArJOLByIndex(i);

                 if(!arJOLList.getIssueCheckbox()) continue;

                 if (Common.validateRequired(arJOLList.getLocation()) &&
                         Common.validateRequired(arJOLList.getItemName()) &&
                         Common.validateRequired(arJOLList.getUnit()) &&
                         Common.validateRequired(arJOLList.getUnitPrice()) &&
                         Common.validateRequired(arJOLList.getQuantity())) continue;

                 ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();

                 mdetails.setJolCode(arJOLList.getInvoiceJobOrderLineCode());
                 mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
                 mdetails.setJolIiName(arJOLList.getItemName());
                 mdetails.setJolLocName(arJOLList.getLocation());
                 mdetails.setJolQuantityDelivered(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
                 mdetails.setJolUomName(arJOLList.getUnit());
                 mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
                 mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
                 mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
                 mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
                 mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
                 mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
                 mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
                 mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));

                 ArrayList tagList = new ArrayList();
           	   //TODO:save and submit taglist
           	   boolean isTraceMisc = ejbINV.getArTraceMisc(arJOLList.getItemName(), user.getCmpCode());
	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
				String misc= arJOLList.getMisc();


	           	   if (isTraceMisc){


	           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	           		   mdetails.setJolTagList(tagList);

	           		  if(tagList.size()<=0) {
	           		     mdetails.setJolMisc(null);
	           		   }

	           	   }




                 jolList.add(mdetails);

             }



             try {

                 Integer invoiceCode = ejbINV.saveArInvJolEntry(details, actionForm.getPaymentTerm(),
                         actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                         actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                         jolList, false, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 actionForm.setInvoiceCode(invoiceCode);
                 
                 ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - JO MATCHED",  new java.util.Date(),user.getUserName(),"SAVE AND SUBMIT", actionForm.getLogEntry(),  user.getCmpCode());
                 

             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

             } catch (GlobalPaymentTermInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

             } catch (GlobalInvItemLocationNotFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.journalNotBalance"));

             } catch (GlobalTransactionAlreadyLockedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

             } catch (GlobalInventoryDateException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

             } catch (ArINVAmountExceedsCreditLimitException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

             } catch (ArInvDuplicateUploadNumberException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

             } catch (EJBException ex) {
                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));
             }


/*******************************************************
   	 -- Ar INV JOL Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("JO MATCHED")) {

             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArInvoiceDetails details = new ArInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvJoNumber(actionForm.getJoNumber());
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());
                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvLvShift(actionForm.getShift());
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));
                 if (actionForm.getInvoiceCode() == null) {

                     details.setInvCreatedBy(user.getUserName());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());
                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                 ArrayList jolList = new ArrayList();

                 for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

                     ArInvoiceJobOrderList arJOLList = actionForm.getArJOLByIndex(i);

                     if(!arJOLList.getIssueCheckbox()) continue;

                     if (Common.validateRequired(arJOLList.getLocation()) &&
                             Common.validateRequired(arJOLList.getItemName()) &&
                             Common.validateRequired(arJOLList.getUnit()) &&
                             Common.validateRequired(arJOLList.getUnitPrice()) &&
                             Common.validateRequired(arJOLList.getQuantity())) continue;

                     ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();

                     mdetails.setJolCode(arJOLList.getInvoiceJobOrderLineCode());
                     mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
                     mdetails.setJolIiName(arJOLList.getItemName());
                     mdetails.setJolLocName(arJOLList.getLocation());
                     mdetails.setJolQuantityDelivered(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
                     mdetails.setJolUomName(arJOLList.getUnit());
                     mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
                     mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
                     mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
                     mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
                     mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
                     mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
                     mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
                     mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));

                     ArrayList tagList = new ArrayList();
               	   //TODO:save and submit taglist
               	   boolean isTraceMisc = ejbINV.getArTraceMisc(arJOLList.getItemName(), user.getCmpCode());
    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
    				String misc= arJOLList.getMisc();

    	           	   if (isTraceMisc){


    	           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
    	           		   mdetails.setJolTagList(tagList);

    	           		  if(tagList.size()<=0) {
    	           		     mdetails.setJolMisc(null);
    	           		   }

    	           	   }



                     jolList.add(mdetails);

                 }



                 try {

                     Integer invoiceCode = ejbINV.saveArInvJolEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             jolList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     actionForm.setInvoiceCode(invoiceCode);
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - JO MATCHED",  new java.util.Date(),user.getUserName(),"PRINT", actionForm.getLogEntry(),  user.getCmpCode());
                     

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlobalInvItemLocationNotFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));

                 } catch (GlobalTransactionAlreadyLockedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

                 } catch (GlobalInventoryDateException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (ArInvDuplicateUploadNumberException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

                  } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }



                 actionForm.setReport(Constants.STATUS_SUCCESS);

                 isInitialPrinting = true;

             }  else {

                 actionForm.setReport(Constants.STATUS_SUCCESS);

                 return(mapping.findForward("arInvoiceEntry"));

             }

/*******************************************************
   -- Ar INV JIL Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("JO MATCHED")) {

             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 ArInvoiceDetails details = new ArInvoiceDetails();

                 details.setInvCode(actionForm.getInvoiceCode());
                 details.setInvDocumentType(actionForm.getDocumentType());
                 details.setInvType(actionForm.getType());
                 details.setInvMemo(actionForm.getMemo());
                 details.setInvDownPayment(Common.convertStringMoneyToDouble(actionForm.getDownPayment(), Constants.MONEY_RATE_PRECISION));
                 details.setInvDate(Common.convertStringToSQLDate(actionForm.getDate()));
                 details.setInvNumber(actionForm.getInvoiceNumber());
                 details.setInvReferenceNumber(actionForm.getReferenceNumber());
                 details.setInvUploadNumber(actionForm.getUploadNumber());
                 details.setInvVoid(Common.convertBooleanToByte(actionForm.getInvoiceVoid()));
                 details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));
                 details.setInvInterest(Common.convertBooleanToByte(actionForm.getInvoiceInterest()));
                 details.setInvJoNumber(actionForm.getJoNumber());
                 details.setInvDescription(actionForm.getDescription());
                 details.setInvBillToAddress(actionForm.getBillTo());
                 details.setInvBillToContact(actionForm.getBillToContact());
                 details.setInvBillToAltContact(actionForm.getBillToAltContact());
                 details.setInvBillToPhone(actionForm.getBillToPhone());
                 details.setInvBillingHeader(actionForm.getBillingHeader());
                 details.setInvBillingFooter(actionForm.getBillingFooter());
                 details.setInvBillingHeader2(actionForm.getBillingHeader2());
                 details.setInvBillingFooter2(actionForm.getBillingFooter2());
                 details.setInvBillingSignatory(actionForm.getBillingSignatory());
                 details.setInvSignatoryTitle(actionForm.getSignatoryTitle());
                 details.setInvShipToAddress(actionForm.getShipTo());
                 details.setInvShipToContact(actionForm.getShipToContact());
                 details.setInvShipToAltContact(actionForm.getShipToAltContact());
                 details.setInvShipToPhone(actionForm.getShipToPhone());
                 details.setInvLvFreight(actionForm.getFreight());
                 details.setInvShipDate(Common.convertStringToSQLDate(actionForm.getShipDate()));
                 details.setInvConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setInvConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 details.setInvPreviousReading(Common.convertStringMoneyToDouble(actionForm.getPreviousReading(), Constants.DEFAULT_PRECISION));
                 details.setInvPresentReading(Common.convertStringMoneyToDouble(actionForm.getPresentReading(), Constants.DEFAULT_PRECISION));
                 details.setInvLvShift(actionForm.getShift());
                 details.setInvDebitMemo(Common.convertBooleanToByte(actionForm.getDebitMemo()));
                 details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
                 details.setInvClientPO(actionForm.getClientPO());
                 details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                 if (actionForm.getInvoiceCode() == null) {

                     details.setInvCreatedBy(user.getUserName());
                     details.setInvDateCreated(new java.util.Date());

                 }

                 details.setInvLastModifiedBy(user.getUserName());
                 details.setInvDateLastModified(new java.util.Date());
                 details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                 ArrayList jolList = new ArrayList();

                 for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

                     ArInvoiceJobOrderList arJOLList = actionForm.getArJOLByIndex(i);

                     if(!arJOLList.getIssueCheckbox()) continue;

                     if (Common.validateRequired(arJOLList.getLocation()) &&
                             Common.validateRequired(arJOLList.getItemName()) &&
                             Common.validateRequired(arJOLList.getUnit()) &&
                             Common.validateRequired(arJOLList.getUnitPrice()) &&
                             Common.validateRequired(arJOLList.getQuantity())) continue;

                     ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();

                     mdetails.setJolCode(arJOLList.getInvoiceJobOrderLineCode());
                     mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
                     mdetails.setJolIiName(arJOLList.getItemName());
                     mdetails.setJolLocName(arJOLList.getLocation());
                     mdetails.setJolQuantityDelivered(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
                     mdetails.setJolUomName(arJOLList.getUnit());
                     mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
                     mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
                     mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
                     mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
                     mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
                     mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
                     mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
                     mdetails.setJolMisc(arJOLList.getMisc());
                     mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));

                     ArrayList tagList = new ArrayList();
               	   //TODO:save and submit taglist
               	   boolean isTraceMisc = ejbINV.getArTraceMisc(arJOLList.getItemName(), user.getCmpCode());
    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
    				String misc= arJOLList.getMisc();


    	           	   if (isTraceMisc){


    	           		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
    	           		   mdetails.setJolTagList(tagList);

    	           		 if(tagList.size()<=0) {
    	           		     mdetails.setJolMisc(null);
    	           		   }

    	           	   }



                     jolList.add(mdetails);

                 }


                 try {

                	 System.out.println(details.getInvCode() + " is the code");
                	 
                     Integer invoiceCode = ejbINV.saveArInvJolEntry(details, actionForm.getPaymentTerm(),
                             actionForm.getTaxCode(), actionForm.getWithholdingTax(),
                             actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
                             jolList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     actionForm.setInvoiceCode(invoiceCode);
                     
                     ejbLOG.addAdLogEntry(invoiceCode,"AR INVOICE - JO MATCHED",  new java.util.Date(),user.getUserName(),"JOURNAL", actionForm.getLogEntry(),  user.getCmpCode());
                     

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.documentNumberNotUnique"));

                 } catch (GlobalConversionDateNotExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

                 } catch (GlobalPaymentTermInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.paymentTermInvalid"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalTransactionAlreadyVoidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyVoid"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noApprovalApproverFound"));

                 } catch (GlobalInvItemLocationNotFoundException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noItemLocationFound", ex.getMessage()));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.journalNotBalance"));

                 } catch (GlobalTransactionAlreadyLockedException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.transactionAlreadyLocked"));

                 } catch (GlobalInventoryDateException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                 } catch (GlobalBranchAccountNumberInvalidException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

                 } catch (ArINVAmountExceedsCreditLimitException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.amountExceedsCreditLimit"));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invoiceEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (ArInvDuplicateUploadNumberException ex) {

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.duplicateUploadNumber"));

                  } catch (EJBException ex) {
                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                     }

                     return(mapping.findForward("cmnErrorPage"));
                 }


          
                 if (!errors.isEmpty()) {

                     saveErrors(request, new ActionMessages(errors));
                     return(mapping.findForward("arInvoiceEntry"));

                 }
             }

             String path = "/arJournal.do?forward=1" +
             "&transactionCode=" + actionForm.getInvoiceCode() +
             "&transaction=INVOICE" +
             "&transactionNumber=" + actionForm.getInvoiceNumber() +
             "&transactionDate=" + actionForm.getDate() +
             "&transactionEnableFields=" + actionForm.getEnableFields();

             return(new ActionForward(path));
             
             
             

/*******************************************************
  -- Ar INV Save Received Date Action --
*******************************************************/

           } else if (request.getParameter("saveReceivedDateButton") != null &&
                           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                       ArInvoiceDetails details = new ArInvoiceDetails();
                       details.setInvCode(actionForm.getInvoiceCode());
                       details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getRecieveDate()));

                       ejbINV.saveRecievedDate(details, new Integer(user.getCurrentBranch().getBrCode()),
                    		   user.getCmpCode());
                       
                       ejbLOG.addAdLogEntry(actionForm.getInvoiceCode(),"AR INVOICE",  new java.util.Date(),user.getUserName(),"SAVE RECEIVED DATE", actionForm.getLogEntry(),  user.getCmpCode());
                       

                       saveMessages(request, messages);
                       actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

                       return(mapping.findForward("arInvoiceEntry"));



   /*******************************************************
   -- Ar INV Save Received Date Action --
 *******************************************************/

    } else if (request.getParameter("disableInterestButton") != null &&
                    actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                ArInvoiceDetails details = new ArInvoiceDetails();
                details.setInvCode(actionForm.getInvoiceCode());

                details.setInvDisableInterest(Common.convertBooleanToByte(actionForm.getDisableInterest()));


                ejbINV.saveDisableInterest(details, new Integer(user.getCurrentBranch().getBrCode()),
             		   user.getCmpCode());
                
                

                saveMessages(request, messages);
                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

                return(mapping.findForward("arInvoiceEntry"));



/*******************************************************
   -- Ar INV Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

             return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar INV Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {

             int listSize = actionForm.getArINVListSize();
             int lineNumber = 0;

             for (int x = listSize + 1; x <= listSize + invoiceLineNumber; x++) {

                 ArInvoiceEntryList arINVList = new ArInvoiceEntryList(actionForm,
                         null, String.valueOf(++lineNumber), null, null, null, null, null, false);

                 arINVList.setMemoLineList(actionForm.getMemoLineList());

                 actionForm.saveArINVList(arINVList);

             }

             for (int i = 0; i<actionForm.getArINVListSize(); i++) {

                 ArInvoiceEntryList arINVList = actionForm.getArINVByIndex(i);

                 arINVList.setLineNumber(String.valueOf(i+1));

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
   -- Ar INV Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {

             for (int i = 0; i<actionForm.getArINVListSize(); i++) {

                 ArInvoiceEntryList arINVList = actionForm.getArINVByIndex(i);

                 if (arINVList.getDeleteCheckbox()) {

                     actionForm.deleteArINVList(i);
                     i--;
                 }

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
   -- Ar INV ILI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {

             int listSize = actionForm.getArILIListSize();

             for (int x = listSize + 1; x <= listSize + invoiceLineNumber; x++) {

                 ArInvoiceLineItemList arILIList = new ArInvoiceLineItemList(actionForm, null, String.valueOf(x), null,
                         null, null, null, null, null, null, false, false, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null ,"Y");

                 arILIList.setLocationList(actionForm.getLocationList());

                 arILIList.setTaxList(actionForm.getTaxList());
                 actionForm.saveArILIList(arILIList);

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
   -- Ar INV ILI Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("ITEMS")) {

             for (int i = 0; i<actionForm.getArILIListSize(); i++) {

                 ArInvoiceLineItemList arILIList = actionForm.getArILIByIndex(i);

                 if (arILIList.getDeleteCheckbox()) {

                     actionForm.deleteArILIList(i);
                     i--;
                 }

             }

             for (int i = 0; i<actionForm.getArILIListSize(); i++) {

                 ArInvoiceLineItemList arILIList = actionForm.getArILIByIndex(i);

                 arILIList.setLineNumber(String.valueOf(i+1));

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
   -- Ar INV Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

             try {

                 ejbINV.deleteArInvEntry(actionForm.getInvoiceCode(), user.getUserName(), user.getCmpCode());

                 ejbLOG.addAdLogEntry(actionForm.getInvoiceCode(),"AR INVOICE",  new java.util.Date(),user.getUserName(),"DELETE", actionForm.getLogEntry(),  user.getCmpCode());
                 
                 
             } catch (GlobalRecordAlreadyDeletedException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.recordAlreadyDeleted"));

             } catch (EJBException ex) {

                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());

                 }

                 return(mapping.findForward("cmnErrorPage"));

             }

/*******************************************************
  -- Ar INV Credit Memo Change Action --
*******************************************************/

     } else if(!Common.validateRequired(request.getParameter("isCreditMemoChanged"))) {



/*******************************************************
  -- Ar INV Credit Memo Change Action --
*******************************************************/

     } else if(!Common.validateRequired(request.getParameter("isProjectTypeEntered"))) {


    	 actionForm.clearProjectPhaseNameList();

    	 ArrayList list = null;
         Iterator i = null;


         list = ejbINV.getPmOpenPpByPrjProjectCodeAndPtProjectTypeCode(actionForm.getProjectCode(), actionForm.getProjectTypeCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

         if (list == null || list.size() == 0) {

             actionForm.setProjectPhaseNameList(Constants.GLOBAL_NO_RECORD_FOUND);

         } else {

             i = list.iterator();

             while (i.hasNext()) {

                 actionForm.setProjectPhaseNameList((String)i.next());

             }

         }



         actionForm.clearContractTermNameList();

    	 ArrayList listCt = null;
         Iterator iCt = null;


         listCt = ejbINV.getPmOpenCtByPtProjectTypeCode(actionForm.getProjectCode(), actionForm.getProjectTypeCode(), user.getCmpCode());

         if (listCt == null || listCt.size() == 0) {

             actionForm.setContractTermNameList(Constants.GLOBAL_NO_RECORD_FOUND);

         } else {

        	 iCt = listCt.iterator();

             while (iCt.hasNext()) {

                 actionForm.setContractTermNameList((String)iCt.next());

             }

         }


         return(mapping.findForward("arInvoiceEntry"));


/*******************************************************
   -- Ar INV Customer Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

             try {

                 ArModCustomerDetails mdetails = ejbINV.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());

                 actionForm.setPaymentTerm(mdetails.getCstPytName());
                 actionForm.setTaxCode(mdetails.getCstCcTcName());
                 actionForm.setDeployedBranchName(mdetails.getCstHrDeployedBranchName());
                 actionForm.setEmployeeNumber(mdetails.getCstHrEmployeeNumber());
                 actionForm.setManagingBranch(mdetails.getCstHrManagingBranch());
                 actionForm.setBioNumber(mdetails.getCstHrBioNumber());
                 actionForm.setCurrentJobPosition(mdetails.getCstHrCurrentJobPosition());

	    		

	    		  if (mdetails.getCstSlpSalespersonCode() != null && mdetails.getCstSlpSalespersonCode2() != null) {

	    			  actionForm.clearSalespersonList();
	    			  actionForm.clearSalespersonNameList();
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode());
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode2());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode() + " - " + mdetails.getCstSlpName());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode2() + " - " + mdetails.getCstSlpName2());

	    		  } else {
	    			  	actionForm.clearSalespersonList();
	    	          	actionForm.clearSalespersonNameList();

	    	          	ArrayList list = ejbINV.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    	          	if ( list == null || list.size() == 0) {

	    	          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
	    	          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

	    	          	} else {

	    	          		Iterator i =list.iterator();

	    	          		while (i.hasNext()) {

	    	          			ArSalespersonDetails slpDetails = (ArSalespersonDetails)i.next();

	    	          			actionForm.setSalespersonList(slpDetails.getSlpSalespersonCode());
	    	          			actionForm.setSalespersonNameList(slpDetails.getSlpSalespersonCode() + " - " + slpDetails.getSlpName());

	    	          		}

	    	          	}
	    		  }

                 actionForm.setPaymentTerm(mdetails.getCstPytName());
                 actionForm.setTaxCode(mdetails.getCstCcTcName());
                 actionForm.setWithholdingTax(mdetails.getCstCcWtcName());
                 actionForm.setBillTo(mdetails.getCstBillToAddress());
                 actionForm.setBillToContact(mdetails.getCstBillToContact());
                 actionForm.setBillToAltContact(mdetails.getCstBillToAltContact());
                 actionForm.setBillToPhone(mdetails.getCstBillToPhone());
                 actionForm.setBillingHeader(mdetails.getCstBillingHeader());
                 actionForm.setBillingFooter(mdetails.getCstBillingFooter());
                 actionForm.setBillingHeader2(mdetails.getCstBillingHeader2());
                 actionForm.setBillingFooter2(mdetails.getCstBillingFooter2());
                 actionForm.setBillingHeader3(mdetails.getCstBillingHeader3());
                 actionForm.setBillingFooter3(mdetails.getCstBillingFooter3());

                 actionForm.setBillingSignatory(mdetails.getCstBillingSignatory());
                 actionForm.setSignatoryTitle(mdetails.getCstSignatoryTitle());
                 actionForm.setShipTo(mdetails.getCstShipToAddress());
                 actionForm.setShipToContact(mdetails.getCstShipToContact());
                 actionForm.setShipToAltContact(mdetails.getCstShipToAltContact());
                 actionForm.setShipToPhone(mdetails.getCstShipToPhone());
                 actionForm.setSalesperson(mdetails.getCstSlpSalespersonCode());
                 actionForm.setCustomerName(mdetails.getCstName());
                 actionForm.setCustomerClass(mdetails.getCstCcName());
                 System.out.println(actionForm.getCustomerClass() + " frm action");
                 actionForm.setTaxRate(mdetails.getCstCcTcRate());
                 actionForm.setTaxType(mdetails.getCstCcTcType());

                 if(actionForm.getType().equalsIgnoreCase("ITEMS") && actionForm.getType() != null) {

                     if(mdetails.getCstLitName() != null && mdetails.getCstLitName().length() > 0) {

                         actionForm.clearArILIList();

                         ArrayList list = ejbINV.getInvLitByCstLitName(mdetails.getCstLitName(), user.getCmpCode());

                         if (!list.isEmpty()) {

                             Iterator i = list.iterator();

                             while (i.hasNext()) {

                                 InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();

                                 ArInvoiceLineItemList arILIList = new ArInvoiceLineItemList(actionForm, null,
                                         Common.convertShortToString(liDetails.getLiLine()), liDetails.getLiLocName(),
                                         liDetails.getLiIiName(), liDetails.getLiIiDescription(), "0",
                                         liDetails.getLiUomName(), null, null, false, false, false, "0.00", "0.00", "0.00", "0.00", "0.00", "0.00", null, "Y");

                                 String itemClass = ejbINV.getInvIiClassByIiName(arILIList.getItemName(), user.getCmpCode());

                                 if(itemClass.equals("Assembly")) {

                                     arILIList.setIsAssemblyItem(true);

                                 } else {

                                     arILIList.setIsAssemblyItem(false);

                                 }

                                 if(ejbINV.getInvAutoBuildEnabledByIiName(arILIList.getItemName(), user.getCmpCode())) {

                                     arILIList.setAutoBuildCheckbox(true);

                                 } else {

                                     arILIList.setAutoBuildCheckbox(false);

                                 }

                                 // populate location list

                                 arILIList.setLocationList(actionForm.getLocationList());

                                 // populate tax line list

                                 arILIList.setTaxList(actionForm.getTaxList());

                                 // populate unit list

                                 ArrayList uomList = new ArrayList();
                                 uomList = ejbINV.getInvUomByIiName(arILIList.getItemName(), user.getCmpCode());

                                 arILIList.clearUnitList();
                                 arILIList.setUnitList(Constants.GLOBAL_BLANK);

                                 Iterator j = uomList.iterator();

                                 while (j.hasNext()) {

                                     InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();

                                     arILIList.setUnitList(mUomDetails.getUomName());

                                 }

                                 // populate unit cost field

                                 if (!Common.validateRequired(arILIList.getItemName()) && !Common.validateRequired(arILIList.getUnit())) {

                                      // populate unit cost field

					                 if (!Common.validateRequired(arILIList.getItemName()) && !Common.validateRequired(arILIList.getUnit())) {
					
										
										
										if(!Common.validateRequired(actionForm.getDate()) && Common.validateDateFormat(actionForm.getDate())){
												
												
											 double unitPrice = ejbINV.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arILIList.getItemName(), Common.convertStringToSQLDate(actionForm.getDate()) ,arILIList.getUnit(), user.getCmpCode());
					                    	 arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
						
												
												
												
										}else{
										  	double unitPrice = ejbINV.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arILIList.getItemName(),  arILIList.getUnit(), user.getCmpCode());
					                        arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
					
										
										}
					                 }

                                 }

                                 // populate amount field

                                 if(!Common.validateRequired(arILIList.getQuantity()) && !Common.validateRequired(arILIList.getUnitPrice())) {

                                     double amount = Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) *
                                     Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit);
                                     arILIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

                                 }

                                 actionForm.saveArILIList(arILIList);

                             }

                         }

                     } else {

                         actionForm.clearArILIList();

                         for (int x = 1; x <= invoiceLineNumber; x++) {

                             ArInvoiceLineItemList arILIList = new ArInvoiceLineItemList(actionForm,
                                     null, new Integer(x).toString(), null, null, null, null, null,
                                     null, null, false, false, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null ,"Y");

                             arILIList.setLocationList(actionForm.getLocationList());
                             arILIList.setTaxList(actionForm.getTaxList());
                             arILIList.setUnitList(Constants.GLOBAL_BLANK);
                             arILIList.setUnitList("Select Item First");

                             actionForm.saveArILIList(arILIList);

                         }
                     }

                 }else if(actionForm.getType().equalsIgnoreCase("MEMO LINES") && actionForm.getType() != null){


                     actionForm.clearArINVList();
                     for (int x = 1; x <= invoiceLineNumber; x++) {

                        ArInvoiceEntryList arINVList = new ArInvoiceEntryList(actionForm,
                                null, new Integer(x).toString(), null, null, null, null, null, false);

                        arINVList.setMemoLineList(actionForm.getMemoLineList());

                        actionForm.saveArINVList(arINVList);

                    }





                 }

             } catch (GlobalNoRecordFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.customerNoRecordFound"));
                 saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage");

                 }

             }

             if(actionForm.getType().equals("SO MATCHED")) {

                 actionForm.setSoNumber(null);
                 actionForm.clearArSOLList();
                 actionForm.setCurrency(null);

             }
             
             if(actionForm.getType().equals("JO MATCHED")) {

                 actionForm.setJoNumber(null);
                 actionForm.clearArJOLList();
                 actionForm.setCurrency(null);

             }

             

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
    -- Ar INV JO Number Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isJobOrderEntered"))) {

             if (actionForm.getType().equals("JO MATCHED")) {

            	 System.out.println("isJobOrderEntered entered");

                 try {
                     ArModJobOrderDetails mdetails = ejbINV.getArJoByJoDocumentNumberAndCstCustomerCodeAndAdBranch(
                             actionForm.getJoNumber(), actionForm.getCustomer(),
                             new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     if(actionForm.getCustomer()==null || actionForm.getCustomer().equals("")){
                    	 actionForm.setCustomer(mdetails.getJoCstCustomerCode());
                    	 actionForm.setCustomerName(mdetails.getJoCstName());
                     }else{

                     }
                     
                     
                     actionForm.setMemo(mdetails.getJoMemo());
                     actionForm.setPaymentTerm(mdetails.getJoPytName());
                     actionForm.setTaxCode(mdetails.getJoTcName());
                     actionForm.setTaxType(mdetails.getJoTcType());
                     actionForm.setTaxRate(mdetails.getJoTcRate());
                     actionForm.setCurrency(mdetails.getJoFcName());
                     actionForm.setBillTo(mdetails.getJoBillTo());
                     actionForm.setShipTo(mdetails.getJoShipTo());
                     actionForm.setSalesperson(mdetails.getJoSlpSalespersonCode() != null ?
                             mdetails.getJoSlpSalespersonCode() : Constants.GLOBAL_BLANK);

                     System.out.println("ref from so is : " + mdetails.getJoReferenceNumber());
                     actionForm.setReferenceNumber(mdetails.getJoReferenceNumber());


                     
                     GlModFunctionalCurrencyRateDetails mdetailsCurr = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
                   	
               		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetailsCurr.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
              		actionForm.setConversionDate(Common.convertSQLDateToString(mdetailsCurr.getFrDate()));
                     
                     

                     actionForm.clearArJOLList();

                     Iterator i = mdetails.getJoJolList().iterator();

                     while (i.hasNext()) {

                         ArModJobOrderLineDetails mJolDetails = (ArModJobOrderLineDetails)i.next();

                         ArInvoiceJobOrderList arJOLList = new ArInvoiceJobOrderList(actionForm,
                                 mJolDetails.getJolCode(), Common.convertShortToString(mJolDetails.getJolLine()),
                                 mJolDetails.getJolLocName(), mJolDetails.getJolIiName(),
                                 mJolDetails.getJolIiDescription(), Common.convertDoubleToStringMoney((mJolDetails.getJolRemaining()), precisionUnit),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolQuantity(), precisionUnit),
                                 mJolDetails.getJolUomName(),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolUnitPrice(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolAmount(), precisionUnit),
                                 mJolDetails.getJolIssue(),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolDiscount1(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolDiscount2(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolDiscount3(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolDiscount4(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolTotalDiscount(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mJolDetails.getJolTotalDiscount(), precisionUnit),
                                 mJolDetails.getJolMisc(),
                                 Common.convertByteToBoolean(mJolDetails.getJolTax())?"Y":"N");


                         arJOLList.setLocationList(actionForm.getLocationList());
                         arJOLList.setTaxList(actionForm.getTaxList());
                         arJOLList.clearUnitList();

                         ArrayList unitList = ejbINV.getInvUomByIiName(mJolDetails.getJolIiName(), user.getCmpCode());

                         Iterator unitListIter = unitList.iterator();

                         while (unitListIter.hasNext()) {

                             InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                             arJOLList.setUnitList(mUomDetails.getUomName());

                         }


                         boolean isTraceMisc = ejbINV.getArTraceMisc(mJolDetails.getJolIiName(), user.getCmpCode());

                         arJOLList.setIsTraceMisc(isTraceMisc);



                         arJOLList.clearTagList();
                         ArrayList tagList = new ArrayList();
                         if(isTraceMisc) {

                        	 System.out.println("taglist size: " + mJolDetails.getJolTagList().size());
                        	 tagList = mJolDetails.getJolTagList();

                        	 if(tagList.size()>0) {


                        		 String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mJolDetails.getJolQuantity()));
                        		 arJOLList.setMisc(misc);
                        	 }else {
                         		if(mJolDetails.getJolMisc()==null) {
                         			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mJolDetails.getJolQuantity()));
                         			arJOLList.setMisc(misc);
                         		}
                         	}




                         }




                         actionForm.saveArJOLList(arJOLList);

                     }



                     ArModCustomerDetails cstDetails = ejbINV.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());

                     actionForm.setWithholdingTax(cstDetails.getCstCcWtcName());

                     actionForm.setBillToContact(cstDetails.getCstBillToContact());
                     actionForm.setBillToAltContact(cstDetails.getCstBillToAltContact());
                     actionForm.setBillToPhone(cstDetails.getCstBillToPhone());
                     actionForm.setBillingHeader(cstDetails.getCstBillingHeader());
                     actionForm.setBillingFooter(cstDetails.getCstBillingFooter());
                     actionForm.setBillingHeader2(cstDetails.getCstBillingHeader2());
                     actionForm.setBillingFooter2(cstDetails.getCstBillingFooter2());
                     actionForm.setBillingHeader3(cstDetails.getCstBillingHeader3());
                     actionForm.setBillingFooter3(cstDetails.getCstBillingFooter3());

                     actionForm.setBillingSignatory(cstDetails.getCstBillingSignatory());
                     actionForm.setSignatoryTitle(cstDetails.getCstSignatoryTitle());

                     actionForm.setShipToContact(cstDetails.getCstShipToContact());
                     actionForm.setShipToAltContact(cstDetails.getCstShipToAltContact());
                     actionForm.setShipToPhone(cstDetails.getCstShipToPhone());
                     actionForm.setCustomerName(cstDetails.getCstName());


    	    		  if (cstDetails.getCstSlpSalespersonCode() != null && cstDetails.getCstSlpSalespersonCode2() != null) {

    	    			  actionForm.clearSalespersonList();
    	    			  actionForm.clearSalespersonNameList();
    	    			  actionForm.setSalespersonList(cstDetails.getCstSlpSalespersonCode());
    	    			  actionForm.setSalespersonList(cstDetails.getCstSlpSalespersonCode2());
    	    			  actionForm.setSalespersonNameList(cstDetails.getCstSlpSalespersonCode() + " - " + cstDetails.getCstSlpName());
    	    			  actionForm.setSalespersonNameList(cstDetails.getCstSlpSalespersonCode2() + " - " + cstDetails.getCstSlpName2());

    	    		  } else {
    	    			  	actionForm.clearSalespersonList();
    	    	          	actionForm.clearSalespersonNameList();

    	    	          	ArrayList list = ejbINV.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

    	    	          	if ( list == null || list.size() == 0) {

    	    	          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
    	    	          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

    	    	          	} else {

    	    	          		Iterator iter =list.iterator();

    	    	          		while (iter.hasNext()) {

    	    	          			ArSalespersonDetails slpDetails = (ArSalespersonDetails)iter.next();

    	    	          			actionForm.setSalespersonList(slpDetails.getSlpSalespersonCode());
    	    	          			actionForm.setSalespersonNameList(slpDetails.getSlpSalespersonCode() + " - " + slpDetails.getSlpName());

    	    	          		}

    	    	          	}
    	    		  }



                 } catch (GlobalNoRecordFoundException ex) {

                     actionForm.clearArJOLList();

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noJobOrderFound"));
                     saveErrors(request, new ActionMessages(errors));

                 } catch (ArINVNoSalesOrderLinesFoundException ex) {

                     actionForm.clearArJOLList();

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noJobOrderLinesFound"));
                     saveErrors(request, new ActionMessages(errors));


                 } catch (EJBException ex) {

                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                         return mapping.findForward("cmnErrorPage");

                     }

                 }

             }

             return(mapping.findForward("arInvoiceEntry"));
             
             
 /*******************************************************
             -- Ar INV SO Number Enter Action --
         *******************************************************/
         } else if(!Common.validateRequired(request.getParameter("isSalesOrderEntered"))) {

             if (actionForm.getType().equals("SO MATCHED")) {

            	 System.out.println("isSalesOrderEntered entered");

                 try {
                     ArModSalesOrderDetails mdetails = ejbINV.getArSoBySoDocumentNumberAndCstCustomerCodeAndAdBranch(
                             actionForm.getSoNumber(), actionForm.getCustomer(),
                             new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                     if(actionForm.getCustomer()==null || actionForm.getCustomer().equals("")){
                    	 actionForm.setCustomer(mdetails.getSoCstCustomerCode());
                    	 actionForm.setCustomerName(mdetails.getSoCstName());
                     }else{

                     }
               
                   
                     actionForm.setMemo(mdetails.getSoMemo());
                     actionForm.setPaymentTerm(mdetails.getSoPytName());
                     actionForm.setTaxCode(mdetails.getSoTcName());
                     actionForm.setTaxType(mdetails.getSoTcType());
                     actionForm.setTaxRate(mdetails.getSoTcRate());
                     actionForm.setCurrency(mdetails.getSoFcName());
                     actionForm.setBillTo(mdetails.getSoBillTo());
                     actionForm.setShipTo(mdetails.getSoShipTo());
                     actionForm.setSalesperson(mdetails.getSoSlpSalespersonCode() != null ?
                             mdetails.getSoSlpSalespersonCode() : Constants.GLOBAL_BLANK);

                     System.out.println("ref from so is : " + mdetails.getSoReferenceNumber());
                     actionForm.setReferenceNumber(mdetails.getSoReferenceNumber());

                     
                     
                     GlModFunctionalCurrencyRateDetails mdetailsCurr = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
              	
              		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetailsCurr.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
             		actionForm.setConversionDate(Common.convertSQLDateToString(mdetailsCurr.getFrDate()));
                  
                  //   actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getSoConversionDate()));
	             //    actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getSoConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                     actionForm.clearArSOLList();

                     Iterator i = mdetails.getSoSolList().iterator();

                     while (i.hasNext()) {

                         ArModSalesOrderLineDetails mSolDetails = (ArModSalesOrderLineDetails)i.next();

                         ArInvoiceSalesOrderList arSOLList = new ArInvoiceSalesOrderList(actionForm,
                                 mSolDetails.getSolCode(), Common.convertShortToString(mSolDetails.getSolLine()),
                                 mSolDetails.getSolLocName(), mSolDetails.getSolIiName(),
                                 mSolDetails.getSolIiDescription(), Common.convertDoubleToStringMoney((mSolDetails.getSolRemaining()), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolQuantity(), precisionUnit),
                                 mSolDetails.getSolUomName(),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolUnitPrice(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolAmount(), precisionUnit),
                                 mSolDetails.getSolIssue(),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount1(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount2(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount3(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount4(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolTotalDiscount(), precisionUnit),
                                 Common.convertDoubleToStringMoney(mSolDetails.getSolTotalDiscount(), precisionUnit),
                                 mSolDetails.getSolMisc(),
                                 Common.convertByteToBoolean(mSolDetails.getSolTax())?"Y":"N");


                         arSOLList.setLocationList(actionForm.getLocationList());
                         arSOLList.setTaxList(actionForm.getTaxList());
                         arSOLList.clearUnitList();

                         ArrayList unitList = ejbINV.getInvUomByIiName(mSolDetails.getSolIiName(), user.getCmpCode());

                         Iterator unitListIter = unitList.iterator();

                         while (unitListIter.hasNext()) {

                             InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                             arSOLList.setUnitList(mUomDetails.getUomName());

                         }


                         boolean isTraceMisc = ejbINV.getArTraceMisc(mSolDetails.getSolIiName(), user.getCmpCode());

                         arSOLList.setIsTraceMisc(isTraceMisc);



                         arSOLList.clearTagList();
                         ArrayList tagList = new ArrayList();
                         if(isTraceMisc) {

                        	 System.out.println("taglist size: " + mSolDetails.getSolTagList().size());
                        	 tagList = mSolDetails.getSolTagList();

                        	 if(tagList.size()>0) {


                        		 String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mSolDetails.getSolQuantity()));
                        		 arSOLList.setMisc(misc);
                        	 }else {
                         		if(mSolDetails.getSolMisc()==null) {
                         			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mSolDetails.getSolQuantity()));
                         			arSOLList.setMisc(misc);
                         		}
                         	}




                         }




                         actionForm.saveArSOLList(arSOLList);

                     }



                     ArModCustomerDetails cstDetails = ejbINV.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());

                     actionForm.setWithholdingTax(cstDetails.getCstCcWtcName());

                     actionForm.setBillToContact(cstDetails.getCstBillToContact());
                     actionForm.setBillToAltContact(cstDetails.getCstBillToAltContact());
                     actionForm.setBillToPhone(cstDetails.getCstBillToPhone());
                     actionForm.setBillingHeader(cstDetails.getCstBillingHeader());
                     actionForm.setBillingFooter(cstDetails.getCstBillingFooter());
                     actionForm.setBillingHeader2(cstDetails.getCstBillingHeader2());
                     actionForm.setBillingFooter2(cstDetails.getCstBillingFooter2());
                     actionForm.setBillingHeader3(cstDetails.getCstBillingHeader3());
                     actionForm.setBillingFooter3(cstDetails.getCstBillingFooter3());

                     actionForm.setBillingSignatory(cstDetails.getCstBillingSignatory());
                     actionForm.setSignatoryTitle(cstDetails.getCstSignatoryTitle());

                     actionForm.setShipToContact(cstDetails.getCstShipToContact());
                     actionForm.setShipToAltContact(cstDetails.getCstShipToAltContact());
                     actionForm.setShipToPhone(cstDetails.getCstShipToPhone());
                     actionForm.setCustomerName(cstDetails.getCstName());


    	    		  if (cstDetails.getCstSlpSalespersonCode() != null && cstDetails.getCstSlpSalespersonCode2() != null) {

    	    			  actionForm.clearSalespersonList();
    	    			  actionForm.clearSalespersonNameList();
    	    			  actionForm.setSalespersonList(cstDetails.getCstSlpSalespersonCode());
    	    			  actionForm.setSalespersonList(cstDetails.getCstSlpSalespersonCode2());
    	    			  actionForm.setSalespersonNameList(cstDetails.getCstSlpSalespersonCode() + " - " + cstDetails.getCstSlpName());
    	    			  actionForm.setSalespersonNameList(cstDetails.getCstSlpSalespersonCode2() + " - " + cstDetails.getCstSlpName2());

    	    		  } else {
    	    			  	actionForm.clearSalespersonList();
    	    	          	actionForm.clearSalespersonNameList();

    	    	          	ArrayList list = ejbINV.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

    	    	          	if ( list == null || list.size() == 0) {

    	    	          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
    	    	          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

    	    	          	} else {

    	    	          		Iterator iter =list.iterator();

    	    	          		while (iter.hasNext()) {

    	    	          			ArSalespersonDetails slpDetails = (ArSalespersonDetails)iter.next();

    	    	          			actionForm.setSalespersonList(slpDetails.getSlpSalespersonCode());
    	    	          			actionForm.setSalespersonNameList(slpDetails.getSlpSalespersonCode() + " - " + slpDetails.getSlpName());

    	    	          		}

    	    	          	}
    	    		  }
    	    		  
    	    		  ArrayList rpList = ejbINV.getArInvoiceReportParameters(user.getCmpCode());
    	    		  
    	    		  actionForm.setReportParameters(Common.convertStringToReportParameters(mdetails.getReportParameter(), rpList));


                 }catch (GlobalConversionDateNotExistException ex) {
                	 actionForm.clearArSOLList();
                	  
                	  
                	  
    	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
    	           				new ActionMessage("invoiceEntry.error.conversionDateNotExist"));
    	           		saveErrors(request, new ActionMessages(errors));
    	           	

                 } catch (GlobalNoRecordFoundException ex) {

                     actionForm.clearArSOLList();

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noSalesOrderFound"));
                     saveErrors(request, new ActionMessages(errors));

                 } catch (ArINVNoSalesOrderLinesFoundException ex) {

                     actionForm.clearArSOLList();

                     errors.add(ActionMessages.GLOBAL_MESSAGE,
                             new ActionMessage("invoiceEntry.error.noSalesOrderLinesFound"));
                     saveErrors(request, new ActionMessages(errors));


                 } catch (EJBException ex) {

                     if (log.isInfoEnabled()) {

                         log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                 " session: " + session.getId());
                         return mapping.findForward("cmnErrorPage");

                     }

                 }

             }

             return(mapping.findForward("arInvoiceEntry"));
/*******************************************************
   -- Ar INV Memo Line Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("arINVList[" +
                actionForm.getRowSelected() + "].isMemoLineEntered")) && actionForm.getType().equals("MEMO LINES")) {

             ArInvoiceEntryList arINVList =
                 actionForm.getArINVByIndex(actionForm.getRowSelected());

             try {
                 ArStandardMemoLineDetails smlDetails = null;

                System.out.println(" memoline entered action : " + actionForm.getCustomerClass());
                actionForm.getCustomerClass();
                smlDetails = ejbINV.getArSmlByCstCstmrCodeSmlNm(actionForm.getCustomer(), arINVList.getMemoLine(), user.getCurrentBranch().getBrCode(), user.getCmpCode());


                 arINVList.setDescription(smlDetails.getSmlDescription());
                 arINVList.setQuantity("1");
                 arINVList.setUnitPrice(Common.convertDoubleToStringMoney(smlDetails.getSmlUnitPrice(), precisionUnit));
                 arINVList.setAmount(arINVList.getUnitPrice());
                 arINVList.setTaxable(Common.convertByteToBoolean(smlDetails.getSmlTax()));

             } catch (GlobalNoRecordFoundException ex) {

             } catch (EJBException ex) {

                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage");

                 }

             }

             return(mapping.findForward("arInvoiceEntry"));


/*******************************************************
-- Ar View Attachment Action --
*******************************************************/

         } else if(request.getParameter("viewAttachmentButton1") != null) {

	    	  //	File file = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1" + attachmentFileExtension );
	    	  //	File filePDF = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1" + attachmentFileExtensionPDF );
	    	 // 	File fileDownload = new File(attachmentPath + actionForm.getPurchaseRequisitionCode() + "-1" + attachmentFileExtensionData);
	    	  	String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getInvoiceCode() + "-1.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}
					System.out.println("attachment extension is : " + attachmentFileExtension);
		    	  	if (fileExtension.equals(".jpg")) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(".pdf") ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}




		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("arInvoiceEntry"));

		         } else {

		           return (mapping.findForward("arInvoiceEntryChild"));

		        }



		      } else if(request.getParameter("viewAttachmentButton2") != null) {

		    	  String filename = "";

		    	  	File fileRef = new File(attachmentPath + actionForm.getInvoiceCode() + "-2.txt");


		    	  	if(fileRef.exists()) {
		    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

		    	  	  String st;
		    	  	  while ((st = br.readLine()) != null) {
		    	  		filename = st;
		    	  	  }
		    	  	}


		    	  	System.out.println("filepath:" + attachmentPath  + filename);
		    	  	File file = new File( attachmentPath  + filename );

		    	  	if(file.exists()) {

		    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  		System.out.println(fileExtension + " <== file extension?");
			    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

			    	  	byte data[] = new byte[fis.available()];

			    	  	int ctr = 0;
			    	  	int c = 0;

			    	  	while ((c = fis.read()) != -1) {

				      		data[ctr] = (byte)c;
				      		ctr++;

			    	  	}

			    	  	if (fileExtension.equals(attachmentFileExtension)) {
					      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
					      	System.out.println("jpg");

					      	session.setAttribute(Constants.IMAGE_KEY, image);
					      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
			    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
			    	  		System.out.println("pdf");

			    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
			    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
			    	  	} else {
			    	  		Object obj = data;

			    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
			    	  		session.setAttribute("DownloadFileName",filename);
			    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
			    	  	}






		    	  	}



		        if (request.getParameter("child") == null) {

		           return (mapping.findForward("arInvoiceEntry"));

		         } else {

		           return (mapping.findForward("arInvoiceEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton3") != null) {

		    	  String filename = "";

		    	  	File fileRef = new File(attachmentPath + actionForm.getInvoiceCode() + "-3.txt");


		    	  	if(fileRef.exists()) {
		    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

		    	  	  String st;
		    	  	  while ((st = br.readLine()) != null) {
		    	  		filename = st;
		    	  	  }
		    	  	}


		    	  	System.out.println("filepath:" + attachmentPath  + filename);
		    	  	File file = new File( attachmentPath  + filename );

		    	  	if(file.exists()) {

		    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  		System.out.println(fileExtension + " <== file extension?");
			    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

			    	  	byte data[] = new byte[fis.available()];

			    	  	int ctr = 0;
			    	  	int c = 0;

			    	  	while ((c = fis.read()) != -1) {

				      		data[ctr] = (byte)c;
				      		ctr++;

			    	  	}

			    	  	if (fileExtension.equals(attachmentFileExtension)) {
					      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
					      	System.out.println("jpg");

					      	session.setAttribute(Constants.IMAGE_KEY, image);
					      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
			    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
			    	  		System.out.println("pdf");

			    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
			    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
			    	  	} else {
			    	  		Object obj = data;

			    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
			    	  		session.setAttribute("DownloadFileName",filename);
			    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
			    	  	}





		    	  	}

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("arInvoiceEntry"));

		         } else {

		           return (mapping.findForward("arInvoiceEntryChild"));

		        }


		      } else if(request.getParameter("viewAttachmentButton4") != null) {

		    	  String filename = "";

		    	  	File fileRef = new File(attachmentPath + actionForm.getInvoiceCode() + "-4.txt");


		    	  	if(fileRef.exists()) {
		    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

		    	  	  String st;
		    	  	  while ((st = br.readLine()) != null) {
		    	  		filename = st;
		    	  	  }
		    	  	}


		    	  	System.out.println("filepath:" + attachmentPath  + filename);
		    	  	File file = new File( attachmentPath  + filename );

		    	  	if(file.exists()) {

		    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

		    	  		System.out.println(fileExtension + " <== file extension?");
			    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

			    	  	byte data[] = new byte[fis.available()];

			    	  	int ctr = 0;
			    	  	int c = 0;

			    	  	while ((c = fis.read()) != -1) {

				      		data[ctr] = (byte)c;
				      		ctr++;

			    	  	}

			    	  	if (fileExtension.equals(attachmentFileExtension)) {
					      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
					      	System.out.println("jpg");

					      	session.setAttribute(Constants.IMAGE_KEY, image);
					      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
			    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
			    	  		System.out.println("pdf");

			    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
			    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
			    	  	} else {
			    	  		Object obj = data;

			    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
			    	  		session.setAttribute("DownloadFileName",filename);
			    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
			    	  	}






		    	  	}

		         if (request.getParameter("child") == null) {

		           return (mapping.findForward("arInvoiceEntry"));

		         } else {

		           return (mapping.findForward("arInvoiceEntryChild"));

		        }


/*******************************************************
    -- Ar INV ILI Item Enter Action --
 *******************************************************/

         } else if(!Common.validateRequired(request.getParameter("arILIList[" +
                 actionForm.getRowILISelected() + "].isItemEntered"))) {

             ArInvoiceLineItemList arILIList =
                 actionForm.getArILIByIndex(actionForm.getRowILISelected());

             String itemClass = ejbINV.getInvIiClassByIiName(arILIList.getItemName(), user.getCmpCode());

             if(itemClass.equals("Assembly")) {
                 arILIList.setIsAssemblyItem(true);
             } else {
                 arILIList.setIsAssemblyItem(false);
             }

             if(ejbINV.getInvAutoBuildEnabledByIiName(arILIList.getItemName(), user.getCmpCode())) {
                 arILIList.setAutoBuildCheckbox(true);
             } else {
                 arILIList.setAutoBuildCheckbox(false);
             }




             try {

                 // populate unit field class

                 ArrayList uomList = new ArrayList();
                 uomList = ejbINV.getInvUomByIiName(arILIList.getItemName(), user.getCmpCode());

                 arILIList.clearUnitList();
                 arILIList.setUnitList(Constants.GLOBAL_BLANK);

                 Iterator i = uomList.iterator();
                 while (i.hasNext()) {

                     InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

                     arILIList.setUnitList(mUomDetails.getUomName());

                     if (mUomDetails.isDefault()) {

                         arILIList.setUnit(mUomDetails.getUomName());

                     }

                 }

               //TODO: populate taglist with empty values

                 arILIList.clearTagList();


          		boolean isTraceMisc =  ejbINV.getArTraceMisc(arILIList.getItemName(), user.getCmpCode());


          		arILIList.setIsTraceMisc(isTraceMisc);
           		System.out.println(isTraceMisc + "<== trace misc item enter action");
           		if (isTraceMisc){
           			arILIList.clearTagList();

           			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), arILIList.getQuantity());


           			arILIList.setMisc(misc);

           		}



                 // populate unit cost field

                 if (!Common.validateRequired(arILIList.getItemName()) && !Common.validateRequired(arILIList.getUnit())) {

					
					
					if(!Common.validateRequired(actionForm.getDate()) && Common.validateDateFormat(actionForm.getDate())){
							
							
						 double unitPrice = ejbINV.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arILIList.getItemName(), Common.convertStringToSQLDate(actionForm.getDate()) ,arILIList.getUnit(), user.getCmpCode());
                    	 arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
	
				
					}else{
					  	double unitPrice = ejbINV.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arILIList.getItemName(),  arILIList.getUnit(), user.getCmpCode());
                        arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

					
					}

                 }
                 
                 
                 


                 arILIList.setDiscount1("0.00");
                 arILIList.setDiscount2("0.00");
                 arILIList.setDiscount3("0.00");
                 arILIList.setDiscount4("0.00");
                 arILIList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));

                 arILIList.setQuantity(Common.convertDoubleToStringMoney(1d, precisionUnit));
                 arILIList.setAmount(arILIList.getUnitPrice());

                 // enable edit sales price if non inventory
                 boolean disableSalesPriceItem = ejbINV.getInvIiNonInventoriableByIiName(arILIList.getItemName(), user.getCmpCode());
                 arILIList.setDisableSalesPrice(disableSalesPrice == true ? disableSalesPriceItem : false);

                 arILIList.setTax("Y");
             } catch (EJBException ex) {

                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage");

                 }

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
    -- Ar INV ILI Unit Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("arILIList[" +
                 actionForm.getRowILISelected() + "].isUnitEntered"))) {

             ArInvoiceLineItemList arILIList =
                 actionForm.getArILIByIndex(actionForm.getRowILISelected());

             try {

                 // populate unit price field

                 if (!Common.validateRequired(arILIList.getLocation()) && !Common.validateRequired(arILIList.getItemName())) {



					  // populate unit cost field

	                 if (!Common.validateRequired(arILIList.getItemName()) && !Common.validateRequired(arILIList.getUnit())) {
	
						
						
						if(!Common.validateRequired(actionForm.getDate()) && Common.validateDateFormat(actionForm.getDate())){
								
								
							 double unitPrice = ejbINV.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arILIList.getItemName(), Common.convertStringToSQLDate(actionForm.getDate()) ,arILIList.getUnit(), user.getCmpCode());
	                    	 arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
		
								
								
								
						}else{
						  	double unitPrice = ejbINV.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arILIList.getItemName(),  arILIList.getUnit(), user.getCmpCode());
	                        arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
	
						
						}
	
	                 }







                 }

                 // populate amount field
                 double amount = 0d;

                 if(!Common.validateRequired(arILIList.getQuantity()) && !Common.validateRequired(arILIList.getUnitPrice())) {

                     amount = Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) *
                     Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit);

                     arILIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

                 }

                 // populate discount field

                 if (!Common.validateRequired(arILIList.getAmount()) &&
                         (!Common.validateRequired(arILIList.getDiscount1()) ||
                         !Common.validateRequired(arILIList.getDiscount2()) ||
                         !Common.validateRequired(arILIList.getDiscount3()) ||
                         !Common.validateRequired(arILIList.getDiscount4()))) {

                     if (actionForm.getTaxType().equals("INCLUSIVE")) {

                         amount = amount / (1 + (actionForm.getTaxRate() / 100));

                     }

                     double discountRate1 = Common.convertStringMoneyToDouble(arILIList.getDiscount1(), precisionUnit)/100;
                     double discountRate2 = Common.convertStringMoneyToDouble(arILIList.getDiscount2(), precisionUnit)/100;
                     double discountRate3 = Common.convertStringMoneyToDouble(arILIList.getDiscount3(), precisionUnit)/100;
                     double discountRate4 = Common.convertStringMoneyToDouble(arILIList.getDiscount4(), precisionUnit)/100;
                     double totalDiscountAmount = 0d;
                     if (discountRate1 > 0) {
                         double discountAmount = amount * discountRate1;
                         totalDiscountAmount += discountAmount;
                         amount -= discountAmount;
                     }
                     if (discountRate2 > 0) {
                         double discountAmount = amount * discountRate2;
                         totalDiscountAmount += discountAmount;
                         amount -= discountAmount;
                     }
                     if (discountRate3 > 0) {
                         double discountAmount = amount * discountRate3;
                         totalDiscountAmount += discountAmount;
                         amount -= discountAmount;
                     }
                     if (discountRate4 > 0) {
                         double discountAmount = amount * discountRate4;
                         totalDiscountAmount += discountAmount;
                         amount -= discountAmount;
                     }
                     if (actionForm.getTaxType().equals("INCLUSIVE")) {

                         amount += amount * (actionForm.getTaxRate() / 100);

                     }

                     arILIList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
                     arILIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

                 }

             } catch (EJBException ex) {

                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                     return mapping.findForward("cmnErrorPage");

                 }

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
   -- Ar INV ILI Type Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {

             if (actionForm.getType().equals("ITEMS")) {

                 actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
                 actionForm.clearArINVList();
                 actionForm.clearArILIList();
                 actionForm.clearArSOLList();
                 actionForm.clearArJOLList();

                 actionForm.setReportType("");
                 // Populate line when not forwarding

                 for (int x = 1; x <= invoiceLineNumber; x++) {

                     ArInvoiceLineItemList arILIList = new ArInvoiceLineItemList(actionForm,
                             null, new Integer(x).toString(), null, null, null, null, null,
                             null, null, false, false, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null, "Y");

                     arILIList.setLocationList(actionForm.getLocationList());
                     arILIList.setTaxList(actionForm.getTaxList());
                     arILIList.setUnitList(Constants.GLOBAL_BLANK);
                     arILIList.setUnitList("Select Item First");

                     actionForm.saveArILIList(arILIList);

                 }

             } else if (actionForm.getType().equals("MEMO LINES")){

                 actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
                 actionForm.clearArINVList();
                 actionForm.clearArILIList();
                 actionForm.clearArSOLList();
                 actionForm.clearArJOLList();
                 actionForm.setReportType("");
                 for (int x = 1; x <= invoiceLineNumber; x++) {

                     ArInvoiceEntryList arINVList = new ArInvoiceEntryList(actionForm,
                             null, new Integer(x).toString(), null, null, null, null, null, false);

                     arINVList.setMemoLineList(actionForm.getMemoLineList());

                     actionForm.saveArINVList(arINVList);

                 }

             } else if (actionForm.getType().equals("SO MATCHED")) {

                 actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
                 actionForm.clearArINVList();
                 actionForm.clearArILIList();
                 actionForm.clearArSOLList();
                 actionForm.clearArJOLList();
                 
                 actionForm.setReportType("");
                 for (int x = 1; x <= invoiceLineNumber; x++) {

                     ArInvoiceSalesOrderList arSOList = new ArInvoiceSalesOrderList(actionForm,
                             null, new Integer(x).toString(), null, null, null,
                             null, null, null, null, null, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null,"Y");

                     arSOList.setLocationList(actionForm.getLocationList());
                     arSOList.setTaxList(actionForm.getTaxList());
                     arSOList.setUnitList(Constants.GLOBAL_BLANK);
                     arSOList.setUnitList("Select Item First");

                     actionForm.saveArSOLList(arSOList);

                 }

             } else {
            	 System.out.println("JO match entered");
                 actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
                 actionForm.clearArINVList();
                 actionForm.clearArILIList();
                 actionForm.clearArSOLList();
                 actionForm.clearArJOLList();
                 
                 actionForm.setReportType("");
                 for (int x = 1; x <= invoiceLineNumber; x++) {

                     ArInvoiceJobOrderList arJOList = new ArInvoiceJobOrderList(actionForm,
                             null, new Integer(x).toString(), null, null, null,
                             null, null, null, null, null, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null,"Y");

                     arJOList.setLocationList(actionForm.getLocationList());
                     arJOList.setTaxList(actionForm.getTaxList());
                     arJOList.setUnitList(Constants.GLOBAL_BLANK);
                     arJOList.setUnitList("Select Item First");

                     actionForm.saveArJOLList(arJOList);

                 }

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
   -- Ar INV Tax Code Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {

             ArTaxCodeDetails details = ejbINV.getArTcByTcName(actionForm.getTaxCode(), user.getCmpCode());
             actionForm.setTaxRate(details.getTcRate());
             actionForm.setTaxType(details.getTcType());

             if (actionForm.getType().equals("ITEMS")) {

                 actionForm.clearArILIList();

                 for (int x = 1; x <= invoiceLineNumber; x++) {

                     ArInvoiceLineItemList arILIList = new ArInvoiceLineItemList(actionForm,
                             null, new Integer(x).toString(), null, null, null, null, null,
                             null, null, false, false, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null, "Y");

                     arILIList.setLocationList(actionForm.getLocationList());
                     arILIList.setTaxList(actionForm.getTaxList());
                     arILIList.setUnitList(Constants.GLOBAL_BLANK);
                     arILIList.setUnitList("Select Item First");

                     actionForm.saveArILIList(arILIList);

                 }

             }

             return(mapping.findForward("arInvoiceEntry"));

/*******************************************************
   -- Ar INV Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

         	try {

         		 actionForm.setConversionRate(Common.convertDoubleToStringMoney(
        				 ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));


         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));

         	}

         	return(mapping.findForward("arInvoiceEntry"));



/*******************************************************
   -- Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

         	try {

         		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
         		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));
 
         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("invoiceEntry.error.conversionDateNotExist"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));

         	}

         	return(mapping.findForward("arInvoiceEntry"));


/*******************************************************
    -- Ar INV recalculate entry and journal --
  *******************************************************/

         }
         
         if (request.getParameter("recalculateEntry") != null) {
        	 
        	 try {
        		 ejbINV.recalculateCostOfSalesEntries();
        		 
        		 System.out.println("DONE RECALCULATE");
        	 }catch(Exception ex) {
        		 if (log.isInfoEnabled()) {

          			log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
          					" session: " + session.getId());

          		}
        	 }
        	
        	 
        	 
        	 
        	 return(mapping.findForward("arInvoiceEntry"));
        	 
        	 
      

/*******************************************************
   -- Ar INV Load Action --
*******************************************************/

         }

         if (frParam != null) {

             if (!errors.isEmpty()) {

                 saveErrors(request, new ActionMessages(errors));
                 return(mapping.findForward("arInvoiceEntry"));

             }
           
             try {

                 ArrayList list = null;
                 Iterator i = null;

                 actionForm.clearCurrencyList();

                 list = ejbINV.getGlFcAllWithDefault(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

                         actionForm.setCurrencyList(mFcDetails.getFcName());

                         if (mFcDetails.getFcSob() == 1) {

                             actionForm.setCurrency(mFcDetails.getFcName());
                             actionForm.setFunctionalCurrency(mFcDetails.getFcName());

                         }

                     }

                 }

                 actionForm.clearPaymentTermList();

                 list = ejbINV.getAdPytAll(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setPaymentTermList((String)i.next());

                     }

                     actionForm.setPaymentTerm("IMMEDIATE");

                 }

                 actionForm.clearTaxCodeList();

                 list = ejbINV.getArTcAll(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setTaxCodeList((String) i.next());

                     }

                 }


                 actionForm.clearUserList();

             	ArrayList userList = ejbINV.getAdUsrAll(user.getCmpCode());

             	if (userList == null || userList.size() == 0) {

             		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

             	} else {

             		Iterator x = userList.iterator();

             		while (x.hasNext()) {

             			actionForm.setUserList((String)x.next());

             		}

             	}
             	 System.out.println(actionForm.getUserList() + "<== user List");

                 actionForm.clearWithholdingTaxList();

                 list = ejbINV.getArWtcAll(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setWithholdingTaxList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setWithholdingTaxList((String)i.next());

                     }

                 }

                 if(actionForm.getUseCustomerPulldown()) {

                     actionForm.clearCustomerList();

                     list = ejbINV.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());




                     if (list == null || list.size() == 0) {

                         actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

                     } else {

                         i = list.iterator();

                         while (i.hasNext()) {

                             actionForm.setCustomerList((String)i.next());

                         }

                     }

                 }

                 actionForm.clearSalespersonList();
                 actionForm.clearSalespersonNameList();

                 list = ejbINV.getArSlpAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
                     actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         ArSalespersonDetails mdetails = (ArSalespersonDetails)i.next();

                         actionForm.setSalespersonList(mdetails.getSlpSalespersonCode());
                         actionForm.setSalespersonNameList(mdetails.getSlpSalespersonCode() + " - " + mdetails.getSlpName());

                     }

                 }

                 actionForm.clearFreightList();

                 list = ejbINV.getAdLvArFreightAll(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setFreightList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setFreightList((String)i.next());

                     }

                 }

                 actionForm.clearShiftList();

                 list = ejbINV.getAdLvInvShiftAll(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setShiftList((String)i.next());

                     }

                 }

                 actionForm.clearMemoLineList();

                 list = ejbINV.getArSmlAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setMemoLineList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setMemoLineList((String)i.next());

                     }

                 }

                 actionForm.clearBatchNameList();

                 list = ejbINV.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setBatchNameList((String)i.next());

                     }

                 }


                 actionForm.clearDeployedBranchNameList();

                 list = ejbINV.getHrOpenDbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setDeployedBranchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setDeployedBranchNameList((String)i.next());

                     }

                 }



                 //PMA
                 actionForm.clearProjectPhaseNameList();
                 actionForm.clearContractTermNameList();

                 actionForm.clearProjectCodeList();
                 list = ejbINV.getPmOpenPrjAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setProjectCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setProjectCodeList((String)i.next());

                     }

                 }




                 actionForm.clearLocationList();

                 list = ejbINV.getInvLocAll(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setLocationList((String)i.next());

                     }

                 }



                 //print type item
                 actionForm.clearReportTypeItemList();


                 list = ejbINV.getAdLvReportTypeAll("ITEM",user.getCmpCode());


                 if (list == null || list.size() == 0) {

                     actionForm.setReportTypeItemList(Constants.GLOBAL_BLANK);

                 } else {

                	 actionForm.setReportTypeItemList(Constants.GLOBAL_BLANK);
                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setReportTypeItemList((String)i.next());

                     }

                 }

                 //get document type list
              	
               	actionForm.clearDocumentTypeList();
               	
               	
               	list = ejbINV.getDocumentTypeList("AR INVOICE DOCUMENT TYPE", user.getCmpCode());
               	
               	
               	
               	i = list.iterator();
               	while(i.hasNext()) {
               		actionForm.setDocumentTypeList((String)i.next());
               	}
               	

                 //print type memo line
                 actionForm.clearReportTypeMemoLineList();


                 list = ejbINV.getAdLvReportTypeAll("MEMO LINE",user.getCmpCode());


                 if (list == null || list.size() == 0) {

                     actionForm.setReportTypeMemoLineList(Constants.GLOBAL_BLANK);

                 } else {

                	 actionForm.setReportTypeMemoLineList(Constants.GLOBAL_BLANK);
                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setReportTypeMemoLineList((String)i.next());

                     }

                 }


                //print type so Matched
                 actionForm.clearReportTypeSoeList();


                 list = ejbINV.getAdLvReportTypeAll("SO MATCHED",user.getCmpCode());


                 if (list == null || list.size() == 0) {

                     actionForm.setReportTypeSoList(Constants.GLOBAL_BLANK);

                 } else {

                	 actionForm.setReportTypeSoList(Constants.GLOBAL_BLANK);
                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setReportTypeSoList((String)i.next());

                     }

                 }


                 actionForm.clearTaxList();
                 actionForm.setTaxList("Y");
                 actionForm.setTaxList("N");

               	//REPORT PARAMETERS
             	actionForm.clearReportParameters();

               	list = ejbINV.getArInvoiceReportParameters(user.getCmpCode());

               	System.out.println("pass through: " +list.size());
               	for(int x=0;x<list.size();x++){

               	  	ReportParameter reportParameter = new ReportParameter();
               		String parameterName =list.get(x).toString();
               		reportParameter.setParameterName(parameterName);
               	  	actionForm.addReportParameters(reportParameter);
               	}



                 if (request.getParameter("forwardBatch") != null) {

                     actionForm.setBatchName(request.getParameter("batchName"));
                     actionForm.setType(null);

                 }

                 actionForm.clearArINVList();
                 actionForm.clearArILIList();
                 actionForm.clearArSOLList();
                 actionForm.clearArJOLList();

                 //actionForm.setInvoiceCode(null);
                 if (request.getParameter("forward") != null || isInitialPrinting) {

                     if (request.getParameter("forward") != null) {

                         actionForm.setInvoiceCode(new Integer(request.getParameter("invoiceCode")));

                     }

                     ArModInvoiceDetails mdetails = ejbINV.getArInvByInvCode(actionForm.getInvoiceCode(), user.getCmpCode());
                     System.out.println("inv code  = " + actionForm.getInvoiceCode());
                     System.out.println("inv code 2 = " + mdetails.getInvCode());
                     actionForm.setInvoiceCode(mdetails.getInvCode());
                     actionForm.setMemo(mdetails.getInvMemo());
                     actionForm.setDescription(mdetails.getInvDescription());
                     actionForm.setDate(Common.convertSQLDateToString(mdetails.getInvDate()));
                     actionForm.setDocumentType(mdetails.getInvDocumentType());
                     actionForm.setInvoiceNumber(mdetails.getInvNumber());
                     actionForm.setReferenceNumber(mdetails.getInvReferenceNumber());
                     actionForm.setUploadNumber(mdetails.getInvUploadNumber());
                     actionForm.setBillTo(mdetails.getInvBillToAddress());
                     actionForm.setBillToContact(mdetails.getInvBillToContact());
                     actionForm.setBillToAltContact(mdetails.getInvBillToAltContact());
                     actionForm.setBillToPhone(mdetails.getInvBillToPhone());
                     actionForm.setBillingHeader(mdetails.getInvBillingHeader());
                     actionForm.setBillingFooter(mdetails.getInvBillingFooter());
                     actionForm.setBillingHeader2(mdetails.getInvBillingHeader2());
                     actionForm.setBillingFooter2(mdetails.getInvBillingFooter2());
                     actionForm.setBillingHeader3(mdetails.getInvBillingHeader3());
                     actionForm.setBillingFooter3(mdetails.getInvBillingFooter3());
                     actionForm.setBillingSignatory(mdetails.getInvBillingSignatory());
                     actionForm.setSignatoryTitle(mdetails.getInvSignatoryTitle());
                     actionForm.setShipTo(mdetails.getInvShipToAddress());
                     actionForm.setShipToContact(mdetails.getInvShipToContact());
                     actionForm.setShipToAltContact(mdetails.getInvShipToAltContact());
                     actionForm.setShipToPhone(mdetails.getInvShipToPhone());
                     actionForm.setFreight(mdetails.getInvLvFreight());
                     actionForm.setShipDate(Common.convertSQLDateToString(mdetails.getInvShipDate()));
                     actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getInvConversionDate()));
                     actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getInvConversionRate(), precisionUnit));

                     actionForm.setPreviousReading(Common.convertDoubleToStringMoney(mdetails.getInvPreviousReading(), precisionUnit));
                     actionForm.setPresentReading(Common.convertDoubleToStringMoney(mdetails.getInvPresentReading(), precisionUnit));

                     actionForm.setAmountDue(Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit));
                     actionForm.setDownPayment(Common.convertDoubleToStringMoney(mdetails.getInvDownPayment(), precisionUnit));


                     actionForm.setAmountPaid(Common.convertDoubleToStringMoney(mdetails.getInvAmountPaid(), precisionUnit));

                     actionForm.setPenaltyDue(Common.convertDoubleToStringMoney(mdetails.getInvPenaltyDue(), precisionUnit));
                     actionForm.setPenaltyPaid(Common.convertDoubleToStringMoney(mdetails.getInvPenaltyPaid(), precisionUnit));

                     actionForm.setAmountUnearnedInterest(Common.convertDoubleToStringMoney(mdetails.getInvAmountUnearnedInterest(), precisionUnit));

                     actionForm.setInvoiceVoid(Common.convertByteToBoolean(mdetails.getInvVoid()));

                     actionForm.setDisableInterest(Common.convertByteToBoolean(mdetails.getInvDisableInterest()));
                     actionForm.setInvoiceInterest(Common.convertByteToBoolean(mdetails.getInvInterest()));
                     actionForm.setInterestReferenceNumber(mdetails.getInvInterestReferenceNumber());
                     System.out.println("mdetails.getInvInterestReferenceNumber()="+mdetails.getInvInterestReferenceNumber());
                     actionForm.setInterestAmount(Common.convertDoubleToStringMoney(mdetails.getInvInterestAmount(), precisionUnit));
                     actionForm.setInterestCreatedBy(mdetails.getInvInterestCreatedBy());
                     actionForm.setInterestDateCreated(Common.convertSQLDateToString(mdetails.getInvInterestDateCreated()));
                     actionForm.setInterestNextRunDate(Common.convertSQLDateToString(mdetails.getInvInterestNextRunDate()));
                     actionForm.setInterestLastRunDate(Common.convertSQLDateToString(mdetails.getInvInterestLastRunDate()));


                     actionForm.setApprovalStatus(mdetails.getInvApprovalStatus());
                     actionForm.setReasonForRejection(mdetails.getInvReasonForRejection());
                     actionForm.setPosted(mdetails.getInvPosted() == 1 ? "YES" : "NO");
                     actionForm.setCreatedBy(mdetails.getInvCreatedBy());
                     actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getInvDateCreated()));
                     actionForm.setLastModifiedBy(mdetails.getInvLastModifiedBy());
                     actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getInvDateLastModified()));
                     actionForm.setApprovedRejectedBy(mdetails.getInvApprovedRejectedBy());
                     actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getInvDateApprovedRejected()));
                     actionForm.setPostedBy(mdetails.getInvPostedBy());
                     actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getInvDatePosted()));
                     actionForm.setDebitMemo(Common.convertByteToBoolean(mdetails.getInvDebitMemo()));
                     actionForm.setSubjectToCommission(Common.convertByteToBoolean(mdetails.getInvSubjectToCommission()));
                     actionForm.setClientPO(mdetails.getInvClientPO());
                     actionForm.setEffectivityDate(Common.convertSQLDateToString(mdetails.getInvEffectivityDate()));
                     actionForm.setDueDate(Common.convertSQLDateToString(mdetails.getInvDueDate()));
                     actionForm.setRecieveDate(Common.convertSQLDateToString(mdetails.getInvRecieveDate()));

                     //HRIS
                     actionForm.setEmployeeNumber(mdetails.getInvHrEmployeeNumber());
                     actionForm.setBioNumber(mdetails.getInvHrBioNumber());
                     actionForm.setManagingBranch(mdetails.getInvHrManagingBranch());
                     actionForm.setCurrentJobPosition(mdetails.getInvHrCurrentJobPosition());

                     ArrayList rpList = ejbINV.getArInvoiceReportParameters(user.getCmpCode());


                     actionForm.setReportParameters(Common.convertStringToReportParameters(mdetails.getReportParameter(), rpList));


                     if (!actionForm.getDeployedBranchNameList().contains(mdetails.getInvHrDeployedBranchName())) {

                         actionForm.setDeployedBranchNameList(mdetails.getInvHrDeployedBranchName());

                     }
                     actionForm.setDeployedBranchName(mdetails.getInvHrDeployedBranchName());



                     //PMA
                     if (!actionForm.getProjectCodeList().contains(mdetails.getInvPmProjectCode())) {

                         actionForm.setProjectCodeList(mdetails.getInvPmProjectCode());

                     }
                     actionForm.setProjectCode(mdetails.getInvPmProjectCode());


                     if (!actionForm.getProjectTypeCodeList().contains(mdetails.getInvPmProjectTypeCode())) {

                         actionForm.setProjectTypeCodeList(mdetails.getInvPmProjectTypeCode());

                     }
                     actionForm.setProjectTypeCode(mdetails.getInvPmProjectTypeCode());

                     if (!actionForm.getProjectTypeCodeList().contains(mdetails.getInvPmProjectTypeCode())) {

                         actionForm.setProjectTypeCodeList(mdetails.getInvPmProjectTypeCode());

                     }
                     actionForm.setProjectTypeCode(mdetails.getInvPmProjectTypeCode());


                     if (!actionForm.getProjectPhaseNameList().contains(mdetails.getInvPmProjectPhaseName())) {

                         actionForm.setProjectPhaseNameList(mdetails.getInvPmProjectPhaseName());

                     }
                     actionForm.setProjectPhaseName(mdetails.getInvPmProjectPhaseName());

                     if (!actionForm.getProjectPhaseNameList().contains(mdetails.getInvPmProjectPhaseName())) {

                         actionForm.setProjectPhaseNameList(mdetails.getInvPmProjectPhaseName());

                     }
                     actionForm.setProjectPhaseName(mdetails.getInvPmProjectPhaseName());


                     if (!actionForm.getContractTermNameList().contains(mdetails.getInvPmContractTermName())) {

                         actionForm.setContractTermNameList(mdetails.getInvPmContractTermName());

                     }
                     actionForm.setContractTermName(mdetails.getInvPmContractTermName());



                     if (!actionForm.getCurrencyList().contains(mdetails.getInvFcName())) {

                         if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

                             actionForm.clearCurrencyList();

                         }

                         actionForm.setCurrencyList(mdetails.getInvFcName());

                     }
                     actionForm.setCurrency(mdetails.getInvFcName());

                     if (!actionForm.getTaxCodeList().contains(mdetails.getInvTcName())) {

                         if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

                             actionForm.clearTaxCodeList();

                         }
                         actionForm.setTaxCodeList(mdetails.getInvTcName());

                     }
                     actionForm.setTaxCode(mdetails.getInvTcName());
                     actionForm.setTaxRate(mdetails.getInvTcRate());
                     actionForm.setTaxType(mdetails.getInvTcType());

                     if (!actionForm.getWithholdingTaxList().contains(mdetails.getInvWtcName())) {

                         if (actionForm.getWithholdingTaxList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

                             actionForm.clearWithholdingTaxList();

                         }
                         actionForm.setWithholdingTaxList(mdetails.getInvWtcName());

                     }
                     actionForm.setWithholdingTax(mdetails.getInvWtcName());

                     if (!actionForm.getCustomerList().contains(mdetails.getInvCstCustomerCode())) {

                         if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

                             actionForm.clearCustomerList();

                         }
                         actionForm.setCustomerList(mdetails.getInvCstCustomerCode());

                     }
                     actionForm.setCustomer(mdetails.getInvCstCustomerCode());
                     actionForm.setCustomerName(mdetails.getInvCstName());


                     if (!actionForm.getPaymentTermList().contains(mdetails.getInvPytName())) {

                         if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

                             actionForm.clearPaymentTermList();

                         }
                         actionForm.setPaymentTermList(mdetails.getInvPytName());

                     }
                     actionForm.setPaymentTerm(mdetails.getInvPytName());

                     actionForm.setShift(mdetails.getInvLvShift());

                     if (!actionForm.getBatchNameList().contains(mdetails.getInvIbName())) {

                         actionForm.setBatchNameList(mdetails.getInvIbName());

                     }
                     actionForm.setBatchName(mdetails.getInvIbName());

                     if (!actionForm.getSalespersonList().contains(mdetails.getInvSlpSalespersonCode())) {

                         if (actionForm.getSalespersonList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

                             actionForm.clearSalespersonList();
                             actionForm.clearSalespersonNameList();

                         }

                         actionForm.setSalespersonList(mdetails.getInvSlpSalespersonCode());
                         actionForm.setSalespersonNameList(mdetails.getInvSlpSalespersonCode() + " - " + mdetails.getInvSlpName());

                     }

                     actionForm.setSalesperson(mdetails.getInvSlpSalespersonCode());

                     if (mdetails.getInvIliList() != null && !mdetails.getInvIliList().isEmpty()) {

                         actionForm.setType("ITEMS");

                         list = mdetails.getInvIliList();

                         i = list.iterator();

                         double totalAmount = 0;

                         while (i.hasNext()) {

                             ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails)i.next();

                             boolean isAssemblyItem = ejbINV.getInvIiClassByIiName(mIliDetails.getIliIiName(), user.getCmpCode()).equals("Assembly");
                             boolean disableSalesPriceItem = ejbINV.getInvIiNonInventoriableByIiName(mIliDetails.getIliIiName(), user.getCmpCode());

                             String fixedDiscountAmount = "0.00";
                             String origDiscountAmount = Common.convertDoubleToStringMoney(mIliDetails.getIliTotalDiscount(), precisionUnit);;

                             if((mIliDetails.getIliDiscount1() + mIliDetails.getIliDiscount2() +
                            		 mIliDetails.getIliDiscount3() + mIliDetails.getIliDiscount4()) == 0)
                            	 fixedDiscountAmount = Common.convertDoubleToStringMoney(mIliDetails.getIliTotalDiscount(), precisionUnit);

                             totalAmount += mIliDetails.getIliAmount();
                             ArInvoiceLineItemList arIliList = new ArInvoiceLineItemList(actionForm,
                                     mIliDetails.getIliCode(),
                                     Common.convertShortToString(mIliDetails.getIliLine()),
                                     mIliDetails.getIliLocName(),
                                     mIliDetails.getIliIiName(),
                                     mIliDetails.getIliIiDescription(),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliQuantity(), precisionUnit),
                                     mIliDetails.getIliUomName(),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliUnitPrice(), precisionUnit),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliAmount(), precisionUnit),
                                     isAssemblyItem,
                                     disableSalesPriceItem,
                                     Common.convertByteToBoolean(mIliDetails.getIliEnableAutoBuild()),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount1(), precisionUnit),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount2(), precisionUnit),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount3(), precisionUnit),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount4(), precisionUnit),
                                     Common.convertDoubleToStringMoney(mIliDetails.getIliTotalDiscount(), precisionUnit),
                                     fixedDiscountAmount,
                                     mIliDetails.getIliMisc(),
                                     Common.convertByteToBoolean(mIliDetails.getIliTax())?"Y":"N");

                             arIliList.setLocationList(actionForm.getLocationList());
                             boolean isTraceMisc = ejbINV.getArTraceMisc(mIliDetails.getIliIiName(), user.getCmpCode());
                             arIliList.setIsTraceMisc(isTraceMisc);

                             arIliList.clearTagList();
                             ArrayList tagList = new ArrayList();
                             if(isTraceMisc) {


                        		 tagList = mIliDetails.getiIliTagList();



                            	if(tagList.size()>0) {

                            		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mIliDetails.getIliQuantity()));
                           		    arIliList.setMisc(misc);

                            	}else {
                            		if(mIliDetails.getIliMisc()==null) {
                            			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mIliDetails.getIliQuantity()));
                                  		arIliList.setMisc(misc);
                            		}
                            	}



                             }



                             System.out.println("size taxlist: " + actionForm.getTaxList().size());
                             arIliList.setTaxList(actionForm.getTaxList());
                             arIliList.clearUnitList();
                             ArrayList unitList = ejbINV.getInvUomByIiName(mIliDetails.getIliIiName(), user.getCmpCode());
                             Iterator unitListIter = unitList.iterator();
                             while (unitListIter.hasNext()) {

                                 InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                                 arIliList.setUnitList(mUomDetails.getUomName());

                             }

                             actionForm.saveArILIList(arIliList);


                         }
                         actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

                         int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();

                         for (int x = list.size() + 1; x <= remainingList; x++) {

                             ArInvoiceLineItemList arILIList = new ArInvoiceLineItemList(actionForm,
                                     null, String.valueOf(x), null, null, null, null, null, null, null, false, false, false,
                                     null, null, null, null, null, null, null, "Y");

                             arILIList.setLocationList(actionForm.getLocationList());
                             arILIList.setTaxList(actionForm.getTaxList());
                             arILIList.setUnitList(Constants.GLOBAL_BLANK);
                             arILIList.setUnitList("Select Item First");

                             actionForm.saveArILIList(arILIList);

                         }

                         this.setFormProperties(actionForm, user.getCompany());

                         actionForm.setShowDeleteButton(false);
                         for(int x=0;x<user.getResCount();x++) {
             		    	Responsibility ds = (Responsibility)user.getRes(x);

             		    	System.out.println("resp: " + ds.getRSName());
             		    	if(ds.getRSName().equals("Super")) {
             		    		actionForm.setShowDeleteButton(true);
             		    		break;
             		    	}

             		    }
                         
                         if (request.getParameter("child") == null) {

                             return (mapping.findForward("arInvoiceEntry"));

                         } else {

                             return (mapping.findForward("arInvoiceEntryChild"));

                         }

                     } else if (mdetails.getInvIlList() != null && !mdetails.getInvIlList().isEmpty()) {

                         actionForm.setType("MEMO LINES");

                         list = mdetails.getInvIlList();

                         i = list.iterator();

                         int lineNumber = 0;

                         double totalAmount = 0;


                         while (i.hasNext()) {

                             ArModInvoiceLineDetails mIlDetails = (ArModInvoiceLineDetails)i.next();

                             totalAmount += mIlDetails.getIlAmount();

                             ArInvoiceEntryList arIlList = new ArInvoiceEntryList(actionForm,
                                     mIlDetails.getIlCode(),
                                     String.valueOf(++lineNumber),
                                     mIlDetails.getIlSmlName(),
                                     mIlDetails.getIlDescription(),
                                     Common.convertDoubleToStringMoney(mIlDetails.getIlQuantity(), precisionUnit),
                                     Common.convertDoubleToStringMoney(mIlDetails.getIlUnitPrice(), precisionUnit),
                                     Common.convertDoubleToStringMoney(mIlDetails.getIlAmount(), precisionUnit),
                                     Common.convertByteToBoolean(mIlDetails.getIlTax()));

                             //System.out.println(mIlDetails.getIlAmount());
                             arIlList.setMemoLineList(actionForm.getMemoLineList());

                             actionForm.saveArINVList(arIlList);


                         }

                         actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

                         int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();
                         lineNumber = 0;

                         for (int x = list.size() + 1; x <= remainingList; x++) {

                             ArInvoiceEntryList arINVList = new ArInvoiceEntryList(actionForm,
                                     null, String.valueOf(++lineNumber), null, null, null, null, null, false);

                             arINVList.setMemoLineList(actionForm.getMemoLineList());

                             actionForm.saveArINVList(arINVList);

                         }

                         this.setFormProperties(actionForm, user.getCompany());

                         
                         actionForm.setShowDeleteButton(false);
                         for(int x=0;x<user.getResCount();x++) {
             		    	Responsibility ds = (Responsibility)user.getRes(x);

             		    	System.out.println("resp: " + ds.getRSName());
             		    	if(ds.getRSName().equals("Super")) {
             		    		actionForm.setShowDeleteButton(true);
             		    		break;
             		    	}

             		    }
                         
                         if (request.getParameter("child") == null) {

                             return (mapping.findForward("arInvoiceEntry"));

                         } else {

                             return (mapping.findForward("arInvoiceEntryChild"));

                         }

                     } else if(mdetails.getInvSoList() != null && !mdetails.getInvSoList().isEmpty()) {


                         actionForm.setType("SO MATCHED");
                         actionForm.setSoNumber(mdetails.getInvSoNumber());

                         ArrayList issuedSolList = new ArrayList();
                         short lineNumber = 0;
                         double totalAmount = 0;

                         i = mdetails.getInvSoList().iterator();

                         while(i.hasNext()) {

                             ArModSalesOrderLineDetails solDetails = (ArModSalesOrderLineDetails) i.next();
                             totalAmount += solDetails.getSolAmount();
                             ArInvoiceSalesOrderList arSOList = new ArInvoiceSalesOrderList(
                                     actionForm, solDetails.getSolCode(),
                                     Common.convertShortToString(lineNumber++),
                                     solDetails.getSolLocName(),
                                     solDetails.getSolIiName(),
                                     solDetails.getSolIiDescription(),
                                     Common.convertDoubleToStringMoney(solDetails.getSolRemaining() + solDetails.getSolQuantity(),precisionUnit),
                                     Common.convertDoubleToStringMoney(solDetails.getSolQuantity(), precisionUnit),
                                     solDetails.getSolUomName(),
                                     Common.convertDoubleToStringMoney(solDetails.getSolUnitPrice(),precisionUnit),
                                     Common.convertDoubleToStringMoney(solDetails.getSolAmount(),precisionUnit),
                                     solDetails.getSolIssue(),
                                     Common.convertDoubleToStringMoney(solDetails.getSolDiscount1(), precisionUnit),
                                     Common.convertDoubleToStringMoney(solDetails.getSolDiscount2(), precisionUnit),
                                     Common.convertDoubleToStringMoney(solDetails.getSolDiscount3(), precisionUnit),
                                     Common.convertDoubleToStringMoney(solDetails.getSolDiscount4(), precisionUnit),
                                     Common.convertDoubleToStringMoney(solDetails.getSolTotalDiscount(), precisionUnit),
                                     Common.convertDoubleToStringMoney(solDetails.getSolTotalDiscount(), precisionUnit),
                                     solDetails.getSolMisc(),
                                     Common.convertByteToBoolean(solDetails.getSolTax())?"Y":"N");

                             arSOList.clearLocationList();
                             arSOList.setLocationList(actionForm.getLocationList());
                             arSOList.clearTaxList();

                             boolean isTraceMisc = ejbINV.getArTraceMisc(solDetails.getSolIiName(), user.getCmpCode());

                             arSOList.setIsTraceMisc(isTraceMisc);



                             arSOList.clearTagList();
                             ArrayList tagList = new ArrayList();
                             if(isTraceMisc) {


                            	 tagList = solDetails.getSolTagList();

                            	 if(tagList.size()>0) {

                             		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(solDetails.getSolQuantity()));
                             		arSOList.setMisc(misc);

                             	}else {
                             		if(solDetails.getSolMisc()==null) {

                             			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(solDetails.getSolQuantity()));
                             			arSOList.setMisc(misc);
                             		}
                             	}




                             }






                             System.out.println("size 1: " + actionForm.getTaxList());
                             arSOList.setTaxList(actionForm.getTaxList());

                             System.out.println("size 2: " + arSOList.getTaxList().size());

                             ArrayList unitList = ejbINV.getInvUomByIiName(solDetails.getSolIiName(), user.getCmpCode());
                             Iterator unitListIter = unitList.iterator();
                             while (unitListIter.hasNext()) {

                                 InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                                 arSOList.setUnitList(mUomDetails.getUomName());

                             }

                             issuedSolList.add(solDetails.getSolCode());
                             actionForm.saveArSOLList(arSOList);

                         }
                         actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

                         this.setFormProperties(actionForm, user.getCompany());

                         if (actionForm.getEnableFields()) {

                             ArrayList solList = ejbINV.getArNotIssuedSolBySoNumberAndCustomer(actionForm.getSoNumber(), actionForm.getCustomer(), issuedSolList, user.getCmpCode());

                             i = solList.iterator();

                             while (i.hasNext()) {

                                 ArModSalesOrderLineDetails solDetails = (ArModSalesOrderLineDetails) i.next();

                                 ArInvoiceSalesOrderList arSOList = new ArInvoiceSalesOrderList(
                                         actionForm, solDetails.getSolCode(),
                                         Common.convertShortToString(lineNumber++),
                                         solDetails.getSolLocName(),
                                         solDetails.getSolIiName(),
                                         solDetails.getSolIiDescription(),
                                         Common.convertDoubleToStringMoney(solDetails.getSolRemaining(),precisionUnit),
                                         Common.convertDoubleToStringMoney(solDetails.getSolQuantity(), precisionUnit),
                                         solDetails.getSolUomName(),
                                         Common.convertDoubleToStringMoney(solDetails.getSolUnitPrice(),precisionUnit),
                                         Common.convertDoubleToStringMoney(solDetails.getSolAmount(),precisionUnit),
                                         solDetails.getSolIssue(),
                                         Common.convertDoubleToStringMoney(solDetails.getSolDiscount1(), precisionUnit),
                                         Common.convertDoubleToStringMoney(solDetails.getSolDiscount2(), precisionUnit),
                                         Common.convertDoubleToStringMoney(solDetails.getSolDiscount3(), precisionUnit),
                                         Common.convertDoubleToStringMoney(solDetails.getSolDiscount4(), precisionUnit),
                                         Common.convertDoubleToStringMoney(solDetails.getSolTotalDiscount(), precisionUnit),
                                         Common.convertDoubleToStringMoney(solDetails.getSolTotalDiscount(), precisionUnit),
                                         solDetails.getSolMisc(),
                                         Common.convertByteToBoolean(solDetails.getSolTax())?"Y":"N");

                                 arSOList.clearLocationList();
                                 arSOList.setLocationList(actionForm.getLocationList());
                                 arSOList.setTaxList(actionForm.getTaxList());

                                 boolean isTraceMisc = ejbINV.getArTraceMisc(solDetails.getSolIiName(), user.getCmpCode());

                                 arSOList.setIsTraceMisc(isTraceMisc);


                                 arSOList.clearUnitList();
                                 ArrayList unitList = ejbINV.getInvUomByIiName(solDetails.getSolIiName(), user.getCmpCode());
                                 Iterator unitListIter = unitList.iterator();
                                 while (unitListIter.hasNext()) {

                                     InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                                     arSOList.setUnitList(mUomDetails.getUomName());

                                 }

                                 actionForm.saveArSOLList(arSOList);

                             }

                         }
                         
                         actionForm.setShowDeleteButton(false);
                         for(int x=0;x<user.getResCount();x++) {
             		    	Responsibility ds = (Responsibility)user.getRes(x);

             		    	System.out.println("resp: " + ds.getRSName());
             		    	if(ds.getRSName().equals("Super")) {
             		    		actionForm.setShowDeleteButton(true);
             		    		break;
             		    	}

             		    }

                         if (request.getParameter("child") == null) {

                             return (mapping.findForward("arInvoiceEntry"));

                         } else {

                             return (mapping.findForward("arInvoiceEntryChild"));

                         }

                     }else if(mdetails.getInvJoList() != null && !mdetails.getInvJoList().isEmpty()) {


                         actionForm.setType("JO MATCHED");
                         actionForm.setJoNumber(mdetails.getInvJoNumber());

                         ArrayList issuedSolList = new ArrayList();
                         short lineNumber = 0;
                         double totalAmount = 0;

                         i = mdetails.getInvJoList().iterator();

                         while(i.hasNext()) {

                             ArModJobOrderLineDetails jolDetails = (ArModJobOrderLineDetails) i.next();
                             totalAmount += jolDetails.getJolAmount();
                             ArInvoiceJobOrderList arJOList = new ArInvoiceJobOrderList(
                                     actionForm, jolDetails.getJolCode(),
                                     Common.convertShortToString(lineNumber++),
                                     jolDetails.getJolLocName(),
                                     jolDetails.getJolIiName(),
                                     jolDetails.getJolIiDescription(),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolRemaining() + jolDetails.getJolQuantity(),precisionUnit),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolQuantity(), precisionUnit),
                                     jolDetails.getJolUomName(),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolUnitPrice(),precisionUnit),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolAmount(),precisionUnit),
                                     jolDetails.getJolIssue(),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolDiscount1(), precisionUnit),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolDiscount2(), precisionUnit),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolDiscount3(), precisionUnit),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolDiscount4(), precisionUnit),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolTotalDiscount(), precisionUnit),
                                     Common.convertDoubleToStringMoney(jolDetails.getJolTotalDiscount(), precisionUnit),
                                     jolDetails.getJolMisc(),
                                     Common.convertByteToBoolean(jolDetails.getJolTax())?"Y":"N");

                             arJOList.clearLocationList();
                             arJOList.setLocationList(actionForm.getLocationList());
                             arJOList.clearTaxList();

                             boolean isTraceMisc = ejbINV.getArTraceMisc(jolDetails.getJolIiName(), user.getCmpCode());

                             arJOList.setIsTraceMisc(isTraceMisc);



                             arJOList.clearTagList();
                             ArrayList tagList = new ArrayList();
                             if(isTraceMisc) {


                            	 tagList = jolDetails.getJolTagList();
                            	 
                            	 
 
                            	 

                            	 if(tagList.size()>0) {

                             		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(jolDetails.getJolQuantity()));
                             		arJOList.setMisc(misc);

                             	}else {
                             		if(jolDetails.getJolMisc()==null) {

                             			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(jolDetails.getJolQuantity()));
                             			arJOList.setMisc(misc);
                             		}
                             	}




                             }






                             System.out.println("size 1: " + actionForm.getTaxList());
                             arJOList.setTaxList(actionForm.getTaxList());

                             System.out.println("size 2: " + arJOList.getTaxList().size());

                             ArrayList unitList = ejbINV.getInvUomByIiName(jolDetails.getJolIiName(), user.getCmpCode());
                             Iterator unitListIter = unitList.iterator();
                             while (unitListIter.hasNext()) {

                                 InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                                 arJOList.setUnitList(mUomDetails.getUomName());

                             }

                             issuedSolList.add(jolDetails.getJolCode());
                             actionForm.saveArJOLList(arJOList);

                         }
                         actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

                         this.setFormProperties(actionForm, user.getCompany());

                         if (actionForm.getEnableFields()) {

                             ArrayList jolList = ejbINV.getArNotIssuedJolByJoNumberAndCustomer(actionForm.getJoNumber(), actionForm.getCustomer(), issuedSolList, user.getCmpCode());

                             i = jolList.iterator();

                             while (i.hasNext()) {

                                 ArModJobOrderLineDetails jolDetails = (ArModJobOrderLineDetails) i.next();

                                 ArInvoiceJobOrderList arJOList = new ArInvoiceJobOrderList(
                                         actionForm, jolDetails.getJolCode(),
                                         Common.convertShortToString(lineNumber++),
                                         jolDetails.getJolLocName(),
                                         jolDetails.getJolIiName(),
                                         jolDetails.getJolIiDescription(),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolRemaining(),precisionUnit),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolQuantity(), precisionUnit),
                                         jolDetails.getJolUomName(),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolUnitPrice(),precisionUnit),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolAmount(),precisionUnit),
                                         jolDetails.getJolIssue(),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolDiscount1(), precisionUnit),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolDiscount2(), precisionUnit),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolDiscount3(), precisionUnit),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolDiscount4(), precisionUnit),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolTotalDiscount(), precisionUnit),
                                         Common.convertDoubleToStringMoney(jolDetails.getJolTotalDiscount(), precisionUnit),
                                         jolDetails.getJolMisc(),
                                         Common.convertByteToBoolean(jolDetails.getJolTax())?"Y":"N");

                                 arJOList.clearLocationList();
                                 arJOList.setLocationList(actionForm.getLocationList());
                                 arJOList.setTaxList(actionForm.getTaxList());

                                 boolean isTraceMisc = ejbINV.getArTraceMisc(jolDetails.getJolIiName(), user.getCmpCode());

                                 arJOList.setIsTraceMisc(isTraceMisc);


                                 arJOList.clearUnitList();
                                 ArrayList unitList = ejbINV.getInvUomByIiName(jolDetails.getJolIiName(), user.getCmpCode());
                                 Iterator unitListIter = unitList.iterator();
                                 while (unitListIter.hasNext()) {

                                     InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
                                     arJOList.setUnitList(mUomDetails.getUomName());

                                 }

                                 actionForm.saveArJOLList(arJOList);

                             }

                         }

                         actionForm.setShowDeleteButton(false);
                         for(int x=0;x<user.getResCount();x++) {
             		    	Responsibility ds = (Responsibility)user.getRes(x);

             		    	System.out.println("resp: " + ds.getRSName());
             		    	if(ds.getRSName().equals("Super")) {
             		    		actionForm.setShowDeleteButton(true);
             		    		break;
             		    	}

             		    }
                         
                         
                         if (request.getParameter("child") == null) {

                             return (mapping.findForward("arInvoiceEntry"));

                         } else {

                             return (mapping.findForward("arInvoiceEntryChild"));

                         }

                     }
                     

                 }
                 // Populate line when not forwarding

                 if (user.getUserApps().contains("OMEGA INVENTORY") && (actionForm.getType() == null || actionForm.getType().equals("ITEMS"))) {

                     for (int x = 1; x <= invoiceLineNumber; x++) {

                         ArInvoiceLineItemList arILIList = new ArInvoiceLineItemList(actionForm,
                                 null, new Integer(x).toString(), null, null, null, null, null,
                                 null, null, false, false, false, "0.00", "0.00", "0.00", "0.00", null, null, null, "Y");

                         arILIList.setLocationList(actionForm.getLocationList());
                         arILIList.setTaxList(actionForm.getTaxList());
                         arILIList.setUnitList(Constants.GLOBAL_BLANK);
                         arILIList.setUnitList("Select Item First");

                         actionForm.saveArILIList(arILIList);

                     }

                 } else if (user.getUserApps().contains("OMEGA INVENTORY") && actionForm.getType().equals("SO MATCHED")){

                     for (int x = 1; x <= invoiceLineNumber; x++) {

                         ArInvoiceSalesOrderList arSOList = new ArInvoiceSalesOrderList(actionForm,
                                 null, new Integer(x).toString(), null, null, null,
                                 null, null, null, null, null, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null, "Y");

                         arSOList.setLocationList(actionForm.getLocationList());
                         arSOList.setTaxList(actionForm.getTaxList());
                         arSOList.setUnitList(Constants.GLOBAL_BLANK);
                         arSOList.setUnitList("Select Item First");

                         actionForm.saveArSOLList(arSOList);

                     }

                 } else if (user.getUserApps().contains("OMEGA INVENTORY") && actionForm.getType().equals("JO MATCHED")){

                     for (int x = 1; x <= invoiceLineNumber; x++) {

                         ArInvoiceJobOrderList arJOList = new ArInvoiceJobOrderList(actionForm,
                                 null, new Integer(x).toString(), null, null, null,
                                 null, null, null, null, null, false, "0.00", "0.00", "0.00", "0.00", null, "0.00", null, "Y");

                         arJOList.setLocationList(actionForm.getLocationList());
                         arJOList.setTaxList(actionForm.getTaxList());
                         arJOList.setUnitList(Constants.GLOBAL_BLANK);
                         arJOList.setUnitList("Select Item First");

                         actionForm.saveArJOLList(arJOList);

                     }

                 } else {

                	 
                     for (int x = 1; x <= invoiceLineNumber; x++) {

                         ArInvoiceEntryList arINVList = new ArInvoiceEntryList(actionForm,
                                 null, new Integer(x).toString(), null, null, null, null, null, false);

                         arINVList.setMemoLineList(actionForm.getMemoLineList());

                         actionForm.saveArINVList(arINVList);

                     }

                 }


             } catch(GlobalNoRecordFoundException ex) {

                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("invoiceEntry.error.invoiceAlreadyDeleted"));

             } catch(EJBException ex) {

                 if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                             " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));

             }



             if (!errors.isEmpty()) {

                 saveErrors(request, new ActionMessages(errors));

             } else {

                 if (request.getParameter("saveSubmitButton") != null &&
                         actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                     try {
                    	 
                         ArrayList list = ejbINV.getAdApprovalNotifiedUsersByInvCode(actionForm.getInvoiceCode(), user.getCmpCode());

                         if (list.isEmpty()) {

                             messages.add(ActionMessages.GLOBAL_MESSAGE,
                                     new ActionMessage("messages.documentSentForPosting"));

                         } else if (list.contains("DOCUMENT POSTED")) {

                             messages.add(ActionMessages.GLOBAL_MESSAGE,
                                     new ActionMessage("messages.documentPosted"));

                         } else {

                             Iterator i = list.iterator();

                             String APPROVAL_USERS = "";

                             while (i.hasNext()) {

                                 APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

                                 if (i.hasNext()) {

                                     APPROVAL_USERS = APPROVAL_USERS + ", ";

                                 }


                             }

                             messages.add(ActionMessages.GLOBAL_MESSAGE,
                                     new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

                         }

                         saveMessages(request, messages);
                         actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                         actionForm.setInvoiceNumber(null);

                     } catch(EJBException ex) {

                         if (log.isInfoEnabled()) {

                             log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                                     " session: " + session.getId());
                         }

                         return(mapping.findForward("cmnErrorPage"));

                     }

                 } else if (request.getParameter("saveAsDraftButton") != null &&
                         actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                     actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                      actionForm.setInvoiceNumber(null);

                 } else{
                     String documentNumber = ejbDSA.getDocumentNumberSequence("AR INVOICE", user.getCurrentBranch().getBrCode(), user.getCmpCode());
                    actionForm.setInvoiceNumber(documentNumber);
                 }

             }

             actionForm.reset(mapping, request);

             if (actionForm.getType() == null) {

                 if (user.getUserApps().contains("OMEGA INVENTORY")) {

                     actionForm.setType("ITEMS");

                 } else {

                     actionForm.setType("MEMO LINES");

                 }

             }

             actionForm.setInvoiceCode(null);
        
             actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
             actionForm.setAmountDue(Common.convertDoubleToStringMoney(0d, precisionUnit));
             actionForm.setDownPayment(Common.convertDoubleToStringMoney(0d, precisionUnit));
             actionForm.setAmountPaid(Common.convertDoubleToStringMoney(0d, precisionUnit));
             actionForm.setAmountUnearnedInterest(Common.convertDoubleToStringMoney(0d, precisionUnit));
             actionForm.setConversionRate("1.000000");
             actionForm.setPreviousReading(Common.convertDoubleToStringMoney(0d, precisionUnit));
             actionForm.setPresentReading(Common.convertDoubleToStringMoney(0d, precisionUnit));
             actionForm.setPosted("NO");
             actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
             actionForm.setCreatedBy(user.getUserName());
             actionForm.setLastModifiedBy(user.getUserName());
             actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
             java.util.Date date = new java.util.Date();
             Calendar c = Calendar.getInstance();
             c.setTime(date);
             c.add(Calendar.DATE, 1);
             date = c.getTime();


             actionForm.setEffectivityDate(Common.convertSQLDateToString(date));
             actionForm.setRecieveDate(Common.convertSQLDateToString(new java.util.Date()));

             this.setFormProperties(actionForm, user.getCompany());
             return(mapping.findForward("arInvoiceEntry"));

         } else {
        	 System.out.println("no permission");
             errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
             saveErrors(request, new ActionMessages(errors));

             return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

            log.info("Exception caught in ArInvoiceEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }

   
  
   
   
   
   
   

	private void setFormProperties(ArInvoiceEntryForm actionForm , String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getPosted().equals("NO") && !actionForm.getInvoiceVoid()) {

				if (actionForm.getInvoiceCode() == null) {
 
					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableInvoiceVoid(false);
					actionForm.setShowJournalButton(true);
					actionForm.setShowSaveReceivedDateButton(false);
					actionForm.setShowDisableInterestButton(false);
					actionForm.setShowGenerateButton(true);

				} else if (actionForm.getInvoiceCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableInvoiceVoid(true);
					actionForm.setShowJournalButton(true);
					actionForm.setShowSaveReceivedDateButton(false);
					actionForm.setShowDisableInterestButton(false);
					actionForm.setShowGenerateButton(true);


				} else {
					actionForm.setEnableFields(false);
					actionForm.setShowSaveButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableInvoiceVoid(false);
					actionForm.setShowJournalButton(true);
					actionForm.setShowSaveReceivedDateButton(true);
					actionForm.setShowDisableInterestButton(true);
					actionForm.setShowGenerateButton(false);
				}

			} else {
				actionForm.setShowSaveReceivedDateButton(true);
				actionForm.setShowDisableInterestButton(true);
				actionForm.setEnableFields(false);
				actionForm.setShowSaveButton(false);
				if (!actionForm.getInvoiceVoid()) {

					actionForm.setShowDeleteButton(false);

				} else {

					actionForm.setShowDeleteButton(true);

				}

				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableInvoiceVoid(false);
				actionForm.setShowJournalButton(true);

			}


		} else {

			actionForm.setShowSaveReceivedDateButton(true);
			actionForm.setShowDisableInterestButton(true);
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setEnableInvoiceVoid(false);
			actionForm.setShowJournalButton(true);

		}
		
	

			if (actionForm.getInvoiceCode() != null) {

 			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

             String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ar/Invoice/";
             String attachmentFileExtensionData = appProperties.getMessage("app.attachmentFileExtensionData");
             String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
             String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			if(new File(attachmentPath + actionForm.getInvoiceCode() + "-1.txt").exists()) {
 				actionForm.setShowViewAttachmentButton1(true);

 			}else {
 				actionForm.setShowViewAttachmentButton1(false);
 			}


 			if(new File(attachmentPath + actionForm.getInvoiceCode() + "-2.txt").exists()) {
 				actionForm.setShowViewAttachmentButton2(true);

 			}else {
 				actionForm.setShowViewAttachmentButton2(false);
 			}

 			if(new File(attachmentPath + actionForm.getInvoiceCode() + "-3.txt").exists()) {
 				actionForm.setShowViewAttachmentButton3(true);

 			}else {
 				actionForm.setShowViewAttachmentButton3(false);
 			}
 			if(new File(attachmentPath + actionForm.getInvoiceCode() + "-4.txt").exists()) {
 				actionForm.setShowViewAttachmentButton4(true);

 			}else {
 				actionForm.setShowViewAttachmentButton4(false);
 			}

 		} else {

 			actionForm.setShowViewAttachmentButton1(false);
 			actionForm.setShowViewAttachmentButton2(false);
 			actionForm.setShowViewAttachmentButton3(false);
 			actionForm.setShowViewAttachmentButton4(false);

 		}
	}
}