package com.struts.ar.invoiceentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.ReportParameter;
import com.util.Debug;


public class ArInvoiceEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
	private Integer invoiceCode = null;
	private String moduleKey = null;
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList();
	private String customer = null;
    private String customerClass = null;
	private ArrayList customerList = new ArrayList();
	private String documentType = null;
	private ArrayList documentTypeList = new ArrayList();
	private String date = null;
	private String memo = null;
	private String invoiceNumber = null;
	private String totalAmount = null;
	private String referenceNumber = null;
	private String uploadNumber = null;
	private String paymentTerm = null;
	private ArrayList paymentTermList = new ArrayList();
	private String amountDue = null;
	private String downPayment = null;
	private String amountPaid = null;
	private String penaltyDue = null;
	private String penaltyPaid = null;
	private String amountUnearnedInterest = null;
	private boolean invoiceVoid = false;
	private String logEntry = null;

	private boolean disableInterest= false;
	private boolean invoiceInterest = false;
	private String interestReferenceNumber = null;
	private String interestAmount = null;
	private String interestCreatedBy = null;
	private String interestDateCreated = null;
	private String interestNextRunDate = null;
	private String interestLastRunDate = null;
	private ArrayList userList = new ArrayList();
	private String user = null;
	private String description = null;
	private String billTo = null;
	private String billToContact = null;
	private String billToAltContact = null;
	private String billToPhone = null;
	private String billingHeader = null;
	private String billingFooter = null;
	private String billingHeader2 = null;
	private String billingFooter2 = null;
	private String billingHeader3 = null;
	private String billingFooter3 = null;
	private String billingSignatory = null;
	private String signatoryTitle = null;
	private String shipTo = null;
	private String shipToContact = null;
	private String shipToAltContact = null;
	private String shipToPhone = null;
	private String freight = null;
	private ArrayList freightList = new ArrayList();
	private String shipDate = null;
	private String taxCode = null;
	private ArrayList taxCodeList = new ArrayList();
	private String withholdingTax = null;
	private ArrayList withholdingTaxList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String conversionDate = null;
	private String conversionRate = null;
	private String previousReading = null;
	private String presentReading = null;
	private String approvalStatus = null;
	private String reasonForRejection = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private ArrayList memoLineList = new ArrayList();
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String shift = null;
	private ArrayList shiftList = new ArrayList();
	private String soNumber = null;
	private String joNumber = null;
	private String functionalCurrency = null;


	//HRIS
	private String employeeNumber = null;
	private String bioNumber = null;
	private String deployedBranchName = null;
	private ArrayList deployedBranchNameList = new ArrayList();
	private String managingBranch = null;
	private String currentJobPosition = null;

	//PMA
	private String projectCode = null;
	private ArrayList projectCodeList = new ArrayList();

	private String projectTypeCode = null;
	private ArrayList projectTypeCodeList = new ArrayList();

	private String projectPhaseName = null;
	private ArrayList projectPhaseNameList = new ArrayList();

	private String contractTermName = null;
	private ArrayList contractTermList = new ArrayList();



	private ArrayList arINVList = new ArrayList(); //MEMO LINES
	private ArrayList arILIList = new ArrayList(); //ITEMS
	private ArrayList arSOLList = new ArrayList(); //SO MATCHED
	private ArrayList arJOLList = new ArrayList(); //JO MATCHED
	private int rowSelected = 0;
	private int rowILISelected = 0;
	private int rowSOLSelected = 0;
	private int rowJOLSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showBatchName = false;
	private boolean salespersonRequired = false;
	private boolean enableInvoiceVoid = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveButton = false;
	private boolean showSaveReceivedDateButton = false;
	private boolean showDisableInterestButton = false;
	private boolean showDeleteButton = false;
	private boolean showJournalButton = false;
	private boolean showGenerateButton = false;
	private boolean showShift = false;
	private boolean useCustomerPulldown = true;
	private boolean enablePaymentTerm = true;
	private boolean disableSalesPrice = false;


	private String isCustomerEntered  = null;
	private String isProjectEntered  = null;
	private String isProjectTypeEntered  = null;
	private String isTypeEntered = null;
	private String isSalesOrderEntered = null;
	private boolean debitMemo = false;

	private String report = null;
	private String report2 = null;
	private String report3 = null;

	private String salesperson = null;
	private ArrayList salespersonList = new ArrayList();
    private ArrayList salespersonNameList = new ArrayList();
    private String customerName = null;
	private double taxRate = 0;
	private String taxType = null;
	private String isTaxCodeEntered = null;
	private String isConversionDateEntered = null;

	private String clientPO = null;
	private boolean subjectToCommission = false;
	private String effectivityDate = null;
	private String dueDate = null;
	private String recieveDate = null;

	private ArrayList reportParameters = new ArrayList();


	private FormFile invoiceFile=null;

	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;

	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;

	private String attachment = null;
	private String attachmentPDF = null;
	private String attachmentDownload = null;
	private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();

	private String reportType = null;
	private ArrayList reportTypeItemList = new ArrayList();

	private ArrayList reportTypeMemoLineList = new ArrayList();

	private ArrayList reportTypeSoList = new ArrayList();

	private ArrayList reportTypeJoList = new ArrayList();

	private ArrayList taxList = new ArrayList();

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getReport2() {

		return report2;

	}

	public void setReport2(String report2) {

		this.report2 = report2;

	}

	public String getReport3() {

		return report3;

	}

	public void setReport3 (String report3) {

		this.report3 = report3;

	}

	public String getAttachmentPDF() {

		return attachmentPDF;

	}

	public void setAttachmentPDF(String attachmentPDF) {

		this.attachmentPDF = attachmentPDF;

	}


	public String getAttachmentDownload() {

		return attachmentDownload;

	}

	public void setAttachmentDownload(String attachmentDownload) {

		this.attachmentDownload = attachmentDownload;

	}

	public String getAttachment() {

		return attachment;

	}

	public void setAttachment(String attachment) {

		this.attachment = attachment;

	}

	public String getViewType() {

		return viewType ;

	}

	public void setViewType(String viewType) {

		this.viewType = viewType;

	}




	public ArrayList getViewTypeList() {

		return viewTypeList;

	}


	public String getReportType() {

		return reportType ;

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeItemList() {
		return reportTypeItemList;
	}

	public void setReportTypeItemList(String reportType) {
		reportTypeItemList.add(reportType);
	}

	public void clearReportTypeItemList() {
		reportTypeItemList.clear();
	}




	public ArrayList getReportTypeMemoLineList() {
		return reportTypeMemoLineList;
	}

	public void setReportTypeMemoLineList(String reportType) {
		reportTypeMemoLineList.add(reportType);
	}

	public void clearReportTypeMemoLineList() {
		reportTypeMemoLineList.clear();
	}


	public ArrayList getReportTypeSoList() {
		return reportTypeSoList;
	}

	public void setReportTypeSoList(String reportType) {
		reportTypeSoList.add(reportType);
	}

	public void clearReportTypeSoeList() {
		reportTypeSoList.clear();
	}


	public ArrayList getReportTypeJoList() {
		return reportTypeJoList;
	}

	public void setReportTypeJoList(String reportType) {
		reportTypeJoList.add(reportType);
	}

	public void clearReportTypeJoList() {
		reportTypeSoList.clear();
	}

	public int getRowSelected(){
		return rowSelected;
	}

	public ArInvoiceEntryList getArINVByIndex(int index){
		return((ArInvoiceEntryList)arINVList.get(index));
	}

	public Object[] getArINVList(){
		return(arINVList.toArray());
	}

	public int getArINVListSize(){
		return(arINVList.size());
	}

	public void saveArINVList(Object newArINVList){
		arINVList.add(newArINVList);
	}

	public void clearArINVList(){
		arINVList.clear();
	}

	public void setRowSelected(Object selectedArINVList, boolean isEdit){
		this.rowSelected = arINVList.indexOf(selectedArINVList);
	}

	public void updateArINVRow(int rowSelected, Object newArINVList){
		arINVList.set(rowSelected, newArINVList);
	}

	public void deleteArINVList(int rowSelected){
		arINVList.remove(rowSelected);
	}

	public int getRowILISelected(){
		return rowILISelected;
	}

	public ArInvoiceLineItemList getArILIByIndex(int index){
		return((ArInvoiceLineItemList)arILIList.get(index));
	}

	public Object[] getArILIList(){
		return(arILIList.toArray());
	}

	public int getArILIListSize(){
		return(arILIList.size());
	}

	public void saveArILIList(Object newArILIList){
		arILIList.add(newArILIList);
	}

	public void clearArILIList(){
		arILIList.clear();
	}

	public void setRowILISelected(Object selectedArILIList, boolean isEdit){
		this.rowILISelected = arILIList.indexOf(selectedArILIList);
	}

	public void updateArILIRow(int rowILISelected, Object newArILIList){
		arILIList.set(rowILISelected, newArILIList);
	}

	public void deleteArILIList(int rowILISelected){
		arILIList.remove(rowILISelected);
	}

	public int getRowSOLSelected(){
		return rowSOLSelected;
	}

	public ArInvoiceSalesOrderList getArSOLByIndex(int index){
		return((ArInvoiceSalesOrderList)arSOLList.get(index));
	}

	public Object[] getArSOLList(){
		return(arSOLList.toArray());
	}

	public int getArSOLListSize(){
		return(arSOLList.size());
	}

	public void saveArSOLList(Object newArSOLList){
		arSOLList.add(newArSOLList);
	}

	public void clearArSOLList(){
		arSOLList.clear();
	}

	public void setRowSOLSelected(Object selectedArSOLList, boolean isEdit){
		this.rowSOLSelected = arSOLList.indexOf(selectedArSOLList);
	}

	public void updateArSOLRow(int rowSOLSelected, Object newArSOLList){
		arSOLList.set(rowSOLSelected, newArSOLList);
	}

	public void deleteArSOLList(int rowSOLSelected){
		arSOLList.remove(rowSOLSelected);
	}




	public int getRowJOLSelected(){
		return rowJOLSelected;
	}

	public ArInvoiceJobOrderList getArJOLByIndex(int index){
		return((ArInvoiceJobOrderList)arJOLList.get(index));
	}

	public Object[] getArJOLList(){
		return(arJOLList.toArray());
	}

	public int getArJOLListSize(){
		return(arJOLList.size());
	}

	public void saveArJOLList(Object newArJOLList){
		arJOLList.add(newArJOLList);
	}

	public void clearArJOLList(){
		arJOLList.clear();
	}

	public void setRowJOLSelected(Object selectedArJOLList, boolean isEdit){
		this.rowJOLSelected = arJOLList.indexOf(selectedArJOLList);
	}

	public void updateArJOLRow(int rowJOSelected, Object newArJOList){
		arJOLList.set(rowSOLSelected, newArJOList);
	}

	public void deleteArJOLList(int rowJOSelected){
		arJOLList.remove(rowJOSelected);
	}

	public String getLogEntry() {
		return logEntry;
	}

	public void setLogEntry(String logEntry) {
		this.logEntry = logEntry;
	}


	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}


public ArrayList getUserList() {

		return userList;

   }

   public void setUserList(String user) {

	   userList.add(user);

   }

   public void clearUserList() {

	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);

   }

   public String getUser() {

	   return user;

   }

   public void setUser(String user) {

	   this.user = user;

   }


	public Integer getInvoiceCode() {

		return invoiceCode;

	}

	public void setInvoiceCode(Integer invoiceCode) {

		this.invoiceCode = invoiceCode;

	}

	public String getBatchName() {

		return batchName;

	}

	public void setBatchName(String batchName) {

		this.batchName = batchName;


	}

	public ArrayList getBatchNameList() {

		return batchNameList;

	}

	public void setBatchNameList(String batchName) {

		batchNameList.add(batchName);

	}

	public void clearBatchNameList() {

		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);

	}


	public String getDeployedBranchName() {

		return deployedBranchName;

	}

	public void setDeployedBranchName(String deployedBranchName) {

		this.deployedBranchName = deployedBranchName;

	}

	public ArrayList getDeployedBranchNameList() {

		return deployedBranchNameList;

	}

	public void setDeployedBranchNameList(String deployedBranchName) {

		deployedBranchNameList.add(deployedBranchName);

	}

	public void clearDeployedBranchNameList() {

		deployedBranchNameList.clear();
		deployedBranchNameList.add(Constants.GLOBAL_BLANK);

	}


	public String getCustomer() {

		return customer;

	}

	public void setCustomer(String customer) {

		this.customer = customer;

	}

        public String getCustomerClass() {

		return customerClass;

	}

	public void setCustomerClass(String customerClass) {

		this.customerClass = customerClass;

	}

	public ArrayList getCustomerList() {

		return customerList;

	}

	public void setCustomerList(String customer) {

		customerList.add(customer);

	}

	public void clearCustomerList() {

		customerList.clear();
		customerList.add(Constants.GLOBAL_BLANK);

	}



	public String getDate() {

		return date;

	}

	public void setDate(String date) {

		this.date = date;

	}

	public String getMemo() {

		return memo;

	}

	public void setMemo(String memo) {

		this.memo = memo;

	}


	public String getInvoiceNumber() {

		return invoiceNumber;

	}

	public void setInvoiceNumber(String invoiceNumber) {

		this.invoiceNumber = invoiceNumber;

	}

	public String getTotalAmount() {

		return totalAmount;

	}

	public void setTotalAmount(String totalAmount) {

		this.totalAmount = totalAmount;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public void setReferenceNumber(String referenceNumber) {

		this.referenceNumber = referenceNumber;

	}

	public String getUploadNumber() {

		return uploadNumber;

	}

	public void setUploadNumber(String uploadNumber) {

		this.uploadNumber = uploadNumber;

	}

	public String getPaymentTerm() {

		return paymentTerm;

	}

	public void setPaymentTerm(String paymentTerm) {

		this.paymentTerm = paymentTerm;

	}

	public ArrayList getPaymentTermList() {

		return paymentTermList;

	}

	public void setPaymentTermList(String paymentTerm) {

		paymentTermList.add(paymentTerm);

	}

	public void clearPaymentTermList() {

		paymentTermList.clear();

	}

	public String getAmountDue() {

		return amountDue;

	}

	public void setAmountDue(String amountDue) {

		this.amountDue = amountDue;

	}

	public String getDownPayment() {

		return downPayment;

	}

	public void setDownPayment(String downPayment) {

		this.downPayment = downPayment;

	}


	public String getAmountPaid() {

		return amountPaid;

	}

	public void setAmountPaid(String amountPaid) {

		this.amountPaid = amountPaid;

	}

public String getPenaltyDue() {

		return penaltyDue;

	}

	public void setPenaltyDue(String penaltyDue) {

		this.penaltyDue = penaltyDue;

	}

	public String getPenaltyPaid() {

		return penaltyPaid;

	}

	public void setPenaltyPaid(String penaltyPaid) {

		this.penaltyPaid = penaltyPaid;

	}

public String getAmountUnearnedInterest() {

		return amountUnearnedInterest;

	}

	public void setAmountUnearnedInterest(String amountUnearnedInterest) {

		this.amountUnearnedInterest = amountUnearnedInterest;

	}

	public boolean getInvoiceVoid() {

		return invoiceVoid;

	}

	public void setInvoiceVoid(boolean invoiceVoid) {

		this.invoiceVoid = invoiceVoid;

	}

public boolean getDisableInterest() {

		return disableInterest;

	}

	public void setDisableInterest(boolean disableInterest) {

		this.disableInterest = disableInterest;

	}



public boolean getInvoiceInterest() {

		return invoiceInterest;

	}

	public void setInvoiceInterest(boolean invoiceInterest) {

		this.invoiceInterest = invoiceInterest;

	}

	public String getInterestReferenceNumber() {

		return interestReferenceNumber;

	}

	public void setInterestReferenceNumber(String interestReferenceNumber) {

		this.interestReferenceNumber = interestReferenceNumber;

	}

	public String getInterestAmount() {

		return interestAmount;

	}

	public void setInterestAmount(String interestAmount) {

		this.interestAmount = interestAmount;

	}

public String getInterestCreatedBy() {

		return interestCreatedBy;

	}

	public void setInterestCreatedBy(String interestCreatedBy) {

		this.interestCreatedBy = interestCreatedBy;

	}

	public String getInterestDateCreated() {

		return interestDateCreated;

	}

	public void setInterestDateCreated(String interestDateCreated) {

		this.interestDateCreated = interestDateCreated;

	}

	public String getInterestNextRunDate() {

		return interestNextRunDate;

	}

	public void setInterestNextRunDate(String interestNextRunDate) {

		this.interestNextRunDate = interestNextRunDate;

	}

	public String getInterestLastRunDate() {

		return interestLastRunDate;

	}

	public void setInterestLastRunDate(String interestLastRunDate) {

		this.interestLastRunDate = interestLastRunDate;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

		this.description = description;

	}

	public String getBillTo() {

		return billTo;

	}

	public void setBillTo(String billTo) {

		this.billTo = billTo;

	}

	public String getBillToContact() {

		return billToContact;

	}

	public void setBillToContact(String billToContact) {

		this.billToContact = billToContact;

	}

	public String getBillToAltContact() {

		return billToAltContact;

	}

	public void setBillToAltContact(String billToAltContact) {

		this.billToAltContact = billToAltContact;

	}

	public String getBillToPhone() {

		return billToPhone;

	}

	public void setBillToPhone(String billToPhone) {

		this.billToPhone = billToPhone;

	}

	public String getBillingHeader() {

		return billingHeader;

	}

	public void setBillingHeader(String billingHeader) {

		this.billingHeader = billingHeader;

	}

	public String getBillingFooter() {

		return billingFooter;

	}

	public void setBillingFooter(String billingFooter) {

		this.billingFooter = billingFooter;

	}

	public String getBillingHeader2() {

		return billingHeader2;

	}

	public void setBillingHeader2(String billingHeader2) {

		this.billingHeader2 = billingHeader2;

	}

	public String getBillingFooter2() {

		return billingFooter2;

	}

	public void setBillingFooter2(String billingFooter2) {

		this.billingFooter2 = billingFooter2;

	}

	public String getBillingHeader3() {

		return billingHeader3;

	}

	public void setBillingHeader3(String billingHeader3) {

		this.billingHeader3 = billingHeader3;

	}

	public String getBillingFooter3() {

		return billingFooter3;

	}

	public void setBillingFooter3(String billingFooter3) {

		this.billingFooter3 = billingFooter3;

	}

	public String getBillingSignatory() {

		return billingSignatory;

	}

	public void setBillingSignatory(String billingSignatory) {

		this.billingSignatory = billingSignatory;

	}

	public String getSignatoryTitle() {

		return signatoryTitle;

	}

	public void setSignatoryTitle(String signatoryTitle) {

		this.signatoryTitle = signatoryTitle;

	}

	public String getShipTo() {

		return shipTo;

	}

	public void setShipTo(String shipTo) {

		this.shipTo = shipTo;

	}

	public String getShipToContact() {

		return shipToContact;

	}

	public void setShipToContact(String shipToContact) {

		this.shipToContact = shipToContact;

	}

	public String getShipToAltContact() {

		return shipToAltContact;

	}

	public void setShipToAltContact(String shipToAltContact) {

		this.shipToAltContact = shipToAltContact;

	}

	public String getShipToPhone() {

		return shipToPhone;

	}

	public void setShipToPhone(String shipToPhone) {

		this.shipToPhone = shipToPhone;

	}

	public String getFreight() {

		return freight;

	}

	public void setFreight(String freight) {

		this.freight = freight;

	}

	public ArrayList getFreightList() {

		return freightList;

	}

	public void setFreightList(String freight) {

		freightList.add(freight);

	}

	public void clearFreightList() {

		freightList.clear();
		freightList.add(Constants.GLOBAL_BLANK);

	}

	public String getShipDate() {

		return shipDate;

	}

	public void setShipDate(String shipDate) {

		this.shipDate = shipDate;

	}

	public String getTaxCode() {

		return taxCode;

	}

	public void setTaxCode(String taxCode) {

		this.taxCode = taxCode;

	}

	public ArrayList getTaxCodeList() {

		return taxCodeList;

	}

	public void setTaxCodeList(String taxCode) {

		taxCodeList.add(taxCode);

	}

	public void clearTaxCodeList() {

		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);

	}


	public String getWithholdingTax() {

		return withholdingTax;

	}

	public void setWithholdingTax(String withholdingTax) {

		this.withholdingTax = withholdingTax;

	}

	public ArrayList getWithholdingTaxList() {

		return withholdingTaxList;

	}

	public void setWithholdingTaxList(String withholdingTax) {

		withholdingTaxList.add(withholdingTax);

	}

	public void clearWithholdingTaxList() {

		withholdingTaxList.clear();
		withholdingTaxList.add(Constants.GLOBAL_BLANK);

	}


	public String getCurrency() {

		return currency;

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}

	public void setCurrencyList(String currency) {

		currencyList.add(currency);

	}

	public void clearCurrencyList() {

		currencyList.clear();

	}

	public String getConversionDate() {

		return conversionDate;

	}

	public void setConversionDate(String conversionDate) {

		this.conversionDate = conversionDate;

	}

	public String getConversionRate() {

		return conversionRate;

	}

	public void setConversionRate(String conversionRate) {

		this.conversionRate = conversionRate;

	}



	public String getPreviousReading() {

		return previousReading;

	}

	public void setPreviousReading(String previousReading) {

		this.previousReading = previousReading;

	}

	public String getPresentReading() {

		return presentReading;

	}

	public void setPresentReading(String presentReading) {

		this.presentReading = presentReading;

	}


	public String getApprovalStatus() {

		return approvalStatus;

	}

	public void setApprovalStatus(String approvalStatus) {

		this.approvalStatus = approvalStatus;

	}

	public String getReasonForRejection() {

		return reasonForRejection;

	}

	public void setReasonForRejection(String reasonForRejection) {

		this.reasonForRejection = reasonForRejection;

	}

	public String getPosted() {

		return posted;

	}

	public void setPosted(String posted) {

		this.posted = posted;

	}

	public String getCreatedBy() {

		return createdBy;

	}

	public void setCreatedBy(String createdBy) {

		this.createdBy = createdBy;

	}

	public String getDateCreated() {

		return dateCreated;

	}

	public void setDateCreated(String dateCreated) {

		this.dateCreated = dateCreated;

	}

	public String getLastModifiedBy() {

		return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

		this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

		return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

		this.dateLastModified = dateLastModified;

	}


	public String getApprovedRejectedBy() {

		return approvedRejectedBy;

	}

	public void setApprovedRejectedBy(String approvedRejectedBy) {

		this.approvedRejectedBy = approvedRejectedBy;

	}

	public String getDateApprovedRejected() {

		return dateApprovedRejected;

	}

	public void setDateApprovedRejected(String dateApprovedRejected) {

		this.dateApprovedRejected = dateApprovedRejected;

	}

	public String getPostedBy() {

		return postedBy;

	}

	public void setPostedBy(String postedBy) {

		this.postedBy = postedBy;

	}

	public String getDatePosted() {

		return datePosted;

	}

	public void setDatePosted(String datePosted) {

		this.datePosted = datePosted;

	}

	public String getFunctionalCurrency() {

		return functionalCurrency;

	}

	public void setFunctionalCurrency(String functionalCurrency) {

		this.functionalCurrency = functionalCurrency;

	}



public String getEmployeeNumber() {

		return employeeNumber;

	}

	public void setEmployeeNumber(String employeeNumber) {

		this.employeeNumber = employeeNumber;

	}



public String getBioNumber() {

		return bioNumber;

	}

	public void setBioNumber(String bioNumber) {

		this.bioNumber = bioNumber;

	}


public String getManagingBranch() {

		return managingBranch;

	}

	public void setManagingBranch(String managingBranch) {

		this.managingBranch = managingBranch;

	}

public String getCurrentJobPosition() {

		return currentJobPosition;

	}

	public void setCurrentJobPosition(String currentJobPosition) {

		this.currentJobPosition = currentJobPosition;

	}






public String getProjectCode() {

		return projectCode;

	}

	public void setProjectCode(String projectCode) {

		this.projectCode = projectCode;

	}

	public ArrayList getProjectCodeList() {

		return projectCodeList;

	}

	public void setProjectCodeList(String projectCode) {

		projectCodeList.add(projectCode);

	}

	public void clearProjectCodeList() {

		projectCodeList.clear();
		projectCodeList.add(Constants.GLOBAL_BLANK);

	}


public String getProjectTypeCode() {

		return projectTypeCode;

	}

	public void setProjectTypeCode(String projectTypeCode) {

		this.projectTypeCode = projectTypeCode;

	}

	public ArrayList getProjectTypeCodeList() {

		return projectTypeCodeList;

	}

	public void setProjectTypeCodeList(String projectTypeCode) {

		projectTypeCodeList.add(projectTypeCode);

	}

	public void clearProjectTypeCodeList() {

		projectTypeCodeList.clear();
		projectTypeCodeList.add(Constants.GLOBAL_BLANK);

	}


public String getProjectPhaseName() {

		return projectPhaseName;

	}

	public void setProjectPhaseName(String projectPhaseName) {

		this.projectPhaseName = projectPhaseName;

	}

	public ArrayList getProjectPhaseNameList() {

		return projectPhaseNameList;

	}

	public void setProjectPhaseNameList(String projectPhaseName) {

		projectPhaseNameList.add(projectPhaseName);

	}

	public void clearProjectPhaseNameList() {

		projectPhaseNameList.clear();
		projectPhaseNameList.add(Constants.GLOBAL_BLANK);

	}



	public String getContractTermName() {

		return contractTermName;

	}

	public void setContractTermName(String contractTermName) {

		this.contractTermName = contractTermName;

	}

	public ArrayList getContractTermNameList() {

		return contractTermList;

	}

	public void setContractTermNameList(String contractTermName) {

		contractTermList.add(contractTermName);

	}

	public void clearContractTermNameList() {

		contractTermList.clear();
		contractTermList.add(Constants.GLOBAL_BLANK);

	}


	public String getShift() {

		return shift;

	}

	public void setShift(String shift) {

		this.shift = shift;

	}

	public ArrayList getShiftList() {

		return shiftList;

	}

	public void setShiftList(String shift) {

		shiftList.add(shift);

	}

	public void clearShiftList() {

		shiftList.clear();
		shiftList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getEnableFields() {

		return enableFields;

	}

	public void setEnableFields(boolean enableFields) {

		this.enableFields = enableFields;

	}

	public boolean getShowBatchName() {

		return showBatchName;

	}

	public void setShowBatchName(boolean showBatchName) {

		this.showBatchName = showBatchName;

	}
	
	public boolean getSalespersonRequired() {

		return salespersonRequired;

	}

	public void setSalespersonRequired(boolean salespersonRequired) {

		this.salespersonRequired = salespersonRequired;

	}

	public boolean getShowShift() {

		return showShift;

	}

	public void setShowShift(boolean showShift) {

		this.showShift = showShift;

	}

	public ArrayList getMemoLineList() {

		return memoLineList;

	}

	public void setMemoLineList(String memoLine) {

		memoLineList.add(memoLine);

	}

	public void clearMemoLineList() {

		memoLineList.clear();
		memoLineList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getEnableInvoiceVoid() {

		return enableInvoiceVoid;

	}

	public void setEnableInvoiceVoid(boolean enableInvoiceVoid) {

		this.enableInvoiceVoid = enableInvoiceVoid;

	}

	public boolean getShowAddLinesButton() {

		return showAddLinesButton;

	}

	public void setShowAddLinesButton(boolean showAddLinesButton) {

		this.showAddLinesButton = showAddLinesButton;

	}

	public boolean getShowDeleteLinesButton() {

		return showDeleteLinesButton;

	}

	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

		this.showDeleteLinesButton = showDeleteLinesButton;

	}

	public boolean getShowSaveButton() {

		return showSaveButton;

	}

	public void setShowSaveButton(boolean showSaveButton) {

		this.showSaveButton = showSaveButton;

	}

	public boolean getShowSaveReceivedDateButton() {

		return showSaveReceivedDateButton;

	}

	public void setShowSaveReceivedDateButton(boolean showSaveReceivedDateButton) {

		this.showSaveReceivedDateButton = showSaveReceivedDateButton;

	}


public boolean getShowDisableInterestButton() {

		return showDisableInterestButton;

	}

	public void setShowDisableInterestButton(boolean showDisableInterestButton) {

		this.showDisableInterestButton = showDisableInterestButton;

	}

	public boolean getShowDeleteButton() {

		return showDeleteButton;

	}

	public void setShowDeleteButton(boolean showDeleteButton) {

		this.showDeleteButton = showDeleteButton;

	}

	public boolean getShowJournalButton() {

		return showJournalButton;

	}

	public void setShowJournalButton(boolean showJournalButton) {

		this.showJournalButton = showJournalButton;

	}

	public boolean getShowGenerateButton(){
		return showGenerateButton;
	}

	public void setShowGenerateButton(boolean showGenerateButton){
		this.showGenerateButton = showGenerateButton;
	}

	public String getIsCustomerEntered() {

		return isCustomerEntered;

	}

public String getIsProjectEntered() {

		return isProjectEntered;

	}



public String getIsProjectTypeEntered() {

	return isProjectTypeEntered;

}

	public String getIsTypeEntered() {

		return isTypeEntered;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {


		this.type = type;

	}

	public ArrayList getTypeList() {

		return typeList;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(String location) {

		locationList.add(location);

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getUseCustomerPulldown() {

		return useCustomerPulldown;

	}

	public void setUseCustomerPulldown(boolean useCustomerPulldown) {

		this.useCustomerPulldown = useCustomerPulldown;

	}

	public String getSoNumber() {

		return soNumber;

	}

	public void setSoNumber(String soNumber) {

		this.soNumber = soNumber;

	}


	public String getJoNumber() {

		return joNumber;

	}

	public void setJoNumber(String joNumber) {

		this.joNumber = joNumber;

	}

	public String getIsSalesOrderEntered() {

		return isSalesOrderEntered;

	}

	public void setIsSalesOrderEntered(String isSalesOrderEntered) {

		this.isSalesOrderEntered = isSalesOrderEntered;

	}

	public boolean getDebitMemo() {

   	   return debitMemo;

   }

   public void setDebitMemo(boolean debitMemo) {

   	   this.debitMemo = debitMemo;

   }

	public String getSalesperson() {

		return salesperson;

	}

	public void setSalesperson(String salesperson) {

		this.salesperson = salesperson;

	}

	public ArrayList getSalespersonList() {

		return salespersonList;

	}

	public void setSalespersonList(String salesperson) {

		salespersonList.add(salesperson);

	}

	public void clearSalespersonList() {

		salespersonList.clear();
		salespersonList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getSubjectToCommission() {

		return subjectToCommission;

	}

	public void setSubjectToCommission(boolean subjecToCommision) {

		this.subjectToCommission = subjecToCommision;

	}

    public String getCustomerName() {

    	return customerName;

    }

    public void setCustomerName(String customerName) {

    	this.customerName = customerName;

    }

    public ArrayList getSalespersonNameList() {

    	return salespersonNameList;

    }

    public void setSalespersonNameList(String salespersonName) {

    	salespersonNameList.add(salespersonName);

    }

    public void clearSalespersonNameList() {

    	salespersonNameList.clear();
    	salespersonNameList.add(Constants.GLOBAL_BLANK);

    }

	public double getTaxRate() {

		return taxRate;

	}

	public void setTaxRate(double taxRate) {

		this.taxRate = taxRate;

	}

	public String getTaxType() {

		return taxType;

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public String getClientPO() {

		return clientPO;

	}

	public void setClientPO(String clientPO) {

		this.clientPO = clientPO;

	}

	public String getIsTaxCodeEntered() {

		return isTaxCodeEntered;

	}

	public String getIsConversionDateEntered(){

		return isConversionDateEntered;

	}

	public String getEffectivityDate() {

		return effectivityDate;

	}

	public void setEffectivityDate(String effectivityDate) {

		this.effectivityDate = effectivityDate;

	}

	public String getRecieveDate() {

		return recieveDate;

	}

	public void setRecieveDate(String recieveDate) {

		this.recieveDate = recieveDate;

	}

	public String getDueDate() {

		return dueDate;

	}

	public void setDueDate(String dueDate) {

		this.dueDate = dueDate;

	}

	public boolean getEnablePaymentTerm() {

		return enablePaymentTerm;

	}

	public void setEnablePaymentTerm(boolean enablePaymentTerm) {

		this.enablePaymentTerm = enablePaymentTerm;

	}

	public boolean getDisableSalesPrice() {

		return disableSalesPrice;

	}

	public void setDisableSalesPrice(boolean disableSalesPrice) {

		this.disableSalesPrice = disableSalesPrice;

	}

public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

	public FormFile getInvoiceFile() {

		return invoiceFile;

	}

	public void setInvoiceFile(FormFile invoiceFile) {

		this.invoiceFile = invoiceFile;

	}







	public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {

		return filename3;

	}

	public void setFilename3(FormFile filename3) {

		this.filename3 = filename3;

	}

	public FormFile getFilename4() {

		return filename4;

	}

	public void setFilename4(FormFile filename4) {

		this.filename4 = filename4;

	}

	public boolean getShowViewAttachmentButton1() {

		return showViewAttachmentButton1;

	}

	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

		this.showViewAttachmentButton1 = showViewAttachmentButton1;

	}

	public boolean getShowViewAttachmentButton2() {

		return showViewAttachmentButton2;

	}

	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

		this.showViewAttachmentButton2 = showViewAttachmentButton2;

	}

	public boolean getShowViewAttachmentButton3() {

		return showViewAttachmentButton3;

	}

	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

		this.showViewAttachmentButton3 = showViewAttachmentButton3;

	}

	public boolean getShowViewAttachmentButton4() {

		return showViewAttachmentButton4;

	}

	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

		this.showViewAttachmentButton4 = showViewAttachmentButton4;

	}


	public Object[] getReportParameters() {
		return(reportParameters.toArray());
	}

	public void setReportParameters(ArrayList reportParameters) {
		this.reportParameters = reportParameters;
	}


	public ArrayList getReportParametersAsArrayList() {
		return reportParameters;
	}

	public void addReportParameters(ReportParameter reportParameter) {
		reportParameters.add(reportParameter);
	}

	public void clearReportParameters() {
		reportParameters.clear();
	}


	public ArrayList getTaxList() {

		return taxList;

	}

	public void setTaxList(String tax) {

		taxList.add(tax);

	}

	public void clearTaxList() {

		taxList.clear();


	}

	public String getDocumentType() {
    	return documentType;
    }

    public void setDocumentType(String documentType) {
    	this.documentType = documentType;
    }

    public ArrayList getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(String documentType) {
		documentTypeList.add(documentType);
	}

	public void clearDocumentTypeList() {
		documentTypeList.clear();
		documentTypeList.add(Constants.GLOBAL_BLANK);
	}


	public String getModuleKey() {
		return moduleKey;
	}

	public void setModuleKey(String moduleKey) {
		this.moduleKey = moduleKey;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request){

		//invoiceCode = null;
		documentType = null;
		reportType = "";
		viewTypeList.clear();
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
		viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);
		viewType = Constants.REPORT_VIEW_TYPE_PDF;
		customer = Constants.GLOBAL_BLANK;
        customerClass = Constants.GLOBAL_BLANK;
		taxCode = Constants.GLOBAL_BLANK;
		withholdingTax = Constants.GLOBAL_BLANK;
		freight = Constants.GLOBAL_BLANK;
		shift = Constants.GLOBAL_BLANK;
		date = null;
		memo = null;
		moduleKey = null;
		//invoiceNumber = null;
		totalAmount = null;
		referenceNumber = null;
		billTo = null;
		billToContact = null;
		billToAltContact = null;
		billToPhone = null;
		billingHeader = null;
		billingFooter = null;
		billingHeader2 = null;
		billingFooter2 = null;
		billingHeader3 = null;
		billingFooter3 = null;
		billingSignatory = null;
		signatoryTitle = null;
		shipTo = null;
		shipToContact = null;
		shipToAltContact = null;
		shipToPhone = null;
		shipDate = null;
		amountDue = null;
		downPayment = null;
		amountPaid = null;
		penaltyDue = null;
		penaltyPaid = null;
		invoiceVoid = false;
		disableInterest = false;
		invoiceInterest = false;
		description = null;
		conversionDate = null;
		conversionRate = null;
		previousReading = null;
		presentReading = null;
		approvalStatus = null;
		reasonForRejection = null;
		posted = null;
		logEntry = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;
		approvedRejectedBy = null;
		dateApprovedRejected = null;
		postedBy = null;
		datePosted = null;
		isCustomerEntered = null;
		isTypeEntered = null;
		typeList.clear();
		typeList.add("ITEMS");
		typeList.add("MEMO LINES");
		typeList.add("SO MATCHED");
		typeList.add("JO MATCHED");
		location = Constants.GLOBAL_BLANK;
		soNumber = null;
		joNumber = null;
		debitMemo = false;
		salesperson = null;
		subjectToCommission = false;
        customerName = Constants.GLOBAL_BLANK;
        taxRate = 0;
        taxType = Constants.GLOBAL_BLANK;
        isTaxCodeEntered = null;
        clientPO = null;
        isConversionDateEntered = null;
        effectivityDate = null;
        dueDate = null;
        recieveDate = null;
        invoiceFile=null;
        filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
		attachment = null;
		attachmentPDF = null;
		attachmentDownload = null;
		employeeNumber = null;
		bioNumber = null;
		deployedBranchName = null;
		managingBranch = null;
		currentJobPosition = null;

		projectCode = null;
		projectTypeCode = null;
		projectPhaseName = null;
		contractTermName = null;

		taxList.clear();
		taxList.add("Y");
		taxList.add("N");

		for (int i=0; i<arILIList.size(); i++) {

			ArInvoiceLineItemList actionList = (ArInvoiceLineItemList)arILIList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);
			actionList.setAutoBuildCheckbox(false);
			actionList.setIsAssemblyItem(false);
		}

		for (int i=0; i<arINVList.size(); i++) {

			ArInvoiceEntryList actionList = (ArInvoiceEntryList)arINVList.get(i);
			actionList.setIsMemoLineEntered(null);
			actionList.setTaxable(false);

		}

		for (int i=0; i<arSOLList.size(); i++) {

			ArInvoiceSalesOrderList actionList = (ArInvoiceSalesOrderList)arSOLList.get(i);
			actionList.setIsUnitEntered(null);
			actionList.setIssueCheckbox(false);

		}

		// reset tax

		/*if(request.getParameter("saveSubmitButton") != null || request.getParameter("saveAsDraftButton") != null ||
				request.getParameter("printButton") != null || request.getParameter("journalButton") != null) {

			Iterator i = arINVList.iterator();

			while (i.hasNext()) {

				ArInvoiceEntryList list = (ArInvoiceEntryList)i.next();

				list.setTaxable(false);

			}

		}*/

	}

	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		   //ActionErrors errors = new ActionErrors();

		   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		   String separator = "$";

		   // Remove first $ character
		   misc = misc.substring(1);

		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;
		   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		   System.out.println("ctr :" + ctr);

		   /*if(y==0)
			   return new ArrayList();*/

		   ArrayList miscList = new ArrayList();

		   for(int x=0; x<ctr; x++) {
			   try {

				   // Date
				   start = nextIndex + 1;
				   nextIndex = misc.indexOf(separator, start);
				   length = nextIndex - start;
				   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				   String checker = misc.substring(start, start + length);
				   if(checker!=""&& checker != " "){
					   miscList.add(checker);
				   }else{
					   miscList.add("null");
				   }

				   //System.out.println(misc.substring(start, start + length));
			   } catch (Exception ex) {

			   }

		   	   }
		   return miscList;
	   }


	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){

		ActionErrors errors = new ActionErrors();
		System.out.println("saveAsDraftButton="+request.getParameter("saveAsDraftButton"));
		//if(request.getParameter("isSalesOrderEntered") != null) return errors;
		System.out.println("saveSubmitButton="+request.getParameter("saveSubmitButton"));
		if(request.getParameter("saveSubmitButton") != null || request.getParameter("saveAsDraftButton") != null ||
				request.getParameter("printButton") != null || request.getParameter("drPrintButton") != null ||
				request.getParameter("journalButton") != null || request.getParameter("tdPrintButton") != null){

			if(Common.validateRequired(description)){
				errors.add("description",
						new ActionMessage("invoiceEntry.error.descriptionIsRequired"));
			}


			if(showBatchName){
				if(Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
					errors.add("batchName",
							new ActionMessage("invoiceEntry.error.batchNameRequired"));
				}else{
					boolean notFound = true;
					for(int x=0;x<batchNameList.size();x++){
						System.out.println("x="+x);
						System.out.println("batchNameList.get(x).toString()="+batchNameList.get(x).toString());
						String batchNameCmp = batchNameList.get(x).toString();

						if(batchNameCmp.equals(batchName)){
							System.out.println(batchNameCmp+ "="+ batchName);
							notFound = false;
							break;
						}
					}

					if(notFound){
						errors.add("batchName",
								new ActionMessage("invoiceEntry.error.batchNameNotExist"));
					}
				}
			}
			
			if(salespersonRequired && !type.equals("JO MATCHED")) {
				
				if(Common.validateRequired(salesperson) || salesperson.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
					
					
					errors.add("salesperson",
							new ActionMessage("preference.prompt.invoiceSalespersonRequired"));
				}
			}

			/*

			if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showBatchName){
				errors.add("batchName",
						new ActionMessage("invoiceEntry.error.batchNameRequired"));
			}

			*/


			if((Common.validateRequired(shift) || shift.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showShift && type.equals("ITEMS")){
				errors.add("shift",
						new ActionMessage("invoiceEntry.error.shiftRequired"));
			}

			if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("customer",
						new ActionMessage("invoiceEntry.error.customerRequired"));
			}

			if(Common.validateRequired(date)){
				errors.add("date",
						new ActionMessage("invoiceEntry.error.dateRequired"));
			}

			if(!Common.validateDateFormat(date)){
				errors.add("date",
						new ActionMessage("invoiceEntry.error.dateInvalid"));
			}
			
			if(Common.validateRequired(invoiceNumber)){
                errors.add("invoiceNumber",
                        new ActionMessage("invoiceEntry.error.invoiceNumberIsRequired"));
            }
			
			try{

				int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
				if(year<1900){
					errors.add("date",
							new ActionMessage("invoiceEntry.error.dateInvalid"));
				}
			} catch(Exception ex){

				errors.add("date",
						new ActionMessage("invoiceEntry.error.dateInvalid"));

			}


			if(Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("paymentTerm",
						new ActionMessage("invoiceEntry.error.paymentTermRequired"));
			}

			if(!Common.validateDateFormat(shipDate)){
				errors.add("shipDate",
						new ActionMessage("invoiceEntry.error.shipDateInvalid"));
			}


			if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("taxCode",
						new ActionMessage("invoiceEntry.error.taxCodeRequired"));
			}

			if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("withholdingTax",
						new ActionMessage("invoiceEntry.error.withholdingTaxRequired"));
			}

			if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("currency",
						new ActionMessage("invoiceEntry.error.currencyRequired"));
			}

			if(!Common.validateDateFormat(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("invoiceEntry.error.conversionDateInvalid"));
			}

			if(!Common.validateMoneyRateFormat(conversionRate)){
				errors.add("conversionRate",
						new ActionMessage("invoiceEntry.error.conversionRateInvalid"));
			}

			if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("invoiceEntry.error.conversionDateMustBeNull"));
			}

			if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
					Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
				errors.add("conversionRate",
						new ActionMessage("invoiceEntry.error.conversionRateMustBeNull"));
			}

			if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
					Common.validateRequired(conversionDate)){
				errors.add("conversionRate",
						new ActionMessage("invoiceEntry.error.conversionRateOrDateMustBeEntered"));
			}

			if(Common.validateRequired(effectivityDate)){
				errors.add("effectivityDate",
						new ActionMessage("invoiceEntry.error.effectivityDateRequired"));
			}

			if(!Common.validateDateFormat(effectivityDate)){
				errors.add("effectivityDate",
						new ActionMessage("invoiceEntry.error.effectivityDateInvalid"));
			}

			if(!Common.validateDateFromTo(date, effectivityDate)){
				errors.add("date", new ActionMessage("invoiceEntry.error.effectivityDateLessThanInvoiceDate"));
			}

			System.out.println("TYPE="+type);
			if (type.equals("MEMO LINES")) {

				int numberOfLines = 0;

				Iterator i = arINVList.iterator();

				while (i.hasNext()) {

					ArInvoiceEntryList ilList = (ArInvoiceEntryList)i.next();

					if (Common.validateRequired(ilList.getMemoLine()) &&
							Common.validateRequired(ilList.getDescription()) &&
							Common.validateRequired(ilList.getQuantity()) &&
							Common.validateRequired(ilList.getUnitPrice()) &&
							Common.validateRequired(ilList.getAmount())) continue;

					numberOfLines++;



					if(Common.validateRequired(ilList.getMemoLine()) || ilList.getMemoLine().equals(Constants.GLOBAL_BLANK)
							|| ilList.getMemoLine().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
						errors.add("memoLine",
								new ActionMessage("invoiceEntry.error.memoLineRequired", ilList.getLineNumber()));
					}
					if(Common.validateRequired(ilList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityRequired", ilList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(ilList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityInvalid", ilList.getLineNumber()));
					}
					if(Common.validateRequired(ilList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("invoiceEntry.error.unitPriceRequired", ilList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(ilList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("invoiceEntry.error.unitPriceInvalid", ilList.getLineNumber()));
					}
					if(Common.validateRequired(ilList.getAmount())){
						errors.add("amount",
								new ActionMessage("invoiceEntry.error.amountRequired", ilList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(ilList.getAmount())){
						errors.add("amount",
								new ActionMessage("invoiceEntry.error.amountInvalid", ilList.getLineNumber()));
					}


				}

				if (numberOfLines == 0) {

					errors.add("customer",
							new ActionMessage("invoiceEntry.error.invoiceMustHaveLine"));

				}

			} else if (type.equals("ITEMS")) {

				int numOfLines = 0;

				Iterator i = arILIList.iterator();

				while (i.hasNext()) {

					ArInvoiceLineItemList iliList = (ArInvoiceLineItemList)i.next();

					if (Common.validateRequired(iliList.getLocation()) &&
							Common.validateRequired(iliList.getItemName()) &&
							Common.validateRequired(iliList.getQuantity()) &&
							Common.validateRequired(iliList.getUnit()) &&
							Common.validateRequired(iliList.getUnitPrice())) continue;

					if (Common.convertStringMoneyToDouble(iliList.getQuantity(), (short)3) <= 0) continue;

					numOfLines++;
					/*
					try{
		      	 		String separator = "$";
		      	 		String misc = "";
		      		   // Remove first $ character
		      		   misc = iliList.getMisc().substring(1);

		      		   // Counter
		      		   int start = 0;
		      		   int nextIndex = misc.indexOf(separator, start);
		      		   int length = nextIndex - start;
		      		   int counter;
		      		   counter = Integer.parseInt(misc.substring(start, start + length));

		      	 		ArrayList miscList = this.getExpiryDateStr(iliList.getMisc(), counter);
		      	 		System.out.println("rilList.getMisc() : " + iliList.getMisc());
		      	 		Iterator mi = miscList.iterator();

		      	 		int ctrError = 0;
		      	 		int ctr = 0;

		      	 		while(mi.hasNext()){
		      	 				String miscStr = (String)mi.next();

		      	 				if(!Common.validateDateFormat(miscStr)){
		      	 					errors.add("date",
		      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
		      	 					ctrError++;
		      	 				}

		      	 				System.out.println("miscStr: "+miscStr);
		      	 				if(miscStr=="null"){
		      	 					ctrError++;
		      	 				}
		      	 		}
		      	 		//if ctr==Error => No Date Will Apply
		      	 		//if ctr>error => Invalid Date
		      	 		System.out.println("CTR: "+  miscList.size());
		      	 		System.out.println("ctrError: "+  ctrError);
		      	 		System.out.println("counter: "+  counter);
		      	 			if(ctrError>0 && ctrError!=miscList.size()){
		      	 				errors.add("date",
		      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
		      	 			}

		      	 		//if error==0 Add Expiry Date


		      	 	}catch(Exception ex){

		  	  	    }*/


					if(Common.validateRequired(iliList.getLocation()) || iliList.getLocation().equals(Constants.GLOBAL_BLANK)){
						errors.add("location",
								new ActionMessage("invoiceEntry.error.locationRequired", iliList.getLineNumber()));
					}
					if(Common.validateRequired(iliList.getItemName()) || iliList.getItemName().equals(Constants.GLOBAL_BLANK)){
						errors.add("itemName",
								new ActionMessage("invoiceEntry.error.itemNameRequired", iliList.getLineNumber()));
					}
					if(Common.validateRequired(iliList.getQuantity())) {
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityRequired", iliList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(iliList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityInvalid", iliList.getLineNumber()));
					}
					//if(!Common.validateRequired(iliList.getQuantity()) && Common.convertStringMoneyToDouble(iliList.getQuantity(), (short)3) <= 0) {
					//	errors.add("quantity",
					//			new ActionMessage("invoiceEntry.error.negativeOrZeroQuantityNotAllowed", iliList.getLineNumber()));
					//}
					if(Common.validateRequired(iliList.getUnit()) || iliList.getUnit().equals(Constants.GLOBAL_BLANK)){
						errors.add("unit",
								new ActionMessage("invoiceEntry.error.unitRequired", iliList.getLineNumber()));
					}
					if(Common.validateRequired(iliList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("invoiceEntry.error.unitPriceRequired", iliList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(iliList.getUnitPrice())){
						errors.add("unitCost",
								new ActionMessage("invoiceEntry.error.unitCostInvalid", iliList.getLineNumber()));
					}

				}

				if (numOfLines == 0) {

					errors.add("customer",
							new ActionMessage("invoiceEntry.error.invoiceMustHaveLine"));

				}

			}	else if (type.equals("JO MATCHED")) {

				int numOfLines = 0;

				Iterator i = arJOLList.iterator();

				while (i.hasNext()) {

					ArInvoiceJobOrderList joList = (ArInvoiceJobOrderList)i.next();

					if(!joList.getIssueCheckbox()) continue;

					numOfLines++;

					if (Common.validateRequired(joList.getLocation()) &&
							Common.validateRequired(joList.getItemName()) &&
							Common.validateRequired(joList.getQuantity()) &&
							Common.validateRequired(joList.getUnit()) &&
							Common.validateRequired(joList.getUnitPrice())) continue;



					if(Common.validateRequired(joList.getLocation()) || joList.getLocation().equals(Constants.GLOBAL_BLANK)){
						errors.add("location",
								new ActionMessage("invoiceEntry.error.locationRequired", joList.getLineNumber()));
					}
					if(Common.validateRequired(joList.getItemName()) || joList.getItemName().equals(Constants.GLOBAL_BLANK)){
						errors.add("itemName",
								new ActionMessage("invoiceEntry.error.itemNameRequired", joList.getLineNumber()));
					}
					if(Common.validateRequired(joList.getQuantity())) {
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityRequired", joList.getLineNumber()));
					}
					if(Common.validateNumGreaterThan(joList.getQuantity().replaceAll(",",""),new Double(joList.getRemaining().replaceAll(",","")).doubleValue())){
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.SoQuantityMustNotGreaterThanRemainingQuantity", joList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(joList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityInvalid", joList.getLineNumber()));
					}
					if(!Common.validateRequired(joList.getQuantity()) && Common.convertStringMoneyToDouble(joList.getQuantity(), (short)3) <= 0) {
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.negativeOrZeroQuantityNotAllowed", joList.getLineNumber()));
					}
					if(Common.validateRequired(joList.getUnit()) || joList.getUnit().equals(Constants.GLOBAL_BLANK)){
						errors.add("unit",
								new ActionMessage("invoiceEntry.error.unitRequired", joList.getLineNumber()));
					}
					if(Common.validateRequired(joList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("invoiceEntry.error.unitPriceRequired", joList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(joList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("invoiceEntry.error.unitPriceInvalid", joList.getLineNumber()));
					}

				}

				if (numOfLines == 0) {

					errors.add("customer",
							new ActionMessage("invoiceEntry.error.invoiceMustHaveLine"));

				}






			}else {

				System.out.println("TYPE SO MATCHED--------------------------------->");

				int numOfLines = 0;

				Iterator i = arSOLList.iterator();

				while (i.hasNext()) {

					ArInvoiceSalesOrderList solList = (ArInvoiceSalesOrderList)i.next();

					if(!solList.getIssueCheckbox()) continue;

					numOfLines++;

					if (Common.validateRequired(solList.getLocation()) &&
							Common.validateRequired(solList.getItemName()) &&
							Common.validateRequired(solList.getQuantity()) &&
							Common.validateRequired(solList.getUnit()) &&
							Common.validateRequired(solList.getUnitPrice())) continue;



					if(Common.validateRequired(solList.getLocation()) || solList.getLocation().equals(Constants.GLOBAL_BLANK)){
						errors.add("location",
								new ActionMessage("invoiceEntry.error.locationRequired", solList.getLineNumber()));
					}
					if(Common.validateRequired(solList.getItemName()) || solList.getItemName().equals(Constants.GLOBAL_BLANK)){
						errors.add("itemName",
								new ActionMessage("invoiceEntry.error.itemNameRequired", solList.getLineNumber()));
					}
					if(Common.validateRequired(solList.getQuantity())) {
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityRequired", solList.getLineNumber()));
					}
				//	if(Common.validateNumGreaterThan(solList.getQuantity().replaceAll(",",""),new Double(solList.getRemaining().replaceAll(",","")).doubleValue())){
					//	errors.add("quantity",
				//				new ActionMessage("invoiceEntry.error.SoQuantityMustNotGreaterThanRemainingQuantity", solList.getLineNumber()));
				///	}
					if(!Common.validateNumberFormat(solList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.quantityInvalid", solList.getLineNumber()));
					}
					if(!Common.validateRequired(solList.getQuantity()) && Common.convertStringMoneyToDouble(solList.getQuantity(), (short)3) <= 0) {
						errors.add("quantity",
								new ActionMessage("invoiceEntry.error.negativeOrZeroQuantityNotAllowed", solList.getLineNumber()));
					}
					if(Common.validateRequired(solList.getUnit()) || solList.getUnit().equals(Constants.GLOBAL_BLANK)){
						errors.add("unit",
								new ActionMessage("invoiceEntry.error.unitRequired", solList.getLineNumber()));
					}
					if(Common.validateRequired(solList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("invoiceEntry.error.unitPriceRequired", solList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(solList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("invoiceEntry.error.unitPriceInvalid", solList.getLineNumber()));
					}

				}

				if (numOfLines == 0) {

					errors.add("customer",
							new ActionMessage("invoiceEntry.error.invoiceMustHaveLine"));

				}

			}

		} else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

			if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("customer",
						new ActionMessage("invoiceEntry.error.customerRequired"));
			}

		} else if (!Common.validateRequired(request.getParameter("isSalesOrderEntered"))) {

			/*
			if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("customer",
						new ActionMessage("invoiceEntry.error.customerRequired"));
			}
			 */
			if (type.equals("SO MATCHED")) {

				if(Common.validateRequired(soNumber)){
					errors.add("soNumber",
							new ActionMessage("invoiceEntry.error.soNumberRequired"));
				}

			}

		} else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

			if(!Common.validateDateFormat(conversionDate)){

				errors.add("conversionDate", new ActionMessage("invoiceEntry.error.conversionDateInvalid"));

			}

		} else if(request.getParameter("generateButton")!=null){

			System.out.println("pasok generate");
			if(invoiceFile.getFileSize()<=0){
				errors.add("location",
						new ActionMessage("invoiceEntry.error.invoiceFileError"));
			}


		}

		return(errors);

	}
}
