package com.struts.ar.invoiceentry;

import java.util.ArrayList;

public class ArInvoiceJobOrderList implements java.io.Serializable {

    private Integer invoiceJobOrderLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String remaining = null;
	private String quantity = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private ArrayList tagList = new ArrayList();
	private String unitPrice = null;
	private String amount = null;
	private String discount1 = null;
	private String discount2 = null;
	private String discount3 = null;
	private String discount4 = null;
	private String totalDiscount = null;
	private String origDiscountAmount = null;
	private String misc = null;
	private ArrayList taxList = new ArrayList();
	private String tax = null;
	private boolean issueCheckbox = false;

	private String isUnitEntered = null;
	private boolean isTraceMisc = false;

	private ArInvoiceEntryForm parentBean;

	public ArInvoiceJobOrderList(ArInvoiceEntryForm parentBean,
			Integer invoiceJobOrderLineCode,
			String lineNumber,
			String location,
			String itemName,
			String itemDescription,
			String remaining,
			String quantity,
			String unit,
			String unitPrice,
			String amount,
			boolean issueCheckbox,
			String discount1,
			String discount2,
			String discount3,
			String discount4,
			String totalDiscount,
			String origDiscountAmount,
			String misc,
			String tax){

		this.parentBean = parentBean;
		this.invoiceJobOrderLineCode = invoiceJobOrderLineCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.remaining = remaining;
		this.quantity = quantity;
		this.unit = unit;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.issueCheckbox = issueCheckbox;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.totalDiscount = totalDiscount;
		this.origDiscountAmount = origDiscountAmount;
		this.misc = misc;
		this.tax = tax;
	}

	public Integer getInvoiceJobOrderLineCode(){

		return(invoiceJobOrderLineCode);

	}

	public String getLineNumber() {

		return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

		this.lineNumber = lineNumber;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

		this.itemDescription = itemDescription;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(ArrayList locationList) {

		this.locationList = locationList;

	}

	public void clearLocationList() {

		locationList.clear();

	}


	public ArInvoiceJobOrderTagList getTagListByIndex(int index){
	      return((ArInvoiceJobOrderTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}

	public String getRemaining() {

		return remaining;

	}

	public void setRemaining(String remaining) {

		this.remaining = remaining;

	}

	public String getQuantity() {

		return quantity;

	}

	public void setQuantity(String quantity) {

		this.quantity = quantity;

	}

	public String getUnit() {

		return unit;

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}

	public ArrayList getUnitList() {

		return unitList;

	}

	public void setUnitList(String unit) {

		unitList.add(unit);

	}

	public void clearUnitList() {

		unitList.clear();

	}

	public String getUnitPrice() {

		return unitPrice;

	}

	public void setUnitPrice(String unitPrice) {

		this.unitPrice = unitPrice;

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}

	public boolean getIssueCheckbox() {

		return issueCheckbox;

	}

	public void setIssueCheckbox(boolean issueCheckbox) {

		this.issueCheckbox = issueCheckbox;

	}

	public String getIsUnitEntered() {

		return isUnitEntered;

	}

	public void setIsUnitEntered(String isUnitEntered) {

		if (isUnitEntered != null && isUnitEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isUnitEntered = null;

	}

	public String getDiscount1() {

		return discount1;

	}

	public void setDiscount1(String discount1) {

		this.discount1 = discount1;

	}

	public String getDiscount2() {

		return discount2;

	}

	public void setDiscount2(String discount2) {

		this.discount2 = discount2;

	}

	public String getDiscount3() {

		return discount3;

	}

	public void setDiscount3(String discount3) {

		this.discount3 = discount3;

	}

	public String getDiscount4() {

		return discount4;

	}

	public void setDiscount4(String discount4) {

		this.discount4 = discount4;

	}

	public String getTotalDiscount() {

		return totalDiscount;

	}

	public void setTotalDiscount(String totalDiscount) {

		this.totalDiscount = totalDiscount;

	}

	public String getOrigDiscountAmount() {

		return origDiscountAmount;

	}

	public void setOrigDiscountAmount(String origDiscountAmount) {

		this.origDiscountAmount = origDiscountAmount;

	}


	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;

	}


	public ArrayList getTaxList() {

		return taxList;

	}

	public void setTaxList(ArrayList taxList) {

		this.taxList = taxList;

	}

	public void clearTaxList() {

		taxList.clear();

	}


	public String getTax() {

		return tax;

	}

	public void setTax(String tax) {

		this.tax = tax;

	}


	public boolean getIsTraceMisc() {
		return isTraceMisc;
	}

	public void setIsTraceMisc(boolean isTraceMisc) {
		this.isTraceMisc = isTraceMisc;
	}


}
