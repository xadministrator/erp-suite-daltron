package com.struts.ar.invoiceimport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.txn.ArInvoiceImportController;
import com.ejb.txn.ArInvoiceImportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineItemDetails;

public final class ArInvoiceImportAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try{
			
/*******************************************************
 Check if user has a session
 *******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ArInvoiceImportAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
					
				}
				
			}else{
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ArInvoiceImportForm actionForm = (ArInvoiceImportForm)form;
			
			
			String frParam = Common.getUserPermission(user, Constants.AR_INVOICE_IMPORT_ID);
				
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						return(mapping.findForward("arInvoiceImport"));
						
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
 Initialize ArInvoiceImportController EJB
 *******************************************************/
			
			ArInvoiceImportControllerHome homeINV = null;
			ArInvoiceImportController ejbINV = null;
			
			try {
				
				homeINV = (ArInvoiceImportControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArInvoiceImportControllerEJB", ArInvoiceImportControllerHome.class);
				
			} catch(NamingException e) {
				if(log.isInfoEnabled()){
					log.info("NamingException caught in ArInvoiceImportAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				return(mapping.findForward("cmnErrorPage"));
			}
			
			try {
				
				ejbINV = homeINV.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()) {
					
					log.info("CreateException caught in ArInvoiceImportAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ApVoucherImportController EJB
   getGlFcPrecisionUnit
   getInvGpQuantityPrecisionUnit
 *******************************************************/

		        short precisionUnit = 0;
		        short quantityPrecisionUnit = 0;
		        
		        try {
		        	
		        	precisionUnit = ejbINV.getGlFcPrecisionUnit(user.getCmpCode());
		        	quantityPrecisionUnit = ejbINV.getInvGpQuantityPrecisionUnit(user.getCmpCode());
		        	
		        } catch(EJBException ex) {
		        	
		        	if (log.isInfoEnabled()) {
		        		
		        		log.info("EJBException caught in ApVoucherImportAction.execute(): " + ex.getMessage() +
		        				" session: " + session.getId());
		        	}
		        	
		        	return(mapping.findForward("cmnErrorPage"));
		        	
		        }
			
/*******************************************************
	AR Invoice Import IMPORT ACTION
 *******************************************************/			

		if (request.getParameter("importButton") != null &&
	            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            String headerFile = actionForm.getHeaderFile().getFileName();
        	String detailsFile = actionForm.getDetailsFile().getFileName();
        	
        	// get pos header
        	
        	ArrayList headerList = new ArrayList();
        	ArrayList detailsList = new ArrayList();
        	
        	CSVParser csvParser1 = new CSVParser(actionForm.getHeaderFile().getInputStream());
        	CSVParser csvParser2 = new CSVParser(actionForm.getDetailsFile().getInputStream());
        	
        	String[][] header = csvParser1.getAllValues();
        	String[][] details = csvParser2.getAllValues();
        	
        	LinkedHashSet set = new LinkedHashSet();
        	
        	for (int i=1; i < header.length; i++) {
        		
        		ArModInvoiceDetails mdetails = new ArModInvoiceDetails();
                
        		mdetails.setInvCstName(header[i][1].trim());
        		mdetails.setInvDate(Common.convertStringToSQLDate(header[i][0].trim()));
                mdetails.setInvNumber(header[i][2].trim());
                mdetails.setInvReferenceNumber(header[i][3].trim());
                mdetails.setInvVoid(Common.convertBooleanToByte(false));
                mdetails.setInvDescription(header[i][4].trim());           
                mdetails.setInvConversionDate(null);
                mdetails.setInvConversionRate(Common.convertStringMoneyToDouble("1.000000", Constants.MONEY_RATE_PRECISION));
                mdetails.setInvSoNumber(null);  
                mdetails.setInvCreatedBy(user.getUserName());
  	           	mdetails.setInvDateCreated(new java.util.Date());
  	           	mdetails.setInvLastModifiedBy(user.getUserName());
  	           	mdetails.setInvDateLastModified(new java.util.Date());
  	           	mdetails.setInvAmountDue(Common.convertStringMoneyToDouble(header[i][5].trim(), precisionUnit));
  	           	
  	           	headerList.add(mdetails);
  	           	
        	}
        	
        	for (int i=1; i < details.length; i++) {
        		
        		ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();
        		
        		mdetails.setIliInvDocumentNumber(details[i][0].trim());
        		mdetails.setIliIiName(details[i][1].trim());
        		mdetails.setIliIiDescription(details[i][2].trim());
        		mdetails.setIliQuantity(Common.convertStringMoneyToDouble(details[i][3].trim(), quantityPrecisionUnit));           	   
        		mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(details[i][4].trim(), precisionUnit));
        		mdetails.setIliAmount(Common.convertStringMoneyToDouble(details[i][5].trim(), precisionUnit));
        		
        		detailsList.add(mdetails);
        		
        	}
        	
        	Collections.sort(headerList, sortInvByDocumentNumber);
    		Collections.sort(detailsList, sortIliByDocumentNumber);
    		      	
    		ArrayList list = new ArrayList();
    		
    		Iterator hdrItr = headerList.iterator();
    		
    		while(hdrItr.hasNext()) {
    			
    			ArModInvoiceDetails mdetails = (ArModInvoiceDetails)hdrItr.next();
    			
    			ArrayList iliList = new ArrayList();
    			short lineNumber = 0;
				
    			Iterator dtlItr = detailsList.iterator();
    			double TOTAL_AMNT = 0;
    			
    			while (dtlItr.hasNext()) {

    				ArModInvoiceLineItemDetails mlidetails = (ArModInvoiceLineItemDetails)dtlItr.next();
    				
    				if(mlidetails.getIliInvDocumentNumber().equals(mdetails.getInvNumber())) {
    					
    					mlidetails.setIliLine(lineNumber++);
    					iliList.add(mlidetails);
    					
    				}
    				
    			}

    			mdetails.setInvIliList(iliList);
    			
    			list.add(mdetails);
    			
    		}
    		
    		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
    		String invBatch = "IMPORT " + formatter.format(new java.util.Date());
    		
    		if (headerList.isEmpty() || detailsList.isEmpty()) {
    			
    			errors.add(ActionMessages.GLOBAL_MESSAGE,
    					new ActionMessage("invoiceImport.error.noInvoiceToImport"));
    			
    		} else {
    			
    			try {
    				
    				ejbINV.importInvoice(list, "IMMEDIATE", "VAT INCLUSIVE", "NONE", "PHP", invBatch, 
    						new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    				
    			} catch (GlobalDocumentNumberNotUniqueException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("invoiceImport.error.documentNumberNotUnique", ex.getMessage()));
    				
    			} catch (GlobalPaymentTermInvalidException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("invoiceEntry.error.paymentTermInvalid"));  
    				
    			} catch (GlobalInvItemLocationNotFoundException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("invoiceImport.error.noItemLocationFound", ex.getMessage()));
    				
    			} catch (GlobalNoRecordFoundException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("invoiceImport.error.customerNotExist", ex.getMessage()));
    				
    			} catch (GlobalJournalNotBalanceException ex) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
    						new ActionMessage("invoiceImport.error.amountNotBalance", ex.getMessage()));
    				
    			} catch (EJBException ex) {
    				if (log.isInfoEnabled()) {
    					
    					log.info("EJBException caught in ApInvoiceImportAction.execute(): " + ex.getMessage() +
    							" session: " + session.getId());
    				}
    				
    				return(mapping.findForward("cmnErrorPage"));
    			}
    			
    		}
    		
    		csvParser1.close();
        	csvParser2.close();

/*******************************************************
 	AR Invoice Import CLOSE ACTION
 *******************************************************/			
			
		} else if (request.getParameter("closeButton") != null) {
			
			return(mapping.findForward("cmnMain"));
			
		}
		
/*******************************************************
 	AR Invoice Import LOAD ACTION
 *******************************************************/			
		
		if (frParam != null) {
			
			if (!errors.isEmpty()) {
				
				saveErrors(request, new ActionMessages(errors));
				
			} else {
				
				if ((request.getParameter("importButton") != null) && 
						actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
					
					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
					
				}
				
			}
			
			actionForm.reset(mapping, request);
			return(mapping.findForward("arInvoiceImport"));
			
		} else {
			
			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
			saveErrors(request, new ActionMessages(errors));
			
			return(mapping.findForward("cmnMain"));
			
		}

      } catch(Exception e) {			
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in ArInvoiceImportAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			e.printStackTrace();
			return(mapping.findForward("cmnErrorPage"));
			
		}
	}
	
	private static Comparator sortInvByDocumentNumber = new Comparator() {
		
		public int compare(Object r1, Object r2) {
			
		    ArModInvoiceDetails invoice1 = (ArModInvoiceDetails) r1;
		    ArModInvoiceDetails invoice2 = (ArModInvoiceDetails) r2;
			return invoice1.getInvNumber().compareTo(invoice2.getInvNumber());
			
		}
		
	};
	
	private static Comparator sortIliByDocumentNumber = new Comparator() {
		
		public int compare(Object r1, Object r2) {
		    
			ArModInvoiceLineItemDetails invoice1 = (ArModInvoiceLineItemDetails) r1;
			ArModInvoiceLineItemDetails invoice2 = (ArModInvoiceLineItemDetails) r2;
		    
			return invoice1.getIliInvDocumentNumber().compareTo(invoice2.getIliInvDocumentNumber());
			
		}
		
	};
	
}
