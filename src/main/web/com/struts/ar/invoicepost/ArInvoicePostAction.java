package com.struts.ar.invoicepost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ArInvoicePostController;
import com.ejb.txn.ArInvoicePostControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModInvoiceDetails;

public final class ArInvoicePostAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArInvoicePostAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArInvoicePostForm actionForm = (ArInvoicePostForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_INVOICE_POST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arInvoicePost");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArInvoicePostController EJB
*******************************************************/

         ArInvoicePostControllerHome homeIP = null;
         ArInvoicePostController ejbIP = null;

         try {
          
            homeIP = (ArInvoicePostControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArInvoicePostControllerEJB", ArInvoicePostControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArInvoicePostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbIP = homeIP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArInvoicePostAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call ArInvoicePostController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableArInvoiceBatch
     getAdPrfArUseCustomerPulldown
  *******************************************************/

         short precisionUnit = 0;
         boolean enableInvoiceBatch = false; 
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbIP.getGlFcPrecisionUnit(user.getCmpCode());
            enableInvoiceBatch = Common.convertByteToBoolean(ejbIP.getAdPrfEnableArInvoiceBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableInvoiceBatch);
            useCustomerPulldown = Common.convertByteToBoolean(ejbIP.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArInvoicePostAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Ar IP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arInvoicePost"));
	     
/*******************************************************
   -- Ar IP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arInvoicePost"));         

/*******************************************************
   -- Ar IP Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ar IP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Ar IP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();

	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
                
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberFrom())) {
	        		
	        		criteria.put("invoiceNumberFrom", actionForm.getInvoiceNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getInvoiceNumberTo())) {
	        		
	        		criteria.put("invoiceNumberTo", actionForm.getInvoiceNumberTo());
	        		
	        	}
	        	

	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getOrderBy())) {
	        		
	        		criteria.put("orderBy", actionForm.getOrderBy());
	        		
	        	}
	        	        	
	        	if (actionForm.getCreditMemo()) {	        	
		        	   		        		
	        		criteria.put("creditMemo", new Byte((byte)1));
                
                }
                
                if (!actionForm.getCreditMemo()) {
                	
                	criteria.put("creditMemo", new Byte((byte)0));
                	
                }
                	 
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		System.out.println("actionForm.getOrderBy()="+actionForm.getOrderBy());
	        		
	        		ArrayList list = ejbIP.getArInvPostableByCriteria(actionForm.getCriteria(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ArInvoicePostAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	        	
	     	}
            
            try {
            	
            	actionForm.clearArIPList();
            	
            	ArrayList list = ejbIP.getArInvPostableByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
            		
            		ArInvoicePostList arIPList = new ArInvoicePostList(actionForm,
            		    mdetails.getInvCode(),
            		    mdetails.getInvCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),
						mdetails.getInvType());
            		    
            		actionForm.saveArIPList(arIPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("invoicePost.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoicePostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoicePost");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("arInvoicePost")); 

/*******************************************************
   -- Ar IP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar IP Post Action --
*******************************************************/

         } else if (request.getParameter("postButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get posted journals
            
		    for(int i=0; i<actionForm.getArIPListSize(); i++) {
		    
		       ArInvoicePostList actionList = actionForm.getArIPByIndex(i);
		    	
		       if (actionList.getPost()) {
		       	
		       		 try {
               	     	
               	     	ejbIP.executeArInvPost(actionList.getInvoiceCode(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteArIPList(i);
               	     	i--;
               	     	
               	     } catch (GlobalInventoryDateException ex) {
                     	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                 		  new ActionMessage("invoicePost.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
               	     	
               	     } catch (GlJREffectiveDateNoPeriodExistException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoicePost.error.effectiveDateNoPeriod", actionList.getInvoiceNumber()));
		                  
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoicePost.error.effectiveDatePeriodClosed", actionList.getInvoiceNumber()));
		                  
		             } catch (GlobalJournalNotBalanceException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoicePost.error.journalNotBalance", actionList.getInvoiceNumber()));
               	  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoicePost.error.transactionAlreadyPosted", actionList.getInvoiceNumber()));
		             
		             } catch (GlobalTransactionAlreadyVoidException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoicePost.error.transactionAlreadyVoid", actionList.getInvoiceNumber()));
		                  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("invoicePost.error.recordAlreadyDeleted", actionList.getInvoiceNumber()));
		
		           
		    			
		             } catch (GlobalBranchAccountNumberInvalidException ex) {
		             	
		             	errors.add(ActionMessages.GLOBAL_MESSAGE,
		             			new ActionMessage("invoicePost.error.branchAccountNumberInvalid", ex.getMessage()));


		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		            	
		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("invoicePost.error.noNegativeInventoryCostingCOA"));

		             } catch (GlobalExpiryDateNotFoundException ex) {
		            	 
		            	 errors.add(ActionMessages.GLOBAL_MESSAGE,
		            			 new ActionMessage("errors.MiscInvalid", ex.getMessage()));
		            	 
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ArInvoicePostAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	           }
	        }	
				        
			if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoicePost");

            }
	        
	        try {
            	
            	actionForm.setLineCount(0);
            	actionForm.clearArIPList();
            	
            	ArrayList list = ejbIP.getArInvPostableByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
            	    actionForm.getOrderBy(), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModInvoiceDetails mdetails = (ArModInvoiceDetails)i.next();
            		
            		ArInvoicePostList arIPList = new ArInvoicePostList(actionForm,
            		    mdetails.getInvCode(),
            		    mdetails.getInvCstCustomerCode(),            		                		    
                        Common.convertSQLDateToString(mdetails.getInvDate()),
                        mdetails.getInvNumber(),
                        mdetails.getInvReferenceNumber(),
                        Common.convertDoubleToStringMoney(mdetails.getInvAmountDue(), precisionUnit),
						mdetails.getInvType());
            		    
            		actionForm.saveArIPList(arIPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoicePostAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }                
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("arInvoicePost")); 
	            

/*******************************************************
   -- Ar IP Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arInvoicePost");

            }
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseCustomerPulldown()) {
            	    
                	actionForm.clearCustomerCodeList();         
                	
            		list = ejbIP.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerCodeList((String)i.next());
            				
            			}
            			
            		}
            		
            	}

            	actionForm.clearCurrencyList();           	
            	
            	list = ejbIP.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbIP.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	
            	actionForm.clearArIPList();
                        	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
                        
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null || request.getParameter("forPosting") != null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	           if(request.getParameter("forPosting") != null) {
           		
	        	   System.out.println("forPosting");
           		return new ActionForward("/arInvoicePost.do?goButton=1");
           	
           		} 
	          
            }	                       
            return(mapping.findForward("arInvoicePost"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArInvoicePostAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}