package com.struts.ar.invoicepost;

import java.io.Serializable;

public class ArInvoicePostList implements Serializable {

   private Integer invoiceCode = null;
   private String customerCode = null;
   private String date = null;
   private String invoiceNumber = null;
   private String referenceNumber = null;  
   private String amountDue = null;
   private String type;
   
   private boolean post = false;
       
   private ArInvoicePostForm parentBean;
    
   public ArInvoicePostList(ArInvoicePostForm parentBean,
      Integer invoiceCode,
      String customerCode,
      String date,
      String invoiceNumber,
      String referenceNumber,
      String amountDue,
	  String type) {

      this.parentBean = parentBean;
      this.invoiceCode = invoiceCode;;
      this.customerCode = customerCode;
      this.date = date;
      this.invoiceNumber = invoiceNumber;
      this.referenceNumber = referenceNumber;
      this.amountDue = amountDue;
      this.type = type;
      
   }

   public Integer getInvoiceCode() {

      return invoiceCode;

   }
   
   public String getCustomerCode() {

      return customerCode;
      
   }

   public String getDate() {
   
      return date;
      
   }
   
   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }

   public String getAmountDue() {
   
      return amountDue;
   
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }

   public boolean getPost() {
   	
   	  return post;
   	
   }
   
   public void setPost(boolean post) {
   	
   	  this.post = post;
   	
   }
      
   
}