package com.struts.ar.joborderassignment;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArJobOrderAssignmentController;
import com.ejb.txn.ArJobOrderAssignmentControllerHome;
import com.ejb.txn.ArJobOrderEntryController;
import com.ejb.txn.ArJobOrderEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModJobOrderAssignmentDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArPersonelDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ArJobOrderAssignmentAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArJobOrderAssignmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArJobOrderAssignmentForm actionForm = (ArJobOrderAssignmentForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AP_JOB_ORDER_ASSIGNMENT_ID);

         if (frParam != null) {
         	
         	if (frParam.trim().equals(Constants.FULL_ACCESS)) {
         		
         		ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
         		if (!fieldErrors.isEmpty()) {
         			
         			saveErrors(request, new ActionMessages(fieldErrors));
         			
         			return mapping.findForward("arJobOrderAssignment");
         		}
         		
         	}
         	
         	actionForm.setUserPermission(frParam.trim());
         	
         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArJobOrderAssignmentController EJB
*******************************************************/

         ArJobOrderAssignmentControllerHome homeJA = null;
         ArJobOrderAssignmentController ejbJA = null;

         ArJobOrderEntryController ejbJO = null;
         ArJobOrderEntryControllerHome homeJO = null;
         
       

         try {

            homeJA = (ArJobOrderAssignmentControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArJobOrderAssignmentControllerEJB", ArJobOrderAssignmentControllerHome.class);      
            
            homeJO = (ArJobOrderEntryControllerHome)com.util.EJBHomeFactory.
            lookUpHome("ejb/ArJobOrderEntryControllerEJB", ArJobOrderEntryControllerHome.class);
        
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArJobOrderAssignmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {
        	 ejbJO = homeJO.create();
            ejbJA = homeJA.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArJobOrderAssignmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call ArJobOrderAssignment Controller EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
        short jobOrderAssignmentLineNumber = 0;
 
      //   byte useSupplierPulldown = 0;
         
         try {
         	
            precisionUnit = ejbJA.getGlFcPrecisionUnit(user.getCmpCode());
            jobOrderAssignmentLineNumber = ejbJA.getAdPrfArInvoiceLineNumber(user.getCmpCode());
          
       //     useSupplierPulldown = ejbJA.getAdPrfArUseSupplierPulldown(user.getCmpCode());
            
     //       actionForm.setUseSupplierPulldown(Common.convertByteToBoolean(useSupplierPulldown));
            actionForm.setPrecisionUnit(precisionUnit);
            
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArJobOrderAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }	
      
         actionForm.clearSelectedArJASoList();
         
/*******************************************************
   -- Ar JA Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	ArrayList list = new ArrayList();
         	System.out.println("pasok");
         	
         	
         	double joQuantity = Common.convertStringToDouble(actionForm.getQuantity());
         	double jaQuantity = 0;
         	for(int i=0; i<actionForm.getArJAListSize(); i++) {
         		
         		ArJobOrderAssignmentList arJAList = (ArJobOrderAssignmentList)actionForm.getArJAByIndex(i);
         		
         		if(Common.validateRequired(arJAList.getPersonelIdNumber()) ||
         			Common.validateRequired(arJAList.getQuantity()) ||
					Common.validateRequired(arJAList.getAmount())) continue;
         		
         		ArModJobOrderAssignmentDetails mdetails = new ArModJobOrderAssignmentDetails();
         		
         		mdetails.setJaLine(Short.parseShort(arJAList.getLineNumber()));
                mdetails.setJaPeIdNumber(arJAList.getPersonelIdNumber());
                mdetails.setJaQuantity(Common.convertStringMoneyToDouble(arJAList.getQuantity(), precisionUnit));
                mdetails.setJaUnitCost(Common.convertStringMoneyToDouble(arJAList.getUnitPrice(), precisionUnit));
                mdetails.setJaAmount(Common.convertStringMoneyToDouble(arJAList.getAmount(), precisionUnit));
                mdetails.setJaPeName(arJAList.getPersonelName());
                mdetails.setJaRemarks(arJAList.getRemarks());
                mdetails.setJaSo(Common.convertBooleanToByte(arJAList.getSoCheckbox()));
                
                System.out.println("SO checked: " + mdetails.getJaSo() );
                jaQuantity += mdetails.getJaQuantity();
                list.add(mdetails);
                
                if(arJAList.getSoCheckbox() == true) {
                  
                  actionForm.saveSelectedArJASoList(arJAList);
                  
                }
         		
         	}
         	/*
         	if(jaQuantity>joQuantity) {
         		
         		
         
         		
         		  errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("jobOrderAssignment.error.totalQuantityExceedingMaxQuantity"));
                  saveErrors(request, new ActionMessages(errors));
     		
     		   if (!errors.isEmpty()) {

                  saveErrors(request, new ActionMessages(errors));
                  return mapping.findForward("arJobOrderAssignment");

               }
         	}
         	*/
         	try {
         		
         		//save ar job order assignment
         		
         		ejbJA.saveArJbOrdrAssgnmnt(list, new Integer(Integer.parseInt(actionForm.getJobOrderLineCode())), user.getCmpCode());
         		
         	} catch (GlobalRecordAlreadyExistException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
		    			new ActionMessage("jobOrderAssignment.error.personelAlreadyExists", ex.getMessage()));
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ArJobOrderAssignmentAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage"); 
         			
         		}
         		
         	}  
         	       	
/*******************************************************
 -- Ar JA Add Lines Action --
 *******************************************************/
         	
         } else if(request.getParameter("addLinesButton") != null) {
         	
         	int listSize = actionForm.getArJAListSize();
         	
         	for (int x = listSize + 1; x <= listSize + jobOrderAssignmentLineNumber; x++) {
         		
         		ArJobOrderAssignmentList arJAList = new ArJobOrderAssignmentList(actionForm, String.valueOf(x), null, null, null, null, null, null, false);
         		
         		arJAList.setPersonelList(actionForm.getPersonelList());
         		
         		actionForm.saveArJAList(arJAList);
         		
         	}	        
         	
         	return(mapping.findForward("arJobOrderAssignment"));
         	
/*******************************************************
 -- Ar JA Delete Lines Action --
 *******************************************************/
         	
         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getArJAListSize(); i++) {
         		
         		ArJobOrderAssignmentList apCNVList = actionForm.getArJAByIndex(i);
         		
         		if (apCNVList.getDeleteCheckbox()) {
         			
         			actionForm.deleteArJAList(i);
         			i--;
         		}
         		
         	}
         	
         	for (int i = 0; i<actionForm.getArJAListSize(); i++) {
         		
         		ArJobOrderAssignmentList apCNVList = actionForm.getArJAByIndex(i);
         		
         		apCNVList.setLineNumber(String.valueOf(i+1));
         		
         	}
         	
         	return(mapping.findForward("arJobOrderAssignment"));	 
 
/*******************************************************
-- Ar JA Personel Enter Action --
*******************************************************/
         
          } else if(!Common.validateRequired(request.getParameter("arJAList[" + 
            actionForm.getRowSelected() + "].isPersonelEntered"))) {
        	  
        	 	ArJobOrderAssignmentList ArJASoList = actionForm.getArJAByIndex(actionForm.getRowSelected());   	    	
        	 	System.out.println("sdsadas");
        	  
        	 
          	            
          	try {
              	
          		ArPersonelDetails details = ejbJA.getArPrsnlByPeIdNmbr(ArJASoList.getPersonelIdNumber(), user.getCmpCode());
        	 	
          		
          		ArJASoList.setPersonelName(details.getPeName());
          		ArJASoList.setUnitPrice(Common.convertDoubleToStringMoney(details.getPeRate(),precisionUnit));
          		      		  
          		
          		
          	}catch (GlobalNoRecordFoundException ex) {
          		
          		
          		errors.add(ActionMessages.GLOBAL_MESSAGE,
		    			new ActionMessage("jobOrderAssignment.error.personelNotExist", ex.getMessage()));
          		
          		
            } catch (EJBException ex) {

	            if (log.isInfoEnabled()) {
	
	               log.info("EJBException caught in ArJobOrderAssignmentAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	               return mapping.findForward("cmnErrorPage"); 
	               
	            }

            }  
          	
          	if (!errors.isEmpty()) {

	             saveErrors(request, new ActionMessages(errors));
	             return mapping.findForward("arJobOrderAssignment");

          	}
              
    	                               
            return(mapping.findForward("arJobOrderAssignment"));
/*******************************************************
 -- Ar JA Close Action --
 *******************************************************/
         	
         } else if (request.getParameter("closeButton") != null) {
            
            return(mapping.findForward("cmnMain"));
          
/*******************************************************
   -- Ar JA Back Action --
*******************************************************/

     	} else if (request.getParameter("backButton") != null) {
     		
     		String path = null;
     		
     		path = "/arJobOrderEntry.do?forward=1&jobOrderCode=" + actionForm.getJobOrderCode() +"&conversionRate="+actionForm.getConversionRate() +"&currency="+actionForm.getCurrency();
     		
     		return(new ActionForward(path));
     		
/*******************************************************
 -- Ar JA Load Action --
 *******************************************************/
     		
     	}
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arJobOrderAssignment");

            }
            
            actionForm.reset(mapping, request);
            
            try {
            	
        		ArrayList list = new ArrayList();
        		ArrayList prList = new ArrayList();
        		Iterator i = null;
        		Iterator y = null;
        		double amount=0;
        		double qty=0;
        		
        		//personels
        		
        	
        		
            	actionForm.clearPersonelList();           	
            	
            	list = ejbJA.getArPeAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	System.out.println("PERSONEL list.size()="+list.size());
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPersonelList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setPersonelList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	
        		
        		if (request.getParameter("forward") != null) {
        			
        			System.out.println("YES");
        			actionForm.clearArJAList();
        			
        			int jolCode = Integer.parseInt(request.getParameter("jobOrderLineCode")); 
        			System.out.println("jobOrderLineCode" + request.getParameter("jobOrderLineCode"));
        			
        			ArModJobOrderLineDetails jolDetails = ejbJA.getArJolByJolCode(new Integer(jolCode), user.getCmpCode());
        			
        			
        			System.out.println("jolDetails.getJolJoNumber()="+jolDetails.getJolJoDocumentNumber());
        			actionForm.setDocumentNumber(jolDetails.getJolJoDocumentNumber());
	       			actionForm.setDate(Common.convertSQLDateToString(jolDetails.setJolJoDate()));
	       			actionForm.setItemName(jolDetails.getJolIiName());
	       			actionForm.setItemDescription(jolDetails.getJolIiDescription());
	       			actionForm.setLocation(jolDetails.getJolLocName());
	       			actionForm.setUomName(jolDetails.getJolUomName());
	       			actionForm.setQuantity(Common.convertDoubleToStringMoney(jolDetails.getJolQuantity(), precisionUnit));
	       			actionForm.setJobOrderCode(request.getParameter("jobOrderCode"));
	       			actionForm.setJobOrderLineCode(request.getParameter("jobOrderLineCode"));
	       			actionForm.setPosted(request.getParameter("posted"));
	       			actionForm.setJobOrderAssignmentApprovalStatus(request.getParameter("jobOrderAssignmentApprovalStatus"));
	       			actionForm.setConversionRate(request.getParameter("conversionRate"));
	       			actionForm.setCurrency(request.getParameter("currency"));
	       			
        			list = jolDetails.getJaList();
        			
        			i = list.iterator();
        			
        			        			
        			while(i.hasNext()) {
        				
        				ArModJobOrderAssignmentDetails mdetails = (ArModJobOrderAssignmentDetails)i.next();
        				
        				
        				
        				ArJobOrderAssignmentList arJAList = new ArJobOrderAssignmentList(actionForm, String.valueOf(mdetails.getJaLine()), mdetails.getJaPeIdNumber(), 
        						Common.convertDoubleToStringMoney(mdetails.getJaQuantity(), precisionUnit), 
								Common.convertDoubleToStringMoney(mdetails.getJaUnitCost(), precisionUnit), 
								Common.convertDoubleToStringMoney(mdetails.getJaAmount(), precisionUnit),
								mdetails.getJaPeName(), mdetails.getJaRemarks(),
								Common.convertByteToBoolean(mdetails.getJaSo()));

        			
        				arJAList.setPersonelList(actionForm.getPersonelList());
        				actionForm.saveArJAList(arJAList);
        				
        			}
        			/*
        			 if(list.size() == 0){
 			        	System.out.println("list.size()" + list.size());
 			        	ApModPurchaseRequisitionDetails prldetails = ejbPR.getApPrByPrCode(Common.convertStringToInteger(request.getParameter("purchaseRequisitionCode")), user.getCmpCode());
 	            		
 	        			prList = prldetails.getPrPrlList();
 	        			y = prList.iterator();
 	        			System.out.println("prListsize()" + prList.size());
 						while (y.hasNext()) {
 	            			
 	            			ApModPurchaseRequisitionLineDetails mPrlDetails = (ApModPurchaseRequisitionLineDetails)y.next();
 	            			System.out.println("mPrlDetails.getPrlAmount() = "+mPrlDetails.getPrlAmount()+"mPrlDetails.getPrlQuantity()="+mPrlDetails.getPrlQuantity());
 	            			amount=mPrlDetails.getPrlAmount();
 	            			qty=mPrlDetails.getPrlQuantity();
 						}
 						
 								        	
 			        }*/
        			 
	       			int remainingList = jobOrderAssignmentLineNumber - (list.size() % jobOrderAssignmentLineNumber) + list.size();
			        System.out.println("remainingList = "+remainingList + "list.size() = "+list.size()+"jobOrderAssignmentLineNumber = "+jobOrderAssignmentLineNumber);
			        
			        for (int x = list.size() + 1; x <= remainingList; x++) {
			        	System.out.println("weee2");
			        	ArJobOrderAssignmentList arJAList = new ArJobOrderAssignmentList(actionForm, String.valueOf(x), null, actionForm.getQuantity(), Common.convertDoubleToStringMoney(0, precisionUnit),Common.convertDoubleToStringMoney(0, precisionUnit), null, null, false);
			        	
			        	arJAList.setPersonelList(actionForm.getPersonelList());
				        actionForm.saveArJAList(arJAList);
				        			        	
			        }
			        
			       
	       			
	       		}
        		System.out.println("actionForm.getSelectedArJASoListSize()="+actionForm.getSelectedArJASoListSize());
        		
        		
        		for(int a=0; a<actionForm.getSelectedArJASoListSize(); a++) {
        			
        			ArJobOrderAssignmentList selectedCnvPo = actionForm.getSelectedArJASoByIndex(a);
        			
        		}
        		
        		for(int j=0; j<actionForm.getSelectedArJASoListSize(); j++) {
        			
        			ArJobOrderAssignmentList selectedCnvPo = actionForm.getSelectedArJASoByIndex(j);
        			
        			for(int k=0; k<actionForm.getArJAListSize(); k++) {
						
						ArJobOrderAssignmentList apCnvList = (ArJobOrderAssignmentList)actionForm.getArJAByIndex(k);
						
						if(selectedCnvPo.getLineNumber().equals(apCnvList.getLineNumber())) {
							
							apCnvList.setSoCheckbox(true);
							break;
							
						}
						
					}
        			
        		}
        		
    			this.setFormProperties(actionForm);    	
    			
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArJobOrderAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {//from update to saveButton
            	
               if ((request.getParameter("saveButton") != null) && 
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            return(mapping.findForward("arJobOrderAssignment"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArJobOrderAssignmentAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          
          e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

       }

    }
   
//   private void setFormProperties(ApCanvassForm actionForm) {
//   	
//   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
//   		
//   		if (actionForm.getEnableFields() == true) {
//   			
//   			actionForm.setEnableFields(true);
//   			actionForm.setShowAddLinesButton(true);
//   			actionForm.setShowDeleteLinesButton(true);
//   			
//   		} else {
//   			
//   			actionForm.setShowAddLinesButton(false);
//   			actionForm.setShowDeleteLinesButton(false);
//   			
//   		}
//   		
//   	} else {
//   		
//   		actionForm.setShowAddLinesButton(false);
//   		actionForm.setShowDeleteLinesButton(false);
//   		
//   	}
//   	
//   }
   
   private void setFormProperties(ArJobOrderAssignmentForm actionForm) {
   	
   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
   		
   		System.out.println("actionForm.getJobOrderAssignmentApprovalStatus()="+actionForm.getJobOrderAssignmentApprovalStatus());
   		if(actionForm.getJobOrderVoid()) {
   			actionForm.setEnableFields(false);
   			actionForm.setEnableSoCheckbox(false);
   			actionForm.setShowSaveButton(false);
   			actionForm.setShowAddLinesButton(false);
   			actionForm.setShowDeleteLinesButton(false);
   		}
   		
   		else if(actionForm.getJobOrderPosted()) {
   			
   			actionForm.setEnableFields(false);
	   			actionForm.setEnableSoCheckbox(false);
	   			actionForm.setShowSaveButton(false);
	   			actionForm.setShowAddLinesButton(false);
	   			actionForm.setShowDeleteLinesButton(false);
   	   		
   		}else {

   			actionForm.setEnableFields(true);
   			actionForm.setEnableSoCheckbox(true);
   			actionForm.setShowSaveButton(true);
   			actionForm.setShowAddLinesButton(true);
   			actionForm.setShowDeleteLinesButton(true);
   
   	
   		}
   		}else {

		actionForm.setEnableFields(false);
		actionForm.setEnableSoCheckbox(false);
		actionForm.setShowSaveButton(false);
		actionForm.setShowAddLinesButton(false);
		actionForm.setShowDeleteLinesButton(false);
	
	
		}
   	
   }

}