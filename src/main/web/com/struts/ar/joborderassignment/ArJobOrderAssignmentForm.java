package com.struts.ar.joborderassignment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.EJBCommon;

public class ArJobOrderAssignmentForm extends ActionForm implements Serializable {
	
	private short precisionUnit = 0;
	private String jobOrderCode = null;
	private boolean jobOrderVoid = false;
	private boolean jobOrderPosted = false;
	private String jobOrderLineCode = null;
	private String documentNumber = null;
	private String date = null;
	private String itemName = null;
	private String itemDescription = null;

	private String location = null;
	private String uomName = null;
	private String quantity = null;
	private String conversionRate = null;
	private String currency = null;
	private ArrayList personelList = new ArrayList();
	
	
	private String txnStatus = null;
	private String userPermission = new String();
	private int rowSelected = 0;
	private String pageState = new String();

	private short quantityPrecisionUnit = 0;
	private String posted = null;
	private String jobOrderAssignmentApprovalStatus = null;
	
	private boolean enableFields = false;
	private boolean enableSoCheckbox = false;
	private String backButton = null;
	private String closeButton = null;
	private boolean showSaveButton = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	
	private String isPersonelEntered = null;
	
	private ArrayList arJAList = new ArrayList();
	private int arJAListSize = 0;
	private ArrayList selectedArJASoList = new ArrayList();
	
	public short getPrecisionUnit() {
		
		return precisionUnit;
		
	}
	
	public void setPrecisionUnit(short precisionUnit) {
		
		this.precisionUnit = precisionUnit;
		
	}
	
	public String getJobOrderCode() {
		
		return jobOrderCode;
		
	}
	
	public void setJobOrderCode(String jobOrderCode) {
		
		this.jobOrderCode = jobOrderCode;
		
	}
	
	
	public boolean getJobOrderVoid() {
		
		return jobOrderVoid;
		
	}
	
	public void setJobOrderVoid(boolean jobOrderVoid) {
		
		this.jobOrderVoid = jobOrderVoid;
		
	}
	
	public boolean getJobOrderPosted() {
		
		return jobOrderPosted;
		
	}
	
	public void setJobOrderPosted(boolean jobOrderPosted) {
		
		this.jobOrderPosted = jobOrderPosted;
		
	}
	
	
	public String getJobOrderLineCode() {
		
		return jobOrderLineCode;
		
	}
	
	public void setJobOrderLineCode(String jobOrderLineCode) {
		
		this.jobOrderLineCode = jobOrderLineCode;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public String getItemName() {
		
		return itemName;
		
	}
	
	public void setItemName(String itemName) {
		
		this.itemName = itemName;
		
	}
	
	public String getItemDescription() {
		
		return itemDescription;
		
	}
	
	public void setItemDescription(String itemDescription) {
		
		this.itemDescription = itemDescription;
		
	}
	
	
	
	public String getLocation() {
		
		return location;
		
	}
	
	public void setLocation(String location) {
		
		this.location = location;
		
	}
	
	public String getUomName() {
		
		return uomName;
		
	}
	
	public void setUomName(String uomName) {
		
		this.uomName = uomName;
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	public ArrayList getPersonelList() {
		
		return personelList;
		
	}
	
	public void setPersonelList(String personel) {
		
		personelList.add(personel);
		
	}
	
	public void clearPersonelList() {
		
		personelList.clear();
		personelList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	public String getIsPersonelEntered() {
		return isPersonelEntered;
	}
	
	public ArJobOrderAssignmentList getArJAByIndex(int index) {
		
		return((ArJobOrderAssignmentList)arJAList.get(index));
		
	}
	
	public Object[] getArJAList() {
		
		return arJAList.toArray();
		
	}
	
	public int getArJAListSize() {
		
		arJAListSize = arJAList.size();
		return arJAList.size();
		
	}
	
	public void saveArJAList(Object newArJAList) {
		
		arJAList.add(newArJAList);
		
	}
	
	public void deleteArJAList(int rowSelected){
		
		arJAList.remove(rowSelected);
		
	}
	
	public void clearArJAList() {
		
		arJAList.clear();
		
	}
	
	public ArJobOrderAssignmentList getSelectedArJASoByIndex(int index) {
		
		return((ArJobOrderAssignmentList)selectedArJASoList.get(index));
		
	}
	
	public Object[] getSelectedArJASoList() {
		
		return selectedArJASoList.toArray();
		
	}
	
	public int getSelectedArJASoListSize() {
		
		return selectedArJASoList.size();
		
	}
	
	public void saveSelectedArJASoList(Object newSelectedArJASoList) {
		
		selectedArJASoList.add(newSelectedArJASoList);
		
	}
	
	public void deleteSelectedArJASoList(int rowSelected){
		
		selectedArJASoList.remove(rowSelected);
		
	}
	
	public void clearSelectedArJASoList() {
		
		selectedArJASoList.clear();
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public void setRowSelected(Object selectedArJAList, boolean isEdit) {
		
		this.rowSelected = arJAList.indexOf(selectedArJAList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	

	
	public short getQuantityPrecisionUnit() {
		
		return quantityPrecisionUnit;
		
	}
	
	public void setQuantityPrecisionUnit(short quantityPrecisionUnit) {
		
		this.quantityPrecisionUnit = quantityPrecisionUnit;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public String getConversionRate() {
		
		return conversionRate;
		
	}
	
	public void setConversionRate(String conversionRate) {
		
		this.conversionRate = conversionRate;
		
	}
	
	public String getCurrency() {
		
		return currency;
		
	}
	
	public void setCurrency(String currency) {
		
		this.currency = currency;
		
	}
	
	public String getJobOrderAssignmentApprovalStatus() {
			
			return jobOrderAssignmentApprovalStatus;
			
	}
	
	public void setJobOrderAssignmentApprovalStatus(String jobOrderAssignmentApprovalStatus) {
		
		this.jobOrderAssignmentApprovalStatus = jobOrderAssignmentApprovalStatus;
		
	}
	
	
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public boolean getEnableSoCheckbox() {
		
		return enableSoCheckbox;
		
	}
	
	public void setEnableSoCheckbox(boolean enableSoCheckbox) {
		
		this.enableSoCheckbox = enableSoCheckbox;
		
	}
	
	public void setBackButton(String backButton) {
		
		this.backButton = backButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getShowAddLinesButton() {
		
		return showAddLinesButton;
		
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		
		this.showAddLinesButton = showAddLinesButton;
		
	}
	
	public boolean getShowDeleteLinesButton() {
		
		return showDeleteLinesButton;
		
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		
		this.showDeleteLinesButton = showDeleteLinesButton;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for(int i=0; i<arJAList.size(); i++) {
			
			ArJobOrderAssignmentList actionList = (ArJobOrderAssignmentList)arJAList.get(i);
			actionList.setSoCheckbox(false);
			
		}
		
		backButton = null;
		closeButton = null;
		
		isPersonelEntered = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if(request.getParameter("saveButton") != null) {
			
			double total = 0d;
			int numOfLines = 0;
			
			Iterator i = arJAList.iterator();
			
			while(i.hasNext()) {
				
				ArJobOrderAssignmentList jaList = (ArJobOrderAssignmentList)i.next();
				System.out.println("cb:  "+ jaList.getSoCheckbox());
				if(jaList.getSoCheckbox() == false || Common.validateRequired(jaList.getPersonelIdNumber()) || Common.validateRequired(jaList.getQuantity()) ||
					Common.validateRequired(jaList.getAmount())) {
					System.out.println("why?");
					continue;
					
				}
				
				
				
				if(Common.validateRequired(jaList.getPersonelIdNumber())) {
					errors.add("personel",
				               new ActionMessage("jobOrderAssignment.error.personelRequired", jaList.getLineNumber()));
				}
				
				if(Common.validateRequired(jaList.getQuantity())) {
					errors.add("quantity",
				               new ActionMessage("jobOrderAssignment.error.quantityRequired", jaList.getLineNumber()));
				}
				
				if(!Common.validateNumberFormat(jaList.getQuantity())) {
					errors.add("quantity",
				               new ActionMessage("jobOrderAssignment.error.quantityInvalid", jaList.getLineNumber()));
				}
				
				if(Common.validateRequired(jaList.getUnitPrice())) {
					errors.add("unitPrice",
				               new ActionMessage("jobOrderAssignment.error.unitPriceRequired", jaList.getLineNumber()));
				}
				
				if(!Common.validateMoneyFormat(jaList.getUnitPrice())) {
					errors.add("unitPrice",
				               new ActionMessage("jobOrderAssignment.error.unitPriceInvalid", jaList.getLineNumber()));
				}
				
				numOfLines++;
				
				total += Common.convertStringMoneyToDouble(jaList.getQuantity(), quantityPrecisionUnit);
				
			}
			
			System.out.println("--------------------numOfLines="+numOfLines);
			if(numOfLines == 0) {
				
				errors.add("apJAList",
			               new ActionMessage("jobOrderAssignment.error.jobOrderAssignmentMustHaveAtLeastOneLine"));
				
			}
			/*
			if(total <= 0) {
				
				errors.add("apJAList",
			               new ActionMessage("jobOrderAssignment.error.negativeOrZeroQuantityNotAllowed"));
				
			}
			*/
			
		}
		
		return errors;
		
	}
	
}