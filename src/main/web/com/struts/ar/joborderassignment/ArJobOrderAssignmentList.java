package com.struts.ar.joborderassignment;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.util.Constants;

public class ArJobOrderAssignmentList implements Serializable {
	
	private String lineNumber = null;
	private String personelIdNumber = null;
	private ArrayList personelList = new ArrayList();
	private String quantity = null;
	private String unitPrice = null;
	private String amount = null;
	private boolean deleteCheckBox = false;
	private boolean soCheckbox = false;
	private String personelName = null;
	private String remarks = null;
	
	private String personelButton = null;
	
	private ArJobOrderAssignmentForm parentBean = null;
	
	public ArJobOrderAssignmentList(ArJobOrderAssignmentForm parentBean, String lineNumber, String personelIdNumber, String quantity, 
			String unitPrice, String amount,String personelName, String remarks, boolean soCheckbox) {
		
		this.parentBean = parentBean;
		this.lineNumber = lineNumber;
		this.personelIdNumber = personelIdNumber;
		this.quantity = quantity;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.personelName = personelName;
		this.remarks = remarks;
		this.soCheckbox = soCheckbox;
		
	}
	
	public void setPersonelButton(String personelButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	
	public String getPersonelName() {
	   	
	   	return personelName;
	   	
	  }
	   
	public void setPersonelName(String personelName) {
   	
		this.personelName = personelName;
   	
  	}
	
	public String getRemarks() {
	   	
	   	return remarks;
	   	
	  }
	   
	public void setRemarks(String remarks) {
   	
		this.remarks = remarks;
   	
  	}
	
	public String getLineNumber() {
		
		return lineNumber;
		
	}
	
	public void setLineNumber(String lineNumber) {
		
		this.lineNumber = lineNumber;
		
	}
	
	public String getPersonelIdNumber() {
		
		return personelIdNumber;
		
	}
	
	public void setPersonelIdNumber(String personelIdNumber) {
		
		this.personelIdNumber = personelIdNumber;
		
	}
	
	public ArrayList getPersonelList() {
		
		return personelList;
		
	}
	
	public void setPersonelList(ArrayList personelList) {
		
		this.personelList = personelList;
		
	}
	
	public void clearPersonelList() {
		
		personelList.clear();
		personelList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getQuantity() {
		
		return quantity;
		
	}
	
	public void setQuantity(String quantity) {
		
		this.quantity = quantity;
		
	}
	
	public String getUnitPrice() {
		
		return unitPrice;
		
	}
	
	public void setUnitPrice(String unitPrice) {
		
		this.unitPrice = unitPrice;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
	public boolean getDeleteCheckbox() {
		
		return deleteCheckBox;
		
	}
	
	public void setDeleteCheckbox(boolean deleteCheckBox) {
		
		this.deleteCheckBox = deleteCheckBox;
		
	}
	
	public boolean getSoCheckbox() {
		
		return soCheckbox;
		
	}
	
	public void setSoCheckbox(boolean soCheckbox) {
		
		this.soCheckbox = soCheckbox;
		
	}
	
	
}