	package com.struts.ar.joborderentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.ArInvDuplicateUploadNumberException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.AdLogController;
import com.ejb.txn.AdLogControllerHome;
import com.ejb.txn.ArJobOrderEntryController;
import com.ejb.txn.ArJobOrderEntryControllerHome;
import com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionEntryList;
import com.struts.ar.invoiceentry.ArInvoiceEntryList;
import com.struts.ar.invoiceentry.ArInvoiceLineItemList;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.struts.util.ReportParameter;
import com.util.ApPurchaseRequisitionDetails;
import com.util.ArInvoiceDetails;
import com.util.ArJobOrderDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;

import com.util.ArModJobOrderDetails;
import com.util.ArModJobOrderLineDetails;
import com.util.ArModJobOrderTypeDetails;
import com.util.ArSalespersonDetails;
import com.util.ArTaxCodeDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public class ArJobOrderEntryAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	    HttpSession session = request.getSession();
  
	   
	    
	    try {

/*******************************************************
    Check if user has a session
 *******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);

          if (user != null) {

             if (log.isInfoEnabled()) {

                 log.info("ArJobOrderEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                    "' performed this action on session " + session.getId());

             }

          }else{

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

             }

             return(mapping.findForward("adLogon"));

          }

          ArJobOrderEntryForm actionForm = (ArJobOrderEntryForm)form;

          actionForm.setReport(null);
          actionForm.setAttachment(null);
          actionForm.setAttachmentPDF(null);
     
       
 	      String frParam = null;

          frParam = Common.getUserPermission(user, Constants.AR_JOB_ORDER_ENTRY_ID);

          if (frParam != null) {

 	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

 	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

                if (!fieldErrors.isEmpty()) {

                   saveErrors(request, new ActionMessages(fieldErrors));
                   return(mapping.findForward("arJobOrderEntry"));

                }

             }

             actionForm.setUserPermission(frParam.trim());

          } else {

             actionForm.setUserPermission(Constants.NO_ACCESS);

          }

 /*******************************************************
    Initialize ArJobOrderEntryController EJB
 *******************************************************/

              ArJobOrderEntryControllerHome homeJO = null;
              ArJobOrderEntryController ejbJO = null;

              AdLogControllerHome homeLOG = null;
              AdLogController ejbLOG = null;
              try {

            	  homeJO = (ArJobOrderEntryControllerHome)com.util.EJBHomeFactory.
                     lookUpHome("ejb/ArJobOrderEntryControllerEJB", ArJobOrderEntryControllerHome.class);
            	  
            	  homeLOG = (AdLogControllerHome)com.util.EJBHomeFactory.
                          lookUpHome("ejb/AdLogControllerEJB", AdLogControllerHome.class);


              } catch(NamingException e) {

              	if(log.isInfoEnabled()){
                     log.info("NamingException caught in ArJobOrderEntryAction.execute(): " + e.getMessage() +
                    " session: " + session.getId());
                 }
                 return(mapping.findForward("cmnErrorPage"));

              }

              try {

                 ejbJO = homeJO.create();
                 
                 ejbLOG = homeLOG.create();

              } catch(CreateException e) {

                 if(log.isInfoEnabled()) {

                     log.info("CreateException caught in ArJobOrderEntryAction.execute(): " + e.getMessage() +
                        " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));

              }

              ActionErrors errors = new ActionErrors();
              ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ArJobOrderEntryController EJB
   getGlFcPrecisionUnit
   getInvGpprecisionUnitUnit
   getAdPrfArJournalLineNumber
   getAdPrfArUseCustomerPulldown
   getAdPrfArEnablePaymentTerm
*******************************************************/

              MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

              short precisionUnit = 0;

              short journalLineNumber = 0;
              boolean isInitialPrinting = false;
              boolean useCustomerPulldown = true;
              boolean enablePaymentTerm = true;
              boolean disableSalesPrice = false;
              String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ar/Job Order/";
              long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
              String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
              String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
              try {


                 precisionUnit = ejbJO.getGlFcPrecisionUnit(user.getCmpCode());
                 journalLineNumber = ejbJO.getAdPrfArJournalLineNumber(user.getCmpCode());
                 useCustomerPulldown = Common.convertByteToBoolean(ejbJO.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
                 actionForm.setUseCustomerPulldown(useCustomerPulldown);
                 enablePaymentTerm = Common.convertByteToBoolean(ejbJO.getAdPrfArEnablePaymentTerm(user.getCmpCode()));
                 actionForm.setEnablePaymentTerm(enablePaymentTerm);
                 disableSalesPrice = Common.convertByteToBoolean(ejbJO.getAdPrfArDisableSalesPrice(user.getCmpCode()));
                 actionForm.setDisableSalesPrice(disableSalesPrice);
                 actionForm.setPrecisionUnit(precisionUnit);

              } catch(EJBException ex) {

                 if (log.isInfoEnabled()) {

                    log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
                       " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));

              }
              
         
              
              for(Integer x=0;x<actionForm.getArJOLListSize();x++) {
            	  String indexNumber = request.getParameter("arJOLList[" +
                          x + "].personelButton");
            	  
            	System.out.println("row:" + indexNumber);
            	  if(indexNumber != null) {
            		  actionForm.setRowSelected(actionForm.getArJOLByIndex(x), false);
            		  break;
            	  }
              }
              
            
              
              
              System.out.println("row selected is: " +  actionForm.getRowSelected());
              
              
              
              /*******************************************************
              -- Ar Job Order Assignment  Action --
              *******************************************************/
                       if(request.getParameter("arJOLList[" +
                               actionForm.getRowSelected() + "].personelButton") != null) {
                      		System.out.println("pasok 1");

                      		if( actionForm.getArJOLByIndex(actionForm.getRowSelected()).getItemName().equals("")) {
                      			
                      			errors.add(ActionMessages.GLOBAL_MESSAGE,
                                  		new ActionMessage("jobOrderEntry.error.itemIsRequired"));
                      			System.out.println("item error");
                          		saveErrors(request, new ActionMessages(errors));
              	   	   		   return (mapping.findForward("arJobOrderEntry"));
                      		}
                      		
                      	
                      		
                      	 if(Common.validateRequired(actionForm.getApprovalStatus())) {
                      		 
                      		ArModJobOrderDetails details = new ArModJobOrderDetails();
                      		
                      		System.out.println("pasok 2");
                      		
                      		details.setJoType(actionForm.getJoType());
                      		  details.setJoCode(actionForm.getJobOrderCode());
                                details.setJoDate(Common.convertStringToSQLDate(actionForm.getDate()));
                                details.setJoDocumentNumber(actionForm.getDocumentNumber());
                                details.setJoReferenceNumber(actionForm.getReferenceNumber());
                                details.setJoDescription(actionForm.getDescription());
                                details.setJoVoid(Common.convertBooleanToByte(actionForm.getJobOrderVoid()));
                  
                                details.setJoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                                details.setJoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.CONVERSION_RATE_PRECISION));

                                if (actionForm.getJobOrderCode() == null) {

                              	  details.setJoCreatedBy(user.getUserName());
                              	  details.setJoDateCreated(new java.util.Date());

                                }

                                details.setJoLastModifiedBy(user.getUserName());
                                details.setJoDateLastModified(new java.util.Date());
                                details.setJoJobOrderStatus(actionForm.getJobOrderStatus());
                                
                                ArrayList jolList = new ArrayList();
                                

                          	  for (int i = 0; i<actionForm.getArJOLListSize(); i++) {
                          		
                          		  ArJobOrderLineList arJOLList = actionForm.getArJOLByIndex(i);
                          	

                               	   if (Common.validateRequired(arJOLList.getLocation()) &&
                               	   	   Common.validateRequired(arJOLList.getItemName()) &&
                 	   				   Common.validateRequired(arJOLList.getUnit()) &&
                 	   				   Common.validateRequired(arJOLList.getUnitPrice()) &&
                 	   				   Common.validateRequired(arJOLList.getQuantity())) continue;

                               	   ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();


                 					   String itemDesc = arJOLList.getItemDescription();


                 					mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
                                  mdetails.setJolIiName(arJOLList.getItemName());
                                  mdetails.setJolLocName(arJOLList.getLocation());
                                  mdetails.setJolLineIDesc(itemDesc);
                                  mdetails.setJolIiDescription(arJOLList.getItemDescription());
                                  mdetails.setJolQuantity(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
                                  mdetails.setJolUomName(arJOLList.getUnit());
                                  mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
                                  mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
                                    mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
                                    mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
                                    mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
                                    mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
                                    mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
                                    mdetails.setJolMisc(arJOLList.getMisc());
                                    mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));

                                    mdetails.setJaList(arJOLList.getArJobOrderAssignmentList());

                                    ArrayList tagList = new ArrayList();
                                	   //TODO:save and submit taglist
                                	   boolean isTraceMisc = ejbJO.getJoTraceMisc(arJOLList.getItemName(), user.getCmpCode());
                     	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
                     				String misc= arJOLList.getMisc();
              	
              	               	   if (isTraceMisc == true){
              	
              	
              	               		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
              	               		   mdetails.setJolTagList(tagList);
              	
              	
              	               	   }


                                	   jolList.add(mdetails);
                          		
                          	  }
                          	   //validate attachment
                                String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
                                
                               
                               
                      	
                      	 
                          	    try {

                               	    Integer jobOrderCode = ejbJO.saveArJoEntry(details, actionForm.getPaymentTerm(),
                               	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), jolList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                               	    	actionForm.setJobOrderCode(jobOrderCode);
                               	    	
                               	    	 ejbLOG.addAdLogEntry(jobOrderCode,"AR JOB ORDER ENTRY",  new java.util.Date(),user.getUserName(),"SAVE AS DRAFT", actionForm.getLogEntry(),  user.getCmpCode());
                      	            	 

              	                 } catch (GlobalRecordAlreadyDeletedException ex) {
              	
              	                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                          new ActionMessage("jobOrderEntry.error.recordAlreadyDeleted"));
              	
              	                 } catch (GlobalDocumentNumberNotUniqueException ex) {
              	
              	                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                          new ActionMessage("jobOrderEntry.error.documentNumberNotUnique"));
              	
              	                 } catch (GlobalConversionDateNotExistException ex) {
              	
              	                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                          new ActionMessage("jobOrderEntry.error.conversionDateNotExist"));
              	
              	                 } catch (GlobalPaymentTermInvalidException ex) {
              	
              	                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                          new ActionMessage("jobOrderEntry.error.paymentTermInvalid"));
              	
              	                 } catch (GlobalTransactionAlreadyPendingException ex) {
              	
              	                       errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                          new ActionMessage("jobOrderEntry.error.transactionAlreadyPending"));
              	
              	                 } catch (GlobalTransactionAlreadyVoidException ex) {
              	
              	                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                          new ActionMessage("jobOrderEntry.error.transactionAlreadyVoid"));
              	
              	                 } catch (GlobalInvItemLocationNotFoundException ex) {
              	
              	                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                          new ActionMessage("jobOrderEntry.error.noItemLocationFound", ex.getMessage()));
              	
              	                 } catch (GlobalNoApprovalApproverFoundException ex) {
              	
              	                  	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                           new ActionMessage("jobOrderEntry.error.noApprovalApproverFound"));
              	
              	                 } catch (GlobalNoApprovalRequesterFoundException ex) {
              	
              	                 		errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                 			new ActionMessage("jobOrderEntry.error.noApprovalRequesterFound"));
              	
              	                 } catch (GlobalRecordAlreadyAssignedException ex) {
              	
              	                 		errors.add(ActionMessages.GLOBAL_MESSAGE,
              	                 			new ActionMessage("jobOrderEntry.error.transactionIsAlreadyAssigned"));
              	
              	
              	                 } catch (EJBException ex) {
              	                 	    if (log.isInfoEnabled()) {
              	
              	                        log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
              	                           " session: " + session.getId());
              	                      }
              	
              	                     return(mapping.findForward("cmnErrorPage"));
              	                 }
              	
              	                 if (!errors.isEmpty()) {
              	
              	   	   	    	   saveErrors(request, new ActionMessages(errors));
              	   	   	   		   return (mapping.findForward("arJobOrderEntry"));
              	
              	      	          }
              	                 
              	                 
              	                 
              	             

              	        	 
                      	 }
                      	 
                      	 
                      	//get job order line code

                         	ArJobOrderLineList arJOLList = actionForm.getArJOLByIndex(actionForm.getRowSelected());

                         	Integer jolCode = null;

                         	try {

                         		jolCode = ejbJO.getArJolCodeByJolLineAndJoCode(Short.parseShort(arJOLList.getLineNumber()),
                         				actionForm.getJobOrderCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                         	} catch (EJBException ex) {
                         		if (log.isInfoEnabled()) {

                         			log.info("EJBException caught in ArJobOrderAssignmentAction.execute(): " + ex.getMessage() +
                         					" session: " + session.getId());
                         		}

                         		return(mapping.findForward("cmnErrorPage"));
                         	}

                         	String path = "/arJobOrderAssignment.do?forward=1" +
                 				"&jobOrderLineCode=" + String.valueOf(jolCode.intValue()) +
                 				"&jobOrderCode=" + String.valueOf(actionForm.getJobOrderCode()) +
                				"&posted=" + actionForm.getPosted() + "&conversionRate="+actionForm.getConversionRate() +"&currency="+actionForm.getCurrency();

                         	System.out.print(path);
                         	return(new ActionForward(path));
                      	 
                      	 
                      	 
                      	 
                      	 
              
              
              
/*******************************************************
      -- Ar SO Save As Draft Action ITEMS --
*******************************************************/

           } else if (request.getParameter("saveAsDraftButton") != null && /*actionForm.getType().equals("ITEMS") &&*/
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	ArModJobOrderDetails details = new ArModJobOrderDetails();

            	System.out.println("save as draft");
            	details.setJoType(actionForm.getJoType());
            	details.setJoCode(actionForm.getJobOrderCode());
                details.setJoDate(Common.convertStringToSQLDate(actionForm.getDate()));
                details.setJoDocumentType(actionForm.getDocumentType());
                details.setJoDocumentNumber(actionForm.getDocumentNumber());
                details.setJoReferenceNumber(actionForm.getReferenceNumber());
                details.setJoTransactionType(actionForm.getTransactionType());
                details.setJoDescription(actionForm.getDescription());
                details.setJoVoid(Common.convertBooleanToByte(actionForm.getJobOrderVoid()));
                details.setJoMobile(Common.convertBooleanToByte(actionForm.getJobOrderMobile()));

                details.setJoMemo(actionForm.getMemo());
                details.setJoJobOrderStatus(actionForm.getJobOrderStatus());
                details.setJoBillTo(actionForm.getBillTo());
                details.setJoShipTo(actionForm.getShipTo());

                details.setJoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                details.setJoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

                if (actionForm.getJobOrderCode() == null) {

                   details.setJoCreatedBy(user.getUserName());
                   details.setJoDateCreated(new java.util.Date());

                }

                details.setJoLastModifiedBy(user.getUserName());
                details.setJoDateLastModified(new java.util.Date());

                details.setJoPytName(actionForm.getPaymentTerm());
                details.setJoTcName(actionForm.getTaxCode());
                details.setJoFcName(actionForm.getCurrency());
                details.setJoCstCustomerCode(actionForm.getCustomer());
                details.setJoSlpSalespersonCode(actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_BLANK) ||
                actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_NO_RECORD_FOUND) ? null :
                actionForm.getSalesperson());
                details.setJoTechnician(actionForm.getTechnician());
              details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));


              ArrayList solList = new ArrayList();

              for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

            	  ArJobOrderLineList arJOLList = actionForm.getArJOLByIndex(i);

              	   if (Common.validateRequired(arJOLList.getLocation()) &&
              	   	   Common.validateRequired(arJOLList.getItemName()) &&
	   				   Common.validateRequired(arJOLList.getUnit()) &&
	   				   Common.validateRequired(arJOLList.getUnitPrice()) &&
	   				   Common.validateRequired(arJOLList.getQuantity())) continue;

              	   ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();


					   String itemDesc = arJOLList.getItemDescription();

					   mdetails.setJolCode(arJOLList.getJobOrderLineCode());
					   mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
	                   mdetails.setJolIiName(arJOLList.getItemName());
	                   mdetails.setJolLocName(arJOLList.getLocation());
	                   mdetails.setJolLineIDesc(itemDesc);
	                   mdetails.setJolIiDescription(arJOLList.getItemDescription());
	                   mdetails.setJolQuantity(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
	                   mdetails.setJolUomName(arJOLList.getUnit());
	                   mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
	                   mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
	                   mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
	                   mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
	                   mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
	                   mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
	                   mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
	                   mdetails.setJolMisc(arJOLList.getMisc());
	                   mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));

	                   mdetails.setJaList(arJOLList.getArJobOrderAssignmentList());

                   ArrayList tagList = new ArrayList();
               	   //TODO:save and submit taglist
               	   boolean isTraceMisc = ejbJO.getJoTraceMisc(arJOLList.getItemName(), user.getCmpCode());
    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
    				String misc= arJOLList.getMisc();

            	   if (isTraceMisc == true){


            		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
            		   mdetails.setJolTagList(tagList);


            	   }


               	   solList.add(mdetails);

              }
              //validate attachment
              String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
              String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
              String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
              String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;


      	   	

              try {

              	    Integer jobOrderCode = ejbJO.saveArJoEntry(details, actionForm.getPaymentTerm(),
              	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), solList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

              	    actionForm.setJobOrderCode(jobOrderCode);
              	    
              	  ejbLOG.addAdLogEntry(jobOrderCode,"AR JOB ORDER ENTRY",  new java.util.Date(),user.getUserName(),"SAVE AS DRAFT", actionForm.getLogEntry(),  user.getCmpCode());
            	    

              	    
              	    

              } catch (GlobalRecordAlreadyDeletedException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("jobOrderEntry.error.recordAlreadyDeleted"));

              } catch (GlobalDocumentNumberNotUniqueException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("jobOrderEntry.error.documentNumberNotUnique"));

              } catch (GlobalConversionDateNotExistException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("jobOrderEntry.error.conversionDateNotExist"));

              } catch (GlobalPaymentTermInvalidException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("jobOrderEntry.error.paymentTermInvalid"));

              } catch (GlobalTransactionAlreadyPendingException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("jobOrderEntry.error.transactionAlreadyPending"));

              } catch (GlobalTransactionAlreadyVoidException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("jobOrderEntry.error.transactionAlreadyVoid"));

              } catch (GlobalInvItemLocationNotFoundException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("jobOrderEntry.error.noItemLocationFound", ex.getMessage()));

              } catch (GlobalNoApprovalApproverFoundException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("jobOrderEntry.error.noApprovalApproverFound"));

              } catch (GlobalNoApprovalRequesterFoundException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("jobOrderEntry.error.noApprovalRequesterFound"));

              } catch (GlobalRecordAlreadyAssignedException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("jobOrderEntry.error.transactionIsAlreadyAssigned"));


              } catch (EJBException ex) {
              	    if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
                        " session: " + session.getId());
                   }

                  return(mapping.findForward("cmnErrorPage"));
              }

              if (!errors.isEmpty()) {

	   	    	   saveErrors(request, new ActionMessages(errors));
	   	   		   return (mapping.findForward("arJobOrderEntry"));

   	          }

        

/*******************************************************
      -- Ar SO Save & Submit Action ITEMS--
*******************************************************/

            } else if (request.getParameter("saveSubmitButton") != null && /*actionForm.getType().equals("ITEMS") &&*/
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            	System.out.println("save and submit");
            	ArModJobOrderDetails details = new ArModJobOrderDetails();

            	details.setJoType(actionForm.getJoType());
            	details.setJoCode(actionForm.getJobOrderCode());
                details.setJoDate(Common.convertStringToSQLDate(actionForm.getDate()));
                details.setJoDocumentType(actionForm.getDocumentType());
                details.setJoDocumentNumber(actionForm.getDocumentNumber());
                details.setJoReferenceNumber(actionForm.getReferenceNumber());
                details.setJoTransactionType(actionForm.getTransactionType());
                details.setJoDescription(actionForm.getDescription());
                details.setJoVoid(Common.convertBooleanToByte(actionForm.getJobOrderVoid()));
                details.setJoMobile(Common.convertBooleanToByte(actionForm.getJobOrderMobile()));
                details.setJoMemo(actionForm.getMemo());
                details.setJoJobOrderStatus(actionForm.getJobOrderStatus());
                details.setJoBillTo(actionForm.getBillTo());
                details.setJoShipTo(actionForm.getShipTo());

                details.setJoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                details.setJoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

                if (actionForm.getJobOrderCode() == null) {

                   details.setJoCreatedBy(user.getUserName());
                   details.setJoDateCreated(new java.util.Date());

                }

                details.setJoLastModifiedBy(user.getUserName());
                details.setJoDateLastModified(new java.util.Date());

                details.setJoPytName(actionForm.getPaymentTerm());
                details.setJoTcName(actionForm.getTaxCode());
                details.setJoFcName(actionForm.getCurrency());
                details.setJoCstCustomerCode(actionForm.getCustomer());
                details.setJoSlpSalespersonCode(actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_BLANK) ||
                  actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_NO_RECORD_FOUND) ? null :
                  actionForm.getSalesperson());
                details.setJoTechnician(actionForm.getTechnician());
                details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                ArrayList solList = new ArrayList();

                for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

                	ArJobOrderLineList arJOLList = actionForm.getArJOLByIndex(i);

            	   if (Common.validateRequired(arJOLList.getLocation()) &&
            	   	   Common.validateRequired(arJOLList.getItemName()) &&
   				   Common.validateRequired(arJOLList.getUnit()) &&
   				   Common.validateRequired(arJOLList.getUnitPrice()) &&
   				   Common.validateRequired(arJOLList.getQuantity())) continue;

            	   boolean chker = arJOLList.getEnableItemDesc();
					String itemDesc = "";


						itemDesc = arJOLList.getItemDescription();


            	   ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();
            	   mdetails.setJolCode(arJOLList.getJobOrderLineCode());
            	   mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
                   mdetails.setJolIiName(arJOLList.getItemName());
                   mdetails.setJolLocName(arJOLList.getLocation());
                   mdetails.setJolLineIDesc(itemDesc);
                   mdetails.setJolIiDescription(arJOLList.getItemDescription());
                   mdetails.setJolQuantity(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
                   mdetails.setJolUomName(arJOLList.getUnit());
                   mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
                   mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
                     mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
                     mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
                     mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
                     mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
                     mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
                     mdetails.setJolMisc(arJOLList.getMisc());
                     mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));
                     mdetails.setJaList(arJOLList.getArJobOrderAssignmentList());
               	  ArrayList tagList = new ArrayList();
              	   //TODO:save and submit taglist
              	   boolean isTraceMisc = ejbJO.getJoTraceMisc(arJOLList.getItemName(), user.getCmpCode());
   	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
   				String misc= arJOLList.getMisc();

   			   if (isTraceMisc == true){


        		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
        		   mdetails.setJolTagList(tagList);


        	   }



               	   solList.add(mdetails);

                }

                //validate attachment

                String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

          	    	if (actionForm.getFilename1().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("jobOrderEntry.error.filename1NotFound"));

          	    	} else {

          	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

                 	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                 	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename1Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename1().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename1SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arJobOrderEntry"));

          	    	}

          	   }

          	   if (!Common.validateRequired(filename2)) {

          	    	if (actionForm.getFilename2().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("jobOrderEntry.error.filename2NotFound"));

          	    	} else {

          	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                 	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename2Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename2().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename2SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arJobOrderEntry"));

          	    	}

          	   }

          	   if (!Common.validateRequired(filename3)) {

          	    	if (actionForm.getFilename3().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("jobOrderEntry.error.filename3NotFound"));

          	    	} else {

          	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
          	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename3Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename3().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename3SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arJobOrderEntry"));

          	    	}

          	   }

          	   if (!Common.validateRequired(filename4)) {

          	    	if (actionForm.getFilename4().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("jobOrderEntry.error.filename4NotFound"));

          	    	} else {

          	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
          	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename4Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename4().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("jobOrderEntry.error.filename4SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arJobOrderEntry"));

          	    	}

          	   	}
                try {

	        	    Integer jobOrderCode = ejbJO.saveArJoEntry(details, actionForm.getPaymentTerm(),
	        	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), solList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	        	    actionForm.setJobOrderCode(jobOrderCode);

	        	    ejbLOG.addAdLogEntry(jobOrderCode,"AR JOB ORDER ENTRY",  new java.util.Date(),user.getUserName(),"SAVE AND SUBMIT", actionForm.getLogEntry(),  user.getCmpCode());
            	    

                } catch (GlobalRecordAlreadyDeletedException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("jobOrderEntry.error.recordAlreadyDeleted"));

                } catch (GlobalDocumentNumberNotUniqueException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("jobOrderEntry.error.documentNumberNotUnique"));

                } catch (GlobalConversionDateNotExistException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("jobOrderEntry.error.conversionDateNotExist"));

                } catch (GlobalPaymentTermInvalidException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("jobOrderEntry.error.paymentTermInvalid"));

                } catch (GlobalTransactionAlreadyVoidException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("jobOrderEntry.error.transactionAlreadyVoid"));

                } catch (GlobalInvItemLocationNotFoundException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("jobOrderEntry.error.noItemLocationFound", ex.getMessage()));

                } catch (GlobalNoApprovalApproverFoundException ex) {

                   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("jobOrderEntry.error.noApprovalApproverFound"));

                } catch (GlobalNoApprovalRequesterFoundException ex) {

                		errors.add(ActionMessages.GLOBAL_MESSAGE,
                			new ActionMessage("jobOrderEntry.error.noApprovalRequesterFound"));

                } catch (GlobalRecordAlreadyAssignedException ex) {

                  		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("jobOrderEntry.error.transactionIsAlreadyAssigned"));

                } catch (GlobalTransactionAlreadyPendingException ex) {

                      errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("jobOrderEntry.error.transactionAlreadyPending"));

                } catch (EJBException ex) {
                	    if (log.isInfoEnabled()) {

                       log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
                          " session: " + session.getId());
                     }

                    return(mapping.findForward("cmnErrorPage"));
                }

                if (!errors.isEmpty()) {

  	   	    	   saveErrors(request, new ActionMessages(errors));
  	   	   		   return (mapping.findForward("arJobOrderEntry"));

     	        }

/*******************************************************
   	 -- Ar SO Print Action ITEMS --
*******************************************************/

		        } else if (request.getParameter("printButton") != null ) {

		        	System.out.println("print");
		        	if(Common.validateRequired(actionForm.getApprovalStatus())) {

		        		ArModJobOrderDetails details = new ArModJobOrderDetails();

		        		details.setJoType(actionForm.getJoType());
		        		details.setJoCode(actionForm.getJobOrderCode());
		                  details.setJoDate(Common.convertStringToSQLDate(actionForm.getDate()));
		                   details.setJoDocumentType(actionForm.getDocumentType());
		                  details.setJoDocumentNumber(actionForm.getDocumentNumber());
		                  details.setJoReferenceNumber(actionForm.getReferenceNumber());
		                  details.setJoTransactionType(actionForm.getTransactionType());
		                  details.setJoDescription(actionForm.getDescription());
		                  details.setJoVoid(Common.convertBooleanToByte(actionForm.getJobOrderVoid()));
		                  details.setJoMobile(Common.convertBooleanToByte(actionForm.getJobOrderMobile()));
		                  details.setJoMemo(actionForm.getMemo());
		                  details.setJoJobOrderStatus(actionForm.getJobOrderStatus());
		                  details.setJoBillTo(actionForm.getBillTo());
		                  details.setJoShipTo(actionForm.getShipTo());

		                  details.setJoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
		                  details.setJoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

		                  if (actionForm.getJobOrderCode() == null) {

		                    details.setJoCreatedBy(user.getUserName());
		                    details.setJoDateCreated(new java.util.Date());

		                  }

		                  details.setJoLastModifiedBy(user.getUserName());
		                  details.setJoDateLastModified(new java.util.Date());

		                  details.setJoPytName(actionForm.getPaymentTerm());
		                  details.setJoTcName(actionForm.getTaxCode());
		                  details.setJoFcName(actionForm.getCurrency());
		                  details.setJoCstCustomerCode(actionForm.getCustomer());
		                    details.setJoSlpSalespersonCode(
		                      actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_BLANK) ||
		                        actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_NO_RECORD_FOUND) ? null :
		                        actionForm.getSalesperson());
		                    details.setJoTechnician(actionForm.getTechnician());
		                details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

			          	ArrayList solList = new ArrayList();

			          	for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

			          		ArJobOrderLineList arJOLList = actionForm.getArJOLByIndex(i);

			          		if (Common.validateRequired(arJOLList.getLocation()) &&
			          		    Common.validateRequired(arJOLList.getItemName()) &&
								Common.validateRequired(arJOLList.getUnit()) &&
								Common.validateRequired(arJOLList.getUnitPrice()) &&
								Common.validateRequired(arJOLList.getQuantity())) continue;

			          		boolean chker = arJOLList.getEnableItemDesc();
							String itemDesc = "";

							System.out.println(arJOLList.getItemDescription());
							System.out.println("checker"+chker);

								itemDesc = arJOLList.getItemDescription();



			          		ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();
			          		 mdetails.setJolCode(arJOLList.getJobOrderLineCode());
			          		mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
		                    mdetails.setJolIiName(arJOLList.getItemName());
		                    mdetails.setJolLocName(arJOLList.getLocation());
		                    mdetails.setJolLineIDesc(itemDesc);
		                    mdetails.setJolIiDescription(arJOLList.getItemDescription());
		                    mdetails.setJolQuantity(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
		                    mdetails.setJolUomName(arJOLList.getUnit());
		                    mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
		                    mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
		                    mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
		                    mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
		                    mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
	                     	mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
	                     	mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
	                     	mdetails.setJolMisc(arJOLList.getMisc());
	                     	mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));
	                     	mdetails.setJaList(arJOLList.getArJobOrderAssignmentList());
			                ArrayList tagList = new ArrayList();
			               	   //TODO:save and submit taglist
			               	   boolean isTraceMisc = ejbJO.getJoTraceMisc(arJOLList.getItemName(), user.getCmpCode());
			    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
			    				String misc= arJOLList.getMisc();

			    				   if (isTraceMisc == true){


			                		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
			                		   mdetails.setJolTagList(tagList);


			                	   }

			               	solList.add(mdetails);

			          	}
			           	//validate attachment
			          	String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
		                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
		                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
		                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

		              
			          try {

			          	    Integer jobOrderCode = ejbJO.saveArJoEntry(details, actionForm.getPaymentTerm(),
			          	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), solList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

			          	    actionForm.setJobOrderCode(jobOrderCode);
			          	    
			          	  ejbLOG.addAdLogEntry(jobOrderCode,"AR JOB ORDER ENTRY",  new java.util.Date(),user.getUserName(),"PRINT", actionForm.getLogEntry(),  user.getCmpCode());
		            	    


			          } catch (GlobalRecordAlreadyDeletedException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("jobOrderEntry.error.recordAlreadyDeleted"));

			          } catch (GlobalDocumentNumberNotUniqueException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("jobOrderEntry.error.documentNumberNotUnique"));

			          } catch (GlobalConversionDateNotExistException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("jobOrderEntry.error.conversionDateNotExist"));

			          } catch (GlobalPaymentTermInvalidException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("jobOrderEntry.error.paymentTermInvalid"));

			          } catch (GlobalTransactionAlreadyVoidException ex) {

			          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("jobOrderEntry.error.transactionAlreadyVoid"));

			          } catch (GlobalInvItemLocationNotFoundException ex) {

			          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("jobOrderEntry.error.noItemLocationFound", ex.getMessage()));

		          
		              } catch (GlobalNoApprovalApproverFoundException ex) {

		               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("jobOrderEntry.error.noApprovalApproverFound"));

		              } catch (GlobalNoApprovalRequesterFoundException ex) {

		              		errors.add(ActionMessages.GLOBAL_MESSAGE,
		              			new ActionMessage("jobOrderEntry.error.noApprovalRequesterFound"));

		              } catch (GlobalRecordAlreadyAssignedException ex) {

		              		errors.add(ActionMessages.GLOBAL_MESSAGE,
		              			new ActionMessage("jobOrderEntry.error.transactionIsAlreadyAssigned"));

		              } catch (GlobalTransactionAlreadyPendingException ex) {

		                    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                       new ActionMessage("jobOrderEntry.error.transactionAlreadyPending"));

			          } catch (EJBException ex) {
			          	    if (log.isInfoEnabled()) {

			                 log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
			                    " session: " + session.getId());
			               }

			              return(mapping.findForward("cmnErrorPage"));

			          }

			           if (!errors.isEmpty()) {

				    	   saveErrors(request, new ActionMessages(errors));
				   		   return (mapping.findForward("arJobOrderEntry"));

				       }

				       actionForm.setReport(Constants.STATUS_SUCCESS);

			           isInitialPrinting = true;
			     
			        }  else {

			        	actionForm.setReport(Constants.STATUS_SUCCESS);

			        	return(mapping.findForward("arJobOrderEntry"));

			        }


/*******************************************************
   -- Ar INV Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null) {
        	 System.out.println("journal");
        	 if(Common.validateRequired(actionForm.getApprovalStatus())) {

        		 
	        		ArModJobOrderDetails details = new ArModJobOrderDetails();
	        	
	        		details.setJoType(actionForm.getJoType());
	        		details.setJoCode(actionForm.getJobOrderCode());
	                  details.setJoDate(Common.convertStringToSQLDate(actionForm.getDate()));
	                   details.setJoDocumentType(actionForm.getDocumentType());
	                  details.setJoDocumentNumber(actionForm.getDocumentNumber());
	                  details.setJoReferenceNumber(actionForm.getReferenceNumber());
	                  details.setJoTransactionType(actionForm.getTransactionType());
	                  details.setJoDescription(actionForm.getDescription());
	                  details.setJoVoid(Common.convertBooleanToByte(actionForm.getJobOrderVoid()));
	                  details.setJoMobile(Common.convertBooleanToByte(actionForm.getJobOrderMobile()));
	                  details.setJoMemo(actionForm.getMemo());
	                  details.setJoJobOrderStatus(actionForm.getJobOrderStatus());
	                  details.setJoBillTo(actionForm.getBillTo());
	                  details.setJoShipTo(actionForm.getShipTo());

	                  details.setJoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	                  details.setJoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

	                  if (actionForm.getJobOrderCode() == null) {

	                    details.setJoCreatedBy(user.getUserName());
	                    details.setJoDateCreated(new java.util.Date());

	                  }

	                  details.setJoLastModifiedBy(user.getUserName());
	                  details.setJoDateLastModified(new java.util.Date());

	                  details.setJoPytName(actionForm.getPaymentTerm());
	                  details.setJoTcName(actionForm.getTaxCode());
	                  details.setJoFcName(actionForm.getCurrency());
	                  details.setJoCstCustomerCode(actionForm.getCustomer());
	                    details.setJoSlpSalespersonCode(
	                      actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_BLANK) ||
	                        actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_NO_RECORD_FOUND) ? null :
	                        actionForm.getSalesperson());
	                    details.setJoTechnician(actionForm.getTechnician());
	                details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

		          	ArrayList solList = new ArrayList();

		          	for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

		          		ArJobOrderLineList arJOLList = actionForm.getArJOLByIndex(i);

		          		if (Common.validateRequired(arJOLList.getLocation()) &&
		          		    Common.validateRequired(arJOLList.getItemName()) &&
							Common.validateRequired(arJOLList.getUnit()) &&
							Common.validateRequired(arJOLList.getUnitPrice()) &&
							Common.validateRequired(arJOLList.getQuantity())) continue;

		          		boolean chker = arJOLList.getEnableItemDesc();
						String itemDesc = "";

						System.out.println(arJOLList.getItemDescription());
						System.out.println("checker"+chker);

							itemDesc = arJOLList.getItemDescription();



		          		ArModJobOrderLineDetails mdetails = new ArModJobOrderLineDetails();
		          		 mdetails.setJolCode(arJOLList.getJobOrderLineCode());
		          		mdetails.setJolLine(Common.convertStringToShort(arJOLList.getLineNumber()));
	                    mdetails.setJolIiName(arJOLList.getItemName());
	                    mdetails.setJolLocName(arJOLList.getLocation());
	                    mdetails.setJolLineIDesc(itemDesc);
	                    mdetails.setJolIiDescription(arJOLList.getItemDescription());
	                    mdetails.setJolQuantity(Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit));
	                    mdetails.setJolUomName(arJOLList.getUnit());
	                    mdetails.setJolUnitPrice(Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit));
	                    mdetails.setJolAmount(Common.convertStringMoneyToDouble(arJOLList.getAmount(), precisionUnit));
	                    mdetails.setJolDiscount1(Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit));
	                    mdetails.setJolDiscount2(Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit));
	                    mdetails.setJolDiscount3(Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit));
                  	mdetails.setJolDiscount4(Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit));
                  	mdetails.setJolTotalDiscount(Common.convertStringMoneyToDouble(arJOLList.getTotalDiscount(), precisionUnit));
                  	mdetails.setJolMisc(arJOLList.getMisc());
                  	mdetails.setJolTax(Common.convertBooleanToByte(arJOLList.getTax().equals("Y")?true:false));
                  	mdetails.setJaList(arJOLList.getArJobOrderAssignmentList());
		                ArrayList tagList = new ArrayList();
		               	   //TODO:save and submit taglist
		               	   boolean isTraceMisc = ejbJO.getJoTraceMisc(arJOLList.getItemName(), user.getCmpCode());
		    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
		    				String misc= arJOLList.getMisc();

		    				   if (isTraceMisc == true){


		                		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
		                		   mdetails.setJolTagList(tagList);


		                	   }

		               	solList.add(mdetails);

		          	}
		           	//validate attachment
		          	String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	              

		          try {

		          	    Integer jobOrderCode = ejbJO.saveArJoEntry(details, actionForm.getPaymentTerm(),
		          	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), solList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

		          	    actionForm.setJobOrderCode(jobOrderCode);
		          	    
		          	  ejbLOG.addAdLogEntry(jobOrderCode,"AR JOB ORDER ENTRY",  new java.util.Date(),user.getUserName(),"JOURNAL", actionForm.getLogEntry(),  user.getCmpCode());
	            	    


		          } catch (GlobalRecordAlreadyDeletedException ex) {

		          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("jobOrderEntry.error.recordAlreadyDeleted"));

		          } catch (GlobalDocumentNumberNotUniqueException ex) {

		          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("jobOrderEntry.error.documentNumberNotUnique"));

		          } catch (GlobalConversionDateNotExistException ex) {

		          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("jobOrderEntry.error.conversionDateNotExist"));

		          } catch (GlobalPaymentTermInvalidException ex) {

		          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("jobOrderEntry.error.paymentTermInvalid"));

		          } catch (GlobalTransactionAlreadyVoidException ex) {

		          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("jobOrderEntry.error.transactionAlreadyVoid"));

		          } catch (GlobalInvItemLocationNotFoundException ex) {

		          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   new ActionMessage("jobOrderEntry.error.noItemLocationFound", ex.getMessage()));


	              } catch (GlobalNoApprovalApproverFoundException ex) {

	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("jobOrderEntry.error.noApprovalApproverFound"));

	              } catch (GlobalNoApprovalRequesterFoundException ex) {

	              		errors.add(ActionMessages.GLOBAL_MESSAGE,
	              			new ActionMessage("jobOrderEntry.error.noApprovalRequesterFound"));

	              } catch (GlobalRecordAlreadyAssignedException ex) {

	              		errors.add(ActionMessages.GLOBAL_MESSAGE,
	              			new ActionMessage("jobOrderEntry.error.transactionIsAlreadyAssigned"));

	              } catch (GlobalTransactionAlreadyPendingException ex) {

	                    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                       new ActionMessage("jobOrderEntry.error.transactionAlreadyPending"));

		          } catch (EJBException ex) {
		          	    if (log.isInfoEnabled()) {

		                 log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
		                    " session: " + session.getId());
		               }

		              return(mapping.findForward("cmnErrorPage"));

		          }

		           if (!errors.isEmpty()) {

			    	   saveErrors(request, new ActionMessages(errors));
			   		   return (mapping.findForward("arJobOrderEntry"));

			       }

			       actionForm.setReport(Constants.STATUS_SUCCESS);

		           isInitialPrinting = true;
		     

		        } 

	             String path = "/arJournal.do?forward=1" +
	             "&transactionCode=" + actionForm.getJobOrderCode() +
	             "&transaction=JOB ORDER" +
	             "&transactionNumber=" + actionForm.getDocumentNumber() +
	             "&transactionDate=" + actionForm.getDate() +
	             "&transactionEnableFields=" + actionForm.getEnableFields();
	
	             return(new ActionForward(path));
/*******************************************************
   -- AR SO Delete Action --
*******************************************************/

	         } else if(request.getParameter("deleteButton") != null) {

	        	 System.out.println("delete");
	            try {
	           	System.out.println("--------------Start");
	           	    ejbJO.deleteArJoEntry(actionForm.getJobOrderCode(), user.getUserName(), user.getCmpCode());
	           	 System.out.println("--------------End");
	            } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("jobOrderEntry.error.recordAlreadyDeleted"));

	            } catch (EJBException ex) {

	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());

	                }

	                return(mapping.findForward("cmnErrorPage"));

	            }
	            


/*******************************************************
    -- Ar JO Add Lines Action --
*******************************************************/

	       } else if(request.getParameter("addLinesButton") != null) {

	      	 int listSize = actionForm.getArJOLListSize();

	         for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	 ArJobOrderLineList arSOLList = new ArJobOrderLineList(actionForm,
	        	    null, String.valueOf(x), null, null, null, false, false,null, null, null, null, null, "0.0","0.0", "0.0", "0.0", null, "0.0",null, "Y");

	        	arSOLList.setLocationList(actionForm.getLocationList());
	        	arSOLList.setTaxList(actionForm.getTaxList());
	        	actionForm.saveArJOLList(arSOLList);

	         }

	         return(mapping.findForward("arJobOrderEntry"));

/*******************************************************
     -- Ar JO Delete Lines Action --
*******************************************************/

	       } else if(request.getParameter("deleteLinesButton") != null) {

		       	for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

		       		ArJobOrderLineList arSOLList = actionForm.getArJOLByIndex(i);

		         	   if (arSOLList.getDeleteCheckbox()) {

		         	   	   actionForm.deleteArSOLList(i);
		         	   	   i--;
		         	   }

		        }

		        for (int i = 0; i<actionForm.getArJOLListSize(); i++) {

		        	ArJobOrderLineList arSOLList = actionForm.getArJOLByIndex(i);

		            	arSOLList.setLineNumber(String.valueOf(i+1));

		        }

		        return(mapping.findForward("arJobOrderEntry"));

/*******************************************************
    -- Ar JO Customer Enter Action --
*******************************************************/

	      } else if(!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

	    	  try {

	    		  ArModCustomerDetails mdetails = ejbJO.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());

	    		  actionForm.setPaymentTerm(mdetails.getCstPytName());
	    		  actionForm.setTaxCode(mdetails.getCstCcTcName());

	    		  actionForm.setCustomerName(mdetails.getCstName());
	    		  actionForm.setTaxRate(mdetails.getCstCcTcRate());
	    		  actionForm.setTaxType(mdetails.getCstCcTcType());

	    		  if (mdetails.getCstSlpSalespersonCode() != null && mdetails.getCstSlpSalespersonCode2() != null) {

	    			  actionForm.clearSalespersonList();
	    			  actionForm.clearSalespersonNameList();
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode());
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode2());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode() + " - " + mdetails.getCstSlpName());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode2() + " - " + mdetails.getCstSlpName2());

	    		  } else {
	    			  	actionForm.clearSalespersonList();
	    	          	actionForm.clearSalespersonNameList();

	    	          	ArrayList list = ejbJO.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    	          	if ( list == null || list.size() == 0) {

	    	          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
	    	          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

	    	          	} else {

	    	          		Iterator i =list.iterator();

	    	          		while (i.hasNext()) {

	    	          			ArSalespersonDetails slpDetails = (ArSalespersonDetails)i.next();

	    	          			actionForm.setSalespersonList(slpDetails.getSlpSalespersonCode());
	    	          			actionForm.setSalespersonNameList(slpDetails.getSlpSalespersonCode() + " - " + slpDetails.getSlpName());

	    	          		}

	    	          	}
	    		  }

	    		  actionForm.setSalesperson(mdetails.getCstSlpSalespersonCode() != null ?
	    				  mdetails.getCstSlpSalespersonCode() : Constants.GLOBAL_BLANK);
	    	
	    		  actionForm.clearArJOLList();

	    		  if(mdetails.getCstLitName() != null && mdetails.getCstLitName().length() > 0) {

	    			  // item template

	    			  ArrayList list = ejbJO.getInvLitByCstLitName(mdetails.getCstLitName(), user.getCmpCode());

	    			  if (!list.isEmpty()) {

	    				  Iterator i = list.iterator();

	    				  while (i.hasNext()) {

	    					  InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();

	    					  ArJobOrderLineList arJOLList = new ArJobOrderLineList(actionForm, null,
	    							  Common.convertShortToString(liDetails.getLiLine()), liDetails.getLiLocName(),
	    							  liDetails.getLiIiName(), liDetails.getLiIiDescription(), false, false, "0",
	    							  liDetails.getLiUomName(), null, null, null,"0.00", "0.0","0.0", "0.0", "0.0", "0.0" , null, "Y");

	    					  String itemClass = ejbJO.getInvIiClassByIiName(arJOLList.getItemName(), user.getCmpCode());

	    					  // populate location list

	    					  arJOLList.setLocationList(actionForm.getLocationList());

	    					  // populate tax line list

	    					  arJOLList.setTaxList(actionForm.getTaxList());
	    					  // populate unit list

	    					  ArrayList uomList = new ArrayList();
	    					  uomList = ejbJO.getInvUomByIiName(arJOLList.getItemName(), user.getCmpCode());

	    					  arJOLList.clearUnitList();
	    					  arJOLList.setUnitList(Constants.GLOBAL_BLANK);

	    					  Iterator j = uomList.iterator();

	    					  while (j.hasNext()) {

	    						  InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();

	    						  arJOLList.setUnitList(mUomDetails.getUomName());

	    					  }

	    					  // populate unit cost field

	    					  if (!Common.validateRequired(arJOLList.getItemName()) && !Common.validateRequired(arJOLList.getUnit())) {

	    						  double unitPrice = ejbJO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(
	    								  mdetails.getCstCustomerCode(), arJOLList.getItemName(), arJOLList.getUnit(), user.getCmpCode());

	    						  arJOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

	    					  }

	    					  // populate amount field

	    					  if(!Common.validateRequired(arJOLList.getQuantity()) && !Common.validateRequired(arJOLList.getUnitPrice())) {

	    						  double amount = Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit) *
	    						  Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit);
	    						  arJOLList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	    					  }

	    					  actionForm.saveArJOLList(arJOLList);

	    				  }

	    			  }

	    		  } else {

	    			  for (int x = 1; x <= journalLineNumber; x++) {

	    				  ArJobOrderLineList arJOLList = new ArJobOrderLineList(actionForm,
	    						  null, new Integer(x).toString(), null, null, null, false, false, null, null,
	    						  null, null, null,"0.0","0.0", "0.0", "0.0", null, null, null, "Y");


	    				  arJOLList.setLocationList(actionForm.getLocationList());
	    				  arJOLList.clearTaxList();
	    				  arJOLList.setTaxList(actionForm.getTaxList());
	    				  arJOLList.setUnitList(Constants.GLOBAL_BLANK);
	    				  arJOLList.setUnitList("Select Item First");

	    				  actionForm.saveArJOLList(arJOLList);

	    			  }

	    		  }

	    	  } catch (GlobalNoRecordFoundException ex) {

	          	 actionForm.clearArJOLList();
	        	 errors.add(ActionMessages.GLOBAL_MESSAGE,
	                 new ActionMessage("jobOrderEntry.error.customerNoRecordFound"));
	             saveErrors(request, new ActionMessages(errors));

	          } catch (EJBException ex) {

	            if (log.isInfoEnabled()) {

	               log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	               return mapping.findForward("cmnErrorPage");

	            }

	          }

	          return(mapping.findForward("arJobOrderEntry"));
	          
	          
	          
  /*******************************************************
      -- Ar JO Type Enter Action --
  *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("isTypeEntered"))) {

    	  try {

    		  
    
    		  
    		  ArModJobOrderTypeDetails mdetails = ejbJO.getArJobOrderTypeByJotNm(actionForm.getJoType(), user.getCmpCode());
    		  
    		  
    		  actionForm.setDocumentType(mdetails.getJotDocumentType());
    		  actionForm.setReportType(mdetails.getJotReportType());

    


          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

          }

          return(mapping.findForward("arJobOrderEntry"));

  
	          
	          
	          
 /*******************************************************
      -- Ar JO View Attachment Action --
 *******************************************************/

        } else if(request.getParameter("viewAttachmentButton1") != null) {

	  	File file = new File(attachmentPath + actionForm.getJobOrderCode() + "-1" + attachmentFileExtension );
	  	File filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-1" + attachmentFileExtensionPDF );
	  	String filename = "";

	  	if (file.exists()){
	  		filename = file.getName();
	  	}else if (filePDF.exists()) {
	  		filename = filePDF.getName();
	  	}

			System.out.println(filename + " <== filename?");
        String fileExtension = filename.substring(filename.lastIndexOf("."));

	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	  		fileExtension = attachmentFileExtension;

	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;

	    	}
	  	System.out.println(fileExtension + " <== file extension?");
	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJobOrderCode() + "-1" + fileExtension);

	  	byte data[] = new byte[fis.available()];

	  	int ctr = 0;
	  	int c = 0;

	  	while ((c = fis.read()) != -1) {

      		data[ctr] = (byte)c;
      		ctr++;

	  	}

	  	if (fileExtension == attachmentFileExtension) {
	      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
	      	System.out.println("jpg");

	      	session.setAttribute(Constants.IMAGE_KEY, image);
	      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	  		System.out.println("pdf");

	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	  	}

         if (request.getParameter("child") == null) {

           return (mapping.findForward("ArJobOrderEntry"));

         } else {

           return (mapping.findForward("ArJobOrderEntryChild"));

        }



      } else if(request.getParameter("viewAttachmentButton2") != null) {

    	  File file = new File(attachmentPath + actionForm.getJobOrderCode() + "-2" + attachmentFileExtension );
    	  	File filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-2" + attachmentFileExtensionPDF );
    	  	String filename = "";

    	  	if (file.exists()){
    	  		filename = file.getName();
    	  	}else if (filePDF.exists()) {
    	  		filename = filePDF.getName();
    	  	}

 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	  		fileExtension = attachmentFileExtension;

  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJobOrderCode() + "-2" + fileExtension);

    	  	byte data[] = new byte[fis.available()];

    	  	int ctr = 0;
    	  	int c = 0;

    	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

    	  	}

    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}




    	  	 if (request.getParameter("child") == null) {

    	           return (mapping.findForward("ArJobOrderEntry"));

    	         } else {

    	           return (mapping.findForward("ArJobOrderEntryChild"));

    	        }


      } else if(request.getParameter("viewAttachmentButton3") != null) {

    	  File file = new File(attachmentPath + actionForm.getJobOrderCode() + "-3" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-3" + attachmentFileExtensionPDF );
    	  String filename = "";

    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }

    	  System.out.println(filename + " <== filename?");
    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    		  fileExtension = attachmentFileExtension;

    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

    	  }
    	  System.out.println(fileExtension + " <== file extension?");
    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJobOrderCode() + "-3" + fileExtension);

    	  byte data[] = new byte[fis.available()];

    	  int ctr = 0;
    	  int c = 0;

    	  while ((c = fis.read()) != -1) {

    		  data[ctr] = (byte)c;
    		  ctr++;

    	  }

    	  if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  } else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  }

    	  if (request.getParameter("child") == null) {

              return (mapping.findForward("ArJobOrderEntry"));

            } else {

              return (mapping.findForward("ArJobOrderEntryChild"));

           }


      } else if(request.getParameter("viewAttachmentButton4") != null) {

    	  File file = new File(attachmentPath + actionForm.getJobOrderCode() + "-4" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-4" + attachmentFileExtensionPDF );
    	  String filename = "";

    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }

 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	  		fileExtension = attachmentFileExtension;

  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJobOrderCode() + "-4" + fileExtension);

    	  	byte data[] = new byte[fis.available()];

    	  	int ctr = 0;
    	  	int c = 0;

    	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

    	  	}

    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}
    	  	 if (request.getParameter("child") == null) {

	           return (mapping.findForward("ArJobOrderEntry"));

	         } else {

	           return (mapping.findForward("ArJobOrderEntryChild"));

	        }

/*******************************************************
  -- Ar JO Item Enter Action --
*******************************************************/

	    } else if(!Common.validateRequired(request.getParameter("arJOLList[" +
	    		actionForm.getRowJOLSelected() + "].isItemEntered"))) {

	    	ArJobOrderLineList arJOLList = actionForm.getArJOLByIndex(actionForm.getRowJOLSelected());
	    	if( actionForm.getCustomer().equals("")) {
    			
	    		arJOLList.setItemName("");
	    		arJOLList.setItemDescription("");
	    		arJOLList.setLocation("");
	    			
    			errors.add(ActionMessages.GLOBAL_MESSAGE,
                		new ActionMessage("jobOrderEntry.error.customerRequired"));
    			
        		saveErrors(request, new ActionMessages(errors));
	   	   		   return (mapping.findForward("arJobOrderEntry"));
    		}
    		
	    	

	    	try {

	    		// populate unit field class

	    		ArrayList uomList = new ArrayList();
	    		uomList = ejbJO.getInvUomByIiName(arJOLList.getItemName(), user.getCmpCode());

	    		arJOLList.clearUnitList();
	    		arJOLList.setUnitList(Constants.GLOBAL_BLANK);

	    		Iterator i = uomList.iterator();
	    		int xx = 0;
	    		while (i.hasNext()) {
	    			System.out.println("" + xx + "");
	    			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

	    			arJOLList.setUnitList(mUomDetails.getUomName());

	    			if (mUomDetails.isDefault()) {

	    			    arJOLList.setUnit(mUomDetails.getUomName());

	    			}
	    			xx= xx + 1;
	    		}

	    		//TODO: populate taglist with empty values
           		boolean isTraceMisc = ejbJO.getJoTraceMisc(arJOLList.getItemName(), user.getCmpCode());
           		boolean isJobServices = ejbJO.getJoJobServices(arJOLList.getItemName(), user.getCmpCode());
           		String misc = arJOLList.getQuantity() + "_";
           	
           		System.out.println(isTraceMisc + "<== trace misc item enter action");
           		if (isTraceMisc == true){


           			arJOLList.clearTagList();

           			arJOLList.setIsTraceMisc(isTraceMisc);


           			misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), arJOLList.getQuantity());
           			arJOLList.setMisc(misc);
           			System.out.println(arJOLList.getTagListSize() + "<== arILIList.getTagListSize() before ");

     	      		System.out.println(arJOLList.getTagListSize() + "<== arILIList.getTagListSize() after ");
           		}
           		arJOLList.setMisc(misc);

           		arJOLList.clearArJobOrderAssignmentList();


	    		// populate unit cost field

	    		if (!Common.validateRequired(arJOLList.getItemName()) && !Common.validateRequired(arJOLList.getUnit())) {

	    			double unitPrice = ejbJO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arJOLList.getItemName(), arJOLList.getUnit(), user.getCmpCode());
	    			arJOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

	    		}

	    		arJOLList.setDiscount1("0.0");
	    		arJOLList.setDiscount2("0.0");
	    		arJOLList.setDiscount3("0.0");
	    		arJOLList.setDiscount4("0.0");
	         	arJOLList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	        
	         	arJOLList.setIsJobServices(isJobServices);
	         	
	         	
		        arJOLList.setQuantity(Common.convertDoubleToStringMoney(1d, precisionUnit));
	    		arJOLList.setAmount(arJOLList.getUnitPrice());



	    	} catch (EJBException ex) {

	    		if (log.isInfoEnabled()) {

	    			log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
	    					" session: " + session.getId());
	    			return mapping.findForward("cmnErrorPage");

	    		}

	    	}

	    	return(mapping.findForward("arJobOrderEntry"));

/*******************************************************
   -- Ar JO Unit Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("arJOLList[" +
         		actionForm.getRowJOLSelected() + "].isUnitEntered"))) {

        	 ArJobOrderLineList arJOLList =
         		actionForm.getArJOLByIndex(actionForm.getRowJOLSelected());

         	try {

         		// populate unit cost field

         		if (!Common.validateRequired(arJOLList.getLocation()) && !Common.validateRequired(arJOLList.getItemName())) {

         			double unitPrice = ejbJO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arJOLList.getItemName(), arJOLList.getUnit(), user.getCmpCode());
         			arJOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

         		}

         		// populate amount field
         		double amount = 0d;

         		if(!Common.validateRequired(arJOLList.getQuantity()) && !Common.validateRequired(arJOLList.getUnitPrice())) {

		        	amount = Common.convertStringMoneyToDouble(arJOLList.getQuantity(), precisionUnit) *
						Common.convertStringMoneyToDouble(arJOLList.getUnitPrice(), precisionUnit);

		        	arJOLList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

         		}

	      		// populate discount field

	      		if (!Common.validateRequired(arJOLList.getAmount()) &&
	      			(!Common.validateRequired(arJOLList.getDiscount1()) ||
					!Common.validateRequired(arJOLList.getDiscount2()) ||
					!Common.validateRequired(arJOLList.getDiscount3()) ||
					!Common.validateRequired(arJOLList.getDiscount4()))) {

	      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

	      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

	      			}

	      			double discountRate1 = Common.convertStringMoneyToDouble(arJOLList.getDiscount1(), precisionUnit)/100;
	      			double discountRate2 = Common.convertStringMoneyToDouble(arJOLList.getDiscount2(), precisionUnit)/100;
	      			double discountRate3 = Common.convertStringMoneyToDouble(arJOLList.getDiscount3(), precisionUnit)/100;
	      			double discountRate4 = Common.convertStringMoneyToDouble(arJOLList.getDiscount4(), precisionUnit)/100;
	      			double totalDiscountAmount = 0d;

	      			if (discountRate1 > 0) {

	      				double discountAmount = amount * discountRate1;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate2 > 0) {

	      				double discountAmount = amount * discountRate2;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate3 > 0) {

	      				double discountAmount = amount * discountRate3;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate4 > 0) {

	      				double discountAmount = amount * discountRate4;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

	      				amount += amount * (actionForm.getTaxRate() / 100);

	      			}

	      			arJOLList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
	      			arJOLList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	      		}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("arJobOrderEntry"));


/*******************************************************
     -- Ar JO Tax Code Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {

         	try {

         		ArTaxCodeDetails details = ejbJO.getArTcByTcName(actionForm.getTaxCode(), user.getCmpCode());
         		actionForm.setTaxRate(details.getTcRate());
         		actionForm.setTaxType(details.getTcType());

         		actionForm.clearArJOLList();

         		for (int x = 1; x <= journalLineNumber; x++) {

         			ArJobOrderLineList arSOLList = new ArJobOrderLineList(actionForm,
              				null, new Integer(x).toString(), null, null, null, false, false, null, null,
    						null, null, null, "0.0","0.0", "0.0", "0.0", null, null, null, "Y");

    	          	arSOLList.setLocationList(actionForm.getLocationList());
    	          	arSOLList.setTaxList(actionForm.getTaxList());
    	          	arSOLList.setUnitList(Constants.GLOBAL_BLANK);
    	          	arSOLList.setUnitList("Select Item First");

              		actionForm.saveArJOLList(arSOLList);

              	}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("arJobOrderEntry"));

/*******************************************************
     -- Ar JO Conversion Date Enter Action --
******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

        	 try {

        		 actionForm.setConversionRate(Common.convertDoubleToStringMoney(
        				 ejbJO.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));

        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("jobOrderEntry.error.conversionDateNotExist"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());

        		 }

        		 return(mapping.findForward("cmnErrorPage"));

        	 }

        	 if (!errors.isEmpty()) {

        		 saveErrors(request, new ActionMessages(errors));

        	 }

        	 return(mapping.findForward("arJobOrderEntry"));

         }

/*******************************************************
     -- Ar JO Load Action --
*******************************************************/

       if (frParam != null) {
          if (!errors.isEmpty()) {

             saveErrors(request, new ActionMessages(errors));
             return(mapping.findForward("arJobOrderEntry"));

          }

          if (request.getParameter("forward") == null &&
              actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

              errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
              saveErrors(request, new ActionMessages(errors));

              return mapping.findForward("cmnMain");

          }

          try {

          	ArrayList list = null;
          	Iterator i = null;


          	actionForm.clearCurrencyList();

          	list = ejbJO.getGlFcAllWithDefault(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

          			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

          			actionForm.setCurrencyList(mFcDetails.getFcName());

          			if (mFcDetails.getFcSob() == 1) {

          				actionForm.setCurrency(mFcDetails.getFcName());
          				actionForm.setFunctionalCurrency(mFcDetails.getFcName());

          			}

          		}

          	}

          	
          	actionForm.clearDocumentTypeList();
          	
          	
          	list = ejbJO.getDocumentTypeList("AR JOB ORDER DOCUMENT TYPE", user.getCmpCode());
          	
          	
          	
          	i = list.iterator();
          	while(i.hasNext()) {
          		actionForm.setDocumentTypeList((String)i.next());
          	}
          	
          	
          	
          	actionForm.clearJoTypeList();
          	
          	
          	list = ejbJO.getAllJobOrderTypeName(user.getCmpCode());
          	
          	actionForm.setJoTypeList(Constants.GLOBAL_BLANK);
          	
          	i = list.iterator();
          	while(i.hasNext()) {
          		actionForm.setJoTypeList((String)i.next());
          	}
          	
          	
          	actionForm.clearPaymentTermList();

          	list = ejbJO.getAdPytAll(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

          		    actionForm.setPaymentTermList((String)i.next());

          		}

          		actionForm.setPaymentTerm("IMMEDIATE");

          	}



            actionForm.clearUserList();

        	ArrayList userList = ejbJO.getAdUsrAll(user.getCmpCode());

        	if (userList == null || userList.size() == 0) {

        		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

        	} else {

        		Iterator x = userList.iterator();

        		while (x.hasNext()) {

        			actionForm.setUserList((String)x.next());

        		}

        	}
        	 System.out.println(actionForm.getUserList() + "<== user List");


          	actionForm.clearTaxCodeList();

          	list = ejbJO.getArTcAll(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

        		    actionForm.setTaxCodeList((String) i.next());

          		}

          	}

          	if(actionForm.getUseCustomerPulldown()) {

          		actionForm.clearCustomerList();

          		list = ejbJO.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

          		if (list == null || list.size() == 0) {

          			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

          		} else {

          			i = list.iterator();

          			while (i.hasNext()) {

          				actionForm.setCustomerList((String)i.next());

          			}

          		}

          	}


            //print type item
            actionForm.clearReportTypeList();


            list = ejbJO.getAdLvReportTypeAll(user.getCmpCode());


            if (list == null || list.size() == 0) {

                actionForm.setReportTypeList(Constants.GLOBAL_BLANK);

            } else {

           	 actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
                i = list.iterator();

                while (i.hasNext()) {

                    actionForm.setReportTypeList((String)i.next());

                }

            }



          	actionForm.clearLocationList();

          	list = ejbJO.getInvLocAll(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

          		    actionForm.setLocationList((String)i.next());

          		}

          	}

          	//get transaction type list
          	actionForm.clearTransactionTypeList();

          	list = ejbJO.getArJoTransactionTypeList(user.getCmpCode());

          	i = list.iterator();

          	actionForm.setTransactionTypeList(Constants.GLOBAL_BLANK);
      		while (i.hasNext()) {

      		    actionForm.setTransactionTypeList((String)i.next());

      		}
      		
      		//get technician list
          	actionForm.clearTechnicianList();

          	list = ejbJO.getAllTechnician(user.getCmpCode());

          	i = list.iterator();

          	actionForm.setTechnicianList(Constants.GLOBAL_BLANK);
      		while (i.hasNext()) {

      		    actionForm.setTechnicianList((String)i.next());

      		}


      		//get job Order Status
          	actionForm.clearJobOrderStatusList();

          	list = ejbJO.getAdLvJobOrderStatus(user.getCmpCode());

          	i = list.iterator();

          	actionForm.setJobOrderStatusList(Constants.GLOBAL_BLANK);
      		while (i.hasNext()) {

      		    actionForm.setJobOrderStatusList((String)i.next());

      		}


          	actionForm.clearSalespersonList();
          	actionForm.clearSalespersonNameList();

          	list = ejbJO.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

          	if ( list == null || list.size() == 0) {

          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i =list.iterator();

          		while (i.hasNext()) {

          			ArSalespersonDetails mdetails = (ArSalespersonDetails)i.next();

          			actionForm.setSalespersonList(mdetails.getSlpSalespersonCode());
          			actionForm.setSalespersonNameList(mdetails.getSlpSalespersonCode() + " - " + mdetails.getSlpName());

          		}

          	}

          	actionForm.clearTaxList();

          	actionForm.setTaxList("Y");
          	actionForm.setTaxList("N");

          	//REPORT PARAMETERS
        	actionForm.clearReportParameters();

          	list = ejbJO.getArJobOrderReportParameters(user.getCmpCode());


          	for(int x=0;x<list.size();x++){

          	  	ReportParameter reportParameter = new ReportParameter();
          		String parameterName =list.get(x).toString();
          		reportParameter.setParameterName(parameterName);
          	  	actionForm.addReportParameters(reportParameter);
          	}


          	actionForm.clearArJOLList();

          	if (request.getParameter("forward") != null || isInitialPrinting) {

          		if (request.getParameter("forward") != null) {

          			actionForm.setJobOrderCode(new Integer(request.getParameter("jobOrderCode")));

          		}

          		ArModJobOrderDetails mdetails = ejbJO.getArJoBySoCode(actionForm.getJobOrderCode(), user.getCmpCode());

          		actionForm.setJoType(mdetails.getJoType());
          		actionForm.setDate(Common.convertSQLDateToString(mdetails.getJoDate()));
                actionForm.setDocumentType(mdetails.getJoDocumentType());
                actionForm.setDocumentNumber(mdetails.getJoDocumentNumber());
                actionForm.setReferenceNumber(mdetails.getJoReferenceNumber());
                actionForm.setTransactionType(mdetails.getJoTransactionType());
                actionForm.setDescription(mdetails.getJoDescription());
                actionForm.setJobOrderVoid(Common.convertByteToBoolean(mdetails.getJoVoid()));
                actionForm.setJobOrderMobile(Common.convertByteToBoolean(mdetails.getJoMobile()));
                actionForm.setBillTo(mdetails.getJoBillTo());
                actionForm.setShipTo(mdetails.getJoShipTo());
                actionForm.setTechnician(mdetails.getJoTechnician());
                actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getJoConversionDate()));
                actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getJoConversionRate(), (short)40));
                actionForm.setApprovalStatus(mdetails.getJoApprovalStatus());
                actionForm.setOrderStatus(mdetails.getJoOrderStatus());
                actionForm.setJoAdvanceAmount(Common.convertDoubleToStringMoney(mdetails.getJoAdvanceAmount(), (short)2));
                System.out.println(mdetails.getJoOrderStatus());
              actionForm.setReasonForRejection(mdetails.getJoReasonForRejection());
              actionForm.setPosted(mdetails.getJoPosted() == 1 ? "YES" : "NO");
                actionForm.setCreatedBy(mdetails.getJoCreatedBy());
                actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getJoDateCreated()));
                actionForm.setLastModifiedBy(mdetails.getJoLastModifiedBy());
                actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getJoDateLastModified()));
                actionForm.setPostedBy(mdetails.getJoPostedBy());
              actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getJoDatePosted()));
              actionForm.setApprovedRejectedBy(mdetails.getJoApprovedRejectedBy());
              actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getJoDateApprovedRejected()));
              actionForm.setMemo(mdetails.getJoMemo());
              actionForm.setJobOrderStatus(mdetails.getJoJobOrderStatus());

       			ArrayList rpList = ejbJO.getArJobOrderReportParameters(user.getCmpCode());




       			actionForm.setReportParameters(Common.convertStringToReportParameters(mdetails.getReportParameter(),rpList));



       			if(!actionForm.getCurrencyList().contains(mdetails.getJoFcName())) {

       				if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearCurrencyList();

          			}
       				actionForm.setCurrencyList(mdetails.getJoFcName());

       			}
          		actionForm.setCurrency(mdetails.getJoFcName());

          		if (!actionForm.getPaymentTermList().contains(mdetails.getJoPytName())) {

          			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearPaymentTermList();

          			}
          			actionForm.setPaymentTermList(mdetails.getJoPytName());

          		}
          		actionForm.setPaymentTerm(mdetails.getJoPytName());

          		if (!actionForm.getTaxCodeList().contains(mdetails.getJoTcName())) {

          			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearTaxCodeList();

          			}
          			actionForm.setTaxCodeList(mdetails.getJoTcName());

          		}
          		actionForm.setTaxCode(mdetails.getJoTcName());
          		actionForm.setTaxType(mdetails.getJoTcType());
          		actionForm.setTaxRate(mdetails.getJoTcRate());

          		if (!actionForm.getCustomerList().contains(mdetails.getJoCstCustomerCode())) {

          			if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearCustomerList();

          			}
          			actionForm.setCustomerList(mdetails.getJoCstCustomerCode());

          		}
          		actionForm.setCustomer(mdetails.getJoCstCustomerCode());
          		actionForm.setCustomerName(mdetails.getJoCstName());

          		if (!actionForm.getSalespersonList().contains(mdetails.getJoSlpSalespersonCode())) {

          			if (actionForm.getSalespersonList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearSalespersonList();

          			}

          			actionForm.setSalespersonList(mdetails.getJoSlpSalespersonCode());
          			actionForm.setSalespersonNameList(mdetails.getJoSlpSalespersonCode() + " - " + mdetails.getJoSlpName());

          		}
          		actionForm.setSalesperson(mdetails.getJoSlpSalespersonCode());

          		double totalAmount = 0;
          	
          		list = mdetails.getJoJolList();

      			i = list.iterator();
      			int curIndex = 0;
      			while (i.hasNext()) {

      				ArModJobOrderLineDetails mJoDetails = (ArModJobOrderLineDetails)i.next();

      				
      				
      				totalAmount += mJoDetails.getJolAmount();

      				String fixedDiscountAmount = "0.00";

                    if((mJoDetails.getJolDiscount1() + mJoDetails.getJolDiscount2() +
                    		mJoDetails.getJolDiscount3() + mJoDetails.getJolDiscount4()) == 0)
                   	 fixedDiscountAmount = Common.convertDoubleToStringMoney(mJoDetails.getJolTotalDiscount(), precisionUnit);

                    String ii_Desc = mJoDetails.getJolIiDescription();
                    String eii_Desc = mJoDetails.getJolEDesc();
                    String ii_DescDisp = "";
                    boolean chker = false;

                    if(eii_Desc != null)
                    {
                    	if(eii_Desc.trim().length() == 0)
                    	{
                    		ii_DescDisp = ii_Desc;
                    		chker = false;
                    	}
                    	else if(eii_Desc.trim().length() > 0)
                    	{
                    		if(ii_Desc.equals(eii_Desc)) {
                    			ii_DescDisp = ii_Desc;
                    			chker = false;
                    		}
                    		else if(!ii_Desc.equals(eii_Desc)) {
                    			ii_DescDisp = eii_Desc;
                    			chker = true;
                    		}
                    	}
                    }
                    else
                    {
                    	ii_DescDisp = ii_Desc;
                		chker = false;
                    }

                    
                    System.out.println("total hrs:" + mJoDetails.getJolTotalHours());
                    System.out.println("total rt pr hrs:" + mJoDetails.getJolTotalRatePerHours());
                    System.out.println("job order services :" + mJoDetails.getJolIiJobServices());
                    ArJobOrderLineList arJolList = new ArJobOrderLineList(actionForm,
                    		mJoDetails.getJolCode(),
                            Common.convertShortToString(mJoDetails.getJolLine()),
                            mJoDetails.getJolLocName(),
                            mJoDetails.getJolIiName(),
                            ii_DescDisp,
                            mJoDetails.getJolIiJobServices(),
                            chker,
                            Common.convertDoubleToStringMoney(mJoDetails.getJolQuantity(), precisionUnit),
                            mJoDetails.getJolUomName(),
                            Common.convertDoubleToStringMoney(mJoDetails.getJolUnitPrice(), precisionUnit),
                            Common.convertDoubleToStringMoney(mJoDetails.getJolAmount(), precisionUnit),
                            Common.convertDoubleToStringMoney(mJoDetails.getJolTotalHours(), precisionUnit),
                   
                              Common.convertDoubleToStringMoney(mJoDetails.getJolDiscount1(), precisionUnit),
                              Common.convertDoubleToStringMoney(mJoDetails.getJolDiscount2(), precisionUnit),
                              Common.convertDoubleToStringMoney(mJoDetails.getJolDiscount3(), precisionUnit),
                              Common.convertDoubleToStringMoney(mJoDetails.getJolDiscount4(), precisionUnit),
                              Common.convertDoubleToStringMoney(mJoDetails.getJolTotalDiscount(), precisionUnit),
                              fixedDiscountAmount,
                              mJoDetails.getJolMisc(),
                              Common.convertByteToBoolean(mJoDetails.getJolTax())?"Y":"N");



                    boolean isTraceMisc = ejbJO.getJoTraceMisc(mJoDetails.getJolIiName(), user.getCmpCode());
                    boolean isJobServices = ejbJO.getJoJobServices(mJoDetails.getJolIiName(), user.getCmpCode());
                    arJolList.setIsTraceMisc(isTraceMisc);
                    arJolList.setIsJobServices(isJobServices);
                    System.out.print("job services: "+ isJobServices);
                	ArrayList tagList = new ArrayList();
                    if(isTraceMisc) {

                    	tagList = mJoDetails.getJolTagList();

                    	if(tagList.size() > 0) {
                    		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mJoDetails.getJolQuantity()));


                    		arJolList.setMisc(misc);

                    	}else {
                      		if(mJoDetails.getJolMisc()==null) {

                      			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mJoDetails.getJolQuantity()));
                      			arJolList.setMisc(misc);
                      		}
                      	}

                    }
                    
                    


                    arJolList.setLocationList(actionForm.getLocationList());
                    arJolList.setTaxList(actionForm.getTaxList());
                    arJolList.clearUnitList();

      				ArrayList unitList = ejbJO.getInvUomByIiName(mJoDetails.getJolIiName(), user.getCmpCode());

      				Iterator unitListIter = unitList.iterator();

      				while (unitListIter.hasNext()) {

	            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
	            		arJolList.setUnitList(mUomDetails.getUomName());

	            	}

      				
      				arJolList.setArJobOrderAssignmentList(mJoDetails.getJaList());
      				
      				
      				
      				
      				actionForm.saveArJOLList(arJolList);
      				curIndex++;
      			}

      			actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

      			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
      			System.out.println("rem list: " + remainingList);
      			System.out.println("rem list: " + remainingList);
      			
      			for (int x = list.size() + 1; x <= remainingList; x++) {
      				
      				ArJobOrderLineList arJOLList = new ArJobOrderLineList(actionForm,
      						null, String.valueOf(x), null, null, null, false, false, null, null, null, null, null,"0.0", "0.0", "0.0", "0.0", null, "0.0", null,"Y");

      				arJOLList.setLocationList(actionForm.getLocationList());
      				arJOLList.setTaxList(actionForm.getTaxList());
      				arJOLList.setUnitList(Constants.GLOBAL_BLANK);
      				arJOLList.setUnitList("Select Item First");

      				arJOLList.setArJobOrderAssignmentList(new ArrayList());
      				actionForm.saveArJOLList(arJOLList);

      			}

      			this.setFormProperties(actionForm, user.getCompany());

    			if (request.getParameter("child") == null) {

    				return (mapping.findForward("arJobOrderEntry"));

    			} else {

    				return (mapping.findForward("arJobOrderEntryChild"));

    			}

          	}

          	

          } catch(GlobalNoRecordFoundException ex) {

		      	errors.add(ActionMessages.GLOBAL_MESSAGE,
		              new ActionMessage("jobOrderEntry.error.jobOrderAlreadyDeleted"));

          } catch(EJBException ex) {

             if (log.isInfoEnabled()) {

                log.info("EJBException caught in ArJobOrderEntryAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
             }

             return(mapping.findForward("cmnErrorPage"));

          }

          if (!errors.isEmpty()) {

             saveErrors(request, new ActionMessages(errors));

          } else {

          	if (request.getParameter("saveSubmitButton") != null &&
          			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

          		try {

          			ArrayList list = ejbJO.getAdApprovalNotifiedUsersBySoCode(actionForm.getJobOrderCode(), user.getCmpCode());

          			if (list.isEmpty()) {

          				messages.add(ActionMessages.GLOBAL_MESSAGE,
          						new ActionMessage("messages.documentSentForPosting"));

          			} else if (list.contains("DOCUMENT POSTED")) {

          				messages.add(ActionMessages.GLOBAL_MESSAGE,
          						new ActionMessage("messages.documentPosted"));

          			} else {

          				Iterator i = list.iterator();

          				String APPROVAL_USERS = "";

          				while (i.hasNext()) {

          					APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

          					if (i.hasNext()) {

          						APPROVAL_USERS = APPROVAL_USERS + ", ";

          					}


          				}

          				messages.add(ActionMessages.GLOBAL_MESSAGE,
          						new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

          			}

          		} catch(EJBException ex) {

          			if (log.isInfoEnabled()) {

          				log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
          						" session: " + session.getId());
          			}

          			return(mapping.findForward("cmnErrorPage"));

          		}

          		saveMessages(request, messages);
          		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

          	} else if (request.getParameter("saveAsDraftButton") != null &&
          			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

          		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

          	}

          }

          actionForm.reset(mapping, request);

          actionForm.setJobOrderCode(null);
          actionForm.setJoType(null);
          actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
          actionForm.setConversionRate("1.000000");
          actionForm.setPosted("NO");
       	  actionForm.setApprovalStatus(null);
          actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
          actionForm.setCreatedBy(user.getUserName());
          actionForm.setLastModifiedBy(user.getUserName());
          actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
          actionForm.setSalesperson(Constants.GLOBAL_BLANK);
          actionForm.setTechnician(null);

          this.setFormProperties(actionForm, user.getCompany());

          return(mapping.findForward("arJobOrderEntry"));

       } else {

          errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
          saveErrors(request, new ActionMessages(errors));

          return(mapping.findForward("cmnMain"));

       }


	    } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

		     if (log.isInfoEnabled()) {

		        log.info("Exception caught in ArJobOrderEntryAction.execute(): " + e.getMessage()
		           + " session: " + session.getId());
		     }

		     e.printStackTrace();
		     return(mapping.findForward("cmnErrorPage"));

       }

    }

    private void setFormProperties(ArJobOrderEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getJobOrderVoid()) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveAndSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableJobOrderVoid(false);

			} else if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getJobOrderCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setEnableJobOrderVoid(false);
					actionForm.setShowSaveAndSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);


				} else if (actionForm.getJobOrderCode() != null && Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setEnableJobOrderVoid(true);
					actionForm.setShowSaveAndSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);

				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
						actionForm.getApprovalStatus().equals("N/A")) {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveAndSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableJobOrderVoid(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveAndSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableJobOrderVoid(false);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveAndSubmitButton(true);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableJobOrderVoid(true);

			}

		} else {

			actionForm.setEnableFields(false);
			actionForm.setShowSaveAndSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);

		}
		// view attachment

		if (actionForm.getJobOrderCode() != null) {

				MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

	            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ar/Job Order/";
	            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
	            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");



	 			File file = new File(attachmentPath + actionForm.getJobOrderCode() + "-1" + attachmentFileExtension);
	 			File filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-1" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton1(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton1(false);

	 			}

	 			file = new File(attachmentPath + actionForm.getJobOrderCode() + "-2" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-2" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton2(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton2(false);

	 			}

	 			file = new File(attachmentPath + actionForm.getJobOrderCode() + "-3" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-3" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton3(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton3(false);

	 			}

	 			file = new File(attachmentPath + actionForm.getJobOrderCode() + "-4" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getJobOrderCode() + "-4" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton4(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton4(false);

	 			}

			} else {

				actionForm.setShowViewAttachmentButton1(false);
				actionForm.setShowViewAttachmentButton2(false);
				actionForm.setShowViewAttachmentButton3(false);
				actionForm.setShowViewAttachmentButton4(false);

			}



	}

}
