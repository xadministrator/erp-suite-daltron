package com.struts.ar.joborderentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.ReportParameter;

public class ArJobOrderEntryForm extends ActionForm implements java.io.Serializable {

	private short precisionUnit = 0;
	private String joType = null;
	private ArrayList joTypeList = new ArrayList();
    private Integer jobOrderCode = null;
    private String moduleKey = null;
    private String logEntry = null;
    private String customer = null;
    private String customerName = null;
    private ArrayList customerList = new ArrayList();

    private ArrayList transactionTypeList = new ArrayList();
    private String transactionType = null;
    
    private String jobOrderStatus = null;
    private ArrayList jobOrderStatusList = new ArrayList();
    
    private ArrayList userList = new ArrayList();
	private String user = null;
    private String date = null;
    private String documentType = null;
    private ArrayList documentTypeList = new ArrayList();
    private String documentNumber = null;
    private String referenceNumber = null;
    private String paymentTerm = null;
    private String joAdvanceAmount = null;
    private ArrayList paymentTermList = new ArrayList();
    private String description = null;
    private boolean jobOrderVoid = false;
    private boolean jobOrderMobile = false;
    private String billTo = null;
    private String shipTo =  null;
    
    private String technician = null;
    private ArrayList technicianList = new ArrayList();
    
    
    private String taxCode = null;
    private ArrayList taxCodeList = new ArrayList();
    private String currency = null;
    private ArrayList currencyList = new ArrayList();
    private String conversionDate = null;
    private String conversionRate = null;
    private String approvalStatus = null;
    private String posted = null;
    private String reasonForRejection = null;
    private String orderStatus = null;
    private String createdBy = null;
    private String dateCreated = null;
    private String lastModifiedBy = null;
    private String dateLastModified = null;
    private String approvedRejectedBy = null;
    private String dateApprovedRejected = null;
    private String postedBy = null;
    private String datePosted = null;
    
    private int rowSelected = 0;

    private FormFile filename1 = null;
    private FormFile filename2 = null;
    private FormFile filename3 = null;
    private FormFile filename4 = null;


    private ArrayList salespersonNameList = new ArrayList();
    private ArrayList salespersonList = new ArrayList();
    private String salesperson = null;

    private String totalAmount = null;
	private double taxRate = 0d;
	private String taxType = null;
	private String isTaxCodeEntered = null;

    private String location = null;
    private ArrayList locationList = new ArrayList();
    private String functionalCurrency = null;

    private String report = null;

    private ArrayList arJOLList = new ArrayList(); //will serve for ITEMS.


    private String reportType = null;
    private ArrayList reportTypeList = new ArrayList();

    private ArrayList reportParameters = new ArrayList();


	private int rowJOLSelected = 0;


    private String userPermission = new String();
    private String txnStatus = new String();
    private boolean enableFields = false;
    private boolean enableJobOrderVoid = false;
    private boolean showAddLinesButton = false;

    private boolean showViewAttachmentButton1 = false;
    private boolean showViewAttachmentButton2 = false;
    private boolean showViewAttachmentButton3 = false;
    private boolean showViewAttachmentButton4 = false;
    private boolean showDeleteLinesButton = false;
    private boolean showDeleteButton = false;
    private boolean showSaveButton = false;
    private boolean showSaveButtonDraft = false;
    private boolean showJournalButton = false;
    private boolean useCustomerPulldown = true;
    private boolean enablePaymentTerm = true;
    private boolean disableSalesPrice = false;

    private String isTypeEntered = null;
    private String isCustomerEntered  = null;
    private String isConversionDateEntered = null;
    private String memo = null;
    private String attachment = null;
    private String attachmentPDF = null;

    private ArrayList taxList = new ArrayList();



    public String getAttachment() {

    	   return attachment;

    }

    public void setAttachment(String attachment) {

    	   this.attachment = attachment;

    }
    public String getAttachmentPDF() {

 	   return attachmentPDF;

	 }

	 public void setAttachmentPDF(String attachmentPDF) {

	 	   this.attachmentPDF = attachmentPDF;

	 }

    public FormFile getFilename1() {

     	  return filename1;

     }

     public void setFilename1(FormFile filename1) {

     	  this.filename1 = filename1;

     }

     public FormFile getFilename2() {

     	  return filename2;

     }

     public void setFilename2(FormFile filename2) {

     	  this.filename2 = filename2;

     }

     public FormFile getFilename3() {

     	  return filename3;

     }

     public void setFilename3(FormFile filename3) {

     	  this.filename3 = filename3;

     }

     public FormFile getFilename4() {

     	  return filename4;

     }

     public void setFilename4(FormFile filename4) {

     	  this.filename4 = filename4;

     }

	 public boolean getShowViewAttachmentButton1() {

	 	   return showViewAttachmentButton1;

	 }

	 public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

	 	   this.showViewAttachmentButton1 = showViewAttachmentButton1;

	 }

	 public boolean getShowViewAttachmentButton2() {

	 	   return showViewAttachmentButton2;

	 }

	 public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

	 	   this.showViewAttachmentButton2 = showViewAttachmentButton2;

	 }

	 public boolean getShowViewAttachmentButton3() {

	 	   return showViewAttachmentButton3;

	 }

	 public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

	 	   this.showViewAttachmentButton3 = showViewAttachmentButton3;

	 }

	 public boolean getShowViewAttachmentButton4() {

	 	   return showViewAttachmentButton4;

	 }

	 public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

	 	   this.showViewAttachmentButton4 = showViewAttachmentButton4;

	 }

    public String getReport() {

        return report;

    }

    public void setReport(String report) {

        this.report = report;

    }

    public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

    public Integer getJobOrderCode() {

   	  	return jobOrderCode;

    }

    public void setJobOrderCode(Integer jobOrderCode) {

        this.jobOrderCode = jobOrderCode;

    }

    public String getCustomer() {

   	  	return customer;

    }

    public void setCustomer(String customer) {

        this.customer = customer;

    }

    
    public String getModuleKey() {
    	return moduleKey;
    }
    
    public void setModuleKey(String moduleKey) {
    	this.moduleKey = moduleKey;
    }
    
    public String getLogEntry() {
    	return logEntry;
    }
    
    public void setLogEntry(String logEntry) {
    	this.logEntry = logEntry;
    }
    
    
    public String getCustomerName() {

    	return customerName;

    }

    public void setCustomerName(String customerName) {

    	this.customerName = customerName;

    }

    public ArrayList getCustomerList() {

   	  	return customerList;

    }

    public void setCustomerList(String customer) {

       	customerList.add(customer);

   	}

    public void clearCustomerList() {

        customerList.clear();
   	  	customerList.add(Constants.GLOBAL_BLANK);

    }

    public String getJoType() {
    	return joType;
    }
    
    public void setJoType(String joType) {
    	this.joType = joType;
    }
    
    
    public ArrayList getJoTypeList() {

   	  	return joTypeList;

    }

    public void setJoTypeList(String joType) {

    	joTypeList.add(joType);

   	}

    public void clearJoTypeList() {

    	joTypeList.clear();


    }
    
    
    public String getTransactionType() {
    	return transactionType;
    }

    public void setTransactionType(String transactionType) {
    	this.transactionType = transactionType;
    }


    public ArrayList getTransactionTypeList() {

   	  	return transactionTypeList;

    }

    public void setTransactionTypeList(String transactionType) {

    	transactionTypeList.add(transactionType);

   	}

    public void clearTransactionTypeList() {

    	transactionTypeList.clear();


    }


    public String getJobOrderStatus() {
    	return jobOrderStatus;
    }
    
    public void setJobOrderStatus(String jobOrderStatus) {
    	this.jobOrderStatus = jobOrderStatus;
    }


    
    public ArrayList getJobOrderStatusList() {

   	  	return jobOrderStatusList;

    }

    public void setJobOrderStatusList(String jobOrderStatus) {

    	jobOrderStatusList.add(jobOrderStatus);

   	}

    public void clearJobOrderStatusList() {

    	jobOrderStatusList.clear();


    }
    
    public String getDate() {

   	  	return date;

    }

    public void setDate(String date) {

   	  	this.date = date;

    }
    
    
    public String getDocumentType() {
    	return documentType;
    }
    
    public void setDocumentType(String documentType) {
    	this.documentType = documentType;
    }

    public ArrayList getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(String documentType) {
		documentTypeList.add(documentType);
	}

	public void clearDocumentTypeList() {
		documentTypeList.clear();
		documentTypeList.add(Constants.GLOBAL_BLANK);
	}


    public String getDocumentNumber() {

   	  	return documentNumber;

    }

    public void setDocumentNumber(String documentNumber) {

   	  	this.documentNumber = documentNumber;

    }

    public String getReferenceNumber() {

   	  	return referenceNumber;

    }

    public void setReferenceNumber(String referenceNumber) {

   	  	this.referenceNumber = referenceNumber;

    }

    public String getPaymentTerm() {

   	  	return paymentTerm;

    }

    public void setPaymentTerm(String paymentTerm) {

   	  	this.paymentTerm = paymentTerm;

    }

    public String getJoAdvanceAmount() {

   	  	return joAdvanceAmount;

    }

    public void setJoAdvanceAmount(String joAdvanceAmount) {

   	  	this.joAdvanceAmount = joAdvanceAmount;

    }


    public ArrayList getPaymentTermList() {

   	  	return paymentTermList;

    }

    public void setPaymentTermList(String paymentTerm) {

   	  	paymentTermList.add(paymentTerm);

    }

    public void clearPaymentTermList() {

   	  	paymentTermList.clear();

    }


public ArrayList getUserList() {

		return userList;

   }

   public void setUserList(String user) {

	   userList.add(user);

   }

   public void clearUserList() {

	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);

   }

   public String getUser() {

	   return user;

   }

   public void setUser(String user) {

	   this.user = user;

   }

    public boolean getJobOrderVoid() {

        return jobOrderVoid;

    }

    public void setJobOrderVoid(boolean jobOrderVoid) {

        this.jobOrderVoid = jobOrderVoid;

    }

    public boolean getJobOrderMobile() {

        return jobOrderMobile;

    }

    public void setJobOrderMobile(boolean jobOrderMobile) {

        this.jobOrderMobile = jobOrderMobile;

    }

    public String getDescription() {

   	  	return description;

    }

    public void setDescription(String description) {

   	  	this.description = description;

    }

    public String getBillTo() {

   	  	return billTo;

    }

    public void setBillTo(String billTo) {

        this.billTo = billTo;

    }

    public String getShipTo() {

        return shipTo;

    }

    public void setShipTo(String shipTo) {

        this.shipTo = shipTo;

    }
    
    public String getTechnician() {

        return technician;

    }

    public void setTechnician(String technician) {

        this.technician = technician;

    }
    
    public ArrayList getTechnicianList() {

   	   	return technicianList;

    }

    public void setTechnicianList(String technician) {

    	technicianList.add(technician);

    }

    public void clearTechnicianList() {

    	technicianList.clear();
    	

    }

    public String getTaxCode() {

   	   	return taxCode;

    }

    public void setTaxCode(String taxCode) {

   	   	this.taxCode = taxCode;

    }

    public ArrayList getTaxCodeList() {

   	   	return taxCodeList;

    }

    public void setTaxCodeList(String taxCode) {

   	   	taxCodeList.add(taxCode);

    }

    public void clearTaxCodeList() {

   	   	taxCodeList.clear();
   	  	taxCodeList.add(Constants.GLOBAL_BLANK);

    }

    public String getCurrency() {

   	   	return currency;

    }

    public void setCurrency(String currency) {

   	   	this.currency = currency;

    }

    public ArrayList getCurrencyList() {

   	   	return currencyList;

    }

    public void setCurrencyList(String currency) {

   	   	currencyList.add(currency);

    }

    public void clearCurrencyList() {

   	   	currencyList.clear();

    }

    public String getConversionDate() {

   	   	return conversionDate;

  	}

    public void setConversionDate(String conversionDate) {

   	   	this.conversionDate = conversionDate;

    }

    public String getConversionRate() {

   	   	return conversionRate;

    }

    public void setConversionRate(String conversionRate) {

   	   	this.conversionRate = conversionRate;

    }

    public String getApprovalStatus() {

   	  	return approvalStatus;

    }

    public void setApprovalStatus(String approvalStatus) {

   	  	this.approvalStatus = approvalStatus;

    }

    public String getPosted() {

   	  	return posted;

    }

    public void setPosted(String posted) {

   	  	this.posted = posted;

    }

    public String getReasonForRejection() {

   	  	return reasonForRejection;

    }

    public void setReasonForRejection(String reasonForRejection) {

   	  	this.reasonForRejection = reasonForRejection;

    }

    public String getOrderStatus() {

   	  	return orderStatus;

    }

    public void setOrderStatus(String orderStatus) {

   	  	this.orderStatus = orderStatus;

    }

    public String getCreatedBy() {

   	   	return createdBy;

    }

    public void setCreatedBy(String createdBy) {

   	 	this.createdBy = createdBy;

    }

    public String getDateCreated() {

   	   	return dateCreated;

    }

    public void setDateCreated(String dateCreated) {

   	   	this.dateCreated = dateCreated;

    }

	public String getLastModifiedBy() {

    	return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

   	  	this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

   	  	return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

   	  	this.dateLastModified = dateLastModified;

  	}

	public String getApprovedRejectedBy() {

	    return approvedRejectedBy;

	}

	public void setApprovedRejectedBy(String approvedRejectedBy) {

	    this.approvedRejectedBy = approvedRejectedBy;

	}

	public String getDateApprovedRejected() {

	    return dateApprovedRejected;

	}

	public void setDateApprovedRejected(String dateApprovedRejected) {

	    this.dateApprovedRejected = dateApprovedRejected;

	}

	public String getPostedBy() {

   	  	return postedBy;

	}

	public void setPostedBy(String postedBy) {

   	  	this.postedBy = postedBy;

	}

	public String getDatePosted() {

   	  	return datePosted;

	}

	public void setDatePosted(String datePosted) {

   	  	this.datePosted = datePosted;

	}

	

	public String getIsTypeEntered() {

		return isTypeEntered;

	}

	
	public String getLocation() {

   	  	return location;

	}

	public void setLocation(String location) {

   	  	this.location = location;

	}

	public ArrayList getLocationList() {

   	  	return locationList;

	}

	public void setLocationList(String location) {

   	  	locationList.add(location);

	}

	public void clearLocationList() {

   	  	locationList.clear();
   	  	locationList.add(Constants.GLOBAL_BLANK);

	}

	public String getFunctionalCurrency() {

   	   	return functionalCurrency;

	}

	public void setFunctionalCurrency(String functionalCurrency) {

   	   	this.functionalCurrency = functionalCurrency;

	}

	public int getRowJOLSelected(){

	    return rowJOLSelected;

	}

	public void setRowJOLSelected(Object selectedArJOLList, boolean isEdit){

	    this.rowJOLSelected = arJOLList.indexOf(selectedArJOLList);

	}

	public void saveArJOLList(Object newArJOLList){

	    arJOLList.add(newArJOLList);

	}

	public void deleteArSOLList(int rowJOLSelected){

	    arJOLList.remove(rowJOLSelected);

	}

	public void clearArJOLList(){

		arJOLList.clear();

	}

	public ArJobOrderLineList getArJOLByIndex(int index){

   	  	return((ArJobOrderLineList)arJOLList.get(index));

	}

	public Object[] getArJOLList(){

	    return(arJOLList.toArray());

	}

	public int getArJOLListSize(){

	    return(arJOLList.size());

	}

	public void updateArJOLRow(int rowSelected, Object newArJOLList){

	    arJOLList.set(rowSelected, newArJOLList);

	}


	public int getRowSelected(){

		return rowSelected;

	}

	public void setRowSelected(Object selectedArJOLList, boolean isEdit){

		this.rowSelected = arJOLList.indexOf(selectedArJOLList);

	}
	

	public String getReportType() {

		return reportType ;

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {
		return reportTypeList;
	}

	public void setReportTypeList(String reportType) {
		reportTypeList.add(reportType);
	}

	public void clearReportTypeList() {
		reportTypeList.clear();
	}






	public Object[] getReportParameters() {
		return(reportParameters.toArray());
	}

	public void setReportParameters(ArrayList reportParameters) {
		this.reportParameters = reportParameters;
	}


	public ArrayList getReportParametersAsArrayList() {
		return reportParameters;
	}

	public void addReportParameters(ReportParameter reportParameter) {
		reportParameters.add(reportParameter);
	}

	public void clearReportParameters() {
		reportParameters.clear();
	}




	public String getTxnStatus(){

	    String passTxnStatus = txnStatus;
	    txnStatus = Constants.GLOBAL_BLANK;
	    return(passTxnStatus);

	}

	public void setTxnStatus(String txnStatus){

	    this.txnStatus = txnStatus;

	}

	public String getUserPermission(){

	    return(userPermission);

	}

	public void setUserPermission(String userPermission){

	    this.userPermission = userPermission;

	}

    public boolean getEnableFields() {

   	   	return enableFields;

    }

    public void setEnableFields(boolean enableFields) {

   	   	this.enableFields = enableFields;

    }

    public boolean getEnableJobOrderVoid() {

   	   	return enableJobOrderVoid;

    }

    public void setEnableJobOrderVoid(boolean enableJobOrderVoid) {

   	   	this.enableJobOrderVoid = enableJobOrderVoid;

    }

    public boolean getShowAddLinesButton() {

   	   	return showAddLinesButton;

    }

    public void setShowAddLinesButton(boolean showAddLinesButton) {

   	   	this.showAddLinesButton = showAddLinesButton;

    }

    public boolean getShowDeleteLinesButton() {

   	   	return showDeleteLinesButton;

    }

    public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

   	   	this.showDeleteLinesButton = showDeleteLinesButton;

    }

    public boolean getShowDeleteButton() {

   	   	return showDeleteButton;

    }

    public void setShowDeleteButton(boolean showDeleteButton) {

   	   	this.showDeleteButton = showDeleteButton;

    }

    public boolean getShowSaveButton() {

   	   	return showSaveButton;

    }

    public void setShowSaveAndSubmitButton(boolean showSaveButton) {

   	   	this.showSaveButton = showSaveButton;

    }

    public String getIsCustomerEntered() {

   	   	return isCustomerEntered;

    }

    public boolean getUseCustomerPulldown() {

   	  	return useCustomerPulldown;

    }

    public void setUseCustomerPulldown(boolean useCustomerPulldown) {

   	  	this. useCustomerPulldown = useCustomerPulldown;

    }

    public boolean getShowSaveButtonDraft() {

        return showSaveButtonDraft;

    }

    public void setShowSaveAsDraftButton(boolean showSaveButtonDraft) {

        this.showSaveButtonDraft = showSaveButtonDraft;

    }

    public String getSalesperson() {

    	return salesperson;

    }

    public void setSalesperson(String salesperson) {

    	this.salesperson = salesperson;

    }

    public ArrayList getSalespersonList() {

    	return salespersonList;

    }

    public void setSalespersonList(String salesperson) {

    	salespersonList.add(salesperson);

    }

    public void clearSalespersonList() {

    	salespersonList.clear();
    	salespersonList.add(Constants.GLOBAL_BLANK);

    }

    public ArrayList getSalespersonNameList() {

    	return salespersonNameList;

    }

    public void setSalespersonNameList(String salespersonName) {

    	salespersonNameList.add(salespersonName);

    }

    public void clearSalespersonNameList() {

    	salespersonNameList.clear();
    	salespersonNameList.add(Constants.GLOBAL_BLANK);

    }

    public String getTotalAmount() {

    	return totalAmount;

    }

    public void setTotalAmount(String totalAmount) {

    	this.totalAmount = totalAmount;

    }

	public double getTaxRate() {

		return taxRate;

	}

	public void setTaxRate(double taxRate) {

		this.taxRate = taxRate;

	}

	public String getTaxType() {

		return taxType;

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public String getIsTaxCodeEntered() {

		return isTaxCodeEntered;

	}

	public String getIsConversionDateEntered(){

   		return isConversionDateEntered;

   	}

	public String getMemo() {

		return memo;

	}

	public void setMemo(String memo) {

		this.memo = memo;

	}

	public boolean getEnablePaymentTerm() {

		return enablePaymentTerm;

	}

	public void setEnablePaymentTerm(boolean enablePaymentTerm) {

		this.enablePaymentTerm = enablePaymentTerm;

	}

	public boolean getDisableSalesPrice() {

		return disableSalesPrice;

	}

	public void setDisableSalesPrice(boolean disableSalesPrice) {

		this.disableSalesPrice = disableSalesPrice;

	}

	public ArrayList getTaxList() {
		return taxList;
	}

	public void setTaxList(String tax) {
		this.taxList.add(tax);
	}

	public void clearTaxList() {
		this.taxList.clear();
	}
	
	public boolean getShowJournalButton() {

		return showJournalButton;

	}

	public void setShowJournalButton(boolean showJournalButton) {

		this.showJournalButton = showJournalButton;

	}

    public void reset(ActionMapping mapping, HttpServletRequest request){

    	joType = null;
        customer = Constants.GLOBAL_BLANK;
        customerName = Constants.GLOBAL_BLANK;
        date = null;
        documentType = null;
	    documentNumber = null;
	    referenceNumber = null;
	    jobOrderVoid = false;
	    jobOrderMobile = false;
	    description = null;
	    billTo = null;
	    shipTo =  null;
	    technician = null;
	    taxCode = Constants.GLOBAL_BLANK;
	    conversionDate = null;
	    conversionRate = null;
	    approvalStatus = null;
	    posted = null;
	    moduleKey = null;
	    logEntry = null;
	    reasonForRejection = null;
	    orderStatus = null;
	    createdBy = null;
	    dateCreated = null;
	    lastModifiedBy = null;
	    dateLastModified = null;
	    filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
	    location = Constants.GLOBAL_BLANK;
	    isTypeEntered = null;
	    isCustomerEntered = null;
	    salesperson = Constants.GLOBAL_BLANK;
	    totalAmount = null;
        taxRate = 0d;
        taxType = Constants.GLOBAL_BLANK;
        isTaxCodeEntered = null;
 	   	isConversionDateEntered = null;
 	   	attachment = null;
 	   	attachmentPDF = null;
 	   	memo = null;
 	   	joAdvanceAmount = null;
	

 	   taxList.clear();
		taxList.add("Y");
		taxList.add("N");
	

		reportType = "";


	    for (int i=0; i<arJOLList.size(); i++) {

	    	ArJobOrderLineList actionList = (ArJobOrderLineList)arJOLList.get(i);
	  	  	actionList.setIsItemEntered(null);
	  	  	actionList.setIsUnitEntered(null);

	    }

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){

      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null ||
      		request.getParameter("saveAsDraftButton") != null ||
   		 	request.getParameter("printButton") != null || 
   		    request.getParameter("journalButton") != null ||
   		    request.getParameter("personelButton") != null) {

    	  
    	  
       	 if(Common.validateRequired(joType) || joType.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
             errors.add("customer",
                new ActionMessage("jobOrderEntry.error.jobOrderTypeRequired"));
          }
    	  
      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("jobOrderEntry.error.customerRequired"));
         }

      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("jobOrderEntry.error.dateRequired"));
         }

      	if(!Common.validateDateFormat(date)){
            errors.add("date",
	       new ActionMessage("jobOrderEntry.error.dateInvalid"));
      	}

      	try{

			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			if(year<1900){
				errors.add("date",
						new ActionMessage("jobOrderEntry.error.dateInvalid"));
			}
		} catch(Exception ex){

			errors.add("date",
					new ActionMessage("jobOrderEntry.error.dateInvalid"));

		}

      	 if(Common.validateRequired(description)){
             errors.add("description",
                new ActionMessage("jobOrderEntry.error.descriptionIsRequired"));
          }



		 if(Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("paymentTerm",
               new ActionMessage("jobOrderEntry.error.paymentTermRequired"));
         }

         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("jobOrderEntry.error.currencyRequired"));
         }

	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("jobOrderEntry.error.conversionDateInvalid"));
         }

	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("jobOrderEntry.error.conversionRateInvalid"));
         }

         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("jobOrderEntry.error.conversionDateMustBeNull"));
	 	 }

		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
		    errors.add("conversionRate",
		       new ActionMessage("jobOrderEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("jobOrderEntry.error.conversionRateOrDateMustBeEntered"));
		 }

		 int numOfLines = 0;

      	 Iterator i = arJOLList.iterator();

      	 while (i.hasNext()) {

      		ArJobOrderLineList jolList = (ArJobOrderLineList)i.next();

      	 	 if (Common.validateRequired(jolList.getItemName()) &&
      	 	 	 Common.validateRequired(jolList.getLocation()) &&
      	 	     Common.validateRequired(jolList.getQuantity()) &&
      	 	     Common.validateRequired(jolList.getUnit()) &&
      	 	     Common.validateRequired(jolList.getUnitPrice())) continue;

      	 	 numOfLines++;

      	 	if(Common.validateRequired(jolList.getItemName()) || jolList.getItemName().equals(Constants.GLOBAL_BLANK)){
                errors.add("itemName",
                  new ActionMessage("jobOrderEntry.error.itemNameRequired", jolList.getLineNumber()));
             }
             if(Common.validateRequired(jolList.getLocation()) || jolList.getLocation().equals(Constants.GLOBAL_BLANK)){
                errors.add("location",
                  new ActionMessage("jobOrderEntry.error.locationRequired", jolList.getLineNumber()));
             }
             if(Common.validateRequired(jolList.getQuantity())) {
              errors.add("quantity",
                new ActionMessage("jobOrderEntry.error.quantityRequired", jolList.getLineNumber()));
             }
         if(!Common.validateNumberFormat(jolList.getQuantity())){
                errors.add("quantity",
                   new ActionMessage("jobOrderEntry.error.quantityInvalid", jolList.getLineNumber()));
             }
         if(!Common.validateRequired(jolList.getQuantity()) && Common.convertStringMoneyToDouble(jolList.getQuantity(), (short)3) <= 0) {
              errors.add("quantity",
                new ActionMessage("jobOrderEntry.error.negativeOrZeroQuantityNotAllowed", jolList.getLineNumber()));
             }
         if(Common.validateRequired(jolList.getUnit()) || jolList.getUnit().equals(Constants.GLOBAL_BLANK)){
                errors.add("unit",
                  new ActionMessage("jobOrderEntry.error.unitRequired", jolList.getLineNumber()));
             }
         if(Common.validateRequired(jolList.getUnitPrice())){
                errors.add("unitCost",
                   new ActionMessage("jobOrderEntry.error.unitPriceRequired", jolList.getLineNumber()));
             }
         if(!Common.validateMoneyFormat(jolList.getUnitPrice())){
                errors.add("unitCost",
                   new ActionMessage("jobOrderEntry.error.unitPriceInvalid", jolList.getLineNumber()));
	         }

	    }

	    if (numOfLines == 0) {

         	errors.add("customer",
               new ActionMessage("jobOrderEntry.error.jobOrderMustHaveItemLine"));

        }

      } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("jobOrderEntry.error.customerRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {

      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("jobOrderEntry.error.customerRequired"));
         }

		 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("taxCode",
               new ActionMessage("jobOrderEntry.error.taxCodeRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

    	  if(!Common.validateDateFormat(conversionDate)){

    		  errors.add("conversionDate", new ActionMessage("jobOrderEntry.error.conversionDateInvalid"));

    	  }

      }

      return(errors);
   }
}

