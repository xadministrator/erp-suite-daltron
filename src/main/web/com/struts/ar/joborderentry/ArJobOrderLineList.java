package com.struts.ar.joborderentry;

import java.util.ArrayList;

import com.struts.ar.invoiceentry.ArInvoiceLineItemTagList;
import com.util.ArModJobOrderAssignmentDetails;

public class ArJobOrderLineList implements java.io.Serializable {

    private Integer jobOrderLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String quantity = null;
	private String unit = null;
	private String totalHours = null;
	private ArrayList unitList = new ArrayList();
	private ArrayList tagList = new ArrayList();
	private String unitPrice = null;
	private String amount = null;
	
	
	
	private String itemDescription = null;
	private boolean isAssemblyItem = false;
	private String discount1 = null;
	private String discount2 = null;
	private String discount3 = null;
	private String discount4 = null;
	private String totalDiscount = null;
	private String fixedDiscountAmount = null;
	private String misc = null;
	private String tax = null;
	private ArrayList taxList = new ArrayList();
	private boolean deleteCheckbox = false;
	private boolean enableItemDesc = false;

	private boolean isJobServices =false;
	private String isItemEntered = null;
	private String isUnitEntered = null;
	
	
	ArrayList jaList = new ArrayList();
	


	private boolean isTraceMisc = false;

	private ArJobOrderEntryForm parentBean;

	public ArJobOrderLineList(ArJobOrderEntryForm parentBean,
			Integer jobOrderLineCode,
			String lineNumber,
			String location,
			String itemName,
			String itemDescription,
			boolean enableItemDesc,
			boolean isJobServices,
			String quantity,
			String unit,
			String unitPrice,
			String amount,
			String totalHours,
			String discount1,
			String discount2,
			String discount3,
			String discount4,
			String totalDiscount,
			String fixedDiscountAmount,
			String misc,
			String tax){

		this.parentBean = parentBean;
		this.jobOrderLineCode = jobOrderLineCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.enableItemDesc = enableItemDesc;
		this.isJobServices = isJobServices;
		this.quantity = quantity;
		this.unit = unit;
		this.unitPrice = unitPrice;
		this.amount = amount;
		this.totalHours = totalHours;
		this.discount1 = discount1;
		this.discount2 = discount2;
		this.discount3 = discount3;
		this.discount4 = discount4;
		this.totalDiscount = totalDiscount;
		this.fixedDiscountAmount = fixedDiscountAmount;
		this.misc = misc;
		this.tax = tax;
	}

	public Integer getJobOrderLineCode() {

	    return jobOrderLineCode;

	}

	public String getLineNumber() {

	    return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

	    this.lineNumber = lineNumber;

	}

	public String getLocation() {

	    return location;

	}

	public void setLocation(String location) {

	    this.location = location;
	}

	public ArrayList getLocationList() {

	    return locationList;

	}



	public void setLocationList(ArrayList locationList) {

	    this.locationList = locationList;
	}



	public ArJobOrderLineTagList getTagListByIndex(int index){
	      return((ArJobOrderLineTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}
	
	public void setArJobOrderAssignmentList(ArrayList list){
		
		jaList= list;
	}
	
	public ArModJobOrderAssignmentDetails getArJobOrderAssignmentByIndex(int index){
	      return((ArModJobOrderAssignmentDetails)jaList.get(index));
	}

	public ArrayList getArJobOrderAssignmentList(){
		return jaList;
	}
	public int getArJobOrderAssignmentListSize(){
	      return(jaList.size());
	}

	public void saveArJobOrderAssignmentList(Object arJobOrderAssignment){
		jaList.add(arJobOrderAssignment);
	}

	public void clearArJobOrderAssignmentList(){
		jaList.clear();
	}

	
	
	


	public String getItemName() {

	    return itemName;

	}

	public void setItemName(String itemName) {

	    this.itemName = itemName;

	}

	public String getQuantity() {

	    return quantity;

	}

	public void setQuantity(String quantity) {

	    this.quantity = quantity;
	}

	public String getUnit() {

	    return unit;

	}

	public void setUnit(String unit) {

	    this.unit = unit;
	}

	public ArrayList getUnitList() {

	    return unitList;

	}

	public void setUnitList(String unit) {

	    unitList.add(unit);

	}

	public void clearUnitList() {

		unitList.clear();

	}

	public String getUnitPrice() {

	    return unitPrice;

	}

	public void setUnitPrice(String unitPrice) {

	    this.unitPrice = unitPrice;

	}

	public String getAmount() {

	    return amount;

	}

	public void setAmount(String amount) {

	    this.amount = amount;

	}
	
	
	public String getTotalHours() {

	    return totalHours;

	}

	public void setTotalHours(String totalHours) {

	    this.totalHours = totalHours;

	}
	



	public String getItemDescription() {

	    return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

	    this.itemDescription = itemDescription;

	}

	public boolean getDeleteCheckbox() {

		return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

		this.deleteCheckbox = deleteCheckbox;

	}

	public boolean getEnableItemDesc() {

		return enableItemDesc;

	}

	public void setEnableItemDesc(boolean enableItemDesc) {

		this.enableItemDesc = enableItemDesc;

	}
	
	
	public boolean getIsJobServices() {

		return isJobServices;

	}

	public void setIsJobServices(boolean isJobServices) {

		this.isJobServices = isJobServices;

	}


	public boolean getIsTraceMisc() {

		return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

		this.isTraceMisc = isTraceMisc;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowJOLSelected(this, false);

		}

		isItemEntered = null;

	}

	public String getIsUnitEntered() {

		return isUnitEntered;

	}

	public void setIsUnitEntered(String isUnitEntered) {

		if (isUnitEntered != null && isUnitEntered.equals("true")) {

			parentBean.setRowJOLSelected(this, false);

		}

		isUnitEntered = null;

	}

	public String getDiscount1() {

		return discount1;

	}

	public void setDiscount1(String discount1) {

		this.discount1 = discount1;

	}

	public String getDiscount2() {

		return discount2;

	}

	public void setDiscount2(String discount2) {

		this.discount2 = discount2;

	}

	public String getDiscount3() {

		return discount3;

	}

	public void setDiscount3(String discount3) {

		this.discount3 = discount3;

	}

	public String getDiscount4() {

		return discount4;

	}

	public void setDiscount4(String discount4) {

		this.discount4 = discount4;

	}

	public String getTotalDiscount() {

		return totalDiscount;

	}

	public void setTotalDiscount(String totalDiscount) {

		this.totalDiscount = totalDiscount;

	}

	public String getFixedDiscountAmount() {

		return fixedDiscountAmount;

	}

	public void setFixedDiscountAmount(String fixedDiscountAmount) {

		this.fixedDiscountAmount = fixedDiscountAmount;

	}

	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;

	}

   public ArrayList getTaxList() {
	   return taxList;
   }

   public void setTaxList(ArrayList taxList) {
	   this.taxList = taxList;
   }

   public void clearTaxList() {
	   this.taxList.clear();
   }




	public String getTax() {

		return tax;

	}

	public void setTax(String tax) {

		this.tax = tax;

	}

}