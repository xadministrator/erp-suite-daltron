package com.struts.ar.joborderentry;

import java.io.Serializable;

import com.struts.ar.invoiceentry.ArInvoiceSalesOrderList;

public class ArJobOrderLineTagList implements Serializable{

	private String propertyCode = null;
	private String specs = null;
	private String serialNumber = null;
	private String custodian = null;
	private String expiryDate = null;
	private String documentNumber = null;

	private ArJobOrderLineList parentBean;

	public ArJobOrderLineTagList(ArJobOrderLineList parentBean,
			String propertyCode, String specs, String documentNumber, String custodian, String serialNumber, String expiryDate){

		this.parentBean = parentBean;
		this.propertyCode = propertyCode;
		this.serialNumber = serialNumber;
		this.specs = specs;
		this.custodian = custodian;
		this.expiryDate = expiryDate;
		this.documentNumber = documentNumber;
	}


	public String getDocumentNumber() {

		return documentNumber;

	}

	public void setDocumentNumber(String documentNumber) {

		this.documentNumber = documentNumber;

	}

	public String getPropertyCode() {

		return propertyCode;

	}

	public void setPropertyCode(String propertyCode) {

		this.propertyCode = propertyCode;

	}
	public String getSpecs() {

		return specs;

	}

	public void setSpecs(String specs) {

		this.specs = specs;

	}

	public String getCustodian() {

		return custodian;

	}

	public void setCustodian(String custodian) {

		this.custodian = custodian;

	}

	public String getSerialNumber() {

		return serialNumber;

	}

	public void setSerialNumber(String serialNumber) {

		this.serialNumber = serialNumber;

	}

	public String getExpiryDate() {

		return expiryDate;

	}

	public void setExpiryDate(String expiryDate) {

		this.expiryDate = expiryDate;

	}
}
