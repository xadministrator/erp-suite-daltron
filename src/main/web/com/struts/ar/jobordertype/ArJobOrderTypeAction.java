package com.struts.ar.jobordertype;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ArJOTCoaGlJobOrderAccountNotFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArJobOrderTypeController;
import com.ejb.txn.ArJobOrderTypeControllerHome;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModJobOrderTypeDetails;

public final class ArJobOrderTypeAction extends Action {
	
    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
      	
      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArJobOrderTypeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ArJobOrderTypeForm actionForm = (ArJobOrderTypeForm)form;

        String frParam = Common.getUserPermission(user, Constants.AR_JOB_ORDER_TYPE_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("arJobOrderType");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ArJobOrderTypeController EJB
*******************************************************/

        ArJobOrderTypeControllerHome homeJOT = null;
        ArJobOrderTypeController ejbJOT = null;


        try {

        	homeJOT = (ArJobOrderTypeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArJobOrderTypeControllerEJB", ArJobOrderTypeControllerHome.class);
            
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArJobOrderTypeAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

        	ejbJOT = homeJOT.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArJobOrderTypeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();


/*******************************************************
   Call ArJobOrderType EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         
         try {
         	
            precisionUnit = ejbJOT.getGlFcPrecisionUnit(user.getCmpCode());        
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArJobOrderTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

     
/*******************************************************
   -- Ar JOT Save Action --
*******************************************************/

        if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	ArModJobOrderTypeDetails mdetails = new ArModJobOrderTypeDetails();

            mdetails.setJotName(actionForm.getName());
            mdetails.setJotDocumentType(actionForm.getDocumentType());
            mdetails.setJotReportType(actionForm.getReportType());
            mdetails.setJotDescription(actionForm.getDescription());
         
            mdetails.setJotGlCoaJobOrderAccountNumber(actionForm.getJobOrderAccount());
            
            mdetails.setJotEnable(Common.convertBooleanToByte(actionForm.getEnable()));
      
            try {
            	
            	ejbJOT.addArJotEntry(mdetails, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("jobOrderType.error.recordAlreadyExist"));
                  	
            } catch (ArJOTCoaGlJobOrderAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("jobOrderType.error.jobOrderAccountNotFound"));

                	                  	

           
            } catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in ArJobOrderTypeAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	return mapping.findForward("cmnErrorPage"); 
                  
               	}

            }
            


/*******************************************************
   -- Ar JOT Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar JOT Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            ArJobOrderTypeList arJOTList =
	            actionForm.getArJOTByIndex(actionForm.getRowSelected());
            

            ArModJobOrderTypeDetails mdetails = new ArModJobOrderTypeDetails();
            mdetails.setJotCode(arJOTList.getJobOrderTypeCode());
            mdetails.setJotName(actionForm.getName());
            mdetails.setJotDocumentType(actionForm.getDocumentType());
            mdetails.setJotReportType(actionForm.getReportType());
            mdetails.setJotDescription(actionForm.getDescription());
          
                                                  
            mdetails.setJotGlCoaJobOrderAccountNumber(actionForm.getJobOrderAccount());
          
            mdetails.setJotEnable(Common.convertBooleanToByte(actionForm.getEnable())); 
           
            try {
            	
            	ejbJOT.updateArJotEntry(mdetails,  user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("jobOrderType.error.recordAlreadyExist"));
                  	
            } catch (ArJOTCoaGlJobOrderAccountNotFoundException ex) {

               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                  	new ActionMessage("jobOrderType.error.jobOrderAccountNotFound"));

           

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArJobOrderTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ar JOT Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ar JOT Edit Action --
*******************************************************/

         } else if (request.getParameter("arJOTList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showArJOTRow(actionForm.getRowSelected());
            
            return mapping.findForward("arJobOrderType");
            
/*******************************************************
   -- Ar JOT Delete Action --
*******************************************************/

         } else if (request.getParameter("arJOTList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ArJobOrderTypeList arJOTList =
              actionForm.getArJOTByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbJOT.deleteArJotEntry(arJOTList.getJobOrderTypeCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("jobOrderType.error.deleteJobOrderTypeAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("jobOrderType.error.jobOrderTypeAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArJobOrderTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar JOT Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arJobOrderType");

            }
            
            ArrayList list = new ArrayList();
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
              
               	
             
               //print type item
                actionForm.clearReportTypeList();


                list = ejbJOT.getAdLvReportTypeAll(user.getCmpCode());


                if (list == null || list.size() == 0) {
                	System.out.println("pasok clear");
                    actionForm.setReportTypeList(Constants.GLOBAL_BLANK);

                } else {

                	System.out.println("pasok");
               	 actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
                    i = list.iterator();

                    while (i.hasNext()) {

                        actionForm.setReportTypeList((String)i.next());

                    }

                }
         	
                actionForm.clearArJOTList();
            	               	
	           	list = ejbJOT.getArJotAll(user.getCmpCode()); 
	           
	           	i = list.iterator();
	            
	           	while(i.hasNext()) {
        	            			            	
	              	ArModJobOrderTypeDetails mdetails = (ArModJobOrderTypeDetails)i.next();
	            	
	              	ArJobOrderTypeList arJOTList = new ArJobOrderTypeList(actionForm,
	            		mdetails.getJotCode(),
	            	    mdetails.getJotName(),
	            	    mdetails.getJotDescription(),
	            	    mdetails.getJotDocumentType(),
	            	    mdetails.getJotReportType(),
	            	    mdetails.getJotTcName(),
	            	    mdetails.getJotWtcName(),
                        mdetails.getJotGlCoaJobOrderAccountNumber(),
                        mdetails.getJotGlCoaJobOrderAccountDescription(),
             
                        Common.convertByteToBoolean(mdetails.getJotEnable())
                        ); 
	              	 	            	    
	              	actionForm.saveArJOTList(arJOTList);
	            	
	           	}
	           	
	           	actionForm.clearDocumentTypeList();
	          	
	          	
	          	list = ejbJOT.getDocumentTypeList("AR JOB ORDER DOCUMENT TYPE", user.getCmpCode());
	          	
	          	
	          	
	          	i = list.iterator();
	          	while(i.hasNext()) {
	          		actionForm.setDocumentTypeList((String)i.next());
	          	}
	           	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArJobOrderTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
                                    
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
     
            
            return(mapping.findForward("arJobOrderType"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ArJobOrderTypeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}