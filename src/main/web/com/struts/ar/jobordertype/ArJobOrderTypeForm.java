package com.struts.ar.jobordertype;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArJobOrderTypeForm extends ActionForm implements Serializable {

	private Integer jobOrderTypeCode = null;
	private String documentType = null;
	private ArrayList documentTypeList = new ArrayList();
	private String name = null;
	private String description = null;


	private String jobOrderAccount =  null;
	private String jobOrderAccountDescription = null;
	
	
	private boolean enable = false;
	

	 private String reportType = null;
	    private ArrayList reportTypeList = new ArrayList();


	private String saveButton = null;
	private String closeButton = null;

	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList arJOTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
	
	    return rowSelected;
	
	}
	
	public ArJobOrderTypeList getArJOTByIndex(int index) {
	
	    return((ArJobOrderTypeList)arJOTList.get(index));
	
	}
	
	public Object[] getArJOTList() {
	
	    return arJOTList.toArray();
	
	}
	
	public int getArJOTListSize() {
	
	    return arJOTList.size();
	
	}
	
	public void saveArJOTList(Object newArJOTList) {
	
		arJOTList.add(newArJOTList);
	
	}
	
	public void clearArJOTList() {
		
		arJOTList.clear();
	  
	}
	
	public void setRowSelected(Object selectedArJOTList, boolean isEdit) {
	
	    this.rowSelected = arJOTList.indexOf(selectedArJOTList);
	
	    if (isEdit) {
	
	        this.pageState = Constants.PAGE_STATE_EDIT;
	
	    }
	
	}
	
	public void showArJOTRow(int rowSelected) {
		
		this.name = ((ArJobOrderTypeList)arJOTList.get(rowSelected)).getName();
		this.description = ((ArJobOrderTypeList)arJOTList.get(rowSelected)).getDescription();
		this.documentType =((ArJobOrderTypeList)arJOTList.get(rowSelected)).getDocumentType();

		this.reportType = ((ArJobOrderTypeList)arJOTList.get(rowSelected)).getReportType();
		
        this.jobOrderAccount = ((ArJobOrderTypeList)arJOTList.get(rowSelected)).getJobOrderAccount();
        this.jobOrderAccountDescription = ((ArJobOrderTypeList)arJOTList.get(rowSelected)).getJobOrderAccountDescription();
		
		
		this.enable = ((ArJobOrderTypeList)arJOTList.get(rowSelected)).getEnable(); 
		
	
	
	}
	
	public void updateArJOTRow(int rowSelected, Object newArCCList) {
	
		arJOTList.set(rowSelected, newArCCList);
	
	}
	
	public void deleteArJOTList(int rowSelected) {
	
		arJOTList.remove(rowSelected);
	
	}
        
	public void setUpdateButton(String updateButton) {
	
	    this.updateButton = updateButton;
	
	}
        

	
	public void setCancelButton(String cancelButton) {
	
	    this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	    this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	    this.closeButton = closeButton;
	
	}
	

	
	public void setPageState(String pageState) {
	
	    this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	    return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getJobOrderTypeCode() {
		
		return jobOrderTypeCode;
		
	}
	
	public void setJobOrderTypeCode(Integer jobOrderTypeCode) {
		
		this.jobOrderTypeCode = jobOrderTypeCode;
		
	}
	
	public String getName() {
	
	  	return name;
	
	}
	
	public void setName(String name) {
	
	  	this.name = name;
	
	}
	
	public String getDescription() {
	
		return description;
	
	}
	
	public void setDescription(String description) {
	
	  	this.description = description;
	
	}
	
	public String getDocumentType() {
		
		return documentType;
	
	}
	
	public void setDocumentType(String documentType) {
	
	  	this.documentType = documentType;
	
	}
	

	 public ArrayList getDocumentTypeList() {
        
        return documentTypeList;
        
    }
    
    public void setDocumentTypeList(String documentType) {
        
    	documentTypeList.add(documentType);
        
    }
    
    public void clearDocumentTypeList() {
        
    	documentTypeList.clear();
    	documentTypeList.add(Constants.GLOBAL_BLANK);
        
    }
	
  
    public String getJobOrderAccount() {
    	
    	return jobOrderAccount;
    	
    }
    
    public void setJobOrderAccount(String jobOrderAccount) {
    	
    	this.jobOrderAccount = jobOrderAccount;
    	
    }
    
    
   public String getJobOrderAccountDescription() {
    	
    	return jobOrderAccountDescription;
    	
    }
    
    public void setJobOrderAccountDescription(String jobOrderAccountDescription) {
    	
    	this.jobOrderAccountDescription = jobOrderAccountDescription;
    	
    }
    
   
    
   
    
    public boolean getEnable() {
    	
    	return enable;
    	
    }
    
    public void setEnable(boolean enable) {
    	
    	this.enable = enable;
    	
    }

    
    
    public String getReportType() {

		return reportType ;

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {
		return reportTypeList;
	}

	public void setReportTypeList(String reportType) {
		reportTypeList.add(reportType);
	}

	public void clearReportTypeList() {
		reportTypeList.clear();
	}


    
    
    





	public void reset(ActionMapping mapping, HttpServletRequest request) {
	
		name = null;
		description = null;
		documentType = null;
		reportType = null;
		 
        jobOrderAccount = null;

        jobOrderAccountDescription = null;
      
        
		enable = false;
	
	
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
			if (Common.validateRequired(name)) {
			
				errors.add("name",
			   		new ActionMessage("jobOrderType.error.nameRequired"));
			
			}
		
													
	  	}
	     
	  	return errors;
	
	}
	
}