package com.struts.ar.jobordertype;

import java.io.Serializable;

public class ArJobOrderTypeList implements Serializable {

    private Integer jobOrderTypeCode = null;
    private String name = null;
    private String description = null;
    private String documentType = null;
    private String reportType = null;
    private String taxCode = null;
    private String withholdingTaxCode = null;
    private String jobOrderAccount = null;
    private String jobOrderAccountDescription = null;

    
    private boolean enable = false;


	private String editButton = null;
	private String deleteButton = null;
	
	private ArJobOrderTypeForm parentBean;
    
	public ArJobOrderTypeList(ArJobOrderTypeForm parentBean,
		Integer jobOrderTypeCode,
        String name,
        String description,
        String documentType,
        String reportType,
        String taxCode,
        String withholdingTaxCode,
        String jobOrderAccount,
        String jobOrderAccountDescription,


		boolean enable) {
	
		this.parentBean = parentBean;
        this.jobOrderTypeCode = jobOrderTypeCode;
        this.name = name;
        this.description = description;
        this.documentType = documentType;

        
        this.taxCode = taxCode;
        this.withholdingTaxCode = withholdingTaxCode;
        this.jobOrderAccount = jobOrderAccount;
        this.jobOrderAccountDescription = jobOrderAccountDescription;
      
		this.enable = enable;

		
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getJobOrderTypeCode() {
	
	    return jobOrderTypeCode;
	
	}
	
	public String getName() {
		
		return name;
		
    }
    
    public String getDescription() {
    	
    	return description;
    	
    }
    
    public String getDocumentType() {
    	
    	return documentType;
    	
    }
    
    public String getReportType() {
    	
    	return reportType;
    	
    }
    
        
    public String getTaxCode() {
    	
    	return taxCode;
    	
    }
    
    public String getWithholdingTaxCode() {
    	
    	return withholdingTaxCode;
    	
    }
    
    public String getJobOrderAccount() {
    	
    	return jobOrderAccount;
    	
    }
    
    public String getJobOrderAccountDescription() {
    	
    	return jobOrderAccountDescription;
    	
    }
   
    
    public boolean getEnable() {
    	
    	return enable;
    	
    }
 
        	    	
}