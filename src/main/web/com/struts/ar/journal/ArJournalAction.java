package com.struts.ar.journal;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArJournalController;
import com.ejb.txn.ArJournalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModDistributionRecordDetails;

public final class ArJournalAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArJournalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArJournalForm actionForm = (ArJournalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_JOURNAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arJournal");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArJournalController EJB
*******************************************************/

         ArJournalControllerHome homeJR = null;
         ArJournalController ejbJR = null;

         try {

            homeJR = (ArJournalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArJournalControllerEJB", ArJournalControllerHome.class);      
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbJR = homeJR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call ArJournalController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short invoiceLineNumber = 0;
         
         try {
         	
            precisionUnit = ejbJR.getGlFcPrecisionUnit(user.getCmpCode()); 
            invoiceLineNumber = ejbJR.getAdPrfArInvoiceLineNumber(user.getCmpCode());
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }	

/*******************************************************
   -- Ar JR Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	ArrayList drList = new ArrayList();
            int lineNumber = 0;  
            
            double TOTAL_DEBIT = 0;
            double TOTAL_CREDIT = 0;         
                       
            for (int i = 0; i < actionForm.getArJRListSize(); i++) {
            	
        	   ArJournalList arJRList = actionForm.getArJRByIndex(i);           	   
        	              	   
        	   if (Common.validateRequired(arJRList.getAccountNumber()) &&
        	   		Common.validateRequired(arJRList.getDebitAmount()) &&
        	   		Common.validateRequired(arJRList.getCreditAmount())) continue;
        	   
        	   byte isDebit = 0;
	           double amount = 0d;

		       if (!Common.validateRequired(arJRList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(arJRList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(arJRList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
	       
		       lineNumber++;

        	   ArModDistributionRecordDetails mdetails = new ArModDistributionRecordDetails();
        	   
        	   mdetails.setDrLine((short)(lineNumber));
        	   mdetails.setDrClass(arJRList.getDrClass());
        	   mdetails.setDrDebit(isDebit);
        	   mdetails.setDrAmount(amount);
        	   mdetails.setDrCoaAccountNumber(arJRList.getAccountNumber());
        	   
        	   drList.add(mdetails);
            	
            }
            
            TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
            TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
            
            if (TOTAL_DEBIT != TOTAL_CREDIT) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("arJournal.error.journalNotBalance"));
                     
                saveErrors(request, new ActionMessages(errors));
                return (mapping.findForward("arJournal"));
            	
            }
            
            Integer primaryKey = null;
            
            if (actionForm.getInvoiceCode() != null) {
            	
            	primaryKey = actionForm.getInvoiceCode();
            	
            } else if (actionForm.getCreditMemoCode() != null) {
            	
            	primaryKey = actionForm.getCreditMemoCode();
            	
            } else if (actionForm.getReceiptCode() != null) {
            	
            	primaryKey = actionForm.getReceiptCode();
            	
            } else if( actionForm.getMiscReceiptCode() != null) {
            	
            	primaryKey = actionForm.getMiscReceiptCode();
            	
            }
            
            try {
            	
            	ejbJR.saveArDrEntry(primaryKey, drList, actionForm.getTransaction(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("arJournal.error.accountNumberInvalid", ex.getMessage()));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }
            
/*******************************************************
	-- Ar JR Add Lines Action --
*******************************************************/
	
	      } else if(request.getParameter("addLinesButton") != null) {
	      	
	      	int listSize = actionForm.getArJRListSize();
	                                
	         for (int x = listSize + 1; x <= listSize + invoiceLineNumber; x++) {
	        	
	        	ArJournalList arJRList = new ArJournalList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);
	        	    
	        	arJRList.setDrClassList(this.getDrClassList());
	        	
	        	actionForm.saveArJRList(arJRList);
	        	
	        }	        
	        
	        return(mapping.findForward("arJournal"));

/*******************************************************
    -- Ar JR Delete Lines Action --
*******************************************************/

          } else if(request.getParameter("deleteLinesButton") != null) {
          	
          	for (int i = 0; i<actionForm.getArJRListSize(); i++) {
            	
            	   ArJournalList arJRList = actionForm.getArJRByIndex(i);
            	   
            	   if (arJRList.getDeleteCheckbox()) {
            	   	
            	   	   actionForm.deleteArJRList(i);
            	   	   i--;
            	   }
            	   
             }
             
             for (int i = 0; i<actionForm.getArJRListSize(); i++) {
            	
             	ArJournalList arJRList = actionForm.getArJRByIndex(i);
            	   
            	   arJRList.setLineNumber(String.valueOf(i+1));
            	   
             }
 	        
 	        return(mapping.findForward("arJournal"));	 

/*******************************************************
   -- Ar JR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {
            
            return(mapping.findForward("cmnMain"));
          
/*******************************************************
   -- Ar JR Back Action --
*******************************************************/

         } else if (request.getParameter("backButton") != null) {

            String path = null;
            
            if (actionForm.getTransaction().equals("INVOICE")) {
            	
            	path = "/arInvoiceEntry.do?forward=1&invoiceCode=" +
            	    actionForm.getInvoiceCode();
            	
            } else if (actionForm.getTransaction().equals("CREDIT MEMO")) {
            	
            	path = "/arCreditMemoEntry.do?forward=1&creditMemoCode=" +
            	    actionForm.getCreditMemoCode();
            	
            } else if (actionForm.getTransaction().equals("RECEIPT")) {
            	
            	path = "/arReceiptEntry.do?forward=1&receiptCode=" +
            	    actionForm.getReceiptCode();

            } else if (actionForm.getTransaction().equals("MISC RECEIPT")) {
            	
            	path = "/arMiscReceiptEntry.do?forward=1&receiptCode=" +
            	    actionForm.getMiscReceiptCode();
            	    
            }
            
            return(new ActionForward(path));
                        
/*******************************************************
   -- Ar JR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arJournal");

            }
            
            try {
            	
        		ArrayList list = new ArrayList();
        		Iterator i = null;
        		
        		actionForm.clearArJRList();

        		if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("INVOICE")) ||
        		    actionForm.getTransaction().equals("INVOICE")) {
        			
        			if (request.getParameter("transactionCode") != null) 
        			   actionForm.setInvoiceCode(new Integer(request.getParameter("transactionCode")));
        			   actionForm.setCreditMemoCode(null);
        			   actionForm.setReceiptCode(null);
        			   actionForm.setMiscReceiptCode(null);
        			
        			list = ejbJR.getArDrByInvCode(actionForm.getInvoiceCode(), user.getCmpCode());
	            			
        		} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("CREDIT MEMO")) ||
        		    actionForm.getTransaction().equals("CREDIT MEMO")) {
        			
        			if (request.getParameter("transactionCode") != null) 
        				actionForm.setCreditMemoCode(new Integer(request.getParameter("transactionCode")));
        				actionForm.setInvoiceCode(null);
        				actionForm.setReceiptCode(null);
        				actionForm.setMiscReceiptCode(null);
       			
        			list = ejbJR.getArDrByInvCode(actionForm.getCreditMemoCode(), user.getCmpCode());
            			
        		} if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("RECEIPT")) ||
        		    actionForm.getTransaction().equals("RECEIPT")) {
        			
        			if (request.getParameter("transactionCode") != null) 
        				actionForm.setReceiptCode(new Integer(request.getParameter("transactionCode")));
        				actionForm.setCreditMemoCode(null);
        				actionForm.setInvoiceCode(null);
        				actionForm.setMiscReceiptCode(null);
        			
        			list = ejbJR.getArDrByRctCode(actionForm.getReceiptCode(), user.getCmpCode());
       			
        		} if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("MISC RECEIPT")) ||
        		    actionForm.getTransaction().equals("MISC RECEIPT")) {
        			
        			if (request.getParameter("transactionCode") != null) 
        				actionForm.setMiscReceiptCode(new Integer(request.getParameter("transactionCode")));
        				actionForm.setCreditMemoCode(null);
        				actionForm.setReceiptCode(null);
        				actionForm.setInvoiceCode(null);
        				
        			list = ejbJR.getArDrByRctCode(actionForm.getMiscReceiptCode(), user.getCmpCode());
        			        			            			
        		}
        		
        		if (request.getParameter("forward") != null) {

	      			actionForm.setNumber(request.getParameter("transactionNumber"));
	       			actionForm.setDate(request.getParameter("transactionDate"));
	       			actionForm.setEnableFields(request.getParameter("transactionEnableFields"));
	       			
	       		}
        		
        		double totalDebit = 0;
				double totalCredit = 0;
				
        		i = list.iterator();        		       		
        		
    			while (i.hasNext()) {
    				
    				ArModDistributionRecordDetails mdetails = (ArModDistributionRecordDetails)i.next();
    				
    				String debitAmount = null;
    				String creditAmount = null;
    				
    				if (mdetails.getDrDebit() == 1) {
    					
    					debitAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalDebit = totalDebit + mdetails.getDrAmount();
    					
    				} else {
    					
    					creditAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalCredit = totalCredit + mdetails.getDrAmount(); 
    					
    				}
    				
    				ArrayList classList = new ArrayList();
    				classList.add(mdetails.getDrClass());
    				
    				ArJournalList arJRList = new ArJournalList(actionForm,
    				    mdetails.getDrCode(), 
    				    Common.convertShortToString(mdetails.getDrLine()),
    				    mdetails.getDrCoaAccountNumber(),
    				    debitAmount,
    				    creditAmount,
    	    		    mdetails.getDrClass(),
    				    mdetails.getDrCoaAccountDescription());
    				
    				arJRList.setDrClassList(classList);
    				        				
    				actionForm.saveArJRList(arJRList);
    				        				
    			}
    			
    			actionForm.setTotalDebit(Common.convertDoubleToStringMoney(totalDebit, precisionUnit));
    			actionForm.setTotalCredit(Common.convertDoubleToStringMoney(totalCredit, precisionUnit));
    			
    			int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();
		        
		        for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	ArJournalList arJRList = new ArJournalList(actionForm,
			        	    null, String.valueOf(x), null, null, null, null, null);
			        	    
			        arJRList.setDrClassList(this.getDrClassList());
			        	
			        actionForm.saveArJRList(arJRList);
			        			        	
		        }
		        
		        this.setFormProperties(actionForm);    			    				      
    				            	
            } catch (GlobalNoRecordFoundException ex) {
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

            if (!errors.isEmpty()) {
				
				saveErrors(request, new ActionMessages(errors));

			} else {
				
				if ((request.getParameter("saveButton") != null &&
						actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

					actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

				}
				
			}
            
			actionForm.reset(mapping, request);
			
			return(mapping.findForward("arJournal"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArJournalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
   
    private void setFormProperties(ArJournalForm actionForm) {
    	
    	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
    		
    		if (actionForm.getEnableFields().equals("true")) {
    		
	    		actionForm.setShowSaveButton(true);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
			
    		} else {
    			
    			actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
    			
    		}
    		
    	} else {
    		
    		actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
    		    		
    	}

   }
    
    private ArrayList getDrClassList() {
		
		ArrayList classList = new ArrayList();
		
     	classList.add("OTHER");
     	
     	return classList;
		
	}
   
}