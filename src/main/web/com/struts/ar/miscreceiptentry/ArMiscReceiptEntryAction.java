package com.struts.ar.miscreceiptentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ArMiscReceiptEntryController;
import com.ejb.txn.ArMiscReceiptEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModReceiptDetails;
import com.util.ArReceiptDetails;
import com.util.ArSalespersonDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.ArTaxCodeDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ArMiscReceiptEntryAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArMiscReceiptEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         ArMiscReceiptEntryForm actionForm = (ArMiscReceiptEntryForm)form;

	      // reset report

	      actionForm.setReport(null);
	      actionForm.setAttachment(null);
	      actionForm.setAttachmentPDF(null);

         String frParam = null;

         if (request.getParameter("child") == null) {

         	frParam = Common.getUserPermission(user, Constants.AR_MISC_RECEIPT_ENTRY_ID);

         } else {

         	frParam = Constants.FULL_ACCESS;

         }

         if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arMiscReceiptEntry"));

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArMiscReceiptEntryController EJB
*******************************************************/

         ArMiscReceiptEntryControllerHome homeRCT = null;
         ArMiscReceiptEntryController ejbRCT = null;

         try {

            homeRCT = (ArMiscReceiptEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArMiscReceiptEntryControllerEJB", ArMiscReceiptEntryControllerHome.class);

         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArMiscReceiptEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {

            ejbRCT = homeRCT.create();

         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in ArMiscReceiptEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ArMiscReceiptEntryController EJB
   getGlFcPrecisionUnit
   getAdPrfArInvoiceLineNumber
   getAdPrfEnableArReceiptBatch
   getInvGpQuantityPrecisionUnit
   getAdPrfEnableInvShift
   getAdPrfArUseCustomerPulldown
   getAdPrfArDisableSalesPrice
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

         short precisionUnit = 0;
         short invoiceLineNumber = 0;

         boolean enableMiscReceiptBatch = false;
         boolean isInitialPrinting = false;
         boolean enableShift = false;
         boolean useCustomerPulldown = true;
         boolean disableSalesPrice = true;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ar/Misc Receipt/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
         String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
         String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

         try {

            precisionUnit = ejbRCT.getGlFcPrecisionUnit(user.getCmpCode());
            actionForm.setPrecisionUnit(precisionUnit);
            invoiceLineNumber = ejbRCT.getAdPrfArInvoiceLineNumber(user.getCmpCode());
            enableMiscReceiptBatch = Common.convertByteToBoolean(ejbRCT.getAdPrfEnableArMiscReceiptBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableMiscReceiptBatch);
            enableShift = Common.convertByteToBoolean(ejbRCT.getAdPrfEnableInvShift(user.getCmpCode()));
            actionForm.setShowShift(enableShift);
            useCustomerPulldown = Common.convertByteToBoolean(ejbRCT.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            disableSalesPrice = Common.convertByteToBoolean(ejbRCT.getAdPrfArDisableSalesPrice(user.getCmpCode()));
            actionForm.setDisableSalesPrice(disableSalesPrice);


            for(int i = 0; i < actionForm.getArRLIListSize(); i++) {

                ArMiscReceiptLineItemList arRLIList = actionForm.getArRLIByIndex(i);

                if(arRLIList.getItemName() != null && arRLIList.getItemName().length() > 0) {

                    boolean isAssemblyItem = ejbRCT.getInvItemClassByIiName(arRLIList.getItemName(), user.getCmpCode()).equals("Assembly");
                    arRLIList.setIsAssemblyItem(isAssemblyItem);

                }
            }

         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }


/*******************************************************
   -- Ar RCT Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("MEMO LINES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ArReceiptDetails details = new ArReceiptDetails();

           details.setRctCode(actionForm.getReceiptCode());
           details.setRctDocumentType(actionForm.getDocumentType());
           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setRctNumber(actionForm.getReceiptNumber());
           details.setRctReferenceNumber(actionForm.getReferenceNumber());
           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
           details.setRctDescription(actionForm.getDescription());
           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
           details.setRctPaymentMethod(actionForm.getPaymentMethod());
           details.setRctCustomerDeposit(Common.convertBooleanToByte(actionForm.getCustomerDeposit()));
           details.setRctCustomerName(actionForm.getCustomerName());
           details.setRctCustomerAddress(actionForm.getCustomerAddress());
           details.setRctInvtrInvestorFund(Common.convertBooleanToByte(actionForm.getInvtrInvestorFund()));
           details.setRctInvtrNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtrNextRunDate()));

           if(actionForm.getIsInvestorSupplier()){
                  details.setRctInvtrBeginningBalance(Common.convertBooleanToByte(actionForm.getInvestorBeginningBalance()));
           }

           if (actionForm.getReceiptCode() == null) {

           	   details.setRctCreatedBy(user.getUserName());
	           details.setRctDateCreated(new java.util.Date());

           }

           details.setRctLastModifiedBy(user.getUserName());
           details.setRctDateLastModified(new java.util.Date());

           details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

           ArrayList ilList = new ArrayList();

           for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

           	   ArMiscReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

           	   if (Common.validateRequired(arRCTList.getMemoLine()) &&
	      	 	     Common.validateRequired(arRCTList.getDescription()) &&
	      	 	     Common.validateRequired(arRCTList.getQuantity()) &&
	      	 	     Common.validateRequired(arRCTList.getUnitPrice()) &&
	      	 	     Common.validateRequired(arRCTList.getAmount())) continue;

           	   ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

           	   mdetails.setIlSmlName(arRCTList.getMemoLine());
           	   mdetails.setIlDescription(arRCTList.getDescription());
           	   mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arRCTList.getQuantity(), precisionUnit));
           	   mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arRCTList.getUnitPrice(), precisionUnit));
           	   mdetails.setIlAmount(Common.convertStringMoneyToDouble(arRCTList.getAmount(), precisionUnit));
           	   mdetails.setIlTax(Common.convertBooleanToByte(arRCTList.getTaxable()));




           	   ilList.add(mdetails);

           }

           // validate attachment

          /* String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   	}*/
 			//Remove This Line to Enable Attachment

           try {

        	   Integer receiptCode = ejbRCT.saveArRctEntry(details, actionForm.getBankAccount(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
           	        ilList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        	   actionForm.setReceiptCode(receiptCode);
           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid"));

           } catch (GlobalRecordAlreadyAssignedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }

        // save attachment

           /*if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }*/
       	   //Remove This Line to Enable Attachment

/*******************************************************
   -- Ar RCT Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("MEMO LINES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ArReceiptDetails details = new ArReceiptDetails();

           details.setRctCode(actionForm.getReceiptCode());
           details.setRctDocumentType(actionForm.getDocumentType());
           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setRctNumber(actionForm.getReceiptNumber());
           details.setRctReferenceNumber(actionForm.getReferenceNumber());
           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
           details.setRctDescription(actionForm.getDescription());
           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
           details.setRctPaymentMethod(actionForm.getPaymentMethod());
           details.setRctCustomerDeposit(Common.convertBooleanToByte(actionForm.getCustomerDeposit()));
           details.setRctCustomerName(actionForm.getCustomerName());
           details.setRctCustomerAddress(actionForm.getCustomerAddress());
           details.setRctInvtrInvestorFund(Common.convertBooleanToByte(actionForm.getInvtrInvestorFund()));
           details.setRctInvtrNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtrNextRunDate()));

           if(actionForm.getIsInvestorSupplier()){
                  details.setRctInvtrBeginningBalance(Common.convertBooleanToByte(actionForm.getInvestorBeginningBalance()));
           }


           if (actionForm.getReceiptCode() == null) {

           	   details.setRctCreatedBy(user.getUserName());
	           details.setRctDateCreated(new java.util.Date());

           }

           details.setRctLastModifiedBy(user.getUserName());
           details.setRctDateLastModified(new java.util.Date());

           details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));
           ArrayList ilList = new ArrayList();

           for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

           	   ArMiscReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

           	   if (Common.validateRequired(arRCTList.getMemoLine()) &&
	      	 	     Common.validateRequired(arRCTList.getDescription()) &&
	      	 	     Common.validateRequired(arRCTList.getQuantity()) &&
	      	 	     Common.validateRequired(arRCTList.getUnitPrice()) &&
	      	 	     Common.validateRequired(arRCTList.getAmount())) continue;

           	   ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

           	   mdetails.setIlSmlName(arRCTList.getMemoLine());
           	   mdetails.setIlDescription(arRCTList.getDescription());
           	   mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arRCTList.getQuantity(), precisionUnit));
           	   mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arRCTList.getUnitPrice(), precisionUnit));
           	   mdetails.setIlAmount(Common.convertStringMoneyToDouble(arRCTList.getAmount(), precisionUnit));
           	   mdetails.setIlTax(Common.convertBooleanToByte(arRCTList.getTaxable()));

           	   ilList.add(mdetails);

           }

           // validate attachment
           // Remove This Line to Enable Attachment
          /* String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   	}*/
    	   //Remove This Line to Enable Attachment*/

           try {

           	    Integer receiptCode = ejbRCT.saveArRctEntry(details, actionForm.getBankAccount(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
           	        ilList, false, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setReceiptCode(receiptCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));


           } catch (GlobalBranchAccountNumberInvalidException ex) {

           	errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid"));

           } catch (GlobalRecordAlreadyAssignedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

           } catch (EJBException ex) {

           	if (log.isInfoEnabled()) {

           		log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
           				" session: " + session.getId());
           	}

               return(mapping.findForward("cmnErrorPage"));
           }
        // save attachment

           /*if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }*/
       	   //Remove This Line to Enable Attachment*/


/*******************************************************
	 -- Ar RCT Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null && actionForm.getType().equals("MEMO LINES")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ArReceiptDetails details = new ArReceiptDetails();

	           details.setRctCode(actionForm.getReceiptCode());
	           details.setRctDocumentType(actionForm.getDocumentType());
	           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setRctNumber(actionForm.getReceiptNumber());
	           details.setRctReferenceNumber(actionForm.getReferenceNumber());
	           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
	           details.setRctDescription(actionForm.getDescription());
	           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
	           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
	           details.setRctCustomerDeposit(Common.convertBooleanToByte(actionForm.getCustomerDeposit()));
	           details.setRctCustomerName(actionForm.getCustomerName());
	           details.setRctCustomerAddress(actionForm.getCustomerAddress());
	           details.setRctInvtrInvestorFund(Common.convertBooleanToByte(actionForm.getInvtrInvestorFund()));
	           details.setRctInvtrNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtrNextRunDate()));

	           if (actionForm.getReceiptCode() == null) {

	           	   details.setRctCreatedBy(user.getUserName());
		           details.setRctDateCreated(new java.util.Date());

	           }

	           details.setRctLastModifiedBy(user.getUserName());
	           details.setRctDateLastModified(new java.util.Date());

	           ArrayList ilList = new ArrayList();

	           for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

	           	   ArMiscReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

	           	   if (Common.validateRequired(arRCTList.getMemoLine()) &&
		      	 	     Common.validateRequired(arRCTList.getDescription()) &&
		      	 	     Common.validateRequired(arRCTList.getQuantity()) &&
		      	 	     Common.validateRequired(arRCTList.getUnitPrice()) &&
		      	 	     Common.validateRequired(arRCTList.getAmount())) continue;

	           	   ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

	           	   mdetails.setIlSmlName(arRCTList.getMemoLine());
	           	   mdetails.setIlDescription(arRCTList.getDescription());
	           	   mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arRCTList.getQuantity(), precisionUnit));
	           	   mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arRCTList.getUnitPrice(), precisionUnit));
	           	   mdetails.setIlAmount(Common.convertStringMoneyToDouble(arRCTList.getAmount(), precisionUnit));
	           	   mdetails.setIlTax(Common.convertBooleanToByte(arRCTList.getTaxable()));

	           	   ilList.add(mdetails);

	           }

	           // validate attachment
	           //Remove This Line to Enable Attachment
	           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	           if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   	}*/
	    	   //Remove This Line to Enable Attachment*/
	           try {

	           	    Integer receiptCode = ejbRCT.saveArRctEntry(details, actionForm.getBankAccount(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
	           	        ilList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	    actionForm.setReceiptCode(receiptCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

	           } catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));

	           } catch (GlobalBranchAccountNumberInvalidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid"));

	            } catch (GlobalRecordAlreadyAssignedException ex) {

	               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	        // save attachment


	           /*if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }*/
	       	//Remove This Line to Enable Attachment*/

	           if (!errors.isEmpty()) {

	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("arMiscReceiptEntry"));

	           }

	           actionForm.setReport(Constants.STATUS_SUCCESS);

	           isInitialPrinting = true;

	        }  else {

	        	actionForm.setReport(Constants.STATUS_SUCCESS);

		        	return(mapping.findForward("arMiscReceiptEntry"));

	        }

/*******************************************************
   -- Ar RCT RLI Save As Draft Action --
*******************************************************/

       } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ArReceiptDetails details = new ArReceiptDetails();

           details.setRctCode(actionForm.getReceiptCode());
           details.setRctDocumentType(actionForm.getDocumentType());
           System.out.println("actionForm.getReceiptCode()" + actionForm.getReceiptCode());
           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setRctNumber(actionForm.getReceiptNumber());
           details.setRctReferenceNumber(actionForm.getReferenceNumber());
           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
           details.setRctDescription(actionForm.getDescription());

           details.setRctChequeNumber(actionForm.getChequeNumber());
           details.setRctVoucherNumber(actionForm.getVoucherNumber());
           details.setRctCardNumber1(actionForm.getCardNumber1());
           details.setRctCardNumber2(actionForm.getCardNumber2());
           details.setRctCardNumber3(actionForm.getCardNumber3());
           details.setRctAmountCash(Common.convertStringMoneyToDouble(actionForm.getAmountCash(), precisionUnit));
           details.setRctAmountCheque(Common.convertStringMoneyToDouble(actionForm.getAmountCheque(), precisionUnit));
           details.setRctAmountVoucher(Common.convertStringMoneyToDouble(actionForm.getAmountVoucher(), precisionUnit));

           details.setRctAmountCard1(Common.convertStringMoneyToDouble(actionForm.getAmountCard1(), precisionUnit));
           details.setRctAmountCard2(Common.convertStringMoneyToDouble(actionForm.getAmountCard2(), precisionUnit));
           details.setRctAmountCard3(Common.convertStringMoneyToDouble(actionForm.getAmountCard3(), precisionUnit));


           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit));
           details.setRctLvShift(actionForm.getShift());
           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
           details.setRctPaymentMethod(actionForm.getPaymentMethod());
           System.out.println("actionForm.getCustomerName()-" + actionForm.getCustomerName());
           details.setRctCustomerName(actionForm.getCustomerName());
           details.setRctCustomerAddress(actionForm.getCustomerAddress());


           details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));
           if (actionForm.getReceiptCode() == null) {

           	   details.setRctCreatedBy(user.getUserName());
	           details.setRctDateCreated(new java.util.Date());

           }

           details.setRctLastModifiedBy(user.getUserName());
           details.setRctDateLastModified(new java.util.Date());

           ArrayList iliList = new ArrayList();

           for (int i = 0; i<actionForm.getArRLIListSize(); i++) {

           	   ArMiscReceiptLineItemList arRLIList = actionForm.getArRLIByIndex(i);

           	   if (Common.validateRequired(arRLIList.getLocation()) &&
           	   	   Common.validateRequired(arRLIList.getItemName()) &&
				   Common.validateRequired(arRLIList.getUnit()) &&
				   Common.validateRequired(arRLIList.getUnitPrice()) &&
				   Common.validateRequired(arRLIList.getQuantity())) continue;

           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

           	   mdetails.setIliLine(Common.convertStringToShort(arRLIList.getLineNumber()));
           	   mdetails.setIliIiName(arRLIList.getItemName());
           	   mdetails.setIliLocName(arRLIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arRLIList.getQuantity(), precisionUnit));
           	   mdetails.setIliUomName(arRLIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arRLIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arRLIList.getAmount(), precisionUnit));
           	   mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arRLIList.getAutoBuildCheckbox()));
           	   mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arRLIList.getDiscount1(), precisionUnit));
           	   mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arRLIList.getDiscount2(), precisionUnit));
           	   mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arRLIList.getDiscount3(), precisionUnit));
           	   mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arRLIList.getDiscount4(), precisionUnit));
           	   mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arRLIList.getTotalDiscount(), precisionUnit));
           	   mdetails.setIliMisc(arRLIList.getMisc());
           	   mdetails.setIliTax(Common.convertBooleanToByte(arRLIList.getTax().equals("Y")?true:false));
        	   if(arRLIList.getIsAssemblyItem() == true) {
        	   		mdetails.setIliIiClass("Assembly");
        	   } else {
        	   		mdetails.setIliIiClass("Stock");
        	   }



        	   ArrayList tagList = new ArrayList();
         	   //TODO:save and submit taglist
         	   boolean isTraceMisc = ejbRCT.getArTraceMisc(arRLIList.getItemName(), user.getCmpCode());
	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
				String misc= arRLIList.getMisc();

	      	   if (isTraceMisc){

	      		   System.out.println("MISC RECIPT misc: " + misc);
	      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	      		   mdetails.setIliTagList(tagList);

	      	   }

           	   iliList.add(mdetails);

           }


           // validate attachment
           ///*
           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           System.out.println("filename1: " + filename1);
           System.out.println("filename2: " + filename2);

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   	}*/
//*/

           try {

           	    Integer rctCode = ejbRCT.saveArRctIliEntry(details,
           	    	actionForm.getBankAccount(), actionForm.getBankAccountCard1(), actionForm.getBankAccountCard2(), actionForm.getBankAccountCard3(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
           	        iliList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	 actionForm.setReceiptCode(rctCode);

           	 System.out.println("details.getRctCode()" + details.getRctCode());
           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));

           } catch (GlobalInventoryDateException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("miscReceiptEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

           } catch (GlobalRecordAlreadyAssignedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("miscReceiptEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalRecordInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
            	  new ActionMessage("miscReceiptEntry.error.insufficientStocks", ex.getMessage()));

           } catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

         }catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }


        // save attachment

           /*if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }*/
//*/
/*******************************************************
   -- Ar RCT Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           ArReceiptDetails details = new ArReceiptDetails();

           details.setRctCode(actionForm.getReceiptCode());
           details.setRctDocumentType(actionForm.getDocumentType());
           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setRctNumber(actionForm.getReceiptNumber());
           details.setRctReferenceNumber(actionForm.getReferenceNumber());
           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
           details.setRctDescription(actionForm.getDescription());
           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setRctLvShift(actionForm.getShift());
           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
           details.setRctPaymentMethod(actionForm.getPaymentMethod());
           details.setRctCustomerName(actionForm.getCustomerName());
           details.setRctCustomerAddress(actionForm.getCustomerAddress());


           details.setRctChequeNumber(actionForm.getChequeNumber());
           details.setRctVoucherNumber(actionForm.getVoucherNumber());
           details.setRctCardNumber1(actionForm.getCardNumber1());
           details.setRctCardNumber2(actionForm.getCardNumber2());
           details.setRctCardNumber3(actionForm.getCardNumber3());
           details.setRctAmountCash(Common.convertStringMoneyToDouble(actionForm.getAmountCash(), precisionUnit));
           details.setRctAmountCheque(Common.convertStringMoneyToDouble(actionForm.getAmountCheque(), precisionUnit));
           details.setRctAmountVoucher(Common.convertStringMoneyToDouble(actionForm.getAmountVoucher(), precisionUnit));

           details.setRctAmountCard1(Common.convertStringMoneyToDouble(actionForm.getAmountCard1(), precisionUnit));
           details.setRctAmountCard2(Common.convertStringMoneyToDouble(actionForm.getAmountCard2(), precisionUnit));
           details.setRctAmountCard3(Common.convertStringMoneyToDouble(actionForm.getAmountCard3(), precisionUnit));

           details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

           if (actionForm.getReceiptCode() == null) {

           	   details.setRctCreatedBy(user.getUserName());
	           details.setRctDateCreated(new java.util.Date());

           }

           details.setRctLastModifiedBy(user.getUserName());
           details.setRctDateLastModified(new java.util.Date());

           ArrayList iliList = new ArrayList();

           for (int i = 0; i<actionForm.getArRLIListSize(); i++) {

           	   ArMiscReceiptLineItemList arRLIList = actionForm.getArRLIByIndex(i);

           	   if (Common.validateRequired(arRLIList.getLocation()) &&
           	   	   Common.validateRequired(arRLIList.getItemName()) &&
				   Common.validateRequired(arRLIList.getUnit()) &&
				   Common.validateRequired(arRLIList.getUnitPrice()) &&
				   Common.validateRequired(arRLIList.getQuantity())) continue;

           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

           	   mdetails.setIliLine(Common.convertStringToShort(arRLIList.getLineNumber()));
           	   mdetails.setIliIiName(arRLIList.getItemName());
           	   mdetails.setIliLocName(arRLIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arRLIList.getQuantity(), precisionUnit));
           	   mdetails.setIliUomName(arRLIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arRLIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arRLIList.getAmount(), precisionUnit));
           	   mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arRLIList.getAutoBuildCheckbox()));
           	   mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arRLIList.getDiscount1(), precisionUnit));
           	   mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arRLIList.getDiscount2(), precisionUnit));
           	   mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arRLIList.getDiscount3(), precisionUnit));
           	   mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arRLIList.getDiscount4(), precisionUnit));
           	   mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arRLIList.getTotalDiscount(), precisionUnit));
           	   mdetails.setIliMisc(arRLIList.getMisc());
           	   mdetails.setIliTax(Common.convertBooleanToByte(arRLIList.getTax().equals("Y")?true:false));
	     	   if(arRLIList.getIsAssemblyItem() == true) {
	     	   		mdetails.setIliIiClass("Assembly");
	     	   } else {
	     	   		mdetails.setIliIiClass("Stock");
	     	   }


	     	  ArrayList tagList = new ArrayList();
        	   //TODO:save and submit taglist
        	   boolean isTraceMisc = ejbRCT.getArTraceMisc(arRLIList.getItemName(), user.getCmpCode());
	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
				String misc= arRLIList.getMisc();

	      	   if (isTraceMisc){


	      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	      		   mdetails.setIliTagList(tagList);

	      	   }


           	   iliList.add(mdetails);

           }


           // validate attachment
           ///*
           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   	}*/
//*/

           try {

           	    Integer receiptCode = ejbRCT.saveArRctIliEntry(details,
           	    		actionForm.getBankAccount(), actionForm.getBankAccountCard1(), actionForm.getBankAccountCard2(), actionForm.getBankAccountCard3(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
           	        iliList, false, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setReceiptCode(receiptCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));

           } catch (GlobalInventoryDateException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("miscReceiptEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

           } catch (GlobalRecordAlreadyAssignedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("miscReceiptEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalRecordInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
            	  new ActionMessage("miscReceiptEntry.error.insufficientStocks", ex.getMessage()));

           } catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

         }catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }


        // save attachment

           /*if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }*/
           //*/

/*******************************************************
	 -- Ar RCT Print Action --
*******************************************************/

     } else if (request.getParameter("printButton") != null && actionForm.getType().equals("ITEMS")) {

     	if(Common.validateRequired(actionForm.getApprovalStatus())) {

           ArReceiptDetails details = new ArReceiptDetails();

           details.setRctCode(actionForm.getReceiptCode());
           details.setRctDocumentType(actionForm.getDocumentType());
           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setRctNumber(actionForm.getReceiptNumber());
           details.setRctReferenceNumber(actionForm.getReferenceNumber());
           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
           details.setRctDescription(actionForm.getDescription());
           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setRctLvShift(actionForm.getShift());
           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
           details.setRctCustomerName(actionForm.getCustomerName());
           details.setRctCustomerAddress(actionForm.getCustomerAddress());

           details.setRctChequeNumber(actionForm.getChequeNumber());
           details.setRctVoucherNumber(actionForm.getVoucherNumber());
           details.setRctCardNumber1(actionForm.getCardNumber1());
           details.setRctCardNumber2(actionForm.getCardNumber2());
           details.setRctCardNumber3(actionForm.getCardNumber3());
           details.setRctAmountCash(Common.convertStringMoneyToDouble(actionForm.getAmountCash(), precisionUnit));
           details.setRctAmountCheque(Common.convertStringMoneyToDouble(actionForm.getAmountCheque(), precisionUnit));
           details.setRctAmountVoucher(Common.convertStringMoneyToDouble(actionForm.getAmountVoucher(), precisionUnit));

           details.setRctAmountCard1(Common.convertStringMoneyToDouble(actionForm.getAmountCard1(), precisionUnit));
           details.setRctAmountCard2(Common.convertStringMoneyToDouble(actionForm.getAmountCard2(), precisionUnit));
           details.setRctAmountCard3(Common.convertStringMoneyToDouble(actionForm.getAmountCard3(), precisionUnit));



           if (actionForm.getReceiptCode() == null) {

           	   details.setRctCreatedBy(user.getUserName());
	           details.setRctDateCreated(new java.util.Date());

           }

           details.setRctLastModifiedBy(user.getUserName());
           details.setRctDateLastModified(new java.util.Date());

           ArrayList iliList = new ArrayList();

           for (int i = 0; i<actionForm.getArRLIListSize(); i++) {

           	   ArMiscReceiptLineItemList arRLIList = actionForm.getArRLIByIndex(i);

           	   if (Common.validateRequired(arRLIList.getLocation()) &&
           	   	   Common.validateRequired(arRLIList.getItemName()) &&
				   Common.validateRequired(arRLIList.getUnit()) &&
				   Common.validateRequired(arRLIList.getUnitPrice()) &&
				   Common.validateRequired(arRLIList.getQuantity())) continue;

           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

           	   mdetails.setIliLine(Common.convertStringToShort(arRLIList.getLineNumber()));
           	   mdetails.setIliIiName(arRLIList.getItemName());
           	   mdetails.setIliLocName(arRLIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arRLIList.getQuantity(), precisionUnit));
           	   mdetails.setIliUomName(arRLIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arRLIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arRLIList.getAmount(), precisionUnit));
           	   mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arRLIList.getAutoBuildCheckbox()));
           	   mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arRLIList.getDiscount1(), precisionUnit));
           	   mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arRLIList.getDiscount2(), precisionUnit));
           	   mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arRLIList.getDiscount3(), precisionUnit));
           	   mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arRLIList.getDiscount4(), precisionUnit));
           	   mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arRLIList.getTotalDiscount(), precisionUnit));
           	   mdetails.setIliMisc(arRLIList.getMisc());
           	   mdetails.setIliTax(Common.convertBooleanToByte(arRLIList.getTax().equals("Y")?true:false));
	     	   if(arRLIList.getIsAssemblyItem() == true) {
	     	   		mdetails.setIliIiClass("Assembly");
	     	   } else {
	     	   		mdetails.setIliIiClass("Stock");
	     	   }


	     	  ArrayList tagList = new ArrayList();
        	   //TODO:save and submit taglist
        	   boolean isTraceMisc = ejbRCT.getArTraceMisc(arRLIList.getItemName(), user.getCmpCode());
	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
				String misc= arRLIList.getMisc();

	      	   if (isTraceMisc){


	      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
	      		   mdetails.setIliTagList(tagList);

	      	   }



           	   iliList.add(mdetails);

           }


           // validate attachment
           //*
           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.filename4NotFound"));

	   	    	} else {

	   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4Invalid"));

	          	    	}

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("miscReceiptEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arMiscReceiptEntry"));

	   	    	}

	   	   	}*/
//*/

           try {

           	    Integer receiptCode = ejbRCT.saveArRctIliEntry(details,
           	    		actionForm.getBankAccount(), actionForm.getBankAccountCard1(), actionForm.getBankAccountCard2(), actionForm.getBankAccountCard3(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
           	        iliList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           	    actionForm.setReceiptCode(receiptCode);

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

       	   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
               	    new ActionMessage("miscReceiptEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));

           } catch (GlobalInventoryDateException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("miscReceiptEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

           } catch (GlobalRecordAlreadyAssignedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("miscReceiptEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

         }catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }

               return(mapping.findForward("cmnErrorPage"));
           }


        // save attachment

           /*if (!Common.validateRequired(filename1)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename1().getInputStream();

       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

       	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename2)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename2().getInputStream();

       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename3)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename3().getInputStream();

       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }

       	   if (!Common.validateRequired(filename4)) {

       	        if (errors.isEmpty()) {

       	        	InputStream is = actionForm.getFilename4().getInputStream();

       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

       	        	new File(attachmentPath).mkdirs();

       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

       	    			fileExtension = attachmentFileExtension;

           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
           	    		fileExtension = attachmentFileExtensionPDF;

           	    	}
       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

	            	int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

       	        }

       	   }*/
//*/

           if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arMiscReceiptEntry"));

           }

           actionForm.setReport(Constants.STATUS_SUCCESS);

           isInitialPrinting = true;

        }  else {

        	actionForm.setReport(Constants.STATUS_SUCCESS);

	        	return(mapping.findForward("arMiscReceiptEntry"));

        }

/*******************************************************
   -- Ar RCT Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar RCT Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {

         	int listSize = actionForm.getArRCTListSize();
         	int lineNumber = 0;

            for (int x = listSize + 1; x <= listSize + invoiceLineNumber; x++) {

	        	ArMiscReceiptEntryList arRCTList = new ArMiscReceiptEntryList(actionForm,
	        	    null, String.valueOf(++lineNumber), null, null, null, null, null, false);

	        	arRCTList.setMemoLineList(actionForm.getMemoLineList());

	        	actionForm.saveArRCTList(arRCTList);

	        }

	        for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

           	   ArMiscReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

           	   arRCTList.setLineNumber(String.valueOf(i+1));

            }

	        return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {

         	for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

           	   ArMiscReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

           	   if (arRCTList.getDeleteCheckbox()) {

           	   	   actionForm.deleteArRCTList(i);
           	   	   i--;
           	   }

            }

	        return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT ILI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	int listSize = actionForm.getArRLIListSize();

            for (int x = listSize + 1; x <= listSize + invoiceLineNumber; x++) {

	        	ArMiscReceiptLineItemList arRLIList = new ArMiscReceiptLineItemList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null, null, null, false, false, null, null, null,
					null, null, null, null , "Y");

	        	arRLIList.setLocationList(actionForm.getLocationList());

	        	actionForm.saveArRLIList(arRLIList);

	        }

	        return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT ILI Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("ITEMS")) {

         	for (int i = 0; i<actionForm.getArRLIListSize(); i++) {

           	   ArMiscReceiptLineItemList arRLIList = actionForm.getArRLIByIndex(i);

           	   if (arRLIList.getDeleteCheckbox()) {

           	   	   actionForm.deleteArRLIList(i);
           	   	   i--;
           	   }

            }

            for (int i = 0; i<actionForm.getArRLIListSize(); i++) {

           	   ArMiscReceiptLineItemList arRLIList = actionForm.getArRLIByIndex(i);

           	   arRLIList.setLineNumber(String.valueOf(i+1));

            }

	        return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("MEMO LINES")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ArReceiptDetails details = new ArReceiptDetails();

	           details.setRctCode(actionForm.getReceiptCode());
	           details.setRctDocumentType(actionForm.getDocumentType());
	           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setRctNumber(actionForm.getReceiptNumber());
	           details.setRctReferenceNumber(actionForm.getReferenceNumber());
	           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
	           details.setRctDescription(actionForm.getDescription());
	           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
	           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
	           details.setRctPaymentMethod(actionForm.getPaymentMethod());
	           details.setRctCustomerDeposit(Common.convertBooleanToByte(actionForm.getCustomerDeposit()));
	           details.setRctCustomerName(actionForm.getCustomerName());
	           details.setRctCustomerAddress(actionForm.getCustomerAddress());
	           details.setRctInvtrInvestorFund(Common.convertBooleanToByte(actionForm.getInvtrInvestorFund()));
	           details.setRctInvtrNextRunDate(Common.convertStringToSQLDate(actionForm.getInvtrNextRunDate()));
	           details.setRctInvtrBeginningBalance(Common.convertBooleanToByte(actionForm.getInvestorBeginningBalance()));
	           if (actionForm.getReceiptCode() == null) {

	           	   details.setRctCreatedBy(user.getUserName());
		           details.setRctDateCreated(new java.util.Date());

	           }

	           details.setRctLastModifiedBy(user.getUserName());
	           details.setRctDateLastModified(new java.util.Date());

	           details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

	           ArrayList ilList = new ArrayList();

	           for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

	           	   ArMiscReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

	           	   if (Common.validateRequired(arRCTList.getMemoLine()) &&
		      	 	     Common.validateRequired(arRCTList.getDescription()) &&
		      	 	     Common.validateRequired(arRCTList.getQuantity()) &&
		      	 	     Common.validateRequired(arRCTList.getUnitPrice()) &&
		      	 	     Common.validateRequired(arRCTList.getAmount())) continue;

	           	   ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

	           	   mdetails.setIlSmlName(arRCTList.getMemoLine());
	           	   mdetails.setIlDescription(arRCTList.getDescription());
	           	   mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arRCTList.getQuantity(), precisionUnit));
	           	   mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arRCTList.getUnitPrice(), precisionUnit));
	           	   mdetails.setIlAmount(Common.convertStringMoneyToDouble(arRCTList.getAmount(), precisionUnit));
	           	   mdetails.setIlTax(Common.convertBooleanToByte(arRCTList.getTaxable()));

	           	   ilList.add(mdetails);

	           }

//	         validate attachment
	           //*
	           /*String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	           if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("miscReceiptEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("miscReceiptEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arMiscReceiptEntry"));

		   	    	}

		   	   	}
	    	   */
	           try {

	           	    Integer miscReceiptCode = ejbRCT.saveArRctEntry(details, actionForm.getBankAccount(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
	           	        ilList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	    actionForm.setReceiptCode(miscReceiptCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

	           } catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));

	           } catch (GlobalBranchAccountNumberInvalidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid"));

	            } catch (GlobalRecordAlreadyAssignedException ex) {

	               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }

	        // save attachment

	           /*if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }


	           if (!errors.isEmpty()) {

	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("arMiscReceiptEntry"));

	           }*/

         	}



  	        String path = "/arJournal.do?forward=1" +
			     "&transactionCode=" + actionForm.getReceiptCode() +
			     "&transaction=MISC RECEIPT" +
			     "&transactionNumber=" + actionForm.getReceiptNumber() +
			     "&transactionDate=" + actionForm.getDate() +
			     "&transactionEnableFields=" + actionForm.getEnableFields();

			return(new ActionForward(path));

/*******************************************************
   -- Ar INV Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbRCT.deleteArRctEntry(actionForm.getReceiptCode(), user.getUserName(), user.getCmpCode());

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());

                }

                return(mapping.findForward("cmnErrorPage"));

            }

/*******************************************************
   -- Ar RCT Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null && actionForm.getType().equals("ITEMS")) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	           ArReceiptDetails details = new ArReceiptDetails();

	           details.setRctCode(actionForm.getReceiptCode());
	           details.setRctDocumentType(actionForm.getDocumentType());
	           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
	           details.setRctNumber(actionForm.getReceiptNumber());
	           details.setRctReferenceNumber(actionForm.getReferenceNumber());
	           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
	           details.setRctDescription(actionForm.getDescription());
	           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
	           details.setRctLvShift(actionForm.getShift());
	           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
	           details.setRctPaymentMethod(actionForm.getPaymentMethod());
	           details.setRctCustomerName(actionForm.getCustomerName());
	           details.setRctCustomerAddress(actionForm.getCustomerAddress());

	           details.setRctChequeNumber(actionForm.getChequeNumber());
	           details.setRctVoucherNumber(actionForm.getVoucherNumber());
	           details.setRctCardNumber1(actionForm.getCardNumber1());
	           details.setRctCardNumber2(actionForm.getCardNumber2());
	           details.setRctCardNumber3(actionForm.getCardNumber3());
	           details.setRctAmountCash(Common.convertStringMoneyToDouble(actionForm.getAmountCash(), precisionUnit));
	           details.setRctAmountCheque(Common.convertStringMoneyToDouble(actionForm.getAmountCheque(), precisionUnit));
	           details.setRctAmountVoucher(Common.convertStringMoneyToDouble(actionForm.getAmountVoucher(), precisionUnit));

	           details.setRctAmountCard1(Common.convertStringMoneyToDouble(actionForm.getAmountCard1(), precisionUnit));
	           details.setRctAmountCard2(Common.convertStringMoneyToDouble(actionForm.getAmountCard2(), precisionUnit));
	           details.setRctAmountCard3(Common.convertStringMoneyToDouble(actionForm.getAmountCard3(), precisionUnit));


	           if (actionForm.getReceiptCode() == null) {

	           	   details.setRctCreatedBy(user.getUserName());
		           details.setRctDateCreated(new java.util.Date());

	           }

	           details.setRctLastModifiedBy(user.getUserName());
	           details.setRctDateLastModified(new java.util.Date());

	           details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));
	           ArrayList iliList = new ArrayList();

	           for (int i = 0; i<actionForm.getArRLIListSize(); i++) {

	           	   ArMiscReceiptLineItemList arRLIList = actionForm.getArRLIByIndex(i);

	           	   if (Common.validateRequired(arRLIList.getLocation()) &&
	           	   	   Common.validateRequired(arRLIList.getItemName()) &&
					   Common.validateRequired(arRLIList.getUnit()) &&
					   Common.validateRequired(arRLIList.getUnitPrice()) &&
					   Common.validateRequired(arRLIList.getQuantity())) continue;

	           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();

	           	   mdetails.setIliLine(Common.convertStringToShort(arRLIList.getLineNumber()));
	           	   mdetails.setIliIiName(arRLIList.getItemName());
	           	   mdetails.setIliLocName(arRLIList.getLocation());
	           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arRLIList.getQuantity(), precisionUnit));
	           	   mdetails.setIliUomName(arRLIList.getUnit());
	           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arRLIList.getUnitPrice(), precisionUnit));
	           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arRLIList.getAmount(), precisionUnit));
	           	   mdetails.setIliEnableAutoBuild(Common.convertBooleanToByte(arRLIList.getAutoBuildCheckbox()));
	           	   mdetails.setIliDiscount1(Common.convertStringMoneyToDouble(arRLIList.getDiscount1(), precisionUnit));
	           	   mdetails.setIliDiscount2(Common.convertStringMoneyToDouble(arRLIList.getDiscount2(), precisionUnit));
	           	   mdetails.setIliDiscount3(Common.convertStringMoneyToDouble(arRLIList.getDiscount3(), precisionUnit));
	           	   mdetails.setIliDiscount4(Common.convertStringMoneyToDouble(arRLIList.getDiscount4(), precisionUnit));
	           	   mdetails.setIliTotalDiscount(Common.convertStringMoneyToDouble(arRLIList.getTotalDiscount(), precisionUnit));
	           	   mdetails.setIliMisc(arRLIList.getMisc());
	           	   mdetails.setIliTax(Common.convertBooleanToByte(arRLIList.getTax().equals("Y")?true:false));
	        	   if(arRLIList.getIsAssemblyItem() == true) {
	        	   		mdetails.setIliIiClass("Assembly");
	        	   } else {
	        	   		mdetails.setIliIiClass("Stock");
	        	   }

	        	   ArrayList tagList = new ArrayList();
	         	   //TODO:save and submit taglist
	         	   boolean isTraceMisc = ejbRCT.getArTraceMisc(arRLIList.getItemName(), user.getCmpCode());
		           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
					String misc= arRLIList.getMisc();

		      	   if (isTraceMisc){


		      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
		      		   mdetails.setIliTagList(tagList);

		      	   }



	           	   iliList.add(mdetails);

	           }



	           try {

	           	    Integer miscReceiptCode = ejbRCT.saveArRctIliEntry(details,
	           	    	actionForm.getBankAccount(), actionForm.getBankAccountCard1(), actionForm.getBankAccountCard2(), actionForm.getBankAccountCard3(),
	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
	           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
	           	        iliList, true, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	           	    actionForm.setReceiptCode(miscReceiptCode);

	           } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalDocumentNumberNotUniqueException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.documentNumberNotUnique"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.noApprovalApproverFound"));

	           } catch (GlobalInvItemLocationNotFoundException ex) {

       	   	       errors.add(ActionMessages.GLOBAL_MESSAGE,
               	        new ActionMessage("miscReceiptEntry.error.noItemLocationFound", ex.getMessage()));

               } catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.journalNotBalance"));

	           } catch (GlobalInventoryDateException ex) {

	              		errors.add(ActionMessages.GLOBAL_MESSAGE,
	              			new ActionMessage("miscReceiptEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

	           } catch (GlobalBranchAccountNumberInvalidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("miscReceiptEntry.error.branchAccountNumberInvalid", ex.getLineNumberError()));

	           } catch (GlobalRecordAlreadyAssignedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("miscReceiptEntry.error.recordAlreadyAssigned"));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("miscReceiptEntry.error.noNegativeInventoryCostingCOA"));

	           } catch (GlobalMiscInfoIsRequiredException ex) {

	        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

             }catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }

	               return(mapping.findForward("cmnErrorPage"));
	           }


	        // save attachment

	           /*if (!Common.validateRequired(filename1)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename1().getInputStream();

	       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

	       	    		int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename2)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename2().getInputStream();

	       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename3)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename3().getInputStream();

	       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }

	       	   if (!Common.validateRequired(filename4)) {

	       	        if (errors.isEmpty()) {

	       	        	InputStream is = actionForm.getFilename4().getInputStream();

	       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	       	        	new File(attachmentPath).mkdirs();

	       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	       	    			fileExtension = attachmentFileExtension;

	           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	           	    		fileExtension = attachmentFileExtensionPDF;

	           	    	}
	       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

		            	int c;

		            	while ((c = is.read()) != -1) {

		            		fos.write((byte)c);

		            	}

		            	is.close();
						fos.close();

	       	        }

	       	   }*/

	           if (!errors.isEmpty()) {

	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("arMiscReceiptEntry"));

	           }

         	}

  	        String path = "/arJournal.do?forward=1" +
			     "&transactionCode=" + actionForm.getReceiptCode() +
			     "&transaction=MISC RECEIPT" +
			     "&transactionNumber=" + actionForm.getReceiptNumber() +
			     "&transactionDate=" + actionForm.getDate() +
			     "&transactionEnableFields=" + actionForm.getEnableFields();

			return(new ActionForward(path));


/*******************************************************
   -- Ar RCT Customer Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

         	try {

                 ArModCustomerDetails mdetails = ejbRCT.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());


                //detect if customer have supplier
                    if(mdetails.getCstSupplierCode()!=null){
                        actionForm.setIsInvestorSupplier(true);
                    }else{
                        actionForm.setIsInvestorSupplier(false);
                    }

	    		  actionForm.setTaxCode(mdetails.getCstCcTcName());

	    		  /*actionForm.setSalesperson(mdetails.getCstSlpSalespersonCode() != null ?
	    				  mdetails.getCstSlpSalespersonCode() : Constants.GLOBAL_BLANK);
	    		  actionForm.setSalesperson(mdetails.getCstSlpSalespersonCode2() != null ?
	    				  mdetails.getCstSlpSalespersonCode2() : Constants.GLOBAL_BLANK);*/


	    		  if (mdetails.getCstSlpSalespersonCode() != null && mdetails.getCstSlpSalespersonCode2() != null) {

	    			  actionForm.clearSalespersonList();
	    			  actionForm.clearSalespersonNameList();
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode());
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode2());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode() + " - " + mdetails.getCstSlpName());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode2() + " - " + mdetails.getCstSlpName2());

	    		  } else {
	    			  	actionForm.clearSalespersonList();
	    	          	actionForm.clearSalespersonNameList();

	    	          	ArrayList list = ejbRCT.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    	          	if ( list == null || list.size() == 0) {

	    	          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
	    	          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

	    	          	} else {

	    	          		Iterator i =list.iterator();

	    	          		while (i.hasNext()) {

	    	          			ArSalespersonDetails slpDetails = (ArSalespersonDetails)i.next();

	    	          			actionForm.setSalespersonList(slpDetails.getSlpSalespersonCode());
	    	          			actionForm.setSalespersonNameList(slpDetails.getSlpSalespersonCode() + " - " + slpDetails.getSlpName());

	    	          		}

	    	          	}
	    		  }

                 actionForm.setBankAccount(mdetails.getCstAdBaName());
                 actionForm.setTaxCode(mdetails.getCstCcTcName());
                 actionForm.setWithholdingTax(mdetails.getCstCcWtcName());
                 actionForm.setSoldTo(mdetails.getCstBillToAddress());
                 actionForm.setPaymentMethod(mdetails.getCstPaymentMethod());
                 actionForm.setSalesperson(mdetails.getCstSlpSalespersonCode());
	             actionForm.setCustomerName(mdetails.getCstName());
	             actionForm.setTaxRate(mdetails.getCstCcTcRate());
	             actionForm.setTaxType(mdetails.getCstCcTcType());

	             if(actionForm.getType().equalsIgnoreCase("ITEMS") && actionForm.getType() != null) {

	             	if(mdetails.getCstLitName() != null && mdetails.getCstLitName().length() > 0) {

						actionForm.clearArRLIList();

						ArrayList list = ejbRCT.getInvLitByCstLitName(mdetails.getCstLitName(), user.getCmpCode());

						if (!list.isEmpty()) {

							Iterator i = list.iterator();

							while (i.hasNext()) {

								InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();

								ArMiscReceiptLineItemList arRLIList = new ArMiscReceiptLineItemList(actionForm, null,
										Common.convertShortToString(liDetails.getLiLine()), liDetails.getLiLocName(),
										liDetails.getLiIiName(), liDetails.getLiIiDescription(), "0",
										liDetails.getLiUomName(), null, null, false, false, null, null, null, null, null, null, null, "Y");


								String itemClass = ejbRCT.getInvIiClassByIiName(arRLIList.getItemName(), user.getCmpCode());

								if(itemClass.equals("Assembly")) {

									arRLIList.setIsAssemblyItem(true);

								} else {

									arRLIList.setIsAssemblyItem(false);

								}

								if(ejbRCT.getInvAutoBuildEnabledByIiName(arRLIList.getItemName(), user.getCmpCode())) {

									arRLIList.setAutoBuildCheckbox(true);

								} else {

									arRLIList.setAutoBuildCheckbox(false);

								}

								// populate location list

								arRLIList.setLocationList(actionForm.getLocationList());

								// populate unit list

								ArrayList uomList = new ArrayList();
								uomList = ejbRCT.getInvUomByIiName(arRLIList.getItemName(), user.getCmpCode());

								arRLIList.clearUnitList();
								arRLIList.setUnitList(Constants.GLOBAL_BLANK);

								Iterator j = uomList.iterator();

								while (j.hasNext()) {

									InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();

									arRLIList.setUnitList(mUomDetails.getUomName());

								}

								// populate unit cost field

								if (!Common.validateRequired(arRLIList.getItemName()) && !Common.validateRequired(arRLIList.getUnit())) {

									double unitPrice = ejbRCT.getInvIiSalesPriceByIiNameAndUomName(arRLIList.getItemName(), arRLIList.getUnit(), user.getCmpCode());
									arRLIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

								}

								// populate amount field

								if(!Common.validateRequired(arRLIList.getQuantity()) && !Common.validateRequired(arRLIList.getUnitPrice())) {

									double amount = Common.convertStringMoneyToDouble(arRLIList.getQuantity(), precisionUnit) *
									Common.convertStringMoneyToDouble(arRLIList.getUnitPrice(), precisionUnit);
									arRLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

								}

								actionForm.saveArRLIList(arRLIList);

							}

						}

					} else {

						actionForm.clearArRLIList();

						for (int x = 1; x <= invoiceLineNumber; x++) {

							ArMiscReceiptLineItemList arRLIList = new ArMiscReceiptLineItemList(actionForm, null,
									new Integer(x).toString(), null, null, null, null, null, null, null, false, false, "0.0",
									"0.0", "0.0", "0.0", null, "0.0", null, "Y");

							arRLIList.setLocationList(actionForm.getLocationList());

							arRLIList.setUnitList(Constants.GLOBAL_BLANK);
							arRLIList.setUnitList("Select Item First");

							actionForm.saveArRLIList(arRLIList);

						}
					}

	             }else if(actionForm.getType().equalsIgnoreCase("MEMO LINES") && actionForm.getType() != null){


                        actionForm.clearArRCTList();
                        for (int x = 1; x <= invoiceLineNumber; x++) {

                           ArMiscReceiptEntryList arRCTList = new ArMiscReceiptEntryList(actionForm,
                                   null, new Integer(x).toString(), null, null, null, null, null, false);

                           arRCTList.setMemoLineList(actionForm.getMemoLineList());

                           actionForm.saveArRCTList(arRCTList);

                    }
                }

             } catch (GlobalNoRecordFoundException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.customerNoRecordFound"));
                saveErrors(request, new ActionMessages(errors));

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT Memo Line Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("arRCTList[" +
                 actionForm.getRowSelected() + "].isMemoLineEntered")) && actionForm.getType().equals("MEMO LINES")) {

           ArMiscReceiptEntryList arRCTList =
              actionForm.getArRCTByIndex(actionForm.getRowSelected());

         	try {

             //    ArStandardMemoLineDetails smlDetails = ejbRCT.getArSmlBySmlName(arRCTList.getMemoLine(), user.getCmpCode());

                  ArStandardMemoLineDetails smlDetails = ejbRCT.getArSmlByCstCstmrCodeSmlNm(actionForm.getCustomer(),arRCTList.getMemoLine(), user.getCurrentBranch().getBrCode(), user.getCmpCode());


                 arRCTList.setDescription(smlDetails.getSmlDescription());
                 arRCTList.setQuantity("1");
                 arRCTList.setUnitPrice(Common.convertDoubleToStringMoney(smlDetails.getSmlUnitPrice(), precisionUnit));
                 arRCTList.setAmount(arRCTList.getUnitPrice());
                 arRCTList.setTaxable(Common.convertByteToBoolean(smlDetails.getSmlTax()));

             } catch (GlobalNoRecordFoundException ex) {

             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
-- Ar RCT View Attachment Action --
 *******************************************************/

        } else if(request.getParameter("viewAttachmentButton1") != null) {

	  	File file = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtension );
	  	File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtensionPDF );
	  	String filename = "";

	  	if (file.exists()){
	  		filename = file.getName();
	  	}else if (filePDF.exists()) {
	  		filename = filePDF.getName();
	  	}

			System.out.println(filename + " <== filename?");
        String fileExtension = filename.substring(filename.lastIndexOf("."));

	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	  		fileExtension = attachmentFileExtension;

	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;

	    	}
	  	System.out.println(fileExtension + " <== file extension?");
	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

	  	byte data[] = new byte[fis.available()];

	  	int ctr = 0;
	  	int c = 0;

	  	while ((c = fis.read()) != -1) {

      		data[ctr] = (byte)c;
      		ctr++;

	  	}

	  	if (fileExtension == attachmentFileExtension) {
	      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
	      	System.out.println("jpg");

	      	session.setAttribute(Constants.IMAGE_KEY, image);
	      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	  		System.out.println("pdf");

	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	  	}

         if (request.getParameter("child") == null) {

           return (mapping.findForward("arMiscReceiptEntry"));

         } else {

           return (mapping.findForward("arMiscReceiptEntryChild"));

        }



      } else if(request.getParameter("viewAttachmentButton2") != null) {

    	  File file = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtension );
    	  	File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtensionPDF );
    	  	String filename = "";

    	  	if (file.exists()){
    	  		filename = file.getName();
    	  	}else if (filePDF.exists()) {
    	  		filename = filePDF.getName();
    	  	}

 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	  		fileExtension = attachmentFileExtension;

  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);

    	  	byte data[] = new byte[fis.available()];

    	  	int ctr = 0;
    	  	int c = 0;

    	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

    	  	}

    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}




         if (request.getParameter("child") == null) {

           return (mapping.findForward("arMiscReceiptEntry"));

         } else {

           return (mapping.findForward("arMiscReceiptEntryChild"));

        }


      } else if(request.getParameter("viewAttachmentButton3") != null) {

    	  File file = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtensionPDF );
    	  String filename = "";

    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }

    	  System.out.println(filename + " <== filename?");
    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    		  fileExtension = attachmentFileExtension;

    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

    	  }
    	  System.out.println(fileExtension + " <== file extension?");
    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

    	  byte data[] = new byte[fis.available()];

    	  int ctr = 0;
    	  int c = 0;

    	  while ((c = fis.read()) != -1) {

    		  data[ctr] = (byte)c;
    		  ctr++;

    	  }

    	  if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  } else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  }

         if (request.getParameter("child") == null) {

           return (mapping.findForward("arMiscReceiptEntry"));

         } else {

           return (mapping.findForward("arMiscReceiptEntryChild"));

        }


      } else if(request.getParameter("viewAttachmentButton4") != null) {

    	  File file = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtensionPDF );
    	  String filename = "";

    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }

 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	  		fileExtension = attachmentFileExtension;

  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

    	  	byte data[] = new byte[fis.available()];

    	  	int ctr = 0;
    	  	int c = 0;

    	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

    	  	}

    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}
         if (request.getParameter("child") == null) {

           return (mapping.findForward("arMiscReceiptEntry"));

         } else {

           return (mapping.findForward("arMiscReceiptEntryChild"));

        }
  /*******************************************************
    -- Ar INV ILI Item Enter Action --
 *******************************************************/
         }
       else if(!Common.validateRequired(request.getParameter("arRLIList[" +
        actionForm.getRowRLISelected() + "].isItemEntered"))) {

      	ArMiscReceiptLineItemList arRLIList =
      		actionForm.getArRLIByIndex(actionForm.getRowRLISelected());

      	boolean isAssemblyItem = ejbRCT.getInvItemClassByIiName(arRLIList.getItemName(), user.getCmpCode()).equals("Assembly");

      	arRLIList.setIsAssemblyItem(isAssemblyItem);

      	if(ejbRCT.getInvAutoBuildEnabledByIiName(arRLIList.getItemName(), user.getCmpCode())) {
      		arRLIList.setAutoBuildCheckbox(true);
      	} else {
      		arRLIList.setAutoBuildCheckbox(false);
      	}

      	try {

      		// populate unit field class

      		ArrayList uomList = new ArrayList();
      		uomList = ejbRCT.getInvUomByIiName(arRLIList.getItemName(), user.getCmpCode());

      		arRLIList.clearUnitList();
      		arRLIList.setUnitList(Constants.GLOBAL_BLANK);

  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {

      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

      			arRLIList.setUnitList(mUomDetails.getUomName());

      			if (mUomDetails.isDefault()) {

      				arRLIList.setUnit(mUomDetails.getUomName());

      			}

      		}


      		 //TODO: populate taglist with empty values

      		arRLIList.clearTagList();


     		boolean isTraceMisc =  ejbRCT.getArTraceMisc(arRLIList.getItemName(), user.getCmpCode());


     		arRLIList.setIsTraceMisc(isTraceMisc);
      		System.out.println(isTraceMisc + "<== trace misc item enter action");
      		if (isTraceMisc){
      			arRLIList.clearTagList();

      			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), arRLIList.getQuantity());


      			arRLIList.setMisc(misc);

      		}


      		// populate unit cost field

      		if (!Common.validateRequired(arRLIList.getItemName()) && !Common.validateRequired(arRLIList.getUnit())) {

	      	//	double unitPrice = ejbRCT.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arRLIList.getItemName(), arRLIList.getUnit(), user.getCmpCode());
	      	//	arRLIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

			
				if(!Common.validateRequired(actionForm.getDate()) && Common.validateDateFormat(actionForm.getDate())){
						
						
					 double unitPrice = ejbRCT.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arRLIList.getItemName(), Common.convertStringToSQLDate(actionForm.getDate()) ,arRLIList.getUnit(), user.getCmpCode());
                	 arRLIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

						
						
						
				}else{
				  	double unitPrice = ejbRCT.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arRLIList.getItemName(),  arRLIList.getUnit(), user.getCmpCode());
                    arRLIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

				
				}
	
					


      		}

      		arRLIList.setDiscount1("0.0");
      		arRLIList.setDiscount2("0.0");
      		arRLIList.setDiscount3("0.0");
      		arRLIList.setDiscount4("0.0");
      		arRLIList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));

      		arRLIList.setQuantity(Common.convertDoubleToStringMoney(1d, precisionUnit));
      		arRLIList.setAmount(arRLIList.getUnitPrice());

          } catch (EJBException ex) {

          	if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
    -- Ar INV ILI Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("arRLIList[" +
        actionForm.getRowRLISelected() + "].isUnitEntered"))) {

      	ArMiscReceiptLineItemList arRLIList =
      		actionForm.getArRLIByIndex(actionForm.getRowRLISelected());

      	try {

      	    // populate unit price field

      		if (!Common.validateRequired(arRLIList.getLocation()) && !Common.validateRequired(arRLIList.getItemName())) {

	      		double unitprice = ejbRCT.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arRLIList.getItemName(), arRLIList.getUnit(), user.getCmpCode());
	      		arRLIList.setUnitPrice(Common.convertDoubleToStringMoney(unitprice, precisionUnit));

	        }

	        // populate amount field
      		double amount = 0d;

	        if(!Common.validateRequired(arRLIList.getQuantity()) && !Common.validateRequired(arRLIList.getUnitPrice())) {

	        	amount = Common.convertStringMoneyToDouble(arRLIList.getQuantity(), precisionUnit) *
				Common.convertStringMoneyToDouble(arRLIList.getUnitPrice(), precisionUnit);

	        	arRLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	        }

	        // populate discount field

      		if (!Common.validateRequired(arRLIList.getAmount()) &&
          			(!Common.validateRequired(arRLIList.getDiscount1()) ||
    				!Common.validateRequired(arRLIList.getDiscount2()) ||
    				!Common.validateRequired(arRLIList.getDiscount3()) ||
					!Common.validateRequired(arRLIList.getDiscount4()))) {

      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

      			}

      			double discountRate1 = Common.convertStringMoneyToDouble(arRLIList.getDiscount1(), precisionUnit)/100;
      			double discountRate2 = Common.convertStringMoneyToDouble(arRLIList.getDiscount2(), precisionUnit)/100;
      			double discountRate3 = Common.convertStringMoneyToDouble(arRLIList.getDiscount3(), precisionUnit)/100;
      			double discountRate4 = Common.convertStringMoneyToDouble(arRLIList.getDiscount4(), precisionUnit)/100;
      			double totalDiscountAmount = 0d;
      			if (discountRate1 > 0) {
      				double discountAmount = amount * discountRate1;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      			}
      			if (discountRate2 > 0) {
      				double discountAmount = amount * discountRate2;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      			}
      			if (discountRate3 > 0) {
      				double discountAmount = amount * discountRate3;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      			}
      			if (discountRate4 > 0) {
      				double discountAmount = amount * discountRate4;
      				totalDiscountAmount += discountAmount;
      				amount -= discountAmount;
      			}
      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

      				amount += amount * (actionForm.getTaxRate() / 100);

      			}

      			arRLIList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
      			arRLIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

      		}

          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT ILI Type Enter Action --
*******************************************************/

      } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))) {
	        actionForm.setIsInvestorSupplier(false);
	        if (actionForm.getType().equals("ITEMS")) {

                    actionForm.setIsInvestorSupplier(false);
                     //detect if customer have supplier

	           	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearArRCTList();
	           	actionForm.clearArRLIList();

	           	// Populate line when not forwarding

                    for (int x = 1; x <= invoiceLineNumber; x++) {

                            ArMiscReceiptLineItemList arRLIList = new ArMiscReceiptLineItemList(actionForm,
                                            null, new Integer(x).toString(), null, null, null, null, null,
                                                            null, null, false, false, "0.0", "0.0", "0.0", "0.0", null , "0.0", null, "Y");

                            arRLIList.setLocationList(actionForm.getLocationList());

                            arRLIList.setUnitList(Constants.GLOBAL_BLANK);
                            arRLIList.setUnitList("Select Item First");

                            actionForm.saveArRLIList(arRLIList);

                    }

	        } else {

	        	actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	           	actionForm.clearArRCTList();
	           	actionForm.clearArRLIList();

		        for (int x = 1; x <= invoiceLineNumber; x++) {

		        	ArMiscReceiptEntryList arRCTList = new ArMiscReceiptEntryList(actionForm,
		        	    null, new Integer(x).toString(), null, null, null, null, null, false);

		        	arRCTList.setMemoLineList(actionForm.getMemoLineList());

		        	actionForm.saveArRCTList(arRCTList);

		         }

	        }

	        return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar INV Tax Code Enter Action --
*******************************************************/

      } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {

      	ArTaxCodeDetails details = ejbRCT.getArTcByTcName(actionForm.getTaxCode(), user.getCmpCode());

      	actionForm.setTaxRate(details.getTcRate());
      	actionForm.setTaxType(details.getTcType());

      	actionForm.clearArRLIList();

      	for (int x = 1; x <= invoiceLineNumber; x++) {

      		ArMiscReceiptLineItemList arRLIList = new ArMiscReceiptLineItemList(actionForm, null,
      				new Integer(x).toString(), null, null, null, null, null, null, null, false, false, "0.0",
					"0.0", "0.0", "0.0", null, "0.0", null, "Y");

      		arRLIList.setLocationList(actionForm.getLocationList());

      		arRLIList.setUnitList(Constants.GLOBAL_BLANK);
      		arRLIList.setUnitList("Select Item First");

      		actionForm.saveArRLIList(arRLIList);

      	}

      	return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT Conversion Date Enter Action --
*******************************************************/
      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

    	  try {

    		  actionForm.setConversionRate(Common.convertDoubleToStringMoney(
    				  ejbRCT.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
    						  Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));

    	  } catch (GlobalConversionDateNotExistException ex) {

    		  errors.add(ActionMessages.GLOBAL_MESSAGE,
    				  new ActionMessage("miscReceiptEntry.error.conversionDateNotExist"));

    	  } catch (EJBException ex) {

    		  if (log.isInfoEnabled()) {

    			  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
    					  " session: " + session.getId());

    		  }

    		  return(mapping.findForward("cmnErrorPage"));

    	  }

    	  if (!errors.isEmpty()) {

    		  saveErrors(request, new ActionMessages(errors));

    	  }

    	  return(mapping.findForward("arMiscReceiptEntry"));

/*******************************************************
   -- Ar RCT Load Action --
*******************************************************/
         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arMiscReceiptEntry"));

            }

            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }

            try {

            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearCurrencyList();

            	list = ejbRCT.getGlFcAllWithDefault(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

            			actionForm.setCurrencyList(mFcDetails.getFcName());

            			if (mFcDetails.getFcSob() == 1) {

            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());

            			}

            		}

            	}

            	actionForm.clearBankAccountList();

            	list = ejbRCT.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);


            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBankAccountList((String)i.next());


            		}
            	}


        	    actionForm.clearUserList();

               	ArrayList userList = ejbRCT.getAdUsrAll(user.getCmpCode());

               	if (userList == null || userList.size() == 0) {

               		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

               	} else {

               		Iterator x = userList.iterator();

               		while (x.hasNext()) {

               			actionForm.setUserList((String)x.next());

               		}

               	}


            	actionForm.clearBankAccountCard1List();

            	list = ejbRCT.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBankAccountCard1List(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBankAccountCard1List((String)i.next());

            		}
            	}

            	actionForm.clearBankAccountCard2List();

            	list = ejbRCT.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBankAccountCard2List(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBankAccountCard2List((String)i.next());

            		}
            	}

            	actionForm.clearBankAccountCard3List();

            	list = ejbRCT.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBankAccountCard3List(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBankAccountCard3List((String)i.next());

            		}
            	}



            	actionForm.clearTaxCodeList();

            	list = ejbRCT.getArTcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setTaxCodeList((String) i.next());

            		}

            	}


            	actionForm.clearWithholdingTaxList();

            	list = ejbRCT.getArWtcAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setWithholdingTaxList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setWithholdingTaxList((String)i.next());

            		}

            	}

            	if(actionForm.getUseCustomerPulldown()) {

            		actionForm.clearCustomerList();

            		list = ejbRCT.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		if (list == null || list.size() == 0) {

            			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {

            			i = list.iterator();

            			while (i.hasNext()) {

            				actionForm.setCustomerList((String)i.next());

            			}

            		}

            	}

            	actionForm.clearMemoLineList();

            	list = ejbRCT.getArSmlAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setMemoLineList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setMemoLineList((String)i.next());

            		}

            	}

            	actionForm.clearBatchNameList();

            	list = ejbRCT.getArOpenRbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBatchNameList((String)i.next());

            		}

            	}
            	
            	//get document type list
              	
               	actionForm.clearDocumentTypeList();
               	
               	
               	list = ejbRCT.getDocumentTypeList("AR RECEIPT DOCUMENT TYPE", user.getCmpCode());
               	
               	
               	
               	i = list.iterator();
               	while(i.hasNext()) {
               		actionForm.setDocumentTypeList((String)i.next());
               	}
               	

            	actionForm.clearLocationList();

            	list = ejbRCT.getInvLocAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setLocationList((String)i.next());

            		}

            	}

            	actionForm.clearShiftList();

            	list = ejbRCT.getAdLvInvShiftAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setShiftList((String)i.next());

            		}

            	}

            	actionForm.clearSalespersonList();
            	actionForm.clearSalespersonNameList();

        		list = ejbRCT.getArSlpAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        		if (list == null || list.size() == 0) {

        			actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
        			actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

        		} else {

        			i = list.iterator();

        			while (i.hasNext()) {

              			ArSalespersonDetails mdetails = (ArSalespersonDetails)i.next();

              			actionForm.setSalespersonList(mdetails.getSlpSalespersonCode());
              			actionForm.setSalespersonNameList(mdetails.getSlpSalespersonCode() + " - " + mdetails.getSlpName());

        			}

        		}

            	if (request.getParameter("forwardBatch") != null) {

            		actionForm.setBatchName(request.getParameter("batchName"));
            		actionForm.setType(null);

            	}

            	actionForm.clearArRCTList();
            	actionForm.clearArRLIList();

            	if (request.getParameter("forward") != null || isInitialPrinting) {

            		if (request.getParameter("forward") != null) {

            			actionForm.setReceiptCode(new Integer(request.getParameter("receiptCode")));

            		}

            		ArModReceiptDetails mdetails = ejbRCT.getArRctByRctCode(actionForm.getReceiptCode(), user.getCmpCode());

            		actionForm.setDescription(mdetails.getRctDescription());
            		actionForm.setDocumentType(mdetails.getRctDocumentType());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getRctDate()));
            		actionForm.setReceiptNumber(mdetails.getRctNumber());
            		actionForm.setReferenceNumber(mdetails.getRctReferenceNumber());

            		actionForm.setChequeNumber(mdetails.getRctChequeNumber());
            		actionForm.setVoucherNumber(mdetails.getRctVoucherNumber());
            		actionForm.setCardNumber1(mdetails.getRctCardNumber1());
            		actionForm.setCardNumber2(mdetails.getRctCardNumber2());
            		actionForm.setCardNumber3(mdetails.getRctCardNumber3());

            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit));
            		actionForm.setAmountCash(Common.convertDoubleToStringMoney(mdetails.getRctAmountCash(), precisionUnit));
            		actionForm.setAmountCheque(Common.convertDoubleToStringMoney(mdetails.getRctAmountCheque(), precisionUnit));
            		actionForm.setAmountVoucher(Common.convertDoubleToStringMoney(mdetails.getRctAmountVoucher(), precisionUnit));


            		actionForm.setAmountCard1(Common.convertDoubleToStringMoney(mdetails.getRctAmountCard1(), precisionUnit));
            		actionForm.setAmountCard2(Common.convertDoubleToStringMoney(mdetails.getRctAmountCard2(), precisionUnit));
            		actionForm.setAmountCard3(Common.convertDoubleToStringMoney(mdetails.getRctAmountCard3(), precisionUnit));

            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getRctConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getRctConversionRate(), (short)40));
            		actionForm.setSoldTo(mdetails.getRctSoldTo());
            		actionForm.setPaymentMethod(mdetails.getRctPaymentMethod());
            		actionForm.setCustomerDeposit(Common.convertByteToBoolean(mdetails.getRctCustomerDeposit()));
            		actionForm.setApprovalStatus(mdetails.getRctApprovalStatus());
            		actionForm.setPosted(mdetails.getRctPosted() == 1 ? "YES" : "NO");
            		actionForm.setVoidApprovalStatus(mdetails.getRctVoidApprovalStatus());
            		actionForm.setVoidPosted(mdetails.getRctVoidPosted() == 1 ? "YES" : "NO");
            		actionForm.setReasonForRejection(mdetails.getRctReasonForRejection());
            		actionForm.setReceiptVoid(Common.convertByteToBoolean(mdetails.getRctVoid()));
            		actionForm.setCreatedBy(mdetails.getRctCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getRctDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getRctLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getRctDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getRctApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getRctDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getRctPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getRctDatePosted()));
            		actionForm.setSubjectToCommission(Common.convertByteToBoolean(mdetails.getRctSubjectToCommission()));
                    actionForm.setInvestorBeginningBalance(Common.convertByteToBoolean(mdetails.getRctInvtrBeginningBalance()));
                    actionForm.setIsInvestorSupplier(mdetails.getIsInvestorSupplier());


                   ArrayList rpList =   ejbRCT.getArMiscReceiptReportParameters( user.getCmpCode());


                    actionForm.setReportParameters(Common.convertStringToReportParameters(mdetails.getReportParameter(), rpList));


            		if (!actionForm.getCurrencyList().contains(mdetails.getRctFcName())) {

            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearCurrencyList();

            			}
            			actionForm.setCurrencyList(mdetails.getRctFcName());

            		}
            		actionForm.setCurrency(mdetails.getRctFcName());

            		if (!actionForm.getTaxCodeList().contains(mdetails.getRctTcName())) {

            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearTaxCodeList();

            			}
            			actionForm.setTaxCodeList(mdetails.getRctTcName());

            		}
            		actionForm.setTaxCode(mdetails.getRctTcName());
            		actionForm.setTaxType(mdetails.getRctTcType());
            		actionForm.setTaxRate(mdetails.getRctTcRate());

            		if (!actionForm.getWithholdingTaxList().contains(mdetails.getRctWtcName())) {

            			if (actionForm.getWithholdingTaxList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearWithholdingTaxList();

            			}
            			actionForm.setWithholdingTaxList(mdetails.getRctWtcName());

            		}
            		actionForm.setWithholdingTax(mdetails.getRctWtcName());

            		if (!actionForm.getCustomerList().contains(mdetails.getRctCstCustomerCode())) {

            			if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearCustomerList();

            			}
            			actionForm.setCustomerList(mdetails.getRctCstCustomerCode());

            		}
            		actionForm.setCustomer(mdetails.getRctCstCustomerCode());
  	              	actionForm.setCustomerName(mdetails.getRctCstName());
  	              	actionForm.setCustomerAddress(mdetails.getRctCstAddress());
  	              	actionForm.setInvtrInvestorFund(Common.convertByteToBoolean(mdetails.getRctInvtrInvestorFund()));
  	              	actionForm.setInvtrNextRunDate(Common.convertSQLDateToString(mdetails.getRctInvtrNextRunDate()));


            		if (!actionForm.getBankAccountList().contains(mdetails.getRctBaName())) {

            			if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearBankAccountList();

            			}
            			actionForm.setBankAccountList(mdetails.getRctBaName());

            		}
            		actionForm.setBankAccount(mdetails.getRctBaName());




            		if (!actionForm.getBankAccountCard1List().contains(mdetails.getRctBaCard1Name())) {

            			if (actionForm.getBankAccountCard1List().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearBankAccountCard1List();

            			}
            			actionForm.setBankAccountCard1List(mdetails.getRctBaCard1Name());

            		}
            		actionForm.setBankAccountCard1(mdetails.getRctBaCard1Name());


            		if (!actionForm.getBankAccountCard2List().contains(mdetails.getRctBaCard2Name())) {

            			if (actionForm.getBankAccountCard2List().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearBankAccountCard2List();

            			}
            			actionForm.setBankAccountCard2List(mdetails.getRctBaCard2Name());

            		}
            		actionForm.setBankAccountCard2(mdetails.getRctBaCard2Name());

            		if (!actionForm.getBankAccountCard3List().contains(mdetails.getRctBaCard3Name())) {

            			if (actionForm.getBankAccountCard3List().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearBankAccountCard3List();

            			}
            			actionForm.setBankAccountCard3List(mdetails.getRctBaCard3Name());

            		}
            		actionForm.setBankAccountCard3(mdetails.getRctBaCard3Name());


            		actionForm.setShift(mdetails.getRctLvShift());

            		if (!actionForm.getBatchNameList().contains(mdetails.getRctRbName())) {

           		    	actionForm.setBatchNameList(mdetails.getRctRbName());

           		    }
            		actionForm.setBatchName(mdetails.getRctRbName());

            		if (!actionForm.getSalespersonList().contains(mdetails.getRctSlpSalespersonCode())) {

            			if (actionForm.getSalespersonList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearSalespersonList();
            				actionForm.clearSalespersonNameList();

            			}
            			actionForm.setSalespersonList(mdetails.getRctSlpSalespersonCode());
              			actionForm.setSalespersonNameList(mdetails.getRctSlpSalespersonCode() + " - " + mdetails.getRctSlpName());

            		}
            		actionForm.setSalesperson(mdetails.getRctSlpSalespersonCode());

            		if (mdetails.getInvIliList() != null && !mdetails.getInvIliList().isEmpty()) {

            		    actionForm.setType("ITEMS");

            		    list = mdetails.getInvIliList();

            			i = list.iterator();

            			double totalAmount = 0;

            			while (i.hasNext()) {

            				ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails)i.next();

            				boolean isAssemblyItem = ejbRCT.getInvItemClassByIiName(mIliDetails.getIliIiName(), user.getCmpCode()).equals("Assembly");

            				String fixedDiscountAmount = "0.00";

                            if((mIliDetails.getIliDiscount1() + mIliDetails.getIliDiscount2() +
                            		mIliDetails.getIliDiscount3() + mIliDetails.getIliDiscount4()) == 0)
                           	 fixedDiscountAmount = Common.convertDoubleToStringMoney(mIliDetails.getIliTotalDiscount(), precisionUnit);

                            totalAmount += (mIliDetails.getIliAmount() - mIliDetails.getIliTotalDiscount());
                            System.out.println(mIliDetails.getIliAmount());

            				ArMiscReceiptLineItemList arRliList = new ArMiscReceiptLineItemList(actionForm,
            						mIliDetails.getIliCode(),
									Common.convertShortToString(mIliDetails.getIliLine()),
									mIliDetails.getIliLocName(),
									mIliDetails.getIliIiName(),
									mIliDetails.getIliIiDescription(),
									Common.convertDoubleToStringMoney(mIliDetails.getIliQuantity(), precisionUnit),
									mIliDetails.getIliUomName(),
									Common.convertDoubleToStringMoney(mIliDetails.getIliUnitPrice(), precisionUnit),
									Common.convertDoubleToStringMoney(mIliDetails.getIliAmount(), precisionUnit),
									isAssemblyItem, Common.convertByteToBoolean(mIliDetails.getIliEnableAutoBuild()),
            						Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount1(), precisionUnit),
            						Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount2(), precisionUnit),
            						Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount3(), precisionUnit),
            						Common.convertDoubleToStringMoney(mIliDetails.getIliDiscount4(), precisionUnit),
            						Common.convertDoubleToStringMoney(mIliDetails.getIliTotalDiscount(), precisionUnit),
            						fixedDiscountAmount,
            						mIliDetails.getIliMisc(),
            						Common.convertByteToBoolean(mIliDetails.getIliTax())?"Y":"N"
            						);

            				arRliList.setLocationList(actionForm.getLocationList());

            				arRliList.clearUnitList();
			            	ArrayList unitList = ejbRCT.getInvUomByIiName(mIliDetails.getIliIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {

			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		arRliList.setUnitList(mUomDetails.getUomName());

			            	}

			            	  boolean isTraceMisc = ejbRCT.getArTraceMisc(mIliDetails.getIliIiName(), user.getCmpCode());
			            	  arRliList.setIsTraceMisc(isTraceMisc);


			            	arRliList.clearTagList();
                             ArrayList tagList = new ArrayList();
                             if(isTraceMisc) {

                            	 tagList = mIliDetails.getiIliTagList();

                            	 if(tagList.size()>0) {

                              		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mIliDetails.getIliQuantity()));
                              		arRliList.setMisc(misc);

                              	}else {
                              		if(mIliDetails.getIliMisc()==null) {

                              			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mIliDetails.getIliQuantity()));
                              			arRliList.setMisc(misc);
                              		}
                              	}




                             }

            				actionForm.saveArRLIList(arRliList);


            			}

            			actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

            			int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();

            			for (int x = list.size() + 1; x <= remainingList; x++) {

            				ArMiscReceiptLineItemList arRLIList = new ArMiscReceiptLineItemList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null, null, null, false, false,
									null, null, null, null, null, null, null, "Y");

            				arRLIList.setLocationList(actionForm.getLocationList());

            				arRLIList.setUnitList(Constants.GLOBAL_BLANK);
            				arRLIList.setUnitList("Select Item First");

            				actionForm.saveArRLIList(arRLIList);

            			}

            			this.setFormProperties(actionForm, user.getCompany());

            			
            			
            			if(request.getParameter("updateVoid")!=null) {
            				actionForm.setReceiptVoid(true);
            				
            				 ArReceiptDetails details = new ArReceiptDetails();

        		           details.setRctCode(actionForm.getReceiptCode());
        		           details.setRctDocumentType(actionForm.getDocumentType());
        		           details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
        		           details.setRctNumber(actionForm.getReceiptNumber());
        		           details.setRctReferenceNumber(actionForm.getReferenceNumber());
        		           details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
        		           details.setRctDescription(actionForm.getDescription());
        		           details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
        		           details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
        		           details.setRctLvShift(actionForm.getShift());
        		           details.setRctSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
        		           details.setRctPaymentMethod(actionForm.getPaymentMethod());
        		           details.setRctCustomerName(actionForm.getCustomerName());
        		           details.setRctCustomerAddress(actionForm.getCustomerAddress());


        		           details.setRctChequeNumber(actionForm.getChequeNumber());
        		           details.setRctVoucherNumber(actionForm.getVoucherNumber());
        		           details.setRctCardNumber1(actionForm.getCardNumber1());
        		           details.setRctCardNumber2(actionForm.getCardNumber2());
        		           details.setRctCardNumber3(actionForm.getCardNumber3());
        		           details.setRctAmountCash(Common.convertStringMoneyToDouble(actionForm.getAmountCash(), precisionUnit));
        		           details.setRctAmountCheque(Common.convertStringMoneyToDouble(actionForm.getAmountCheque(), precisionUnit));
        		           details.setRctAmountVoucher(Common.convertStringMoneyToDouble(actionForm.getAmountVoucher(), precisionUnit));

        		           details.setRctAmountCard1(Common.convertStringMoneyToDouble(actionForm.getAmountCard1(), precisionUnit));
        		           details.setRctAmountCard2(Common.convertStringMoneyToDouble(actionForm.getAmountCard2(), precisionUnit));
        		           details.setRctAmountCard3(Common.convertStringMoneyToDouble(actionForm.getAmountCard3(), precisionUnit));

        		           details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

        		           if (actionForm.getReceiptCode() == null) {

        		           	   details.setRctCreatedBy(user.getUserName());
        			           details.setRctDateCreated(new java.util.Date());

        		           }

        		           details.setRctLastModifiedBy(user.getUserName());
        		           details.setRctDateLastModified(new java.util.Date());
            				
        		           
        		           //mdetails.getInvIliList()
        		      //     asdas
        		           
        		           Integer receiptCode = ejbRCT.saveArRctIliEntry(details,
        	           	    		actionForm.getBankAccount(), actionForm.getBankAccountCard1(), actionForm.getBankAccountCard2(), actionForm.getBankAccountCard3(),
        	           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
        	           	        actionForm.getCurrency(), actionForm.getCustomer(), actionForm.getBatchName(),
        	           	     mdetails.getInvIliList(), false, actionForm.getSalesperson(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        		           
            			}
            			
            			
            			
            			if (request.getParameter("child") == null) {

            				return (mapping.findForward("arMiscReceiptEntry"));

            			} else {

            				return (mapping.findForward("arMiscReceiptEntryChild"));

            			}

            		} else {

            			actionForm.setType("MEMO LINES");

	            		list = mdetails.getInvIlList();

			            i = list.iterator();

			            int lineNumber = 0;
			            double totalAmount = 0;

			            while (i.hasNext()) {

				           ArModInvoiceLineDetails mIlDetails = (ArModInvoiceLineDetails)i.next();

				           totalAmount += mIlDetails.getIlAmount();

					       ArMiscReceiptEntryList arIlList = new ArMiscReceiptEntryList(actionForm,
					          mIlDetails.getIlCode(),
					          String.valueOf(++lineNumber),
					          mIlDetails.getIlSmlName(),
					          mIlDetails.getIlDescription(),
					          Common.convertDoubleToStringMoney(mIlDetails.getIlQuantity(), precisionUnit),
					          Common.convertDoubleToStringMoney(mIlDetails.getIlUnitPrice(), precisionUnit),
					          Common.convertDoubleToStringMoney(mIlDetails.getIlAmount(), precisionUnit),
					          Common.convertByteToBoolean(mIlDetails.getIlTax()));

					      arIlList.setMemoLineList(actionForm.getMemoLineList());

					      actionForm.saveArRCTList(arIlList);


				        }
			            actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

				        int remainingList = invoiceLineNumber - (list.size() % invoiceLineNumber) + list.size();
				        lineNumber = 0;

				        for (int x = list.size() + 1; x <= remainingList; x++) {

				        	ArMiscReceiptEntryList arRCTList = new ArMiscReceiptEntryList(actionForm,
				        	    null, String.valueOf(++lineNumber), null, null, null, null, null, false);

				        	arRCTList.setMemoLineList(actionForm.getMemoLineList());

				        	actionForm.saveArRCTList(arRCTList);

				         }

				         this.setFormProperties(actionForm, user.getCompany());

				         if (request.getParameter("child") == null) {

				         	return (mapping.findForward("arMiscReceiptEntry"));

				         } else {

				         	return (mapping.findForward("arMiscReceiptEntryChild"));

				         }
	            	 }
	            }

            	// Populate line when not forwarding

            	if (user.getUserApps().contains("OMEGA INVENTORY") && (actionForm.getType() == null || actionForm.getType().equals("ITEMS"))) {


	            	for (int x = 1; x <= invoiceLineNumber; x++) {

	            		ArMiscReceiptLineItemList arRLIList = new ArMiscReceiptLineItemList(actionForm,
	            				null, new Integer(x).toString(), null, null, null, null, null,
								null, null, false, false, "0.0", "0.0", "0.0", "0.0", null, "0.0", null, "Y");

	            		arRLIList.setLocationList(actionForm.getLocationList());

	            		arRLIList.setUnitList(Constants.GLOBAL_BLANK);
	            		arRLIList.setUnitList("Select Item First");

	            		actionForm.saveArRLIList(arRLIList);

	            	}

	            } else {

	                for (int x = 1; x <= invoiceLineNumber; x++) {

			        	ArMiscReceiptEntryList arRCTList = new ArMiscReceiptEntryList(actionForm,
			        	    null, new Integer(x).toString(), null, null, null, null, null, false);

			        	arRCTList.setMemoLineList(actionForm.getMemoLineList());

			        	actionForm.saveArRCTList(arRCTList);

			        }
	            }

            } catch(GlobalNoRecordFoundException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("miscReceiptEntry.error.receiptAlreadyDeleted"));

            } catch(EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArMiscReceiptEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }

               return(mapping.findForward("cmnErrorPage"));

           }



            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveSubmitButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   try {

                   	   ArrayList list = ejbRCT.getAdApprovalNotifiedUsersByRctCode(actionForm.getReceiptCode(), user.getCmpCode());

                   	   if (list.isEmpty()) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));

                   	   } else if (list.contains("DOCUMENT POSTED")) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                	   	       new ActionMessage("messages.documentPosted"));

                   	   } else {

                   	   	   Iterator i = list.iterator();

                   	   	   String APPROVAL_USERS = "";

                   	   	   while (i.hasNext()) {

                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

                   	   	       if (i.hasNext()) {

                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";

                   	   	       }


                   	   	   }

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

                   	   }

                   	   if (actionForm.getReceiptVoid() && actionForm.getPosted().equals("NO")) {

                   	   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

                   	   } else {

	                   	   saveMessages(request, messages);
	                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

	                   }


                   } catch(EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }

		               return(mapping.findForward("cmnErrorPage"));

		           }

               } else if (request.getParameter("saveAsDraftButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }

            }

            actionForm.reset(mapping, request);

            if (actionForm.getType() == null) {

	           if (user.getUserApps().contains("OMEGA INVENTORY")) {

                   actionForm.setType("ITEMS");

               } else {

                   actionForm.setType("MEMO LINES");

               }

            }

            actionForm.setReceiptCode(null);
         
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));

            actionForm.setConversionRate("1.000000");
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

            actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountCash(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountCheque(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountVoucher(Common.convertDoubleToStringMoney(0d, precisionUnit));

            actionForm.setAmountCard1(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountCard2(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountCard3(Common.convertDoubleToStringMoney(0d, precisionUnit));

            this.setFormProperties(actionForm, user.getCompany());
            return(mapping.findForward("arMiscReceiptEntry"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

            log.info("Exception caught in ArMiscReceiptEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }


	private void setFormProperties(ArMiscReceiptEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getReceiptVoid() && actionForm.getPosted().equals("NO")) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableReceiptVoid(false);
				actionForm.setShowJournalButton(true);

			} else if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getReceiptCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableReceiptVoid(false);
					actionForm.setShowJournalButton(true);

				} else if (actionForm.getReceiptCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setEnableReceiptVoid(true);
					actionForm.setShowJournalButton(true);


				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
				    actionForm.getApprovalStatus().equals("N/A")) {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableReceiptVoid(true);
					actionForm.setShowJournalButton(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableReceiptVoid(false);
					actionForm.setShowJournalButton(true);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(true);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableReceiptVoid(true);
				actionForm.setShowJournalButton(true);

				if (actionForm.getVoidApprovalStatus() == null) {
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setEnableReceiptVoid(true);
				} else {
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setEnableReceiptVoid(false);
				}

			}



		} else {


			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setEnableReceiptVoid(false);

		}

		// view attachment

		if (actionForm.getReceiptCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ar/Misc Receipt/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			File file = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtensionPDF);
 			//to avoid a bug, 2 different buttons will be used.... one for MEMO LINES, one for ITEMS
 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton1(true);
 				actionForm.setShowViewAttachmentB1(true);
 			} else {

 				actionForm.setShowViewAttachmentButton1(false);
 				actionForm.setShowViewAttachmentB1(false);
 			}

 			file = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton2(true);
 				actionForm.setShowViewAttachmentB2(true);
 			} else {

 				actionForm.setShowViewAttachmentButton2(false);
 				actionForm.setShowViewAttachmentB3(false);
 			}

 			file = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton3(true);
 				actionForm.setShowViewAttachmentB3(true);
 			} else {

 				actionForm.setShowViewAttachmentButton3(false);
 				actionForm.setShowViewAttachmentB3(false);
 			}

 			file = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton4(true);
 				actionForm.setShowViewAttachmentB4(true);
 			} else {

 				actionForm.setShowViewAttachmentButton4(false);
 				actionForm.setShowViewAttachmentB4(false);
 			}

		} else {

			actionForm.setShowViewAttachmentB1(false);
			actionForm.setShowViewAttachmentB2(false);
			actionForm.setShowViewAttachmentB3(false);
			actionForm.setShowViewAttachmentB4(false);

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);


		}



	}

}