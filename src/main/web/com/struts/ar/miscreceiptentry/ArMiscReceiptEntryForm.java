package com.struts.ar.miscreceiptentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.ReportParameter;
import com.util.Debug;


public class ArMiscReceiptEntryForm extends ActionForm implements Serializable {

	private short precisionUnit = 0;
	private Integer receiptCode = null;
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList();
	private String documentType = null;
	private ArrayList documentTypeList = new ArrayList();
	private String shift = null;
	private ArrayList shiftList = new ArrayList();
	private String customer = null;
	private ArrayList customerList = new ArrayList();

	private String bankAccount = null;
	private ArrayList bankAccountList = new ArrayList();
	private String bankAccountCheque = null;
	private ArrayList bankAccountChequeList = new ArrayList();
	private String bankAccountVoucher = null;
	private ArrayList bankAccountVoucherList = new ArrayList();

	private String bankAccountCard1 = null;
	private ArrayList bankAccountCard1List = new ArrayList();
	private String bankAccountCard2 = null;
	private ArrayList bankAccountCard2List = new ArrayList();
	private String bankAccountCard3 = null;
	private ArrayList bankAccountCard3List = new ArrayList();

	private String amountCard1 = null;
	private String amountCard2 = null;
	private String amountCard3 = null;


	private String date = null;
	private String receiptNumber = null;
	private String referenceNumber = null;
	private String cardNumber1 = null;
	private String cardNumber2 = null;
	private String cardNumber3 = null;
	private String chequeNumber = null;
	private String voucherNumber = null;

	private String paymentMethod = null;
	private boolean customerDeposit = false;
	private ArrayList paymentMethodList = new ArrayList();
	private String amount = null;
	private String amountCash = null;
	private String amountCheque = null;
	private String amountVoucher= null;
	private String totalAmount = null;
	private boolean receiptVoid = false;
	private String description = null;
	private String soldTo = null;
	private String taxCode = null;
	private ArrayList taxCodeList = new ArrayList();
	private String withholdingTax = null;
	private ArrayList withholdingTaxList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String conversionDate = null;
	private String conversionRate = null;
	private String approvalStatus = null;
	private String posted = null;
	private String voidApprovalStatus = null;
	private String voidPosted = null;
	private String reasonForRejection = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private ArrayList memoLineList = new ArrayList();
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String location = null;
	private ArrayList locationList = new ArrayList();
    private String customerName = null;
    private String customerAddress = null;
	private String functionalCurrency = null;
	private boolean invtrInvestorFund = false;
	private String invtrNextRunDate = null;


	private ArrayList userList = new ArrayList();


	private ArrayList arRCTList = new ArrayList();
	private ArrayList arRLIList = new ArrayList();
	private int rowSelected = 0;
	private int rowRLISelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showBatchName = false;
	private boolean showShift = false;
	private boolean enableReceiptVoid = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveSubmitButton = false;
	private boolean showSaveAsDraftButton = false;
	private boolean showDeleteButton = false;
	private boolean showJournalButton = false;
	private boolean useCustomerPulldown = true;
	private boolean disableSalesPrice = false;

	private String isCustomerEntered  = null;
	private String isTypeEntered = null;

	private String report = null;

	private String salesperson = null;
	private ArrayList salespersonList = new ArrayList();
    private ArrayList salespersonNameList = new ArrayList();
	private double taxRate = 0;
	private String taxType = null;
	private String isTaxCodeEntered = null;
	private String isConversionDateEntered = null;



        private boolean isInvestorSupplier = false;
	private boolean investorBeginningBalance = false;

	private boolean subjectToCommission = false;

	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;


	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;

	private boolean showViewAttachmentB1 = false;
	private boolean showViewAttachmentB2 = false;
	private boolean showViewAttachmentB3 = false;
	private boolean showViewAttachmentB4 = false;

	private String attachment = null;
	private String attachmentPDF = null;

	private ArrayList reportParameters = new ArrayList();

	public String getReport() {

		return report;

	}

	public void setReport(String report) {

		this.report = report;

	}

	public String getAttachment() {

		return attachment;

	}

	public void setAttachment(String attachment) {

		this.attachment = attachment;

	}

	public String getAttachmentPDF() {

		return attachmentPDF;

	}

	public void setAttachmentPDF(String attachmentPDF) {

		this.attachmentPDF = attachmentPDF;

	}

	public int getRowSelected() {

		return rowSelected;

	}

	public ArMiscReceiptEntryList getArRCTByIndex(int index) {

		return((ArMiscReceiptEntryList)arRCTList.get(index));

	}

	public Object[] getArRCTList() {

		return(arRCTList.toArray());

	}

	public int getArRCTListSize() {

		return(arRCTList.size());

	}

	public void saveArRCTList(Object newArRCTList) {

		arRCTList.add(newArRCTList);

	}

	public void clearArRCTList(){

		arRCTList.clear();

	}

	public void setRowSelected(Object selectedArRCTList, boolean isEdit){

		this.rowSelected = arRCTList.indexOf(selectedArRCTList);

	}

	public void updateArRCTRow(int rowSelected, Object newArRCTList){

		arRCTList.set(rowSelected, newArRCTList);

	}

	public void deleteArRCTList(int rowSelected){

		arRCTList.remove(rowSelected);

	}

	public int getRowRLISelected(){

		return rowRLISelected;

	}

	public ArMiscReceiptLineItemList getArRLIByIndex(int index){

		return((ArMiscReceiptLineItemList)arRLIList.get(index));

	}

	public Object[] getArRLIList(){

		return(arRLIList.toArray());

	}

	public int getArRLIListSize(){

		return(arRLIList.size());

	}

	public void saveArRLIList(Object newArRLIList){

		arRLIList.add(newArRLIList);

	}

	public void clearArRLIList(){

		arRLIList.clear();

	}



public ArrayList getUserList() {

		return userList;

   }

   public void setUserList(String user) {

	   userList.add(user);

   }

   public void clearUserList() {

	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);

   }



	public void setRowRLISelected(Object selectedArRLIList, boolean isEdit){

		this.rowRLISelected = arRLIList.indexOf(selectedArRLIList);

	}

	public void updateArRLIRow(int rowRLISelected, Object newArRLIList){

		arRLIList.set(rowRLISelected, newArRLIList);

	}

	public void deleteArRLIList(int rowRLISelected){

		arRLIList.remove(rowRLISelected);

	}

	public String getTxnStatus(){

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}

public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

	public Integer getReceiptCode() {

		return receiptCode;

	}

	public void setReceiptCode(Integer receiptCode) {

		this.receiptCode = receiptCode;

	}

	public String getBatchName() {

		return batchName;

	}

	public void setBatchName(String batchName) {

		this.batchName = batchName;

	}

	public ArrayList getBatchNameList() {

		return batchNameList;

	}

	public void setBatchNameList(String batchName) {

		batchNameList.add(batchName);

	}

	public void clearBatchNameList() {

		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);

	}

	public String getShift() {

		return shift;

	}

	public void setShift(String shift) {

		this.shift = shift;

	}

	public ArrayList getShiftList() {

		return shiftList;

	}

	public void setShiftList(String shift) {

		shiftList.add(shift);

	}

	public void clearShiftList() {

		shiftList.clear();
		shiftList.add(Constants.GLOBAL_BLANK);

	}

	public String getCustomer() {

		return customer;

	}

	public void setCustomer(String customer) {

		this.customer = customer;

	}

	public ArrayList getCustomerList() {

		return customerList;

	}

	public void setCustomerList(String customer) {

		customerList.add(customer);

	}

	public void clearCustomerList() {

		customerList.clear();
		customerList.add(Constants.GLOBAL_BLANK);

	}

	public String getBankAccount() {

		return bankAccount;

	}

	public void setBankAccount(String bankAccount) {

		this.bankAccount = bankAccount;

	}

	public ArrayList getBankAccountList() {

		return bankAccountList;

	}

	public void setBankAccountList(String bankAccount) {

		bankAccountList.add(bankAccount);

	}

	public void clearBankAccountList() {

		bankAccountList.clear();
		bankAccountList.add(Constants.GLOBAL_BLANK);

	}

public String getBankAccountCheque() {

		return bankAccountCheque;

	}

	public void setBankAccountCheque(String bankAccountCheque) {

		this.bankAccountCheque = bankAccountCheque;

	}

	public ArrayList getBankAccountChequeList() {

		return bankAccountChequeList;

	}

	public void setBankAccountChequeList(String bankAccountCheque) {

		bankAccountChequeList.add(bankAccountCheque);

	}

	public void clearBankAccountChequeList() {

		bankAccountChequeList.clear();
		bankAccountChequeList.add(Constants.GLOBAL_BLANK);

	}

public String getBankAccountVoucher() {

		return bankAccountVoucher;

	}

	public void setBankAccountVoucher(String bankAccountVoucher) {

		this.bankAccountVoucher = bankAccountVoucher;

	}

	public ArrayList getBankAccountVoucherList() {

		return bankAccountVoucherList;

	}

	public void setBankAccountVoucherList(String bankAccountVoucher) {

		bankAccountVoucherList.add(bankAccountVoucher);

	}

	public void clearBankAccountVoucherList() {

		bankAccountVoucherList.clear();
		bankAccountVoucherList.add(Constants.GLOBAL_BLANK);

	}



public String getBankAccountCard1() {

		return bankAccountCard1;

	}

	public void setBankAccountCard1(String bankAccountCard1) {

		this.bankAccountCard1 = bankAccountCard1;

	}

	public ArrayList getBankAccountCard1List() {

		return bankAccountCard1List;

	}

	public void setBankAccountCard1List(String bankAccountCard1) {

		bankAccountCard1List.add(bankAccountCard1);

	}

	public void clearBankAccountCard1List() {

		bankAccountCard1List.clear();
		bankAccountCard1List.add(Constants.GLOBAL_BLANK);

	}

public String getBankAccountCard2() {

		return bankAccountCard2;

	}

	public void setBankAccountCard2(String bankAccountCard2) {

		this.bankAccountCard2 = bankAccountCard2;

	}

	public ArrayList getBankAccountCard2List() {

		return bankAccountCard2List;

	}

	public void setBankAccountCard2List(String bankAccountCard2) {

		bankAccountCard2List.add(bankAccountCard2);

	}

	public void clearBankAccountCard2List() {

		bankAccountCard2List.clear();
		bankAccountCard2List.add(Constants.GLOBAL_BLANK);

	}

public String getBankAccountCard3() {

		return bankAccountCard3;

	}

	public void setBankAccountCard3(String bankAccountCard3) {

		this.bankAccountCard3 = bankAccountCard3;

	}

	public ArrayList getBankAccountCard3List() {

		return bankAccountCard3List;

	}

	public void setBankAccountCard3List(String bankAccountCard3) {

		bankAccountCard3List.add(bankAccountCard3);

	}

	public void clearBankAccountCard3List() {

		bankAccountCard3List.clear();
		bankAccountCard3List.add(Constants.GLOBAL_BLANK);

	}


	public String getDate() {

		return date;

	}

	public void setDate(String date) {

		this.date = date;

	}

	public String getReceiptNumber() {

		return receiptNumber;

	}

	public void setReceiptNumber(String receiptNumber) {

		this.receiptNumber = receiptNumber;

	}

	public String getReferenceNumber() {

		return referenceNumber;

	}

	public void setReferenceNumber(String referenceNumber) {

		this.referenceNumber = referenceNumber;

	}

public String getCardNumber1() {

		return cardNumber1;

	}

	public void setCardNumber1(String cardNumber1) {

		this.cardNumber1 = cardNumber1;

	}

public String getCardNumber2() {

		return cardNumber2;

	}

	public void setCardNumber2(String cardNumber2) {

		this.cardNumber2 = cardNumber2;

	}

public String getCardNumber3() {

		return cardNumber3;

	}

	public void setCardNumber3(String cardNumber3) {

		this.cardNumber3 = cardNumber3;

	}

public String getChequeNumber() {

		return chequeNumber;

	}

	public void setChequeNumber(String chequeNumber) {

		this.chequeNumber = chequeNumber;

	}

public String getVoucherNumber() {

		return voucherNumber;

	}

	public void setVoucherNumber(String voucherNumber) {

		this.voucherNumber = voucherNumber;

	}


	public boolean getCustomerDeposit() {

		return customerDeposit;

	}

	public void setCustomerDeposit(boolean customerDeposit) {

		this.customerDeposit = customerDeposit;

	}

	public String getPaymentMethod() {

		return paymentMethod;

	}

	public void setPaymentMethod(String paymentMethod) {

		this.paymentMethod = paymentMethod;

	}

	public ArrayList getPaymentMethodList() {

		return paymentMethodList;

	}

	public void setPaymentMethodList(String paymentMethod) {

		paymentMethodList.add(paymentMethod);

	}

	public void clearPaymentMethodList() {

		paymentMethodList.clear();

	}

	public String getAmount() {

		return amount;

	}

	public void setAmount(String amount) {

		this.amount = amount;

	}

public String getAmountCash() {

		return amountCash;

	}

	public void setAmountCash(String amountCash) {

		this.amountCash = amountCash;

	}

public String getAmountCheque() {

		return amountCheque;

	}

	public void setAmountCheque(String amountCheque) {

		this.amountCheque = amountCheque;

	}

public String getAmountVoucher() {

		return amountVoucher;

	}

	public void setAmountVoucher(String amountVoucher) {

		this.amountVoucher = amountVoucher;

	}


public String getAmountCard1() {

		return amountCard1;

	}

	public void setAmountCard1(String amountCard1) {

		this.amountCard1 = amountCard1;

	}

public String getAmountCard2() {

		return amountCard2;

	}

	public void setAmountCard2(String amountCard2) {

		this.amountCard2 = amountCard2;

	}

public String getAmountCard3() {

		return amountCard3;

	}

	public void setAmountCard3(String amountCard3) {

		this.amountCard3 = amountCard3;

	}

	public String getTotalAmount() {

		return totalAmount;

	}

	public void setTotalAmount(String totalAmount) {

		this.totalAmount = totalAmount;

	}

	public boolean getReceiptVoid() {

		return receiptVoid;

	}

	public void setReceiptVoid(boolean receiptVoid) {

		this.receiptVoid = receiptVoid;

	}

	public String getDescription() {

		return description;

	}

	public void setDescription(String description) {

		this.description = description;

	}

	public String getSoldTo() {

		return soldTo;

	}

	public void setSoldTo(String soldTo) {

		this.soldTo = soldTo;

	}

	public String getTaxCode() {

		return taxCode;

	}

	public void setTaxCode(String taxCode) {

		this.taxCode = taxCode;

	}

	public ArrayList getTaxCodeList() {

		return taxCodeList;

	}

	public void setTaxCodeList(String taxCode) {

		taxCodeList.add(taxCode);

	}

	public void clearTaxCodeList() {

		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);

	}


	public String getWithholdingTax() {

		return withholdingTax;

	}

	public void setWithholdingTax(String withholdingTax) {

		this.withholdingTax = withholdingTax;

	}

	public ArrayList getWithholdingTaxList() {

		return withholdingTaxList;

	}

	public void setWithholdingTaxList(String withholdingTax) {

		withholdingTaxList.add(withholdingTax);

	}

	public void clearWithholdingTaxList() {

		withholdingTaxList.clear();
		withholdingTaxList.add(Constants.GLOBAL_BLANK);

	}


	public String getCurrency() {

		return currency;

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}

	public void setCurrencyList(String currency) {

		currencyList.add(currency);

	}

	public void clearCurrencyList() {

		currencyList.clear();

	}

	public String getConversionDate() {

		return conversionDate;

	}

	public void setConversionDate(String conversionDate) {

		this.conversionDate = conversionDate;

	}

	public String getConversionRate() {

		return conversionRate;

	}

	public void setConversionRate(String conversionRate) {

		this.conversionRate = conversionRate;

	}

	public String getApprovalStatus() {

		return approvalStatus;

	}

	public void setApprovalStatus(String approvalStatus) {

		this.approvalStatus = approvalStatus;

	}

	public String getPosted() {

		return posted;

	}

	public void setPosted(String posted) {

		this.posted = posted;

	}

	public String getVoidApprovalStatus() {

		return voidApprovalStatus;

	}

	public void setVoidApprovalStatus(String voidApprovalStatus) {

		this.voidApprovalStatus = voidApprovalStatus;

	}

	public String getVoidPosted() {

		return voidPosted;

	}

	public void setVoidPosted(String voidPosted) {

		this.voidPosted = voidPosted;

	}

	public String getReasonForRejection() {

		return reasonForRejection;

	}

	public void setReasonForRejection(String reasonForRejection) {

		this.reasonForRejection = reasonForRejection;

	}


	public String getCreatedBy() {

		return createdBy;

	}

	public void setCreatedBy(String createdBy) {

		this.createdBy = createdBy;

	}

	public String getDateCreated() {

		return dateCreated;

	}

	public void setDateCreated(String dateCreated) {

		this.dateCreated = dateCreated;

	}

	public String getLastModifiedBy() {

		return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

		this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

		return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

		this.dateLastModified = dateLastModified;

	}


	public String getApprovedRejectedBy() {

		return approvedRejectedBy;

	}

	public void setApprovedRejectedBy(String approvedRejectedBy) {

		this.approvedRejectedBy = approvedRejectedBy;

	}

	public String getDateApprovedRejected() {

		return dateApprovedRejected;

	}

	public void setDateApprovedRejected(String dateApprovedRejected) {

		this.dateApprovedRejected = dateApprovedRejected;

	}

	public String getPostedBy() {

		return postedBy;

	}

	public void setPostedBy(String postedBy) {

		this.postedBy = postedBy;

	}

	public String getDatePosted() {

		return datePosted;

	}

	public void setDatePosted(String datePosted) {

		this.datePosted = datePosted;

	}

	public String getFunctionalCurrency() {

		return functionalCurrency;

	}

	public void setFunctionalCurrency(String functionalCurrency) {

		this.functionalCurrency = functionalCurrency;

	}

	public boolean getInvtrInvestorFund() {

		return invtrInvestorFund;

	}

	public void setInvtrInvestorFund(boolean invtrInvestorFund) {

		this.invtrInvestorFund = invtrInvestorFund;

	}

public String getInvtrNextRunDate() {

		return invtrNextRunDate;

	}

	public void setInvtrNextRunDate(String invtrNextRunDate) {

		this.invtrNextRunDate = invtrNextRunDate;

	}

	public boolean getEnableFields() {

		return enableFields;

	}

	public void setEnableFields(boolean enableFields) {

		this.enableFields = enableFields;

	}

	public boolean getShowBatchName() {

		return showBatchName;

	}

	public void setShowBatchName(boolean showBatchName) {

		this.showBatchName = showBatchName;

	}

	public boolean getShowShift() {

		return showShift;

	}

	public void setShowShift(boolean showShift) {

		this.showShift = showShift;

	}

	public ArrayList getMemoLineList() {

		return memoLineList;

	}

	public void setMemoLineList(String memoLine) {

		memoLineList.add(memoLine);

	}

	public void clearMemoLineList() {

		memoLineList.clear();
		memoLineList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getEnableReceiptVoid() {

		return enableReceiptVoid;

	}

	public void setEnableReceiptVoid(boolean enableReceiptVoid) {

		this.enableReceiptVoid = enableReceiptVoid;

	}

	public boolean getShowAddLinesButton() {

		return showAddLinesButton;

	}

	public void setShowAddLinesButton(boolean showAddLinesButton) {

		this.showAddLinesButton = showAddLinesButton;

	}

	public boolean getShowDeleteLinesButton() {

		return showDeleteLinesButton;

	}

	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

		this.showDeleteLinesButton = showDeleteLinesButton;

	}

	public boolean getShowSaveSubmitButton() {

		return showSaveSubmitButton;

	}

	public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {

		this.showSaveSubmitButton = showSaveSubmitButton;

	}

	public boolean getShowSaveAsDraftButton() {

		return showSaveAsDraftButton;

	}

	public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {

		this.showSaveAsDraftButton = showSaveAsDraftButton;

	}

	public boolean getShowDeleteButton() {

		return showDeleteButton;

	}

	public void setShowDeleteButton(boolean showDeleteButton) {

		this.showDeleteButton = showDeleteButton;

	}

	public boolean getShowJournalButton() {

		return showJournalButton;

	}

	public void setShowJournalButton(boolean showJournalButton) {

		this.showJournalButton = showJournalButton;

	}

	public String getIsCustomerEntered() {

		return isCustomerEntered;

	}

	public String getIsTypeEntered() {

		return isTypeEntered;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {


		this.type = type;

	}

	public ArrayList getTypeList() {

		return typeList;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(String location) {

		locationList.add(location);

	}

	public void clearLocationList() {

		locationList.clear();
		locationList.add(Constants.GLOBAL_BLANK);

	}

	public boolean getUseCustomerPulldown() {

		return useCustomerPulldown;

	}

	public void setUseCustomerPulldown(boolean useCustomerPulldown) {

		this.useCustomerPulldown = useCustomerPulldown;

	}

	public boolean getDisableSalesPrice() {

		return disableSalesPrice;

	}

	public void setDisableSalesPrice(boolean disableSalesPrice) {

		this.disableSalesPrice = disableSalesPrice;

	}

	public String getSalesperson() {

		return salesperson;

	}

	public void setSalesperson(String salesperson) {

		this.salesperson = salesperson;

	}

	public ArrayList getSalespersonList() {

		return salespersonList;

	}

	public void setSalespersonList(String salesperson) {

		salespersonList.add(salesperson);

	}

	public void clearSalespersonList() {

		salespersonList.clear();
		salespersonList.add(Constants.GLOBAL_BLANK);

	}



        public boolean getIsInvestorSupplier() {

		return isInvestorSupplier;

	}

	public void setIsInvestorSupplier(boolean isInvestorSupplier) {

		this.isInvestorSupplier = isInvestorSupplier;

	}


        public boolean getInvestorBeginningBalance() {

		return investorBeginningBalance;

	}

	public void setInvestorBeginningBalance(boolean investorBeginningBalance) {

		this.investorBeginningBalance = investorBeginningBalance;

	}








	public boolean getSubjectToCommission() {

		return subjectToCommission;

	}

	public void setSubjectToCommission(boolean subjecToCommision) {

		this.subjectToCommission = subjecToCommision;

	}

    public String getCustomerName() {

    	return customerName;

    }

    public void setCustomerName(String customerName) {

    	this.customerName = customerName;

    }

public String getCustomerAddress() {

    	return customerAddress;

    }

    public void setCustomerAddress(String customerAddress) {

    	this.customerAddress = customerAddress;

    }

    public ArrayList getSalespersonNameList() {

    	return salespersonNameList;

    }

    public void setSalespersonNameList(String salespersonName) {

    	salespersonNameList.add(salespersonName);

    }

    public void clearSalespersonNameList() {

    	salespersonNameList.clear();
    	salespersonNameList.add(Constants.GLOBAL_BLANK);

    }

	public double getTaxRate() {

		return taxRate;

	}

	public void setTaxRate(double taxRate) {

		this.taxRate = taxRate;

	}


	public String getTaxType() {

		return taxType;

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public String getIsTaxCodeEntered() {

		return isTaxCodeEntered;

	}

	public String getIsConversionDateEntered(){

		return isConversionDateEntered;

	}


	public boolean getShowViewAttachmentB1() {

		return showViewAttachmentB1;

	}

	public void setShowViewAttachmentB1(boolean showViewAttachmentB1) {

		this.showViewAttachmentB1 = showViewAttachmentB1;

	}


	public void setShowViewAttachmentB2(boolean showViewAttachmentB2) {

		this.showViewAttachmentB2 = showViewAttachmentB2;

	}

	public boolean getShowViewAttachmentB2() {

		return showViewAttachmentB2;

	}

	public void setShowViewAttachmentB3(boolean showViewAttachmentB3) {

		this.showViewAttachmentB3= showViewAttachmentB3;

	}
	public boolean getShowViewAttachmentB3() {

		return showViewAttachmentB3;

	}

	public void setShowViewAttachmentB4(boolean showViewAttachmentB4) {

		this.showViewAttachmentB4 = showViewAttachmentB4;

	}
	public boolean getShowViewAttachmentB4() {

		return showViewAttachmentB4;

	}

	public boolean getShowViewAttachmentButton1() {

		return showViewAttachmentButton1;

	}

	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

		this.showViewAttachmentButton1 = showViewAttachmentButton1;

	}
	public boolean getShowViewAttachmentButton2() {

		return showViewAttachmentButton2;

	}

	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

		this.showViewAttachmentButton2 = showViewAttachmentButton2;

	}

	public boolean getShowViewAttachmentButton3() {

		return showViewAttachmentButton3;

	}

	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

		this.showViewAttachmentButton3 = showViewAttachmentButton3;

	}

	public boolean getShowViewAttachmentButton4() {

		return showViewAttachmentButton4;

	}

	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

		this.showViewAttachmentButton4 = showViewAttachmentButton4;

	}

	public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {

		return filename3;

	}

	public void setFilename3(FormFile filename3) {

		this.filename3 = filename3;

	}

	public FormFile getFilename4() {

		return filename4;

	}

	public void setFilename4(FormFile filename4) {

		this.filename4 = filename4;

	}


	public Object[] getReportParameters() {
		return(reportParameters.toArray());
	}

	public void setReportParameters(ArrayList reportParameters) {
		this.reportParameters = reportParameters;
	}


	public ArrayList getReportParametersAsArrayList() {
		return reportParameters;
	}

	public void addReportParameters(ReportParameter reportParameter) {
		reportParameters.add(reportParameter);
	}

	public void clearReportParameters() {
		reportParameters.clear();
	}
	
	public String getDocumentType() {
    	return documentType;
    }
    
    public void setDocumentType(String documentType) {
    	this.documentType = documentType;
    }

    public ArrayList getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(String documentType) {
		documentTypeList.add(documentType);
	}

	public void clearDocumentTypeList() {
		documentTypeList.clear();
		documentTypeList.add(Constants.GLOBAL_BLANK);
	}


	public void reset(ActionMapping mapping, HttpServletRequest request){

		customer = Constants.GLOBAL_BLANK;
		taxCode = Constants.GLOBAL_BLANK;
		documentType = null;
		withholdingTax = Constants.GLOBAL_BLANK;
		bankAccount = Constants.GLOBAL_BLANK;
		bankAccountCheque = Constants.GLOBAL_BLANK;
		bankAccountVoucher = Constants.GLOBAL_BLANK;
		bankAccountCard1 = Constants.GLOBAL_BLANK;
		bankAccountCard2 = Constants.GLOBAL_BLANK;
		bankAccountCard3 = Constants.GLOBAL_BLANK;
		paymentMethodList.clear();
		paymentMethodList.add(Constants.GLOBAL_BLANK);
		paymentMethodList.add(Constants.AR_MR_PAYMENT_METHOD_CASH);
		paymentMethodList.add(Constants.AR_MR_PAYMENT_METHOD_CHECK);
		paymentMethodList.add(Constants.AR_MR_PAYMENT_METHOD_CREDIT_CARD);
		paymentMethod = Constants.GLOBAL_BLANK;
		customerDeposit = false;
		date = null;
		receiptNumber = null;
		referenceNumber = null;

		cardNumber1 = null;
		cardNumber2 = null;
		cardNumber3 = null;
		chequeNumber = null;
		voucherNumber = null;

		soldTo = null;
		amount = null;
		amountCash = null;
		amountCheque = null;
		amountVoucher = null;
		amountCard1 = null;
		amountCard2 = null;
		amountCard3 = null;
		totalAmount = null;
		receiptVoid = false;
		description = null;
		conversionDate = null;
		conversionRate = null;
		approvalStatus = null;
		posted = null;
		voidApprovalStatus = null;
		voidPosted = null;
		reasonForRejection = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;
		approvedRejectedBy = null;
		dateApprovedRejected = null;
		postedBy = null;
		datePosted = null;
		isCustomerEntered = null;
		isTypeEntered = null;
		typeList.clear();
		typeList.add("ITEMS");
		typeList.add("MEMO LINES");
		location = Constants.GLOBAL_BLANK;
		salesperson = null;

             investorBeginningBalance = false;
		subjectToCommission = false;
        customerName = Constants.GLOBAL_BLANK;
        customerAddress = Constants.GLOBAL_BLANK;
        taxRate = 0;
        taxType = Constants.GLOBAL_BLANK;
        isTaxCodeEntered = null;
        isConversionDateEntered = null;

        invtrInvestorFund = false;
        invtrNextRunDate = null;

        filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;


		attachment = null;
		attachmentPDF = null;

		for (int i=0; i<arRLIList.size(); i++) {

			ArMiscReceiptLineItemList actionList = (ArMiscReceiptLineItemList)arRLIList.get(i);
			actionList.setIsItemEntered(null);
			actionList.setIsUnitEntered(null);
			actionList.setAutoBuildCheckbox(false);
			actionList.setIsAssemblyItem(false);

		}

		for (int i=0; i<arRCTList.size(); i++) {

			ArMiscReceiptEntryList actionList = (ArMiscReceiptEntryList)arRCTList.get(i);
			actionList.setIsMemoLineEntered(null);
			actionList.setTaxable(false);

		}

		// reset tax

		/*if(request.getParameter("saveSubmitButton") != null ||
				request.getParameter("saveAsDraftButton") != null || request.getParameter("printButton") != null ||
				request.getParameter("journalButton") != null) {

			Iterator i = arRCTList.iterator();

			while (i.hasNext()) {

				ArMiscReceiptEntryList list = (ArMiscReceiptEntryList)i.next();

				list.setTaxable(false);

			}

		}*/
	}

	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		   //ActionErrors errors = new ActionErrors();

		   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		   String separator = "$";

		   // Remove first $ character
		   misc = misc.substring(1);

		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;
		   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		   System.out.println("ctr :" + ctr);

		   /*if(y==0)
			   return new ArrayList();*/

		   ArrayList miscList = new ArrayList();

		   for(int x=0; x<ctr; x++) {
			   try {

				   // Date
				   start = nextIndex + 1;
				   nextIndex = misc.indexOf(separator, start);
				   length = nextIndex - start;
				   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				   String checker = misc.substring(start, start + length);
				   if(checker!=""&& checker != " "){
					   miscList.add(checker);
				   }else{
					   miscList.add("null");
				   }

				   //System.out.println(misc.substring(start, start + length));
			   } catch (Exception ex) {
				   ex.printStackTrace();
			   }

		   	   }
		   return miscList;
	   }

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("saveSubmitButton") != null ||
				request.getParameter("saveAsDraftButton") != null || request.getParameter("printButton") != null ||
				request.getParameter("journalButton") != null){

			if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showBatchName){
				errors.add("batchName",
						new ActionMessage("miscReceiptEntry.error.batchNameRequired"));
			}

			if((Common.validateRequired(shift) || shift.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
					showShift && type.equals("ITEMS")){
				errors.add("shift",
						new ActionMessage("miscReceiptEntry.error.shiftRequired"));
			}

			if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("customer",
						new ActionMessage("miscReceiptEntry.error.customerRequired"));
			}

			if(Common.validateRequired(date)){
				errors.add("date",
						new ActionMessage("miscReceiptEntry.error.dateRequired"));
			}

			if(!Common.validateDateFormat(date)){
				errors.add("date",
						new ActionMessage("miscReceiptEntry.error.dateInvalid"));
			}


			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			
		
			if(year<1900){
				errors.add("date",
						new ActionMessage("invoiceEntry.error.dateInvalid"));
			}

			if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("bankAccount",
						new ActionMessage("miscReceiptEntry.error.bankAccountRequired"));
			}

			if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("taxCode",
						new ActionMessage("miscReceiptEntry.error.taxCodeRequired"));
			}

			if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("withholdingTax",
						new ActionMessage("miscReceiptEntry.error.withholdingTaxRequired"));
			}

			if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				errors.add("currency",
						new ActionMessage("miscReceiptEntry.error.currencyRequired"));
			}

			if(!Common.validateDateFormat(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("miscReceiptEntry.error.conversionDateInvalid"));
			}

			if(!Common.validateMoneyRateFormat(conversionRate)){
				errors.add("conversionRate",
						new ActionMessage("miscReceiptEntry.error.conversionRateInvalid"));
			}

			if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("miscReceiptEntry.error.conversionDateMustBeNull"));
			}

			if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
					Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
				errors.add("conversionRate",
						new ActionMessage("miscReceiptEntry.error.conversionRateMustBeNull"));
			}

			if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
					Common.validateRequired(conversionDate)){
				errors.add("conversionRate",
						new ActionMessage("miscReceiptEntry.error.conversionRateOrDateMustBeEntered"));
			}

			if(type.equals("MEMO LINES")) {

				int numberOfLines = 0;

				Iterator i = arRCTList.iterator();

				while (i.hasNext()) {

					ArMiscReceiptEntryList ilList = (ArMiscReceiptEntryList)i.next();

					if (Common.validateRequired(ilList.getMemoLine()) &&
							Common.validateRequired(ilList.getDescription()) &&
							Common.validateRequired(ilList.getQuantity()) &&
							Common.validateRequired(ilList.getUnitPrice()) &&
							Common.validateRequired(ilList.getAmount())) continue;

					numberOfLines++;

					if(Common.validateRequired(ilList.getMemoLine()) || ilList.getMemoLine().equals(Constants.GLOBAL_BLANK)
							|| ilList.getMemoLine().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
						errors.add("memoLine",
								new ActionMessage("miscReceiptEntry.error.memoLineRequired", ilList.getLineNumber()));
					}
					if(Common.validateRequired(ilList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("miscReceiptEntry.error.quantityRequired", ilList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(ilList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("miscReceiptEntry.error.quantityInvalid", ilList.getLineNumber()));
					}
					if(Common.validateRequired(ilList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("miscReceiptEntry.error.unitPriceRequired", ilList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(ilList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("miscReceiptEntry.error.unitPriceInvalid", ilList.getLineNumber()));
					}
					if(Common.validateRequired(ilList.getAmount())){
						errors.add("amount",
								new ActionMessage("miscReceiptEntry.error.amountRequired", ilList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(ilList.getAmount())){
						errors.add("amount",
								new ActionMessage("miscReceiptEntry.error.amountInvalid", ilList.getLineNumber()));
					}

				}

				if (numberOfLines == 0) {

					errors.add("customer",
							new ActionMessage("miscReceiptEntry.error.miscReceiptMustHaveLine"));

				}

			} else {

				int numOfLines = 0;

				Iterator i = arRLIList.iterator();

				while (i.hasNext()) {

					ArMiscReceiptLineItemList rliList = (ArMiscReceiptLineItemList)i.next();

					if (Common.validateRequired(rliList.getLocation()) &&
							Common.validateRequired(rliList.getItemName()) &&
							Common.validateRequired(rliList.getQuantity()) &&
							Common.validateRequired(rliList.getUnit()) &&
							Common.validateRequired(rliList.getUnitPrice())) continue;

					numOfLines++;
					/*
					try{
		      	 		String separator = "$";
		      	 		String misc = "";
		      		   // Remove first $ character
		      		   misc = rliList.getMisc().substring(1);

		      		   // Counter
		      		   int start = 0;
		      		   int nextIndex = misc.indexOf(separator, start);
		      		   int length = nextIndex - start;
		      		   int counter;
		      		   counter = Integer.parseInt(misc.substring(start, start + length));

		      	 		ArrayList miscList = this.getExpiryDateStr(rliList.getMisc(), counter);
		      	 		System.out.println("rilList.getMisc() : " + rliList.getMisc());
		      	 		Iterator mi = miscList.iterator();

		      	 		int ctrError = 0;
		      	 		int ctr = 0;

		      	 		while(mi.hasNext()){
		      	 				String miscStr = (String)mi.next();

		      	 				if(!Common.validateDateFormat(miscStr)){
		      	 					errors.add("date",
		      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
		      	 					ctrError++;
		      	 				}

		      	 				System.out.println("miscStr: "+miscStr);
		      	 				if(miscStr=="null"){
		      	 					ctrError++;
		      	 				}
		      	 		}
		      	 		//if ctr==Error => No Date Will Apply
		      	 		//if ctr>error => Invalid Date
		      	 		System.out.println("CTR: "+  miscList.size());
		      	 		System.out.println("ctrError: "+  ctrError);
		      	 		System.out.println("counter: "+  counter);
		      	 			if(ctrError>0 && ctrError!=miscList.size()){
		      	 				errors.add("date",
		      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
		      	 			}

		      	 		//if error==0 Add Expiry Date


		      	 	}catch(Exception ex){
		  	  	    	ex.printStackTrace();
		  	  	    }*/


					if(Common.validateRequired(rliList.getLocation()) || rliList.getLocation().equals(Constants.GLOBAL_BLANK)){
						errors.add("location",
								new ActionMessage("miscReceiptEntry.error.locationRequired", rliList.getLineNumber()));
					}
					if(Common.validateRequired(rliList.getItemName()) || rliList.getItemName().equals(Constants.GLOBAL_BLANK)){
						errors.add("itemName",
								new ActionMessage("miscReceiptEntry.error.itemNameRequired", rliList.getLineNumber()));
					}
					if(Common.validateRequired(rliList.getQuantity())) {
						errors.add("quantity",
								new ActionMessage("miscReceiptEntry.error.quantityRequired", rliList.getLineNumber()));
					}
					if(!Common.validateNumberFormat(rliList.getQuantity())){
						errors.add("quantity",
								new ActionMessage("miscReceiptEntry.error.quantityInvalid", rliList.getLineNumber()));
					}
					if(!Common.validateRequired(rliList.getQuantity()) && Common.convertStringMoneyToDouble(rliList.getQuantity(), (short)3) <= 0) {
						errors.add("quantity",
								new ActionMessage("miscReceiptEntry.error.negativeOrZeroQuantityNotAllowed", rliList.getLineNumber()));
					}
					if(Common.validateRequired(rliList.getUnit()) || rliList.getUnit().equals(Constants.GLOBAL_BLANK)){
						errors.add("unit",
								new ActionMessage("miscReceiptEntry.error.unitRequired", rliList.getLineNumber()));
					}
					if(Common.validateRequired(rliList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("miscReceiptEntry.error.unitPriceRequired", rliList.getLineNumber()));
					}
					if(!Common.validateMoneyFormat(rliList.getUnitPrice())){
						errors.add("unitPrice",
								new ActionMessage("miscReceiptEntry.error.unitPriceInvalid", rliList.getLineNumber()));
					}

				}

				if (numOfLines == 0) {

					errors.add("customer",
							new ActionMessage("miscReceiptEntry.error.miscReceiptMustHaveLine"));

				}

			}

		} else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

			if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
				errors.add("customer",
						new ActionMessage("miscReceiptEntry.error.customerRequired"));
			}

		} else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

			if(!Common.validateDateFormat(conversionDate)){

				errors.add("conversionDate", new ActionMessage("miscReceiptEntry.error.conversionDateInvalid"));

			}

		}

		return(errors);
	}
}
