package com.struts.ar.pdcentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlCustomerDepositAccountNotFoundException;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.ArRCTInvoiceHasNoWTaxCodeException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ArPdcEntryController;
import com.ejb.txn.ArPdcEntryControllerHome;
import com.ejb.txn.ArReceiptEntryController;
import com.ejb.txn.ArReceiptEntryControllerHome;
import com.struts.ar.receiptentry.ArReceiptEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModInvoicePaymentScheduleDetails;
import com.util.ArModPdcDetails;
import com.util.ArPdcDetails;
import com.util.ArReceiptDetails;
import com.util.ArStandardMemoLineDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class ArPdcEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArPdcEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArPdcEntryForm actionForm = (ArPdcEntryForm)form;

	      // reset report
	      
	      actionForm.setReport(null);
         
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
         	frParam = Common.getUserPermission(user, Constants.AR_PDC_ENTRY_ID);
         
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arPdcEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ArPdcEntryController EJB
*******************************************************/

         ArPdcEntryControllerHome homePDC = null;
         ArPdcEntryController ejbPDC = null;
              

         try {
            
            homePDC = (ArPdcEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArPdcEntryControllerEJB", ArPdcEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArPdcEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbPDC = homePDC.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ArPdcEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }


 /*******************************************************
     Initialize ArReceiptEntryController EJB
 *******************************************************/

       ArReceiptEntryControllerHome homeRCT = null;
       ArReceiptEntryController ejbRCT = null;

       try {
          
          homeRCT = (ArReceiptEntryControllerHome)com.util.EJBHomeFactory.
              lookUpHome("ejb/ArReceiptEntryControllerEJB", ArReceiptEntryControllerHome.class);
          
       } catch(NamingException e) {
          if(log.isInfoEnabled()){
              log.info("NamingException caught in ArReceiptEntryAction.execute(): " + e.getMessage() +
             " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }

       try {
       	
          ejbRCT = homeRCT.create();
          
       } catch(CreateException e) {
       	
          if(log.isInfoEnabled()) {
          	
              log.info("CreateException caught in ArReceiptEntryAction.execute(): " + e.getMessage() +
                 " session: " + session.getId());
          }
          
          return(mapping.findForward("cmnErrorPage"));
          
       }

       ActionErrors errors = new ActionErrors();
       ActionMessages messages = new ActionMessages();
         
         
         
/*******************************************************
   Call ArPdcEntryController EJB
   getGlFcPrecisionUnit
   getAdPrfArInvoiceLineNumber
   getInvGpQuantityPrecisionUnit
   getAdPrfEnableInvShift
   getAdPrfArUseCustomerPulldown
*******************************************************/

         short precisionUnit = 0;
         short pdcLineNumber = 0;
         short quantityPrecision = 0;
         boolean enablePdcBatch = false;
         boolean isInitialPrinting = false;
         boolean enableShift = false;
         boolean useCustomerPulldown = true;
         
         try {
         	
            precisionUnit = ejbPDC.getGlFcPrecisionUnit(user.getCmpCode());
            pdcLineNumber = ejbPDC.getAdPrfArInvoiceLineNumber(user.getCmpCode());   
            quantityPrecision = ejbPDC.getInvGpQuantityPrecisionUnit(user.getCmpCode());
            enableShift = Common.convertByteToBoolean(ejbPDC.getAdPrfEnableInvShift(user.getCmpCode()));
            actionForm.setShowShift(enableShift);
            useCustomerPulldown = Common.convertByteToBoolean(ejbPDC.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         /*******************************************************
         -- Ar RCT Previous Action --
      *******************************************************/ 
               
               if(request.getParameter("previousButton") != null){
               	
               	actionForm.setLineCount(actionForm.getLineCount() - actionForm.getMaxRows());
               	
               	// check if prev should be disabled
               	if (actionForm.getLineCount() == 0) {
               		
               		actionForm.setDisablePreviousButton(true);
               		actionForm.setDisableFirstButton(true);
               		
               	} else {
               		
               		actionForm.setDisablePreviousButton(false);
               		actionForm.setDisableFirstButton(false);
               		
               	}
               	
               	// check if next should be disabled
               	if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {
               		
               		actionForm.setDisableNextButton(true);
               		actionForm.setDisableLastButton(true);
               		
               	} else {
               		
               		actionForm.setDisableNextButton(false);
               		actionForm.setDisableLastButton(false);
               		
               	}
               	      	        	
               		return (mapping.findForward("arPdcEntry"));         
      	     
               	
      /*******************************************************
         -- Ar RCT Next Action --
      *******************************************************/ 
               	
               } else if(request.getParameter("nextButton") != null){
               	
               	actionForm.setLineCount(actionForm.getLineCount() + actionForm.getMaxRows());
               	
               	// check if prev should be disabled
               	if (actionForm.getLineCount() == 0) {
               		
               		actionForm.setDisablePreviousButton(true);
               		actionForm.setDisableFirstButton(true);
               		
               	} else {
               		
               		actionForm.setDisablePreviousButton(false);
               		actionForm.setDisableFirstButton(false);
               		
               	}
               	
               	// check if next should be disabled
               	if (actionForm.getArRCTListSize() <= actionForm.getLineCount() + actionForm.getMaxRows()) {
               		
               		actionForm.setDisableNextButton(true);
               		actionForm.setDisableLastButton(true);
               		
               	} else {
               		
               		actionForm.setDisableNextButton(false);
               		actionForm.setDisableLastButton(false);
               		
               	}
               	
           		return (mapping.findForward("arPdcEntry"));

      /*******************************************************
         -- Ar RCT First Action --
      *******************************************************/ 

               } else if(request.getParameter("firstButton") != null){

              	 actionForm.setLineCount(0);

              	 // check if prev should be disabled
              	 if (actionForm.getLineCount() == 0) {

              		 actionForm.setDisablePreviousButton(true);
              		 actionForm.setDisableFirstButton(true);

              	 } else {

              		 actionForm.setDisablePreviousButton(false);
              		 actionForm.setDisableFirstButton(false);

              	 }

              	 // check if next should be disabled
              	 if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

              		 actionForm.setDisableNextButton(true);
              		 actionForm.setDisableLastButton(true);

              	 } else {

              		 actionForm.setDisableNextButton(false);
              		 actionForm.setDisableLastButton(false);

              	 }

            		return (mapping.findForward("arPdcEntry")); 	

      /*******************************************************
        -- Ar RCT Last Action --
      *******************************************************/ 

               } else if(request.getParameter("lastButton") != null){

              	 int size = actionForm.getArRCTListSize();

              	 if((size % actionForm.getMaxRows()) != 0) {

              		 actionForm.setLineCount(size -(size % actionForm.getMaxRows()));

              	 } else {

              		 actionForm.setLineCount(size - actionForm.getMaxRows());

              	 }

              	 // check if prev should be disabled
              	 if (actionForm.getLineCount() == 0) {

              		 actionForm.setDisablePreviousButton(true);
              		 actionForm.setDisableFirstButton(true);

              	 } else {

              		 actionForm.setDisablePreviousButton(false);
              		 actionForm.setDisableFirstButton(false);

              	 }

              	 // check if next should be disabled
              	 if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

              		 actionForm.setDisableNextButton(true);
              		 actionForm.setDisableLastButton(true);

              	 } else {

              		 actionForm.setDisableNextButton(false);
              		 actionForm.setDisableLastButton(false);

              	 }

            		return (mapping.findForward("arPdcEntry"));  
               	
               }         
         
         
         
         
/*******************************************************
   -- Ar PDC Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("MEMO LINES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArPdcDetails details = new ArPdcDetails();
           
           details.setPdcCode(actionForm.getPdcCode());
           
           if(Common.convertStringToSQLDate(actionForm.getMaturityDate()).before(new java.util.Date()) ||
           		Common.convertStringToSQLDate(actionForm.getMaturityDate()).equals(new java.util.Date())) {
           	
           	details.setPdcStatus("MATURED");
            
           } else {
           	
           	details.setPdcStatus(actionForm.getStatus());
            
           }
           
           details.setPdcCheckNumber(actionForm.getCheckNumber());
           details.setPdcReferenceNumber(actionForm.getReferenceNumber());           
           details.setPdcDateReceived(Common.convertStringToSQLDate(actionForm.getDateReceived()));
           details.setPdcMaturityDate(Common.convertStringToSQLDate(actionForm.getMaturityDate()));
           details.setPdcAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setPdcDescription(actionForm.getDescription());           
           details.setPdcCancelled(Common.convertBooleanToByte(actionForm.getCancelled()));           
           details.setPdcLvFreight(actionForm.getFreight());
           details.setPdcConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPdcConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setPdcEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));           
           
           if (actionForm.getPdcCode() == null) {
           	
           	   details.setPdcCreatedBy(user.getUserName());
	           details.setPdcDateCreated(new java.util.Date());
	                      
           }
           
           details.setPdcLastModifiedBy(user.getUserName());
           details.setPdcDateLastModified(new java.util.Date());
           
           ArrayList ilList = new ArrayList(); 
                                 
           for (int i = 0; i<actionForm.getArPDCListSize(); i++) {
           	
           	   ArPdcEntryList arPDCList = actionForm.getArPDCByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arPDCList.getMemoLine()) &&
	      	 	     Common.validateRequired(arPDCList.getDescription()) &&
	      	 	     Common.validateRequired(arPDCList.getQuantity()) &&
	      	 	     Common.validateRequired(arPDCList.getUnitPrice()) &&
	      	 	     Common.validateRequired(arPDCList.getAmount())) continue;
           	   
           	   ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();
           	   
           	   mdetails.setIlSmlName(arPDCList.getMemoLine());
           	   mdetails.setIlDescription(arPDCList.getDescription());
           	   mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arPDCList.getQuantity(), precisionUnit));
           	   mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arPDCList.getUnitPrice(), precisionUnit));
           	   mdetails.setIlAmount(Common.convertStringMoneyToDouble(arPDCList.getAmount(), precisionUnit));
           	   mdetails.setIlTax(Common.convertBooleanToByte(arPDCList.getTaxable()));
           	   
           	   ilList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	    ejbPDC.saveArPdcEntry(details, actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(),
           	        ilList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));

           } catch (GlobalDocumentNumberNotUniqueException ex) {
              	
          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("pdcEntry.error.documentNumberNotUnique"));
           	    
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.conversionDateNotExist"));   
           	    
           } catch (GlobalPaymentTermInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.paymentTermInvalid"));      
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyVoid"));
                                                   	           	           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
   -- Ar PDC Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("MEMO LINES") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArPdcDetails details = new ArPdcDetails();
           
           details.setPdcCode(actionForm.getPdcCode());  
           
           if(Common.convertStringToSQLDate(actionForm.getMaturityDate()).before(new java.util.Date()) ||
           		Common.convertStringToSQLDate(actionForm.getMaturityDate()).equals(new java.util.Date())) {
           	
           	details.setPdcStatus("MATURED");
            
           } else {
           	
           	details.setPdcStatus(actionForm.getStatus());
            
           }
           
           details.setPdcCheckNumber(actionForm.getCheckNumber());
           details.setPdcReferenceNumber(actionForm.getReferenceNumber());           
           details.setPdcDateReceived(Common.convertStringToSQLDate(actionForm.getDateReceived()));
           details.setPdcMaturityDate(Common.convertStringToSQLDate(actionForm.getMaturityDate()));
           details.setPdcAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setPdcDescription(actionForm.getDescription());           
           details.setPdcCancelled(Common.convertBooleanToByte(actionForm.getCancelled()));           
           details.setPdcLvFreight(actionForm.getFreight());
           details.setPdcConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPdcConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));           
           details.setPdcEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
           
           if (actionForm.getPdcCode() == null) {
           	
           	   details.setPdcCreatedBy(user.getUserName());
	           details.setPdcDateCreated(new java.util.Date());
	                      
           }
           
           details.setPdcLastModifiedBy(user.getUserName());
           details.setPdcDateLastModified(new java.util.Date());
           
           ArrayList ilList = new ArrayList(); 
                                 
           for (int i = 0; i<actionForm.getArPDCListSize(); i++) {
           	
           	   ArPdcEntryList arPDCList = actionForm.getArPDCByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arPDCList.getMemoLine()) &&
	      	 	     Common.validateRequired(arPDCList.getDescription()) &&
	      	 	     Common.validateRequired(arPDCList.getQuantity()) &&
	      	 	     Common.validateRequired(arPDCList.getUnitPrice()) &&
	      	 	     Common.validateRequired(arPDCList.getAmount())) continue;
           	   
           	   ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();
           	   
           	   mdetails.setIlSmlName(arPDCList.getMemoLine());
           	   mdetails.setIlDescription(arPDCList.getDescription());
           	   mdetails.setIlQuantity(Common.convertStringMoneyToDouble(arPDCList.getQuantity(), precisionUnit));
           	   mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arPDCList.getUnitPrice(), precisionUnit));
           	   mdetails.setIlAmount(Common.convertStringMoneyToDouble(arPDCList.getAmount(), precisionUnit));
           	   mdetails.setIlTax(Common.convertBooleanToByte(arPDCList.getTaxable()));
           	   
           	   ilList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	    Integer pdcCode = ejbPDC.saveArPdcEntry(details, actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(),
           	        ilList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setPdcCode(pdcCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));
           	    
           } catch (GlobalDocumentNumberNotUniqueException ex) {
             	
         	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("pdcEntry.error.documentNumberNotUnique"));
          	    
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.conversionDateNotExist"));   
           	
           } catch (GlobalPaymentTermInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.paymentTermInvalid"));   
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyVoid"));
                                       	           	           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
                    	
/*******************************************************
   -- Ar PDC ILI Save As Draft Action --
*******************************************************/

        } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArPdcDetails details = new ArPdcDetails();
                      
           details.setPdcCode(actionForm.getPdcCode());  
           
           if(Common.convertStringToSQLDate(actionForm.getMaturityDate()).before(new java.util.Date()) ||
           		Common.convertStringToSQLDate(actionForm.getMaturityDate()).equals(new java.util.Date())) {
           	
           	details.setPdcStatus("MATURED");
            
           } else {
           	
           	details.setPdcStatus(actionForm.getStatus());
            
           }
           
           details.setPdcCheckNumber(actionForm.getCheckNumber());
           details.setPdcReferenceNumber(actionForm.getReferenceNumber());           
           details.setPdcDateReceived(Common.convertStringToSQLDate(actionForm.getDateReceived()));
           details.setPdcMaturityDate(Common.convertStringToSQLDate(actionForm.getMaturityDate()));
           details.setPdcAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setPdcDescription(actionForm.getDescription());           
           details.setPdcCancelled(Common.convertBooleanToByte(actionForm.getCancelled()));           
           details.setPdcLvFreight(actionForm.getFreight());
           details.setPdcConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPdcConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setPdcLvShift(actionForm.getShift());
           details.setPdcEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
           
           if (actionForm.getPdcCode() == null) {
           	
           	   details.setPdcCreatedBy(user.getUserName());
	           details.setPdcDateCreated(new java.util.Date());
	                      
           }
           
           details.setPdcLastModifiedBy(user.getUserName());
           details.setPdcDateLastModified(new java.util.Date());
           
           ArrayList iliList = new ArrayList();

           for (int i = 0; i<actionForm.getArILIListSize(); i++) {
           	
           	   ArPdcLineItemList arILIList = actionForm.getArILIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arILIList.getLocation()) &&
           	   	   Common.validateRequired(arILIList.getItemName()) &&
				   Common.validateRequired(arILIList.getUnit()) &&
				   Common.validateRequired(arILIList.getUnitPrice()) &&
				   Common.validateRequired(arILIList.getQuantity())) continue;

           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();
           	   
           	   mdetails.setIliLine(Common.convertStringToShort(arILIList.getLineNumber()));
           	   mdetails.setIliIiName(arILIList.getItemName());
           	   mdetails.setIliLocName(arILIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arILIList.getQuantity(), quantityPrecision));
           	   mdetails.setIliUomName(arILIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arILIList.getAmount(), precisionUnit));
           	   
           	   iliList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	    ejbPDC.saveArPdcIliEntry(details, actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(),
           	        iliList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));
           	    
           } catch (GlobalDocumentNumberNotUniqueException ex) {
             	
         	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("pdcEntry.error.documentNumberNotUnique"));
          	    
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.conversionDateNotExist"));   
           	    
           } catch (GlobalPaymentTermInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.paymentTermInvalid"));      
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyVoid"));
           	   
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.noItemLocationFound", ex.getMessage()));           	   
                                	           	           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 
           
/*******************************************************
   -- Ar PDC ILI Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("ITEMS") &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArPdcDetails details = new ArPdcDetails();
           
           details.setPdcCode(actionForm.getPdcCode());  
           
           if(Common.convertStringToSQLDate(actionForm.getMaturityDate()).before(new java.util.Date()) ||
           		Common.convertStringToSQLDate(actionForm.getMaturityDate()).equals(new java.util.Date())) {
           	
           	details.setPdcStatus("MATURED");
            
           } else {
           	
           	details.setPdcStatus(actionForm.getStatus());
            
           }
           
           details.setPdcCheckNumber(actionForm.getCheckNumber());
           details.setPdcReferenceNumber(actionForm.getReferenceNumber());           
           details.setPdcDateReceived(Common.convertStringToSQLDate(actionForm.getDateReceived()));
           details.setPdcMaturityDate(Common.convertStringToSQLDate(actionForm.getMaturityDate()));
           details.setPdcAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
           details.setPdcDescription(actionForm.getDescription());           
           details.setPdcCancelled(Common.convertBooleanToByte(actionForm.getCancelled()));           
           details.setPdcLvFreight(actionForm.getFreight());
           details.setPdcConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setPdcConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
           details.setPdcLvShift(actionForm.getShift());
           details.setPdcEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
                                 
           if (actionForm.getPdcCode() == null) {
           	
           	   details.setPdcCreatedBy(user.getUserName());
	           details.setPdcDateCreated(new java.util.Date());
	                      
           }
           
           details.setPdcLastModifiedBy(user.getUserName());
           details.setPdcDateLastModified(new java.util.Date());
           
           ArrayList iliList = new ArrayList();

           for (int i = 0; i<actionForm.getArILIListSize(); i++) {
           	
           	   ArPdcLineItemList arILIList = actionForm.getArILIByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(arILIList.getLocation()) &&
           	   	   Common.validateRequired(arILIList.getItemName()) &&
				   Common.validateRequired(arILIList.getUnit()) &&
				   Common.validateRequired(arILIList.getUnitPrice()) &&
				   Common.validateRequired(arILIList.getQuantity())) continue;

           	   ArModInvoiceLineItemDetails mdetails = new ArModInvoiceLineItemDetails();
           	   
           	   mdetails.setIliLine(Common.convertStringToShort(arILIList.getLineNumber()));
           	   mdetails.setIliIiName(arILIList.getItemName());
           	   mdetails.setIliLocName(arILIList.getLocation());
           	   mdetails.setIliQuantity(Common.convertStringMoneyToDouble(arILIList.getQuantity(), quantityPrecision));
           	   mdetails.setIliUomName(arILIList.getUnit());
           	   mdetails.setIliUnitPrice(Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit));
           	   mdetails.setIliAmount(Common.convertStringMoneyToDouble(arILIList.getAmount(), precisionUnit));
           	   
           	   iliList.add(mdetails);
           	
           }                     	
           
           try {
           	
           	    Integer pdcCode = ejbPDC.saveArPdcIliEntry(details, actionForm.getPaymentTerm(),
           	        actionForm.getTaxCode(), actionForm.getWithholdingTax(),
           	        actionForm.getCurrency(), actionForm.getCustomer(),
           	        iliList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	        
           	    actionForm.setPdcCode(pdcCode);
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));
           	    
           } catch (GlobalDocumentNumberNotUniqueException ex) {
             	
         	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("pdcEntry.error.documentNumberNotUnique"));
          	    
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.conversionDateNotExist"));   
           	
           } catch (GlobalPaymentTermInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.paymentTermInvalid"));   
                     	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.transactionAlreadyVoid"));
           	   
           } catch (GlobalInvItemLocationNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.noItemLocationFound", ex.getMessage()));           	   
                                       	           	           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }

           
/*******************************************************
   -- Ar PDC RCT Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null && actionForm.getType().equals("PR") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                 	           	
                 ArPdcDetails details = new ArPdcDetails();
                            
                 details.setPdcCode(actionForm.getPdcCode());  
                 
                 if(Common.convertStringToSQLDate(actionForm.getMaturityDate()).before(new java.util.Date()) ||
                 		Common.convertStringToSQLDate(actionForm.getMaturityDate()).equals(new java.util.Date())) {
                 	
                 	details.setPdcStatus("MATURED");
                  
                 } else {
                 	
                 	details.setPdcStatus(actionForm.getStatus());
                  
                 }
                 
                 details.setPdcCheckNumber(actionForm.getCheckNumber());
                 details.setPdcReferenceNumber(actionForm.getReferenceNumber());           
                 details.setPdcDateReceived(Common.convertStringToSQLDate(actionForm.getDateReceived()));
                 details.setPdcMaturityDate(Common.convertStringToSQLDate(actionForm.getMaturityDate()));
                 details.setPdcAmount(0);
                 details.setPdcDescription(actionForm.getDescription());           
                 details.setPdcConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                 details.setPdcConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
                 
                 if (actionForm.getPdcCode() == null) {
                 	
                 	   details.setPdcCreatedBy(user.getUserName());
                 	   details.setPdcDateCreated(new java.util.Date());
      	                      
                 }
                 
                 details.setPdcLastModifiedBy(user.getUserName());
                 details.setPdcDateLastModified(new java.util.Date());
                 
                 ArrayList ipsList = new ArrayList();
                 
                 for (int i = 0; i<actionForm.getArRCTListSize(); i++) {
                 	
                 	   ArPdcPrList arRCTList = actionForm.getArRCTByIndex(i);           	   
                 	              	   
                 	   if (!arRCTList.getPayCheckbox()) continue;
                 	   
                 	   ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();
                 	   
                 	   mdetails.setAiIpsCode(arRCTList.getInvoicePaymentScheduleCode());
                 	   mdetails.setAiApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit));
                 	   mdetails.setAiCreditableWTax(Common.convertStringMoneyToDouble(arRCTList.getCreditableWTax(), precisionUnit));
                 	   mdetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(arRCTList.getDiscount(), precisionUnit));
                 	   mdetails.setAiAllocatedPaymentAmount(Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit));
                 	   mdetails.setAiAppliedDeposit(Common.convertStringMoneyToDouble(arRCTList.getAppliedDeposit(), precisionUnit));
                 	   ipsList.add(mdetails);
                 	   
                 	   details.setPdcAmount(details.getPdcAmount() + mdetails.getAiApplyAmount());
                 }
                 
                 try {
                	 
                 	    ejbPDC.saveArRctEntry(details, actionForm.getBankAccount(), actionForm.getCurrency(), 
                 	    		actionForm.getCustomer(), ipsList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                 	    
                 } catch (GlobalRecordAlreadyDeletedException ex) {
                 	
                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));
                 	    
                 } catch (GlobalDocumentNumberNotUniqueException ex) {
                   	
               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("pdcEntry.error.documentNumberNotUnique"));
                	    
                 } catch (GlobalConversionDateNotExistException ex) {
                 	
                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.conversionDateNotExist"));   
                 	    
                 } catch (GlobalPaymentTermInvalidException ex) {
                 	
                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.paymentTermInvalid"));      
                           	
                 } catch (GlobalTransactionAlreadyApprovedException ex) {
                 	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.transactionAlreadyApproved"));
                 	
                 } catch (GlobalTransactionAlreadyPendingException ex) {
                 	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.transactionAlreadyPending"));
                 	
                 } catch (GlobalTransactionAlreadyPostedException ex) {
                 	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.transactionAlreadyPosted"));
                 	
                 } catch (GlobalTransactionAlreadyVoidException ex) {
                 	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.transactionAlreadyVoid"));

                 } catch (ArINVOverapplicationNotAllowedException ex) {
                  	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.overapplicationNotAllowed"));
                 
                 } catch (ArRCTInvoiceHasNoWTaxCodeException ex) {
                 	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.invoiceHasNoWTaxCode"));
                 	   
                 } catch (GlobalTransactionAlreadyLockedException ex) {
                 	
                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("pdcEntry.error.transactionAlreadyLocked"));
                 	   
                 } catch (EJBException ex) {
                 	    if (log.isInfoEnabled()) {
                     	
                        log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                           " session: " + session.getId());
                 	    }
                 
               return(mapping.findForward("cmnErrorPage"));
               
             }
             
/*******************************************************
   -- Ar PDC RCT Save & Submit Action --
*******************************************************/
                 
         } else if (request.getParameter("saveSubmitButton") != null && actionForm.getType().equals("PR") &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

             ArPdcDetails details = new ArPdcDetails();
             
             details.setPdcCode(actionForm.getPdcCode());  
             
             if(Common.convertStringToSQLDate(actionForm.getMaturityDate()).before(new java.util.Date()) ||
             		Common.convertStringToSQLDate(actionForm.getMaturityDate()).equals(new java.util.Date())) {
             	
             	details.setPdcStatus("MATURED");
              
             } else {
             	
             	details.setPdcStatus(actionForm.getStatus());
              
             }
             
             details.setPdcCheckNumber(actionForm.getCheckNumber());
             details.setPdcReferenceNumber(actionForm.getReferenceNumber());           
             details.setPdcDateReceived(Common.convertStringToSQLDate(actionForm.getDateReceived()));
             details.setPdcMaturityDate(Common.convertStringToSQLDate(actionForm.getMaturityDate()));
             details.setPdcAmount(0);
             details.setPdcDescription(actionForm.getDescription());           
             details.setPdcConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setPdcConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
             
             if (actionForm.getPdcCode() == null) {
             	
             	   details.setPdcCreatedBy(user.getUserName());
             	   details.setPdcDateCreated(new java.util.Date());
  	                      
             }
             
             details.setPdcLastModifiedBy(user.getUserName());
             details.setPdcDateLastModified(new java.util.Date());
             
             ArrayList ipsList = new ArrayList();
             
             for (int i = 0; i<actionForm.getArRCTListSize(); i++) {
             	
             	   ArPdcPrList arRCTList = actionForm.getArRCTByIndex(i);           	   
             	              	   
             	   if (!arRCTList.getPayCheckbox()) continue;
             	   
             	   ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();
             	   
             	   mdetails.setAiIpsCode(arRCTList.getInvoicePaymentScheduleCode());
             	   mdetails.setAiApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit));
             	   mdetails.setAiCreditableWTax(Common.convertStringMoneyToDouble(arRCTList.getCreditableWTax(), precisionUnit));
             	   mdetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(arRCTList.getDiscount(), precisionUnit));
             	   mdetails.setAiAllocatedPaymentAmount(Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit));
             	   mdetails.setAiAppliedDeposit(Common.convertStringMoneyToDouble(arRCTList.getAppliedDeposit(), precisionUnit));
             	   ipsList.add(mdetails);
             	   
             	   details.setPdcAmount(details.getPdcAmount() + mdetails.getAiApplyAmount());
             }
             
             try {
            	 
             	    Integer pdcCode = ejbPDC.saveArRctEntry(details, actionForm.getBankAccount(), actionForm.getCurrency(), 
             	    		actionForm.getCustomer(), ipsList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
             	    
               	    actionForm.setPdcCode(pdcCode);
            	
             } catch (GlobalRecordAlreadyDeletedException ex) {
             	
             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));
             	    
             } catch (GlobalDocumentNumberNotUniqueException ex) {
               	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.documentNumberNotUnique"));
            	    
             } catch (GlobalConversionDateNotExistException ex) {
             	
             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.conversionDateNotExist"));   
             	    
             } catch (GlobalPaymentTermInvalidException ex) {
             	
             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.paymentTermInvalid"));      
                       	
             } catch (GlobalTransactionAlreadyApprovedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.transactionAlreadyApproved"));
             	
             } catch (GlobalTransactionAlreadyPendingException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.transactionAlreadyPending"));
             	
             } catch (GlobalTransactionAlreadyPostedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.transactionAlreadyPosted"));
             	
             } catch (GlobalTransactionAlreadyVoidException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.transactionAlreadyVoid"));

             } catch (ArINVOverapplicationNotAllowedException ex) {
              	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.overapplicationNotAllowed"));
             
             } catch (ArRCTInvoiceHasNoWTaxCodeException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.invoiceHasNoWTaxCode"));
             	   
             } catch (GlobalTransactionAlreadyLockedException ex) {
             	
             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("pdcEntry.error.transactionAlreadyLocked"));
             	   
             } catch (EJBException ex) {
             	    if (log.isInfoEnabled()) {
                 	
                    log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                       " session: " + session.getId());
             	    }
             
           return(mapping.findForward("cmnErrorPage"));
           
         }

 /*******************************************************
	 -- Ar RCT Print Action --
*******************************************************/             	
                    	
	     } else if (request.getParameter("printButton") != null) {
	
	     	if(Common.validateRequired(actionForm.getApprovalStatus())) {
	
	           ArPdcDetails details = new ArPdcDetails();
	           
	           details.setPdcCode(actionForm.getPdcCode());
	           details.setPdcStatus(actionForm.getStatus());
	           details.setPdcCheckNumber(actionForm.getCheckNumber());
	           details.setPdcReferenceNumber(actionForm.getReferenceNumber());           
	           details.setPdcDateReceived(Common.convertStringToSQLDate(actionForm.getDateReceived()));
	           details.setPdcMaturityDate(Common.convertStringToSQLDate(actionForm.getMaturityDate()));
	           //details.setPdcAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	           details.setPdcAmount(0);
	           details.setPdcDescription(actionForm.getDescription());           
	           details.setPdcCancelled(Common.convertBooleanToByte(actionForm.getCancelled()));           
	           details.setPdcLvFreight(actionForm.getFreight());
	           details.setPdcConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	           details.setPdcConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
	           details.setPdcLvShift(actionForm.getShift());
	           details.setPdcEffectivityDate(Common.convertStringToSQLDate(actionForm.getEffectivityDate()));
	                                 
	           if (actionForm.getPdcCode() == null) {
	           	
	           	   details.setPdcCreatedBy(user.getUserName());
		           details.setPdcDateCreated(new java.util.Date());
		                      
	           }
	           
	           details.setPdcLastModifiedBy(user.getUserName());
	           details.setPdcDateLastModified(new java.util.Date());
	           
	           ArrayList ipsList = new ArrayList();
	                      
	           for (int i = 0; i<actionForm.getArRCTListSize(); i++) {
	           	
	           	   ArPdcPrList arRCTList = actionForm.getArRCTByIndex(i);           	   
	           	              	   
	           	   if (!arRCTList.getPayCheckbox()) continue;
	           	   
	           	   ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();
	           	   
	           	   mdetails.setAiIpsCode(arRCTList.getInvoicePaymentScheduleCode());
	           	   mdetails.setAiApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit));
	           	   mdetails.setAiCreditableWTax(Common.convertStringMoneyToDouble(arRCTList.getCreditableWTax(), precisionUnit));
	           	   mdetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(arRCTList.getDiscount(), precisionUnit));
	           	   mdetails.setAiAllocatedPaymentAmount(Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit));
	           	   mdetails.setAiAppliedDeposit(Common.convertStringMoneyToDouble(arRCTList.getAppliedDeposit(), precisionUnit));
	           	
	           	   ipsList.add(mdetails);
	           	   
	           	   details.setPdcAmount(details.getPdcAmount() + mdetails.getAiApplyAmount());
	           }
	                      
	           try {
	        	   
	           	    Integer pdcCode  = ejbPDC.saveArRctEntry(details, actionForm.getBankAccount(),
	           	        actionForm.getCurrency(), actionForm.getCustomer(), 
	           	        ipsList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	           	        
	           	    actionForm.setPdcCode(pdcCode);
	           	
	           } catch (GlobalRecordAlreadyDeletedException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));
	           	    
	           } catch (GlobalDocumentNumberNotUniqueException ex) {
	              	
	          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("pdcEntry.error.documentNumberNotUnique"));
	           	    
	           } catch (GlobalConversionDateNotExistException ex) {
	           	
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.conversionDateNotExist"));           	                      	
	                     	
	           } catch (GlobalTransactionAlreadyApprovedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.transactionAlreadyApproved"));
	           	
	           } catch (GlobalTransactionAlreadyPendingException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.transactionAlreadyPending"));
	           	
	           } catch (GlobalTransactionAlreadyPostedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.transactionAlreadyPosted"));
	           	
	           } catch (GlobalTransactionAlreadyVoidException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.transactionAlreadyVoid"));
	                    
	           } catch (ArINVOverapplicationNotAllowedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.overapplicationNotAllowed", ex.getMessage()));
	           
	           } catch (ArRCTInvoiceHasNoWTaxCodeException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.invoiceHasNoWTaxCode", ex.getMessage()));
	                    
	           } catch (GlobalTransactionAlreadyLockedException ex) {
	           	
	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("pdcEntry.error.transactionAlreadyLocked", ex.getMessage()));
	                    
	           } catch (EJBException ex) {
	           	    if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                }
	               
	               return(mapping.findForward("cmnErrorPage"));
	           }
	
	     		
	           if (!errors.isEmpty()) {
	    	
	               saveErrors(request, new ActionMessages(errors));
	               return(mapping.findForward("arPdcEntry"));
	               
	           }
	           
	           actionForm.setReport(Constants.STATUS_SUCCESS);
	           
	           isInitialPrinting = true;
	                     	
	        }  else {
	        	
	        	actionForm.setReport(Constants.STATUS_SUCCESS);
	        	
	        	return(mapping.findForward("arPdcEntry"));
	        	
	        }
        	 
/*******************************************************
   -- Ar PDC Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ar PDC Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {
         	         	         	
         	int listSize = actionForm.getArPDCListSize();
         	int lineNumber = 0;
                                   
            for (int x = listSize + 1; x <= listSize + pdcLineNumber; x++) {
	        	
	        	ArPdcEntryList arPDCList = new ArPdcEntryList(actionForm, 
	        	    null, String.valueOf(++lineNumber), null, null, null, null, null, false);	       
	        		        	    
	        	arPDCList.setMemoLineList(actionForm.getMemoLineList());
	        	
	        	actionForm.saveArPDCList(arPDCList);
	        	
	        }	
	        
	        for (int i = 0; i<actionForm.getArPDCListSize(); i++) {
           	
           	   ArPdcEntryList arPDCList = actionForm.getArPDCByIndex(i);
           	   
           	   arPDCList.setLineNumber(String.valueOf(i+1));
           	   
            }        
	        
	        return(mapping.findForward("arPdcEntry"));
	        
/*******************************************************
   -- Ar PDC Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("MEMO LINES")) {
         	
         	for (int i = 0; i<actionForm.getArPDCListSize(); i++) {
           	
           	   ArPdcEntryList arPDCList = actionForm.getArPDCByIndex(i);
           	   
           	   if (arPDCList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteArPDCList(i);
           	   	   i--;
           	   }
           	   
            }
            	        
	        return(mapping.findForward("arPdcEntry"));
	        
/*******************************************************
   -- Ar PDC ILI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null && actionForm.getType().equals("ITEMS")) {
         	
         	int listSize = actionForm.getArILIListSize();
                                   
            for (int x = listSize + 1; x <= listSize + pdcLineNumber; x++) {
	        	
	        	ArPdcLineItemList arILIList = new ArPdcLineItemList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null, null, null);
	        	    
	        	arILIList.setLocationList(actionForm.getLocationList());
	        	
	        	actionForm.saveArILIList(arILIList);
	        	
	        }	        
	        
	        return(mapping.findForward("arPdcEntry"));
	        
/*******************************************************
   -- Ar PDC ILI Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null && actionForm.getType().equals("ITEMS")) {
         	
         	for (int i = 0; i<actionForm.getArILIListSize(); i++) {
           	
           	   ArPdcLineItemList arILIList = actionForm.getArILIByIndex(i);
           	   
           	   if (arILIList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteArILIList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getArILIListSize(); i++) {
           	
           	   ArPdcLineItemList arILIList = actionForm.getArILIByIndex(i);
           	   
           	   arILIList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("arPdcEntry"));
			
/*******************************************************
   -- Ar PDC Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbPDC.deleteArPdcEntry(actionForm.getPdcCode(), user.getCmpCode());
           	
            } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.recordAlreadyDeleted"));
                    
            } catch (EJBException ex) {
            
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  
                }
               
                return(mapping.findForward("cmnErrorPage"));
                
            }			

/*******************************************************
   -- Ar PDC Customer Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

        	 
	        	 if(actionForm.getType().equals("ITEMS") || actionForm.getType().equals("MEMO LINES"))
	        	 {
		         	try {
		                 ArModCustomerDetails mdetails = ejbPDC.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());
		                 
		                 actionForm.setPaymentTerm(mdetails.getCstPytName());
		                 actionForm.setTaxCode(mdetails.getCstCcTcName());
		                 actionForm.setWithholdingTax(mdetails.getCstCcWtcName());
		
		             } catch (GlobalNoRecordFoundException ex) {
		             	           	
		           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("pdcEntry.error.customerNoRecordFound"));
		                saveErrors(request, new ActionMessages(errors));
		             	
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		             }
	        	 }
	             
	             //**********************
	             
	             if (actionForm.getType().equals("PR")) 
	             {
	            	 try {
	            		 System.out.print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$PR");
	              		
	                    ArModCustomerDetails mdetails = ejbPDC.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());
	                    
	                    actionForm.clearArRCTList();
	                    actionForm.setBankAccount(mdetails.getCstCtBaName());
	                    
	                    System.out.print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$Bank Name: "+ mdetails.getCstCtBaName());
	                    
	                    
	                    actionForm.setPaymentMethod(mdetails.getCstPaymentMethod());
	                    actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
	                    actionForm.setCustomerName(mdetails.getCstName());
	                    
	                } catch (GlobalNoRecordFoundException ex) {
	                	
	                	actionForm.clearArRCTList();
	              	
	              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                       new ActionMessage("receiptEntry.error.customerNoRecordFound"));
	                   saveErrors(request, new ActionMessages(errors));
	                	
	                } catch (EJBException ex) {

	                  if (log.isInfoEnabled()) {

	                     log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());
	                     return mapping.findForward("cmnErrorPage"); 
	                     
	                  }

	               } 

	               try {
	                 	
	                 	// populate availabe deposit read-only
	                 	
	                 	double deposit = ejbPDC.getArRctDepositAmountByCstCustomerCode(actionForm.getCustomer(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	                 	
	                 	actionForm.setCustomerDeposit(Common.convertDoubleToStringMoney(deposit, precisionUnit));
	                 	
	               } catch (GlobalNoRecordFoundException ex) {
	                 	
	                 	actionForm.setCustomerDeposit(null);
	                 	
	               } catch (EJBException ex) {
	                 	
		               if (log.isInfoEnabled()) {
		                 		
	                 		log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
	                 				" session: " + session.getId());
	                 		return mapping.findForward("cmnErrorPage"); 
		                 		
		               }
	                 	
	               }
	                 
	                try {
	                	
	                     ArrayList list = ejbPDC.getArIpsByCstCustomerCode(actionForm.getCustomer(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	                     
	                     Iterator i = list.iterator();
	                     
	                     while (i.hasNext()) {
	                     	
	                     	ArModInvoicePaymentScheduleDetails mdetails = (ArModInvoicePaymentScheduleDetails)i.next();
	                     	
	                     	ArPdcPrList ipsList = new ArPdcPrList(
	                     		actionForm, mdetails.getIpsCode(), 
	                     		mdetails.getIpsInvNumber(),
	                     		Common.convertShortToString(mdetails.getIpsNumber()),
	                     		Common.convertSQLDateToString(mdetails.getIpsDueDate()),
	                     		mdetails.getIpsInvReferenceNumber(),
	                     		mdetails.getIpsInvFcName(),
	                     		Common.convertDoubleToStringMoney(mdetails.getIpsAmountDue(), precisionUnit),
	                     		Common.convertDoubleToStringMoney(mdetails.getIpsAiApplyAmount(), precisionUnit),
	                     		Common.convertDoubleToStringMoney(mdetails.getIpsAiCreditableWTax(), precisionUnit),
	                     		Common.convertDoubleToStringMoney(0d, precisionUnit),
	    					    Common.convertDoubleToStringMoney(mdetails.getIpsAiDiscountAmount(), precisionUnit),
	                     		Common.convertDoubleToStringMoney(0d, precisionUnit),
	                     		Common.convertDoubleToStringMoney(mdetails.getIpsAiDiscountAmount(), precisionUnit));
	                     	
	                    		
	                         actionForm.saveArRCTList(ipsList);
	                         
	                     }
	                     
	                     // check if prev should be disabled
	                     if (actionForm.getLineCount() == 0) {
	                     	
	                     	actionForm.setDisablePreviousButton(true);
	                     	actionForm.setDisableFirstButton(true);
	                     	
	                     } else {
	                     	
	                     	actionForm.setDisablePreviousButton(false);
	                     	actionForm.setDisableFirstButton(false);
	                     	
	                     }
	                     
	                     // check if next should be disabled
	                     if (actionForm.getArRCTListSize() <= actionForm.getLineCount() + actionForm.getMaxRows()) {
	                     	
	                     	actionForm.setDisableNextButton(true);
	                     	actionForm.setDisableLastButton(true);
	                     	
	                     } else {
	                     	
	                     	actionForm.setDisableNextButton(false);
	                     	actionForm.setDisableLastButton(false);
	                     	
	                     }
	                     
	                 } catch (GlobalNoRecordFoundException ex) {
	                 	
	                 	actionForm.clearArRCTList();
	                	 
	               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("receiptEntry.error.noOpenInvoiceFound"));
	                    saveErrors(request, new ActionMessages(errors));
	                 	
	                 } catch (EJBException ex) {
	
	                   if (log.isInfoEnabled()) {
	
	                      log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
	                      " session: " + session.getId());
	                      return mapping.findForward("cmnErrorPage"); 
	                      
	                   }
	
	                }
             }
             //**********************
            return(mapping.findForward("arPdcEntry"));

/*******************************************************
   -- Ar PDC Memo Line Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("arPDCList[" + 
            actionForm.getRowSelected() + "].isMemoLineEntered")) && actionForm.getType().equals("MEMO LINES")) {
            	
           ArPdcEntryList arPDCList =
              actionForm.getArPDCByIndex(actionForm.getRowSelected());
                         
         	try {
             	
                 ArStandardMemoLineDetails smlDetails = ejbPDC.getArSmlBySmlName(arPDCList.getMemoLine(), user.getCmpCode());
                 
                 arPDCList.setDescription(smlDetails.getSmlDescription());
                 arPDCList.setQuantity("1");
                 arPDCList.setUnitPrice(Common.convertDoubleToStringMoney(smlDetails.getSmlUnitPrice(), precisionUnit));
                 arPDCList.setAmount(arPDCList.getUnitPrice());
                 arPDCList.setTaxable(Common.convertByteToBoolean(smlDetails.getSmlTax()));
                 
             } catch (GlobalNoRecordFoundException ex) {             	          	
           	                 	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  	
                        
            return(mapping.findForward("arPdcEntry"));
            
/*******************************************************
    -- Ar PDC ILI Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("arILIList[" + 
        actionForm.getRowILISelected() + "].isItemEntered"))) {
        
      	ArPdcLineItemList arILIList = 
      		actionForm.getArILIByIndex(actionForm.getRowILISelected());      	    	
                      
      	try {
          	
      		// populate unit field class
      		
      		ArrayList uomList = new ArrayList();
      		uomList = ejbPDC.getInvUomByIiName(arILIList.getItemName(), user.getCmpCode());
      		
      		arILIList.clearUnitList();
      		arILIList.setUnitList(Constants.GLOBAL_BLANK);
      		
  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {
      			
      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();
      			
      			arILIList.setUnitList(mUomDetails.getUomName());
      			
      			if (mUomDetails.isDefault()) {
      				
      				arILIList.setUnit(mUomDetails.getUomName());      				
      				
      			}      			
      			
      		}
      		
      		// populate unit cost field
      		
      		if (!Common.validateRequired(arILIList.getItemName()) && !Common.validateRequired(arILIList.getUnit())) {
      		
	      		double unitPrice = ejbPDC.getInvIiSalesPriceByIiNameAndUomName(arILIList.getItemName(), arILIList.getUnit(), user.getCmpCode());
	      		arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
	      		
      		}
      		
      		// populate amount field
	        
	        if(!Common.validateRequired(arILIList.getQuantity()) && !Common.validateRequired(arILIList.getUnitPrice())) {
	                        	        	
	        	double amount = Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) * Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit);
	        	arILIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit)); 
	        	        
	        }
      		      		      		
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  
	                               
         return(mapping.findForward("arPdcEntry"));
         
/*******************************************************
    -- Ar PDC ILI Unit Enter Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("arILIList[" + 
        actionForm.getRowILISelected() + "].isUnitEntered"))) {
        
      	ArPdcLineItemList arILIList = 
      		actionForm.getArILIByIndex(actionForm.getRowILISelected());
                      
      	try {
      	
      	    // populate unit price field
      		
      		if (!Common.validateRequired(arILIList.getLocation()) && !Common.validateRequired(arILIList.getItemName())) {
      			      			      		
	      		double unitprice = ejbPDC.getInvIiSalesPriceByIiNameAndUomName(arILIList.getItemName(), arILIList.getUnit(), user.getCmpCode());
	      		arILIList.setUnitPrice(Common.convertDoubleToStringMoney(unitprice, precisionUnit));
	      		
	        }
	        
	        // populate amount field
	        
	        if(!Common.validateRequired(arILIList.getQuantity()) && !Common.validateRequired(arILIList.getUnitPrice())) {
	                        	        	
	        	double amount = Common.convertStringMoneyToDouble(arILIList.getQuantity(), precisionUnit) * Common.convertStringMoneyToDouble(arILIList.getUnitPrice(), precisionUnit);
	        	arILIList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit)); 
	        	        
	        }
      		       	                 	
          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage"); 
               
            }

         }  	
                     
         return(mapping.findForward("arPdcEntry"));
         
/*******************************************************
   -- Ar PDC ILI Type Enter Action --
*******************************************************/

         } else if (!Common.validateRequired(request.getParameter("isTypeEntered"))){
	       
	        if (actionForm.getType().equals("ITEMS")) {
	        	
	        	System.out.println("ITEMS");
	        
	           	actionForm.clearArPDCList();
	           	actionForm.clearArILIList();
	           	actionForm.clearArRCTList();

	           	// Populate line when not forwarding
            	
            	for (int x = 1; x <= pdcLineNumber; x++) {
            		
            		ArPdcLineItemList arILIList = new ArPdcLineItemList(actionForm, 
            				null, new Integer(x).toString(), null, null, null, null, null, 
							null, null);	
            		
            		arILIList.setLocationList(actionForm.getLocationList());
            		
            		arILIList.setUnitList(Constants.GLOBAL_BLANK);
            		arILIList.setUnitList("Select Item First");
            		
            		actionForm.saveArILIList(arILIList);
            		
            	}
	        } else if (actionForm.getType().equals("PR")) {
	        	
	        	System.out.println("PR");

	           	actionForm.clearArPDCList();
	           	actionForm.clearArILIList();
	           	actionForm.clearArRCTList();
	           	
		        actionForm.setLineCount(0);
		        actionForm.setMaxRows(5);
		        actionForm.setDisablePreviousButton(true);
		        actionForm.setDisableFirstButton(true);
		        
		        // check if next should be disabled
		        if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {
		        	
		        	actionForm.setDisableNextButton(true);
		        	actionForm.setDisableLastButton(true);
		        	
		        } else {
		        	
		        	actionForm.setDisableNextButton(false);
		        	actionForm.setDisableLastButton(false);
		        	
		        }
	        
	        } else {
	        	
	        	System.out.println("MEMO LINES");
	        	
	           	actionForm.clearArPDCList();
	           	actionForm.clearArILIList();
	           	actionForm.clearArRCTList();
	           	
		        for (int x = 1; x <= pdcLineNumber; x++) {
		        	
		        	ArPdcEntryList arPDCList = new ArPdcEntryList(actionForm, 
		        	    null, new Integer(x).toString(), null, null, null, null, null, false);
		        	
		        	arPDCList.setMemoLineList(actionForm.getMemoLineList());
		        	
		        	actionForm.saveArPDCList(arPDCList);
		        	
		         }

	        }
	     
	        return(mapping.findForward("arPdcEntry"));                        

/*******************************************************
   -- Ar PDC Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {
         	
         	try {
         		
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
         				ejbPDC.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
         						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));
         		
         	} catch (GlobalConversionDateNotExistException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("pdcEntry.error.conversionDateNotExist"));
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			
         		}
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	} 
         	
         	if (!errors.isEmpty()) {
         		
         		saveErrors(request, new ActionMessages(errors));
         		
         	}
         	
         	return(mapping.findForward("arPdcEntry"));


/*******************************************************
   -- Ar PDC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arPdcEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbPDC.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();
            			
            			actionForm.setCurrencyList(mFcDetails.getFcName());
            			
            			if (mFcDetails.getFcSob() == 1) {
            				
            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());
            				
            			}            			
            		                			
            		}
            		
            	}
            	
            	actionForm.clearPaymentTermList();           	
            	
            	list = ejbPDC.getAdPytAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setPaymentTermList((String)i.next());
            			
            		}            		
            		
            		actionForm.setPaymentTerm("IMMEDIATE");
            		            		
            	}    
            	
            	actionForm.clearTaxCodeList();           	
            	
            	list = ejbPDC.getArTcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setTaxCodeList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            	
            	actionForm.clearWithholdingTaxList();           	
            	
            	list = ejbPDC.getArWtcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setWithholdingTaxList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setWithholdingTaxList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            	actionForm.clearBankAccountList();           	
            	
            	list = ejbPDC.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}            		
            		            		            		
            	}                	
            	
            	if (actionForm.getUseCustomerPulldown()) {
            		
            		actionForm.clearCustomerList();              
            		
            		list = ejbPDC.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerList((String)i.next());
            				
            			}
            			
            		} 
            		
            	}
            	
            	actionForm.clearFreightList();           	
            	
            	list = ejbPDC.getAdLvArFreightAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setFreightList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setFreightList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.clearShiftList();           	
            	
            	list = ejbPDC.getAdLvInvShiftAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setShiftList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setShiftList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.clearMemoLineList();           	
            	
            	list = ejbPDC.getArSmlAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setMemoLineList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setMemoLineList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            	actionForm.clearLocationList();       	
            	
            	list = ejbPDC.getInvLocAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setLocationList((String)i.next());
            			
            		}
            		            		
            	} 
            	            	
            	
            	actionForm.clearArPDCList();
            	actionForm.clearArILIList();
            	actionForm.clearArRCTList();
            	
            	if (request.getParameter("forward") != null || isInitialPrinting) {
            		
            		if (request.getParameter("forward") != null) {
            			            	    
            			actionForm.setPdcCode(new Integer(request.getParameter("pdcCode")));
            			
            		}
            		
            		ArModPdcDetails mdetails = ejbPDC.getArPdcByPdcCode(actionForm.getPdcCode(), user.getCmpCode());
            		
            		actionForm.setStatus(mdetails.getPdcStatus());
            		actionForm.setDescription(mdetails.getPdcDescription());
            		actionForm.setDateReceived(Common.convertSQLDateToString(mdetails.getPdcDateReceived()));
            		actionForm.setMaturityDate(Common.convertSQLDateToString(mdetails.getPdcMaturityDate()));
            		actionForm.setCheckNumber(mdetails.getPdcCheckNumber());
            		actionForm.setReferenceNumber(mdetails.getPdcReferenceNumber());            		
            		actionForm.setFreight(mdetails.getPdcLvFreight());
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getPdcConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getPdcConversionRate(), (short)40));            		
            		actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getPdcAmount(), precisionUnit));
            		actionForm.setCancelled(Common.convertByteToBoolean(mdetails.getPdcCancelled()));
            		actionForm.setApprovalStatus(mdetails.getPdcApprovalStatus());
            		actionForm.setPosted(mdetails.getPdcPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getPdcCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getPdcDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getPdcLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getPdcDateLastModified()));            		
            		actionForm.setPostedBy(mdetails.getPdcPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getPdcDatePosted()));
            		actionForm.setEffectivityDate(Common.convertSQLDateToString(mdetails.getPdcEffectivityDate()));
            		
            		if (!actionForm.getCurrencyList().contains(mdetails.getPdcFcName())) {
            			
            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearCurrencyList();
            				
            			}
            			actionForm.setCurrencyList(mdetails.getPdcFcName());
            			
            		}
            		actionForm.setCurrency(mdetails.getPdcFcName());
            		
            		if (!actionForm.getTaxCodeList().contains(mdetails.getPdcTcName())) {
            			
            			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearTaxCodeList();
            				
            			}
            			actionForm.setTaxCodeList(mdetails.getPdcTcName());
            			
            		}
            		actionForm.setTaxCode(mdetails.getPdcTcName());
            		
            		if (!actionForm.getWithholdingTaxList().contains(mdetails.getPdcWtcName())) {
            			
            			if (actionForm.getWithholdingTaxList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearWithholdingTaxList();
            				
            			}
            			actionForm.setWithholdingTaxList(mdetails.getPdcWtcName());
            			
            		}
            		actionForm.setWithholdingTax(mdetails.getPdcWtcName());
            		
            		if (!actionForm.getCustomerList().contains(mdetails.getPdcCstCustomerCode())) {
            			
            			if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearCustomerList();
            				
            			}
            			actionForm.setCustomerList(mdetails.getPdcCstCustomerCode());
            			
            		}
            		actionForm.setCustomer(mdetails.getPdcCstCustomerCode());
            		
            		if (!actionForm.getPaymentTermList().contains(mdetails.getPdcPytName())) {
            			
            			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearPaymentTermList();
            				
            			}
            			actionForm.setPaymentTermList(mdetails.getPdcPytName());
            			
            		}
            		actionForm.setPaymentTerm(mdetails.getPdcPytName());
            		
            		actionForm.setShift(mdetails.getPdcLvShift());					            		           		    
            		
            		if (mdetails.getPdcIliList() != null && !mdetails.getPdcIliList().isEmpty()) {
            		
            			System.out.println("Hello Items");
            			
            		    actionForm.setType("ITEMS");
            		
            		    list = mdetails.getPdcIliList();            		
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails)i.next();
            				
            				ArPdcLineItemList arIliList = new ArPdcLineItemList(actionForm,
            						mIliDetails.getIliCode(),
									Common.convertShortToString(mIliDetails.getIliLine()),
									mIliDetails.getIliLocName(),
									mIliDetails.getIliIiName(),
									mIliDetails.getIliIiDescription(),
									Common.convertDoubleToStringMoney(mIliDetails.getIliQuantity(), quantityPrecision),
									mIliDetails.getIliUomName(),
									Common.convertDoubleToStringMoney(mIliDetails.getIliUnitPrice(), precisionUnit),
									Common.convertDoubleToStringMoney(mIliDetails.getIliQuantity() *  mIliDetails.getIliUnitPrice(), precisionUnit));
            				
            				arIliList.setLocationList(actionForm.getLocationList());
            				
            				arIliList.clearUnitList();
			            	ArrayList unitList = ejbPDC.getInvUomByIiName(mIliDetails.getIliIiName(), user.getCmpCode());
			            	Iterator unitListIter = unitList.iterator();
			            	while (unitListIter.hasNext()) {
			            		
			            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
			            		arIliList.setUnitList(mUomDetails.getUomName()); 
			            		
			            	}	
            				
            				actionForm.saveArILIList(arIliList);
            				
            				
            			}	
            			
            			int remainingList = pdcLineNumber - (list.size() % pdcLineNumber) + list.size();
            			
            			for (int x = list.size() + 1; x <= remainingList; x++) {
            				
            				ArPdcLineItemList arILIList = new ArPdcLineItemList(actionForm,
            						null, String.valueOf(x), null, null, null, null, null, null, null);
            				
            				arILIList.setLocationList(actionForm.getLocationList());
            				
            				arILIList.setUnitList(Constants.GLOBAL_BLANK);
            				arILIList.setUnitList("Select Item First");
            				
            				actionForm.saveArILIList(arILIList);
            				
            			}	
            			
            			this.setFormProperties(actionForm);
            			
            			if (request.getParameter("child") == null) {
            				
            				return (mapping.findForward("arPdcEntry"));         
            				
            			} else {
            				
            				return (mapping.findForward("arPdcEntryChild"));         
            				
            			}
            			            			            			
            		} else if(mdetails.getPdcRctList() != null && !mdetails.getPdcRctList().isEmpty()) {
            			
            			System.out.println("Hello PR");
            			
            			actionForm.setType("PR");
            			
                		if (!actionForm.getBankAccountList().contains(mdetails.getRctBaName())) {
                			
                			if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
                				
                				actionForm.clearBankAccountList();
                				
                			}
                			actionForm.setBankAccountList(mdetails.getRctBaName());
                			
                		}
                		actionForm.setBankAccount(mdetails.getRctBaName());

            			
            			list = mdetails.getPdcRctList();
		            		
    		            i = list.iterator();
    		            		            
    		            while (i.hasNext()) {
    		            	
    			           ArModAppliedInvoiceDetails mAiDetails = (ArModAppliedInvoiceDetails)i.next();
    			           
    				       ArPdcPrList ipsList = new ArPdcPrList(
                     		actionForm, mAiDetails.getAiIpsCode(), 
                     		mAiDetails.getAiIpsInvNumber(),
                     		Common.convertShortToString(mAiDetails.getAiIpsNumber()),
                     		Common.convertSQLDateToString(mAiDetails.getAiIpsDueDate()),
                     		mAiDetails.getAiIpsInvReferenceNumber(),
                     		mAiDetails.getAiIpsInvFcName(),
                     		Common.convertDoubleToStringMoney(mAiDetails.getAiIpsAmountDue(), precisionUnit),
                     		Common.convertDoubleToStringMoney(mAiDetails.getAiApplyAmount(), precisionUnit),
                     		Common.convertDoubleToStringMoney(mAiDetails.getAiCreditableWTax(), precisionUnit),
                     		Common.convertDoubleToStringMoney(mAiDetails.getAiAppliedDeposit(), precisionUnit), // add apply deposit
                     		Common.convertDoubleToStringMoney(mAiDetails.getAiDiscountAmount(), precisionUnit),
                     		Common.convertDoubleToStringMoney(mAiDetails.getAiAllocatedPaymentAmount(), precisionUnit),
                     		Common.convertDoubleToStringMoney(mAiDetails.getAiDiscountAmount(), precisionUnit));
                     		
                     		ipsList.setPayCheckbox(true);
    				        
    				        actionForm.saveArRCTList(ipsList); 
    				        				        				      
    			        }
    			        
    			        this.setFormProperties(actionForm);
    			        
    			        
    			        if (actionForm.getEnableFields()) {				        			        				        
    			        			        			        
    				        try {
    	         	
    			                 list = ejbPDC.getArIpsByCstCustomerCode(actionForm.getCustomer(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    			                 		                 		                 
    			                 i = list.iterator();	                 
    			                 
    			                 while (i.hasNext()) {
    			                 	
    			                 	ArModInvoicePaymentScheduleDetails mIpsDetails = (ArModInvoicePaymentScheduleDetails)i.next();
    			                 			                 			                 			                 			                 			                 			                 	
    			                 	ArPdcPrList ipsList = new ArPdcPrList(
    			                 		actionForm, mIpsDetails.getIpsCode(), 
    			                 		mIpsDetails.getIpsInvNumber(),
    			                 		Common.convertShortToString(mIpsDetails.getIpsNumber()),
    			                 		Common.convertSQLDateToString(mIpsDetails.getIpsDueDate()),
    			                 		mIpsDetails.getIpsInvReferenceNumber(),
    			                 		mIpsDetails.getIpsInvFcName(),
    			                 		Common.convertDoubleToStringMoney(mIpsDetails.getIpsAmountDue(), precisionUnit),
    			                 		Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiApplyAmount(), precisionUnit),
    			                 		Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiCreditableWTax(), precisionUnit),
    			                 		Common.convertDoubleToStringMoney(0d, precisionUnit),
    			                 		Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiDiscountAmount(), precisionUnit),
    			                 		Common.convertDoubleToStringMoney(0d, precisionUnit),
    			                 		Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiDiscountAmount(), precisionUnit));
    			                 		
    			                     actionForm.saveArRCTList(ipsList);
    			                 	
    			                 }
    			                 
    			             } catch (GlobalNoRecordFoundException ex) {
    			             			           			             	
    			             } 
    			                    
    			         }     
    			        
    			        actionForm.setLineCount(0);
    			        actionForm.setMaxRows(5);
    			        actionForm.setDisablePreviousButton(true);
    			        actionForm.setDisableFirstButton(true);
    			        
    			        // check if next should be disabled
    			        if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {
    			        	
    			        	actionForm.setDisableNextButton(true);
    			        	actionForm.setDisableLastButton(true);
    			        	
    			        } else {
    			        	
    			        	actionForm.setDisableNextButton(false);
    			        	actionForm.setDisableLastButton(false);
    			        	
    			        }
    			        
    			         			         		
    			        return (mapping.findForward("arPdcEntry"));			        
            		
            		
            		} else {
            			
            			System.out.println("Hello Memo");
            		
            		    actionForm.setType("MEMO LINES");
            		   
	            		list = mdetails.getPdcIlList();              		
	            		         		            		            		
			            i = list.iterator();
			            
			            int lineNumber = 0;
			            
			            while (i.hasNext()) {
			            	
				           ArModInvoiceLineDetails mIlDetails = (ArModInvoiceLineDetails)i.next();
				           				       
					       ArPdcEntryList arIlList = new ArPdcEntryList(actionForm,
					          mIlDetails.getIlCode(),
					          String.valueOf(++lineNumber),
					          mIlDetails.getIlSmlName(),
					          mIlDetails.getIlDescription(),
					          Common.convertDoubleToStringMoney(mIlDetails.getIlQuantity(), precisionUnit),
					          Common.convertDoubleToStringMoney(mIlDetails.getIlUnitPrice(), precisionUnit),
					          Common.convertDoubleToStringMoney(mIlDetails.getIlAmount(), precisionUnit),
					          Common.convertByteToBoolean(mIlDetails.getIlTax()));
					          
					      arIlList.setMemoLineList(actionForm.getMemoLineList());
					      
					      actionForm.saveArPDCList(arIlList);
					         
					      
				        }	
				        			        			        
				        int remainingList = pdcLineNumber - (list.size() % pdcLineNumber) + list.size();
				        lineNumber = 0;
				        
				        for (int x = list.size() + 1; x <= remainingList; x++) {
				        	
				        	ArPdcEntryList arPDCList = new ArPdcEntryList(actionForm, 
				        	    null, String.valueOf(++lineNumber), null, null, null, null, null, false);
				        	
				        	arPDCList.setMemoLineList(actionForm.getMemoLineList());
				        	
				        	actionForm.saveArPDCList(arPDCList);
				        	
				         }	
				         
				         this.setFormProperties(actionForm);
				         
				         if (request.getParameter("child") == null) {
				         
				         	return (mapping.findForward("arPdcEntry"));         
				         	
				         } else {
				         	
				         	return (mapping.findForward("arPdcEntryChild"));         
				         	
				         }
			         }
            	}     
            	
            	// Populate line when not forwarding
            	
            	if (user.getUserApps().contains("OMEGA INVENTORY") && (actionForm.getType() == null || actionForm.getType().equals("ITEMS"))) {
            	
	            	for (int x = 1; x <= pdcLineNumber; x++) {
	            		
	            		ArPdcLineItemList arILIList = new ArPdcLineItemList(actionForm, 
	            				null, new Integer(x).toString(), null, null, null, null, null, 
								null, null);	
	            		
	            		arILIList.setLocationList(actionForm.getLocationList());
	            		
	            		arILIList.setUnitList(Constants.GLOBAL_BLANK);
	            		arILIList.setUnitList("Select Item First");
	            		
	            		actionForm.saveArILIList(arILIList);
	            		
	            	}
	            	
            	} else if(user.getUserApps().contains("OMEGA INVENTORY") && (actionForm.getType() == null || actionForm.getType().equals("PR"))) {
            	
    		        actionForm.setLineCount(0);
    		        actionForm.setMaxRows(5);
    		        actionForm.setDisablePreviousButton(true);
    		        actionForm.setDisableFirstButton(true);
    		        
    		        // check if next should be disabled
    		        if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {
    		        	
    		        	actionForm.setDisableNextButton(true);
    		        	actionForm.setDisableLastButton(true);
    		        	
    		        } else {
    		        	
    		        	actionForm.setDisableNextButton(false);
    		        	actionForm.setDisableLastButton(false);
    		        	
    		        }

            	} else {
            		
            		for (int x = 1; x <= pdcLineNumber; x++) {
    		        	
    		        	ArPdcEntryList arPDCList = new ArPdcEntryList(actionForm, 
    		        	    null, new Integer(x).toString(), null, null, null, null, null, false);
    		        	
    		        	arPDCList.setMemoLineList(actionForm.getMemoLineList());
    		        	
    		        	actionForm.saveArPDCList(arPDCList);
    		        	
    		         }
            		
            	}
	  	                            	           			
            		           			   	
            } catch(GlobalNoRecordFoundException ex) {
            	            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("pdcEntry.error.pdcAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArPdcEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveAsDraftButton") != null || request.getParameter("saveSubmitButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }

               
            }
                        
            actionForm.reset(mapping, request);   
            
            if (actionForm.getType() == null) {
      		      	
	           if (user.getUserApps().contains("OMEGA INVENTORY")) {
               
                   actionForm.setType("ITEMS");
                   
               } else {
               
                   actionForm.setType("MEMO LINES");
               
               }
	          
            }           

            actionForm.setPdcCode(null);          
            actionForm.setDateReceived(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setStatus("OPEN");
            actionForm.setConversionRate("1.000000");
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("arPdcEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ArPdcEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


   private void setFormProperties(ArPdcEntryForm actionForm) {
   	
   	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
   		
   		if (actionForm.getStatus().equals("CLEARED")) {
   			
   			actionForm.setEnableFields(false);
   			actionForm.setShowSaveButton(false);
   			actionForm.setShowDeleteButton(false);
   			actionForm.setShowAddLinesButton(false);
   			actionForm.setShowDeleteLinesButton(false);					
   			actionForm.setEnableCancelled(false);
   			
   		} else if (actionForm.getStatus().equals("CANCELLED")) {
   			
   			if (actionForm.getPosted().equals("NO")) {
   				
   				actionForm.setEnableFields(false);
   				actionForm.setShowSaveButton(false);
   				actionForm.setShowDeleteButton(true);
   				actionForm.setShowAddLinesButton(false);
   				actionForm.setShowDeleteLinesButton(false);
   				actionForm.setEnableCancelled(false);
   				
   			} else {
   				
   				actionForm.setEnableFields(false);
   				actionForm.setShowSaveButton(false);
   				actionForm.setShowDeleteButton(false);
   				actionForm.setShowAddLinesButton(false);
   				actionForm.setShowDeleteLinesButton(false);
   				actionForm.setEnableCancelled(false);
   				
   			}
   			
   		} else {
   			
   			if (actionForm.getPosted().equals("NO")) {
   			
   				if (actionForm.getPdcCode() == null) {
   					
   					actionForm.setEnableFields(true);
   					actionForm.setShowSaveButton(true);
   					actionForm.setShowDeleteButton(false);
   					actionForm.setShowAddLinesButton(true);
   					actionForm.setShowDeleteLinesButton(true);					
   					actionForm.setEnableCancelled(false);
   					
   				} else if (actionForm.getPdcCode() != null &&
   						Common.validateRequired(actionForm.getApprovalStatus())) {
   					
   					actionForm.setEnableFields(true);
   					actionForm.setShowSaveButton(true);
   					actionForm.setShowDeleteButton(true);
   					actionForm.setShowAddLinesButton(true);
   					actionForm.setShowDeleteLinesButton(true);
   					actionForm.setEnableCancelled(true);
   					
   				} else {
   					
   					actionForm.setEnableFields(false);
   					actionForm.setShowSaveButton(true);
   					actionForm.setShowDeleteButton(true);
   					actionForm.setShowAddLinesButton(false);
   					actionForm.setShowDeleteLinesButton(false);
   					actionForm.setEnableCancelled(true);
   					
   				}
   			
   			} else {
   				
   				actionForm.setEnableFields(false);
   				actionForm.setShowSaveButton(true);
   				actionForm.setShowDeleteButton(true);
   				actionForm.setShowAddLinesButton(false);
   				actionForm.setShowDeleteLinesButton(false);
   				actionForm.setEnableCancelled(true);
   				
   			}
   		}
   		
   	} else {
   		
   		actionForm.setEnableFields(false);
   		actionForm.setShowSaveButton(false);
   		actionForm.setShowDeleteButton(false);
   		actionForm.setShowAddLinesButton(false);
   		actionForm.setShowDeleteLinesButton(false);
   		actionForm.setEnableCancelled(false);
   		
   	}
   	
   }
		
}