package com.struts.ar.pdcentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ar.receiptentry.ArReceiptEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;


public class ArPdcEntryForm extends ActionForm implements Serializable {

   private Integer pdcCode = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String status = null;
   private String shift = null;
   private ArrayList shiftList = new ArrayList();      
   private String customer = null;
   private ArrayList customerList = new ArrayList();
   private String checkNumber = null;
   private String referenceNumber = null;      
   private String dateReceived = null;
   private String maturityDate = null;   
   private String paymentTerm = null;
   private ArrayList paymentTermList = new ArrayList();
   private String amount = null;
   private String description = null;
   private boolean cancelled = false;
   private String freight = null;
   private ArrayList freightList = new ArrayList();   
   private String taxCode = null;
   private ArrayList taxCodeList = new ArrayList();
   private String withholdingTax = null;
   private ArrayList withholdingTaxList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String conversionDate = null;
   private String conversionRate = null;
   private String approvalStatus = null;
   private String posted = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;   
   private String postedBy = null;
   private String datePosted = null;
   private ArrayList memoLineList = new ArrayList();
   private String location = null;
   private ArrayList locationList = new ArrayList();
   private String functionalCurrency = null;
   private ArrayList arPDCList = new ArrayList();
   private ArrayList arILIList = new ArrayList();
   private int rowSelected = 0;
   private int rowILISelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean enableCancelled = false;
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showSaveButton = false;
   private boolean showDeleteButton = false;
   private boolean showShift = false;  
   private boolean useCustomerPulldown = true;
   private String isCustomerEntered  = null;
   private String isTypeEntered = null;
   private String isConversionDateEntered = null;
   private String effectivityDate = null;
   
   private String report = null;

   //FOR PROVISIONARY RECEIPT
   
   private String customerName = null;
   private ArrayList arRCTList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();   
   private String paymentMethod = null;
   private ArrayList paymentMethodList = new ArrayList();
   private String customerDeposit = null;
   private boolean enableReceiptVoid = false;
   
   private Integer receiptCode = null;
   private String date = null;
   private String receiptNumber = null;
   private boolean receiptVoid = false;
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   private int lineCount = 0;
   private int maxRows = 0;

   
   
   
   public String getReport() {
   	
   	   return report;
   	
   }
   
   public void setReport(String report) {
   	
   	   this.report = report;
   	
   }
  
   public int getRowSelected(){
      return rowSelected;
   }

   public ArPdcEntryList getArPDCByIndex(int index){
      return((ArPdcEntryList)arPDCList.get(index));
   }

   public Object[] getArPDCList(){
      return(arPDCList.toArray());
   }

   public int getArPDCListSize(){
      return(arPDCList.size());
   }

   public void saveArPDCList(Object newArPDCList){
      arPDCList.add(newArPDCList);
   }

   public void clearArPDCList(){
      arPDCList.clear();
   }

   public void setRowSelected(Object selectedArPDCList, boolean isEdit){
      this.rowSelected = arPDCList.indexOf(selectedArPDCList);
   }

   public void updateArPDCRow(int rowSelected, Object newArPDCList){
      arPDCList.set(rowSelected, newArPDCList);
   }

   public void deleteArPDCList(int rowSelected){
      arPDCList.remove(rowSelected);
   }
   
   public int getRowILISelected(){
      return rowILISelected;
   }

   public ArPdcLineItemList getArILIByIndex(int index){
      return((ArPdcLineItemList)arILIList.get(index));
   }

   public Object[] getArILIList(){
      return(arILIList.toArray());
   }

   public int getArILIListSize(){
      return(arILIList.size());
   }

   public void saveArILIList(Object newArILIList){
      arILIList.add(newArILIList);
   }

   public void clearArILIList(){
      arILIList.clear();
   }

   public void setRowILISelected(Object selectedArILIList, boolean isEdit){
      this.rowILISelected = arILIList.indexOf(selectedArILIList);
   }

   public void updateArILIRow(int rowILISelected, Object newArILIList){
      arILIList.set(rowILISelected, newArILIList);
   }

   public void deleteArILIList(int rowILISelected){
      arILIList.remove(rowILISelected);
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getPdcCode() {
   	
   	  return pdcCode;
   	
   }
   
   public void setPdcCode(Integer pdcCode) {
   	
   	  this.pdcCode = pdcCode;
   	
   }
   
   public String getType() {
   	
   	   return type;
   	
   }
   
   public void setType(String type) {
   	
   	
   	   this.type = type;
   	
   }
   
   public ArrayList getTypeList() {
   	
   	   return typeList;
   	
   }
   
   public String getStatus() {
   	
   	   return status;
   	
   }
   
   public void setStatus(String status) {
   	
       this.status = status;	
   
   }
   
   public String getShift() {
   	
   	  return shift;
   	
   }
   
   public void setShift(String shift) {
   	
   	  this.shift = shift;
   	
   }
   
   public ArrayList getShiftList() {
   	
   	  return shiftList;
   	
   }
   
   public void setShiftList(String shift) {
   	
   	  shiftList.add(shift);
   	
   }
   
   public void clearShiftList() {
   	
   	  shiftList.clear();
   	  shiftList.add(Constants.GLOBAL_BLANK);
   	
   }   
   
   public String getCustomer() {
   	
   	  return customer;
   	
   }
   
   public void setCustomer(String customer) {
   	
   	  this.customer = customer;
   	
   }
   
   public ArrayList getCustomerList() {
   	
   	  return customerList;
   	
   }
   
   public void setCustomerList(String customer) {
   	
   	  customerList.add(customer);
   	
   }
   
   public void clearCustomerList() {
   	
   	  customerList.clear();
   	  customerList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getCheckNumber() {

      return checkNumber;
   	
   }
   
   public void setCheckNumber(String checkNumber) {
   	
   	  this.checkNumber = checkNumber;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	   this.referenceNumber = referenceNumber;
   	
   }   
   
   public String getDateReceived() {
   	
   	  return dateReceived;
   	
   }
   
   public void setDateReceived(String dateReceived) {
   	
   	  this.dateReceived = dateReceived;   	
   	
   }
   
   public String getMaturityDate() {
   	  
   	  return maturityDate;
   	   	
   }
   
   public void setMaturityDate(String maturityDate) {
   	
   	  this.maturityDate = maturityDate;
   	
   }

	public String getPaymentTerm() {
   	
   	  return paymentTerm;
   	
   }
   
   public void setPaymentTerm(String paymentTerm) {
   	
   	  this.paymentTerm = paymentTerm;
   	
   }
   
   public ArrayList getPaymentTermList() {
   	
   	  return paymentTermList;
   	
   }
   
   public void setPaymentTermList(String paymentTerm) {
   	
   	  paymentTermList.add(paymentTerm);
   	
   }
   
   public void clearPaymentTermList() {
   	
   	  paymentTermList.clear();
   	
   }   
         
   public String getAmount() {
   	
   	  return amount;
   	
   }
   
   public void setAmount(String amount) {
   	
   	  this.amount = amount;
   	
   } 

   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public boolean getCancelled() {
   	
   	  return cancelled;
   	
   }
   
   public void setCancelled(boolean cancelled) {
   	
   	  this.cancelled = cancelled;
   	
   }
 
   public String getFreight() {
   	
   	  return freight;
   	
   }
   
   public void setFreight(String freight) {
   	
   	  this.freight = freight;
   	
   } 
   
   public ArrayList getFreightList() {
   	
   	  return freightList;
   	
   }
   
   public void setFreightList(String freight) {
   	
   	  freightList.add(freight);
   	
   }
   
   public void clearFreightList() {
   	
   	  freightList.clear();
   	  freightList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getTaxCode() {
   	
   	   return taxCode;
   	
   }
   
   public void setTaxCode(String taxCode) {
   	
   	   this.taxCode = taxCode;
   	
   }
   
   public ArrayList getTaxCodeList() {
   	
   	   return taxCodeList;
   	
   }
   
   public void setTaxCodeList(String taxCode) {
   	
   	   taxCodeList.add(taxCode);
   	
   }
   
   public void clearTaxCodeList() {
   	
   	   taxCodeList.clear();
   	   taxCodeList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   
   public String getWithholdingTax() {
   	
   	   return withholdingTax;
   	
   }
   
   public void setWithholdingTax(String withholdingTax) {
   	
   	   this.withholdingTax = withholdingTax;
   	
   }
   
   public ArrayList getWithholdingTaxList() {
   	
   	   return withholdingTaxList;
   	
   }
   
   public void setWithholdingTaxList(String withholdingTax) {
   	
   	   withholdingTaxList.add(withholdingTax);
   	
   }
   
   public void clearWithholdingTaxList() {
   	
   	   withholdingTaxList.clear();
   	   withholdingTaxList.add(Constants.GLOBAL_BLANK);
   	
   }
      
   public String getCurrency() {
   	
   	   return currency;
   	
   }
   
   public void setCurrency(String currency) {
   	
   	   this.currency = currency;
   	
   }
   
   public ArrayList getCurrencyList() {
   	
   	   return currencyList;
   	
   }
   
   public void setCurrencyList(String currency) {
   	
   	   currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	   currencyList.clear();
   	
   }
   
   public String getConversionDate() {
   	
   	   return conversionDate;
   	
   }
   
   public void setConversionDate(String conversionDate) {
   	
   	   this.conversionDate = conversionDate;
   	
   }
   
   public String getConversionRate() {
   	 
   	   return conversionRate;
   	
   }
   
   public void setConversionRate(String conversionRate) {
   	
   	   this.conversionRate = conversionRate;
   	
   }   
      
   public String getApprovalStatus() {
   	
   	   return approvalStatus;
   	
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	   this.approvalStatus = approvalStatus;
   	
   }
   
   public String getPosted() {
   	
   	   return posted;
   	
   }
   
   public void setPosted(String posted) {
   	
   	   this.posted = posted;
   	
   }

   public String getCreatedBy() {
   	
   	   return createdBy;
   	
   }
   
   public void setCreatedBy(String createdBy) {
   	
   	  this.createdBy = createdBy;
   	
   }
   
   public String getDateCreated() {
   	
   	   return dateCreated;
   	
   }
   
   public void setDateCreated(String dateCreated) {
   	
   	   this.dateCreated = dateCreated;
   	
   }   
   
   public String getLastModifiedBy() {
   	
   	  return lastModifiedBy;
   	
   }
   
   public void setLastModifiedBy(String lastModifiedBy) {
   	
   	  this.lastModifiedBy = lastModifiedBy;
   	
   }
   
   public String getDateLastModified() {
   	
   	  return dateLastModified;
   	
   }
   
   public void setDateLastModified(String dateLastModified) {
   	
   	  this.dateLastModified = dateLastModified;
   	
   }
   
   public String getPostedBy() {
   	
   	  return postedBy;
   	
   }
   
   public void setPostedBy(String postedBy) {
   	
   	  this.postedBy = postedBy;
   	
   }
   
   public String getDatePosted() {
   	
   	  return datePosted;
   	
   }
   
   public void setDatePosted(String datePosted) {
   	
   	  this.datePosted = datePosted;
   	
   }
   
   public String getFunctionalCurrency() {
   	
   	   return functionalCurrency;
   	
   }
   
   public void setFunctionalCurrency(String functionalCurrency) {
   	
   	   this.functionalCurrency = functionalCurrency;
   	
   }
     
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
      
   public boolean getShowShift() {
   	
   	   return showShift;
   	
   }
   
   public void setShowShift(boolean showShift) {
   	
   	   this.showShift = showShift;
   	
   }
   
   public ArrayList getMemoLineList() {
   	
   	   return memoLineList;
   	
   }
   
   public void setMemoLineList(String memoLine) {
   	
   	   memoLineList.add(memoLine);
   	
   }
   
   public void clearMemoLineList() {
   	
   	   memoLineList.clear();
   	   memoLineList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public boolean getEnableCancelled() {
   	
   	   return enableCancelled;
   	
   }
   
   public void setEnableCancelled(boolean enableCancelled) {
   	
   	   this.enableCancelled = enableCancelled;
   	
   }
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
   
   public boolean getShowDeleteButton() {
   	
   	   return showDeleteButton;
   	
   }
   
   public void setShowDeleteButton(boolean showDeleteButton) {
   	
   	   this.showDeleteButton = showDeleteButton;
   	
   }
            
   public String getIsCustomerEntered() {
   	
   	   return isCustomerEntered;
   	
   }
   
   public String getIsTypeEntered() {
   	
   	  return isTypeEntered;
   	
   }
   

   
   public String getLocation() {
   
	   return location;
		
   }
	
   public void setLocation(String location) {
   
	   this.location = location;
		
   }	
	
   public ArrayList getLocationList() {
   
	   return locationList;
		
   }
	
   public void setLocationList(String location) {
	
	   locationList.add(location);
		
   }
	
   public void clearLocationList() {
		
	   locationList.clear();
	   locationList.add(Constants.GLOBAL_BLANK);
		
   }
     
   public boolean getUseCustomerPulldown() {
   	
   	   return useCustomerPulldown;
   	   
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	   this.useCustomerPulldown = useCustomerPulldown;
   	   
   }
  	
   public String getIsConversionDateEntered(){
   	
   	return isConversionDateEntered;
   	
   }
   
   public String getEffectivityDate() {

	   return effectivityDate;

   }

   public void setEffectivityDate(String effectivityDate) {

	   this.effectivityDate = effectivityDate;

   }
   
   public ArPdcPrList getArRCTByIndex(int index){
	      return((ArPdcPrList)arRCTList.get(index));
   }

   public Object[] getArRCTList(){
      return(arRCTList.toArray());
   }

   public int getArRCTListSize(){
      return(arRCTList.size());
   }

   public void saveArRCTList(Object newArRCTList){
      arRCTList.add(newArRCTList);
   }

   public void clearArRCTList(){
      arRCTList.clear();
   }

   public void setRowRCTSelected(Object selectedArRCTList, boolean isEdit){
      this.rowSelected = arRCTList.indexOf(selectedArRCTList);
   }

   public void updateArRCTRow(int rowSelected, Object newArRCTList){
      arRCTList.set(rowSelected, newArRCTList);
   }

   public void deleteArRCTList(int rowSelected){
      arRCTList.remove(rowSelected);
   }

   
   public String getCustomerName() {
	   	
   	return customerName;
   	
   }
   
   public void setCustomerName(String customerName) {
   	
   	this.customerName = customerName;
   	
   }

   //FOR PROVISIONARY RECEIPT
   
   public Integer getReceiptCode() {
	   	
   	  return receiptCode;
   	
   }
   
   public void setReceiptCode(Integer receiptCode) {
   	
   	  this.receiptCode = receiptCode;
   	
   }
   
   public String getDate() {
	   	  
   	  return date;
   	   	
   }
   
   public void setDate(String date) {
   	
   	  this.date = date;
   	
   }

   public String getReceiptNumber() {
	   	
   	  return receiptNumber;
   	
   }
   
   public void setReceiptNumber(String receiptNumber) {
   	
   	  this.receiptNumber = receiptNumber;
   	
   }

   public boolean getReceiptVoid() {
	   	
   	  return receiptVoid;
   	
   }
   
   public void setReceiptVoid(boolean receiptVoid) {
   	
   	  this.receiptVoid = receiptVoid;
   	
   }

   public String getBankAccount() {
	   	
	   	  return bankAccount;
	   	
	}
	
	public void setBankAccount(String bankAccount) {
		
		  this.bankAccount = bankAccount;
		
	}
	
	public ArrayList getBankAccountList() {
		
		  return bankAccountList;
		
	}
	
	public void setBankAccountList(String bankAccount) {
		
		  bankAccountList.add(bankAccount);
		
	}
	
	public void clearBankAccountList() {
		
		  bankAccountList.clear();
		  bankAccountList.add(Constants.GLOBAL_BLANK);
		
	}

	public String getPaymentMethod() {
	   	
		return paymentMethod;
	   	
	}

	public void setPaymentMethod(String paymentMethod) {
	
		this.paymentMethod = paymentMethod;
	
	}

	public ArrayList getPaymentMethodList() {
	
	  return paymentMethodList;
	
	}

   public String getCustomerDeposit() {
	   	
   	   return customerDeposit;
   	
   }
   
   public void setCustomerDeposit(String customerDeposit) {
   	
   	   this.customerDeposit = customerDeposit;
   	
   }
   
   public boolean getEnableReceiptVoid() {
	   	
   	   return enableReceiptVoid;
   	
   }
   
   public void setEnableReceiptVoid(boolean enableReceiptVoid) {
   	
   	   this.enableReceiptVoid = enableReceiptVoid;
   	
   }
   
   public int getLineCount(){
	   	
	   	return lineCount;
	   	
   }
   
   public void setLineCount(int lineCount){  	
   	
   	this.lineCount = lineCount;
   	
   }
   
   public int getMaxRows(){
   	
   	return maxRows;
   	
   }
   
   public void setMaxRows(int maxRows){  	
   	
   	this.maxRows = maxRows;
   	
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }

   public boolean getDisableFirstButton() {

	   return disableFirstButton;

   }

   public void setDisableFirstButton(boolean disableFirstButton) {

	   this.disableFirstButton = disableFirstButton;

   }

   public boolean getDisableLastButton() {

	   return disableLastButton;

   }

   public void setDisableLastButton(boolean disableLastButton) {

	   this.disableLastButton = disableLastButton;

   }

   
   
  	
   public void reset(ActionMapping mapping, HttpServletRequest request){      
   	
   	   typeList.clear();
	   typeList.add("ITEMS");
	   typeList.add("MEMO LINES");
	   typeList.add("PR");
	   status = null;
	   shift = Constants.GLOBAL_BLANK;
       customer = Constants.GLOBAL_BLANK;
       checkNumber = null;
	   referenceNumber = null;
	   dateReceived = null;
	   maturityDate = null;
	   amount = null;
	   description = null;
	   cancelled = false;
       freight = Constants.GLOBAL_BLANK;       
       taxCode = Constants.GLOBAL_BLANK;
       withholdingTax = Constants.GLOBAL_BLANK;
	   conversionDate = null;
	   conversionRate = null;
	   approvalStatus = null;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   postedBy = null;
	   datePosted = null;
	   isCustomerEntered = null;
	   isTypeEntered = null;
	   isConversionDateEntered = null;
	   location = Constants.GLOBAL_BLANK;
	   effectivityDate = null;

	   //FOR PROVISIONAL RECEIPT
	   receiptNumber = null;
	   date = null;
	   receiptVoid = false;
       bankAccount = Constants.GLOBAL_BLANK;
       paymentMethodList.clear();
       paymentMethodList.add(Constants.GLOBAL_BLANK);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CASH);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CHECK);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CREDIT_CARD);
       paymentMethod = Constants.GLOBAL_BLANK;
       customerDeposit = null;

	   for (int i=0; i<arILIList.size(); i++) {
	  	
	  	  ArPdcLineItemList actionList = (ArPdcLineItemList)arILIList.get(i);
	  	  actionList.setIsItemEntered(null);
	  	  actionList.setIsUnitEntered(null);
	  	
	  }
	   
	   for (int i=0; i<arPDCList.size(); i++) {
	  	
	  	  ArPdcEntryList actionList = (ArPdcEntryList)arPDCList.get(i);
	  	  actionList.setIsMemoLineEntered(null);
	  	
	  }
	   int ctr = 0;
   
	   Iterator i = arRCTList.iterator();
   	   
	   while (i.hasNext()) {	   	
		   ArPdcPrList list = (ArPdcPrList)i.next();	   	   
	   	   if (ctr >= lineCount && ctr <= lineCount + maxRows - 1) {		   	   
	   	   		list.setPayCheckbox(false);		   	   	
		   }		   	   
		   ctr++;		   
	   }
	   
	   // reset tax
	   
	   if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null) {
	   	
		   i = arPDCList.iterator();
		   
		   while (i.hasNext()) {
		   	
		   	   ArPdcEntryList list = (ArPdcEntryList)i.next();
		   	   
		   	   list.setTaxable(false);
		   	
		   }
	         
	   }
	   
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null ||
    	 request.getParameter("printButton") != null) {
      	          
         if((Common.validateRequired(shift) || shift.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
          	    showShift && type.equals("ITEMS")){
                errors.add("shift",
                   new ActionMessage("pdcEntry.error.shiftRequired"));
         }
      	
      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("pdcEntry.error.customerRequired"));
         }
      	
      	 if(Common.validateRequired(dateReceived)){
            errors.add("dateReceived",
               new ActionMessage("pdcEntry.error.dateReceivedRequired"));
         }
      	 
      	 if(Common.validateRequired(checkNumber)){
            errors.add("checkNumber",
               new ActionMessage("pdcEntry.error.checkNumberRequired"));
         }
         
		 if(!Common.validateDateFormat(dateReceived)){
	            errors.add("dateReceived", 
		       new ActionMessage("pdcEntry.error.dateReceivedInvalid"));
		 }
		 
      	 if(Common.validateRequired(maturityDate)){
            errors.add("maturityDate",
               new ActionMessage("pdcEntry.error.maturityDateRequired"));
         }
         
		 if(!Common.validateDateFormat(maturityDate)){
	            errors.add("maturityDate", 
		       new ActionMessage("pdcEntry.error.maturityDateInvalid"));
		 }
		 
      	 if(Common.validateRequired(referenceNumber)){
             errors.add("referenceNumber",
                new ActionMessage("pdcEntry.error.referenceNumberRequired"));
          }
		         
         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("pdcEntry.error.currencyRequired"));
         }
         
	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("pdcEntry.error.conversionDateInvalid"));
         }
         
	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("pdcEntry.error.conversionRateInvalid"));
         }
         
         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("pdcEntry.error.conversionDateMustBeNull"));
	 	 }
	 	 
		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){ 
		    errors.add("conversionRate",
		       new ActionMessage("pdcEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("pdcEntry.error.conversionRateOrDateMustBeEntered"));
		 }
		 
		 if(!Common.validateDateLessThanCurrent(dateReceived)){
		 	errors.add("dateReceived",
				       new ActionMessage("pdcEntry.error.dateReceivedMustNotBeLaterThanCurrentDate"));
		 }    
		
		 if(!Common.validateRequired(dateReceived) && !Common.validateRequired(maturityDate)){
		 	
		 	if(!Common.convertStringToSQLDate(maturityDate).after(Common.convertStringToSQLDate(dateReceived))){
			 	errors.add("maturityDate",
					       new ActionMessage("pdcEntry.error.maturityDateMustBeLaterThanDateReceived"));
			 }
		 	
		 }
		 
		 if(type.equals("ITEMS") || type.equals("MEMO LINES"))
		 {
	         if(Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
	             errors.add("paymentTerm",
	                new ActionMessage("pdcEntry.error.paymentTermRequired"));
	          }
	          
	       	 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
	             errors.add("taxCode",
	                new ActionMessage("pdcEntry.error.taxCodeRequired"));
	          }
	          
	          if(Common.validateRequired(withholdingTax) || withholdingTax.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
	             errors.add("withholdingTax",
	                new ActionMessage("pdcEntry.error.withholdingTaxRequired"));
	          }
			 
			 if(Common.validateRequired(effectivityDate)){
				 errors.add("effectivityDate",
						 new ActionMessage("pdcEntry.error.effectivityDateRequired"));
			 }
	
			 if(!Common.validateDateFormat(effectivityDate)){
				 errors.add("effectivityDate", 
						 new ActionMessage("pdcEntry.error.effectivityDateInvalid"));
			 }
	
			 if(!Common.validateDateFromTo(maturityDate, effectivityDate)){
				 errors.add("maturityDate", new ActionMessage("pdcEntry.error.effectivityDateLessThanMaturityDate"));
			 }
		 }
		 
		 if(type.equals("PR"))
		 {
			 System.out.println("Hello  1111");
             if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            	 
            	 System.out.println("Hello!!!!!");
                 errors.add("bankAccount",
                    new ActionMessage("pdcEntry.error.bankAccountRequired"));
              }
		 }
		
		 if (type.equals("MEMO LINES")) {
		 	
		 	int numberOfLines = 0;
		 	
		 	Iterator i = arPDCList.iterator();      	 
		 	
		 	while (i.hasNext()) {
		 		
		 		ArPdcEntryList ilList = (ArPdcEntryList)i.next();      	 	 
		 		
		 		if (Common.validateRequired(ilList.getMemoLine()) &&
		 				Common.validateRequired(ilList.getDescription()) &&
						Common.validateRequired(ilList.getQuantity()) &&
						Common.validateRequired(ilList.getUnitPrice()) &&
						Common.validateRequired(ilList.getAmount())) continue;
		 		
		 		numberOfLines++;
		 		
		 		if(Common.validateRequired(ilList.getMemoLine()) || ilList.getMemoLine().equals(Constants.GLOBAL_BLANK)){
		 			errors.add("memoLine",
		 					new ActionMessage("pdcEntry.error.memoLineRequired", ilList.getLineNumber()));
		 		}
		 		if(Common.validateRequired(ilList.getQuantity())){
		 			errors.add("quantity",
		 					new ActionMessage("pdcEntry.error.quantityRequired", ilList.getLineNumber()));
		 		}
		 		if(!Common.validateNumberFormat(ilList.getQuantity())){
		 			errors.add("quantity",
		 					new ActionMessage("pdcEntry.error.quantityInvalid", ilList.getLineNumber()));
		 		}
		 		if(Common.validateRequired(ilList.getUnitPrice())){
		 			errors.add("unitPrice",
		 					new ActionMessage("pdcEntry.error.unitPriceRequired", ilList.getLineNumber()));
		 		}
		 		if(!Common.validateMoneyFormat(ilList.getUnitPrice())){
		 			errors.add("unitPrice",
		 					new ActionMessage("pdcEntry.error.unitPriceInvalid", ilList.getLineNumber()));
		 		}
		 		if(Common.validateRequired(ilList.getAmount())){
		 			errors.add("amount",
		 					new ActionMessage("pdcEntry.error.amountRequired", ilList.getLineNumber()));
		 		}
		 		if(!Common.validateMoneyFormat(ilList.getAmount())){
		 			errors.add("amount",
		 					new ActionMessage("pdcEntry.error.amountInvalid", ilList.getLineNumber()));
		 		}
		 		
		 		
		 	}
		 	
		 	if (numberOfLines == 0) {
		 		
		 		errors.add("customer",
		 				new ActionMessage("pdcEntry.error.pdcMustHaveLine"));
		 		
		 	}
		 	
		 } else if (type.equals("PR")) {
			 	
	         int numberOfLines = 0;
	      	 
	      	 Iterator i = arRCTList.iterator();      	 
	      	 
	      	 while (i.hasNext()) {
	      	 	
	      	 	 ArPdcPrList ipsList = (ArPdcPrList)i.next();      	 	 
	      	 	       	 	 
	      	 	 if (!ipsList.getPayCheckbox()) continue;
	      	 	     
	      	 	 numberOfLines++;
	      	 
	    	     if(Common.validateRequired(ipsList.getApplyAmount())){
		            errors.add("applyAmount",
		               new ActionMessage("pdcEntry.error.applyAmountRequired", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
		         }
		         if(!Common.validateMoneyFormat(ipsList.getApplyAmount())){
		            errors.add("applyAmount",
		               new ActionMessage("pdcEntry.error.applyAmountInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
		         }
			 	 if(!Common.validateMoneyFormat(ipsList.getDiscount())){
		            errors.add("discount",
		               new ActionMessage("pdcEntry.error.discountInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
		         }
		         if(!Common.validateMoneyFormat(ipsList.getCreditableWTax())){
		            errors.add("creditableWTax",
		               new ActionMessage("pdcEntry.error.creditableWTaxInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
		         }
		         if(!Common.validateMoneyFormat(ipsList.getAllocatedReceiptAmount())){
		            errors.add("allocatedReceiptAmount",
		               new ActionMessage("pdcEntry.error.allocatedReceiptAmountInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
		         }	
		         if((!currency.equals(functionalCurrency) || !ipsList.getCurrency().equals(functionalCurrency)) &&
		            (Common.validateRequired(ipsList.getAllocatedReceiptAmount()) || Common.convertStringMoneyToDouble(ipsList.getAllocatedReceiptAmount(), (short)6) == 0d)) {
		         	errors.add("currency",
		               new ActionMessage("pdcEntry.error.allocatedReceiptAmountRequired", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
		         }	  
		         if((currency.equals(functionalCurrency) && ipsList.getCurrency().equals(functionalCurrency)) &&
		            (!Common.validateRequired(ipsList.getAllocatedReceiptAmount()) && Common.convertStringMoneyToDouble(ipsList.getAllocatedReceiptAmount(), (short)6) != 0d)) {
		         	errors.add("currency",
		               new ActionMessage("pdcEntry.error.allocatedReceiptAmountMustNotBeEntered", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
		         }
		         
		         /* if ipsList.due date < date error
		         if(Common.convertStringToSQLDate(ipsList.getDueDate()).before(Common.convertStringToSQLDate(date))) {
		         	errors.add("date",
		 	               new ActionMessage("receiptEntry.error.receiptDateMustBeLessThanDueDate", ipsList.getDueDate()));
		         	
		         }*/
			          
		    }
		    
		    if (numberOfLines == 0) {
	         	
	         	errors.add("customer",
	               new ActionMessage("pdcEntry.error.receiptMustHaveLine"));
	         	
	         }	  	    

			 	
			 } else {
		 	
		 	int numOfLines = 0;
		 	
		 	Iterator i = arILIList.iterator();      	 
		 	
		 	while (i.hasNext()) {
		 		
		 		ArPdcLineItemList iliList = (ArPdcLineItemList)i.next();      	 	 
		 		
		 		if (Common.validateRequired(iliList.getLocation()) &&
		 			Common.validateRequired(iliList.getItemName()) &&
					Common.validateRequired(iliList.getQuantity()) &&
					Common.validateRequired(iliList.getUnit()) &&
					Common.validateRequired(iliList.getUnitPrice())) continue;
		 		
		 		numOfLines++;
		 		
		 		if(Common.validateRequired(iliList.getLocation()) || iliList.getLocation().equals(Constants.GLOBAL_BLANK)){
		 			errors.add("location",
		 					new ActionMessage("pdcEntry.error.locationRequired", iliList.getLineNumber()));
		 		}
		 		if(Common.validateRequired(iliList.getItemName()) || iliList.getItemName().equals(Constants.GLOBAL_BLANK)){
		 			errors.add("itemName",
		 					new ActionMessage("pdcEntry.error.itemNameRequired", iliList.getLineNumber()));
		 		}
		 		if(Common.validateRequired(iliList.getQuantity())) {
		 			errors.add("quantity",
		 					new ActionMessage("pdcEntry.error.quantityRequired", iliList.getLineNumber()));
		 		}
		 		if(!Common.validateNumberFormat(iliList.getQuantity())){
		 			errors.add("quantity",
		 					new ActionMessage("pdcEntry.error.quantityInvalid", iliList.getLineNumber()));
		 		}
		 		if(!Common.validateRequired(iliList.getQuantity()) && Common.convertStringMoneyToDouble(iliList.getQuantity(), (short)3) <= 0) {
		         	errors.add("quantity",
		         		new ActionMessage("pdcEntry.error.negativeOrZeroQuantityNotAllowed", iliList.getLineNumber()));
		         }
		 		if(Common.validateRequired(iliList.getUnit()) || iliList.getUnit().equals(Constants.GLOBAL_BLANK)){
		 			errors.add("unit",
		 					new ActionMessage("pdcEntry.error.unitRequired", iliList.getLineNumber()));
		 		}
		 		if(Common.validateRequired(iliList.getUnitPrice())){
		 			errors.add("unitPrice",
		 					new ActionMessage("pdcEntry.error.unitPriceRequired", iliList.getLineNumber()));
		 		}
		 		if(!Common.validateMoneyFormat(iliList.getUnitPrice())){
		 			errors.add("unitCost",
		 					new ActionMessage("pdcEntry.error.unitCostInvalid", iliList.getLineNumber()));
		 		}	        
		 		
		 	}
		 	
		 	if (numOfLines == 0) {
		 		
		 		errors.add("customer",
		 				new ActionMessage("pdcEntry.error.pdcMustHaveLine"));
		 		
		 	}
		 	
		 }	  	    
    	 
      } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {
      	
      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("pdcEntry.error.customerRequired"));
         }
      	 
      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){
      	
      	if(!Common.validateDateFormat(conversionDate)){
      		
      		errors.add("conversionDate", new ActionMessage("pdcEntry.error.conversionDateInvalid"));
      		
      	}
      	
      } 
      
      return(errors);	
   }
}
