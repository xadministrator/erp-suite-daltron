package com.struts.ar.pdcentry;

import java.io.Serializable;
import java.util.ArrayList;

public class ArPdcEntryList implements Serializable {

   private Integer pdcLineCode = null;
   private String lineNumber = null;
   private String memoLine = null;
   private ArrayList memoLineList = new ArrayList();
   private String description = null;
   private String quantity = null;
   private String unitPrice = null;
   private String amount = null;
   private boolean taxable = false;
   
   private boolean deleteCheckbox = false;
   
   private String isMemoLineEntered = null;
    
   private ArPdcEntryForm parentBean;
    
   public ArPdcEntryList(ArPdcEntryForm parentBean,
      Integer pdcLineCode,
      String lineNumber,
      String memoLine,
      String description,
      String quantity,
      String unitPrice,
      String amount,
      boolean taxable){

      this.parentBean = parentBean;
      this.lineNumber = lineNumber;
      this.pdcLineCode = pdcLineCode;
      this.memoLine = memoLine;
      this.description = description;
      this.quantity = quantity;
      this.unitPrice = unitPrice;
      this.amount = amount;
      this.taxable = taxable;
   }
         
   public Integer getPdcLineCode() {
   	
      return pdcLineCode;
      
   }
   
   public String getLineNumber() {
   	
   	  return lineNumber;
   	
   }
   
   public void setLineNumber(String lineNumber) {
   	
   	  this.lineNumber = lineNumber;
   	
   }

   public String getMemoLine() {
   	
      return memoLine;
      
   }
   
   public void setMemoLine(String memoLine) {
   	  
   	  this.memoLine = memoLine;
   	
   }
   
   public ArrayList getMemoLineList() {
   	
   	  return memoLineList;
   	
   }
   
   public void setMemoLineList(ArrayList memoLineList) {
   	
   	  this.memoLineList = memoLineList;
   	
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public String getQuantity() {
   	
   	   return quantity;
   	
   }
   
   public void setQuantity(String quantity) {
   	
   	   this.quantity = quantity;
   	
   }
   
   public String getUnitPrice() {
   	
   	   return unitPrice;
   	
   }
   
   public void setUnitPrice(String unitPrice) {
   	
   	   this.unitPrice = unitPrice;
   	
   }
   
   public String getAmount() {
   	
   	   return amount;
   	
   }
   
   public void setAmount(String amount) {
   	
   	   this.amount = amount;
   	
   }
   
   public boolean getTaxable() {
   	
   	   return taxable;
   	
   }
   
   public void setTaxable(boolean taxable) {
   	
   	   this.taxable = taxable;
   	
   }

   
   public boolean getDeleteCheckbox() {   	
   
   	  return deleteCheckbox;   	
   	  
   }
   
   public void setDeleteCheckbox(boolean deleteCheckbox) {
   	
   	  this.deleteCheckbox = deleteCheckbox;
   	  
   }
   
   public String getIsMemoLineEntered() {
   	
   	   return isMemoLineEntered;
   	
   }
   
   public void setIsMemoLineEntered(String isMemoLineEntered) {
   	   
   	   if (isMemoLineEntered != null && isMemoLineEntered.equals("true")) {
   	   	
   	   	    parentBean.setRowSelected(this, false);
   	   	
   	   }   	   
   	
   }

}
