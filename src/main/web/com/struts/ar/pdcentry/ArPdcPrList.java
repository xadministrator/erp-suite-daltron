package com.struts.ar.pdcentry;

import java.io.Serializable;

public class ArPdcPrList implements Serializable {

   private Integer invoicePaymentScheduleCode = null;
   private String invoiceNumber = null;
   private String installmentNumber = null;
   private String dueDate = null;
   private String referenceNumber = null;
   private String currency = null;
   private String amountDue = null;
   private String applyAmount = null;
   private String creditableWTax = null;
   private String appliedDeposit = null;
   private String allocatedReceiptAmount = null;
   private String discount = null;
   private String tempDiscount = null;
   
   private boolean payCheckbox = false;
    
   private ArPdcEntryForm parentBean;
    
   public ArPdcPrList(ArPdcEntryForm parentBean,
      Integer invoicePaymentScheduleCode,
      String invoiceNumber,
      String installmentNumber,
      String dueDate,
      String referenceNumber,
      String currency,
      String amountDue,
      String applyAmount,
      String creditableWTax,
	  String appliedDeposit,
      String discount,
      String allocatedReceiptAmount,
      String tempDiscount){

      this.parentBean = parentBean;
      this.invoicePaymentScheduleCode = invoicePaymentScheduleCode;
      this.invoiceNumber = invoiceNumber;
      this.installmentNumber = installmentNumber;
      this.dueDate = dueDate;
      this.referenceNumber = referenceNumber;
      this.currency = currency;
      this.amountDue = amountDue;      
      this.applyAmount = applyAmount;
      this.creditableWTax = creditableWTax;
      this.appliedDeposit  = appliedDeposit;
      this.discount = discount;
      this.allocatedReceiptAmount = allocatedReceiptAmount; 
      this.tempDiscount = tempDiscount;
      
   }
         
   public Integer getInvoicePaymentScheduleCode() {
   	
      return invoicePaymentScheduleCode;
      
   }

   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	
   }
   
   public String getInstallmentNumber() {
   	
   	  return installmentNumber;
   	
   }
   
   public String getDueDate() {
   	  
   	  return dueDate;
   }
   
   
   public String getReferenceNumber() {
   	  
   	  return referenceNumber;
   	
   }
   
   public String getCurrency() {
   	 
   	  return currency;
   	
   }
   
   public String getAmountDue() {
   	 
   	   return amountDue;
   	
   }
   
   public String getApplyAmount() {
   	
   	   return applyAmount;
   	
   }
   
   public void setApplyAmount(String applyAmount) {
   	
   	   this.applyAmount = applyAmount;
   	
   }
   
   public String getDiscount() {
   	
   	   return discount;
   	
   }
   
   public void setDiscount(String discount) {
   	   
   	   this.discount = discount;
   	   
   }
   
   public String getCreditableWTax() {
   	
   	   return creditableWTax;
   	
   }
   
   public void setCreditableWTax(String creditableWTax) {
   	   
   	   this.creditableWTax = creditableWTax;
   	   
   }
   public String getAppliedDeposit() {
   	
   	   return appliedDeposit;
   	
   }
   
   public void setAppliedDeposit(String appliedDeposit) {
   	   
   	   this.appliedDeposit = appliedDeposit;
   	   
   }
      
   public String getAllocatedReceiptAmount() {
   	
   	   return allocatedReceiptAmount;
   	
   }
   
   public void setAllocatedReceiptAmount(String allocatedReceiptAmount) {
   	
   	   this.allocatedReceiptAmount = allocatedReceiptAmount;
   	
   }
   
   public boolean getPayCheckbox() {   	
   	  return payCheckbox;   	
   }
   
   public void setPayCheckbox(boolean payCheckbox) {
   	  this.payCheckbox = payCheckbox;
   }

   public String getTempDiscount() {
	   
	  return tempDiscount;
	  
   }
   
   public void setTempDiscount(String tempDiscount) {
	   
	   this.tempDiscount = tempDiscount;
	   
   }
}