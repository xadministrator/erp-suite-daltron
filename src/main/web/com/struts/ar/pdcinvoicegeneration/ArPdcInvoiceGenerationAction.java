package com.struts.ar.pdcinvoicegeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.ArPdcInvoiceGenerationController;
import com.ejb.txn.ArPdcInvoiceGenerationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModPdcDetails;

public final class ArPdcInvoiceGenerationAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArPdcInvoiceGenerationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArPdcInvoiceGenerationForm actionForm = (ArPdcInvoiceGenerationForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_PDC_INVOICE_GENERATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arPdcInvoiceGeneration");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArPdcInvoiceGenerationController EJB
*******************************************************/

         ArPdcInvoiceGenerationControllerHome homePDCI = null;
         ArPdcInvoiceGenerationController ejbPDCI = null;

         try {
          
            homePDCI = (ArPdcInvoiceGenerationControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArPdcInvoiceGenerationControllerEJB", ArPdcInvoiceGenerationControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArPdcInvoiceGenerationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbPDCI = homePDCI.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArPdcInvoiceGenerationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
    Call ArPdcInvoiceGenerationController EJB
    getGlFcPrecisionUnit
    getAdPrfArUseCustomerPulldown
 *******************************************************/

         short precisionUnit = 0;
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbPDCI.getGlFcPrecisionUnit(user.getCmpCode());
            useCustomerPulldown = Common.convertByteToBoolean(ejbPDCI.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArPdcInvoiceGenerationAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	


/*******************************************************
   -- Ar PDCI Previous Action --
*******************************************************/ 

         if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ar PDCI Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 
         
/*******************************************************
   -- Ar PDCI Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null ||
            request.getParameter("forward") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getCustomer())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomer());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getMaturityDateFrom())) {
	        		
	        		criteria.put("maturityDateFrom", Common.convertStringToSQLDate(actionForm.getMaturityDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getMaturityDateTo())) {
	        		
	        		criteria.put("maturityDateTo", Common.convertStringToSQLDate(actionForm.getMaturityDateTo()));
	        		
	        	}	        		        	
	        		        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	} else if (request.getParameter("forward") != null) {
	     		
	     		HashMap criteria = new HashMap();
	     		
	     		criteria.put("maturityDateTo", Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	     		
	     		// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	     		
	     	}
            
            try {
            	
            	actionForm.clearArPDCIList();
            	
            	ArrayList list = ejbPDCI.getArPdcByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModPdcDetails mdetails = (ArModPdcDetails)i.next();
            		
            		ArPdcInvoiceGenerationList pdciList = new ArPdcInvoiceGenerationList(
            			actionForm,
            			mdetails.getPdcCode(), mdetails.getPdcCstCustomerCode(),
            			Common.convertSQLDateToString(mdetails.getPdcMaturityDate()),
            			mdetails.getPdcCheckNumber(), Common.convertDoubleToStringMoney(mdetails.getPdcAmount(), precisionUnit));
            			
            	   actionForm.saveArPDCIList(pdciList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("pdcInvoiceGeneration.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArPdcInvoiceGenerationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arPdcInvoiceGeneration");

            }
                        
            actionForm.reset(mapping, request);            	                               
            
            return(mapping.findForward("arPdcInvoiceGeneration")); 

/*******************************************************
   -- Ar PDCI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar PDCI Generate Action --
*******************************************************/

         } else if (request.getParameter("generateButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
  	        // generate invoices
  	        
		    for(int i=0; i<actionForm.getArPDCIListSize(); i++) {
		    
		       ArPdcInvoiceGenerationList actionList = actionForm.getArPDCIByIndex(i);
		    	
               if (actionList.getGenerate()) {
               	
               	     try {
               	     
               	     	ejbPDCI.executeArPdcInvoiceGeneration(actionList.getPdcInvoiceCode(), 
               	     	   actionList.getInvoiceNumber(), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());               	     	
               	     	
               	     	actionForm.deleteArPDCIList(i);
               	        i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("pdcInvoiceGeneration.error.recordAlreadyDeleted", actionList.getCheckNumber()));
  
		             } catch (GlobalDocumentNumberNotUniqueException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("pdcInvoiceGeneration.error.invoiceNumberNotUnique", actionList.getInvoiceNumber()));

		             } catch (GlobalBranchAccountNumberInvalidException ex) {
		             	
		             	errors.add(ActionMessages.GLOBAL_MESSAGE,
		             			new ActionMessage("pdcInvoiceGeneration.error.branchAccountNumberInvalid", ex.getMessage()));
		             	
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ArPdcInvoiceGenerationAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 		                           
	            
	           }
	           
	       }		      
		
/*******************************************************
   -- Ar PDCI Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arPdcInvoiceGeneration");

            }
            
            actionForm.clearArPDCIList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	if(actionForm.getUseCustomerPulldown()) {
            		
            		actionForm.clearCustomerList();           	
            		
            		list = ejbPDCI.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerList((String)i.next());
            				
            			}
            			
            		}   
            		
            	}
            	           	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArPdcInvoiceGenerationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }             
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("generateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            actionForm.reset(mapping, request);
            actionForm.setLineCount(0);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            	        
            return(mapping.findForward("arPdcInvoiceGeneration"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArPdcInvoiceGenerationAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();

          return mapping.findForward("cmnErrorPage");

       }

    }
}