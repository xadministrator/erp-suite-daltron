package com.struts.ar.pdcinvoicegeneration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArPdcInvoiceGenerationForm extends ActionForm implements Serializable {

   private String customer = null;
   private ArrayList customerList = new ArrayList(); 
   private String maturityDateFrom = null;
   private String maturityDateTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
    
   private ArrayList arPDCIList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean useCustomerPulldown = true;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {
   	
      return rowSelected;
   
   }
   
   public int getLineCount() {
   
   	  return lineCount;   	 
   
   }
   
   public void setLineCount(int lineCount) {  
   	
   	  this.lineCount = lineCount;
   
   }

   public ArPdcInvoiceGenerationList getArPDCIByIndex(int index) {
   
      return((ArPdcInvoiceGenerationList)arPDCIList.get(index));
   
   }

   public Object[] getArPDCIList() {
   	
      return(arPDCIList.toArray());
   
   }

   public int getArPDCIListSize() {
   
      return(arPDCIList.size());
   
   }

   public void saveArPDCIList(Object newArPDCIList) {
   	
      arPDCIList.add(newArPDCIList);
   
   }

   public void clearArPDCIList() {
   
      arPDCIList.clear();
   
   }

   public void setRowSelected(Object selectedArPDCIList, boolean isEdit) {
   
      this.rowSelected = arPDCIList.indexOf(selectedArPDCIList);
  
   }

   public void updateApRVGRow(int rowSelected, Object newArPDCIList) {
   	
      arPDCIList.set(rowSelected, newArPDCIList);
   
   }

   public void deleteArPDCIList(int rowSelected) {
   
      arPDCIList.remove(rowSelected);
   
   }
   
   public String getTxnStatus() {
   	
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   
   }

   public void setTxnStatus(String txnStatus) {
   
      this.txnStatus = txnStatus;
   
   }

   public String getUserPermission() {
   	
      return(userPermission);
   
   }

   public void setUserPermission(String userPermission) {
   
      this.userPermission = userPermission;
   
   }
   
   public String getMaturityDateFrom() {
   
   	  return maturityDateFrom;	  
   
   }
   
   public void setMaturityDateFrom(String maturityDateFrom) {
   	
   	  this.maturityDateFrom = maturityDateFrom;
   
   }
   
   public String getMaturityDateTo() {
   
   	  return maturityDateTo;	  
   
   }
   
   public void setMaturityDateTo(String maturityDateTo) {
   	
   	  this.maturityDateTo = maturityDateTo;
   
   }   
      
   public String getCustomer() {
   
   	  return customer;
   
   }
   
   public void setCustomer(String customer) {
   	
   	  this.customer = customer;
   
   }
   
   public ArrayList getCustomerList() {
   
   	  return customerList;
   
   }
   
   public void setCustomerList(String customer) {
   
   	  customerList.add(customer);
   
   }
   
   public void clearCustomerList() {
   	 
   	  customerList.clear();
   	  customerList.add(Constants.GLOBAL_BLANK);
   
   }   
      
   public String getOrderBy() {
   	
   	  return orderBy;
   
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   
   }
   
   public ArrayList getOrderByList() {
   
   	  return orderByList;
   
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public boolean getUseCustomerPulldown() {
   	
   	   return useCustomerPulldown;
   	   
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	  this.useCustomerPulldown = useCustomerPulldown;
   	  
   }
      
   public void reset(ActionMapping mapping, HttpServletRequest request){
      
   	  maturityDateFrom = null;
      maturityDateTo = null;
      customer = Constants.GLOBAL_BLANK;      
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.AR_PDCI_ORDER_BY_CUSTOMER);
	      orderByList.add(Constants.AR_PDCI_ORDER_BY_MATURITY_DATE);
	      orderBy = Constants.GLOBAL_BLANK;                  
	      
	  }
	  
      previousButton = null;
      nextButton = null;	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
   	
       ActionErrors errors = new ActionErrors();
       
       if (request.getParameter("goButton") != null) {
      
	       if (!Common.validateDateFormat(maturityDateFrom)) {
	
		     errors.add("maturityDateFrom",
		        new ActionMessage("pdcInvoiceGeneration.error.maturityDateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(maturityDateTo)) {
		
		     errors.add("maturityDateTo",
		        new ActionMessage("pdcInvoiceGeneration.error.maturityDateToInvalid"));
		
		  } 
		  
	   }  
	            
      return(errors);
   }
}
