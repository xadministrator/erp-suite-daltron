package com.struts.ar.pdcinvoicegeneration;

import java.io.Serializable;

public class ArPdcInvoiceGenerationList implements Serializable {

   private Integer pdcInvoiceCode = null;
   private String customer = null;
   private String maturityDate = null;
   private String invoiceNumber = null;
   private String checkNumber = null;
   private String amount = null;
   
   private boolean generate = false;
    
   private ArPdcInvoiceGenerationForm parentBean;
    
   public ArPdcInvoiceGenerationList(ArPdcInvoiceGenerationForm parentBean,
      Integer pdcInvoiceCode,
      String customer,
      String maturityDate,
	  String checkNumber,
	  String amount){

      this.parentBean = parentBean;
      this.pdcInvoiceCode = pdcInvoiceCode;
      this.customer = customer;
      this.maturityDate = maturityDate;
      this.checkNumber = checkNumber;
      this.amount = amount;
      
   }
   
   public Integer getPdcInvoiceCode(){
      return pdcInvoiceCode;
   }

   public String getCustomer(){
      return customer;
   }
   
   public void setCustomer(String customer) {
   	  this.customer = customer;
   }

   public String getMaturityDate(){
      return(maturityDate);
   }
   
   public void setMaturityDate(String maturityDate) {
   	  this.maturityDate = maturityDate;   	
   }
   
   public String getInvoiceNumber(){
      return(invoiceNumber);
   }
   
   public void setInvoiceNumber(String invoiceNumber) {
   	  this.invoiceNumber = invoiceNumber;
   }

   public String getAmount() {
   	  return amount;
   }
   
   public void setAmount(String amount) {
   	  this.amount = amount;
   }
   
   public String getCheckNumber() {
   	  return checkNumber;
   }
   
   public void setCheckNumber(String checkNumber) {
   	  this.checkNumber = checkNumber;
   }
   
   public boolean getGenerate() {
   	  return(generate);
   }

   public void setGenerate(boolean generate){
   	  this.generate = generate;   	
   }
   
}

