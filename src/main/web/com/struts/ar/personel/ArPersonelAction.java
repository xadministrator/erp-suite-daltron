package com.struts.ar.personel;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArPersonelController;
import com.ejb.txn.ArPersonelControllerHome;
import com.struts.ar.personeltype.ArPersonelTypeList;
import com.struts.ar.salesperson.ArSalespersonList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModPersonelDetails;
import com.util.ArPersonelDetails;
import com.util.ArPersonelTypeDetails;

public class ArPersonelAction extends Action{

private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		
		try {
			
			
	/*******************************************************
	 Check if user has a session
	 *******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ArPersonelAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ArPersonelForm actionForm = (ArPersonelForm)form;
			
			String frParam = Common.getUserPermission(user,Constants.AR_PERSONEL_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("arPersonel");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
			
			/*******************************************************
			 Initialize ArPersonelController EJB
			 *******************************************************/
			
			ArPersonelControllerHome homePRSNL = null;
			ArPersonelController ejbPRSNL = null;
			
			
			try {
				
				homePRSNL = (ArPersonelControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArPersonelControllerEJB", ArPersonelControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in ArPersonelAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbPRSNL = homePRSNL.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in ArPersonelAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
	/*******************************************************
	 Call ArPersonelController EJB
	 getGlFcPrecisionUnit
	 *******************************************************/
			
			short precisionUnit = 0;
			
			try {
				
				precisionUnit = ejbPRSNL.getGlFcPrecisionUnit(user.getCmpCode());        
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in ArPersonelAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			/*******************************************************

			 save Button
			 *******************************************************/	
			
			if (request.getParameter("saveButton") != null &&
				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				
				
				ArModPersonelDetails details = new ArModPersonelDetails();
				details.setPeIdNumber(actionForm.getPersonelIdNumber());
				details.setPeName(actionForm.getPersonelName());
				details.setPeDescription(actionForm.getPersonelDescription());
				details.setPeAddress(actionForm.getPersonelAddress());
				details.setPeRate(Common.convertStringMoneyToDouble(actionForm.getPersonelRate(),precisionUnit));
				details.setPePtName(actionForm.getPersonelType());
				
				try {
					
					ejbPRSNL.addArPeEntry(details, user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("personel.error.recordAlreadyExist"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArPersonelAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				
				
			
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
/*******************************************************
 -- Ar PRL Update Action --
 *******************************************************/
				
			} else if (request.getParameter("updateButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
				
				ArPersonelList arPrsnlList = actionForm.getArPeByIndex(actionForm.getRowSelected());
				
				ArModPersonelDetails details = new ArModPersonelDetails();
				
				details.setPeCode(actionForm.getPeCode());
				details.setPeIdNumber(actionForm.getPersonelIdNumber());
				details.setPeName(actionForm.getPersonelName());
				details.setPeDescription(actionForm.getPersonelDescription());
				details.setPeAddress(actionForm.getPersonelAddress());
				details.setPeRate(Common.convertStringMoneyToDouble(actionForm.getPersonelRate(),precisionUnit));
				details.setPePtName(actionForm.getPersonelType());
				
				try {
					
					ejbPRSNL.updateArPeEntry(details, user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("personel.error.recordAlreadyExist"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArPersonelAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				
				
				
/*******************************************************
 -- Ar PRL isPersonelTypeEntered  Action --
 *******************************************************/
				
				
			}  else if (request.getParameter("isPersonelTypeEntered") != null) {
				
				System.out.println("isPersonelTypeEntered");
				try {
					double ptRate = ejbPRSNL.getPersonelTypeRateByPersonelTypeName(actionForm.getPersonelType(), user.getCmpCode());
					
					System.out.println("rate is:" + ptRate);
					
					
					
					actionForm.setPersonelRate(Common.convertDoubleToStringMoney(ptRate, precisionUnit));
					
					
					
					
					
					
				}catch(Exception ex) {
				    if (log.isInfoEnabled()) {

				    	log.info("EJBException caught in ArPersonelAction.execute(): " + ex.getMessage() +
				    			" session: " + session.getId());
	                 	return mapping.findForward("cmnErrorPage"); 
	                  
	                }
				}
				
				 return(mapping.findForward("arPersonel"));
				
/*******************************************************
 -- Ar PRL Edit Action --
 *******************************************************/
				
			} else if (request.getParameter("arPeList[" + 
					actionForm.getRowSelected() + "].editButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				System.out.println("edit button entered");
				actionForm.reset(mapping, request);
				
				Object[] arPrsnlList = actionForm.getArPeList();
				
				actionForm.showArPrsnlRow(actionForm.getRowSelected());
				
				return mapping.findForward("arPersonel");
/*******************************************************
 -- Ar PRL Delete Action --
 *******************************************************/
			} else if (request.getParameter("arPeList[" +
					actionForm.getRowSelected() + "].deleteButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				
				
				ArPersonelList arPrsnlList =
						actionForm.getArPeByIndex(actionForm.getRowSelected());
					
					try {
						
						ejbPRSNL.deleteArPeEntry(arPrsnlList.getPeCode(), user.getCmpCode());
						
					} catch (GlobalRecordAlreadyDeletedException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("personel.error.personelAlreadyDeleted"));
					
					} catch (GlobalRecordAlreadyAssignedException ex) {
						
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("personel.error.personelAlreadyAssigned"));
						
					} catch(EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in ArSalespersonAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
						}
						
						return(mapping.findForward("cmnErrorPage"));
						
					}     
				
				
			}
			
			
/*******************************************************
 -- Ar PRL Load Action --
*******************************************************/
			
			if (frParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("arPersonel");
					
				}
				
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
					
					actionForm.clearArPeList();	                               
					
					list = ejbPRSNL.getArPeAll(user.getCmpCode()); 
					
					i = list.iterator();
					
					while(i.hasNext()) {
						
						ArModPersonelDetails details = (ArModPersonelDetails)i.next();
						
						ArPersonelList arPrsnlList = new ArPersonelList(actionForm,
								details.getPeCode(),
								details.getPeIdNumber(),
								details.getPeName(),
								details.getPeDescription(),
								details.getPeAddress(),
								Common.convertDoubleToStringMoney(details.getPeRate(),precisionUnit),
								details.getPePtName());
	                        
						
						actionForm.saveArPeList(arPrsnlList);
						
					}
					
					
					actionForm.clearPersonelTypeList();
					
					list = ejbPRSNL.getArPtAll(user.getCmpCode());
					
					i = list.iterator();
					
					while(i.hasNext()) {
						
						ArPersonelTypeDetails details = (ArPersonelTypeDetails)i.next();
					
	                        
						
						actionForm.setPersonelTypeList(details.getPtName());
						
					}
					
					
					
					if (request.getParameter("forward") != null) {
	            		
	            		if (request.getParameter("forward") != null) {
	            			
	            			actionForm.setPeCode(new Integer(request.getParameter("peCode")));
	            			
	            		
	            
            			 ArModPersonelDetails details = ejbPRSNL.getArPeByPeCode(actionForm.getPeCode());
            			
        				System.out.println("peCode=" + actionForm.getPeCode());
        				
        				System.out.println("peRate=" + details.getPeRate());
        			
            			 actionForm.setPersonelName(details.getPeName());
            			 actionForm.setPersonelIdNumber(details.getPeIdNumber());
            			 actionForm.setPersonelDescription(details.getPeDescription());
            			 actionForm.setPersonelAddress(details.getPeAddress());
            			 actionForm.setPersonelRate(Common.convertDoubleToStringMoney( details.getPeRate(),precisionUnit));
            			 actionForm.setPersonelType(details.getPePtName());

            			 actionForm.setPageState(Constants.PAGE_STATE_EDIT);
	            		}
	            		return (mapping.findForward("arPersonel"));       
					}
					
					
					
					
				} catch (GlobalNoRecordFoundException ex) {
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArPersonelAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				
				actionForm.reset(mapping, request);
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if (request.getParameter("saveButton") != null || 
							request.getParameter("updateButton") != null) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
				}
				
				
				actionForm.setPageState(Constants.PAGE_STATE_SAVE);
				
				return(mapping.findForward("arPersonel"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
	
		}catch(Exception e) {
			
/*******************************************************
 System Failed: Forward to error page 
 *******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in ArPersonel.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
				e.printStackTrace();
			
			return mapping.findForward("cmnErrorPage");
		
		
		}
		
		
	}
	
	
}
