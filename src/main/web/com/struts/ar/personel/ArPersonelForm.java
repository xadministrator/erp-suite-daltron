package com.struts.ar.personel;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ar.salesperson.ArBranchSalespersonList;
import com.struts.ar.salesperson.ArSalespersonList;
import com.struts.util.Common;
import com.struts.util.Constants;

public class ArPersonelForm extends ActionForm implements Serializable {

	
	private Integer peCode = null;
	private String personelIdNumber = null;
	private String personelName = null;
	private String personelDescription = null;
	private String personelAddress = null;
	private String personelRate = null;
	
	private ArrayList arPeList = new ArrayList();
	private String pageState = new String();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private String personelType = null;
	private ArrayList personelTypeList = new ArrayList();
	
	private String isPersonelTypeEntered = null;
	
	public Integer getPeCode() {
		return peCode;
	}
	
	public void setPeCode(Integer peCode) {
		this.peCode = peCode;
	}
	
	
	public String getPersonelIdNumber() {
		return personelIdNumber;
	}
	
	public void setPersonelIdNumber(String personelIdNumber) {
		this.personelIdNumber= personelIdNumber;
	}
	
	
	public String getPersonelName() {
		return personelName;
	}
	
	public void setPersonelName(String personelName) {
		this.personelName = personelName;
	}
	
	public String getPersonelDescription() {
		return personelDescription;
	}
	
	public void setPersonelDescription(String personelDescription) {
		this.personelDescription = personelDescription;
	}
	
	public String getPersonelAddress() {
		return personelAddress;
	}
	
	public void setPersonelAddress(String personelAddress) {
		this.personelAddress = personelAddress;
	}
	
	public String getPersonelRate() {
		return personelRate;
	}
	
	public void setPersonelRate(String personelRate) {
		this.personelRate = personelRate;
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public String getPersonelType() {
		
		return personelType;
		
	}
	
	public void setPersonelType(String personelType) {
		
		this.personelType = personelType;
		
	}
	
	public ArrayList getPersonelTypeList() {

        return personelTypeList;

    }

    public void setPersonelTypeList(String personelType) {

    	personelTypeList.add(personelType);

    }

    public void clearPersonelTypeList() {

    	personelTypeList.clear();
    	personelTypeList.add(Constants.GLOBAL_BLANK);

    }
    
    public String getIsPersonelTypeEntered() {
		
		return isPersonelTypeEntered;
		
	}
	
	public void setIsPersonelTypeEntered(String isPersonelTypeEntered) {
		
		this.isPersonelTypeEntered = isPersonelTypeEntered;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public ArPersonelList getArPeByIndex(int index) {
		
		return((ArPersonelList)arPeList.get(index));
		
	}
	
	public Object[] getArPeList() {
		
		return arPeList.toArray();
		
	}
	
	public int getArPeListSize() {
		
		return arPeList.size();
		
	}
	
	public void saveArPeList(Object newArPeList) {
		
		arPeList.add(newArPeList);
		
	}
	
	public void clearArPeList() {
		
		arPeList.clear();
		
	}
	
	
	public void setRowSelected(Object selectedArPeList, boolean isEdit) {
		
		this.rowSelected = arPeList.indexOf(selectedArPeList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void showArPrsnlRow(int rowSelected) {
			
			this.peCode = ((ArPersonelList)arPeList.get(rowSelected)).getPeCode();
			this.personelIdNumber = ((ArPersonelList)arPeList.get(rowSelected)).getPersonelIdNumber();
			this.personelName = ((ArPersonelList)arPeList.get(rowSelected)).getPersonelName();
			this.personelDescription = (((ArPersonelList)arPeList.get(rowSelected)).getPersonelDescription());
			this.personelAddress = ((ArPersonelList)arPeList.get(rowSelected)).getPersonelAddress();
			this.personelType =  ((ArPersonelList)arPeList.get(rowSelected)).getPersonelType();
			
	}
	
	
	public void updateArPeRow(int rowSelected, Object newArPeList) {
		
		arPeList.set(rowSelected, newArPeList);
		
	}
	
	public void deleteArPeList(int rowSelected) {
		
		arPeList.remove(rowSelected);
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		
		personelIdNumber = null;
		personelName = null;
		personelDescription = null;
		personelAddress = null;
		personelType = null;
		isPersonelTypeEntered = null;
		personelRate = null;
	
		
	}
	
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
			
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
			
			if (Common.validateRequired(personelIdNumber)) {
				
				errors.add("personelCode",
						new ActionMessage("personel.error.personelIdNumberRequired"));
				
				
			}
			
			if (Common.validateRequired(personelName)) {
				
				errors.add("personelCode",
						new ActionMessage("personel.error.personelNameRequired"));
				
				
			}
			
			
			if (Common.validateRequired(personelType)) {
				
				errors.add("personelType",
						new ActionMessage("personel.error.personelTypeRequired"));
				
				
			}
			
			
			if (!Common.validateMoneyFormat(personelRate)) {
				
				errors.add("personelRate",
						new ActionMessage("personel.error.personelTypeRequired"));
				
				
			}
			
			
			if(Common.validateRequired(personelRate)){
				errors.add("rate",
						new ActionMessage("personel.error.personelRateRequired"));
			}
			if(!Common.validateMoneyFormat(personelRate)){
				errors.add("rate",
						new ActionMessage("personel.error.personelRateInvalid"));
			}
			
			
		
		}
		return errors;
	}
	
	
}
