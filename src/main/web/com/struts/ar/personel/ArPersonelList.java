package com.struts.ar.personel;

import java.io.Serializable;

public class ArPersonelList implements Serializable{
	
	private Integer peCode = null;
	private String personelIdNumber = null;
	private String personelName = null;
	private String personelDescription = null;
	private String personelAddress = null;
	private String personelType = null;
	private String personelRate = null;
	
	private String editButton = null;
	private String deleteButton = null;
	
	private ArPersonelForm parentBean;
	
	public ArPersonelList(ArPersonelForm parentBean,
			Integer peCode,
			String personelIdNumber,
			String personelName,
			String personelDescription,
			String personelAddress,
			String personelRate,
			String personelType) {
		
		this.parentBean = parentBean;
		this.peCode = peCode;
		this.personelIdNumber = personelIdNumber;
		this.personelName = personelName;
		this.personelDescription = personelDescription;
		this.personelAddress = personelAddress;
		this.personelRate = personelRate;
		this.personelType = personelType;
		
		
	}
	
	public void setEditButton(String editButton) {
	    parentBean.setRowSelected(this, true);
	}
	
	public void setDeleteButton(String deleteButton) {
	    parentBean.setRowSelected(this, true);
	}  
	
	
	public Integer getPeCode() {
		return peCode;
	}
	
	
	public String getPersonelIdNumber() {
		return personelIdNumber;
	}

	
	
	public String getPersonelName() {
		return personelName;
	}
	

	public String getPersonelDescription() {
		return personelDescription;
	}
	

	public String getPersonelAddress() {
		return personelAddress;
	}
	
	public String getPersonelRate() {
		return personelRate;
	}
	
	public String getPersonelType() {
		return personelType;
	}
	

}
