   package com.struts.ar.personeltype;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArPersonelTypeController;
import com.ejb.txn.ArPersonelTypeControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArCustomerTypeDetails;
import com.util.ArModCustomerTypeDetails;
import com.util.ArModPersonelTypeDetails;
import com.util.ArPersonelTypeDetails;

public final class ArPersonelTypeAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
      	
      	try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArPersonelTypeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

        } else {

            if (log.isInfoEnabled()) {

               	log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ArPersonelTypeForm actionForm = (ArPersonelTypeForm)form;
        
        String frParam = Common.getUserPermission(user, Constants.AR_PERSONEL_TYPE_ID);

        if (frParam != null) {

	    	if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         	ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               	if (!fieldErrors.isEmpty()) {

                	saveErrors(request, new ActionMessages(fieldErrors));

                  	return mapping.findForward("arPersonelType");
               }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ArPersonelTypeController EJB
*******************************************************/

        ArPersonelTypeControllerHome homePT = null;
        ArPersonelTypeController ejbPT = null;

        try {

            homePT = (ArPersonelTypeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArPersonelTypeControllerEJB", ArPersonelTypeControllerHome.class);
            
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArPersonelTypeAction.execute(): " + e.getMessage() +
               	" session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbPT = homePT.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArPersonelTypeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
         
    /*******************************************************
 Call ArPersonelController EJB
 getGlFcPrecisionUnit
 *******************************************************/
   			
   			short precisionUnit = 0;
   			
   			try {
   				
   				precisionUnit = ejbPT.getGlFcPrecisionUnit(user.getCmpCode());        
   				
   			} catch(EJBException ex) {
   				
   				if (log.isInfoEnabled()) {
   					
   					log.info("EJBException caught in ArPersonelAction.execute(): " + ex.getMessage() +
   							" session: " + session.getId());
   				}
   				
   				return(mapping.findForward("cmnErrorPage"));
   				
   			}       
         
/*******************************************************
   -- Ar  PT Save Action --
*******************************************************/

        if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArModPersonelTypeDetails details = new ArModPersonelTypeDetails();
            
            
            details.setPtShortName(actionForm.getShortName());
            details.setPtName(actionForm.getName());
            details.setPtDescription(actionForm.getDescription());
            details.setPtRate(Common.convertStringMoneyToDouble(actionForm.getRate(),precisionUnit));
                
            
            try {
            	
            	ejbPT.addArPtEntry(details,  user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               	errors.add(ActionMessages.GLOBAL_MESSAGE,
                 	new ActionMessage("customerType.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               	if (log.isInfoEnabled()) {

                  	log.info("EJBException caught in ArPersonelTypeAction.execute(): " + ex.getMessage() +
                  	" session: " + session.getId());
                  	return mapping.findForward("cmnErrorPage"); 
                  
               	}

            }

/*******************************************************
   -- Ar  PT Close Action --
*******************************************************/

        } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar  PT Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            ArPersonelTypeList arPTList =
	            actionForm.getArPTByIndex(actionForm.getRowSelected());
            
            
            ArModPersonelTypeDetails details = new ArModPersonelTypeDetails();
            details.setPtCode(arPTList.getPersonelTypeCode());
            details.setPtName(actionForm.getName());
            details.setPtShortName(actionForm.getShortName());
            details.setPtDescription(actionForm.getDescription());
            details.setPtRate(Common.convertStringMoneyToDouble(actionForm.getRate(), precisionUnit));
        
            
            try {
            	
            	ejbPT.updateArPtEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("customerType.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArPersonelTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ar  PT Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ar  PT Edit Action --
*******************************************************/

         } else if (request.getParameter("arPTList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showArPTRow(actionForm.getRowSelected());
            
            return mapping.findForward("arPersonelType");
            
/*******************************************************
   -- Ar  PT Delete Action --
*******************************************************/

         } else if (request.getParameter("arPTList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ArPersonelTypeList arPTList =
              actionForm.getArPTByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbPT.deleteArPtEntry(arPTList.getPersonelTypeCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("personelType.error.personelAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("personelType.error.personelAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArPersonelTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar  PT Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arPersonelType");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               	actionForm.clearArPTList();	        

                  
                              	           
	           	list = ejbPT.getArPtAll(user.getCmpCode()); 
	           
	           	i = list.iterator();
	            
	           	while(i.hasNext()) {
        	            			            	
	              	ArModPersonelTypeDetails mdetails = (ArModPersonelTypeDetails)i.next();
	            	
	              	ArPersonelTypeList arPTList = new ArPersonelTypeList(actionForm,
	            		mdetails.getPtCode(),
	            		mdetails.getPtShortName(),
	            	    mdetails.getPtName(),
	            	  
	            	    mdetails.getPtDescription(),
	            	    Common.convertDoubleToStringMoney(mdetails.getPtRate(),precisionUnit)
	            	  
	            	    );                            
	            	 	            	    
	              	actionForm.saveArPTList(arPTList);
	            	
	           	}
	                          
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArPersonelTypeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("arPersonelType"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ArPersonelTypeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}