package com.struts.ar.personeltype;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArPersonelTypeForm extends ActionForm implements Serializable {

	private Integer personelTypeCode = null;
	private String shortName = null;
	private String name = null;
    private String description = null;
    private String rate = null;
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList arPTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected() {
	
	    return rowSelected;
	
	}
	
	public ArPersonelTypeList getArPTByIndex(int index) {
	
	    return((ArPersonelTypeList)arPTList.get(index));
	
	}
	
	public Object[] getArPTList() {
	
	    return arPTList.toArray();
	
	}
	
	public int getArPTListSize() {
	
	    return arPTList.size();
	
	}
	
	public void saveArPTList(Object newArPTList) {
	
		arPTList.add(newArPTList);
	
	}
	
	public void clearArPTList() {
		
		arPTList.clear();
	  
	}
	
	public void setRowSelected(Object selectedArPTList, boolean isEdit) {
	
	    this.rowSelected = arPTList.indexOf(selectedArPTList);
	
	    if (isEdit) {
	
	        this.pageState = Constants.PAGE_STATE_EDIT;
	
	    }
	
	}
	
	public void showArPTRow(int rowSelected) {
		
		this.name = ((ArPersonelTypeList)arPTList.get(rowSelected)).getName();
		this.shortName = ((ArPersonelTypeList)arPTList.get(rowSelected)).getShortName();
		this.description = ((ArPersonelTypeList)arPTList.get(rowSelected)).getDescription();
		this.rate = ((ArPersonelTypeList)arPTList.get(rowSelected)).getRate();
		
	   
	}
	
	public void updateArPTRow(int rowSelected, Object newArPTList) {
	
	    arPTList.set(rowSelected, newArPTList);
	
	}
	
	public void deleteArPTList(int rowSelected) {
	
	    arPTList.remove(rowSelected);
	
	}
	
	public void setUpdateButton(String updateButton) {
	
	    this.updateButton = updateButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
	
	    this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	    this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	    this.closeButton = closeButton;
	
	}
	

	public void setPageState(String pageState) {
	
	    this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	    return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getPersonelTypeCode() {
		
		return personelTypeCode;
		
	}
	
	public void setPersonelTypeCode(Integer personelTypeCode) {
		
		this.personelTypeCode = personelTypeCode;
		
	}
	
	
	public String getShortName() {
		
	  	return shortName;
	
	}
	
	public void setShortName(String shortName) {
	
	  	this.shortName = shortName;
	
	}
	
	
	public String getName() {
	
	  	return name;
	
	}
	
	public void setName(String name) {
	
	  	this.name = name;
	
	}
	
	public String getDescription() {
	
		return description;
	
	}
	
	public void setDescription(String description) {
	
	  	this.description = description;
	
	}
	
	public String getRate() {
		
		return rate;
	
	}
	
	public void setRate(String rate) {
	
	  	this.rate = rate;
	
	}
	

	public void reset(ActionMapping mapping, HttpServletRequest request) {
	
		shortName = null;
		name = null;
		description = null;
		rate = null;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
	
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
			if (Common.validateRequired(shortName)) {
				
				errors.add("name",
			   		new ActionMessage("personelType.error.personelTypeShortNameRequired"));
			
			}
			
			
			if (Common.validateRequired(name)) {
			
				errors.add("name",
			   		new ActionMessage("personelType.error.personelTypeNameRequired"));
			
			}
			
			
			
			if(Common.validateRequired(rate)){
				errors.add("rate",
						new ActionMessage("personelType.error.personelTypeRateRequired"));
			}
			if(!Common.validateMoneyFormat(rate)){
				errors.add("rate",
						new ActionMessage("personelType.error.personelTypeRateInvalid"));
			}
			
			
		
		
			
	  	}
	     
	  	return errors;
	
	}
	
}