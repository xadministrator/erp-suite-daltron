package com.struts.ar.personeltype;

import java.io.Serializable;

public class ArPersonelTypeList implements Serializable {

	private Integer personelTypeCode = null;
	private String shortName = null;
	private String name = null;
	private String description = null;
	private String rate = null;

	private String editButton = null;
	private String deleteButton = null;
	
	private ArPersonelTypeForm parentBean;
    
	public ArPersonelTypeList(ArPersonelTypeForm parentBean,
		Integer personelTypeCode,
		String shortName,
		String name,
		String description,
		String rate) {
	
		this.parentBean = parentBean;
		this.personelTypeCode = personelTypeCode;
		this.shortName = shortName;
		this.name = name;
		this.description = description;
		this.rate = rate;

	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getPersonelTypeCode() {
		
		return personelTypeCode;
		
	}
	
	
	
	public String getShortName() {
		
		return shortName;
		
    }
    
	public void setShortName(String shortName) {
		
		this.shortName = shortName;
		
    }
    
	
	
	public String getName() {
		
		return name;
		
    }
    
	public void setName(String name) {
		
		this.name = name;
		
    }
    
    public String getDescription() {
    	
    	return description;
    	
    }
    
   public void setDescription(String description) {
    	
    	this.description = description;
    	
    }
   
   
   public String getRate() {
   	
   	return rate;
   	
   }
   
  public void setRate(String rate) {
   	
   	this.rate = rate;
   	
   }
       	
        	    	
}