package com.struts.ar.receiptbatchcopy;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalBatchCopyInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.ArReceiptBatchCopyController;
import com.ejb.txn.ArReceiptBatchCopyControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModReceiptDetails;

public final class ArReceiptBatchCopyAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArReceiptBatchCopyAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArReceiptBatchCopyForm actionForm = (ArReceiptBatchCopyForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.AR_RECEIPT_BATCH_COPY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arReceiptBatchCopy"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ArReceiptBatchCopyController EJB
*******************************************************/

         ArReceiptBatchCopyControllerHome homeRBC = null;
         ArReceiptBatchCopyController ejbRBC = null;

         try {
            
            homeRBC = (ArReceiptBatchCopyControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArReceiptBatchCopyControllerEJB", ArReceiptBatchCopyControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArReceiptBatchCopyAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbRBC = homeRBC.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ArReceiptBatchCopyAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
     Call ArReceiptBatchCopy EJB
     getGlFcPrecisionUnit
  *******************************************************/
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbRBC.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArReceiptBatchCopyAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ar RBC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arReceiptBatchCopy"));
	     
/*******************************************************
   -- Ar RBC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arReceiptBatchCopy"));  
	        
	     }
         
/*******************************************************
   -- Ar RBC Copy Action --
*******************************************************/

         if (request.getParameter("copyButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           
           ArrayList list = new ArrayList();
           
           for (int i=0; i<actionForm.getArRBCListSize(); i++) {
           
              ArReceiptBatchCopyList actionList = actionForm.getArRBCByIndex(i);
              
              if (actionList.getCopy()) list.add(actionList.getReceiptCode());
           
           }
           
           try {
           	
           	    ejbRBC.executeArRctBatchCopy(list, actionForm.getBatchTo(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	           	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchCopy.error.transactionBatchClose")); 
           
           } catch (GlobalBatchCopyInvalidException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchCopy.error.batchCopyInvalid"));  
           
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArReceiptBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }

/*******************************************************
   -- Ar RBC Batch From Entered Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isBatchFromEntered"))) {
         
            try {
            
                actionForm.clearArRBCList();
            
                ArrayList list = ejbRBC.getArRctByRbName(actionForm.getBatchFrom(), user.getCmpCode());
                                                
                Iterator i = list.iterator();
                
                while (i.hasNext()) {
                                   
                   ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
      		
            	   ArReceiptBatchCopyList actionList = new ArReceiptBatchCopyList(actionForm,
            		    mdetails.getRctCode(),
            		    mdetails.getRctType(),
            		    mdetails.getRctCstCustomerCode(),
            		    mdetails.getRctBaName(),
            		    Common.convertSQLDateToString(mdetails.getRctDate()),
            		    mdetails.getRctNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit));
                       
                   actionForm.saveArRBCList(actionList);
                                                     
                }
                
            } catch (GlobalNoRecordFoundException ex) {
            
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchCopy.error.noReceiptFound"));                    
            
            } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArInvoiceBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arReceiptBatchCopy"));
               
           }
           
           return(mapping.findForward("arReceiptBatchCopy"));
           
/*******************************************************
   -- Ar RBC Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Ar RBC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arReceiptBatchCopy"));
               
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearArRBCList();
            	actionForm.clearBatchFromList();
            	actionForm.clearBatchToList();
            	
            	list = ejbRBC.getArOpenRbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchFromList(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setBatchToList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			String batchName = (String)i.next();
            			
            		    actionForm.setBatchFromList(batchName);
            		    actionForm.setBatchToList(batchName);
            			
            		}
            		            		
            	}
            	            	
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArReceiptBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("copyButton") != null) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               
               }
            }

            actionForm.reset(mapping, request);
            
            if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            } 
            
            return(mapping.findForward("arReceiptBatchCopy"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ArReceiptBatchCopyAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }
}