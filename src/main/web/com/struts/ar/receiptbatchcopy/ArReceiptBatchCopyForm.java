package com.struts.ar.receiptbatchcopy;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArReceiptBatchCopyForm extends ActionForm implements Serializable {

   private Integer receiptBatchCode = null;
   private String batchFrom = null;
   private ArrayList batchFromList = new ArrayList();
   private String batchTo = null;
   private ArrayList batchToList = new ArrayList();
   
   private String tableType = null;
   private ArrayList arRBCList = new ArrayList();
   private int rowSelected = 0;
      
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String isBatchFromEntered = null;
   
   public int getRowSelected(){
    return rowSelected;
   }
   
   public ArReceiptBatchCopyList getArRBCByIndex(int index){
      return((ArReceiptBatchCopyList)arRBCList.get(index));
   }

   public Object[] getArRBCList(){
      return(arRBCList.toArray());
   }

   public int getArRBCListSize(){
      return(arRBCList.size());
   }

   public void saveArRBCList(Object newArRBCList){
      arRBCList.add(newArRBCList);
   }

   public void clearArRBCList(){
      arRBCList.clear();
   }

   public void setRowSelected(Object selectedArRBCList, boolean isEdit){
      this.rowSelected = arRBCList.indexOf(selectedArRBCList);
   }

   public void updateArRBCRow(int rowSelected, Object newArRBCList){
      arRBCList.set(rowSelected, newArRBCList);
   }

   public void deleteArRBCList(int rowSelected){
      arRBCList.remove(rowSelected);
   }
      
   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getReceiptBatchCode() {
   	
   	  return receiptBatchCode;
   	
   }
   
   public void setReceiptBatchCode(Integer receiptBatchCode) {
   	
   	  this.receiptBatchCode = receiptBatchCode;
   	
   }
   
   public String getBatchFrom() {
   	
   	  return batchFrom;
   	
   }
   
   public void setBatchFrom(String batchFrom) {
   	
   	  this.batchFrom = batchFrom;
   	
   }
   
   public ArrayList getBatchFromList() {
   	
   	  return batchFromList;
   	
   }
   
   public void setBatchFromList(String batchFrom) {
   	
   	  batchFromList.add(batchFrom);
   	
   }
   
   public void clearBatchFromList() {   	 
   	
   	  batchFromList.clear();
   	  batchFromList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getBatchTo() {
   	
   	  return batchTo;
   	
   }
   
   public void setBatchTo(String batchTo) {
   	
   	  this.batchTo = batchTo;
   	
   }
   
   public ArrayList getBatchToList() {
   	
   	  return batchToList;
   	
   }
   
   public void setBatchToList(String batchTo) {
   	
   	  batchToList.add(batchTo);
   	
   }
   
   public void clearBatchToList() {   	 
   	
   	  batchToList.clear();
   	  batchToList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public String getIsBatchFromEntered() {
    	
      return(isBatchFromEntered);
    	
   }
    
   public void setIsBatchFromEntered(String isBatchFromEntered) {
    	
      this.isBatchFromEntered = isBatchFromEntered;
    	
   }  
            	   	            
   public void reset(ActionMapping mapping, HttpServletRequest request){ 
   	
   	 batchFrom = Constants.GLOBAL_BLANK;
   	 batchTo = Constants.GLOBAL_BLANK;
   	 isBatchFromEntered = null;
	   	         
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
      ActionErrors errors = new ActionErrors();
            
      if(request.getParameter("copyButton") != null){
      	
      	 if(Common.validateRequired(batchFrom) || batchFrom.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("batchFrom",
               new ActionMessage("receiptBatchCopy.error.batchFromRequired"));
         } 
      	 
      	 if(Common.validateRequired(batchTo) || batchTo.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("batchTo",
               new ActionMessage("receiptBatchCopy.error.batchToRequired"));
         }
      	 
      	 if(!Common.validateRequired(batchFrom) && !Common.validateRequired(batchTo) && batchFrom.equals(batchTo)){
            errors.add("batchFrom",
               new ActionMessage("receiptBatchCopy.error.batchFromAndBatchToIsInvalid"));
         }
      }
      
      return(errors);	
   }
}
