package com.struts.ar.receiptbatchcopy;

import java.io.Serializable;

public class ArReceiptBatchCopyList implements Serializable {

   private Integer receiptCode = null;
   private String receiptType = null;
   private String customerCode = null;
   private String bankAccount = null;
   private String date = null;
   private String receiptNumber = null;
   private String amount = null;

   private boolean copy = false;
       
   private ArReceiptBatchCopyForm parentBean;
    
   public ArReceiptBatchCopyList(ArReceiptBatchCopyForm parentBean,
	  Integer receiptCode,
	  String receiptType,  
	  String customerCode,  
	  String bankAccount,  
	  String date,  
	  String receiptNumber, 
	  String amount) {

      this.parentBean = parentBean;
      this.receiptCode = receiptCode;
      this.receiptType = receiptType;
      this.customerCode = customerCode;
      this.bankAccount = bankAccount;
      this.date = date;
      this.receiptNumber = receiptNumber;
      this.amount = amount;
      
   }

   public Integer getReceiptCode() {
   	
   	  return receiptCode;
   	  
   }
   
   public String getReceiptType() {
   	
   	  return receiptType;
   	  
   }

   public String getCustomerCode() {

      return customerCode;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getReceiptNumber() {
   	
   	  return receiptNumber;
   	 
   }

   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public boolean getCopy() {
   	
   	  return copy;
   	
   }
   
   public void setCopy(boolean copy) {
   	
   	  this.copy = copy;
   	
   }
      
}