package com.struts.ar.receiptbatchentry;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.ArReceiptBatchEntryController;
import com.ejb.txn.ArReceiptBatchEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArReceiptBatchDetails;

public final class ArReceiptBatchEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("ArReceiptBatchEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         ArReceiptBatchEntryForm actionForm = (ArReceiptBatchEntryForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.AR_RECEIPT_BATCH_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("arReceiptBatchEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize ArReceiptBatchEntryController EJB
*******************************************************/

         ArReceiptBatchEntryControllerHome homeRB = null;
         ArReceiptBatchEntryController ejbRB = null;

         try {
            
            homeRB = (ArReceiptBatchEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArReceiptBatchEntryControllerEJB", ArReceiptBatchEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in ArReceiptBatchEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbRB = homeRB.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in ArReceiptBatchEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- AR RB Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArReceiptBatchDetails details = new ArReceiptBatchDetails();
           
           details.setRbCode(actionForm.getReceiptBatchCode());
           details.setRbName(actionForm.getBatchName());
           details.setRbDescription(actionForm.getDescription());
           details.setRbStatus(actionForm.getStatus());
           details.setRbType(actionForm.getType());
           
           if (actionForm.getReceiptBatchCode() == null) {
           	
           	   details.setRbCreatedBy(user.getUserName());
	           details.setRbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbRB.saveArRbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.transactionBatchClose"));   
                    
           } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.saveRecordAlreadyAssigned"));           	                      	
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArReceiptBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }           

/*******************************************************
   -- AR RB Receipt Action --
*******************************************************/

         } else if (request.getParameter("receiptButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           ArReceiptBatchDetails details = new ArReceiptBatchDetails();
           
           details.setRbCode(actionForm.getReceiptBatchCode());
           details.setRbName(actionForm.getBatchName());
           details.setRbDescription(actionForm.getDescription());
           details.setRbStatus(actionForm.getStatus());
           details.setRbType(actionForm.getType());
           
           if (actionForm.getReceiptBatchCode() == null) {
           	
           	   details.setRbCreatedBy(user.getUserName());
	           details.setRbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbRB.saveArRbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.transactionBatchClose"));   
                    
           } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.saveRecordAlreadyAssigned"));           	                      	
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArReceiptBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }      
           
           if (errors.isEmpty()) {
      	   
 	     	   String path = null;
 	     	
 	     	   if (actionForm.getType().equals("COLLECTION")) {
 	     	   	
 	         	 path = "/arReceiptEntry.do?forwardBatch=1" +
 				         "&batchName=" + actionForm.getBatchName();	
 	     	                      	   	
 	     	   } else {
 	     	
 	     	   	 path = "/arMiscReceiptEntry.do?forwardBatch=1" +
 			         	  "&batchName=" + actionForm.getBatchName();                   	   	 
 	     	   	
 	     	   }
 	     	                      	   
 	     	   return(new ActionForward(path));
 	     	 
 	        }
           
           
/*******************************************************
   -- AR RB Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbRB.deleteArRbEntry(actionForm.getReceiptBatchCode(), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.recordAlreadyDeleted"));
                    
            } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.deleteRecordAlreadyAssigned"));
                    
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArReceiptBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- AR RB Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- AR RB Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("arReceiptBatchEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {            	
            	
            	if (request.getParameter("forward") != null) {
            		            			            	    
            		actionForm.setReceiptBatchCode(new Integer(request.getParameter("receiptBatchCode")));            		            		
            		
            		ArReceiptBatchDetails details = ejbRB.getArRbByRbCode(actionForm.getReceiptBatchCode(), user.getCmpCode());
            		
            		actionForm.setBatchName(details.getRbName());
            		actionForm.setDescription(details.getRbDescription());
            		actionForm.setCreatedBy(details.getRbCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(details.getRbDateCreated()));
            		actionForm.setStatus(details.getRbStatus());
            		actionForm.setType(details.getRbType());            		
            					         
			        this.setFormProperties(actionForm);
			         
			        return (mapping.findForward("arReceiptBatchEntry"));   			         
			         
            		
            	}            	
	                    		           			   	
            } catch(GlobalNoRecordFoundException ex) {            	            	
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("receiptBatchEntry.error.invoiceBatchAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArReceiptBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("saveButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setReceiptBatchCode(null);          
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setStatus("OPEN");
            actionForm.setType("COLLECTION");
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("arReceiptBatchEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in ArReceiptBatchEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(ArReceiptBatchEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
							
			if (actionForm.getReceiptBatchCode() == null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(false);
				
			} else {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(true);
				
			}									
						
		} else {
						
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			
			
		}
				    
	}
	
}