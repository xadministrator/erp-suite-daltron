package com.struts.ar.receiptbatchprint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.ArReceiptBatchPrintController;
import com.ejb.txn.ArReceiptBatchPrintControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModReceiptDetails;

public final class ArReceiptBatchPrintAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Receipt if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArReceiptBatchPrintAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArReceiptBatchPrintForm actionForm = (ArReceiptBatchPrintForm)form;
         
         actionForm.setReceiptReport(null);
         actionForm.setReceiptEditListReport(null);
         actionForm.setReceiptCodeList(null);
         
         String frParam = Common.getUserPermission(user, Constants.AR_RECEIPT_BATCH_PRINT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arReceiptBatchPrint");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize ArReceiptBatchPrintController EJB
*******************************************************/

         ArReceiptBatchPrintControllerHome homeRBP = null;
         ArReceiptBatchPrintController ejbRBP = null;

         try {

            homeRBP = (ArReceiptBatchPrintControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArReceiptBatchPrintControllerEJB", ArReceiptBatchPrintControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArReceiptBatchPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbRBP = homeRBP.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArReceiptBatchPrintAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
 /*******************************************************
     Call ArReceiptBatchPrintController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableArReceiptBatch
     getAdPrfArUseCustomerPulldown
  *******************************************************/     
         
         short precisionUnit = 0;
         boolean enableReceiptBatch = false;
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbRBP.getGlFcPrecisionUnit(user.getCmpCode());
            enableReceiptBatch = Common.convertByteToBoolean(ejbRBP.getAdPrfEnableArReceiptBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableReceiptBatch);
            useCustomerPulldown = Common.convertByteToBoolean(ejbRBP.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArReceiptBatchPrintAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ar RBP Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arReceiptBatchPrint"));
	     
/*******************************************************
   -- Ar RBP Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arReceiptBatchPrint"));                         

/*******************************************************
   -- Ar RBP Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ar RBP Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 

/*******************************************************
   -- Ar RBP Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("refresh") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReceiptType())) {
	        		
	        		criteria.put("receiptType", actionForm.getReceiptType());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}	        	

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReceiptNumberFrom())) {
	        		
	        		criteria.put("receiptNumberFrom", actionForm.getReceiptNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptNumberTo())) {
	        		
	        		criteria.put("receiptNumberTo", actionForm.getReceiptNumberTo());
	        		
	        	}
	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	
	        	if (actionForm.getReceiptVoid()) {	        	
		        	   		        		
	        		criteria.put("receiptVoid", new Byte((byte)1));
                
                }
                
                if (!actionForm.getReceiptVoid()) {
                	
                	criteria.put("receiptVoid", new Byte((byte)0));
                	
                }
	        
	        	if (!Common.validateRequired(actionForm.getMiscType())) {
	        		
	        		criteria.put("miscType", actionForm.getMiscType());
	        		
	        	}
                
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbRBP.getArRctByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ArReceiptBatchPrintAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }

	        	
	     	}
            
            try {
            	
            	actionForm.clearArRBPList();
            	
            	ArrayList list = ejbRBP.getArRctByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
      		
            		ArReceiptBatchPrintList arRBPList = new ArReceiptBatchPrintList(actionForm,
            		    mdetails.getRctCode(),
            		    mdetails.getRctType(),
            		    mdetails.getRctCstCustomerCode(),
            		    mdetails.getRctBaName(),
            		    Common.convertSQLDateToString(mdetails.getRctDate()),
            		    mdetails.getRctNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit));
            		    
            		actionForm.saveArRBPList(arRBPList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("receiptBatchPrint.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArReceiptBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arReceiptBatchPrint");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("arReceiptBatchPrint"));

/*******************************************************
   -- Ar RBP Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar RBP Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
  	         // get selected receipts
  	         
  	        ArrayList receiptCodeList = new ArrayList();
  	        
  	        int j = 0;
                        
		    for(int i=0; i<actionForm.getArRBPListSize(); i++) {
		    
		       ArReceiptBatchPrintList actionList = actionForm.getArRBPByIndex(i);
		    	
               if (actionList.getPrint()) {

               		receiptCodeList.add(actionList.getReceiptCode());
               
               }
	           
	       }	
	       
	       if (receiptCodeList.size() > 0) {

	       	   actionForm.setReceiptReport(Constants.STATUS_SUCCESS);	      
	       	   actionForm.setReceiptCodeList(receiptCodeList);
	       	   	       	   	       	
	       }
	       
	       try {
        	
	        	actionForm.clearArRBPList();
	        	
	        	ArrayList list = ejbRBP.getArRctByCriteria(actionForm.getCriteria(),
	        	    actionForm.getOrderBy(),
	        	    new Integer(actionForm.getLineCount()), 
	        	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), 
	        	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        	
	        	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
	        	Iterator i = list.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
	  		
	        		ArReceiptBatchPrintList arRBPList = new ArReceiptBatchPrintList(actionForm,
	        		    mdetails.getRctCode(),
	        		    mdetails.getRctType(),
	        		    mdetails.getRctCstCustomerCode(),
	        		    mdetails.getRctBaName(),
	        		    Common.convertSQLDateToString(mdetails.getRctDate()),
	        		    mdetails.getRctNumber(),
	        		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit));
	        		    
	        		actionForm.saveArRBPList(arRBPList);
	        		
	        	}
	
	        } catch (GlobalNoRecordFoundException ex) {
	           
	           // disable prev next buttons
		       actionForm.setDisableNextButton(true);
	           actionForm.setDisablePreviousButton(true);
	
	        } catch (EJBException ex) {
	
	           if (log.isInfoEnabled()) {
	
	              log.info("EJBException caught in ArReceiptBatchPrintAction.execute(): " + ex.getMessage() +
	              " session: " + session.getId());
	              return mapping.findForward("cmnErrorPage"); 
	              
	           }
	
	        }
	        
	        actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
	        return(mapping.findForward("arReceiptBatchPrint"));
	        
/*******************************************************
    -- Ar RBP Edit List Action --
 *******************************************************/
	
	      } else if (request.getParameter("editListButton") != null  &&
	        actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
	         	
	         // get selected receipts
	         
	        ArrayList receiptCodeList = new ArrayList();
	        
	        int j = 0;
	                     
		    for(int i=0; i<actionForm.getArRBPListSize(); i++) {
		    
		       ArReceiptBatchPrintList actionList = actionForm.getArRBPByIndex(i);
		    	
	            if (actionList.getPrint()) {
	
	            	receiptCodeList.add(actionList.getReceiptCode());
	            	
	            }
	           
	       }	
	       
	       if (receiptCodeList.size() > 0) {
	
	       	   actionForm.setReceiptEditListReport(Constants.STATUS_SUCCESS);	      
	       	   actionForm.setReceiptCodeList(receiptCodeList);
	       	   	       	   	       	
	       }
	       
	       try {
	     	
	        	actionForm.clearArRBPList();
	        	
	        	ArrayList list = ejbRBP.getArRctByCriteria(actionForm.getCriteria(),
	        	    actionForm.getOrderBy(),
	        	    new Integer(actionForm.getLineCount()), 
	        	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), 
	        	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        	
	        	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
	        	Iterator i = list.iterator();
	        	
	        	while (i.hasNext()) {
	        		
	        		ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
	  		
	        		ArReceiptBatchPrintList arRBPList = new ArReceiptBatchPrintList(actionForm,
	        		    mdetails.getRctCode(),
	        		    mdetails.getRctType(),
	        		    mdetails.getRctCstCustomerCode(),
	        		    mdetails.getRctBaName(),
	        		    Common.convertSQLDateToString(mdetails.getRctDate()),
	        		    mdetails.getRctNumber(),
	        		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit));
	        		    
	        		actionForm.saveArRBPList(arRBPList);
	        		
	        	}
	
	        } catch (GlobalNoRecordFoundException ex) {
	           
	           // disable prev next buttons
		       actionForm.setDisableNextButton(true);
	           actionForm.setDisablePreviousButton(true);
	
	        } catch (EJBException ex) {
	
	           if (log.isInfoEnabled()) {
	
	              log.info("EJBException caught in ArReceiptBatchPrintAction.execute(): " + ex.getMessage() +
	              " session: " + session.getId());
	              return mapping.findForward("cmnErrorPage"); 
	              
	           }
	
	        }
	        
	        actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
	        return(mapping.findForward("arReceiptBatchPrint"));	        


/*******************************************************
   -- Ar RBP Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearArRBPList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseCustomerPulldown()) {
            		
            		actionForm.clearCustomerCodeList();
            		
            		list = ejbRBP.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerCodeList((String)i.next());
            				
            			}
            			
            	}    
            	
            	}

            	actionForm.clearBankAccountList();
            	
            	list = ejbRBP.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();
            	
            	list = ejbRBP.getArOpenRbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArReceiptBatchPrintAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);

	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
                        
            return(mapping.findForward("arReceiptBatchPrint"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArReceiptBatchPrintAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}