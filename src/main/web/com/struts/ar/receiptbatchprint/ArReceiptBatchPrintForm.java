package com.struts.ar.receiptbatchprint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArReceiptBatchPrintForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList(); 
   private String receiptType = null;
   private ArrayList receiptTypeList = new ArrayList();
   private boolean receiptVoid = false;
   private String customerCode = null;
   private ArrayList customerCodeList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String receiptNumberFrom = null;
   private String receiptNumberTo = null;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private String pageState = new String();
   private ArrayList arRBPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean useCustomerPulldown = true;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();
   
   private String receiptReport = null; 
   private String receiptEditListReport = null;  
   private ArrayList receiptCodeList = null;
   
   private String miscType = null;
   private ArrayList miscTypeList = new ArrayList();

   public String getReceiptReport() {
   	
   	   return receiptReport;
   	
   }
   
   public void setReceiptReport(String receiptReport) {
   	
   	   this.receiptReport = receiptReport;
   	
   }
   
   public String getReceiptEditListReport() {
   	
   	   return receiptEditListReport;
   	
   }
   
   public void setReceiptEditListReport(String receiptEditListReport) {
   	
   	   this.receiptEditListReport = receiptEditListReport;
   	
   }
   
   public ArrayList getReceiptCodeList() {   	  	
   	  return(receiptCodeList);
   }
   
   public void setReceiptCodeList(ArrayList receiptCodeList) {
   	  this.receiptCodeList = receiptCodeList;
   }

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public ArReceiptBatchPrintList getArRBPByIndex(int index) {

      return((ArReceiptBatchPrintList)arRBPList.get(index));

   }

   public Object[] getArRBPList() {

      return arRBPList.toArray();

   }

   public int getArRBPListSize() {

      return arRBPList.size();

   }

   public void saveArRBPList(Object newArRBPList) {

      arRBPList.add(newArRBPList);

   }

   public void clearArRBPList() {
   	
      arRBPList.clear();
      
   }

   public void setRowSelected(Object selectedArRBPList, boolean isEdit) {

      this.rowSelected = arRBPList.indexOf(selectedArRBPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getReceiptType() {
   	
   	  return receiptType;
   	  
   }
   
   public void setReceiptType(String receiptType) {
   	
   	  this.receiptType = receiptType;
   	  
   }                         

   public ArrayList getReceiptTypeList() {

      return receiptTypeList;

   }
   
   public boolean getReceiptVoid() {

      return receiptVoid;

   }

   public void setReceiptVoid(boolean receiptVoid) {

      this.receiptVoid = receiptVoid;

   }

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }
   
   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getBankAccount() {

      return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

      this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

      return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

      bankAccountList.add(bankAccount);

   }
   
   public void clearBankAccountList() {

      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }

   public String getReceiptNumberFrom() {

      return receiptNumberFrom;

   }

   public void setReceiptNumberFrom(String receiptNumberFrom) {

      this.receiptNumberFrom = receiptNumberFrom;

   }                         

   public String getReceiptNumberTo() {

      return receiptNumberTo;

   }

   public void setReceiptNumberTo(String receiptNumberTo) {

      this.receiptNumberTo = receiptNumberTo;

   }

   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }
   
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }       
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }  
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseCustomerPulldown() {
   	
   	  return useCustomerPulldown;
	  
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	  this.useCustomerPulldown = useCustomerPulldown;
   	  
   }

   public String getMiscType() {
   	
   	  return miscType;
   	  
   }
   
   public void setMiscType(String miscType) {
   	
   	  this.miscType = miscType;
   	  
   } 
   
   public ArrayList getMiscTypeList() {
   	
   	return miscTypeList;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      receiptTypeList.clear();
      receiptTypeList.add("COLLECTION");
      receiptTypeList.add("MISC");
      receiptType = "COLLECTION";          
      receiptVoid = false;
      batchName = Constants.GLOBAL_BLANK;
   	  customerCode = Constants.GLOBAL_BLANK;
   	  bankAccount = Constants.GLOBAL_BLANK;
   	  dateFrom = null;
   	  dateTo = null;
   	  receiptNumberFrom = null;
   	  receiptNumberTo = null; 	  
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      approvalStatus = "DRAFT";    
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add("YES");
      postedList.add("NO");
      posted = Constants.GLOBAL_NO;
      miscTypeList.clear();
      miscTypeList.add("ITEMS");
      miscTypeList.add("MEMO LINES");
      miscType = "ITEMS";  
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("BANK ACCOUNT");
	      orderByList.add("CUSTOMER CODE");
	      orderByList.add("RECEIPT NUMBER");
	      orderBy = Constants.GLOBAL_BLANK;   
	  
	  } 
	  
	  for (int i=0; i<arRBPList.size(); i++) {
	  	
	  	  ArReceiptBatchPrintList actionList = (ArReceiptBatchPrintList)arRBPList.get(i);
	  	  actionList.setPrint(false);
	  	
	  }    
	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("receiptBatchPrint.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("receiptBatchPrint.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("receiptBatchPrint.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("receiptBatchPrint.error.maxRowsInvalid"));
		
		   }
	 	  
         if (Common.validateRequired(receiptType)) {

            errors.add("receiptType",
               new ActionMessage("receiptBatchPrint.error.receiptTypeRequired"));

         }

         if (Common.validateRequired(receiptType) && receiptType.equals("MISC") &&
         		Common.validateRequired(miscType)) {

            errors.add("miscType",
               new ActionMessage("receiptBatchPrint.error.miscTypeRequired"));

         }

      } else if (request.getParameter("printButton") != null) {
      	
      	 String selectedBankAccount = null;
      	
      	 for (int i=0; i<arRBPList.size(); i++) {
	  	
		  	 ArReceiptBatchPrintList actionList = (ArReceiptBatchPrintList)arRBPList.get(i);
		  	 
		  	 if (actionList.getPrint()) {
		  	 
		  	 
			  	 if (selectedBankAccount != null && !selectedBankAccount.equals(actionList.getBankAccount())) {
			  	 	
			  	 	errors.add("bankAccount",
	               		new ActionMessage("receiptBatchPrint.error.bankAccountError"));
			  	 	
			  	 } else {
			  	 	 
			  	 	 selectedBankAccount = actionList.getBankAccount();
			  	 	
			  	 }
			  	 
			  }		  	 
	  	
	  	 }
      	
      	
      }
      
      return errors;

   }
}