package com.struts.ar.receiptbatchsubmit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidPostedException;
import com.ejb.txn.ArReceiptBatchSubmitController;
import com.ejb.txn.ArReceiptBatchSubmitControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModReceiptDetails;

public final class ArReceiptBatchSubmitAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Receipt if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArReceiptBatchSubmitAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArReceiptBatchSubmitForm actionForm = (ArReceiptBatchSubmitForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_RECEIPT_BATCH_SUBMIT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arReceiptBatchSubmit");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize ArReceiptBatchSubmitController EJB
*******************************************************/

         ArReceiptBatchSubmitControllerHome homeRBS = null;
         ArReceiptBatchSubmitController ejbRBS = null;

         try {

            homeRBS = (ArReceiptBatchSubmitControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArReceiptBatchSubmitControllerEJB", ArReceiptBatchSubmitControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArReceiptBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbRBS = homeRBS.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArReceiptBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
 /*******************************************************
     Call ArReceiptBatchSubmitController EJB
     getGlFcPrecisionUnit
     getAdPrfEnableArReceiptBatch
     getAdPrfArUseCustomerPulldown
  *******************************************************/         
         
         short precisionUnit = 0;
         boolean enableReceiptBatch = false; 
         boolean useCustomerPulldown = true;
         
         try { 
         	
            precisionUnit = ejbRBS.getGlFcPrecisionUnit(user.getCmpCode());
            enableReceiptBatch = Common.convertByteToBoolean(ejbRBS.getAdPrfEnableArReceiptBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableReceiptBatch);
            useCustomerPulldown = Common.convertByteToBoolean(ejbRBS.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
            actionForm.setUseCustomerPulldown(useCustomerPulldown);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in ArReceiptBatchSubmitAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Ar RBS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arReceiptBatchSubmit"));
	     
/*******************************************************
   -- Ar RBS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arReceiptBatchSubmit"));                         

/*******************************************************
   -- Ar RBS Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Ar RBS Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 

/*******************************************************
   -- Ar RBS Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        		        		        	
	        	if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}	 
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}       	

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getReceiptNumberFrom())) {
	        		
	        		criteria.put("receiptNumberFrom", actionForm.getReceiptNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReceiptNumberTo())) {
	        		
	        		criteria.put("receiptNumberTo", actionForm.getReceiptNumberTo());
	        		
	        	}
	        		        		        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbRBS.getArRctByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in ArReceiptBatchSubmitAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	     	}
            
            try {
            	
            	actionForm.clearArRBSList();
            	
            	ArrayList list = ejbRBS.getArRctByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// receipt if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // receipt if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
      		
            		ArReceiptBatchSubmitList arRBSList = new ArReceiptBatchSubmitList(actionForm,
            		    mdetails.getRctCode(),
            		    mdetails.getRctCstCustomerCode(),
            		    mdetails.getRctBaName(),
            		    Common.convertSQLDateToString(mdetails.getRctDate()),
            		    mdetails.getRctNumber(),
            		    mdetails.getRctReferenceNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getRctVoid()),
						mdetails.getRctType());
            		    
            		actionForm.saveArRBSList(arRBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("receiptBatchSubmit.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArReceiptBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arReceiptBatchSubmit");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("arReceiptBatchSubmit"));

/*******************************************************
   -- Ar RBS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar RBS Submit Action --
*******************************************************/

         } else if (request.getParameter("submitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get submit receipts
                        
		    for(int i=0; i<actionForm.getArRBSListSize(); i++) {
		    
		       ArReceiptBatchSubmitList actionList = actionForm.getArRBSByIndex(i);
		    	
               if (actionList.getSubmit()) {
               	
               	     try {
               	     	
               	     	ejbRBS.executeArRctBatchSubmit(actionList.getReceiptCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteArRBSList(i);
               	     	i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("receiptBatchSubmit.error.recordAlreadyDeleted", actionList.getReceiptNumber()));
		             
		             } catch (GlobalTransactionAlreadyApprovedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("receiptBatchSubmit.error.transactionAlreadyApproved", actionList.getReceiptNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPendingException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("receiptBatchSubmit.error.transactionAlreadyPending", actionList.getReceiptNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("receiptBatchSubmit.error.transactionAlreadyPosted", actionList.getReceiptNumber()));
		             
		             } catch (GlobalNoApprovalRequesterFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("receiptBatchSubmit.error.noApprovalRequesterFound", actionList.getReceiptNumber()));
		                  
		             } catch (GlobalNoApprovalApproverFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("receiptBatchSubmit.error.noApprovalApproverFound", actionList.getReceiptNumber()));
		               
		             } catch (GlobalTransactionAlreadyVoidPostedException ex) {
		               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("receiptBatchSubmit.error.transactionAlreadyVoidPosted", actionList.getReceiptNumber()));
	               	   
		             } catch (GlJREffectiveDateNoPeriodExistException ex) {
	               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("receiptBatchSubmit.error.effectiveDateNoPeriodExist", actionList.getReceiptNumber()));
	                        
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
	               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("receiptBatchSubmit.error.effectiveDatePeriodClosed", actionList.getReceiptNumber()));
	                        
		             } catch (GlobalJournalNotBalanceException ex) {
	               	
	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("receiptBatchSubmit.error.journalNotBalance", actionList.getReceiptNumber()));
	               	   
		             } catch (GlobalInventoryDateException ex) {
  		               
		             	errors.add(ActionMessages.GLOBAL_MESSAGE,
		             			new ActionMessage("receiptBatchSubmit.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));
		
		             } catch (GlobalBranchAccountNumberInvalidException ex) {
		             	
		             	errors.add(ActionMessages.GLOBAL_MESSAGE,
		             			new ActionMessage("receiptBatchSubmit.error.branchAccountNumberInvalid", ex.getMessage()));

		             } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {
		            	
		                errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("receiptBatchSubmit.error.noNegativeInventoryCostingCOA"));

		             } catch (EJBException ex) {
		             	
		             	if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ArReceiptBatchSubmitAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }	
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arReceiptBatchSubmit");

            }	     
	        
	        try {
	        
	            actionForm.setLineCount(0);
            	
            	actionForm.clearArRBSList();
            	
            	ArrayList list = ejbRBS.getArRctByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// receipt if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // receipt if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
      		
            		ArReceiptBatchSubmitList arRBSList = new ArReceiptBatchSubmitList(actionForm,
            		    mdetails.getRctCode(),
            		    mdetails.getRctCstCustomerCode(),
            		    mdetails.getRctBaName(),
            		    Common.convertSQLDateToString(mdetails.getRctDate()),
            		    mdetails.getRctNumber(),
            		    mdetails.getRctReferenceNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit),
            		    Common.convertByteToBoolean(mdetails.getRctVoid()),
						mdetails.getRctType());
            		    
            		actionForm.saveArRBSList(arRBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArReceiptBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }              
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("arReceiptBatchSubmit"));
	        


/*******************************************************
   -- Ar RBS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arReceiptBatchSubmit");

            }            
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	if(actionForm.getUseCustomerPulldown()) {
            		
            		actionForm.clearCustomerCodeList();
            		
            		list = ejbRBS.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            		
            		if (list == null || list.size() == 0) {
            			
            			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
            			
            		} else {
            			
            			i = list.iterator();
            			
            			while (i.hasNext()) {
            				
            				actionForm.setCustomerCodeList((String)i.next());
            				
            			}
            			
            		}  
            		
            	}
            	
            	actionForm.clearCurrencyList();
            	
            	list = ejbRBS.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}            	

            	actionForm.clearBankAccountList();
            	
            	list = ejbRBS.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();
            	
            	list = ejbRBS.getArOpenRbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearArRBSList();
            	            
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArReceiptBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
                        
            return(mapping.findForward("arReceiptBatchSubmit"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArReceiptBatchSubmitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}