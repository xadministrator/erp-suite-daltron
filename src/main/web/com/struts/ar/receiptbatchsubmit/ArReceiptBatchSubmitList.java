package com.struts.ar.receiptbatchsubmit;

import java.io.Serializable;

public class ArReceiptBatchSubmitList implements Serializable {

   private Integer receiptCode = null;
   private String customerCode = null;
   private String bankAccount = null;
   private String date = null;
   private String receiptNumber = null;
   private String referenceNumber = null;
   private String amount = null;  
   private boolean receiptVoid = false;
   private String type = null;

   private boolean submit = false;
       
   private ArReceiptBatchSubmitForm parentBean;
    
   public ArReceiptBatchSubmitList(ArReceiptBatchSubmitForm parentBean,
	  Integer receiptCode,
	  String customerCode,  
	  String bankAccount,  
	  String date,  
	  String receiptNumber,
	  String referenceNumber,  
	  String amount,  
	  boolean receiptVoid,
	  String type) {

      this.parentBean = parentBean;
      this.receiptCode = receiptCode;
      this.customerCode = customerCode;
      this.bankAccount = bankAccount;
      this.date = date;
      this.receiptNumber = receiptNumber;
      this.referenceNumber = referenceNumber;
      this.amount = amount;
      this.receiptVoid = receiptVoid;
      this.type = type;
      
   }
   
   public Integer getReceiptCode() {
   	
   	  return receiptCode;
   	  
   }
   
   public String getCustomerCode() {

      return customerCode;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getReceiptNumber() {
   	
   	  return receiptNumber;
   	 
   }

   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public boolean getReceiptVoid() {
   	
   	  return receiptVoid;
   	  
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }
   
   public boolean getSubmit() {
   	
   	  return submit;
   	
   }
   
   public void setSubmit(boolean submit) {
   	
   	  this.submit = submit;
   	
   }
      
}