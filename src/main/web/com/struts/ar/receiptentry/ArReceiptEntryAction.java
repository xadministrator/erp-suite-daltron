package com.struts.ar.receiptentry;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;
import org.apache.struts.util.MessageResources;

import com.Ostermiller.util.CSVParser;
import com.ejb.ad.LocalAdPreferenceHome;
import com.ejb.ar.LocalArReceipt;
import com.ejb.ar.LocalArReceiptHome;
import com.ejb.exception.AdPRFCoaGlCustomerDepositAccountNotFoundException;
import com.ejb.exception.ArINVOverCreditBalancePaidapplicationNotAllowedException;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.ArRCTInvoiceHasNoWTaxCodeException;
import com.ejb.exception.ArREDuplicatePayfileReferenceNumberException;
import com.ejb.exception.ArReceiptEntryValidationException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalDuplicateEmployeeIdException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.exception.InsufficientCreditBalanceException;
import com.ejb.txn.ArReceiptEntryController;
import com.ejb.txn.ArReceiptEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.ReportParameter;
import com.struts.util.User;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoicePaymentScheduleDetails;
import com.util.ArModReceiptDetails;
import com.util.ArReceiptDetails;
import com.util.EJBCommon;
import com.util.EJBHomeFactory;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;

public final class ArReceiptEntryAction extends Action{

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		HttpSession session = request.getSession();

		try{

			/*******************************************************
   Check if user has a session
			 *******************************************************/

			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("ArReceiptEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());

				}

			}else{

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			ArReceiptEntryForm actionForm = (ArReceiptEntryForm)form;

			// reset report

			actionForm.setReport(null);
			actionForm.setReport2(null);
			actionForm.setJournal(null);

			String frParam = null;

			if (request.getParameter("child") == null) {

				frParam = Common.getUserPermission(user, Constants.AR_RECEIPT_ENTRY_ID);

			} else {

				frParam = Constants.FULL_ACCESS;

			}

			if (frParam != null) {

				if (frParam.trim().equals(Constants.FULL_ACCESS)) {

					System.out.println(actionForm.getReceiptNumber() + " RECEIPT NUMBER");
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));
						return(mapping.findForward("arReceiptEntry"));

					}

				}

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}

			/*******************************************************
   Initialize ArReceiptEntryController EJB
			 *******************************************************/

			ArReceiptEntryControllerHome homeRCT = null;
			ArReceiptEntryController ejbRCT = null;

			 GlDailyRateControllerHome homeFR = null;
	         GlDailyRateController ejbFR = null;

			try {

				homeRCT = (ArReceiptEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArReceiptEntryControllerEJB", ArReceiptEntryControllerHome.class);

				homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
	                    lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);


			} catch(NamingException e) {
				if(log.isInfoEnabled()){
					log.info("NamingException caught in ArReceiptEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				return(mapping.findForward("cmnErrorPage"));
			}

			try {

				ejbRCT = homeRCT.create();
				
				 ejbFR = homeFR.create();

			} catch(CreateException e) {

				if(log.isInfoEnabled()) {

					log.info("CreateException caught in ArReceiptEntryAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}

				return(mapping.findForward("cmnErrorPage"));

			}

			ActionErrors errors = new ActionErrors();
			ActionMessages messages = new ActionMessages();

			/*******************************************************
   Call ArReceiptEntryController EJB
   getGlFcPrecisionUnit
   getAdPrfEnableArReceiptBatch
   getAdPrfArUseCustomerPulldown
			 *******************************************************/

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

			short precisionUnit = 0;
			boolean enableReceiptBatch = false;
			boolean isInitialPrinting = false;
			boolean useCustomerPulldown = true;


			String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ar/Receipt/";
			long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
			String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
			String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

			try {

				precisionUnit = ejbRCT.getGlFcPrecisionUnit(user.getCmpCode());
				enableReceiptBatch = Common.convertByteToBoolean(ejbRCT.getAdPrfEnableArReceiptBatch(user.getCmpCode()));
				actionForm.setShowBatchName(enableReceiptBatch);
				useCustomerPulldown = Common.convertByteToBoolean(ejbRCT.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
				actionForm.setUseCustomerPulldown(useCustomerPulldown);
				actionForm.setPrecisionUnit(precisionUnit);

			} catch(EJBException ex) {

				if (log.isInfoEnabled()) {

					log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}

				return(mapping.findForward("cmnErrorPage"));

			}

			/*******************************************************
   -- Ar RCT Previous Action --
			 *******************************************************/

			if(request.getParameter("previousButton") != null){

				actionForm.setLineCount(actionForm.getLineCount() - actionForm.getMaxRows());

				// check if prev should be disabled
				if (actionForm.getLineCount() == 0) {

					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);

				} else {

					actionForm.setDisablePreviousButton(false);
					actionForm.setDisableFirstButton(false);

				}

				// check if next should be disabled
				if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

					actionForm.setDisableNextButton(true);
					actionForm.setDisableLastButton(true);

				} else {

					actionForm.setDisableNextButton(false);
					actionForm.setDisableLastButton(false);

				}

				if (request.getParameter("child") == null) {

					return (mapping.findForward("arReceiptEntry"));

				} else {

					return (mapping.findForward("arReceiptEntryChild"));

				}

				/*******************************************************
   -- Ar RCT Next Action --
				 *******************************************************/

			} else if(request.getParameter("nextButton") != null){

				actionForm.setLineCount(actionForm.getLineCount() + actionForm.getMaxRows());

				// check if prev should be disabled
				if (actionForm.getLineCount() == 0) {

					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);

				} else {

					actionForm.setDisablePreviousButton(false);
					actionForm.setDisableFirstButton(false);

				}

				// check if next should be disabled
				if (actionForm.getArRCTListSize() <= actionForm.getLineCount() + actionForm.getMaxRows()) {

					actionForm.setDisableNextButton(true);
					actionForm.setDisableLastButton(true);

				} else {

					actionForm.setDisableNextButton(false);
					actionForm.setDisableLastButton(false);

				}


				if (request.getParameter("child") == null) {

					return (mapping.findForward("arReceiptEntry"));

				} else {

					return (mapping.findForward("arReceiptEntryChild"));

				}

				/*******************************************************
   -- Ar RCT First Action --
				 *******************************************************/

			} else if(request.getParameter("firstButton") != null){

				actionForm.setLineCount(0);

				// check if prev should be disabled
				if (actionForm.getLineCount() == 0) {

					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);

				} else {

					actionForm.setDisablePreviousButton(false);
					actionForm.setDisableFirstButton(false);

				}

				// check if next should be disabled
				if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

					actionForm.setDisableNextButton(true);
					actionForm.setDisableLastButton(true);

				} else {

					actionForm.setDisableNextButton(false);
					actionForm.setDisableLastButton(false);

				}

				if (request.getParameter("child") == null) {

					return (mapping.findForward("arReceiptEntry"));

				} else {

					return (mapping.findForward("arReceiptEntryChild"));

				}





				/*******************************************************
  -- Ar RCT Last Action --
				 *******************************************************/

			} else if(request.getParameter("lastButton") != null){

				int size = actionForm.getArRCTListSize();

				if((size % actionForm.getMaxRows()) != 0) {

					actionForm.setLineCount(size -(size % actionForm.getMaxRows()));

				} else {

					actionForm.setLineCount(size - actionForm.getMaxRows());

				}

				// check if prev should be disabled
				if (actionForm.getLineCount() == 0) {

					actionForm.setDisablePreviousButton(true);
					actionForm.setDisableFirstButton(true);

				} else {

					actionForm.setDisablePreviousButton(false);
					actionForm.setDisableFirstButton(false);

				}

				// check if next should be disabled
				if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

					actionForm.setDisableNextButton(true);
					actionForm.setDisableLastButton(true);

				} else {

					actionForm.setDisableNextButton(false);
					actionForm.setDisableLastButton(false);

				}

				if (request.getParameter("child") == null) {

					return (mapping.findForward("arReceiptEntry"));

				} else {

					return (mapping.findForward("arReceiptEntryChild"));

				}

			}

			/*******************************************************
   -- Ar RCT Save As Draft Action --
			 *******************************************************/

			if (request.getParameter("saveAsDraftButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArReceiptDetails details = new ArReceiptDetails();

				details.setRctCode(actionForm.getReceiptCode());
				details.setRctDocumentType(actionForm.getDocumentType());
				details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
				details.setRctNumber(actionForm.getReceiptNumber());
				details.setRctReferenceNumber(actionForm.getReferenceNumber());
				details.setRctCheckNo(actionForm.getCheckNo());
				details.setRctAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
				details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
				details.setRctDescription(actionForm.getDescription());
				details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
				details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
				details.setRctPaymentMethod(actionForm.getPaymentMethod());
				details.setRctCustomerName(actionForm.getCustomerName());
				details.setRctEnableAdvancePayment(Common.convertBooleanToByte(actionForm.getEnableAdvancePayment()));

				if(actionForm.getEnableAdvancePayment()){

					double excessAmount = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(),precisionUnit) - details.getRctAmount() ;

					if(excessAmount < 0){
						errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("receiptEntry.error.amountPaidLessThanAmount"));

					}


					if(excessAmount == 0){

						details.setRctEnableAdvancePayment(Common.convertBooleanToByte(false));
					}

					details.setRctExcessAmount(excessAmount);
				}

				if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arReceiptEntry"));

	   	    	}

				if (actionForm.getReceiptCode() == null) {

					details.setRctCreatedBy(user.getUserName());
					details.setRctDateCreated(new java.util.Date());

				}

				details.setRctLastModifiedBy(user.getUserName());
				details.setRctDateLastModified(new java.util.Date());


				double totalCreditBalancePaid = 0d;

				ArrayList ipsList = new ArrayList();

				for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

					ArReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

					if (!arRCTList.getPayCheckbox()) continue;

					ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();

					mdetails.setAiIpsCode(arRCTList.getInvoicePaymentScheduleCode());
					mdetails.setAiApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit));
					mdetails.setAiPenaltyApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getPenaltyApplyAmount(), precisionUnit));

					mdetails.setAiCreditableWTax(Common.convertStringMoneyToDouble(arRCTList.getCreditableWTax(), precisionUnit));
					mdetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(arRCTList.getDiscount(), precisionUnit));

					mdetails.setAiAllocatedPaymentAmount(Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit));
					mdetails.setAiAppliedDeposit(Common.convertStringMoneyToDouble(arRCTList.getAppliedDeposit(), precisionUnit));
					mdetails.setAiCreditBalancePaid(Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit));

					mdetails.setAiRebate(arRCTList.getRebateCheckbox() == true ? Common.convertStringMoneyToDouble(arRCTList.getRebate(), precisionUnit) : 0d  );
					mdetails.setAiApplyRebate(mdetails.getAiRebate() > 0 ? Common.convertBooleanToByte(arRCTList.getRebateCheckbox()) : (byte)0);


					totalCreditBalancePaid += Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit);
					ipsList.add(mdetails);

				}



				if(actionForm.getEnableAdvancePayment()){
					double amount = Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit);

					double amountPaid = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(), precisionUnit);


					if(amountPaid<amount){
						errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invoiceEntry.error.filename1NotFound"));
					}
				}

				if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arReceiptEntry"));

	   	    	}


				// Validate attachment

				String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
				String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
				String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
				String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

				if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invoiceEntry.error.filename1NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename1Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arInvoiceEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invoiceEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename2Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arInvoiceEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invoiceEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename3Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arInvoiceEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invoiceEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename4Invalid"));

		          	    	}

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invoiceEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arInvoiceEntry"));

		   	    	}

		   	   	}

				try {

					ejbRCT.saveArRctEntry(details, actionForm.getRecalculateJournal(), actionForm.getBankAccount(),
							actionForm.getCurrency(),
							actionForm.getCustomer(), actionForm.getBatchName(),
							ipsList, true, false, actionForm.getPayrollPeriodName(),  new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.recordAlreadyDeleted"));

				} catch (GlobalDocumentNumberNotUniqueException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.documentNumberNotUnique"));

				} catch (GlobalConversionDateNotExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.conversionDateNotExist"));

				} catch (GlobalTransactionAlreadyApprovedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyApproved"));

				} catch (ArREDuplicatePayfileReferenceNumberException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.payfileReferenceNumberNotUnique"));

				}catch (GlobalTransactionAlreadyPendingException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyPending"));

				} catch (GlobalTransactionAlreadyPostedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyPosted"));


				} catch (GlobalTransactionAlreadyVoidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyVoid"));

				} catch (ArINVOverapplicationNotAllowedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.overapplicationNotAllowed", ex.getMessage()));

				} catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receiptEntry.error.insufficientCreditBalance"));


				} catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.invoiceHasNoWTaxCode", ex.getMessage()));

				} catch (GlobalTransactionAlreadyLockedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyLocked", ex.getMessage()));

				} catch (GlobalNoApprovalRequesterFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noApprovalRequesterFound"));

				} catch (GlobalNoApprovalApproverFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noApprovalApproverFound"));

				} catch (GlJREffectiveDateNoPeriodExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.effectiveDateNoPeriodExist"));

				} catch (GlJREffectiveDatePeriodClosedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.effectiveDatePeriodClosed"));

				} catch (GlobalJournalNotBalanceException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.journalNotBalance"));

				} catch (GlobalBranchAccountNumberInvalidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.branchAccountNumberInvalid"));

				} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.recordAlreadyAssigned"));

				} catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noCustomerDepositCOA"));

				} catch (EJBException ex) {
					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}

					return(mapping.findForward("cmnErrorPage"));
				}


				// save attachment

		           if (!Common.validateRequired(filename1)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename1().getInputStream();

		       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

		       	    		int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename2)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename2().getInputStream();

		       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename3)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename3().getInputStream();

		       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename4)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename4().getInputStream();

		       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

	/*******************************************************
   -- Ar RCT Void Action --
				 *******************************************************/

			} else if (request.getParameter("executeVoid") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


				String RctCode = request.getParameter("RctCode");
				String branchCode = request.getParameter("BranchCode");


				ArModReceiptDetails mdetails = ejbRCT.getArRctByRctCode(Integer.parseInt(RctCode), user.getCmpCode());

				ArReceiptDetails details = new ArReceiptDetails();

				details.setRctCode(mdetails.getRctCode());
				details.setRctDocumentType(actionForm.getDocumentType());
				details.setRctDate(mdetails.getRctDate());
				details.setRctNumber(mdetails.getRctNumber());
				details.setRctReferenceNumber(mdetails.getRctReferenceNumber());
				details.setRctCheckNo(mdetails.getRctCheckNo());
				details.setRctAmount(mdetails.getRctAmount());
				details.setRctVoid(Common.convertBooleanToByte(true));
				details.setRctDescription(mdetails.getRctDescription());
				details.setRctConversionDate(mdetails.getRctConversionDate());
				details.setRctConversionRate(mdetails.getRctConversionRate());
				details.setRctPaymentMethod(mdetails.getRctPaymentMethod());
				details.setRctCustomerName(mdetails.getRctCustomerName());
				details.setRctEnableAdvancePayment(mdetails.getRctEnableAdvancePayment());
				details.setRctExcessAmount(mdetails.getRctExcessAmount());


				if (actionForm.getReceiptCode() == null) {

					details.setRctCreatedBy(user.getUserName());
					details.setRctDateCreated(new java.util.Date());

				}

				details.setRctLastModifiedBy(user.getUserName());
				details.setRctDateLastModified(new java.util.Date());


				double totalCreditBalancePaid = 0d;

				ArrayList ipsList = new ArrayList();



				for (int i = 0; i<mdetails.getRctAiList().size(); i++) {

					ArModAppliedInvoiceDetails mAiDetails = (ArModAppliedInvoiceDetails)mdetails.getRctAiList().get(i);

					ipsList.add(mAiDetails);

				}


				try {


					System.out.println("BRANCH: " + branchCode);
					System.out.println("RCT_CODE: " + RctCode);
					Integer receiptCode = ejbRCT.saveArRctEntry(details, actionForm.getRecalculateJournal(), mdetails.getRctBaName(),
							mdetails.getRctFcName(),
							mdetails.getRctCstCustomerCode(), mdetails.getRctRbName(),
							ipsList, false,false, actionForm.getPayrollPeriodName(),Integer.parseInt(branchCode), user.getCmpCode());




				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.recordAlreadyDeleted"));

				} catch (GlobalDocumentNumberNotUniqueException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.documentNumberNotUnique"));

				} catch (GlobalConversionDateNotExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.conversionDateNotExist"));

				} catch (ArREDuplicatePayfileReferenceNumberException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.payfileReferenceNumberNotUnique"));

				} catch (GlobalTransactionAlreadyApprovedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyApproved"));

				} catch (GlobalTransactionAlreadyPendingException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyPending"));

				} catch (GlobalTransactionAlreadyPostedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyPosted"));

				}catch (GlobalTransactionAlreadyVoidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyVoid"));

				} catch (ArINVOverapplicationNotAllowedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.overapplicationNotAllowed", ex.getMessage()));

				} catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receiptEntry.error.insufficientCreditBalance"));


				} catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.invoiceHasNoWTaxCode", ex.getMessage()));

				} catch (GlobalTransactionAlreadyLockedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyLocked", ex.getMessage()));

				} catch (GlobalNoApprovalRequesterFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noApprovalRequesterFound"));

				} catch (GlobalNoApprovalApproverFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noApprovalApproverFound"));

				} catch (GlJREffectiveDateNoPeriodExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.effectiveDateNoPeriodExist"));

				} catch (GlJREffectiveDatePeriodClosedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.effectiveDatePeriodClosed"));

				} catch (GlobalJournalNotBalanceException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.journalNotBalance"));

				} catch (GlobalBranchAccountNumberInvalidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.branchAccountNumberInvalid"));

				} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.recordAlreadyAssigned"));

				} catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noCustomerDepositCOA"));

				} catch (EJBException ex) {
					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}

					return(mapping.findForward("cmnErrorPage"));
				}




				/*******************************************************
   -- Ar RCT Save & Submit Action --
				 *******************************************************/

			} else if (request.getParameter("saveSubmitButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArReceiptDetails details = new ArReceiptDetails();

				details.setRctCode(actionForm.getReceiptCode());
				details.setRctDocumentType(actionForm.getDocumentType());
				details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
				details.setRctNumber(actionForm.getReceiptNumber());
				details.setRctReferenceNumber(actionForm.getReferenceNumber());
				details.setRctCheckNo(actionForm.getCheckNo());
				details.setRctAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
				details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
				details.setRctDescription(actionForm.getDescription());
				details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
				details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
				details.setRctPaymentMethod(actionForm.getPaymentMethod());
				details.setRctCustomerName(actionForm.getCustomerName());
				details.setRctEnableAdvancePayment(Common.convertBooleanToByte(actionForm.getEnableAdvancePayment()));

				if(actionForm.getEnableAdvancePayment()){

					double excessAmount = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(),precisionUnit) - details.getRctAmount() ;

					if(excessAmount < 0){
						errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("receiptEntry.error.amountPaidLessThanAmount"));

					}


					if(excessAmount == 0){

						details.setRctEnableAdvancePayment(Common.convertBooleanToByte(false));
					}

					details.setRctExcessAmount(excessAmount);
				}

				if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arReceiptEntry"));

	   	    	}

				if (actionForm.getReceiptCode() == null) {

					details.setRctCreatedBy(user.getUserName());
					details.setRctDateCreated(new java.util.Date());

				}

				details.setRctLastModifiedBy(user.getUserName());
				details.setRctDateLastModified(new java.util.Date());


				double totalCreditBalancePaid = 0d;

				ArrayList ipsList = new ArrayList();

				for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

					ArReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

					if (!arRCTList.getPayCheckbox()) continue;

					ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();

					mdetails.setAiIpsCode(arRCTList.getInvoicePaymentScheduleCode());
					mdetails.setAiApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit) );
					mdetails.setAiPenaltyApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getPenaltyApplyAmount(), precisionUnit));
					mdetails.setAiCreditableWTax(Common.convertStringMoneyToDouble(arRCTList.getCreditableWTax(), precisionUnit));
					mdetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(arRCTList.getDiscount(), precisionUnit));
					mdetails.setAiAllocatedPaymentAmount(Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit));
					mdetails.setAiAppliedDeposit(Common.convertStringMoneyToDouble(arRCTList.getAppliedDeposit(), precisionUnit));
					mdetails.setAiCreditBalancePaid(Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit));

					mdetails.setAiRebate(arRCTList.getRebateCheckbox() == true ? Common.convertStringMoneyToDouble(arRCTList.getRebate(), precisionUnit) : 0d  );
					mdetails.setAiApplyRebate(mdetails.getAiRebate() > 0 ? Common.convertBooleanToByte(arRCTList.getRebateCheckbox()) : (byte)0);

					totalCreditBalancePaid += Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit);
					ipsList.add(mdetails);

				}



				if(actionForm.getEnableAdvancePayment()){
					double amount = Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit);

					double amountPaid = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(), precisionUnit);


					if(amountPaid<amount){
						errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invoiceEntry.error.filename1NotFound"));
					}
				}

				if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("arReceiptEntry"));

	   	    	}


//	           validate attachment

	             String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	             String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	             String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	             String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	             if (!Common.validateRequired(filename1)) {

			   	    	if (actionForm.getFilename1().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invoiceEntry.error.filename1NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename1Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename1().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename1SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("arInvoiceEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename2)) {

			   	    	if (actionForm.getFilename2().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invoiceEntry.error.filename2NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename2Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename2().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename2SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("arInvoiceEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename3)) {

			   	    	if (actionForm.getFilename3().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invoiceEntry.error.filename3NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename3Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename3().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename3SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("arInvoiceEntry"));

			   	    	}

			   	   }

			   	   if (!Common.validateRequired(filename4)) {

			   	    	if (actionForm.getFilename4().getFileSize() == 0) {

			   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("invoiceEntry.error.filename4NotFound"));

			   	    	} else {

			   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
			   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename4Invalid"));

			          	    	}

			          	    	InputStream is = actionForm.getFilename4().getInputStream();

			          	    	if (is.available() > maxAttachmentFileSize) {

			          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   		new ActionMessage("invoiceEntry.error.filename4SizeInvalid"));

			          	    	}

			          	    	is.close();

			   	    	}

			   	    	if (!errors.isEmpty()) {

			   	    		saveErrors(request, new ActionMessages(errors));
			          			return (mapping.findForward("arInvoiceEntry"));

			   	    	}

			   	   	}


				try {

					Integer receiptCode = ejbRCT.saveArRctEntry(details, actionForm.getRecalculateJournal(), actionForm.getBankAccount(),
							actionForm.getCurrency(),
							actionForm.getCustomer(), actionForm.getBatchName(),
							ipsList, false,false, actionForm.getPayrollPeriodName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					actionForm.setReceiptCode(receiptCode);





				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.recordAlreadyDeleted"));

				} catch (GlobalDocumentNumberNotUniqueException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.documentNumberNotUnique"));

				} catch (GlobalConversionDateNotExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.conversionDateNotExist"));

				} catch (ArREDuplicatePayfileReferenceNumberException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.payfileReferenceNumberNotUnique"));

				} catch (GlobalTransactionAlreadyApprovedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyApproved"));

				} catch (GlobalTransactionAlreadyPendingException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyPending"));

				} catch (GlobalTransactionAlreadyPostedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyPosted"));

				}catch (GlobalTransactionAlreadyVoidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyVoid"));

				} catch (ArINVOverapplicationNotAllowedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.overapplicationNotAllowed", ex.getMessage()));

				} catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("receiptEntry.error.insufficientCreditBalance"));


				} catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.invoiceHasNoWTaxCode", ex.getMessage()));

				} catch (GlobalTransactionAlreadyLockedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.transactionAlreadyLocked", ex.getMessage()));

				} catch (GlobalNoApprovalRequesterFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noApprovalRequesterFound"));

				} catch (GlobalNoApprovalApproverFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noApprovalApproverFound"));

				} catch (GlJREffectiveDateNoPeriodExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.effectiveDateNoPeriodExist"));

				} catch (GlJREffectiveDatePeriodClosedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.effectiveDatePeriodClosed"));

				} catch (GlobalJournalNotBalanceException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.journalNotBalance"));

				} catch (GlobalBranchAccountNumberInvalidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.branchAccountNumberInvalid"));

				} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.recordAlreadyAssigned"));

				} catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.noCustomerDepositCOA"));

				} catch (EJBException ex) {
					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}

					return(mapping.findForward("cmnErrorPage"));
				}


				// save attachment

		           if (!Common.validateRequired(filename1)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename1().getInputStream();

		       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

		       	    		int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename2)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename2().getInputStream();

		       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename3)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename3().getInputStream();

		       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

		       	   if (!Common.validateRequired(filename4)) {

		       	        if (errors.isEmpty()) {

		       	        	InputStream is = actionForm.getFilename4().getInputStream();

		       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		       	        	new File(attachmentPath).mkdirs();

		       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

		       	    			fileExtension = attachmentFileExtension;

		           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		           	    		fileExtension = attachmentFileExtensionPDF;

		           	    	}
		       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

			            	int c;

			            	while ((c = is.read()) != -1) {

			            		fos.write((byte)c);

			            	}

			            	is.close();
							fos.close();

		       	        }

		       	   }

				/*******************************************************
	 -- Ar RCT Print Action --
				 *******************************************************/

			} else if (request.getParameter("printButton") != null) {

				if(Common.validateRequired(actionForm.getApprovalStatus())) {

					ArReceiptDetails details = new ArReceiptDetails();

					details.setRctCode(actionForm.getReceiptCode());
					details.setRctDocumentType(actionForm.getDocumentType());
					details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
					details.setRctNumber(actionForm.getReceiptNumber());
					details.setRctReferenceNumber(actionForm.getReferenceNumber());
					details.setRctCheckNo(actionForm.getCheckNo());
					details.setRctAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
					details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
					details.setRctDescription(actionForm.getDescription());
					details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
					details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
					details.setRctPaymentMethod(actionForm.getPaymentMethod());
					details.setRctCustomerName(actionForm.getCustomerName());
					details.setRctEnableAdvancePayment(Common.convertBooleanToByte(actionForm.getEnableAdvancePayment()));

					if(actionForm.getEnableAdvancePayment()){

						double excessAmount = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(),precisionUnit) - details.getRctAmount() ;

						if(excessAmount < 0){
							errors.add(ActionMessages.GLOBAL_MESSAGE,
				           			new ActionMessage("receiptEntry.error.amountPaidLessThanAmount"));

						}


						if(excessAmount == 0){

							details.setRctEnableAdvancePayment(Common.convertBooleanToByte(false));
						}

						details.setRctExcessAmount(excessAmount);
					}

					if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arReceiptEntry"));

		   	    	}

					if (actionForm.getReceiptCode() == null) {

						details.setRctCreatedBy(user.getUserName());
						details.setRctDateCreated(new java.util.Date());

					}

					details.setRctLastModifiedBy(user.getUserName());
					details.setRctDateLastModified(new java.util.Date());


					double totalCreditBalancePaid = 0d;

					ArrayList ipsList = new ArrayList();

					for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

						ArReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

						if (!arRCTList.getPayCheckbox()) continue;

						ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();

						mdetails.setAiIpsCode(arRCTList.getInvoicePaymentScheduleCode());
						mdetails.setAiApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit));
						mdetails.setAiPenaltyApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getPenaltyApplyAmount(), precisionUnit));
						mdetails.setAiCreditableWTax(Common.convertStringMoneyToDouble(arRCTList.getCreditableWTax(), precisionUnit));
						mdetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(arRCTList.getDiscount(), precisionUnit));
						mdetails.setAiAllocatedPaymentAmount(Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit));
						mdetails.setAiAppliedDeposit(Common.convertStringMoneyToDouble(arRCTList.getAppliedDeposit(), precisionUnit));
						mdetails.setAiCreditBalancePaid(Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit));

						mdetails.setAiRebate(arRCTList.getRebateCheckbox() == true ? Common.convertStringMoneyToDouble(arRCTList.getRebate(), precisionUnit) : 0d  );
						mdetails.setAiApplyRebate(mdetails.getAiRebate() > 0 ? Common.convertBooleanToByte(arRCTList.getRebateCheckbox()) : (byte)0);

						totalCreditBalancePaid += Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit);
						ipsList.add(mdetails);

					}



					if(actionForm.getEnableAdvancePayment()){
						double amount = Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit);

						double amountPaid = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(), precisionUnit);


						if(amountPaid<amount){
							errors.add(ActionMessages.GLOBAL_MESSAGE,
				           			new ActionMessage("invoiceEntry.error.filename1NotFound"));
						}
					}

					if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arReceiptEntry"));

		   	    	}

//	               validate attachment

	                 String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	                 String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	                 String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	                 String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	                 if (!Common.validateRequired(filename1)) {

	 		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename1NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	 		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename1Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename1().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename1SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   }

	 		   	   if (!Common.validateRequired(filename2)) {

	 		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename2NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	 		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename2Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename2().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename2SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   }

	 		   	   if (!Common.validateRequired(filename3)) {

	 		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename3NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	 		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename3Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename3().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename3SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   }

	 		   	   if (!Common.validateRequired(filename4)) {

	 		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename4NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	 		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename4Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename4().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename4SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   	}


					try {

						Integer receiptCode  = ejbRCT.saveArRctEntry(details, actionForm.getRecalculateJournal(), actionForm.getBankAccount(),
								actionForm.getCurrency(),
								actionForm.getCustomer(), actionForm.getBatchName(),
								ipsList, true, false, actionForm.getPayrollPeriodName(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						actionForm.setReceiptCode(receiptCode);

					} catch (GlobalRecordAlreadyDeletedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.recordAlreadyDeleted"));

					} catch (GlobalDocumentNumberNotUniqueException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.documentNumberNotUnique"));

					} catch (GlobalConversionDateNotExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.conversionDateNotExist"));

					} catch (GlobalTransactionAlreadyApprovedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyApproved"));

					} catch (GlobalTransactionAlreadyPendingException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyPending"));

					} catch (GlobalTransactionAlreadyPostedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyPosted"));

					} catch (GlobalTransactionAlreadyVoidException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyVoid"));

					} catch (ArINVOverapplicationNotAllowedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.overapplicationNotAllowed", ex.getMessage()));

					} catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("receiptEntry.error.insufficientCreditBalance"));


					} catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.invoiceHasNoWTaxCode", ex.getMessage()));

					} catch (GlobalTransactionAlreadyLockedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyLocked", ex.getMessage()));

					} catch (GlobalNoApprovalRequesterFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.noApprovalRequesterFound"));

					} catch (GlobalNoApprovalApproverFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.noApprovalApproverFound"));
					} catch (GlJREffectiveDateNoPeriodExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.effectiveDateNoPeriodExist"));

					} catch (GlJREffectiveDatePeriodClosedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.effectiveDatePeriodClosed"));

					} catch (GlobalJournalNotBalanceException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.journalNotBalance"));

					} catch (GlobalBranchAccountNumberInvalidException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.branchAccountNumberInvalid"));

					} catch (GlobalRecordAlreadyAssignedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.recordAlreadyAssigned"));

					} catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.noCustomerDepositCOA"));

					} catch (EJBException ex) {
						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
						}

						return(mapping.findForward("cmnErrorPage"));
					}


					// save attachment

			           if (!Common.validateRequired(filename1)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename1().getInputStream();

			       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

			       	    		int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename2)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename2().getInputStream();

			       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename3)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename3().getInputStream();

			       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename4)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename4().getInputStream();

			       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

					if (!errors.isEmpty()) {

						saveErrors(request, new ActionMessages(errors));
						return(mapping.findForward("arReceiptEntry"));

					}

					actionForm.setReport(Constants.STATUS_SUCCESS);

					isInitialPrinting = true;

				}  else {

					actionForm.setReport(Constants.STATUS_SUCCESS);

					return(mapping.findForward("arReceiptEntry"));

				}




				/*******************************************************
  -- Ar RCT Upload Receipt File FIRST IN FIRST OUT--
				 *******************************************************/

			}else if(request.getParameter("saveUpload2Button") != null &&
		    		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				 if(actionForm.getUploadFile2().getFileSize() == 0){

			       		errors.add(ActionMessages.GLOBAL_MESSAGE,
			   				 new ActionMessage("bankReconciliation.error.filenotfound"));

			       		saveErrors(request, new ActionMessages(errors));
			       		return mapping.findForward("cmBankReconciliation");
			     }

	    		 int NumberOfSuccess = 0;
	    		 int NumberOfErrors = 0;
	    		 int NumberOfAlreadyPosted = 0;
	    		 int NumberOfAdvCreated = 0;
		    	 boolean noCustomerError = false;
		    	 boolean noNoInvoiceError = false;
		    	 boolean noInvoiceList = false;
		    	 ArrayList transactionDetails = new ArrayList();
		    	 ArrayList statusDetails = new ArrayList();

		    	 String[][] values = new String[0][0];
		    	 if(actionForm.getUploadFileType().equals("lis")){

		    		 InputStream input = actionForm.getUploadFile2().getInputStream();
		    		 BufferedReader br = new BufferedReader(new InputStreamReader(input));
		    		 ArrayList readLineList = new ArrayList();

		    		 String strLine;
		    		 System.out.println("LIS READING");
		    		 //Read File Line By Line
		    		 strLine = br.readLine();
		    		 System.out.println("1st line" + strLine);
		    		 Integer controlNumber = 1;
		    		 while ((strLine = br.readLine()) != null)   {

		    		   String[] dataLine = new String[10];

		    		 //  System.out.println (strLine);

		    		   if(strLine.split(" ").length>1){

		    			   System.out.println("next line " + strLine);
		    			   String empIdName =  strLine.substring(0,41);
		    			   //emp id
		    			   String empID = empIdName.substring(0,10).trim();
		    			   //emp name
		    			   String empName = empIdName.substring(10,empIdName.length()).trim();
		    			   //Amount
		    			   strLine = strLine.substring(41,strLine.length());
		    			   String[] amountSplit = strLine.split("\\+");

		    			   String ftstAmt = amountSplit[2].substring(0,  amountSplit[2].length()-2) ;
		    			   String scndAmt = amountSplit[2].substring(ftstAmt.length(),amountSplit[2].length());

		    			   Double tmpAmnt2 ;
		    			   String amount = "0";
		    			   System.out.println(scndAmt + " decimal val");
		    			   if(Double.parseDouble(scndAmt)>0){
		    				   tmpAmnt2 = Double.parseDouble(ftstAmt + "." + scndAmt);
		    				   amount = tmpAmnt2.toString();
		    			   }else{
		    				   DecimalFormat df = new DecimalFormat("####");
		    				   System.out.println("no decimal");
		    				   tmpAmnt2 = Double.parseDouble(ftstAmt);
		    				   amount = df.format(tmpAmnt2);
		    			   }

		    			   //date
		    			   StringTokenizer st = new StringTokenizer(strLine);

		    			   st.nextToken();
		    			   st.nextToken();
		    			   String date = 	   st.nextToken();


		    			   date = date.substring(2,date.trim().length());


		    		//	   System.out.println(strLine+ "<>");





		    			   readLineList.add(controlNumber.toString() + "$" + empID + "$" + empName + "$" + amount + "$" + date);


		    		   }


		    		   controlNumber++;



		    		 }

		    		 values = new String [readLineList.size()+1][5];
		    		 values[0][0] = "Control";
    				 values[0][1] = "EMpID";
    				 values[0][2] = "EmpName";
    				 values[0][3] = "Date";
    				 values[0][4] = "Amount";
		    		 for(Integer x = 0;x<readLineList.size();x++){

		    			 String[] rawLine = (String[])readLineList.get(x).toString().split("\\$");


		    			 System.out.println(rawLine[0] + "=" + rawLine[1]+"=" + rawLine[2] +"="+ rawLine[3] + "<<");

		    			 //control number
		    			values[x+1][0] = rawLine[0];
		    			//emp id
		    			values[x+1][1] = "#" + rawLine[1];
		    			//name
		    			values[x+1][2] = rawLine[2].replace(',', ' ');


		    			//date

		    			String str_date=rawLine[4];
		    			DateFormat formatter ;
		    			Date dateData ;
	    			   formatter = new SimpleDateFormat("dd/MM/yy");
	    			   dateData = formatter.parse(str_date);

	    			   DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		    			values[x+1][3] = df.format(dateData).toString();

		    			//amount
		    			values[x+1][4] =rawLine[3];
		    		//	System.out.println(values[x][0] + "=" + values[x][1]+"=" + values[x][2] +"="+ values[x][3] +"=" + values[x][4]);

		    		 }



		    	 }else{
		    		 CSVParser csvParser = new CSVParser(actionForm.getUploadFile2().getInputStream());

		    		 values= csvParser.getAllValues();


		    	 }

                        boolean duplicateReceipt  = false;

                        String customer_code = "";
                        String date_inv = "";
                        int numOferrPerTrans = 0;
                        String[] data_value = new String[9];

                        boolean new_transaction = false;
                        String control_number = "";
                        boolean noCustomerFound = false;
                        ArrayList arReceipEntryUploadList = new ArrayList();

                        String transactionErrorLog = "";
                        ArrayList customerInvLineDetails = new ArrayList();

                        ArReceiptEntryUploadList receiptUploadDetails = new ArReceiptEntryUploadList(precisionUnit);

                        double applyAmount = 0.00;

                        for(int x=1;x<values.length;x++){
                                receiptUploadDetails = new ArReceiptEntryUploadList(precisionUnit);

                                receiptUploadDetails.addCsvDataList2(values[x],true);
                                Double APPLY_AMOUNT = receiptUploadDetails.details.getRctAmount();
                                System.out.println("APPLY AMOUNT: " + APPLY_AMOUNT);
                                String EMPLOYEE_ID = receiptUploadDetails.getEmployeeId().replace('#', ' ').trim();
                                String CONTROL_NUMBER = receiptUploadDetails.getControlNumber();
                                String CUSTOMER_CODE = "";
                                int CUSTOMER_AD_BRANCH = 0;
                                String CUSTOMER_NAME = receiptUploadDetails.getCustomerName();

                                System.out.println(CONTROL_NUMBER + " contrl");
                                boolean isInvoiceFound = false;

                                customerInvLineDetails = new ArrayList();


                                receiptUploadDetails.details.setRctCreatedBy(user.getUserName());
                                receiptUploadDetails.details.setRctLastModifiedBy(user.getUserName());


                               //EMPLOYEE ID
                               try{
                                   System.out.println("emp id : " + EMPLOYEE_ID);
                                   CUSTOMER_CODE = ejbRCT.getArCstCstmrCodeByEmplyId(EMPLOYEE_ID, user.getCmpCode());
                                   CUSTOMER_AD_BRANCH = ejbRCT.getArCstAdBrnchByEmplyId(EMPLOYEE_ID, user.getCmpCode());

                                   receiptUploadDetails.setCustomerCode(CUSTOMER_CODE);
                                   receiptUploadDetails.setCstAdBranch(CUSTOMER_AD_BRANCH);

                               }catch(GlobalDuplicateEmployeeIdException gdx){

                                   receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " +  EMPLOYEE_ID + " " + CUSTOMER_NAME + " more than 2 Employee ID";
                                   receiptUploadDetails.isError = true;
                                   arReceipEntryUploadList.add(receiptUploadDetails);
                                   System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " +  EMPLOYEE_ID + " " + CUSTOMER_NAME + " more than 2 Employee ID");
                                   continue;


                               }catch(GlobalNoRecordFoundException gx){
                                   receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " +  EMPLOYEE_ID + " " + CUSTOMER_NAME + " not exist";
                                   receiptUploadDetails.isError = true;
                                   arReceipEntryUploadList.add(receiptUploadDetails);
                                   System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " +  EMPLOYEE_ID + " " + CUSTOMER_NAME + " not exist");
                                   continue;
                               }






                                //Check Bank Account

                               ArModCustomerDetails mdetails = ejbRCT.getArCstByCstCustomerCode(receiptUploadDetails.getCustomerCode(), user.getCmpCode());

                                System.out.println(mdetails.getCstCtBaName());
                               receiptUploadDetails.setBankAccount(mdetails.getCstCtBaName());

                               //currency
                               receiptUploadDetails.setCurrency("PGK");

                               if(APPLY_AMOUNT<=0){

                                   receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - "+ EMPLOYEE_ID + " " + CUSTOMER_NAME +" - APPLY AMOUNT CANNOT BE ZERO";
                                   receiptUploadDetails.isError = true;
                                   arReceipEntryUploadList.add(receiptUploadDetails);
                                   System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + EMPLOYEE_ID + " " + CUSTOMER_NAME + " APPLY AMOUNT CANNOT BE ZERO");
                                   continue;
                                }


                                //get invoice of customer


                                try{
                                   //ArrayList list = ejbRCT.getArIpsByCstCustomerCode(receiptUploadDetails.getCustomerCode(),Common.convertStringToSQLDate(actionForm.getDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                                   ArrayList list = ejbRCT.getArIpsByCstCustomerCode(receiptUploadDetails.getCustomerCode(),receiptUploadDetails.details.getRctDate(), false,0, user.getCmpCode());
                                   int branch_code_confirm = 0;


                                   Iterator i = list.iterator();
                                   while (i.hasNext()) {
                                       ArModInvoicePaymentScheduleDetails mLinedetails = (ArModInvoicePaymentScheduleDetails)i.next();
                                       if(branch_code_confirm!=mLinedetails.getIpsAdBranch()){
                                           receiptUploadDetails.branchCodes.add(mLinedetails.getIpsAdBranch());
                                           branch_code_confirm = mLinedetails.getIpsAdBranch();


                                       }
                                       customerInvLineDetails.add(mLinedetails);

                                   }



                                }catch (GlobalNoRecordFoundException ex) {


                                   if(ejbRCT.checkIfExistArRctReferenceNumber(receiptUploadDetails.payfileRefNum,user.getCmpCode())){
                                       //check if payfile exist
                                       System.out.println(ex.toString() + " payfile error here");
                                       receiptUploadDetails.isError =true;
                                       receiptUploadDetails.errorInformation = "Already Posted";
                                       System.out.println("Control No:" + receiptUploadDetails.controlNumber + " - " + EMPLOYEE_ID + " " + CUSTOMER_NAME + " Payfile Reference Number Already Posted");

                                       arReceipEntryUploadList.add(receiptUploadDetails);
                                   }else{

                                       if(actionForm.getEnableCreateAdvance()){




                                           int brnchCode = receiptUploadDetails.getCstAdBranch();
                                           if(brnchCode<=0){
                                                 //no list invoice found
                                                receiptUploadDetails.isError =true;
                                                receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Cannot create advance, No Branch assigned in " + EMPLOYEE_ID + " " + CUSTOMER_NAME ;
                                                System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Cannot create advance, No Branch assigned in: " + EMPLOYEE_ID + " " + CUSTOMER_NAME );
                                                arReceipEntryUploadList.add(receiptUploadDetails);
                                           }

                                            String status = ejbRCT.createAdvancePayment( receiptUploadDetails.details,CUSTOMER_CODE,receiptUploadDetails.getBankAccount(),receiptUploadDetails.payfileRefNum,brnchCode,user.getCmpCode());

                                            receiptUploadDetails.isError =true;
                                            receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + status ;
                                            System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + status );

                                            arReceipEntryUploadList.add(receiptUploadDetails);

                                       }else{
                                           //no list invoice found
                                           receiptUploadDetails.isError =true;
                                           receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "No List of Invoice Found in " + EMPLOYEE_ID + " " + CUSTOMER_NAME ;
                                           System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "no list of invoice found in: " + EMPLOYEE_ID + " " + CUSTOMER_NAME );
                                           arReceipEntryUploadList.add(receiptUploadDetails);

                                       }





                                   }



                                  continue;
                                  //saveErrors(request, new ActionMessages(errors));

                                } catch (EJBException ex) {
                                    receiptUploadDetails.isError =true;
                                    receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "System Error in control : " + EMPLOYEE_ID + " " + CUSTOMER_NAME ;
                                    arReceipEntryUploadList.add(receiptUploadDetails);
                                    System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "System Error in control : " + EMPLOYEE_ID + " " + CUSTOMER_NAME);

                                    if (log.isInfoEnabled()) {


                                        log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
                                                        " session: " + session.getId());


                                    }
                                    continue;
                                }





                                //check invoice if exist



                                Double amountRemaining = APPLY_AMOUNT;
                                String referenceNumber = "";
                                for(int cnt=0;cnt<customerInvLineDetails.size();cnt++){
                                   ArModInvoicePaymentScheduleDetails data_details = (ArModInvoicePaymentScheduleDetails)customerInvLineDetails.get(cnt);
                                   System.out.println(data_details.getIpsInvNumber());
                                   String tmpReferenceNumber = data_details.getIpsInvNumber();
                                   Double totalAmountDue = data_details.getIpsAmountDue() - data_details.getIpsAmountPaid();

                                   System.out.println("APPLY AMOUNT: " + APPLY_AMOUNT);
                                   System.out.println("TOTAL AMOUNT DUE: " + totalAmountDue);


                                   if(amountRemaining >= totalAmountDue){

                                       ArModAppliedInvoiceDetails appliedInvDetails = new ArModAppliedInvoiceDetails();
                                       appliedInvDetails.setAiIpsCode(data_details.getIpsCode());

                                       appliedInvDetails.setAiApplyAmount(Common.convertStringMoneyToDouble(totalAmountDue.toString(), precisionUnit));
                                       appliedInvDetails.setAiPenaltyApplyAmount(0.00);
                                       appliedInvDetails.setAiDiscountAmount(0.00);
                                       appliedInvDetails.setAiCreditBalancePaid(0.00);

                                       appliedInvDetails.setAiRebate(data_details.getIpsAiRebate());
                                       appliedInvDetails.setAiAllocatedPaymentAmount(0.00);
                                       appliedInvDetails.setAiAppliedDeposit(0.00);
                                       appliedInvDetails.setAiAdBranch(data_details.getIpsAdBranch());
                                       System.out.println("part 1  " + amountRemaining);
                                       amountRemaining -= totalAmountDue;

                                       System.out.println("A DUE: " + data_details.getIpsAmountDue());
                                       System.out.println("A PAID: " + data_details.getIpsAmountPaid());
                                       System.out.println("T A DUE: " + totalAmountDue);
                                       System.out.println("REM: " + amountRemaining);

                                       if(!referenceNumber.contains(tmpReferenceNumber)){
                                               referenceNumber = referenceNumber + " " + tmpReferenceNumber;
                                       }

                                       receiptUploadDetails.addInvoiceList(appliedInvDetails);
                                   }else{
                                        System.out.println("part 2  " + amountRemaining);
                                        System.out.println("A DUE: " + data_details.getIpsAmountDue());
                                        System.out.println("A PAID: " + data_details.getIpsAmountPaid());
                                        System.out.println("T A DUE: " + totalAmountDue);
                                        System.out.println("REM: " + amountRemaining);

                                        ArModAppliedInvoiceDetails appliedInvDetails = new ArModAppliedInvoiceDetails();
                                        appliedInvDetails.setAiIpsCode(data_details.getIpsCode());

                                        appliedInvDetails.setAiApplyAmount(Common.convertStringMoneyToDouble(amountRemaining.toString(), precisionUnit));
                                        appliedInvDetails.setAiPenaltyApplyAmount(0.00);
                                        appliedInvDetails.setAiDiscountAmount(0.00);
                                        appliedInvDetails.setAiCreditBalancePaid(0.00);

                                        appliedInvDetails.setAiRebate(data_details.getIpsAiRebate());
                                        appliedInvDetails.setAiAllocatedPaymentAmount(0.00);
                                        appliedInvDetails.setAiAppliedDeposit(0.00);
                                        appliedInvDetails.setAiAdBranch(data_details.getIpsAdBranch());

                                        amountRemaining = 0.00;
                                        receiptUploadDetails.addInvoiceList(appliedInvDetails);


                                        if(!referenceNumber.contains(tmpReferenceNumber)){
                                                referenceNumber = referenceNumber + " " + tmpReferenceNumber;
                                        }
                                         break;

                                   }


                                }
                                if(amountRemaining>0){
                                    receiptUploadDetails.isError =true;
                                    receiptUploadDetails.errorInformation = "Control No:" + receiptUploadDetails.controlNumber + " - Over apply amount  in " + EMPLOYEE_ID + " " + CUSTOMER_NAME;
                                    arReceipEntryUploadList.add(receiptUploadDetails);
                                    System.out.println( "Control No:" + receiptUploadDetails.controlNumber + " - Over apply amount  in  " + EMPLOYEE_ID + " " + CUSTOMER_NAME );
                                    continue;
                                }

                                receiptUploadDetails.details.setRctReferenceNumber(referenceNumber);


                                //check if have credit balance remaining

                                       double creditBalanceRemaining = ejbRCT.getArRctCreditBalanceByCstCustomerCode(CUSTOMER_CODE,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                                        if(creditBalanceRemaining>0){
                                                receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber +  " - " + EMPLOYEE_ID + " " + CUSTOMER_NAME + "  Credit Balance Remaining Exist:  " + creditBalanceRemaining;
                                        receiptUploadDetails.isError = true;
                                        arReceipEntryUploadList.add(receiptUploadDetails);
                                        System.out.println("Control No: " + receiptUploadDetails.controlNumber +  " - " + EMPLOYEE_ID + " " + CUSTOMER_NAME + "  Credit Balance Remaining Exist:  " + creditBalanceRemaining);
                                        continue;
                                        }



                                System.out.println(receiptUploadDetails.getInvoiceList().size() + " no of invoice checked");
                        arReceipEntryUploadList.add(receiptUploadDetails);


                        }



		    		 ArrayList errorLog = new ArrayList();
		    		 ArrayList csvRewriteData = new ArrayList();

		    		 System.out.println("ENTER RECEIPT TRANSACTION==============");
	    			 //entry into receipt
		    		 ArrayList payfileReferenceNumberList = new ArrayList();
		    		 String payfileReferenceNumber = "";
	    			 for(int cnt = 0;cnt<arReceipEntryUploadList.size();cnt++)
	    			 {

	    				ArReceiptEntryUploadList arReceiptUpload =  (ArReceiptEntryUploadList)arReceipEntryUploadList.get(cnt);
	    				payfileReferenceNumber = arReceiptUpload.details.getRctPayfileReferenceNumber();

	    				System.out.println("------->" + arReceiptUpload.isError);
	    				if(arReceiptUpload.isError){
	    					System.out.println("Writing Error");
	    					errorLog.add(arReceiptUpload.errorInformation);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+arReceiptUpload.errorInformation);
	    					}

	    					if(arReceiptUpload.errorInformation.contains("Already Posted")){
	    						NumberOfAlreadyPosted++;
	    						System.out.println("count posted");
	    					}else if(arReceiptUpload.errorInformation.contains("Already Created ADVANCE")){
	    						NumberOfAlreadyPosted++;


	    					}else if(arReceiptUpload.errorInformation.contains("Success Creating ADVANCE")){
	    						NumberOfSuccess++;


	    					}else{
	    						System.out.println("count error");
	    						NumberOfErrors++;
	    					}

	    					continue;
	    				}
	    				ArrayList invoiceList = arReceiptUpload.getInvoiceList();

	    			    ArModCustomerDetails cstDetails = ejbRCT.getArCstByCstCustomerCode(arReceiptUpload.getCustomerCode(), user.getCmpCode());
	    			    arReceiptUpload.details.setRctCustomerName(cstDetails.getCstName());
	    			    System.out.println("the name is " + cstDetails.getCstAdBaName());

	    			    arReceiptUpload.details.setRctCode(null);
	    			    arReceiptUpload.details.setRctNumber(null);

	    			    arReceiptUpload.details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	    			    arReceiptUpload.details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

	    			    arReceiptUpload.details.setRctCreatedBy(user.getUserName());
	    			    arReceiptUpload.details.setRctDateCreated(new java.util.Date());

	    			    arReceiptUpload.details.setRctLastModifiedBy(user.getUserName());
	    			    arReceiptUpload.details.setRctDateLastModified(new java.util.Date());

                                    try{
                                            //validating
                                            System.out.println("Validating size paylist " + payfileReferenceNumberList.size());



                                            if(arReceiptUpload.branchCodes.size()>1){
                                                    System.out.print("One or More branch involve");




                                                    for(int x=0;x<arReceiptUpload.branchCodes.size();x++){
                                                            int brnchCode = Integer.parseInt(arReceiptUpload.branchCodes.get(x).toString());


                                                            ArrayList brnchInvoiceList = new ArrayList();
                                                            arReceiptUpload.details.setRctPayfileReferenceNumber(arReceiptUpload.getPayfileRefNum() + " " + brnchCode);

                                                            for(int rfrncCnt = 0;rfrncCnt<payfileReferenceNumberList.size();rfrncCnt++){
                                                                    String tmpPyflRfrncNum = payfileReferenceNumberList.get(rfrncCnt).toString();

                                                                    if(tmpPyflRfrncNum.equals(arReceiptUpload.details.getRctPayfileReferenceNumber())){

                                                                            throw new ArREDuplicatePayfileReferenceNumberException();

                                                                    }

                                                            }



                                                            for(int invCnt=0;invCnt<invoiceList.size();invCnt++){
                                                                    ArModAppliedInvoiceDetails appliedInvDetails = (ArModAppliedInvoiceDetails)invoiceList.get(invCnt);
                                                                    if(appliedInvDetails.getAiAdBranch() == brnchCode){
                                                                            brnchInvoiceList.add(appliedInvDetails);
                                                                    }
                                                            }




                                                            ejbRCT.saveArRctEntry(arReceiptUpload.details, actionForm.getRecalculateJournal(), arReceiptUpload.bankAccount,
                                                                            arReceiptUpload.currency,
                                                                            arReceiptUpload.getCustomerCode(), arReceiptUpload.batchName,
                                                                            brnchInvoiceList, false,false, "", brnchCode, user.getCmpCode());


                                                    }



                                            }else{

                                                    System.out.print("One branch only");

                                                    int brnchCode = Integer.parseInt(arReceiptUpload.branchCodes.get(0).toString());
                                                    arReceiptUpload.details.setRctPayfileReferenceNumber(arReceiptUpload.getPayfileRefNum()+ " " + brnchCode);

                                                    System.out.println(arReceiptUpload.details.getRctPayfileReferenceNumber() + " pyref");

                                                    for(int rfrncCnt = 0;rfrncCnt<payfileReferenceNumberList.size();rfrncCnt++){
                                                            String tmpPyflRfrncNum = payfileReferenceNumberList.get(rfrncCnt).toString();
                                                            System.out.print(tmpPyflRfrncNum + " and " + arReceiptUpload.details.getRctPayfileReferenceNumber());
                                                            if(tmpPyflRfrncNum.equals(arReceiptUpload.details.getRctPayfileReferenceNumber())){
                                                                    System.out.println("found dupli");
                                                                    throw new ArREDuplicatePayfileReferenceNumberException();

                                                            }

                                                    }


                                                    System.out.println("o1");


                                                    ejbRCT.saveArRctEntry(arReceiptUpload.details,actionForm.getRecalculateJournal(),  arReceiptUpload.bankAccount,
                                                                    arReceiptUpload.currency,
                                                                    arReceiptUpload.getCustomerCode(), arReceiptUpload.batchName,
                                                                    invoiceList, false,false,"" , brnchCode, user.getCmpCode());
                                                    //'true' if you want validating
                                            }

                                            for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ",SUCCESS");
                                    }
                                            NumberOfSuccess++;


                                            payfileReferenceNumberList.add(arReceiptUpload.details.getRctPayfileReferenceNumber());

                                            System.out.println("size : " + payfileReferenceNumberList.size());


                                    }catch (ArReceiptEntryValidationException ex){

                                            for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ",SUCCESS");
                                    }
                                            NumberOfSuccess++;


                                            payfileReferenceNumberList.add(arReceiptUpload.details.getRctPayfileReferenceNumber());

                                            System.out.println("size : " + payfileReferenceNumberList.size());


                                    }catch (GlobalRecordAlreadyDeletedException ex) {

                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " Record Already Deleted";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    NumberOfErrors++;
                                    continue;



                                    } catch (GlobalDocumentNumberNotUniqueException ex) {

                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.getControlNumber() + " - " + arReceiptUpload.getEmployeeId() + " Document Number/Receipt Number already Exist";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    NumberOfErrors++;
                                    continue;

                                    } catch (ArREDuplicatePayfileReferenceNumberException ex) {

                                            System.out.println(ex.toString() + " payfile error");


                                            String msgErr ="Already Posted";
                                    //	errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    NumberOfAlreadyPosted++;
                                    continue;

                                    } catch (GlobalConversionDateNotExistException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Conversion Date not Exist";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;

                                    } catch (GlobalTransactionAlreadyApprovedException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Transaction Already Approved";

                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlobalTransactionAlreadyPendingException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Transaction Already Pending";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlobalTransactionAlreadyPostedException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Transaction Already Posted";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;

                                    } catch (GlobalTransactionAlreadyVoidException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Transaction Already Void";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (ArINVOverapplicationNotAllowedException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Overapplication Not Allowed";

                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;

                                    } catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Insufficient Credit Balance";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;



                                    } catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - "+ arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Invoice has no WTax Code";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;

                                    } catch (GlobalTransactionAlreadyLockedException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Transaction already locked";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlobalNoApprovalRequesterFoundException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " No approval requested found";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlobalNoApprovalApproverFoundException ex) {
                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " No approval approve found";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;

                                    } catch (GlJREffectiveDateNoPeriodExistException ex) {
                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Effective Period Date not Exist";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlJREffectiveDatePeriodClosedException ex) {
                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Effective Date Period is Closed";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlobalJournalNotBalanceException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Journal Not Balance";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlobalBranchAccountNumberInvalidException ex) {

                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " Branch Account Number Invalid";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (GlobalRecordAlreadyAssignedException ex) {
                                            NumberOfErrors++;
                                            System.out.println("whats this?");
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + "Record already Assigned";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;


                                    } catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {
                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " No Customer deposit in COA";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;




                                    }catch(Exception ex){

                                            if (log.isInfoEnabled()) {

                                            log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
                                                            " session: " + session.getId());
                                            System.out.println(ex.toString());


                                    }
                                            NumberOfErrors++;
                                            System.out.println(ex.toString());
                                            String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + arReceiptUpload.getEmployeeId() + " " + arReceiptUpload.getCustomerName() + " " +  "System Error, Check server log for details";
                                            errorLog.add(msgErr);
                                    for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
                                            csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
                                    }
                                    continue;

                             }

                     }

                    //saving  if cannot submit if theres an error one of them

                     if(NumberOfErrors<=0){


                       System.out.println("SAVING . . . . ");
                            for(int cnt = 0;cnt<arReceipEntryUploadList.size();cnt++)
                            {
                               System.out.println(arReceipEntryUploadList.size() + "size");
                               ArReceiptEntryUploadList arReceiptUpload =  (ArReceiptEntryUploadList)arReceipEntryUploadList.get(cnt);


                               ArrayList invoiceList = arReceiptUpload.getInvoiceList();

                               ArModCustomerDetails cstDetails = ejbRCT.getArCstByCstCustomerCode(arReceiptUpload.getCustomerCode(), user.getCmpCode());
                               arReceiptUpload.details.setRctCustomerName(cstDetails.getCstName());
                               System.out.println("the name is " + cstDetails.getCstAdBaName());

                               arReceiptUpload.details.setRctCode(null);

                               arReceiptUpload.details.setRctNumber(null);
                               arReceiptUpload.details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                               arReceiptUpload.details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

                               arReceiptUpload.details.setRctCreatedBy(user.getUserName());
                               arReceiptUpload.details.setRctDateCreated(new java.util.Date());

                               arReceiptUpload.details.setRctLastModifiedBy(user.getUserName());
                               arReceiptUpload.details.setRctDateLastModified(new java.util.Date());

                                try{

                                    if(arReceiptUpload.branchCodes.size()>1){
                                        System.out.print("One or More branch involve");

                                        for(int x=0;x<arReceiptUpload.branchCodes.size();x++){
                                            int brnchCode = Integer.parseInt(arReceiptUpload.branchCodes.get(x).toString());

                                            ArrayList brnchInvoiceList = new ArrayList();
                                            for(int invCnt=0;invCnt<invoiceList.size();invCnt++){
                                                ArModAppliedInvoiceDetails appliedInvDetails = (ArModAppliedInvoiceDetails)invoiceList.get(invCnt);
                                                if(appliedInvDetails.getAiAdBranch() == brnchCode){
                                                        brnchInvoiceList.add(appliedInvDetails);
                                                }
                                            }


                                            ejbRCT.saveArRctEntry(arReceiptUpload.details, actionForm.getRecalculateJournal(), arReceiptUpload.bankAccount,
                                                            arReceiptUpload.currency,
                                                            arReceiptUpload.getCustomerCode(), arReceiptUpload.batchName,
                                                            brnchInvoiceList, false,false,"", brnchCode, user.getCmpCode());
                                        }



                                    }else{
                                            int brnchCode = Integer.parseInt(arReceiptUpload.branchCodes.get(0).toString());
                                            String payfileReferenceNo = arReceiptUpload.details.getRctPayfileReferenceNumber();

                                            ejbRCT.saveArRctEntry(arReceiptUpload.details,actionForm.getRecalculateJournal(),  arReceiptUpload.bankAccount,
                                                            arReceiptUpload.currency,
                                                            arReceiptUpload.getCustomerCode(), arReceiptUpload.batchName,
                                                            invoiceList, false,false,"", brnchCode, user.getCmpCode());

                                    }

                                }catch (Exception ex) {
                                    System.out.println("error saving");
                                    System.out.println("error saving");
                                    if (log.isInfoEnabled()) {

                                    log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
                                                    " session: " + session.getId());
                                    System.out.println(ex.toString());


                                    }
                                }
                        }

                    }




                    try {
                   //Whatever the file path is.
                    System.out.println("create file");

                           File dir = new File("c:/opt/RECEIPT ENTRY LOG");

                           if(!dir.exists()){
                                   new File("c:/opt/RECEIPT ENTRY LOG").mkdir();
                           }


                           Date dtNew = new Date();

                           SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd hh-mm-ss");


                           String fileName = "R ENTRY " + dt.format(dtNew) + ".csv";
                            File statText = new File("c:/opt/RECEIPT ENTRY LOG/" + fileName);
                            FileOutputStream is = new FileOutputStream(statText);
                            OutputStreamWriter osw = new OutputStreamWriter(is);
                            Writer w = new BufferedWriter(osw);
                            w.write(ArReceiptEntryUploadList.columns2);
                            w.write("\n");
                            for(int x=0;x<csvRewriteData.size();x++){

                            w.write("#" + csvRewriteData.get(x).toString());
                            w.write("\n");
                       }
                       w.close();

                       System.out.println("create file end");
                    } catch (IOException e) {
                       System.out.println("Problem writing to the file statsTest.txt  " + e.toString());
                    }

                    /*
                    if(NumberOfErrors>0){
                            messages.add(ActionMessages.GLOBAL_MESSAGE,
                                                   new ActionMessage("receiptEntry.prompt.uploadReceipt", "UPLOADING FAILED"));

                    }else{
                            messages.add(ActionMessages.GLOBAL_MESSAGE,
                                                           new ActionMessage("receiptEntry.prompt.uploadReceipt", "UPLOADING SUCCESS"));
                    }*/
                    messages.add(ActionMessages.GLOBAL_MESSAGE,
                                                   new ActionMessage("receiptEntry.prompt.uploadReceipt", "UPLOADING SUCCESS"));
                    messages.add(ActionMessages.GLOBAL_MESSAGE,
                                           new ActionMessage("receiptEntry.prompt.uploadReceipt", "SUCCESS: " + NumberOfSuccess + "  FAILED: " + NumberOfErrors + "  ALREADY POSTED: " + NumberOfAlreadyPosted));

                    for(int x=0;x<errorLog.size();x++){

                           if(errorLog.get(x).toString().contains("Already Posted")){
                                   continue;
                           }
                            messages.add(ActionMessages.GLOBAL_MESSAGE,
                                                   new ActionMessage("receiptEntry.prompt.uploadReceipt", errorLog.get(x).toString()));

                    }
                    System.out.println("write error logs" + messages.size() + " no of errors:" + NumberOfErrors);

                    saveMessages(request, messages);




				/*******************************************************
  -- Ar RCT Upload Receipt File --
				 *******************************************************/

			}else if(request.getParameter("saveUploadButton") != null &&
		    		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


				 if(actionForm.getUploadFile().getFileSize() == 0){

			       		errors.add(ActionMessages.GLOBAL_MESSAGE,
			   				 new ActionMessage("bankReconciliation.error.filenotfound"));

			       		saveErrors(request, new ActionMessages(errors));
			       		return mapping.findForward("cmBankReconciliation");
			     }




		    	 boolean noCustomerError = false;
		    	 boolean noNoInvoiceError = false;
		    	 boolean noInvoiceList = false;
		    	 ArrayList transactionDetails = new ArrayList();
		    	 ArrayList statusDetails = new ArrayList();




		    		 CSVParser csvParser = new CSVParser(actionForm.getUploadFile().getInputStream());
		    		 String[][] values = csvParser.getAllValues();


		    		 boolean duplicateReceipt  = false;


		    		 String customer_code = "";
		    		 String date_inv = "";
		    		 int numOferrPerTrans = 0;
		    		 String[] data_value = new String[9];

		    		 boolean new_transaction = false;
		    		 String control_number = "";
		    		 boolean noCustomerFound = false;
		    		 ArrayList arReceipEntryUploadList = new ArrayList();

		    		 String transactionErrorLog = "";
		    		 ArrayList customerInvLineDetails = new ArrayList();

		    		 ArReceiptEntryUploadList receiptUploadDetails = new ArReceiptEntryUploadList(precisionUnit);

		    		 double applyAmount = 0.00;

		    		 for(int x=1;x<values.length;x++){

		    			 if( values[x][0].equals(control_number)){
		    				 receiptUploadDetails.addCsvDataList(values[x],false);

		    				 if(receiptUploadDetails.isError){
		    					 continue;
		    				 }
		    				//check invoice if exist
	    					 String invoiceNumber = values[x][14];
	    					 String invoicePaymentScheduleNumber = values[x][15];


	    					 boolean isInvoiceFound = false;
	    					 for(int cnt=0;cnt<customerInvLineDetails.size();cnt++){
								ArModInvoicePaymentScheduleDetails data_details = (ArModInvoicePaymentScheduleDetails)customerInvLineDetails.get(cnt);
								System.out.println(data_details.getIpsInvNumber() + " and ---------" + invoiceNumber);
								System.out.println(Short.toString(data_details.getIpsNumber()) + " and ---------" + invoicePaymentScheduleNumber);


								if(data_details.getIpsInvNumber().equals(invoiceNumber) && Short.toString(data_details.getIpsNumber()).equals(invoicePaymentScheduleNumber)){
									//if invoice found match in the file
									isInvoiceFound = true;

									ArModAppliedInvoiceDetails appliedInvDetails = new ArModAppliedInvoiceDetails();
									appliedInvDetails.setAiIpsCode(data_details.getIpsCode());

									appliedInvDetails.setAiApplyAmount(Common.convertStringMoneyToDouble(values[x][16], precisionUnit));
									appliedInvDetails.setAiPenaltyApplyAmount(Common.convertStringMoneyToDouble(values[x][17],precisionUnit));
									appliedInvDetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(values[x][18],precisionUnit));
									appliedInvDetails.setAiCreditBalancePaid(Common.convertStringMoneyToDouble(values[x][19],precisionUnit));

									appliedInvDetails.setAiRebate(data_details.getIpsAiRebate());
									appliedInvDetails.setAiAllocatedPaymentAmount(0.00);
									appliedInvDetails.setAiAppliedDeposit(0.00);

									applyAmount+=Common.convertStringMoneyToDouble(values[x][16], precisionUnit);
									receiptUploadDetails.addInvoiceList(appliedInvDetails);
									break;
								}
							 }
	    					 if(!isInvoiceFound){
	    						 receiptUploadDetails.isError =true;
	    						 receiptUploadDetails.errorInformation = "Control No:" + receiptUploadDetails.controlNumber + " - " + " No invoice: " +invoiceNumber + " or IPS No: " + invoicePaymentScheduleNumber + "  found in Customer Code : " + receiptUploadDetails.customerCode + " Control No: " + receiptUploadDetails.controlNumber;
	    						 System.out.println("Control No:" + receiptUploadDetails.controlNumber + " - " + "No invoice: " + invoiceNumber+ " or IPS No: " + invoicePaymentScheduleNumber + " found in Customer Code : " + receiptUploadDetails.customerCode + " Control No: " + receiptUploadDetails.controlNumber);
			    				 continue;
	    					 }

		    			 }else{
		    				 if(new_transaction){
		    					 receiptUploadDetails.details.setRctAmount(applyAmount);
		    					 arReceipEntryUploadList.add(receiptUploadDetails);
		    				 }

		    				 new_transaction = true;
		    				 String invoiceNumber = values[x][14];
		    				 String invoicePaymentScheduleNumber = values[x][15];
		    				 receiptUploadDetails = new ArReceiptEntryUploadList(precisionUnit);
		    				 receiptUploadDetails.addCsvDataList(values[x],true);

		    			 	 if( actionForm.getShowBatchName()){
		    					receiptUploadDetails.setBatchName( values[x][3]);
		    				 }

		    				 control_number = receiptUploadDetails.getControlNumber();
		    				 applyAmount = 0;

		    				 customerInvLineDetails = new ArrayList();

	    					 boolean ifUniqueIdExist = false;
	    					 boolean ifBankAccountExist = false;
	    					 boolean ifPaymentMethodExist = false;
	    					 boolean ifCurrencyExist = false;
	    					 boolean ifBatchNameExist = false;

	    					 //check unique Id  if exist

	    					 if(actionForm.getUniqueIdCode().equals("Employee ID")){
	    						 //EMPLOYEE ID

	    						 String empId_cd = receiptUploadDetails.getCustomerCode();
	    						 try{
	    							 System.out.println("emp id : " + empId_cd);
	    							 String customerCode = ejbRCT.getArCstCstmrCodeByEmplyId(empId_cd, user.getCmpCode());

	    							 receiptUploadDetails.setCustomerCode(customerCode);
	    							 ifUniqueIdExist = true;
	    						 }catch(GlobalNoRecordFoundException gx){
	    							 System.out.println("not found");
	    							 ifUniqueIdExist = false;
	    						 }

	    						 if(!ifUniqueIdExist){

		    						 receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Employee ID " + empId_cd + " not exist";
		    						 receiptUploadDetails.isError = true;
		    						 System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Employee ID " + empId_cd + " not exist");
		    						 continue;
		    					 }

	    					 }else{

	    						 //CUSTOMER CODE
	    						 for(int cnt=1;cnt<actionForm.getCustomerList().size();cnt++){
		    						 String cst_cd = actionForm.getCustomerList().get(cnt).toString();
		    						 if(receiptUploadDetails.getCustomerCode().equals(cst_cd)){
		    							 ifUniqueIdExist = true;
		    						 }
		    					 }

		    					 if(!ifUniqueIdExist){

		    						 receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Customer " + receiptUploadDetails.getCustomerCode() + " not exist";
		    						 receiptUploadDetails.isError = true;
		    						 System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Customer " + receiptUploadDetails.getCustomerCode() + " not exist");
		    						 continue;
		    					 }
	    					 }




	    					 //check Bank Account exist
	    					 for(int cnt=1;cnt<actionForm.getBankAccountList().size();cnt++){
	    						 String bnk_cd = actionForm.getBankAccountList().get(cnt).toString();
	    						 if(receiptUploadDetails.getBankAccount().equals(bnk_cd)){
	    							 ifBankAccountExist = true;
	    						 }
	    					 }

	    					 if(!ifBankAccountExist){

	    						 receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Bank Account " + receiptUploadDetails.bankAccount + " not exist";
	    						 receiptUploadDetails.isError = true;
	    						 System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Bank Account " + receiptUploadDetails.bankAccount + " not exist");
	    						 continue;
	    					 }



	    					 //check Payment Method exist
	    					 for(int cnt=0;cnt<actionForm.getPaymentMethodList().size();cnt++){
	    						 String pym_cd = actionForm.getPaymentMethodList().get(cnt).toString();
	    						 if(receiptUploadDetails.details.getRctPaymentMethod().equals(pym_cd)){
	    							 ifPaymentMethodExist = true;
	    						 }
	    					 }

	    					 if(!ifPaymentMethodExist){

	    						 receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Payment Method " + receiptUploadDetails.details.getRctPaymentMethod() + " not exist";
	    						 receiptUploadDetails.isError = true;
	    						 System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Payment Method " + receiptUploadDetails.details.getRctPaymentMethod() + " not exist");
	    						 continue;
	    					 }


	    					//check currency exist
	    					 for(int cnt=0;cnt<actionForm.getCurrencyList().size();cnt++){
	    						 String curr_cd = actionForm.getCurrencyList().get(cnt).toString();
	    						 if(receiptUploadDetails.currency.equals(curr_cd)){
	    							 ifCurrencyExist = true;
	    						 }
	    					 }

	    					 if(!ifCurrencyExist){

	    						 receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Currency " + receiptUploadDetails.currency+ " not exist";
	    						 receiptUploadDetails.isError = true;
	    						 System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Currency" + receiptUploadDetails.currency+ " not exist");
	    						 continue;
	    					 }

	    					 //check Batch Name
	    					 if(actionForm.getShowBatchName()){
	    						 for(int cnt=1;cnt<actionForm.getBatchNameList().size();cnt++){
		    						 String bcn_cd = actionForm.getBatchNameList().get(cnt).toString();
		    						 if(receiptUploadDetails.batchName.equals(bcn_cd)){
		    							 ifBatchNameExist = true;
		    						 }
		    					 }

		    					 if(!ifBatchNameExist){

		    						 receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Batch Name " + receiptUploadDetails.batchName+ " not exist";
		    						 receiptUploadDetails.isError = true;
		    						 System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Batch Name " + receiptUploadDetails.batchName+ " not exist");
		    						 continue;
		    					 }
	    					 }






	    					 //check receipt is already exist
	    					 ArModReceiptDetails rctDetails = null;
	    					 try{
	    						 rctDetails =  ejbRCT.getArRctByRctNum(receiptUploadDetails.details.getRctNumber(),user.getCurrentBranch().getBrCode(),user.getCmpCode());

	    						 //if receipt already exist
	    						 receiptUploadDetails.isError = true;
	    						 receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "Receipt Number " + receiptUploadDetails.details.getRctNumber() + " already exist";
	    						 System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "Receipt Number " + receiptUploadDetails.details.getRctNumber() + " already exist");
	    						 continue;
	    					 }catch(GlobalNoRecordFoundException gfx){
	    						 //unique receipt number
	    					 }






		    				 //get invoice of customer
	    					 try{
		    					ArrayList list = ejbRCT.getArIpsByCstCustomerCode(receiptUploadDetails.getCustomerCode(),Common.convertStringToSQLDate(actionForm.getDate()), false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		    					Iterator i = list.iterator();
	    						while (i.hasNext()) {
	    							ArModInvoicePaymentScheduleDetails mLinedetails = (ArModInvoicePaymentScheduleDetails)i.next();
	    							customerInvLineDetails.add(mLinedetails);

	    						}

	    					 }catch (GlobalNoRecordFoundException ex) {
		    					// no list invoice found

		    					receiptUploadDetails.isError =true;
		    					receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "No List of Invoice Found in " + receiptUploadDetails.getCustomerCode();
		    					System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "no list of invoice found in: " + receiptUploadDetails.getCustomerCode());

		    					continue;
		    					//saveErrors(request, new ActionMessages(errors));

	    					 } catch (EJBException ex) {
	    						 	receiptUploadDetails.isError =true;
			    					receiptUploadDetails.errorInformation = "Control No: " + receiptUploadDetails.controlNumber + " - " + "System Error in control : " + receiptUploadDetails.controlNumber;
			    					System.out.println("Control No: " + receiptUploadDetails.controlNumber + " - " + "System Error in control : " + receiptUploadDetails.controlNumber);

		    					if (log.isInfoEnabled()) {


		    						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
		    								" session: " + session.getId());


		    					}
		    					continue;
	    					 }

	    					 //check invoice if exist
	    					 boolean isInvoiceFound = false;
	    					 for(int cnt=0;cnt<customerInvLineDetails.size();cnt++){
								ArModInvoicePaymentScheduleDetails data_details = (ArModInvoicePaymentScheduleDetails)customerInvLineDetails.get(cnt);
								System.out.println(data_details.getIpsInvNumber() + " and ---------" + invoiceNumber);
								System.out.println(Short.toString(data_details.getIpsNumber()) + " and ---------" + invoicePaymentScheduleNumber);


								if(data_details.getIpsInvNumber().equals(invoiceNumber) && Short.toString(data_details.getIpsNumber()).equals(invoicePaymentScheduleNumber)){
									//if invoice found match in the file
									isInvoiceFound = true;

									ArModAppliedInvoiceDetails appliedInvDetails = new ArModAppliedInvoiceDetails();
									appliedInvDetails.setAiIpsCode(data_details.getIpsCode());

									appliedInvDetails.setAiApplyAmount(Common.convertStringMoneyToDouble(values[x][16], precisionUnit));
									appliedInvDetails.setAiPenaltyApplyAmount(Common.convertStringMoneyToDouble(values[x][17],precisionUnit));
									appliedInvDetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(values[x][18],precisionUnit));
									appliedInvDetails.setAiCreditBalancePaid(Common.convertStringMoneyToDouble(values[x][19],precisionUnit));

									appliedInvDetails.setAiRebate(data_details.getIpsAiRebate());
									appliedInvDetails.setAiAllocatedPaymentAmount(0.00);
									appliedInvDetails.setAiAppliedDeposit(0.00);

									applyAmount+=Common.convertStringMoneyToDouble(values[x][16], precisionUnit);
									receiptUploadDetails.addInvoiceList(appliedInvDetails);
									break;
								}
							 }
	    					 if(!isInvoiceFound){
	    						 receiptUploadDetails.isError =true;
	    						 receiptUploadDetails.errorInformation = "Control No:" + receiptUploadDetails.controlNumber + " - " + " No invoice: " +invoiceNumber + " or IPS No: " + invoicePaymentScheduleNumber + "  found in Customer Code : " + receiptUploadDetails.customerCode + " Control No: " + receiptUploadDetails.controlNumber;
	    						 System.out.println("Control No:" + receiptUploadDetails.controlNumber + " - " + "No invoice: " + invoiceNumber+ " or IPS No: " + invoicePaymentScheduleNumber + " found in Customer Code : " + receiptUploadDetails.customerCode + " Control No: " + receiptUploadDetails.controlNumber);
			    				 continue;
	    					 }

	    					 //check advance payment
	    					 if(values[x][9].equals("1") || values[x][9].equals("true")){

    							double excessAmount = Common.convertStringMoneyToDouble(values[x][10],precisionUnit) - applyAmount ;

    							if(excessAmount < 0){

									receiptUploadDetails.isError =true;
		    						 receiptUploadDetails.errorInformation ="Control No: " + receiptUploadDetails.controlNumber + " - " + "Amount Paid less thatn the amount in Customer Code : " + receiptUploadDetails.customerCode + " Control No: " + receiptUploadDetails.controlNumber;
		    						 System.out.println("Control No:" + receiptUploadDetails.controlNumber + " - " + "Amount Paid less thatn the amount in Customer Code : " + receiptUploadDetails.customerCode + " Control No: " + receiptUploadDetails.controlNumber);
				    				 continue;



    							}

    							receiptUploadDetails.details.setRctEnableAdvancePayment(Common.convertBooleanToByte(true));

    							if(excessAmount == 0){

    								receiptUploadDetails.details.setRctEnableAdvancePayment(Common.convertBooleanToByte(false));
    							}

    							receiptUploadDetails.details.setRctExcessAmount(excessAmount);
    						}


		    			 }
		    		 }
		    		 if(new_transaction){
		    			 receiptUploadDetails.details.setRctAmount(applyAmount);
		    			 arReceipEntryUploadList.add(receiptUploadDetails);
    				 }



		    		 int NumberOfSuccess = 0;
		    		 int NumberOfErrors = 0;
		    		 ArrayList errorLog = new ArrayList();
		    		 ArrayList csvRewriteData = new ArrayList();

		    		 System.out.println("ENTER RECEIPT TRANSACTION==============");
	    			 //entry into receipt

	    			 for(int cnt = 0;cnt<arReceipEntryUploadList.size();cnt++)
	    			 {

	    				ArReceiptEntryUploadList arReceiptUpload =  (ArReceiptEntryUploadList)arReceipEntryUploadList.get(cnt);

	    				System.out.println("------->" + arReceiptUpload.isError);
	    				if(arReceiptUpload.isError){
	    					System.out.println("Writing Error");
	    					errorLog.add(arReceiptUpload.errorInformation);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+arReceiptUpload.errorInformation);
	    					}
	    					NumberOfErrors++;
	    					continue;
	    				}
	    				ArrayList invoiceList = arReceiptUpload.getInvoiceList();

	    			    ArModCustomerDetails cstDetails = ejbRCT.getArCstByCstCustomerCode(arReceiptUpload.getCustomerCode(), user.getCmpCode());
	    			    arReceiptUpload.details.setRctCustomerName(cstDetails.getCstName());
	    			    System.out.println("the name is " + cstDetails.getCstAdBaName());

	    			    arReceiptUpload.details.setRctCode(null);


	    			    arReceiptUpload.details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	    			    arReceiptUpload.details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

	    			    arReceiptUpload.details.setRctCreatedBy(user.getUserName());
	    			    arReceiptUpload.details.setRctDateCreated(new java.util.Date());

	    			    arReceiptUpload.details.setRctLastModifiedBy(user.getUserName());
	    			    arReceiptUpload.details.setRctDateLastModified(new java.util.Date());

						try{
							ejbRCT.saveArRctEntry(arReceiptUpload.details, actionForm.getRecalculateJournal(), arReceiptUpload.bankAccount,
									arReceiptUpload.currency,
									arReceiptUpload.getCustomerCode(), arReceiptUpload.batchName,
									invoiceList, true,false,"", new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

							for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ",SUCCESS");
	    					}
							NumberOfSuccess++;


						}catch (GlobalRecordAlreadyDeletedException ex) {

							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.recordAlreadyDeleted").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					NumberOfErrors++;
	    					continue;



						} catch (GlobalDocumentNumberNotUniqueException ex) {

							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.documentNumberNotUnique").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					NumberOfErrors++;
	    					continue;

						} catch (GlobalConversionDateNotExistException ex) {

							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.conversionDateNotExist").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;

						} catch (GlobalTransactionAlreadyApprovedException ex) {

							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.transactionAlreadyApproved").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlobalTransactionAlreadyPendingException ex) {

							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.transactionAlreadyPending").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlobalTransactionAlreadyPostedException ex) {


							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.transactionAlreadyPosted").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;

						} catch (GlobalTransactionAlreadyVoidException ex) {

							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.transactionAlreadyVoid").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (ArINVOverapplicationNotAllowedException ex) {

							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.overapplicationNotAllowed").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;

						} catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.insufficientCreditBalance").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;



						} catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.invoiceHasNoWTaxCode").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;

						} catch (GlobalTransactionAlreadyLockedException ex) {

							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.transactionAlreadyLocked").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlobalNoApprovalRequesterFoundException ex) {


							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.noApprovalRequesterFound").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlobalNoApprovalApproverFoundException ex) {

							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.noApprovalApproverFound").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;

						} catch (GlJREffectiveDateNoPeriodExistException ex) {

							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.effectiveDateNoPeriodExist").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlJREffectiveDatePeriodClosedException ex) {

							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.effectiveDatePeriodClosed").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlobalJournalNotBalanceException ex) {


							System.out.println(ex.toString());
							String msgErr ="Control No:" + arReceiptUpload.controlNumber + " - " +  new ActionMessage("receiptEntry.error.journalNotBalance").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlobalBranchAccountNumberInvalidException ex) {


							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.branchAccountNumberInvalid").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (GlobalRecordAlreadyAssignedException ex) {
							System.out.println("whats this?");
							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.recordAlreadyAssigned").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;


						} catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

							System.out.println(ex.toString());
							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " + new ActionMessage("receiptEntry.error.noCustomerDepositCOA").getValues().toString();
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;




						}catch(Exception ex){
							if (log.isInfoEnabled()) {

	    						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
	    								" session: " + session.getId());
	    						System.out.println(ex.toString());


	    					}


							String msgErr = "Control No:" + arReceiptUpload.controlNumber + " - " +  "System Error, Check server log for details";
							errorLog.add(msgErr);
	    					for(int x=0;x<arReceiptUpload.csvDataList.size();x++){
	    						csvRewriteData.add(arReceiptUpload.csvDataList.get(x).toString() + ","+msgErr);
	    					}
	    					continue;

				    	 }

	    			 }


	    			 try {
		    	        //Whatever the file path is.
		    		 System.out.println("create file");

	    				 File statText = new File("c:/opt/ReceiptEntryStatus.csv");
	    		            FileOutputStream is = new FileOutputStream(statText);
	    		            OutputStreamWriter osw = new OutputStreamWriter(is);
	    		            Writer w = new BufferedWriter(osw);
	    		            w.write(ArReceiptEntryUploadList.columns1);
	    		            w.write("\n");
	    		            for(int x=0;x<csvRewriteData.size();x++){

		    	            	 w.write(csvRewriteData.get(x).toString());
		    	            	 w.write("\n");
		    	            }
	    		            w.close();

	    		     		System.out.println("create file end");
		    		 } catch (IOException e) {
	    	            System.out.println("Problem writing to the file statsTest.txt  " + e.toString());
		    		 }



	    			 messages.add(ActionMessages.GLOBAL_MESSAGE,
	 						new ActionMessage("receiptEntry.prompt.uploadReceipt", "SUCCESS: " + NumberOfSuccess + "  FAILED: " + NumberOfErrors));

	    			 for(int x=0;x<errorLog.size();x++){
	    				 messages.add(ActionMessages.GLOBAL_MESSAGE,
	 	 						new ActionMessage("receiptEntry.prompt.uploadReceipt", errorLog.get(x).toString()));

	    			 }


	    			 saveMessages(request, messages);


/*******************************************************
	 -- Ar RCT Journal Action --
*******************************************************/

			} else if (request.getParameter("journalButton") != null) {

				if(Common.validateRequired(actionForm.getApprovalStatus())) {

					ArReceiptDetails details = new ArReceiptDetails();

					details.setRctCode(actionForm.getReceiptCode());
					details.setRctDate(Common.convertStringToSQLDate(actionForm.getDate()));
					details.setRctDocumentType(actionForm.getDocumentType());
					details.setRctNumber(actionForm.getReceiptNumber());
					details.setRctReferenceNumber(actionForm.getReferenceNumber());
					details.setRctCheckNo(actionForm.getCheckNo());
					details.setRctAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
					details.setRctVoid(Common.convertBooleanToByte(actionForm.getReceiptVoid()));
					details.setRctDescription(actionForm.getDescription());
					details.setRctConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
					details.setRctConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));
					details.setRctPaymentMethod(actionForm.getPaymentMethod());
					details.setRctCustomerName(actionForm.getCustomerName());
					details.setRctEnableAdvancePayment(Common.convertBooleanToByte(actionForm.getEnableAdvancePayment()));

					if(actionForm.getEnableAdvancePayment()){

						double excessAmount = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(),precisionUnit) - details.getRctAmount() ;

						if(excessAmount < 0){
							errors.add(ActionMessages.GLOBAL_MESSAGE,
				           			new ActionMessage("receiptEntry.error.amountPaidLessThanAmount"));

						}


						if(excessAmount == 0){

							details.setRctEnableAdvancePayment(Common.convertBooleanToByte(false));
						}

						details.setRctExcessAmount(excessAmount);
					}

					if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arReceiptEntry"));

		   	    	}

					if (actionForm.getReceiptCode() == null) {

						details.setRctCreatedBy(user.getUserName());
						details.setRctDateCreated(new java.util.Date());

					}

					details.setRctLastModifiedBy(user.getUserName());
					details.setRctDateLastModified(new java.util.Date());


					double totalCreditBalancePaid = 0d;

					ArrayList ipsList = new ArrayList();

					for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

						ArReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

						if (!arRCTList.getPayCheckbox()) continue;

						ArModAppliedInvoiceDetails mdetails = new ArModAppliedInvoiceDetails();

						mdetails.setAiIpsCode(arRCTList.getInvoicePaymentScheduleCode());
						mdetails.setAiApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit));
						mdetails.setAiPenaltyApplyAmount(Common.convertStringMoneyToDouble(arRCTList.getPenaltyApplyAmount(), precisionUnit));
						mdetails.setAiCreditableWTax(Common.convertStringMoneyToDouble(arRCTList.getCreditableWTax(), precisionUnit));
						mdetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(arRCTList.getDiscount(), precisionUnit));
						mdetails.setAiAllocatedPaymentAmount(Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit));
						mdetails.setAiAppliedDeposit(Common.convertStringMoneyToDouble(arRCTList.getAppliedDeposit(), precisionUnit));
						mdetails.setAiCreditBalancePaid(Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit));

						mdetails.setAiRebate(arRCTList.getRebateCheckbox() == true ? Common.convertStringMoneyToDouble(arRCTList.getRebate(), precisionUnit) : 0d  );
						mdetails.setAiApplyRebate(mdetails.getAiRebate() > 0 ? Common.convertBooleanToByte(arRCTList.getRebateCheckbox()) : (byte)0);


						totalCreditBalancePaid += Common.convertStringMoneyToDouble(arRCTList.getCreditBalancePaid(), precisionUnit);
						ipsList.add(mdetails);

					}



					if(actionForm.getEnableAdvancePayment()){
						double amount = Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit);

						double amountPaid = Common.convertStringMoneyToDouble(actionForm.getTotalAmountPaid(), precisionUnit);


						if(amountPaid<amount){
							errors.add(ActionMessages.GLOBAL_MESSAGE,
				           			new ActionMessage("invoiceEntry.error.filename1NotFound"));
						}
					}

					if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("arReceiptEntry"));

		   	    	}

//	               validate attachment

	                 String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	                 String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	                 String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	                 String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	                 if (!Common.validateRequired(filename1)) {

	 		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename1NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

	 		          	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename1Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename1().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename1SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   }

	 		   	   if (!Common.validateRequired(filename2)) {

	 		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename2NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

	 		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		          	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename2Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename2().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename2SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   }

	 		   	   if (!Common.validateRequired(filename3)) {

	 		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename3NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

	 		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename3Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename3().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename3SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   }

	 		   	   if (!Common.validateRequired(filename4)) {

	 		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	 		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		           			new ActionMessage("invoiceEntry.error.filename4NotFound"));

	 		   	    	} else {

	 		   	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

	 		   	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
	 		   	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename4Invalid"));

	 		          	    	}

	 		          	    	InputStream is = actionForm.getFilename4().getInputStream();

	 		          	    	if (is.available() > maxAttachmentFileSize) {

	 		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	 		                   		new ActionMessage("invoiceEntry.error.filename4SizeInvalid"));

	 		          	    	}

	 		          	    	is.close();

	 		   	    	}

	 		   	    	if (!errors.isEmpty()) {

	 		   	    		saveErrors(request, new ActionMessages(errors));
	 		          			return (mapping.findForward("arInvoiceEntry"));

	 		   	    	}

	 		   	   	}

					try {

						Integer receiptCode = ejbRCT.saveArRctEntry(details,actionForm.getRecalculateJournal(),  actionForm.getBankAccount(),
								actionForm.getCurrency(),
								actionForm.getCustomer(), actionForm.getBatchName(),
								ipsList, true,false, actionForm.getPayrollPeriodName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						actionForm.setReceiptCode(receiptCode);

					} catch (GlobalRecordAlreadyDeletedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.recordAlreadyDeleted"));

					} catch (GlobalDocumentNumberNotUniqueException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.documentNumberNotUnique"));

					} catch (GlobalConversionDateNotExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.conversionDateNotExist"));

					} catch (GlobalTransactionAlreadyApprovedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyApproved"));

					} catch (GlobalTransactionAlreadyPendingException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyPending"));

					} catch (GlobalTransactionAlreadyPostedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyPosted"));

					} catch (GlobalTransactionAlreadyVoidException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyVoid"));

					} catch (ArINVOverapplicationNotAllowedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.overapplicationNotAllowed", ex.getMessage()));

					} catch (ArINVOverCreditBalancePaidapplicationNotAllowedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
			           			new ActionMessage("receiptEntry.error.insufficientCreditBalance"));

					} catch (ArRCTInvoiceHasNoWTaxCodeException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.invoiceHasNoWTaxCode", ex.getMessage()));

					} catch (GlobalTransactionAlreadyLockedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.transactionAlreadyLocked", ex.getMessage()));

					} catch (GlobalNoApprovalRequesterFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.noApprovalRequesterFound"));

					} catch (GlobalNoApprovalApproverFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.noApprovalApproverFound"));

					} catch (GlJREffectiveDateNoPeriodExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.effectiveDateNoPeriodExist"));

					} catch (GlJREffectiveDatePeriodClosedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.effectiveDatePeriodClosed"));

					} catch (GlobalJournalNotBalanceException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.journalNotBalance"));

					} catch (GlobalBranchAccountNumberInvalidException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.branchAccountNumberInvalid"));

					} catch (GlobalRecordAlreadyAssignedException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.recordAlreadyAssigned"));

					} catch (AdPRFCoaGlCustomerDepositAccountNotFoundException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.noCustomerDepositCOA"));

					} catch (EJBException ex) {
						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
						}

						return(mapping.findForward("cmnErrorPage"));
					}

					// save attachment

			           if (!Common.validateRequired(filename1)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename1().getInputStream();

			       	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

			       	    		int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename2)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename2().getInputStream();

			       	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);
				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename3)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename3().getInputStream();

			       	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }

			       	   if (!Common.validateRequired(filename4)) {

			       	        if (errors.isEmpty()) {

			       	        	InputStream is = actionForm.getFilename4().getInputStream();

			       	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			       	        	new File(attachmentPath).mkdirs();

			       	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			       	    			fileExtension = attachmentFileExtension;

			           	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			           	    		fileExtension = attachmentFileExtensionPDF;

			           	    	}
			       	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

				            	int c;

				            	while ((c = is.read()) != -1) {

				            		fos.write((byte)c);

				            	}

				            	is.close();
								fos.close();

			       	        }

			       	   }


					if (!errors.isEmpty()) {

						saveErrors(request, new ActionMessages(errors));
						return(mapping.findForward("arReceiptEntry"));

					}

				}

				String path = "/arJournal.do?forward=1" +
				"&transactionCode=" + actionForm.getReceiptCode() +
				"&transaction=RECEIPT" +
				"&transactionNumber=" + actionForm.getReceiptNumber() +
				"&transactionDate=" + actionForm.getDate() +
				"&transactionEnableFields=" + actionForm.getEnableFields();

				return(new ActionForward(path));

				/*******************************************************
   -- Ar RCT Delete Action --
				 *******************************************************/

			} else if(request.getParameter("deleteButton") != null) {

				try {

					ejbRCT.deleteArRctEntry(actionForm.getReceiptCode(), user.getUserName(), user.getCmpCode());

				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.recordAlreadyDeleted"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}

				/*******************************************************
   -- Ar RCT Close Action --
				 *******************************************************/

			} else if(request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

				/*******************************************************
   -- Ar RCT Customer Enter Action --
				 *******************************************************/

				} else if(!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

					try {

						ArModCustomerDetails mdetails = ejbRCT.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());

						actionForm.clearArRCTList();
						actionForm.setBankAccount(mdetails.getCstCtBaName());
						actionForm.setPaymentMethod(mdetails.getCstPaymentMethod());
						actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
						actionForm.setCustomerName(mdetails.getCstName());

					} catch (GlobalNoRecordFoundException ex) {

						actionForm.clearArRCTList();

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.customerNoRecordFound"));
						saveErrors(request, new ActionMessages(errors));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}

					try {

						// populate availabe deposit read-only

						double creditBalanceRemaining = ejbRCT.getArRctCreditBalanceByCstCustomerCode(actionForm.getCustomer(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						actionForm.setCreditBalanceRemaining(Common.convertDoubleToStringMoney(creditBalanceRemaining, precisionUnit));

					} catch (GlobalNoRecordFoundException ex) {

						actionForm.setCustomerDeposit(null);

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}

					try {

						System.out.println("Receipt Entry Action customer : " + actionForm.getCustomer());

						ArrayList list = ejbRCT.getArIpsByCstCustomerCode(actionForm.getCustomer(),Common.convertStringToSQLDate(actionForm.getDate()), true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						Iterator i = list.iterator();


						while (i.hasNext()) {



                                                    ArModInvoicePaymentScheduleDetails mdetails = (ArModInvoicePaymentScheduleDetails)i.next();


                                                    //double creditBalancePaid = ejbRCT.getAiCreditBalancePaidByRctCodeByInvNum(mIpsDetails.getIpsCode(),mdetails.get(),user.getCmpCode());

                                                    ArReceiptEntryList ipsList = new ArReceiptEntryList(
                                                                    actionForm, mdetails.getIpsCode(),
                                                                    mdetails.getIpsInvNumber(),mdetails.getIpsArCustomerCode(),
                                                                    Common.convertShortToString(mdetails.getIpsNumber()),
                                                                    Common.convertSQLDateToString(mdetails.getIpsDueDate()),
                                                                    Common.convertSQLDateToString(mdetails.getIpsInvDate()),
                                                                    mdetails.getIpsInvReferenceNumber(),
                                                                    mdetails.getIpsInvFcName(),
                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAmountDue(), precisionUnit),
                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiApplyAmount(), precisionUnit),

                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsPenaltyDue(), precisionUnit),
                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiPenaltyApplyAmount(), precisionUnit),

                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiCreditableWTax(), precisionUnit),
                                                                    Common.convertDoubleToStringMoney(0d, precisionUnit),
                                                                    Common.convertDoubleToStringMoney(0d, precisionUnit),
                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiDiscountAmount(), precisionUnit),
                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiRebate(), precisionUnit),
                                                                    Common.convertDoubleToStringMoney(0d, precisionUnit),
                                                                    mdetails.getIpsAiApplyRebate(),
                                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiDiscountAmount(), precisionUnit),
                                                                    Common.convertDoubleToStringMoney(0d, precisionUnit));



                                                    actionForm.saveArRCTList(ipsList);

                                                    String invoiceNumber = mdetails.getIpsInvNumber();



						}

						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {

							actionForm.setDisablePreviousButton(true);
							actionForm.setDisableFirstButton(true);

						} else {

							actionForm.setDisablePreviousButton(false);
							actionForm.setDisableFirstButton(false);

						}

						// check if next should be disabled
						if (actionForm.getArRCTListSize() <= actionForm.getLineCount() + actionForm.getMaxRows()) {

							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);

						} else {

							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);

						}

					} catch (GlobalNoRecordFoundException ex) {

						actionForm.clearArRCTList();

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.noOpenInvoiceFound"));
						saveErrors(request, new ActionMessages(errors));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage");

						}

					}

					return(mapping.findForward("arReceiptEntry"));






 /*******************************************************
   -- Ar RCT Invoice Number Entered Action --
 *******************************************************/

                        } else if(!Common.validateRequired(request.getParameter("isInvoiceNumberEntered"))) {

                            String customerCode = "";

                            try {
                                actionForm.clearArRCTList();
                                System.out.println("action invoice num; " + actionForm.getInvoiceNumber());
                                ArrayList list = ejbRCT.getArIpsByInvcNmbr(actionForm.getInvoiceNumber(),Common.convertStringToSQLDate(actionForm.getDate()), true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                                Iterator i = list.iterator();


                                while (i.hasNext()) {



                                    ArModInvoicePaymentScheduleDetails mdetails = (ArModInvoicePaymentScheduleDetails)i.next();

                                    customerCode = mdetails.getIpsArCustomerCode();
                                    //double creditBalancePaid = ejbRCT.getAiCreditBalancePaidByRctCodeByInvNum(mIpsDetails.getIpsCode(),mdetails.get(),user.getCmpCode());

                                    ArReceiptEntryList ipsList = new ArReceiptEntryList(
                                                    actionForm, mdetails.getIpsCode(),
                                                    mdetails.getIpsInvNumber(),mdetails.getIpsArCustomerCode(),
                                                    Common.convertShortToString(mdetails.getIpsNumber()),
                                                    Common.convertSQLDateToString(mdetails.getIpsDueDate()),
                                                    Common.convertSQLDateToString(mdetails.getIpsInvDate()),
                                                    mdetails.getIpsInvReferenceNumber(),
                                                    mdetails.getIpsInvFcName(),
                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAmountDue(), precisionUnit),
                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiApplyAmount(), precisionUnit),

                                                    Common.convertDoubleToStringMoney(mdetails.getIpsPenaltyDue(), precisionUnit),
                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiPenaltyApplyAmount(), precisionUnit),

                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiCreditableWTax(), precisionUnit),
                                                    Common.convertDoubleToStringMoney(0d, precisionUnit),
                                                    Common.convertDoubleToStringMoney(0d, precisionUnit),
                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiDiscountAmount(), precisionUnit),
                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiRebate(), precisionUnit),
                                                    Common.convertDoubleToStringMoney(0d, precisionUnit),
                                                    mdetails.getIpsAiApplyRebate(),
                                                    Common.convertDoubleToStringMoney(mdetails.getIpsAiDiscountAmount(), precisionUnit),
                                                    Common.convertDoubleToStringMoney(0d, precisionUnit));



                                    actionForm.saveArRCTList(ipsList);

                                    String invoiceNumber = mdetails.getIpsInvNumber();




                                }

                                // check if prev should be disabled
                                if (actionForm.getLineCount() == 0) {

                                        actionForm.setDisablePreviousButton(true);
                                        actionForm.setDisableFirstButton(true);

                                } else {

                                        actionForm.setDisablePreviousButton(false);
                                        actionForm.setDisableFirstButton(false);

                                }

                                // check if next should be disabled
                                if (actionForm.getArRCTListSize() <= actionForm.getLineCount() + actionForm.getMaxRows()) {

                                        actionForm.setDisableNextButton(true);
                                        actionForm.setDisableLastButton(true);

                                } else {

                                        actionForm.setDisableNextButton(false);
                                        actionForm.setDisableLastButton(false);

                                }

                        } catch (GlobalNoRecordFoundException ex) {

                                actionForm.clearArRCTList();

                                errors.add(ActionMessages.GLOBAL_MESSAGE,
                                                new ActionMessage("receiptEntry.error.noOpenInvoiceFound"));
                                saveErrors(request, new ActionMessages(errors));

                        } catch (EJBException ex) {

                                if (log.isInfoEnabled()) {

                                        log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
                                                        " session: " + session.getId());
                                        return mapping.findForward("cmnErrorPage");

                                }

                        }

                        //run customer code
                        actionForm.setCustomer(customerCode);

                        try {

                                ArModCustomerDetails mdetails = ejbRCT.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());


                                actionForm.setBankAccount(mdetails.getCstCtBaName());
                                actionForm.setPaymentMethod(mdetails.getCstPaymentMethod());
                                actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
                                actionForm.setCustomerName(mdetails.getCstName());

                        } catch (GlobalNoRecordFoundException ex) {

                                actionForm.clearArRCTList();

                                errors.add(ActionMessages.GLOBAL_MESSAGE,
                                                new ActionMessage("receiptEntry.error.customerNoRecordFound"));
                                saveErrors(request, new ActionMessages(errors));

                        } catch (EJBException ex) {

                                if (log.isInfoEnabled()) {

                                        log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
                                                        " session: " + session.getId());
                                        return mapping.findForward("cmnErrorPage");

                                }

                        }

                        try {

                                // populate availabe deposit read-only

                                double creditBalanceRemaining = ejbRCT.getArRctCreditBalanceByCstCustomerCode(actionForm.getCustomer(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                                actionForm.setCreditBalanceRemaining(Common.convertDoubleToStringMoney(creditBalanceRemaining, precisionUnit));

                        } catch (GlobalNoRecordFoundException ex) {

                                actionForm.setCustomerDeposit(null);

                        } catch (EJBException ex) {

                                if (log.isInfoEnabled()) {

                                        log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
                                                        " session: " + session.getId());
                                        return mapping.findForward("cmnErrorPage");

                                }

                        }


                       return(mapping.findForward("arReceiptEntry"));


/*******************************************************
-- Ar View Attachment Action --
*******************************************************/

			}else if(request.getParameter("viewAttachmentButton1") != null) {

    	  	File file = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtension );
    	  	File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtensionPDF );
    	  	String filename = "";

    	  	if (file.exists()){
    	  		filename = file.getName();
    	  	}else if (filePDF.exists()) {
    	  		filename = filePDF.getName();
    	  	}

 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	  		fileExtension = attachmentFileExtension;

  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-1" + fileExtension);

    	  	byte data[] = new byte[fis.available()];

    	  	int ctr = 0;
    	  	int c = 0;

    	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

    	  	}

    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("arReceiptEntry"));

	         } else {

	           return (mapping.findForward("arReceiptEntryChild"));

	        }



	      } else if(request.getParameter("viewAttachmentButton2") != null) {

	    	  File file = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtension );
	    	  	File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtensionPDF );
	    	  	String filename = "";

	    	  	if (file.exists()){
	    	  		filename = file.getName();
	    	  	}else if (filePDF.exists()) {
	    	  		filename = filePDF.getName();
	    	  	}

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-2" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}




	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("arReceiptEntry"));

	         } else {

	           return (mapping.findForward("arReceiptEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton3") != null) {

	    	  File file = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtensionPDF );
	    	  String filename = "";

	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }

	    	  System.out.println(filename + " <== filename?");
	    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    		  fileExtension = attachmentFileExtension;

	    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	    	  }
	    	  System.out.println(fileExtension + " <== file extension?");
	    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-3" + fileExtension);

	    	  byte data[] = new byte[fis.available()];

	    	  int ctr = 0;
	    	  int c = 0;

	    	  while ((c = fis.read()) != -1) {

	    		  data[ctr] = (byte)c;
	    		  ctr++;

	    	  }

	    	  if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  } else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  }

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("arReceiptEntry"));

	         } else {

	           return (mapping.findForward("arReceiptEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton4") != null) {

	    	  File file = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtension );
	    	  File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtensionPDF );
	    	  String filename = "";

	    	  if (file.exists()){
	    		  filename = file.getName();
	    	  }else if (filePDF.exists()) {
	    		  filename = filePDF.getName();
	    	  }

	 			System.out.println(filename + " <== filename?");
		        String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	    	  		fileExtension = attachmentFileExtension;

	  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	  	    		fileExtension = attachmentFileExtensionPDF;

	  	    	}
	    	  	System.out.println(fileExtension + " <== file extension?");
	    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getReceiptCode() + "-4" + fileExtension);

	    	  	byte data[] = new byte[fis.available()];

	    	  	int ctr = 0;
	    	  	int c = 0;

	    	  	while ((c = fis.read()) != -1) {

		      		data[ctr] = (byte)c;
		      		ctr++;

	    	  	}

	    	  	if (fileExtension == attachmentFileExtension) {
			      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
			      	System.out.println("jpg");

			      	session.setAttribute(Constants.IMAGE_KEY, image);
			      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	    	  		System.out.println("pdf");

	    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	    	  	}
	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("arReceiptEntry"));

	         } else {

	           return (mapping.findForward("arReceiptEntryChild"));

	        }

/*******************************************************
   -- Ar RCT Currency Enter Action --
 *******************************************************/

			} else if(!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

				double amount = 0;

				for (int i = 0; i<actionForm.getArRCTListSize(); i++) {

					ArReceiptEntryList arRCTList = actionForm.getArRCTByIndex(i);

					double applyAmount = Common.convertStringMoneyToDouble(arRCTList.getApplyAmount(), precisionUnit);
					double penaltyApplyAmount = Common.convertStringMoneyToDouble(arRCTList.getPenaltyApplyAmount(), precisionUnit);

					double allocatedReceiptAmount = Common.convertStringMoneyToDouble(arRCTList.getAllocatedReceiptAmount(), precisionUnit);

					if (!arRCTList.getPayCheckbox()) continue;

					if(arRCTList.getCurrency().equals(actionForm.getCurrency())) {

						amount = amount + applyAmount + penaltyApplyAmount;

					} else {

						amount = amount + allocatedReceiptAmount;

					}

				}

				actionForm.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

				if (Common.validateRequired(actionForm.getConversionDate())) {

					try {

						
						GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
		         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
		         		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));
						
						
					
					} catch (GlobalConversionDateNotExistException ex) {

						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("receiptEntry.error.conversionDateNotExist"));

					} catch (EJBException ex) {

						if (log.isInfoEnabled()) {

							log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());

						}

						return(mapping.findForward("cmnErrorPage"));

					}

				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

				}

				return(mapping.findForward("arReceiptEntry"));

/*******************************************************
   -- Ar RCT Conversion Date Enter Action --
 *******************************************************/
			} else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

				try {

				
					actionForm.setConversionRate(Common.convertDoubleToStringMoney(
	        				 ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
	        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

					
					
				} catch (GlobalConversionDateNotExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.conversionDateNotExist"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());

					}

					return(mapping.findForward("cmnErrorPage"));

				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

				}

				return(mapping.findForward("arReceiptEntry"));


				/*******************************************************
   -- Ar RCT Load Action --
				 *******************************************************/

			}

			if (frParam != null) {
				System.out.println("CLEAR LIST---------------------------");
				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return(mapping.findForward("arReceiptEntry"));

				}

				if (request.getParameter("forward") == null &&
						actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

					errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
					saveErrors(request, new ActionMessages(errors));

					return mapping.findForward("cmnMain");

				}

				try {

					ArrayList list = null;
					Iterator i = null;

					actionForm.clearCurrencyList();

					list = ejbRCT.getGlFcAllWithDefault(user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

							actionForm.setCurrencyList(mFcDetails.getFcName());

							if (mFcDetails.getFcSob() == 1) {

								actionForm.setCurrency(mFcDetails.getFcName());
								actionForm.setFunctionalCurrency(mFcDetails.getFcName());

							}

						}

					}

					actionForm.clearBankAccountList();


					list = ejbRCT.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							String baName = (String)i.next();

							actionForm.setBankAccountList(baName);

						}

					}

					actionForm.clearPayrollPeriodNameList();

					list = ejbRCT.getHrOpenPpAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	                 if (list == null || list.size() == 0) {

	                     actionForm.setPayrollPeriodNameList(Constants.GLOBAL_NO_RECORD_FOUND);

	                 } else {

	                     i = list.iterator();

	                     while (i.hasNext()) {

	                         actionForm.setPayrollPeriodNameList((String)i.next());

	                     }

	                 }

					if(actionForm.getUseCustomerPulldown()) {

						actionForm.clearCustomerList();

						list = ejbRCT.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

						if (list == null || list.size() == 0) {

							actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

						} else {

							i = list.iterator();

							while (i.hasNext()) {

								actionForm.setCustomerList((String)i.next());

							}

						}

					}

					actionForm.clearBatchNameList();
					actionForm.clearBatchNameUpload2List();
					list = ejbRCT.getArOpenRbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

					if (list == null || list.size() == 0) {

						actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
						actionForm.setBatchNameUpload2List(Constants.GLOBAL_NO_RECORD_FOUND);

					} else {

						i = list.iterator();

						while (i.hasNext()) {

							String batchNameData =(String)i.next();
							actionForm.setBatchNameList(batchNameData);
							actionForm.setBatchNameUpload2List(batchNameData);

						}

					}

					
					//get document type list
	              	
	               	actionForm.clearDocumentTypeList();
	               	
	               	
	               	list = ejbRCT.getDocumentTypeList("AR RECEIPT DOCUMENT TYPE", user.getCmpCode());
	               	
	               	
	               	
	               	i = list.iterator();
	               	while(i.hasNext()) {
	               		actionForm.setDocumentTypeList((String)i.next());
	               	}
	               	
					
					//REPORT PARAMETERS
		        	actionForm.clearReportParameters();

		          	list = ejbRCT.getArReceiptReportParameters(user.getCmpCode());


		          	for(int x=0;x<list.size();x++){

		          	  	ReportParameter reportParameter = new ReportParameter();
		          		String parameterName =list.get(x).toString();
		          		reportParameter.setParameterName(parameterName);
		          	  	actionForm.addReportParameters(reportParameter);
		          	}



					if (request.getParameter("forwardBatch") != null) {

						actionForm.setBatchName(request.getParameter("batchName"));

					}

					actionForm.clearArRCTList();

					if (request.getParameter("forward") != null || isInitialPrinting) {

						if (request.getParameter("forward") != null) {

							actionForm.setReceiptCode(new Integer(request.getParameter("receiptCode")));

						}

						ArModReceiptDetails mdetails = ejbRCT.getArRctByRctCode(actionForm.getReceiptCode(), user.getCmpCode());

						actionForm.setDate(Common.convertSQLDateToString(mdetails.getRctDate()));
						actionForm.setDocumentType(mdetails.getRctDocumentType());
						actionForm.setReceiptNumber(mdetails.getRctNumber());
						actionForm.setReferenceNumber(mdetails.getRctReferenceNumber());
						actionForm.setCheckNo(mdetails.getRctCheckNo());
						actionForm.setPayfileReferenceNumber(mdetails.getRctPayfileReferenceNumber());
						actionForm.setAmount(Common.convertDoubleToStringMoney(mdetails.getRctAmount(), precisionUnit));
						actionForm.setPaymentMethod(mdetails.getRctPaymentMethod());
						actionForm.setReceiptVoid(Common.convertByteToBoolean(mdetails.getRctVoid()));
						actionForm.setEnableAdvancePayment(Common.convertByteToBoolean(mdetails.getRctEnableAdvancePayment()));
						actionForm.setDescription(mdetails.getRctDescription());
						actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getRctConversionDate()));
						actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getRctConversionRate(), precisionUnit));
						actionForm.setApprovalStatus(mdetails.getRctApprovalStatus());
						actionForm.setReasonForRejection(mdetails.getRctReasonForRejection());
						actionForm.setPosted(mdetails.getRctPosted() == 1 ? "YES" : "NO");
						actionForm.setVoidApprovalStatus(mdetails.getRctVoidApprovalStatus());
						actionForm.setVoidPosted(mdetails.getRctVoidPosted() == 1 ? "YES" : "NO");
						actionForm.setCreatedBy(mdetails.getRctCreatedBy());
						actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getRctDateCreated()));
						actionForm.setLastModifiedBy(mdetails.getRctLastModifiedBy());
						actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getRctDateLastModified()));
						actionForm.setApprovedRejectedBy(mdetails.getRctApprovedRejectedBy());
						actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getRctDateApprovedRejected()));
						actionForm.setPostedBy(mdetails.getRctPostedBy());
						actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getRctDatePosted()));

						actionForm.setShowRecalculateJournal(true);
						if (!actionForm.getPayrollPeriodNameList().contains(mdetails.getRctHrPayrollPeriodName())) {

	                         actionForm.setPayrollPeriodNameList(mdetails.getRctHrPayrollPeriodName());

	                     }
	                     actionForm.setPayrollPeriodName(mdetails.getRctHrPayrollPeriodName());

	                     ArrayList rpList = ejbRCT.getArReceiptReportParameters(user.getCmpCode());

	                     actionForm.setReportParameters(Common.convertStringToReportParameters(mdetails.getReportParameter(), rpList));

						double creditBalanceRemaining = ejbRCT.getArRctCreditBalanceByCstCustomerCode(mdetails.getRctCstCustomerCode(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						System.out.println("creditBalanceRemaining="+creditBalanceRemaining);
						System.out.println("customer="+actionForm.getCustomer());
						actionForm.setCreditBalanceRemaining(Common.convertDoubleToStringMoney(creditBalanceRemaining, precisionUnit));

						actionForm.setTotalAmountPaid(Common.convertDoubleToStringMoney(mdetails.getRctAmount()+mdetails.getRctExcessAmount(), precisionUnit));

						if (!actionForm.getCurrencyList().contains(mdetails.getRctFcName())) {

							if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

								actionForm.clearCurrencyList();

							}
							actionForm.setCurrencyList(mdetails.getRctFcName());

						}
						actionForm.setCurrency(mdetails.getRctFcName());

						if (!actionForm.getCustomerList().contains(mdetails.getRctCstCustomerCode())) {

							if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

								actionForm.clearCustomerList();

							}
							actionForm.setCustomerList(mdetails.getRctCstCustomerCode());

						}
						actionForm.setCustomer(mdetails.getRctCstCustomerCode());
						actionForm.setCustomerName(mdetails.getRctCstName());

						String creditBalanceRemaining2 = Common.convertDoubleToStringMoney(ejbRCT.getArRctCreditBalanceByCstCustomerCode(mdetails.getRctCstCustomerCode(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()), precisionUnit);

						actionForm.setCreditBalanceRemaining(creditBalanceRemaining2);




						// get available deposit if draft

						if(actionForm.getApprovalStatus() == null){

							try {

								// populate availabe deposit read-only

								double deposit = ejbRCT.getArRctCreditBalanceByCstCustomerCode(actionForm.getCustomer(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

								actionForm.setCustomerDeposit(Common.convertDoubleToStringMoney(deposit, precisionUnit));

							} catch (GlobalNoRecordFoundException ex) {

								actionForm.setCustomerDeposit(null);

							} catch (EJBException ex) {

								if (log.isInfoEnabled()) {

									log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
											" session: " + session.getId());
									return mapping.findForward("cmnErrorPage");

								}

							}

						}

						if (!actionForm.getBankAccountList().contains(mdetails.getRctBaName())) {

							if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

								actionForm.clearBankAccountList();

							}
							actionForm.setBankAccountList(mdetails.getRctBaName());

						}
						actionForm.setBankAccount(mdetails.getRctBaName());

						if (!actionForm.getBatchNameList().contains(mdetails.getRctRbName())) {

							actionForm.setBatchNameList(mdetails.getRctRbName());

						}
						actionForm.setBatchName(mdetails.getRctRbName());

						list = mdetails.getRctAiList();

						i = list.iterator();

						while (i.hasNext()) {

							ArModAppliedInvoiceDetails mAiDetails = (ArModAppliedInvoiceDetails)i.next();


							String totalApplyAmount = Common.convertDoubleToStringMoney(mAiDetails.getAiApplyAmount() - mAiDetails.getAiCreditBalancePaid(), precisionUnit);


							ArReceiptEntryList ipsList = new ArReceiptEntryList(
									actionForm, mAiDetails.getAiIpsCode(),
									mAiDetails.getAiIpsInvNumber(),mdetails.getRctCstCustomerCode(),
									Common.convertShortToString(mAiDetails.getAiIpsNumber()),
									Common.convertSQLDateToString(mAiDetails.getAiIpsDueDate()),
									Common.convertSQLDateToString(mAiDetails.getAiIpsInvDate()),
									mAiDetails.getAiIpsInvReferenceNumber(),
									mAiDetails.getAiIpsInvFcName(),
									Common.convertDoubleToStringMoney(mAiDetails.getAiIpsAmountDue(), precisionUnit),
									Common.convertDoubleToStringMoney(mAiDetails.getAiApplyAmount(), precisionUnit),

									Common.convertDoubleToStringMoney(mAiDetails.getAiIpsPenaltyDue(), precisionUnit),
									Common.convertDoubleToStringMoney(mAiDetails.getAiPenaltyApplyAmount(), precisionUnit),

									Common.convertDoubleToStringMoney(mAiDetails.getAiCreditableWTax(), precisionUnit),
									Common.convertDoubleToStringMoney(mAiDetails.getAiAppliedDeposit(), precisionUnit), // add apply deposit
									Common.convertDoubleToStringMoney(mAiDetails.getAiCreditBalancePaid(), precisionUnit),
									Common.convertDoubleToStringMoney(mAiDetails.getAiDiscountAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mAiDetails.getAiRebate(), precisionUnit),
									Common.convertDoubleToStringMoney(mAiDetails.getAiAllocatedPaymentAmount(), precisionUnit),

									Common.convertByteToBoolean(mAiDetails.getAiApplyRebate()),
									Common.convertDoubleToStringMoney(mAiDetails.getAiDiscountAmount(), precisionUnit),
									Common.convertDoubleToStringMoney(mAiDetails.getAiCreditBalancePaid(), precisionUnit)
									);

							ipsList.setPayCheckbox(true);

							actionForm.saveArRCTList(ipsList);

						}

						this.setFormProperties(actionForm, user.getCompany());

						if (actionForm.getEnableFields()) {

							try {

								list = ejbRCT.getArIpsByCstCustomerCode(actionForm.getCustomer(),Common.convertStringToSQLDate(actionForm.getDate()), true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

								i = list.iterator();

								while (i.hasNext()) {

									ArModInvoicePaymentScheduleDetails mIpsDetails = (ArModInvoicePaymentScheduleDetails)i.next();

									double creditBalancePaid = ejbRCT.getAiCreditBalancePaidByRctCodeByInvNum(mIpsDetails.getIpsCode(),mdetails.getRctCode(),user.getCmpCode());


								   String totalApplyAmount = Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiApplyAmount() - creditBalancePaid, precisionUnit);



									ArReceiptEntryList ipsList = new ArReceiptEntryList(
											actionForm, mIpsDetails.getIpsCode(),
											mIpsDetails.getIpsInvNumber(),mIpsDetails.getIpsArCustomerCode(),
											Common.convertShortToString(mIpsDetails.getIpsNumber()),
											Common.convertSQLDateToString(mIpsDetails.getIpsDueDate()),
											Common.convertSQLDateToString(mIpsDetails.getIpsInvDate()),
											mIpsDetails.getIpsInvReferenceNumber(),
											mIpsDetails.getIpsInvFcName(),
											Common.convertDoubleToStringMoney(mIpsDetails.getIpsAmountDue(), precisionUnit),
											Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiApplyAmount(), precisionUnit),

											Common.convertDoubleToStringMoney(mIpsDetails.getIpsPenaltyDue(), precisionUnit),
											Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiPenaltyApplyAmount(), precisionUnit),


											Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiCreditableWTax(), precisionUnit),
											Common.convertDoubleToStringMoney(0d, precisionUnit),
											Common.convertDoubleToStringMoney(creditBalancePaid, precisionUnit),
											Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiDiscountAmount(), precisionUnit),
											Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiRebate(), precisionUnit),
											Common.convertDoubleToStringMoney(0d, precisionUnit),

											mIpsDetails.getIpsAiApplyRebate(),
											Common.convertDoubleToStringMoney(mIpsDetails.getIpsAiDiscountAmount(), precisionUnit),
											Common.convertDoubleToStringMoney(creditBalancePaid, precisionUnit));




									actionForm.saveArRCTList(ipsList);

								}

							} catch (GlobalNoRecordFoundException ex) {

							}

						}


						actionForm.setLineCount(0);
						actionForm.setMaxRows(20);
						actionForm.setDisablePreviousButton(true);
						actionForm.setDisableFirstButton(true);

						// check if next should be disabled
						if (actionForm.getArRCTListSize()  <= actionForm.getLineCount() + actionForm.getMaxRows()) {

							actionForm.setDisableNextButton(true);
							actionForm.setDisableLastButton(true);

						} else {

							actionForm.setDisableNextButton(false);
							actionForm.setDisableLastButton(false);

						}

						if (request.getParameter("child") == null) {

							return (mapping.findForward("arReceiptEntry"));

						} else {

							return (mapping.findForward("arReceiptEntryChild"));

						}


					}

				} catch(GlobalNoRecordFoundException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("receiptEntry.error.receiptAlreadyDeleted"));

				} catch(EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}

					return(mapping.findForward("cmnErrorPage"));

				}



				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

				} else {

					if (request.getParameter("saveSubmitButton") != null &&
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

						try {

							ArrayList list = ejbRCT.getAdApprovalNotifiedUsersByRctCode(actionForm.getReceiptCode(), user.getCmpCode());

							if (list.isEmpty()) {

								messages.add(ActionMessages.GLOBAL_MESSAGE,
										new ActionMessage("messages.documentSentForPosting"));

							} else if (list.contains("DOCUMENT POSTED")) {

								messages.add(ActionMessages.GLOBAL_MESSAGE,
										new ActionMessage("messages.documentPosted"));

							} else {

								Iterator i = list.iterator();

								String APPROVAL_USERS = "";

								while (i.hasNext()) {

									APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

									if (i.hasNext()) {

										APPROVAL_USERS = APPROVAL_USERS + ", ";

									}


								}

								messages.add(ActionMessages.GLOBAL_MESSAGE,
										new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

							}

							if (actionForm.getReceiptVoid() && actionForm.getPosted().equals("NO")) {

								actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

							} else {

								saveMessages(request, messages);
								actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

							}


						} catch(EJBException ex) {

							if (log.isInfoEnabled()) {

								log.info("EJBException caught in ArReceiptEntryAction.execute(): " + ex.getMessage() +
										" session: " + session.getId());
							}

							return(mapping.findForward("cmnErrorPage"));

						}

					} else if (request.getParameter("saveAsDraftButton") != null &&
							actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

					}

				}

				actionForm.reset(mapping, request);

				actionForm.setReceiptCode(null);
				actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
				actionForm.setPaymentMethod(Constants.GLOBAL_BLANK);
				actionForm.setConversionRate("1.000000");
				actionForm.setPosted("NO");
				actionForm.setVoidPosted("NO");
				actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
				actionForm.setCreatedBy(user.getUserName());
				actionForm.setLastModifiedBy(user.getUserName());
				actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

				actionForm.setLineCount(0);
				actionForm.setMaxRows(20);
				actionForm.setDisableNextButton(true);
				actionForm.setDisablePreviousButton(true);
				actionForm.setDisableLastButton(true);
				actionForm.setDisableFirstButton(true);

				this.setFormProperties(actionForm, user.getCompany());
				return(mapping.findForward("arReceiptEntry"));

			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}

		} catch(Exception e) {

			/*******************************************************
   System Failed: Forward to error page
			 *******************************************************/

			if (log.isInfoEnabled()) {

				log.info("Exception caught in ArReceiptEntryAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}

			e.printStackTrace();
			return(mapping.findForward("cmnErrorPage"));

		}
	}


	private void setFormProperties(ArReceiptEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getReceiptVoid() && actionForm.getPosted().equals("NO")) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableReceiptVoid(false);
				actionForm.setShowJournalButton(true);

			} else if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getReceiptCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableReceiptVoid(false);
					actionForm.setShowJournalButton(true);

				} else if (actionForm.getReceiptCode() != null &&
						Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableReceiptVoid(true);
					actionForm.setShowJournalButton(true);


				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
						actionForm.getApprovalStatus().equals("N/A")) {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableReceiptVoid(true);
					actionForm.setShowJournalButton(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableReceiptVoid(false);
					actionForm.setShowJournalButton(true);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowJournalButton(true);

				if (actionForm.getVoidApprovalStatus() == null) {
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setEnableReceiptVoid(true);
				} else {
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setEnableReceiptVoid(false);
				}

			}

		} else {


			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableReceiptVoid(false);
			actionForm.setShowJournalButton(true);

		}

//		view attachment
		if (actionForm.getReceiptCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

			String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ar/Receipt/";
			String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
			String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");


 			File file = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtension);
 			File filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-1" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton1(true);

 			} else {

 				actionForm.setShowViewAttachmentButton1(false);

 			}

 			file = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-2" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton2(true);

 			} else {

 				actionForm.setShowViewAttachmentButton2(false);

 			}

 			file = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-3" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton3(true);

 			} else {

 				actionForm.setShowViewAttachmentButton3(false);

 			}

 			file = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtension);
 			filePDF = new File(attachmentPath + actionForm.getReceiptCode() + "-4" + attachmentFileExtensionPDF);

 			if (file.exists() || filePDF.exists()) {

 				actionForm.setShowViewAttachmentButton4(true);

 			} else {

 				actionForm.setShowViewAttachmentButton4(false);

 			}
		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}

	}






}