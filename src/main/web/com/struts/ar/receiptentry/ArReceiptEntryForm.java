package com.struts.ar.receiptentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.ReportParameter;


public class ArReceiptEntryForm extends ActionForm implements Serializable {
	private short precisionUnit = 0;
   private Integer receiptCode = null;
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();

   private String batchNameUpload2 = null;
   private ArrayList batchNameUpload2List = new ArrayList();

   private String documentType = null;
	private ArrayList documentTypeList = new ArrayList();
   
   private String customer = null;
   private ArrayList customerList = new ArrayList();
   private String invoiceNumber = null;
;
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();

   private String columnName = null;
   private ArrayList columnList = new ArrayList();
   private String uniqueIdCode = null;
   private ArrayList uniqueIdCodeList = new ArrayList();

   private String viewType = null;
	private ArrayList viewTypeList = new ArrayList();

	private String payrollPeriodName = null;
	private ArrayList payrollPeriodNameList = new ArrayList();
	private boolean showRecalculateJournal = false;
	private boolean recalculateJournal = false;

   private String date = null;
   private String receiptNumber = null;
   private String referenceNumber = null;
   private String checkNo = null;
   private String payfileReferenceNumber = null;
   private String paymentMethod = null;
   private ArrayList paymentMethodList = new ArrayList();
   private String amount = null;
   private String creditBalanceRemaining = null;
   private boolean receiptVoid = false;
   private String description = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String conversionDate = null;
   private String conversionRate = null;
   private String approvalStatus = null;
   private boolean enableAdvancePayment = false;
   private String totalAmountPaid = null;
   private String posted = null;
   private String voidApprovalStatus = null;
   private String voidPosted = null;
   private String reasonForRejection = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;
   private String approvedRejectedBy = null;
   private String dateApprovedRejected = null;
   private String postedBy = null;
   private String datePosted = null;
   private String customerDeposit = null;
   private String totalCreditBalancePaid = null;
   private String functionalCurrency = null;
   private String customerName = null;



   private ArrayList arRCTList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showBatchName = false;
   private boolean enableReceiptVoid = false;
   private boolean isAdvancePayment =false;
   private boolean showSaveSubmitButton = false;
   private boolean showSaveAsDraftButton = false;
   private boolean showDeleteButton = false;
   private boolean showJournalButton = false;
   private boolean useCustomerPulldown = true;
   private boolean showInvoiceNumber = true;
   private String isCustomerEntered  = null;
   private String isInvoiceNumberEntered = null;
   private String isCurrencyEntered = null;
   private String isConversionDateEntered = null;

   private String report = null;
   private String report2 = null;
   private String journal = null;

   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   private String uploadButton = null;
   private String uploadButton2 = null;
   private int lineCount = 0;
   private int maxRows = 0;
   private boolean enableCreateAdvance = false;

   private FormFile uploadFile = null;
   private FormFile uploadFile2 = null;
   private FormFile filename1 = null;
    private FormFile filename2 = null;
    private FormFile filename3 = null;
    private FormFile filename4 = null;

    ArrayList uploadFileTypeList = new ArrayList();
    private String uploadFileType = null;


    private boolean showViewAttachmentButton1 = false;
    private boolean showViewAttachmentButton2 = false;
    private boolean showViewAttachmentButton3 = false;
    private boolean showViewAttachmentButton4 = false;

    private String attachment = null;
    private String attachmentPDF = null;


   private ArrayList reportParameters = new ArrayList();

   public String getReport() {

   	   return report;

   }

   public void setReport(String report) {

   	   this.report = report;

   }

   public String getReport2() {

   	   return report2;

   }

   public void setReport2(String report2) {

   	   this.report2 = report2;

   }

   public String getJournal() {

   	   return journal;

   }

   public void setJournal(String journal) {

   	   this.journal = journal;

   }

   public int getRowSelected(){
      return rowSelected;
   }

   public ArReceiptEntryList getArRCTByIndex(int index){
      return((ArReceiptEntryList)arRCTList.get(index));
   }

   public Object[] getArRCTList(){
      return(arRCTList.toArray());
   }

   public int getArRCTListSize(){
      return(arRCTList.size());
   }

   public void saveArRCTList(Object newArRCTList){
      arRCTList.add(newArRCTList);
   }

   public void clearArRCTList(){
      arRCTList.clear();
   }

   public void setRowSelected(Object selectedArRCTList, boolean isEdit){
      this.rowSelected = arRCTList.indexOf(selectedArRCTList);
   }

   public void updateArRCTRow(int rowSelected, Object newArRCTList){
      arRCTList.set(rowSelected, newArRCTList);
   }

   public void deleteArRCTList(int rowSelected){
      arRCTList.remove(rowSelected);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

   public Integer getReceiptCode() {

   	  return receiptCode;

   }

   public void setReceiptCode(Integer receiptCode) {

   	  this.receiptCode = receiptCode;

   }

   public String getBatchName() {

   	  return batchName;

   }

   public void setBatchName(String batchName) {

   	  this.batchName = batchName;

   }


   public String getBatchNameUpload2() {

   	  return batchNameUpload2;

   }

   public void setBatchNameUpload2(String batchNameUpload2) {

   	  this.batchNameUpload2 = batchNameUpload2;

   }


   public ArrayList getBatchNameList() {

   	  return batchNameList;

   }

   public void setBatchNameList(String batchName) {

   	  batchNameList.add(batchName);

   }

   public void clearBatchNameList() {

   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);

   }


   public ArrayList getBatchNameUpload2List() {

   	  return batchNameUpload2List;

   }

   public void setBatchNameUpload2List(String batchNameUpload2) {

   	  batchNameUpload2List.add(batchNameUpload2);

   }

   public void clearBatchNameUpload2List() {

   	  batchNameUpload2List.clear();
   	  batchNameUpload2List.add(Constants.GLOBAL_BLANK);

   }

   public String getCustomer() {

   	  return customer;

   }

   public void setCustomer(String customer) {

   	  this.customer = customer;

   }

   public ArrayList getCustomerList() {

   	  return customerList;

   }

   public void setCustomerList(String customer) {

   	  customerList.add(customer);

   }

   public void clearCustomerList() {

   	  customerList.clear();
   	  customerList.add(Constants.GLOBAL_BLANK);

   }

   public String getBankAccount() {

   	  return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

   	  this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

   	  return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

   	  bankAccountList.add(bankAccount);

   }

   public void clearBankAccountList() {

   	  bankAccountList.clear();
   	  bankAccountList.add(Constants.GLOBAL_BLANK);

   }



   public String getInvoiceNumber() {

   	  return invoiceNumber;

   }

   public void setInvoiceNumber(String invoiceNumber) {

   	  this.invoiceNumber = invoiceNumber;

   }





   public String getUniqueIdCode() {

   	  return uniqueIdCode;

   }

   public void setUniqueIdCode(String uniqueIdCode) {

   	  this.uniqueIdCode = uniqueIdCode;

   }

   public boolean getEnableCreateAdvance(){
	   return enableCreateAdvance;
   }

   public void setEnableCreateAdvance(boolean enableCreateAdvance){
	   this.enableCreateAdvance = enableCreateAdvance;
   }

   public String getUploadFileType() {

   	  return uploadFileType;

   }

   public void setUploadFileType(String uploadFileType) {

   	  this.uploadFileType = uploadFileType;

   }

   public ArrayList getUniqueIdCodeList() {

   	  return uniqueIdCodeList;

   }

   public void setUniqueIdCodeList(String uniqueIdCode) {

	   uniqueIdCodeList.add(uniqueIdCode);

   }

   public void clearUniqueIdCodeList() {

	   uniqueIdCodeList.clear();
	   uniqueIdCodeList.add(Constants.GLOBAL_BLANK);

   }


   public String getViewType(){
		return(viewType);
	}

	public void setViewType(String viewType){
		this.viewType = viewType;
	}

	public ArrayList getViewTypeList(){
		return viewTypeList;
	}




public String getPayrollPeriodName() {

		return payrollPeriodName;

	}

	public void setPayrollPeriodName(String payrollPeriodName) {

		this.payrollPeriodName = payrollPeriodName;

	}

	public ArrayList getPayrollPeriodNameList() {

		return payrollPeriodNameList;

	}

	public void setPayrollPeriodNameList(String payrollPeriodName) {

		payrollPeriodNameList.add(payrollPeriodName);

	}

	public void clearPayrollPeriodNameList() {

		payrollPeriodNameList.clear();
		payrollPeriodNameList.add(Constants.GLOBAL_BLANK);

	}



   public String getColumnName() {

	   	  return columnName;

	   }

	   public void setColumnName(String columnName) {

	   	  this.columnName = columnName;

	   }

	   public ArrayList getColumnList() {

	   	  return columnList;

	   }

	   public void setColumnList(String columnName) {

		   columnList.add(columnName);

	   }

	   public void clearColumnList() {

		   columnList.clear();
		   columnList.add(Constants.GLOBAL_BLANK);

	   }









   public String getDate() {

   	  return date;

   }

   public void setDate(String date) {

   	  this.date = date;

   }

   public String getReceiptNumber() {

   	  return receiptNumber;

   }

   public void setReceiptNumber(String receiptNumber) {

   	  this.receiptNumber = receiptNumber;

   }

   public String getReferenceNumber() {

   	  return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

   	   this.referenceNumber = referenceNumber;

   }

   public String getCheckNo() {

	   	  return checkNo;

	   }

   public void setCheckNo(String checkNo) {

   	   this.checkNo = checkNo;

   }

   public String getPayfileReferenceNumber() {

	   	  return payfileReferenceNumber;

	   }

	public void setPayfileReferenceNumber(String payfileReferenceNumber) {

		   this.payfileReferenceNumber = payfileReferenceNumber;

	}

   public String getAmount() {

   	  return amount;

   }

   public void setAmount(String amount) {

   	  this.amount = amount;

   }

   public String getCreditBalanceRemaining(){
	   return creditBalanceRemaining;
   }

   public void setCreditBalanceRemaining(String creditBalanceRemaining){
	   this.creditBalanceRemaining = creditBalanceRemaining;
   }

   public String getPaymentMethod() {

   	  return paymentMethod;

   }

   public void setPaymentMethod(String paymentMethod) {

   	  this.paymentMethod = paymentMethod;

   }

   public ArrayList getPaymentMethodList() {

   	  return paymentMethodList;

   }


   public ArrayList getUploadFileTypeList(){
	   return uploadFileTypeList;
   }




   public boolean getReceiptVoid() {

   	  return receiptVoid;

   }

   public void setReceiptVoid(boolean receiptVoid) {

   	  this.receiptVoid = receiptVoid;

   }

   public String getDescription() {

   	  return description;

   }

   public void setDescription(String description) {

   	  this.description = description;

   }

   public String getCurrency() {

   	   return currency;

   }

   public void setCurrency(String currency) {

   	   this.currency = currency;

   }

   public ArrayList getCurrencyList() {

   	   return currencyList;

   }

   public void setCurrencyList(String currency) {

   	   currencyList.add(currency);

   }

   public void clearCurrencyList() {

   	   currencyList.clear();

   }

   public String getConversionDate() {

   	   return conversionDate;

   }

   public void setConversionDate(String conversionDate) {

   	   this.conversionDate = conversionDate;

   }

   public String getConversionRate() {

   	   return conversionRate;

   }

   public void setConversionRate(String conversionRate) {

   	   this.conversionRate = conversionRate;

   }

   public String getApprovalStatus() {

   	   return approvalStatus;

   }

   public void setApprovalStatus(String approvalStatus) {

   	   this.approvalStatus = approvalStatus;

   }

   public boolean getEnableAdvancePayment(){
	   return enableAdvancePayment;
   }

   public void setEnableAdvancePayment(boolean enableAdvancePayment){
	   this.enableAdvancePayment = enableAdvancePayment;
   }


   public String getTotalAmountPaid(){
	   return totalAmountPaid;
   }

   public void setTotalAmountPaid(String totalAmountPaid){
	   this.totalAmountPaid = totalAmountPaid;
   }

   public String getPosted() {

   	   return posted;

   }

   public void setPosted(String posted) {

   	   this.posted = posted;

   }

   public String getVoidApprovalStatus() {

   	   return voidApprovalStatus;

   }

   public void setVoidApprovalStatus(String voidApprovalStatus) {

   	   this.voidApprovalStatus = voidApprovalStatus;

   }

   public String getVoidPosted() {

   	   return voidPosted;

   }

   public void setVoidPosted(String voidPosted) {

   	   this.voidPosted = voidPosted;

   }

   public String getReasonForRejection() {

   	   return reasonForRejection;

   }

   public void setReasonForRejection(String reasonForRejection) {

   	   this.reasonForRejection = reasonForRejection;

   }

   public String getCreatedBy() {

   	   return createdBy;

   }

   public void setCreatedBy(String createdBy) {

   	  this.createdBy = createdBy;

   }

   public String getDateCreated() {

   	   return dateCreated;

   }

   public void setDateCreated(String dateCreated) {

   	   this.dateCreated = dateCreated;

   }

   public String getLastModifiedBy() {

   	  return lastModifiedBy;

   }

   public void setLastModifiedBy(String lastModifiedBy) {

   	  this.lastModifiedBy = lastModifiedBy;

   }

   public String getDateLastModified() {

   	  return dateLastModified;

   }

   public void setDateLastModified(String dateLastModified) {

   	  this.dateLastModified = dateLastModified;

   }


   public String getApprovedRejectedBy() {

   	  return approvedRejectedBy;

   }

   public void setApprovedRejectedBy(String approvedRejectedBy) {

   	  this.approvedRejectedBy = approvedRejectedBy;

   }

   public String getDateApprovedRejected() {

   	  return dateApprovedRejected;

   }

   public void setDateApprovedRejected(String dateApprovedRejected) {

   	  this.dateApprovedRejected = dateApprovedRejected;

   }

   public String getPostedBy() {

   	  return postedBy;

   }

   public void setPostedBy(String postedBy) {

   	  this.postedBy = postedBy;

   }

   public String getDatePosted() {

   	  return datePosted;

   }

   public void setDatePosted(String datePosted) {

   	  this.datePosted = datePosted;

   }

   public String getFunctionalCurrency() {

   	   return functionalCurrency;

   }

   public void setFunctionalCurrency(String functionalCurrency) {

   	   this.functionalCurrency = functionalCurrency;

   }

   public boolean getEnableFields() {

   	   return enableFields;

   }

   public void setEnableFields(boolean enableFields) {

   	   this.enableFields = enableFields;

   }

   public boolean getShowBatchName() {

   	   return showBatchName;

   }

   public void setShowBatchName(boolean showBatchName) {

   	   this.showBatchName = showBatchName;

   }

   public boolean getEnableReceiptVoid() {

   	   return enableReceiptVoid;

   }

   public void setEnableReceiptVoid(boolean enableReceiptVoid) {

   	   this.enableReceiptVoid = enableReceiptVoid;

   }

   public boolean getIsAdvancePayment() {

   	   return isAdvancePayment;

   }

   public void setIsAdvancePayment(boolean isAdvancePayment) {

   	   this.isAdvancePayment = isAdvancePayment;

   }



   public boolean getShowSaveSubmitButton() {

   	   return showSaveSubmitButton;

   }

   public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {

   	   this.showSaveSubmitButton = showSaveSubmitButton;

   }

   public boolean getShowSaveAsDraftButton() {

   	   return showSaveAsDraftButton;

   }

   public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {

   	   this.showSaveAsDraftButton = showSaveAsDraftButton;

   }

   public boolean getShowDeleteButton() {

   		return showDeleteButton;

   }

   public void setShowDeleteButton(boolean showDeleteButton) {

   		this.showDeleteButton = showDeleteButton;

   }

   public boolean getShowJournalButton() {

   	   return showJournalButton;

   }

   public void setShowJournalButton(boolean showJournalButton) {

   	   this.showJournalButton = showJournalButton;

   }

   public String getIsCustomerEntered() {

   	   return isCustomerEntered;

   }

   public String getIsInvoiceNumberEntered() {

   	   return isInvoiceNumberEntered;

   }

   public int getLineCount(){

   	return lineCount;

   }

   public void setLineCount(int lineCount){

   	this.lineCount = lineCount;

   }

   public int getMaxRows(){

   	return maxRows;

   }

   public void setMaxRows(int maxRows){

   	this.maxRows = maxRows;

   }

   public boolean getDisablePreviousButton() {

   	  return disablePreviousButton;

   }

   public void setDisablePreviousButton(boolean disablePreviousButton) {

   	  this.disablePreviousButton = disablePreviousButton;

   }

   public boolean getDisableNextButton() {

      return disableNextButton;

   }

   public void setDisableNextButton(boolean disableNextButton) {

   	  this.disableNextButton = disableNextButton;

   }

   public boolean getUseCustomerPulldown() {

   	  return useCustomerPulldown;

   }

   public void setUseCustomerPulldown(boolean useCustomerPulldown) {

   	  this.useCustomerPulldown = useCustomerPulldown;

   }


   public boolean getShowInvoiceNumber() {

   	  return showInvoiceNumber;

   }

   public void setShowInvoiceNumber(boolean showInvoiceNumber) {

   	  this.showInvoiceNumber = showInvoiceNumber;

   }

   public String getIsCurrencyEntered() {

   	   return isCurrencyEntered;

   }

   public String getCustomerDeposit() {

   	   return customerDeposit;

   }

   public void setCustomerDeposit(String customerDeposit) {

   	   this.customerDeposit = customerDeposit;

   }

   public String getTotalCreditBalancePaid(){
	   return totalCreditBalancePaid;
   }

   public void setTotalCreditBalancePaid(String totalCreditBalancePaid){
	   this.totalCreditBalancePaid = totalCreditBalancePaid;
   }

   public String getIsConversionDateEntered(){

   	return isConversionDateEntered;

   }

   public boolean getDisableFirstButton() {

	   return disableFirstButton;

   }

   public void setDisableFirstButton(boolean disableFirstButton) {

	   this.disableFirstButton = disableFirstButton;

   }

   public boolean getDisableLastButton() {

	   return disableLastButton;

   }

   public void setDisableLastButton(boolean disableLastButton) {

	   this.disableLastButton = disableLastButton;

   }

   public String getCustomerName() {

   	return customerName;

   }

   public void setCustomerName(String customerName) {

   	this.customerName = customerName;

   }




   // FILE ATTACHMENTS
   //========================================================================
   public String getAttachment() {

		return attachment;

	}

	public void setAttachment(String attachment) {

		this.attachment = attachment;

	}
	public String getAttachmentPDF() {

		return attachmentPDF;

	}

	public void setAttachmentPDF(String attachmentPDF) {

		this.attachmentPDF = attachmentPDF;

	}

	public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {

		return filename3;

	}

	public void setFilename3(FormFile filename3) {

		this.filename3 = filename3;

	}

	public FormFile getFilename4() {

		return filename4;

	}

	public void setFilename4(FormFile filename4) {

		this.filename4 = filename4;

	}

	public boolean getShowViewAttachmentButton1() {

		return showViewAttachmentButton1;

	}

	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

		this.showViewAttachmentButton1 = showViewAttachmentButton1;

	}

	public boolean getShowViewAttachmentButton2() {

		return showViewAttachmentButton2;

	}

	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

		this.showViewAttachmentButton2 = showViewAttachmentButton2;

	}

	public boolean getShowViewAttachmentButton3() {

		return showViewAttachmentButton3;

	}

	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

		this.showViewAttachmentButton3 = showViewAttachmentButton3;

	}

	public boolean getShowViewAttachmentButton4() {

		return showViewAttachmentButton4;

	}

	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

		this.showViewAttachmentButton4 = showViewAttachmentButton4;

	}


	public String getUploadButton(){
		return uploadButton;
	}

	public void setUploadButton(String uploadButton){
		this.uploadButton = uploadButton;
	}

	public String getUploadButton2(){
		return uploadButton2;
	}

	public void setUploadButton2(String uploadButton2){
		this.uploadButton2 = uploadButton2;
	}

	public FormFile getUploadFile(){
		return uploadFile;
	}
	public void setUploadFile(FormFile uploadFile){

		this.uploadFile = uploadFile;
	}

	public FormFile getUploadFile2(){
		return uploadFile2;
	}
	public void setUploadFile2(FormFile uploadFile2){

		this.uploadFile2 = uploadFile2;
	}


	public Object[] getReportParameters() {
		return(reportParameters.toArray());
	}

	public void setReportParameters(ArrayList reportParameters) {
		this.reportParameters = reportParameters;
	}


	public ArrayList getReportParametersAsArrayList() {
		return reportParameters;
	}

	public void addReportParameters(ReportParameter reportParameter) {
		reportParameters.add(reportParameter);
	}

	public void clearReportParameters() {
		reportParameters.clear();
	}


	public String getDocumentType() {
    	return documentType;
    }
    
    public void setDocumentType(String documentType) {
    	this.documentType = documentType;
    }

    public ArrayList getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(String documentType) {
		documentTypeList.add(documentType);
	}

	public void clearDocumentTypeList() {
		documentTypeList.clear();
		documentTypeList.add(Constants.GLOBAL_BLANK);
	}


	public boolean getRecalculateJournal() {

		return recalculateJournal;

	}

	public void setRecalculateJournal(boolean recalculateJournal) {

		this.recalculateJournal= recalculateJournal;

	}
	
	
	public boolean getShowRecalculateJournal() {

		return showRecalculateJournal;

	}

	public void setShowRecalculateJournal(boolean showRecalculateJournal) {

		this.showRecalculateJournal = showRecalculateJournal;

	}

	//==========================================================================

   public void reset(ActionMapping mapping, HttpServletRequest request){

       customer = Constants.GLOBAL_BLANK;
       customerName = Constants.GLOBAL_BLANK;
       invoiceNumber = Constants.GLOBAL_BLANK;
       uniqueIdCode = Constants.GLOBAL_BLANK;
       bankAccount = Constants.GLOBAL_BLANK;
       documentType = null;
	   date = null;
	   receiptNumber = null;
	   referenceNumber = null;
	   checkNo = null;
	   payfileReferenceNumber = null;
	   amount = null;
	   creditBalanceRemaining = null;
	   customerDeposit = null;
	   totalCreditBalancePaid= null;
	   paymentMethodList.clear();
	   enableCreateAdvance = false;

       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CASH);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_BANK_TRANSFER);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CHECK);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_CREDIT_CARD);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_DIRECT);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_ONLINE_BANK);
       paymentMethodList.add(Constants.AR_CE_PAYMENT_METHOD_OFFSETTING);

       uniqueIdCodeList.clear();
       uniqueIdCodeList.add("Customer Code");
       uniqueIdCodeList.add("Employee ID");
       uniqueIdCode = "Customer Code";

       uploadFileTypeList.clear();
       uploadFileTypeList.add("lis");
       uploadFileTypeList.add("csv");
       uploadFileType = "ils";

       columnList.clear();
       String[] cols = ArReceiptEntryUploadList.columns1.split(",");
       for(int x=0;x<cols.length;x++){
    	   columnList.add(cols[x]);
       }


       viewTypeList.clear();
 	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_PDF);
       viewTypeList.add(Constants.REPORT_VIEW_TYPE_EXCEL);
 	   viewTypeList.add(Constants.REPORT_VIEW_TYPE_HTML);

 	   viewType = Constants.REPORT_VIEW_TYPE_PDF;


 	   payrollPeriodName = null;

       isAdvancePayment = false;
       totalAmountPaid = null;
       paymentMethod = Constants.GLOBAL_BLANK;
	   receiptVoid = false;
	   description = null;
	   conversionDate = null;
	   conversionRate = null;
	   approvalStatus = null;
	   posted = null;
	   voidApprovalStatus = null;
	   voidPosted = null;
	   reasonForRejection = null;
	   createdBy = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;
	   isCustomerEntered = null;
	   isCurrencyEntered = null;
	   isConversionDateEntered = null;
	   uploadFile = null;
	   uploadFile2 = null;
	   uploadButton = null;
	   uploadButton2 = null;
	   enableAdvancePayment = false;


	   filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
		attachment = null;
		attachment = null;

	   // clear pay checkbox


		recalculateJournal = false;
	   Iterator i = arRCTList.iterator();

	   int ctr = 0;

	   while (i.hasNext()) {

	   	   ArReceiptEntryList list = (ArReceiptEntryList)i.next();

	   	   if (ctr >= lineCount && ctr <= lineCount + maxRows - 1) {

	   	   		list.setPayCheckbox(false);

		   }

		   ctr++;

	   }

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null ||
	     request.getParameter("saveAsDraftButton") != null ||
         request.getParameter("cvPrintButton") != null ||
         request.getParameter("printButton") != null ||
         request.getParameter("journalButton") != null){

      	 if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
          	    showBatchName){
                errors.add("batchName",
                   new ActionMessage("receiptEntry.error.batchNameRequired"));
         }



      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("receiptEntry.error.customerRequired"));
         }

         if(Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("bankAccount",
               new ActionMessage("receiptEntry.error.bankAccountRequired"));
         }

         if(Common.validateRequired(paymentMethod) || paymentMethod.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
             errors.add("paymentMethod",
                new ActionMessage("receiptEntry.error.paymentMethodRequired"));
          }

      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("receiptEntry.error.dateRequired"));
         }

      	 if(!Common.validateDateFormat(date)){
      		 errors.add("date",
      				 new ActionMessage("receiptEntry.error.dateInvalid"));
      	 }

      	 try{

			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			if(year<1900){
				errors.add("date",
						new ActionMessage("receiptEntry.error.dateInvalid"));
			}
		} catch(Exception ex){

			errors.add("date",
					new ActionMessage("receiptEntry.error.dateInvalid"));

		}

         if(Common.validateRequired(amount) ){
            errors.add("amount",
               new ActionMessage("receiptEntry.error.amountRequired"));
         }


		 if(!Common.validateMoneyFormat(amount)){
	            errors.add("amount",
		       new ActionMessage("receiptEntry.error.amountInvalid"));
		 }

         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("receiptEntry.error.currencyRequired"));
         }

	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("receiptEntry.error.conversionDateInvalid"));
         }

	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("receiptEntry.error.conversionRateInvalid"));
         }

         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("receiptEntry.error.conversionDateMustBeNull"));
	 	 }

		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
		    errors.add("conversionRate",
		       new ActionMessage("receiptEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("receiptEntry.error.conversionRateOrDateMustBeEntered"));
		 }

		 if(enableAdvancePayment){
			 if(Common.validateRequired(totalAmountPaid)){
		            errors.add("totalAmountPaid",
		               new ActionMessage("receiptEntry.error.totalAmountPaidRequired"));
		     }

			 if(!Common.validateMoneyFormat(totalAmountPaid)){
		            errors.add("totalAmountPaid",
			       new ActionMessage("receiptEntry.error.TotalAmountInvalid"));
			 }


		 }

         int numberOfLines = 0;

      	 Iterator i = arRCTList.iterator();

      	 while (i.hasNext()) {

      	 	 ArReceiptEntryList ipsList = (ArReceiptEntryList)i.next();

      	 	 if (!ipsList.getPayCheckbox()) continue;

      	 	 numberOfLines++;




	         if(Common.validateRequired(ipsList.getApplyAmount())){
	            errors.add("applyAmount",
	               new ActionMessage("receiptEntry.error.applyAmountRequired", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	         }
	         if(!Common.validateMoneyFormat(ipsList.getApplyAmount())){
	            errors.add("applyAmount",
	               new ActionMessage("receiptEntry.error.applyAmountInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(ipsList.getDiscount())){
	            errors.add("discount",
	               new ActionMessage("receiptEntry.error.discountInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	         }
	         if(!Common.validateMoneyFormat(ipsList.getCreditableWTax())){
	            errors.add("creditableWTax",
	               new ActionMessage("receiptEntry.error.creditableWTaxInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	         }
	         if(!Common.validateMoneyFormat(ipsList.getAllocatedReceiptAmount())){
	            errors.add("allocatedReceiptAmount",
	               new ActionMessage("receiptEntry.error.allocatedReceiptAmountInvalid", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	         }
	         if(( !ipsList.getCurrency().equals(currency)) ) {
	        	 
	        	 
	        	 System.out.println("f currency in rct : " + functionalCurrency);
	        	 System.out.println("f ipsList.getCurrency(): " + ipsList.getCurrency());
	        	 
	        	 System.out.println("f currency: " + currency);
	 	         	errors.add("currency: ",
	
	 	         	
	 	               new ActionMessage("receiptEntry.error.currencyMissmatched", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	 	         }
	         
	         /*
	         if((!currency.equals(functionalCurrency) || !ipsList.getCurrency().equals(functionalCurrency)) &&
	            (Common.validateRequired(ipsList.getAllocatedReceiptAmount()) || Common.convertStringMoneyToDouble(ipsList.getAllocatedReceiptAmount(), (short)6) == 0d)) {
	         	errors.add("currency",
	               new ActionMessage("receiptEntry.error.allocatedReceiptAmountRequired", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	         }
	         
	        
	         
	         if((currency.equals(functionalCurrency) && ipsList.getCurrency().equals(functionalCurrency)) &&
	            (!Common.validateRequired(ipsList.getAllocatedReceiptAmount()) && Common.convertStringMoneyToDouble(ipsList.getAllocatedReceiptAmount(), (short)6) != 0d)) {
	         	errors.add("currency",
	               new ActionMessage("receiptEntry.error.allocatedReceiptAmountMustNotBeEntered", ipsList.getInvoiceNumber() + "-" + ipsList.getInstallmentNumber()));
	         }
*/
	         /* if ipsList.due date < date error
	         if(Common.convertStringToSQLDate(ipsList.getDueDate()).before(Common.convertStringToSQLDate(date))) {
	         	errors.add("date",
	 	               new ActionMessage("receiptEntry.error.receiptDateMustBeLessThanDueDate", ipsList.getDueDate()));

	         }*/

	    }

	    if (numberOfLines == 0) {

         	errors.add("customer",
               new ActionMessage("receiptEntry.error.receiptMustHaveLine"));

         }


      } else if(request.getParameter("saveUploadButton") != null ){

    	  if(uploadFile.getFileSize() <= 0){
    		  errors.add("date",
	 	       new ActionMessage("receiptEntry.error.noFileSelected"));

    	  }



      } else if(request.getParameter("saveUpload2Button") != null ){

    	  if(uploadFile2.getFileSize() <= 0){
    		  errors.add("date",
	 	       new ActionMessage("receiptEntry.error.noFileSelected"));

    	  }





      }else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("receiptEntry.error.customerRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

      	if(!Common.validateDateFormat(conversionDate)){

      		errors.add("conversionDate", new ActionMessage("receiptEntry.error.conversionDateInvalid"));

      	}

      }

      return(errors);
   }
}
