package com.struts.ar.receiptentry;

import java.io.Serializable;

public class ArReceiptEntryList implements Serializable {

   private Integer invoicePaymentScheduleCode = null;
   private String invoiceNumber = null;
   private String installmentNumber = null;
   private String dueDate = null;
   private String date = null;
   private String referenceNumber = null;
   private String currency = null;
   private String customerCode = null;
   private String amountDue = null;
   private String applyAmount = null;
   
   private String penaltyDue = null;
   private String penaltyApplyAmount = null;
   
   private String creditableWTax = null;
   private String appliedDeposit = null;
   private String creditBalancePaid = null;
   private String allocatedReceiptAmount = null;
   private String discount = null;
   private String rebate = null;
   private String tempDiscount = null;
   private String tempCreditBalancePaid = null;
   
   private boolean payCheckbox = false;
   private boolean allSiCheckbox = false;
   private boolean rebateCheckbox = false;
    
   private ArReceiptEntryForm parentBean;
    
   public ArReceiptEntryList(ArReceiptEntryForm parentBean,
      Integer invoicePaymentScheduleCode,
      String invoiceNumber,
      String customerCode,
      String installmentNumber,
      String dueDate,
      String date,
      String referenceNumber,
      String currency,
      
      String amountDue,
      String applyAmount,
      
      String penaltyDue,
      String penaltyApplyAmount,
      
      String creditableWTax,
	  String appliedDeposit,
	  String creditBalancePaid,
      String discount,
      String rebate,
      String allocatedReceiptAmount,
      boolean rebateCheckbox,
      String tempDiscount,
      String tempCreditBalancePaid){

      this.parentBean = parentBean;
      this.invoicePaymentScheduleCode = invoicePaymentScheduleCode;
      this.invoiceNumber = invoiceNumber;
      this.customerCode = customerCode;
      this.installmentNumber = installmentNumber;
      this.dueDate = dueDate;
      this.date = date;
      this.referenceNumber = referenceNumber;
      this.currency = currency;
      
      this.amountDue = amountDue;      
      this.applyAmount = applyAmount;
      this.penaltyDue = penaltyDue;      
      this.penaltyApplyAmount = penaltyApplyAmount;
      
      this.creditableWTax = creditableWTax;
      this.appliedDeposit  = appliedDeposit;
      this.creditBalancePaid =  creditBalancePaid;
      this.discount = discount;
      this.rebate = rebate;
      this.allocatedReceiptAmount = allocatedReceiptAmount; 
      this.rebateCheckbox = rebateCheckbox;
      this.tempDiscount = tempDiscount;
      this.tempCreditBalancePaid = tempCreditBalancePaid;
      
   }
         
   public Integer getInvoicePaymentScheduleCode() {
   	
      return invoicePaymentScheduleCode;
      
   }

   public String getInvoiceNumber() {
   	
   	  return invoiceNumber;
   	
   }
   
    public String getCustomerCode() {
   	
   	  return customerCode;
   	
   }
   
   public String getInstallmentNumber() {
   	
   	  return installmentNumber;
   	
   }
   
   public String getDueDate() {
   	  
   	  return dueDate;
   }
   
   public String getDate() {
	   	  
	   	  return date;
	   }
   
   
   public String getReferenceNumber() {
   	  
   	  return referenceNumber;
   	
   }
   
   public String getCurrency() {
   	 
   	  return currency;
   	
   }
   
   public String getAmountDue() {
   	 
   	   return amountDue;
   	
   }
   
   public String getApplyAmount() {
   	
   	   return applyAmount;
   	
   }

   
   public void setApplyAmount(String applyAmount) {
   	
   	   this.applyAmount = applyAmount;
   	
   }
   
   
   


   
   
   
   
   
   
   
   
   public String getPenaltyDue() {
	   	 
   	   return penaltyDue;
   	
   }
   
   public String getPenaltyApplyAmount() {
   	
   	   return penaltyApplyAmount;
   	
   }

   
   public void setPenaltyApplyAmount(String penaltyApplyAmount) {
   	
   	   this.penaltyApplyAmount = penaltyApplyAmount;
   	
   }
   
   
   
   
   public String getDiscount() {
   	
   	   return discount;
   	
   }
   
   public void setDiscount(String discount) {
   	   
   	   this.discount = discount;
   	   
   }
   
   public String getRebate() {
	   	
   	   return rebate;
   	
   }
   
   public void setRebate(String rebate) {
   	   
   	   this.rebate = rebate;
   	   
   }
   
   public String getCreditableWTax() {
   	
   	   return creditableWTax;
   	
   }
   
   public void setCreditableWTax(String creditableWTax) {
   	   
   	   this.creditableWTax = creditableWTax;
   	   
   }
   public String getAppliedDeposit() {
   	
   	   return appliedDeposit;
   	
   }
   
   public void setAppliedDeposit(String appliedDeposit) {
   	   
   	   this.appliedDeposit = appliedDeposit;
   	   
   }
   
   public String getCreditBalancePaid(){
	   return creditBalancePaid;
   }
   
   public void setCreditBalancePaid(String creditBalancePaid){
	   this.creditBalancePaid = creditBalancePaid;
   }
      
   public String getAllocatedReceiptAmount() {
   	
   	   return allocatedReceiptAmount;
   	
   }
   
   public void setAllocatedReceiptAmount(String allocatedReceiptAmount) {
   	
   	   this.allocatedReceiptAmount = allocatedReceiptAmount;
   	
   }
   
   public boolean getPayCheckbox() {   	
   	  return payCheckbox;   	
   }
   
   public void setPayCheckbox(boolean payCheckbox) {
   	  this.payCheckbox = payCheckbox;
   }
   
   public boolean getAllSiCheckbox() {
		
		return allSiCheckbox;
		
	}
	
	public void setAllSiCheckbox(boolean allSiCheckbox) {
		
		this.allSiCheckbox = allSiCheckbox;
		
	}
	
	
	public boolean getRebateCheckbox() {
		
		return rebateCheckbox;
		
	}
	
	public void setRebateCheckbox(boolean rebateCheckbox) {
		
		this.rebateCheckbox = rebateCheckbox;
		
	}

   public String getTempDiscount() {
	   
	  return tempDiscount;
	  
   }
   
   public void setTempDiscount(String tempDiscount) {
	   
	   this.tempDiscount = tempDiscount;
	   
   }
   
   
   public String getTempCreditBalancePaid() {
	   
		  return tempCreditBalancePaid;
		  
}
	   
	public void setTempCreditBalancePaid(String tempCreditBalancePaid) {
		   
		   this.tempCreditBalancePaid = tempCreditBalancePaid;
		   
	   }
}