package com.struts.ar.receiptentry;

import java.util.ArrayList;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArReceiptDetails;

public class ArReceiptEntryUploadList {

	public ArReceiptDetails details;
	public ArrayList invoiceList;
	String controlNumber = "";
	String bankAccount = "";
	String currency = "";
	String customerCode = "";
	String customerName = "";
	String batchName = "";
	short precisionUnit;
	String employeeid = "";
	String payfileRefNum="";
        int cstAdBranch = 0;
	public ArrayList branchCodes = new ArrayList();
	
	public static String columns2 =  "CONTROL,"
								+ "EMPLOYEE_ID,"
								+ "EMPLOYEE_NAME,"
								+ "DATE,"				
								+ "APPLY_AMOUNT,"					
								+ "STATUS";
						
	public static String columns1 = "CONTROL,"
								+ "UNIQUE_CODE,"
								+ "DATE,"
								+ "BATCH_NAME,"
								+ "REF_NO,"
								+ "RECEIPT_NO,"
								+ "CHECK_NO.,"
								+ "DESCRIPTION,"
								+ "CUSTOMER_NAME,"
								+ "ENABLE_ADV,"
								+ "TOTAL_AMOUNT_PAID,"
								+ "BANK_ACCOUNT,"
								+ "PAYMENT_METHOD,"
								+ "CURRENCY,"
								+ "INVOICE_NO,"
								+ "IPS_NO.,"
								+ "APPLY_AMOUNT,"
								+ "PENALTY_APPLY,"
								+ "DISCOUNT,"
								+ "CREDIT_BAL_PAID,"
								+ "STATUS";
	
	public boolean isError = false;
	public String errorInformation = "";
	
	ArrayList csvDataList;

	
	public ArReceiptEntryUploadList(short precisionUnit){
		 details = new ArReceiptDetails();
		 invoiceList = new ArrayList();
		 csvDataList = new ArrayList();
		 branchCodes = new ArrayList();
		 this.precisionUnit = precisionUnit;
	}
	
	public void setArReceiptDetails (ArReceiptDetails details){
		this.details = details;
	}
	
	public ArReceiptDetails getArReceiptDetails(){
		return this.details;
	}
	
	public void addInvoiceList(ArModAppliedInvoiceDetails appInvDetails){
		invoiceList.add(appInvDetails);
	}
	
	public void setInvoiceList(ArrayList list){

		this.invoiceList = list;
	}
	
	public ArrayList getInvoiceList(){
		return invoiceList;
	}
	
	public void addCsvDataList2(String[] data,boolean isNew){
		/*
		*0 "Control,"
		1 "Employee ID,"
		2 Employee Name
		3 "Date,"	
		4 Apply Amount,"
		5 STATUS";
		 */
		if(isNew){
			setControlNumber(data[0]);
			details.setRctCode(null); 
			employeeid = data[1].toString();
			details.setRctDate(Common.convertStringToSQLDate(data[3]));		
			System.out.println(data[3].toString() + " is a date");
			System.out.println(details.getRctDate().toString());
			customerName = data[2].toString();
			details.setRctNumber(null);
			details.setRctAmount(Common.convertStringMoneyToDouble(data[4], Constants.MONEY_RATE_PRECISION));
			details.setRctCheckNo("");
			details.setRctDescription("PAYFILE");
			details.setRctVoid(Common.convertBooleanToByte(false));
		    
			details.setRctConversionDate(Common.convertStringToSQLDate(""));
			details.setRctConversionRate(Common.convertStringMoneyToDouble("1", Constants.MONEY_RATE_PRECISION));
			setBankAccount("");
			details.setRctPaymentMethod("");
			
			String payfileReferenceNo = "";
		
			payfileReferenceNo = employeeid.replace('#', ' ').trim() + " " + data[4] + " " + data[3];
			
		    payfileRefNum = payfileReferenceNo;
			
			
			setCurrency("");
		}
		String str_data = "";
		for(int x=0;x<data.length;x++){
			if(str_data.equals("")){
				str_data = str_data + data[x];
			}else{
				str_data = str_data + "," +data[x];
			}
			
		}
		
		csvDataList.add(str_data);	
	}
	
	
	public void addCsvDataList(String[] data,boolean isNew){
		/*
		*0 "Control,"
		1 "Customer Code,"
		2 "Date,"
		3 "Batch Name
		4 "Reference No.,"
		5 "Receipt No.,"
		6 "Check No.,"
		7 "Description,"
		8 "Customer Name,"
		9 "Enable Advance,"
		10 "Total Amount Paid,"
		11 "Bank Account,"
		12 "Payment Method,"
		13 "Currency,"
		14 Invoice No.,"
		15 Apply Amount,"
		16 Penalty Apply,"
		17 Discount,"
		18 Credit Balance Paid,"
		19 STATUS";
		 */
		
		if(isNew){
			details.setRctCode(null); 
			
		
			setControlNumber(data[0]);
			setCustomerCode(data[1]);          
			details.setRctDate(Common.convertStringToSQLDate(data[2]));
			
			details.setRctReferenceNumber(data[4]);
			if(data[5].trim().equals("")){
				details.setRctNumber(null);
			}else{
				details.setRctNumber(data[6]);
			}
	
			
			details.setRctCheckNo(data[7]);
			details.setRctAmount(Common.convertStringMoneyToDouble("0.0", precisionUnit));
			details.setRctVoid(Common.convertBooleanToByte(false));
			details.setRctDescription(data[8]);           
			details.setRctConversionDate(Common.convertStringToSQLDate(""));
			details.setRctConversionRate(Common.convertStringMoneyToDouble("1", Constants.MONEY_RATE_PRECISION));
			setBankAccount(data[11]);
			details.setRctPaymentMethod(data[12]);
			setCurrency(data[13]);
		}
		String str_data = "";
		for(int x=0;x<data.length;x++){
			if(str_data.equals("")){
				str_data = str_data + data[x];
			}else{
				str_data = str_data + "," +data[x];
			}
			
		}
		
		csvDataList.add(str_data);	
	}
	
	public ArrayList getCsvDataList(){
		return csvDataList;
	}
	
	public void setBankAccount(String bankAccount){
		this.bankAccount = bankAccount;
	}
	
	public String getBankAccount(){
		return bankAccount;
	}
	
	
	public void setCurrency(String currency){
		this.currency = currency;
	}
	
	public String getCurrency(){
		return currency;
	}
	
	public void setCustomerCode(String customerCode){
		this.customerCode = customerCode;
	}
	
	public String getCustomerCode(){
		return customerCode;
	}
	
	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}
	
	public String getCustomerName(){
		return customerName;
	}
	
	
	public void setEmployeeId(String employeeid){
		this.employeeid = employeeid;
	}
	
	public String getEmployeeId(){
		return employeeid;
	}
	
	public void setPayfileRefNum(String payfileRefNum){
		this.payfileRefNum = payfileRefNum;
	}
	
	public String getPayfileRefNum(){
		return payfileRefNum;
	}
	
	
	
	
	public void setBatchName(String batchName){
		this.batchName = batchName;
	}
	
	public String getBatchName(){
		return batchName;
	}
	
	public void setControlNumber(String control){
		this.controlNumber =control;
	}
	
	public String getControlNumber(){
		return controlNumber;
	}
	
	public void setCstAdBranch(int cstAdBranch){
		this.cstAdBranch =cstAdBranch;
	}
	
	public int getCstAdBranch(){
		return cstAdBranch;
	}
}



