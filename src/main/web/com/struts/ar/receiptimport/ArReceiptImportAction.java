package com.struts.ar.receiptimport;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.ArINVInvoiceDoesNotExist;
import com.ejb.exception.ArINVOverapplicationNotAllowedException;
import com.ejb.exception.ArRICustomerRequiredException;
import com.ejb.exception.GlobalAmountInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalNotAllTransactionsArePostedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordDisabledException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalRecordInvalidForCurrentBranchException;
import com.ejb.exception.GlobalTransactionAlreadyLockedException;
import com.ejb.txn.ArReceiptImportController;
import com.ejb.txn.ArReceiptImportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModAppliedInvoiceDetails;
import com.util.ArModDistributionRecordDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModInvoiceLineItemDetails;
import com.util.ArModReceiptDetails;
import com.util.ArModReceiptImportPreferenceDetails;
import com.util.ArReceiptImportPreferenceLineDetails;

public final class ArReceiptImportAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
        
        
        try {
        	
/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArReceiptImportAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }

        } else {

            if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        ArReceiptImportForm actionForm = (ArReceiptImportForm)form;
        
        String frParam = Common.getUserPermission(user, Constants.AR_RECEIPT_IMPORT_ID);

        if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

		        ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	            if (!fieldErrors.isEmpty()) {
	
	                saveErrors(request, new ActionMessages(fieldErrors));
	
	                return mapping.findForward("arReceiptImport");
	                
	            }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize ArReceiptImportController EJB
*******************************************************/

        ArReceiptImportControllerHome homeRI = null;
        ArReceiptImportController ejbRI = null;
        
        try {

        	homeRI = (ArReceiptImportControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArReceiptImportControllerEJB", ArReceiptImportControllerHome.class);
             
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArReceiptImportAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbRI = homeRI.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArReceiptImportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();
/*******************************************************
    Call ArReceiptImportController EJB
    getGlFcPrecisionUnit
    getInvGpQuantityPrecisionUnit
*******************************************************/

        short precisionUnit = 0;
        short quantityPrecisionUnit = 0;
        
        try {
        	
        	precisionUnit = ejbRI.getGlFcPrecisionUnit(user.getCmpCode());
        	
        } catch(EJBException ex) {
        	
        	if (log.isInfoEnabled()) {
        		
        		log.info("EJBException caught in ArReceiptImportAction.execute(): " + ex.getMessage() +
        				" session: " + session.getId());
        	}
        	
        	return(mapping.findForward("cmnErrorPage"));
        	
        }
                	
/*******************************************************
   -- Ar RI Import Action --
*******************************************************/
        	
        if (request.getParameter("importButton") != null &&  
        		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
        	
        	String fileName1 = actionForm.getFilename1().getFileName();
        	String fileName2 = actionForm.getFilename2().getFileName();
        	
        	ArrayList headerList = new ArrayList();
        	ArrayList detailsList = new ArrayList();
        	ArrayList drList = new ArrayList();
        	
        	CSVParser csvParser1 = new CSVParser(actionForm.getFilename1().getInputStream());
        	CSVParser csvParser2 = new CSVParser(actionForm.getFilename2().getInputStream());
        	
        	String[][] header = csvParser1.getAllValues();
        	String[][] details = csvParser2.getAllValues();
        	
        	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");
    		String rctBatch = "IMPORT " + formatter.format(new java.util.Date());
        	
    		ArModReceiptImportPreferenceDetails ripDetails = null;
    		
    		ArrayList summRctList = new ArrayList();
			ArrayList rctList = new ArrayList();
			
			boolean IS_SMMRZD = false; 
						
			if(actionForm.getImportMiscReceipt() == false) {
    			
				if(header == null || details == null) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
        					new ActionMessage("receiptImport.error.noReceiptToImport"));
					
				} else {
					
					//COLLECTION
					
					try {
	    				
	    				ripDetails = ejbRI.getArRipByRipType("COLLECTION", user.getCmpCode());
	    				
	    				actionForm.setIsSummarized(Common.convertByteToBoolean(ripDetails.getRipIsSummarized()));
	    				
	    				ArrayList rilList = ripDetails.getRipRilList();
	    				
	        			for (int i=0; i < header.length; i++) {
	                		
	        				ArModReceiptDetails rcptDetails = this.setReceiptHeaderData(actionForm, ripDetails, header, "HEADER", i);
	        				
	                		ArModAppliedInvoiceDetails aiDetails = new ArModAppliedInvoiceDetails();
	                    	
	                		//details
	                		if(ripDetails.getRipReceiptNumberLineFile().equals("HEADER")) {
	                        	
	                        	aiDetails.setAiReceiptNumber(header[i][ripDetails.getRipReceiptNumberLineColumn() - 1].trim());
	                        	
	                        }
	                        	
	                        if(ripDetails.getRipInvoiceNumberFile().equals("HEADER")) {
	                        	
	                        	aiDetails.setAiIpsInvNumber(header[i][ripDetails.getRipInvoiceNumberColumn() - 1].trim());
	                        	
	                        }
	                        	
	                        if(ripDetails.getRipApplyAmountFile().equals("HEADER")) {
	                        	
	                        	aiDetails.setAiApplyAmount(Common.convertStringMoneyToDouble(header[i][ripDetails.getRipApplyAmountColumn() - 1].trim(), precisionUnit));
	                        	
	                        }
	                        	
	                        if(ripDetails.getRipApplyDiscountFile().equals("HEADER")) {
	                        	
	                        	aiDetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(header[i][ripDetails.getRipApplyDiscountColumn() - 1].trim(), precisionUnit));
	                        	
	                        }
	                        	
	                        Iterator rilIter = rilList.iterator();
	                        
	                        while(rilIter.hasNext()) {
	                        	
	                        	ArReceiptImportPreferenceLineDetails rilDetails = (ArReceiptImportPreferenceLineDetails)rilIter.next();
	                    		
	                    		if(rilDetails.getRilFile().equals("HEADER") && (rilDetails.getRilColumn() == 0 || header[i][rilDetails.getRilColumn() - 1].trim().contains(rilDetails.getRilName()))) {
	                        		
	                        		if((rilDetails.getRilGlAccountNumber() != null && !rilDetails.getRilGlAccountNumber().equals(""))) {
	                        			
	                        			ArModDistributionRecordDetails drDetails = new ArModDistributionRecordDetails();
	                        			
	                        			drDetails.setDrReceiptNumber(header[i][ripDetails.getRipReceiptNumberColumn() - 1].trim());
	                        			drDetails.setDrClass(rilDetails.getRilType());
	                        			drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)));
	                        			drDetails.setDrCoaAccountNumber(rilDetails.getRilGlAccountNumber());
	                        			
	                        			drList.add(drDetails);
	                        			
	                        		} else {
	                        			
	                        			rcptDetails.setRctBaName(rilDetails.getRilBankAccountName());

	                        			double RCPT_AMNT = Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit);
                            			
                            			if(RCPT_AMNT <= 0) {
                            				
                            				throw new GlobalAmountInvalidException("total amount for receipt " + rcptDetails.getRctNumber());
                            				
                            			}
                            			
                            			rcptDetails.setRctAmount(RCPT_AMNT);
	                        				                        			
	                        		}
	                        		
	                        	}
	                    		
	                        }
	                        
	                        if(rcptDetails.getRctNumber() != null && !rcptDetails.getRctNumber().equals(""))
	                        	headerList.add(rcptDetails);
	                        
	                        if(aiDetails.getAiReceiptNumber() != null && !aiDetails.getAiReceiptNumber().equals(""))
	                        	detailsList.add(aiDetails);
	                        
	                	}
	        			
	        			for (int i=0; i<details.length; i++) {
	        				
	                		ArModReceiptDetails rcptDetails = this.setReceiptHeaderData(actionForm, ripDetails, details, "LINE", i);
	                		
	                		ArModAppliedInvoiceDetails aiDetails = new ArModAppliedInvoiceDetails();
	                		
	                		//details
	                        if(ripDetails.getRipReceiptNumberLineFile().equals("LINE")) {
	                        	
	                        	aiDetails.setAiReceiptNumber(details[i][ripDetails.getRipReceiptNumberLineColumn() - 1].trim());
	                        	
	                        }
	                        	
	                        if(ripDetails.getRipInvoiceNumberFile().equals("LINE")) {
	                        	
	                        	aiDetails.setAiIpsInvNumber(details[i][ripDetails.getRipInvoiceNumberColumn() - 1].trim());
	                        	
	                        }
	                        	
	                        if(ripDetails.getRipApplyAmountFile().equals("LINE")) {
	                        	
	                        	aiDetails.setAiApplyAmount(Common.convertStringMoneyToDouble(details[i][ripDetails.getRipApplyAmountColumn() - 1].trim(), precisionUnit));
	                        	
	                        }
	                        	
	                        if(ripDetails.getRipApplyDiscountFile().equals("LINE")) {
	                        	
	                        	aiDetails.setAiDiscountAmount(Common.convertStringMoneyToDouble(details[i][ripDetails.getRipApplyDiscountColumn() - 1].trim(), precisionUnit));
	                        	
	                        }
	                        	
	                        Iterator rilIter = rilList.iterator();
	                        
	                        while(rilIter.hasNext()) {
	                        	
	                        	ArReceiptImportPreferenceLineDetails rilDetails = (ArReceiptImportPreferenceLineDetails)rilIter.next();
	                    		
	                    		if(rilDetails.getRilFile().equals("LINE") && (rilDetails.getRilColumn() == 0 || details[i][rilDetails.getRilColumn() - 1].trim().contains(rilDetails.getRilName()))) {
	                        		
	                        		if((rilDetails.getRilGlAccountNumber() != null && !rilDetails.getRilGlAccountNumber().equals(""))) {
	                        			
	                        			ArModDistributionRecordDetails drDetails = new ArModDistributionRecordDetails();
	                        			
	                        			drDetails.setDrReceiptNumber(details[i][ripDetails.getRipReceiptNumberColumn() - 1].trim());
	                        			drDetails.setDrClass(rilDetails.getRilType());
	                        			drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(details[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)));
	                        			drDetails.setDrCoaAccountNumber(rilDetails.getRilGlAccountNumber());
	                        			
	                        			drList.add(drDetails);
	                        			
	                        		} else {
	                        			
	                        			rcptDetails.setRctBaName(rilDetails.getRilBankAccountName());

	                        			double RCPT_AMNT = Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit);
                            			
                            			if(RCPT_AMNT <= 0) {
                            				
                            				throw new GlobalAmountInvalidException("total amount for receipt " + rcptDetails.getRctNumber());
                            				
                            			}
                            			
                            			rcptDetails.setRctAmount(RCPT_AMNT);
	                        			
	                        		}
	                        		
	                        	}
	                    		
	                        }
	                        
	                        if(rcptDetails.getRctNumber() != null && !rcptDetails.getRctNumber().equals(""))
	                        	headerList.add(rcptDetails);
	                        
	                        if(aiDetails.getAiReceiptNumber() != null && !aiDetails.getAiReceiptNumber().equals(""))
	                        	detailsList.add(aiDetails);
	                        
	        			}
	        			
	        			Collections.sort(headerList, sortRctByDocumentNumber);
	            		Collections.sort(detailsList, sortAiByDocumentNumber);
	            		
	            		Iterator rctIter = headerList.iterator();
	        			
	            		while(rctIter.hasNext()) {
	            			
	            			ArModReceiptDetails rctDetails = (ArModReceiptDetails)rctIter.next();
	            			
	            			//add applied invoices
	        				Iterator aiIter = detailsList.iterator();
	            			while(aiIter.hasNext()) {
	            				
	            				ArModAppliedInvoiceDetails aiDetails = (ArModAppliedInvoiceDetails) aiIter.next();
	            				
	            				if(aiDetails.getAiReceiptNumber().equals(rctDetails.getRctNumber())) {
	            					
	            					rctDetails.saveRctAiList(aiDetails);
	            					
	            				}
	            					
	            			}
	            			
	            			if(!rctList.contains(rctDetails))
	            				rctList.add(rctDetails);
	            			
	            		}
	            		
	            		IS_SMMRZD = Common.convertByteToBoolean(ripDetails.getRipIsSummarized());
	            		
	    				if(IS_SMMRZD) {
	    					
	    					Collections.sort(rctList, sortRctByBankAccountName);
	    					
	    					//group receipts
	    					
	    					String BA_NM = null;
	    					ArModReceiptDetails rctDetails = null;
	    					
	    					Iterator i = rctList.iterator();
	    					
	    					while(i.hasNext()) {
	    						
	    						ArModReceiptDetails mdetails = (ArModReceiptDetails)i.next();
	    						
	    						if(BA_NM == null || !BA_NM.equals(mdetails.getRctBaName())) {
	    							
	    							BA_NM = mdetails.getRctBaName();
	    							
	    							rctDetails = new ArModReceiptDetails();
	    							
	    							rctDetails.setRctCstName(mdetails.getRctCstName());
	    							rctDetails.setRctDate(mdetails.getRctDate());
	    							rctDetails.setRctTcName(mdetails.getRctTcName());
	    							rctDetails.setRctWtcName(mdetails.getRctWtcName());
	    							rctDetails.setRctBaName(mdetails.getRctBaName());
	    							rctDetails.setRctAmount(mdetails.getRctAmount());
	    							rctDetails.setRctConversionRate(mdetails.getRctConversionRate());
	    							rctDetails.setRctFcName(mdetails.getRctFcName());
	    							rctDetails.setRctPosTaxAccountNumber(mdetails.getRctPosTaxAccountNumber());
	    							rctDetails.setRctPosWtaxAccountNumber(mdetails.getRctPosWtaxAccountNumber());
	    							
	    							summRctList.add(rctDetails);
	    							
	    						}
	    						
	    						//add applied invoices
	    						
	    						ArrayList aiList = mdetails.getRctAiList();
	    						
	    						Iterator aiIter = aiList.iterator();
	    						
	    						while(aiIter.hasNext()) {
	    							
	    							ArModAppliedInvoiceDetails mAiDetails = (ArModAppliedInvoiceDetails)aiIter.next();
	    							
	    							ArModAppliedInvoiceDetails aiDetails = this.containsAppliedInvoice(mAiDetails.getAiIpsInvNumber(), rctDetails);
	    							
	    							if(aiDetails != null) {
	    								
	    								aiDetails.setAiApplyAmount(aiDetails.getAiApplyAmount() + mAiDetails.getAiApplyAmount());
	    								aiDetails.setAiDiscountAmount(aiDetails.getAiDiscountAmount() + mAiDetails.getAiDiscountAmount());
	    								
	    							} else {
	    								
	    								rctDetails.saveRctAiList(mAiDetails);
	    								
	    							}
	    							
	    						}
	    						
	    					}
	    					
	    				} 
	        					
	    			} catch (GlobalNoRecordFoundException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.importPreferenceNotFound", "Collection"));
	    				
	    			} catch (GlobalAmountInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.amountInvalid", ex.getMessage()));

        			} catch (EJBException ex) {
	    				
	        			if (log.isInfoEnabled()) {
	        				
	        				log.info("EJBException caught in ArReceiptImportAction.execute(): " + ex.getMessage() +
	        						" session: " + session.getId());
	        			}
	        			
	        			return(mapping.findForward("cmnErrorPage"));
	        		}
	    			
	    			if (!errors.isEmpty()) {
	    				
	    				saveErrors(request, new ActionMessages(errors));
	    				return(mapping.findForward("arReceiptImport"));
	    				
	    			}
	    			
	    			//import receipt
	    			
	    			try {
	    				
	    				ejbRI.importRctAi(IS_SMMRZD == true ? summRctList : rctList, IS_SMMRZD, rctBatch, actionForm.getTaxCode(),
	    						user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), 
	    						user.getCmpCode());
	    				
	    			} catch (GlobalDocumentNumberNotUniqueException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.documentNumberNotUnique", ex.getMessage()));

	    			} catch (GlobalTransactionAlreadyLockedException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.transactionAlreadyLocked", ex.getMessage()));

	    			} catch (GlobalNotAllTransactionsArePostedException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.invoiceNotPosted", ex.getMessage()));

	    			} catch (ArINVInvoiceDoesNotExist ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.invoiceDoesNotExist", ex.getMessage()));

	    			} catch (GlobalNoRecordFoundException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.recordNotFound", ex.getMessage()));

	    			} catch (ArINVOverapplicationNotAllowedException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.overapplicationNotAllowed", ex.getMessage()));

	    			} catch (GlobalRecordInvalidException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.recordInvalid", ex.getMessage()));

	    			} catch (GlobalRecordInvalidForCurrentBranchException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.recordInvalidForCurrentBranch", ex.getMessage()));

	    			} catch (GlobalRecordDisabledException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.recordDisabled", ex.getMessage()));

	    			} catch (GlobalJournalNotBalanceException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.amountNotEqual", ex.getMessage()));

	    			} catch (GlobalAmountInvalidException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.amountInvalid", ex.getMessage()));

	    			} catch (ArRICustomerRequiredException ex) {
	    				
	    				errors.add(ActionMessages.GLOBAL_MESSAGE,
	        					new ActionMessage("receiptImport.error.collectionCustomerRequired", ex.getMessage()));

	    			} catch (EJBException ex) {
	    				
	        			if (log.isInfoEnabled()) {
	        				
	        				log.info("EJBException caught in ArReceiptImportAction.execute(): " + ex.getMessage() +
	        						" session: " + session.getId());
	        			}
	        			
	        			return(mapping.findForward("cmnErrorPage"));
	        		}
					
				}
				
				csvParser1.close();
        		csvParser2.close();
				
    		} else if(actionForm.getImportMiscReceipt() == true && actionForm.getMiscReceiptType().equals("ITEMS")) {
    			
    			if(header == null || details == null) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
        					new ActionMessage("receiptImport.error.noReceiptToImport"));
    				
    			} else {
    				            		
            		//MISC RECEIPT - ITEMS
        			
        			try {
        				
        				ripDetails = ejbRI.getArRipByRipType("MISC-ITEM", user.getCmpCode());
        				actionForm.setIsSummarized(Common.convertByteToBoolean(ripDetails.getRipIsSummarized()));
        				
        				//set header & line item data
        				
        				ArrayList rilList = ripDetails.getRipRilList();
        				
            			for (int i=0; i < header.length; i++) {
            				
            				ArModReceiptDetails rcptDetails = this.setReceiptHeaderData(actionForm, ripDetails, header, "HEADER", i);        				
            				ArModInvoiceLineItemDetails iliDetails = new ArModInvoiceLineItemDetails();
            				    
                            if(ripDetails.getRipReceiptNumberLineFile().equals("HEADER")) {
                            
                            	iliDetails.setIliReceiptNumber(header[i][ripDetails.getRipReceiptNumberLineColumn() - 1].trim());
                            	
                            }
                            	
                            if(ripDetails.getRipItemNameFile().equals("HEADER")) {
                            	
                            	iliDetails.setIliIiName(header[i][ripDetails.getRipItemNameColumn() - 1].trim());
                            	
                            }
                            	
                            if(ripDetails.getRipLocationNameFile().equals("HEADER")) {
                            	
                            	iliDetails.setIliLocName(header[i][ripDetails.getRipLocationNameColumn() - 1].trim());
                            	
                            } else if (ripDetails.getRipLocationNameFile().equals("") || ripDetails.getRipLocationNameFile() == null){
                            	
                            	iliDetails.setIliLocName("DEFAULT");
                            	
                            }
                            	
                            if(ripDetails.getRipUomNameColumn() != 0 && (!ripDetails.getRipUomNameFile().equals("") || ripDetails.getRipUomNameFile() != null)) {
                            
    	                        if(ripDetails.getRipUomNameFile().equals("HEADER")) {
    	                        	
    	                        	iliDetails.setIliUomName(header[i][ripDetails.getRipUomNameColumn() - 1].trim());
    	                        	
    	                        }
                            
                            } else {
                            	
                            	iliDetails.setIliUomName("");
                            	
                            }
                            	
                            if(ripDetails.getRipQuantityFile().equals("HEADER")) {
                            	
                            	iliDetails.setIliQuantity(Common.convertStringMoneyToDouble(header[i][ripDetails.getRipQuantityColumn() - 1].trim(), precisionUnit));
                            	
                            }
                            	
                            if(ripDetails.getRipUnitPriceFile().equals("HEADER")) {
                            	
                            	iliDetails.setIliUnitPrice(Common.convertStringMoneyToDouble(header[i][ripDetails.getRipUnitPriceColumn() - 1].trim(), precisionUnit));
                            	
                            }
                            	
                            if(ripDetails.getRipTotalFile().equals("HEADER")) {
                            	
                            	iliDetails.setIliAmount(Common.convertStringMoneyToDouble(header[i][ripDetails.getRipTotalColumn() - 1].trim(), precisionUnit));
                            	
                            	if(ripDetails.getRipUnitPriceColumn() == 0 && ripDetails.getRipUnitPriceFile().equals("")) {
                            		
                            		iliDetails.setIliUnitPrice(iliDetails.getIliAmount() / iliDetails.getIliQuantity());
                            		
                            	}
                            	
                            }
                            	
                            Iterator rilIter = rilList.iterator();
                            
                            while(rilIter.hasNext()) {
                            	
                            	ArReceiptImportPreferenceLineDetails rilDetails = (ArReceiptImportPreferenceLineDetails)rilIter.next();
                        		
                        		if(rilDetails.getRilFile().equals("HEADER") && (rilDetails.getRilColumn() == 0 || header[i][rilDetails.getRilColumn() - 1].trim().contains(rilDetails.getRilName()))) {
                            		
                            		if((rilDetails.getRilGlAccountNumber() != null && !rilDetails.getRilGlAccountNumber().equals(""))) {
                            			
                            			ArModDistributionRecordDetails drDetails = new ArModDistributionRecordDetails();
                            			
                            			drDetails.setDrReceiptNumber(header[i][ripDetails.getRipReceiptNumberColumn() - 1].trim());
                            			drDetails.setDrClass(rilDetails.getRilType());
                            			drDetails.setDrCoaAccountNumber(rilDetails.getRilGlAccountNumber());
                            			
                            			if(iliDetails.getIliReceiptNumber() != null && !iliDetails.getIliReceiptNumber().equals("")) {
                            				
                            				drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)) * iliDetails.getIliQuantity());	
                            				
                            			} else {
                            				
                            				drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)));
                            				
                            			}
                            			
                            			drList.add(drDetails);
                            			
                            		} 
                            		
                            		if(rilDetails.getRilBankAccountName() != null) {
                            			
                            			double RCPT_AMNT = Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit);
                            			
                            			if(RCPT_AMNT <= 0) {
                            				
                            				throw new GlobalAmountInvalidException("total amount for receipt " + rcptDetails.getRctNumber());
                            				
                            			}
                        				
                            			if(!rilDetails.getRilType().equals("GIFT CHECK")) {
                            				
                            				rcptDetails.setRctAmount(RCPT_AMNT);
                            				rcptDetails.setIsGiftCheckLine(false);
                            				
                            			} else {
                            				
                            				rcptDetails.setIsGiftCheckLine(true);
                            				
                            			}
                            			
                            			if(this.isBankAccountSet(rcptDetails.getRctNumber(), headerList) == null || (this.isBankAccountSet(rcptDetails.getRctNumber() , headerList) != null && !rilDetails.getRilType().equals("GIFT CHECK"))) {
                                			
                                			rcptDetails.setRctBaName(rilDetails.getRilBankAccountName());
                                			
                            			}
                            			
                            		}
                            		
                            	}
                        		
                            }
                            
                            if(rcptDetails.getRctNumber() != null && !rcptDetails.getRctNumber().equals("")) 
                            	headerList.add(rcptDetails);
                            	
                            if(iliDetails.getIliReceiptNumber() != null && !iliDetails.getIliReceiptNumber().equals(""))
                            	detailsList.add(iliDetails);
                            
                    	}
            			
            			for (int i=0; i<details.length; i++) {
            				
            				ArModReceiptDetails rcptDetails = this.setReceiptHeaderData(actionForm, ripDetails, details, "LINE", i);
                    		ArModInvoiceLineItemDetails iliDetails = new ArModInvoiceLineItemDetails();
                    		
                			if(ripDetails.getRipReceiptNumberLineFile().equals("LINE")) {
                				
                				iliDetails.setIliReceiptNumber(details[i][ripDetails.getRipReceiptNumberLineColumn() - 1].trim());
                				
                			}
                            	
                            if(ripDetails.getRipItemNameFile().equals("LINE")) {
                            	
                            	iliDetails.setIliIiName(details[i][ripDetails.getRipItemNameColumn() - 1].trim());
                            	
                            }
                            	
                            if(ripDetails.getRipLocationNameFile().equals("LINE")) {
                            	
                            	iliDetails.setIliLocName(details[i][ripDetails.getRipLocationNameColumn() - 1].trim());
                            	
                            } else if (ripDetails.getRipLocationNameFile().equals("") || ripDetails.getRipLocationNameFile() == null){
                            	
                            	iliDetails.setIliLocName("DEFAULT");
                            	
                            }
                            	
                            if(ripDetails.getRipUomNameColumn() != 0 && (!ripDetails.getRipUomNameFile().equals("") || ripDetails.getRipUomNameFile() != null)) {
                            	
                            	if(ripDetails.getRipUomNameFile().equals("LINE")) {
                            		
                            		iliDetails.setIliUomName(details[i][ripDetails.getRipUomNameColumn() - 1].trim());
                            		
                            	}
                            	
                            } else {
                            	
                            	iliDetails.setIliUomName("");
                            	
                            }
                            	
                            if(ripDetails.getRipQuantityFile().equals("LINE")) {
                            	
                            	iliDetails.setIliQuantity(Common.convertStringMoneyToDouble(details[i][ripDetails.getRipQuantityColumn() - 1].trim(), precisionUnit));
                            	
                            }
                            	
                            if(ripDetails.getRipUnitPriceFile().equals("LINE")) {
                            	
                            	iliDetails.setIliUnitPrice(Common.convertStringMoneyToDouble(details[i][ripDetails.getRipUnitPriceColumn() - 1].trim(), precisionUnit));
                            	
                            }
                            	
                            if(ripDetails.getRipTotalFile().equals("LINE")) {
                            	
                            	iliDetails.setIliAmount(Common.convertStringMoneyToDouble(details[i][ripDetails.getRipTotalColumn() - 1].trim(), precisionUnit));
                            	
                            	if(ripDetails.getRipUnitPriceColumn() == 0 && ripDetails.getRipUnitPriceFile().equals("")) {
                            		
                            		iliDetails.setIliUnitPrice(iliDetails.getIliAmount() / iliDetails.getIliQuantity());
                            		
                            	}
                            	
                            }
                            	
                            Iterator rilIter = rilList.iterator();
                            
                            while(rilIter.hasNext()) {
                            	
                            	ArReceiptImportPreferenceLineDetails rilDetails = (ArReceiptImportPreferenceLineDetails)rilIter.next();
                        		
                        		if(rilDetails.getRilFile().equals("LINE") && (rilDetails.getRilColumn() == 0 || details[i][rilDetails.getRilColumn() - 1].trim().contains(rilDetails.getRilName()))) {
                            		
                            		if((rilDetails.getRilGlAccountNumber() != null && !rilDetails.getRilGlAccountNumber().equals(""))) {
                            			
                            			ArModDistributionRecordDetails drDetails = new ArModDistributionRecordDetails();
                            			
                            			drDetails.setDrReceiptNumber(details[i][ripDetails.getRipReceiptNumberColumn() - 1].trim());
                            			drDetails.setDrClass(rilDetails.getRilType());
                            			drDetails.setDrCoaAccountNumber(rilDetails.getRilGlAccountNumber());
                            			
                            			if(iliDetails.getIliReceiptNumber() != null && !iliDetails.getIliReceiptNumber().equals("")) {
                            				
                            				drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(details[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)) * iliDetails.getIliQuantity());	
                            				
                            			} else {
                            				
                            				drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(details[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)));
                            				
                            			}
                            			
                            			
                            			drList.add(drDetails);
                            			
                            		} 
                            		
                            		if(rilDetails.getRilBankAccountName() != null) {
                            			
                            			if(this.isBankAccountSet(rcptDetails.getRctNumber(), headerList) == null || (this.isBankAccountSet(rcptDetails.getRctNumber(), headerList) != null && !rilDetails.getRilType().equals("GIFT CHECK"))) {
                            				
                            				rcptDetails.setRctBaName(rilDetails.getRilBankAccountName());
                            				
                            			}
                            			
                            			double RCPT_AMNT = Common.convertStringMoneyToDouble(details[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit);
                            			
                            			if(RCPT_AMNT <= 0) {
                            				
                            				throw new GlobalAmountInvalidException("total amount for receipt " + rcptDetails.getRctNumber());
                            				
                            			}
                            			
                            			if(!rilDetails.getRilType().equals("GIFT CHECK")) {
                            				
                            				rcptDetails.setRctAmount(RCPT_AMNT);
                            				
                            			}
                            			
                            		}
                            		
                            	}
                        		
                            }
                            
                            if(rcptDetails.getRctNumber() != null && !rcptDetails.getRctNumber().equals(""))
                            	headerList.add(rcptDetails);
                            	
                            if(iliDetails.getIliReceiptNumber() != null && !iliDetails.getIliReceiptNumber().equals(""))
                            	detailsList.add(iliDetails);
                            
            			}
            			
            			csvParser1.close();
                    	csvParser2.close();
            			
            			Collections.sort(headerList, sortRctByDocumentNumber);
                		Collections.sort(detailsList, sortIliByDocumentNumber);
                		Collections.sort(drList, sortDrByReceiptNumber);
                		
                		Iterator rctIter = headerList.iterator();
            			
                		String RCPT_NMBR = null;
                		String CSTMR_NM = null;
                		
                		while(rctIter.hasNext()) {
                			
                			ArModReceiptDetails rctDetails = (ArModReceiptDetails)rctIter.next();
                			            			
                			if(RCPT_NMBR == null || !rctDetails.getRctNumber().equals(RCPT_NMBR)) {
                				
                				RCPT_NMBR = rctDetails.getRctNumber();
                				CSTMR_NM = rctDetails.getRctCstName();
                				
                			} else {
                				
                				if(!CSTMR_NM.equals(rctDetails.getRctCstName())) {
                					
                					errors.add(ActionMessages.GLOBAL_MESSAGE,
                        					new ActionMessage("receiptImport.error.customerInvalid", rctDetails.getRctNumber()));
                					
                					break;
                					
                				}
                				
                			}
                			
                			//add line items
            				Iterator iliIter = detailsList.iterator();
                			while(iliIter.hasNext()) {
                				
                				ArModInvoiceLineItemDetails iliDetails = (ArModInvoiceLineItemDetails) iliIter.next();
                				
                				if(iliDetails.getIliReceiptNumber().equals(rctDetails.getRctNumber())) {
                					
                					rctDetails.saveInvIliList(iliDetails);
                					
                				}
                					
                			}
                			
                			//add dr
                			Iterator drIter = drList.iterator();
                			while(drIter.hasNext()) {
                				
                				ArModDistributionRecordDetails drDetails = (ArModDistributionRecordDetails)drIter.next();
                				
                				if(drDetails.getDrReceiptNumber().equals(rctDetails.getRctNumber())) {
                					
                					rctDetails.saveRctDrList(drDetails);
                					
                				}
                				
                			}
                			
                			ArModReceiptDetails existingRctDetails = this.containsReceipt(rctDetails.getRctNumber(), rctList);
                			
                			if(existingRctDetails != null && !existingRctDetails.getRctBaName().equals(rctDetails.getRctBaName()) && rctDetails.getRctBaName() != null && !rctDetails.getRctBaName().equals("")) {
                				
                				if(this.containsDistributionRecord("GIFT CHECK", rctDetails) == null) {
                					
                					throw new GlobalRecordAlreadyExistException(rctDetails.getRctNumber());
                					
                				} 
                				
                			}
                			
                			ArModReceiptDetails tempDetails = this.isBankAccountSet(rctDetails.getRctNumber(), rctList);
                			
                			if(tempDetails != null && tempDetails.getRctBaName() != null && !tempDetails.getRctBaName().equals("") && !rctDetails.getRctBaName().equals("") && rctDetails.getRctBaName() != null && this.containsReceipt(rctDetails.getRctNumber(), rctList) != null) {
                				
                				if(tempDetails.getIsGiftCheckLine()) {
                					
                					rctList.remove(tempDetails);
                					
                				} else {
                					
                					continue;
                					
                				}
								
            				} 
                			
                			if(rctDetails.getRctBaName() != null && !rctDetails.getRctBaName().equals("") && !rctList.contains(rctDetails) 
                					&& this.containsReceipt(rctDetails.getRctNumber(), rctList) == null) {
                				
                				//get receipt's tax code
                				
                				ArModDistributionRecordDetails drDetails = this.containsTaxCode("TAX", rctDetails.getRctDrList());
                				
                				if(drDetails != null) {
                					
                					rctDetails.setRctTcName(drDetails.getDrTcName());
                					
                				}
                				
                				//get receipt's wtax code
                				
                				drDetails = this.containsTaxCode("WTAX", rctDetails.getRctDrList());
                				
                				if(drDetails != null) {
                					
                					rctDetails.setRctWtcName(drDetails.getDrWtcName());
                					
                				}
                				
                				rctList.add(rctDetails);
                				
                			}
                			
                			
                		}
                		
        			} catch (GlobalNoRecordFoundException ex){
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.importPreferenceNotFound", "Misc Receipt-Items"));
        				
        			} catch (GlobalRecordAlreadyExistException ex){
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.invalidBankAccount", ex.getMessage()));
        				
        			} catch (GlobalAmountInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.amountInvalid", ex.getMessage()));

        			} catch (EJBException ex) {
        				
            			if (log.isInfoEnabled()) {
            				
            				log.info("EJBException caught in ArReceiptImportAction.execute(): " + ex.getMessage() +
            						" session: " + session.getId());
            			}
            			
            			return(mapping.findForward("cmnErrorPage"));
            		}
        			
        			if (!errors.isEmpty()) {
        				
        				saveErrors(request, new ActionMessages(errors));
        				return mapping.findForward("arReceiptImport");
        				
        			}
        			
            		//import misc receipt
            		
        			try {
        				
        				IS_SMMRZD = Common.convertByteToBoolean(ripDetails.getRipIsSummarized());
        				
        				if(IS_SMMRZD) {
        					
        					Collections.sort(rctList, sortRctByBankAccountName);
        					
        					String BA_NM = null;
        					ArModReceiptDetails rctDetails = null;
        					
        					Iterator rctIter = rctList.iterator();
        					
        					while(rctIter.hasNext()) {
        						
        						ArModReceiptDetails mdetails = (ArModReceiptDetails)rctIter.next();
        						
        						if(BA_NM == null || !mdetails.getRctBaName().equals(BA_NM)) {
        							
        							BA_NM = mdetails.getRctBaName();
        							
        							rctDetails = new ArModReceiptDetails();
        							
        							rctDetails.setRctCstName(mdetails.getRctCstName());
        							rctDetails.setRctDate(mdetails.getRctDate());
        							rctDetails.setRctTcName(mdetails.getRctTcName());
        							rctDetails.setRctWtcName(mdetails.getRctWtcName());
        							rctDetails.setRctBaName(mdetails.getRctBaName());
        							rctDetails.setRctConversionRate(mdetails.getRctConversionRate());
        							rctDetails.setRctFcName(mdetails.getRctFcName());
        							rctDetails.setRctPosTaxAccountNumber(mdetails.getRctPosTaxAccountNumber());
        							rctDetails.setRctPosWtaxAccountNumber(mdetails.getRctPosWtaxAccountNumber());
        							
        							summRctList.add(rctDetails);
        							
        						}
        						
        						rctDetails.setRctAmount(rctDetails.getRctAmount() + mdetails.getRctAmount());
        						
        						//add invoice line items
        						
        						ArrayList iliList = mdetails.getInvIliList();
        						
        						Iterator iliIter = iliList.iterator();
        						
        						while(iliIter.hasNext()) {
        							
        							ArModInvoiceLineItemDetails mIliDetails = (ArModInvoiceLineItemDetails)iliIter.next();
        							
        							ArModInvoiceLineItemDetails iliDetails = this.containsInvoiceLineItem(mIliDetails.getIliIiName(), mIliDetails.getIliLocName(), mIliDetails.getIliUnitPrice(), mIliDetails.getIliUomName(), rctDetails);
        							
        							if(mIliDetails.getIliQuantity() <= 0) {
    								
    									throw new GlobalAmountInvalidException("quantity for item " + mIliDetails.getIliIiName() + " of receipt " + mIliDetails.getIliReceiptNumber());
    									
    								}
        							
        							if(mIliDetails.getIliUnitPrice() < 0) {
        								
        								throw new GlobalAmountInvalidException("unit price for item " + mIliDetails.getIliIiName() + " of receipt " + mIliDetails.getIliReceiptNumber());
        								
        							}
        							
        							if(iliDetails != null) {
        								
        								iliDetails.setIliQuantity(iliDetails.getIliQuantity() + mIliDetails.getIliQuantity());
        								
        							} else {
        								
        								rctDetails.saveInvIliList(mIliDetails);
        								
        							}
        							
        						}
        						
        						//add distribution records
        						
        						ArrayList rctDrList = mdetails.getRctDrList();
        						
        						Iterator drIter = rctDrList.iterator();
        						
        						while(drIter.hasNext()) {
        							
        							ArModDistributionRecordDetails mDrDetails = (ArModDistributionRecordDetails)drIter.next();
        							
        							ArModDistributionRecordDetails drDetails = this.containsDistributionRecord(mDrDetails.getDrClass(), rctDetails);
        							
        							if(drDetails != null) {
        								
        								drDetails.setDrAmount(drDetails.getDrAmount() + mDrDetails.getDrAmount());
        								
        							} else {
        								
        								rctDetails.saveRctDrList(mDrDetails);
        								
        							}
        							
        						}
        						
        					}
        						
        				} 
    						
        				ejbRI.importRctIli(IS_SMMRZD == true ? summRctList: rctList, IS_SMMRZD, rctBatch, actionForm.getTaxCode(), 
                				actionForm.getServiceChargeTaxable(), actionForm.getDiscountTaxable(), user.getUserName(), 
    							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        				
        			} catch (GlobalInvItemLocationNotFoundException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.invItemLocationNotFound", ex.getMessage()));
        				
        			} catch (GlobalBranchAccountNumberInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.branchAccountNumberInvalid"));
        				
        			} catch (GlobalNoRecordFoundException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.recordNotFound", ex.getMessage()));
        				
        			} catch (GlobalDocumentNumberNotUniqueException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.documentNumberNotUnique", ex.getMessage()));
        				
        			} catch (GlobalRecordInvalidForCurrentBranchException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.recordInvalidForCurrentBranch", ex.getMessage()));
        				
        			} catch (GlobalRecordInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.customerRequired"));
        				
        			} catch (GlobalRecordDisabledException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.recordDisabled", ex.getMessage()));

        			} catch (GlobalJournalNotBalanceException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.amountNotEqual", ex.getMessage()));

        			} catch (GlobalAmountInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.amountInvalid", ex.getMessage()));

        			} catch (GlobalRecordAlreadyExistException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.POSDataAlreadyExist", ex.getMessage()));

        			} catch (EJBException ex) {
        				
        				if (log.isInfoEnabled()) {
            				
            				log.info("EJBException caught in ArReceiptImportAction.execute(): " + ex.getMessage() +
            						" session: " + session.getId());
            			}
            			
            			return(mapping.findForward("cmnErrorPage"));
        				
        			}
        			
            	}
    		
    		} else if(actionForm.getImportMiscReceipt() == true && actionForm.getMiscReceiptType().equals("MEMO LINES")) {
    			
    			if(header == null || details == null) {
    				
    				errors.add(ActionMessages.GLOBAL_MESSAGE,
        					new ActionMessage("receiptImport.error.noReceiptToImport"));
        			
    				
    			} else {
            		
            		//MISC RECEIPT - MEMO LINES
            		
            		try {
        				
        				ripDetails = ejbRI.getArRipByRipType("MISC-MEMO", user.getCmpCode());
        				
        				actionForm.setIsSummarized(Common.convertByteToBoolean(ripDetails.getRipIsSummarized()));
        				
        				ArrayList rilList = ripDetails.getRipRilList();
        				
        				for (int i=0; i < header.length; i++) {
            				
            				ArModReceiptDetails rcptDetails = this.setReceiptHeaderData(actionForm, ripDetails, header, "HEADER", i);        				
            				ArModInvoiceLineDetails ilDetails = new ArModInvoiceLineDetails();
            				    
            				if(ripDetails.getRipReceiptNumberLineFile().equals("HEADER")) {
            					
            					ilDetails.setIlReceiptNumber(header[i][ripDetails.getRipReceiptNumberLineColumn() - 1].trim());
            					
            				}
            				
            				if(ripDetails.getRipMemoLineFile().equals("HEADER")) {
                            	
                            	ilDetails.setIlSmlName(header[i][ripDetails.getRipMemoLineColumn() - 1].trim());
                            	
                            }
            				
            				if(ripDetails.getRipQuantityFile().equals("HEADER")) {
            					
            					ilDetails.setIlQuantity(Double.parseDouble(header[i][ripDetails.getRipQuantityColumn() - 1].trim()));
            					
            				}
            				
            				if(ripDetails.getRipUnitPriceFile().equals("HEADER")) {
            					
            					ilDetails.setIlUnitPrice(Common.convertStringMoneyToDouble(header[i][ripDetails.getRipUnitPriceColumn() - 1].trim(), precisionUnit));
            					
            				}
            				
            				if(ripDetails.getRipTotalFile().equals("HEADER")) {
            					
            					ilDetails.setIlAmount(Common.convertStringMoneyToDouble(header[i][ripDetails.getRipTotalColumn() - 1].trim(), precisionUnit));
            					
            				}
                            	
            				Iterator rilIter = rilList.iterator();
                            
                            while(rilIter.hasNext()) {
                            	
                            	ArReceiptImportPreferenceLineDetails rilDetails = (ArReceiptImportPreferenceLineDetails)rilIter.next();
                        		
                        		if(rilDetails.getRilFile().equals("HEADER") && (rilDetails.getRilColumn() == 0 || header[i][rilDetails.getRilColumn() - 1].trim().contains(rilDetails.getRilName()))) {
                            		
                            		if((rilDetails.getRilGlAccountNumber() != null && !rilDetails.getRilGlAccountNumber().equals(""))) {
                            			
                            			ArModDistributionRecordDetails drDetails = new ArModDistributionRecordDetails();
                            			
                            			drDetails.setDrReceiptNumber(header[i][ripDetails.getRipReceiptNumberColumn() - 1].trim());
                            			drDetails.setDrClass(rilDetails.getRilType());
                            			drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)));
                            			drDetails.setDrCoaAccountNumber(rilDetails.getRilGlAccountNumber());
                            			
                            			drList.add(drDetails);
                            			
                            		} else {
                            			
                            			rcptDetails.setRctBaName(rilDetails.getRilBankAccountName());
                            			
                            			double RCPT_AMNT = Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit);
                            			
                            			if(RCPT_AMNT <= 0) {
                            				
                            				throw new GlobalAmountInvalidException("total amount for receipt " + rcptDetails.getRctNumber());
                            				
                            			}
                            			
                            			rcptDetails.setRctAmount(RCPT_AMNT);
                            			
                            		}
                            		
                            	}
                        		
                            }
                            
                            if(rcptDetails.getRctNumber() != null && !rcptDetails.getRctNumber().equals("")) 
                            	headerList.add(rcptDetails);
                            	
                            if(ilDetails.getIlReceiptNumber() != null && !ilDetails.getIlReceiptNumber().equals(""))
                            	detailsList.add(ilDetails);
                            
                    	}
        				
        				for (int i=0; i < details.length; i++) {
            				
            				ArModReceiptDetails rcptDetails = this.setReceiptHeaderData(actionForm, ripDetails, details, "LINE", i);        				
            				ArModInvoiceLineDetails ilDetails = new ArModInvoiceLineDetails();
            				    
            				if(ripDetails.getRipReceiptNumberLineFile().equals("LINE")) {
            					
            					ilDetails.setIlReceiptNumber(details[i][ripDetails.getRipReceiptNumberLineColumn() - 1].trim());
            					
            				}
            				
            				if(ripDetails.getRipMemoLineFile().equals("LINE")) {
                            	
                            	ilDetails.setIlSmlName(details[i][ripDetails.getRipMemoLineColumn() - 1].trim());
                            	
                            }
            				
            				if(ripDetails.getRipQuantityFile().equals("LINE")) {
            					
            					//ilDetails.setIlQuantity(new Integer(Integer.parseInt(details[i][ripDetails.getRipQuantityColumn() - 1].trim())));
            					ilDetails.setIlQuantity(Double.parseDouble(details[i][ripDetails.getRipQuantityColumn() - 1].trim()));
            					
            				}
            				
            				if(ripDetails.getRipUnitPriceFile().equals("LINE")) {
            					
            					ilDetails.setIlUnitPrice(Common.convertStringMoneyToDouble(details[i][ripDetails.getRipUnitPriceColumn() - 1].trim(), precisionUnit));
            					
            				}
            				
            				if(ripDetails.getRipTotalFile().equals("LINE")) {
            					
            					ilDetails.setIlAmount(Common.convertStringMoneyToDouble(details[i][ripDetails.getRipTotalColumn() - 1].trim(), precisionUnit));
            					
            				}
                            	
            				Iterator rilIter = rilList.iterator();
                            
                            while(rilIter.hasNext()) {
                            	
                            	ArReceiptImportPreferenceLineDetails rilDetails = (ArReceiptImportPreferenceLineDetails)rilIter.next();
                        		
                        		if(rilDetails.getRilFile().equals("LINE") && (rilDetails.getRilColumn() == 0 || details[i][rilDetails.getRilColumn() - 1].trim().contains(rilDetails.getRilName()))) {
                            		
                            		if((rilDetails.getRilGlAccountNumber() != null && !rilDetails.getRilGlAccountNumber().equals(""))) {
                            			
                            			ArModDistributionRecordDetails drDetails = new ArModDistributionRecordDetails();
                            			
                            			drDetails.setDrReceiptNumber(details[i][ripDetails.getRipReceiptNumberColumn() - 1].trim());
                            			drDetails.setDrClass(rilDetails.getRilType());
                            			drDetails.setDrAmount(Math.abs(Common.convertStringMoneyToDouble(details[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit)));
                            			drDetails.setDrCoaAccountNumber(rilDetails.getRilGlAccountNumber());
                            			
                            			drList.add(drDetails);
                            			
                            		} else {
                            			
                            			rcptDetails.setRctBaName(rilDetails.getRilBankAccountName());
                            			
                            			double RCPT_AMNT = Common.convertStringMoneyToDouble(header[i][rilDetails.getRilAmountColumn() - 1].trim(), precisionUnit);
                            			
                            			if(RCPT_AMNT <= 0) {
                            				
                            				throw new GlobalAmountInvalidException("total amount for receipt " + rcptDetails.getRctNumber());
                            				
                            			}
                            			
                            			rcptDetails.setRctAmount(RCPT_AMNT);
                            			
                            		}
                            		
                            	}
                        		
                            }
                            
                            if(rcptDetails.getRctNumber() != null && !rcptDetails.getRctNumber().equals("")) 
                            	headerList.add(rcptDetails);
                            	
                            if(ilDetails.getIlReceiptNumber() != null && !ilDetails.getIlReceiptNumber().equals(""))
                            	detailsList.add(ilDetails);
                            
                    	}
        				
        				csvParser1.close();
        	        	csvParser2.close();
        				
        				Collections.sort(headerList, sortRctByDocumentNumber);
                		Collections.sort(detailsList, sortIlByDocumentNumber);
                		Collections.sort(drList, sortDrByReceiptNumber);
                		
                		Iterator rctIter = headerList.iterator();
            			
                		while(rctIter.hasNext()) {
                			
                			ArModReceiptDetails rctDetails = (ArModReceiptDetails)rctIter.next();
                			
                			//add invoice lines
            				Iterator ilIter = detailsList.iterator();
                			while(ilIter.hasNext()) {
                				
                				ArModInvoiceLineDetails ilDetails = (ArModInvoiceLineDetails) ilIter.next();
                				
                				if(ilDetails.getIlReceiptNumber().equals(rctDetails.getRctNumber())) {
                					
                					rctDetails.saveInvIlList(ilDetails);
                					
                				}
                					
                			}
                			
                			//add dr
                			Iterator drIter = drList.iterator();
                			while(drIter.hasNext()) {
                				
                				ArModDistributionRecordDetails drDetails = (ArModDistributionRecordDetails)drIter.next();
                				
                				if(drDetails.getDrReceiptNumber().equals(rctDetails.getRctNumber())) {
                					
                					rctDetails.saveRctDrList(drDetails);
                					
                				}
                				
                			}
                			
                			ArModReceiptDetails existingRctDetails = this.containsReceipt(rctDetails.getRctNumber(), rctList);
                			
                			if(existingRctDetails != null && !existingRctDetails.getRctBaName().equals(rctDetails.getRctBaName()) && rctDetails.getRctBaName() != null && !rctDetails.getRctBaName().equals("")) {
                				
                				throw new GlobalRecordAlreadyExistException(rctDetails.getRctNumber());
                				
                			}
                			
                			if(rctDetails.getRctBaName() != null && !rctDetails.getRctBaName().equals("") && !rctList.contains(rctDetails) && this.containsReceipt(rctDetails.getRctNumber(), rctList) == null) {
                				
                				//get receipt's tax code
                				
                				ArModDistributionRecordDetails drDetails = this.containsTaxCode("TAX", rctDetails.getRctDrList());
                				
                				if(drDetails != null) {
                					
                					rctDetails.setRctTcName(drDetails.getDrTcName());
                					
                				}
                				
                				//get receipt's wtax code
                				
                				drDetails = this.containsTaxCode("WTAX", rctDetails.getRctDrList());
                				
                				if(drDetails != null) {
                					
                					rctDetails.setRctWtcName(drDetails.getDrWtcName());
                					
                				}
                				
                				rctList.add(rctDetails);
                				
                			}
                			
                		}
                		
        			} catch (GlobalNoRecordFoundException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.importPreferenceNotFound", "Misc Receipt-Memo Lines"));
        				
        			} catch (GlobalRecordAlreadyExistException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.invalidBankAccount", ex.getMessage()));
        				
        			} catch (GlobalAmountInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.amountInvalid", ex.getMessage()));

        			} catch (EJBException ex) {
        				
        				if (log.isInfoEnabled()) {
            				
            				log.info("EJBException caught in ArReceiptImportAction.execute(): " + ex.getMessage() +
            						" session: " + session.getId());
            			}
            			
            			return(mapping.findForward("cmnErrorPage"));
        				
        			}
        			
        			if (!errors.isEmpty()) {
        				
        				saveErrors(request, new ActionMessages(errors));
        				return(mapping.findForward("arReceiptImport"));
        				
        			}
        			
        			//import misc receipt
        			
        			try {
        				
        				IS_SMMRZD = Common.convertByteToBoolean(ripDetails.getRipIsSummarized());
        				
        				if(IS_SMMRZD) {
        					
        					// group receipts
        					
        					Collections.sort(rctList, sortRctByBankAccountName);
        					
        					String BA_NM = null;
        					ArModReceiptDetails rctDetails = null;
        					
        					Iterator rctIter = rctList.iterator();
        					
        					while(rctIter.hasNext()) {
        						
        						ArModReceiptDetails mdetails = (ArModReceiptDetails)rctIter.next();
        						
        						if(BA_NM == null || !BA_NM.equals(mdetails.getRctBaName())) {
        							
        							BA_NM = mdetails.getRctBaName();
        							
        							rctDetails = new ArModReceiptDetails();
        							
        							rctDetails.setRctCstName(mdetails.getRctCstName());
        							rctDetails.setRctDate(mdetails.getRctDate());
        							rctDetails.setRctTcName(mdetails.getRctTcName());
        							rctDetails.setRctWtcName(mdetails.getRctWtcName());
        							rctDetails.setRctBaName(mdetails.getRctBaName());
        							rctDetails.setRctConversionRate(mdetails.getRctConversionRate());
        							rctDetails.setRctFcName(mdetails.getRctFcName());
        							rctDetails.setRctPosTaxAccountNumber(mdetails.getRctPosTaxAccountNumber());
        							rctDetails.setRctPosWtaxAccountNumber(mdetails.getRctPosWtaxAccountNumber());
        							
        							summRctList.add(rctDetails);
        							
        						}
        						
        						rctDetails.setRctAmount(rctDetails.getRctAmount() + mdetails.getRctAmount());
        						
        						//add invoice lines
        						
        						ArrayList ilList = mdetails.getInvIlList();
        						
        						Iterator ilIter = ilList.iterator();
        						
        						while(ilIter.hasNext()) {
        							
        							ArModInvoiceLineDetails mIlDetails = (ArModInvoiceLineDetails)ilIter.next();
        							
        							ArModInvoiceLineDetails ilDetails = this.containsInvoiceLine(mIlDetails.getIlSmlName(), mIlDetails.getIlUnitPrice(), rctDetails);
        							
        							if(mIlDetails.getIlQuantity() <= 0) {
        								
        								throw new GlobalAmountInvalidException("quantity for memo line " + mIlDetails.getIlSmlName() + " of receipt " + mIlDetails.getIlReceiptNumber());
        								
        							}
        							
        							if(mIlDetails.getIlUnitPrice() < 0) {
        								
    									throw new GlobalAmountInvalidException("unit price for memo line " + mIlDetails.getIlSmlName() + " of receipt " + mIlDetails.getIlReceiptNumber());
        								
        							}
        							
        							if(ilDetails != null) {
        								
        								ilDetails.setIlQuantity(ilDetails.getIlQuantity() + mIlDetails.getIlQuantity());
        								ilDetails.setIlAmount(ilDetails.getIlQuantity() * ilDetails.getIlUnitPrice());
        								
        							} else {
        								
        								rctDetails.saveInvIlList(mIlDetails);
        								
        							}
        							
        						}
        						
        						//add distribution records
        						
        						ArrayList rctDrList = mdetails.getRctDrList();
        						
        						Iterator drIter = rctDrList.iterator();
        						
        						while(drIter.hasNext()) {
        							
        							ArModDistributionRecordDetails mDrDetails = (ArModDistributionRecordDetails)drIter.next();
        							
        							ArModDistributionRecordDetails drDetails = this.containsDistributionRecord(mDrDetails.getDrClass(), rctDetails);
        							
        							if(drDetails != null) {
        								
        								drDetails.setDrAmount(drDetails.getDrAmount() + mDrDetails.getDrAmount());
        								
        							} else {
        								
        								rctDetails.saveRctDrList(mDrDetails);
        								
        							}
        							
        						}
        						
        					}
        					
        				}
        				
        				ejbRI.importRctIl(IS_SMMRZD == true ? summRctList : rctList, IS_SMMRZD, rctBatch, actionForm.getTaxCode(), 
        						actionForm.getServiceChargeTaxable(), actionForm.getDiscountTaxable(), user.getUserName(), 
    							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        				
        			} catch(GlobalDocumentNumberNotUniqueException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.documentNumberNotUnique", ex.getMessage()));
        				
        			} catch(GlobalNoRecordFoundException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.recordNotFound", ex.getMessage()));
        				
        			} catch(GlobalRecordInvalidForCurrentBranchException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.recordInvalidForCurrentBranch", ex.getMessage()));
        				
        			} catch (GlobalRecordDisabledException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.recordDisabled", ex.getMessage()));

        			} catch (GlobalRecordInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.customerRequired", ex.getMessage()));

        			} catch (GlobalBranchAccountNumberInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.branchAccountNumberInvalid"));

        			} catch (GlobalJournalNotBalanceException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.amountNotEqual", ex.getMessage()));

        			} catch (GlobalAmountInvalidException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.amountInvalid", ex.getMessage()));

        			} catch (GlobalRecordAlreadyExistException ex) {
        				
        				errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("receiptImport.error.POSDataAlreadyExist", ex.getMessage()));

        			} catch (EJBException ex) {
        				
        				if (log.isInfoEnabled()) {
            				
            				log.info("EJBException caught in ArReceiptImportAction.execute(): " + ex.getMessage() +
            						" session: " + session.getId());
            			}
            			
            			return(mapping.findForward("cmnErrorPage"));
        				
        			}
            		
    			}
    		
    		}
			
		
        	
        } else if (request.getParameter("closeButton") != null) {
        	
        	return(mapping.findForward("cmnMain"));
        	
/*******************************************************
 -- Ar RI Load Action --
 *******************************************************/
        	
        }
         
         if (frParam != null) {
         	
         	actionForm.clearCustomerList();           
        	
    		ArrayList list = ejbRI.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    		
    		if (list == null || list.size() == 0) {
    			
    			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);
    			
    		} else {
    			
    			Iterator i = list.iterator();
    			
    			while (i.hasNext()) {
    				
    				actionForm.setCustomerList((String)i.next());
    				
    			}
    			
    		} 
    		
    		actionForm.clearTaxCodeList();
    		
    		list = ejbRI.getArTaxCodeAll(user.getCmpCode());
    		
    		if(list == null || list.size() == 0) {
    			
    			actionForm.setTaxCodeList(Constants.GLOBAL_BLANK);
    			
    		} else {
    			
    			Iterator i = list.iterator();
    			
    			while(i.hasNext()) {
    				
    				actionForm.setTaxCodeList((String)i.next());
    				
    			}
    			
    		}

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arReceiptImport");

            }
            
            if ((request.getParameter("importButton") != null) && 
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            	actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            
            actionForm.reset(mapping, request);
            
            return(mapping.findForward("arReceiptImport"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	
      	  e.printStackTrace();

          if (log.isInfoEnabled()) {

             log.info("Exception caught in ArReceiptImportAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
    
    private ArModReceiptDetails setReceiptHeaderData(ArReceiptImportForm actionForm, ArModReceiptImportPreferenceDetails ripDetails, String[][] data, String FILE, int i) {
    	
    	log.info("ArReceiptImportAction setReceiptHeaderData");
    	
    	ArModReceiptDetails mDetails = new ArModReceiptDetails();
    	
    	if(actionForm.getIsSummarized() == true) {
    		
    		mDetails.setRctCstName(actionForm.getCustomer());
    		
    	} else {
    		
    		if(actionForm.getCustomer() != null && !actionForm.getCustomer().equals("")) {
    			
    			mDetails.setRctCstName(actionForm.getCustomer());
    			
    		} else if(ripDetails.getRipCustomerColumn() != 0 && !ripDetails.getRipCustomerFile().equals("")){
    			
    			mDetails.setRctCstName(data[i][ripDetails.getRipCustomerColumn() - 1].trim());
    			
    		}
    		
    	}
    		
		if(ripDetails.getRipDateFile().equals(FILE)) {
			
			mDetails.setRctDate(Common.convertStringToSQLDate(data[i][ripDetails.getRipDateColumn() - 1].trim()));
			
		}
			
		if(ripDetails.getRipReceiptNumberFile().equals(FILE)) {
			
			mDetails.setRctNumber(data[i][ripDetails.getRipReceiptNumberColumn() - 1].trim());
			
		}
		
		if(ripDetails.getRipTcFile().equals(FILE)) {
        	
			mDetails.setRctTcName(data[i][ripDetails.getRipTcColumn() - 1].trim());
        	
        }
		
		if(ripDetails.getRipWtcFile().equals(FILE)) {
        	
			mDetails.setRctWtcName(data[i][ripDetails.getRipWtcColumn() - 1].trim());
        	
        }
			
		mDetails.setRctConversionRate(Common.convertStringMoneyToDouble("1.000000", Constants.MONEY_RATE_PRECISION));
		mDetails.setRctFcName("PHP");
		
		return mDetails;
		
    }
    
    private ArModDistributionRecordDetails containsTaxCode(String TX_NM, ArrayList drList) {
    	
    	Iterator i = drList.iterator();
    	
    	while(i.hasNext()) {
    		
    		ArModDistributionRecordDetails mdetails = (ArModDistributionRecordDetails)i.next();
    		
    		if(mdetails.getDrClass().equals(TX_NM)) {
    			
    			return mdetails;
    			
    		}
    		
    	}
    	
    	return null;
    	
    }
    
    private ArModAppliedInvoiceDetails containsAppliedInvoice(String INV_NMBR, ArModReceiptDetails details) {
    	
    	ArrayList aiList = details.getRctAiList();
    	
    	Iterator aiIter = aiList.iterator();
    	
    	while(aiIter.hasNext()) {
    		
    		ArModAppliedInvoiceDetails aiDetails = (ArModAppliedInvoiceDetails)aiIter.next();
    		
    		if(aiDetails.getAiIpsInvNumber().equals(INV_NMBR)) {
    			
    			return aiDetails;
    			
    		}
    		
    	}
    	
    	return null;
    	
    }
    
    private ArModInvoiceLineItemDetails containsInvoiceLineItem(String ITM_NM, String ITM_LCTN, double UNT_PRC, String UOM_NM, ArModReceiptDetails details) {
    	
    	ArrayList iliList = details.getInvIliList();
    	
    	Iterator iliIter = iliList.iterator();
    	
    	while(iliIter.hasNext()) {
    		
    		ArModInvoiceLineItemDetails iliDetails = (ArModInvoiceLineItemDetails)iliIter.next();
    		
    		if(iliDetails.getIliIiName().equals(ITM_NM) && iliDetails.getIliLocName().equals(ITM_LCTN) &&
    				iliDetails.getIliUnitPrice() == UNT_PRC && iliDetails.getIliUomName().equals(UOM_NM)) {
    			
    			return iliDetails;
    			
    		}
    		
    	}
    	
    	return null;
    	
    }
    
    private ArModInvoiceLineDetails containsInvoiceLine(String SML_NM, double UNT_PRC, ArModReceiptDetails details) {
    	
    	ArrayList ilList = details.getInvIlList();
    	
    	Iterator ilIter = ilList.iterator();
    	
    	while(ilIter.hasNext()) {
    		
    		ArModInvoiceLineDetails ilDetails = (ArModInvoiceLineDetails)ilIter.next();
    		
    		if(ilDetails.getIlSmlName().equals(SML_NM) && UNT_PRC == ilDetails.getIlUnitPrice()) {
    			
    			return ilDetails;
    			
    		}
    		
    	}
    	
    	return null;
    	
    }
    
    private ArModDistributionRecordDetails containsDistributionRecord(String DR_CLSS, ArModReceiptDetails details) {
    	
    	ArrayList drList = details.getRctDrList();
    	
    	Iterator drIter = drList.iterator();
    	
    	while(drIter.hasNext()) {
    		
    		ArModDistributionRecordDetails drDetails = (ArModDistributionRecordDetails)drIter.next();
    		
    		if(drDetails.getDrClass().equals(DR_CLSS)) {
    			
    			return drDetails;
    			
    		}
    		
    	}
    	
    	return null;
			
    }
    
    private ArModReceiptDetails containsReceipt(String RCPT_NMBR, ArrayList list) {
    	
    	Iterator i = list.iterator();
    	
    	while(i.hasNext()) {
    		
    		ArModReceiptDetails details = (ArModReceiptDetails)i.next();
    		
    		if(details.getRctNumber().equals(RCPT_NMBR)) {
    			
    			return details;
    			
    		}
    			
    	}
    	
    	return null;
    	
    }
    
    public ArModReceiptDetails isBankAccountSet(String RCPT_NMBR, ArrayList list) {
    	
    	Iterator i = list.iterator();
    	
    	while(i.hasNext()) {
    		
    		ArModReceiptDetails details = (ArModReceiptDetails)i.next();
    		
    		if(details.getRctNumber().equals(RCPT_NMBR) && details.getRctBaName() != null && !details.getRctBaName().equals("")) {
    			
    			return details;
    			
    		}
    		
    	}
    	
    	return null;
    	
    }
    
    private static Comparator sortRctByDocumentNumber = new Comparator() {
    	
    	public int compare(Object r1, Object r2) {
    		
    		ArModReceiptDetails receipt1 = (ArModReceiptDetails) r1;
    		ArModReceiptDetails receipt2 = (ArModReceiptDetails) r2;
    		
    		return receipt1.getRctNumber().compareTo(receipt2.getRctNumber());
    		
    	}
    	
    };
    
    private static Comparator sortAiByDocumentNumber = new Comparator() {
    	
    	public int compare(Object ai1, Object ai2) {
    		
    		ArModAppliedInvoiceDetails appInv1 = (ArModAppliedInvoiceDetails) ai1;
    		ArModAppliedInvoiceDetails appInv2 = (ArModAppliedInvoiceDetails) ai2;
    		
    		return appInv1.getAiIpsInvNumber().compareTo(appInv2.getAiIpsInvNumber());
    		
    	}
    	
    };
    
    private static Comparator sortIliByDocumentNumber = new Comparator() {
		
		public int compare(Object il1, Object il2) {
		    
			ArModInvoiceLineItemDetails invIli1 = (ArModInvoiceLineItemDetails) il1;
			ArModInvoiceLineItemDetails invIli2 = (ArModInvoiceLineItemDetails) il2;
		    
			return invIli1.getIliReceiptNumber().compareTo(invIli2.getIliReceiptNumber());
			
		}
		
	};
	
	private static Comparator sortIlByDocumentNumber = new Comparator() {
		
		public int compare(Object il1, Object il2) {
		    
			ArModInvoiceLineDetails invIl1 = (ArModInvoiceLineDetails) il1;
			ArModInvoiceLineDetails invIl2 = (ArModInvoiceLineDetails) il2;
		    
			return invIl1.getIlReceiptNumber().compareTo(invIl2.getIlReceiptNumber());
			
		}
		
	};
	
	private static Comparator sortDrByReceiptNumber = new Comparator() {
		
		public int compare(Object dr1, Object dr2) {
		    
			ArModDistributionRecordDetails rctDr1 = (ArModDistributionRecordDetails) dr1;
			ArModDistributionRecordDetails rctDr2 = (ArModDistributionRecordDetails) dr2;
		    
			return rctDr1.getDrReceiptNumber().compareTo(rctDr2.getDrReceiptNumber());
			
		}
		
	};
	
	private static Comparator sortRctByBankAccountName = new Comparator() {
		
		public int compare(Object rct1, Object rct2) {
		    
			ArModReceiptDetails receipt1 = (ArModReceiptDetails) rct1;
			ArModReceiptDetails receipt2 = (ArModReceiptDetails) rct2;
		    
			return receipt1.getRctBaName().compareTo(receipt2.getRctBaName());
			
		}
		
	};
	
}