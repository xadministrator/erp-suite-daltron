package com.struts.ar.receiptimport;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArReceiptImportForm extends ActionForm implements Serializable {
	
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	
	private String pageState = new String();
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private boolean importMiscReceipt = false;
	private ArrayList customerList = new ArrayList();
	private String customer = null;
	private boolean enableFields = false;
	private ArrayList miscReceiptTypeList = new ArrayList();
	private String miscReceiptType = null;
	private boolean isSummarized = false;
	private ArrayList taxCodeList = new ArrayList();
	private String taxCode = null;
	private boolean serviceChargeTaxable = false;
	private boolean discountTaxable = false;
	
	public FormFile getFilename1() {
		
		return filename1;
		
	}
	
	public void setFilename1(FormFile filename1) {
		
		this.filename1 = filename1;
		
	}
	
	public FormFile getFilename2() {
		
		return filename2;
		
	}
	
	public void setFilename2(FormFile filename2) {
		
		this.filename2 = filename2;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	} 
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public boolean getImportMiscReceipt() {
		
		return importMiscReceipt;
		
	}
	
	public void setImportMiscReceipt(boolean importMiscReceipt) {
		
		this.importMiscReceipt = importMiscReceipt;
		
	}
	
	public ArrayList getCustomerList() {
		
		return customerList;
		
	}
	
	public String getCustomer() {
		
		return customer;
		
	}
	
	public void setCustomer(String customer) {
		
		this.customer = customer;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public ArrayList getMiscReceiptTypeList() {
		
		return miscReceiptTypeList;
		
	}
	
	public void setCustomerList(String miscReceiptType) {
	
		customerList.add(miscReceiptType);
		
	}
	
	public void clearCustomerList() {
	
		customerList.clear();
		customerList.add(Constants.GLOBAL_BLANK);
		
    }
	
	public String getMiscReceiptType() {
		
		return miscReceiptType;
		
	}
	
	public void setMiscReceiptType(String miscReceiptType) {
		
		this.miscReceiptType = miscReceiptType;
		
	}
	
	public boolean getIsSummarized() {
		
		return isSummarized;
		
	}
	
	public void setIsSummarized(boolean isSummarized) {
		
		this.isSummarized = isSummarized;
		
	}
	
	public ArrayList getTaxCodeList() {
		
		return taxCodeList;
		
	}
	
	public void setTaxCodeList(String taxCode) {
		
		taxCodeList.add(taxCode);
		
	}
	
	public void clearTaxCodeList() {
		
		taxCodeList.clear();
		taxCodeList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getTaxCode() {
		
		return taxCode;
		
	}
	
	public void setTaxCode(String taxCode) {
		
		this.taxCode = taxCode;
		
	}
	
	public boolean getServiceChargeTaxable() {
		
		return serviceChargeTaxable;
		
	}
	
	public void setServiceChargeTaxable(boolean serviceChargeTaxable) {
		
		this.serviceChargeTaxable = serviceChargeTaxable;
		
	}
	
	public boolean getDiscountTaxable() {
		
		return discountTaxable;
		
	}
	
	public void setDiscountTaxable(boolean discountTaxable) {
		
		this.discountTaxable = discountTaxable;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){ 
		
		filename1 = null;
		filename2 = null;
		importMiscReceipt = false;
		customer = null;
		enableFields = false;
		miscReceiptType = null;
		taxCode = null;
		serviceChargeTaxable = false;
		discountTaxable = false;
		
		miscReceiptTypeList.clear();
		miscReceiptTypeList.add(Constants.GLOBAL_BLANK);
		miscReceiptTypeList.add("ITEMS");
		miscReceiptTypeList.add("MEMO LINES");
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if(request.getParameter("importButton") != null) {
			
			if(Common.validateRequired(filename1.getFileName())){
				errors.add("filename1",
						new ActionMessage("receiptImport.error.headerFileRequired"));
			}
			
			if(Common.validateRequired(filename2.getFileName())){
				errors.add("filename2",
						new ActionMessage("receiptImport.error.detailsFileRequired"));
			}
			
			if(!Common.validateRequired(filename1.getFileName()) && !filename1.getFileName().substring(filename1.getFileName().indexOf(".") + 1, filename1.getFileName().length()).equalsIgnoreCase("CSV")){
				errors.add("filename1",
						new ActionMessage("receiptImport.error.filenameInvalid"));
			}
			
			if(!Common.validateRequired(filename2.getFileName()) && !filename2.getFileName().substring(filename2.getFileName().indexOf(".") + 1, filename2.getFileName().length()).equalsIgnoreCase("CSV")){
				errors.add("filename2",
						new ActionMessage("receiptImport.error.filenameInvalid"));
			}
			
			if(importMiscReceipt == true && isSummarized == true && (customer == null || customer.equals(""))) {
				errors.add("customer",
						new ActionMessage("receiptImport.error.customerRequired"));
			}
			
			if(importMiscReceipt == true && (miscReceiptType == null || miscReceiptType.equals(""))) {
				errors.add("miscReceiptType",
						new ActionMessage("receiptImport.error.miscReceiptTypeRequired"));
			}
			
			if(importMiscReceipt == true && Common.validateRequired(taxCode)) {
				errors.add("taxCode",
						new ActionMessage("receiptImport.error.taxCodeRequired"));
			}
			
		}
		
		return errors;
		
	}
	
}