package com.struts.ar.receiptpost;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class ArReceiptPostForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList(); 
   private String customerCode = null;
   private ArrayList customerCodeList = new ArrayList();
   private boolean receiptVoid = false;
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String receiptType = null;
   private ArrayList receiptTypeList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String receiptNumberFrom = null;
   private String receiptNumberTo = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;
   private boolean showBatchName = false;
   private String pageState = new String();
   private ArrayList arRPList = new ArrayList();
   private int rowSelected = 0;
   private int rowILISelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean useCustomerPulldown = true;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public ArReceiptPostList getArRPByIndex(int index) {

      return((ArReceiptPostList)arRPList.get(index));

   }

   public Object[] getArRPList() {

      return arRPList.toArray();

   }

   public int getArRPListSize() {

      return arRPList.size();

   }

   public void saveArRPList(Object newArRPList) {

      arRPList.add(newArRPList);

   }
   
   public void deleteArRPList(int rowSelected) {

      arRPList.remove(rowSelected);

   }

   public void clearArRPList() {
   	
      arRPList.clear();
      
   }

   public void setRowSelected(Object selectedArRPList, boolean isEdit) {

      this.rowSelected = arRPList.indexOf(selectedArRPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void showArRPRow(int rowSelected) {

   }

   public void updateArRPRow(int rowSelected, Object newArIPList) {

   	arRPList.set(rowSelected, newArIPList);

   }
   
   public void setRowILISelected(Object selectedArILIList, boolean isEdit){
   	
    this.rowILISelected = arRPList.indexOf(selectedArILIList);
    
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }   

   public String getCustomerCode() {

      return customerCode;

   }

   public void setCustomerCode(String customerCode) {

      this.customerCode = customerCode;

   }

   public ArrayList getCustomerCodeList() {

      return customerCodeList;

   }

   public void setCustomerCodeList(String customerCode) {

      customerCodeList.add(customerCode);

   }
   
   public void clearCustomerCodeList() {

      customerCodeList.clear();
      customerCodeList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public boolean getReceiptVoid() {
   	
   	  return receiptVoid;
   	
   }
   
   public void setReceiptVoid(boolean receiptVoid) {
   	
   	  this.receiptVoid = receiptVoid;
   	
   }

   public String getBankAccount() {

      return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

      this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

      return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

      bankAccountList.add(bankAccount);

   }
   
   public void clearBankAccountList() {

      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }
   
   
   
   
   
   
   public String getReceiptType() {

	      return receiptType;

	   }

	   public void setReceiptType(String receiptType) {

	      this.receiptType = receiptType;

	   }

	   public ArrayList getReceiptTypeList() {

	      return receiptTypeList;

	   }

	   public void setReceiptTypeList(String receiptType) {

		   receiptTypeList.add(receiptType);

	   }
	   
	   public void clearReceiptTypeList() {

		   receiptTypeList.clear();
		   receiptTypeList.add(Constants.GLOBAL_BLANK);
	      
	   }
   
   
   
   

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }

   public String getReceiptNumberFrom() {

      return receiptNumberFrom;

   }

   public void setReceiptNumberFrom(String receiptNumberFrom) {

      this.receiptNumberFrom = receiptNumberFrom;

   }                         

   public String getReceiptNumberTo() {

      return receiptNumberTo;

   }

   public void setReceiptNumberTo(String receiptNumberTo) {

      this.receiptNumberTo = receiptNumberTo;

   }
 
   public String getCurrency() {
   	
   	  return currency;
   	  
   }
   
   public void setCurrency(String currency) {
   	
   	  this.currency = currency;
   	  
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   	     	
   }
   
   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }   
       
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getUseCustomerPulldown() {
   	
   	   return useCustomerPulldown;
   	   
   }
   
   public void setUseCustomerPulldown(boolean useCustomerPulldown) {
   	
   	  this.useCustomerPulldown = useCustomerPulldown;
   	  
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  batchName = Constants.GLOBAL_BLANK;
   	  customerCode = Constants.GLOBAL_BLANK;
   	  receiptVoid = false;
   	  bankAccount = Constants.GLOBAL_BLANK;
   	  receiptType = Constants.GLOBAL_BLANK;
   	  currency = Constants.GLOBAL_BLANK;
   	  dateFrom = null;
   	  dateTo = null;
   	  receiptNumberFrom = null;
   	  receiptNumberTo = null;
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("APPROVED");
      approvalStatusList.add("N/A");
      
      receiptTypeList.clear();
      receiptTypeList.add(Constants.GLOBAL_BLANK);
      receiptTypeList.add("COLLECTION");
      receiptTypeList.add("MISC");
      approvalStatus = Constants.GLOBAL_BLANK; 
      
      for (int i=0; i<arRPList.size(); i++) {
	  	
      	ArReceiptPostList actionList = (ArReceiptPostList)arRPList.get(i);
   	  	
   	  		actionList.setPost(false);
   	  		
   	  }
             
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("BANK ACCOUNT");
	      orderByList.add("CUSTOMER CODE");
	      orderByList.add("RECEIPT NUMBER");
	      orderBy = Constants.GLOBAL_BLANK;   
	  
	  }     
	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("receiptPost.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("receiptPost.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("receiptPost.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("receiptPost.error.maxRowsInvalid"));
		
		   }
	 	  
      }
      
      return errors;

   }
}