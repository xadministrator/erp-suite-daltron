	package com.struts.ar.salesorderentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.ArINVAmountExceedsCreditLimitException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalPaymentTermInvalidException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.AdDocumentSequenceAssignmentController;
import com.ejb.txn.AdDocumentSequenceAssignmentControllerHome;
import com.ejb.txn.AdLogController;
import com.ejb.txn.AdLogControllerHome;
import com.ejb.txn.ArSalesOrderEntryController;
import com.ejb.txn.ArSalesOrderEntryControllerHome;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.struts.ar.invoiceentry.ArInvoiceEntryList;
import com.struts.ar.invoiceentry.ArInvoiceLineItemList;
import com.struts.ar.invoiceentry.ArInvoiceSalesOrderList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.struts.util.ReportParameter;
import com.util.ArInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModSalesOrderDetails;
import com.util.ArModSalesOrderLineDetails;
import com.util.ArSalespersonDetails;
import com.util.ArTaxCodeDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModFunctionalCurrencyRateDetails;
import com.util.InvModLineItemDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public class ArSalesOrderEntryAction extends Action {

    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

	    HttpSession session = request.getSession();

	    try {

/*******************************************************
    Check if user has a session
 *******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);

          if (user != null) {

             if (log.isInfoEnabled()) {

                 log.info("ArSalesOrderEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                    "' performed this action on session " + session.getId());

             }

          }else{

             if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

             }

             return(mapping.findForward("adLogon"));

          }

          ArSalesOrderEntryForm actionForm = (ArSalesOrderEntryForm)form;

          actionForm.setReport(null);
          actionForm.setAttachment(null);
          actionForm.setAttachmentPDF(null);

 	      String frParam = null;

          frParam = Common.getUserPermission(user, Constants.AR_SALES_ORDER_ENTRY_ID);

          if (frParam != null) {

 	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

 	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

                if (!fieldErrors.isEmpty()) {

                   saveErrors(request, new ActionMessages(fieldErrors));
                   return(mapping.findForward("arSalesOrderEntry"));

                }

             }

             actionForm.setUserPermission(frParam.trim());

          } else {

             actionForm.setUserPermission(Constants.NO_ACCESS);

          }

 /*******************************************************
    Initialize ArSalesOrderEntryController EJB
 *******************************************************/

              ArSalesOrderEntryControllerHome homeSO = null;
              ArSalesOrderEntryController ejbSO = null;
              
              AdLogControllerHome homeLOG = null;
              AdLogController ejbLOG = null;
              
              GlDailyRateControllerHome homeFR = null;
              GlDailyRateController ejbFR = null;
              
              AdDocumentSequenceAssignmentControllerHome homeDSA = null;
              AdDocumentSequenceAssignmentController ejbDSA = null;

              try {

                 homeSO = (ArSalesOrderEntryControllerHome)com.util.EJBHomeFactory.
                     lookUpHome("ejb/ArSalesOrderEntryControllerEJB", ArSalesOrderEntryControllerHome.class);
                 
                 homeLOG = (AdLogControllerHome)com.util.EJBHomeFactory.
                         lookUpHome("ejb/AdLogControllerEJB", AdLogControllerHome.class);

                 homeFR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                         lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
                         
                 homeDSA = (AdDocumentSequenceAssignmentControllerHome)com.util.EJBHomeFactory.
                         lookUpHome("ejb/AdDocumentSequenceAssignmentControllerEJB", AdDocumentSequenceAssignmentControllerHome.class);
           
              } catch(NamingException e) {

              	if(log.isInfoEnabled()){
                     log.info("NamingException caught in ArSalesOrderEntryAction.execute(): " + e.getMessage() +
                    " session: " + session.getId());
                 }
                 return(mapping.findForward("cmnErrorPage"));

              }

              try {

                 ejbSO = homeSO.create();
                 ejbLOG = homeLOG.create();
                 ejbFR = homeFR.create();
                 ejbDSA = homeDSA.create();

              } catch(CreateException e) {

                 if(log.isInfoEnabled()) {

                     log.info("CreateException caught in ArSalesOrderEntryAction.execute(): " + e.getMessage() +
                        " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));

              }

              ActionErrors errors = new ActionErrors();
              ActionMessages messages = new ActionMessages();

/*******************************************************
   Call ArSalesOrderEntryController EJB
   getGlFcPrecisionUnit
   getInvGpprecisionUnitUnit
   getAdPrfArJournalLineNumber
   getAdPrfArUseCustomerPulldown
   getAdPrfArEnablePaymentTerm
*******************************************************/

              MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

              short precisionUnit = 0;

              short journalLineNumber = 0;
              boolean isInitialPrinting = false;
              boolean useCustomerPulldown = true;
              boolean enablePaymentTerm = true;
              boolean disableSalesPrice = false;
              boolean salespersonRequired = false;
              String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/ar/Sales Order/";
              long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
              String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
              String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
              try {


                 precisionUnit = ejbSO.getGlFcPrecisionUnit(user.getCmpCode());
                 journalLineNumber = ejbSO.getAdPrfArJournalLineNumber(user.getCmpCode());
                 useCustomerPulldown = Common.convertByteToBoolean(ejbSO.getAdPrfArUseCustomerPulldown(user.getCmpCode()));
                 actionForm.setUseCustomerPulldown(useCustomerPulldown);
                 enablePaymentTerm = Common.convertByteToBoolean(ejbSO.getAdPrfArEnablePaymentTerm(user.getCmpCode()));
                 actionForm.setEnablePaymentTerm(enablePaymentTerm);
                 disableSalesPrice = Common.convertByteToBoolean(ejbSO.getAdPrfArDisableSalesPrice(user.getCmpCode()));
                 salespersonRequired = Common.convertByteToBoolean(ejbSO.getAdPrfSalesOrderSalespersonRequired(user.getCmpCode()));
                 actionForm.setDisableSalesPrice(disableSalesPrice);
                 actionForm.setPrecisionUnit(precisionUnit);
                 actionForm.setSalespersonRequired(salespersonRequired);

              } catch(EJBException ex) {

                 if (log.isInfoEnabled()) {

                    log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
                       " session: " + session.getId());
                 }

                 return(mapping.findForward("cmnErrorPage"));

              }
/*******************************************************
      -- Ar SO Save As Draft Action ITEMS --
*******************************************************/

            if (request.getParameter("saveAsDraftButton") != null && /*actionForm.getType().equals("ITEMS") &&*/
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
              ArModSalesOrderDetails details = new ArModSalesOrderDetails();

              details.setSoCode(actionForm.getSalesOrderCode());
              details.setSoDate(Common.convertStringToSQLDate(actionForm.getDate()));
              details.setSoDocumentType(actionForm.getDocumentType());
              details.setSoDocumentNumber(actionForm.getDocumentNumber());
              details.setSoReferenceNumber(actionForm.getReferenceNumber());
              details.setSoTransactionType(actionForm.getTransactionType());
              details.setSoDescription(actionForm.getDescription());
              
              details.setSoShippingLine(actionForm.getShippingLine());
              details.setSoPort(actionForm.getPort());
              
              details.setSoVoid(Common.convertBooleanToByte(actionForm.getSalesOrderVoid()));
              details.setSoMobile(Common.convertBooleanToByte(actionForm.getSalesOrderMobile()));

              details.setSoMemo(actionForm.getMemo());
              details.setSoTransactionStatus(actionForm.getTransactionStatus());
              details.setSoBillTo(actionForm.getBillTo());
              details.setSoShipTo(actionForm.getShipTo());

              details.setSoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
              details.setSoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

              if (actionForm.getSalesOrderCode() == null) {

	          	   details.setSoCreatedBy(user.getUserName());
	   	           details.setSoDateCreated(new java.util.Date());

              }

              details.setSoLastModifiedBy(user.getUserName());
              details.setSoDateLastModified(new java.util.Date());

              details.setSoPytName(actionForm.getPaymentTerm());
              details.setSoTcName(actionForm.getTaxCode());
              details.setSoFcName(actionForm.getCurrency());
              details.setSoCstCustomerCode(actionForm.getCustomer());
              details.setSoSlpSalespersonCode(actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_BLANK) ||
          	  actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_NO_RECORD_FOUND) ? null :
              actionForm.getSalesperson());

              details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));


              ArrayList solList = new ArrayList();

              for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

            	  ArSalesOrderLineItemList arSOLList = actionForm.getArSOLByIndex(i);

              	   if (Common.validateRequired(arSOLList.getLocation()) &&
              	   	   Common.validateRequired(arSOLList.getItemName()) &&
	   				   Common.validateRequired(arSOLList.getUnit()) &&
	   				   Common.validateRequired(arSOLList.getUnitPrice()) &&
	   				   Common.validateRequired(arSOLList.getQuantity())) continue;

              	   ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();


					   String itemDesc = arSOLList.getItemDescription();


              	   mdetails.setSolLine(Common.convertStringToShort(arSOLList.getLineNumber()));
              	   mdetails.setSolIiName(arSOLList.getItemName());
              	   mdetails.setSolLocName(arSOLList.getLocation());
              	   mdetails.setSolLineIDesc(itemDesc);
              	   mdetails.setSolIiDescription(arSOLList.getItemDescription());
              	   mdetails.setSolQuantity(Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit));
              	   mdetails.setSolUomName(arSOLList.getUnit());
              	   mdetails.setSolUnitPrice(Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit));
              	   mdetails.setSolAmount(Common.convertStringMoneyToDouble(arSOLList.getAmount(), precisionUnit));
               	   mdetails.setSolDiscount1(Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit));
               	   mdetails.setSolDiscount2(Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit));
               	   mdetails.setSolDiscount3(Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit));
               	   mdetails.setSolDiscount4(Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit));
               	   mdetails.setSolTotalDiscount(Common.convertStringMoneyToDouble(arSOLList.getTotalDiscount(), precisionUnit));
               	   mdetails.setSolMisc(arSOLList.getMisc());
               	   mdetails.setSolTax(Common.convertBooleanToByte(arSOLList.getTax().equals("Y")?true:false));



                   ArrayList tagList = new ArrayList();
               	   //TODO:save and submit taglist
               	   boolean isTraceMisc = ejbSO.getSoTraceMisc(arSOLList.getItemName(), user.getCmpCode());
    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
    				String misc= arSOLList.getMisc();

            	   if (isTraceMisc == true){


            		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
            		   mdetails.setSolTagList(tagList);


            	   }


               	   solList.add(mdetails);

              }
              //validate attachment
              String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
              String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
              String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
              String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

              if (!Common.validateRequired(filename1)) {

      	    	if (actionForm.getFilename1().getFileSize() == 0) {

      	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("salesOrderEntry.error.filename1NotFound"));

      	    	} else {

      	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

             	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
             	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename1Invalid"));

             	    	}

             	    	InputStream is = actionForm.getFilename1().getInputStream();

             	    	if (is.available() > maxAttachmentFileSize) {

             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename1SizeInvalid"));

             	    	}

             	    	is.close();

      	    	}

      	    	if (!errors.isEmpty()) {

      	    		saveErrors(request, new ActionMessages(errors));
             			return (mapping.findForward("arSalesOrderEntry"));

      	    	}

      	   }

      	   if (!Common.validateRequired(filename2)) {

      	    	if (actionForm.getFilename2().getFileSize() == 0) {

      	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("salesOrderEntry.error.filename2NotFound"));

      	    	} else {

      	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

      	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
             	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename2Invalid"));

             	    	}

             	    	InputStream is = actionForm.getFilename2().getInputStream();

             	    	if (is.available() > maxAttachmentFileSize) {

             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename2SizeInvalid"));

             	    	}

             	    	is.close();

      	    	}

      	    	if (!errors.isEmpty()) {

      	    		saveErrors(request, new ActionMessages(errors));
             			return (mapping.findForward("arSalesOrderEntry"));

      	    	}

      	   }

      	   if (!Common.validateRequired(filename3)) {

      	    	if (actionForm.getFilename3().getFileSize() == 0) {

      	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("salesOrderEntry.error.filename3NotFound"));

      	    	} else {

      	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

      	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename3Invalid"));

             	    	}

             	    	InputStream is = actionForm.getFilename3().getInputStream();

             	    	if (is.available() > maxAttachmentFileSize) {

             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename3SizeInvalid"));

             	    	}

             	    	is.close();

      	    	}

      	    	if (!errors.isEmpty()) {

      	    		saveErrors(request, new ActionMessages(errors));
             			return (mapping.findForward("arSalesOrderEntry"));

      	    	}

      	   }

      	   if (!Common.validateRequired(filename4)) {

      	    	if (actionForm.getFilename4().getFileSize() == 0) {

      	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("salesOrderEntry.error.filename4NotFound"));

      	    	} else {

      	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

      	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
      	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename4Invalid"));

             	    	}

             	    	InputStream is = actionForm.getFilename4().getInputStream();

             	    	if (is.available() > maxAttachmentFileSize) {

             	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                      		new ActionMessage("salesOrderEntry.error.filename4SizeInvalid"));

             	    	}

             	    	is.close();

      	    	}

      	    	if (!errors.isEmpty()) {

      	    		saveErrors(request, new ActionMessages(errors));
             			return (mapping.findForward("arSalesOrderEntry"));

      	    	}

      	   	}

              try {

              	    Integer salesOrderCode = ejbSO.saveArSoEntry(details, actionForm.getPaymentTerm(),
              	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), solList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

              	    actionForm.setSalesOrderCode(salesOrderCode);
              	    
                  ejbLOG.addAdLogEntry(salesOrderCode,"AR SALES ORDER ENTRY",  new java.util.Date(),user.getUserName(),"SAVE AS DRAFT", actionForm.getLogEntry(),  user.getCmpCode());
              	    

              } catch (GlobalRecordAlreadyDeletedException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("salesOrderEntry.error.recordAlreadyDeleted"));

              } catch (GlobalDocumentNumberNotUniqueException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("salesOrderEntry.error.documentNumberNotUnique"));

              } catch (GlobalConversionDateNotExistException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("salesOrderEntry.error.conversionDateNotExist"));

              } catch (GlobalPaymentTermInvalidException ex) {

              	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("salesOrderEntry.error.paymentTermInvalid"));

              } catch (GlobalTransactionAlreadyPendingException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("salesOrderEntry.error.transactionAlreadyPending"));

              } catch (GlobalTransactionAlreadyVoidException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("salesOrderEntry.error.transactionAlreadyVoid"));

              } catch (GlobalInvItemLocationNotFoundException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                       new ActionMessage("salesOrderEntry.error.noItemLocationFound", ex.getMessage()));

              } catch (GlobalNoApprovalApproverFoundException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("salesOrderEntry.error.noApprovalApproverFound"));

              } catch (GlobalNoApprovalRequesterFoundException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("salesOrderEntry.error.noApprovalRequesterFound"));

              } catch (GlobalRecordAlreadyAssignedException ex) {

              		errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("salesOrderEntry.error.transactionIsAlreadyAssigned"));


              } catch (EJBException ex) {
              	    if (log.isInfoEnabled()) {

                     log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
                        " session: " + session.getId());
                   }

                  return(mapping.findForward("cmnErrorPage"));
              }

              if (!errors.isEmpty()) {

	   	    	   saveErrors(request, new ActionMessages(errors));
	   	   		   return (mapping.findForward("arSalesOrderEntry"));

   	          }

           // save attachment

              if (!Common.validateRequired(filename1)) {

          	        if (errors.isEmpty()) {

          	        	InputStream is = actionForm.getFilename1().getInputStream();

          	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

          	        	new File(attachmentPath).mkdirs();

          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

          	    			fileExtension = attachmentFileExtension;

              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
              	    		fileExtension = attachmentFileExtensionPDF;

              	    	}
          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-1" + fileExtension);

          	    		int c;

   	            	while ((c = is.read()) != -1) {

   	            		fos.write((byte)c);

   	            	}

   	            	is.close();
   					fos.close();

          	        }

          	   }

          	   if (!Common.validateRequired(filename2)) {

          	        if (errors.isEmpty()) {

          	        	InputStream is = actionForm.getFilename2().getInputStream();

          	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

          	        	new File(attachmentPath).mkdirs();

          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

          	    			fileExtension = attachmentFileExtension;

              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
              	    		fileExtension = attachmentFileExtensionPDF;

              	    	}
          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-2" + fileExtension);
   	            	int c;

   	            	while ((c = is.read()) != -1) {

   	            		fos.write((byte)c);

   	            	}

   	            	is.close();
   					fos.close();

          	        }

          	   }

          	   if (!Common.validateRequired(filename3)) {

          	        if (errors.isEmpty()) {

          	        	InputStream is = actionForm.getFilename3().getInputStream();

          	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

          	        	new File(attachmentPath).mkdirs();

          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

          	    			fileExtension = attachmentFileExtension;

              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
              	    		fileExtension = attachmentFileExtensionPDF;

              	    	}
          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-3" + fileExtension);

   	            	int c;

   	            	while ((c = is.read()) != -1) {

   	            		fos.write((byte)c);

   	            	}

   	            	is.close();
   					fos.close();

          	        }

          	   }

          	   if (!Common.validateRequired(filename4)) {

          	        if (errors.isEmpty()) {

          	        	InputStream is = actionForm.getFilename4().getInputStream();

          	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

          	        	new File(attachmentPath).mkdirs();

          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

          	    			fileExtension = attachmentFileExtension;

              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
              	    		fileExtension = attachmentFileExtensionPDF;

              	    	}
          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-4" + fileExtension);

   	            	int c;

   	            	while ((c = is.read()) != -1) {

   	            		fos.write((byte)c);

   	            	}

   	            	is.close();
   					fos.close();

          	        }

          	   }


/*******************************************************
      -- Ar SO Save & Submit Action ITEMS--
*******************************************************/

            } else if (request.getParameter("saveSubmitButton") != null && /*actionForm.getType().equals("ITEMS") &&*/
            		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            	ArModSalesOrderDetails details = new ArModSalesOrderDetails();

                details.setSoCode(actionForm.getSalesOrderCode());
                details.setSoDate(Common.convertStringToSQLDate(actionForm.getDate()));
                details.setSoDocumentType(actionForm.getDocumentType());
                details.setSoDocumentNumber(actionForm.getDocumentNumber());
                details.setSoReferenceNumber(actionForm.getReferenceNumber());
                details.setSoTransactionType(actionForm.getTransactionType());
                details.setSoDescription(actionForm.getDescription());
                details.setSoShippingLine(actionForm.getShippingLine());
                details.setSoPort(actionForm.getPort());
                details.setSoVoid(Common.convertBooleanToByte(actionForm.getSalesOrderVoid()));
                details.setSoMobile(Common.convertBooleanToByte(actionForm.getSalesOrderMobile()));
                details.setSoMemo(actionForm.getMemo());
                details.setSoTransactionStatus(actionForm.getTransactionStatus());
                details.setSoBillTo(actionForm.getBillTo());
                details.setSoShipTo(actionForm.getShipTo());

                details.setSoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                details.setSoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

                if (actionForm.getSalesOrderCode() == null) {

  	          	   details.setSoCreatedBy(user.getUserName());
  	   	           details.setSoDateCreated(new java.util.Date());

                }

                details.setSoLastModifiedBy(user.getUserName());
                details.setSoDateLastModified(new java.util.Date());

                details.setSoPytName(actionForm.getPaymentTerm());
                details.setSoTcName(actionForm.getTaxCode());
                details.setSoFcName(actionForm.getCurrency());
                details.setSoCstCustomerCode(actionForm.getCustomer());
                details.setSoSlpSalespersonCode(actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_BLANK) ||
                	actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_NO_RECORD_FOUND) ? null :
                	actionForm.getSalesperson());

                details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

                ArrayList solList = new ArrayList();

                for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

                	ArSalesOrderLineItemList arSOLList = actionForm.getArSOLByIndex(i);

            	   if (Common.validateRequired(arSOLList.getLocation()) &&
            	   	   Common.validateRequired(arSOLList.getItemName()) &&
   				   Common.validateRequired(arSOLList.getUnit()) &&
   				   Common.validateRequired(arSOLList.getUnitPrice()) &&
   				   Common.validateRequired(arSOLList.getQuantity())) continue;

            	   boolean chker = arSOLList.getEnableItemDesc();
					String itemDesc = "";


						itemDesc = arSOLList.getItemDescription();


            	   ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

            	   mdetails.setSolLine(Common.convertStringToShort(arSOLList.getLineNumber()));
            	   mdetails.setSolIiName(arSOLList.getItemName());
            	   mdetails.setSolLocName(arSOLList.getLocation());
            	   mdetails.setSolLineIDesc(itemDesc);
            	   mdetails.setSolIiDescription(arSOLList.getItemDescription());
            	   mdetails.setSolQuantity(Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit));
            	   mdetails.setSolUomName(arSOLList.getUnit());
            	   mdetails.setSolUnitPrice(Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit));
            	   mdetails.setSolAmount(Common.convertStringMoneyToDouble(arSOLList.getAmount(), precisionUnit));
               	   mdetails.setSolDiscount1(Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit));
               	   mdetails.setSolDiscount2(Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit));
               	   mdetails.setSolDiscount3(Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit));
               	   mdetails.setSolDiscount4(Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit));
               	   mdetails.setSolTotalDiscount(Common.convertStringMoneyToDouble(arSOLList.getTotalDiscount(), precisionUnit));
               	   mdetails.setSolMisc(arSOLList.getMisc());
               	   mdetails.setSolTax(Common.convertBooleanToByte(arSOLList.getTax().equals("Y")?true:false));

               	  ArrayList tagList = new ArrayList();
              	   //TODO:save and submit taglist
              	   boolean isTraceMisc = ejbSO.getSoTraceMisc(arSOLList.getItemName(), user.getCmpCode());
   	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
   				String misc= arSOLList.getMisc();

   			   if (isTraceMisc == true){


        		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
        		   mdetails.setSolTagList(tagList);


        	   }



               	   solList.add(mdetails);

                }

                //validate attachment

                String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

                if (!Common.validateRequired(filename1)) {

          	    	if (actionForm.getFilename1().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("salesOrderEntry.error.filename1NotFound"));

          	    	} else {

          	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

                 	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                 	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename1Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename1().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename1SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arSalesOrderEntry"));

          	    	}

          	   }

          	   if (!Common.validateRequired(filename2)) {

          	    	if (actionForm.getFilename2().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("salesOrderEntry.error.filename2NotFound"));

          	    	} else {

          	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
                 	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename2Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename2().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename2SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arSalesOrderEntry"));

          	    	}

          	   }

          	   if (!Common.validateRequired(filename3)) {

          	    	if (actionForm.getFilename3().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("salesOrderEntry.error.filename3NotFound"));

          	    	} else {

          	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
          	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename3Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename3().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename3SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arSalesOrderEntry"));

          	    	}

          	   }

          	   if (!Common.validateRequired(filename4)) {

          	    	if (actionForm.getFilename4().getFileSize() == 0) {

          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("salesOrderEntry.error.filename4NotFound"));

          	    	} else {

          	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
          	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename4Invalid"));

                 	    	}

                 	    	InputStream is = actionForm.getFilename4().getInputStream();

                 	    	if (is.available() > maxAttachmentFileSize) {

                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                          		new ActionMessage("salesOrderEntry.error.filename4SizeInvalid"));

                 	    	}

                 	    	is.close();

          	    	}

          	    	if (!errors.isEmpty()) {

          	    		saveErrors(request, new ActionMessages(errors));
                 			return (mapping.findForward("arSalesOrderEntry"));

          	    	}

          	   	}
                try {

	        	    Integer salesOrderCode = ejbSO.saveArSoEntry(details, actionForm.getPaymentTerm(),
	        	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), solList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	        	    actionForm.setSalesOrderCode(salesOrderCode);
	        	    
	        	    ejbLOG.addAdLogEntry(salesOrderCode,"AR SALES ORDER ENTRY",  new java.util.Date(),user.getUserName(),"SAVE AND SUBMIT", actionForm.getLogEntry(),  user.getCmpCode());
	              	 

                } catch (GlobalRecordAlreadyDeletedException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("salesOrderEntry.error.recordAlreadyDeleted"));

                } catch (GlobalDocumentNumberNotUniqueException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("salesOrderEntry.error.documentNumberNotUnique"));

                } catch (GlobalConversionDateNotExistException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("salesOrderEntry.error.conversionDateNotExist"));

                } catch (GlobalPaymentTermInvalidException ex) {

                	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("salesOrderEntry.error.paymentTermInvalid"));

                } catch (GlobalTransactionAlreadyVoidException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("salesOrderEntry.error.transactionAlreadyVoid"));

                } catch (GlobalInvItemLocationNotFoundException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("salesOrderEntry.error.noItemLocationFound", ex.getMessage()));

                } catch (GlobalNoApprovalApproverFoundException ex) {

                   	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("salesOrderEntry.error.noApprovalApproverFound"));

                } catch (GlobalNoApprovalRequesterFoundException ex) {

                		errors.add(ActionMessages.GLOBAL_MESSAGE,
                			new ActionMessage("salesOrderEntry.error.noApprovalRequesterFound"));

                } catch (GlobalRecordAlreadyAssignedException ex) {

                  		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  			new ActionMessage("salesOrderEntry.error.transactionIsAlreadyAssigned"));

                } catch (GlobalTransactionAlreadyPendingException ex) {

                      errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("salesOrderEntry.error.transactionAlreadyPending"));

                } catch (EJBException ex) {
                	    if (log.isInfoEnabled()) {

                       log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
                          " session: " + session.getId());
                     }

                    return(mapping.findForward("cmnErrorPage"));
                }

                if (!errors.isEmpty()) {

  	   	    	   saveErrors(request, new ActionMessages(errors));
  	   	   		   return (mapping.findForward("arSalesOrderEntry"));

     	        }

             // save attachment

                if (!Common.validateRequired(filename1)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename1().getInputStream();

            	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-1" + fileExtension);

            	    		int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename2)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename2().getInputStream();

            	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-2" + fileExtension);
     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename3)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename3().getInputStream();

            	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-3" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

            	   if (!Common.validateRequired(filename4)) {

            	        if (errors.isEmpty()) {

            	        	InputStream is = actionForm.getFilename4().getInputStream();

            	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

            	        	new File(attachmentPath).mkdirs();

            	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

            	    			fileExtension = attachmentFileExtension;

                	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
                	    		fileExtension = attachmentFileExtensionPDF;

                	    	}
            	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-4" + fileExtension);

     	            	int c;

     	            	while ((c = is.read()) != -1) {

     	            		fos.write((byte)c);

     	            	}

     	            	is.close();
     					fos.close();

            	        }

            	   }

/*******************************************************
   	 -- Ar SO Print Action ITEMS --
*******************************************************/

		        } else if (request.getParameter("printButton") != null /*&& actionForm.getType().equals("ITEMS")*/) {

		        	if(Common.validateRequired(actionForm.getApprovalStatus())) {

		        		ArModSalesOrderDetails details = new ArModSalesOrderDetails();

			          	details.setSoCode(actionForm.getSalesOrderCode());
			          	details.setSoDate(Common.convertStringToSQLDate(actionForm.getDate()));
			          	 details.setSoDocumentType(actionForm.getDocumentType());
			          	details.setSoDocumentNumber(actionForm.getDocumentNumber());
			          	details.setSoReferenceNumber(actionForm.getReferenceNumber());
			          	details.setSoTransactionType(actionForm.getTransactionType());
			          	details.setSoDescription(actionForm.getDescription());
			          	details.setSoShippingLine(actionForm.getShippingLine());
			              details.setSoPort(actionForm.getPort());
			          	details.setSoVoid(Common.convertBooleanToByte(actionForm.getSalesOrderVoid()));
			          	details.setSoMobile(Common.convertBooleanToByte(actionForm.getSalesOrderMobile()));
			          	details.setSoMemo(actionForm.getMemo());
			          	details.setSoTransactionStatus(actionForm.getTransactionStatus());
			          	details.setSoBillTo(actionForm.getBillTo());
			          	details.setSoShipTo(actionForm.getShipTo());

			          	details.setSoConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
			          	details.setSoConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));

			          	if (actionForm.getSalesOrderCode() == null) {

			          		details.setSoCreatedBy(user.getUserName());
			          		details.setSoDateCreated(new java.util.Date());

			          	}

			          	details.setSoLastModifiedBy(user.getUserName());
			          	details.setSoDateLastModified(new java.util.Date());

			          	details.setSoPytName(actionForm.getPaymentTerm());
			          	details.setSoTcName(actionForm.getTaxCode());
			          	details.setSoFcName(actionForm.getCurrency());
			          	details.setSoCstCustomerCode(actionForm.getCustomer());
		                details.setSoSlpSalespersonCode(
		                	actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_BLANK) ||
		                    actionForm.getSalesperson().equalsIgnoreCase(Constants.GLOBAL_NO_RECORD_FOUND) ? null :
		                    actionForm.getSalesperson());

		                details.setReportParameter(Common.convertReportParametersToString(actionForm.getReportParametersAsArrayList()));

			          	ArrayList solList = new ArrayList();

			          	for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

			          		ArSalesOrderLineItemList arSOLList = actionForm.getArSOLByIndex(i);

			          		if (Common.validateRequired(arSOLList.getLocation()) &&
			          		    Common.validateRequired(arSOLList.getItemName()) &&
								Common.validateRequired(arSOLList.getUnit()) &&
								Common.validateRequired(arSOLList.getUnitPrice()) &&
								Common.validateRequired(arSOLList.getQuantity())) continue;

			          		boolean chker = arSOLList.getEnableItemDesc();
							String itemDesc = "";

							System.out.println(arSOLList.getItemDescription());
							System.out.println("checker"+chker);

								itemDesc = arSOLList.getItemDescription();



			          		ArModSalesOrderLineDetails mdetails = new ArModSalesOrderLineDetails();

			          		mdetails.setSolLine(Common.convertStringToShort(arSOLList.getLineNumber()));
			          		mdetails.setSolIiName(arSOLList.getItemName());
			          		mdetails.setSolLocName(arSOLList.getLocation());
			          		mdetails.setSolLineIDesc(itemDesc);
			          		mdetails.setSolIiDescription(arSOLList.getItemDescription());
			          		mdetails.setSolQuantity(Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit));
			          		mdetails.setSolUomName(arSOLList.getUnit());
			          		mdetails.setSolUnitPrice(Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit));
			          		mdetails.setSolAmount(Common.convertStringMoneyToDouble(arSOLList.getAmount(), precisionUnit));
			               	mdetails.setSolDiscount1(Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit));
			               	mdetails.setSolDiscount2(Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit));
			               	mdetails.setSolDiscount3(Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit));
			               	mdetails.setSolDiscount4(Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit));
			               	mdetails.setSolTotalDiscount(Common.convertStringMoneyToDouble(arSOLList.getTotalDiscount(), precisionUnit));
			               	mdetails.setSolMisc(arSOLList.getMisc());
			               	mdetails.setSolTax(Common.convertBooleanToByte(arSOLList.getTax().equals("Y")?true:false));
							
							
							
							
			                ArrayList tagList = new ArrayList();
			               	   //TODO:save and submit taglist
			               	   boolean isTraceMisc = ejbSO.getSoTraceMisc(arSOLList.getItemName(), user.getCmpCode());
			    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
			    				String misc= arSOLList.getMisc();

			    				   if (isTraceMisc == true){


			                		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), "IN");
			                		//   mdetails.setSolTagList(tagList);


			                	   }

			               	solList.add(mdetails);

			          	}
			           	//validate attachment
			          	String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
		                String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
		                String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
		                String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

		                if (!Common.validateRequired(filename1)) {

		          	    	if (actionForm.getFilename1().getFileSize() == 0) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  			new ActionMessage("salesOrderEntry.error.filename1NotFound"));

		          	    	} else {

		          	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));

		                 	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		                 	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename1Invalid"));

		                 	    	}

		                 	    	InputStream is = actionForm.getFilename1().getInputStream();

		                 	    	if (is.available() > maxAttachmentFileSize) {

		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename1SizeInvalid"));

		                 	    	}

		                 	    	is.close();

		          	    	}

		          	    	if (!errors.isEmpty()) {

		          	    		saveErrors(request, new ActionMessages(errors));
		                 			return (mapping.findForward("arSalesOrderEntry"));

		          	    	}

		          	   }

		          	   if (!Common.validateRequired(filename2)) {

		          	    	if (actionForm.getFilename2().getFileSize() == 0) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  			new ActionMessage("salesOrderEntry.error.filename2NotFound"));

		          	    	} else {

		          	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));

		          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		                 	    			&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename2Invalid"));

		                 	    	}

		                 	    	InputStream is = actionForm.getFilename2().getInputStream();

		                 	    	if (is.available() > maxAttachmentFileSize) {

		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename2SizeInvalid"));

		                 	    	}

		                 	    	is.close();

		          	    	}

		          	    	if (!errors.isEmpty()) {

		          	    		saveErrors(request, new ActionMessages(errors));
		                 			return (mapping.findForward("arSalesOrderEntry"));

		          	    	}

		          	   }

		          	   if (!Common.validateRequired(filename3)) {

		          	    	if (actionForm.getFilename3().getFileSize() == 0) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  			new ActionMessage("salesOrderEntry.error.filename3NotFound"));

		          	    	} else {

		          	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));

		          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename3Invalid"));

		                 	    	}

		                 	    	InputStream is = actionForm.getFilename3().getInputStream();

		                 	    	if (is.available() > maxAttachmentFileSize) {

		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename3SizeInvalid"));

		                 	    	}

		                 	    	is.close();

		          	    	}

		          	    	if (!errors.isEmpty()) {

		          	    		saveErrors(request, new ActionMessages(errors));
		                 			return (mapping.findForward("arSalesOrderEntry"));

		          	    	}

		          	   }

		          	   if (!Common.validateRequired(filename4)) {

		          	    	if (actionForm.getFilename4().getFileSize() == 0) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  			new ActionMessage("salesOrderEntry.error.filename4NotFound"));

		          	    	} else {

		          	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));

		          	    		if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())
		          	    				&& !fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase()) ) {

		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename4Invalid"));

		                 	    	}

		                 	    	InputStream is = actionForm.getFilename4().getInputStream();

		                 	    	if (is.available() > maxAttachmentFileSize) {

		                 	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                          		new ActionMessage("salesOrderEntry.error.filename4SizeInvalid"));

		                 	    	}

		                 	    	is.close();

		          	    	}

		          	    	if (!errors.isEmpty()) {

		          	    		saveErrors(request, new ActionMessages(errors));
		                 			return (mapping.findForward("arSalesOrderEntry"));

		          	    	}

		          	   	}

			          try {

			          	    Integer salesOrderCode = ejbSO.saveArSoEntry(details, actionForm.getPaymentTerm(),
			          	        actionForm.getTaxCode(), actionForm.getCurrency(), actionForm.getCustomer(), solList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

			          	    actionForm.setSalesOrderCode(salesOrderCode);
			          	    
			          	  ejbLOG.addAdLogEntry(salesOrderCode,"AR SALES ORDER ENTRY",  new java.util.Date(),user.getUserName(),"PRINT", actionForm.getLogEntry(),  user.getCmpCode());
			              	 

			          } catch (GlobalRecordAlreadyDeletedException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("salesOrderEntry.error.recordAlreadyDeleted"));

			          } catch (GlobalDocumentNumberNotUniqueException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("salesOrderEntry.error.documentNumberNotUnique"));

			          } catch (GlobalConversionDateNotExistException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("salesOrderEntry.error.conversionDateNotExist"));

			          } catch (GlobalPaymentTermInvalidException ex) {

			          	    errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("salesOrderEntry.error.paymentTermInvalid"));

			          } catch (GlobalTransactionAlreadyVoidException ex) {

			          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("salesOrderEntry.error.transactionAlreadyVoid"));

			          } catch (GlobalInvItemLocationNotFoundException ex) {

			          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
			                   new ActionMessage("salesOrderEntry.error.noItemLocationFound", ex.getMessage()));

		              } catch (GlobalNoApprovalApproverFoundException ex) {

		               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                        new ActionMessage("salesOrderEntry.error.noApprovalApproverFound"));

		              } catch (GlobalNoApprovalRequesterFoundException ex) {

		              		errors.add(ActionMessages.GLOBAL_MESSAGE,
		              			new ActionMessage("salesOrderEntry.error.noApprovalRequesterFound"));

		              } catch (GlobalRecordAlreadyAssignedException ex) {

		              		errors.add(ActionMessages.GLOBAL_MESSAGE,
		              			new ActionMessage("salesOrderEntry.error.transactionIsAlreadyAssigned"));

		              } catch (GlobalTransactionAlreadyPendingException ex) {

		                    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                       new ActionMessage("salesOrderEntry.error.transactionAlreadyPending"));

			          } catch (EJBException ex) {
			          	    if (log.isInfoEnabled()) {

			                 log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
			                    " session: " + session.getId());
			               }

			              return(mapping.findForward("cmnErrorPage"));

			          }

			           if (!errors.isEmpty()) {

				    	   saveErrors(request, new ActionMessages(errors));
				   		   return (mapping.findForward("arSalesOrderEntry"));

				       }

				       actionForm.setReport(Constants.STATUS_SUCCESS);

			           isInitialPrinting = true;
			        // save attachment

			              if (!Common.validateRequired(filename1)) {

			          	        if (errors.isEmpty()) {

			          	        	InputStream is = actionForm.getFilename1().getInputStream();

			          	        	String fileExtension = filename1.substring(filename1.lastIndexOf("."));

			          	        	new File(attachmentPath).mkdirs();

			          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			          	    			fileExtension = attachmentFileExtension;

			              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			              	    		fileExtension = attachmentFileExtensionPDF;

			              	    	}
			          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-1" + fileExtension);

			          	    		int c;

			   	            	while ((c = is.read()) != -1) {

			   	            		fos.write((byte)c);

			   	            	}

			   	            	is.close();
			   					fos.close();

			          	        }

			          	   }

			          	   if (!Common.validateRequired(filename2)) {

			          	        if (errors.isEmpty()) {

			          	        	InputStream is = actionForm.getFilename2().getInputStream();

			          	        	String fileExtension = filename2.substring(filename2.lastIndexOf("."));

			          	        	new File(attachmentPath).mkdirs();

			          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			          	    			fileExtension = attachmentFileExtension;

			              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			              	    		fileExtension = attachmentFileExtensionPDF;

			              	    	}
			          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-2" + fileExtension);
			   	            	int c;

			   	            	while ((c = is.read()) != -1) {

			   	            		fos.write((byte)c);

			   	            	}

			   	            	is.close();
			   					fos.close();

			          	        }

			          	   }

			          	   if (!Common.validateRequired(filename3)) {

			          	        if (errors.isEmpty()) {

			          	        	InputStream is = actionForm.getFilename3().getInputStream();

			          	        	String fileExtension = filename3.substring(filename3.lastIndexOf("."));

			          	        	new File(attachmentPath).mkdirs();

			          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			          	    			fileExtension = attachmentFileExtension;

			              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			              	    		fileExtension = attachmentFileExtensionPDF;

			              	    	}
			          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-3" + fileExtension);

			   	            	int c;

			   	            	while ((c = is.read()) != -1) {

			   	            		fos.write((byte)c);

			   	            	}

			   	            	is.close();
			   					fos.close();

			          	        }

			          	   }

			          	   if (!Common.validateRequired(filename4)) {

			          	        if (errors.isEmpty()) {

			          	        	InputStream is = actionForm.getFilename4().getInputStream();

			          	        	String fileExtension = filename4.substring(filename4.lastIndexOf("."));

			          	        	new File(attachmentPath).mkdirs();

			          	    		if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

			          	    			fileExtension = attachmentFileExtension;

			              	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
			              	    		fileExtension = attachmentFileExtensionPDF;

			              	    	}
			          	    		FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getSalesOrderCode() + "-4" + fileExtension);

			   	            	int c;

			   	            	while ((c = is.read()) != -1) {

			   	            		fos.write((byte)c);

			   	            	}

			   	            	is.close();
			   					fos.close();

			          	        }

			          	   }

			        }  else {

			        	actionForm.setReport(Constants.STATUS_SUCCESS);

			        	return(mapping.findForward("arSalesOrderEntry"));

			        }


/*******************************************************
    -- Ar Delivery Action --
******************************************************/
            } if (request.getParameter("deliveryButton") != null ) {
            		
            		     	   
            	
            	   String path = "/arDelivery.do?forward=1" +
            				"&salesOrderCode=" + String.valueOf(actionForm.getSalesOrderCode()) ;

                    	System.out.print(path);
                    	return(new ActionForward(path));
/*******************************************************
   -- AR SO Delete Action --
*******************************************************/

	         } else if(request.getParameter("deleteButton") != null) {

	            try {
	           	System.out.println("--------------Start");
	           	    ejbSO.deleteArSoEntry(actionForm.getSalesOrderCode(), user.getUserName(), user.getCmpCode());
	           	 System.out.println("--------------End");
	           	 
	           	 	ejbLOG.addAdLogEntry(actionForm.getSalesOrderCode(),"AR SALES ORDER ENTRY",  new java.util.Date(),user.getUserName(),"DELETE", actionForm.getLogEntry(),  user.getCmpCode());
              	 
	            } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("salesOrderEntry.error.recordAlreadyDeleted"));

	            } catch (EJBException ex) {

	           	    if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
	                     " session: " + session.getId());

	                }

	                return(mapping.findForward("cmnErrorPage"));

	            }

/*******************************************************
    -- Ar SO Add Lines Action --
*******************************************************/

	       } else if(request.getParameter("addLinesButton") != null) {

	      	 int listSize = actionForm.getArSOLListSize();

	         for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {

	        	 ArSalesOrderLineItemList arSOLList = new ArSalesOrderLineItemList(actionForm,
	        	    null, String.valueOf(x), null, null, null, false, null, null, null, null, "0.0","0.0", "0.0", "0.0", null, "0.0",null, "Y");

	        	arSOLList.setLocationList(actionForm.getLocationList());
	        	arSOLList.setTaxList(actionForm.getTaxList());
	        	actionForm.saveArSOLList(arSOLList);

	         }

	         return(mapping.findForward("arSalesOrderEntry"));

/*******************************************************
     -- Ar SO Delete Lines Action --
*******************************************************/

	       } else if(request.getParameter("deleteLinesButton") != null) {

		       	for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

		       		ArSalesOrderLineItemList arSOLList = actionForm.getArSOLByIndex(i);

		         	   if (arSOLList.getDeleteCheckbox()) {

		         	   	   actionForm.deleteArSOLList(i);
		         	   	   i--;
		         	   }

		        }

		        for (int i = 0; i<actionForm.getArSOLListSize(); i++) {

		        	ArSalesOrderLineItemList arSOLList = actionForm.getArSOLByIndex(i);

		            	arSOLList.setLineNumber(String.valueOf(i+1));

		        }

		        return(mapping.findForward("arSalesOrderEntry"));

/*******************************************************
    -- Ar SO Customer Enter Action --
*******************************************************/

	      } else if(!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

	    	  try {

	    		  ArModCustomerDetails mdetails = ejbSO.getArCstByCstCustomerCode(actionForm.getCustomer(), user.getCmpCode());

	    		  actionForm.setPaymentTerm(mdetails.getCstPytName());
	    		  actionForm.setTaxCode(mdetails.getCstCcTcName());

	    		  actionForm.setCustomerName(mdetails.getCstName());
	    		  actionForm.setTaxRate(mdetails.getCstCcTcRate());
	    		  actionForm.setTaxType(mdetails.getCstCcTcType());

	    		  if (mdetails.getCstSlpSalespersonCode() != null && mdetails.getCstSlpSalespersonCode2() != null) {

	    			  actionForm.clearSalespersonList();
	    			  actionForm.clearSalespersonNameList();
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode());
	    			  actionForm.setSalespersonList(mdetails.getCstSlpSalespersonCode2());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode() + " - " + mdetails.getCstSlpName());
	    			  actionForm.setSalespersonNameList(mdetails.getCstSlpSalespersonCode2() + " - " + mdetails.getCstSlpName2());

	    		  } else {
	    			  	actionForm.clearSalespersonList();
	    	          	actionForm.clearSalespersonNameList();

	    	          	ArrayList list = ejbSO.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

	    	          	if ( list == null || list.size() == 0) {

	    	          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
	    	          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

	    	          	} else {

	    	          		Iterator i =list.iterator();

	    	          		while (i.hasNext()) {

	    	          			ArSalespersonDetails slpDetails = (ArSalespersonDetails)i.next();

	    	          			actionForm.setSalespersonList(slpDetails.getSlpSalespersonCode());
	    	          			actionForm.setSalespersonNameList(slpDetails.getSlpSalespersonCode() + " - " + slpDetails.getSlpName());

	    	          		}

	    	          	}
	    		  }

	    		  actionForm.setSalesperson(mdetails.getCstSlpSalespersonCode() != null ?
	    				  mdetails.getCstSlpSalespersonCode() : Constants.GLOBAL_BLANK);

	    		  actionForm.clearArSOLList();

	    		  if(mdetails.getCstLitName() != null && mdetails.getCstLitName().length() > 0) {

	    			  // item template

	    			  ArrayList list = ejbSO.getInvLitByCstLitName(mdetails.getCstLitName(), user.getCmpCode());

	    			  if (!list.isEmpty()) {

	    				  Iterator i = list.iterator();

	    				  while (i.hasNext()) {

	    					  InvModLineItemDetails liDetails = (InvModLineItemDetails)i.next();

	    					  ArSalesOrderLineItemList arSOLList = new ArSalesOrderLineItemList(actionForm, null,
	    							  Common.convertShortToString(liDetails.getLiLine()), liDetails.getLiLocName(),
	    							  liDetails.getLiIiName(), liDetails.getLiIiDescription(), false, "0",
	    							  liDetails.getLiUomName(), null, null, "0.00", "0.0","0.0", "0.0", "0.0", "0.0" , null, "Y");

	    					  String itemClass = ejbSO.getInvIiClassByIiName(arSOLList.getItemName(), user.getCmpCode());

	    					  // populate location list

	    					  arSOLList.setLocationList(actionForm.getLocationList());

	    					  // populate tax line list

	    					  arSOLList.setTaxList(actionForm.getTaxList());
	    					  // populate unit list

	    					  ArrayList uomList = new ArrayList();
	    					  uomList = ejbSO.getInvUomByIiName(arSOLList.getItemName(), user.getCmpCode());

	    					  arSOLList.clearUnitList();
	    					  arSOLList.setUnitList(Constants.GLOBAL_BLANK);

	    					  Iterator j = uomList.iterator();

	    					  while (j.hasNext()) {

	    						  InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)j.next();

	    						  arSOLList.setUnitList(mUomDetails.getUomName());

	    					  }

	    					  // populate unit cost field

	    					  if (!Common.validateRequired(arSOLList.getItemName()) && !Common.validateRequired(arSOLList.getUnit())) {

	    						  double unitPrice = ejbSO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(
	    								  mdetails.getCstCustomerCode(), arSOLList.getItemName(), arSOLList.getUnit(), user.getCmpCode());

	    						  arSOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

	    					  }

	    					  // populate amount field

	    					  if(!Common.validateRequired(arSOLList.getQuantity()) && !Common.validateRequired(arSOLList.getUnitPrice())) {

	    						  double amount = Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit) *
	    						  Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit);
	    						  arSOLList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	    					  }

	    					  actionForm.saveArSOLList(arSOLList);

	    				  }

	    			  }

	    		  } else {

	    			  for (int x = 1; x <= journalLineNumber; x++) {

	    				  ArSalesOrderLineItemList arSOLList = new ArSalesOrderLineItemList(actionForm,
	    						  null, new Integer(x).toString(), null, null, null, false, null, null,
	    						  null, null, "0.0","0.0", "0.0", "0.0", null, null, null, "Y");


	    				  arSOLList.setLocationList(actionForm.getLocationList());
	    				  arSOLList.clearTaxList();
	    				  arSOLList.setTaxList(actionForm.getTaxList());
	    				  arSOLList.setUnitList(Constants.GLOBAL_BLANK);
	    				  arSOLList.setUnitList("Select Item First");

	    				  actionForm.saveArSOLList(arSOLList);

	    			  }

	    		  }

	    	  } catch (GlobalNoRecordFoundException ex) {

	          	 actionForm.clearArSOLList();
	        	 errors.add(ActionMessages.GLOBAL_MESSAGE,
	                 new ActionMessage("salesOrderEntry.error.customerNoRecordFound"));
	             saveErrors(request, new ActionMessages(errors));

	          } catch (EJBException ex) {

	            if (log.isInfoEnabled()) {

	               log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	               return mapping.findForward("cmnErrorPage");

	            }

	          }

	          return(mapping.findForward("arSalesOrderEntry"));
 /*******************************************************
      -- Ar SO View Attachment Action --
 *******************************************************/

        } else if(request.getParameter("viewAttachmentButton1") != null) {

	  	File file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-1" + attachmentFileExtension );
	  	File filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-1" + attachmentFileExtensionPDF );
	  	String filename = "";

	  	if (file.exists()){
	  		filename = file.getName();
	  	}else if (filePDF.exists()) {
	  		filename = filePDF.getName();
	  	}

			System.out.println(filename + " <== filename?");
        String fileExtension = filename.substring(filename.lastIndexOf("."));

	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

	  		fileExtension = attachmentFileExtension;

	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
	    		fileExtension = attachmentFileExtensionPDF;

	    	}
	  	System.out.println(fileExtension + " <== file extension?");
	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getSalesOrderCode() + "-1" + fileExtension);

	  	byte data[] = new byte[fis.available()];

	  	int ctr = 0;
	  	int c = 0;

	  	while ((c = fis.read()) != -1) {

      		data[ctr] = (byte)c;
      		ctr++;

	  	}

	  	if (fileExtension == attachmentFileExtension) {
	      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
	      	System.out.println("jpg");

	      	session.setAttribute(Constants.IMAGE_KEY, image);
	      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
	  	} else if (fileExtension == attachmentFileExtensionPDF ){
	  		System.out.println("pdf");

	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
	  	}

         if (request.getParameter("child") == null) {

           return (mapping.findForward("apPurchaseRequisitionEntry"));

         } else {

           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

        }



      } else if(request.getParameter("viewAttachmentButton2") != null) {

    	  File file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-2" + attachmentFileExtension );
    	  	File filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-2" + attachmentFileExtensionPDF );
    	  	String filename = "";

    	  	if (file.exists()){
    	  		filename = file.getName();
    	  	}else if (filePDF.exists()) {
    	  		filename = filePDF.getName();
    	  	}

 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	  		fileExtension = attachmentFileExtension;

  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getSalesOrderCode() + "-2" + fileExtension);

    	  	byte data[] = new byte[fis.available()];

    	  	int ctr = 0;
    	  	int c = 0;

    	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

    	  	}

    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}




         if (request.getParameter("child") == null) {

           return (mapping.findForward("apPurchaseRequisitionEntry"));

         } else {

           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

        }


      } else if(request.getParameter("viewAttachmentButton3") != null) {

    	  File file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-3" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-3" + attachmentFileExtensionPDF );
    	  String filename = "";

    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }

    	  System.out.println(filename + " <== filename?");
    	  String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    		  fileExtension = attachmentFileExtension;

    	  } else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

    	  }
    	  System.out.println(fileExtension + " <== file extension?");
    	  FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getSalesOrderCode() + "-3" + fileExtension);

    	  byte data[] = new byte[fis.available()];

    	  int ctr = 0;
    	  int c = 0;

    	  while ((c = fis.read()) != -1) {

    		  data[ctr] = (byte)c;
    		  ctr++;

    	  }

    	  if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  } else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  }

         if (request.getParameter("child") == null) {

           return (mapping.findForward("apPurchaseRequisitionEntry"));

         } else {

           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

        }


      } else if(request.getParameter("viewAttachmentButton4") != null) {

    	  File file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-4" + attachmentFileExtension );
    	  File filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-4" + attachmentFileExtensionPDF );
    	  String filename = "";

    	  if (file.exists()){
    		  filename = file.getName();
    	  }else if (filePDF.exists()) {
    		  filename = filePDF.getName();
    	  }

 			System.out.println(filename + " <== filename?");
	        String fileExtension = filename.substring(filename.lastIndexOf("."));

    	  	if (fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())){

    	  		fileExtension = attachmentFileExtension;

  	    	} else if (fileExtension.toUpperCase().equals(attachmentFileExtensionPDF.toUpperCase())) {
  	    		fileExtension = attachmentFileExtensionPDF;

  	    	}
    	  	System.out.println(fileExtension + " <== file extension?");
    	  	FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getSalesOrderCode() + "-4" + fileExtension);

    	  	byte data[] = new byte[fis.available()];

    	  	int ctr = 0;
    	  	int c = 0;

    	  	while ((c = fis.read()) != -1) {

	      		data[ctr] = (byte)c;
	      		ctr++;

    	  	}

    	  	if (fileExtension == attachmentFileExtension) {
		      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
		      	System.out.println("jpg");

		      	session.setAttribute(Constants.IMAGE_KEY, image);
		      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
    	  	} else if (fileExtension == attachmentFileExtensionPDF ){
    	  		System.out.println("pdf");

    	  		session.setAttribute(Constants.PDF_REPORT_KEY, filePDF);
    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
    	  	}
         if (request.getParameter("child") == null) {

           return (mapping.findForward("apPurchaseRequisitionEntry"));

         } else {

           return (mapping.findForward("apPurchaseRequisitionEntryChild"));

        }

/*******************************************************
  -- Ar SO Item Enter Action --
*******************************************************/

	    } else if(!Common.validateRequired(request.getParameter("arSOLList[" +
	    		actionForm.getRowSOLSelected() + "].isItemEntered"))) {

	    	ArSalesOrderLineItemList arSOLList =
	    		actionForm.getArSOLByIndex(actionForm.getRowSOLSelected());

	    	try {

	    		// populate unit field class

	    		ArrayList uomList = new ArrayList();
	    		uomList = ejbSO.getInvUomByIiName(arSOLList.getItemName(), user.getCmpCode());

	    		arSOLList.clearUnitList();
	    		arSOLList.setUnitList(Constants.GLOBAL_BLANK);

	    		Iterator i = uomList.iterator();
	    		int xx = 0;
	    		while (i.hasNext()) {
	    			System.out.println("" + xx + "");
	    			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

	    			arSOLList.setUnitList(mUomDetails.getUomName());

	    			if (mUomDetails.isDefault()) {

	    			    arSOLList.setUnit(mUomDetails.getUomName());

	    			}
	    			xx= xx + 1;
	    		}

	    		//TODO: populate taglist with empty values
           		boolean isTraceMisc = ejbSO.getSoTraceMisc(arSOLList.getItemName(), user.getCmpCode());
           		String misc = arSOLList.getQuantity() + "_";
           		String propertyCode = "";
        		 	String specs = "";
        		 	String serialNumber = "";
        		 	String custodian = "";
        		 	String expiryDate = "";
        		 	String tgDocumentNumber = "";
           		System.out.println(isTraceMisc + "<== trace misc item enter action");
           		if (isTraceMisc == true){


           			arSOLList.clearTagList();

           			arSOLList.setIsTraceMisc(isTraceMisc);


           			misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), arSOLList.getQuantity());
           			arSOLList.setMisc(misc);
           			System.out.println(arSOLList.getTagListSize() + "<== arILIList.getTagListSize() before ");

     	      		System.out.println(arSOLList.getTagListSize() + "<== arILIList.getTagListSize() after ");
           		}
           		arSOLList.setMisc(misc);




	    		// populate unit cost field

	    		if (!Common.validateRequired(arSOLList.getItemName()) && !Common.validateRequired(arSOLList.getUnit())) {

	    			//double unitPrice = ejbSO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arSOLList.getItemName(), arSOLList.getUnit(), user.getCmpCode());
	    		//	arSOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

	    			 

	                 if (!Common.validateRequired(arSOLList.getItemName()) && !Common.validateRequired(arSOLList.getUnit())) {
	
						
						
						if(!Common.validateRequired(actionForm.getDate()) && Common.validateDateFormat(actionForm.getDate())){
								
								
							 double unitPrice =  ejbSO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arSOLList.getItemName(), Common.convertStringToSQLDate(actionForm.getDate()) ,arSOLList.getUnit(), user.getCmpCode());
	                    	 arSOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
		
								
								
								
						}else{
						  	double unitPrice =  ejbSO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arSOLList.getItemName(),  arSOLList.getUnit(), user.getCmpCode());
	                        arSOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));
	
						
						}
	                 }
	    		
	    		
	    		
	    		
	    		
	    		
	    		
	    		
	    		}

	    		arSOLList.setDiscount1("0.0");
	    		arSOLList.setDiscount2("0.0");
	    		arSOLList.setDiscount3("0.0");
	    		arSOLList.setDiscount4("0.0");
	         	arSOLList.setTotalDiscount(Common.convertDoubleToStringMoney(0d, precisionUnit));

		        arSOLList.setQuantity(Common.convertDoubleToStringMoney(1d, precisionUnit));
	    		arSOLList.setAmount(arSOLList.getUnitPrice());



	    	} catch (EJBException ex) {

	    		if (log.isInfoEnabled()) {

	    			log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
	    					" session: " + session.getId());
	    			return mapping.findForward("cmnErrorPage");

	    		}

	    	}

	    	return(mapping.findForward("arSalesOrderEntry"));

/*******************************************************
   -- Ar SO Unit Enter Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("arSOLList[" +
         		actionForm.getRowSOLSelected() + "].isUnitEntered"))) {

        	 ArSalesOrderLineItemList arSOLList =
         		actionForm.getArSOLByIndex(actionForm.getRowSOLSelected());

         	try {

         		// populate unit cost field

         		if (!Common.validateRequired(arSOLList.getLocation()) && !Common.validateRequired(arSOLList.getItemName())) {

         			double unitPrice = ejbSO.getIiSalesPriceByInvCstCustomerCodeAndIiNameAndUomName(actionForm.getCustomer(), arSOLList.getItemName(), arSOLList.getUnit(), user.getCmpCode());
         			arSOLList.setUnitPrice(Common.convertDoubleToStringMoney(unitPrice, precisionUnit));

         		}

         		// populate amount field
         		double amount = 0d;

         		if(!Common.validateRequired(arSOLList.getQuantity()) && !Common.validateRequired(arSOLList.getUnitPrice())) {

		        	amount = Common.convertStringMoneyToDouble(arSOLList.getQuantity(), precisionUnit) *
						Common.convertStringMoneyToDouble(arSOLList.getUnitPrice(), precisionUnit);

         			arSOLList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

         		}

	      		// populate discount field

	      		if (!Common.validateRequired(arSOLList.getAmount()) &&
	      			(!Common.validateRequired(arSOLList.getDiscount1()) ||
					!Common.validateRequired(arSOLList.getDiscount2()) ||
					!Common.validateRequired(arSOLList.getDiscount3()) ||
					!Common.validateRequired(arSOLList.getDiscount4()))) {

	      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

	      				amount = amount / (1 + (actionForm.getTaxRate() / 100));

	      			}

	      			double discountRate1 = Common.convertStringMoneyToDouble(arSOLList.getDiscount1(), precisionUnit)/100;
	      			double discountRate2 = Common.convertStringMoneyToDouble(arSOLList.getDiscount2(), precisionUnit)/100;
	      			double discountRate3 = Common.convertStringMoneyToDouble(arSOLList.getDiscount3(), precisionUnit)/100;
	      			double discountRate4 = Common.convertStringMoneyToDouble(arSOLList.getDiscount4(), precisionUnit)/100;
	      			double totalDiscountAmount = 0d;

	      			if (discountRate1 > 0) {

	      				double discountAmount = amount * discountRate1;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate2 > 0) {

	      				double discountAmount = amount * discountRate2;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate3 > 0) {

	      				double discountAmount = amount * discountRate3;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (discountRate4 > 0) {

	      				double discountAmount = amount * discountRate4;
	      				totalDiscountAmount += discountAmount;
	      				amount -= discountAmount;

	      			}

	      			if (actionForm.getTaxType().equals("INCLUSIVE")) {

	      				amount += amount * (actionForm.getTaxRate() / 100);

	      			}

	      			arSOLList.setTotalDiscount(Common.convertDoubleToStringMoney(totalDiscountAmount, precisionUnit));
	      			arSOLList.setAmount(Common.convertDoubleToStringMoney(amount, precisionUnit));

	      		}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("arSalesOrderEntry"));

/*******************************************************
     -- Ar SO Tax Code Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isTaxCodeEntered"))) {

         	try {

         		ArTaxCodeDetails details = ejbSO.getArTcByTcName(actionForm.getTaxCode(), user.getCmpCode());
         		actionForm.setTaxRate(details.getTcRate());
         		actionForm.setTaxType(details.getTcType());

         		actionForm.clearArSOLList();

         		for (int x = 1; x <= journalLineNumber; x++) {

         			ArSalesOrderLineItemList arSOLList = new ArSalesOrderLineItemList(actionForm,
              				null, new Integer(x).toString(), null, null, null, false, null, null,
    						null, null, "0.0","0.0", "0.0", "0.0", null, null, null, "Y");

    	          	arSOLList.setLocationList(actionForm.getLocationList());
    	          	arSOLList.setTaxList(actionForm.getTaxList());
    	          	arSOLList.setUnitList(Constants.GLOBAL_BLANK);
    	          	arSOLList.setUnitList("Select Item First");

              		actionForm.saveArSOLList(arSOLList);

              	}

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage");

         		}

         	}

         	return(mapping.findForward("arSalesOrderEntry"));

/*******************************************************
     -- Ar SO Conversion Date Enter Action --
******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

        	 try {

        		 actionForm.setConversionRate(Common.convertDoubleToStringMoney(
        				 ejbFR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.CONVERSION_RATE_PRECISION));

        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("salesOrderEntry.error.conversionDateNotExist"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());

        		 }

        		 return(mapping.findForward("cmnErrorPage"));

        	 }

        	 if (!errors.isEmpty()) {

        		 saveErrors(request, new ActionMessages(errors));

        	 }

        	 return(mapping.findForward("arSalesOrderEntry"));


/*******************************************************
   -- Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isCurrencyEntered"))) {

         	try {

         		GlModFunctionalCurrencyRateDetails mdetails = ejbFR.getFrRateByFrName(actionForm.getCurrency(), user.getCmpCode());
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
         		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));

         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("ArSalesOrderEntryAction.error.conversionDateNotExist"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));

         	}

         	return(mapping.findForward("arSalesOrderEntry"));

         }

            
            
/*******************************************************
     -- Ar SO Load Action --
*******************************************************/

       if (frParam != null) {
          if (!errors.isEmpty()) {

             saveErrors(request, new ActionMessages(errors));
             return(mapping.findForward("arSalesOrderEntry"));

          }

          if (request.getParameter("forward") == null &&
              actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

              errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
              saveErrors(request, new ActionMessages(errors));

              return mapping.findForward("cmnMain");

          }

          try {

          	ArrayList list = null;
          	Iterator i = null;


          	actionForm.clearCurrencyList();

          	list = ejbSO.getGlFcAllWithDefault(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

          			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

          			actionForm.setCurrencyList(mFcDetails.getFcName());

          			if (mFcDetails.getFcSob() == 1) {

          				actionForm.setCurrency(mFcDetails.getFcName());
          				actionForm.setFunctionalCurrency(mFcDetails.getFcName());

          			}

          		}

          	}

          	
          	actionForm.clearDocumentTypeList();
          	
          	
          	list = ejbSO.getDocumentTypeList("AR SALES ORDER DOCUMENT TYPE", user.getCmpCode());
          	
          	
          	
          	i = list.iterator();
          	while(i.hasNext()) {
          		actionForm.setDocumentTypeList((String)i.next());
          	}
          	
          	
          	
          	
          	
          	actionForm.clearPaymentTermList();

          	list = ejbSO.getAdPytAll(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

          		    actionForm.setPaymentTermList((String)i.next());

          		}

          		actionForm.setPaymentTerm("IMMEDIATE");

          	}


      	    //get transaction status list
          	actionForm.clearTransactionStatusList();

          	list = ejbSO.getAdLvTransactionStatusAll(user.getCmpCode());

          	i = list.iterator();

          	actionForm.setTransactionStatusList(Constants.GLOBAL_BLANK);
      		while (i.hasNext()) {

      		    actionForm.setTransactionStatusList((String)i.next());

      		}


            actionForm.clearUserList();

        	ArrayList userList = ejbSO.getAdUsrAll(user.getCmpCode());

        	if (userList == null || userList.size() == 0) {

        		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

        	} else {

        		Iterator x = userList.iterator();

        		while (x.hasNext()) {

        			actionForm.setUserList((String)x.next());

        		}

        	}
        	 System.out.println(actionForm.getUserList() + "<== user List");


          	actionForm.clearTaxCodeList();

          	list = ejbSO.getArTcAll(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setTaxCodeList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

        		    actionForm.setTaxCodeList((String) i.next());

          		}

          	}

          	
          	
          	if(actionForm.getUseCustomerPulldown()) {

          		actionForm.clearCustomerList();

          		list = ejbSO.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

          		if (list == null || list.size() == 0) {

          			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

          		} else {

          			i = list.iterator();

          			while (i.hasNext()) {

          				actionForm.setCustomerList((String)i.next());

          			}

          		}

          	}


            //print type item
            actionForm.clearReportTypeList();


            list = ejbSO.getAdLvReportTypeAll(user.getCmpCode());


            if (list == null || list.size() == 0) {

                actionForm.setReportTypeList(Constants.GLOBAL_BLANK);

            } else {

           	 actionForm.setReportTypeList(Constants.GLOBAL_BLANK);
                i = list.iterator();

                while (i.hasNext()) {

                    actionForm.setReportTypeList((String)i.next());

                }

            }



          	actionForm.clearLocationList();

          	list = ejbSO.getInvLocAll(user.getCmpCode());

          	if (list == null || list.size() == 0) {

          		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i = list.iterator();

          		while (i.hasNext()) {

          		    actionForm.setLocationList((String)i.next());

          		}

          	}

          	//get transaction type list
          	actionForm.clearTransactionTypeList();

          	list = ejbSO.getArSoTransactionTypeList(user.getCmpCode());

          	i = list.iterator();

          	actionForm.setTransactionTypeList(Constants.GLOBAL_BLANK);
      		while (i.hasNext()) {

      		    actionForm.setTransactionTypeList((String)i.next());

      		}



          	actionForm.clearSalespersonList();
          	actionForm.clearSalespersonNameList();

          	list = ejbSO.getArSlpAll(new Integer (user.getCurrentBranch().getBrCode()), user.getCmpCode());

          	if ( list == null || list.size() == 0) {

          		actionForm.setSalespersonList(Constants.GLOBAL_NO_RECORD_FOUND);
          		actionForm.setSalespersonNameList(Constants.GLOBAL_NO_RECORD_FOUND);

          	} else {

          		i =list.iterator();

          		while (i.hasNext()) {

          			ArSalespersonDetails mdetails = (ArSalespersonDetails)i.next();

          			actionForm.setSalespersonList(mdetails.getSlpSalespersonCode());
          			actionForm.setSalespersonNameList(mdetails.getSlpSalespersonCode() + " - " + mdetails.getSlpName());

          		}

          	}

          	actionForm.clearTaxList();

          	actionForm.setTaxList("Y");
          	actionForm.setTaxList("N");

          	//REPORT PARAMETERS
        	actionForm.clearReportParameters();

          	list = ejbSO.getArSalesOrderReportParameters(user.getCmpCode());


          	for(int x=0;x<list.size();x++){

          	  	ReportParameter reportParameter = new ReportParameter();
          		String parameterName =list.get(x).toString();
          		reportParameter.setParameterName(parameterName);
          	  	actionForm.addReportParameters(reportParameter);
          	}


          	actionForm.clearArSOLList();

          	if (request.getParameter("forward") != null || isInitialPrinting) {

          		if (request.getParameter("forward") != null) {

          			actionForm.setSalesOrderCode(new Integer(request.getParameter("salesOrderCode")));

          		}

          		ArModSalesOrderDetails mdetails = ejbSO.getArSoBySoCode(actionForm.getSalesOrderCode(), user.getCmpCode());

          		actionForm.setDate(Common.convertSQLDateToString(mdetails.getSoDate()));
          		actionForm.setDocumentType(mdetails.getSoDocumentType());
          		actionForm.setDocumentNumber(mdetails.getSoDocumentNumber());
          		actionForm.setReferenceNumber(mdetails.getSoReferenceNumber());
          		actionForm.setTransactionType(mdetails.getSoTransactionType());
          		actionForm.setDescription(mdetails.getSoDescription());
          		actionForm.setShippingLine(mdetails.getSoShippingLine());
          		actionForm.setPort(mdetails.getSoPort());
          		actionForm.setSalesOrderVoid(Common.convertByteToBoolean(mdetails.getSoVoid()));
          		actionForm.setSalesOrderMobile(Common.convertByteToBoolean(mdetails.getSoMobile()));
          		actionForm.setBillTo(mdetails.getSoBillTo());
          		actionForm.setShipTo(mdetails.getSoShipTo());
          		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getSoConversionDate()));
          		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getSoConversionRate(), (short)40));
          		actionForm.setApprovalStatus(mdetails.getSoApprovalStatus());
          		actionForm.setOrderStatus(mdetails.getSoOrderStatus());
          		actionForm.setSoAdvanceAmount(Common.convertDoubleToStringMoney(mdetails.getSoAdvanceAmount(), (short)2));
          		System.out.println(mdetails.getSoOrderStatus());
       			actionForm.setReasonForRejection(mdetails.getSoReasonForRejection());
       			actionForm.setPosted(mdetails.getSoPosted() == 1 ? "YES" : "NO");
          		actionForm.setCreatedBy(mdetails.getSoCreatedBy());
          		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getSoDateCreated()));
          		actionForm.setLastModifiedBy(mdetails.getSoLastModifiedBy());
          		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getSoDateLastModified()));
          		actionForm.setPostedBy(mdetails.getSoPostedBy());
       			actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getSoDatePosted()));
       			actionForm.setApprovedRejectedBy(mdetails.getSoApprovedRejectedBy());
       			actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getSoDateApprovedRejected()));
       			actionForm.setMemo(mdetails.getSoMemo());
       			actionForm.setTransactionStatus(mdetails.getSoTransactionStatus());
       			ArrayList rpList = ejbSO.getArSalesOrderReportParameters(user.getCmpCode());




       			actionForm.setReportParameters(Common.convertStringToReportParameters(mdetails.getReportParameter(),rpList));



       			if(!actionForm.getCurrencyList().contains(mdetails.getSoFcName())) {

       				if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearCurrencyList();

          			}
       				actionForm.setCurrencyList(mdetails.getSoFcName());

       			}
          		actionForm.setCurrency(mdetails.getSoFcName());

          		if (!actionForm.getPaymentTermList().contains(mdetails.getSoPytName())) {

          			if (actionForm.getPaymentTermList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearPaymentTermList();

          			}
          			actionForm.setPaymentTermList(mdetails.getSoPytName());

          		}
          		actionForm.setPaymentTerm(mdetails.getSoPytName());

          		if (!actionForm.getTaxCodeList().contains(mdetails.getSoTcName())) {

          			if (actionForm.getTaxCodeList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearTaxCodeList();

          			}
          			actionForm.setTaxCodeList(mdetails.getSoTcName());

          		}
          		actionForm.setTaxCode(mdetails.getSoTcName());
          		actionForm.setTaxType(mdetails.getSoTcType());
          		actionForm.setTaxRate(mdetails.getSoTcRate());

          		if (!actionForm.getCustomerList().contains(mdetails.getSoCstCustomerCode())) {

          			if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearCustomerList();

          			}
          			actionForm.setCustomerList(mdetails.getSoCstCustomerCode());

          		}
          		actionForm.setCustomer(mdetails.getSoCstCustomerCode());
          		actionForm.setCustomerName(mdetails.getSoCstName());

          		if (!actionForm.getSalespersonList().contains(mdetails.getSoSlpSalespersonCode())) {

          			if (actionForm.getSalespersonList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

          				actionForm.clearSalespersonList();

          			}

          			actionForm.setSalespersonList(mdetails.getSoSlpSalespersonCode());
          			actionForm.setSalespersonNameList(mdetails.getSoSlpSalespersonCode() + " - " + mdetails.getSoSlpName());

          		}
          		actionForm.setSalesperson(mdetails.getSoSlpSalespersonCode());

          		double totalAmount = 0;

          		list = mdetails.getSoSolList();

      			i = list.iterator();
      			int curIndex = 0;
      			while (i.hasNext()) {

      				ArModSalesOrderLineDetails mSolDetails = (ArModSalesOrderLineDetails)i.next();

      				totalAmount += mSolDetails.getSolAmount();

      				String fixedDiscountAmount = "0.00";

                    if((mSolDetails.getSolDiscount1() + mSolDetails.getSolDiscount2() +
                    		mSolDetails.getSolDiscount3() + mSolDetails.getSolDiscount4()) == 0)
                   	 fixedDiscountAmount = Common.convertDoubleToStringMoney(mSolDetails.getSolTotalDiscount(), precisionUnit);

                    String ii_Desc = mSolDetails.getSolIiDescription();
                    String eii_Desc = mSolDetails.getSolEDesc();
                    String ii_DescDisp = "";
                    boolean chker = false;

                    if(eii_Desc != null)
                    {
                    	if(eii_Desc.trim().length() == 0)
                    	{
                    		ii_DescDisp = ii_Desc;
                    		chker = false;
                    	}
                    	else if(eii_Desc.trim().length() > 0)
                    	{
                    		if(ii_Desc.equals(eii_Desc)) {
                    			ii_DescDisp = ii_Desc;
                    			chker = false;
                    		}
                    		else if(!ii_Desc.equals(eii_Desc)) {
                    			ii_DescDisp = eii_Desc;
                    			chker = true;
                    		}
                    	}
                    }
                    else
                    {
                    	ii_DescDisp = ii_Desc;
                		chker = false;
                    }

                    ArSalesOrderLineItemList arSolList = new ArSalesOrderLineItemList(actionForm,
      				        mSolDetails.getSolCode(),
							Common.convertShortToString(mSolDetails.getSolLine()),
							mSolDetails.getSolLocName(),
							mSolDetails.getSolIiName(),
							ii_DescDisp,
							chker,
							Common.convertDoubleToStringMoney(mSolDetails.getSolQuantity(), precisionUnit),
							mSolDetails.getSolUomName(),
							Common.convertDoubleToStringMoney(mSolDetails.getSolUnitPrice(), precisionUnit),
							Common.convertDoubleToStringMoney(mSolDetails.getSolAmount(), precisionUnit),
    						Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount1(), precisionUnit),
    						Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount2(), precisionUnit),
    						Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount3(), precisionUnit),
    						Common.convertDoubleToStringMoney(mSolDetails.getSolDiscount4(), precisionUnit),
    						Common.convertDoubleToStringMoney(mSolDetails.getSolTotalDiscount(), precisionUnit),
    						fixedDiscountAmount,
    						mSolDetails.getSolMisc(),
    						Common.convertByteToBoolean(mSolDetails.getSolTax())?"Y":"N");



                    boolean isTraceMisc = ejbSO.getSoTraceMisc(mSolDetails.getSolIiName(), user.getCmpCode());
                    arSolList.setIsTraceMisc(isTraceMisc);
                	ArrayList tagList = new ArrayList();
                    if(isTraceMisc) {

                    	tagList = mSolDetails.getSolTagList();

                    	if(tagList.size() > 0) {
                    		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mSolDetails.getSolQuantity()));


                    		arSolList.setMisc(misc);

                    	}else {
                      		if(mSolDetails.getSolMisc()==null) {

                      			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mSolDetails.getSolQuantity()));
                      			arSolList.setMisc(misc);
                      		}
                      	}

                    }


      				arSolList.setLocationList(actionForm.getLocationList());
      				arSolList.setTaxList(actionForm.getTaxList());
      				arSolList.clearUnitList();

      				ArrayList unitList = ejbSO.getInvUomByIiName(mSolDetails.getSolIiName(), user.getCmpCode());

      				Iterator unitListIter = unitList.iterator();

      				while (unitListIter.hasNext()) {

	            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
	            		arSolList.setUnitList(mUomDetails.getUomName());

	            	}

      				actionForm.saveArSOLList(arSolList);
      				curIndex++;
      			}

      			actionForm.setTotalAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

      			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();

      			for (int x = list.size() + 1; x <= remainingList; x++) {

      				ArSalesOrderLineItemList arSOLList = new ArSalesOrderLineItemList(actionForm,
      						null, String.valueOf(x), null, null, null, false, null, null, null, null,"0.0", "0.0", "0.0", "0.0", null, "0.0", null,"Y");

      				arSOLList.setLocationList(actionForm.getLocationList());
      				arSOLList.setTaxList(actionForm.getTaxList());
      				arSOLList.setUnitList(Constants.GLOBAL_BLANK);
      				arSOLList.setUnitList("Select Item First");

      				actionForm.saveArSOLList(arSOLList);

      			}

      			this.setFormProperties(actionForm, user.getCompany());

    			if (request.getParameter("child") == null) {

    				return (mapping.findForward("arSalesOrderEntry"));

    			} else {

    				return (mapping.findForward("arSalesOrderEntryChild"));

    			}

          	}
          	// Populate line when not forwarding

          	for (int x = 1; x <= journalLineNumber; x++) {

          		ArSalesOrderLineItemList arSOLList = new ArSalesOrderLineItemList(actionForm,
          				null, new Integer(x).toString(), null, null, null, false, null, null,
						null, null, "0.0","0.0", "0.0", "0.0", null,"0.0", null,"Y");

	          	arSOLList.setLocationList(actionForm.getLocationList());
	          	arSOLList.setTaxList(actionForm.getTaxList());
	          	arSOLList.setUnitList(Constants.GLOBAL_BLANK);
	          	arSOLList.setUnitList("Select Item First");

          		actionForm.saveArSOLList(arSOLList);

          	}

          } catch(GlobalNoRecordFoundException ex) {

		      	errors.add(ActionMessages.GLOBAL_MESSAGE,
		              new ActionMessage("salesOrderEntry.error.salesOrderAlreadyDeleted"));

          } catch(EJBException ex) {

             if (log.isInfoEnabled()) {

                log.info("EJBException caught in ArSalesOrderEntryAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
             }

             return(mapping.findForward("cmnErrorPage"));

          }

          if (!errors.isEmpty()) {

             saveErrors(request, new ActionMessages(errors));

          } else {

          	if (request.getParameter("saveSubmitButton") != null &&
          			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

          		try {

          			ArrayList list = ejbSO.getAdApprovalNotifiedUsersBySoCode(actionForm.getSalesOrderCode(), user.getCmpCode());

          			if (list.isEmpty()) {

          				messages.add(ActionMessages.GLOBAL_MESSAGE,
          						new ActionMessage("messages.documentSentForPosting"));

          			} else if (list.contains("DOCUMENT POSTED")) {

          				messages.add(ActionMessages.GLOBAL_MESSAGE,
          						new ActionMessage("messages.documentPosted"));

          			} else {

          				Iterator i = list.iterator();

          				String APPROVAL_USERS = "";

          				while (i.hasNext()) {

          					APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

          					if (i.hasNext()) {

          						APPROVAL_USERS = APPROVAL_USERS + ", ";

          					}


          				}

          				messages.add(ActionMessages.GLOBAL_MESSAGE,
          						new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

          			}

          		} catch(EJBException ex) {

          			if (log.isInfoEnabled()) {

          				log.info("EJBException caught in ApVoucherEntryAction.execute(): " + ex.getMessage() +
          						" session: " + session.getId());
          			}

          			return(mapping.findForward("cmnErrorPage"));

          		}

          		saveMessages(request, messages);
          		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
          		      actionForm.setDocumentNumber(null);

          	} else if (request.getParameter("saveAsDraftButton") != null &&
          			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

          		actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
          		  actionForm.setDocumentNumber(null);

          	}else{
           
            
              String documentNumber = ejbDSA.getDocumentNumberSequence("AR SALES ORDER", user.getCurrentBranch().getBrCode(), user.getCmpCode());
              actionForm.setDocumentNumber(documentNumber);
             }

          }
        

          actionForm.reset(mapping, request);

          actionForm.setSalesOrderCode(null);
          actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
          actionForm.setConversionRate("1.000000");
          actionForm.setPosted("NO");
       	  actionForm.setApprovalStatus(null);
          actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
          actionForm.setCreatedBy(user.getUserName());
          actionForm.setLastModifiedBy(user.getUserName());
          actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
          actionForm.setSalesperson(Constants.GLOBAL_BLANK);

          this.setFormProperties(actionForm, user.getCompany());

          return(mapping.findForward("arSalesOrderEntry"));

       } else {

          errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
          saveErrors(request, new ActionMessages(errors));

          return(mapping.findForward("cmnMain"));

       }


	    } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

		     if (log.isInfoEnabled()) {

		        log.info("Exception caught in ArSalesOrderEntryAction.execute(): " + e.getMessage()
		           + " session: " + session.getId());
		     }

		     e.printStackTrace();
		     return(mapping.findForward("cmnErrorPage"));

       }

    }

    private void setFormProperties(ArSalesOrderEntryForm actionForm, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getSalesOrderVoid()) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveAndSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableSalesOrderVoid(false);

			} else if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getSalesOrderCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setEnableSalesOrderVoid(false);
					actionForm.setShowSaveAndSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);


				} else if (actionForm.getSalesOrderCode() != null && Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setEnableSalesOrderVoid(true);
					actionForm.setShowSaveAndSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);

				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
						actionForm.getApprovalStatus().equals("N/A")) {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveAndSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableSalesOrderVoid(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveAndSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setEnableSalesOrderVoid(false);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveAndSubmitButton(true);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setEnableSalesOrderVoid(true);

			}

		} else {

			actionForm.setEnableFields(false);
			actionForm.setShowSaveAndSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);

		}
		// view attachment

		if (actionForm.getSalesOrderCode() != null) {

				MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

	            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/ar/Sales Order/";
	            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
	            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

	            actionForm.setShowDeliveryButton(true);

	 			File file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-1" + attachmentFileExtension);
	 			File filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-1" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton1(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton1(false);

	 			}

	 			file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-2" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-2" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton2(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton2(false);

	 			}

	 			file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-3" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-3" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton3(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton3(false);

	 			}

	 			file = new File(attachmentPath + actionForm.getSalesOrderCode() + "-4" + attachmentFileExtension);
	 			filePDF = new File(attachmentPath + actionForm.getSalesOrderCode() + "-4" + attachmentFileExtensionPDF);

	 			if (file.exists() || filePDF.exists()) {

	 				actionForm.setShowViewAttachmentButton4(true);

	 			} else {

	 				actionForm.setShowViewAttachmentButton4(false);

	 			}

			} else {
				actionForm.setShowDeliveryButton(false);
				actionForm.setShowViewAttachmentButton1(false);
				actionForm.setShowViewAttachmentButton2(false);
				actionForm.setShowViewAttachmentButton3(false);
				actionForm.setShowViewAttachmentButton4(false);

			}



	}

}
