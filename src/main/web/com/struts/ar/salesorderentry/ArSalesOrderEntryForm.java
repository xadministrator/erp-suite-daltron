package com.struts.ar.salesorderentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.ApproverList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.ReportParameter;

public class ArSalesOrderEntryForm extends ActionForm implements java.io.Serializable {

	private short precisionUnit = 0;
    private Integer salesOrderCode = null;
    private String moduleKey = null;
    private String customer = null;
    private String customerName = null;
    private ArrayList customerList = new ArrayList();

    private ArrayList transactionTypeList = new ArrayList();
    private String transactionType = null;
    private ArrayList userList = new ArrayList();
	private String user = null;
    private String date = null;
    private String documentType = null;
    private ArrayList documentTypeList = new ArrayList();
    private String documentNumber = null;
    private String referenceNumber = null;
    private String paymentTerm = null;
    private String soAdvanceAmount = null;
    private ArrayList paymentTermList = new ArrayList();
    private String description = null;
    private boolean salesOrderVoid = false;
    private boolean salesOrderMobile = false;
    private String shippingLine = null;
    private String port = null;
    private String billTo = null;
    private String shipTo =  null;
    private String taxCode = null;
    private ArrayList taxCodeList = new ArrayList();
    private String currency = null;
    private ArrayList currencyList = new ArrayList();
    private String conversionDate = null;
    private String conversionRate = null;
    private String approvalStatus = null;
    private String posted = null;
    private String reasonForRejection = null;
    private String orderStatus = null;
    private String createdBy = null;
    private String dateCreated = null;
    private String lastModifiedBy = null;
    private String dateLastModified = null;
    private String approvedRejectedBy = null;
    private String dateApprovedRejected = null;
    private String postedBy = null;
    private String datePosted = null;
    private boolean salespersonRequired;
    private FormFile filename1 = null;
    private FormFile filename2 = null;
    private FormFile filename3 = null;
    private FormFile filename4 = null;

    private ArrayList memoLineList = new ArrayList();
	private String type = null;
	private ArrayList typeList = new ArrayList();

    private String salesperson = null;
    private ArrayList salespersonList = new ArrayList();
    private ArrayList salespersonNameList = new ArrayList();
    private String totalAmount = null;
	private double taxRate = 0d;
	private String taxType = null;
	private String isTaxCodeEntered = null;

    private String location = null;
    private ArrayList locationList = new ArrayList();
    private String functionalCurrency = null;

    private String report = null;

    private ArrayList arSOLList = new ArrayList(); //will serve for ITEMS.
    private ArrayList arINVList = new ArrayList(); //will serve for MEMO LINES.

    private String reportType = null;
    private ArrayList reportTypeList = new ArrayList();

    private ArrayList reportParameters = new ArrayList();

    private String transactionStatus = null;
    private ArrayList transactionStatusList = new ArrayList();
    
	private int rowSOLSelected = 0;
	private int rowINVSelected = 0;

    private String userPermission = new String();
    private String txnStatus = new String();
    private boolean enableFields = false;
    private boolean enableSalesOrderVoid = false;
    private boolean showAddLinesButton = false;
    private boolean showDeliveryButton = false;
    private boolean showViewAttachmentButton1 = false;
    private boolean showViewAttachmentButton2 = false;
    private boolean showViewAttachmentButton3 = false;
    private boolean showViewAttachmentButton4 = false;
    private boolean showDeleteLinesButton = false;
    private boolean showDeleteButton = false;
    private boolean showSaveButton = false;
    private boolean showSaveButtonDraft = false;
    private boolean useCustomerPulldown = true;
    private boolean enablePaymentTerm = true;
    private boolean disableSalesPrice = false;

    private String isTypeEntered = null;
    private String isCustomerEntered  = null;
    private String isConversionDateEntered = null;
    private String memo = null;
    private String attachment = null;
    private String attachmentPDF = null;
    
    private String logEntry = null;

    private ArrayList taxList = new ArrayList();

 private ArrayList approverList = new ArrayList();

    public String getAttachment() {

    	   return attachment;

    }

    public void setAttachment(String attachment) {

    	   this.attachment = attachment;

    }
    public String getAttachmentPDF() {

 	   return attachmentPDF;

	 }

	 public void setAttachmentPDF(String attachmentPDF) {

	 	   this.attachmentPDF = attachmentPDF;

	 }
	 
	 public String getLogEntry() {

 	   return logEntry;

	 }

	 public void setLogEntry(String logEntry) {

	 	   this.logEntry = logEntry;

	 }

    public FormFile getFilename1() {

     	  return filename1;

     }

     public void setFilename1(FormFile filename1) {

     	  this.filename1 = filename1;

     }

     public FormFile getFilename2() {

     	  return filename2;

     }

     public void setFilename2(FormFile filename2) {

     	  this.filename2 = filename2;

     }

     public FormFile getFilename3() {

     	  return filename3;

     }

     public void setFilename3(FormFile filename3) {

     	  this.filename3 = filename3;

     }

     public FormFile getFilename4() {

     	  return filename4;

     }

     public void setFilename4(FormFile filename4) {

     	  this.filename4 = filename4;

     }
     
     public boolean getShowDeliveryButton() {

	 	   return showDeliveryButton;

	 }

	 public void setShowDeliveryButton(boolean showDeliveryButton) {

	 	   this.showDeliveryButton = showDeliveryButton;

	 }

	 public boolean getShowViewAttachmentButton1() {

	 	   return showViewAttachmentButton1;

	 }

	 public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {

	 	   this.showViewAttachmentButton1 = showViewAttachmentButton1;

	 }

	 public boolean getShowViewAttachmentButton2() {

	 	   return showViewAttachmentButton2;

	 }

	 public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {

	 	   this.showViewAttachmentButton2 = showViewAttachmentButton2;

	 }

	 public boolean getShowViewAttachmentButton3() {

	 	   return showViewAttachmentButton3;

	 }

	 public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {

	 	   this.showViewAttachmentButton3 = showViewAttachmentButton3;

	 }

	 public boolean getShowViewAttachmentButton4() {

	 	   return showViewAttachmentButton4;

	 }

	 public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {

	 	   this.showViewAttachmentButton4 = showViewAttachmentButton4;

	 }

    public String getReport() {

        return report;

    }

    public void setReport(String report) {

        this.report = report;

    }

    public short getPrecisionUnit() {

		return precisionUnit;

	}

	public void setPrecisionUnit(short precisionUnit) {

		this.precisionUnit = precisionUnit;

	}

    public Integer getSalesOrderCode() {

   	  	return salesOrderCode;

    }

    public void setSalesOrderCode(Integer salesOrderCode) {

        this.salesOrderCode = salesOrderCode;

    }
    
    public String getModuleKey() {

   	  	return moduleKey;

    }

    public void setModuleKey(String moduleKey) {

        this.moduleKey = moduleKey;

    }

    public String getCustomer() {

   	  	return customer;

    }

    public void setCustomer(String customer) {

        this.customer = customer;

    }

    public String getCustomerName() {

    	return customerName;

    }

    public void setCustomerName(String customerName) {

    	this.customerName = customerName;

    }

    public ArrayList getCustomerList() {

   	  	return customerList;

    }

    public void setCustomerList(String customer) {

       	customerList.add(customer);

   	}

    public void clearCustomerList() {

        customerList.clear();
   	  	customerList.add(Constants.GLOBAL_BLANK);

    }

    public String getTransactionType() {
    	return transactionType;
    }

    public void setTransactionType(String transactionType) {
    	this.transactionType = transactionType;
    }


    public ArrayList getTransactionTypeList() {

   	  	return transactionTypeList;

    }

    public void setTransactionTypeList(String transactionType) {

    	transactionTypeList.add(transactionType);

   	}

    public void clearTransactionTypeList() {

    	transactionTypeList.clear();


    }





    public String getDate() {

   	  	return date;

    }

    public void setDate(String date) {

   	  	this.date = date;

    }
    
    
    public String getDocumentType() {
    	return documentType;
    }
    
    public void setDocumentType(String documentType) {
    	this.documentType = documentType;
    }

    public ArrayList getDocumentTypeList() {
		return documentTypeList;
	}

	public void setDocumentTypeList(String documentType) {
		documentTypeList.add(documentType);
	}

	public void clearDocumentTypeList() {
		documentTypeList.clear();
		documentTypeList.add(Constants.GLOBAL_BLANK);
	}


    public String getDocumentNumber() {

   	  	return documentNumber;

    }

    public void setDocumentNumber(String documentNumber) {

   	  	this.documentNumber = documentNumber;

    }

    public String getReferenceNumber() {

   	  	return referenceNumber;

    }

    public void setReferenceNumber(String referenceNumber) {

   	  	this.referenceNumber = referenceNumber;

    }

    public String getPaymentTerm() {

   	  	return paymentTerm;

    }

    public void setPaymentTerm(String paymentTerm) {

   	  	this.paymentTerm = paymentTerm;

    }

    public String getSoAdvanceAmount() {

   	  	return soAdvanceAmount;

    }

    public void setSoAdvanceAmount(String soAdvanceAmount) {

   	  	this.soAdvanceAmount = soAdvanceAmount;

    }


    public ArrayList getPaymentTermList() {

   	  	return paymentTermList;

    }

    public void setPaymentTermList(String paymentTerm) {

   	  	paymentTermList.add(paymentTerm);

    }

    public void clearPaymentTermList() {

   	  	paymentTermList.clear();

    }


public ArrayList getUserList() {

		return userList;

   }

   public void setUserList(String user) {

	   userList.add(user);

   }

   public void clearUserList() {

	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);

   }

   public String getUser() {

	   return user;

   }

   public void setUser(String user) {

	   this.user = user;

   }

    public boolean getSalesOrderVoid() {

        return salesOrderVoid;

    }

    public void setSalesOrderVoid(boolean salesOrderVoid) {

        this.salesOrderVoid = salesOrderVoid;

    }

    public boolean getSalesOrderMobile() {

        return salesOrderMobile;

    }

    public void setSalesOrderMobile(boolean salesOrderMobile) {

        this.salesOrderMobile = salesOrderMobile;

    }

    public String getDescription() {

   	  	return description;

    }

    public void setDescription(String description) {

   	  	this.description = description;

    }
    
    public String getShippingLine() {

   	  	return shippingLine;

    }

    public void setShippingLine(String shippingLine) {

        this.shippingLine = shippingLine;

    }
    

    public String getPort() {

   	  	return port;

    }

    public void setPort(String port) {

        this.port = port;

    }

    public String getBillTo() {

   	  	return billTo;

    }

    public void setBillTo(String billTo) {

        this.billTo = billTo;

    }

    public String getShipTo() {

        return shipTo;

    }

    public void setShipTo(String shipTo) {

        this.shipTo = shipTo;

    }

    public String getTaxCode() {

   	   	return taxCode;

    }

    public void setTaxCode(String taxCode) {

   	   	this.taxCode = taxCode;

    }

    public ArrayList getTaxCodeList() {

   	   	return taxCodeList;

    }

    public void setTaxCodeList(String taxCode) {

   	   	taxCodeList.add(taxCode);

    }

    public void clearTaxCodeList() {

   	   	taxCodeList.clear();
   	  	taxCodeList.add(Constants.GLOBAL_BLANK);

    }

    public String getCurrency() {

   	   	return currency;

    }

    public void setCurrency(String currency) {

   	   	this.currency = currency;

    }

    public ArrayList getCurrencyList() {

   	   	return currencyList;

    }

    public void setCurrencyList(String currency) {

   	   	currencyList.add(currency);

    }

    public void clearCurrencyList() {

   	   	currencyList.clear();

    }

    public String getConversionDate() {

   	   	return conversionDate;

  	}

    public void setConversionDate(String conversionDate) {

   	   	this.conversionDate = conversionDate;

    }

    public String getConversionRate() {

   	   	return conversionRate;

    }

    public void setConversionRate(String conversionRate) {

   	   	this.conversionRate = conversionRate;

    }

    public String getApprovalStatus() {

   	  	return approvalStatus;

    }

    public void setApprovalStatus(String approvalStatus) {

   	  	this.approvalStatus = approvalStatus;

    }

    public String getPosted() {

   	  	return posted;

    }

    public void setPosted(String posted) {

   	  	this.posted = posted;

    }

    public String getReasonForRejection() {

   	  	return reasonForRejection;

    }

    public void setReasonForRejection(String reasonForRejection) {

   	  	this.reasonForRejection = reasonForRejection;

    }

    public String getOrderStatus() {

   	  	return orderStatus;

    }

    public void setOrderStatus(String orderStatus) {

   	  	this.orderStatus = orderStatus;

    }

    public String getCreatedBy() {

   	   	return createdBy;

    }

    public void setCreatedBy(String createdBy) {

   	 	this.createdBy = createdBy;

    }

    public String getDateCreated() {

   	   	return dateCreated;

    }

    public void setDateCreated(String dateCreated) {

   	   	this.dateCreated = dateCreated;

    }

	public String getLastModifiedBy() {

    	return lastModifiedBy;

	}

	public void setLastModifiedBy(String lastModifiedBy) {

   	  	this.lastModifiedBy = lastModifiedBy;

	}

	public String getDateLastModified() {

   	  	return dateLastModified;

	}

	public void setDateLastModified(String dateLastModified) {

   	  	this.dateLastModified = dateLastModified;

  	}

	public String getApprovedRejectedBy() {

	    return approvedRejectedBy;

	}

	public void setApprovedRejectedBy(String approvedRejectedBy) {

	    this.approvedRejectedBy = approvedRejectedBy;

	}

	public String getDateApprovedRejected() {

	    return dateApprovedRejected;

	}

	public void setDateApprovedRejected(String dateApprovedRejected) {

	    this.dateApprovedRejected = dateApprovedRejected;

	}

	public String getPostedBy() {

   	  	return postedBy;

	}

	public void setPostedBy(String postedBy) {

   	  	this.postedBy = postedBy;

	}

	public String getDatePosted() {

   	  	return datePosted;

	}

	public void setDatePosted(String datePosted) {

   	  	this.datePosted = datePosted;

	}
	
	public boolean getSalespersonRequired() {

   	  	return salespersonRequired;

	}

	public void setSalespersonRequired(boolean salespersonRequired) {

   	  	this.salespersonRequired = salespersonRequired;

	}

	public ArrayList getMemoLineList() {

		return memoLineList;

	}

	public void setMemoLineList(String memoLine) {

		memoLineList.add(memoLine);

	}

	public String getIsTypeEntered() {

		return isTypeEntered;

	}

	public String getType() {

		return type;

	}

	public void setType(String type) {


		this.type = type;

	}

	public ArrayList getTypeList() {

		return typeList;

	}

	public String getLocation() {

   	  	return location;

	}

	public void setLocation(String location) {

   	  	this.location = location;

	}

	public ArrayList getLocationList() {

   	  	return locationList;

	}

	public void setLocationList(String location) {

   	  	locationList.add(location);

	}

	public void clearLocationList() {

   	  	locationList.clear();
   	  	locationList.add(Constants.GLOBAL_BLANK);

	}

	public String getFunctionalCurrency() {

   	   	return functionalCurrency;

	}

	public void setFunctionalCurrency(String functionalCurrency) {

   	   	this.functionalCurrency = functionalCurrency;

	}

	public int getRowSOLSelected(){

	    return rowSOLSelected;

	}

	public void setRowSOLSelected(Object selectedArSOLList, boolean isEdit){

	    this.rowSOLSelected = arSOLList.indexOf(selectedArSOLList);

	}

	public void saveArSOLList(Object newArSOLList){

	    arSOLList.add(newArSOLList);

	}

	public void deleteArSOLList(int rowSOLSelected){

	    arSOLList.remove(rowSOLSelected);

	}

	public void clearArSOLList(){

	    arSOLList.clear();

	}

	public ArSalesOrderLineItemList getArSOLByIndex(int index){

   	  	return((ArSalesOrderLineItemList)arSOLList.get(index));

	}

	public Object[] getArSOLList(){

	    return(arSOLList.toArray());

	}

	public int getArSOLListSize(){

	    return(arSOLList.size());

	}

	public void updateArSOLRow(int rowSelected, Object newArSOLList){

	    arSOLList.set(rowSelected, newArSOLList);

	}

	public int getRowINVSelected(){
		return rowINVSelected;
	}

	public ArSalesOrderEntryList getArINVByIndex(int index){
		return((ArSalesOrderEntryList)arINVList.get(index));
	}

	public Object[] getArINVList(){
		return(arINVList.toArray());
	}

	public int getArINVListSize(){
		return(arINVList.size());
	}

	public void saveArINVList(Object newArINVList){
		arINVList.add(newArINVList);
	}

	public void clearArINVList(){
		arINVList.clear();
	}


	public String getReportType() {

		return reportType ;

	}

	public void setReportType(String reportType) {

		this.reportType = reportType;

	}

	public ArrayList getReportTypeList() {
		return reportTypeList;
	}

	public void setReportTypeList(String reportType) {
		reportTypeList.add(reportType);
	}

	public void clearReportTypeList() {
		reportTypeList.clear();
	}


	public String getTransactionStatus() {

   	  	return transactionStatus;

	}

	public void setTransactionStatus(String transactionStatus) {

   	  	this.transactionStatus = transactionStatus;

	}
	
	
	public ArrayList getTransactionStatusList() {
		return transactionStatusList;
	}

	public void setTransactionStatusList(String transactionStatus) {
		transactionStatusList.add(transactionStatus);
	}

	public void clearTransactionStatusList() {
		transactionStatusList.clear();
	}




	public Object[] getReportParameters() {
		return(reportParameters.toArray());
	}

	public void setReportParameters(ArrayList reportParameters) {
		this.reportParameters = reportParameters;
	}


	public ArrayList getReportParametersAsArrayList() {
		return reportParameters;
	}

	public void addReportParameters(ReportParameter reportParameter) {
		reportParameters.add(reportParameter);
	}

	public void clearReportParameters() {
		reportParameters.clear();
	}


	public void setRowINVSelected(Object selectedArINVList, boolean isEdit){
		this.rowINVSelected = arINVList.indexOf(selectedArINVList);
	}

	public void updateArINVRow(int rowSelected, Object newArINVList){
		arINVList.set(rowSelected, newArINVList);
	}

	public void deleteArINVList(int rowSelected){
		arINVList.remove(rowSelected);
	}


	public String getTxnStatus(){

	    String passTxnStatus = txnStatus;
	    txnStatus = Constants.GLOBAL_BLANK;
	    return(passTxnStatus);

	}

	public void setTxnStatus(String txnStatus){

	    this.txnStatus = txnStatus;

	}

	public String getUserPermission(){

	    return(userPermission);

	}

	public void setUserPermission(String userPermission){

	    this.userPermission = userPermission;

	}

    public boolean getEnableFields() {

   	   	return enableFields;

    }

    public void setEnableFields(boolean enableFields) {

   	   	this.enableFields = enableFields;

    }

    public boolean getEnableSalesOrderVoid() {

   	   	return enableSalesOrderVoid;

    }

    public void setEnableSalesOrderVoid(boolean enableSalesOrderVoid) {

   	   	this.enableSalesOrderVoid = enableSalesOrderVoid;

    }

    public boolean getShowAddLinesButton() {

   	   	return showAddLinesButton;

    }

    public void setShowAddLinesButton(boolean showAddLinesButton) {

   	   	this.showAddLinesButton = showAddLinesButton;

    }

    public boolean getShowDeleteLinesButton() {

   	   	return showDeleteLinesButton;

    }

    public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {

   	   	this.showDeleteLinesButton = showDeleteLinesButton;

    }

    public boolean getShowDeleteButton() {

   	   	return showDeleteButton;

    }

    public void setShowDeleteButton(boolean showDeleteButton) {

   	   	this.showDeleteButton = showDeleteButton;

    }

    public boolean getShowSaveButton() {

   	   	return showSaveButton;

    }

    public void setShowSaveAndSubmitButton(boolean showSaveButton) {

   	   	this.showSaveButton = showSaveButton;

    }

    public String getIsCustomerEntered() {

   	   	return isCustomerEntered;

    }

    public boolean getUseCustomerPulldown() {

   	  	return useCustomerPulldown;

    }

    public void setUseCustomerPulldown(boolean useCustomerPulldown) {

   	  	this. useCustomerPulldown = useCustomerPulldown;

    }

    public boolean getShowSaveButtonDraft() {

        return showSaveButtonDraft;

    }

    public void setShowSaveAsDraftButton(boolean showSaveButtonDraft) {

        this.showSaveButtonDraft = showSaveButtonDraft;

    }

    public String getSalesperson() {

    	return salesperson;

    }

    public void setSalesperson(String salesperson) {

    	this.salesperson = salesperson;

    }

    public ArrayList getSalespersonList() {

    	return salespersonList;

    }

    public void setSalespersonList(String salesperson) {

    	salespersonList.add(salesperson);

    }

    public void clearSalespersonList() {

    	salespersonList.clear();
    	salespersonList.add(Constants.GLOBAL_BLANK);

    }

    public ArrayList getSalespersonNameList() {

    	return salespersonNameList;

    }

    public void setSalespersonNameList(String salespersonName) {

    	salespersonNameList.add(salespersonName);

    }

    public void clearSalespersonNameList() {

    	salespersonNameList.clear();
    	salespersonNameList.add(Constants.GLOBAL_BLANK);

    }

    public String getTotalAmount() {

    	return totalAmount;

    }

    public void setTotalAmount(String totalAmount) {

    	this.totalAmount = totalAmount;

    }

	public double getTaxRate() {

		return taxRate;

	}

	public void setTaxRate(double taxRate) {

		this.taxRate = taxRate;

	}

	public String getTaxType() {

		return taxType;

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public String getIsTaxCodeEntered() {

		return isTaxCodeEntered;

	}

	public String getIsConversionDateEntered(){

   		return isConversionDateEntered;

   	}

	public String getMemo() {

		return memo;

	}

	public void setMemo(String memo) {

		this.memo = memo;

	}

	public boolean getEnablePaymentTerm() {

		return enablePaymentTerm;

	}

	public void setEnablePaymentTerm(boolean enablePaymentTerm) {

		this.enablePaymentTerm = enablePaymentTerm;

	}

	public boolean getDisableSalesPrice() {

		return disableSalesPrice;

	}

	public void setDisableSalesPrice(boolean disableSalesPrice) {

		this.disableSalesPrice = disableSalesPrice;

	}

	public ArrayList getTaxList() {
		return taxList;
	}

	public void setTaxList(String tax) {
		this.taxList.add(tax);
	}

	public void clearTaxList() {
		this.taxList.clear();
	}



public Object[] getApproverList(){

		return(approverList.toArray());

	}

	public void saveApproverList(Object newApprover){

		approverList.add(newApprover);

	}

	public void deleteApproverList(int rowSelected){

		approverList.remove(rowSelected);

	}

	public int getApproverListSize(){

		return(approverList.size());

	}

	public void clearApproverList(){

		approverList.clear();

	}
	
	
	public ApproverList getApproverListIndex(int index){

		return((ApproverList)approverList.get(index));

	}
    public void reset(ActionMapping mapping, HttpServletRequest request){

        customer = Constants.GLOBAL_BLANK;
        customerName = Constants.GLOBAL_BLANK;
        date = null;
        documentType = null;
	//    documentNumber = null;
	    referenceNumber = null;
	    moduleKey = null;
	    salesOrderVoid = false;
	    salesOrderMobile = false;
	    description = null;
	    shippingLine = null;
        port = null;
	    billTo = null;
	    shipTo =  null;
	    taxCode = Constants.GLOBAL_BLANK;
	    conversionDate = null;
	    conversionRate = null;
	    approvalStatus = null;
	    posted = null;
	    reasonForRejection = null;
	    orderStatus = null;
	    createdBy = null;
	    dateCreated = null;
	    lastModifiedBy = null;
	    dateLastModified = null;
	    filename1 = null;
		filename2 = null;
		filename3 = null;
		filename4 = null;
	    location = Constants.GLOBAL_BLANK;
	    isTypeEntered = null;
	    isCustomerEntered = null;
	    salesperson = Constants.GLOBAL_BLANK;
	    totalAmount = null;
        taxRate = 0d;
        taxType = Constants.GLOBAL_BLANK;
        isTaxCodeEntered = null;
 	   	isConversionDateEntered = null;
 	   	attachment = null;
 	   	attachmentPDF = null;
 	   	memo = null;
 	   	soAdvanceAmount = null;
		typeList.clear();
		typeList.add("ITEMS");
		typeList.add("MEMO LINES");

		taxList.clear();
		taxList.add("Y");
		taxList.add("N");
		
		logEntry = null;

		reportType = "";

		for (int i=0; i<arINVList.size(); i++) {

			ArSalesOrderEntryList actionList = (ArSalesOrderEntryList)arINVList.get(i);
			actionList.setIsMemoLineEntered(null);
			actionList.setTaxable(false);

		}

	    for (int i=0; i<arSOLList.size(); i++) {

	    	ArSalesOrderLineItemList actionList = (ArSalesOrderLineItemList)arSOLList.get(i);
	  	  	actionList.setIsItemEntered(null);
	  	  	actionList.setIsUnitEntered(null);

	    }

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){

      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null ||
      		request.getParameter("saveAsDraftButton") != null ||
   		 	request.getParameter("printButton") != null) {

      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("salesOrderEntry.error.customerRequired"));
         }

      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("salesOrderEntry.error.dateRequired"));
         }

      	if(!Common.validateDateFormat(date)){
            errors.add("date",
	       new ActionMessage("salesOrderEntry.error.dateInvalid"));
      	}

      	try{

			int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
			if(year<1900){
				errors.add("date",
						new ActionMessage("salesOrderEntry.error.dateInvalid"));
			}
		} catch(Exception ex){

			errors.add("date",
					new ActionMessage("salesOrderEntry.error.dateInvalid"));

		}

      	 if(Common.validateRequired(description)){
             errors.add("description",
                new ActionMessage("salesOrderEntry.error.descriptionIsRequired"));
          }

         if(Common.validateRequired(documentNumber)){
             errors.add("description",
                new ActionMessage("salesOrderEntry.error.documentNumberRequired"));
          }

		 if(Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("paymentTerm",
               new ActionMessage("salesOrderEntry.error.paymentTermRequired"));
         }

         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("salesOrderEntry.error.currencyRequired"));
         }

	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("salesOrderEntry.error.conversionDateInvalid"));
         }

	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("salesOrderEntry.error.conversionRateInvalid"));
         }

         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("salesOrderEntry.error.conversionDateMustBeNull"));
	 	 }

		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
		    errors.add("conversionRate",
		       new ActionMessage("salesOrderEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("salesOrderEntry.error.conversionRateOrDateMustBeEntered"));
		 }
		 
		 
		 System.out.println("salesperson byte: " + salespersonRequired);
		 if(salespersonRequired) {
				
				if(Common.validateRequired(salesperson) || salesperson.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
					
					
					errors.add("salesperson",
							new ActionMessage("preference.prompt.salesOrderSalespersonRequired"));
				}
			}


		 int numOfLines = 0;

      	 Iterator i = arSOLList.iterator();

      	 while (i.hasNext()) {

      		ArSalesOrderLineItemList solList = (ArSalesOrderLineItemList)i.next();

      	 	 if (Common.validateRequired(solList.getItemName()) &&
      	 	 	 Common.validateRequired(solList.getLocation()) &&
      	 	     Common.validateRequired(solList.getQuantity()) &&
      	 	     Common.validateRequired(solList.getUnit()) &&
      	 	     Common.validateRequired(solList.getUnitPrice())) continue;

      	 	 numOfLines++;

	         if(Common.validateRequired(solList.getItemName()) || solList.getItemName().equals(Constants.GLOBAL_BLANK)){
	            errors.add("itemName",
	            	new ActionMessage("salesOrderEntry.error.itemNameRequired", solList.getLineNumber()));
	         }
	         if(Common.validateRequired(solList.getLocation()) || solList.getLocation().equals(Constants.GLOBAL_BLANK)){
	            errors.add("location",
	            	new ActionMessage("salesOrderEntry.error.locationRequired", solList.getLineNumber()));
	         }
	         if(Common.validateRequired(solList.getQuantity())) {
	         	errors.add("quantity",
	         		new ActionMessage("salesOrderEntry.error.quantityRequired", solList.getLineNumber()));
	         }
		 	 if(!Common.validateNumberFormat(solList.getQuantity())){
	            errors.add("quantity",
	               new ActionMessage("salesOrderEntry.error.quantityInvalid", solList.getLineNumber()));
	         }
		 	 if(!Common.validateRequired(solList.getQuantity()) && Common.convertStringMoneyToDouble(solList.getQuantity(), (short)3) <= 0) {
	         	errors.add("quantity",
	         		new ActionMessage("salesOrderEntry.error.negativeOrZeroQuantityNotAllowed", solList.getLineNumber()));
	         }
		 	 if(Common.validateRequired(solList.getUnit()) || solList.getUnit().equals(Constants.GLOBAL_BLANK)){
	            errors.add("unit",
	            	new ActionMessage("salesOrderEntry.error.unitRequired", solList.getLineNumber()));
	         }
		 	 if(Common.validateRequired(solList.getUnitPrice())){
	            errors.add("unitCost",
	               new ActionMessage("salesOrderEntry.error.unitPriceRequired", solList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(solList.getUnitPrice())){
	            errors.add("unitCost",
	               new ActionMessage("salesOrderEntry.error.unitPriceInvalid", solList.getLineNumber()));
	         }

	    }

	    if (numOfLines == 0) {

         	errors.add("customer",
               new ActionMessage("salesOrderEntry.error.salesOrderMustHaveItemLine"));

        }

      } else if (!Common.validateRequired(request.getParameter("isCustomerEntered"))) {

      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("salesOrderEntry.error.customerRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isBillAmountEntered"))) {

      	 if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("customer",
               new ActionMessage("salesOrderEntry.error.customerRequired"));
         }

		 if(Common.validateRequired(taxCode) || taxCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("taxCode",
               new ActionMessage("salesOrderEntry.error.taxCodeRequired"));
         }

      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

    	  if(!Common.validateDateFormat(conversionDate)){

    		  errors.add("conversionDate", new ActionMessage("salesOrderEntry.error.conversionDateInvalid"));

    	  }

      }

      return(errors);
   }
}

