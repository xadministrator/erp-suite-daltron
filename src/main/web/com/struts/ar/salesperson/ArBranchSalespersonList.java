package com.struts.ar.salesperson;

import java.io.Serializable;


/**
 * 
 * @author Franco Antonio R. Roig
 * Created: 10/24/2005 5:11 PM
 * 
 */
public class ArBranchSalespersonList implements Serializable {
	
	private String brName = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;
	
	private ArSalespersonForm parentBean;
	
	public ArBranchSalespersonList(ArSalespersonForm parentBean, 
			String brName, Integer brCode) {
		
		this.parentBean = parentBean;
		this.brName = brName;
		this.brCode = brCode;
		
	}
	
	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
		
}