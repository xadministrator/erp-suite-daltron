package com.struts.ar.salesperson;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArSalespersonController;
import com.ejb.txn.ArSalespersonControllerHome;
import com.struts.util.Branch;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdResponsibilityDetails;
import com.util.ArSalespersonDetails;

public final class ArSalespersonAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
			/*******************************************************
			 Check if user has a session
			 *******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("ArSalespersonAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			ArSalespersonForm actionForm = (ArSalespersonForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.AR_SALESPERSON_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("arSalesperson");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
			/*******************************************************
			 Initialize ArSalespersonController EJB
			 *******************************************************/
			
			ArSalespersonControllerHome homeSLP = null;
			ArSalespersonController ejbSLP = null;
			
			try {
				
				homeSLP = (ArSalespersonControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArSalespersonControllerEJB", ArSalespersonControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in ArSalespersonAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbSLP = homeSLP.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in ArSalespersonAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
			
			/*******************************************************
			 Call ArSalespersonController EJB
			 getGlFcPrecisionUnit
			 *******************************************************/
			
			short precisionUnit = 0;
			
			try {
				
				precisionUnit = ejbSLP.getGlFcPrecisionUnit(user.getCmpCode());        
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in ArSalespersonAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			
			/*******************************************************
			 -- Ar SLP Show Details Action --
			 *******************************************************/
			
			if (request.getParameter("showDetailsButton") != null) {
				
				actionForm.setTableType(Constants.GLOBAL_DETAILED);
				
				return(mapping.findForward("arSalesperson"));
				
				/*******************************************************
				 -- Ar SLP Hide Details Action --
				 *******************************************************/	     
				
			} else if (request.getParameter("hideDetailsButton") != null) { 
				
				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
				
				return(mapping.findForward("arSalesperson"));                  
				
				/*******************************************************
				 -- Ar SLP Save Action --
				 *******************************************************/
				
			} else if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				ArSalespersonDetails details = new ArSalespersonDetails();
				details.setSlpSalespersonCode(actionForm.getSalespersonCode());
				details.setSlpName(actionForm.getSalespersonName());
				details.setSlpEntryDate(Common.convertStringToSQLDate(actionForm.getEntryDate()));
				details.setSlpAddress(actionForm.getAddress());
				details.setSlpPhone(actionForm.getPhone());
				details.setSlpMobilePhone(actionForm.getMobilePhone());    
				details.setSlpEmail(actionForm.getEmail());

				// For Branch Setting
	            ArrayList branchList = new ArrayList();
	            
	            for(int i=0; i<actionForm.getArBSlpListSize(); i++) {
		        	ArBranchSalespersonList bslpList = actionForm.getArBSlpByIndex(i);
		        	
		        	if(bslpList.getBranchCheckbox() == true) {
		        		
						if(bslpList.getBranchCheckbox() == true) {
							
							AdBranchDetails brDetails = new AdBranchDetails();                                          
							
							brDetails.setBrAdCompany(user.getCmpCode());	     	        
							brDetails.setBrCode(bslpList.getBrCode());                    
							
							branchList.add(brDetails);
						}		
						
					}
		        	
	            }
				
				try {
					
					ejbSLP.addArSlpEntry(details, branchList, user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("salesperson.error.recordAlreadyExist"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArSalespersonAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				/*******************************************************
				 -- Ar SLP Close Action --
				 *******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
				/*******************************************************
				 -- Ar SLP Update Action --
				 *******************************************************/
				
			} else if (request.getParameter("updateButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				
				ArSalespersonList arSlpList =
					actionForm.getArSlpByIndex(actionForm.getRowSelected());
				
				
				ArSalespersonDetails details = new ArSalespersonDetails();
				details.setSlpCode(arSlpList.getSlpCode());
				details.setSlpSalespersonCode(actionForm.getSalespersonCode());
				details.setSlpName(actionForm.getSalespersonName());
				details.setSlpEntryDate(Common.convertStringToSQLDate(actionForm.getEntryDate()));
				details.setSlpAddress(actionForm.getAddress());
				details.setSlpPhone(actionForm.getPhone());
				details.setSlpMobilePhone(actionForm.getMobilePhone());
				details.setSlpEmail(actionForm.getEmail());

				// For Branch Map Setting
	            ArrayList branchList = new ArrayList();
	            
	            for(int i=0; i<actionForm.getArBSlpListSize(); i++) {
		        	ArBranchSalespersonList bslpList = actionForm.getArBSlpByIndex(i);
		        	
		        	if(bslpList.getBranchCheckbox() == true) {
		        		
						if(bslpList.getBranchCheckbox() == true) {
							
							AdBranchDetails brDetails = new AdBranchDetails();                                          
							
							brDetails.setBrAdCompany(user.getCmpCode());	     	        
							brDetails.setBrCode(bslpList.getBrCode());                    
							
							branchList.add(brDetails);
						}		
						
					}
		        	
	            }

				
	            // get the selected responsibility name
	            AdResponsibilityDetails rsDetails = ejbSLP.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
	            
				try {
					
					ejbSLP.updateArSlpEntry(details, rsDetails.getRsName(), branchList, user.getCmpCode());
					
				} catch (GlobalRecordAlreadyExistException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("salesperson.error.recordAlreadyExist"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArSalespersonAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				/*******************************************************
				 -- Ar SLP Cancel Action --
				 *******************************************************/
				
			} else if (request.getParameter("cancelButton") != null) {
				
				/*******************************************************
				 -- Ar SLP Edit Action --
				 *******************************************************/
				
			} else if (request.getParameter("arSlpList[" + 
					actionForm.getRowSelected() + "].editButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				actionForm.reset(mapping, request);
				Object[] arSlpList = actionForm.getArSlpList();
				
				// get list of branches
	        	ArrayList branchList = new ArrayList();
	        	
	        	AdResponsibilityDetails details = ejbSLP.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
	        	
	         	try {
	         		branchList = ejbSLP.getAdBrSlpAll(((ArSalespersonList)arSlpList[actionForm.getRowSelected()]).getSlpCode(), details.getRsName(), user.getCmpCode());
	         	} catch(GlobalNoRecordFoundException ex) {
	         			
	         	}				
	            
	         	actionForm.showArSlpRow(branchList, actionForm.getRowSelected());
				
				return mapping.findForward("arSalesperson");
				
				/*******************************************************
				 -- Ar SLP Delete Action --
				 *******************************************************/
				
			} else if (request.getParameter("arSlpList[" +
					actionForm.getRowSelected() + "].deleteButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				ArSalespersonList arSlpList =
					actionForm.getArSlpByIndex(actionForm.getRowSelected());
				
				try {
					
					ejbSLP.deleteArSlpEntry(arSlpList.getSlpCode(), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("salesperson.error.salespersonAlreadyDeleted"));
				
				} catch (GlobalRecordAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("salesperson.error.salespersonAlreadyAssigned"));
					
				} catch(EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArSalespersonAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}
					
					return(mapping.findForward("cmnErrorPage"));
					
				}            
				
/*******************************************************
 -- Ar SLP Load Action --
*******************************************************/
				
			}
			
			if (frParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("arSalesperson");
					
				}
				
				ArrayList list = null;
				Iterator i = null;
				
				try {
					
					actionForm.clearArSlpList();	                               
					
					list = ejbSLP.getArSlpAll(user.getCmpCode()); 
					
					i = list.iterator();
					
					while(i.hasNext()) {
						
						ArSalespersonDetails details = (ArSalespersonDetails)i.next();
						
						ArSalespersonList arSlpList = new ArSalespersonList(actionForm,
								details.getSlpCode(),
								details.getSlpSalespersonCode(),
								details.getSlpName(),
								Common.convertSQLDateToString(details.getSlpEntryDate()),
								details.getSlpAddress(),
								details.getSlpPhone(),
								details.getSlpMobilePhone(),
								details.getSlpEmail());                            
						
						actionForm.saveArSlpList(arSlpList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in ArSalespersonAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				actionForm.reset(mapping, request);
				
				actionForm.clearArBSlpList();
				
				for (int j = 0; j < user.getBrnchCount(); j++) {

					 Branch branch = user.getBranch(j);
					 AdBranchDetails details = new AdBranchDetails();
					 details.setBrCode(new Integer(branch.getBrCode()));
					 details.setBrName(branch.getBrName());
					 ArBranchSalespersonList arBSlpList = new ArBranchSalespersonList(actionForm,
							details.getBrName(), details.getBrCode());
					 
					 if ( request.getParameter("forward") == null )
					 	arBSlpList.setBranchCheckbox(true);
					 
					 actionForm.saveArBSlpList(arBSlpList);
				}

				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if (request.getParameter("saveButton") != null || 
							request.getParameter("updateButton") != null) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}
				}
				
				if (actionForm.getTableType() == null) {
					
					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
					
				}	                                  
				
				actionForm.setPageState(Constants.PAGE_STATE_SAVE);
				
				actionForm.setEntryDate(Common.convertSQLDateToString(new java.util.Date()));
				
				return(mapping.findForward("arSalesperson"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
			/*******************************************************
			 System Failed: Forward to error page 
			 *******************************************************/
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in ArSalespersonAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
				e.printStackTrace();
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
	
}