package com.struts.ar.salesperson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdBranchDetails;


public class ArSalespersonForm extends ActionForm implements Serializable {
	
	private String salespersonCode = null;
	private String salespersonName = null;
	private String entryDate = null;
	private String address = null;
	private String phone = null;
	private String mobilePhone = null;
	private String email = null; 
	
	private String tableType = null;
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList arSlpList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private ArrayList arBSlpList = new ArrayList();
	
	public String getSalespersonCode() {
		return salespersonCode;
	}
	
	public void setSalespersonCode(String salespersonCode) {
		this.salespersonCode = salespersonCode;
	}
	
	public String getSalespersonName() {
		return salespersonName;
	}
	
	public void setSalespersonName(String salespersonName) {
		this.salespersonName = salespersonName;
	}
	
	public String getEntryDate() {
		return entryDate;
	}
	
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getMobilePhone() {
		return mobilePhone;
	}
	
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public ArSalespersonList getArSlpByIndex(int index) {
		
		return((ArSalespersonList)arSlpList.get(index));
		
	}
	
	public Object[] getArSlpList() {
		
		return arSlpList.toArray();
		
	}
	
	public int getArSlpListSize() {
		
		return arSlpList.size();
		
	}
	
	public void saveArSlpList(Object newArSlpList) {
		
		arSlpList.add(newArSlpList);
		
	}
	
	public void clearArSlpList() {
		
		arSlpList.clear();
		
	}
	
	public void setRowSelected(Object selectedArSLPList, boolean isEdit) {
		
		this.rowSelected = arSlpList.indexOf(selectedArSLPList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void showArSlpRow(ArrayList branchList, int rowSelected) {
		
		this.salespersonCode = ((ArSalespersonList)arSlpList.get(rowSelected)).getSalespersonCode(); 
		this.salespersonName = ((ArSalespersonList)arSlpList.get(rowSelected)).getSalespersonName();
		this.entryDate = (((ArSalespersonList)arSlpList.get(rowSelected)).getEntryDate());
		this.address = ((ArSalespersonList)arSlpList.get(rowSelected)).getAddress();
		this.phone = ((ArSalespersonList)arSlpList.get(rowSelected)).getPhone(); 
		this.mobilePhone = ((ArSalespersonList)arSlpList.get(rowSelected)).getMobilePhone();
		this.email = ((ArSalespersonList)arSlpList.get(rowSelected)).getEmail();
				
		Object[] obj = this.getarBSlpList();
		
		this.clearArBSlpList();

		for(int i=0; i<obj.length; i++) {
			ArBranchSalespersonList bslpList = (ArBranchSalespersonList)obj[i];
			
			if(branchList != null) {
				Iterator j = branchList.iterator();
				
				while(j.hasNext()) {
					AdBranchDetails details = (AdBranchDetails)j.next();
					if (details.getBrCode().equals(bslpList.getBrCode())) {
						bslpList.setBranchCheckbox(true);
						break;
					}
				}
			}
			
			this.arBSlpList.add(bslpList);
		}
	}
	
	
	public void updateArSMLRow(int rowSelected, Object newArSLPList) {
		
		arSlpList.set(rowSelected, newArSLPList);
		
	}
	
	public void deleteArSLPList(int rowSelected) {
		
		arSlpList.remove(rowSelected);
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
		
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public Object[] getarBSlpList(){
		
		return arBSlpList.toArray();
		
	}
	
	public ArBranchSalespersonList getArBSlpByIndex(int index) {
		
		return ((ArBranchSalespersonList)arBSlpList.get(index));
		
	}
	
	public int getArBSlpListSize(){
		
		return(arBSlpList.size());
		
	}
	
	public void saveArBSlpList(Object newarBSlpList){
		
		arBSlpList.add(newarBSlpList);   	  
		
	}
	
	public void clearArBSlpList(){
		
		arBSlpList.clear();
		
	}
	
	public void setArBSlpList(ArrayList arBSlpList) {
		
		this.arBSlpList = arBSlpList;
		
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<arBSlpList.size(); i++) {
			
			ArBranchSalespersonList actionList = (ArBranchSalespersonList)arBSlpList.get(i);
			actionList.setBranchCheckbox(false);
			
		}
		
		salespersonCode = null;
		salespersonName = null;
		entryDate = null;
		address = null;
		phone = null;
		mobilePhone = null;
		email = null;
		showDetailsButton = null;
		hideDetailsButton = null;      
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
			
			if (Common.validateRequired(salespersonCode)) {
				
				errors.add("salespersonCode",
						new ActionMessage("salesperson.error.salespersonCodeRequired"));
				
			}
			
			if (Common.validateRequired(salespersonName)) {
				
				errors.add("salespersonName",
						new ActionMessage("salesperson.error.salespersonNameRequired"));
				
			}
			
			if (Common.validateRequired(entryDate)) {
				
				errors.add("entryDate",
						new ActionMessage("salesperson.error.entryDateRequired"));
				
			}
			
			if(!Common.validateDateFormat(entryDate)) {
				
				errors.add("entryDate",
						new ActionMessage("salesperson.error.entryDateInvalid"));
				
			}
			
		}
		
		return errors;
		
	}
	
}