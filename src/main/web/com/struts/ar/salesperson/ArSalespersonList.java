package com.struts.ar.salesperson;

import java.io.Serializable;

public class ArSalespersonList implements Serializable {

	private Integer slpCode = null;
    private String salespersonCode = null;
    private String salespersonName = null;
    private String entryDate = null;
    private String address = null;
    private String phone = null;
    private String mobilePhone = null;
    private String email = null;
    
	private String editButton = null;
	private String deleteButton = null;
	
	private ArSalespersonForm parentBean;
    
	public ArSalespersonList(ArSalespersonForm parentBean,
		Integer slpCode,
		String salespersonCode,
		String salespersonName,
        String entryDate,
        String address,
		String phone,
        String mobilePhone,
        String email) {
	
		this.slpCode = slpCode;
		this.parentBean = parentBean;
		this.salespersonCode = salespersonCode;
		this.salespersonName = salespersonName;
		this.entryDate = entryDate;
		this.address = address;
		this.phone = phone;
		this.mobilePhone = mobilePhone;
		this.email = email;
		
	}
	
	public void setEditButton(String editButton) {
	    parentBean.setRowSelected(this, true);
	}
	
	public void setDeleteButton(String deleteButton) {
	    parentBean.setRowSelected(this, true);
	}   
	
	public Integer getSlpCode() {
		return slpCode;
	}
	
	public String getSalespersonCode() {
		return salespersonCode;
	}
	
	public String getSalespersonName() {
		return salespersonName;
	}
	
	public String getEntryDate() {
		return entryDate;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public String getMobilePhone() {
		return mobilePhone;
	}
	
	public String getEmail() {
		return email;
	}
        	    	
}