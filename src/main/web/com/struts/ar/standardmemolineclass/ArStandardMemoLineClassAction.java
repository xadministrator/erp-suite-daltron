
package com.struts.ar.standardmemolineclass;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.ArStandardMemoLineClassController;
import com.ejb.txn.ArStandardMemoLineClassControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.ArCustomerDetails;
import com.util.ArModStandardMemoLineClassDetails;
import com.util.ArStandardMemoLineDetails;
import java.util.ArrayList;
import java.util.Iterator;
import javax.ejb.FinderException;

public class ArStandardMemoLineClassAction extends Action{
    
     private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

     
    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        
        HttpSession session = request.getSession();
        
        try{
            User user = (User) session.getAttribute(Constants.USER_KEY);
         
            if (user != null) {
                if (log.isInfoEnabled()) {

                    log.info("ArStandardMemoLineClassAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                       "' performed this action on session " + session.getId());
                }   
            }else{
         	
                if (log.isInfoEnabled()) {
            	
                    log.info("User is not logged on in session" + session.getId());
                }
                return(mapping.findForward("adLogon"));
            
            }
            
            ArStandardMemoLineClassForm actionForm = (ArStandardMemoLineClassForm)form;
            
            String frParam = Common.getUserPermission(user, Constants.AR_STANDARD_MEMO_LINE_ID);
			
            if (frParam != null) {

                if (frParam.trim().equals(Constants.FULL_ACCESS)) {

                    ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

                    if (!fieldErrors.isEmpty()) {

                        saveErrors(request, new ActionMessages(fieldErrors));

                        return mapping.findForward("arStandardMemoLineClass");
                    }

                }

                actionForm.setUserPermission(frParam.trim());
                
                
                

            } else {

                actionForm.setUserPermission(Constants.NO_ACCESS);

            }
           
            /*******************************************************
            Initialize ArInvoiceEntryController EJB
            *******************************************************/
            
            ArStandardMemoLineClassControllerHome homeSMC = null;
            ArStandardMemoLineClassController ejbSMC = null;

            try {

                homeSMC = (ArStandardMemoLineClassControllerHome)com.util.EJBHomeFactory.
                   lookUpHome("ejb/ArStandardMemoLineClassControllerEJB", ArStandardMemoLineClassControllerHome.class);

            } catch(NamingException e) {
                if(log.isInfoEnabled()){
                   log.info("NamingException caught in ArStandardMemoLineClassAction.execute(): " + e.getMessage() +
                  " session: " + session.getId());
                }
               return(mapping.findForward("cmnErrorPage"));
            }

            try {

                ejbSMC = homeSMC.create();

            } catch(CreateException e) {

                if(log.isInfoEnabled()) {

                    log.info("CreateException caught in ArStandardMemoLineClassAction.execute(): " + e.getMessage() +
                      " session: " + session.getId());
                }

                return(mapping.findForward("cmnErrorPage"));

            }

            ActionErrors errors = new ActionErrors();
            ActionMessages messages = new ActionMessages();
            
            short smcLineNumber = 3;
            
            
            /*******************************************************
            -- Ar SMC Save Memo Line Class --
            *******************************************************/

            if (request.getParameter("saveButton") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
               
                boolean isError = false;

                ArrayList list = new ArrayList();
                ArrayList duplicateCheckList = new ArrayList();
                
                try{
                    for(int x=0;x<actionForm.getArSmcListSize();x++){
               
                        ArStandardMemoLineClassList arSmcList = (ArStandardMemoLineClassList)actionForm.getArSmcByIndex(x);
            
                     
                        if(arSmcList.getMemoLine()==null){
                            continue;
                        }
                     
                     
 
                        if(!arSmcList.getMemoLine().equals("") && arSmcList.getMemoLine()!=null){
                      
                            ArModStandardMemoLineClassDetails details = new ArModStandardMemoLineClassDetails();

                            //new lines check if already exist
                            if(arSmcList.getSmcCode()==null){

                                String tempStringDataLine = arSmcList.getMemoLine() + "|" + arSmcList.getCustomerCode()+ "|" + arSmcList.getBranchName() ;
                               
                                
                                details.setSmcCode(null);
                                try{
                                    ArModStandardMemoLineClassDetails chkDetail = ejbSMC.getSmcByCcNmSmlNmCstCstmrCodeBrNm(actionForm.getCustomerClassName(), arSmcList.getMemoLine(),arSmcList.getCustomerCode(), arSmcList.getBranchName(), user.getCmpCode());


                                    System.out.println("found duplicate in existing add");
                                    isError = true;
                                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                                        new ActionMessage("standardMemoLineClass.error.memolineClassAlreadyExist",arSmcList.getLineNumber()));
                                    continue;


                                }catch(FinderException ex){

                                   // means no duplicate - good data!
                                }
                                
                                 //check current add duplicate check List
                                if(duplicateCheckList.size()>0 && duplicateCheckList.contains(tempStringDataLine)){
                                    System.out.println("found duplicate in new add");
                                    isError = true;
                                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                                        new ActionMessage("standardMemoLineClass.error.memolineClassAlreadyExist",arSmcList.getLineNumber()));
                                    continue;     
                                }
                                
                                duplicateCheckList.add(tempStringDataLine);
                                

                            }else{
                                details.setSmcCode(Integer.parseInt(arSmcList.getSmcCode()));
                            }

                            details.setSmcLine(arSmcList.getLineNumber());     
                            details.setSmcCustomerClassName(actionForm.getCustomerClassName());
                            details.setSmcStandardMemoLineName(arSmcList.getMemoLine());
                            details.setSmcStandardMemoLineDescription(arSmcList.getDescription());
                            details.setSmcUnitPrice(Common.convertStringToDouble(arSmcList.getUnitPrice()));
                            details.setSmcCustomerCode(arSmcList.getCustomerCode());
                            details.setSmcAdBranchName(arSmcList.getBranchName());


                            list.add(details);
                        }
                        
                        
                     

                    }

                  
                    if(list.size()<=0){
                        isError = true;
                        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("standardMemoLineClass.error.noMemoLineClassToSave"));

                    }else{
                        if(!isError){
                      
                            ejbSMC.saveArSmcEntry(list,  user.getUserName(), actionForm.getCustomerClassName(),  user.getCmpCode()); 
                           
                            actionForm.clearArSmcList();
                            int lineNumber = 0;
                            ArrayList smclist = ejbSMC.getAllSmcByCcNm(actionForm.getCustomerClassName(), user.getCmpCode());

                            for(int x=0;x<smclist.size();x++){

                                ArModStandardMemoLineClassDetails details = (ArModStandardMemoLineClassDetails)smclist.get(x);


                                ArStandardMemoLineClassList arSmcList = new ArStandardMemoLineClassList(actionForm, details.getSmcCode().toString(),
                                String.valueOf(++lineNumber), null, details.getSmcStandardMemoLineDescription(),Double.toString(details.getSmcUnitPrice()),null );	       

                                arSmcList.setMemoLineList(actionForm.getStandardMemoLineList());
                                arSmcList.setMemoLine(details.getSmcStandardMemoLineName());
                                arSmcList.setBranchList(actionForm.getBranchList());
                                arSmcList.setBranchName(details.getSmcAdBranchName());
                                arSmcList.setCustomerCodeList(actionForm.getCustomerList());
                                arSmcList.setCustomerCode(details.getSmcCustomerCode());
                                arSmcList.setCustomerName(details.getSmcCustomerName());
                             
                                arSmcList.setIsListed(true);

                                actionForm.saveArSmcList(arSmcList);
                            }


                            actionForm.setShowSaveButton(true);
                            actionForm.setShowAddLines(true);
                        
                        
                        
                        
                        }

                    }
                }catch(Exception ex){
                    if (log.isInfoEnabled()) {

                        log.info("EJBException caught in ArStandardMemoLineClassAction.execute(): " + ex.getMessage() +
                                " session: " + session.getId());
                        return mapping.findForward("cmnErrorPage"); 

                     }
                }

                
                if (!errors.isEmpty()) {
			
                    System.out.println("error");
                    saveErrors(request, new ActionMessages(errors));
                    return mapping.findForward("arStandardMemoLineClass");

                }
                
                

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
        
                return(mapping.findForward("arStandardMemoLineClass"));    
    
            }
            
            /*******************************************************
            -- Ar SMC Close Action --
            *******************************************************/
				
            else if (request.getParameter("closeButton") != null) {

                   return(mapping.findForward("cmnMain"));
            
            }
            /*******************************************************
            -- Ar SMC Add Line Button --
            *******************************************************/
            else if(request.getParameter("addLinesButton") != null  && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
               
                System.out.println("Add Line Button");
                int listSize = actionForm.getArSmcListSize();
                int lineNumber = 0;
                
                for (int x = listSize + 1; x <= listSize + smcLineNumber; x++) {
                 
                    ArStandardMemoLineClassList arSmcList = new ArStandardMemoLineClassList(actionForm, null,String.valueOf(++lineNumber), null, null, "0", null);	       

                    arSmcList.setMemoLineList(actionForm.getStandardMemoLineList());
                    arSmcList.setBranchList(actionForm.getBranchList());

                     arSmcList.setCustomerCodeList(actionForm.getCustomerList());
                      
                    actionForm.saveArSmcList(arSmcList);
                 
                }	
                System.out.println(actionForm.getArSmcListSize() + " size");
                for (int i = 0; i<actionForm.getArSmcListSize(); i++) {
                 
                    ArStandardMemoLineClassList arSmcList = actionForm.getArSmcByIndex(i);

                    arSmcList.setLineNumber(String.valueOf(i+1));
                 
                }        
             
      
                return(mapping.findForward("arStandardMemoLineClass"));
            }
            /*******************************************************
            -- Ar SMC Delete Line --
            *******************************************************/
            else if (request.getParameter("arSmcList[" + 
					actionForm.getRowSelected() + "].deleteButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                System.out.println("delete button");
                
            
                ArStandardMemoLineClassList arSmcList = actionForm.getArSmcByIndex(actionForm.getRowSelected());
                
                if(arSmcList.getIsListed()){
                    //some data that already added
                    
                    int smcCode = Integer.parseInt(arSmcList.getSmcCode());
                    
                    try{
                        ejbSMC.deleteArSmcEntry(smcCode);
                        actionForm.deleteArSmcList(actionForm.getRowSelected());
                        
                        
                    }catch(GlobalRecordAlreadyDeletedException ex){
                        errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("standardMemoLineClass.error.memoLineClassAreadyDeleted"));

                    }
                             
                }else{
                    //some added frequently
                    actionForm.deleteArSmcList(actionForm.getRowSelected());
                
                }

                int lineNumber = 1;  
                //recalculate line number
                 for (int i = 0; i<actionForm.getArSmcListSize(); i++) {
                    ArStandardMemoLineClassList mdetails = actionForm.getArSmcByIndex(i);
                    mdetails.setLineNumber(Integer.toString(lineNumber));
                    lineNumber++;
                 }
                
                if (!errors.isEmpty()) {
			
                    System.out.println("error");
                    saveErrors(request, new ActionMessages(errors));
                    return mapping.findForward("arStandardMemoLineClass");

                }
                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
        
         
                return(mapping.findForward("arStandardMemoLineClass"));
            }
            
             /*******************************************************
            -- Ar SMC Customer Entered --
            *******************************************************/
            else if (!Common.validateRequired(request.getParameter("arSmcList[" + 
                actionForm.getRowSelected()+ "].isCustomerEntered"))){
                     
                ArStandardMemoLineClassList arSmcList = actionForm.getArSmcByIndex(actionForm.getRowSelected()); 
                
                System.out.println("is Customer Entered");
                if(arSmcList.getCustomerCode().equals("") || arSmcList.getCustomerCode()==null){
                    
                    arSmcList.setCustomerName(Constants.GLOBAL_BLANK);
  
                }else{
                    try{
                        ArCustomerDetails cstDetails = ejbSMC.getArCstByCstCstmrCode(arSmcList.getCustomerCode(), user.getCmpCode());

                        arSmcList.setCustomerName(cstDetails.getCstName());

                     
                    }catch(GlobalNoRecordFoundException ex){
                         errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("standardMemoLineClass.error.memoLineNotExist" ,arSmcList.getCustomerCode()));
 
                    }catch (EJBException ex) {

                        if (log.isInfoEnabled()) {

                           log.info("EJBException caught in ArStandardMemoLineClassAction.execute(): " + ex.getMessage() +
                                   " session: " + session.getId());
                           return mapping.findForward("cmnErrorPage"); 

                        }
                    }
                
                 
                }  
                saveErrors(request, new ActionMessages(errors));
                return(mapping.findForward("arStandardMemoLineClass"));
         
            }
            /*******************************************************
            -- Ar SMC Memo Line Entered --
            *******************************************************/
            else if (!Common.validateRequired(request.getParameter("arSmcList[" + 
                actionForm.getRowSelected()+ "].isMemoLineEntered"))){
                     
                ArStandardMemoLineClassList arSmcList = actionForm.getArSmcByIndex(actionForm.getRowSelected()); 
                
                System.out.println("is MemoLine enterd");
                if(arSmcList.getMemoLine().equals("") ||arSmcList.getMemoLine()==null){
                    
                    arSmcList.setDescription("");
                    arSmcList.setUnitPrice("0");
                    arSmcList.setBranchName(Constants.GLOBAL_BLANK);
                    
                    
                    
                }else{
                    try{
                        ArStandardMemoLineDetails smlDetails = ejbSMC.getArSmlBySmlName(arSmcList.getMemoLine(), user.getCmpCode());


                        arSmcList.setDescription(smlDetails.getSmlDescription());
                        arSmcList.setUnitPrice(Double.toString(smlDetails.getSmlUnitPrice()));
        
                        arSmcList.setBranchName(user.getCurrentBranch().getBrName());
                     
                    }catch(GlobalNoRecordFoundException ex){
                         errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("standardMemoLineClass.error.memoLineNotExist" ,arSmcList.getMemoLine() ));
 
                    }catch (EJBException ex) {

                        if (log.isInfoEnabled()) {

                           log.info("EJBException caught in ArStandardMemoLineClassAction.execute(): " + ex.getMessage() +
                                   " session: " + session.getId());
                           return mapping.findForward("cmnErrorPage"); 

                        }
                    }
                
                 
                }  	
                saveErrors(request, new ActionMessages(errors));
                return(mapping.findForward("arStandardMemoLineClass"));
            }
           
            /*******************************************************
            -- Ar SMC Customer Class Entered --
            *******************************************************/
            
            else if (request.getParameter("isCustomerClassEntered") != null && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                
                
          
                if(actionForm.getCustomerClassName().equals("")){
                    
                    actionForm.clearArSmcList();   
                    actionForm.setShowSaveButton(false);
                    actionForm.setShowAddLines(false);
                }else{
                    
                    actionForm.clearArSmcList();
                    int lineNumber = 0;
                    ArrayList list = ejbSMC.getAllSmcByCcNm(actionForm.getCustomerClassName(), user.getCmpCode());
                    
                    for(int x=0;x<list.size();x++){
                        
                        ArModStandardMemoLineClassDetails details = (ArModStandardMemoLineClassDetails)list.get(x);
                        
                        
                  
                        ArStandardMemoLineClassList arSmcList = new ArStandardMemoLineClassList(actionForm, details.getSmcCode().toString(),
                        String.valueOf(++lineNumber), null, details.getSmcStandardMemoLineDescription(),Double.toString(details.getSmcUnitPrice()), null);	       
                        
                        arSmcList.setMemoLineList(actionForm.getStandardMemoLineList());
                        arSmcList.setMemoLine(details.getSmcStandardMemoLineName());
          
                        arSmcList.setDescription(details.getSmcStandardMemoLineDescription());
                        arSmcList.setBranchList(actionForm.getBranchList());
                        arSmcList.setBranchName(details.getSmcAdBranchName());
                        arSmcList.setCustomerCodeList(actionForm.getCustomerList());
                        arSmcList.setCustomerCode(details.getSmcCustomerCode());
                        arSmcList.setCustomerName(details.getSmcCustomerName());
                
                        
                        
                        arSmcList.setIsListed(true);

                        actionForm.saveArSmcList(arSmcList);
                    }

                    
                    actionForm.setShowSaveButton(true);
                    actionForm.setShowAddLines(true);
                }
                
            }
           
            
           
            if (frParam != null) {
               

                ArrayList list = null;
                Iterator i = null;
                
                //Populate Customer Class Names
                actionForm.clearCustomerClassList();
                
                list = ejbSMC.getAllCcNm(user.getCmpCode());
                
                i = list.iterator();

                while (i.hasNext()) {

                    String customerClassName = (String)i.next();
                    actionForm.setCustomerClassList(customerClassName);

                }
                
                //Populate Memo Lines
                actionForm.clearStandardMemoLineList();
                
                list = ejbSMC.getAllSmlNm(user.getCmpCode());
                
                i = list.iterator();

                while (i.hasNext()) {

                    String memoLine = (String)i.next();
                    actionForm.setStandardMemoLineList(memoLine);

                }
                
                
                //Populate Customer
                actionForm.clearCustomerList();
                list = ejbSMC.getAllCst(user.getCmpCode());
                
                i = list.iterator();

                while (i.hasNext()) {

                    String customer = (String)i.next();
                    actionForm.setCustomerList(customer);

                }
                
                

                //Populate Branch
                actionForm.clearBranchList();
                list = ejbSMC.getAllBrNm(user.getCmpCode());

                i = list.iterator();

                while (i.hasNext()) {

                    String branch = (String)i.next();
                    actionForm.setBranchList(branch);

                }
                actionForm.reset(mapping, request);
                
                
               
                
                
                return(mapping.findForward("arStandardMemoLineClass"));
                
            } else {
				
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return(mapping.findForward("cmnMain"));

            }
            
            
            
           
        }catch(Exception e) {

            /*******************************************************
               System Failed: Forward to error page 
            *******************************************************/
 
            if (log.isInfoEnabled()) {

                log.info("Exception caught in ArInvoiceEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
            }
         
            e.printStackTrace();
            return(mapping.findForward("cmnErrorPage"));
         
        }
        
     
    
    }

}
