
package com.struts.ar.standardmemolineclass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class ArStandardMemoLineClassForm extends ActionForm implements Serializable{
    
    private String customerClassName = null;
    private ArrayList customerClassList = new ArrayList();
    
    private ArrayList standardMemoLineList = new ArrayList();
    
    private ArrayList branchList = new ArrayList();
    
    private ArrayList customerList = new ArrayList();
    
    private ArrayList arSmcList = new ArrayList();
    private int rowSelected = 0;
    
    
    private String isCustomerClassEntered = null;

    
    private String userPermission = new String();
    private String txnStatus = new String();
    
    
    private boolean showAddLines = false;
    private boolean showSaveButton = false;
    
   
    
    
    public String getCustomerClassName(){
        return customerClassName;
    }
    
    public void setCustomerClassName(String customerClassName){
        this.customerClassName = customerClassName;
    } 
    
    public ArrayList getCustomerClassList() {
		
        return customerClassList;
		
    }

    public void setCustomerClassList(String customerClassName) {

        customerClassList.add(customerClassName);

    }

    public void clearCustomerClassList() {   	 

        customerClassList.clear();
        customerClassList.add(Constants.GLOBAL_BLANK);

    } 
    
    public ArrayList getStandardMemoLineList() {
		
        return standardMemoLineList;
		
    }

    public void setStandardMemoLineList(String standardMemoLine) {

        standardMemoLineList.add(standardMemoLine);

    }
    
    public void clearStandardMemoLineList(){
        standardMemoLineList.clear();
        standardMemoLineList.add(Constants.GLOBAL_BLANK);
    }
    
    
    public ArrayList getBranchList() {
		
        return branchList;
		
    }

    public void setBranchList(String branch) {

        branchList.add(branch);

    }
    
    public void clearBranchList(){
        branchList.clear();
        branchList.add(Constants.GLOBAL_BLANK);
    }
    
    public ArrayList getCustomerList() {
		
        return customerList;
		
    }

    public void setCustomerList(String customer) {

        customerList.add(customer);

    }
    
    public void clearCustomerList(){
        customerList.clear();
        customerList.add(Constants.GLOBAL_BLANK);
    }

    public int getRowSelected(){
        return rowSelected;
    }

   
    
    public String getIsCustomerClassEntered(){
        return isCustomerClassEntered;
    }
    
    public void setIsCustomerClassEntered(String isCustomerClassEntered){
        this.isCustomerClassEntered = isCustomerClassEntered;
    }
    
    public ArStandardMemoLineClassList getArSmcByIndex(int index){
        return((ArStandardMemoLineClassList)arSmcList.get(index));
    }

    public Object[] getArSmcList(){
            return(arSmcList.toArray());
    }

    public int getArSmcListSize(){
            return(arSmcList.size());
    }

    public void saveArSmcList(Object newArSmcList){
            arSmcList.add(newArSmcList);
    }

    public void clearArSmcList(){
            arSmcList.clear();
    }

    public void setRowSelected(Object selectedArSmcList, boolean isEdit){
            this.rowSelected = arSmcList.indexOf(selectedArSmcList);
    }

    public void updateArSmcRow(int rowSelected, Object newArSmcList){
            arSmcList.set(rowSelected, newArSmcList);
    }

    public void deleteArSmcList(int rowSelected){
            arSmcList.remove(rowSelected);
    }
    
    public String getUserPermission() {		
        return userPermission;		
    }
	
    public void setUserPermission(String userPermission) {
        this.userPermission = userPermission;
    }
    
    public String getTxnStatus(){
        String passTxnStatus = txnStatus;
        txnStatus = Constants.GLOBAL_BLANK;
        return(passTxnStatus);
    }

    public void setTxnStatus(String txnStatus){
        this.txnStatus = txnStatus;
    }
    
    
    public boolean getShowAddLines() {		
        return showAddLines;		
    }
	
    public void setShowAddLines(boolean showAddLines) {
        this.showAddLines = showAddLines;
    }
    
    public boolean getShowSaveButton() {		
        return showSaveButton;		
    }
	
    public void setShowSaveButton(boolean showSaveButton) {
        this.showSaveButton = showSaveButton;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        
        isCustomerClassEntered = null;
        
        
    }
    
   
    
    public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
        ActionErrors errors = new ActionErrors();
        
        
        if(request.getParameter("saveButton") != null ){
            if(Common.validateRequired(customerClassName)){
                errors.add("customerClassName",new ActionMessage("standardMemoLineClass.error.customerClassIsRequired"));
            }
            
     
            Iterator i = arSmcList.iterator();      	 

            while (i.hasNext()) {

                ArStandardMemoLineClassList smcList = (ArStandardMemoLineClassList)i.next();

                
                if(smcList.getMemoLine() == null){
                    continue;
                }
                
              
                
                if(smcList.getMemoLine().equals("") || smcList.getDescription().equals(Constants.GLOBAL_BLANK) || smcList.getDescription().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
                    continue;
                }

                if(Common.validateRequired(smcList.getDescription()) || smcList.getDescription().equals(Constants.GLOBAL_BLANK)
                                || smcList.getDescription().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
                        errors.add("decription",
                                        new ActionMessage("standardMemoLineClass.error.memoLineDescriptionRequired", smcList.getLineNumber()));
                }
                
                if(Common.validateRequired(smcList.getUnitPrice())){
                        errors.add("unitPrice",
                                        new ActionMessage("standardMemoLineClass.error.memoLineUnitPriceRequired", smcList.getLineNumber()));
                }
                
                if(!Common.validateMoneyFormat(smcList.getUnitPrice())){
                    System.out.println(smcList.getUnitPrice() + " unit price" );
                        errors.add("unitPrice",
                                        new ActionMessage("standardMemoLineClass.error.memoLineUnitPriceInvalidFormat", smcList.getLineNumber()));
                }
                
                if(Common.validateRequired(smcList.getBranchName()) || smcList.getBranchName().equals(Constants.GLOBAL_BLANK)
                                || smcList.getBranchName().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
                        errors.add("branchName",
                                        new ActionMessage("standardMemoLineClass.error.memoLineBranchNameRequired", smcList.getLineNumber()));
                }

            }
    
            
        }
        
        
        return(errors);
    }
	
        
}
