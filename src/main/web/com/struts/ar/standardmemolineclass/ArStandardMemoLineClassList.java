
package com.struts.ar.standardmemolineclass;

import java.io.Serializable;
import java.util.ArrayList;

public class ArStandardMemoLineClassList {
    
    private String lineNumber = null;
    private String smcCode = null;
    private String memoLine = null;
    private ArrayList memoLineList = new ArrayList();
    private String description = null;
    private String unitPrice = null;
    private ArrayList branchList = new ArrayList();
    private String branchName = null;
    private ArrayList branchNameList = new ArrayList();
    private String customerCode = null;
    private ArrayList customerCodeList = new ArrayList();
    private String customerName = null;
    private String isMemoLineEntered = null;
    private String isCustomerEntered = null;
    
    
    private boolean isListed = false;

    private String deleteButton = null;
    
    private ArStandardMemoLineClassForm parentBean;
    
    
    public ArStandardMemoLineClassList(
        ArStandardMemoLineClassForm parentBean,
        String smcCode,
        String lineNumber,
        String memoLine,
        String description,
        String unitPrice,
        String branchName

        ){
        
        this.parentBean = parentBean;
        this.smcCode = smcCode;
        this.lineNumber = lineNumber;
        this.memoLine = memoLine;
        this.description = description;
        this.unitPrice = unitPrice;
        this.branchName = branchName;

        
     
    }
    
    
    public String getSmcCode(){
        return smcCode;
    }
    
    public void setSmcCode(String smcCode){
        this.smcCode = smcCode;
    }
    
    public String getLineNumber(){
        return lineNumber;
    }
    
    public void setLineNumber(String lineNumber){
        this.lineNumber = lineNumber;
    }
    
    public String getMemoLine(){
        return memoLine;
    }
    
    public void setMemoLine(String memoLine){
        this.memoLine = memoLine;
    }
    
    public ArrayList getMemoLineList() {
   	
        return memoLineList;
    
    }
   
    public void setMemoLineList(ArrayList memoLineList) {
   	
        this.memoLineList = memoLineList;
   	
    }
    
    public ArrayList getBranchList() {
   	
        return branchList;
    
    }
   
    public void setBranchList(ArrayList branchList) {
   	
        this.branchList = branchList;
   	
    }
   
    public String getDescription() {
   	
        return description;
   	
    }
   
    public void setDescription(String description) {
   	
        this.description = description;
   	
    }
    
    public String getUnitPrice() {
   	
        return unitPrice;
   	
    }
   
    public void setUnitPrice(String unitPrice) {
   	
        this.unitPrice = unitPrice;
   	
    }
    
    public String getBranchName() {
   	
        return branchName;
   	
    }
   
    public void setBranchName(String branchName) {
   	
        this.branchName = branchName;
   	
    }
    
    
    public ArrayList getBranchNameList() {
   	
        return branchNameList;
    
    }
   
    public void setBranchNameList(ArrayList branchNameList) {
   	
        this.branchNameList = branchNameList;
   	
    }
   
    
    
    public String getCustomerCode() {
   	
        return customerCode;
   	
    }
   
    public void setCustomerCode(String customerCode) {
   	
        this.customerCode = customerCode;
   	
    }
    
    
    public ArrayList getCustomerCodeList() {
   	
        return customerCodeList;
    
    }
   
    public void setCustomerCodeList(ArrayList customerCodeList) {
   	
        this.customerCodeList = customerCodeList;
   	
    }
    
    
    public String getCustomerName() {
   	
        return customerName;
   	
    }
   
    public void setCustomerName(String customerName) {
   	
        this.customerName = customerName;
   	
    }

  
    
    public String getIsMemoLineEntered() {
   	
   	   return isMemoLineEntered;
   	
    }
   
    public void setIsMemoLineEntered(String isMemoLineEntered) {  	   
        if (isMemoLineEntered != null && isMemoLineEntered.equals("true")) {

            parentBean.setRowSelected(this, false);

        }   	   	
    }
    
    
    
    public String getIsCustomerEntered() {
   	
   	   return isCustomerEntered;
   	
    }
    
    
    public void setIsCustomerEntered(String isCustomerEntered) {  	   
        if (isCustomerEntered != null && isCustomerEntered.equals("true")) {

            parentBean.setRowSelected(this, false);

        }   	   	
    }
    
    public boolean getIsListed(){
        return isListed;
    }
    
    public void setIsListed(boolean isListed){
        this.isListed = isListed;
    }
    
   
    public void setDeleteButton(String deleteButton) {
	
        parentBean.setRowSelected(this, true);
	
    }   
    
}
