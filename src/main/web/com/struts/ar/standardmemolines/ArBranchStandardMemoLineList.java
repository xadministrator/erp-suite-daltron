package com.struts.ar.standardmemolines;

import java.io.Serializable;


/**
 * 
 * @author Franco Antonio R. Roig
 * Created: 10/24/2005 5:11 PM
 * 
 */
public class ArBranchStandardMemoLineList implements Serializable {
	
	private String brName = null;
	private Integer brCode = null;
	private boolean branchCheckbox = false;
	private String branchAccountNumber= null;
	private String branchAccountNumberDescription = null;
	private String branchReceivableAccountNumber = null;
	private String branchReceivableAccountNumberDescription = null;
	
	private String branchRevenueAccountNumber = null;
	private String branchRevenueAccountNumberDescription = null;
	
	
	private ArStandardMemoLineForm parentBean;
	
	private boolean subjectToCommission = false;
	
	public ArBranchStandardMemoLineList(ArStandardMemoLineForm parentBean, 
			String brName, Integer brCode) {
		
		this.parentBean = parentBean;
		this.brName = brName;
		this.brCode = brCode;
		
	}
	
	public String getBrName() {
		
		return brName;
		
	}
	
	public void setBrName(String brName) {
		
		this.brName = brName;
		
	}
	
	public Integer getBrCode() {
		
		return brCode;
		
	}
	
	public void setBrCode(Integer brCode) {
		
		this.brCode = brCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
	public String getBranchAccountNumber() {
		return this.branchAccountNumber;
	}
	
	public void setBranchAccountNumber(String branchAccountNumber) {
		this.branchAccountNumber = branchAccountNumber;
	}
	
	public String getBranchAccountNumberDescription() {
		return this.branchAccountNumberDescription;
	}
	
	public void setBranchAccountNumberDescription(String branchAccountNumberDescription) {
		this.branchAccountNumberDescription = branchAccountNumberDescription;
	}
	
	
	
	
	public String getBranchReceivableAccountNumber() {
		
		return this.branchReceivableAccountNumber;
		
	}
	
	public void setBranchReceivableAccountNumber(String branchReceivableAccountNumber) {
		
		this.branchReceivableAccountNumber = branchReceivableAccountNumber;
		
	}
	
	public String getBranchReceivableAccountNumberDescription() {
		
		return this.branchReceivableAccountNumberDescription;
		
	}
	
	public void setBranchReceivableAccountNumberDescription(String branchReceivableAccountNumberDescription) {
		
		this.branchReceivableAccountNumberDescription = branchReceivableAccountNumberDescription;
		
	}
	
	public String getBranchRevenueAccountNumber() {
		
		return this.branchRevenueAccountNumber;
		
	}
	
	public void setBranchRevenueAccountNumber(String branchRevenueAccountNumber) {
		
		this.branchRevenueAccountNumber = branchRevenueAccountNumber;
		
	}
	
	public String getBranchRevenueAccountNumberDescription() {
		
		return this.branchRevenueAccountNumberDescription;
		
	}
	
	public void setBranchRevenueAccountNumberDescription(String branchRevenueAccountNumberDescription) {
		
		this.branchRevenueAccountNumberDescription = branchRevenueAccountNumberDescription;
		
	}
	
	public boolean getSubjectToCommission() {
		return this.subjectToCommission;
	}
	
	public void setSubjectToCommission(boolean subjectToCommission) {
		this.subjectToCommission = subjectToCommission;
	}
	
	


}