package com.struts.ar.standardmemolines;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ArRICustomerRequiredException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.AdDocumentSequenceAssignmentController;
import com.ejb.txn.AdDocumentSequenceAssignmentControllerHome;
import com.ejb.txn.ArInvoiceEntryController;
import com.ejb.txn.ArInvoiceEntryControllerHome;
import com.ejb.txn.ArStandardMemoLineController;
import com.ejb.txn.ArStandardMemoLineControllerHome;
import com.ejb.txn.GlYearEndClosingController;
import com.ejb.txn.GlYearEndClosingControllerHome;
import com.struts.util.Branch;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.sun.mail.handlers.message_rfc822;
import com.util.AdBranchDetails;
import com.util.AdDocumentSequenceAssignmentDetails;
import com.util.AdModBranchDocumentSequenceAssignmentDetails;
import com.util.AdModBranchStandardMemoLineDetails;
import com.util.AdModDocumentSequenceAssignmentDetails;
import com.util.AdResponsibilityDetails;
import com.util.ArInvoiceDetails;
import com.util.ArModCustomerDetails;
import com.util.ArModInvoiceDetails;
import com.util.ArModInvoiceLineDetails;
import com.util.ArModStandardMemoLineDetails;
import com.util.EJBCommon;
import com.util.GlModAccountingCalendarValueDetails;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
public final class ArStandardMemoLineAction extends Action {

	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		HttpSession session = request.getSession();

		try {

			/*******************************************************
			 Check if user has a session
			 *******************************************************/


			User user = (User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {

				if (log.isInfoEnabled()) {

					log.info("ArStandardMemoLineAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}

			} else {

				if (log.isInfoEnabled()) {

					log.info("User is not logged on in session" + session.getId());

				}

				return(mapping.findForward("adLogon"));

			}

			ArStandardMemoLineForm actionForm = (ArStandardMemoLineForm)form;

			String frParam = Common.getUserPermission(user, Constants.AR_STANDARD_MEMO_LINE_ID);

			if (frParam != null) {

				if (frParam.trim().equals(Constants.FULL_ACCESS)) {

					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

					if (!fieldErrors.isEmpty()) {

						saveErrors(request, new ActionMessages(fieldErrors));

						return mapping.findForward("arStandardMemoLines");
					}

				}

				actionForm.setUserPermission(frParam.trim());

			} else {

				actionForm.setUserPermission(Constants.NO_ACCESS);

			}

			/*******************************************************
			 Initialize ArStandardMemoLineController EJB
			 *******************************************************/

			ArStandardMemoLineControllerHome homeSML = null;
			ArStandardMemoLineController ejbSML = null;

			ArInvoiceEntryControllerHome homeINVC = null;
			ArInvoiceEntryController ejbINVC = null;

			AdDocumentSequenceAssignmentControllerHome homeDSA = null;
			AdDocumentSequenceAssignmentController ejbDSA = null;

                        GlYearEndClosingControllerHome homeYC = null;
                        GlYearEndClosingController ejbYC = null;

			try {

				homeSML = (ArStandardMemoLineControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArStandardMemoLineControllerEJB", ArStandardMemoLineControllerHome.class);

				homeINVC = (ArInvoiceEntryControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/ArInvoiceEntryControllerEJB", ArInvoiceEntryControllerHome.class);

				homeDSA = (AdDocumentSequenceAssignmentControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/AdDocumentSequenceAssignmentControllerEJB", AdDocumentSequenceAssignmentControllerHome.class);

                                homeYC = (GlYearEndClosingControllerHome)com.util.EJBHomeFactory.
                                lookUpHome("ejb/GlYearEndClosingControllerEJB", GlYearEndClosingControllerHome.class);


                        } catch (NamingException e) {

				if (log.isInfoEnabled()) {

					log.info("NamingException caught in ArStandardMemoLineAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}

				return mapping.findForward("cmnErrorPage");

			}

			try {

				ejbSML = homeSML.create();
				ejbINVC = homeINVC.create();
				ejbDSA  = homeDSA.create();
                                ejbYC = homeYC.create();

			} catch (CreateException e) {

				if (log.isInfoEnabled()) {

					log.info("CreateException caught in ArStandardMemoLineAction.execute(): " + e.getMessage() +
							" session: " + session.getId());

				}

				return mapping.findForward("cmnErrorPage");

			}



			ActionMessages messages = new ActionMessages();
			ActionErrors errors = new ActionErrors();
			ActionMessages warning = new ActionMessages();

			/*******************************************************
			 Call ArStandardMemoLineController EJB
			 getGlFcPrecisionUnit
			 *******************************************************/

			short precisionUnit = 0;
			boolean enableInvoiceBatch =false;
			boolean enablePaymentTerm = false;
			try {

				precisionUnit = ejbSML.getGlFcPrecisionUnit(user.getCmpCode());
				enableInvoiceBatch = Common.convertByteToBoolean(ejbINVC.getAdPrfEnableArInvoiceBatch(user.getCmpCode()));
	            System.out.println(" show bn :" + enableInvoiceBatch);
				actionForm.setShowBatchName(enableInvoiceBatch);


				enablePaymentTerm = Common.convertByteToBoolean(ejbINVC.getAdPrfArEnablePaymentTerm(user.getCmpCode()));
				actionForm.setShowPaymentTerm(enablePaymentTerm);
				actionForm.setEnablePaymentTerm(enablePaymentTerm);
			} catch(EJBException ex) {

				if (log.isInfoEnabled()) {

					log.info("EJBException caught in ArStandardMemoLineAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}

				return(mapping.findForward("cmnErrorPage"));

			}


			/*******************************************************
			 -- Ar SML Show Details Action --
			 *******************************************************/

			if (request.getParameter("showDetailsButton") != null) {

				actionForm.setTableType(Constants.GLOBAL_DETAILED);

				return(mapping.findForward("arStandardMemoLines"));

				/*******************************************************
				 -- Ar SML Hide Details Action --
				 *******************************************************/

			} else if (request.getParameter("hideDetailsButton") != null) {

				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

				return(mapping.findForward("arStandardMemoLines"));


/*******************************************************
 -- Ar SML Generate Investor Interest --
 *******************************************************/

		} else if (request.getParameter("generateArInvestorBonusAndInterest") != null &&
				actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			try{

				String generateArInvestorInterest = ejbSML.generateArInvestorBonusAndInterest(actionForm.getSupplier(), actionForm.getIsRecalculate(), actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')),
				Common.convertStringToInt(actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)), user.getUserName(), user.getCurrentBranch().getBrCode(), user.getCmpCode());

				System.out.println("generateArInvestorInterest="+generateArInvestorInterest);

				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.messages", generateArInvestorInterest ));


				saveMessages(request, messages);
			}catch(GlJREffectiveDatePeriodClosedException gx){

				errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("standardMemoLine.error.datePeriodNotOpen"));

			}catch (Exception ex) {
				messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.errorInfo", "Error generating penalty"));

			}



/*******************************************************
 -- Ar SML Generate Penalty --
 *******************************************************/

			} else if (request.getParameter("generateOverDue") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				try{

					int generateOverDue = ejbSML.generateArOverDueInvoices(user.getUserName(), user.getCurrentBranch().getBrCode(), user.getCmpCode());



					System.out.println("generateOverDue="+generateOverDue);

					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.messages", "Successfully generated " + generateOverDue + " invoices"));


					saveMessages(request, messages);
				}catch(GlJREffectiveDatePeriodClosedException gx){

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("standardMemoLine.error.datePeriodNotOpen"));


				}catch (Exception ex) {
					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.errorInfo", "Error generating penalty"));

				}




/*******************************************************
 -- Ar SML Generate Accrued Interest IS
 *******************************************************/

        } else if (request.getParameter("generateAccruedInterestIS") != null &&
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            try{

                   int generateArAccruedInterestIS  = ejbSML.generateArAccruedInterestIS(user.getUserName(), user.getCurrentBranch().getBrCode(), user.getCmpCode());





                    messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.messages", "Successfully generated " + "d"  + " invoices"));


                    saveMessages(request, messages);
            }catch(GlJREffectiveDatePeriodClosedException gx){

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                                    new ActionMessage("standardMemoLine.error.datePeriodNotOpen"));


            }catch (Exception ex) {
                    messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.errorInfo", "Error generating accrued Interest IS"));

            }




/*******************************************************
 -- Ar SML Generate Accrued Interest TB
 *******************************************************/

	} else if (request.getParameter("generateAccruedInterestTB") != null &&
			actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

		try{

			int generateArAccruedInterestTB = ejbSML.generateArAccruedInterestTB(user.getUserName(), user.getCurrentBranch().getBrCode(), user.getCmpCode());

			System.out.println("generateArAccruedInterest="+generateArAccruedInterestTB);

			messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.messages", "Successfully generated " + generateArAccruedInterestTB + " invoices"));


			saveMessages(request, messages);
		}catch(GlJREffectiveDatePeriodClosedException gx){

			errors.add(ActionMessages.GLOBAL_MESSAGE,
					new ActionMessage("standardMemoLine.error.datePeriodNotOpen"));


		}catch (Exception ex) {
			messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.errorInfo", "Error generating accrued Interest TB"));

		}
/*******************************************************
 -- Ar SML Generate Penalty --
 *******************************************************/

			} else if (request.getParameter("generatePenalty") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				try{

					int generatedArPenalty = ejbSML.generateArPastDueInvoices(user.getUserName(), user.getCurrentBranch().getBrCode(), user.getCmpCode());

					System.out.println("generatedArPenalty="+generatedArPenalty);

					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.messages", "Successfully generated " + generatedArPenalty + " penalties"));


					saveMessages(request, messages);


				}catch (Exception ex) {
					messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.errorInfo", "Error generating penalty"));

				}

/*******************************************************
 -- Ar SML Generate Invoice --
 *******************************************************/

			} else if (request.getParameter("generateButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArrayList cstList = new ArrayList();
				String customerCode = actionForm.getCustomer();


				ArrayList customerBatchSelectedList = new ArrayList();

	       		for (int i=0; i < actionForm.getCustomerBatchSelectedList().length; i++) {

	       			customerBatchSelectedList.add(actionForm.getCustomerBatchSelectedList()[i]);

	       		}




				boolean failCst = false;
				int NoOfErr = 0;
				int NoOfSucc = 0;
				int NoOfWarn = 0;

				System.out.println("pasok 1");

					try{


						ArInvoiceDetails details = new ArInvoiceDetails();
						Date d = new Date();

						details.setInvCode(null);
						details.setInvType("MEMO LINES");
			             details.setInvDate(Common.convertStringToSQLDate(actionForm.getInvcEffDate()));
			             details.setInvNumber(null);
			             details.setInvReferenceNumber(null);
			             details.setInvVoid(Common.convertBooleanToByte(false));
			             details.setInvDescription(actionForm.getGenInvoiceDesc());
			             details.setInvBillToAddress("");
			             details.setInvBillToContact("");
			             details.setInvBillToAltContact("");
			             details.setInvBillToPhone("");
			             details.setInvBillingHeader("");
			             details.setInvBillingFooter("");
			             details.setInvBillingHeader2("");
			             details.setInvBillingFooter2("");
			             details.setInvBillingHeader3(null);
			             details.setInvBillingFooter3(null);
			             details.setInvBillingSignatory("");
			             details.setInvSignatoryTitle("");
			             details.setInvShipToAddress("");
			             details.setInvShipToContact("");
			             details.setInvShipToAltContact("");
			             details.setInvShipToPhone("");
			             details.setInvLvFreight("");
			             details.setInvShipDate(Common.convertStringToSQLDate(null));
			             details.setInvConversionDate(Common.convertStringToSQLDate(null));
			             details.setInvConversionRate(Common.convertStringMoneyToDouble("1.00000", Constants.MONEY_RATE_PRECISION));
			             details.setInvDebitMemo(Common.convertBooleanToByte(false));
			             details.setInvSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));
			             details.setInvClientPO("");
			             details.setInvEffectivityDate(Common.convertStringToSQLDate(actionForm.getInvcEffDate()));
			             details.setInvRecieveDate(Common.convertStringToSQLDate(actionForm.getInvcRcvDate()));

			             details.setInvCreatedBy(user.getUserName());
			             details.setInvDateCreated(new java.util.Date());

			             details.setInvLastModifiedBy(user.getUserName());
			             details.setInvDateLastModified(new java.util.Date());
			             String cstCode	= "";
			        double numbersParking = 0;
			        double squareMeter = 0;
			        double rptRate = 0;
			        double asdRate =0;
			        String taxCode= "NONE";
			        if(customerCode.equals("") && customerBatchSelectedList.size() <=0){
						System.out.println("1-------------------");
						cstList = ejbINVC.getArCstAllForGeneration(user.getCurrentBranch().getBrCode(), user.getCmpCode());
			        } else if(customerBatchSelectedList.size() > 0 && customerCode.equals("") ){
			        	System.out.println("2-------------------");


			        	System.out.println("batch list no:" + customerBatchSelectedList.size());

						cstList = ejbINVC.getArCstAllByCustomerBatch(customerBatchSelectedList, user.getCurrentBranch().getBrCode(), user.getCmpCode());


					}else{
						System.out.println("3-------------------");
						ArModCustomerDetails cstDetails = new ArModCustomerDetails();

						cstDetails = ejbINVC.getArCstByCstCustomerCode(customerCode,user.getCmpCode());
						System.out.println("YAHOOOOOO");

						System.out.println(customerCode + "#" + cstDetails.getCstNumbersParking() + "#" + cstDetails.getCstSquareMeter() + "#" + cstDetails.getCstCcTcName());



						cstList.add(customerCode + "#" +
								cstDetails.getCstNumbersParking() + "#" +
								cstDetails.getCstSquareMeter() + "#" +
								cstDetails.getCstRealPropertyTaxRate() + "#" +
								cstDetails.getCstCcTcName() + "#" +
								cstDetails.getCstAssociationDuesRate());
					}


						System.out.println("cstList.size()="+cstList.size());

						if (cstList == null || cstList.size() == 0) {


							throw new ArRICustomerRequiredException();

		                } else {
				             //looping customer

		                	Iterator i = cstList.iterator();
		                    int cnt = 0;
		                    while (i.hasNext()) {
		                    	ArrayList ilList = new ArrayList();

		                    	String cst = (String)i.next();
		                    	String [] data = cst.split("#");
		                    	 cstCode = data[0];
		                    	 numbersParking = Common.convertStringToDouble(data[1]);
		                    	 squareMeter = Common.convertStringToDouble(data[2]);
		                    	 rptRate = Common.convertStringToDouble(data[3]);
		                    	 taxCode = data[4];
		                    	 asdRate = Common.convertStringToDouble(data[5]);

		                    	System.out.println("cstCode="+cstCode+" numbersParking="+numbersParking+" squareMeter="+squareMeter + " rptRate=" + rptRate);
		                    	boolean IsMMLSelected = false;
		                    	boolean IsWarning = false;
		                    	System.out.println("actionForm.getArSMLListSize()="+actionForm.getArSMLListSize());

		                    	for (int x = 0; x<actionForm.getArSMLListSize(); x++) {

					            	 ArStandardMemoLineList arSmlList = actionForm.getArSMLByIndex(x);

					            	 System.out.println("arSmlList.getGenInvoiceEnable()="+arSmlList.getGenInvoiceEnable());
					            	 if(arSmlList.getGenInvoiceEnable()){


						                 System.out.println("arSmlList.getName()="+arSmlList.getName());

					            		 if(arSmlList.getName().equals("AR - Parking Dues") ){

					            			 System.out.println("AR - Parking Dues");


					            			 if(numbersParking <= 0){
					            				 IsMMLSelected = false;
					            				 IsWarning = true;

					            			 }else{
					            				 IsMMLSelected = true;
						            			 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

						            			 mdetails.setIlSmlName(arSmlList.getName());
								                 mdetails.setIlDescription(arSmlList.getDescription());
						            			 mdetails.setIlQuantity(numbersParking);
						            			 mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arSmlList.getUnitPrice(), precisionUnit));
						            			 mdetails.setIlAmount(Common.convertStringMoneyToDouble(arSmlList.getUnitPrice() , precisionUnit) * numbersParking);
						            			 mdetails.setIlTax(Common.convertBooleanToByte(arSmlList.getTaxable()));
						            			 ilList.add(mdetails);
					            			 }




					            		 } else if(arSmlList.getName().contains("AR - Association Dues")  ){

					            			 System.out.println("AR - Association Dues");
					            			 if(squareMeter <= 0 || asdRate <=0){
					            				 IsMMLSelected = false;
					            				 IsWarning = true;

					            			 }else{
					            				 IsMMLSelected = true;
						            			 System.out.println(" sqr mtrs " + squareMeter);

						            			 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

						            			 mdetails.setIlSmlName(arSmlList.getName());
								                 mdetails.setIlDescription(arSmlList.getDescription());
								                 mdetails.setIlQuantity(squareMeter);
								                 mdetails.setIlUnitPrice(asdRate );
						            			 mdetails.setIlAmount(asdRate * squareMeter);
						            			 mdetails.setIlTax(Common.convertBooleanToByte(arSmlList.getTaxable()));
						            			 ilList.add(mdetails);
					            			 }


					            		 }else if(arSmlList.getName().contains("AR - RPT")  ){

					            			 System.out.println("AR - RPT");
					            			 if(rptRate <= 0){
					            				 IsMMLSelected = false;
					            				 IsWarning = true;

					            			 }else{
					            				 IsMMLSelected = true;
						            			 System.out.println(" sqr mtrs " + squareMeter);

						            			 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

						            			 mdetails.setIlSmlName(arSmlList.getName());
								                 mdetails.setIlDescription(arSmlList.getDescription());
								                 mdetails.setIlQuantity(1);
								                 mdetails.setIlUnitPrice(rptRate );
						            			 mdetails.setIlAmount(rptRate);
						            			 mdetails.setIlTax(Common.convertBooleanToByte(arSmlList.getTaxable()));
						            			 ilList.add(mdetails);

					            			 }


					            		 }else if(arSmlList.getName().contains("AR - Rental")  ){

					            			 System.out.println("AR - Rental");
					            			 if(asdRate <= 0){
					            				 IsMMLSelected = false;
					            				 IsWarning = true;

					            			 }else{
					            				 IsMMLSelected = true;
						            			 System.out.println(" asdRate " + asdRate);

						            			 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

						            			 mdetails.setIlSmlName(arSmlList.getName());
								                 mdetails.setIlDescription(arSmlList.getDescription());
								                 mdetails.setIlQuantity(1);
								                 mdetails.setIlUnitPrice(asdRate );
						            			 mdetails.setIlAmount(asdRate);
						            			 mdetails.setIlTax(Common.convertBooleanToByte(arSmlList.getTaxable()));
						            			 ilList.add(mdetails);

					            			 }

					            		 }else{
					            			 IsMMLSelected = true;
					            			 ArModInvoiceLineDetails mdetails = new ArModInvoiceLineDetails();

					            			 mdetails.setIlSmlName(arSmlList.getName());
							                 mdetails.setIlDescription(arSmlList.getDescription());
							                 mdetails.setIlQuantity(Common.convertStringMoneyToDouble("1", precisionUnit));
							                 mdetails.setIlUnitPrice(Common.convertStringMoneyToDouble(arSmlList.getUnitPrice(), precisionUnit) );
					            			 mdetails.setIlAmount(Common.convertStringMoneyToDouble(arSmlList.getUnitPrice() , precisionUnit));
					            			 mdetails.setIlTax(Common.convertBooleanToByte(arSmlList.getTaxable()));
					            			 ilList.add(mdetails);
					            		 }


					            	 }

					             }

		                    	if(ilList.size()<=0){
		                    		failCst =true;
		                    		NoOfErr++;
		                    		if(IsWarning){/*


			                    		String errMessage = "<br><font color='red'>Error: Zero Parking or Sqr m in Cst Code: " + cstCode + " Transaction failed due to no memo line valid</font>";
			                    		errors.add(ActionMessages.GLOBAL_MESSAGE,
												new ActionMessage("app.messages",errMessage));*/

			                    	}else{
			                    		/*String errMessage = "<br><font color='red'>No memoline exist in Cst Code: "+  cstCode + "</font><br>";

						            	 errors.add(ActionMessages.GLOBAL_MESSAGE,
													new ActionMessage("app.messages",errMessage));*/
			                    	}


		                    	}else{
		                    		 if(IsWarning){
		                    			 /*failCst =true;
		                    			 NoOfWarn++;
		                    			 String warnMessage = "Warning: No Parking or Sqr m in Cst Code: " + cstCode + " Memo Line involve not added";
		                    			 messages.add(ActionMessages.GLOBAL_MESSAGE,
													new ActionMessage("app.messages",warnMessage));
		 */
		                    		 }else{
		                    			 NoOfSucc ++;
		                    		 }
		                    		 System.out.println("saveArInvEntry----->");

		                    		 // multiple lines , one invoice
		                    		 /*
		                    		 	Integer invoiceCode = ejbINVC.saveArInvEntry(details, actionForm.getPaymentTerm(), taxCode, "NONE", "PHP", cstCode, actionForm.getBatchName(), ilList, true, null, user.getCurrentBranch().getBrCode(), user.getCmpCode());
				                     	details.setInvNumber(null);
		                    		  *
		                    		  */

		                    		 //one line, one invoice

		                    		 for(int ctr = 0; ctr<ilList.size();ctr++){
		                    			 ArrayList dataList = new ArrayList();
		                    			 dataList.add(ilList.get(ctr));
		                    			 Integer invoiceCode = ejbINVC.saveArInvEntry(details, actionForm.getPaymentTerm(), taxCode, "NONE", "PHP", cstCode, actionForm.getBatchName(), dataList, true, null,
		                    					 null, null, null, null,
		                    					 user.getCurrentBranch().getBrCode(), user.getCmpCode());
					                     details.setInvNumber(null);
		                    		 }


		                    	}

			                    /*cnt++;

			                     if(cnt==3)break;*/

		                    }


		                }

					if(failCst){

						messages.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("app.messages", "Success: " + NoOfSucc + "      Warning: " + NoOfWarn + "     Fail:" + NoOfErr ));


					}

					System.out.println("fail msg");

					saveMessages(request, messages); // storing messages as request attributes


					System.out.println("fail msg 2");

				}catch(GlobalNoRecordFoundException ex){
					errors.add(ActionMessages.GLOBAL_MESSAGE,
					new ActionMessage("standardMemoLine.error.noMemoLinesSelected"));


				}catch(ArRICustomerRequiredException ex){

					errors.add(ActionMessages.GLOBAL_MESSAGE,
					new ActionMessage("standardMemoLine.error.noCustomerListFound"));

				}catch(Exception ex){
					if (log.isInfoEnabled()) {
						System.out.println(ex.toString());
						log.info("EJBException caught in ArStandardMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}
				}







				/*******************************************************
				 -- Ar SML Save Action --
				 *******************************************************/

			} else if (request.getParameter("saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArModStandardMemoLineDetails mdetails = new ArModStandardMemoLineDetails();
				mdetails.setSmlType(actionForm.getType());
				mdetails.setSmlName(actionForm.getName());
				mdetails.setSmlDescription(actionForm.getDescription());
				mdetails.setSmlWordPressProductID(actionForm.getWpProductID());
				mdetails.setSmlUnitPrice(Common.convertStringMoneyToDouble(actionForm.getUnitPrice(), precisionUnit));
				mdetails.setSmlTax(Common.convertBooleanToByte(actionForm.getTaxable()));
				mdetails.setSmlEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				mdetails.setSmlUnitOfMeasure(actionForm.getUnitOfMeasure());
				mdetails.setSmlSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));

				// For Branch Setting
	            ArrayList branchList = new ArrayList();

	            for(int i=0; i<actionForm.getArBSMLListSize(); i++) {
		        	ArBranchStandardMemoLineList bsmlList = actionForm.getArBSMLByIndex(i);

		        	if(bsmlList.getBranchCheckbox() == true) {

		        		//AdBranchDetails brDetails = new AdBranchDetails();
		        		AdModBranchStandardMemoLineDetails adBrSmlDetails = new AdModBranchStandardMemoLineDetails();
		        		adBrSmlDetails.setBsmlBranchCode(bsmlList.getBrCode());
			        	if (bsmlList.getBranchAccountNumber() == null || bsmlList.getBranchAccountNumber().length() < 1)
			        		bsmlList.setBranchAccountNumber(actionForm.getAccountNumber());

			        	adBrSmlDetails.setBsmlAccountNumber(bsmlList.getBranchAccountNumber());

			        	if (bsmlList.getBranchReceivableAccountNumber() == null || bsmlList.getBranchAccountNumber().length() < 1)
			        		bsmlList.setBranchReceivableAccountNumber(actionForm.getReceivableAccountNumber());

			        	adBrSmlDetails.setBsmlReceivableAccountNumber(bsmlList.getBranchReceivableAccountNumber());

			        	if (bsmlList.getBranchRevenueAccountNumber() == null || bsmlList.getBranchAccountNumber().length() < 1)
			        		bsmlList.setBranchRevenueAccountNumber(actionForm.getRevenueAccountNumber());

			        	adBrSmlDetails.setBsmlRevenueAccountNumber(bsmlList.getBranchRevenueAccountNumber());


			        	adBrSmlDetails.setBsmlSubjectToCommission(Common.convertBooleanToByte(bsmlList.getSubjectToCommission()));

			        	branchList.add(adBrSmlDetails);
		        	}
	            }

				try {

					ejbSML.addArSmlEntry(mdetails, actionForm.getAccountNumber(), actionForm.getReceivableAccountNumber(), actionForm.getRevenueAccountNumber(), actionForm.getInterimAccount(), branchList, user.getCmpCode());

				} catch (GlobalRecordAlreadyExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("standardMemoLine.error.recordAlreadyExist"));

				} catch (GlobalAccountNumberInvalidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("standardMemoLine.error.accountNumberNotFound"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArStandardMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}

				/*******************************************************
				 -- Ar SML Close Action --
				 *******************************************************/

			} else if (request.getParameter("closeButton") != null) {

				return(mapping.findForward("cmnMain"));

				/*******************************************************
				 -- Ar SML Update Action --
				 *******************************************************/

			} else if (request.getParameter("updateButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArStandardMemoLineList arSMLList =
					actionForm.getArSMLByIndex(actionForm.getRowSelected());

				System.out.println("-----" + actionForm.getWpProductID());

				ArModStandardMemoLineDetails mdetails = new ArModStandardMemoLineDetails();
				mdetails.setSmlCode(arSMLList.getStandardMemoLineCode());
				mdetails.setSmlType(actionForm.getType());
				mdetails.setSmlName(actionForm.getName());
				mdetails.setSmlDescription(actionForm.getDescription());
				mdetails.setSmlWordPressProductID(actionForm.getWpProductID());
				mdetails.setSmlUnitPrice(Common.convertStringMoneyToDouble(actionForm.getUnitPrice(), precisionUnit));
				mdetails.setSmlTax(Common.convertBooleanToByte(actionForm.getTaxable()));
				mdetails.setSmlEnable(Common.convertBooleanToByte(actionForm.getEnable()));
				mdetails.setSmlUnitOfMeasure(actionForm.getUnitOfMeasure());
				mdetails.setSmlSubjectToCommission(Common.convertBooleanToByte(actionForm.getSubjectToCommission()));

				// For Branch Map Setting
	            ArrayList branchList = new ArrayList();
	            ArrayList glCoaAccntList = new ArrayList();

	            for(int i=0; i<actionForm.getArBSMLListSize(); i++) {
		        	ArBranchStandardMemoLineList bsmlList = actionForm.getArBSMLByIndex(i);

		        	if(bsmlList.getBranchCheckbox() == true) {
		        		System.out.println("subject to commission: " + bsmlList.getSubjectToCommission());
		        		AdModBranchStandardMemoLineDetails adBrSmlDetails = new AdModBranchStandardMemoLineDetails();
			        	adBrSmlDetails.setBsmlBranchCode(bsmlList.getBrCode());

			        	if (bsmlList.getBranchAccountNumber() == null || bsmlList.getBranchAccountNumber().length() < 1)
			        		bsmlList.setBranchAccountNumber(actionForm.getAccountNumber());

			        	adBrSmlDetails.setBsmlAccountNumber(bsmlList.getBranchAccountNumber());


			        	if (bsmlList.getBranchReceivableAccountNumber() == null || bsmlList.getBranchReceivableAccountNumber().length() < 1)
			        		bsmlList.setBranchReceivableAccountNumber(actionForm.getReceivableAccountNumber());

			        	adBrSmlDetails.setBsmlReceivableAccountNumber(bsmlList.getBranchReceivableAccountNumber());

			        	if (bsmlList.getBranchRevenueAccountNumber() == null || bsmlList.getBranchRevenueAccountNumber().length() < 1)
			        		bsmlList.setBranchRevenueAccountNumber(actionForm.getRevenueAccountNumber());

			        	adBrSmlDetails.setBsmlRevenueAccountNumber(bsmlList.getBranchRevenueAccountNumber());



			        	adBrSmlDetails.setBsmlSubjectToCommission((Common.convertBooleanToByte(bsmlList.getSubjectToCommission())));

			        	branchList.add(adBrSmlDetails);
		        	}
	            }

	            // get the selected responsibility name
	            AdResponsibilityDetails details = ejbSML.getAdRsByRsCode(new Integer(user.getCurrentResCode()));

				try {

					ejbSML.updateArSmlEntry(mdetails, actionForm.getAccountNumber(), actionForm.getReceivableAccountNumber(), actionForm.getRevenueAccountNumber(), actionForm.getInterimAccount(), details.getRsName(), branchList, user.getCmpCode());

				} catch (GlobalRecordAlreadyExistException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("standardMemoLine.error.recordAlreadyExist"));

				} catch (GlobalAccountNumberInvalidException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("standardMemoLine.error.accountNumberNotFound"));

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArStandardMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}

				/*******************************************************
				 -- Ar SML Cancel Action --
				 *******************************************************/

			} else if (request.getParameter("cancelButton") != null) {

				/*******************************************************
				 -- Ar SML Edit Action --
				 *******************************************************/

			} else if (request.getParameter("arSMLList[" +
					actionForm.getRowSelected() + "].editButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				actionForm.reset(mapping, request);
				Object[] arSMLList = actionForm.getArSMLList();

				// get list of branches
	        	ArrayList branchList = new ArrayList();

	        	AdResponsibilityDetails details = ejbSML.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
	        	System.out.println("the num: "+((ArStandardMemoLineList)arSMLList[actionForm.getRowSelected()]).getStandardMemoLineCode().toString());
	         	try {
	         		branchList = ejbSML.getAdBrSMLAll(((ArStandardMemoLineList)arSMLList[actionForm.getRowSelected()]).getStandardMemoLineCode(), details.getRsName(), user.getCmpCode());
	         	} catch(GlobalNoRecordFoundException ex) {

	         	}

	         	actionForm.showArSMLRow(branchList, actionForm.getRowSelected());

				return mapping.findForward("arStandardMemoLines");

				/*******************************************************
				 -- Ar SML Delete Action --
				 *******************************************************/

			} else if (request.getParameter("arSMLList[" +
					actionForm.getRowSelected() + "].deleteButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

				ArStandardMemoLineList arSMLList =
					actionForm.getArSMLByIndex(actionForm.getRowSelected());

				try {

					ejbSML.deleteArSmlEntry(arSMLList.getStandardMemoLineCode(), user.getCmpCode());

				} catch (GlobalRecordAlreadyAssignedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("standardMemoLine.error.deleteStandardMemoLineAlreadyAssigned"));

				} catch (GlobalRecordAlreadyDeletedException ex) {

					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("standardMemoLine.error.standardMemoLineAlreadyDeleted"));

				} catch(EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArStandardMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
					}

					return(mapping.findForward("cmnErrorPage"));

				}

/*******************************************************
 -- Ar SML Load Action --
*******************************************************/

			}

			if (frParam != null) {

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("arStandardMemoLines");

				}

				ArrayList list = null;
				Iterator i = null;

                                try{
                                    actionForm.clearPeriodList();

                                    list = ejbYC.getGlReportableAcvAll(user.getCmpCode());

                                    if (list == null || list.size() == 0) {

                                            actionForm.setPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);

                                    } else {

                                            i = list.iterator();

                                            while (i.hasNext()) {

                                                GlModAccountingCalendarValueDetails mdetails =
                                                    (GlModAccountingCalendarValueDetails)i.next();

                                                actionForm.setPeriodList(mdetails.getAcvPeriodPrefix() + "-" +
                                                    mdetails.getAcvYear());

                                                if (mdetails.getAcvCurrent()) {

                                                    actionForm.setPeriod(mdetails.getAcvPeriodPrefix() + "-" +
                                                        mdetails.getAcvYear());

                                                }

                                            }

                                    }
                                }catch(Exception ex){

                                }


				try {

					actionForm.setEnableAccountNumber(ejbSML.getArSmlGlCoaAccountNumberEnable(user.getCmpCode()));

					actionForm.clearArSMLList();

					list = ejbSML.getArSmlAll(user.getCmpCode());

					i = list.iterator();

					while(i.hasNext()) {

						ArModStandardMemoLineDetails mdetails = (ArModStandardMemoLineDetails)i.next();

						ArStandardMemoLineList arSMLList = new ArStandardMemoLineList(actionForm,
								mdetails.getSmlCode(),
								mdetails.getSmlType(),
								mdetails.getSmlName(),
								mdetails.getSmlDescription(),
								mdetails.getSmlWordPressProductID(),
								Common.convertDoubleToStringMoney(mdetails.getSmlUnitPrice(), precisionUnit),
								Common.convertByteToBoolean(mdetails.getSmlTax()),
								mdetails.getSmlGlCoaAccountNumber(),
								mdetails.getSmlGlCoaAccountDescription(),

								mdetails.getSmlGlCoaReceivableAccountNumber(),
								mdetails.getSmlGlCoaReceivableAccountDescription(),
								mdetails.getSmlGlCoaRevenueAccountNumber(),
								mdetails.getSmlGlCoaRevenueAccountDescription(),

								mdetails.getSmlInterimAccountNumber(),
								mdetails.getSmlInterimAccountDescription(),
								Common.convertByteToBoolean(mdetails.getSmlEnable()),
								mdetails.getSmlUnitOfMeasure(),
								Common.convertByteToBoolean(mdetails.getSmlSubjectToCommission()),false);

						actionForm.saveArSMLList(arSMLList);

					}

				} catch (GlobalNoRecordFoundException ex) {

				} catch (EJBException ex) {

					if (log.isInfoEnabled()) {

						log.info("EJBException caught in ArStandardMemoLineAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage");

					}

				}


				 actionForm.clearBatchNameList();
				 actionForm.clearPenaltyBatchNameList();

	             list = ejbINVC.getArOpenIbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	             if (list == null || list.size() == 0) {

	                actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);

	             } else {

	                i = list.iterator();

	                while (i.hasNext()) {

	                     actionForm.setBatchNameList((String)i.next());


	                }

	             }


	             if (list == null || list.size() == 0) {


		                actionForm.setPenaltyBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
		             } else {

		                i = list.iterator();

		                while (i.hasNext()) {


		                     actionForm.setPenaltyBatchNameList((String)i.next());

		                }

		             }


	             actionForm.clearPaymentTermList();

                 list = ejbINVC.getAdPytAll(user.getCmpCode());

                 if (list == null || list.size() == 0) {

                     actionForm.setPaymentTermList(Constants.GLOBAL_NO_RECORD_FOUND);

                 } else {

                     i = list.iterator();

                     while (i.hasNext()) {

                         actionForm.setPaymentTermList((String)i.next());

                     }

                     actionForm.setPaymentTerm("IMMEDIATE");

                 }



                 actionForm.clearCustomerBatchList();

	         		list = ejbINVC.getAdLvCustomerBatchAll(user.getCmpCode());

	         		if (list == null || list.size() == 0) {

	         			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);

	         		} else {

	         			i = list.iterator();

	         			while (i.hasNext()) {

	         				actionForm.setCustomerBatchList((String)i.next());

	         			}

	         		}



                 actionForm.clearCustomerList();

 				list = ejbINVC.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

 				if (list == null || list.size() == 0) {

 					actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

 				} else {

 					i = list.iterator();

 					while (i.hasNext()) {

 						actionForm.setCustomerList((String)i.next());

 					}

 				}

				actionForm.clearSupplierList();

                                list = ejbSML.getSupplierInvestors(user.getCmpCode());


 				if (list == null || list.size() == 0) {

 					actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

 				} else {

 					i = list.iterator();

 					while (i.hasNext()) {

 						actionForm.setSupplierList((String)i.next());

 					}

 				}


				actionForm.reset(mapping, request);

				actionForm.clearArBSMLList();

				for (int j = 0; j < user.getBrnchCount(); j++) {

					 Branch branch = user.getBranch(j);
					 AdBranchDetails details = new AdBranchDetails();
					 details.setBrCode(new Integer(branch.getBrCode()));
					 details.setBrName(branch.getBrName());
					 ArBranchStandardMemoLineList arBSMLList = new ArBranchStandardMemoLineList(actionForm,
							details.getBrName(), details.getBrCode());
					 arBSMLList.setSubjectToCommission(false);

					 if ( request.getParameter("forward") == null )
					 	arBSMLList.setBranchCheckbox(true);

					 actionForm.saveArBSMLList(arBSMLList);
				}

				if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));

				} else {

					if (request.getParameter("saveButton") != null ||
							request.getParameter("updateButton") != null||
							request.getParameter("generateButton")!=null) {

						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

					}
				}

				if (actionForm.getTableType() == null) {

					actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

				}

				actionForm.setPageState(Constants.PAGE_STATE_SAVE);
				actionForm.setEnable(true);
				actionForm.setTaxable(true);

				if (request.getParameter("child") != null)
				{
					System.out.println("HELLO HELLO");
					actionForm.setSelectedItemName(request.getParameter("selectedItemName"));
					return(mapping.findForward("arFindMemoLinesChild"));
				}
				else{
					return(mapping.findForward("arStandardMemoLines"));
				}







			} else {

				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));

				return(mapping.findForward("cmnMain"));

			}







		} catch(Exception e) {

			/*******************************************************
			 System Failed: Forward to error page
			 *******************************************************/

			if (log.isInfoEnabled()) {

				log.info("Exception caught in ArStandardMemoLineAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
				e.printStackTrace();

			return mapping.findForward("cmnErrorPage");

		}

	}

}