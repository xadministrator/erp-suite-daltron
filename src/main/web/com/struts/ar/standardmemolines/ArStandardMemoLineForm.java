package com.struts.ar.standardmemolines;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ar.taxcodes.ArTaxCodeList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdModBranchStandardMemoLineDetails;


public class ArStandardMemoLineForm extends ActionForm implements Serializable {
	
	private Integer standardMemoLineCode = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String name = null;
	private String description = null;
	private String wpProductID =null;
	private String unitPrice = null;
	private boolean taxable = false;	
	private String accountNumber = null;
	private String accountNumberDescription = null;
	private boolean enableAccountNumber = false;
	private boolean enable = false;
	private String unitOfMeasure = null;
	private String interimAccount = null;	
	private String interimAccountDescription = null;
	private String receivableAccountNumber = null;
    private String receivableAccountNumberDescription = null;
    private boolean enableRevenueAccountNumber = false;
    private String revenueAccountNumber = null;
    private String revenueAccountNumberDescription = null;
	
	private String tableType = null;
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String pageState = new String();
	private ArrayList arSMLList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	
	private String invcEffDate = null;
	private String invcRcvDate = null;
	private String genInvoiceDesc = null;
	private String selectedItemName = null;
        private String sourceType = null;
	
	private ArrayList arBSMLList = new ArrayList();
	
	private boolean isAllCustomer= true;
	
	
	
	private String batchName = null;
	private ArrayList batchNameList = new ArrayList();
	
	private String penaltyBatchName = null;
	private ArrayList penaltyBatchNameList = new ArrayList();
	
	private boolean showBatchName = false;
	
	
	private ArrayList paymentTermList = new ArrayList();
	private boolean showPaymentTerm = false;
	private String paymentTerm = null;
	private boolean enablePaymentTerm = false;
	
	private String customer = null;
	private ArrayList customerBatchList = new ArrayList();
	private String[] customerBatchSelectedList = new String[0];
	
	private boolean subjectToCommission = false;
	
	private ArrayList customerList = new ArrayList();   
        
        private String supplier = null;
        private ArrayList supplierList = new ArrayList();
        
        private boolean isRecalculate = false;
        private String period = null;
        private ArrayList periodList = new ArrayList();

	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public ArStandardMemoLineList getArSMLByIndex(int index) {
		
		return((ArStandardMemoLineList)arSMLList.get(index));
		
	}
	
	public Object[] getArSMLList() {
		
		return arSMLList.toArray();
		
	}
	
	
	
	
	public int getArSMLListSize() {
		
		return arSMLList.size();
		
	}
	
	
	
	
	
	public ArrayList getPaymentTermList(){
		return paymentTermList;
	}
	
	public void setPaymentTermList(String paymentTerm){
		
		paymentTermList.add(paymentTerm);
	}
	
	
	
	public void clearPaymentTermList(){
		paymentTermList.clear();
		paymentTermList.add(Constants.GLOBAL_BLANK);
	}
	
	
	public void saveArSMLList(Object newArSMLList) {
		
		arSMLList.add(newArSMLList);
		
	}
	
	public void clearArSMLList() {
		
		arSMLList.clear();
		
	}
	
	

	public ArrayList getCustomerList() {
   	
   	  return customerList;
   	
   }
   
   public void setCustomerList(String customer) {
   	
   	  customerList.add(customer);
   	
   }
   
             
   
   public void clearCustomerList() {
   	
   	  customerList.clear();
   	  customerList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   
   
   public String getSupplier(){
       return supplier;
   }
   
   public void setSupplier(String supplier){
       this.supplier = supplier;
   }
   
   
   
   public ArrayList getSupplierList() {
   	
   	  return supplierList;
   	
   }
   
   public void setSupplierList(String supplier) {
   	
   	  supplierList.add(supplier);
   	
   }
   
             
   
   public void clearSupplierList() {
   	
   	  supplierList.clear();
   	  supplierList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   
   
   
   
   public String[] getCustomerBatchSelectedList() {

		return customerBatchSelectedList;

	}

public void setCustomerBatchSelectedList(String[] customerBatchSelectedList) {

	this.customerBatchSelectedList = customerBatchSelectedList;

}


	   public ArrayList getCustomerBatchList() {
	   	
	      return customerBatchList;
	      
	   }

	   public void setCustomerBatchList(String customerBatch) {
	   	
	      customerBatchList.add(customerBatch);
	      
	   }
	   
	   public void clearCustomerBatchList() {
	   	
		   customerBatchList.clear();
		   customerBatchList.add(Constants.GLOBAL_BLANK);
	      
	   }
   
	
	
	public void setRowSelected(Object selectedArSMLList, boolean isEdit) {
		
		this.rowSelected = arSMLList.indexOf(selectedArSMLList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void showArSMLRow(ArrayList branchList, int rowSelected) {
		
		this.type = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getType(); 
		this.name = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getName();
		this.description = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getDescription();
		this.wpProductID = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getWpProductID();
		this.unitPrice = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getUnitPrice();
		this.taxable = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getTaxable(); 
		this.accountNumber = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getAccountNumber();
		this.accountNumberDescription = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getAccountNumberDescription();
		
		this.receivableAccountNumber = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getReceivableAccountNumber();
		this.receivableAccountNumberDescription = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getReceivableAccountNumberDescription();
		this.revenueAccountNumber = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getRevenueAccountNumber();
		this.revenueAccountNumberDescription = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getRevenueAccountNumberDescription();
		
		
		this.enable = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getEnable(); 
		this.unitOfMeasure = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getUnitOfMeasure();
		this.subjectToCommission = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getSubjectToCommission();
		this.interimAccount = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getInterimAccount();		
		this.interimAccountDescription = ((ArStandardMemoLineList)arSMLList.get(rowSelected)).getInterimAccountDescription();

		System.out.println("this.interimAccount: " + this.interimAccount);
		System.out.println("this.interimAccountDescription: " + this.interimAccountDescription);
		
		Object[] obj = this.getArBSMLList();
		
		this.clearArBSMLList();

		for(int i=0; i<obj.length; i++) {
			ArBranchStandardMemoLineList bsmlList = (ArBranchStandardMemoLineList)obj[i];
			
			if(branchList != null) {
				Iterator j = branchList.iterator();
				
				while(j.hasNext()) {
					AdModBranchStandardMemoLineDetails mdetails = (AdModBranchStandardMemoLineDetails)j.next();
					if (mdetails.getBsmlBranchCode().equals(bsmlList.getBrCode())) {
						bsmlList.setBranchCheckbox(true);
						bsmlList.setBranchAccountNumber( mdetails.getBsmlAccountNumber() );
						bsmlList.setBranchAccountNumberDescription(mdetails.getBsmlAccountNumberDescription() );
						
						bsmlList.setBranchReceivableAccountNumber( mdetails.getBsmlReceivableAccountNumber() );
						bsmlList.setBranchReceivableAccountNumberDescription(mdetails.getBsmlReceivableAccountNumberDescription() );
						bsmlList.setBranchRevenueAccountNumber( mdetails.getBsmlRevenueAccountNumber() );
						bsmlList.setBranchRevenueAccountNumberDescription(mdetails.getBsmlRevenueAccountNumberDescription() );
						
						
						bsmlList.setSubjectToCommission(Common.convertByteToBoolean(mdetails.getBsmlSubjectToCommission()));
						break;
					}
				}
			}
			
			this.arBSMLList.add(bsmlList);
		}
	}
	
	
	public void updateArSMLRow(int rowSelected, Object newArSMLList) {
		
		arSMLList.set(rowSelected, newArSMLList);
		
	}
	
	public void deleteArSMLList(int rowSelected) {
		
		arSMLList.remove(rowSelected);
		
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
		this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
		this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getStandardMemoLineCode() {
		
		return standardMemoLineCode;
		
	}
	
	public void setStandardMemoLineCode(Integer standardMemoLineCode) {
		
		this.standardMemoLineCode = standardMemoLineCode;
		
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public String getInvcEffDate(){
		return invcEffDate;
	}
	
	public void setInvcEffDate(String invEffDate){
		this.invcEffDate = invEffDate;
	}
	
	
	public String getInvcRcvDate(){
		return invcRcvDate;
	}
	
	public void setInvRcvDate(String invRcvDate){
		this.invcRcvDate = invcRcvDate;
	}
	
	
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	
	public String getName() {
		
		return name;
		
	}
	
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	
	public String getWpProductID() {
		
		return wpProductID;
		
	}
	
	public void setWpProductID(String wpProductID) {
		
		this.wpProductID = wpProductID;
		
	}
	
	public String getUnitPrice() {
		
		return unitPrice;
		
	}
	
	public void setUnitPrice(String unitPrice) {
		
		this.unitPrice = unitPrice;
		
	}
	
	public boolean getTaxable() {
		
		return taxable;
		
	}
	
	public void setTaxable(boolean taxable) {
		
		this.taxable = taxable;
		
	}
	
	public String getAccountNumber() {
		
		return accountNumber;
		
	}
	
	public void setAccountNumber(String accountNumber) {
		
		this.accountNumber = accountNumber;
		
	}
	
	public String getAccountNumberDescription() {
		
		return accountNumberDescription;
		
	}
	
	public void setAccountNumberDescription(String accountNumberDescription) {
		
		this.accountNumberDescription = accountNumberDescription;
		
	}
	
	public String getInterimAccountDescription() {
		
	  	return interimAccountDescription;
	
	}
	
	public void setInterimAccountDescription(String interimAccountDescription) {
	
	  	this.interimAccountDescription = interimAccountDescription;
	
	}
	
	public String getReceivableAccountNumber() {
        
        return receivableAccountNumber;   	
        
    }
    
    public void setReceivableAccountNumber(String receivableAccountNumber) {
        
        this.receivableAccountNumber = receivableAccountNumber;
        
    }
    
    public String getReceivableAccountNumberDescription() {
        
        return receivableAccountNumberDescription;   	
        
    }
    
    public void setReceivableAccountNumberDescription(String receivableAccountNumberDescription) {
        
        this.receivableAccountNumberDescription = receivableAccountNumberDescription;
        
    }
    
    public boolean getEnableRevenueAccountNumber() {
        
        return enableRevenueAccountNumber;
        
    }
    
    public void setEnableRevenueAccountNumber(boolean enableRevenueAccountNumber) {
        
        this.enableRevenueAccountNumber = enableRevenueAccountNumber;
        
    }
    
    public String getRevenueAccountNumber() {
        
        return revenueAccountNumber;   	
        
    }
    
    public void setRevenueAccountNumber(String revenueAccountNumber) {
        
    	this.revenueAccountNumber = revenueAccountNumber;
        
    }
    
    public String getRevenueAccountNumberDescription() {
        
        return revenueAccountNumberDescription;   	
        
    }
    
    public void setRevenueAccountNumberDescription(String revenueAccountNumberDescription) {
        
        this.revenueAccountNumberDescription = revenueAccountNumberDescription;
        
    }
	
	public String getInterimAccount() {
		
	  	return interimAccount;
	
	}
	
	public void setInterimAccount(String interimAccount) {
	
	  	this.interimAccount = interimAccount;
	
	}
	
	public boolean getEnable() {
		
		return enable;
		
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		
	}
	
	public boolean getEnableAccountNumber() {
		
		return enableAccountNumber;
		
	}
	
	public void setEnableAccountNumber(boolean enableAccountNumber) {
		
		this.enableAccountNumber = enableAccountNumber;
		
	}    
	
	public String getUnitOfMeasure() {
		
		return unitOfMeasure;
		
	}
	
	public void setUnitOfMeasure(String unitOfMeasure) {
		
		this.unitOfMeasure = unitOfMeasure;
		
	}
	
	public String getSelectedItemName() {
		
		return selectedItemName;
		
	}
	
	public void setSelectedItemName(String selectedItemName) {
		
		this.selectedItemName = selectedItemName;
		
	}
        
        
        public String getSourceType() {
		
		return sourceType;
		
	}
	
	public void setSourceType(String sourceType) {
		
		this.sourceType = sourceType;
		
	}
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}   
	
	public Object[] getArBSMLList(){
		
		return arBSMLList.toArray();
		
	}
	
	public ArBranchStandardMemoLineList getArBSMLByIndex(int index) {
		
		return ((ArBranchStandardMemoLineList)arBSMLList.get(index));
		
	}
	
	public int getArBSMLListSize(){
		
		return(arBSMLList.size());
		
	}
	
	public void saveArBSMLList(Object newArBSMLList){
		
		arBSMLList.add(newArBSMLList);   	  
		
	}
	
	public void clearArBSMLList(){
		
		arBSMLList.clear();
		
	}
	
	public void setArBSMLList(ArrayList arBSMLList) {
		
		this.arBSMLList = arBSMLList;
		
	}
	
	public boolean getSubjectToCommission() {
		
		return subjectToCommission;
		
	}
	
	public void setSubjectToCommission(boolean subjecToCommision) {
		
		this.subjectToCommission = subjecToCommision;
		
	}
	
	
	public boolean getIsAllCustomer(){
		return isAllCustomer;
	}
	
	public void setIsAllCutomer(boolean isAllCustomer){
		this.isAllCustomer = isAllCustomer;
	}
	
	
	public String getGenInvoiceDesc(){
		return genInvoiceDesc;
	}
	
	public void setGenInvoiceDesc(String genInvoiceDesc){
		this.genInvoiceDesc = genInvoiceDesc;
	}
	
	public boolean getShowBatchName() {
		
		return showBatchName;
		
	}
	
	
	
	public void setShowBatchName(boolean showBatchName) {
		
		this.showBatchName = showBatchName;
		
	}
	
	public boolean getShowPaymentTerm(){
		return showPaymentTerm;
	}
	
	public void setShowPaymentTerm(boolean showPaymentTerm){
		this.showPaymentTerm = showPaymentTerm;
	}
	
	public boolean getEnablePaymentTerm(){
		return enablePaymentTerm;
	}
	
	public void setEnablePaymentTerm(boolean enablePaymentTerm){
		this.enablePaymentTerm =  enablePaymentTerm;
	}
	
	
	
	
	public String getBatchName(){
		
		return batchName;
	}
	
	public void setBatchName(String batchName){
		
		this.batchName = batchName;
		
	}
	
	public ArrayList getBatchNameList() {
		
		return batchNameList;
		
	}
	
	public void setBatchNameList(String batchName) {
		
		batchNameList.add(batchName);
		
	}
	
	public void clearBatchNameList() {   	 
		
		batchNameList.clear();
		batchNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	
	
	public String getPenaltyBatchName(){
		     
		return penaltyBatchName;
	}
	
	public void setPenaltyBatchName(String penaltyBatchName){
		
		this.penaltyBatchName = penaltyBatchName;
		
	}
	
	public ArrayList getPenaltyBatchNameList() {
		
		return penaltyBatchNameList;
		
	}
	
	public void setPenaltyBatchNameList(String penaltyBatchName) {
		
		penaltyBatchNameList.add(penaltyBatchName);
		
	}
	
	public void clearPenaltyBatchNameList() {   	 
		
		penaltyBatchNameList.clear();
		penaltyBatchNameList.add(Constants.GLOBAL_BLANK);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getPaymentTerm(){
		return paymentTerm;
	}
	
	public void setPaymentTerm(String paymentTerm){
		this.paymentTerm = paymentTerm;
	}
	
	
	public String getCustomer() {
	   	
	   	  return customer;
	   	
	   }
	   
	   public void setCustomer(String customer) {
	   	
	   	  this.customer = customer;
	   	
	   }
	   
	public boolean getIsRecalculate(){
		return isRecalculate;
	}
	
	public void setIsRecalculate(boolean isRecalculate){
		this.isRecalculate = isRecalculate;
	}   
           
           
           
        public String getPeriod(){
		return(period);
	}
	
	public void setPeriod(String period){
		this.period = period;
	}
	
	public ArrayList getPeriodList(){
		return(periodList);
	}
	
	public void setPeriodList(String period){
		periodList.add(period);
	}
	
	public void clearPeriodList(){
		periodList.clear();
		periodList.add(Constants.GLOBAL_BLANK);
        }
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<arBSMLList.size(); i++) {
			
			ArBranchStandardMemoLineList actionList = (ArBranchStandardMemoLineList)arBSMLList.get(i);
			actionList.setBranchAccountNumber("");
			actionList.setBranchAccountNumberDescription("");
			
			actionList.setBranchReceivableAccountNumber("");
			actionList.setBranchReceivableAccountNumberDescription("");
			actionList.setBranchRevenueAccountNumber("");
			actionList.setBranchRevenueAccountNumberDescription("");
			
			actionList.setBranchCheckbox(false);
			actionList.setSubjectToCommission(false);
			
		}
		
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK); 
		typeList.add(Constants.AR_STANDARD_MEMO_LINE_TYPE_LINE);
		typeList.add(Constants.AR_STANDARD_MEMO_LINE_TYPE_TAX);
		typeList.add(Constants.AR_STANDARD_MEMO_LINE_TYPE_FREIGHT);
		typeList.add(Constants.AR_STANDARD_MEMO_LINE_TYPE_SERVICE_CHARGE);		
		type = Constants.AR_STANDARD_MEMO_LINE_TYPE_LINE; 
		name = null;
		description = null;
		wpProductID = null;
		unitPrice = null;
		taxable = false;
		accountNumber = null;
		accountNumberDescription = null;
		interimAccount = null;		
		interimAccountDescription = null;
		receivableAccountNumber = null;
        receivableAccountNumberDescription = null;
        revenueAccountNumber = null;
        revenueAccountNumberDescription = null;
		enable = false;		
		unitOfMeasure = null;
		showDetailsButton = null;
		hideDetailsButton = null;      
		subjectToCommission = false;
		isAllCustomer = true;
		genInvoiceDesc = null;
		customer = Constants.GLOBAL_BLANK;
                isRecalculate = false;
                supplier =Constants.GLOBAL_BLANK;
                
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
            ActionErrors errors = new ActionErrors();
            if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

                if (Common.validateRequired(type)) {

                        errors.add("type",
                                        new ActionMessage("standardMemoLine.error.typeRequired"));

                }

                if (Common.validateRequired(name)) {

                        errors.add("name",
                                        new ActionMessage("standardMemoLine.error.nameRequired"));

                }

                if (Common.validateRequired(unitPrice)) {

                        errors.add("unitPrice",
                                        new ActionMessage("standardMemoLine.error.unitPriceRequired"));

                }

                if (!Common.validateNumberFormat(unitPrice)) {

                        errors.add("unitPrice",
                                        new ActionMessage("standardMemoLine.error.unitPriceInvalid"));

                }

                if (Common.validateRequired(accountNumber) && (enableAccountNumber == true)) {

                        errors.add("accountNumber",
                                        new ActionMessage("standardMemoLine.error.accountNumberRequired"));

                }				

                if (!Common.validateRequired(genInvoiceDesc)){
                        errors.add("description",
                                        new ActionMessage("standardMemoLine.error.descriptionRequired"));
                }

                if (Common.validateRequired(receivableAccountNumber)) {

                errors.add("receivableAccount",
                    new ActionMessage("customerEntry.error.receivableAccountRequired"));

                }

                if (Common.validateRequired(revenueAccountNumber) && enableRevenueAccountNumber) {

                    errors.add("revenueAccount",
                            new ActionMessage("customerEntry.error.revenueAccountRequired"));

                }
            }
		
		
            if (request.getParameter("generateButton") != null ) {
                    System.out.println("=---------->request.getParameter(generateButton) != null="+invcEffDate);


                if(Common.validateRequired(invcEffDate)){
                        errors.add("invcEffDate",
                                        new ActionMessage("invoiceEntry.error.dateRequired"));
                }

                if(!Common.validateDateFormat(invcEffDate)){
                        errors.add("invcEffDate", 
                                        new ActionMessage("invoiceEntry.error.dateInvalid"));
                }
                try{

                        int year = Integer.parseInt(invcEffDate.substring(invcEffDate.lastIndexOf("/")+1, (invcEffDate.lastIndexOf("/")+5)));
                        if(year<1900){
                                errors.add("invcEffDate", 
                                                new ActionMessage("invoiceEntry.error.dateInvalid"));
                        }
                } catch(Exception ex){

                        errors.add("invcEffDate", 
                                        new ActionMessage("invoiceEntry.error.dateInvalid"));

                }


                /*
                  if (!Common.validateDateFormat(invcEffDate)) {
                                System.out.println("Common.validateDateFormat(invcEffDate))");
                             errors.add("invoice Effective Date",
                                new ActionMessage("invoiceEntry.error.dateInvalid"));

                  }     */    

                  if (!Common.validateDateFormat(invcRcvDate)) {

                             errors.add("invoice Receive Date",
                                new ActionMessage("invoiceEntry.error.dateInvalid"));

                  }   
                  if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
                                        showBatchName){
                                errors.add("batchName",
                                                new ActionMessage("invoiceEntry.error.batchNameRequired"));
                  }

                  if((Common.validateRequired(paymentTerm) || paymentTerm.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
                                        enablePaymentTerm){
                                errors.add("paymentTerm",
                                                new ActionMessage("paymentTerm.error.termNameRequired"));
                  }

                  if((Common.validateRequired(genInvoiceDesc) || genInvoiceDesc.equals(Constants.GLOBAL_NO_RECORD_FOUND))){
                                errors.add("paymentTerm",
                                                new ActionMessage("paymentTerm.error.descriptionRequired"));
                  }

            }
		
           if (request.getParameter("generateArInvestorBonusAndInterest") != null ) {

                if (request.getParameter("executeFADepreciationExecuteButton") != null) {
                        if(Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
                                errors.add("period", new ActionMessage("detailTrialBalance.error.periodRequired"));
                        }
                        if(!Common.validateStringExists(periodList, period)){
                                errors.add("period", new ActionMessage("detailTrialBalance.error.periodInvalid"));
                        }
                }
               
           }
                 

            return errors;
		
	}
	
}