package com.struts.ar.standardmemolines;

import java.io.Serializable;

public class ArStandardMemoLineList implements Serializable {

    private Integer standardMemoLineCode = null;
    private String type = null;
    private String name = null;
    private String description = null;
    private String wpProductID = null;
    private String unitPrice = null;
    private boolean taxable = false;
    private String accountNumber = null;
    private String accountNumberDescription = null;
    private String receivableAccountNumber = null;
    private String receivableAccountNumberDescription = null;
    private String revenueAccountNumber = null;
    private String revenueAccountNumberDescription = null;
    
    private String interimAccount = null;
    private String interimAccountDescription = null;
    private boolean enable = false;
    private String unitOfMeasure = null;
    private boolean genInvoiceEnable = false;
    private boolean IsAllCustomer = false;

	private String editButton = null;
	private String deleteButton = null;
	
	private ArStandardMemoLineForm parentBean;
    
	private boolean subjectToCommission = false;
	
	public ArStandardMemoLineList(ArStandardMemoLineForm parentBean,
		Integer standardMemoLineCode,
		String type,
        String name,
        String description,
        String wpProductID,
		String unitPrice,
		boolean taxable,        
        String accountNumber,
        String accountNumberDescription,
        
        String receivableAccountNumber,
        String receivableAccountNumberDescription,
        String revenueAccountNumber,
        String revenueAccountNumberDescription,
        
        String interimAccount,
        String interimAccountDescription,
		boolean enable,
		String unitOfMeasure,
		boolean subjectToCommission,
		boolean genInvoiceEnable) {
	
		this.parentBean = parentBean;
		this.type = type;
        this.standardMemoLineCode = standardMemoLineCode;
        this.name = name;
        this.description = description;
        this.wpProductID = wpProductID;
        this.unitPrice = unitPrice;
        this.taxable = taxable;
        this.accountNumber = accountNumber;
        this.accountNumberDescription = accountNumberDescription;
        this.receivableAccountNumber = receivableAccountNumber;
        this.receivableAccountNumberDescription = receivableAccountNumberDescription;
        this.revenueAccountNumber = revenueAccountNumber;
        this.revenueAccountNumberDescription = revenueAccountNumberDescription;
        this.interimAccount = interimAccount;
        this.interimAccountDescription = interimAccountDescription;
		this.enable = enable;
		this.unitOfMeasure = unitOfMeasure;
		this.subjectToCommission = subjectToCommission;
		this.genInvoiceEnable = genInvoiceEnable;
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getStandardMemoLineCode() {
	
	    return standardMemoLineCode;
	
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public String getName() {
		
		return name;
		
    }
    
    public String getDescription() {
    	
    	return description;
    	
    }
    
    public String getWpProductID() {
    	
    	return wpProductID;
    	
    }
    
    public String getUnitPrice() {
    	
    	return unitPrice;
    	
    }
        
    public boolean getTaxable() {
    	
    	return taxable;
    	
    }
    
    public String getAccountNumber() {
    	
    	return accountNumber;
    	
    }
    
    public String getAccountNumberDescription() {
    	
    	return accountNumberDescription;
    	
    }
    
    public String getReceivableAccountNumber() {
    	
    	return receivableAccountNumber;
    	
    }
    
    public String getReceivableAccountNumberDescription() {
    	
    	return receivableAccountNumberDescription;
    	
    }
    
    public String getRevenueAccountNumber() {
    	
    	return revenueAccountNumber;
    	
    }
    
    public String getRevenueAccountNumberDescription() {
    	
    	return revenueAccountNumberDescription;
    	
    }
    
    
    public String getInterimAccount() {
		
	    return interimAccount;
	
	}
	
	public String getInterimAccountDescription() {
	
	    return interimAccountDescription;
	
	}

    public boolean getEnable() {
    	
    	return enable;
    	
    }
    
    public String getUnitOfMeasure() {
    	
    	return unitOfMeasure;
    	
    }
       
    public boolean getSubjectToCommission() {
    	
    	return subjectToCommission;
    	
    }
    
    public boolean getGenInvoiceEnable(){
    	return genInvoiceEnable;
    }
    
    public void setGenInvoiceEnable(boolean genInvoiceEnable){
    	this.genInvoiceEnable = genInvoiceEnable;
    }

    
}