package com.struts.ar.taxcodes;

import java.io.Serializable;

public class AdBranchArTaxCodeList implements Serializable {
	
	
	private Integer branchCode = null;
	private String branchName = null;
	
	private boolean branchCheckbox = false;	
	private String branchTaxCodeAccount = null;
	private String branchTaxCodeAccountDescription = null;
	
	private String branchInterimCodeAccount = null;
	private String branchInterimCodeAccountDescription = null;

	private ArTaxCodeForm parentBean;
	
	public AdBranchArTaxCodeList(ArTaxCodeForm parentBean,
			Integer branchCode,
			String branchName,
			String branchTaxCodeAccount,
			String branchTaxCodeAccountDescription,
			String branchInterimCodeAccount,
			String branchInterimCodeAccountDescription

			) {
		
		this.parentBean = parentBean;
		this.branchCode = branchCode;
		this.branchName = branchName;
		this.branchTaxCodeAccount = branchTaxCodeAccount;
		this.branchTaxCodeAccountDescription = branchTaxCodeAccountDescription;
		
		this.branchInterimCodeAccount = branchInterimCodeAccount;
		this.branchInterimCodeAccountDescription = branchInterimCodeAccountDescription;

		
	}
	
	public String getBranchName() {
		
		return branchName;
		
	}
	
	public void setBranchName(String branchName) {
		
		this.branchName = branchName;
		
	}
	
	public Integer getBranchCode() {
		
		return branchCode;
		
	}
	
	public void setBranchCode(Integer branchCode) {
		
		this.branchCode = branchCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	

	public String getBranchTaxCodeAccount() {
		
		return this.branchTaxCodeAccount;
		
	}
	
	public void setBranchTaxCodeAccount(String branchTaxCodeAccount) {
		
		this.branchTaxCodeAccount = branchTaxCodeAccount;
		
	}
	
	public String getBranchTaxCodeAccountDescription() {
		
		return this.branchTaxCodeAccountDescription;
		
	}
	
	public void setBranchTaxCodeAccountDescription(String branchTaxCodeAccountDescription) {
		
		this.branchTaxCodeAccountDescription = branchTaxCodeAccountDescription;
		
	}
	
	
	
public String getBranchInterimCodeAccount() {
		
		return this.branchInterimCodeAccount;
		
	}
	
	public void setBranchInterimCodeAccount(String branchInterimCodeAccount) {
		
		this.branchInterimCodeAccount = branchInterimCodeAccount;
		
	}
	
	public String getBranchInterimCodeAccountDescription() {
		
		return this.branchInterimCodeAccountDescription;
		
	}
	
	public void setBranchInterimCodeAccountDescription(String branchInterimCodeAccountDescription) {
		
		this.branchInterimCodeAccountDescription = branchInterimCodeAccountDescription;
		
	}
	
}