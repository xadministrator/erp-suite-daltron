package com.struts.ar.taxcodes;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ArTCCoaGlTaxCodeAccountNotFoundException;
import com.ejb.exception.ArTCInterimAccountInvalidException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArTaxCodeController;
import com.ejb.txn.ArTaxCodeControllerHome;
import com.struts.ad.bankaccount.AdBankAccountForm;
import com.struts.ad.bankaccount.AdBankAccountList;
import com.struts.ad.bankaccount.AdBranchBankAccountList;
import com.struts.ar.standardmemolines.ArStandardMemoLineList;
import com.struts.ar.taxcodes.ArTaxCodeForm;
import com.struts.ar.taxcodes.ArTaxCodeList;
import com.struts.ar.taxcodes.AdBranchArTaxCodeList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdModBranchBankAccountDetails;
import com.util.AdResponsibilityDetails;
import com.util.AdModBranchArTaxCodeDetails;
import com.util.ArModTaxCodeDetails;
import com.util.ArTaxCodeDetails;

public final class ArTaxCodeAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArTaxCodeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArTaxCodeForm actionForm = (ArTaxCodeForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_TAX_CODE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arTaxCodes");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArTaxCodeController EJB
*******************************************************/

         ArTaxCodeControllerHome homeTC = null;
         ArTaxCodeController ejbTC = null;

         try {

            homeTC = (ArTaxCodeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArTaxCodeControllerEJB", ArTaxCodeControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbTC = homeTC.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ar TC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
        	 ((ArTaxCodeForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arTaxCodes"));
	     
/*******************************************************
   -- Ar TC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	    	 ((ArTaxCodeForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arTaxCodes"));                  
         
/*******************************************************
   -- Ar TC Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 ArModTaxCodeDetails details = new ArModTaxCodeDetails();
            details.setTcName(actionForm.getTaxName());
            details.setTcDescription(actionForm.getDescription());
            details.setTcType(actionForm.getType());                                    
            details.setTcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setTcEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            

         // For Branch Setting
            ArrayList branchList = new ArrayList();
                     
                     for (int i = 0; i<actionForm.getArBTCListSize(); i++) {
                     	
                    	 AdBranchArTaxCodeList arBtcList = actionForm.getArBTCByIndex(i);           	   
                     	
                    	
                     	if (arBtcList.getBranchCheckbox() == true ){ 
                     		
                     		// checks if branches glcoa values are null
                     		
                     		if ( Common.validateRequired(arBtcList.getBranchTaxCodeAccount()) ||
                     				Common.validateRequired(arBtcList.getBranchTaxCodeAccountDescription())) {

                     			arBtcList.setBranchTaxCodeAccount(actionForm.getTaxAccount());
                    

                     		}
                     		
                     		if ( Common.validateRequired(arBtcList.getBranchInterimCodeAccount()) ||
                     				Common.validateRequired(arBtcList.getBranchInterimCodeAccountDescription())) {

                     			arBtcList.setBranchInterimCodeAccount(actionForm.getInterimAccount());
                    

                     		}
                     		
                     		
                     		
                     		AdModBranchArTaxCodeDetails brTcDetails = new AdModBranchArTaxCodeDetails();
                     		brTcDetails.setBtcAdCompany(user.getCmpCode());
                     		brTcDetails.setBtcBranchCode(arBtcList.getBranchCode()  );
                     		brTcDetails.setBtcBranchName(arBtcList.getBranchName() );
                     		brTcDetails.setBtcTaxCodeAccountNumber(arBtcList.getBranchTaxCodeAccount());
                     		brTcDetails.setBtcTaxCodeAccountDescription(arBtcList.getBranchTaxCodeAccountDescription());
                     		
                     		brTcDetails.setBtcInterimCodeAccountNumber(arBtcList.getBranchInterimCodeAccount());
                     		brTcDetails.setBtcInterimCodeAccountDescription(arBtcList.getBranchInterimCodeAccountDescription());
                     				
                     		
                         	branchList.add(brTcDetails);

                     	}
                     	
                     	
                     }
                     
            try {
            	
            	ejbTC.addArTcEntry(details,
            			actionForm.getTaxAccount(), actionForm.getInterimAccount(), branchList, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("arTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("arTaxCode.error.accountNumberInvalid"));
               
            } catch (ArTCInterimAccountInvalidException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("arTaxCode.error.interimAccountInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("2EJBException caught in ArTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ar TC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar TC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
        	 ArTaxCodeList arTCList =
                     ((ArTaxCodeForm)form).getArTCByIndex(
                     ((ArTaxCodeForm) form).getRowSelected());
            
           
            ArModTaxCodeDetails details = new ArModTaxCodeDetails();
            
            details.setTcCode(arTCList.getTaxCode());
            details.setTcName(actionForm.getTaxName());
            details.setTcDescription(actionForm.getDescription());
            details.setTcType(actionForm.getType());
            System.out.println("actionForm.getRate()="+actionForm.getRate());
            details.setTcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setTcEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            
         // For Branch Setting
            ArrayList branchList = new ArrayList();
                     
                     for (int i = 0; i<actionForm.getArBTCListSize(); i++) {
                     	
                    	 AdBranchArTaxCodeList arBtcList = actionForm.getArBTCByIndex(i);           	   
                     	
                    	
                     	if (arBtcList.getBranchCheckbox() == true ){ 
                     		
                     		// checks if branches glcoa values are null
                     		
                     		if ( Common.validateRequired(arBtcList.getBranchTaxCodeAccount()) ||
                     				Common.validateRequired(arBtcList.getBranchTaxCodeAccountDescription())) {

                     			arBtcList.setBranchTaxCodeAccount(actionForm.getTaxAccount());
                    

                     		}
                     		
                     		
                     		
                     		AdModBranchArTaxCodeDetails brTcDetails = new AdModBranchArTaxCodeDetails();
                     		brTcDetails.setBtcAdCompany(user.getCmpCode());
                     		brTcDetails.setBtcBranchCode(arBtcList.getBranchCode()  );
                     		brTcDetails.setBtcBranchName(arBtcList.getBranchName() );
                     		brTcDetails.setBtcTaxCodeAccountNumber(arBtcList.getBranchTaxCodeAccount());
                     		brTcDetails.setBtcTaxCodeAccountDescription(arBtcList.getBranchTaxCodeAccountDescription());
                     		
                     		brTcDetails.setBtcInterimCodeAccountNumber(arBtcList.getBranchInterimCodeAccount());
                     		brTcDetails.setBtcInterimCodeAccountDescription(arBtcList.getBranchInterimCodeAccountDescription());
                     		
                     				
                     		
                         	branchList.add(brTcDetails);

                     	}
                     	
                     	
                     }
                     
            AdResponsibilityDetails mdetails  = ejbTC.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
                     
            
            try {
            	
            	ejbTC.updateArTcEntry(details, actionForm.getTaxAccount(), actionForm.getInterimAccount(), mdetails.getRsName(),branchList ,user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("arTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("arTaxCode.error.accountNumberInvalid"));
               
            } catch (GlobalRecordAlreadyAssignedException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("arTaxCode.error.updateTaxCodeAlreadyAssigned"));

     
            } catch (ArTCCoaGlTaxCodeAccountNotFoundException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("arTaxCode.error.taxCodeNoRecordFound"));
               
            
            } catch (ArTCInterimAccountInvalidException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("arTaxCode.error.interimAccountInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("3EJBException caught in ArTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ar TC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ar TC Edit Action --
*******************************************************/
            
         }else if(request.getParameter("arTCList[" + 
                 ((ArTaxCodeForm)form).getRowSelected() + "].editButton") != null &&
                 ((ArTaxCodeForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
        	 
        	 
        	 	actionForm.reset(mapping, request);
        	 
        	 	Object[] arTCList = actionForm.getArTCList();
				
				// get list of branches
	        	ArrayList branchList = new ArrayList();
	        	
	        	AdResponsibilityDetails details = ejbTC.getAdRsByRsCode(new Integer(user.getCurrentResCode()));

	        	try {                                  
	                 
	        		branchList = ejbTC.getAdBrTcAll(((ArTaxCodeList)arTCList[actionForm.getRowSelected()]).getTaxCode(),details.getRsName(), user.getCmpCode());
	                 
	                 
	             } catch(GlobalNoRecordFoundException ex) {
	                 
	                 
	             }				
	            
	         	actionForm.showArTCRow(branchList, actionForm.getRowSelected());
		
           
            return mapping.findForward("arTaxCodes");
            
/*******************************************************
   -- Ar TC Delete Action --
*******************************************************/

         } else if (request.getParameter("arTCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ArTaxCodeList arTCList =
              actionForm.getArTCByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbTC.deleteArTcEntry(arTCList.getTaxCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("arTaxCode.error.deleteTaxCodeAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("arTaxCode.error.taxCodeAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("4EJBException caught in ArTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar TC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arTaxCodes");

            }
            
            if (request.getParameter("forward") == null &&
                    actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                    	                	
                    errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                    saveErrors(request, new ActionMessages(errors));
                  
                    return mapping.findForward("cmnMain");	
                    
                }
            System.out.println("----------------1");
            actionForm.reset(mapping, request);
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	           
	           
	           
	           
	           
	           actionForm.clearArBTCList();
               
               list = ejbTC.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
               
               i = list.iterator();
               
               while(i.hasNext()) {
               
                   AdBranchDetails details = (AdBranchDetails)i.next();
                   
                   
                   AdBranchArTaxCodeList arBTCList = new AdBranchArTaxCodeList(actionForm,
                       details.getBrCode(),
                       details.getBrName(),
                       null,
                       null,
                       null,
                       null
                       );
                   
                   
                   
                   System.out.println("----------------2");
                   if ( request.getParameter("forward") == null )
                	   arBTCList.setBranchCheckbox(true);
                                
                   actionForm.saveArBTCList(arBTCList);
                   
               }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("xxxxxxxxxxxEJBException caught in ArTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  
	        
	        
	        
	        try{
	        	

	        	System.out.println("----------------3");
	               actionForm.clearArTCList();	        
		           
		           list = ejbTC.getArTcAll(user.getCmpCode()); 
		           
		           i = list.iterator();
		            
		           while(i.hasNext()) {
	        	            			            	
		              ArModTaxCodeDetails mdetails = (ArModTaxCodeDetails)i.next();
		            	
		              ArTaxCodeList arTCList = new ArTaxCodeList(actionForm,
		            	    mdetails.getTcCode(),
		            	    mdetails.getTcName(),
		            	    mdetails.getTcDescription(),
		            	    mdetails.getTcType(),
		            	    Common.convertDoubleToStringMoney(mdetails.getTcRate(), Constants.MONEY_RATE_PRECISION),
	                        mdetails.getTcCoaGlTaxAccountNumber(),
	                        mdetails.getTcCoaGlTaxDescription(),
							mdetails.getTcInterimAccountNumber(),
							mdetails.getTcInterimAccountDescription(),
	                        Common.convertByteToBoolean(mdetails.getTcEnable()));                            
		            	 	            	    
		              actionForm.saveArTCList(arTCList);
		            	
		           }
		           System.out.println("----------------4");
		           
	        } catch (GlobalNoRecordFoundException ex) {
		        
		        
		    } catch (EJBException ex) {
                
                if (log.isInfoEnabled()) {

                  log.info("-------------EJBException caught in ArTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
	        
	        
	        System.out.println("----------------5");
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

            	if((request.getParameter("saveButton") != null || 
                        request.getParameter("updateButton") != null || 
                        request.getParameter("arTCList[" + 
                        ((ArTaxCodeForm) form).getRowSelected() + "].deleteButton") != null) && 
                        ((ArTaxCodeForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                        ((ArTaxCodeForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
                    }
            }

            if (((ArTaxCodeForm)form).getTableType() == null) {
                
                ((ArTaxCodeForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
                
            }                
            System.out.println("----------------6");
            ((ArTaxCodeForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("arTaxCodes"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

  /*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("1Exception caught in ArTaxCodeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}