package com.struts.ar.taxcodes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.ad.bankaccount.AdBranchBankAccountList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.AdModBranchBankAccountDetails;
import com.util.AdModBranchArTaxCodeDetails;
import com.util.Debug;


public class ArTaxCodeForm extends ActionForm implements Serializable {

	private Integer taxCode = null;
	private String taxName = null;
	private String description = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String rate = null;
	private String taxAccount = null;
	private String interimAccount = null;
	private String accountDescription = null;
	private String interimAccountDescription = null;
	private boolean enable = false;
	private String tableType = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList arTCList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private ArrayList arBTCList = new ArrayList();
	public Object[] getArBTCList(){
	       
	       return arBTCList.toArray();
	       
	   }
	   
	   public AdBranchArTaxCodeList getArBTCByIndex(int index){
	       
	       return ((AdBranchArTaxCodeList)arBTCList.get(index));
	       
	   }
	   
	   public int getArBTCListSize(){
	       
	       return(arBTCList.size());
	       
	   }
	   
	   public void saveArBTCList(Object newArBTCList){
	       
	       arBTCList.add(newArBTCList);   	  
	       
	   }
	   
	   public void clearArBTCList(){
	       
	       arBTCList.clear();
	       
	   }
	   
	   public void setArBTCList(ArrayList arBTCList) {
	       
	       this.arBTCList = arBTCList;
	       
	   }
	   
	   
	   
	public int getRowSelected() {
	
	    return rowSelected;
	
	}
	
	public ArTaxCodeList getArTCByIndex(int index) {
	
	    return((ArTaxCodeList)arTCList.get(index));
	
	}
	
	public Object[] getArTCList() {
	
	    return arTCList.toArray();
	
	}
	
	public int getArTCListSize() {
	
	    return arTCList.size();
	
	}
	
	public void saveArTCList(Object newArTCList) {
	
	    arTCList.add(newArTCList);
	
	}
	
	public void clearArTCList() {
		
	    arTCList.clear();
	  
	}
	
	public void setRowSelected(Object selectedArTCList, boolean isEdit) {
	
	    this.rowSelected = arTCList.indexOf(selectedArTCList);
	
	    if (isEdit) {
	
	        this.pageState = Constants.PAGE_STATE_EDIT;
	
	    }
	
	}
	
	public void showArTCRow(ArrayList branchList,int rowSelected) {
		
		Debug.print("ArTaxCodeForm showArTCRow");
		
		this.taxName = ((ArTaxCodeList)arTCList.get(rowSelected)).getTaxName();
		this.description = ((ArTaxCodeList)arTCList.get(rowSelected)).getDescription();
		this.type = ((ArTaxCodeList)arTCList.get(rowSelected)).getType();
		this.rate = ((ArTaxCodeList)arTCList.get(rowSelected)).getRate();
		this.taxAccount = ((ArTaxCodeList)arTCList.get(rowSelected)).getTaxAccount();
		this.accountDescription = ((ArTaxCodeList)arTCList.get(rowSelected)).getAccountDescription();
		
		this.interimAccount = ((ArTaxCodeList)arTCList.get(rowSelected)).getInterimAccount();
		this.interimAccountDescription = ((ArTaxCodeList)arTCList.get(rowSelected)).getInterimAccountDescription();
		
		this.enable = ((ArTaxCodeList)arTCList.get(rowSelected)).getEnable(); 
	   
		
		
		Object[] obj = this.getArBTCList();
	       
	       this.clearArBTCList();
	       
	       for(int i=0; i<obj.length; i++) {
	           
	    	   AdBranchArTaxCodeList btcList = (AdBranchArTaxCodeList)obj[i];
	           
	           
	           if(branchList != null) {
	               
	               Iterator brListIter = branchList.iterator();
	               
	               while(brListIter.hasNext()) {
	            	   
	            	   AdModBranchArTaxCodeDetails brTcDetails = (AdModBranchArTaxCodeDetails)brListIter.next();
	                  
	                   if(brTcDetails.getBtcBranchCode().equals(btcList.getBranchCode())) {
	                	   
	                	   btcList.setBranchCheckbox(true);  
	                	   btcList.setBranchTaxCodeAccount(brTcDetails.getBtcTaxCodeAccountNumber());
	                	   btcList.setBranchTaxCodeAccountDescription(brTcDetails.getBtcTaxCodeAccountDescription());
	                	  
	                	   btcList.setBranchInterimCodeAccount(brTcDetails.getBtcInterimCodeAccountNumber());
	                	   btcList.setBranchInterimCodeAccountDescription(brTcDetails.getBtcInterimCodeAccountDescription());
	                	  
	                       
	                       break;
	                       
	                   }
	                   
	               }
	           }
	           
	           this.arBTCList.add(btcList);
	           
	       }
	}
	
	public void updateArTCRow(int rowSelected, Object newArTCList) {
	
	    arTCList.set(rowSelected, newArTCList);
	
	}
	
	public void deleteArTCList(int rowSelected) {
	
	    arTCList.remove(rowSelected);
	
	}
	
	public void setUpdateButton(String updateButton) {
	
	    this.updateButton = updateButton;
	
	}
	
	public void setCancelButton(String cancelButton) {
	
	    this.cancelButton = cancelButton;
	
	}
	
	public void setSaveButton(String saveButton) {
	
	    this.saveButton = saveButton;
	
	}
	
	public void setCloseButton(String closeButton) {
	
	    this.closeButton = closeButton;
	
	}
	
	public void setShowDetailsButton(String showDetailsButton) {
		
	    this.showDetailsButton = showDetailsButton;
		
	}
	
	public void setHideDetailsButton(String hideDetailsButton) {
		
	    this.hideDetailsButton = hideDetailsButton;
		
	}   
	
	public void setPageState(String pageState) {
	
	    this.pageState = pageState;
	
	}
	
	public String getPageState() {
	
	    return pageState;
	
	}
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}
	
	public Integer getTaxCode() {
		
		return taxCode;
		
	}
	
	public void setTaxCode(Integer taxCode) {
		
		this.taxCode = taxCode;
		
	}
	
	public String getTaxName() {
	
	  	return taxName;
	
	}
	
	public void setTaxName(String taxName) {
	
	  	this.taxName = taxName;
	
	}
	
	public String getDescription() {
	
		return description;
	
	}
	
	public void setDescription(String description) {
	
	  	this.description = description;
	
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public void setType(String type) {
		
		this.type = type;
		
	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	  
	public String getRate() {
	
	  	return rate;
	
	}
	
	public void setRate(String rate) {
	
	  	this.rate = rate;
	
	}
	
	public String getTaxAccount() {
	
	  	return taxAccount;
	
	}
	
	public void setTaxAccount(String taxAccount) {
	
	  	this.taxAccount = taxAccount;
	
	}
	
	public String getAccountDescription() {
	
	  	return accountDescription;
	
	}
	
	public void setAccountDescription(String accountDescription) {
	
	  	this.accountDescription = accountDescription;
	
	}   
	
	
	
	public String getInterimAccount() {
		
	  	return interimAccount;
	
	}
	
	public void setInterimAccount(String interimAccount) {
	
	  	this.interimAccount = interimAccount;
	
	}
	
	
	public String getInterimAccountDescription() {
		
	  	return interimAccountDescription;
	
	}
	
	public void setInterimAccountDescription(String interimAccountDescription) {
	
	  	this.interimAccountDescription = interimAccountDescription;
	
	}
	
	
	
	public boolean getEnable() {
		
	  	return enable;
	  
	}
	
	public void setEnable(boolean enable) {
		
		this.enable = enable;
		  
	}
	
	public String getTableType() {
		
	  	return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
	  	this.tableType = tableType;
		
	}   
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
	
		
		Debug.print("ArTaxCodeForm reset");
		
	       System.out.println("arBTCList.size()="+arBTCList.size());
	       for (int i=0; i<arBTCList.size(); i++) {
	           
	    	   AdBranchArTaxCodeList list = (AdBranchArTaxCodeList)arBTCList.get(i);
	           
	           
	           list.setBranchCheckbox(false);
	           list.setBranchTaxCodeAccount(null);
	           list.setBranchTaxCodeAccountDescription(null);

	       }  
	       
		taxName = null;
		description = null;
		typeList.clear();
		typeList.add(Constants.AR_TAX_CODE_TYPE_NONE);
		typeList.add(Constants.AR_TAX_CODE_TYPE_EXEMPT);
		typeList.add(Constants.AR_TAX_CODE_TYPE_INCLUSIVE);
		typeList.add(Constants.AR_TAX_CODE_TYPE_EXCLUSIVE);
		typeList.add(Constants.AR_TAX_CODE_TYPE_ZERO_RATED);
		type = Constants.AR_TAX_CODE_TYPE_NONE;
		rate = null;
		taxAccount = null;
		interimAccount = null;
		accountDescription = null;
		interimAccountDescription = null;		
		enable = false;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;   
		
		System.out.println("END ArTaxCodeForm reset");
	
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
	
			if (Common.validateRequired(taxName)) {
			
				errors.add("taxName",
			   		new ActionMessage("arTaxCode.error.taxNameRequired"));
			
			}
			
			if (type.equals(Constants.AP_TAX_CODE_TYPE_INCLUSIVE) || type.equals(Constants.AP_TAX_CODE_TYPE_EXCLUSIVE) || type.equals(Constants.AP_TAX_CODE_TYPE_ZERO_RATED)) {
			
				if (Common.validateRequired(taxAccount)) {
				
					errors.add("taxAccount",
				   		new ActionMessage("arTaxCode.error.taxAccountRequired"));
				
				}
				
		    }
			
			if (!Common.validateMoneyFormat(rate)) {
			
				errors.add("rate",
			   		new ActionMessage("arTaxCode.error.rateInvalid"));
			
			}
			
			if ((type.equals(Constants.AR_TAX_CODE_TYPE_NONE) ||
			     type.equals(Constants.AR_TAX_CODE_TYPE_EXEMPT) ||
			     type.equals(Constants.AR_TAX_CODE_TYPE_ZERO_RATED)) &&
			    (Common.validateMoneyFormat(rate)) && Common.convertStringMoneyToDouble(rate, (short)6) > 0d) {       
			    
				errors.add("rate",
			   		new ActionMessage("arTaxCode.error.rateMustBeZero"));
			   		
			}			    
	     
	  	}
	     
	  	return errors;
	
	}
	
}