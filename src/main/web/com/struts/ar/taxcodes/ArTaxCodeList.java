package com.struts.ar.taxcodes;

import java.io.Serializable;

public class ArTaxCodeList implements Serializable {

    private Integer taxCode = null;
    private String taxName = null;
    private String description = null;
    private String type = null;
    private String rate = null;
    private String taxAccount = null;
    private String accountDescription = null;
    
    private String interimAccount = null;
    private String interimAccountDescription = null;
    
    private boolean enable = false;

	private String editButton = null;
	private String deleteButton = null;

	private ArTaxCodeForm parentBean;
    
	public ArTaxCodeList(ArTaxCodeForm parentBean,
		Integer taxCode,
		String taxName,
		String description,
		String type,
		String rate,
		String taxAccount,
		String accountDescription,
		String interimAccount,
		String interimAccountDescription,
		boolean enable) {
	
		this.parentBean = parentBean;
		this.taxCode = taxCode;
		this.taxName = taxName;      
		this.description = description;
		this.type = type;
		this.rate = rate;
		this.taxAccount = taxAccount;
		this.accountDescription = accountDescription;
		this.interimAccount = interimAccount;
		this.interimAccountDescription = interimAccountDescription;
		this.enable = enable;
		
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getTaxCode() {
	
	    return taxCode;
	
	}
	
	public String getTaxName() {
	   
	    return taxName;
	   
	}
	
	public String getDescription() {
	
	    return description;
	
	}
	
	public String getType() {
		
		return type;
		
	}
	
	public String getRate() {
		
		return rate;
		 
	}
	
	public String getTaxAccount() {
	
	    return taxAccount;
	
	}
	
	public String getAccountDescription() {
	
	    return accountDescription;
	
	}
	
	public String getInterimAccount() {
		
	    return interimAccount;
	
	}
	
	public String getInterimAccountDescription() {
	
	    return interimAccountDescription;
	
	}
	
	public boolean getEnable() {
		
		return enable;
		  
	}

}