package com.struts.ar.uploadinvoiceitem;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.ar.uploadinvoiceitem.ArUploadInvoiceItemForm;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;

public class ArUploadInvoiceItemAction extends Action{

	  private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

	   public ActionForward execute(ActionMapping mapping,  ActionForm form,
	      HttpServletRequest request, HttpServletResponse response)
	      throws Exception {
		   
		  HttpSession session = request.getSession();
		      
		  try {
		    	  
			  User user = (User) session.getAttribute(Constants.USER_KEY);

		      if (user != null) {

		         if (log.isInfoEnabled()) {

		             log.info("ArUploadInvoiceItemEntry: Company '" + user.getCompany() + "' User '" + user.getUserName() +
		             "' performed this action on session " + session.getId());
		         }

		      } else {

		         if (log.isInfoEnabled()) {

		            log.info("User is not logged on in session" + session.getId());

		         }

		         return(mapping.findForward("adLogon"));

		      }
		         
		      ArUploadInvoiceItemForm actionForm = (ArUploadInvoiceItemForm)form;
		         
		      String frParam = Common.getUserPermission(user, Constants.AR_INVOICE_ENTRY_ID);

		      if (frParam != null) {

		    	  if (frParam.trim().equals(Constants.FULL_ACCESS)) {

			         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
		               if (!fieldErrors.isEmpty()) {

		                  saveErrors(request, new ActionMessages(fieldErrors));

		                  return mapping.findForward("arUploadInvoiceItemEntry");
		               }

		            }

		            actionForm.setUserPermission(frParam.trim());

		      } else {

		         actionForm.setUserPermission(Constants.NO_ACCESS);
		      }

/*******************************************************
	Initialize Controller EJB
*******************************************************/
			  
		      ActionErrors errors = new ActionErrors();
		    
/*******************************************************
	 Ar  Action 
 *******************************************************/		     			  
			  
		      
		      
		   System.out.println("test");
/*******************************************************
	 Ar Load Action --
*******************************************************/	    	
		 return(mapping.findForward("arUploadInvoiceItemEntry"));
		   /*
			  if (frParam != null) {
				  
				  
				  return(mapping.findForward("cmnMain"));
				  
			  }else{
				  
				  
				  errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
		            saveErrors(request, new ActionMessages(errors));

		            return(mapping.findForward("cmnMain"));
			  }
			  
			  */
			  
		  }catch(Exception e){
/*******************************************************
	System Failed: Forward to error page 
*******************************************************/

			   if (log.isInfoEnabled()) {

			       log.info("Exception caught in ArUploadInvoiceItemAction.execute(): " + e.getMessage()
			                + " session: " + session.getId());
			   }

			   return mapping.findForward("cmnErrorPage");

		  }
		   
		   
		   
		   
	   }
	
	
}
