package com.struts.ar.uploadinvoiceitem;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.struts.ar.taxcodes.ArTaxCodeList;
import com.struts.util.Constants;

public class ArUploadInvoiceItemForm extends ActionForm implements Serializable {

	
	
	private String userPermission = new String();
	private FormFile csvFile = null;



	private String txnStatus = new String();
	private String noOfValid = new String();
	private String noOfFailed = new String();
	private String noOfLines = new String();
	private boolean showUploadButton = false;
	private String noOfTransactions= new String();
	private ArrayList arTrnsctnLst = new ArrayList();
	
	
	public ArrayList getArTrnsctnLst() {
		return arTrnsctnLst;
	}

	public void setArTrnsctnLst(ArrayList arTrnsctnLst) {
		this.arTrnsctnLst = arTrnsctnLst;
	}

	public ArUploadInvoiceItemList getArItmLstIndex(int index) {
		
	    return((ArUploadInvoiceItemList)arTrnsctnLst.get(index));
	
	}
	
	public Object[] getArItmLst() {
	
	    return arTrnsctnLst.toArray();
	
	}
	
	public int getArItmLstSize() {
	
	    return arTrnsctnLst.size();
	
	}
	
	public void saveArItmLst(Object newArItmLst) {
	
		arTrnsctnLst.add(newArItmLst);
	
	}
	
	public void clearArTCList() {
		
		arTrnsctnLst.clear();
	  
	}
	
	public String getNoOfTransactions() {
		return noOfTransactions;
	}

	public void setNoOfTransactions(String noOfTransactions) {
		this.noOfTransactions = noOfTransactions;
	}

	public String getNoOfValid() {
		return noOfValid;
	}

	public void setNoOfValid(String noOfValid) {
		this.noOfValid = noOfValid;
	}

	public String getNoOfFailed() {
		return noOfFailed;
	}

	public void setNoOfFailed(String noOfFailed) {
		this.noOfFailed = noOfFailed;
	}
	
	public String getNoOfLines() {
		return noOfLines;
	}

	public void setNoOfLines(String noOfLines) {
		this.noOfLines = noOfLines;
	}


	public FormFile getCsvFile() {
		return csvFile;
	}

	public void setCsvFile(FormFile csvFile) {
		this.csvFile = csvFile;
	}
	

	public boolean isShowUploadButton() {
		return showUploadButton;
	}

	public void setShowUploadButton(boolean showUploadButton) {
		this.showUploadButton = showUploadButton;
	}


	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	
	public void setUserPermission(String userPermission) {
		
	  	this.userPermission = userPermission;
	
	}
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		
	}
	
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();	
		
		return errors;
	}
	
}
