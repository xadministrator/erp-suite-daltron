package com.struts.ar.uploadinvoiceitem;

import java.io.Serializable;

public class ArUploadInvoiceItemLines implements Serializable {

	private String control = null;
	private String itemCode = null;
	private String itemLocation = null;
	private double quantity = 0;
	private String unit = null;
	private double unitPrice = 0;
	private double amount = 0;
	
	
	public ArUploadInvoiceItemLines(String control, String itemCode,
			String itemLocation, double quantity, String unit,
			double unitPrice, double amount) {
		super();
		this.control = control;
		this.itemCode = itemCode;
		this.itemLocation = itemLocation;
		this.quantity = quantity;
		this.unit = unit;
		this.unitPrice = unitPrice;
		this.amount = amount;
	}


	public String getControl() {
		return control;
	}


	public void setControl(String control) {
		this.control = control;
	}


	public String getItemCode() {
		return itemCode;
	}


	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}


	public String getItemLocation() {
		return itemLocation;
	}


	public void setItemLocation(String itemLocation) {
		this.itemLocation = itemLocation;
	}


	public double getQuantity() {
		return quantity;
	}


	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public double getUnitPrice() {
		return unitPrice;
	}


	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}

}
