package com.struts.ar.uploadinvoiceitem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ArUploadInvoiceItemList implements Serializable  {

	private ArUploadInvoiceItemForm parentBean;
	
	private String control = null;
	private String customer = null;
	private String date = null;
	private String referenceNo = null;
	private String invoiceNo = null;
	private String clientPo = null;
	private String description = null;
	private String paymentTerm = null;
	private String effectiveDate = null;
	private String receiveDate = null;
	private String taxCode = null;
	private String withholdingTaxCode = null;
	private String currency = null;
	private String status = null;


	private int itemLinesCount = 0;
	private List<ArUploadInvoiceItemLines> itemLines = new ArrayList<ArUploadInvoiceItemLines>();
	
	
	public ArUploadInvoiceItemList(ArUploadInvoiceItemForm parentBean,
			String control, String customer, String date, String referenceNo, String invoiceNo,
			String clientPo, String description, String paymentTerm,
			String effectiveDate, String receiveDate, String taxCode,
			String withholdingTaxCode, String currency, String status,
			List<ArUploadInvoiceItemLines> itemLines) {
		super();
		this.parentBean = parentBean;
		this.control = control;
		this.customer = customer;
		this.date = date;
		this.referenceNo = referenceNo;
		this.invoiceNo = invoiceNo;
		this.clientPo = clientPo;
		this.description = description;
		this.paymentTerm = paymentTerm;
		this.effectiveDate = effectiveDate;
		this.receiveDate = receiveDate;
		this.taxCode = taxCode;
		this.withholdingTaxCode = withholdingTaxCode;
		this.currency = currency;
		this.status = status;
		this.itemLines = itemLines;
	}
	
	
	public ArUploadInvoiceItemForm getParentBean() {
		return parentBean;
	}
	public void setParentBean(ArUploadInvoiceItemForm parentBean) {
		this.parentBean = parentBean;
	}
	public String getControl() {
		return control;
	}


	public void setControl(String control) {
		this.control = control;
	}
	
	
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getClientPo() {
		return clientPo;
	}
	public void setClientPo(String clientPo) {
		this.clientPo = clientPo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPaymentTerm() {
		return paymentTerm;
	}
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getReceiveDate() {
		return receiveDate;
	}
	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}
	public String getTaxCode() {
		return taxCode;
	}
	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}
	public String getWithholdingTaxCode() {
		return withholdingTaxCode;
	}
	public void setWithholdingTaxCode(String withholdingTaxCode) {
		this.withholdingTaxCode = withholdingTaxCode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	public List<ArUploadInvoiceItemLines> getItemLines() {
		return itemLines;
	}
	public void setItemLines(ArUploadInvoiceItemLines itemLine) {
		itemLines.add(itemLine);
	}
	
	public int getItemLinesCount(){
		itemLinesCount = itemLines.size();	
		return itemLinesCount;
	}


	
	
}
