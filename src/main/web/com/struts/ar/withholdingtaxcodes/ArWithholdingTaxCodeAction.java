package com.struts.ar.withholdingtaxcodes;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.ArWithholdingTaxCodeController;
import com.ejb.txn.ArWithholdingTaxCodeControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ArModWithholdingTaxCodeDetails;
import com.util.ArWithholdingTaxCodeDetails;

public final class ArWithholdingTaxCodeAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("ArWithholdingTaxCodeAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         ArWithholdingTaxCodeForm actionForm = (ArWithholdingTaxCodeForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.AR_WITHHOLDING_TAX_CODE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("arWithholdingTaxCodes");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize ArWithholdingTaxCodeController EJB
*******************************************************/

         ArWithholdingTaxCodeControllerHome homeWTC = null;
         ArWithholdingTaxCodeController ejbWTC = null;

         try {

            homeWTC = (ArWithholdingTaxCodeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/ArWithholdingTaxCodeControllerEJB", ArWithholdingTaxCodeControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in ArWithholdingTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbWTC = homeWTC.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in ArWithholdingTaxCodeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ar WTC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("arWithholdingTaxCodes"));
	     
/*******************************************************
   -- Ar WTC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("arWithholdingTaxCodes"));                  
         
/*******************************************************
   -- Ar WTC Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            ArWithholdingTaxCodeDetails details = new ArWithholdingTaxCodeDetails();
            details.setWtcName(actionForm.getName());
            details.setWtcDescription(actionForm.getDescription());
            details.setWtcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setWtcEnable(Common.convertBooleanToByte(actionForm.getEnable()));            
            
            try {
            	
            	ejbWTC.addArWtcEntry(details, actionForm.getAccount(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("arWithholdingTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("arWithholdingTaxCode.error.accountNumberInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Ar WTC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar WTC Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            ArWithholdingTaxCodeList arWTCList =
	            actionForm.getArWTCByIndex(actionForm.getRowSelected());
            
            
            ArWithholdingTaxCodeDetails details = new ArWithholdingTaxCodeDetails();
            details.setWtcCode(arWTCList.getWithholdingTaxCode());
            details.setWtcName(actionForm.getName());
            details.setWtcDescription(actionForm.getDescription());
            details.setWtcRate(Common.convertStringMoneyToDouble(actionForm.getRate(), Constants.MONEY_RATE_PRECISION));
            details.setWtcEnable(Common.convertBooleanToByte(actionForm.getEnable()));
            
            try {
            	
            	ejbWTC.updateArWtcEntry(details, actionForm.getAccount(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("arWithholdingTaxCode.error.recordAlreadyExist"));

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("arWithholdingTaxCode.error.accountNumberInvalid"));
               
            } catch (GlobalRecordAlreadyAssignedException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("arWithholdingTaxCode.error.updateWithholdingTaxCodeAlreadyAssigned"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Ar WTC Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ar WTC Edit Action --
*******************************************************/

         } else if (request.getParameter("arWTCList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showArWTCRow(actionForm.getRowSelected());
            
            return mapping.findForward("arWithholdingTaxCodes");
            
/*******************************************************
   -- Ar WTC Delete Action --
*******************************************************/

         } else if (request.getParameter("arWTCList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            ArWithholdingTaxCodeList arWTCList =
              actionForm.getArWTCByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbWTC.deleteArWtcEntry(arWTCList.getWithholdingTaxCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("arWithholdingTaxCode.error.deleteWithholdingTaxCodeAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("arWithholdingTaxCode.error.withholdingTaxCodeAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ArWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Ar WTC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("arWithholdingTaxCodes");

            }
            
            ArrayList list = null;
            Iterator i = null;
                      	                        	                          	            	
	        try {
	    	
               actionForm.clearArWTCList();	        
	           
	           list = ejbWTC.getArWtcAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	              ArModWithholdingTaxCodeDetails mdetails = (ArModWithholdingTaxCodeDetails)i.next();
	            	
	              ArWithholdingTaxCodeList arWTCList = new ArWithholdingTaxCodeList(actionForm,
	            	    mdetails.getWtcCode(),
	            	    mdetails.getWtcName(),
	            	    mdetails.getWtcDescription(),
	            	    Common.convertDoubleToStringMoney(mdetails.getWtcRate(), Constants.MONEY_RATE_PRECISION),
                        mdetails.getWtcCoaGlWithholdingTaxAccountNumber(),
                        mdetails.getWtcCoaGlWithholdingTaxAccountDescription(),
                        Common.convertByteToBoolean(mdetails.getWtcEnable()));                            
	            	 	            	    
	              actionForm.saveArWTCList(arWTCList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArWithholdingTaxCodeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            actionForm.setEnable(true);
            
            return(mapping.findForward("arWithholdingTaxCodes"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in ArWithholdingTaxCodeAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}