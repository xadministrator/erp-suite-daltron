package com.struts.ar.withholdingtaxcodes;

import java.io.Serializable;

public class ArWithholdingTaxCodeList implements Serializable {

    private Integer withholdingTaxCode = null;
    private String name = null;
    private String description = null;
    private String rate = null;
    private String account = null;
    private String accountDescription = null;
    private boolean enable = false;

	private String editButton = null;
	private String deleteButton = null;

	private ArWithholdingTaxCodeForm parentBean;
    
	public ArWithholdingTaxCodeList(ArWithholdingTaxCodeForm parentBean,
		Integer withholdingTaxCode,
		String name,
		String description,
		String rate,
		String account,
		String accountDescription,
		boolean enable) {
	
		this.parentBean = parentBean;
		this.withholdingTaxCode = withholdingTaxCode;
		this.name = name;      
		this.description = description;
		this.rate = rate;
		this.account=account;
		this.accountDescription = accountDescription;
		this.enable = enable;
		
	}
	
	public void setEditButton(String editButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}
	
	public void setDeleteButton(String deleteButton) {
	
	    parentBean.setRowSelected(this, true);
	
	}   
	
	public Integer getWithholdingTaxCode() {
	
	    return withholdingTaxCode;
	
	}
	
	public String getName() {
	   
	    return name;
	   
	}
	
	public String getDescription() {
	
	    return description;
	
	}
	
	public String getRate() {
		
		return rate;
		 
	}
	
	public String getAccount() {
	
	    return account;
	
	}
	
	public String getAccountDescription() {
	
	    return accountDescription;
	
	}
	
	public boolean getEnable() {
		
		return enable;
		  
	}

}