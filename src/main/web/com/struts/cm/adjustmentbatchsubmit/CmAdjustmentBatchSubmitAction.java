package com.struts.cm.adjustmentbatchsubmit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.CmAdjustmentBatchSubmitController;
import com.ejb.txn.CmAdjustmentBatchSubmitControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.CmModAdjustmentDetails;

public final class CmAdjustmentBatchSubmitAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());  	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmAdjustmentBatchSubmitAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         CmAdjustmentBatchSubmitForm actionForm = (CmAdjustmentBatchSubmitForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.CM_ADJUSTMENT_POST_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmAdjustmentBatchSubmit");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize CmAdjustmentBatchSubmitController EJB
*******************************************************/

         CmAdjustmentBatchSubmitControllerHome homeABS = null;
         CmAdjustmentBatchSubmitController ejbABS = null;

         try {

            homeABS = (CmAdjustmentBatchSubmitControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmAdjustmentBatchSubmitControllerEJB", CmAdjustmentBatchSubmitControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmAdjustmentBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbABS = homeABS.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmAdjustmentBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbABS.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in CmAdjustmentBatchSubmitAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Cm ABS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("cmAdjustmentBatchSubmit"));
	     
/*******************************************************
   -- Cm ABS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("cmAdjustmentBatchSubmit"));                         

/*******************************************************
   -- Cm ABS Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Cm ABS Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 

/*******************************************************
   -- Cm ABS Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	
	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        		
	        	}
	        	
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}  
	        		        		        	
	        	if (!Common.validateRequired(actionForm.getAdjustmentType())) {
	        		
	        		criteria.put("adjustmentType", actionForm.getAdjustmentType());
	        		
	        	}	      	

	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}

	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbABS.getCmAdjByCriteria(actionForm.getCriteria(),                	    
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	    	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in CmAdjustmentBatchSubmitAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	        	
	     	}
 
            try {
            	
            	actionForm.clearCmABSList();
            	
            	ArrayList list = ejbABS.getCmAdjByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		CmModAdjustmentDetails mdetails = (CmModAdjustmentDetails)i.next();
      		
            		CmAdjustmentBatchSubmitList cmABSList = new CmAdjustmentBatchSubmitList(actionForm,
            		    mdetails.getAdjCode(),
            		    Common.convertSQLDateToString(mdetails.getAdjDate()),
            		    mdetails.getAdjReferenceNumber(),
            		    mdetails.getAdjBaName(),
            		    mdetails.getAdjType(),
            		    Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit));
            		    
            		actionForm.saveCmABSList(cmABSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("adjustmentBatchSubmit.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmAdjustmentBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmAdjustmentBatchSubmit");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("cmAdjustmentBatchSubmit"));

/*******************************************************
   -- Cm ABS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Cm ABS Submit Action --
*******************************************************/

         } else if (request.getParameter("submitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get submit receipts
                        
		    for(int i=0; i<actionForm.getCmABSListSize(); i++) {
		    
		       CmAdjustmentBatchSubmitList actionList = actionForm.getCmABSByIndex(i);
		    	
               if (actionList.getSubmit()) {
               	
               	     try {
               	     	
               	     	ejbABS.executeArAdjBatchSubmit(actionList.getAdjustmentCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteCmABSList(i);
               	     	i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("adjustmentBatchSubmit.error.recordAlreadyDeleted", actionList.getReferenceNumber()));
		             
		             } catch (GlobalTransactionAlreadyApprovedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("adjustmentBatchSubmit.error.transactionAlreadyApproved", actionList.getReferenceNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPendingException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("adjustmentBatchSubmit.error.transactionAlreadyPending", actionList.getReferenceNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("adjustmentBatchSubmit.error.transactionAlreadyPosted", actionList.getReferenceNumber()));
		             
		             } catch (GlobalNoApprovalRequesterFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("adjustmentBatchSubmit.error.noApprovalRequesterFound", actionList.getReferenceNumber()));
		                  
		             } catch (GlobalNoApprovalApproverFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("adjustmentBatchSubmit.error.noApprovalApproverFound", actionList.getReferenceNumber()));
		                  
		             } catch (GlobalTransactionAlreadyVoidException ex) {
           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("adjustmentBatchSubmit.error.transactionAlreadyVoid", actionList.getReferenceNumber()));
		               
		             } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("adjustmentBatchSubmit.error.effectiveDateNoPeriodExist", actionList.getReferenceNumber()));
		                    
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("adjustmentBatchSubmit.error.effectiveDatePeriodClosed", actionList.getReferenceNumber()));
		                    
		             } catch (GlobalJournalNotBalanceException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("adjustmentBatchSubmit.error.journalNotBalance", actionList.getReferenceNumber()));  
		
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ArAdjustmentBatchSubmitAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }		  
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmAdjustmentBatchSubmit");

           }
	        
	        
	        try {
	        
	            actionForm.setLineCount(0);
            	
            	actionForm.clearCmABSList();
            	
            	ArrayList list = ejbABS.getCmAdjByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		CmModAdjustmentDetails mdetails = (CmModAdjustmentDetails)i.next();
      		
            		CmAdjustmentBatchSubmitList cmABSList = new CmAdjustmentBatchSubmitList(actionForm,
            		    mdetails.getAdjCode(),
            		    Common.convertSQLDateToString(mdetails.getAdjDate()),
            		    mdetails.getAdjReferenceNumber(),
            		    mdetails.getAdjBaName(),
            		    mdetails.getAdjType(),
            		    Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit));
            		    
            		actionForm.saveCmABSList(cmABSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmAdjustmentBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
           
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("cmAdjustmentBatchSubmit"));

	           


/*******************************************************
   -- Cm ABS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmAdjustmentBatchSubmit");

            }            
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	actionForm.clearCurrencyList();
            	
            	list = ejbABS.getGlFcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList((String)i.next());
            			
            		}
            		
            	}            	

            	actionForm.clearBankAccountList();
            	
            	list = ejbABS.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	
            	actionForm.clearCmABSList();
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmAdjustmentBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            
			actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
                        
            return(mapping.findForward("cmAdjustmentBatchSubmit"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in CmAdjustmentBatchSubmitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}