package com.struts.cm.adjustmentbatchsubmit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmAdjustmentBatchSubmitForm extends ActionForm implements Serializable {

   private String adjustmentType = null;
   private ArrayList adjustmentTypeList = new ArrayList();
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String referenceNumber = null;
   private String dateFrom = null;
   private String dateTo = null;   
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String pageState = new String();
   private ArrayList cmABSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public CmAdjustmentBatchSubmitList getCmABSByIndex(int index) {

      return((CmAdjustmentBatchSubmitList)cmABSList.get(index));

   }

   public Object[] getCmABSList() {

      return cmABSList.toArray();

   }

   public int getCmABSListSize() {

      return cmABSList.size();

   }

   public void saveCmABSList(Object newCmABSList) {

      cmABSList.add(newCmABSList);

   }
   
   public void deleteCmABSList(int rowSelected) {

      cmABSList.remove(rowSelected);

   }

   public void clearCmABSList() {
   	
      cmABSList.clear();
      
   }

   public void setRowSelected(Object selectedCmABSList, boolean isEdit) {

      this.rowSelected = cmABSList.indexOf(selectedCmABSList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }   

   public String getAdjustmentType() {

      return adjustmentType;

   }

   public void setAdjustmentType(String adjustmentType) {

      this.adjustmentType = adjustmentType;

   }

   public ArrayList getAdjustmentTypeList() {

      return adjustmentTypeList;

   }

   public String getBankAccount() {

      return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

      this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

      return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

      bankAccountList.add(bankAccount);

   }
   
   public void clearBankAccountList() {

      bankAccountList.clear();
      bankAccountList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getReferenceNumber() {

      return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

      this.referenceNumber = referenceNumber;

   }

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
                         
   public String getCurrency() {
   	
   	  return currency;
   	  
   }
   
   public void setCurrency(String currency) {
   	
   	  this.currency = currency;
   	  
   }
   
   public ArrayList getCurrencyList() {
   	
   	  return currencyList;
   
   }
   
   public void setCurrencyList(String currency) {
   	
   	  currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   	     	
   }
       
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  adjustmentTypeList.clear();
      adjustmentTypeList.add(Constants.GLOBAL_BLANK);
      adjustmentTypeList.add("ADVANCE");
      adjustmentTypeList.add("INTEREST");
      adjustmentTypeList.add("BANK CHARGE");
      adjustmentTypeList.add("DEBIT MEMO");
      adjustmentTypeList.add("CREDIT MEMO");
      adjustmentType = Constants.GLOBAL_BLANK;  
   	  bankAccount = Constants.GLOBAL_BLANK;
   	  currency = Constants.GLOBAL_BLANK;
   	  dateFrom = null;
   	  dateTo = null;
   	  referenceNumber = null;  
   	  
   	  for (int i=0; i<cmABSList.size(); i++) {
	  	
      	CmAdjustmentBatchSubmitList actionList = (CmAdjustmentBatchSubmitList)cmABSList.get(i);
      	
      		actionList.setSubmit(false);
	  	
      }
             
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("BANK ACCOUNT");
	      orderByList.add("TYPE");
	      orderByList.add("REFERENCE NUMBER");
	      orderBy = "TYPE";   
	  
	  }     
	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("adjustmentBatchSubmit.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("adjustmentBatchSubmit.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("adjustmentBatchSubmit.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("adjustmentBatchSubmit.error.maxRowsInvalid"));
		
		   }
	 	  
      }
      
      return errors;

   }
}