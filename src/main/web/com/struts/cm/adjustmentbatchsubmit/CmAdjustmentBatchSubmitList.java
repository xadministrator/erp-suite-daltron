package com.struts.cm.adjustmentbatchsubmit;

import java.io.Serializable;

public class CmAdjustmentBatchSubmitList implements Serializable {

   private Integer adjustmentCode = null;
   private String date = null;
   private String referenceNumber = null;
   private String bankAccount = null;
   private String adjustmentType = null;
   private String amount = null;

   private boolean submit = false;
       
   private CmAdjustmentBatchSubmitForm parentBean;
    
   public CmAdjustmentBatchSubmitList(CmAdjustmentBatchSubmitForm parentBean,
	  Integer adjustmentCode,
	  String date,
	  String referenceNumber,
	  String bankAccount,
	  String adjustmentType,
	  String amount) {

      this.parentBean = parentBean;
      this.adjustmentCode = adjustmentCode;
      this.date = date;
      this.referenceNumber = referenceNumber;
      this.bankAccount = bankAccount;
      this.adjustmentType = adjustmentType;
      this.amount = amount;
      
   }
   
   public Integer getAdjustmentCode() {
   	
   	  return adjustmentCode;
   	  
   }
   
   public String getDate() {
   	
   	  return date;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getBankAccount() {
   	
   	  return bankAccount;
   	  
   }
   
   public String getAdjustmentType() {
   	
   	  return adjustmentType;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }

   public boolean getSubmit() {
   	
   	  return submit;
   	
   }
   
   public void setSubmit(boolean submit) {
   	
   	  this.submit = submit;
   	
   }
      
}