package com.struts.cm.adjustmententry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApDirectCheckEntryController;
import com.ejb.txn.ApDirectCheckEntryControllerHome;
import com.ejb.txn.ArFindInvoiceController;
import com.ejb.txn.ArFindInvoiceControllerHome;
import com.ejb.txn.CmAdjustmentEntryController;
import com.ejb.txn.CmAdjustmentEntryControllerHome;
import com.struts.ap.directcheckentry.ApDirectCheckEntryForm;
import com.struts.ap.directcheckentry.ApDirectCheckEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModBankAccountDetails;
import com.util.ApCheckDetails;
import com.util.ApModDistributionRecordDetails;
import com.util.ArModSalesOrderDetails;
import com.util.CmAdjustmentDetails;
import com.util.CmModAdjustmentDetails;

public final class CmAdjustmentEntryAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmAdjustmentEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         CmAdjustmentEntryForm actionForm = (CmAdjustmentEntryForm)form;


         System.out.println("<11----" + request.getParameter("adjustmentCode") + " --->");
        actionForm.setReport(null);

         String ceParam = null;

         if (request.getParameter("child") == null) {

         	ceParam = Common.getUserPermission(user, Constants.CM_ADJUSTMENT_ENTRY_ID);

         } else {

         	ceParam = Constants.FULL_ACCESS;

         }

         if (ceParam != null) {

	      if (ceParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmAdjustmentEntry");
               }

            }

            actionForm.setUserPermission(ceParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize CmAdjustmentEntryController EJB
*******************************************************/

         CmAdjustmentEntryControllerHome homeAE = null;
         CmAdjustmentEntryController ejbAE = null;

         //ArFindInvoiceControllerHome homeFI = null;
         //ArFindInvoiceController ejbFI = null;

         ApDirectCheckEntryControllerHome homeDC = null;
         ApDirectCheckEntryController ejbDC = null;

         try {

            homeAE = (CmAdjustmentEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmAdjustmentEntryControllerEJB", CmAdjustmentEntryControllerHome.class);

           /* homeFI = (ArFindInvoiceControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ArFindInvoiceControllerEJB", ArFindInvoiceControllerHome.class);
           */
            homeDC = (ApDirectCheckEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ApDirectCheckEntryControllerEJB", ApDirectCheckEntryControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmAdjustmentEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAE = homeAE.create();

            //ejbFI = homeFI.create();

            ejbDC = homeDC.create();

         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmAdjustmentEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call CmAdjustmentEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/
         boolean isInitialPrinting = false;
         short precisionUnit = 0;

         try {

            precisionUnit = ejbAE.getGlFcPrecisionUnit(user.getCmpCode());

         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }



/*******************************************************
   -- Cm AE Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            CmAdjustmentDetails details = new CmAdjustmentDetails();

            details.setAdjCode(actionForm.getAdjustmentCode());
            details.setAdjType(actionForm.getType());
            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
            details.setAdjAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
            details.setAdjConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
            details.setAdjConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit));
            details.setAdjMemo(actionForm.getMemo());
            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
            details.setAdjRefund(Common.convertBooleanToByte(actionForm.getAdjustmentRefund()));
            if (actionForm.getAdjustmentCode() == null) {

           	   details.setAdjCreatedBy(user.getUserName());
	           details.setAdjDateCreated(new java.util.Date());

           }

           details.setAdjLastModifiedBy(user.getUserName());
           details.setAdjDateLastModified(new java.util.Date());


            try {

            	System.out.println(actionForm.getBankAccount() + "b acc action");
                ejbAE.saveCmAdjEntry(details, actionForm.getBankAccount(), actionForm.getCustomer(), actionForm.getSoNumber(),
                   actionForm.getCurrency(), true, null, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            } catch (GlobalDocumentNumberNotUniqueException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            		new ActionMessage("adjustmentEntry.error.documentNumberNotUnique"));

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.recordAlreadyDeleted"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.branchAccountNumberInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

/*******************************************************
   -- Cm AE Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           	CmAdjustmentDetails details = new CmAdjustmentDetails();

            details.setAdjCode(actionForm.getAdjustmentCode());
            details.setAdjType(actionForm.getType());
            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
            details.setAdjReferenceNumber(actionForm.getReferenceNumber());

            details.setAdjAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
            details.setAdjConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
            details.setAdjConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit));
            details.setAdjMemo(actionForm.getMemo());
            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
            details.setAdjRefund(Common.convertBooleanToByte(actionForm.getAdjustmentRefund()));
            if (actionForm.getAdjustmentCode() == null) {

           	   details.setAdjCreatedBy(user.getUserName());
	           details.setAdjDateCreated(new java.util.Date());

           }

           details.setAdjLastModifiedBy(user.getUserName());
           details.setAdjDateLastModified(new java.util.Date());



            try {

               Integer adjustment = ejbAE.saveCmAdjEntry(details, actionForm.getBankAccount(), actionForm.getCustomer(), actionForm.getSoNumber(),
                   actionForm.getCurrency(), false, null, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

               actionForm.setAdjustmentCode(adjustment);

            } catch (GlobalDocumentNumberNotUniqueException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            		new ActionMessage("adjustmentEntry.error.documentNumberNotUnique"));

            } catch (GlobalRecordAlreadyAssignedException ex) {

				errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("adjustmentEntry.error.recordAlreadyAssigned"));


            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.recordAlreadyDeleted"));

           } catch (GlobalConversionDateNotExistException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.conversionDateNotExist"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPosted"));

           } catch (GlobalTransactionAlreadyVoidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.transactionAlreadyVoid"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.noApprovalApproverFound"));

           } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.journalNotBalance"));

           } catch (GlobalBranchAccountNumberInvalidException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.branchAccountNumberInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

/*******************************************************
    -- Cm AE Close Action --
 *******************************************************/

         } else if (request.getParameter("refundButton") != null &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


        	 ApCheckDetails details = new ApCheckDetails();

             details.setChkCode(null);
             details.setChkDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setChkCheckDate(Common.convertStringToSQLDate(actionForm.getDate()));
             details.setChkNumber("");
             details.setChkDocumentNumber("");

             details.setChkReferenceNumber(actionForm.getDocumentNumber());
             details.setChkBillAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit) - Common.convertStringMoneyToDouble(actionForm.getAmountApplied(), precisionUnit));
             details.setChkAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit) - Common.convertStringMoneyToDouble(actionForm.getAmountApplied(), precisionUnit));
             details.setChkCrossCheck(Common.convertBooleanToByte(false));
             details.setChkVoid(Common.convertBooleanToByte(false));
             details.setChkDescription("REFUND FROM ADVANCE: "+ actionForm.getDocumentNumber());
             details.setChkConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
             details.setChkConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION));


             details.setChkCreatedBy(user.getUserName());
  	         details.setChkDateCreated(new java.util.Date());

             details.setChkLastModifiedBy(user.getUserName());
             details.setChkDateLastModified(new java.util.Date());
             details.setChkMemo(actionForm.getMemo());
             details.setChkSupplierName(actionForm.getCustomerName());


             ArrayList list = ejbDC.getApDrBySplSupplierCodeAndTcNameAndWtcNameAndChkBillAmountAndBaName("Customer Refund",
                     "NONE", "NONE",
                     Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit) - Common.convertStringMoneyToDouble(actionForm.getAmountApplied(), precisionUnit),
                     actionForm.getBankAccount(),new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());



             ArrayList drList = new ArrayList();
             int lineNumber = 0;

            Iterator i = list.iterator();

	         while (i.hasNext()) {

	        	 ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)i.next();

	        	 ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();

	           	   mdetails.setDrLine((short)(lineNumber++));
	           	   mdetails.setDrClass(mDrDetails.getDrClass());
	           	   mdetails.setDrDebit(mDrDetails.getDrDebit());
	           	   mdetails.setDrAmount(mDrDetails.getDrAmount());
	           	   mdetails.setDrCoaAccountNumber(mDrDetails.getDrCoaAccountNumber());

	           	   drList.add(mdetails);


	         }


             try {
             	System.out.println("saveApChkEntry----------->");
             	Integer checkCode = ejbDC.saveApChkEntry(details, null, actionForm.getBankAccount(),
             	        "NONE", "NONE",
             	        actionForm.getCurrency(), "Customer Refund", "",
             	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


          	 ejbAE.saveCmAdjRefundDetailsByAdjCode(details, checkCode , actionForm.getAdjustmentCode(),
          			 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

          	  String path = "/apDirectCheckEntry.do?forward=1" +
     			     "&checkCode=" + checkCode;

     			   return(new ActionForward(path));

             } catch (GlobalRecordAlreadyDeletedException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));

             } catch (GlobalDocumentNumberNotUniqueException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

             } catch (ApCHKCheckNumberNotUniqueException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));

             } catch (GlobalConversionDateNotExistException ex) {

             	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.conversionDateNotExist"));

             } catch (GlobalTransactionAlreadyApprovedException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));

             } catch (GlobalTransactionAlreadyPendingException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));

             } catch (GlobalTransactionAlreadyPostedException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));

             } catch (GlobalTransactionAlreadyVoidException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));

             } catch (GlobalNoApprovalRequesterFoundException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));

             } catch (GlobalNoApprovalApproverFoundException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));

             } catch (GlJREffectiveDateNoPeriodExistException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));

             } catch (GlJREffectiveDatePeriodClosedException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));

             } catch (GlobalJournalNotBalanceException ex) {

             	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("directCheckEntry.error.journalNotBalance"));

             } catch (GlobalBranchAccountNumberInvalidException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

             } catch (EJBException ex) {
             	    if (log.isInfoEnabled()) {

                    log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
                       " session: " + session.getId());
                  }

                 return(mapping.findForward("cmnErrorPage"));
             }




/*******************************************************
   -- Cm AE Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar RCT Print Action --
 *******************************************************/


        } else if (request.getParameter("printButton") != null) {

        	System.out.println("Entering print button");
        	if(Common.validateRequired(actionForm.getApprovalStatus())) {
        		CmAdjustmentDetails details = new CmAdjustmentDetails();
        	 	System.out.println("Entering print button  1");
                details.setAdjCode(actionForm.getAdjustmentCode());
                details.setAdjType(actionForm.getType());
                details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
                details.setAdjDocumentNumber(actionForm.getDocumentNumber());
                details.setAdjReferenceNumber(actionForm.getReferenceNumber());
                details.setAdjAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
                details.setAdjConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
                details.setAdjConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit));
                details.setAdjMemo(actionForm.getMemo());
                details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
                details.setAdjRefund(Common.convertBooleanToByte(actionForm.getAdjustmentRefund()));
                if (actionForm.getAdjustmentCode() == null) {

               	   details.setAdjCreatedBy(user.getUserName());
    	           details.setAdjDateCreated(new java.util.Date());

               }

               details.setAdjLastModifiedBy(user.getUserName());
               details.setAdjDateLastModified(new java.util.Date());


                try {

                	System.out.println(actionForm.getBankAccount() + "b acc action");
                	Integer adjustmentCode = ejbAE.saveCmAdjEntry(details, actionForm.getBankAccount(), actionForm.getCustomer(), actionForm.getSoNumber(),
                       actionForm.getCurrency(), true, null, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


                	System.out.println(adjustmentCode + "  ------------>");
                    actionForm.setAdjustmentCode(adjustmentCode);

                } catch (GlobalDocumentNumberNotUniqueException ex) {

                	errors.add(ActionMessages.GLOBAL_MESSAGE,
                		new ActionMessage("adjustmentEntry.error.documentNumberNotUnique"));

                } catch (GlobalRecordAlreadyDeletedException ex) {

               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.recordAlreadyDeleted"));

               } catch (GlobalConversionDateNotExistException ex) {

               	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.conversionDateNotExist"));

               } catch (GlobalTransactionAlreadyApprovedException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.transactionAlreadyApproved"));

               } catch (GlobalTransactionAlreadyPendingException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.transactionAlreadyPending"));

               } catch (GlobalTransactionAlreadyPostedException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.transactionAlreadyPosted"));

               } catch (GlobalTransactionAlreadyVoidException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.transactionAlreadyVoid"));

               } catch (GlobalNoApprovalRequesterFoundException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.noApprovalRequesterFound"));

               } catch (GlobalNoApprovalApproverFoundException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.noApprovalApproverFound"));

               } catch (GlJREffectiveDateNoPeriodExistException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.effectiveDateNoPeriodExist"));

               } catch (GlJREffectiveDatePeriodClosedException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.effectiveDatePeriodClosed"));

               } catch (GlobalJournalNotBalanceException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.journalNotBalance"));

               } catch (GlobalBranchAccountNumberInvalidException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("adjustmentEntry.error.branchAccountNumberInvalid"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage");

                   }

                }


                if (!errors.isEmpty()) {

					saveErrors(request, new ActionMessages(errors));
					return(mapping.findForward("cmAdjustmentEntry"));

				}

				actionForm.setReport(Constants.STATUS_SUCCESS);

				isInitialPrinting = true;

	        }  else {

				actionForm.setReport(Constants.STATUS_SUCCESS);

				return(mapping.findForward("cmAdjustmentEntry"));

			}

/*******************************************************
   -- Cm FTE Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null) {


             if (Common.validateRequired(actionForm.getApprovalStatus())) {

                CmAdjustmentDetails details = new CmAdjustmentDetails();

	            details.setAdjCode(actionForm.getAdjustmentCode());
	            details.setAdjType(actionForm.getType());
	            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
	            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
	            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
	            details.setAdjAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));
	            details.setAdjConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
	            details.setAdjConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit));
	            details.setAdjMemo(actionForm.getMemo());
	            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
	            details.setAdjRefund(Common.convertBooleanToByte(actionForm.getAdjustmentRefund()));

	            if (actionForm.getAdjustmentCode() == null) {

	           	   details.setAdjCreatedBy(user.getUserName());
		           details.setAdjDateCreated(new java.util.Date());

	           }

	           details.setAdjLastModifiedBy(user.getUserName());
	           details.setAdjDateLastModified(new java.util.Date());


	            try {

	               Integer adjustment = ejbAE.saveCmAdjEntry(details, actionForm.getBankAccount(), actionForm.getCustomer(), actionForm.getSoNumber(),
	                   actionForm.getCurrency(), true, null, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	               actionForm.setAdjustmentCode(adjustment);

	            } catch (GlobalDocumentNumberNotUniqueException ex) {

	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	            		new ActionMessage("adjustmentEntry.error.documentNumberNotUnique"));

	            } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.conversionDateNotExist"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.noApprovalApproverFound"));

	           } catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.journalNotBalance"));

	           } catch (GlobalBranchAccountNumberInvalidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.branchAccountNumberInvalid"));

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }


	            if (!errors.isEmpty()) {

                   saveErrors(request, new ActionMessages(errors));
                   return mapping.findForward("cmAdjustmentEntry");

                }

             }

  	         String path = "/cmJournal.do?forward=1" +
			     "&adjustmentCode=" + actionForm.getAdjustmentCode() +
			     "&transaction=BANK ADJUSTMENTS" +
			     "&referenceNumber=" + actionForm.getReferenceNumber() +
			     "&date=" + actionForm.getDate() +
			     "&enableFields=" + actionForm.getEnableFields();

			   return(new ActionForward(path));

/*******************************************************
   -- Cm AE Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbAE.deleteCmAdjEntry(actionForm.getAdjustmentCode(), user.getUserName(), user.getCmpCode());

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("adjustmentEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());

                }

                return(mapping.findForward("cmnErrorPage"));

            }
/*******************************************************
    -- Cm AE Sales Order Enter Action --
 *******************************************************/

          } else if (request.getParameter("isSalesOrderEntered") != null &&
             actionForm.getType().equals(Constants.CM_AR_SO_ADVANCE) &&
             actionForm.getSoNumber() != null &&
             actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
             	System.out.println("is sales order entered");
              try {


                 if(!actionForm.getSoNumber().equals("")) {




                	 ArModSalesOrderDetails details = new ArModSalesOrderDetails();

                	 details = ejbAE.getCmSoBySoNumber(actionForm.getSoNumber(), user.getCurrentBranch().getBrCode(), user.getCmpCode());
                	 String soNumber =details.getSoDocumentNumber();
                	 if(Common.convertByteToBoolean(details.getSoVoid())) {

                		 soNumber = "";

                		 errors.add(ActionMessages.GLOBAL_MESSAGE,
                       			new ActionMessage("adjustmentEntry.error.salesOrderVoid"));
                	 }

                	 /*
                	 if(Common.convertByteToBoolean(details.getSoLock()) || Common.convertByteToBoolean(details.getSoBoLock())) {
                		 soNumber = "";

                		 errors.add(ActionMessages.GLOBAL_MESSAGE,
                       			new ActionMessage("adjustmentEntry.error.salesOrderVoid"));
                	 }
                	 */

                	 if(!soNumber.equals("")) {
                		 actionForm.setReferenceNumber(details.getSoReferenceNumber());
                	 }

                 }







              } catch (EJBException ex) {

              	if (log.isInfoEnabled()) {

              		log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
              				" session: " + session.getId());
              		return mapping.findForward("cmnErrorPage");

              	}

              }

              if (!errors.isEmpty()) {

              	saveErrors(request, new ActionMessages(errors));

              }

              return(mapping.findForward("cmAdjustmentEntry"));

/*******************************************************
   -- Cm AE Bank Account Enter Action --
*******************************************************/

         } else if (request.getParameter("isBankAccountEntered") != null &&
            request.getParameter("isBankAccountEntered").equals("true") &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	System.out.println("is bank account entered");
             try {

            	 if(actionForm.getType().equals("ADVANCE")){

            		 actionForm.setShowCustomerList(true);

            	 } else if(actionForm.getType().equals("SUPPLIER ADVANCE")){

            		 actionForm.setShowSupplierList(true);

            	 } else if(actionForm.getType().equals("SO ADVANCE")){

            		 actionForm.setShowSupplierList(false);
            		 actionForm.setShowCustomerList(false);
            		 actionForm.setSoNumber(null);
            		 actionForm.setReferenceNumber(null);

            	 }else{

            		 actionForm.setShowSupplierList(false);
            		 actionForm.setShowCustomerList(false);

            	 }


             	 if (!actionForm.getBankAccount().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

	             	 AdModBankAccountDetails mfdetails = ejbAE.getAdBaByBaName(actionForm.getBankAccount(), user.getCmpCode());

	                 actionForm.setCurrency(mfdetails.getBaFcName());

	                 actionForm.setIsBankAccountEntered(false);

	                 if(!Common.validateRequired(actionForm.getConversionDate()))

	                 	actionForm.setConversionRate(Common.convertDoubleToStringMoney(
                 			ejbAE.getFrRateByFrNameAndFrDate(actionForm.getCurrency(), Common.convertStringToSQLDate(
            				actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));



             	 }
             } catch (GlobalConversionDateNotExistException ex) {

             	errors.add(ActionMessages.GLOBAL_MESSAGE,
             			new ActionMessage("adjustmentEntry.error.conversionDateNotExist"));

             } catch (EJBException ex) {

             	if (log.isInfoEnabled()) {

             		log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
             				" session: " + session.getId());
             		return mapping.findForward("cmnErrorPage");

             	}

             }

             if (!errors.isEmpty()) {

             	saveErrors(request, new ActionMessages(errors));

             }

             return(mapping.findForward("cmAdjustmentEntry"));



/*******************************************************
    -- Cm AE Type Enter Action --
*******************************************************/

         } else if (request.getParameter("isTypeEntered") != null &&
                      request.getParameter("isTypeEntered").equals("true") &&
                      actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {



        	 if(actionForm.getType().equals("ADVANCE")){

        		 actionForm.setShowCustomerList(true);
        		 actionForm.setShowSupplierList(false);

        	 } else if(actionForm.getType().equals("SUPPLIER ADVANCE")){

        		 actionForm.setShowSupplierList(true);
        		 actionForm.setShowCustomerList(false);

        	 } else if(actionForm.getType().equals("SO ADVANCE")){

        		 actionForm.setSoNumber(null);
        		 actionForm.setReferenceNumber(null);
        		 actionForm.setShowSupplierList(false);
        		 actionForm.setShowCustomerList(false);

        	 }else{

        		 actionForm.setShowSupplierList(false);
        		 actionForm.setShowCustomerList(false);

        	 }

        	 return(mapping.findForward("cmAdjustmentEntry"));
/*******************************************************
   -- Cm AE Conversion Date Enter Action --
*******************************************************/
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

         	try {

         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
         				ejbAE.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
         						Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));

         	} catch (GlobalConversionDateNotExistException ex) {

         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("adjustmentEntry.error.conversionDateNotExist"));

         	} catch (EJBException ex) {

         		if (log.isInfoEnabled()) {

         			log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());

         		}

         		return(mapping.findForward("cmnErrorPage"));

         	}

         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));

         	}

         	return(mapping.findForward("cmAdjustmentEntry"));

/*******************************************************
   -- Cm FTE Load Action --
*******************************************************/

         }

         if (ceParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmAdjustmentEntry");

            }


            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }


            actionForm.reset(mapping, request);

            ArrayList list = null;
            Iterator i = null;

            try {


            	actionForm.clearBankAccountList();

            	list = ejbAE.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setBankAccountList((String)i.next());

            		}

            	}



            	actionForm.clearCustomerList();

        		list = ejbAE.getCmCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


        		System.out.println("size cst = " + list.size() + " --------------");
        		if (list == null || list.size() == 0) {

        			actionForm.setCustomerList(Constants.GLOBAL_NO_RECORD_FOUND);

        		} else {

        			i = list.iterator();

        			while (i.hasNext()) {

        				actionForm.setCustomerList((String)i.next());

        			}

        		}



        		actionForm.clearSupplierList();

        		list = ejbAE.getCmSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


        		System.out.println("size cst = " + list.size() + " --------------");
        		if (list == null || list.size() == 0) {

        			actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

        		} else {

        			i = list.iterator();

        			while (i.hasNext()) {

        				actionForm.setSupplierList((String)i.next());

        			}

        		}





            	if (request.getParameter("forward") != null || isInitialPrinting) {

            		System.out.println("<----" + request.getParameter("adjustmentCode") + " --->");

            		CmModAdjustmentDetails aeDetails = null;
            		if(request.getParameter("adjustmentCode")!=null){
            			 aeDetails = ejbAE.getCmAdjByAdjCode(
                    		    new Integer(request.getParameter("adjustmentCode")), user.getCmpCode());

            		}else{
            			 aeDetails = ejbAE.getCmAdjByAdjCode(
                    		    actionForm.getAdjustmentCode(), user.getCmpCode());

            		}

		            actionForm.setType(aeDetails.getAdjType());

		            if(aeDetails.getAdjType().equals(Constants.CM_AE_ADVANCE)){
		            	 actionForm.setShowCustomerList(true);

		            	 if (!actionForm.getCustomerList().contains(aeDetails.getAdjCustomerCode())) {

	                         if (actionForm.getCustomerList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

	                             actionForm.clearCustomerList();

	                         }
	                         actionForm.setCustomerList(aeDetails.getAdjCustomerCode());

	                     }
	                     actionForm.setCustomer(aeDetails.getAdjCustomerCode());
	                     actionForm.setCustomerName(aeDetails.getAdjCustomerName());

		            } else if(aeDetails.getAdjType().equals(Constants.CM_AE_SPL_ADVANCE)){

		            	actionForm.setShowSupplierList(true);

		            	 if (!actionForm.getSupplierList().contains(aeDetails.getAdjSupplierCode())) {

	                         if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

	                             actionForm.clearSupplierList();

	                         }
	                         actionForm.setSupplierList(aeDetails.getAdjCustomerCode());

	                     }
	                     actionForm.setSupplier(aeDetails.getAdjSupplierCode());
	                     actionForm.setCustomerName(aeDetails.getAdjCustomerName());



		            } else if(aeDetails.getAdjType().equals(Constants.CM_AR_SO_ADVANCE)){

		            	actionForm.setShowSupplierList(false);
		            	 actionForm.setShowCustomerList(false);

		            	actionForm.setSoNumber(aeDetails.getAdjSoNumber());


		            } else {
		            	actionForm.setShowSupplierList(false);
		            	 actionForm.setShowCustomerList(false);

		            }


		        	if (request.getParameter("forward") != null) {

						actionForm.setAdjustmentCode(new Integer(request.getParameter("adjustmentCode")));

					}
		            actionForm.setDate(Common.convertSQLDateToString(aeDetails.getAdjDate()));
		            actionForm.setDocumentNumber(aeDetails.getAdjDocumentNumber());
		            actionForm.setReferenceNumber(aeDetails.getAdjReferenceNumber());
		            actionForm.setSoNumber(aeDetails.getAdjSoNumber());
		            actionForm.setAmount(Common.convertDoubleToStringMoney(aeDetails.getAdjAmount(), precisionUnit));
		            actionForm.setAmountApplied(Common.convertDoubleToStringMoney(aeDetails.getAdjAmountApplied(), precisionUnit));
		            actionForm.setConversionDate(Common.convertSQLDateToString(aeDetails.getAdjConversionDate()));
		            actionForm.setConversionRate(Common.convertDoubleToStringMoney(aeDetails.getAdjConversionRate(), (short)40));
		            actionForm.setMemo(aeDetails.getAdjMemo());
		            actionForm.setAdjustmentVoid(Common.convertByteToBoolean(aeDetails.getAdjVoid()));

		            actionForm.setAdjustmentRefund(Common.convertByteToBoolean(aeDetails.getAdjRefund()));

		            actionForm.setRefundAmount(Common.convertDoubleToStringMoney(aeDetails.getAdjRefundAmount() > 0 ? aeDetails.getAdjRefundAmount() : aeDetails.getAdjAmount() -  aeDetails.getAdjAmountApplied() , precisionUnit)

		            		);
		            actionForm.setRefundReferenceNumber(aeDetails.getAdjRefundReferenceNumber());

		            actionForm.setApprovalStatus(aeDetails.getAdjApprovalStatus());
            		actionForm.setPosted(aeDetails.getAdjPosted() == 1 ? "YES" : "NO");
            		actionForm.setVoidApprovalStatus(aeDetails.getAdjVoidApprovalStatus());
            		actionForm.setVoidPosted(aeDetails.getAdjVoidPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(aeDetails.getAdjCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(aeDetails.getAdjDateCreated()));
            		actionForm.setLastModifiedBy(aeDetails.getAdjLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(aeDetails.getAdjDateLastModified()));
            		actionForm.setApprovedRejectedBy(aeDetails.getAdjApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(aeDetails.getAdjDateApprovedRejected()));
            		actionForm.setPostedBy(aeDetails.getAdjPostedBy());
            		actionForm.setReasonForRejection(aeDetails.getAdjReasonForRejection());
            		actionForm.setDatePosted(Common.convertSQLDateToString(aeDetails.getAdjDatePosted()));
            		actionForm.setTotalDebit(Common.convertDoubleToStringMoney(aeDetails.getAdjTotalDebit(), precisionUnit));
            		actionForm.setTotalCredit(Common.convertDoubleToStringMoney(aeDetails.getAdjTotalCredit(), precisionUnit));

            		if (!actionForm.getBankAccountList().contains(aeDetails.getAdjBaName())) {

   						if (actionForm.getBankAccountList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

   							actionForm.clearBankAccountList();

   						}
   						actionForm.setBankAccountList(aeDetails.getAdjBaName());

   					}
		            actionForm.setBankAccount(aeDetails.getAdjBaName());

		            actionForm.setCurrency(aeDetails.getAdjBaFcName());

		            this.setFormProperties(actionForm);

		            if (request.getParameter("child") == null) {

			         	return (mapping.findForward("cmAdjustmentEntry"));

			        } else {

			         	return (mapping.findForward("cmAdjustmentEntryChild"));

			        }

			    }

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }


            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if ((request.getParameter("saveSubmitButton") != null &&
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

                   	try {

                   	   list = ejbAE.getAdApprovalNotifiedUsersByAdjCode(actionForm.getAdjustmentCode(), user.getCmpCode());

                   	   if (list.isEmpty()) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));

                   	   } else if (list.contains("DOCUMENT POSTED")) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                	   	       new ActionMessage("messages.documentPosted"));

                   	   } else {

                   	   	   i = list.iterator();

                   	   	   String APPROVAL_USERS = "";

                   	   	   while (i.hasNext()) {

                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

                   	   	       if (i.hasNext()) {

                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";

                   	   	       }


                   	   	   }

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

                   	   }

                   	   saveMessages(request, messages);
                       actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

                   } catch(EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }

	               	   return(mapping.findForward("cmnErrorPage"));

		           }

              } else if (request.getParameter("saveAsDraftButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

              } else if (request.getParameter("refundButton") != null &&
                      actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                      actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

              }

            }

            actionForm.reset(mapping, request);

            actionForm.setAdjustmentCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setAmountApplied(Common.convertDoubleToStringMoney(0d, precisionUnit));
            actionForm.setConversionRate("1.000000");
            actionForm.setPosted("NO");
            actionForm.setVoidPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));

            this.setFormProperties(actionForm);
            return(mapping.findForward("cmAdjustmentEntry"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(ArithmeticException e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in CmAdjustmentEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();
          return mapping.findForward("cmnErrorPage");

      }
   }

   private void setFormProperties(CmAdjustmentEntryForm actionForm) {


		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {


			actionForm.setEnableFields(true);
			actionForm.setShowSaveSubmitButton(true);
			actionForm.setShowSaveAsDraftButton(true);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableAdjustmentVoid(false);
			actionForm.setShowRefundButton(false);
			actionForm.setShowJournalButton(true);

			if (actionForm.getAdjustmentVoid() && actionForm.getPosted().equals("NO")) {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(true);
				actionForm.setEnableAdjustmentVoid(false);
				actionForm.setEnableAdjustmentRefund(false);
				actionForm.setShowJournalButton(true);

			} else if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getAdjustmentCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setEnableAdjustmentVoid(false);
					actionForm.setEnableAdjustmentRefund(false);
					actionForm.setShowJournalButton(true);

				} else if (actionForm.getAdjustmentCode() != null &&
						Common.validateRequired(actionForm.getApprovalStatus())) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowRefundButton(false);
					actionForm.setEnableAdjustmentVoid(false);
					actionForm.setEnableAdjustmentRefund(false);
					actionForm.setShowJournalButton(true);


				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
						actionForm.getApprovalStatus().equals("N/A")) {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(true);

					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableAdjustmentVoid(true);
					actionForm.setEnableAdjustmentRefund(true);
					actionForm.setShowJournalButton(true);

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowRefundButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setEnableAdjustmentVoid(false);
					actionForm.setEnableAdjustmentRefund(false);
					actionForm.setShowJournalButton(true);

				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowJournalButton(true);

				if (actionForm.getVoidApprovalStatus() == null &&
						actionForm.getAdjustmentRefund() == false &&
							actionForm.getAdjustmentVoid()== false) {

					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowRefundButton(true);
					actionForm.setEnableAdjustmentVoid(true);
					actionForm.setEnableAdjustmentRefund(true);
				} else {
					actionForm.setShowRefundButton(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setEnableAdjustmentVoid(false);
					actionForm.setEnableAdjustmentRefund(false);
				}

			}




		} else {


			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setEnableAdjustmentVoid(false);
			actionForm.setEnableAdjustmentRefund(false);
			actionForm.setShowJournalButton(true);

		}

	}
}