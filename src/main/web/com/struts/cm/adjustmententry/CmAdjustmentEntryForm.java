package com.struts.cm.adjustmententry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmAdjustmentEntryForm extends ActionForm implements Serializable {

   private Integer adjustmentCode = null;
   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String currency = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String amount = null;
   private String amountApplied = null;

   private String soNumber = null;
   private String checkNumber = null;

   private String memo = null;
   private String voidApprovalStatus = null;
   private String voidPosted = null;

   private String customerName = null;
   private boolean adjustmentVoid = false;

   private boolean adjustmentRefund = false;
   private String refundAmount = null;
   private String refundReferenceNumber = null;

   private String conversionDate = null;
   private String conversionRate = null;
   private String approvalStatus = null;
   private String posted = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;
   private String approvedRejectedBy = null;
   private String dateApprovedRejected = null;
   private String postedBy = null;
   private String datePosted = null;
   private String reasonForRejection = null;

   private String report = null;

   private boolean isBankAccountEntered = false;
   private boolean isTypeEntered = false;
   private String functionalCurrency = null;
   private String isConversionDateEntered = null;
   private String isSalesOrderEntered = null;

   private String totalDebit = null;
   private String totalCredit = null;

   private String saveButton = null;
   private String closeButton = null;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean enableAdjustmentVoid = false;
   private boolean enableAdjustmentRefund = false;
   private boolean showSaveSubmitButton = false;
   private boolean showRefundButton = false;
   private boolean showSaveAsDraftButton = false;

   private boolean showDeleteButton = false;
   private boolean showJournalButton = false;


   private String customer = null;
   private ArrayList customerList = new ArrayList();
   private boolean showCustomerList = false;

   private String supplier = null;
   private ArrayList supplierList = new ArrayList();
   private boolean showSupplierList = false;


   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public Integer getAdjustmentCode() {

   	  return adjustmentCode;

   }

   public void setAdjustmentCode(Integer adjustmentCode) {

      this.adjustmentCode = adjustmentCode;

   }

   public String getBankAccount() {

   	  return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

   	  this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

   	  return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

   	  bankAccountList.add(bankAccount);

   }

   public void clearBankAccountList() {

   	  bankAccountList.clear();
   	  bankAccountList.add(Constants.GLOBAL_BLANK);

   }

   public String getCurrency() {

   	  return currency;

   }

   public void setCurrency(String currency) {

   	  this.currency = currency;

   }

   public String getType() {

   	  return type;

   }

   public void setType(String type) {

   	  this.type = type;

   }

   public ArrayList getTypeList() {

   	  return typeList;

   }

   public void setTypeList(String type) {

   	  typeList.add(type);

   }

   public void clearTypeList() {

   	  typeList.clear();
   	  typeList.add(Constants.GLOBAL_BLANK);

   }

   public String getDate() {

   	  return date;

   }

   public void setDate(String date) {

   	  this.date = date;

   }

   public String getDocumentNumber() {

   	  return documentNumber;

   }

   public void setDocumentNumber(String documentNumber) {

   	  this.documentNumber = documentNumber;

   }

   public String getReferenceNumber() {

   	  return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

   	  this.referenceNumber = referenceNumber;

   }

   public String getAmount() {

   	  return amount;

   }

   public void setAmount(String amount) {

   	  this.amount = amount;

   }

   public String getAmountApplied() {

   	  return amountApplied;

   }

   public void setAmountApplied(String amountApplied) {

   	  this.amountApplied = amountApplied;

   }


   public String getSoNumber() {

   	  return soNumber;

   }

   public void setSoNumber(String soNumber) {

   	  this.soNumber = soNumber;

   }

   public String getCheckNumber() {

   	  return checkNumber;

   }

   public void setCheckNumber(String checkNumber) {

   	  this.checkNumber = checkNumber;

   }



   public String getMemo() {

   	  return memo;

   }

   public void setMemo(String memo) {

   	  this.memo = memo;

   }

   public String getVoidApprovalStatus() {

   	   return voidApprovalStatus;

   }

   public void setVoidApprovalStatus(String voidApprovalStatus) {

   	   this.voidApprovalStatus = voidApprovalStatus;

   }

   public String getVoidPosted() {

   	   return voidPosted;

   }

   public void setVoidPosted(String voidPosted) {

   	   this.voidPosted = voidPosted;

   }

   public String getCustomerName() {

	   	  return customerName;

	   }

	   public void setCustomerName(String customerName) {

	   	  this.customerName = customerName;

	   }

   public boolean getAdjustmentVoid() {

   	  return adjustmentVoid;

   }

   public void setAdjustmentVoid(boolean adjustmentVoid) {

   	  this.adjustmentVoid = adjustmentVoid;

   }

   public boolean getAdjustmentRefund() {

	   	  return adjustmentRefund;

	   }

	   public void setAdjustmentRefund(boolean adjustmentRefund) {

	   	  this.adjustmentRefund = adjustmentRefund;

	   }

	   public String getRefundAmount() {

	   	  return refundAmount;

	   }

	   public void setRefundAmount(String refundAmount) {

	   	  this.refundAmount = refundAmount;

	   }


	   public String getRefundReferenceNumber() {

	   	  return refundReferenceNumber;

	   }

	   public void setRefundReferenceNumber(String refundReferenceNumber) {

	   	  this.refundReferenceNumber = refundReferenceNumber;

	   }

   public String getConversionDate() {

   	  return conversionDate;

   }

   public void setConversionDate(String conversionDate) {

   	  this.conversionDate = conversionDate;

   }

   public String getConversionRate() {

   	  return conversionRate;

   }

   public void setConversionRate(String conversionRate) {

   	  this.conversionRate = conversionRate;

   }

   public String getApprovalStatus() {

   	   return approvalStatus;

   }

   public void setApprovalStatus(String approvalStatus) {

   	   this.approvalStatus = approvalStatus;

   }

   public String getPosted() {

   	   return posted;

   }

   public void setPosted(String posted) {

   	   this.posted = posted;

   }

   public String getCreatedBy() {

   	   return createdBy;

   }

   public void setCreatedBy(String createdBy) {

   	  this.createdBy = createdBy;

   }

   public String getDateCreated() {

   	   return dateCreated;

   }

   public void setDateCreated(String dateCreated) {

   	   this.dateCreated = dateCreated;

   }

   public String getLastModifiedBy() {

   	  return lastModifiedBy;

   }

   public void setLastModifiedBy(String lastModifiedBy) {

   	  this.lastModifiedBy = lastModifiedBy;

   }

   public String getDateLastModified() {

   	  return dateLastModified;

   }

   public void setDateLastModified(String dateLastModified) {

   	  this.dateLastModified = dateLastModified;

   }


   public String getApprovedRejectedBy() {

   	  return approvedRejectedBy;

   }

   public void setApprovedRejectedBy(String approvedRejectedBy) {

   	  this.approvedRejectedBy = approvedRejectedBy;

   }

   public String getDateApprovedRejected() {

   	  return dateApprovedRejected;

   }

   public void setDateApprovedRejected(String dateApprovedRejected) {

   	  this.dateApprovedRejected = dateApprovedRejected;

   }

   public String getPostedBy() {

   	  return postedBy;

   }

   public void setPostedBy(String postedBy) {

   	  this.postedBy = postedBy;

   }

   public String getDatePosted() {

   	  return datePosted;

   }

   public void setDatePosted(String datePosted) {

   	  this.datePosted = datePosted;

   }

   public String getReasonForRejection() {

   	  return reasonForRejection;

   }

   public void setReasonForRejection(String reasonForRejection) {

   	  this.reasonForRejection = reasonForRejection;

   }


   public String getReport() {

   	   return report;

   }

   public void setReport(String report) {

   	   this.report = report;

   }

   public boolean getEnableFields() {

   	   return enableFields;

   }

   public void setEnableFields(boolean enableFields) {

   	   this.enableFields = enableFields;

   }

   public boolean getEnableAdjustmentVoid() {

   	   return enableAdjustmentVoid;

   }

   public void setEnableAdjustmentVoid(boolean enableAdjustmentVoid) {

   	   this.enableAdjustmentVoid = enableAdjustmentVoid;

   }

   public boolean getEnableAdjustmentRefund() {

   	   return enableAdjustmentRefund;

   }

   public void setEnableAdjustmentRefund(boolean enableAdjustmentRefund) {

   	   this.enableAdjustmentRefund = enableAdjustmentRefund;

   }

   public boolean getShowSaveSubmitButton() {

   	   return showSaveSubmitButton;

   }

   public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {

   	   this.showSaveSubmitButton = showSaveSubmitButton;

   }

   public boolean getShowRefundButton() {

   	   return showRefundButton;

   }

   public void setShowRefundButton(boolean showRefundButton) {

   	   this.showRefundButton = showRefundButton;

   }

   public boolean getShowSaveAsDraftButton() {

   	   return showSaveAsDraftButton;

   }

   public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {

   	   this.showSaveAsDraftButton = showSaveAsDraftButton;

   }

   public boolean getShowDeleteButton() {

   	   return showDeleteButton;

   }

   public void setShowDeleteButton(boolean showDeleteButton) {

   	   this.showDeleteButton = showDeleteButton;

   }

   public boolean getShowJournalButton() {

   	   return showJournalButton;

   }

   public void setShowJournalButton(boolean showJournalButton) {

   	   this.showJournalButton = showJournalButton;

   }

   public String getCustomer(){

	   return customer;

   }

   public void setCustomer(String customer){

	   this.customer = customer;

   }


   public ArrayList getCustomerList() {

   	  return customerList;

   }

   public void setCustomerList(String customerCode) {

	   customerList.add(customerCode);

   }

   public void clearCustomerList() {

	   customerList.clear();
	   customerList.add(Constants.GLOBAL_BLANK);

   }


   public boolean getShowCustomerList() {

   	   return showCustomerList;

   }

   public void setShowCustomerList(boolean showCustomerList) {

   	   this.showCustomerList = showCustomerList;

   }







public String getSupplier(){

	   return supplier;

   }

   public void setSupplier(String supplier){

	   this.supplier = supplier;

   }


   public ArrayList getSupplierList() {

   	  return supplierList;

   }

   public void setSupplierList(String supplierCode) {

	   supplierList.add(supplierCode);

   }

   public void clearSupplierList() {

	   supplierList.clear();
	   supplierList.add(Constants.GLOBAL_BLANK);

   }


   public boolean getShowSupplierList() {

   	   return showSupplierList;

   }

   public void setShowSupplierList(boolean showSupplierList) {

   	   this.showSupplierList = showSupplierList;

   }



   public boolean getIsBankAccountEntered() {

   	  return isBankAccountEntered;

   }

   public void setIsBankAccountEntered(boolean isBankAccountEntered) {

   	  this.isBankAccountEntered = isBankAccountEntered;

   }


   public boolean getIsTypeEntered(){
	   return isTypeEntered;
   }



   public String getIsSalesOrderEntered(){
	   return isSalesOrderEntered;
   }



   public String getIsConversionDateEntered(){

   	return isConversionDateEntered;

   }

   public String getTotalDebit() {

		return totalDebit;

	}

	public void setTotalDebit(String totalDebit) {

		this.totalDebit = totalDebit;

	}

	public String getTotalCredit() {

		return totalCredit;

	}

	public void setTotalCredit(String totalCredit) {

		this.totalCredit = totalCredit;

	}







   public void reset(ActionMapping mapping, HttpServletRequest request) {

      bankAccount = Constants.GLOBAL_BLANK;
      currency = null;



      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK);
      typeList.add(Constants.CM_AE_INTEREST);
      typeList.add(Constants.CM_AE_BANK_CHARGE);
      typeList.add(Constants.CM_AE_CREDIT_MEMO);
      typeList.add(Constants.CM_AE_DEBIT_MEMO);
      typeList.add(Constants.CM_AE_ADVANCE);
     // typeList.add(Constants.CM_AE_SPL_ADVANCE);
   //   typeList.add(Constants.CM_AR_SO_ADVANCE);

      type = Constants.GLOBAL_BLANK;
      date = Common.convertSQLDateToString(new Date());
      documentNumber = null;
      referenceNumber = null;
      soNumber = null;
      amount = null;
      amountApplied = null;

      memo = null;
      voidApprovalStatus = null;
	  voidPosted = null;
      customerName = null;
      adjustmentVoid = false;



      adjustmentRefund = false;
      refundAmount = null;
      refundReferenceNumber = null;

      conversionDate = null;
      conversionRate = "1.000000";
      approvalStatus = null;
	  posted = null;
	  reasonForRejection = null;
	  createdBy = null;
	  dateCreated = null;
	  lastModifiedBy = null;
	  dateLastModified = null;
	  approvedRejectedBy = null;
	  dateApprovedRejected = null;
	  postedBy = null;
	  datePosted = null;
      isBankAccountEntered = false;
      isTypeEntered = false;
      isConversionDateEntered = null;
	  showCustomerList = false;
      saveButton = null;
      closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();

      if (request.getParameter("saveSubmitButton") != null ||
	     request.getParameter("saveAsDraftButton") != null ||
         request.getParameter("journalButton") != null ||
    	  request.getParameter("printButton") != null) {

    	  if(showCustomerList){
    		  if(Common.validateRequired(customer) || customer.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
    			  errors.add("bankAccount",
    		         new ActionMessage("adjustmentEntry.error.customerRequired"));

    		  }
    	  }


         if (Common.validateRequired(bankAccount) || bankAccount.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

            errors.add("bankAccount",
               new ActionMessage("adjustmentEntry.error.bankAccountRequired"));

         }

         if (Common.validateRequired(type)) {

            errors.add("type",
               new ActionMessage("adjustmentEntry.error.typeRequired"));

         }

         if (Common.validateRequired(date)) {

            errors.add("date",
               new ActionMessage("adjustmentEntry.error.dateRequired"));

         }

         if (!Common.validateDateFormat(date)) {

            errors.add("date",
               new ActionMessage("adjustmentEntry.error.dateInvalid"));

         }

         if (Common.validateRequired(amount)) {

            errors.add("amount",
               new ActionMessage("adjustmentEntry.error.amountRequired"));

         }

         if (!Common.validateMoneyFormat(amount)) {

            errors.add("amount",
               new ActionMessage("adjustmentEntry.error.amountInvalid"));

         }

	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("adjustmentEntry.error.conversionDateInvalid"));
         }

	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("adjustmentEntry.error.conversionRateInvalid"));
         }

         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("adjustmentEntry.error.conversionDateMustBeNull"));
	 	 }

		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){
		    errors.add("conversionRate",
		       new ActionMessage("adjustmentEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("adjustmentEntry.error.conversionRateOrDateMustBeEntered"));
		 }


		 if(type.equals(Constants.CM_AR_SO_ADVANCE)) {

			 if(Common.validateRequired(soNumber)) {
				 errors.add("soNumber",
					       new ActionMessage("adjustmentEntry.error.salesOrderNotExist"));
			 }

		 }

      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

      	if(!Common.validateDateFormat(conversionDate)){

      		errors.add("conversionDate", new ActionMessage("adjustmentEntry.error.conversionDateInvalid"));

      	}

      }

      return errors;
   }
}