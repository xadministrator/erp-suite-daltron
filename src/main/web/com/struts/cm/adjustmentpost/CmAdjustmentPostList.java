package com.struts.cm.adjustmentpost;

import java.io.Serializable;

public class CmAdjustmentPostList implements Serializable {

   private Integer adjustmentCode = null;
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String bankAccount = null;
   private String adjustmentType = null;
   private String amount = null;

   private boolean post = false;
       
   private CmAdjustmentPostForm parentBean;
    
   public CmAdjustmentPostList(CmAdjustmentPostForm parentBean,
	  Integer adjustmentCode,
	  String date,
	  String documentNumber,
	  String referenceNumber,
	  String bankAccount,
	  String adjustmentType,
	  String amount) {

      this.parentBean = parentBean;
      this.adjustmentCode = adjustmentCode;
      this.date = date;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.bankAccount = bankAccount;
      this.adjustmentType = adjustmentType;
      this.amount = amount;
      
   }
   
   public Integer getAdjustmentCode() {
   	
   	  return adjustmentCode;
   	  
   }
   
   public String getDate() {
   	
   	  return date;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getBankAccount() {
   	
   	  return bankAccount;
   	  
   }
   
   public String getAdjustmentType() {
   	
   	  return adjustmentType;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }

   public boolean getPost() {
   	
   	  return post;
   	
   }
   
   public void setPost(boolean post) {
   	
   	  this.post = post;
   	
   }
      
}