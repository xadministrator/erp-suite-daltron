package com.struts.cm.approval;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmApprovalForm extends ActionForm implements Serializable {

   private String document = null;
   private ArrayList documentList = new ArrayList();
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();  
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	   
   private String pageState = new String();
   private ArrayList cmAPRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private HashMap criteria = new HashMap();
   private String maxRows = null;
   private String queryCount = null;
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public CmApprovalList getCmAPRByIndex(int index) {

      return((CmApprovalList)cmAPRList.get(index));

   }

   public Object[] getCmAPRList() {

      return cmAPRList.toArray();

   }

   public int getCmAPRListSize() {

      return cmAPRList.size();

   }

   public void saveCmAPRList(Object newCmAPRList) {

      cmAPRList.add(newCmAPRList);

   }

   public void clearCmAPRList() {
      cmAPRList.clear();
   }

   public void setRowSelected(Object selectedCmAPRList, boolean isEdit) {

      this.rowSelected = cmAPRList.indexOf(selectedCmAPRList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showCmAPRRow(int rowSelected) {

   }

   public void updateCmAPRRow(int rowSelected, Object newCmAPRList) {

      cmAPRList.set(rowSelected, newCmAPRList);

   }

   public void deleteCmAPRList(int rowSelected) {

      cmAPRList.remove(rowSelected);

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getDocument() {

      return document;

   }

   public void setDocument(String document) {

      this.document = document;

   }

   public ArrayList getDocumentList() {

      return documentList;

   }
   
   public String getDocumentNumberFrom() {
   	
   	  return documentNumberFrom;
   	
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom) {
   	
   	  this.documentNumberFrom = documentNumberFrom;
   	
   }
   
   public String getDocumentNumberTo() {
   	
   	  return documentNumberTo;
   	
   }
   
   public void setDocumentNumberTo(String documentNumberTo) {
   	
   	  this.documentNumberTo = documentNumberTo;
   	
   }
   
   public String getDateFrom() {
   	
   	  return dateFrom;
   	
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   	
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	
   }
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
      
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  documentNumberFrom = null;
 	  documentNumberTo = null;
 	  dateFrom = null;
 	  dateTo = null;
   	
      if (documentList.isEmpty()) {
      	
	      documentList.clear();
	      documentList.add("CM ADJUSTMENT");
	      documentList.add("CM FUND TRANSFER");
	      document = "CM ADJUSTMENT";
	      
      }
      
      for (int i=0; i<cmAPRList.size(); i++) {
      	
          CmApprovalList actionList = (CmApprovalList)cmAPRList.get(i);          
          actionList.setApprove(false);
          actionList.setReject(false);
          
      }
      
      if (orderByList.isEmpty()) {
   		
   		orderByList.clear();
   		orderByList.add(Constants.GLOBAL_BLANK);
   		orderByList.add("DATE");
   		orderByList.add("DOCUMENT NUMBER");
   		orderBy = "DOCUMENT NUMBER";
   		
   	  }
                          
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
      		request.getParameter("previousButton") != null) {   
      	
      	if (Common.validateRequired(maxRows)) {
      		
      		errors.add("maxRows",
      				new ActionMessage("cmApproval.error.maxRowsRequired"));
      		
      	}
      	
      	if (!Common.validateNumberFormat(maxRows)) {
      		
      		errors.add("maxRows",
      				new ActionMessage("cmApproval.error.maxRowsInvalid"));
      		
      	}
      	
      	if (!Common.validateDateFormat(dateFrom)) {
      		
      		errors.add("dateFrom",
      				new ActionMessage("cmApproval.error.dateFromInvalid"));
      		
      	}         
      	
      	if (!Common.validateDateFormat(dateTo)) {
      		
      		errors.add("dateTo",
      				new ActionMessage("cmApproval.error.dateToInvalid"));
      		
      	}
      	
      }
      
      return errors;

   }
   
}