package com.struts.cm.approval;

import java.io.Serializable;

public class CmApprovalList implements Serializable {

   private Integer documentCode = null;
   private String document = null;   
   private String date = null;
   private String documentNumber = null;
   private String amount = null;
   private String documentType = null;
   private String reasonForRejection = null;
   
   private boolean approve = false;
   private boolean reject = false;
       
   private CmApprovalForm parentBean;
    
   public CmApprovalList(CmApprovalForm parentBean,
      Integer documentCode,
      String document,
      String date,
      String documentNumber,
      String amount,
      String documentType) {

      this.parentBean = parentBean;
      this.documentCode = documentCode;
      this.document = document;
      this.date = date;
      this.documentNumber = documentNumber;
      this.amount = amount;
      this.documentType = documentType;
      
   }

   public Integer getDocumentCode() {

      return documentCode;

   }
   
   public String getDocument() {
   	
   	  return document;
   	
   }
   
   public String getDate() {
   
      return date;
      
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getAmount() {
   
      return amount;
   
   }
   
   public String getDocumentType() {
   	
   	  return documentType;
   	
   }

   public boolean getApprove() {
   	
   	  return approve;
   	
   }
   
   public void setApprove(boolean approve) {
   	
   	  this.approve = approve;
   	
   }
   
   public boolean getReject() {
   	
   	  return reject;
   	
   }
   
   public void setReject(boolean reject) {
   	
   	  this.reject = reject;
   	  
   }
   
   public String getReasonForRejection() {
   	
   	  return reasonForRejection;
   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
         
}