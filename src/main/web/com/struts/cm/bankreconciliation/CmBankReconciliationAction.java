package com.struts.cm.bankreconciliation;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.CmAdjustmentEntryController;
import com.ejb.txn.CmAdjustmentEntryControllerHome;
import com.ejb.txn.CmAdjustmentPostController;
import com.ejb.txn.CmAdjustmentPostControllerHome;
import com.ejb.txn.CmBankReconciliationController;
import com.ejb.txn.CmBankReconciliationControllerHome;
import com.ejb.txn.CmFindAdjustmentController;
import com.ejb.txn.CmFindAdjustmentControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModBankAccountDetails;
import com.util.CmAdjustmentDetails;
import com.util.CmModAdjustmentDetails;
import com.util.CmModBankReconciliationDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.CmAdjustmentDetails;

public final class CmBankReconciliationAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();

      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmBankReconciliationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         CmBankReconciliationForm actionForm = (CmBankReconciliationForm)form;

         String frParam = Common.getUserPermission(user, Constants.CM_BANK_RECONCILIATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmBankReconciliation");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize CmBankReconciliationControllerand, CmAdjustmentEntryController and CmAdjustmentPostController EJB
*******************************************************/

         CmBankReconciliationControllerHome homeBR = null;
         CmBankReconciliationController ejbBR = null;

         CmAdjustmentEntryControllerHome homeAdjEntry = null;
         CmAdjustmentEntryController ejbAdjEntry = null;

         CmAdjustmentPostControllerHome homeAdjPost = null;
         CmAdjustmentPostController ejbAdjPost = null;

         CmFindAdjustmentControllerHome homeFindAdj = null;
         CmFindAdjustmentController ejbFindAdj = null;


         try {

            homeBR = (CmBankReconciliationControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmBankReconciliationControllerEJB", CmBankReconciliationControllerHome.class);
            homeAdjEntry = (CmAdjustmentEntryControllerHome)com.util.EJBHomeFactory.
            	lookUpHome("ejb/CmAdjustmentEntryControllerEJB",CmAdjustmentEntryControllerHome.class);
            homeAdjPost = (CmAdjustmentPostControllerHome)com.util.EJBHomeFactory.
            	lookUpHome("ejb/CmAdjustmentPostControllerEJB", CmAdjustmentPostControllerHome.class);
            homeFindAdj = (CmFindAdjustmentControllerHome)com.util.EJBHomeFactory.
                	lookUpHome("ejb/CmFindAdjustmentControllerEJB", CmFindAdjustmentControllerHome.class);



         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmBankReconciliationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBR = homeBR.create();
            ejbAdjEntry = homeAdjEntry.create();
            ejbAdjPost = homeAdjPost.create();
            ejbFindAdj = homeFindAdj.create();

         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmBankReconciliationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

         short precisionUnit = 0;

         try {

            precisionUnit = ejbBR.getGlFcPrecisionUnit(user.getCmpCode());

         } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in CmBankReconciliationAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());

            }

            return(mapping.findForward("cmnErrorPage"));
         }

/*******************************************************
   -- Cm BR Show Details Action --
*******************************************************/
         System.out.println(request.getParameter("validationButton"));
         if (request.getParameter("showDetailsButton") != null) {

        	 System.out.println("showDetailsButton----------->");

	        actionForm.setTableType(Constants.GLOBAL_DETAILED);

	        return(mapping.findForward("cmBankReconciliation"));

/*******************************************************
   -- Cm BR Hide Details Action --
*******************************************************/

	     } else if (request.getParameter("hideDetailsButton") != null) {

	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	        return(mapping.findForward("cmBankReconciliation"));



/*******************************************************
    -- Cm BR Bank Account Check/Deposit Validation --
 *******************************************************/

      } else if (request.getParameter("validationButton") != null  &&

         actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

    	  double clearbalance = 0.0;
    	  double difference = 0.0;
    	  double endingbalance = 0.0;




    	  System.out.print(actionForm.getValidationFile().getInputStream() +  " test");

    	// when date in excel is dd/MM/yyyy
	    	 //SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_MMDDYYYY);

	    	 // when date in excel is MM/dd/yyyy
//	    	 SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_DDMMYYYY);

     	 try {




       	  if(actionForm.getUploadFile().getFileSize() == 0){

       		errors.add(ActionMessages.GLOBAL_MESSAGE,
   				 new ActionMessage("bankReconciliation.error.filenotfound"));
       		saveErrors(request, new ActionMessages(errors));
       		return mapping.findForward("cmBankReconciliation");
       	  }

       	System.out.print("1");
  		 CmAdjustmentDetails details = new CmAdjustmentDetails();
  		System.out.print("2");
 		 CSVParser csvParser = new CSVParser(actionForm.getValidationFile().getInputStream());
 		 System.out.print("3");
 		 String[][] values = csvParser.getAllValues();
 		 System.out.print("4");
 		 /* 0 - deposit date
 		  * 1 - Cleared date
 		  * 2 - Cheque No.
 		  * 3 - Description
 		  * 4 - Debit
 		  * 5 - Credit
 		  */
  		 // get bank account details
 		 System.out.print(values.length + " asdas");







  		 AdModBankAccountDetails mBaDetails = ejbBR.getAdBaByBaName(actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), user.getCmpCode());

  		 actionForm.setAccountBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaAvailableBalance(), precisionUnit));
  		 actionForm.setLastReconciledDate(Common.convertSQLDateToString(mBaDetails.getBaLastReconciledDate()));
  		 actionForm.setOpeningBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaLastReconciledBalance(),precisionUnit));
  		 actionForm.setClearedBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaLastReconciledBalance(),precisionUnit));
  		 actionForm.setDifference(Common.convertDoubleToStringMoney(0 - mBaDetails.getBaLastReconciledBalance(),precisionUnit));
  		 actionForm.setCurrency(mBaDetails.getBaFcName());
  		 actionForm.setEndingBalance("");
  		 clearbalance =  mBaDetails.getBaLastReconciledBalance();


  		 difference = 0 - mBaDetails.getBaLastReconciledBalance();
  		 // get deposit in transit

  		 String BR_NUMBER = null;

  		 actionForm.clearCmDINList();

  		 ArrayList list = ejbBR.getCmDepositInTransitByBaName(actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

  		 Iterator i = list.iterator();


  		 while (i.hasNext()) {

  			 CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();


  			 CmBankReconciliationDepositList cmDINList = new CmBankReconciliationDepositList(
  					 actionForm, mdetails.getBrCode(),
  					 Common.convertSQLDateToString(mdetails.getBrDate()),
  					 mdetails.getBrDocumentNumber(),
  					 mdetails.getBrReferenceNumber(),
  					mdetails.getBrNumber(),
  					 mdetails.getBrName(),
  					 mdetails.getBrType(),
  					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit),
  					 actionForm.getReconcileDate());


  			 if(values.length > 0){

  				 for(int x=1;x<values.length;x++){
       				System.out.print(values[x][2] + " and " + BR_NUMBER);
       				System.out.print("Amount Clear Balance Deposit" + clearbalance);
       				System.out.print(values[x][4] + " and " + cmDINList.getDepositAmount() );
       				String amount = Common.convertDoubleToStringMoney(Double.parseDouble(values[x][4]),precisionUnit);
       				 if(values[x][2].trim().toString().equals(BR_NUMBER.trim().toString()) && amount.equals(cmDINList.getDepositAmount()) && !values[x][1].trim().equals("")){
       					cmDINList.setDepositMark(true);



       					details.setAdjDate(Common.convertStringToSQLDate(values[x][0].trim()));


       					cmDINList.setDateCleared(Common.convertSQLDateToString(Common.convertStringToSQLDate(values[x][1].trim())));
       					clearbalance = clearbalance*1 + Double.parseDouble(cmDINList.getDepositAmount().replace(",", ""))*1;


       					 System.out.print("pasok1");
       				 }
       			 }

  			 }




  			 actionForm.saveCmDINList(cmDINList);

  		 }

  		 // get outstanding checks

  		 actionForm.clearCmOCList();

  		 list = ejbBR.getCmOutstandingCheckByBaName (actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

  		 i = list.iterator();

  		 while (i.hasNext()) {

  			 CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();

  			 CmBankReconciliationCheckList cmOCList = new CmBankReconciliationCheckList(
  					 actionForm, mdetails.getBrCode(),
  					 Common.convertSQLDateToString(mdetails.getBrDate()),
  					 mdetails.getBrDocumentNumber(),
  					 mdetails.getBrReferenceNumber(),
  					 mdetails.getBrNumber(),
  					 mdetails.getBrName(),
  					 mdetails.getBrType(),
  					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit),
  					 actionForm.getReconcileDate());

  			 if(values.length > 0){
	     			 for(int x=1;x<values.length;x++){
	     				System.out.print(values[x][2] + " and " + BR_NUMBER);
	     				System.out.print("Amount Clear Balance Checks" + clearbalance);
	     				String amount = Common.convertDoubleToStringMoney(Double.parseDouble(values[x][5]),precisionUnit);
	  					if(values[x][2].trim().toString().equals(BR_NUMBER.trim().toString()) && amount.equals(cmOCList.getCheckAmount())&& !values[x][1].trim().equals("")){
		  					cmOCList.setCheckMark(true);
		  					cmOCList.setDateCleared(values[x][1]);
		  					clearbalance = clearbalance*1 - Double.parseDouble(cmOCList.getCheckAmount().replace(",", ""))*1;

		  					 System.out.print("pasok2");
	  					}
	     			 }
  			 }
	     			System.out.print(cmOCList.getCheckMark());
	     			System.out.print(cmOCList.getDateCleared());


  			 actionForm.saveCmOCList(cmOCList);

  		 }
  		difference = difference - clearbalance;

  		 actionForm.setAdjustmentDate(actionForm.getReconcileDate());
  		 actionForm.setInterestDate(actionForm.getReconcileDate());
  		 actionForm.setServiceChargeDate(actionForm.getReconcileDate());
  		 actionForm.setClearedBalance(Common.convertDoubleToStringMoney(clearbalance,precisionUnit));
  		 actionForm.setDifference(Common.convertDoubleToStringMoney(difference,precisionUnit));






     	 } catch (GlobalRecordAlreadyExistException ex) {

    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
    				 new ActionMessage("bankReconciliation.error.recordAlreadyExist"));
    		 saveErrors(request, new ActionMessages(errors));

    	 }catch (IllegalArgumentException ex){

    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
    				 new ActionMessage("bankReconciliation.error.filenotfound"));
    		 saveErrors(request, new ActionMessages(errors));


    	 }



     	 catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in CmBankReconciliationAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("cmBankReconciliation"));





/*******************************************************
   -- Cm BR Upload Bank Statement Action --
*******************************************************/

	     }else if(request.getParameter("uploadButton") != null &&
	    		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

	    	 // when date in excel is dd/MM/yyyy
	    	 //SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_MMDDYYYY);


	       	  if(actionForm.getUploadFile().getFileSize() == 0){

	       		errors.add(ActionMessages.GLOBAL_MESSAGE,
	   				 new ActionMessage("bankReconciliation.error.filenotfound"));
	       		saveErrors(request, new ActionMessages(errors));
	       		return mapping.findForward("cmBankReconciliation");
	       	  }




	    	 // when date in excel is MM/dd/yyyy
	    	// SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_INPUT);
	    	 //test data
	    	 try{


	    		 CmAdjustmentDetails details = new CmAdjustmentDetails();
	    		 CSVParser csvParser = new CSVParser(actionForm.getUploadFile().getInputStream());
	    		 String[][] values = csvParser.getAllValues();
	    		 /* 0 - deposit date
	    		  * 1 - Cleared date
	    		  * 2 - Cheque No.
	    		  * 3 - Description
	    		  * 4 - Amount

	    		 */

	    		 //convert string to date

	    		 ArrayList AdjList = new ArrayList();

	    		 for(int x=1;x<values.length;x++){
	    			 details = new CmAdjustmentDetails();
	    			 System.out.println("date: " + values[x][0] );
	    			if(!values[x][0].trim().equals("") ){

	    				System.out.println("Either Debit or Creadit");
	    				//set date
	    				details.setAdjDate( Common.convertStringToSQLDate(values[x][0].trim()));
	    				//set amount
		    			details.setAdjAmount(Double.parseDouble(values[x][4]));

		    			if(values[x][4].contains("-")){
		    				details.setAdjType("DEBIT MEMO");
		    				details.setAdjAmount(Double.parseDouble(values[x][4]));
		    				details.setAdjAmount(details.getAdjAmount()*-1);

		    			}else{
		    				details.setAdjType("CREDIT MEMO");
		    				details.setAdjAmount(Double.parseDouble(values[x][4]));
		    			}



	    			}else{

	    				System.out.println("Either Bank Charge or Interest");
	    				//set date
	    				details.setAdjDate( Common.convertStringToSQLDate(values[x][1].trim()));
	    				//set amount


	    				if(values[x][4].contains("-")){
		    				details.setAdjType("BANK CHARGE");
		    				details.setAdjAmount(Double.parseDouble(values[x][4]));
		    				details.setAdjAmount(details.getAdjAmount()*-1);

		    			}else{
		    				details.setAdjType("INTEREST");
		    				details.setAdjAmount(Double.parseDouble(values[x][4]));
		    			}


	    			}

	    			//set reference number from cheque number
	    			details.setAdjReferenceNumber(values[x][2]);
	    			//set description from Narrative
	    			details.setAdjMemo(values[x][3]);
	    			ejbAdjEntry.checkCmAdjEntryUpload(details, actionForm.getBankAccountName(), actionForm.getCurrency(), false	, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	    		 }


	    		 //re execute insert data


	    		csvParser = new CSVParser(actionForm.getUploadFile().getInputStream());
	    		values = csvParser.getAllValues();
	    		 /* 0 - deposit date
	    		  * 1 - cleared date
	    		  * 2 - Cheque No.
	    		  * 3 - Narrative
	    		  * 4 - Amount

	    		  */

	    		 //convert string to date


	    		AdjList = new ArrayList();

	    		 for(int x=1;x<values.length;x++){
	    			 details = new CmAdjustmentDetails();
	    			 if(!values[x][0].trim().equals("")){
	    				//set date
	    				details.setAdjDate(Common.convertStringToSQLDate(values[x][0].trim()));
	    				//set amount



		    			if(values[x][4].contains("-")){
		    				details.setAdjType("CREDIT MEMO");
		    				details.setAdjAmount(Double.parseDouble(values[x][4]));

		    				details.setAdjAmount(details.getAdjAmount()*-1);

		    			}else{
		    				details.setAdjType("DEBIT MEMO");
		    				details.setAdjAmount(Double.parseDouble(values[x][4]));

		    			}



	    			}else{
                                    //set date
                                    details.setAdjDate( Common.convertStringToSQLDate(values[x][1].trim()));
                                    //set amount


                                    if(values[x][4].contains("-")){
                                            details.setAdjType("BANK CHARGE");
                                            details.setAdjAmount(Double.parseDouble(values[x][4]));
                                            details.setAdjAmount(details.getAdjAmount()*-1);
                                    }else{
                                            details.setAdjType("INTEREST");
                                            details.setAdjAmount(Double.parseDouble(values[x][4]));
                                    }


	    			}


 	    			System.out.println(user.getUserName() + " the name");
 	    			//set reference number from cheque number
	    			details.setAdjReferenceNumber(values[x][2]);
	    			//set description from Narrative
	    			details.setAdjMemo(values[x][3]);
 	    			//entry the data into adjustment
 	    			details.setAdjCreatedBy(user.getUserName());

 	 	           	details.setAdjDateCreated(new java.util.Date());

 	 	            details.setAdjApprovalStatus(null);
 	 	            details.setAdjLastModifiedBy(user.getUserName());
 	 	            details.setAdjDateLastModified(new java.util.Date());



 	    			//posting data in adjusment except bank charge and interest

 	    			if(details.getAdjType()=="CREDIT MEMO" || details.getAdjType()=="DEBIT MEMO"){
 	    				Integer ADJ_CODE = ejbAdjEntry.saveCmAdjEntry(details, actionForm.getBankAccountName(), "", "",actionForm.getCurrency(), false	,0 , new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

 	    				ejbAdjPost.executeCmAdjPost(ADJ_CODE, user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

 	    			}

 	    			//save as draft interest and bank charge
 	    			if(details.getAdjType()=="INTEREST" || details.getAdjType()=="BANK CHARGE"){
 	    				Integer ADJ_CODE = ejbAdjEntry.saveCmAdjEntry(details, actionForm.getBankAccountName(),"", "", actionForm.getCurrency(), true, 0, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


 	    			}


	    		}

	    		//	ejbAdjPost.executeCmAdjPost(ADJ_CODE, USR_NM, AD_BRNCH, AD_CMPNY);

	    	 }


	    	 catch (GlobalDocumentNumberNotUniqueException ex) {

	            	errors.add(ActionMessages.GLOBAL_MESSAGE,
	            		new ActionMessage("adjustmentEntry.error.documentNumberNotUnique"));

	            } catch (GlobalRecordAlreadyDeletedException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.recordAlreadyDeleted"));

	           } catch (GlobalConversionDateNotExistException ex) {

	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.conversionDateNotExist"));

	           } catch (GlobalTransactionAlreadyApprovedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyApproved"));

	           } catch (GlobalTransactionAlreadyPendingException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPending"));

	           } catch (GlobalTransactionAlreadyPostedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyPosted"));

	           } catch (GlobalTransactionAlreadyVoidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.transactionAlreadyVoid"));

	           } catch (GlobalNoApprovalRequesterFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.noApprovalRequesterFound"));

	           } catch (GlobalNoApprovalApproverFoundException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.noApprovalApproverFound"));

	           } catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.journalNotBalance"));

	           } catch (GlobalBranchAccountNumberInvalidException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("adjustmentEntry.error.branchAccountNumberInvalid"));

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in CmAdjustmentEntryAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage");

	               }

	            }





/*******************************************************
   -- Cm BR Reconcile Action --
*******************************************************/


	     } else if (request.getParameter("reconcileButton") != null &&
	    		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

	    	 boolean autoAdjustment = actionForm.getCreateAutoAdjustment();

	    	 CmAdjustmentDetails adjustmentDetails = null;
	    	 CmAdjustmentDetails interestDetails = null;
	    	 CmAdjustmentDetails serviceChargeDetails = null;

	    	 ArrayList depositInTransitList = new ArrayList();
	    	 ArrayList outstandingCheckList = new ArrayList();

	    	 boolean IsNotPostedDetected = false;


	    	 // get adjustment details

	    	 if (Common.convertStringMoneyToDouble(actionForm.getDifference(), precisionUnit) != 0d) {

	    		 adjustmentDetails = new CmAdjustmentDetails();
	    		 adjustmentDetails.setAdjDate(Common.convertStringToSQLDate(actionForm.getAdjustmentDate()));
	    		 adjustmentDetails.setAdjConversionDate(Common.convertStringToSQLDate(actionForm.getAdjustmentConversionDate()));
	    		 adjustmentDetails.setAdjConversionRate(Common.convertStringMoneyToDouble(actionForm.getAdjustmentConversionRate(), Constants.MONEY_RATE_PRECISION));
	    		 adjustmentDetails.setAdjCreatedBy(user.getUserName());
	    		 adjustmentDetails.setAdjDateCreated(new java.util.Date());
	    		 adjustmentDetails.setAdjLastModifiedBy(user.getUserName());
	    		 adjustmentDetails.setAdjDateLastModified(new java.util.Date());
	    		 adjustmentDetails.setAdjPostedBy(user.getUserName());
	    		 adjustmentDetails.setAdjDatePosted(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	    		 adjustmentDetails.setAdjDateReconciled(Common.convertStringToSQLDate(actionForm.getReconcileDate()));

	    	 }

	    	 // get interest details

	    	 if (Common.convertStringMoneyToDouble(actionForm.getInterestEarned(), precisionUnit) != 0) {

	    		 interestDetails = new CmAdjustmentDetails();
	    		 interestDetails.setAdjDate(Common.convertStringToSQLDate(actionForm.getInterestDate()));
	    		 interestDetails.setAdjConversionDate(Common.convertStringToSQLDate(actionForm.getInterestConversionDate()));
	    		 interestDetails.setAdjConversionRate(Common.convertStringMoneyToDouble(actionForm.getInterestConversionRate(), Constants.MONEY_RATE_PRECISION));
	    		 interestDetails.setAdjAmount(Common.convertStringMoneyToDouble(actionForm.getInterestEarned(), precisionUnit));
	    		 interestDetails.setAdjReferenceNumber(actionForm.getInterestReferenceNumber());
	    		 interestDetails.setAdjCreatedBy(user.getUserName());
	    		 interestDetails.setAdjDateCreated(new java.util.Date());
	    		 interestDetails.setAdjLastModifiedBy(user.getUserName());
	    		 interestDetails.setAdjDateLastModified(new java.util.Date());
	    		 interestDetails.setAdjPostedBy(user.getUserName());
	    		 interestDetails.setAdjDatePosted(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	    		 interestDetails.setAdjDateReconciled(Common.convertStringToSQLDate(actionForm.getReconcileDate()));

	    	 }

	    	 // get service charge details

	    	 if (Common.convertStringMoneyToDouble(actionForm.getServiceCharge(), precisionUnit) != 0) {

	    		 serviceChargeDetails = new CmAdjustmentDetails();
	    		 serviceChargeDetails.setAdjDate(Common.convertStringToSQLDate(actionForm.getServiceChargeDate()));
	    		 serviceChargeDetails.setAdjConversionDate(Common.convertStringToSQLDate(actionForm.getServiceChargeConversionDate()));
	    		 serviceChargeDetails.setAdjConversionRate(Common.convertStringMoneyToDouble(actionForm.getServiceChargeConversionRate(), Constants.MONEY_RATE_PRECISION));
	    		 serviceChargeDetails.setAdjAmount(Common.convertStringMoneyToDouble(actionForm.getServiceCharge(), precisionUnit));
	    		 serviceChargeDetails.setAdjReferenceNumber(actionForm.getServiceChargeReferenceNumber());
	    		 serviceChargeDetails.setAdjCreatedBy(user.getUserName());
	    		 serviceChargeDetails.setAdjDateCreated(new java.util.Date());
	    		 serviceChargeDetails.setAdjLastModifiedBy(user.getUserName());
	    		 serviceChargeDetails.setAdjDateLastModified(new java.util.Date());
	    		 serviceChargeDetails.setAdjPostedBy(user.getUserName());
	    		 serviceChargeDetails.setAdjDatePosted(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	    		 serviceChargeDetails.setAdjDateReconciled(Common.convertStringToSQLDate(actionForm.getReconcileDate()));

	    	 }

	    	 // get cleared deposits

	    	 Object[] cmDINObjectList = actionForm.getCmDINList();

	    	 for(int i=0; i<actionForm.getCmDINListSize(); i++) {

	    		 //if ((request.getParameter("cmDINList[" + i + "].depositMark")) != null) {
	    		 if (((CmBankReconciliationDepositList)cmDINObjectList[i]).getDepositMark() == true) {

	    			 CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();
	    			 mdetails.setBrCode(((CmBankReconciliationDepositList)cmDINObjectList[i]).getDepositCode());
	    			 mdetails.setBrType(((CmBankReconciliationDepositList)cmDINObjectList[i]).getDepositType());
	    			 mdetails.setBrDateCleared(Common.convertStringToSQLDate(((CmBankReconciliationDepositList)cmDINObjectList[i]).getDateCleared()));
	    			 depositInTransitList.add(mdetails);

	    		 }

	    	 }

	    	 // get bank debit

	    	 Object[] cmBDObjectList = actionForm.getCmBDList();

	    	 for(int i=0; i<actionForm.getCmBDListSize(); i++) {

	    		 //if ((request.getParameter("cmOCList[" + i + "].checkMark")) != null) {
	    		 if (((CmBankReconciliationBankDebitList)cmBDObjectList[i]).getBankDebitMark() == true) {

	    			 if(((CmBankReconciliationBankDebitList)cmBDObjectList[i]).getPosted() == "No"){


	    				 IsNotPostedDetected =true;



	    			 }
	    			 CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();
	    			 mdetails.setBrCode(((CmBankReconciliationBankDebitList)cmBDObjectList[i]).getBankDebitCode());
	    			 mdetails.setBrType(((CmBankReconciliationBankDebitList)cmBDObjectList[i]).getBankDebitType());
	    			 mdetails.setBrDateCleared(Common.convertStringToSQLDate(((CmBankReconciliationBankDebitList)cmBDObjectList[i]).getDateCleared()));
	    			 depositInTransitList.add(mdetails);

	    		 }

	    	 }

	    	 // get cleared checks

	    	 Object[] cmOCObjectList = actionForm.getCmOCList();

	    	 for(int i=0; i<actionForm.getCmOCListSize(); i++) {

	    		 //if ((request.getParameter("cmOCList[" + i + "].checkMark")) != null) {
	    		 if (((CmBankReconciliationCheckList)cmOCObjectList[i]).getCheckMark() == true) {


	    			 CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();
	    			 mdetails.setBrCode(((CmBankReconciliationCheckList)cmOCObjectList[i]).getCheckCode());
	    			 mdetails.setBrType(((CmBankReconciliationCheckList)cmOCObjectList[i]).getCheckType());
	    			 mdetails.setBrDateCleared(Common.convertStringToSQLDate(((CmBankReconciliationCheckList)cmOCObjectList[i]).getDateCleared()));
	    			 outstandingCheckList.add(mdetails);

	    		 }

	    	 }


	    	 // get cleared Bank Credit

	    	 Object[] cmBCObjectList = actionForm.getCmBCList();

	    	 for(int i=0; i<actionForm.getCmBCListSize(); i++) {

	    		 //if ((request.getParameter("cmDINList[" + i + "].depositMark")) != null) {
	    		 if (((CmBankReconciliationBankCreditList)cmBCObjectList[i]).getBankCreditMark() == true) {

	    			 if(((CmBankReconciliationBankCreditList)cmBCObjectList[i]).getPosted() == "No"){

	    				 IsNotPostedDetected =true;

	    			 }

	    			 CmModBankReconciliationDetails mdetails = new CmModBankReconciliationDetails();
	    			 mdetails.setBrCode(((CmBankReconciliationBankCreditList)cmBCObjectList[i]).getBankCreditCode());
	    			 mdetails.setBrType(((CmBankReconciliationBankCreditList)cmBCObjectList[i]).getBankCreditType());
	    			 mdetails.setBrDateCleared(Common.convertStringToSQLDate(((CmBankReconciliationBankCreditList)cmBCObjectList[i]).getDateCleared()));
	    			 outstandingCheckList.add(mdetails);

	    		 }

	    	 }


	    	 if(!IsNotPostedDetected){

		    	 try {

		    		 ejbBR.executeCmBankReconciliation (
		    				 Common.convertStringMoneyToDouble(actionForm.getOpeningBalance(), precisionUnit),
		    				 Common.convertStringMoneyToDouble(actionForm.getEndingBalance(), precisionUnit),
		    				 actionForm.getBankAccountName(),
		    				 adjustmentDetails, interestDetails, serviceChargeDetails,
		    				 depositInTransitList,
		    				 outstandingCheckList,

		    				 Common.convertStringToSQLDate(actionForm.getReconcileDate()),
		    				 actionForm.getCreateAutoAdjustment(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

		    	 } catch (GlobalRecordAlreadyDeletedException ex) {

		    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				 new ActionMessage("bankReconciliation.error.recordAlreadyDeleted"));

		    	 } catch (GlobalTransactionAlreadyPostedException ex) {

		    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				 new ActionMessage("bankReconciliation.error.transactionAlreadyPosted"));

		    	 } catch (GlobalTransactionAlreadyVoidException ex) {

		    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				 new ActionMessage("bankReconciliation.error.transactionAlreadyVoid"));

		    	 } catch (GlJREffectiveDateNoPeriodExistException ex) {

		    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				 new ActionMessage("bankReconciliation.error.effectiveDateNoPeriodExist"));

		    	 } catch (GlJREffectiveDatePeriodClosedException ex) {

		    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				 new ActionMessage("bankReconciliation.error.effectiveDatePeriodClosed"));

		    	 } catch (GlobalJournalNotBalanceException ex) {

		    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
		    				 new ActionMessage("bankReconciliation.error.journalNotBalance"));

		    	 } catch (EJBException ex) {

		    		 if (log.isInfoEnabled()) {

		    			 log.info("EJBException caught in CmBankReconciliationAction.execute(): " + ex.getMessage() +
		    					 " session: " + session.getId());
		    			 return mapping.findForward("cmnErrorPage");

		    		 }

		    	 }
	    	 }else{
	    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				 new ActionMessage("bankReconciliation.error.AdjustmentNotPosted"));
	    	 }
/*******************************************************
   -- Cm BR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Cm BR Bank Account Enter Action --
*******************************************************/

         } else if (request.getParameter("isBankAccountEntered") != null  &&
            request.getParameter("isBankAccountEntered").equals("true") &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 try {

        		 // get bank account details

        		 AdModBankAccountDetails mBaDetails = ejbBR.getAdBaByBaName(actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), user.getCmpCode());

        		 actionForm.setAccountBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaAvailableBalance(), precisionUnit));
        		 actionForm.setLastReconciledDate(Common.convertSQLDateToString(mBaDetails.getBaLastReconciledDate()));
        		 actionForm.setOpeningBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaLastReconciledBalance(),precisionUnit));
        		 actionForm.setClearedBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaLastReconciledBalance(),precisionUnit));
        		 actionForm.setDifference(Common.convertDoubleToStringMoney(0 - mBaDetails.getBaLastReconciledBalance(),precisionUnit));
        		 actionForm.setCurrency(mBaDetails.getBaFcName());

        		 // DEPOSIT, ADVANCES = saveCmDINList
        		 // BANK DEBIT MEMO, INTEREST = saveCmBDList

        		 String BR_NUMBER = null;

        		 actionForm.clearCmDINList();
        		 actionForm.clearCmBDList();

        		 ArrayList list = ejbBR.getCmDepositInTransitByBaName(actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        		 Iterator i = list.iterator();

        		 while (i.hasNext()) {

        			 CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();


        			 if(mdetails.getBrType().contains("RECEIPT")){

        				 CmBankReconciliationDepositList cmDINList = new CmBankReconciliationDepositList(
            					 actionForm, mdetails.getBrCode(),
            					 Common.convertSQLDateToString(mdetails.getBrDate()),
            					 mdetails.getBrDocumentNumber(),
            					 mdetails.getBrReferenceNumber(),
            					 mdetails.getBrNumber(),
            					 mdetails.getBrName(),
            					 mdetails.getBrType(),
            					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit),
            					 actionForm.getReconcileDate());


        				 cmDINList.setDateCleared(Common.convertSQLDateToString(mdetails.getBrDate()));
            			 actionForm.saveCmDINList(cmDINList);


        			 }


        			 System.out.println("mdetails.getBrType()="+mdetails.getBrType());
        			 if(mdetails.getBrType().equals("DEBIT MEMO")  || mdetails.getBrType().equals("ADVANCE") || mdetails.getBrType().equals("INTEREST") || mdetails.getBrType().equals("FUND TRANSFER IN")){


        				 System.out.println("mdetails.getBrDocumentNumber()="+mdetails.getBrDocumentNumber());

        				 CmBankReconciliationBankDebitList cmBDList = new CmBankReconciliationBankDebitList(
            					 actionForm,
            					 mdetails.getBrCode(),
            					 Common.convertSQLDateToString(mdetails.getBrDate()),
            					 mdetails.getBrDocumentNumber(),
            					 mdetails.getBrReferenceNumber(),
            					 mdetails.getBrNumber(),
            					 mdetails.getBrName(),
            					 mdetails.getBrType(),
            					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit),
            					 actionForm.getReconcileDate(),"Yes");



        				 cmBDList.setDateCleared(Common.convertSQLDateToString(mdetails.getBrDate()));
        				 actionForm.saveCmBDList(cmBDList);

        			 }


        		 }

        		// get draft of INTEREST

        		 try{
        			 list = ejbFindAdj.getCmAdjByCriteria(getAdjDraftCriteria(actionForm.getBankAccountName(),"INTEREST"),"adj.adjDate",0,0,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


        			 System.out.println("list size is : "+ list.size());
        			 i = list.iterator();

            		 while (i.hasNext()) {

            			 CmAdjustmentDetails mdetails = (CmAdjustmentDetails)i.next();

            			 BR_NUMBER = mdetails.getAdjReferenceNumber();

            			 CmBankReconciliationBankDebitList cmBCList = new CmBankReconciliationBankDebitList(
            					 actionForm, mdetails.getAdjCode(),
            					 Common.convertSQLDateToString(mdetails.getAdjDate()),
            					 mdetails.getAdjDocumentNumber(),
            					 mdetails.getAdjReferenceNumber(),
            					 "",
            					 "",
            					 mdetails.getAdjType(),
            					 Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit),
            					 actionForm.getReconcileDate(),"No");


            			 cmBCList.setDateCleared(Common.convertSQLDateToString(mdetails.getAdjDate()));
        				 actionForm.saveCmBDList(cmBCList);

            		 }
        		 }catch(GlobalNoRecordFoundException ex){


        		 }


        		// CHECK = saveCmOCList
        		 // BANK CREDIT MEMO, BANK CHARGE = saveCmBCList

        		 actionForm.clearCmOCList();
        		 actionForm.clearCmBCList();

        		 list = ejbBR.getCmOutstandingCheckByBaName (actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        		 i = list.iterator();

        		 while (i.hasNext()) {

        			 CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();


        			 if(mdetails.getBrType().equals("CHECK")){
        				 CmBankReconciliationCheckList cmOCList = new CmBankReconciliationCheckList(
            					 actionForm, mdetails.getBrCode(),
            					 Common.convertSQLDateToString(mdetails.getBrDate()),
            					 mdetails.getBrDocumentNumber(),
            					 mdetails.getBrReferenceNumber(),
            					 mdetails.getBrNumber(),
            					 mdetails.getBrName(),
            					 mdetails.getBrType(),
            					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit), actionForm.getReconcileDate());



        				 actionForm.saveCmOCList(cmOCList);

        			 }


        			 if(mdetails.getBrType().equals("CREDIT MEMO") || mdetails.getBrType().equals("BANK CHARGE") || mdetails.getBrType().equals("FUND TRANSFER OUT")){
        				    System.out.println("mdetails.getBrDate()="+mdetails.getBrDate());

        				    CmBankReconciliationBankCreditList cmBCList = new CmBankReconciliationBankCreditList(
            					 actionForm,
            					 mdetails.getBrCode(),
            					 Common.convertSQLDateToString(mdetails.getBrDate()),
            					 mdetails.getBrDocumentNumber(),
            					 mdetails.getBrReferenceNumber(),
            					 mdetails.getBrNumber(),
            					 mdetails.getBrName(),
            					 mdetails.getBrType(),
            					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit), actionForm.getReconcileDate(),"Yes");

        				 cmBCList.setDateCleared(Common.convertSQLDateToString(mdetails.getBrDate()));
        				 actionForm.saveCmBCList(cmBCList);

        			 }

        		 }

        		 // get draft of BANK CHARGE

        		 try{
	        		 list = ejbFindAdj.getCmAdjByCriteria(getAdjDraftCriteria(actionForm.getBankAccountName(),"BANK CHARGE"),"adj.adjDate",0,0,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

	        		 System.out.println("the count is : " + list.size());
	        		 i = list.iterator();

	        		 while (i.hasNext()) {
	        			 CmAdjustmentDetails mdetails = (CmAdjustmentDetails)i.next();

	        			 BR_NUMBER = mdetails.getAdjReferenceNumber();

	        			 CmBankReconciliationBankCreditList cmBDList = new CmBankReconciliationBankCreditList(
	        					 actionForm,
	        					 mdetails.getAdjCode(),
	        					 Common.convertSQLDateToString(mdetails.getAdjDate()),
	        					 mdetails.getAdjDocumentNumber(),
	        					 mdetails.getAdjReferenceNumber(),
	        					 "",
	        					 "",
	        					 mdetails.getAdjType(),
	        					 Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit),
	        					 actionForm.getReconcileDate(),"No");

	        			 cmBDList.setDateCleared(Common.convertSQLDateToString(mdetails.getAdjDate()));
	    				 actionForm.saveCmBCList(cmBDList);

	        		 }
        		 }catch(GlobalNoRecordFoundException ex){}

        		 actionForm.setAdjustmentDate(actionForm.getReconcileDate());
        		 actionForm.setInterestDate(actionForm.getReconcileDate());
        		 actionForm.setServiceChargeDate(actionForm.getReconcileDate());

        	 } catch (GlobalRecordAlreadyExistException ex) {

	    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				 new ActionMessage("bankReconciliation.error.recordAlreadyExist"));
	    		 saveErrors(request, new ActionMessages(errors));

	    	 } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmBankReconciliationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            return(mapping.findForward("cmBankReconciliation"));

 /*******************************************************
    -- Cm BR Reconcile Date Enter Action --
 *******************************************************/

      } else if (request.getParameter("isRecompute") != null ) {

    	  try {
    		  
    		  String bankAccountName = request.getParameter("baName");
    		  
    		  ejbBR.recomputeAccountBalanceByBankCode(bankAccountName,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
    		  
    		  
    	  }catch(Exception ex) {
    		  if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmBankReconciliationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }
    	  }



    	  return(mapping.findForward("cmBankReconciliation"));
/*******************************************************
   -- Cm BR Reconcile Date Enter Action --
*******************************************************/

         } else if (request.getParameter("isReconcileDateEntered") != null  &&
        		 request.getParameter("isReconcileDateEntered").equals("true") &&
        		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 try {

        		 if(actionForm.getBankAccountName() != null || actionForm.getBankAccountName().length() > 0) {

            		 // get bank account details

            		 AdModBankAccountDetails mBaDetails = ejbBR.getAdBaByBaName(actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), user.getCmpCode());

            		 actionForm.setAccountBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaAvailableBalance(), precisionUnit));
            		 actionForm.setLastReconciledDate(Common.convertSQLDateToString(mBaDetails.getBaLastReconciledDate()));
            		 actionForm.setOpeningBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaLastReconciledBalance(),precisionUnit));
            		 actionForm.setClearedBalance(Common.convertDoubleToStringMoney(mBaDetails.getBaLastReconciledBalance(),precisionUnit));
            		 actionForm.setDifference(Common.convertDoubleToStringMoney(0 - mBaDetails.getBaLastReconciledBalance(),precisionUnit));
            		 actionForm.setCurrency(mBaDetails.getBaFcName());

            		 // DEPOSIT, ADVANCES = saveCmDINList
            		 // BANK DEBIT MEMO, INTEREST = saveCmBDList

            		 String BR_NUMBER = null;

            		 actionForm.clearCmDINList();
            		 actionForm.clearCmBDList();

            		 ArrayList list = ejbBR.getCmDepositInTransitByBaName(actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		 Iterator i = list.iterator();

            		 while (i.hasNext()) {

            			 CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();


            			 if(mdetails.getBrType().contains("RECEIPT")){

            				 CmBankReconciliationDepositList cmDINList = new CmBankReconciliationDepositList(
                					 actionForm, mdetails.getBrCode(),
                					 Common.convertSQLDateToString(mdetails.getBrDate()),
                					 mdetails.getBrDocumentNumber(),
                					 mdetails.getBrReferenceNumber(),
                					 mdetails.getBrNumber(),
                					 mdetails.getBrName(),
                					 mdetails.getBrType(),
                					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit),
                					 actionForm.getReconcileDate());



                			 actionForm.saveCmDINList(cmDINList);


            			 }

            			 if(mdetails.getBrType().equals("DEBIT MEMO")  || mdetails.getBrType().equals("ADVANCE") || mdetails.getBrType().equals("INTEREST") || mdetails.getBrType().equals("FUND TRANSFER IN")){


            				 CmBankReconciliationBankDebitList cmBDList = new CmBankReconciliationBankDebitList(
                					 actionForm, mdetails.getBrCode(),
                					 Common.convertSQLDateToString(mdetails.getBrDate()),
                					 mdetails.getBrDocumentNumber(),
                					 mdetails.getBrReferenceNumber(),
                					 mdetails.getBrNumber(),
                					 mdetails.getBrName(),
                					 mdetails.getBrType(),
                					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit), actionForm.getReconcileDate(),"Yes");



            				 cmBDList.setDateCleared(Common.convertSQLDateToString(mdetails.getBrDate()));
            				 actionForm.saveCmBDList(cmBDList);

            			 }


            		 }

            		// get draft of INTEREST

            		 try{
            			 list = ejbFindAdj.getCmAdjByCriteria(getAdjDraftCriteria(actionForm.getBankAccountName(),"INTEREST"),"adj.adjDate",0,0,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());


            			 System.out.println("list size is : "+ list.size());
            			 i = list.iterator();

                		 while (i.hasNext()) {

                			 CmAdjustmentDetails mdetails = (CmAdjustmentDetails)i.next();

                			 BR_NUMBER = mdetails.getAdjReferenceNumber();

                			 CmBankReconciliationBankDebitList cmBCList = new CmBankReconciliationBankDebitList(
                					 actionForm, mdetails.getAdjCode(),
                					 Common.convertSQLDateToString(mdetails.getAdjDate()),
                					 mdetails.getAdjDocumentNumber(),
                					 mdetails.getAdjReferenceNumber(),
                					 "",
                					 "",
                					 mdetails.getAdjType(),
                					 Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit),
                					 actionForm.getReconcileDate(),"No");


                			 cmBCList.setDateCleared(Common.convertSQLDateToString(mdetails.getAdjDate()));
            				 actionForm.saveCmBDList(cmBCList);

                		 }
            		 }catch(GlobalNoRecordFoundException ex){


            		 }


            		// CHECK = saveCmOCList
            		 // BANK CREDIT MEMO, BANK CHARGE = saveCmBCList

            		 actionForm.clearCmOCList();
            		 actionForm.clearCmBCList();

            		 list = ejbBR.getCmOutstandingCheckByBaName (actionForm.getBankAccountName(), Common.convertStringToSQLDate(actionForm.getReconcileDate()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		 i = list.iterator();

            		 while (i.hasNext()) {

            			 CmModBankReconciliationDetails mdetails = (CmModBankReconciliationDetails)i.next();


            			 if(mdetails.getBrType().equals("CHECK")){
            				 CmBankReconciliationCheckList cmOCList = new CmBankReconciliationCheckList(
                					 actionForm, mdetails.getBrCode(),
                					 Common.convertSQLDateToString(mdetails.getBrDate()),
                					 mdetails.getBrDocumentNumber(),
                					 mdetails.getBrReferenceNumber(),
                					 mdetails.getBrNumber(),
                					 mdetails.getBrName(),
                					 mdetails.getBrType(),
                					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit), actionForm.getReconcileDate());



            				 actionForm.saveCmOCList(cmOCList);

            			 }


            			 if(mdetails.getBrType().equals("CREDIT MEMO") || mdetails.getBrType().equals("BANK CHARGE") || mdetails.getBrType().equals("FUND TRANSFER OUT")){

            				 CmBankReconciliationBankCreditList cmBCList = new CmBankReconciliationBankCreditList(
                					 actionForm, mdetails.getBrCode(),
                					 Common.convertSQLDateToString(mdetails.getBrDate()),
                					 mdetails.getBrDocumentNumber(),
                					 mdetails.getBrReferenceNumber(),
                					 mdetails.getBrNumber(),
                					 mdetails.getBrName(),
                					 mdetails.getBrType(),
                					 Common.convertDoubleToStringMoney(mdetails.getBrAmount(), precisionUnit), actionForm.getReconcileDate(),"Yes");

            				 cmBCList.setDateCleared(Common.convertSQLDateToString(mdetails.getBrDate()));
            				 actionForm.saveCmBCList(cmBCList);

            			 }

            		 }

            		 // get draft of BANK CHARGE

            		 try{
    	        		 list = ejbFindAdj.getCmAdjByCriteria(getAdjDraftCriteria(actionForm.getBankAccountName(),"BANK CHARGE"),"adj.adjDate",0,0,new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

    	        		 System.out.println("the count is : " + list.size());
    	        		 i = list.iterator();

    	        		 while (i.hasNext()) {
    	        			 CmAdjustmentDetails mdetails = (CmAdjustmentDetails)i.next();

    	        			 BR_NUMBER = mdetails.getAdjReferenceNumber();

    	        			 CmBankReconciliationBankCreditList cmBDList = new CmBankReconciliationBankCreditList(
    	        					 actionForm,
    	        					 mdetails.getAdjCode(),
    	        					 Common.convertSQLDateToString(mdetails.getAdjDate()),
    	        					 mdetails.getAdjDocumentNumber(),
    	        					 mdetails.getAdjReferenceNumber(),
    	        					 "",
    	        					 "",
    	        					 mdetails.getAdjType(),
    	        					 Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit),
    	        					 actionForm.getReconcileDate(),"No");

    	        			 cmBDList.setDateCleared(Common.convertSQLDateToString(mdetails.getAdjDate()));
    	    				 actionForm.saveCmBCList(cmBDList);

    	        		 }
            		 }catch(GlobalNoRecordFoundException ex){}

            		 actionForm.setAdjustmentDate(actionForm.getReconcileDate());
            		 actionForm.setInterestDate(actionForm.getReconcileDate());
            		 actionForm.setServiceChargeDate(actionForm.getReconcileDate());



        		 }

        	 } catch (GlobalRecordAlreadyExistException ex) {

	    		 errors.add(ActionMessages.GLOBAL_MESSAGE,
	    				 new ActionMessage("bankReconciliation.error.recordAlreadyExist"));
	    		 saveErrors(request, new ActionMessages(errors));

	    	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmBankReconciliationAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage");

        		 }

        	 }

             return(mapping.findForward("cmBankReconciliation"));

/*******************************************************
   -- Ar PM Load Action --
*******************************************************/

         }

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmBankReconciliation");

            }

            ArrayList list = null;
            Iterator i = null;

            actionForm.clearCmDINList();
            actionForm.clearCmOCList();
            actionForm.clearCmBCList();
            actionForm.clearCmBDList();



	        try {

	    	   actionForm.clearBankAccountNameList();

	           list = ejbBR.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

               if (list == null || list.size() == 0) {

                  actionForm.setBankAccountNameList(Constants.GLOBAL_NO_RECORD_FOUND);

               } else {

                  i = list.iterator();

                  while (i.hasNext()) {

                     actionForm.setBankAccountNameList((String)i.next());

                  }

               }

               actionForm.clearCurrencyList();

            	list = ejbBR.getGlFcAllWithDefault(user.getCmpCode());

            	if (list == null  || list.size() == 0) {

            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mdetails = (GlModFunctionalCurrencyDetails)i.next();

            		    if (mdetails.getFcSob() == 1) {

            		    	actionForm.setCurrencyList(mdetails.getFcName());
            		    	actionForm.setCurrency(mdetails.getFcName());

            		    } else {

            		    	actionForm.setCurrencyList(mdetails.getFcName());

            		    }

            		}

            	}

	        } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmBankReconciliationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage");

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("reconcileButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);

	        if (actionForm.getTableType() == null) {

	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

            }

            actionForm.setPageState(Constants.PAGE_STATE_SAVE);

            return(mapping.findForward("cmBankReconciliation"));

         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in CmBankReconciliationAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }




   }


   private boolean CheckBankStatementData(){


	   return true;

   }


   private HashMap getAdjDraftCriteria(String BankAccount,String AdjType){


	   HashMap criteria = new HashMap();


	   criteria.put("bankAccount",BankAccount);
	   criteria.put("type",AdjType);
	   criteria.put("approvalStatus", "DRAFT");
	   criteria.put("adjustmentVoid", new Byte((byte)0));


	   return criteria;
   }


}