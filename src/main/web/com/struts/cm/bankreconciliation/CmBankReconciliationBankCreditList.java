package com.struts.cm.bankreconciliation;

import java.io.Serializable;

public class CmBankReconciliationBankCreditList implements Serializable{
	
	private Integer bankCreditCode = null;
	   private String bankCreditDate = null;
	   private String bankCreditDocumentNumber = null;
	   private String bankCreditReferenceNumber = null;
	   private String bankCreditNumber = null;
	   private String bankCreditPayee = null;
	   private String bankCreditType = null;
	   private String bankCreditAmount = null;

	   private boolean bankCreditMark = false;
	   private String dateCleared = null;
	   private String posted = "Yes";
	   
	   private CmBankReconciliationForm parentBean;
	   
	   
	 
	   
	   
	   public CmBankReconciliationBankCreditList(CmBankReconciliationForm parentBean,
			      Integer bankCreditCode,
			      String bankCreditDate,
			      String bankCreditDocumentNumber,
			      String bankCreditReferenceNumber,
			      String bankCreditNumber,
			      String bankCreditPayee,
			      String bankCreditType,
			      String bankCreditAmount,
			      String dateCleared,
			      String posted) {

			      this.bankCreditCode = bankCreditCode;
			      this.bankCreditDate = bankCreditDate;
			      this.bankCreditDocumentNumber = bankCreditDocumentNumber;
			      this.bankCreditReferenceNumber = bankCreditReferenceNumber;
			      this.bankCreditNumber = bankCreditNumber;
			      this.bankCreditPayee = bankCreditPayee;      
			      this.bankCreditType = bankCreditType;
			      this.bankCreditAmount = bankCreditAmount;
			      this.dateCleared = dateCleared;
			      this.posted = posted;
			      

			   }
	   
	   	public Integer getBankCreditCode() {

		      return bankCreditCode;

		   }
		   
		   public String getBankCreditDate() {
		       
		       return bankCreditDate;
		       
		   }

		   public String getBankCreditDocumentNumber() {

		      return bankCreditDocumentNumber;

		   }
		   
		   public String getBankCreditReferenceNumber() {

			      return bankCreditReferenceNumber;

			   }
		   
		   public String getBankCreditNumber() {

			      return bankCreditNumber;

			   }

		   public String getBankCreditPayee() {

		      return bankCreditPayee;

		   }

		   public String getBankCreditType() {

		      return bankCreditType;

		   }
		   
		   public String getBankCreditAmount() {

		      return bankCreditAmount;

		   }
		   
		   public boolean getBankCreditMark() {
		   	
		   	  return bankCreditMark;
		   	
		   }
		   
		   public void setBankCreditMark(boolean checkMark) {
			   
			   this.bankCreditMark = checkMark;
			   
		   }
		   
		   public String getDateCleared() {
			   
			   return dateCleared;
			   
		   }

		   public void setDateCleared(String dateCleared) {

			   this.dateCleared = dateCleared;

		   }
		   public String getPosted() {
			   
			   return posted;
			   
		   }
		   public void setPosted(String posted){
			   
			   this.posted = posted;
		   }
}
