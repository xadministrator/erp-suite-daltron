package com.struts.cm.bankreconciliation;

import java.io.Serializable;

public class CmBankReconciliationBankDebitList implements Serializable {

	private Integer bankDebitCode = null;
	   private String bankDebitDate = null;
	   private String bankDebitDocumentNumber = null;
	   private String bankDebitReferenceNumber = null;
	   private String bankDebitNumber = null;
	   private String bankDebitPayee = null;
	   private String bankDebitType = null;
	   private String bankDebitAmount = null;

	   private boolean bankDebitMark = false;
	   private String dateCleared = null;
	   private String posted = "Yes";
	   
	   private CmBankReconciliationForm parentBean;
	   
	   
	   
	   
	   
	   public CmBankReconciliationBankDebitList(CmBankReconciliationForm parentBean,
			      Integer bankDebitCode,
			      String bankDebitDate,
			      String bankDebitDocumentNumber,
			      String bankDebitReferenceNumber,
			      String bankDebitNumber,
			      String bankDebitPayee,
			      String bankDebitType,
			      String bankDebitAmount,
			      String dateCleared,
			      String posted) {

			      this.bankDebitCode = bankDebitCode;
			      this.bankDebitDate = bankDebitDate;
			      this.bankDebitDocumentNumber = bankDebitDocumentNumber;
			      this.bankDebitReferenceNumber = bankDebitReferenceNumber;
			      this.bankDebitNumber = bankDebitNumber;
			      this.bankDebitPayee = bankDebitPayee;      
			      this.bankDebitType = bankDebitType;
			      this.bankDebitAmount = bankDebitAmount;
			      this.dateCleared = dateCleared;
			      this.posted = posted;

			   }
	   
	   public Integer getBankDebitCode() {

		      return bankDebitCode;

		   }
		   
		   public String getBankDebitDate() {
		       
		       return bankDebitDate;
		       
		   }

		   public String getBankDebitDocumentNumber() {

		      return bankDebitDocumentNumber;

		   }

		   
		   public String getBankDebitReferenceNumber() {

			      return bankDebitReferenceNumber;

			   }
		   
		   public String getBankDebitNumber() {

			      return bankDebitNumber;

			   }
		   
		   public String getBankDebitPayee() {

		      return bankDebitPayee;

		   }

		   public String getBankDebitType() {

		      return bankDebitType;

		   }
		   
		   public String getBankDebitAmount() {

		      return bankDebitAmount;

		   }
		   
		   public boolean getBankDebitMark() {
		   	
		   	  return bankDebitMark;
		   	
		   }
		   
		   public void setBankDebitMark(boolean checkMark) {
			   
			   this.bankDebitMark = checkMark;
			   
		   }
		   
		   public String getDateCleared() {
			   
			   return dateCleared;
			   
		   }

		   public void setDateCleared(String dateCleared) {

			   this.dateCleared = dateCleared;

		   }
		   public String getPosted() {
			   
			   return posted;
			   
		   }

		   public void setPosted(String posted) {

			   this.posted = posted;

		   }
}
