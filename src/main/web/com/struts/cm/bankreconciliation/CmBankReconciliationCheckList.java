package com.struts.cm.bankreconciliation;

import java.io.Serializable;

public class CmBankReconciliationCheckList implements Serializable {

   private Integer checkCode = null;
   private String checkDate = null;
   private String checkDocumentNumber = null;
   private String checkReferenceNumber = null;
   private String checkNumber = null;
   private String checkPayee = null;
   private String checkType = null;
   private String checkAmount = null;

   private boolean checkMark = false;
   private String dateCleared = null;
    
   private CmBankReconciliationForm parentBean;
    
   public CmBankReconciliationCheckList(CmBankReconciliationForm parentBean,
      Integer checkCode,
      String checkDate,
      String checkDocumentNumber,
      String checkReferenceNumber,
      String checkNumber,
      String checkPayee,
      String checkType,
      String checkAmount,
      String dateCleared) {

      this.checkCode = checkCode;
      this.checkDate = checkDate;
      this.checkDocumentNumber = checkDocumentNumber;
      this.checkReferenceNumber = checkReferenceNumber;
      this.checkNumber = checkNumber;
      this.checkPayee = checkPayee;      
      this.checkType = checkType;
      this.checkAmount = checkAmount;
      this.dateCleared = dateCleared;

   }
   
   public Integer getCheckCode() {

      return checkCode;

   }
   
   public String getCheckDate() {
       
       return checkDate;
       
   }

   
   
   public String getCheckDocumentNumber() {

	      return checkDocumentNumber;

	   }
   
   public String getCheckReferenceNumber() {

	      return checkReferenceNumber;

	   }
   
   public String getCheckNumber() {

	      return checkNumber;

	   }

   public String getCheckPayee() {

      return checkPayee;

   }

   public String getCheckType() {

      return checkType;

   }
   
   public String getCheckAmount() {

      return checkAmount;

   }
   
   public boolean getCheckMark() {
   	
   	  return checkMark;
   	
   }
   
   public void setCheckMark(boolean checkMark) {
	   
	   this.checkMark = checkMark;
	   
   }
   
   public String getDateCleared() {
	   
	   return dateCleared;
	   
   }

   public void setDateCleared(String dateCleared) {

	   this.dateCleared = dateCleared;

   }

}