package com.struts.cm.bankreconciliation;

import java.io.Serializable;

public class CmBankReconciliationDepositList implements Serializable {

   private Integer depositCode = null;
   private String depositDate = null;
   private String depositDocumentNumber = null;
   private String depositReferenceNumber = null;
   private String depositNumber = null;
   private String depositPayee = null;
   private String depositType = null;
   private String depositAmount = null;

   private boolean depositMark = false;
   private String dateCleared = null;
    
   private CmBankReconciliationForm parentBean;
    
   public CmBankReconciliationDepositList(CmBankReconciliationForm parentBean,
      Integer depositCode,
      String depositDate,
      String depositDocumentNumber,
      String depositReferenceNumber,
      String depositNumber,
      String depositPayee,
      String depositType,
      String depositAmount,
      String dateCleared) {

      this.depositCode = depositCode;
      this.depositDate = depositDate;
      this.depositDocumentNumber = depositDocumentNumber;
      this.depositReferenceNumber = depositReferenceNumber;
      this.depositNumber = depositNumber;
      this.depositPayee = depositPayee;      
      this.depositType = depositType;
      this.depositAmount = depositAmount;
      this.dateCleared = dateCleared;

   }
   
   public Integer getDepositCode() {

      return depositCode;

   }
   
   public String getDepositDate() {
       
       return depositDate;
       
   }

   public String getDepositNumber() {

      return depositNumber;

   }
   
   public String getDepositDocumentNumber() {

	      return depositDocumentNumber;

	   }
   
   
   public String getDepositReferenceNumber() {

	      return depositReferenceNumber;

	   }

   public String getDepositPayee() {

      return depositPayee;

   }

   public String getDepositType() {

      return depositType;

   }
   
   public String getDepositAmount() {

      return depositAmount;

   }
   
   public boolean getDepositMark() {
   	
   	  return depositMark;
   	
   }
   
   public void setDepositMark(boolean depositMark) {
	   
	   this.depositMark = depositMark;
	   
   }
   
   public String getDateCleared() {
	   
	   return dateCleared;
	   
   }
   
   public void setDateCleared(String dateCleared) {
	   
	   this.dateCleared = dateCleared;
	   
   }

}