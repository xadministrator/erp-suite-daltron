package com.struts.cm.bankreconciliation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.ap.voucherpost.ApVoucherPostList;
import com.struts.cm.fundtransferentry.CmFundTransferEntryList;
import com.struts.util.Common;
import com.struts.util.Constants;


public class CmBankReconciliationForm extends ActionForm implements Serializable {

	private String bankAccountName = null;
	private ArrayList bankAccountNameList = new ArrayList();
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String accountBalance = null;
	private String lastReconciledDate = null;
	private String reconcileDate = null;
	private String openingBalance = null;
	private String endingBalance = null;
	private String clearedBalance = null;
	private String difference = null;
	private String adjustmentDate = null;
	private String adjustmentConversionDate = null;
	private String adjustmentConversionRate = null;
	private String interestDate = null;
	private String interestEarned = null;
	private String interestReferenceNumber = null;
	private String interestConversionDate = null;
	private String interestConversionRate = null;
	private String serviceChargeDate = null;
	private String serviceCharge = null;
	private String serviceChargeReferenceNumber = null;
	private String serviceChargeConversionDate = null;
	private String serviceChargeConversionRate = null;
	private boolean isBankAccountEntered = false;
	private boolean createAutoAdjustment = false;
	private boolean isReconcileDateEntered = false;
	private boolean isUploadEntered = false;
	private boolean isValidationEntered = false;
	private String tableType = null;
	private FormFile uploadFile = null;
	private FormFile validationFile = null;
	
	private String showDetailsButton = null;
	private String hideDetailsButton = null;	
	private String reconcileButton = null;
	private String uploadButton = null;
	private String validationButton = null;
	
	private String closeButton = null;
	private String pageState = new String();
	
	private ArrayList cmDINList = new ArrayList();
	private ArrayList cmOCList = new ArrayList();  
	private ArrayList cmBCList = new ArrayList();
	private ArrayList cmBDList = new ArrayList();
	

	private String userPermission = new String();
	private String txnStatus = new String();
	


	public Object[] getCmDINList() {

		return cmDINList.toArray();
		
	}

	public Object[] getCmOCList() {

		return cmOCList.toArray();

	}
	
	public Object[] getCmBCList() {

		return cmBCList.toArray();

	}
	
	
	public Object[] getCmBDList() {

		return cmBDList.toArray();

	}
	


	public int getCmDINListSize() {

		return cmDINList.size();

	}

	public int getCmOCListSize() {

		return cmOCList.size();

	}
	
	public int getCmBCListSize() {

		return cmBCList.size();

	}
	
	public int getCmBDListSize() {

		return cmBDList.size();

	}



	public void saveCmDINList(Object newCmDINList) {

		cmDINList.add(newCmDINList);

	}

	public void saveCmOCList(Object newCmOCList) {

		cmOCList.add(newCmOCList);

	}
	
	
	public void saveCmBCList(Object newCmBCList) {

		cmBCList.add(newCmBCList);

	}
	
	public void saveCmBDList(Object newCmBDList) {

		cmBDList.add(newCmBDList);

	}
	


	public void clearCmDINList() {
		cmDINList.clear();
	}

	public void clearCmOCList() {
		cmOCList.clear();
	}  
	
	public void clearCmBCList() {
		cmBCList.clear();
	}  
	
	public void clearCmBDList() {
		cmBDList.clear();
	}  


	public void setReconcileButton(String reconcileButton) {

		this.reconcileButton = reconcileButton;

	}

	public void setCloseButton(String closeButton) {

		this.closeButton = closeButton;

	}
	
	
	public void setUploadButton(String uploadButton){
		
		this.uploadButton = uploadButton;
	}

	
	public void setValidationButton(String validationButton){
		
		this.validationButton = validationButton;
	}
	
	
	public void setShowDetailsButton(String showDetailsButton) {

		this.showDetailsButton = showDetailsButton;

	}
	
	
	
	

	public void setHideDetailsButton(String hideDetailsButton) {

		this.hideDetailsButton = hideDetailsButton;

	}

	public void setPageState(String pageState) {

		this.pageState = pageState;

	}

	public String getPageState() {

		return pageState;

	}

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}

	public void setTxnStatus(String txnStatus) {

		this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

		return userPermission;

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

	public String getBankAccountName() {

		return bankAccountName;

	}

	public void setBankAccountName(String bankAccountName) {

		this.bankAccountName = bankAccountName;

	}

	public ArrayList getBankAccountNameList() {

		return bankAccountNameList;

	}

	public void setBankAccountNameList(String bankAccountName) {

		bankAccountNameList.add(bankAccountName);

	}

	public void clearBankAccountNameList() {

		bankAccountNameList.clear();
		bankAccountNameList.add(Constants.GLOBAL_BLANK);

	}

	public String getCurrency() {

		return currency;

	}

	public void setCurrency(String currency) {

		this.currency = currency;

	}

	public ArrayList getCurrencyList() {

		return currencyList;

	}   

	public void setCurrencyList(String currency) {

		currencyList.add(currency);

	}

	public void clearCurrencyList() {

		currencyList.clear();

	}

	public String getAccountBalance() {

		return accountBalance;  	 
	}

	public void setAccountBalance(String accountBalance) {

		this.accountBalance = accountBalance;

	}

	public String getLastReconciledDate() {

		return lastReconciledDate;

	}

	public void setLastReconciledDate(String lastReconciledDate) {

		this.lastReconciledDate = lastReconciledDate;

	}

	public String getOpeningBalance() {

		return openingBalance;

	}

	public void setOpeningBalance(String openingBalance) {

		this.openingBalance = openingBalance;

	}

	public String getEndingBalance() {

		return endingBalance;

	}

	public void setEndingBalance(String endingBalance) {

		this.endingBalance = endingBalance;

	}

	public String getClearedBalance() {

		return clearedBalance;

	}

	public void setClearedBalance(String clearedBalance) {

		this.clearedBalance = clearedBalance;

	}

	public String getDifference() {

		return difference;

	}

	public void setDifference(String difference) {

		this.difference = difference;

	}

	public String getAdjustmentDate() {

		return adjustmentDate;

	}

	public void setAdjustmentDate(String adjustmentDate) {

		this.adjustmentDate = adjustmentDate;

	}

	public String getAdjustmentConversionDate() {

		return adjustmentConversionDate;

	}

	public void setAdjustmentConversionDate(String adjustmentConversionDate) {

		this.adjustmentConversionDate = adjustmentConversionDate;

	}

	public String getAdjustmentConversionRate() {

		return adjustmentConversionRate;

	}   

	public void setAdjustmentConversionRate(String adjustmentConversionRate) {

		this.adjustmentConversionRate = adjustmentConversionRate;

	}

	public String getInterestDate() {

		return interestDate;

	}

	public void setInterestDate(String interestDate) {

		this.interestDate = interestDate;

	}

	public String getInterestEarned() {

		return interestEarned;

	}   

	public void setInterestEarned(String interestEarned) {

		this.interestEarned = interestEarned;

	}

	public String getInterestReferenceNumber() {

		return interestReferenceNumber;

	}

	public void setInterestReferenceNumber(String interestReferenceNumber) {

		this.interestReferenceNumber = interestReferenceNumber;

	}

	public String getInterestConversionDate() {

		return interestConversionDate;

	}

	public void setInterestConversionDate(String interestConversionDate) {

		this.interestConversionDate = interestConversionDate;

	}

	public String getInterestConversionRate() {

		return interestConversionRate;

	}

	public void setInterestConversionRate(String interestConversionRate) {

		this.interestConversionRate = interestConversionRate;

	}

	public String getServiceChargeDate() {

		return serviceChargeDate;

	}

	public void setServiceChargeDate(String serviceChargeDate) {

		this.serviceChargeDate = serviceChargeDate;

	}

	public String getServiceCharge() {

		return serviceCharge;

	}

	public void setServiceCharge(String serviceCharge) {

		this.serviceCharge = serviceCharge;

	}

	public String getServiceChargeReferenceNumber() {

		return serviceChargeReferenceNumber;

	}

	public void setServiceChargeReferenceNumber(String serviceChargeReferenceNumber) {

		this.serviceChargeReferenceNumber = serviceChargeReferenceNumber;

	}

	public String getServiceChargeConversionDate() {

		return serviceChargeConversionDate;

	}

	public void setServiceChargeConversionDate(String serviceChargeConversionDate) {

		this.serviceChargeConversionDate = serviceChargeConversionDate;

	}

	public String getServiceChargeConversionRate() {

		return serviceChargeConversionRate;

	}

	public void setServiceChargeConversionRate(String serviceChargeConversionRate) {

		this.serviceChargeConversionRate = serviceChargeConversionRate;

	}

	public boolean getIsBankAccountEntered() {

		return isBankAccountEntered;

	}

	public void setIsBankAccountEntered(boolean isBankAccountEntered) {

		this.isBankAccountEntered = isBankAccountEntered;

	}

	public String getTableType() {

		return(tableType);

	}

	public void setTableType(String tableType) {

		this.tableType = tableType;

	}  

	public String getReconcileDate() {

		return reconcileDate;

	}

	public void setReconcileDate(String reconcileDate) {

		this.reconcileDate = reconcileDate;

	}

	public boolean getCreateAutoAdjustment() {

		return createAutoAdjustment;

	}

	public void setCreateAutoAdjustment(boolean createAutoAdjustment) {

		this.createAutoAdjustment = createAutoAdjustment;

	}

	public boolean getIsReconcileDateEntered() {

		return isReconcileDateEntered;

	}

	public void setIsReconcileDateEntered(boolean isReconcileDateEntered) {

		this.isReconcileDateEntered = isReconcileDateEntered;

	}
	
	
	
	public boolean getUploadDateEntered() {

		return isUploadEntered;

	}

	public void setIsUploadEntered(boolean isUploadEntered) {

		this.isUploadEntered = isUploadEntered;

	}
	
	public void setIsValidationEntered(boolean isValidationEntered) {

		this.isValidationEntered = isValidationEntered;

	}
	
	
	
	
	public FormFile getUploadFile(){
		return uploadFile;
	}
	public void setUploadFile(FormFile uploadFile){
		
		this.uploadFile = uploadFile;
	}
	
	
	public FormFile getValidationFile(){
		
		return validationFile;
	}
	public void setValidationFile(FormFile validationFile){
		
		this.validationFile = validationFile;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		bankAccountName = Constants.GLOBAL_BLANK;						
		accountBalance = null;
		lastReconciledDate = null;
		reconcileDate = Common.convertSQLDateToString(new Date());
		openingBalance = null;
		endingBalance = null;
		clearedBalance = null;
		difference = null;
		adjustmentDate = Common.convertSQLDateToString(new Date());
		adjustmentConversionDate = null;
		adjustmentConversionRate = "1.000000";
		interestDate = Common.convertSQLDateToString(new Date());
		interestEarned = null;
		interestReferenceNumber = null;
		interestConversionDate = null;
		interestConversionRate = "1.000000";
		serviceChargeDate = Common.convertSQLDateToString(new Date());
		serviceCharge = null;
		serviceChargeReferenceNumber = null;
		serviceChargeConversionDate = null;
		serviceChargeConversionRate = "1.000000";
		isBankAccountEntered = false;
		isReconcileDateEntered = false;
		isUploadEntered = false;
		createAutoAdjustment = false;
		reconcileButton = null;
		uploadButton = null;
		validationButton=null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;		
		uploadFile = null;
		validationFile = null;
		
		for (int i=0; i<cmDINList.size(); i++) {

			CmBankReconciliationDepositList actionList = (CmBankReconciliationDepositList)cmDINList.get(i);
			actionList.setDepositMark(false);

		}

		for (int i=0; i<cmOCList.size(); i++) {

			CmBankReconciliationCheckList actionList = (CmBankReconciliationCheckList)cmOCList.get(i);
			actionList.setCheckMark(false);

		}
		
		for (int i=0; i<cmBCList.size(); i++) {

			CmBankReconciliationBankCreditList actionList = (CmBankReconciliationBankCreditList)cmBCList.get(i);
			actionList.setBankCreditMark(false);
			
		}
		
		for (int i=0; i<cmBDList.size(); i++) {

			CmBankReconciliationBankDebitList actionList = (CmBankReconciliationBankDebitList)cmBDList.get(i);
			actionList.setBankDebitMark(false);
		}

	}

	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		if (request.getParameter("reconcileButton") != null) {
			
			if (Common.validateRequired(bankAccountName)) {

				errors.add("bankAccountName",
						new ActionMessage("bankReconciliation.error.bankAccountNameRequired"));

			}

			if (Common.validateRequired(endingBalance)) {

				errors.add("endingBalance",
						new ActionMessage("bankReconciliation.error.endingBalanceRequired"));

			}

			if (!Common.validateMoneyFormat(endingBalance)) {

				errors.add("endingBalance",
						new ActionMessage("bankReconciliation.error.endingBalanceInvalid"));

			}

			if (!Common.validateDateFormat(interestDate)) {

				errors.add("interestDate",
						new ActionMessage("bankReconciliation.error.interestDateInvalid"));

			}

			if (!Common.validateMoneyFormat(interestEarned)) {

				errors.add("interestEarned",
						new ActionMessage("bankReconciliation.error.interestEarnedInvalid"));

			}

			if (!Common.validateDateFormat(interestConversionDate)) {

				errors.add("interestConversionDate",
						new ActionMessage("bankReconciliation.error.interestConversionDateInvalid"));

			}

			if (!Common.validateMoneyFormat(interestConversionRate)) {

				errors.add("interestConversionRate",
						new ActionMessage("bankReconciliation.error.interestConversionRate"));

			}

			if (!Common.validateDateFormat(serviceChargeDate)) {

				errors.add("serviceChargeDate",
						new ActionMessage("bankReconciliation.error.serviceChargeDateInvalid"));

			}

			if (!Common.validateMoneyFormat(serviceCharge)) {

				errors.add("serviceCharge",
						new ActionMessage("bankReconciliation.error.serviceChargeInvalid"));

			}

			if (!Common.validateDateFormat(serviceChargeConversionDate)) {

				errors.add("serviceChargeConversionDate",
						new ActionMessage("bankReconciliation.error.serviceChargeConversionDateInvalid"));

			}

			if (!Common.validateMoneyFormat(serviceChargeConversionRate)) {

				errors.add("serviceChargeConversionRate",
						new ActionMessage("bankReconciliation.error.serviceChargeConversionRate"));

			}

			if (Common.validateRequired(reconcileDate)) {

				errors.add("reconcileDate",
						new ActionMessage("bankReconciliation.error.reconcileDateRequired"));

			}

			if (!Common.validateDateFormat(reconcileDate)) {

				errors.add("reconcileDate",
						new ActionMessage("bankReconciliation.error.reconcileDateInvalid"));

			}
			
			if (!Common.validateDateFromTo(lastReconciledDate, reconcileDate)) {

				errors.add("lastReconciledDate",
						new ActionMessage("bankReconciliation.error.lastReconciledDateMustBeLessThanOrEqualToReconcileDate"));

			}
			
			if (Common.convertStringMoneyToDouble(interestEarned, Constants.DEFAULT_PRECISION) != 0 && 
					!Common.validateRequired(interestDate) && !Common.validateDateFromTo(interestDate, reconcileDate)) {

				errors.add("interestDate",
						new ActionMessage("bankReconciliation.error.interestDateMustBeLessThanOrEqualToReconcileDate"));

			}
			
			if (Common.convertStringMoneyToDouble(serviceCharge, Constants.DEFAULT_PRECISION) != 0 && 
					!Common.validateRequired(serviceChargeDate) && !Common.validateDateFromTo(serviceChargeDate, reconcileDate)) {

				errors.add("serviceChargeDate",
						new ActionMessage("bankReconciliation.error.serviceChargeDateMustBeLessThanOrEqualToReconcileDate"));

			}

			Iterator i = cmDINList.iterator();

			while (i.hasNext()) {

				CmBankReconciliationDepositList brDINList = (CmBankReconciliationDepositList) i.next();

				if (!brDINList.getDepositMark()) continue;
				
				if (Common.validateRequired(brDINList.getDateCleared()) || brDINList.getDateCleared().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedRequired"));

				}

				if (!Common.validateDateFormat(brDINList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedInvalid"));

				}

				if (!Common.validateDateFromTo(brDINList.getDepositDate(), brDINList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.depositDateMustBeLessThanOrEqualToDateCleared"));

				}

				if (!Common.validateDateFromTo(brDINList.getDateCleared(), reconcileDate)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.depositDateClearedMustBeLessThanOrEqualToReconcileDate"));

				}

			}

			i = cmOCList.iterator();

			while (i.hasNext()) {

				CmBankReconciliationCheckList brOCList = (CmBankReconciliationCheckList) i.next();

				if (!brOCList.getCheckMark()) continue;

				if (Common.validateRequired(brOCList.getDateCleared()) || brOCList.getDateCleared().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedRequired"));

				}

				if (!Common.validateDateFormat(brOCList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedInvalid"));

				}

				if (!Common.validateDateFromTo(brOCList.getCheckDate(), brOCList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.checkDateMustBeLessThanOrEqualToDateCleared"));

				}

				if (!Common.validateDateFromTo(brOCList.getDateCleared(), reconcileDate)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.checkDateClearedMustBeLessThanOrEqualToReconcileDate"));

				}

			}
			
			
			i = cmBCList.iterator();

			while (i.hasNext()) {

				CmBankReconciliationBankCreditList brBCList = (CmBankReconciliationBankCreditList) i.next();
				
				

				if (!brBCList.getBankCreditMark()) continue;

				if (Common.validateRequired(brBCList.getDateCleared()) || brBCList.getDateCleared().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedRequired"));

				}

				if (!Common.validateDateFormat(brBCList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedInvalid"));

				}

				if (!Common.validateDateFromTo(brBCList.getBankCreditDate(), brBCList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.checkDateMustBeLessThanOrEqualToDateCleared"));

				}

				if (!Common.validateDateFromTo(brBCList.getDateCleared(), reconcileDate)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.checkDateClearedMustBeLessThanOrEqualToReconcileDate"));

				}

			}
			
			
			
			i = cmBDList.iterator();
			
			while (i.hasNext()) {

				CmBankReconciliationBankDebitList brBDList = (CmBankReconciliationBankDebitList) i.next();
				
				if (!brBDList.getBankDebitMark()) continue;

				if (Common.validateRequired(brBDList.getDateCleared()) || brBDList.getDateCleared().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedRequired"));

				}

				if (!Common.validateDateFormat(brBDList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.dateClearedInvalid"));

				}

				if (!Common.validateDateFromTo(brBDList.getBankDebitDate(), brBDList.getDateCleared())) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.checkDateMustBeLessThanOrEqualToDateCleared"));

				}

				if (!Common.validateDateFromTo(brBDList.getDateCleared(), reconcileDate)) {

					errors.add("dateCleared",
							new ActionMessage("bankReconciliation.error.checkDateClearedMustBeLessThanOrEqualToReconcileDate"));

				}

			}
			
			
			
			
			
			
			
			

			if(request.getParameter("createAutoAdjustment") != null && createAutoAdjustment ==  true) {
				
				if (!Common.validateDateFormat(adjustmentDate)) {

					errors.add("adjustmentDate",
							new ActionMessage("bankReconciliation.error.adjustmentDateInvalid"));

				}

				if (!Common.validateDateFormat(adjustmentConversionDate)) {

					errors.add("adjustmentConversionDate",
							new ActionMessage("bankReconciliation.error.adjustmentConversionDateInvalid"));

				}

				if (!Common.validateMoneyFormat(adjustmentConversionRate)) {

					errors.add("adjustmentConversionRate",
							new ActionMessage("bankReconciliation.error.adjustmentConversionRateInvalid"));

				}
				
				if (Common.convertStringMoneyToDouble(difference, Constants.DEFAULT_PRECISION) != 0 && 
						!Common.validateRequired(adjustmentDate) && !Common.validateDateFromTo(adjustmentDate, reconcileDate)) {

					errors.add("adjustmentDate",
							new ActionMessage("bankReconciliation.error.adjustmentDateMustBeLessThanOrEqualToReconcileDate"));

				}
				
			}

		} else if(request.getParameter("isBankAccountEntered") != null) {

			if (Common.validateRequired(bankAccountName) || bankAccountName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

				errors.add("bankAccountName",
						new ActionMessage("bankReconciliation.error.bankAccountRequired"));

			}

		}

		return errors;

	}
}