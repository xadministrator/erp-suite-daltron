package com.struts.cm.findadjustment;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.ApCHKCheckNumberNotUniqueException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.ApDirectCheckEntryController;
import com.ejb.txn.ApDirectCheckEntryControllerHome;
import com.ejb.txn.CmAdjustmentEntryController;
import com.ejb.txn.CmAdjustmentEntryControllerHome;
import com.ejb.txn.CmFindAdjustmentController;
import com.ejb.txn.CmFindAdjustmentControllerHome;
import com.struts.ar.findinvoice.ArFindInvoiceList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.ApCheckDetails;
import com.util.ApModDistributionRecordDetails;
import com.util.CmModAdjustmentDetails;

public final class CmFindAdjustmentAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmFindAdjustmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         CmFindAdjustmentForm actionForm = (CmFindAdjustmentForm)form;

         String frParam = Common.getUserPermission(user, Constants.CM_FIND_ADJUSTMENT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmFindAdjustment");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize CmFindAdjustmentController EJB
*******************************************************/

         CmFindAdjustmentControllerHome homeFA = null;
         CmFindAdjustmentController ejbFA = null;
         
         ApDirectCheckEntryControllerHome homeDC = null;
         ApDirectCheckEntryController ejbDC = null;

         CmAdjustmentEntryControllerHome homeAE = null;
         CmAdjustmentEntryController ejbAE = null;
         
         try {

            homeFA = (CmFindAdjustmentControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmFindAdjustmentControllerEJB", CmFindAdjustmentControllerHome.class);
            
            homeDC = (ApDirectCheckEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/ApDirectCheckEntryControllerEJB", ApDirectCheckEntryControllerHome.class);
            homeAE = (CmAdjustmentEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/CmAdjustmentEntryControllerEJB", CmAdjustmentEntryControllerHome.class);


         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmFindAdjustmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFA = homeFA.create();
            
            ejbDC = homeDC.create();
            
            ejbAE = homeAE.create();
            
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmFindAdjustmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbFA.getGlFcPrecisionUnit(user.getCmpCode());
                        
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in CmFindAdjustmentAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Cm FA Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("cmFindAdjustment"));
	     
/*******************************************************
   -- Cm FA Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("cmFindAdjustment"));         
                  
/*******************************************************
    -- Cm FA First Action --
*******************************************************/ 

	     } else if(request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	     	
/*******************************************************
   -- Cm FA Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Cm FA Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Cm FA Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            	            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	
	        	if (!Common.validateRequired(actionForm.getCustomerBatch())) {
	        		
	        		criteria.put("customerBatch", actionForm.getCustomerBatch());
	        		
	        	}


				if (!Common.validateRequired(actionForm.getCustomerCode())) {
	        		
	        		criteria.put("customerCode", actionForm.getCustomerCode());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccount())) {
	        		
	        		criteria.put("bankAccount", actionForm.getBankAccount());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getType())) {
	        		
	        		criteria.put("type", actionForm.getType());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	
	        	if (actionForm.getAdjustmentVoid()) {	        	
		        	   		        		
	        		criteria.put("adjustmentVoid", new Byte((byte)1));
                
                }
                
                if (!actionForm.getAdjustmentVoid()) {
                	
                	criteria.put("adjustmentVoid", new Byte((byte)0));
                	
                }

	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFA.getCmAdjSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
                        
            try {
            	
            	actionForm.clearCmFAList();
            	
            	ArrayList list = ejbFA.getCmAdjByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(), 
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
           	   // check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		CmModAdjustmentDetails mdetails = (CmModAdjustmentDetails)i.next();
            		
            		CmFindAdjustmentList cmFAList = new CmFindAdjustmentList(actionForm,
            		    mdetails.getAdjCode(),
            		    mdetails.getAdjBaName(),
            		    mdetails.getAdjType(),
            		    mdetails.getAdjDocumentNumber(),
            		    mdetails.getAdjReferenceNumber(),
            		    mdetails.getAdjCustomerCode(),
            		    mdetails.getAdjCustomerName(),
            		    Common.convertSQLDateToString(mdetails.getAdjDate()),
            		    Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit),  
            		    Common.convertDoubleToStringMoney(mdetails.getAdjAmountApplied(), precisionUnit),  
            		    Common.convertDoubleToStringMoney(mdetails.getAdjRefundAmount(), precisionUnit), 
            		    Common.convertByteToBoolean(mdetails.getAdjVoid()),
            		    Common.convertByteToBoolean(mdetails.getAdjPosted()));
            		    
            		actionForm.saveCmFAList(cmFAList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("cmFindAdjustment.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmFindAdjustmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmFindAdjustment");

            }                       

            actionForm.reset(mapping, request);
            
            return(mapping.findForward("cmFindAdjustment"));
            
/*******************************************************
	-- Cm Refund Button --
*******************************************************/                
         } else if (request.getParameter("refundButton") != null) {
        	 
        	 ArrayList cmAdjustmentCodeList = new ArrayList(); 
        	 double totalRefund = 0d;
    		 String refundReferences = "";
    		 String customerName = "";
    		 String baName ="";
    		 String fcName ="";
    		 Date cmDate = null;
    		 boolean hasChecked = false;
    		 
        	 for (int i = 0; i<actionForm.getCmFAListSize(); i++) {
        		
        		 CmFindAdjustmentList cmFAList =
        	                actionForm.getCmFAByIndex(i);
        		 

        		 if(cmFAList.getSelectRefund()){
        			 
        			 CmModAdjustmentDetails aeDetails  = ejbAE.getCmAdjByAdjCode(
        					 cmFAList.getAdjustmentCode(), user.getCmpCode());
 
        			 
        			 System.out.println("aeDetails.getAdjDocumentNumber()="+aeDetails.getAdjDocumentNumber());
        			 
                     totalRefund += (aeDetails.getAdjAmount() - aeDetails.getAdjAmountApplied() - aeDetails.getAdjRefundAmount());
                     refundReferences += aeDetails.getAdjDocumentNumber() + ":";
                     customerName = aeDetails.getAdjCustomerName();
                     baName = aeDetails.getAdjBaName();
                     fcName = aeDetails.getAdjBaFcName();
                     cmDate = aeDetails.getAdjDate();
                     
                     if((aeDetails.getAdjAmount() - aeDetails.getAdjAmountApplied() - aeDetails.getAdjRefundAmount()) > 0){
                    	 cmAdjustmentCodeList.add(aeDetails.getAdjCode());
                          
                     }
                     
                    	 
                     
                     hasChecked = true;		 
        		 }
     		 
        	 }
        	 

        	 if(!hasChecked){
        		 //if theres no checked detected
        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
                         new ActionMessage("cmFindAdjustment.error.noCheckedFound"));
        		 
        		 
        		 saveErrors(request, new ActionMessages(errors));
                 return mapping.findForward("cmFindAdjustment");
        	 }
        	 
        	 if(totalRefund > 0){
        		 
        	 
        	 ApCheckDetails details = new ApCheckDetails();
    		 
             details.setChkCode(null);           
             details.setChkDate(new java.util.Date());
             details.setChkCheckDate(new java.util.Date());
             details.setChkNumber("");
             details.setChkDocumentNumber("");
             details.setChkReferenceNumber(refundReferences);
             details.setChkBillAmount(totalRefund);
             details.setChkAmount(totalRefund);
             details.setChkCrossCheck(Common.convertBooleanToByte(false));
             details.setChkVoid(Common.convertBooleanToByte(false));
             details.setChkDescription("REFUND FROM ADVANCE: "+ refundReferences);           
             //details.setChkConversionDate(cmDate);
             details.setChkConversionRate(1d);
             
        	
             details.setChkCreatedBy(user.getUserName());
  	         details.setChkDateCreated(new java.util.Date());
  	      
             details.setChkLastModifiedBy(user.getUserName());
             details.setChkDateLastModified(new java.util.Date());
             details.setChkMemo("");
             details.setChkSupplierName(customerName);
             
             
             ArrayList list = ejbDC.getApDrBySplSupplierCodeAndTcNameAndWtcNameAndChkBillAmountAndBaName("Customer Refund",
                     "NONE", "NONE", totalRefund, baName, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());                            		
                     
             	
             
             ArrayList drList = new ArrayList();
             int lineNumber = 0;  
    		 
            Iterator x = list.iterator();
	            
	         while (x.hasNext()) {
	        	 
	        	 ApModDistributionRecordDetails mDrDetails = (ApModDistributionRecordDetails)x.next();
	        	 
	        	 ApModDistributionRecordDetails mdetails = new ApModDistributionRecordDetails();
 
	           	   mdetails.setDrLine((short)(lineNumber++));
	           	   mdetails.setDrClass(mDrDetails.getDrClass());
	           	   mdetails.setDrDebit(mDrDetails.getDrDebit());
	           	   mdetails.setDrAmount(mDrDetails.getDrAmount());
	           	   mdetails.setDrCoaAccountNumber(mDrDetails.getDrCoaAccountNumber());
	           	   
	           	   drList.add(mdetails);
           	   
	        	 
	         }
	         
	         try {
	        	 
		         Integer checkCode = ejbDC.saveApChkEntry(details, null, baName,
	            	        "NONE", "NONE", fcName, "Customer Refund", "", 
	            	        drList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	         	   
	            Iterator z = cmAdjustmentCodeList.iterator();
	            
	            while(z.hasNext()){
	            	
	            	Integer cmAdjustmentCode = (Integer)z.next();
	            	
	            	ejbAE.saveCmAdjRefundDetailsByAdjCode(details, checkCode , cmAdjustmentCode, 
	  	          			 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            }
            

    		String path = "/apDirectCheckEntry.do?forward=1" +
 			     "&checkCode=" + checkCode; 		     
 			     		      
 			return(new ActionForward(path));
             
	         } catch (GlobalRecordAlreadyDeletedException ex) {
	             	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.recordAlreadyDeleted"));
           	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.documentNumberNotUnique"));

           } catch (ApCHKCheckNumberNotUniqueException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.checkNumberNotUnique"));
                               	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.conversionDateNotExist"));           	                      	           
         	
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyApproved"));
           	
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyPending"));
           	
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyPosted"));
           	
           } catch (GlobalTransactionAlreadyVoidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.transactionAlreadyVoid"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDateNoPeriodExist"));
                    
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.effectiveDatePeriodClosed"));
                    
           } catch (GlobalJournalNotBalanceException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("directCheckEntry.error.journalNotBalance"));
           	   
           } catch (GlobalBranchAccountNumberInvalidException ex) {
              	
          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("directCheckEntry.error.branchAccountNumberInvalid"));

           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in ApDirectCheckAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
        	 
        } 

/*******************************************************
   -- Cm FA Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));


/*******************************************************
   -- Cm FA Open Action --
*******************************************************/

         } else if (request.getParameter("cmFAList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             CmFindAdjustmentList cmFAList =
                actionForm.getCmFAByIndex(actionForm.getRowSelected());

  	         String path = "/cmAdjustmentEntry.do?forward=1" +
			     "&adjustmentCode=" + cmFAList.getAdjustmentCode();
		      
			   return(new ActionForward(path));

/*******************************************************
   -- Cm FA Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmFindAdjustment");

            }
            
            actionForm.clearCmFAList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearBankAccountList();
            	
            	list = ejbFA.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountList((String)i.next());
            			
            		}
            		
            	}
            	
            	

            	
            	actionForm.clearCustomerCodeList();           	
            	
        		list = ejbFA.getArCstAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
        		
        		if (list == null || list.size() == 0) {
        			
        			actionForm.setCustomerCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
        			
        		} else {
        			
        			i = list.iterator();
        			
        			while (i.hasNext()) {
        				
        				actionForm.setCustomerCodeList((String)i.next());
        				
        			}
        			
        		}





        		actionForm.clearCustomerBatchList();           	
     		
     		list = ejbFA.getAdLvCustomerBatchAll(user.getCmpCode());
     		
     		if (list == null || list.size() == 0) {
     			
     			actionForm.setCustomerBatchList(Constants.GLOBAL_NO_RECORD_FOUND);
     			
     		} else {
     			
     			i = list.iterator();
     			
     			while (i.hasNext()) {
     				
     				actionForm.setCustomerBatchList((String)i.next());
     				
     			}
     			
     		}
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmFindAdjustmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } 
            
            if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {
            	
            	actionForm.setCustomerCode(Constants.GLOBAL_BLANK);
            	actionForm.setBankAccount(Constants.GLOBAL_BLANK);
            	actionForm.setType(Constants.GLOBAL_BLANK);
            	actionForm.setDateFrom(null);
            	actionForm.setDateTo(null);
            	actionForm.setDocumentNumberFrom(null);
            	actionForm.setDocumentNumberTo(null);
            	actionForm.setApprovalStatus("DRAFT");
            	actionForm.setPosted(Constants.GLOBAL_NO);
            	actionForm.setReferenceNumber(null);
            	actionForm.setAdjustmentVoid(false);
            	actionForm.setOrderBy("REFERENCE NUMBER");
            	
            	actionForm.setGoButton(null);  
            	actionForm.setLineCount(0);
                actionForm.setDisableNextButton(true);
                actionForm.setDisablePreviousButton(true);
                actionForm.setDisableFirstButton(true);
                actionForm.setDisableLastButton(true);
                actionForm.reset(mapping, request);
                
                HashMap criteria = new HashMap();
                criteria.put("approvalStatus", actionForm.getApprovalStatus());
                criteria.put("posted", actionForm.getPosted());
                criteria.put("adjustmentVoid", new Byte((byte)0));
                
                actionForm.setCriteria(criteria);
                
            	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
            	
            	if(request.getParameter("findDraft") != null) {
	            	
	            	return new ActionForward("/cmFindAdjustment.do?goButton=1");
	            	
	            }
	          
            } else {
            	
            	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("adjustmentVoid")) {
            		
            		Byte b = (Byte)c.get("adjustmentVoid");
            		
            		actionForm.setAdjustmentVoid(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	try {
                	
                	actionForm.clearCmFAList();
                	
                	ArrayList newList = ejbFA.getCmAdjByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(), 
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
               	   // check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	newList.remove(newList.size() - 1);
    	           	
    	           }
    	           
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
                		CmModAdjustmentDetails mdetails = (CmModAdjustmentDetails)j.next();
                		
                		CmFindAdjustmentList cmFAList = new CmFindAdjustmentList(actionForm,
                		    mdetails.getAdjCode(),
                		    mdetails.getAdjBaName(),
                		    mdetails.getAdjType(),
                		    mdetails.getAdjDocumentNumber(),
                		    mdetails.getAdjReferenceNumber(),
                		    mdetails.getAdjCustomerCode(),
                		    mdetails.getAdjCustomerName(),
                		    Common.convertSQLDateToString(mdetails.getAdjDate()),
                		    Common.convertDoubleToStringMoney(mdetails.getAdjAmount(), precisionUnit), 
                		    Common.convertDoubleToStringMoney(mdetails.getAdjAmountApplied(), precisionUnit),  
                		    Common.convertDoubleToStringMoney(mdetails.getAdjRefundAmount(), precisionUnit), 
                		    Common.convertByteToBoolean(mdetails.getAdjVoid()),
                		    Common.convertByteToBoolean(mdetails.getAdjPosted()));
                		    
         
                		actionForm.saveCmFAList(cmFAList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev next buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("cmFindAdjustment.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in CmFindAdjustmentAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }

            return(mapping.findForward("cmFindAdjustment"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          e.printStackTrace();
          if (log.isInfoEnabled()) {

             log.info("Exception caught in CmFindAdjustmentAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}