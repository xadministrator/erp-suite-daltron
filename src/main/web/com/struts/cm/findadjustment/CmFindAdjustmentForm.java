 package com.struts.cm.findadjustment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmFindAdjustmentForm extends ActionForm implements Serializable {



	private String customerCode = null;
	   private ArrayList customerCodeList = new ArrayList();

	private String customerBatch = null;
	   private ArrayList customerBatchList = new ArrayList();

   private String bankAccount = null;
   private ArrayList bankAccountList = new ArrayList();
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String referenceNumber = null;
   private String amount = null;
   private boolean adjustmentVoid = false;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();

   private String tableType = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;
   private String goButton = null;
   private String closeButton = null;
   private String pageState = new String();
   private ArrayList cmFAList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;

   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;

   private int lineCount = 0;

   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }

   public int getLineCount(){
   	  return lineCount;
   }

   public void setLineCount(int lineCount){
   	  this.lineCount = lineCount;
   }

   public CmFindAdjustmentList getCmFAByIndex(int index) {

      return((CmFindAdjustmentList)cmFAList.get(index));

   }

   public Object[] getCmFAList() {

      return cmFAList.toArray();

   }

   public int getCmFAListSize() {

      return cmFAList.size();

   }

   public void saveCmFAList(Object newCmFAList) {

      cmFAList.add(newCmFAList);

   }

   public void clearCmFAList() {
      cmFAList.clear();
   }

   public void setRowSelected(Object selectedCmFAList, boolean isEdit) {

      this.rowSelected = cmFAList.indexOf(selectedCmFAList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showCmFARow(int rowSelected) {

   }

   public void updateCmFARow(int rowSelected, Object newCmFAList) {

      cmFAList.set(rowSelected, newCmFAList);

   }

   public void deleteCmFAList(int rowSelected) {

      cmFAList.remove(rowSelected);

   }

   public void setGoButton(String goButton) {

      this.goButton = goButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setShowDetailsButton(String showDetailsButton) {

	  this.showDetailsButton = showDetailsButton;

   }

   public void setHideDetailsButton(String hideDetailsButton) {

      this.hideDetailsButton = hideDetailsButton;

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }




   public String getCustomerCode() {

	      return customerCode;

	   }

	   public void setCustomerCode(String customerCode) {

	      this.customerCode = customerCode;

	   }

	   public ArrayList getCustomerCodeList() {

	      return customerCodeList;

	   }

	   public void setCustomerCodeList(String customerCode) {

	      customerCodeList.add(customerCode);

	   }

	   public void clearCustomerCodeList() {

	      customerCodeList.clear();
	      customerCodeList.add(Constants.GLOBAL_BLANK);

	   }









	public String getCustomerBatch() {

		      return customerBatch;

		   }

		   public void setCustomerBatch(String customerBatch) {

		      this.customerBatch = customerBatch;

		   }

		   public ArrayList getCustomerBatchList() {

		      return customerBatchList;

		   }

		   public void setCustomerBatchList(String customerBatch) {

		      customerBatchList.add(customerBatch);

		   }

		   public void clearCustomerBatchList() {

			   customerBatchList.clear();
			   customerBatchList.add(Constants.GLOBAL_BLANK);

		   }






   public String getBankAccount() {

   	  return bankAccount;

   }

   public void setBankAccount(String bankAccount) {

   	  this.bankAccount = bankAccount;

   }

   public ArrayList getBankAccountList() {

   	   return bankAccountList;

   }

   public void setBankAccountList(String bankAccount) {

   	   bankAccountList.add(bankAccount);

   }

   public void clearBankAccountList() {

   	   bankAccountList.clear();
   	   bankAccountList.add(Constants.GLOBAL_BLANK);

   }

   public String getType() {

   	  return type;

   }

   public void setType(String type) {

   	  this.type = type;

   }

   public ArrayList getTypeList() {

   	  return typeList;

   }

   public void setTypeList(String type) {

   	  typeList.add(type);

   }

   public void clearTypeList() {

   	  typeList.clear();
   	  typeList.add(Constants.GLOBAL_BLANK);

   }

   public String getDateFrom() {

      return dateFrom;

   }

   public void setDateFrom(String dateFrom) {

      this.dateFrom = dateFrom;

   }

   public String getDateTo() {

   	  return dateTo;

   }

   public void setDateTo(String dateTo) {

   	  this.dateTo = dateTo;

   }

   public String getDocumentNumberFrom() {

   	  return documentNumberFrom;

   }

   public void setDocumentNumberFrom(String documentNumberFrom) {

   	  this.documentNumberFrom = documentNumberFrom;

   }

   public String getDocumentNumberTo() {

   	  return documentNumberTo;

   }

   public void setDocumentNumberTo(String documentNumberTo) {

   	  this.documentNumberTo = documentNumberTo;

   }

   public String getReferenceNumber() {

   	  return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

   	  this.referenceNumber = referenceNumber;

   }

   public String getApprovalStatus() {

   	  return approvalStatus;

   }

   public void setApprovalStatus(String approvalStatus) {

   	  this.approvalStatus = approvalStatus;

   }

   public ArrayList getApprovalStatusList() {

   	  return approvalStatusList;

   }

   public String getPosted() {

   	  return posted;

   }

   public void setPosted(String posted) {

   	  this.posted = posted;

   }

   public ArrayList getPostedList() {

   	  return postedList;

   }

   public String getAmount() {

   	  return amount;

   }

   public void setAmount(String amount) {

   	  this.amount = amount;

   }

   public boolean getAdjustmentVoid() {

   	  return adjustmentVoid;

   }

   public void setAdjustmentVoid(boolean adjustmentVoid) {

   	  this.adjustmentVoid = adjustmentVoid;

   }

   public String getOrderBy() {

   	  return orderBy;

   }

   public void setOrderBy(String orderBy) {

   	  this.orderBy = orderBy;

   }

   public ArrayList getOrderByList() {

   	  return orderByList;

   }

   public boolean getDisablePreviousButton() {

   	  return disablePreviousButton;

   }

   public void setDisablePreviousButton(boolean disablePreviousButton) {

   	  this.disablePreviousButton = disablePreviousButton;

   }

   public boolean getDisableNextButton() {

      return disableNextButton;

   }

   public void setDisableNextButton(boolean disableNextButton) {

   	  this.disableNextButton = disableNextButton;

   }

   public boolean getDisableFirstButton() {

   	  return disableFirstButton;

   }

   public void setDisableFirstButton(boolean disableFirstButton) {

   	  this.disableFirstButton = disableFirstButton;

   }

   public boolean getDisableLastButton() {

   	  return disableLastButton;

   }

   public void setDisableLastButton(boolean disableLastButton) {

   	  this.disableLastButton = disableLastButton;

   }

   public HashMap getCriteria() {

   	   return criteria;

   }

   public void setCriteria(HashMap criteria) {

   	   this.criteria = criteria;

   }

   public String getTableType() {

      return(tableType);

   }

   public void setTableType(String tableType) {

      this.tableType = tableType;

   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  adjustmentVoid = false;

      typeList.clear();
      typeList.add(Constants.GLOBAL_BLANK);
      typeList.add(Constants.CM_AE_ADVANCE);
      typeList.add(Constants.CM_AE_INTEREST);
      typeList.add(Constants.CM_AE_BANK_CHARGE);
      typeList.add(Constants.CM_AE_CREDIT_MEMO);
      typeList.add(Constants.CM_AE_DEBIT_MEMO);

      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");
      approvalStatusList.add("REJECTED");

      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add("YES");
      postedList.add("NO");

      if (orderByList.isEmpty()) {

	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("REFERENCE NUMBER");
	      orderByList.add("DOCUMENT NUMBER");

	   }

      closeButton = null;
      showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

         if (!Common.validateStringExists(bankAccountList, bankAccount)) {

            errors.add("bankAccount",
               new ActionMessage("cmFindAdjustment.error.bankAccountInvalid"));

         }

         if (!Common.validateStringExists(typeList, type)) {

            errors.add("type",
               new ActionMessage("cmFindAdjustment.error.typeInvalid"));

         }

         if (!Common.validateDateFormat(dateFrom)) {

         	errors.add("dateFrom",
               new ActionMessage("cmFindAdjustment.error.dateFromInvalid"));

         }

         if (!Common.validateDateFormat(dateTo)) {

         	errors.add("dateTo",
               new ActionMessage("cmFindAdjustment.error.dateToInvalid"));

         }



      }

      if(request.getParameter("refundButton")!=null){




      }
      return errors;

   }
}