package com.struts.cm.findadjustment;

import java.io.Serializable;

public class CmFindAdjustmentList implements Serializable {

   private Integer adjustmentCode = null;
   private String bankAccount = null;
   private String type = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String customerCode = null;
   private String customerName = null;
   private String date = null;
   private String amount = null; 
   private String creditedBalance = null; 
   private String refundAmount = null;
   private boolean adjustmentVoid = false;
   private boolean selectRefund = false;
   private boolean isRefundable = false;

       
   private CmFindAdjustmentForm parentBean;
    
   public CmFindAdjustmentList(CmFindAdjustmentForm parentBean,
      Integer adjustmentCode,
      String bankAccount,
      String type,
      String documentNumber,
      String referenceNumber,
      String customerCode,
      String customerName,
      String date,
      String amount,
      String creditedBalance,
      String refundAmount,
      boolean adjustmentVoid,
      boolean isPosted) {

      this.parentBean = parentBean;
      this.adjustmentCode = adjustmentCode;
      this.bankAccount = bankAccount;
      this.type = type;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.customerCode = customerCode;
      this.customerName = customerName;
      this.amount = amount;
      this.creditedBalance = creditedBalance;
      this.refundAmount = refundAmount;
      this.date = date;
      this.adjustmentVoid = adjustmentVoid;
      

      
      if(this.customerCode.equals("") || this.customerCode.equals(null) || !isPosted ){
    	  this.isRefundable =false;
      }else{
    	  this.isRefundable = true;
 
      }
      

   }
   
   public boolean getSelectRefund() {
	   	
   	  return selectRefund;
	   	
   }
   
   public void setSelectRefund(boolean selectRefund) {
   	
   	  this.selectRefund = selectRefund;
   	
   }
   
   public boolean getIsRefundable() {
		   	
   	  return isRefundable;
   	
   }
   
   public void setIsRefundable(boolean isRefundable) {
   	
   	  this.isRefundable = isRefundable;
   	
   } 
   
	   
   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }

   public Integer getAdjustmentCode() {

      return adjustmentCode;

   }
   
   public String getBankAccount() {
   	  
   	  return bankAccount;
   	  
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getCustomerCode() {
	   	
	   	  return customerCode;
	   	  
	   }
   
   public String getCustomerName() {
	   	
	   	  return customerName;
	   	  
	   }
   
   public String getDate() {
   	
   	  return date;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public String getCreditedBalance() {
	   	
	   	  return creditedBalance;
	   	  
	   }
   
   public String getRefundAmount() {
	   	
	   	  return refundAmount;
	   	  
	   }
   
   public boolean getAdjustmentVoid() {
   	
   	  return adjustmentVoid;
   	  
   }
   
}