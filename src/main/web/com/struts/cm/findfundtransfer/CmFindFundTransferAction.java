package com.struts.cm.findfundtransfer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.CmFindFundTransferController;
import com.ejb.txn.CmFindFundTransferControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.CmModFundTransferEntryDetails;

public final class CmFindFundTransferAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmFindFundTransferAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         CmFindFundTransferForm actionForm = (CmFindFundTransferForm)form;
      
         String frParam = Common.getUserPermission(user, Constants.CM_FIND_FUND_TRANSFER_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmFindFundTransfer");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize CmFindFundTransferController EJB
*******************************************************/

         CmFindFundTransferControllerHome homeFF = null;
         CmFindFundTransferController ejbFF = null;

         try {

            homeFF = (CmFindFundTransferControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmFindFundTransferControllerEJB", CmFindFundTransferControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmFindFundTransferAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFF = homeFF.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmFindFundTransferAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbFF.getGlFcPrecisionUnit(user.getCmpCode());
                        
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in CmFindFundTransferAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Cm FF Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("cmFindFundTransfer"));
	     
/*******************************************************
   -- Cm FF Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("cmFindFundTransfer"));         
                  
/*******************************************************
    -- Cm FF First Action --
*******************************************************/ 

	     } else if(request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	        
/*******************************************************
   -- Cm FF Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Cm FF Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Cm FF Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            	            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccountFrom())) {
	        		
	        		criteria.put("bankAccountFrom", actionForm.getBankAccountFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccountTo())) {
	        		
	        		criteria.put("bankAccountTo", actionForm.getBankAccountTo());
	        		
	        	}	
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	
	        	if (actionForm.getFundTransferVoid()) {	        	
		        	   		        		
	        		criteria.put("fundTransferVoid", new Byte((byte)1));
                
                }
                
                if (!actionForm.getFundTransferVoid()) {
                	
                	criteria.put("fundTransferVoid", new Byte((byte)0));
                	
                }

	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFF.getCmFtSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
                        
            try {
            	
            	actionForm.clearCmFFList();
            	
            	ArrayList list = ejbFF.getCmFtByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(), 
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
           	   // check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		CmModFundTransferEntryDetails mdetails = (CmModFundTransferEntryDetails)i.next();
            		
            		CmFindFundTransferList cmFFList = new CmFindFundTransferList(actionForm,
            		    mdetails.getFtCode(),
            		    mdetails.getFtAdBankAccountNameFrom(),
            		    mdetails.getFtAdBankAccountNameTo(),
            		    mdetails.getFtCurrencyFrom(),
            		    mdetails.getFtCurrencyTo(),
						mdetails.getFtDocumentNumber(),
            		    mdetails.getFtReferenceNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getFtAmount(), precisionUnit),          
            		    Common.convertSQLDateToString(mdetails.getFtDate()),
            		    Common.convertByteToBoolean(mdetails.getFtVoid()));
            		    
            		actionForm.saveCmFFList(cmFFList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findFundTransfer.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmFindFundTransferAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmFindFundTransfer");

            }                       

            actionForm.reset(mapping, request);
            
            return(mapping.findForward("cmFindFundTransfer"));     	

/*******************************************************
   -- Cm FF Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));


/*******************************************************
   -- Cm FF Open Action --
*******************************************************/

         } else if (request.getParameter("cmFFList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             CmFindFundTransferList cmFFList =
                actionForm.getCmFFByIndex(actionForm.getRowSelected());

  	         String path = "/cmFundTransferEntry.do?forward=1" +
			     "&fundTransferCode=" + cmFFList.getFundTransferCode();
		      
			   return(new ActionForward(path));

/*******************************************************
   -- Cm FF Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmFindFundTransfer");

            }
            
            actionForm.clearCmFFList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearBankAccountFromList();
            	
            	list = ejbFF.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountFromList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountFromList((String)i.next());
            			
            		}
            		
            	}            	
            	
            	actionForm.clearBankAccountToList();
            	
            	list = ejbFF.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountToList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountToList((String)i.next());
            			
            		}
            		
            	}
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmFindFundTransferAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } 
            
            if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {
            	
            	actionForm.setBankAccountFrom(Constants.GLOBAL_BLANK);
            	actionForm.setBankAccountTo(Constants.GLOBAL_BLANK);
            	actionForm.setCurrencyFrom(null);
                actionForm.setCurrencyTo(null);
            	actionForm.setDocumentNumberFrom(null);
            	actionForm.setDocumentNumberTo(null);
            	actionForm.setDateFrom(null);
            	actionForm.setDateTo(null);
            	actionForm.setApprovalStatus("DRAFT");
            	actionForm.setPosted(Constants.GLOBAL_NO);
            	actionForm.setReferenceNumber(null);
            	actionForm.setFundTransferVoid(false);
            	actionForm.setOrderBy(Constants.GLOBAL_BLANK);
            	
            	actionForm.setGoButton(null);
            	actionForm.setLineCount(0);
                actionForm.setDisableNextButton(true);
                actionForm.setDisablePreviousButton(true);
                actionForm.setDisableFirstButton(true);
                actionForm.setDisableLastButton(true);
                actionForm.reset(mapping, request);
                
                HashMap criteria = new HashMap();
                criteria.put("approvalStatus", actionForm.getApprovalStatus());
                criteria.put("posted", actionForm.getPosted());
                criteria.put("fundTransferVoid", new Byte((byte)0));
                
                actionForm.setCriteria(criteria);
                
            	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
            	
            	if(request.getParameter("findDraft") != null) {
	            	
	            	return new ActionForward("/cmFindFundTransfer.do?goButton=1");
	            	
	            }
	          
            } else {
            	
            	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("fundTransferVoid")) {
            		
            		Byte b = (Byte)c.get("fundTransferVoid");
            		
            		actionForm.setFundTransferVoid(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
            	
            	try {
                	
            		actionForm.clearCmFFList();
                	
                	ArrayList newList = ejbFF.getCmFtByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(), 
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
               	   // check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);	
    	           	  
    	           	  //remove last record
    	           	newList.remove(newList.size() - 1);
    	           	
    	           }
    	           
                	Iterator j = newList.iterator();
                	
                	while (j.hasNext()) {
                		
                		CmModFundTransferEntryDetails mdetails = (CmModFundTransferEntryDetails)j.next();
                		
                		CmFindFundTransferList cmFFList = new CmFindFundTransferList(actionForm,
                		    mdetails.getFtCode(),
                		    mdetails.getFtAdBankAccountNameFrom(),
                		    mdetails.getFtAdBankAccountNameTo(),
                		    mdetails.getFtCurrencyFrom(),
                		    mdetails.getFtCurrencyTo(),
    						mdetails.getFtDocumentNumber(),
                		    mdetails.getFtReferenceNumber(),
                		    Common.convertDoubleToStringMoney(mdetails.getFtAmount(), precisionUnit),          
                		    Common.convertSQLDateToString(mdetails.getFtDate()),
                		    Common.convertByteToBoolean(mdetails.getFtVoid()));
                		    
                		actionForm.saveCmFFList(cmFFList);
                		
                	}

                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev next buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findFundTransfer.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in CmFindFundTransferAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
            	
            }

            return(mapping.findForward("cmFindFundTransfer"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          e.printStackTrace();
          if (log.isInfoEnabled()) {

             log.info("Exception caught in CmFindFundTransferAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}