 package com.struts.cm.findfundtransfer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmFindFundTransferForm extends ActionForm implements Serializable {

   private String bankAccountFrom = null;
   private ArrayList bankAccountFromList = new ArrayList(); 
   private String bankAccountTo = null;
   private ArrayList bankAccountToList = new ArrayList();    
   private String dateFrom = null;
   private String dateTo = null;
   private String currencyFrom = null;
   private String currencyTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String referenceNumber = null;
   private String amount = null;
   private boolean fundTransferVoid = false;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String posted = null;
   private ArrayList postedList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   
   private String tableType = null;
   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String goButton = null;
   private String closeButton = null;   
   private String pageState = new String();
   private ArrayList cmFFList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public CmFindFundTransferList getCmFFByIndex(int index) {

      return((CmFindFundTransferList)cmFFList.get(index));

   }

   public Object[] getCmFFList() {

      return cmFFList.toArray();

   }

   public int getCmFFListSize() {

      return cmFFList.size();

   }

   public void saveCmFFList(Object newCmFFList) {
   	      
      cmFFList.add(newCmFFList);

   }

   public void clearCmFFList() {
      cmFFList.clear();
   }
   
   public void setRowSelected(Object selectedCmFFList, boolean isEdit) {

      this.rowSelected = cmFFList.indexOf(selectedCmFFList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showCmFFRow(int rowSelected) {

   }

   public void updateCmFFRow(int rowSelected, Object newCmFFList) {

      cmFFList.set(rowSelected, newCmFFList);

   }

   public void deleteCmFFList(int rowSelected) {

      cmFFList.remove(rowSelected);

   }   

   public void setGoButton(String goButton) {

      this.goButton = goButton;

   }
   
   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBankAccountFrom() {
   	
   	  return bankAccountFrom;
   	
   }
   
   public void setBankAccountFrom(String bankAccountFrom) {
   	
   	  this.bankAccountFrom = bankAccountFrom;
   	
   }
   
   public ArrayList getBankAccountFromList() {
   	
   	   return bankAccountFromList;
   	
   }
   
   public void setBankAccountFromList(String bankAccountFrom) {
   	
   	   bankAccountFromList.add(bankAccountFrom);
   	
   }
   
   public void clearBankAccountFromList() {
   	   	   
   	   bankAccountFromList.clear();
   	   bankAccountFromList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getBankAccountTo() {
   	
   	  return bankAccountTo;
   	
   }
   
   public void setBankAccountTo(String bankAccountTo) {
   	
   	  this.bankAccountTo = bankAccountTo;
   	
   }
   
   public ArrayList getBankAccountToList() {
   	
   	   return bankAccountToList;
   	
   }
   
   public void setBankAccountToList(String bankAccountTo) {
   	
   	   bankAccountToList.add(bankAccountTo);
   	
   }
   
   public void clearBankAccountToList() {
   	   	   
   	   bankAccountToList.clear();
   	   bankAccountToList.add(Constants.GLOBAL_BLANK);
   	
   }   

   public String getDateFrom() {

      return dateFrom;

   }

   public void setDateFrom(String dateFrom) {

      this.dateFrom = dateFrom;

   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	
   }

   public String getCurrencyFrom() {

      return currencyFrom;

   }

   public void setCurrencyFrom(String currencyFrom) {

      this.currencyFrom = currencyFrom;

   }
   
   public String getCurrencyTo() {
   	
   	  return currencyTo;
   	
   }
   
   public void setCurrencyTo(String currencyTo) {
   	
   	  this.currencyTo = currencyTo;
   	
   }
   
   public String getDocumentNumberFrom() {
   	
   	  return documentNumberFrom;
   	
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom) {
   	
   	  this.documentNumberFrom = documentNumberFrom;
   	
   }
   
   public String getDocumentNumberTo() {
   	
   	  return documentNumberTo;
   	
   }
   
   public void setDocumentNumberTo(String documentNumberTo) {
   	
   	  this.documentNumberTo = documentNumberTo;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	  this.referenceNumber = referenceNumber;
   	  
   }
   
   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }
      
   public String getPosted() {
   	
   	  return posted;
   
   }
   
   public void setPosted(String posted) {
   	
   	  this.posted = posted;
   
   }
   
   public ArrayList getPostedList() {
   	
   	  return postedList;
   
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public void setAmount(String amount) {
   	
   	  this.amount = amount;
   	  
   }
   
   public boolean getFundTransferVoid() {
   	
   	  return fundTransferVoid;
   	
   }
   
   public void setFundTransferVoid(boolean fundTransferVoid) {
   	
   	  this.fundTransferVoid = fundTransferVoid;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  fundTransferVoid = false;
   	
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add("YES");
      postedList.add("NO");
      
      
      if (orderByList.isEmpty()) {      
	      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("REFERENCE NUMBER");
	      orderByList.add("DOCUMENT NUMBER");
	      
	   }
      
      closeButton = null;
      showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
         if (!Common.validateStringExists(bankAccountFromList, bankAccountFrom)) {

            errors.add("bankAccountFrom",
               new ActionMessage("findFundTransfer.error.bankAccountFromInvalid"));

         }       
         
         if (!Common.validateStringExists(bankAccountToList, bankAccountTo)) {

            errors.add("bankAccountTo",
               new ActionMessage("findFundTransfer.error.bankAccountToInvalid"));

         }
         
         if (!Common.validateDateFormat(dateFrom)) {
         	
         	errors.add("dateFrom",
               new ActionMessage("findFundTransfer.error.dateFromInvalid"));
               
         }
         
         if (!Common.validateDateFormat(dateTo)) {
         	
         	errors.add("dateTo",
               new ActionMessage("findFundTransfer.error.dateToInvalid"));
               
         }                                      
         
      }
      return errors;

   }
}