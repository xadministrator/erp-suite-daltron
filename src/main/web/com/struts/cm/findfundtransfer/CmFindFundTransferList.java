package com.struts.cm.findfundtransfer;

import java.io.Serializable;

public class CmFindFundTransferList implements Serializable {

   private Integer fundTransferCode = null;
   private String bankAccountFrom = null;
   private String bankAccountTo = null;
   private String currencyFrom = null;
   private String currencyTo = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String amount = null;
   private String date = null;
   private boolean fundTransferVoid = false;
       
   private CmFindFundTransferForm parentBean;
    
   public CmFindFundTransferList(CmFindFundTransferForm parentBean,
      Integer fundTransferCode,
      String bankAccountFrom,
      String bankAccountTo,
      String currencyFrom,
      String currencyTo,
      String documentNumber,
      String referenceNumber,
      String amount,
      String date,
      boolean fundTransferVoid) {

      this.parentBean = parentBean;
      this.fundTransferCode = fundTransferCode;
      this.bankAccountFrom = bankAccountFrom;
      this.bankAccountTo = bankAccountTo;
      this.currencyFrom = currencyFrom;
      this.currencyTo = currencyTo;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amount = amount;
      this.date = date;
      this.fundTransferVoid = fundTransferVoid;

   }
   
   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }

   public Integer getFundTransferCode() {

      return fundTransferCode;

   }
   
   public String getBankAccountFrom() {
   	  
   	  return bankAccountFrom;
   	  
   }
   
   public String getBankAccountTo() {
   	
   	  return bankAccountTo;
   	  
   }
   
   public String getCurrencyFrom() {
   	
   	  return currencyFrom;
   	  
   }
   
   public String getCurrencyTo() {
   	
   	  return currencyTo;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public String getDate() {
   	
   	  return date;
   	  
   }
   
   public boolean getFundTransferVoid() {
   	
   	  return fundTransferVoid;
   	  
   }
   
}