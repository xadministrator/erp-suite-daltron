package com.struts.cm.fundtransferbatchsubmit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.CmFundTransferBatchSubmitController;
import com.ejb.txn.CmFundTransferBatchSubmitControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.CmModFundTransferEntryDetails;

public final class CmFundTransferBatchSubmitAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmFundTransferBatchSubmitAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         CmFundTransferBatchSubmitForm actionForm = (CmFundTransferBatchSubmitForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.CM_FUND_TRANSFER_BATCH_SUBMIT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmFundTransferBatchSubmit");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize CmFundTransferBatchSubmitController EJB
*******************************************************/

         CmFundTransferBatchSubmitControllerHome homeFBS = null;
         CmFundTransferBatchSubmitController ejbFBS = null;

         try {

            homeFBS = (CmFundTransferBatchSubmitControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmFundTransferBatchSubmitControllerEJB", CmFundTransferBatchSubmitControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmFundTransferBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFBS = homeFBS.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmFundTransferBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
         
         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbFBS.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in CmFundTransferBatchSubmitAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Cm FBS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("cmFundTransferBatchSubmit"));
	     
/*******************************************************
   -- Cm FBS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("cmFundTransferBatchSubmit"));                         

/*******************************************************
   -- Cm FBS Previous Action --
*******************************************************/ 

         } else if (request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Cm FBS Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 

/*******************************************************
   -- Cm FBS Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccountFrom())) {
	        		
	        		criteria.put("bankAccountFrom", actionForm.getBankAccountFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBankAccountTo())) {
	        		
	        		criteria.put("bankAccountTo", actionForm.getBankAccountTo());
	        		
	        	}	        	
	        	
	        	if (!Common.validateRequired(actionForm.getReferenceNumber())) {
	        		
	        		criteria.put("referenceNumber", actionForm.getReferenceNumber());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}

	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbFBS.getCmFtByCriteria(actionForm.getCriteria(),                	    
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in CmFundTransferBatchSubmitAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }        	
	     	}
 
            try {
            	
            	actionForm.clearCmFBSList();
            	
            	ArrayList list = ejbFBS.getCmFtByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		CmModFundTransferEntryDetails mdetails = (CmModFundTransferEntryDetails)i.next();
      		
            		CmFundTransferBatchSubmitList cmFBSList = new CmFundTransferBatchSubmitList(actionForm,
            		    mdetails.getFtCode(),
            		    Common.convertSQLDateToString(mdetails.getFtDate()),
            		    mdetails.getFtAdBankAccountNameFrom(),
            		    mdetails.getFtAdBankAccountNameTo(),
            		    mdetails.getFtCurrencyFrom(),
            		    mdetails.getFtCurrencyTo(),
            		    mdetails.getFtReferenceNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getFtAmount(), precisionUnit));
            		    
            		actionForm.saveCmFBSList(cmFBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("fundTransferBatchSubmit.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmFundTransferBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmFundTransferBatchSubmit");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("cmFundTransferBatchSubmit"));

/*******************************************************
   -- Cm FBS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Cm FBS BatchSubmit Action --
*******************************************************/

         } else if (request.getParameter("submitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get submit receipts
                        
		    for(int i=0; i<actionForm.getCmFBSListSize(); i++) {
		    
		       CmFundTransferBatchSubmitList actionList = actionForm.getCmFBSByIndex(i);
		    	
               if (actionList.getSubmit()) {
               	
               	     try {
               	     	
               	     	ejbFBS.executeArFtBatchSubmit(actionList.getFundTransferCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteCmFBSList(i);
               	     	i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("fundTransferBatchSubmit.error.recordAlreadyDeleted", actionList.getReferenceNumber()));
		             
		             } catch (GlobalTransactionAlreadyApprovedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("fundTransferBatchSubmit.error.transactionAlreadyApproved", actionList.getReferenceNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPendingException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("fundTransferBatchSubmit.error.transactionAlreadyPending", actionList.getReferenceNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("fundTransferBatchSubmit.error.transactionAlreadyPosted", actionList.getReferenceNumber()));
		             
		             } catch (GlobalNoApprovalRequesterFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("fundTransferBatchSubmit.error.noApprovalRequesterFound", actionList.getReferenceNumber()));
		                  
		             } catch (GlobalNoApprovalApproverFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("fundTransferBatchSubmit.error.noApprovalApproverFound", actionList.getReferenceNumber()));
		               
		             } catch (GlobalTransactionAlreadyVoidException ex) {
           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("fundTransferBatchSubmit.error.transactionAlreadyVoid", actionList.getReferenceNumber()));
		               
		             } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("fundTransferBatchSubmit.error.effectiveDateNoPeriodExist", actionList.getReferenceNumber()));
		                    
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("fundTransferBatchSubmit.error.effectiveDatePeriodClosed", actionList.getReferenceNumber()));
		                    
		             } catch (GlobalJournalNotBalanceException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("fundTransferBatchSubmit.error.journalNotBalance", actionList.getReferenceNumber()));  
		
		
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in ArAdjustmentBatchSubmitAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }	
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmFundTransferBatchSubmit");

           }
	        
	        try {
            	
            	actionForm.setLineCount(0);
            	
            	actionForm.clearCmFBSList();
            	
            	ArrayList list = ejbFBS.getCmFtByCriteria(actionForm.getCriteria(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		CmModFundTransferEntryDetails mdetails = (CmModFundTransferEntryDetails)i.next();
      		
            		CmFundTransferBatchSubmitList cmFBSList = new CmFundTransferBatchSubmitList(actionForm,
            		    mdetails.getFtCode(),
            		    Common.convertSQLDateToString(mdetails.getFtDate()),
            		    mdetails.getFtAdBankAccountNameFrom(),
            		    mdetails.getFtAdBankAccountNameTo(),
            		    mdetails.getFtCurrencyFrom(),
            		    mdetails.getFtCurrencyTo(),
            		    mdetails.getFtReferenceNumber(),
            		    Common.convertDoubleToStringMoney(mdetails.getFtAmount(), precisionUnit));
            		    
            		actionForm.saveCmFBSList(cmFBSList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmFundTransferBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
	                       
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("cmFundTransferBatchSubmit"));
	        	     

/*******************************************************
   -- Cm FBS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmFundTransferBatchSubmit");

            }            
            
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	actionForm.clearBankAccountFromList();
            	
            	list = ejbFBS.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountFromList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountFromList((String)i.next());
            			
            		}
            		
            	}            	
            	
            	actionForm.clearBankAccountToList();
            	
            	list = ejbFBS.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBankAccountToList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBankAccountToList((String)i.next());
            			
            		}
            		
            	}
            	
            	
            	actionForm.clearCmFBSList();
            	            
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmFundTransferBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
                        
            return(mapping.findForward("cmFundTransferBatchSubmit"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in CmFundTransferBatchSubmitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}