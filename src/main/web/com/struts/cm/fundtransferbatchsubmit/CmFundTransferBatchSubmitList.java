package com.struts.cm.fundtransferbatchsubmit;

import java.io.Serializable;

public class CmFundTransferBatchSubmitList implements Serializable {

   private Integer fundTransferCode = null;
   private String date = null;
   private String bankAccountFrom = null;
   private String bankAccountTo = null;
   private String currencyFrom = null;
   private String currencyTo = null;
   private String referenceNumber = null;
   private String amount = null;

   private boolean submit = false;
       
   private CmFundTransferBatchSubmitForm parentBean;
    
   public CmFundTransferBatchSubmitList(CmFundTransferBatchSubmitForm parentBean,
	  Integer fundTransferCode,
	  String date,
	  String bankAccountFrom,
      String bankAccountTo,
      String currencyFrom,
      String currencyTo,
      String referenceNumber,
      String amount) {

      this.parentBean = parentBean;
      this.fundTransferCode = fundTransferCode;
      this.date = date;
      this.bankAccountFrom = bankAccountFrom;
      this.bankAccountTo = bankAccountTo;
      this.currencyFrom = currencyFrom;
      this.currencyTo = currencyTo;
      this.referenceNumber = referenceNumber;
      this.amount = amount;
      
   }
   
   public Integer getFundTransferCode() {
   	
   	  return fundTransferCode;
   	  
   }
   
   public String getBankAccountFrom() {
   	  
   	  return bankAccountFrom;
   	  
   }
   
   public String getBankAccountTo() {
   	
   	  return bankAccountTo;
   	  
   }
   
   public String getCurrencyFrom() {
   	
   	  return currencyFrom;
   	  
   }
   
   public String getCurrencyTo() {
   	
   	  return currencyTo;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public String getDate() {
   	
   	  return date;
   	  
   }

   public boolean getSubmit() {
   	
   	  return submit;
   	
   }
   
   public void setSubmit(boolean submit) {
   	
   	  this.submit = submit;
   	
   }
      
}