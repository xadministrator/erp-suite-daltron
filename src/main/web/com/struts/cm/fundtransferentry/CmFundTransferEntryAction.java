package com.struts.cm.fundtransferentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalOverapplicationNotAllowedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.GlobalTransactionAlreadyVoidException;
import com.ejb.txn.CmFundTransferEntryController;
import com.ejb.txn.CmFundTransferEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModBankAccountDetails;
import com.util.CmFundTransferDetails;
import com.util.CmModFundTransferEntryDetails;
import com.util.CmModFundTransferReceiptDetails;

public final class CmFundTransferEntryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmFundTransferEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         CmFundTransferEntryForm actionForm = (CmFundTransferEntryForm)form;
         
         
                  
         String ceParam = null;
         
         if (request.getParameter("child") == null) {
         
         	ceParam = Common.getUserPermission(user, Constants.CM_FUND_TRANSFER_ENTRY_ID);
         
         } else {
         	
         	ceParam = Constants.FULL_ACCESS;
         	
         }

         if (ceParam != null) {

	      if (ceParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmFundTransferEntry");
               }

            }

            actionForm.setUserPermission(ceParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize CmFundTransferEntryController EJB
*******************************************************/

         CmFundTransferEntryControllerHome homeFTE = null;
         CmFundTransferEntryController ejbFTE = null;

         try {

            homeFTE = (CmFundTransferEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmFundTransferEntryControllerEJB", CmFundTransferEntryControllerHome.class);

            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmFundTransferEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFTE = homeFTE.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmFundTransferEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call CmFundTransferEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         
         try {
         	
            precisionUnit = ejbFTE.getGlFcPrecisionUnit(user.getCmpCode());          
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }	
         
/*******************************************************
   -- Cm FTE Previous Action --
*******************************************************/ 
               
         if(request.getParameter("previousButton") != null){

        	 actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));

        	 // check if prev should be disabled
        	 if (actionForm.getLineCount() == 0) {

        		 actionForm.setDisablePreviousButton(true);

        	 } else {

        		 actionForm.setDisablePreviousButton(false);

        	 }

        	 // check if next should be disabled
        	 if (actionForm.getCmFTListSize()  <= actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows())) {

        		 actionForm.setDisableNextButton(true);

        	 } else {

        		 actionForm.setDisableNextButton(false);

        	 }

        	 if (request.getParameter("child") == null) {

        		 return (mapping.findForward("cmFundTransferEntry"));         

        	 } else {

        		 return (mapping.findForward("cmFundTransferEntryChild"));         

        	 }

/*******************************************************
   -- Cm FTE Next Action --
*******************************************************/ 
               	
         } else if(request.getParameter("nextButton") != null){

        	 actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));

        	 // check if prev should be disabled
        	 if (actionForm.getLineCount() == 0) {

        		 actionForm.setDisablePreviousButton(true);

        	 } else {

        		 actionForm.setDisablePreviousButton(false);

        	 }

        	 // check if next should be disabled
        	 if (actionForm.getCmFTListSize() <= actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows())) {

        		 actionForm.setDisableNextButton(true);

        	 } else {

        		 actionForm.setDisableNextButton(false);

        	 }

        	 if (request.getParameter("child") == null) {

        		 return (mapping.findForward("cmFundTransferEntry"));         

        	 } else {

        		 return (mapping.findForward("cmFundTransferEntryChild"));         

        	 }

         }

/*******************************************************
   -- Cm FTE Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
        		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 CmFundTransferDetails details = new CmFundTransferDetails();
        	 details.setFtCode(actionForm.getFundTransferCode());
        	 details.setFtDate(Common.convertStringToSQLDate(actionForm.getDate()));
        	 details.setFtDocumentNumber(actionForm.getDocumentNumber());
        	 details.setFtReferenceNumber(actionForm.getReferenceNumber());
        	 details.setFtMemo(actionForm.getMemo());

        	 details.setFtConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
        	 details.setFtConversionRateFrom(Common.convertStringMoneyToDouble(actionForm.getConversionRateFrom(), precisionUnit));
        	 details.setFtVoid(Common.convertBooleanToByte(actionForm.getFundTransferVoid()));
        	 details.setFtType(actionForm.getType());
        	 details.setFtConversionRateTo(Common.convertStringMoneyToDouble(actionForm.getConversionRateTo(), precisionUnit));

        	 if (actionForm.getFundTransferCode() == null) {

        		 details.setFtCreatedBy(user.getUserName());
        		 details.setFtDateCreated(new java.util.Date());

        	 }

        	 details.setFtLastModifiedBy(user.getUserName());
        	 details.setFtDateLastModified(new java.util.Date());

        	 ArrayList ftList = new ArrayList();

        	 double total =0;

        	 for (int i = 0; i < actionForm.getCmFTListSize(); i++){

        		 CmFundTransferEntryList cmFtList = actionForm.getCmFTByIndex(i);

        		 if (!cmFtList.getDepositMark()) continue;

        		 CmModFundTransferReceiptDetails ftrDetails = new CmModFundTransferReceiptDetails();

        		 ftrDetails.setFtrAmountDeposited(Common.convertStringMoneyToDouble(cmFtList.getAmountDeposited(), precisionUnit));
        		 ftrDetails.setFtrAmountUndeposited(Common.convertStringMoneyToDouble(cmFtList.getAmountUndeposited(), precisionUnit));
        		 ftrDetails.setFtrReceiptNumber(cmFtList.getReceiptNumber());

        		 total = total + Common.convertStringMoneyToDouble(cmFtList.getAmountDeposited(), precisionUnit); 

        		 ftList.add(ftrDetails);

        	 }                  

        	 details.setFtAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit) == 0 ? total: 
        		 Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));


        	 try {

        		 ejbFTE.saveCmFtEntry(details, actionForm.getBankAccountFrom(), 
        				 actionForm.getBankAccountTo(), actionForm.getCurrencyFrom(), true, ftList,
        				 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        	 } catch (GlobalDocumentNumberNotUniqueException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.documentNumberNotUnique"));     

        	 } catch (GlobalRecordAlreadyDeletedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.recordAlreadyDeleted"));

        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.conversionDateNotExist"));           	                      	

        	 } catch (GlobalTransactionAlreadyApprovedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyApproved"));

        	 } catch (GlobalTransactionAlreadyPendingException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyPending"));

        	 } catch (GlobalTransactionAlreadyPostedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyPosted"));

        	 } catch (GlobalTransactionAlreadyVoidException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyVoid"));

        	 } catch (GlobalNoApprovalRequesterFoundException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.noApprovalRequesterFound"));

        	 } catch (GlobalNoApprovalApproverFoundException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.noApprovalApproverFound"));

        	 } catch (GlJREffectiveDateNoPeriodExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.effectiveDateNoPeriodExist"));

        	 } catch (GlJREffectiveDatePeriodClosedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.effectiveDatePeriodClosed"));

        	 } catch (GlobalJournalNotBalanceException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.journalNotBalance"));

        	 } catch (GlobalBranchAccountNumberInvalidException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.branchAccountNumberInvalid"));

        	 } catch (GlobalOverapplicationNotAllowedException ex ) {

        		 errors.add("amount",
        				 new ActionMessage("fundTransferEntry.error.amountDepositedMustNotBeGreaterThanAmountUndeposited"));

        	 } catch (EJBException ex) {

        		 ex.printStackTrace();

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage"); 

        		 }

        	 }

/*******************************************************
 	-- Cm FTE Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
        		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 CmFundTransferDetails details = new CmFundTransferDetails();
        	 details.setFtCode(actionForm.getFundTransferCode());
        	 details.setFtDate(Common.convertStringToSQLDate(actionForm.getDate()));
        	 details.setFtDocumentNumber(actionForm.getDocumentNumber());
        	 details.setFtReferenceNumber(actionForm.getReferenceNumber());
        	 details.setFtMemo(actionForm.getMemo());

        	 details.setFtConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
        	 details.setFtConversionRateFrom(Common.convertStringMoneyToDouble(actionForm.getConversionRateFrom(), precisionUnit));
        	 details.setFtVoid(Common.convertBooleanToByte(actionForm.getFundTransferVoid()));
        	 details.setFtConversionRateTo(Common.convertStringMoneyToDouble(actionForm.getConversionRateTo(), precisionUnit));

        	 if (actionForm.getFundTransferCode() == null) {

        		 details.setFtCreatedBy(user.getUserName());
        		 details.setFtDateCreated(new java.util.Date());

        	 }

        	 details.setFtLastModifiedBy(user.getUserName());
        	 details.setFtDateLastModified(new java.util.Date());
        	 details.setFtType(actionForm.getType());

        	 ArrayList ftList = new ArrayList();

        	 double total = 0;

        	 for (int i = 0; i < actionForm.getCmFTListSize(); i++){

        		 CmFundTransferEntryList cmFtList = actionForm.getCmFTByIndex(i);

        		 if (!cmFtList.getDepositMark()) continue;

        		 CmModFundTransferReceiptDetails ftrDetails = new CmModFundTransferReceiptDetails();

        		 ftrDetails.setFtrAmountDeposited(Common.convertStringMoneyToDouble(cmFtList.getAmountDeposited(), precisionUnit));
        		 ftrDetails.setFtrReceiptNumber(cmFtList.getReceiptNumber());
        		 ftrDetails.setFtrAmountUndeposited(Common.convertStringMoneyToDouble(cmFtList.getAmountUndeposited(), precisionUnit));

        		 total = total + Common.convertStringMoneyToDouble(cmFtList.getAmountDeposited(), precisionUnit);

        		 ftList.add(ftrDetails);

        	 }

        	 details.setFtAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit) == 0 ? total: 
        		 Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));

        	 try {

        		 Integer fundTransferCode = ejbFTE.saveCmFtEntry(details, actionForm.getBankAccountFrom(), 
        				 actionForm.getBankAccountTo(), actionForm.getCurrencyFrom(), false, ftList,
        				 new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        		 actionForm.setFundTransferCode(fundTransferCode);      

        	 } catch (GlobalDocumentNumberNotUniqueException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.documentNumberNotUnique"));     

        	 } catch (GlobalRecordAlreadyDeletedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.recordAlreadyDeleted"));

        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.conversionDateNotExist"));           	                      	

        	 } catch (GlobalTransactionAlreadyApprovedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyApproved"));

        	 } catch (GlobalTransactionAlreadyPendingException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyPending"));

        	 } catch (GlobalTransactionAlreadyPostedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyPosted"));

        	 } catch (GlobalTransactionAlreadyVoidException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.transactionAlreadyVoid"));

        	 } catch (GlobalNoApprovalRequesterFoundException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.noApprovalRequesterFound"));

        	 } catch (GlobalNoApprovalApproverFoundException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.noApprovalApproverFound"));

        	 } catch (GlJREffectiveDateNoPeriodExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.effectiveDateNoPeriodExist"));

        	 } catch (GlJREffectiveDatePeriodClosedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.effectiveDatePeriodClosed"));

        	 } catch (GlobalJournalNotBalanceException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.journalNotBalance"));

        	 } catch (GlobalBranchAccountNumberInvalidException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.branchAccountNumberInvalid"));

        	 } catch (GlobalOverapplicationNotAllowedException ex ) {

        		 errors.add("amount",
        				 new ActionMessage("fundTransferEntry.error.amountDepositedMustNotBeGreaterThanAmountUndeposited"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage"); 

        		 }

        	 }           	            

/*******************************************************
	-- Cm FTE Close Action --
*******************************************************/
         	
         } else if (request.getParameter("closeButton") != null) {

        	 return(mapping.findForward("cmnMain"));

/*******************************************************
	-- Cm FTE Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null) {

        	 if (Common.validateRequired(actionForm.getApprovalStatus())) {

        		 CmFundTransferDetails details = new CmFundTransferDetails();
        		 details.setFtCode(actionForm.getFundTransferCode());
        		 details.setFtDate(Common.convertStringToSQLDate(actionForm.getDate()));
        		 details.setFtDocumentNumber(actionForm.getDocumentNumber());
        		 details.setFtReferenceNumber(actionForm.getReferenceNumber());
        		 details.setFtMemo(actionForm.getMemo());

        		 details.setFtConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
        		 details.setFtConversionRateFrom(Common.convertStringMoneyToDouble(actionForm.getConversionRateFrom(), precisionUnit));
        		 details.setFtVoid(Common.convertBooleanToByte(actionForm.getFundTransferVoid()));
        		 details.setFtConversionRateTo(Common.convertStringMoneyToDouble(actionForm.getConversionRateTo(), precisionUnit));

        		 if (actionForm.getFundTransferCode() == null) {

        			 details.setFtCreatedBy(user.getUserName());
        			 details.setFtDateCreated(new java.util.Date());

        		 }

        		 details.setFtLastModifiedBy(user.getUserName());
        		 details.setFtDateLastModified(new java.util.Date());
        		 details.setFtType(actionForm.getType());

        		 ArrayList ftList = new ArrayList();

        		 double total =0;         		

        		 for (int i = 0; i < actionForm.getCmFTListSize(); i++){

        			 CmFundTransferEntryList cmFtList = actionForm.getCmFTByIndex(i);

        			 if (!cmFtList.getDepositMark()) continue;

        			 CmModFundTransferReceiptDetails ftrDetails = new CmModFundTransferReceiptDetails();

        			 ftrDetails.setFtrAmountDeposited(Common.convertStringMoneyToDouble(cmFtList.getAmountDeposited(), precisionUnit));
        			 ftrDetails.setFtrReceiptNumber(cmFtList.getReceiptNumber());
        			 ftrDetails.setFtrAmountUndeposited(Common.convertStringMoneyToDouble(cmFtList.getAmountUndeposited(), precisionUnit));

        			 total = total + Common.convertStringMoneyToDouble(cmFtList.getAmountDeposited(), precisionUnit);

        			 ftList.add(ftrDetails);

        		 }

        		 details.setFtAmount(Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit) == 0 ? total: 
        			 Common.convertStringMoneyToDouble(actionForm.getAmount(), precisionUnit));


        		 try {

        			 Integer fundTransferCode = ejbFTE.saveCmFtEntry(details, actionForm.getBankAccountFrom(), 
        					 actionForm.getBankAccountTo(), actionForm.getCurrencyFrom(), true, ftList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        			 actionForm.setFundTransferCode(fundTransferCode);                	

        		 } catch (GlobalDocumentNumberNotUniqueException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.documentNumberNotUnique"));     

        		 } catch (GlobalRecordAlreadyDeletedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.recordAlreadyDeleted"));

        		 } catch (GlobalConversionDateNotExistException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.conversionDateNotExist"));           	                      	

        		 } catch (GlobalTransactionAlreadyApprovedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.transactionAlreadyApproved"));

        		 } catch (GlobalTransactionAlreadyPendingException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.transactionAlreadyPending"));

        		 } catch (GlobalTransactionAlreadyPostedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.transactionAlreadyPosted"));

        		 } catch (GlobalTransactionAlreadyVoidException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.transactionAlreadyVoid"));

        		 } catch (GlobalNoApprovalRequesterFoundException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.noApprovalRequesterFound"));

        		 } catch (GlobalNoApprovalApproverFoundException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.noApprovalApproverFound"));

        		 } catch (GlJREffectiveDateNoPeriodExistException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.effectiveDateNoPeriodExist"));

        		 } catch (GlJREffectiveDatePeriodClosedException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.effectiveDatePeriodClosed"));

        		 } catch (GlobalJournalNotBalanceException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.journalNotBalance"));

        		 } catch (GlobalBranchAccountNumberInvalidException ex) {

        			 errors.add(ActionMessages.GLOBAL_MESSAGE,
        					 new ActionMessage("fundTransferEntry.error.branchAccountNumberInvalid"));

        		 } catch (GlobalOverapplicationNotAllowedException ex ) {

        			 errors.add("amount",
        					 new ActionMessage("fundTransferEntry.error.amountDepositedMustNotBeGreaterThanAmountUndeposited"));

        		 } catch (EJBException ex) {

        			 if (log.isInfoEnabled()) {

        				 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        						 " session: " + session.getId());
        				 return mapping.findForward("cmnErrorPage"); 

        			 }

        		 }


        		 if (!errors.isEmpty()) {

        			 saveErrors(request, new ActionMessages(errors));
        			 return mapping.findForward("cmFundTransferEntry");

        		 } 	

        	 }

        	 String path = "/cmJournal.do?forward=1" +
        	 "&fundTransferCode=" + actionForm.getFundTransferCode() +
        	 "&transaction=FUND TRANSFER" +
        	 "&referenceNumber=" + actionForm.getReferenceNumber() + 
        	 "&date=" + actionForm.getDate() +
        	 "&enableFields=" + actionForm.getEnableFields();			     

        	 return(new ActionForward(path));

/*******************************************************
 	-- Cm FTE Delete Action --
*******************************************************/
         	
         } else if(request.getParameter("deleteButton") != null) {

        	 try {

        		 ejbFTE.deleteCmFtEntry(actionForm.getFundTransferCode(), user.getUserName(), user.getCmpCode());

        	 } catch (GlobalRecordAlreadyDeletedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.recordAlreadyDeleted"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());

        		 }

        		 return(mapping.findForward("cmnErrorPage"));

        	 }			   

/*******************************************************
   -- Cm FTE Bank Account FROM Enter Action --
*******************************************************/

         } else if (request.getParameter("isBankAccountFromEntered") != null &&
        		 request.getParameter("isBankAccountFromEntered").equals("true") &&
        		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 try {

        		 actionForm.setType(Constants.CM_FUND_TRANSFER_TYPE_TRANSFER);
        		 actionForm.clearCmFTList();

        		 if (!actionForm.getBankAccountFrom().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

        			 AdModBankAccountDetails mfdetails = ejbFTE.getAdBaByBaName(actionForm.getBankAccountFrom(), user.getCmpCode());

        			 actionForm.setCurrencyFrom(mfdetails.getBaFcName());

        			 actionForm.setBankAccountFromIsCash(Common.convertByteToBoolean(mfdetails.getBaIsCashAccount()));
        			 actionForm.setIsBankAccountFromEntered(false);
        			 actionForm.setIsBankAccountToEntered(false);
        			 actionForm.setIsTypeEntered(false);	                 

        			 if(!Common.validateRequired(actionForm.getConversionDate()))
        				 actionForm.setConversionRateFrom(Common.convertDoubleToStringMoney(
        						 ejbFTE.getFrRateByFrNameAndFrDate(actionForm.getCurrencyFrom(),
        								 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), (short)3));

        			 return(mapping.findForward("cmFundTransferEntry"));

        		 }
        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.conversionDateNotExist"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage"); 

        		 }

        	 }  

/*******************************************************
   -- Cm FTE Bank Account TO Enter Action --
*******************************************************/

         } else if (request.getParameter("isBankAccountToEntered") != null &&
        		 request.getParameter("isBankAccountToEntered").equals("true") && 
        		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        	 try {

        		 if (!actionForm.getBankAccountTo().equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

        			 AdModBankAccountDetails mtdetails = ejbFTE.getAdBaByBaName(actionForm.getBankAccountTo(), user.getCmpCode());

        			 actionForm.setCurrencyTo(mtdetails.getBaFcName());

        			 if ( actionForm.getBankAccountFrom() != Constants.GLOBAL_BLANK ) {

        				 mtdetails = ejbFTE.getAdBaByBaName(actionForm.getBankAccountFrom(), user.getCmpCode());
        				 actionForm.setBankAccountFromIsCash(Common.convertByteToBoolean(mtdetails.getBaIsCashAccount()));

        			 }

        			 actionForm.setIsBankAccountFromEntered(false);
        			 actionForm.setIsBankAccountToEntered(false);
        			 actionForm.setIsTypeEntered(false);

        			 if(!Common.validateRequired(actionForm.getConversionDate()))
        				 actionForm.setConversionRateTo(Common.convertDoubleToStringMoney(
        						 ejbFTE.getFrRateByFrNameAndFrDate(actionForm.getCurrencyTo(),
        								 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), (short)3));

        			 return(mapping.findForward("cmFundTransferEntry"));

        		 }

        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.conversionDateNotExist"));

        	 } catch (EJBException ex) {

        		 ex.printStackTrace();

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage"); 

        		 }

        	 } 

/*******************************************************
   -- Cm FTE Type DEPOSIT Enter Action --
*******************************************************/
        
         } else if (request.getParameter("isTypeEntered") != null && request.getParameter("isTypeEntered").equals("true") && 
        		 actionForm.getType().equals("DEPOSIT") && actionForm.getUserPermission().equals(Constants.FULL_ACCESS) ){

        	 actionForm.setBankAccountFromIsCash(true);
        	 actionForm.setIsBankAccountFromEntered(false);
        	 actionForm.setIsBankAccountToEntered(false);
        	 actionForm.setIsTypeEntered(false);
        	 actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));

        	 actionForm.clearCmFTList();

        	 actionForm.setDisablePreviousButton(true);
        	 actionForm.setDisableNextButton(true);

        	 return(mapping.findForward("cmFundTransferEntry"));

/*******************************************************
   -- Cm FTE Type TRANSFER Enter Action --
*******************************************************/
         		
         } else if (request.getParameter("isTypeEntered") != null && request.getParameter("isTypeEntered").equals("true") && 
        		 actionForm.getType().equals("TRANSFER") && actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){

        	 try {

        		 actionForm.setBankAccountFromIsCash(true);
            	 actionForm.setIsBankAccountFromEntered(false);
            	 actionForm.setIsBankAccountToEntered(false);
            	 actionForm.setIsTypeEntered(false);
            	 actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
            	 
            	 actionForm.clearCmFTList();
            	 
            	 if (actionForm.getFundTransferCode() != null) {
            		 
            		 ejbFTE.clearCmFundTransferReceipts(actionForm.getFundTransferCode(), user.getCmpCode());

            	 }
            	 
            	 return(mapping.findForward("cmFundTransferEntry"));
            	 
        	 } catch (GlobalRecordAlreadyDeletedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.recordAlreadyDeleted"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());

        		 }

        		 return(mapping.findForward("cmnErrorPage"));

        	 }
        	 
/*******************************************************
   -- Cm FTE Conversion Date Enter Action --
******************************************************/
        
         } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {         	

        	 try {

        		 actionForm.setConversionRateFrom(Common.convertDoubleToStringMoney(
        				 ejbFTE.getFrRateByFrNameAndFrDate(actionForm.getCurrencyFrom(),
        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), (short)3));

        		 actionForm.setConversionRateTo(Common.convertDoubleToStringMoney(
        				 ejbFTE.getFrRateByFrNameAndFrDate(actionForm.getCurrencyTo(),
        						 Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), (short)3));

        	 } catch (GlobalConversionDateNotExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("journalEntry.error.conversionDateNotExist"));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());

        		 }

        		 return(mapping.findForward("cmnErrorPage"));

        	 } 

        	 if (!errors.isEmpty()) {

        		 saveErrors(request, new ActionMessages(errors));

        	 }

        	 return(mapping.findForward("cmFundTransferEntry"));

/*******************************************************
 	 -- CM FTE Go Action --
*******************************************************/
         	
         } else	if (request.getParameter("goButton") != null) {

        	 try {

        		 actionForm.setBankAccountFromIsCash(true);
        		 actionForm.clearCmFTList();
        		 
        		 double totalAmount = 0;

        		 if (actionForm.getFundTransferCode() != null){

        			 CmModFundTransferEntryDetails fteDetails = ejbFTE.getCmFtByFtCode(actionForm.getFundTransferCode(),
        					 user.getCmpCode());

        			 ArrayList ftrlist = fteDetails.getFtCmFTRList();
        			 Iterator iFTR = ftrlist.iterator();

        			 // populate fund transfer receipt

        			 while (iFTR.hasNext()) {

        				 CmModFundTransferReceiptDetails ftrDetails = (CmModFundTransferReceiptDetails) iFTR.next();

        				 CmFundTransferEntryList ftList = new CmFundTransferEntryList(actionForm, null,
        						 Common.convertSQLDateToString(ftrDetails.getFtrReceiptDate()), ftrDetails.getFtrReceiptNumber(),
        						 Common.convertDoubleToStringMoney(ftrDetails.getFtrAmountDeposited(), precisionUnit),
        						 Common.convertDoubleToStringMoney(ftrDetails.getFtrTotalAmount(), precisionUnit),
        						 Common.convertDoubleToStringMoney(ftrDetails.getFtrAmountUndeposited(), precisionUnit));

        				 ftList.setDepositMark(true);             				
        				 actionForm.saveCmFTList(ftList);
        				 
        				 totalAmount = totalAmount + ftrDetails.getFtrAmountDeposited();

        			 }
        			
        		 }

        		 ArrayList list = ejbFTE.getPostedArReceiptByRctDateFromAndRctDateToAndBankAccountName(
        				 Common.convertStringToSQLDate(actionForm.getReceiptDateFrom()),
        				 Common.convertStringToSQLDate(actionForm.getreceiptDateTo()), 
        				 actionForm.getBankAccountFrom(), actionForm.getOrderBy(),
        				 new Integer(user.getCurrentBranch().getBrCode()) ,user.getCmpCode());
        		 Iterator i = list.iterator();         			

        		 // populate fund transfer receipts

        		 while (i.hasNext()) {

        			 CmModFundTransferReceiptDetails ftrDetails =  (CmModFundTransferReceiptDetails) i.next();

        			 CmFundTransferEntryList ftList = new CmFundTransferEntryList(actionForm, null,
        					 Common.convertSQLDateToString(ftrDetails.getFtrReceiptDate()), ftrDetails.getFtrReceiptNumber(),
        					 Common.convertDoubleToStringMoney(ftrDetails.getFtrAmountDeposited(), precisionUnit),
        					 Common.convertDoubleToStringMoney(ftrDetails.getFtrTotalAmount(), precisionUnit),
        					 Common.convertDoubleToStringMoney(ftrDetails.getFtrAmountUndeposited(), precisionUnit)); 

        			 actionForm.saveCmFTList(ftList);

        		 }
        		 
        		 // set total amount deposited
        		 actionForm.setAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

        		 // check if prev should be disabled
        		 if (actionForm.getLineCount() == 0) {

        			 actionForm.setDisablePreviousButton(true);

        		 } else {

        			 actionForm.setDisablePreviousButton(false);

        		 }

        		 // check if next should be disabled
        		 if (actionForm.getCmFTListSize() <= actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows())) {

        			 actionForm.setDisableNextButton(true);

        		 } else {

        			 actionForm.setDisableNextButton(false);

        		 }

        	 } catch (GlobalNoRecordFoundException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("fundTransferEntry.error.noRecordFound"));
        		 saveErrors(request, new ActionMessages(errors));

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage"); 

        		 }

        	 }

        	 return(mapping.findForward("cmFundTransferEntry"));

/*******************************************************
   -- Cm FTE Select All Action --
*******************************************************/ 

         } else if(request.getParameter("selectAllButton") != null){

        	 double totalAmount = 0;

        	 // set deposit mark to true
        	 for (int i = 0; i < actionForm.getCmFTListSize(); i++){

        		 CmFundTransferEntryList cmFtList = actionForm.getCmFTByIndex(i);

        		 cmFtList.setDepositMark(true);

        		 totalAmount = totalAmount + Common.convertStringMoneyToDouble(cmFtList.getAmountDeposited(), precisionUnit);

        	 }

        	 // set total amount deposited 
        	 actionForm.setAmount(Common.convertDoubleToStringMoney(totalAmount, precisionUnit));

        	 return (mapping.findForward("cmFundTransferEntry"));

         }
         
/*******************************************************
    -- Cm FTE Load Action --
*******************************************************/         
         
         if (ceParam != null) {

        	 if (!errors.isEmpty()) {

        		 saveErrors(request, new ActionMessages(errors));
        		 return mapping.findForward("cmFundTransferEntry");

        	 }


        	 if (request.getParameter("forward") == null &&
        			 actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
        		 saveErrors(request, new ActionMessages(errors));

        		 return mapping.findForward("cmnMain");	

        	 }

        	 // actionForm.reset(mapping, request);

        	 ArrayList list = null;
        	 Iterator i = null;

        	 try {

        		 actionForm.clearCmFTList();

        		 String bankAccount = actionForm.getBankAccountFrom(); 

        		 actionForm.clearBankAccountFromList();

        		 list = ejbFTE.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        		 if (list == null || list.size() == 0) {

        			 actionForm.setBankAccountFromList(Constants.GLOBAL_NO_RECORD_FOUND);
        			 bankAccount = Constants.GLOBAL_BLANK;

        		 } else {

        			 i = list.iterator();

        			 while (i.hasNext()) {

        				 actionForm.setBankAccountFromList((String)i.next());

        			 }

        		 }            	

        		 actionForm.setBankAccountFrom(bankAccount);

        		 actionForm.clearBankAccountToList();

        		 bankAccount = actionForm.getBankAccountTo();

        		 list = ejbFTE.getAdBaAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

        		 if (list == null || list.size() == 0) {

        			 actionForm.setBankAccountToList(Constants.GLOBAL_NO_RECORD_FOUND);
        			 bankAccount = Constants.GLOBAL_BLANK;

        		 } else {

        			 i = list.iterator();

        			 while (i.hasNext()) {

        				 actionForm.setBankAccountToList((String)i.next());

        			 }

        		 }

        		 actionForm.setBankAccountTo(bankAccount);

        		 if (!actionForm.getBankAccountFrom().equals(Constants.GLOBAL_BLANK)) {

        			 AdModBankAccountDetails mtdetails = ejbFTE.getAdBaByBaName(actionForm.getBankAccountFrom(), user.getCmpCode());
        			 actionForm.setBankAccountFromIsCash(Common.convertByteToBoolean(mtdetails.getBaIsCashAccount()));

        		 }

        		 if (request.getParameter("forward") != null) {

        			 CmModFundTransferEntryDetails fteDetails = ejbFTE.getCmFtByFtCode(
        					 new Integer(request.getParameter("fundTransferCode")), user.getCmpCode());

        			 actionForm.setFundTransferCode(fteDetails.getFtCode());
        			 actionForm.setDate(Common.convertSQLDateToString(fteDetails.getFtDate()));
        			 actionForm.setDocumentNumber(fteDetails.getFtDocumentNumber());
        			 actionForm.setReferenceNumber(fteDetails.getFtReferenceNumber());
        			 actionForm.setAmount(Common.convertDoubleToStringMoney(fteDetails.getFtAmount(), precisionUnit));
        			 actionForm.setMemo(fteDetails.getFtMemo());	

        			 if (!actionForm.getBankAccountFromList().contains(fteDetails.getFtAdBankAccountNameFrom())						) {

        				 if (actionForm.getBankAccountFromList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

        					 actionForm.clearBankAccountFromList();

        				 }					

        				 actionForm.setBankAccountFromList(fteDetails.getFtAdBankAccountNameFrom());

        			 }	

        			 actionForm.setBankAccountFrom(fteDetails.getFtAdBankAccountNameFrom());

        			 if (!actionForm.getBankAccountToList().contains(fteDetails.getFtAdBankAccountNameTo())) {

        				 if (actionForm.getBankAccountToList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

        					 actionForm.clearBankAccountToList();

        				 }

        				 actionForm.setBankAccountToList(fteDetails.getFtAdBankAccountNameTo());

        			 }		            

        			 actionForm.setBankAccountTo(fteDetails.getFtAdBankAccountNameTo());
        			 actionForm.setCurrencyFrom(fteDetails.getFtCurrencyFrom());	
        			 actionForm.setCurrencyTo(fteDetails.getFtCurrencyTo());			            	            
        			 actionForm.setConversionDate(Common.convertSQLDateToString(fteDetails.getFtConversionDate()));
        			 actionForm.setConversionRateFrom(Common.convertDoubleToStringMoney(fteDetails.getFtConversionRateFrom(), Constants.MONEY_RATE_PRECISION));
        			 actionForm.setFundTransferVoid(Common.convertByteToBoolean(fteDetails.getFtVoid()));	            		            	            
        			 actionForm.setApprovalStatus(fteDetails.getFtApprovalStatus());
        			 actionForm.setPosted(fteDetails.getFtPosted() == 1 ? "YES" : "NO");
        			 actionForm.setReasonForRejection(fteDetails.getFtReasonForRejection());
        			 actionForm.setCreatedBy(fteDetails.getFtCreatedBy());
        			 actionForm.setDateCreated(Common.convertSQLDateToString(fteDetails.getFtDateCreated()));
        			 actionForm.setLastModifiedBy(fteDetails.getFtLastModifiedBy());
        			 actionForm.setDateLastModified(Common.convertSQLDateToString(fteDetails.getFtDateLastModified()));            		
        			 actionForm.setApprovedRejectedBy(fteDetails.getFtApprovedRejectedBy());
        			 actionForm.setDateApprovedRejected(Common.convertSQLDateToString(fteDetails.getFtDateApprovedRejected()));
        			 actionForm.setPostedBy(fteDetails.getFtPostedBy());
        			 actionForm.setDatePosted(Common.convertSQLDateToString(fteDetails.getFtDatePosted()));
        			 actionForm.setType(fteDetails.getFtType());
        			 actionForm.setBankAccountFromIsCash(fteDetails.getFtType() == null? false : true);
        			 actionForm.setConversionRateTo(Common.convertDoubleToStringMoney(fteDetails.getFtConversionRateTo(), Constants.MONEY_RATE_PRECISION));

        			 actionForm.clearCmFTList();

        			 if (actionForm.getType().equals(Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT)) {

        				 ArrayList ftrlist = fteDetails.getFtCmFTRList();
        				 Iterator iFTR = ftrlist.iterator();

        				 // populate fund transfer receipt

        				 while (iFTR.hasNext()) {

        					 CmModFundTransferReceiptDetails ftrDetails = (CmModFundTransferReceiptDetails) iFTR.next();

        					 CmFundTransferEntryList ftList = new CmFundTransferEntryList(actionForm, null,
        							 Common.convertSQLDateToString(ftrDetails.getFtrReceiptDate()), ftrDetails.getFtrReceiptNumber(),
        							 Common.convertDoubleToStringMoney(ftrDetails.getFtrAmountDeposited(), precisionUnit),
        							 Common.convertDoubleToStringMoney(ftrDetails.getFtrTotalAmount(), precisionUnit),
        							 Common.convertDoubleToStringMoney(ftrDetails.getFtrAmountUndeposited(), precisionUnit));

        					 ftList.setDepositMark(true);             				
        					 actionForm.saveCmFTList(ftList);

        				 }

        			 }

        			 actionForm.setLineCount(0);
        			 actionForm.setMaxRows("10");
        			 actionForm.setDisablePreviousButton(true);

        			 // check if next should be disabled
        			 if (actionForm.getCmFTListSize()  <= actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows())) {

        				 actionForm.setDisableNextButton(true);

        			 } else {

        				 actionForm.setDisableNextButton(false);

        			 }

        			 this.setFormProperties(actionForm);	

        			 if (request.getParameter("child") == null) {

        				 return (mapping.findForward("cmFundTransferEntry"));         

        			 } else {

        				 return (mapping.findForward("cmFundTransferEntryChild"));         

        			 } 

        		 }          		                	            	

        	 } catch (EJBException ex) {

        		 if (log.isInfoEnabled()) {

        			 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        					 " session: " + session.getId());
        			 return mapping.findForward("cmnErrorPage"); 

        		 }

        	 }           


        	 if (!errors.isEmpty()) {

        		 saveErrors(request, new ActionMessages(errors));

        	 } else {

        		 if ((request.getParameter("saveSubmitButton") != null &&
        				 actionForm.getUserPermission().equals(Constants.FULL_ACCESS))) {

        			 try {

        				 list = ejbFTE.getAdApprovalNotifiedUsersByFtCode(actionForm.getFundTransferCode(), user.getCmpCode());

        				 if (list.isEmpty()) {

        					 messages.add(ActionMessages.GLOBAL_MESSAGE,
        							 new ActionMessage("messages.documentSentForPosting"));     

        				 } else if (list.contains("DOCUMENT POSTED")) {

        					 messages.add(ActionMessages.GLOBAL_MESSAGE,	
        							 new ActionMessage("messages.documentPosted"));

        				 } else {

        					 i = list.iterator();

        					 String APPROVAL_USERS = "";

        					 while (i.hasNext()) {

        						 APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

        						 if (i.hasNext()) {

        							 APPROVAL_USERS = APPROVAL_USERS + ", ";

        						 }


        					 }

        					 messages.add(ActionMessages.GLOBAL_MESSAGE,
        							 new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     

        				 }

        				 saveMessages(request, messages);
        				 actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

        			 } catch(EJBException ex) {

        				 if (log.isInfoEnabled()) {

        					 log.info("EJBException caught in CmFundTransferEntryAction.execute(): " + ex.getMessage() +
        							 " session: " + session.getId());
        				 }

        				 return(mapping.findForward("cmnErrorPage"));

        			 } 

        		 } else if (request.getParameter("saveAsDraftButton") != null && 
        				 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

        			 actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

        		 }

        	 }

        	 actionForm.reset(mapping, request);              

        	 actionForm.setFundTransferCode(null);          
        	 actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
        	 actionForm.setAmount(Common.convertDoubleToStringMoney(0d, precisionUnit));
        	 actionForm.setConversionRateFrom("1.000000");
        	 actionForm.setPosted("NO");
        	 actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
        	 actionForm.setCreatedBy(user.getUserName());
        	 actionForm.setLastModifiedBy(user.getUserName());
        	 actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
        	 actionForm.setLineCount(0);
        	 actionForm.setMaxRows("10");
        	 actionForm.setDisableNextButton(true);
        	 actionForm.setDisablePreviousButton(true);


        	 this.setFormProperties(actionForm);           
        	 return(mapping.findForward("cmFundTransferEntry"));

         } else {

        	 errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
        	 saveErrors(request, new ActionMessages(errors));

        	 return(mapping.findForward("cmnMain"));

         }

      } catch(ArithmeticException e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

    	  if (log.isInfoEnabled()) {

    		  log.info("Exception caught in CmFundTransferEntryAction.execute(): " + e.getMessage()
    				  + " session: " + session.getId());
    	  }

    	  return mapping.findForward("cmnErrorPage");

      }
   }

   private void setFormProperties(CmFundTransferEntryForm actionForm) {

	   if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

		   if (actionForm.getPosted().equals("NO") && !actionForm.getFundTransferVoid()) {

			   if (actionForm.getFundTransferCode() == null) {

				   actionForm.setEnableFields(true);
				   actionForm.setShowSaveButton(true);
				   actionForm.setShowDeleteButton(false);
				   actionForm.setEnableFundTransferVoid(false);
				   actionForm.setShowJournalButton(true);

			   } else if (actionForm.getFundTransferCode() != null &&
					   Common.validateRequired(actionForm.getApprovalStatus())) {

				   actionForm.setEnableFields(true);
				   actionForm.setShowSaveButton(true);
				   actionForm.setShowDeleteButton(true);
				   actionForm.setEnableFundTransferVoid(true);
				   actionForm.setShowJournalButton(true);


			   } else {

				   actionForm.setEnableFields(false);
				   actionForm.setShowSaveButton(false);
				   actionForm.setShowDeleteButton(true);
				   actionForm.setEnableFundTransferVoid(false);
				   actionForm.setShowJournalButton(true);

			   }

		   } else {

			   actionForm.setEnableFields(false);
			   actionForm.setShowSaveButton(false);

			   if (actionForm.getPosted().equals("YES")) {

				   actionForm.setShowDeleteButton(false);

			   } else {

				   actionForm.setShowDeleteButton(true);

			   }

			   actionForm.setEnableFundTransferVoid(false);
			   actionForm.setShowJournalButton(true);

		   }

	   } else {

		   actionForm.setEnableFields(false);
		   actionForm.setShowSaveButton(false);
		   actionForm.setShowDeleteButton(false);
		   actionForm.setEnableFundTransferVoid(false);
		   actionForm.setShowJournalButton(true);

	   }

   }
}