package com.struts.cm.fundtransferentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmFundTransferEntryForm extends ActionForm implements Serializable {
	
	private Integer fundTransferCode = null;
	private String bankAccountFrom = null;
	private ArrayList bankAccountFromList = new ArrayList();
	private String currencyFrom = null;
	private String bankAccountTo = null;
	private ArrayList bankAccountToList = new ArrayList();
	private String currencyTo = null;
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String amount = null;
	private String memo = null;
	private boolean fundTransferVoid = false;
	private String conversionDate = null;
	private String conversionRateFrom = null;
	private String conversionRateTo = null;
	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String reasonForRejection = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private ArrayList cmFTList = new ArrayList(); 
	private boolean bankAccountFromIsCash = false;
	
	private boolean isBankAccountFromEntered = false;
	private boolean isBankAccountToEntered = false;
	private boolean isTypeEntered = false;
	private String isConversionDateEntered = null;
	private String functionalCurrency = null;
	
	private String saveButton = null;
	private String closeButton = null;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean enableFundTransferVoid = false;  
	private boolean showSaveButton = false;
	private boolean showDeleteButton = false;
	private boolean showJournalButton = false;  
	private int rowSelected = 0;
	
	private String nextButton = null;
	private String previousButton = null;
	private String goButton = null;
	private String selectAllButton = null;
	
	private boolean disableNextButton = false;
	private boolean disablePreviousButton = false;
	private boolean disableGoButton = false;
	
	private int lineCount = 0;
	private String maxRows = null;
		
	private String receiptDateFrom = null;
	private String receiptDateTo = null;
	private String orderBy = null;
	private ArrayList orderByList = new ArrayList();
	
	public void setSaveButton(String saveButton) {
		
		this.saveButton = saveButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getFundTransferCode() {
		
		return fundTransferCode;
		
	}
	
	public void setFundTransferCode(Integer fundTransferCode) {
		
		this.fundTransferCode = fundTransferCode;
		
	}
	
	public String getBankAccountFrom() {
		
		return bankAccountFrom;
		
	}
	
	public void setBankAccountFrom(String bankAccountFrom) {
		
		this.bankAccountFrom = bankAccountFrom;
		
	}
	
	public ArrayList getBankAccountFromList() {
		
		return bankAccountFromList;
		
	}
	
	public void setBankAccountFromList(String bankAccountFrom) {
		
		bankAccountFromList.add(bankAccountFrom);
		
	}
	
	public void clearBankAccountFromList() {
		
		bankAccountFromList.clear();
		bankAccountFromList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getCurrencyFrom() {
		
		return currencyFrom;
		
	}
	
	public void setCurrencyFrom(String currencyFrom) {
		
		this.currencyFrom = currencyFrom;
		
	}
	
	public String getBankAccountTo() {
		
		return bankAccountTo;
		
	}
	
	public void setBankAccountTo(String bankAccountTo) {
		
		this.bankAccountTo = bankAccountTo;
		
	}
	
	public ArrayList getBankAccountToList() {
		
		return bankAccountToList;
		
	}
	
	public void setBankAccountToList(String bankAccountTo) {
		
		bankAccountToList.add(bankAccountTo);
		
	}
	
	public void clearBankAccountToList() {
		
		bankAccountToList.clear();
		bankAccountToList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public String getCurrencyTo() {
		
		return currencyTo;
		
	}
	
	public void setCurrencyTo(String currencyTo) {
		
		this.currencyTo = currencyTo;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	public String getReferenceNumber() {
		
		return referenceNumber;
		
	}
	
	public void setReferenceNumber(String referenceNumber) {
		
		this.referenceNumber = referenceNumber;
		
	}
	
	public String getAmount() {
		
		return amount;
		
	}
	
	public void setAmount(String amount) {
		
		this.amount = amount;
		
	}
	
	public String getMemo() {
		
		return memo;
		
	}
	
	public void setMemo(String memo) {
		
		this.memo = memo;
		
	}
	
	public boolean getFundTransferVoid() {
		
		return fundTransferVoid;
		
	}
	
	public void setFundTransferVoid(boolean fundTransferVoid) {
		
		this.fundTransferVoid = fundTransferVoid; 
		
	}
	
	public String getConversionDate() {
		
		return conversionDate;
		
	}
	
	public void setConversionDate(String conversionDate) {
		
		this.conversionDate = conversionDate;
		
	}
	
	public String getConversionRateFrom() {
		
		return conversionRateFrom;
		
	}
	
	public void setConversionRateFrom(String conversionRateFrom) {
		
		this.conversionRateFrom = conversionRateFrom;
		
	}

	public String getConversionRateTo() {
		
		return conversionRateTo;
		
	}
	
	public void setConversionRateTo(String conversionRateTo) {
		
		this.conversionRateTo = conversionRateTo;
		
	}

	public String getApprovalStatus() {
		
		return approvalStatus;
		
	}
	
	public void setApprovalStatus(String approvalStatus) {
		
		this.approvalStatus = approvalStatus;
		
	}
	
	public String getPosted() {
		
		return posted;
		
	}
	
	public void setPosted(String posted) {
		
		this.posted = posted;
		
	}
	
	public String getCreatedBy() {
		
		return createdBy;
		
	}
	
	public void setCreatedBy(String createdBy) {
		
		this.createdBy = createdBy;
		
	}
	
	public String getDateCreated() {
		
		return dateCreated;
		
	}
	
	public void setDateCreated(String dateCreated) {
		
		this.dateCreated = dateCreated;
		
	}   
	
	public String getLastModifiedBy() {
		
		return lastModifiedBy;
		
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		
		this.lastModifiedBy = lastModifiedBy;
		
	}
	
	public String getDateLastModified() {
		
		return dateLastModified;
		
	}
	
	public void setDateLastModified(String dateLastModified) {
		
		this.dateLastModified = dateLastModified;
		
	}
	
	
	public String getApprovedRejectedBy() {
		
		return approvedRejectedBy;
		
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		
		this.approvedRejectedBy = approvedRejectedBy;
		
	}
	
	public String getDateApprovedRejected() {
		
		return dateApprovedRejected;
		
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		
		this.dateApprovedRejected = dateApprovedRejected;
		
	}
	
	public String getPostedBy() {
		
		return postedBy;
		
	}
	
	public void setPostedBy(String postedBy) {
		
		this.postedBy = postedBy;
		
	}
	
	public String getDatePosted() {
		
		return datePosted;
		
	}
	
	public void setDatePosted(String datePosted) {
		
		this.datePosted = datePosted;
		
	}
	
	public String getReasonForRejection() {
		
		return reasonForRejection;
		
	}
	
	public void setReasonForRejection(String reasonForRejection) {
		
		this.reasonForRejection = reasonForRejection;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public boolean getEnableFundTransferVoid() {
		
		return enableFundTransferVoid;
		
	}
	
	public void setEnableFundTransferVoid(boolean enableFundTransferVoid) {
		
		this.enableFundTransferVoid = enableFundTransferVoid;
		
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	public boolean getShowJournalButton() {
		
		return showJournalButton;
		
	}
	
	public void setShowJournalButton(boolean showJournalButton) {
		
		this.showJournalButton = showJournalButton;
		
	}
	
	public boolean getIsBankAccountFromEntered() {
		
		return isBankAccountFromEntered;
		
	}
	
	public void setIsBankAccountFromEntered(boolean isBankAccountFromEntered) {
		
		this.isBankAccountFromEntered = isBankAccountFromEntered;
		
	}
	
	public boolean getIsBankAccountToEntered() {
		
		return isBankAccountToEntered;
		
	}
	
	public void setIsBankAccountToEntered(boolean isBankAccountToEntered) {
		
		this.isBankAccountToEntered = isBankAccountToEntered;
		
	}
	
	public int getRowSelected(){
		
		return rowSelected;
		
	}
	
	public void setRowSelected(Object selectedCmFTList, boolean isEdit){
		
		this.rowSelected = cmFTList.indexOf(selectedCmFTList);
		
	}
	
	public Object[] getCmFTList(){
		
		return(cmFTList.toArray());
		
	}
	
	public void saveCmFTList(Object newCmFTList){
		
		cmFTList.add(newCmFTList);
		
	}
	
	public void deleteCmFTList(int rowSelected){
		
		cmFTList.remove(rowSelected);
		
	}
	
	public int getCmFTListSize(){
		
		return(cmFTList.size());
		
	}
	
	public void clearCmFTList(){
		
		cmFTList.clear();

	}
	
	public CmFundTransferEntryList getCmFTByIndex(int index){
		
		return((CmFundTransferEntryList)cmFTList.get(index));
		
	}
	
	public void updateCmFTRow(int rowSelected, Object newCmFTList){
		
		cmFTList.set(rowSelected, newCmFTList);
		
	}
	
	public boolean getIsTypeEntered() {
		
		return isTypeEntered;
		
	}
	
	public void setIsTypeEntered(boolean isTypeEntered) {
		
		this.isTypeEntered=isTypeEntered;
		
	}

	public String getType() {

		return type;

	}

	public void setType(String type) {

		this.type = type;

	}
	
	public ArrayList getTypeList() {
		
		return typeList;
		
	}
	
	public void setTypeList(ArrayList typeList) {
		
		this.typeList = typeList;
		
	}
	
	public boolean getBankAccountFromIsCash() {
		
		return bankAccountFromIsCash;
		
	}
	
	public void setBankAccountFromIsCash(boolean bankAccountFromIsCash) {
		
		this.bankAccountFromIsCash = bankAccountFromIsCash;
		
	}

   	public String getIsConversionDateEntered(){
   		
   		return isConversionDateEntered;
   		
   	}	
	
	public int getLineCount(){
		return lineCount;   	 
	}
	
	public void setLineCount(int lineCount){  	
		this.lineCount = lineCount;
	}
	
	public boolean getDisablePreviousButton() {
		
		return disablePreviousButton;
		
	}
	
	public void setDisablePreviousButton(boolean disablePreviousButton) {
		
		this.disablePreviousButton = disablePreviousButton;
		
	}
	
	public boolean getDisableNextButton() {
		
		return disableNextButton;
		
	}
	
	public void setDisableNextButton(boolean disableNextButton) {
		
		this.disableNextButton = disableNextButton;
		
	}
	
	public boolean getDisableGoButton() {
		
		return disableGoButton;
		
	}
	
	public void setDisableGoButton(boolean disableGoButton) {
		
		this.disableGoButton = disableGoButton;
		
	}
	
	public String getReceiptDateFrom() {
		
		return receiptDateFrom;
		
	}
	
	public void setReceiptDateFrom(String receiptDateFrom) {
		
		this.receiptDateFrom = receiptDateFrom;
		
	}
	
	public String getreceiptDateTo() {
		
		return receiptDateTo;
		
	}
	
	public void setreceiptDateTo(String receiptDateTo) {
		
		this.receiptDateTo = receiptDateTo;
		
	}
	
	public String getMaxRows(){
	   	
	   	return maxRows;
	   	
	}
	 
	public void setMaxRows(String maxRows){  	
	   	
	   this.maxRows = maxRows;
	   	
	}
	
	public String getOrderBy() {

		return orderBy;

	}

	public void setOrderBy(String orderBy) {

		this.orderBy = orderBy;

	}
	
	public ArrayList getOrderByList() {
		
		return orderByList;
		
	}
	
	public void setOrderByList(ArrayList orderByList) {
		
		this.orderByList = orderByList;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		bankAccountFrom = Constants.GLOBAL_BLANK;
		currencyFrom = null;
		bankAccountTo = Constants.GLOBAL_BLANK;
		currencyTo = null;
		date = Common.convertSQLDateToString(new Date());
		documentNumber = null;
		referenceNumber = null;
		amount = null;
		memo = null;
		fundTransferVoid = false;
		conversionDate = null;
		conversionRateFrom = "1.000000";
		conversionRateTo = "1.000000";
		approvalStatus = null;
		posted = null;
		createdBy = null;
		dateCreated = null;
		lastModifiedBy = null;
		dateLastModified = null;   
		approvedRejectedBy = null;
		dateApprovedRejected = null;
		postedBy = null;
		datePosted = null;
		reasonForRejection = null;
		isBankAccountFromEntered = false;
		isBankAccountToEntered = false;
		date = Common.convertSQLDateToString(new Date());
		receiptDateFrom = Common.convertSQLDateToString(new Date());;
		receiptDateTo = Common.convertSQLDateToString(new Date());;
		
		isTypeEntered = false;
		isConversionDateEntered = null;
		   
		typeList.clear();
		typeList.add(Constants.CM_FUND_TRANSFER_TYPE_TRANSFER);
		typeList.add(Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT);
		type = Constants.CM_FUND_TRANSFER_TYPE_TRANSFER;
		
		orderByList.clear();
		orderByList.add("DATE");
		orderByList.add("RECEIPT NUMBER");
		orderBy = "DATE";
		
		// clear pay checkbox
		   
		Iterator i = cmFTList.iterator();
		
		int ctr = 0;
		
		while (i.hasNext()) {
			
			CmFundTransferEntryList list = (CmFundTransferEntryList)i.next();
			
			if (ctr >= lineCount && ctr <= lineCount + Integer.parseInt(maxRows) - 1) {
				
				list.setDepositMark(false);
				
			}
			
			ctr++;
			
		}
		
		saveButton = null;
		closeButton = null;
		goButton = null;
		selectAllButton = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("goButton") != null && (type.equals(Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT))) {
			
			if (Common.validateRequired(bankAccountFrom) || bankAccountFrom.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				errors.add("bankAccountFrom",
						new ActionMessage("fundTransferEntry.error.bankAccountFromRequired"));
				
			}
			
			if(!Common.validateDateFormat(receiptDateFrom)){
				errors.add("receiptDateFrom", new ActionMessage("fundTransferEntry.error.receiptDateFromInvalid"));
			}
			
			if(!Common.validateDateFormat(receiptDateTo)){
				errors.add("receiptDateTo", new ActionMessage("fundTransferEntry.error.receiptDateToInvalid"));
			}
			
			if((receiptDateFrom != null && receiptDateTo.length() < 0)){
				errors.add("receiptDateTo", new ActionMessage("fundTransferEntry.error.receiptDateToIncomplete"));
			}
			
			if((receiptDateFrom.length() < 0  && receiptDateTo != null)){
				errors.add("receiptDateFrom", new ActionMessage("fundTransferEntry.error.receiptDateFromIncomplete"));
			}
			
		}
		
		if (request.getParameter("saveSubmitButton") != null || 
				request.getParameter("saveAsDraftButton") != null ||
				request.getParameter("journalButton") != null) {
			
			if (Common.validateRequired(bankAccountFrom) || bankAccountFrom.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				errors.add("bankAccountFrom",
						new ActionMessage("fundTransferEntry.error.bankAccountFromRequired"));
				
			}
			
			if (Common.validateRequired(bankAccountTo) || bankAccountTo.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
				
				errors.add("bankAccountTo",
						new ActionMessage("fundTransferEntry.error.bankAccountToRequired"));
				
			}         
			
			if (Common.validateRequired(date)) {
				
				errors.add("date",
						new ActionMessage("fundTransferEntry.error.dateRequired"));
				
			}
			
			if (!Common.validateDateFormat(date)) {
				
				errors.add("date",
						new ActionMessage("fundTransferEntry.error.dateInvalid"));
				
			}
			
			
			if(!Common.validateDateFormat(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("fundTransferEntry.error.conversionDateInvalid"));
			}
			
			if(!Common.validateMoneyRateFormat(conversionRateFrom) || !Common.validateMoneyRateFormat(conversionRateTo)){
				errors.add("conversionRateFrom",
						new ActionMessage("fundTransferEntry.error.conversionRateInvalid"));
			}
			
			if(currencyFrom.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("fundTransferEntry.error.conversionDateMustBeNull"));
			}
			
			if(currencyTo.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
				errors.add("conversionDate",
						new ActionMessage("fundTransferEntry.error.conversionDateMustBeNull"));
			}
			
			if(currencyFrom.equals(functionalCurrency) && !Common.validateRequired(conversionRateFrom) &&
					Common.convertStringMoneyToDouble(conversionRateFrom, (short)6) != 1d){ 
				errors.add("conversionRateFrom",
						new ActionMessage("fundTransferEntry.error.conversionRateMustBeNull"));
			}
			
			if(currencyTo.equals(functionalCurrency) && !Common.validateRequired(conversionRateTo) &&
					Common.convertStringMoneyToDouble(conversionRateTo, (short)6) != 1d){ 
				errors.add("conversionRateFrom",
						new ActionMessage("fundTransferEntry.error.conversionRateMustBeNull"));
			}
			
			if((!currencyFrom.equals(functionalCurrency)) && Common.validateRequired(conversionRateFrom) &&
					Common.validateRequired(conversionDate)){
				errors.add("conversionRateFrom",
						new ActionMessage("fundTransferEntry.error.conversionRateOrDateMustBeEntered"));
			}
			
			if((!currencyTo.equals(functionalCurrency)) && Common.validateRequired(conversionRateTo) &&
					Common.validateRequired(conversionDate)){
				errors.add("conversionRateFrom",
						new ActionMessage("fundTransferEntry.error.conversionRateOrDateMustBeEntered"));
			}
			
			if (bankAccountFrom.equals(bankAccountTo)) {
				
				errors.add("bankAccountTo",
						new ActionMessage("fundTransferEntry.error.bankAccountMustNotBeTheSame"));

			}
			
			if (type.equals(Constants.CM_FUND_TRANSFER_TYPE_DEPOSIT)){
				
				int numberOfLines = 0;
				
				Iterator i = cmFTList.iterator();
				
				while (i.hasNext()) {
					
					CmFundTransferEntryList ftrList = (CmFundTransferEntryList) i.next();
					
					
					if (!ftrList.getDepositMark()) continue;
					
					numberOfLines++;
					
					if (Common.validateRequired(ftrList.getAmountDeposited())) {
						
						errors.add("amount",
								new ActionMessage("fundTransferEntry.error.amountRequired"));
						
					}
					
					if (!Common.validateMoneyFormat(ftrList.getAmountDeposited())) {
						
						errors.add("amount",
								new ActionMessage("fundTransferEntry.error.amountInvalid"));
						
					}
						
				} 
				
				if (numberOfLines == 0) {
					
					errors.add("fundTransfer",
							new ActionMessage("fundTransferEntry.error.fundTransferMustHaveLines"));
					
				}
				
			} else {
				
				if (Common.validateRequired(amount)) {
					
					errors.add("amount",
							new ActionMessage("fundTransferEntry.error.amountRequired"));
					
				}         
				
				if (!Common.validateMoneyFormat(amount)) {
					
					errors.add("amount",
							new ActionMessage("fundTransferEntry.error.amountInvalid"));
					
				}
				
				
			}
		} else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){
			
			if(!Common.validateDateFormat(conversionDate)){
				
				errors.add("conversionDate", new ActionMessage("fundTransferEntry.error.conversionDateInvalid"));
				
			}
			
		}
		
		return errors;
	}
}