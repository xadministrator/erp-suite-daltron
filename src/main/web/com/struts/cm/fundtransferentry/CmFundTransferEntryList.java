package com.struts.cm.fundtransferentry;

public class CmFundTransferEntryList {
	
	private CmFundTransferEntryForm parentBean = null;	
	private String ftrCode = null;
	private String receiptDate = null;
	private String receiptNumber = null;
	private String amountDeposited = null;
	private String totalAmount = null;
	private String amountUndeposited = null;
	private boolean depositMark = false;
	
	
	public CmFundTransferEntryList () {;}
	
	public CmFundTransferEntryList (CmFundTransferEntryForm parentBean, String ftrCode, String receiptDate, String receiptNumber,
			String amountDeposited, String totalAmount, String amountUndeposited){
		
		this.parentBean = parentBean;
		this.ftrCode = ftrCode;
		this.receiptDate = receiptDate;
		this.receiptNumber = receiptNumber;
		this.amountDeposited = amountDeposited;
		this.totalAmount = totalAmount;
		this.amountUndeposited = amountUndeposited;
		
	}
	
	
	
	public String getReceiptDate(){
		
		return this.receiptDate;
		
	}
	
	public  void setReceiptDate(String receiptDate){
		
		this.receiptDate = receiptDate;
		
	}
	
	public String getReceiptNumber(){
		
		return this.receiptNumber;
		
	}
	
	public  void setReceiptNumber(String receiptNumber){
		
		this.receiptNumber = receiptNumber;
		
	}

	public String getTotalAmount(){
		
		
		return this.totalAmount;
		
	}
	
	public void setTotalAmount(String totalAmount){
		
		this.totalAmount = totalAmount;
		
	}

	
	public String getAmountDeposited(){
		
		return this.amountDeposited;
		
	}
	
	public void setAmountDeposited(String amountDeposited){
		
		this.amountDeposited = amountDeposited;
		
	}
	
	public String getFtrCode(){
		
		return this.ftrCode;
		
	}
	
	public void setFtrCode(String ftrCode){
		
		this.ftrCode = ftrCode;
		
	}
	
	public boolean getDepositMark() {
		
		return depositMark;
		
	}
	
	public void setAmountUndeposited(String amountUndeposited) {
		
		this.amountUndeposited = amountUndeposited;
		
	}
	
	public String getAmountUndeposited() {
		
		return amountUndeposited;
		
	}
	
	public void setDepositMark(boolean depositMark) {
		
		this.depositMark = depositMark;
		
	}
		
}
