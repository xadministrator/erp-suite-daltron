package com.struts.cm.fundtransferpost;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmFundTransferPostForm extends ActionForm implements Serializable {

   private String bankAccountFrom = null;
   private ArrayList bankAccountFromList = new ArrayList(); 
   private String bankAccountTo = null;
   private ArrayList bankAccountToList = new ArrayList();  
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String referenceNumber = null;
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String pageState = new String();
   private ArrayList cmFPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public CmFundTransferPostList getCmFPByIndex(int index) {

      return((CmFundTransferPostList)cmFPList.get(index));

   }

   public Object[] getCmFPList() {

      return cmFPList.toArray();

   }

   public int getCmFPListSize() {

      return cmFPList.size();

   }

   public void saveCmFPList(Object newCmFPList) {

      cmFPList.add(newCmFPList);

   }
   
   public void deleteCmFPList(int rowSelected) {

      cmFPList.remove(rowSelected);

   }

   public void clearCmFPList() {
   	
      cmFPList.clear();
      
   }

   public void setRowSelected(Object selectedCmFPList, boolean isEdit) {

      this.rowSelected = cmFPList.indexOf(selectedCmFPList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

  }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }   

   public String getBankAccountFrom() {
   	
   	  return bankAccountFrom;
   	
   }
   
   public void setBankAccountFrom(String bankAccountFrom) {
   	
   	  this.bankAccountFrom = bankAccountFrom;
   	
   }
   
   public ArrayList getBankAccountFromList() {
   	
   	   return bankAccountFromList;
   	
   }
   
   public void setBankAccountFromList(String bankAccountFrom) {
   	
   	   bankAccountFromList.add(bankAccountFrom);
   	
   }
   
   public void clearBankAccountFromList() {
   	   	   
   	   bankAccountFromList.clear();
   	   bankAccountFromList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getBankAccountTo() {
   	
   	  return bankAccountTo;
   	
   }
   
   public void setBankAccountTo(String bankAccountTo) {
   	
   	  this.bankAccountTo = bankAccountTo;
   	
   }
   
   public ArrayList getBankAccountToList() {
   	
   	   return bankAccountToList;
   	
   }
   
   public void setBankAccountToList(String bankAccountTo) {
   	
   	   bankAccountToList.add(bankAccountTo);
   	
   }
   
   public void clearBankAccountToList() {
   	   	   
   	   bankAccountToList.clear();
   	   bankAccountToList.add(Constants.GLOBAL_BLANK);
   	
   }   
   
   public String getDocumentNumberFrom() {
   	
   	  return documentNumberFrom;
   	
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom) {
   	
   	  this.documentNumberFrom = documentNumberFrom;
   	
   }
   
   public String getDocumentNumberTo() {
   	
   	  return documentNumberTo;
   	
   }
   
   public void setDocumentNumberTo(String documentNumberTo) {
   	
   	  this.documentNumberTo = documentNumberTo;
   	
   }

   public String getReferenceNumber() {

      return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

      this.referenceNumber = referenceNumber;

   }

   public String getDateFrom() {
   	
      return dateFrom;
   
   }

   public void setDateFrom(String dateFrom) {
   
      this.dateFrom = dateFrom;
   
   }
   
   public String getDateTo() {
   	
   	  return dateTo;
   	  
   }
   
   public void setDateTo(String dateTo) {
   	
   	  this.dateTo = dateTo;
   	  
   }
                         
   public String getApprovalStatus() {
   	
   	  return approvalStatus;
   
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	  this.approvalStatus = approvalStatus;
   
   }
   
   public ArrayList getApprovalStatusList() {
   	
   	  return approvalStatusList;
   
   }   
       
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {
 
   	  bankAccountFrom = Constants.GLOBAL_BLANK;
      bankAccountTo = Constants.GLOBAL_BLANK;
      dateFrom = null;
      dateTo = null;
      documentNumberFrom = null;
   	  documentNumberTo = null;
   	  referenceNumber = null;
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("APPROVED");
      approvalStatusList.add("N/A");
      approvalStatus = Constants.GLOBAL_BLANK;     
      
      for (int i=0; i<cmFPList.size(); i++) {
	  	
      	CmFundTransferPostList actionList = (CmFundTransferPostList)cmFPList.get(i);
      	
	  	  actionList.setPost(false);
	  	    	
      }
             
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("BANK ACCOUNT");
	      orderByList.add("TYPE");
	      orderByList.add("REFERENCE NUMBER");
	      orderByList.add("DOCUMENT NUMBER");
	      orderBy = "TYPE";   
	  
	  }     
	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("fundTransferPost.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("fundTransferPost.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("fundTransferPost.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("fundTransferPost.error.maxRowsInvalid"));
		
		   }
	 	  
      }
      
      return errors;

   }
}