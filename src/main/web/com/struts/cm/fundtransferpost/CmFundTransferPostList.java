package com.struts.cm.fundtransferpost;

import java.io.Serializable;

public class CmFundTransferPostList implements Serializable {

   private Integer fundTransferCode = null;
   private String date = null;
   private String bankAccountFrom = null;
   private String bankAccountTo = null;
   private String currencyFrom = null;
   private String currencyTo = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String amount = null;

   private boolean post = false;
       
   private CmFundTransferPostForm parentBean;
    
   public CmFundTransferPostList(CmFundTransferPostForm parentBean,
	  Integer fundTransferCode,
	  String date,
	  String bankAccountFrom,
      String bankAccountTo,
      String currencyFrom,
      String currencyTo,
      String documentNumber,
      String referenceNumber,
      String amount) {

      this.parentBean = parentBean;
      this.fundTransferCode = fundTransferCode;
      this.date = date;
      this.bankAccountFrom = bankAccountFrom;
      this.bankAccountTo = bankAccountTo;
      this.currencyFrom = currencyFrom;
      this.currencyTo = currencyTo;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amount = amount;
      
   }
   
   public Integer getFundTransferCode() {
   	
   	  return fundTransferCode;
   	  
   }
   
   public String getBankAccountFrom() {
   	  
   	  return bankAccountFrom;
   	  
   }
   
   public String getBankAccountTo() {
   	
   	  return bankAccountTo;
   	  
   }
   
   public String getCurrencyFrom() {
   	
   	  return currencyFrom;
   	  
   }
   
   public String getCurrencyTo() {
   	
   	  return currencyTo;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }
   
   public String getDate() {
   	
   	  return date;
   	  
   }

   public boolean getPost() {
   	
   	  return post;
   	
   }
   
   public void setPost(boolean post) {
   	
   	  this.post = post;
   	
   }
      
}