package com.struts.cm.journal;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.CmJournalController;
import com.ejb.txn.CmJournalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.CmModDistributionRecordDetails;

public final class CmJournalAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("CmJournalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         CmJournalForm actionForm = (CmJournalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.CM_JOURNAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("cmJournal");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize CmJournalController EJB
*******************************************************/

         CmJournalControllerHome homeJR = null;
         CmJournalController ejbJR = null;

         try {

            homeJR = (CmJournalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/CmJournalControllerEJB", CmJournalControllerHome.class);      
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in CmJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbJR = homeJR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in CmJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
         short precisionUnit = 0;
         short journalLineNumber = 0;
         
         try { 
         	
            precisionUnit = ejbJR.getGlFcPrecisionUnit(user.getCmpCode());
            journalLineNumber = ejbJR.getAdPrfGlJournalLineNumber(user.getCmpCode());
            
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in CmJournalAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Cm JR Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	ArrayList drList = new ArrayList();
            int lineNumber = 0;  
            
            double TOTAL_DEBIT = 0;
            double TOTAL_CREDIT = 0;         
                       
            for (int i = 0; i < actionForm.getCmJRListSize(); i++) {
            	
        	   CmJournalList cmJRList = actionForm.getCmJRByIndex(i);           	   
        	              	   
        	   if (Common.validateRequired(cmJRList.getAccountNumber()) &&
        	   		Common.validateRequired(cmJRList.getDebitAmount()) &&
        	   		Common.validateRequired(cmJRList.getCreditAmount())) continue;
        	   
        	   byte isDebit = 0;
	           double amount = 0d;

		       if (!Common.validateRequired(cmJRList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(cmJRList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(cmJRList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
	       
		       lineNumber++;

        	   CmModDistributionRecordDetails mdetails = new CmModDistributionRecordDetails();
        	   
        	   mdetails.setDrLine((short)(lineNumber));
        	   mdetails.setDrClass(cmJRList.getDrClass());
        	   mdetails.setDrDebit(isDebit);
        	   mdetails.setDrAmount(amount);
        	   mdetails.setDrCoaAccountNumber(cmJRList.getAccountNumber());
        	   
        	   drList.add(mdetails);
            	
            }
            
            TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
            TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
            
            if (TOTAL_DEBIT != TOTAL_CREDIT) {
            	
            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("cmJournal.error.journalNotBalance"));
                     
                saveErrors(request, new ActionMessages(errors));
                return (mapping.findForward("cmJournal"));
            	
            }
            
            Integer primaryKey = null;
            
            if (actionForm.getAdjustmentCode() != null) {
            	
            	primaryKey = actionForm.getAdjustmentCode();
            	
            } else if (actionForm.getFundTransferCode() != null) {
            	
            	primaryKey = actionForm.getFundTransferCode();
            	
            }
            
            try {
            	
            	ejbJR.saveCmDrEntry(primaryKey, drList, actionForm.getTransaction(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            } catch (GlobalAccountNumberInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("cmJournal.error.accountNumberInvalid", ex.getMessage()));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }
            
/*******************************************************
	-- Cm JR Add Lines Action --
*******************************************************/

      } else if(request.getParameter("addLinesButton") != null) {
      	
      	int listSize = actionForm.getCmJRListSize();
                                
         for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
        	
        	CmJournalList cmJRList = new CmJournalList(actionForm,
        	    null, String.valueOf(x), null, null, null, null, null);
        	    
        	cmJRList.setDrClassList(this.getDrClassList());
        	
        	actionForm.saveCmJRList(cmJRList);
        	
        }	        
        
        return(mapping.findForward("cmJournal"));
        
/*******************************************************
    -- Cm JR Delete Lines Action --
*******************************************************/        
        
      } else if(request.getParameter("deleteLinesButton") != null) {
      	
      	for (int i = 0; i<actionForm.getCmJRListSize(); i++) {
        	
        	   CmJournalList cmJRList = actionForm.getCmJRByIndex(i);
        	   
        	   if (cmJRList.getDeleteCheckbox()) {
        	   	
        	   	   actionForm.deleteCmJRList(i);
        	   	   i--;
        	   }
        	   
         }
         
         for (int i = 0; i<actionForm.getCmJRListSize(); i++) {
        	
         	CmJournalList cmJRList = actionForm.getCmJRByIndex(i);
        	   
        	   cmJRList.setLineNumber(String.valueOf(i+1));
        	   
         }
	        
	        return(mapping.findForward("cmJournal"));	         

/*******************************************************
   -- Cm JR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Cm JR Back Action --
*******************************************************/

         } else if (request.getParameter("backButton") != null) {

            String path = null;
            
            if (actionForm.getTransaction().equals("FUND TRANSFER")) {
            	
            	path = "/cmFundTransferEntry.do?forward=1&fundTransferCode=" +
            	    actionForm.getFundTransferCode();
            	
            } else if (actionForm.getTransaction().equals("BANK ADJUSTMENTS")) {
            	
            	path = "/cmAdjustmentEntry.do?forward=1&adjustmentCode=" +
            	    actionForm.getAdjustmentCode();
            	
            } 
            
            return(new ActionForward(path));
            
/*******************************************************
   -- Cm JR Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("cmJournal");

            }
            
            try {
            	
        		ArrayList list = new ArrayList();
        		Iterator i = null;
        		
        		actionForm.clearCmJRList();

        		if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("FUND TRANSFER")) ||
        		   actionForm.getTransaction().equals("FUND TRANSFER")) {
        			
        			if (request.getParameter("fundTransferCode") != null) 
        			   actionForm.setFundTransferCode(new Integer(request.getParameter("fundTransferCode")));
        			   actionForm.setAdjustmentCode(null);
        			
        			list = ejbJR.getCmDrByFtCode(actionForm.getFundTransferCode(), user.getCmpCode());
        			
        			            			
        		} else if ((request.getParameter("forward") != null && request.getParameter("transaction").equals("BANK ADJUSTMENTS")) ||
        		          actionForm.getTransaction().equals("BANK ADJUSTMENTS")) {
        			
        			if (request.getParameter("adjustmentCode") != null) 
        				actionForm.setAdjustmentCode(new Integer(request.getParameter("adjustmentCode")));
        			    actionForm.setFundTransferCode(null);
       			
        			list = ejbJR.getCmDrByAdjCode(actionForm.getAdjustmentCode(), user.getCmpCode());
        			
        			            			
        		}
        		
                if (request.getParameter("forward") != null) {
        			
	      			actionForm.setReferenceNumber(request.getParameter("referenceNumber"));
	       			actionForm.setDate(request.getParameter("date"));
	       			actionForm.setEnableFields(request.getParameter("enableFields"));
	       			
	       		}

        		double totalDebit = 0;
        		double totalCredit = 0;
                
        		i = list.iterator();        		       		
        			
    			while (i.hasNext()) {
    				
    				CmModDistributionRecordDetails mdetails = (CmModDistributionRecordDetails)i.next();
    				
    				String debitAmount = null;
    				String creditAmount = null;
    				
    				if (mdetails.getDrDebit() == 1) {
    					
    					debitAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalDebit = totalDebit + mdetails.getDrAmount();
    					
    				} else {
    					
    					creditAmount = Common.convertDoubleToStringMoney(mdetails.getDrAmount(), precisionUnit);				          
    					totalCredit = totalCredit + mdetails.getDrAmount();
    					
    				}
    				
    				ArrayList classList = new ArrayList();
    				classList.add(mdetails.getDrClass());    				
    				
    				CmJournalList cmJRList = new CmJournalList(actionForm,
    				    mdetails.getDrCode(), 
    				    Common.convertShortToString(mdetails.getDrLine()),
    				    mdetails.getDrCoaAccountNumber(),
    				    debitAmount,
    				    creditAmount,
    				    mdetails.getDrClass(),
    				    mdetails.getDrCoaAccountDescription());
    				
    				cmJRList.setDrClassList(classList);
    				
    				actionForm.saveCmJRList(cmJRList);
    				        				
    			}
    			
    			actionForm.setTotalDebit(Common.convertDoubleToStringMoney(totalDebit, precisionUnit));
    			actionForm.setTotalCredit(Common.convertDoubleToStringMoney(totalCredit, precisionUnit));
    			System.out.println("------->"+Common.convertDoubleToStringMoney(totalCredit, precisionUnit));
    			int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
		        
		        for (int x = list.size() + 1; x <= remainingList; x++) {
		        	
		        	CmJournalList cmJRList = new CmJournalList(actionForm,
			        	    null, String.valueOf(x), null, null, null, null, null);
			        	    
		        	cmJRList.setDrClassList(this.getDrClassList());
			        	
			        actionForm.saveCmJRList(cmJRList);
			        			        	
		        }
    			
    			this.setFormProperties(actionForm);
            	
            } catch (GlobalNoRecordFoundException ex) {
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in CmJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if ((request.getParameter("saveButton") != null) && 
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
            return(mapping.findForward("cmJournal"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	e.printStackTrace();
      	
          if (log.isInfoEnabled()) {

             log.info("Exception caught in CmJournalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
   
	   private void setFormProperties(CmJournalForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getEnableFields().equals("true")) {
			
	    		actionForm.setShowSaveButton(true);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
			
			} else {
				
				actionForm.setShowSaveButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				
			}
			
		} else {
			
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			    		
		}
	
	}

	private ArrayList getDrClassList() {
		
		ArrayList classList = new ArrayList();
		
	 	classList.add("OTHER");
	 	
	 	return classList;
		
	}   
	
}