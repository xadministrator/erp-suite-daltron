package com.struts.cm.journal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class CmJournalForm extends ActionForm implements Serializable {

   private Integer fundTransferCode = null;
   private Integer adjustmentCode = null;
   private String transaction = null;
   private String date = null;
   private String referenceNumber = null;
   private String totalDebit = null;
   private String totalCredit = null;
   private String enableFields = null;

   private String backButton = null;
   private String closeButton = null;
   private ArrayList cmJRList = new ArrayList();
   private int cmJRListSize = 0;
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showSaveButton = false;   

   public int getRowSelected() {

      return rowSelected;

   }

   public CmJournalList getCmJRByIndex(int index) {

      return((CmJournalList)cmJRList.get(index));

   }

   public Object[] getCmJRList() {

      return cmJRList.toArray();

   }

   public int getCmJRListSize() {
	   
	   cmJRListSize = cmJRList.size();

      return cmJRList.size();

   }

   public void saveCmJRList(Object newCmJRList) {

      cmJRList.add(newCmJRList);

   }

   public void clearCmJRList() {
      cmJRList.clear();
   }

   public void deleteCmJRList(int rowSelected) {

   	cmJRList.remove(rowSelected);

   }
   
   public void setBackButton(String backButton) {

      this.backButton = backButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public Integer getFundTransferCode() {

      return fundTransferCode;

   }

   public void setFundTransferCode(Integer fundTransferCode) {

      this.fundTransferCode = fundTransferCode;

   }

   public Integer getAdjustmentCode() {

      return adjustmentCode;

   }

   public void setAdjustmentCode(Integer adjustmentCode) {

      this.adjustmentCode = adjustmentCode;

   }

   public String getTransaction() {

      return transaction;

   }

   public void setTransaction(String transaction) {

      this.transaction = transaction;

   }

   public String getDate() {

      return date;

   }

   public void setDate(String date) {

      this.date = date;

   }

   public String getReferenceNumber() {

      return referenceNumber;

   }

   public void setReferenceNumber(String referenceNumber) {

      this.referenceNumber = referenceNumber;

   }
   
   public String getTotalDebit() {
   	
   	  return totalDebit;
   	
   }
   
   public void setTotalDebit(String totalDebit) {
   	
   	  this.totalDebit = totalDebit;
   	
   }
   
   public String getTotalCredit() {
   	
   	  return totalCredit;
   	
   }
   
   public void setTotalCredit(String totalCredit) {
   	
   	  this.totalCredit = totalCredit;
   	
   }
   
   public String getEnableFields() {
   	
   	  return enableFields;
   	  
   }
   
   public void setEnableFields(String enableFields) {
   	
   	  this.enableFields = enableFields;
   	  
   }   
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      backButton = null;
      closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null) {
      	 
      	int numberOfLines = 0;
    	 
    	 Iterator i = cmJRList.iterator();      	 
    	 
    	 while (i.hasNext()) {
    	 	
    	 	 CmJournalList drList = (CmJournalList)i.next();      	 	 
    	 	       	 	 
    	 	 if (Common.validateRequired(drList.getAccountNumber()) &&
    	 	     Common.validateRequired(drList.getDebitAmount()) &&
    	 	     Common.validateRequired(drList.getCreditAmount())) continue;
    	 	     
    	 	 numberOfLines++;
    	 
	         if(Common.validateRequired(drList.getAccountNumber())){
	            errors.add("accountNumber",
	               new ActionMessage("cmJournal.error.accountNumberRequired", drList.getLineNumber()));
	         }
		 	 if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateMoneyFormat(drList.getDebitAmount())){
	            errors.add("debitAmount",
	               new ActionMessage("cmJournal.error.debitAmountInvalid", drList.getLineNumber()));
	         }
	         if(! Common.validateRequired(drList.getCreditAmount()) && !Common.validateMoneyFormat(drList.getCreditAmount())){
	            errors.add("creditAmount",
	               new ActionMessage("cmJournal.error.creditAmountInvalid", drList.getLineNumber()));
	         }
			 if(Common.validateRequired(drList.getDebitAmount()) && Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("cmJournal.error.debitCreditAmountRequired", drList.getLineNumber()));
			 }
			 if(!Common.validateRequired(drList.getDebitAmount()) && !Common.validateRequired(drList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("cmJournal.error.debitCreditAmountMustBeNull", drList.getLineNumber()));
		     }	    		     
		     
		          
	    }
	    
	    if (numberOfLines == 0) {
       	
       	errors.add("transaction",
             new ActionMessage("cmJournal.error.journalMustHaveLine"));
       	
       }
     	
     }
      
      return errors;

   }
}