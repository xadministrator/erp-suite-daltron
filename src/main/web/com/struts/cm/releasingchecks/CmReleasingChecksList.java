package com.struts.cm.releasingchecks;

import java.io.Serializable;
import java.util.Date;

public class CmReleasingChecksList implements Serializable {

   private Integer checkCode = null;
   private String supplierName = null;
   private String bankAccount = null;
   private String date = null;
   private String checkNumber = null;
   private String documentNumber = null;
   private String amount = null;
   private String type = null;
   private String referenceNumber = null;
   private String desrciption = null;
   private String chkCheckDate = null;
   private String dateReleased = null;
   private String remarks = null;
   
   private boolean release = false;
       
    
   private CmReleasingChecksForm parentBean;
   public CmReleasingChecksList(CmReleasingChecksForm parentBean,
	  Integer checkCode,
	  String supplierName,  
	  String bankAccount,  
	  String date,  
	  String checkNumber,
	  String documentNumber,  
	  String amount,
	  String referenceNumber,
	  String type,
	  String chkCheckDate,
	  String dateReleased,
	  String remarks) {

      this.parentBean = parentBean;
      this.checkCode = checkCode;
      this.supplierName = supplierName;
      this.bankAccount = bankAccount;
      this.date = date;
      this.checkNumber = checkNumber;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amount = amount;
      this.type = type;
      this.chkCheckDate = chkCheckDate;
      this.dateReleased = dateReleased;
      this.remarks = remarks;
      
   }
   
   public Integer getCheckCode() {
   	
   	  return checkCode;
   	  
   }
   
   public String getSupplierName() {

      return supplierName;

   }
   
   public String getBankAccount() {

      return bankAccount;

   }
   
   public String getDate() {
   	 
   	  return date;
   	 
   }
   
   public String getCheckNumber() {
   	
   	  return checkNumber;
   	 
   }
   
   public void setCheckNumber (String checkNumber) {
	   
	   this.checkNumber = checkNumber;
	   
   }

   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getAmount() {
   	
   	  return amount;
   	  
   }

   public String getType() {
   	
   	  return type;
   	  
   }
   
   public boolean getRelease() {
   	
   	  return release;
   	
   }
   
   public void setRelease(boolean release) {
   	
   	  this.release = release;
   	
   }
   
   public String getDescription() {

    return desrciption;

 }

   public String getReferenceNumber() {

    return referenceNumber;

 }
   
   public String getChkCheckDate() {
	   
	   return chkCheckDate;
	   
 }
   
  public void setChkCheckDate(String chkCheckDate) {
	   
	   this.chkCheckDate = chkCheckDate;
	   
 }
  
   public String getDateReleased() {
	     
	   return dateReleased;
	   
 }
   
   public void setDateReleased(String dateReleased) {
	   
	   this.dateReleased = dateReleased;
	   
   }
   
   public String getRemarks() {
	   
	   return remarks;
   }
   
   public void setRemarks (String remarks) {
	   
	   this.remarks = remarks;
   }

}