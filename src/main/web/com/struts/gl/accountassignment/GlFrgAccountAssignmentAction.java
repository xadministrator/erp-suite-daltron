package com.struts.gl.accountassignment;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlFrgAAAccountHighInvalidException;
import com.ejb.exception.GlFrgAAAccountLowInvalidException;
import com.ejb.exception.GlFrgAARowAlreadyHasCalculationException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.GlFrgAccountAssignmentController;
import com.ejb.txn.GlFrgAccountAssignmentControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFrgAccountAssignmentDetails;
import com.util.GlFrgRowDetails;

public final class GlFrgAccountAssignmentAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFrgAccountAssignmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFrgAccountAssignmentForm actionForm = (GlFrgAccountAssignmentForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FRG_ACCOUNT_ASSIGNMENT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFrgAccountAssignment");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFrgAccountAssignmentController EJB
*******************************************************/

         GlFrgAccountAssignmentControllerHome homeAA = null;
         GlFrgAccountAssignmentController ejbAA = null;

         try {

            homeAA = (GlFrgAccountAssignmentControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFrgAccountAssignmentControllerEJB", GlFrgAccountAssignmentControllerHome.class);      
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFrgAccountAssignmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAA = homeAA.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFrgAccountAssignmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();	

/*******************************************************
   -- Gl AA Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFrgAccountAssignment"));
	     
/*******************************************************
   -- Gl AA Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFrgAccountAssignment"));         


/*******************************************************
   -- Gl AA Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlFrgAccountAssignmentDetails details = new GlFrgAccountAssignmentDetails();
            details.setAaAccountLow(actionForm.getAccountLow());
            details.setAaAccountHigh(actionForm.getAccountHigh());
            details.setAaActivityType(actionForm.getActivityType());
            
            try {
            	
            	ejbAA.addGlFrgAaEntry(details, actionForm.getRowCode(), user.getCmpCode());

            } catch (GlFrgAARowAlreadyHasCalculationException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("frgAccountAssignment.error.rowAlreadyHasCalculation"));
                  
            } catch (GlFrgAAAccountHighInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("frgAccountAssignment.error.accountHighInvalid"));
                  
            } catch (GlFrgAAAccountLowInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("frgAccountAssignment.error.accountLowInvalid"));                                    

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgAccountAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }             
            
/*******************************************************
   -- Gl AA Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl AA Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           
            GlFrgAccountAssignmentList glAAList =
	            actionForm.getGlAAByIndex(actionForm.getRowSelected());
                      
            GlFrgAccountAssignmentDetails details = new GlFrgAccountAssignmentDetails();
            details.setAaCode(glAAList.getAccountAssignmentCode());
            details.setAaAccountLow(actionForm.getAccountLow());
            details.setAaAccountHigh(actionForm.getAccountHigh());
            details.setAaActivityType(actionForm.getActivityType());
            
            try {
            	
            	ejbAA.updateGlFrgAaEntry(details, actionForm.getRowCode(), user.getCmpCode());
                  
            } catch (GlFrgAAAccountHighInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("frgAccountAssignment.error.accountHighInvalid"));
                  
            } catch (GlFrgAAAccountLowInvalidException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("frgAccountAssignment.error.accountLowInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgAccountAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl AA Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl AA Edit Action --
*******************************************************/

         } else if (request.getParameter("glAAList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlAARow(actionForm.getRowSelected());
            
            return mapping.findForward("glFrgAccountAssignment");
            
/*******************************************************
   -- Gl AA Delete Action --
*******************************************************/

         } else if (request.getParameter("glAAList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlFrgAccountAssignmentList glAAList =
	            actionForm.getGlAAByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbAA.deleteGlFrgAaEntry(glAAList.getAccountAssignmentCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("frgAccountAssignment.error.accountAssignmentAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlFrgAccountAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            } 

/******************************************************
   -- Gl AA Row Action --
*******************************************************/

         } else if (request.getParameter("rowButton") != null) {
         	        	
         	 String path = "/glFrgRowEntry.do"; 
			     
             return(new ActionForward(path));

/*******************************************************
   -- Gl AA Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFrgAccountAssignment");

            }
            
			GlFrgRowDetails rowDetails = null;
			
			if (request.getParameter("forward") != null) {
			
				rowDetails = ejbAA.getGlFrgRowByRowCode(new Integer(request.getParameter("rowCode")), user.getCmpCode());        		
				actionForm.setRowCode(new Integer(request.getParameter("rowCode")));
			
			} else {
			
				rowDetails = ejbAA.getGlFrgRowByRowCode(actionForm.getRowCode(), user.getCmpCode());
			
			}
			
			actionForm.setRowCode(actionForm.getRowCode());
			actionForm.setRowName(rowDetails.getRowName());
			actionForm.setLineNumber(String.valueOf(rowDetails.getRowLineNumber()));            
            
            ArrayList list = null;
            Iterator i = null;    	   

	        try {
	        	
	        	actionForm.clearGlAAList();
	                        
	            list = ejbAA.getGlFrgAaByRowCode(actionForm.getRowCode(), user.getCmpCode());
	            
	            i = list.iterator();
	            
	            while(i.hasNext()) {
	            	
	            	GlFrgAccountAssignmentDetails details = (GlFrgAccountAssignmentDetails)i.next();
	            	
	            	GlFrgAccountAssignmentList glAAList = new GlFrgAccountAssignmentList(actionForm,
	            	    details.getAaCode(),
	            	    details.getAaAccountLow(),
	            	    details.getAaAccountHigh(),
	            	    details.getAaActivityType());
	            	    
	                actionForm.saveGlAAList(glAAList);
	            	
	            }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		           	        	
	        } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgAccountAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glFrgAccountAssignment"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(ArithmeticException e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFrgAccountAssignmentAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}