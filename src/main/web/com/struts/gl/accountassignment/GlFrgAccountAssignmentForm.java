package com.struts.gl.accountassignment;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFrgAccountAssignmentForm extends ActionForm implements Serializable {

   private Integer rowCode = null;
   private String rowName = null;
   private String lineNumber = null;   
   private String accountLow = null;
   private String accountHigh = null;
   private String activityType = null;  
   private ArrayList activityTypeList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	   
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glAAList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();  

   public int getRowSelected() {

      return rowSelected;

   }

   public GlFrgAccountAssignmentList getGlAAByIndex(int index) {

      return((GlFrgAccountAssignmentList)glAAList.get(index));

   }

   public Object[] getGlAAList() {

      return glAAList.toArray();

   }

   public int getGlAAListSize() {

      return glAAList.size();

   }

   public void saveGlAAList(Object newGlAAList) {

      glAAList.add(newGlAAList);

   }

   public void clearGlAAList() {
      glAAList.clear();
   }

   public void setRowSelected(Object selectedGlAAList, boolean isEdit) {

      this.rowSelected = glAAList.indexOf(selectedGlAAList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlAARow(int rowSelected) {
   	
       this.accountLow = ((GlFrgAccountAssignmentList)glAAList.get(rowSelected)).getAccountLow();
       this.accountHigh = ((GlFrgAccountAssignmentList)glAAList.get(rowSelected)).getAccountHigh();
       this.activityType = ((GlFrgAccountAssignmentList)glAAList.get(rowSelected)).getActivityType();
       
   }

   public void updateGlAARow(int rowSelected, Object newGlAAList) {

      glAAList.set(rowSelected, newGlAAList);

   }

   public void deleteGlAAList(int rowSelected) {

      glAAList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }  
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getRowCode() {
   	
   	  return rowCode;
   	
   }
   
   public void setRowCode(Integer rowCode) {
   	
   	  this.rowCode = rowCode;
   	
   }

   public String getRowName() {

      return rowName;

   }

   public void setRowName(String rowName) {

      this.rowName = rowName;

   }

   public String getLineNumber() {

      return lineNumber;

   }

   public void setLineNumber(String lineNumber) {

      this.lineNumber = lineNumber;

   }

   public String getAccountLow() {

      return accountLow;

   }

   public void setAccountLow(String accountLow) {

      this.accountLow = accountLow;

   }
   
   public String getAccountHigh() {

      return accountHigh;

   }

   public void setAccountHigh(String accountHigh) {

      this.accountHigh = accountHigh;

   }   
   
   public String getActivityType() {

      return activityType;

   }

   public void setActivityType(String activityType) {

      this.activityType = activityType;

   }   

   public ArrayList getActivityTypeList() {

      return activityTypeList;

   } 
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      accountLow = null;
      accountHigh = null;      
      activityType = null;
	  activityTypeList.clear();
	  activityTypeList.add(Constants.GL_FRG_AA_ACTIVITY_TYPE_BEG);
	  activityTypeList.add(Constants.GL_FRG_AA_ACTIVITY_TYPE_DEBIT);
      activityTypeList.add(Constants.GL_FRG_AA_ACTIVITY_TYPE_CREDIT);
	  activityTypeList.add(Constants.GL_FRG_AA_ACTIVITY_TYPE_END);
	  activityTypeList.add(Constants.GL_FRG_AA_ACTIVITY_TYPE_NET);
	  activityType = Constants.GL_FRG_AA_ACTIVITY_TYPE_NET;  
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;
	  
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(accountLow)) {

            errors.add("accountLow",
               new ActionMessage("frgAccountAssignment.error.accountLowRequired"));

         }               
         
         if (Common.validateRequired(accountHigh)) {

            errors.add("accountHigh",
               new ActionMessage("frgAccountAssignment.error.accountHighRequired"));

         }

         if (Common.validateRequired(activityType)) {

            errors.add("activityType",
               new ActionMessage("frgAccountAssignment.error.activityTypeRequired"));

         }

      }
      return errors;

   }
}