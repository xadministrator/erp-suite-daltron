package com.struts.gl.accountassignment;

import java.io.Serializable;

public class GlFrgAccountAssignmentList implements Serializable {

   private Integer accountAssignmentCode = null;
   private String accountLow = null;
   private String accountHigh = null;
   private String activityType = null;

   private String editButton = null;
   private String deleteButton = null;
    
   private GlFrgAccountAssignmentForm parentBean;
    
   public GlFrgAccountAssignmentList(GlFrgAccountAssignmentForm parentBean,
      Integer accountAssignmentCode,
      String accountLow,
      String accountHigh,
      String activityType) {

      this.parentBean = parentBean;
      this.accountAssignmentCode = accountAssignmentCode;
      this.accountLow = accountLow;
      this.accountHigh = accountHigh;
      this.activityType = activityType;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {
   	
   	  parentBean.setRowSelected(this, false);
   	
   }

   public Integer getAccountAssignmentCode() {

      return accountAssignmentCode;

   }
   
   public String getAccountLow() {
   	
   	  return accountLow;
   	  
   }
   
   public String getAccountHigh() {
   	
   	  return accountHigh;
   	  
   }
   
   public String getActivityType() {
   	
   	  return activityType;
   	  
   }
}