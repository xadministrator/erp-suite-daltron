package com.struts.gl.accountingcalendar;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlACAccountingCalendarAlreadyAssignedException;
import com.ejb.exception.GlACAccountingCalendarAlreadyDeletedException;
import com.ejb.exception.GlACAccountingCalendarAlreadyExistException;
import com.ejb.exception.GlACNoAccountingCalendarFoundException;
import com.ejb.exception.GlPTNoPeriodTypeFoundException;
import com.ejb.txn.GlAccountingCalendarController;
import com.ejb.txn.GlAccountingCalendarControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlAccountingCalendarDetails;
import com.util.GlModAccountingCalendarDetails;
import com.util.GlPeriodTypeDetails;


public final class GlAccountingCalendarAction extends Action {
	
    private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
   
   
       HttpSession session = request.getSession();
       try{
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          if (user != null) {
             if (log.isInfoEnabled()){
                log.info("GlAccountingCalendarAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             }
          }else{
             if (log.isInfoEnabled()){
	        log.info("User is not logged on in session " + session.getId());
             }
	     return(mapping.findForward("adLogon"));
          }
	  String frParam = Common.getUserPermission(user, Constants.GL_ACCOUNTING_CALENDAR_ID);
          if(frParam != null){
  	     if(frParam.trim().equals(Constants.FULL_ACCESS)){
	        ActionErrors fieldErrors = ((GlAccountingCalendarForm)form).validateFields(mapping, request);
		if(!fieldErrors.isEmpty()){
		   saveErrors(request, new ActionMessages(fieldErrors));
		   return(mapping.findForward("glAccountingCalendar"));
	        }
	     }
             ((GlAccountingCalendarForm)form).setUserPermission(frParam.trim());
          }else{
	     ((GlAccountingCalendarForm)form).setUserPermission(Constants.NO_ACCESS);
	  }
	  
/*******************************************************
   Initialize GlAccountingCalendarController EJB
*******************************************************/

          GlAccountingCalendarControllerHome homeAC = null;
          GlAccountingCalendarController ejbAC = null;
       
          try{
          	
	         homeAC = (GlAccountingCalendarControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlAccountingCalendarControllerEJB", GlAccountingCalendarControllerHome.class);
                
          }catch(NamingException e){
             if(log.isInfoEnabled()){
	       log.info("NamingException caught in GlAccountingCalendarAction.execute(): " + e.getMessage() + 
	          " session: " + session.getId()); 
	     }
             return(mapping.findForward("cmnErrorPage")); 
          }
       
          try{
              ejbAC = homeAC.create();
          }catch(CreateException e){
             if(log.isInfoEnabled()){
                log.info("CreateException caught in GlAccountingCalendarAction.execute(): " + e.getMessage() + 
	           " session: " + session.getId());
             }
             return(mapping.findForward("cmnErrorPage"));
          }
	  
          ActionErrors errors = new ActionErrors();
          
/*******************************************************
   -- Gl AC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlAccountingCalendarForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glAccountingCalendar"));
	     
/*******************************************************
   -- Gl AC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlAccountingCalendarForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glAccountingCalendar"));                   
          
/*******************************************************
   -- GL AC Save Action --
*******************************************************/

	  } else if(request.getParameter("saveButton") != null &&
	     ((GlAccountingCalendarForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     GlAccountingCalendarDetails details = new GlAccountingCalendarDetails(
	        ((GlAccountingCalendarForm)form).getAccountingCalendarName().trim().toUpperCase(),
		((GlAccountingCalendarForm)form).getDescription().trim().toUpperCase());
	  
/*******************************************************
   Call GlAccountingCalendarController EJB 
   addGlAcEntry(GlAccountingCalendarDetails details,
   String AC_PT_NM) method
*******************************************************/

	     try{
                ejbAC.addGlAcEntry(details, ((GlAccountingCalendarForm)form).getPeriodType().trim(), user.getCmpCode());
	     }catch(GlACAccountingCalendarAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("accountingCalendar.error.accountingCalendarNameAlreadyExists"));
             }catch(GlPTNoPeriodTypeFoundException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
		   new ActionMessage("accountingCalendar.error.periodTypeNotFound"));
             }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlAccountingCalendarAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
	        }
                return(mapping.findForward("cmnErrorPage"));
	     }

/*******************************************************
   -- GL AC Close Action --
*******************************************************/

	  }else if(request.getParameter("closeButton") != null){
	     return(mapping.findForward("cmnMain"));
	      
/*******************************************************
   -- GL AC Update Action --
*******************************************************/

	  }else if(request.getParameter("updateButton") != null &&
	     ((GlAccountingCalendarForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     GlAccountingCalendarList glACList = 
	        ((GlAccountingCalendarForm)form).getGlACByIndex(((GlAccountingCalendarForm) form).getRowSelected());
	    
	     GlAccountingCalendarDetails details = new GlAccountingCalendarDetails(
	        glACList.getAccountingCalendarCode(),
                ((GlAccountingCalendarForm)form).getAccountingCalendarName().trim().toUpperCase(),
                ((GlAccountingCalendarForm)form).getDescription().trim().toUpperCase());
	       
/*******************************************************
   Call GlAccountingCalendarController EJB updateGlAcEntry(
   GlAccountingCalendarDetails details, String
   AC_PT_NM) method
*******************************************************/

	     try {
                ejbAC.updateGlAcEntry(details, ((GlAccountingCalendarForm)form).getPeriodType().trim(), user.getCmpCode());
             }catch(GlACAccountingCalendarAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("accountingCalendar.error.accountingCalendarNameAlreadyExists"));
	     }catch(GlACAccountingCalendarAlreadyAssignedException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("accountingCalendar.error.updateAccountingCalendarAlreadyAssigned"));
	     }catch(GlPTNoPeriodTypeFoundException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
		   new ActionMessage("accountingCalendar.error.periodTypeNotFound"));
             }catch(GlACAccountingCalendarAlreadyDeletedException ex){
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("accountingCalendar.error.accountingCalendarAlreadyDeleted"));
	     }catch(EJBException ex){
	        if(log.isInfoEnabled()){
		   log.info("EJBException caught in GlAccountingCalendarAction.execute(): " + ex.getMessage() +
	              " session: " + session.getId());
	        }
	        return(mapping.findForward("cmnErrorPage"));
	     }

/*******************************************************
   -- GL AC Cancel Action --
*******************************************************/

	  }else if(request.getParameter("cancelButton") != null){
	       
/*******************************************************
   -- GL AC Edit Action --
*******************************************************/

	  }else if(request.getParameter("glACList[" + 
             ((GlAccountingCalendarForm) form).getRowSelected() + "].editButton") != null &&
	     ((GlAccountingCalendarForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlAccountingCalendarForm properties
*******************************************************/

	        ((GlAccountingCalendarForm) form).showGlACRow(((GlAccountingCalendarForm) form).getRowSelected());
		return(mapping.findForward("glAccountingCalendar"));
		     
/*******************************************************
  -- GL AC Delete Action --
*******************************************************/

	   }else if(request.getParameter("glACList[" + 
	      ((GlAccountingCalendarForm) form).getRowSelected() + "].deleteButton") != null &&
	      ((GlAccountingCalendarForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	      GlAccountingCalendarList glACList =
                 ((GlAccountingCalendarForm)form).getGlACByIndex(((GlAccountingCalendarForm) form).getRowSelected());

/*******************************************************
   Call GlAccountingCalendarController EJB deleteGlAcEntry(
   Integer AC_CODE)
*******************************************************/

              try{
	         ejbAC.deleteGlAcEntry(glACList.getAccountingCalendarCode(), user.getCmpCode());
	      }catch(GlACAccountingCalendarAlreadyAssignedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
		    new ActionMessage("accountingCalendar.error.deleteAccountingCalendarAlreadyAssigned"));
	      }catch(GlACAccountingCalendarAlreadyDeletedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
		    new ActionMessage("accountingCalendar.error.accountingCalendarAlreadyDeleted"));
	      }catch(EJBException ex){
                 if(log.isInfoEnabled()){
                    log.info("EJBException caught in GlAccountingCalendarAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	         }
	         return(mapping.findForward("cmnErrorPage"));
	      }

/*******************************************************
   -- GL AC Load Action --
*******************************************************/

	   }
	   if(frParam != null){
   	      if (!errors.isEmpty()) {
	         saveErrors(request, new ActionMessages(errors));
		 return(mapping.findForward("glAccountingCalendar"));
	      }
		    
  	      ((GlAccountingCalendarForm)form).clearGlACList();
  	      ArrayList ejbGlACList = null;
		       
/*******************************************************
   Call GlAccountingCalendarController EJB getGlPtAll()
   Populate PeriodTypeList property from GlAccounting
   CalendarController EJB getGlPtAll() method ArrayList
   return
*******************************************************/

              ((GlAccountingCalendarForm)form).clearPeriodTypeList();
              Iterator i = null;
	      try{
	         ejbGlACList = ejbAC.getGlPtAll(user.getCmpCode());
	         i = ejbGlACList.iterator();
	         while(i.hasNext()){
	            ((GlAccountingCalendarForm)form).setPeriodTypeList(
	            ((GlPeriodTypeDetails)i.next()).getPtName());
	         }
	      }catch(GlPTNoPeriodTypeFoundException ex){
	         ((GlAccountingCalendarForm)form).setPeriodTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
	      }
		    
/*******************************************************
   Call GlAccountingCalendarController EJB
   getGlAcAll() method
   Populate ArrayList property from 
   GlAccountingCalendarController
   EJB getGlAcAll() method ArrayList return
*******************************************************/
       
	      try{
	         ejbGlACList = ejbAC.getGlAcAll(user.getCmpCode());
		 i = ejbGlACList.iterator();
                 while(i.hasNext()){
                    GlModAccountingCalendarDetails mdetails = (GlModAccountingCalendarDetails)i.next();
                    GlAccountingCalendarList glACList = new GlAccountingCalendarList(((GlAccountingCalendarForm)form),
                       mdetails.getAcCode(),
                       mdetails.getAcName(),
                       mdetails.getAcPtName(),
                       mdetails.getAcDescription());
                      ((GlAccountingCalendarForm)form).saveGlACList(glACList);
                 }
	      }catch(GlACNoAccountingCalendarFoundException ex){
	         
	      }catch(EJBException ex){
	         if(log.isInfoEnabled()){
	            log.info("EJBException caught in GlAccountingCalendarAction.execute(): " + ex.getMessage() + 
	               " session: " + session.getId());
	          }
	          return(mapping.findForward("cmnErrorPage"));
	      }
	
	      if(!errors.isEmpty()){
	         saveErrors(request, new ActionMessages(errors));
	      }else{
	         if((request.getParameter("saveButton") != null || request.getParameter("updateButton") != null ||
		    request.getParameter("glACList[" +
		    ((GlAccountingCalendarForm) form).getRowSelected() + "].deleteButton") != null) &&
		    ((GlAccountingCalendarForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

		    ((GlAccountingCalendarForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
		 }
	      }
	      
	      ((GlAccountingCalendarForm)form).reset(mapping, request);
	      
	        if (((GlAccountingCalendarForm)form).getTableType() == null) {
      		      	
	           ((GlAccountingCalendarForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           	      
	      
	      ((GlAccountingCalendarForm)form).setPageState(Constants.PAGE_STATE_SAVE);
              return(mapping.findForward("glAccountingCalendar"));
	   }else{
	      errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
              saveErrors(request, new ActionMessages(errors));
               		   
	      return(mapping.findForward("cmnMain"));
	   }
      }catch(Exception e){

/*******************************************************
   Failed: Forward to error page
********************************************************/
         if(log.isInfoEnabled()){
	    log.info("Exception caught in GlAccountingCalendarAction.execute(): " + e.getMessage() + " session: " 
	       + session.getId());
	 }
 	return(mapping.findForward("cmnErrorPage"));
      }  
    }
}
