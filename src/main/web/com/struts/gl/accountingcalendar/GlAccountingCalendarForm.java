package com.struts.gl.accountingcalendar;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlAccountingCalendarForm extends ActionForm implements Serializable{

	private String accountingCalendarName = null;	
	private String periodType = null;
	private ArrayList periodTypeList = new ArrayList();
	private String description = null;
    private String tableType = null;

    private String showDetailsButton = null;
    private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList glACList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String(); 
	
	public int getRowSelected(){
	   return rowSelected;
	}
	
        public GlAccountingCalendarList getGlACByIndex(int index){
	   return((GlAccountingCalendarList)glACList.get(index));
	}
	public Object[] getGlACList(){
	    return(glACList.toArray());
	}

	public int getGlACListSize(){
	   return(glACList.size());
	}

	public void saveGlACList(Object newGlACList){
	   glACList.add(newGlACList);
	}

	public void clearGlACList(){
	   glACList.clear();
	}
	
	public void setRowSelected(Object selectedGlACList, boolean isEdit){
	   this.rowSelected = glACList.indexOf(selectedGlACList);
	   if(isEdit){
	      this.pageState = Constants.PAGE_STATE_EDIT;
	   }
	}
	
	public void showGlACRow(int rowSelected){
	   this.accountingCalendarName = ((GlAccountingCalendarList)glACList.get(rowSelected)).getAccountingCalendarName();
	   this.periodType = ((GlAccountingCalendarList)glACList.get(rowSelected)).getPeriodType();
	   this.description = ((GlAccountingCalendarList)glACList.get(rowSelected)).getDescription();
	}

	public void updateGlACRow(int rowSelected, Object newGlACList){
	   glACList.set(rowSelected, newGlACList);
	}

	public void deleteGlACList(int rowSelected){
	   glACList.remove(rowSelected);
	}
	
	public void setUpdateButton(String updateButton){
	   this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton){
	   this.cancelButton = cancelButton;
	}
	
	public void setSaveButton(String saveButton){
	   this.saveButton = saveButton;
	}

	public void setCloseButton(String closeButton){
	   this.closeButton = closeButton;
	}
	
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }	

	public void setPageState(String pageState){
	   this.pageState = pageState;
	}

	public String getPageState(){
	    return(pageState);
	}

        public String getTxnStatus(){
	   String passTxnStatus = txnStatus;
	   txnStatus = Constants.GLOBAL_BLANK;
	   return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
	   this.txnStatus = txnStatus;
	}

        public String getAccountingCalendarName(){
	   return(accountingCalendarName);
	}

	public void setAccountingCalendarName(String accountingCalendarName){
	   this.accountingCalendarName = accountingCalendarName;
	}

	public String getPeriodType(){
	  return(periodType);
	}

	public void setPeriodType(String periodType){
	   this.periodType = periodType;
	}

	public String getDescription(){
	   return(description);
	}

	public void setDescription(String description){
	   this.description = description;
	}

        public String getUserPermission(){
	   return(userPermission);
	}

	public void setUserPermission(String userPermission){
	   this.userPermission = userPermission;
	}

	public ArrayList getPeriodTypeList(){
	   return(periodTypeList);
	}

	public void setPeriodTypeList(String periodType){
	   periodTypeList.add(periodType);
	}

	public void clearPeriodTypeList(){
	   periodTypeList.clear();
	   periodTypeList.add(Constants.GLOBAL_BLANK);
	}
	
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }	

        public void reset(ActionMapping mapping, HttpServletRequest request){
	   accountingCalendarName = null;
	   periodType = Constants.GLOBAL_BLANK;
	   description = null;
	   saveButton = null;
	   closeButton = null;
	   updateButton = null;
	   cancelButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;	   
        }

        public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
           ActionErrors errors = new ActionErrors();
           if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
             if(Common.validateRequired(accountingCalendarName)) {
                errors.add("accountingCalendarName", 
		   new ActionMessage("accountingCalendar.error.accountingCalendarNameRequired"));
             }
	     if(Common.validateRequired(periodType)){
	        errors.add("periodType", new ActionMessage("accountingCalendar.error.periodTypeRequired"));
	     }
	     if(!Common.validateStringExists(periodTypeList, periodType)){
	        errors.add("periodType", new ActionMessage("accountingCalendar.error.periodTypeInvalid"));
	     }
	     if(periodType.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
	        errors.add("periodType", new ActionMessage("accountingCalendar.error.periodTypeNotFound"));
	     }
	   }
	  return(errors);
	}
		
}
