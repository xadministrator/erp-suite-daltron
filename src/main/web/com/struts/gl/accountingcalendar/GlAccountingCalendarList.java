package com.struts.gl.accountingcalendar;

import java.io.Serializable;

public class GlAccountingCalendarList implements Serializable{

	private Integer accountingCalendarCode = null;
	private String accountingCalendarName = null;	
	private String periodType = null;
	private String description = null;
	
	private String deleteButton = null;
	private String editButton = null;
	
	private GlAccountingCalendarForm parentBean;

	public GlAccountingCalendarList(GlAccountingCalendarForm parentBean, Integer accountingCalendarCode, 
	      String accountingCalendarName, String periodType, String description){
	   this.parentBean = parentBean;
	   this.accountingCalendarCode = accountingCalendarCode;
	   this.accountingCalendarName = accountingCalendarName;
	   this.periodType = periodType;
	   this.description = description;
	}

	public void setParentBean(GlAccountingCalendarForm parentBean){
	   this.parentBean = parentBean;
	}

	public void setDeleteButton(String deleteButton){
	   parentBean.setRowSelected(this, false);
	}

	public void setEditButton(String editButton){
	   parentBean.setRowSelected(this, true);
	}

	public Integer getAccountingCalendarCode(){
	   return(accountingCalendarCode);
	}

	public void setAccountingCalendarCode(Integer accountingCalendarCode){
	   this.accountingCalendarCode = accountingCalendarCode;
	}

        public String getAccountingCalendarName(){
	   return(accountingCalendarName);
	}

	public void setAccountingCalendarName(String accountingCalendarName){
	   this.accountingCalendarName = accountingCalendarName;
	}

	public String getPeriodType(){
	   return(periodType);
	}

	public void setPeriodType(String periodType){
	   this.periodType = periodType;
	}

	public String getDescription(){
	   return(description);
	}

	public void setDescription(String description){
	   this.description = description;
	}

}
