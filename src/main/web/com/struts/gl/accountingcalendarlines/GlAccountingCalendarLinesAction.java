package com.struts.gl.accountingcalendarlines;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlACAccountingCalendarAlreadyAssignedException;
import com.ejb.exception.GlACNoAccountingCalendarFoundException;
import com.ejb.exception.GlACVDateIsNotSequentialOrHasGapException;
import com.ejb.exception.GlACVLastPeriodIncorrectException;
import com.ejb.exception.GlACVNoAccountingCalendarValueFoundException;
import com.ejb.exception.GlACVNotTotalToOneYearException;
import com.ejb.exception.GlACVPeriodNumberNotUniqueException;
import com.ejb.exception.GlACVPeriodOverlappedException;
import com.ejb.exception.GlACVPeriodPrefixNotUniqueException;
import com.ejb.exception.GlACVQuarterIsNotSequentialOrHasGapException;
import com.ejb.exception.GlPTNoPeriodTypeFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.txn.GlAccountingCalendarValueController;
import com.ejb.txn.GlAccountingCalendarValueControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlAccountingCalendarDetails;
import com.util.GlAccountingCalendarValueDetails;
import com.util.GlModAccountingCalendarDetails;

public final class GlAccountingCalendarLinesAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
   
   
       HttpSession session = request.getSession();
       try{
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          if (user != null) {
             if (log.isInfoEnabled()){
                log.info("GlAccountingCalendarLinesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             }
          }else{
             if (log.isInfoEnabled()){
	        log.info("User is not logged on in session " + session.getId());
             }
	     return(mapping.findForward("adLogon"));
          }

          String frParam = Common.getUserPermission(user, Constants.GL_ACCOUNTING_CALENDAR_LINES_ID);
          if(frParam != null){
  	     if(frParam.trim().equals(Constants.FULL_ACCESS)){
	        ActionErrors fieldErrors = ((GlAccountingCalendarLinesForm)form).validateFields(mapping, request);
		if(!fieldErrors.isEmpty()){
		   saveErrors(request, new ActionMessages(fieldErrors));
		   return(mapping.findForward("glAccountingCalendarLines"));
		}
	     }
	     ((GlAccountingCalendarLinesForm)form).setUserPermission(frParam.trim());
	  }else{
	     ((GlAccountingCalendarLinesForm)form).setUserPermission(Constants.NO_ACCESS);
	  }
	  
/*******************************************************
   Initialize GlAccountingCalendarValues EJB
*******************************************************/

          GlAccountingCalendarValueControllerHome homeACV = null;
          GlAccountingCalendarValueController ejbACV = null;
       
          try{
          	
	         homeACV = (GlAccountingCalendarValueControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlAccountingCalendarValueControllerEJB", GlAccountingCalendarValueControllerHome.class);
                
          }catch(NamingException e){
             if(log.isInfoEnabled()){
	       log.info("NamingException caught in GlAccountingCalendarValueAction.execute(): " + e.getMessage() + 
	          " session: " + session.getId()); 
	     }
             return(mapping.findForward("cmnErrorPage")); 
          }
       
          try{
              ejbACV = homeACV.create();
          }catch(CreateException e){
             if(log.isInfoEnabled()){
                log.info("CreateException caught in GlAccountingCalendarValueAction.execute(): " + e.getMessage() + 
	           " session: " + session.getId());
             }
             return(mapping.findForward("cmnErrorPage"));
          }
	  
          ActionErrors errors = new ActionErrors();
          
/*******************************************************
   -- Gl ACV Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlAccountingCalendarLinesForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glAccountingCalendarLines"));
	     
/*******************************************************
   -- Gl ACV Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlAccountingCalendarLinesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glAccountingCalendarLines"));                   
 
/*******************************************************
   -- GL ACV Add Action --
*******************************************************/

	  } else if(request.getParameter("addButton") != null && 
	     ((GlAccountingCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	  
	     GlAccountingCalendarLinesList glACVList = new GlAccountingCalendarLinesList(
	        ((GlAccountingCalendarLinesForm)form),
		((GlAccountingCalendarLinesForm)form).getPeriodPrefix().trim().toUpperCase(),
		((GlAccountingCalendarLinesForm)form).getQuarterNumber().trim().toUpperCase(),
		((GlAccountingCalendarLinesForm)form).getPeriodNumber().trim(),
		Common.formatDate(((GlAccountingCalendarLinesForm)form).getDateFrom().trim()),
		Common.formatDate(((GlAccountingCalendarLinesForm)form).getDateTo().trim()));
	     
	     ((GlAccountingCalendarLinesForm)form).saveGlACVList(glACVList);
	     ((GlAccountingCalendarLinesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
	     ((GlAccountingCalendarLinesForm) form).reset(mapping, request);
	     
	        if (((GlAccountingCalendarLinesForm)form).getTableType() == null) {
      		      	
	           ((GlAccountingCalendarLinesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           	     
	
	     return(mapping.findForward("glAccountingCalendarLines"));
	     
/*******************************************************
   -- GL ACV Close Action --
*******************************************************/

	  }else if(request.getParameter("closeButton") != null){
	     return(mapping.findForward("cmnMain"));
	      
/*******************************************************
   -- GL ACV Update Action --
*******************************************************/

	  }else if(request.getParameter("updateButton") != null &&
	     ((GlAccountingCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
  	     GlAccountingCalendarLinesList glACVList = new GlAccountingCalendarLinesList(
                ((GlAccountingCalendarLinesForm)form),
                ((GlAccountingCalendarLinesForm)form).getPeriodPrefix().trim().toUpperCase(),
                ((GlAccountingCalendarLinesForm)form).getQuarterNumber().trim(),
                ((GlAccountingCalendarLinesForm)form).getPeriodNumber().trim(),
                Common.formatDate(((GlAccountingCalendarLinesForm)form).getDateFrom().trim()),
                Common.formatDate(((GlAccountingCalendarLinesForm)form).getDateTo().trim()));

             ((GlAccountingCalendarLinesForm)form).updateGlACVRow(((GlAccountingCalendarLinesForm)form).getRowSelected(),
	        glACVList);
	     
             ((GlAccountingCalendarLinesForm) form).reset(mapping, request);
             
	        if (((GlAccountingCalendarLinesForm)form).getTableType() == null) {
      		      	
	           ((GlAccountingCalendarLinesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           	                  
             
	     ((GlAccountingCalendarLinesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
	     
             return(mapping.findForward("glAccountingCalendarLines"));

/*******************************************************
   -- GL ACV Cancel Action --
*******************************************************/

	  }else if(request.getParameter("cancelButton") != null){
	     ((GlAccountingCalendarLinesForm) form).reset(mapping, request);
	     
	        if (((GlAccountingCalendarLinesForm)form).getTableType() == null) {
      		      	
	           ((GlAccountingCalendarLinesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           	     	     
	     
	     ((GlAccountingCalendarLinesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
	       
	     return(mapping.findForward("glAccountingCalendarLines"));

/*******************************************************
   -- GL ACV Edit Action --
*******************************************************/

	  }else if(request.getParameter("glACVList[" + 
             ((GlAccountingCalendarLinesForm) form).getRowSelected() + "].editButton") != null &&
	     ((GlAccountingCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlAccountingCalendarLinesForm properties
*******************************************************/

	        ((GlAccountingCalendarLinesForm) form).showGlACVRow(((GlAccountingCalendarLinesForm) form).getRowSelected());
		return(mapping.findForward("glAccountingCalendarLines"));
		     
/*******************************************************
  -- GL ACV Delete Action --
*******************************************************/

	   }else if(request.getParameter("glACVList[" + 
	      ((GlAccountingCalendarLinesForm) form).getRowSelected() + "].deleteButton") != null &&
	      ((GlAccountingCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	      
	      ((GlAccountingCalendarLinesForm) form).deleteGlACVList(((GlAccountingCalendarLinesForm) form).getRowSelected());
	      ((GlAccountingCalendarLinesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
	      return(mapping.findForward("glAccountingCalendarLines")); 

/*******************************************************
   --GL ACV Save Action --
*******************************************************/

           }else if(request.getParameter("saveButton") != null &&
	      ((GlAccountingCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	      ArrayList detailsACVList = new ArrayList();
	      for(int i=0; i<((GlAccountingCalendarLinesForm)form).getGlACVListSize(); i++){
	         GlAccountingCalendarLinesList glACVList = ((GlAccountingCalendarLinesForm)form).getGlACVByIndex(i);
		 GlAccountingCalendarValueDetails details = new GlAccountingCalendarValueDetails(
		    glACVList.getPeriodPrefix().trim().toUpperCase(), 
		    Common.convertStringToShort(glACVList.getQuarterNumber()), 
		    Common.convertStringToShort(glACVList.getPeriodNumber()),
		    Common.convertStringToSQLDate(glACVList.getDateFrom()), 
		    Common.convertStringToSQLDate(glACVList.getDateTo()));
                  
		  detailsACVList.add(details);
	      }

/*******************************************************
   Call GlAccountingCalendarValues EJB saveGlAcvEntry(
   ArrayList acvList, String AC_NM)) method
********************************************************/
	      try{
	         ejbACV.saveGlAcvEntry(detailsACVList, 
		    ((GlAccountingCalendarLinesForm)form).getAccountingCalendar(), user.getCmpCode());
	      }catch(GlACAccountingCalendarAlreadyAssignedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
		    new ActionMessage("accountingCalendarLines.error.accountingCalendarAlreadyAssigned"));   
	      }catch(GlACNoAccountingCalendarFoundException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("accountingCalendarLines.error.noAccountingCalendarFound"));
	      }catch(GlACVPeriodPrefixNotUniqueException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("accountingCalendarLines.error.periodPrefixLinesMustBeUnique"));
	      }catch(GlACVPeriodNumberNotUniqueException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("accountingCalendarLines.error.periodNumberLinesMustBeUnique"));
	      }catch(GlACVPeriodOverlappedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("accountingCalendarLines.error.nonAdjustingPeriodOverlapped"));	      
	      }catch(GlACVLastPeriodIncorrectException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("accountingCalendarLines.error.incompleteNumberOfPeriods"));
              }catch(GlACVQuarterIsNotSequentialOrHasGapException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("accountingCalendarLines.error.quarterNumberNotSequentialOrHasGap"));
	      }catch(GlACVDateIsNotSequentialOrHasGapException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
                     new ActionMessage("accountingCalendarLines.error.nonAdjustingPeriodDateNotSequentialOrHasGap"));
	      }catch(GlACVNotTotalToOneYearException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("accountingCalendarLines.error.totalPeriodsNotEqualToOneYear"));
	      }catch(GlobalRecordInvalidException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("accountingCalendarLines.error.recordInvalidException"));
	      }catch(GlobalRecordAlreadyExistException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("accountingCalendarLines.error.recordAlreadyExistException"));   
	      }catch(EJBException ex){
                 if(log.isInfoEnabled()){
                    log.info("EJBException caught in GlAccountingCalendarLinesAction.execute(): " + 
		       ex.getMessage() + " session: " + session.getId());
		 }
		 return(mapping.findForward("cmnErrorPage"));
             }	      
           }
	   
/*******************************************************
   -- GL ACV Load/Go Action --
*******************************************************/

	   
	         if(frParam != null){
		    if (!errors.isEmpty()) {
                       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glAccountingCalendarLines")); 
                    }
		    ((GlAccountingCalendarLinesForm)form).clearGlACVList();

/*******************************************************
   Call GlAccountingCalendarValues EJB getGlAcAll()
   method
********************************************************/
                 if(request.getParameter("saveButton") == null && 
		    request.getParameter("goButton") == null){
		    ((GlAccountingCalendarLinesForm)form).clearAccountingCalendarList();
		    try{
		       ArrayList ejbGlACList = ejbACV.getGlAcAll(user.getCmpCode());
		       Iterator i = ejbGlACList.iterator();
		       while(i.hasNext()){
			   ((GlAccountingCalendarLinesForm)form).setAccountingCalendarList(
			      ((GlAccountingCalendarDetails)i.next()).getAcName());
		       }
		    }catch(GlACNoAccountingCalendarFoundException ex){
		       ((GlAccountingCalendarLinesForm)form).setAccountingCalendarList(Constants.GLOBAL_NO_RECORD_FOUND);
		    }catch(EJBException ex){
		       if(log.isInfoEnabled()){
		          log.info("EJBException caught in GlAccountingCalendarLinesAction.execute(): " + 
			     ex.getMessage() + " session: " + session.getId());
		       }
		       return(mapping.findForward("cmnErrorPage"));
		    }
		    ((GlAccountingCalendarLinesForm)form).setAccountingCalendar(null);
		    ((GlAccountingCalendarLinesForm)form).setDescription(null);
		    ((GlAccountingCalendarLinesForm)form).setPeriodType(null);
		 }
		 
/*******************************************************
Call GlAccountingCalendarValues EJB
getGlPtNameAndAcDescriptionByGlAcName(String ACV_AC_NM)
method
******************************************************/

                    if(((GlAccountingCalendarLinesForm)form).getAccountingCalendar() != null){
		       try{
		          GlModAccountingCalendarDetails details = ejbACV.getGlPtNameAndAcDescriptionByGlAcName(
		             ((GlAccountingCalendarLinesForm)form).getAccountingCalendar(), user.getCmpCode());
		           ((GlAccountingCalendarLinesForm)form).setDescription(details.getAcDescription());
			   ((GlAccountingCalendarLinesForm)form).setPeriodType(details.getAcPtName());
		       }catch(GlACNoAccountingCalendarFoundException ex){
		           errors.add(ActionMessages.GLOBAL_MESSAGE,
                              new ActionMessage("accountingCalendarLines.error.noAccountingCalendarFound"));					     }catch(GlPTNoPeriodTypeFoundException ex){
   			   errors.add(ActionMessages.GLOBAL_MESSAGE,
			      new ActionMessage("accountingCalendarLines.error.noPeriodTypeFound"));
		       }catch(EJBException ex){
		           if(log.isInfoEnabled()){
                              log.info("EJBException caught in GlAccountingCalendarLinesAction.execute(): " +
                                 ex.getMessage() + " session: " + session.getId());
		           }
		           return(mapping.findForward("cmnErrorPage"));
		       }
		       if(!errors.isEmpty()){
                          saveErrors(request, new ActionMessages(errors));
		          return(mapping.findForward("glAccountingCalendarLines"));
                       }
		    
/*******************************************************
   Call GlAccountingCalendarValues EJB getGlAcvByAcName(
   String ACV_AC_NM) method
********************************************************/
	               try{
                          ArrayList ejbGlACVList = ejbACV.getGlAcvByGlAcName(
                          ((GlAccountingCalendarLinesForm)form).getAccountingCalendar(), user.getCmpCode());
		          Iterator i = ejbGlACVList.iterator();
		          GlAccountingCalendarLinesList glACVList = null;
		          while(i.hasNext()){
		             GlAccountingCalendarValueDetails details = (GlAccountingCalendarValueDetails)i.next();
		             glACVList = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
			        details.getAcvPeriodPrefix(),
			        Common.convertShortToString(details.getAcvQuarter()),
			        Common.convertShortToString(details.getAcvPeriodNumber()),
			        Common.convertSQLDateToString(details.getAcvDateFrom()),
			        Common.convertSQLDateToString(details.getAcvDateTo()));
		             ((GlAccountingCalendarLinesForm)form).saveGlACVList(glACVList);
		             
		            if (details.getAcvCode() == null) {
		            	
		            	((GlAccountingCalendarLinesForm)form).setShowButtons(true);
		            	
		            } else {
		            	
		            	((GlAccountingCalendarLinesForm)form).setShowButtons(false);
		            	
		            }
		          }
		          		          	
		          
		          	
		       }catch(GlACVNoAccountingCalendarValueFoundException ex){
		       	
                   ((GlAccountingCalendarLinesForm)form).setShowButtons(true);
                   
                   // generate default (calendar year)
                   
                   GregorianCalendar currentGc = new GregorianCalendar();
                   
                   GregorianCalendar gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 0, 1, 0, 0, 0);                                  
                   GregorianCalendar gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 0, 31, 0, 0, 0);                                  
                                      
                   GlAccountingCalendarLinesList newList1 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "JAN", "1", "1", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList1);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 1, 1, 0, 0, 0);                                  
                   
                   if (!currentGc.isLeapYear(currentGc.get(Calendar.YEAR))) {
                   	
                      gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 1, 28, 0, 0, 0);                                  	
                   	
                   } else {
                   	
                   	  gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 1, 29, 0, 0, 0);                                  	
                   	
                   }
                   
                       
                   GlAccountingCalendarLinesList newList2 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "FEB", "1", "2", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));                                        
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList2);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 2, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 2, 31, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList3 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "MAR", "1", "3", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList3);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 3, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 3, 30, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList4 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "APR", "2", "4", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList4);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 4, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 4, 31, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList5 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "MAY", "2", "5", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList5);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 5, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 5, 30, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList6 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "JUN", "2", "6", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList6);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 6, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 6, 31, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList7 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "JUL", "3", "7", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList7);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 7, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 7, 31, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList8 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "AUG", "3", "8", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList8);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 8, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 8, 30, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList9 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "SEP", "3", "9", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList9);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 9, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 9, 31, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList10 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "OCT", "4", "10", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList10);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 10, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 10, 30, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList11 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "NOV", "4", "11", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                       
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList11);
                   
                   gcFrom = new GregorianCalendar(currentGc.get(Calendar.YEAR), 11, 1, 0, 0, 0);                                  
                   gcTo = new GregorianCalendar(currentGc.get(Calendar.YEAR), 11, 31, 0, 0, 0);                                  
                       
                   GlAccountingCalendarLinesList newList12 = new GlAccountingCalendarLinesList(((GlAccountingCalendarLinesForm)form),
                       "DEC", "4", "12", Common.convertSQLDateToString(gcFrom.getTime()), Common.convertSQLDateToString(gcTo.getTime()));
                   
                   ((GlAccountingCalendarLinesForm)form).saveGlACVList(newList12);
                       
 		       }catch(GlACNoAccountingCalendarFoundException ex){
                           errors.add(ActionMessages.GLOBAL_MESSAGE,
		              new ActionMessage("accountingCalendarLines.error.noAccountingCalendarFound"));
		       }catch(EJBException ex){
                          if(log.isInfoEnabled()){
                             log.info("EJBException caught in GlAccountingCalendarLinesAction.execute(): " +
                                ex.getMessage() + " session: " + session.getId());
                          }
                          return(mapping.findForward("cmnErrorPage"));
                       }
	               if(!errors.isEmpty()){
		          saveErrors(request, new ActionMessages(errors));			                    
		       }else{
		          if(request.getParameter("saveButton") != null &&
			  ((GlAccountingCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
			     ((GlAccountingCalendarLinesForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
			  }
		       }
		    }
		    ((GlAccountingCalendarLinesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
		    ((GlAccountingCalendarLinesForm) form).reset(mapping, request);
		    
	        if (((GlAccountingCalendarLinesForm)form).getTableType() == null) {
      		      	
	           ((GlAccountingCalendarLinesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           	     		    
		    
		    return(mapping.findForward("glAccountingCalendarLines"));
		}else{
		   errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                   saveErrors(request, new ActionMessages(errors));
		   
		   return(mapping.findForward("cmnMain"));
		}
	     
      }catch(Exception e){

/*******************************************************
   Failed: Forward to error page
********************************************************/

         if(log.isInfoEnabled()){
	    log.info("Exception caught in GlAccountingCalendarLinesAction.execute(): " + e.getMessage() + " session: " 
	       + session.getId());
	 }
 	return(mapping.findForward("cmnErrorPage"));
      } 
    }
}
