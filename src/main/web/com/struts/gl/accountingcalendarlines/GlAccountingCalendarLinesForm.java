package com.struts.gl.accountingcalendarlines;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlAccountingCalendarLinesForm extends ActionForm implements Serializable{

	private String accountingCalendar = null;
	private ArrayList accountingCalendarList = new ArrayList();
	private String description = null;
	private String periodType = null;
	
	private String periodPrefix = null;	
	private String quarterNumber = null;
	private ArrayList quarterNumberList = new ArrayList();
	private String periodNumber = null;
	private ArrayList periodNumberList = new ArrayList();
	private String dateFrom = null;
	private String dateTo = null;
    private String tableType = null;
    
    private boolean showButtons = false;

    private String showDetailsButton = null;
    private String hideDetailsButton = null;	
	private String saveButton = null;
	private String addButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String goButton = null;
	private String pageState = new String();
	private ArrayList glACVList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected(){
	   return rowSelected;
	}
	
        public GlAccountingCalendarLinesList getGlACVByIndex(int index){
	   return((GlAccountingCalendarLinesList)glACVList.get(index));
	}
	public Object[] getGlACVList(){
	    return(glACVList.toArray());
	}

	public int getGlACVListSize(){
	   return(glACVList.size());
	}

	public void saveGlACVList(Object newGlACVList){
	   glACVList.add(newGlACVList);
	}

	public void clearGlACVList(){
	   glACVList.clear();
	}
	
	public void setRowSelected(Object selectedGlACVList, boolean isEdit){
	   this.rowSelected = glACVList.indexOf(selectedGlACVList);
	   if(isEdit){
	      this.pageState = Constants.PAGE_STATE_EDIT;
	   }
	}
	
	public void showGlACVRow(int rowSelected){
	   this.periodPrefix = ((GlAccountingCalendarLinesList)glACVList.get(rowSelected)).getPeriodPrefix();
	   this.quarterNumber = ((GlAccountingCalendarLinesList)glACVList.get(rowSelected)).getQuarterNumber();
	   this.periodNumber = ((GlAccountingCalendarLinesList)glACVList.get(rowSelected)).getPeriodNumber();
	   this.dateFrom = ((GlAccountingCalendarLinesList)glACVList.get(rowSelected)).getDateFrom();
	   this.dateTo = ((GlAccountingCalendarLinesList)glACVList.get(rowSelected)).getDateTo();
	}

	public void updateGlACVRow(int rowSelected, Object newGlACVList){
	   glACVList.set(rowSelected, newGlACVList);
	}

	public void deleteGlACVList(int rowSelected){
	   glACVList.remove(rowSelected);
	}
	
	public void setUpdateButton(String updateButton){
	   this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton){
	   this.cancelButton = cancelButton;
	}
	
	public void setSaveButton(String saveButton){
	   this.saveButton = saveButton;
	}

	public void setAddButton(String addButton){
	   this.addButton = addButton;
	}

	public void setCloseButton(String closeButton){
	   this.closeButton = closeButton;
	}

	public void setGoButton(String goButton){
	   this.goButton = goButton;
	}
	
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }	

	public void setPageState(String pageState){
	   this.pageState = pageState;
	}

	public String getPageState(){
	    return(pageState);
	}

	public String getTxnStatus(){
	   String passTxnStatus = txnStatus;
	   txnStatus = Constants.GLOBAL_BLANK;
	   return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
	   this.txnStatus = txnStatus;
	}

        public String getUserPermission(){
	   return(userPermission);
	}

	public void setUserPermission(String userPermission){
	   this.userPermission = userPermission;
	}

	public String getAccountingCalendar(){
	   return(accountingCalendar);
	}

	public void setAccountingCalendar(String accountingCalendar){
	   this.accountingCalendar = accountingCalendar;
	}

	public ArrayList getAccountingCalendarList(){
	   return(accountingCalendarList);
	}

	public void setAccountingCalendarList(String accountingCalendar){
	   accountingCalendarList.add(accountingCalendar);
	}

	public void clearAccountingCalendarList(){
	   accountingCalendarList.clear();
	   accountingCalendarList.add(Constants.GLOBAL_BLANK);
	}

	public String getDescription(){
	   return(description); 
	}
	
	public void setDescription(String description){
	   this.description = description;
	}

	public String getPeriodType(){
	   return(periodType);
	}

	public void setPeriodType(String periodType){
	   this.periodType = periodType;
	}
	
	public String getPeriodPrefix(){
	   return(periodPrefix);
	}

	public void setPeriodPrefix(String periodPrefix){
	   this.periodPrefix = periodPrefix;
	}

	public String getQuarterNumber(){
	   return(quarterNumber);
	}

	public void setQuarterNumber(String quarterNumber){
	   this.quarterNumber = quarterNumber;
	}

	public ArrayList getQuarterNumberList(){
	   return(quarterNumberList);
	}

	public String getPeriodNumber(){
	   return(periodNumber);
	}

	public void setPeriodNumber(String periodNumber){
	   this.periodNumber = periodNumber;
	}

	public ArrayList getPeriodNumberList(){
	   return(periodNumberList);
	}

	public String getDateFrom(){
	   return(dateFrom);
	}

	public void setDateFrom(String dateFrom){
	   this.dateFrom = dateFrom;
	}

	public String getDateTo(){
	   return(dateTo);
	}

	public void setDateTo(String dateTo){
	   this.dateTo = dateTo;
	}

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public boolean getShowButtons() {
   	
   	  return showButtons;
   	
   }
   
   public void setShowButtons(boolean showButtons) {
   	
   	  this.showButtons = showButtons;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
	   periodPrefix = null;
	   quarterNumber = null;
	   periodNumber = null;
	   dateFrom = null;
	   dateTo = null;
	   saveButton = null;
	   addButton = null;
	   closeButton = null;
	   updateButton = null;
	   cancelButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;	   
	   periodNumberList.clear();
	   quarterNumberList.clear();
	   periodNumberList.add(Constants.GLOBAL_BLANK);
	   quarterNumberList.add(Constants.GLOBAL_BLANK);
           for(int i=1; i<367; i++){
              periodNumberList.add(String.valueOf(i));
           }
	   for(int i=1; i<5; i++){
	      quarterNumberList.add(String.valueOf(i));
	   }
        }
                

        public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
           ActionErrors errors = new ActionErrors();
	   if(request.getParameter("goButton") != null || request.getParameter("saveButton") != null ||
	      request.getParameter("addButton") != null || request.getParameter("updateButton") != null){
	      if(Common.validateRequired(accountingCalendar) || accountingCalendar.equals(Constants.GLOBAL_NO_RECORD_FOUND) ){
	         errors.add("accountingCalendar", 
	           new ActionMessage("accountingCalendarLines.error.accountingCalendarRequired"));
	      }
	   }
	   if(request.getParameter("addButton") != null || request.getParameter("updateButton") != null){
             if(Common.validateRequired(periodPrefix)) {
                errors.add("periodPrefix", new ActionMessage("accountingCalendarLines.error.periodPrefixRequired"));
             }
	     if(Common.validateRequired(quarterNumber)){
	        errors.add("quarterNumber", new ActionMessage("accountingCalendarLines.error.quarterNumberRequired"));
	     }
	     if(!Common.validateNumberFormat(quarterNumber)){
                errors.add("quarterNumber", new ActionMessage("accountingCalendarLines.error.quarterNumberInvalid"));
             }
             if(!Common.validateStringNumLessThanEqual(quarterNumber, 4)){
                errors.add("quarterNumber", new ActionMessage("accountingCalendarLines.error.quarterNumberInvalid"));
             }
	     if(Common.validateRequired(periodNumber)){
                errors.add("periodNumber", new ActionMessage("accountingCalendarLines.error.periodNumberRequired"));
             }
             if(!Common.validateNumberFormat(periodNumber)){
                errors.add("periodNumber", new ActionMessage("accountingCalendarLines.error.periodNumberInvalid"));
             }
             if(!Common.validateStringNumLessThanEqual(periodNumber, 366)){
                errors.add("periodNumberNumber", new ActionMessage("accountingCalendarLines.error.periodNumberInvalid"));
             }
	     if(Common.validateRequired(dateFrom)){
	        errors.add("dateFrom", new ActionMessage("accountingCalendarLines.error.dateFromRequired"));
	     }
	     if(Common.validateRequired(dateTo)){
	        errors.add("dateTo", new ActionMessage("accountingCalendarLines.error.dateToRequired"));
	     }
	     if(!Common.validateDateFormat(dateFrom)){
                errors.add("dateFrom", new ActionMessage("accountingCalendarLines.error.dateFromInvalid"));
             }
             if(!Common.validateDateFormat(dateTo)){
                errors.add("dateTo", new ActionMessage("accountingCalendarLines.error.dateToInvalid"));
             }
             if(!Common.validateDateFromTo(dateFrom, dateTo)){
                errors.add("dateFrom", new ActionMessage("accountingCalendarLines.error.dateFromToInvalid"));
             }
          }else if(request.getParameter("saveButton") != null){
	     if(glACVList.size() < 4){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("accountingCalendarLines.error.LinesLessThanFour"));
	     }
	  }
	  return(errors);
	}
		
}
