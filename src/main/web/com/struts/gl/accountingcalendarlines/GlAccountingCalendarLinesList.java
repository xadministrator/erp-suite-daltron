package com.struts.gl.accountingcalendarlines;

import java.io.Serializable;

public class GlAccountingCalendarLinesList implements Serializable{

	private String periodPrefix = null;	
	private String quarterNumber = null;
	private String periodNumber = null;
	private String dateFrom = null;
	private String dateTo = null;
	
	private String deleteButton = null;
	private String editButton = null;
	
	private GlAccountingCalendarLinesForm parentBean;

	public GlAccountingCalendarLinesList(GlAccountingCalendarLinesForm parentBean, String periodPrefix, 
	   String quarterNumber, String periodNumber, String dateFrom, String dateTo){
	   
	   this.parentBean = parentBean;
	   this.periodPrefix = periodPrefix;
	   this.quarterNumber = quarterNumber;
	   this.periodNumber = periodNumber;
	   this.dateFrom = dateFrom;
	   this.dateTo = dateTo;
	}

	public void setParentBean(GlAccountingCalendarLinesForm parentBean){
	   this.parentBean = parentBean;
	}

	public void setDeleteButton(String deleteButton){
	   parentBean.setRowSelected(this, false);
	}

	public void setEditButton(String editButton){
	   parentBean.setRowSelected(this, true);
	}

	public String getPeriodPrefix(){
	   return(periodPrefix);
	}

	public String getQuarterNumber(){
	   return(quarterNumber);
	}

	public String getPeriodNumber(){
	   return(periodNumber);
	}

	public String getDateFrom(){
	   return(dateFrom);
	}

	public String getDateTo(){
	   return(dateTo);
	}

}
