package com.struts.gl.approval;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.GlApprovalController;
import com.ejb.txn.GlApprovalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdModApprovalQueueDetails;

public final class GlApprovalAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlApprovalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlApprovalForm actionForm = (GlApprovalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_APPROVAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glApproval");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlApprovalController EJB
*******************************************************/

         GlApprovalControllerHome homeAPR = null;
         GlApprovalController ejbAPR = null;

         try {
          
            homeAPR = (GlApprovalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlApprovalControllerEJB", GlApprovalControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlApprovalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbAPR = homeAPR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlApprovalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbAPR.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlApprovalAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Gl APR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glApproval"));
	     
/*******************************************************
   -- Gl APR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glApproval"));         

/*******************************************************
   -- Gl APR Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Gl APR Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Gl APR Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
                        
            if (request.getParameter("goButton") != null) {
            	
            	HashMap criteria = new HashMap();
         		
         		if (!Common.validateRequired(actionForm.getDocument())) {
         			
         			criteria.put("document", actionForm.getDocument());
         		}	        		        	
         		
         		if (!Common.validateRequired(actionForm.getDateFrom())) {
         			
         			criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
         			
         		}
         		
         		if (!Common.validateRequired(actionForm.getDateTo())) {
         			
         			criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
         			
         		}
         		
         		if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
         			
         			criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
         			
         		}
         		
         		if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
         			
         			criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
         			
         		}
         		
         		// save criteria
         		
         		actionForm.setLineCount(0);
         		actionForm.setCriteria(criteria);
         		
         		// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbAPR.getAdAqByAqDocumentAndUserName(actionForm.getCriteria(), user.getUserName(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE),
                	    actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in CmApprovalAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
         		
         	}	     	
            
            try {
            	
            	actionForm.clearGlAPRList();
            	                        
            	ArrayList list = ejbAPR.getAdAqByAqDocumentAndUserName(actionForm.getCriteria(), user.getUserName(),
         				new Integer(actionForm.getLineCount()), 
         				new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		AdModApprovalQueueDetails mdetails = (AdModApprovalQueueDetails)i.next();
            		
            		GlApprovalList glAPRList = new GlApprovalList(actionForm,
            		    mdetails.getAqDocumentCode(),
            		    mdetails.getAqDocument(),
            		    Common.convertSQLDateToString(mdetails.getAqDate()),
                        mdetails.getAqJrName(),
                        mdetails.getAqDocumentNumber(),                        
                        Common.convertDoubleToStringMoney(mdetails.getAqAmount(), precisionUnit),
                        Common.convertDoubleToStringMoney(mdetails.getAqAmount(), precisionUnit),
                        mdetails.getAqDocumentType());
            		    
            		actionForm.saveGlAPRList(glAPRList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("glApproval.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlApprovalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glApproval");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("glApproval")); 

/*******************************************************
   -- Gl APR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl APR Post Action --
*******************************************************/

         } else if (request.getParameter("approveRejectButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
             // get posted journals
                        
		    for(int i=0; i<actionForm.getGlAPRListSize(); i++) {
		    
		       GlApprovalList actionList = actionForm.getGlAPRByIndex(i);
		    	
               if (actionList.getApprove() || actionList.getReject()) {
               	
               	     try {
               	     	
               	     	ejbAPR.executeGlApproval(actionList.getDocument(), 
               	     	    actionList.getDocumentCode(), user.getUserName(),
               	     	    actionList.getApprove(), actionList.getReasonForRejection(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteGlAPRList(i);
               	     	i--;
               	  		                  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("glApproval.error.recordAlreadyDeleted", actionList.getDocumentNumber()));
		             
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("glApproval.error.effectiveDatePeriodClosed", actionList.getDocumentNumber()));
		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("glApproval.error.transactionAlreadyPosted", actionList.getDocumentNumber()));
		
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in GlApprovalAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	            
	        }		
	        
	        if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glApproval");

            }  
	        
	        try {
            	
         		actionForm.setLineCount(0);
            	actionForm.clearGlAPRList();
            	                        
            	ArrayList list = ejbAPR.getAdAqByAqDocumentAndUserName(actionForm.getCriteria(), user.getUserName(),
         				new Integer(actionForm.getLineCount()), 
         				new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1),
						actionForm.getOrderBy(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		AdModApprovalQueueDetails mdetails = (AdModApprovalQueueDetails)i.next();
            		
            		GlApprovalList glAPRList = new GlApprovalList(actionForm,
            		    mdetails.getAqDocumentCode(),
            		    mdetails.getAqDocument(),
            		    Common.convertSQLDateToString(mdetails.getAqDate()),
                        mdetails.getAqJrName(),
                        mdetails.getAqDocumentNumber(),                        
                        Common.convertDoubleToStringMoney(mdetails.getAqAmount(), precisionUnit),
                        Common.convertDoubleToStringMoney(mdetails.getAqAmount(), precisionUnit),
                        mdetails.getAqDocumentType());
            		    
            		actionForm.saveGlAPRList(glAPRList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlApprovalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
	                       
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            return(mapping.findForward("glApproval")); 
	           

/*******************************************************
   -- Gl APR Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glApproval");

            }
            
            try {
            	
            	actionForm.clearGlAPRList();
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlApprovalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            
			actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("glApproval"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlApprovalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}