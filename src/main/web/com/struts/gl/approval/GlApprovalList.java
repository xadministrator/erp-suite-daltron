package com.struts.gl.approval;

import java.io.Serializable;

public class GlApprovalList implements Serializable {

   private Integer documentCode = null;
   private String document = null;   
   private String date = null;
   private String name = null;   
   private String documentNumber = null;
   private String totalDebit = null;
   private String totalCredit = null;
   private String documentType = null;
   private String reasonForRejection = null;
   
   private boolean approve = false;
   private boolean reject = false;
       
   private GlApprovalForm parentBean;
    
   public GlApprovalList(GlApprovalForm parentBean,
      Integer documentCode,
      String document,
      String date,
      String name,
      String documentNumber,
      String totalDebit,
      String totalCredit,
      String documentType) {

      this.parentBean = parentBean;
      this.documentCode = documentCode;
      this.document = document;
      this.date = date;
      this.name = name;
      this.documentNumber = documentNumber;
      this.totalDebit = totalDebit;
      this.totalCredit = totalCredit;
      this.documentType = documentType;
      
   }

   public Integer getDocumentCode() {

      return documentCode;

   }
   
   public String getDocument() {
   	
   	  return document;
   	
   }
   
   public String getDate() {
   
      return date;
      
   }

   public String getName() {

      return name;
      
   }
      
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getTotalDebit() {
   
      return totalDebit;
   
   }
   
   public String getTotalCredit() {
   	
   	   return totalCredit;
   	
   }
   
   public String getDocumentType() {
   	
   	  return documentType;
   	
   }
   
   public String getReasonForRejection() {
   	
   	  return reasonForRejection;
   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }

   public boolean getApprove() {
   	
   	  return approve;
   	
   }
   
   public void setApprove(boolean approve) {
   	
   	  this.approve = approve;
   	
   }
   
   public boolean getReject() {
   	
   	  return reject;
   	
   }
   
   public void setReject(boolean reject) {
   	
   	  this.reject = reject;
   	  
   }
         
}