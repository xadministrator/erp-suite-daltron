package com.struts.gl.budgetaccountassignment;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlBudgetAccountAssignmentForm extends ActionForm implements Serializable {
   
   private Integer organizationCode = null;
   private String organizationName = null;
   private String segmentOrder = null;
   private String accountFrom = null;
   private String accountTo = null;
   private String type = null;
   private ArrayList typeList = new ArrayList();
   private String tableType = null;

   private String pageState = new String();
   private ArrayList glBAAList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlBudgetAccountAssignmentList getGlBAAByIndex(int index) {

      return((GlBudgetAccountAssignmentList)glBAAList.get(index));

   }

   public Object[] getGlBAAList() {

      return glBAAList.toArray();

   }

   public int getGlBAAListSize() {

      return glBAAList.size();

   }

   public void saveGlBAAList(Object newGlBAAList) {

      glBAAList.add(newGlBAAList);

   }

   public void clearGlBAAList() {
      glBAAList.clear();
   }

   public void setRowSelected(Object selectedGlBAAList, boolean isEdit) {

      this.rowSelected = glBAAList.indexOf(selectedGlBAAList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlBAARow(int rowSelected) {
   	
       this.accountFrom = ((GlBudgetAccountAssignmentList)glBAAList.get(rowSelected)).getAccountFrom();
       this.accountTo = ((GlBudgetAccountAssignmentList)glBAAList.get(rowSelected)).getAccountTo();
       this.type = ((GlBudgetAccountAssignmentList)glBAAList.get(rowSelected)).getType();

   }

   public void updateGlBAARow(int rowSelected, Object newGlBAAList) {

      glBAAList.set(rowSelected, newGlBAAList);

   }

   public void deleteGlBAAList(int rowSelected) {

      glBAAList.remove(rowSelected);

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getOrganizationCode() {
   	
   	return organizationCode;
   	
   }
   
   public void setOrganizationCode(Integer organizationCode) {
   	
   	this.organizationCode = organizationCode;
   	
   }

   public String getOrganizationName() {

      return organizationName;

   }

   public void setOrganizationName(String organizationName) {

      this.organizationName = organizationName;

   }
   
   public String getSegmentOrder() {
   	
   	  return segmentOrder;
   	  
   }
   
   public void setSegmentOrder(String segmentOrder) {
   	
   	  this.segmentOrder = segmentOrder;
   	
   }
   
   public String getAccountFrom() {

      return accountFrom;

   }

   public void setAccountFrom(String accountFrom) {

      this.accountFrom = accountFrom;

   }   
   
   public String getAccountTo() {

      return accountTo;

   }

   public void setAccountTo(String accountTo) {

      this.accountTo = accountTo;

   }
   
   public String getType() {

      return type;

   }

   public void setType(String type) {

      this.type = type;

   }
   
   public ArrayList getTypeList() {
   	
   	  return typeList;
   	  
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

		accountFrom = null;
		accountTo = null;
		typeList.clear();
		typeList.add(Constants.GLOBAL_BLANK);
		typeList.add("ENTERED");
		type = Constants.GLOBAL_BLANK;
		
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(accountFrom)) {

            errors.add("accountFrom",
               new ActionMessage("budgetAccountAssignment.error.accountFromRequired"));

         }
         
         if (Common.validateRequired(accountTo)) {

            errors.add("accountTo",
               new ActionMessage("budgetAccountAssignment.error.accountToRequired"));

         }

      }
         
      return errors;

   }
}