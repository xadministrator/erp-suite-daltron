package com.struts.gl.budgetaccountassignment;

import java.io.Serializable;

public class GlBudgetAccountAssignmentList implements Serializable {

   private Integer budgetAccountAssignmentCode = null;
   private String accountFrom = null;
   private String accountTo = null;
   private String type = null;
   
   private String editButton = null;
   private String deleteButton = null;
    
   private GlBudgetAccountAssignmentForm parentBean;
    
   public GlBudgetAccountAssignmentList(GlBudgetAccountAssignmentForm parentBean,
		Integer budgetAccountAssignmentCode,
		String accountFrom,
		String accountTo, 
		String type) {
		
		this.parentBean = parentBean;
		this.budgetAccountAssignmentCode = budgetAccountAssignmentCode;
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.type = type;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }     
   
   public Integer getBudgetAccountAssignmentCode() {
   	
   	  return budgetAccountAssignmentCode;
   	  
   }
   
   public String getAccountFrom() {
   	
   	  return accountFrom;
   
   }
   
   public String getAccountTo() {
   	
   	  return accountTo;
   
   }
   
   public String getType() {
   	
   	  return type;
   	  
   }

}