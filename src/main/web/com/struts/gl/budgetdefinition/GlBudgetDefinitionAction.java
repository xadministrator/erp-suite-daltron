package com.struts.gl.budgetdefinition;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlBGTFirstLastPeriodDifferentAcYearException;
import com.ejb.exception.GlBGTFirstPeriodGreaterThanLastPeriodException;
import com.ejb.exception.GlBGTStatusAlreadyDefinedException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlBudgetDefinitionController;
import com.ejb.txn.GlBudgetDefinitionControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlBudgetDetails;
import com.util.GlModAccountingCalendarValueDetails;

public final class GlBudgetDefinitionAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlBudgetDefinitionAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlBudgetDefinitionForm actionForm = (GlBudgetDefinitionForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_BUDGET_DEFINITION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glBudgetDefinition");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlBudgetDefinitionController EJB
*******************************************************/

         GlBudgetDefinitionControllerHome homeBD = null;
         GlBudgetDefinitionController ejbBD = null;

         try {

            homeBD = (GlBudgetDefinitionControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlBudgetDefinitionControllerEJB", GlBudgetDefinitionControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlBudgetDefinitionAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBD = homeBD.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlBudgetDefinitionAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl Bgt Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glBudgetDefinition"));
	     
/*******************************************************
   -- Gl Bgt Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glBudgetDefinition"));                  
         
/*******************************************************
   -- Gl Bgt Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlBudgetDetails details = new GlBudgetDetails();
            details.setBgtName(actionForm.getBudgetName());
            details.setBgtDescription(actionForm.getBudgetDescription());
            details.setBgtStatus(actionForm.getBudgetStatus());
            details.setBgtFirstPeriod(actionForm.getFirstPeriod());
            details.setBgtLastPeriod(actionForm.getLastPeriod());

            try {
            	
            	ejbBD.addGlBgtEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.budgetNameAlreadyExist"));
               
            } catch (GlBGTStatusAlreadyDefinedException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.budgetStatusAlreadyDefined"));
               
            } catch (GlBGTFirstPeriodGreaterThanLastPeriodException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.firstPeriodGreaterThanLastPeriod"));
               
            } catch (GlBGTFirstLastPeriodDifferentAcYearException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.firstLastPeriodDifferentAcYear"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetDefinitionAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl Bgt Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl Bgt Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlBudgetDefinitionList glBGTList =
	            actionForm.getGlBGTByIndex(actionForm.getRowSelected());
                        
            GlBudgetDetails details = new GlBudgetDetails();
            details.setBgtCode(glBGTList.getBudgetCode());
            details.setBgtName(actionForm.getBudgetName());
            details.setBgtDescription(actionForm.getBudgetDescription());
            details.setBgtStatus(actionForm.getBudgetStatus());
            details.setBgtFirstPeriod(actionForm.getFirstPeriod());
            details.setBgtLastPeriod(actionForm.getLastPeriod());

            try {
            	
            	ejbBD.updateGlBgtEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.budgetNameAlreadyExist"));

            } catch (GlobalRecordAlreadyAssignedException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.updateBudgetAlreadyAssigned"));
               
            } catch (GlBGTStatusAlreadyDefinedException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.budgetStatusAlreadyDefined"));
               
            } catch (GlBGTFirstPeriodGreaterThanLastPeriodException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.firstPeriodGreaterThanLastPeriod"));
               
            } catch (GlBGTFirstLastPeriodDifferentAcYearException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetDefinition.error.firstLastPeriodDifferentAcYear"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetDefinitionAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl Bgt Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl Bgt Edit Action --
*******************************************************/

         } else if (request.getParameter("glBGTList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
            actionForm.showGlBGTRow(actionForm.getRowSelected());
            
            return mapping.findForward("glBudgetDefinition");  
      
/*******************************************************
   -- Gl Bgt Delete Action --
*******************************************************/

         } else if (request.getParameter("glBGTList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlBudgetDefinitionList glBGTList =
	            actionForm.getGlBGTByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbBD.deleteGlBgtEntry(glBGTList.getBudgetCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("budgetDefinition.error.budgetAlreadyDeleted"));
               
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("budgetDefinition.error.deleteBudgetAlreadyAssigned"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlBudgetDefinitionAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            

/*******************************************************
   -- Gl Bgt Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glBudgetDefinition");

            }
       	                          	                           	
            ArrayList list = null;
            Iterator i = null;

	        try {
	        	
	        	actionForm.clearGlBGTList();
	        	
	        	actionForm.clearFirstPeriodList();
            	
            	list = ejbBD.getGlAcvAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setFirstPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModAccountingCalendarValueDetails details = (GlModAccountingCalendarValueDetails)i.next();

            			actionForm.setFirstPeriodList(details.getAcvPeriodPrefix() + "-" + details.getAcvYear());

            	    }
            		
               }
            	
            	actionForm.clearLastPeriodList();
            	
            	list = ejbBD.getGlAcvAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setLastPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModAccountingCalendarValueDetails details = (GlModAccountingCalendarValueDetails)i.next();

            			actionForm.setLastPeriodList(details.getAcvPeriodPrefix() + "-" + details.getAcvYear());
            			
            		}
            		
               } 
	        		    	
	           list = ejbBD.getGlBgtAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	           	  GlBudgetDetails details = (GlBudgetDetails)i.next();
	            	
	              GlBudgetDefinitionList glBGTList = new GlBudgetDefinitionList(actionForm,
	            	    details.getBgtCode(),
	            	    details.getBgtName(),
	            	    details.getBgtDescription(),
	            	    details.getBgtStatus(),
                        details.getBgtFirstPeriod(),
                        details.getBgtLastPeriod());
	            	 	            	    
	              actionForm.saveGlBGTList(glBGTList);
	            	
	           }
	           
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetDefinitionAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glBudgetDefinition"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlBudgetDefinitionAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}