package com.struts.gl.budgetdefinition;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlBudgetDefinitionForm extends ActionForm implements Serializable {
   
   private String budgetName = null;
   private String budgetDescription = null;
   private String budgetStatus = null;
   private ArrayList budgetStatusList = new ArrayList();
   private String firstPeriod = null;
   private ArrayList firstPeriodList = new ArrayList();
   private String lastPeriod = null;
   private ArrayList lastPeriodList = new ArrayList();
   private String tableType = null;

   private String pageState = new String();
   private ArrayList glBGTList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlBudgetDefinitionList getGlBGTByIndex(int index) {

      return((GlBudgetDefinitionList)glBGTList.get(index));

   }

   public Object[] getGlBGTList() {

      return glBGTList.toArray();

   }

   public int getGlBGTListSize() {

      return glBGTList.size();

   }

   public void saveGlBGTList(Object newGlBGTList) {

      glBGTList.add(newGlBGTList);

   }

   public void clearGlBGTList() {
      glBGTList.clear();
   }

   public void setRowSelected(Object selectedGlBGTList, boolean isEdit) {

      this.rowSelected = glBGTList.indexOf(selectedGlBGTList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlBGTRow(int rowSelected) {
   	
       this.budgetName = ((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getBudgetName();
       this.budgetDescription = ((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getBudgetDescription();
       this.budgetStatus = ((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getBudgetStatus();
       
       if (!firstPeriodList.contains(((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getFirstPeriod())) {
       	
       	  firstPeriodList.add(((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getFirstPeriod());
       	
       }
       this.firstPeriod = ((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getFirstPeriod();
       
       if (!lastPeriodList.contains(((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getLastPeriod())) {
       	
       	lastPeriodList.add(((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getLastPeriod());
       	
       }
       this.lastPeriod = ((GlBudgetDefinitionList)glBGTList.get(rowSelected)).getLastPeriod();

       
   }

   public void updateGlBGTRow(int rowSelected, Object newGlBGTList) {

      glBGTList.set(rowSelected, newGlBGTList);

   }

   public void deleteGlBGTList(int rowSelected) {

      glBGTList.remove(rowSelected);

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getBudgetName() {

      return budgetName;

   }

   public void setBudgetName(String budgetName) {

      this.budgetName = budgetName;

   }
   
   public String getBudgetDescription() {

      return budgetDescription;

   }

   public void setBudgetDescription(String budgetDescription) {

      this.budgetDescription = budgetDescription;

   }   
   
   public String getBudgetStatus() {

      return budgetStatus;

   }

   public void setBudgetStatus(String budgetStatus) {

      this.budgetStatus = budgetStatus;

   }
   
   public ArrayList getBudgetStatusList() {
   	
   	  return budgetStatusList;
   	  
   }
   
   public String getFirstPeriod() {
   	
   	  return firstPeriod;
   	  
   }
   
   public void setFirstPeriod(String firstPeriod) {
   	
   	  this.firstPeriod = firstPeriod;
   	  
   }
   
   public ArrayList getFirstPeriodList() {
   	
   	  return firstPeriodList;
   	  
   }
   
   public void setFirstPeriodList(String firstPeriod) {
   	
   	  firstPeriodList.add(firstPeriod);
   	  
   }
   
   public void clearFirstPeriodList() {
   	
   	  firstPeriodList.clear();
   	  firstPeriodList.add(Constants.GLOBAL_BLANK);
   	  
   }
   
   public String getLastPeriod() {
   	
   	  return lastPeriod;
   	  
   }
   
   public void setLastPeriod(String lastPeriod) {
   	
   	  this.lastPeriod = lastPeriod;
   	
   }    	

   public ArrayList getLastPeriodList() {
   	
   	  return lastPeriodList;
   	   
   }
   
   public void setLastPeriodList(String lastPeriod) {
   	
   	  lastPeriodList.add(lastPeriod);
   	   
   }  
   
   public void clearLastPeriodList() {
   	
   	  lastPeriodList.clear();
   	  lastPeriodList.add(Constants.GLOBAL_BLANK);
   	
   } 

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

		budgetName = null;
		budgetDescription = null;
		budgetStatusList.clear();
		budgetStatusList.add("OPEN");
		budgetStatusList.add("CURRENT");
		budgetStatus = Constants.GLOBAL_BLANK;
		firstPeriod = Constants.GLOBAL_BLANK;
		lastPeriod = Constants.GLOBAL_BLANK;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(budgetName)) {

            errors.add("budgetName",
               new ActionMessage("budgetDefinition.error.budgetNameRequired"));

         }
         
         if (Common.validateRequired(budgetStatus)) {

            errors.add("budgetStatus",
               new ActionMessage("budgetDefinition.error.budgetStatusRequired"));

         }
         
         if (Common.validateRequired(firstPeriod) || firstPeriod.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

            errors.add("firstPeriod",
               new ActionMessage("budgetDefinition.error.firstPeriodRequired"));

         }
         
         if (Common.validateRequired(lastPeriod) || lastPeriod.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

            errors.add("lastPeriod",
               new ActionMessage("budgetDefinition.error.lastPeriodRequired"));

         }

      }
         
      return errors;

   }
}