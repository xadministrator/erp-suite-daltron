package com.struts.gl.budgetdefinition;

import java.io.Serializable;

public class GlBudgetDefinitionList implements Serializable {

   private Integer budgetCode = null;
   private String budgetName = null;
   private String budgetDescription = null;
   private String budgetStatus = null;
   private String firstPeriod = null;
   private String lastPeriod = null;

   private String editButton = null;
   private String deleteButton = null;
    
   private GlBudgetDefinitionForm parentBean;
    
   public GlBudgetDefinitionList(GlBudgetDefinitionForm parentBean,
		Integer budgetCode,
		String budgetName,
		String budgetDescription,
		String budgetStatus,
		String firstPeriod,
		String lastPeriod) {
		
		this.parentBean = parentBean;
		this.budgetCode = budgetCode;
        this.budgetName = budgetName;
        this.budgetDescription = budgetDescription;
        this.budgetStatus = budgetStatus;
        this.firstPeriod = firstPeriod;
        this.lastPeriod = lastPeriod;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }     
   
   public Integer getBudgetCode() {
   	
   	  return budgetCode;
   	  
   }
   
   public String getBudgetName() {
   	
   	  return budgetName;
   	
   }
   
   public String getBudgetDescription() {
   	
   	  return budgetDescription;
   
   }
   
   public String getBudgetStatus() {
   	
   	  return budgetStatus;
   	  
   }
   
   public String getFirstPeriod() {
   	
   	  return firstPeriod;
   	
   }
   
   public String getLastPeriod() {
   	
   	  return lastPeriod;
   	
   }
  
}