package com.struts.gl.budgetentry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlBGAPasswordInvalidException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlBudgetEntryController;
import com.ejb.txn.GlBudgetEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModBudgetAmountCoaDetails;
import com.util.GlModBudgetAmountDetails;
import com.util.GlModBudgetAmountPeriodDetails;

public final class GlBudgetEntryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlBudgetEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlBudgetEntryForm actionForm = (GlBudgetEntryForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_BUDGET_ENTRY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glBudgetEntry");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlBudgetEntryController EJB
*******************************************************/

         GlBudgetEntryControllerHome homeBE = null;
         GlBudgetEntryController ejbBE = null;

         try {

            homeBE = (GlBudgetEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlBudgetEntryControllerEJB", GlBudgetEntryControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlBudgetEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBE = homeBE.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlBudgetEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call GlBudgetEntryController EJB
     getGlFcPrecisionUnit
  *******************************************************/

               short precisionUnit = 0;
               
               try {
               	
                  precisionUnit = ejbBE.getGlFcPrecisionUnit(user.getCmpCode());

               } catch(EJBException ex) {
               	
                  if (log.isInfoEnabled()) {
                  	
                     log.info("EJBException caught in GlBudgetEntryAction.execute(): " + ex.getMessage() +
                        " session: " + session.getId());
                  }
                  
                  return(mapping.findForward("cmnErrorPage"));
                  
               }
                      
         
/*******************************************************
   -- Gl Bga Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlModBudgetAmountDetails mdetails = new GlModBudgetAmountDetails();
            mdetails.setBgaCode(actionForm.getBudgetAmountCode());
            
            if (actionForm.getBudgetAmountCode() == null) {
           	
	        	mdetails.setBgaCreatedBy(user.getUserName());
	            mdetails.setBgaDateCreated(new java.util.Date());
		                      
            }
           
            mdetails.setBgaLastModifiedBy(user.getUserName());
            mdetails.setBgaDateLastModified(new java.util.Date());
            
            GlModBudgetAmountCoaDetails  coaDetails = new GlModBudgetAmountCoaDetails();
            coaDetails.setBcCoaAccountNumber(actionForm.getAccountNumber());
            coaDetails.setBcDescription(actionForm.getDescription());
            coaDetails.setBcRuleType(actionForm.getRuleType());  
            coaDetails.setBcRuleAmount(Common.convertStringMoneyToDouble(actionForm.getRuleAmount(), precisionUnit));
            coaDetails.setBcRulePercentage1(Common.convertStringMoneyToDouble(actionForm.getRulePercentage1(), precisionUnit));
            coaDetails.setBcRulePercentage2(Common.convertStringMoneyToDouble(actionForm.getRulePercentage2(), precisionUnit));
            
            
            ArrayList glBgaBudgetAmountCoaList = new ArrayList();
            ArrayList glBgaBudgetAmountPeriodList = new ArrayList();
            
            for (int i = 0; i<actionForm.getGlBgaPeriodListSize(); i++) {
            	
            	GlBudgetEntryPeriodList prdList = actionForm.getGlBgaPeriodByIndex(i);
                
            	GlModBudgetAmountPeriodDetails prdDetails = new GlModBudgetAmountPeriodDetails();
            	prdDetails.setBapAmount(Common.convertStringMoneyToDouble(prdList.getPeriodAmount(), precisionUnit));
            	prdDetails.setBapAcvPeriodPrefix(prdList.getPeriod().substring(0, prdList.getPeriod().indexOf('-')));
            	prdDetails.setBapAcvYear(Integer.parseInt(prdList.getPeriod().substring(prdList.getPeriod().indexOf('-') + 1)));

				glBgaBudgetAmountPeriodList.add(prdDetails);
				
            }
            
            coaDetails.setBcBudgetAmountPeriodList(glBgaBudgetAmountPeriodList);
            glBgaBudgetAmountCoaList.add(coaDetails);      
            
            mdetails.setBgaBudgetAmountCoaList(glBgaBudgetAmountCoaList);
      	 	mdetails.setBgaPassword(actionForm.getPassword());
	         
      	 	/* ArrayList glBgaBudgetAmountCoaList = new ArrayList();

			for (int i = 0; i<actionForm.getGlBgaCOAListSize(); i++) {
           	
           	    GlBudgetEntryList glBgaCOAList = actionForm.getGlBgaCOAByIndex(i);

                GlModBudgetAmountCoaDetails  coaDetails = new GlModBudgetAmountCoaDetails();
                coaDetails.setBcCoaAccountNumber(glBgaCOAList.getAccountNumber());
                coaDetails.setBcRuleType(glBgaCOAList.getRuleType());  
                coaDetails.setBcRuleAmount(Common.convertStringMoneyToDouble(glBgaCOAList.getRuleAmount(), precisionUnit));

                ArrayList glBgaBudgetAmountPeriodList = new ArrayList();
                
                Collection periods = glBgaCOAList.getPeriodList();
                
                Iterator j = periods.iterator();
                
                while(j.hasNext()) {
                
                    GlBudgetEntryPeriodList prdList = (GlBudgetEntryPeriodList)j.next();
                
                	GlModBudgetAmountPeriodDetails prdDetails = new GlModBudgetAmountPeriodDetails();
                	prdDetails.setBapAmount(Common.convertStringMoneyToDouble(prdList.getPeriodAmount(), precisionUnit));
                	prdDetails.setBapAcvPeriodPrefix(prdList.getPeriod().substring(0, prdList.getPeriod().indexOf('-')));
                	prdDetails.setBapAcvYear(Integer.parseInt(prdList.getPeriod().substring(prdList.getPeriod().indexOf('-') + 1)));

					glBgaBudgetAmountPeriodList.add(prdDetails);

                }
                            
                coaDetails.setBcBudgetAmountPeriodList(glBgaBudgetAmountPeriodList);
                
                glBgaBudgetAmountCoaList.add(coaDetails);      	   
  
      	 	} */
      	 	
      	 	
            try {
            	
            	Integer budgetAmountCode = ejbBE.saveGlBgaEntry(mdetails, actionForm.getBudgetOrganization(), actionForm.getBudgetName(), actionForm.getAccountNumber(), user.getCmpCode());
                
            	actionForm.setBudgetAmountCode(budgetAmountCode);
            	
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetEntry.error.budgetAlreadyExist"));
               
            } catch (GlobalAccountNumberInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetEntry.error.accountNumberInvalid"));
               
            } catch (GlBGAPasswordInvalidException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetEntry.error.passwordInvalid"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl Bga Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
                        
/*******************************************************
    -- Gl Bga Budget/Budget Organization Enter Action --
 *******************************************************/
            
         } else if ((!Common.validateRequired(request.getParameter("isBudgetEntered")) ||
         		    !Common.validateRequired(request.getParameter("isBudgetOrganizationEntered"))) &&
         		    !Common.validateRequired(actionForm.getBudgetName()) &&
         		    !Common.validateRequired(actionForm.getBudgetOrganization())) {
                	
        	 try {

        		 actionForm.clearGlBgaPeriodList();

        		 ArrayList list = ejbBE.getGlBgaTemplateByBoNameAndBgtName(actionForm.getBudgetOrganization(), actionForm.getBudgetName(), user.getCmpCode());                

        		 int ctr = 0;

        		 Iterator i = list.iterator();

        		 while (i.hasNext()) {

        			 GlModBudgetAmountPeriodDetails bapDetails = (GlModBudgetAmountPeriodDetails)i.next();

        			 GlBudgetEntryPeriodList glBgaPeriodList = new GlBudgetEntryPeriodList(
        					 actionForm,				
        					 new Integer(ctr++),
        					 bapDetails.getBapCode(), 
        					 bapDetails.getBapAcvPeriodPrefix() + "-" + bapDetails.getBapAcvYear(),
        					 Common.convertDoubleToStringMoney(bapDetails.getBapAmount(), precisionUnit));

        			 actionForm.saveGlBgaPeriodList(glBgaPeriodList);

        		 }     

        	 } catch (GlobalNoRecordFoundException ex) {
                    
	                   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("budgetEntry.error.noRecordFound"));
                 	                 	
                 } catch (EJBException ex) {

                    if (log.isInfoEnabled()) {

                       log.info("EJBException caught in GlBudgetEntryAction.execute(): " + ex.getMessage() +
                       " session: " + session.getId());
                       return mapping.findForward("cmnErrorPage"); 
                       
                    }

                 }  	
                 
                             
                 return(mapping.findForward("glBudgetEntry"));   
                 
/*******************************************************
    -- Gl Bga Budget Organization Enter Action --
 *******************************************************/
            
         } else if ((!Common.validateRequired(request.getParameter("isBudgetEntered")))) {
         
         	return(mapping.findForward("glBudgetEntry"));  
         	         		    
/*******************************************************
    -- Gl Bga Budget Enter Action --
 *******************************************************/
            
         } else if ((!Common.validateRequired(request.getParameter("isBudgetOrganizationEntered")))) {
         
         	return(mapping.findForward("glBudgetEntry"));  
                 
/*******************************************************
   -- Gl Bga Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glBudgetEntry");

            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
       	                          	                           	
            ArrayList list = null;
            Iterator i = null;

	        try {

	        	actionForm.clearBudgetOrganizationList();
            	
            	list = ejbBE.getGlBoAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBudgetOrganizationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
   
            			actionForm.setBudgetOrganizationList((String)i.next());

            	    }
            		
               }
            	
            	actionForm.clearBudgetNameList();
            	
            	list = ejbBE.getGlBgtAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBudgetNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
   
            			actionForm.setBudgetNameList((String)i.next());

            	    }
            		
               }

                actionForm.clearGlBgaPeriodList();

            	if (request.getParameter("forward") != null) {

            		GlModBudgetAmountDetails bgaDetails = ejbBE.getGlBgaByBgaCode(
	        		    new Integer(request.getParameter("budgetCode")), user.getCmpCode());
	
					actionForm.setBudgetAmountCode(bgaDetails.getBgaCode());
					actionForm.setCreatedBy(bgaDetails.getBgaCreatedBy());
					actionForm.setDateCreated(Common.convertSQLDateToString(bgaDetails.getBgaDateCreated()));
					actionForm.setLastModifiedBy(bgaDetails.getBgaLastModifiedBy());
					actionForm.setDateLastModified(Common.convertSQLDateToString(bgaDetails.getBgaDateLastModified()));
					actionForm.setBudgetOrganization(bgaDetails.getBgaBoName());
					actionForm.setBudgetName(bgaDetails.getBgaBgtName());

					list = bgaDetails.getBgaBudgetAmountCoaList();
					
					int ctr = 0;
					
					i = list.iterator();
					
					while(i.hasNext()) {

						GlModBudgetAmountCoaDetails coaDetails = (GlModBudgetAmountCoaDetails)i.next();

						actionForm.setAccountNumber(coaDetails.getBcCoaAccountNumber());
						actionForm.setAccountDescription(coaDetails.getBcCoaAccountDescription());
						actionForm.setDescription(coaDetails.getBcDescription());
						actionForm.setRuleType(coaDetails.getBcRuleType());
						actionForm.setRuleAmount(Common.convertDoubleToStringMoney(coaDetails.getBcRuleAmount(), precisionUnit));
						actionForm.setRulePercentage1(Common.convertDoubleToStringMoney(coaDetails.getBcRulePercentage1(), precisionUnit));
						actionForm.setRulePercentage2(Common.convertDoubleToStringMoney(coaDetails.getBcRulePercentage2(), precisionUnit));
						
						
						Collection glBcBudgetAmountPeriods = coaDetails.getBcBudgetAmountPeriodList();

						Iterator j = glBcBudgetAmountPeriods.iterator();

						while (j.hasNext()) {

							GlModBudgetAmountPeriodDetails bapDetails = (GlModBudgetAmountPeriodDetails)j.next();

							GlBudgetEntryPeriodList glBcBudgetAmountPeriodList = new GlBudgetEntryPeriodList(
									actionForm,				
									new Integer(ctr++),
									bapDetails.getBapCode(), 
									bapDetails.getBapAcvPeriodPrefix() + "-" + bapDetails.getBapAcvYear(),
									Common.convertDoubleToStringMoney(bapDetails.getBapAmount(), precisionUnit));

							actionForm.saveGlBgaPeriodList(glBcBudgetAmountPeriodList);

						}             

					}
					
					this.setFormProperties(actionForm);

				    return(mapping.findForward("glBudgetEntry"));  
			    
            	}          		                	            	
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }   

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null &&
                    actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);

            actionForm.setBudgetAmountCode(null);
            actionForm.setDescription(null);
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("glBudgetEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlBudgetEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
   private void setFormProperties(GlBudgetEntryForm actionForm) {
	
	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
		actionForm.setEnableFields(true);
		actionForm.setShowSaveButton(true);

	} else {

		actionForm.setEnableFields(false);
		actionForm.setShowSaveButton(false);

	}
	
   }
}