package com.struts.gl.budgetentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlBudgetEntryForm extends ActionForm implements Serializable {
   
   private Integer budgetAmountCode = null;
   private String budgetOrganization = null;
   private ArrayList budgetOrganizationList = new ArrayList();
   private String budgetName = null;
   private ArrayList budgetNameList = new ArrayList();
   private String accountNumber = null;
   private String accountDescription = null;
   private String description = null;
   private String password = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;  
   private String ruleType = null;
   private String ruleAmount = null;
   private String rulePercentage1 = null;
   private String rulePercentage2 = null;

   private String pageState = new String();
   private ArrayList glBgaPeriodList  = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showSaveButton = false;
   
   private String isBudgetOrganizationEntered = null;
   private String isBudgetEntered = null;
   
   
   public GlBudgetEntryPeriodList getGlBgaPeriodByIndex(int index) {
   	
      return((GlBudgetEntryPeriodList)glBgaPeriodList.get(index));
   
   }
   
   public Object[] getGlBgaPeriodList() {

   	  return glBgaPeriodList.toArray();
 
   }
   
   public int getGlBgaPeriodListSize() {

      return glBgaPeriodList.size();

   }
 
   public void saveGlBgaPeriodList(Object newGlBgaPeriodList) {

   	  glBgaPeriodList.add(newGlBgaPeriodList);

   }
   
   public void clearGlBgaPeriodList() {
      
   	  glBgaPeriodList.clear();
   
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getBudgetAmountCode() {
   
   	  return budgetAmountCode;
   	
   }
   
   public void setBudgetAmountCode(Integer budgetAmountCode) {
   	
   	  this.budgetAmountCode = budgetAmountCode;
   	
   }

   public String getBudgetOrganization() {

      return budgetOrganization;

   }

   public void setBudgetOrganization(String budgetOrganization) {

      this.budgetOrganization = budgetOrganization;

   }
   
   public ArrayList getBudgetOrganizationList() {
   	
   	  return budgetOrganizationList;
   	  
   }
   
   public void setBudgetOrganizationList(String budgetOrganization) {
   	
   	  budgetOrganizationList.add(budgetOrganization);
   	  
   }
   
   public void clearBudgetOrganizationList() {
   	
   	  budgetOrganizationList.clear();
      budgetOrganizationList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getBudgetName() {

      return budgetName;

   }

   public void setBudgetName(String budgetName) {

      this.budgetName = budgetName;

   } 
   
   public ArrayList getBudgetNameList() {
   	
   	  return budgetNameList;
   	
   }
   
   public void setBudgetNameList(String budgetName) {
   	
   	  budgetNameList.add(budgetName);
   	  
   }
   
   public void clearBudgetNameList() {
   	
   	  budgetNameList.clear();
      budgetNameList.add(Constants.GLOBAL_BLANK);
   	
   }

   public String getAccountNumber() {

	   return accountNumber;

   }

   public void setAccountNumber(String accountNumber) {

	   this.accountNumber = accountNumber;

   }
   
   public String getAccountDescription() {
   	
   	   return accountDescription;
   	
   }
   
   public void setAccountDescription(String accountDescription) {
   	
   	  this.accountDescription = accountDescription;
   	
   }
   
   public String getDescription() {
	   	
   	   return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public String getPassword() {
   	
   	   return password;
   	
   }
   
   public void setPassword(String password) {
   	
   	  this.password = password;
   	
   }
   
   public String getCreatedBy() {
   	
   	   return createdBy;
   	
   }
   
   public void setCreatedBy(String createdBy) {
   	
   	  this.createdBy = createdBy;
   	
   }
   
   public String getDateCreated() {
   	
   	   return dateCreated;
   	
   }
   
   public void setDateCreated(String dateCreated) {
   	
   	   this.dateCreated = dateCreated;
   	
   }   
   
   public String getLastModifiedBy() {
   	
   	  return lastModifiedBy;
   	
   }
   
   public void setLastModifiedBy(String lastModifiedBy) {
   	
   	  this.lastModifiedBy = lastModifiedBy;
   	
   }
   
   public String getDateLastModified() {
   	
   	  return dateLastModified;
   	
   }
   
   public void setDateLastModified(String dateLastModified) {
   	
   	  this.dateLastModified = dateLastModified;
   	
   }
   
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
   
   public String getIsBudgetOrganizationEntered() {
   	
   	   return isBudgetOrganizationEntered;
   	
   }
   
   public void setIsBudgetOrganizationEntered(String isBudgetOrganizationEntered) {
   	
   	   this.isBudgetOrganizationEntered = isBudgetOrganizationEntered;
   	
   }
   
   public String getIsBudgetEntered() {
   	
   	   return isBudgetEntered;
   	
   }
   
   public void setIsBudgetEntered(String isBudgetEntered) {
   	
   	   this.isBudgetEntered = isBudgetEntered;
   	
   }

   public String getRuleType() {

	   return ruleType;

   }

   public void setRuleType(String ruleType) {

	   this.ruleType = ruleType;

   }

   public String getRuleAmount() {

	   return ruleAmount;

   }

   public void setRuleAmount(String ruleAmount) {

	   this.ruleAmount = ruleAmount;

   }
   
   public String getRulePercentage1() {

	   return rulePercentage1;

   }

   public void setRulePercentage1(String rulePercentage1) {

	   this.rulePercentage1 = rulePercentage1;

   }
   
   public String getRulePercentage2() {

	   return rulePercentage2;

   }

   public void setRulePercentage2(String rulePercentage2) {

	   this.rulePercentage2 = rulePercentage2;

   }
   
   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

		budgetOrganization = Constants.GLOBAL_BLANK;
		budgetName = Constants.GLOBAL_BLANK;
		accountNumber = null;
		accountDescription = null;
		password = null;
	    createdBy = null;
	    dateCreated = null;
	    lastModifiedBy = null;
	    dateLastModified = null;
	    isBudgetOrganizationEntered = null;
	    isBudgetEntered = null;
	    ruleType = null;
	    ruleAmount = null;
	    rulePercentage1 = null;
	    rulePercentage2 = null;
	    

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null) {

         if (Common.validateRequired(budgetOrganization) || budgetOrganization.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

            errors.add("budgetOrganization",
               new ActionMessage("budgetEntry.error.budgetOrganizationRequired"));

         }
         
         if (Common.validateRequired(budgetName) || budgetName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

            errors.add("budgetName",
               new ActionMessage("budgetEntry.error.budgetNameRequired"));

         }
         
         if (Common.validateRequired(accountNumber)) {

             errors.add("accountNumber",
                new ActionMessage("budgetEntry.error.accountNumberRequired"));

         }
         
         Iterator i = glBgaPeriodList.iterator();      	 

         while (i.hasNext()) {
        	 
        	 GlBudgetEntryPeriodList prdList = (GlBudgetEntryPeriodList)i.next();  

        	 if(!Common.validateMoneyFormat(prdList.getPeriodAmount())){
        		 errors.add("periodAmount",
        				 new ActionMessage("budgetEntry.error.periodAmountInvalid", prdList.getPeriod()));
        	 }
        	 
         }

      } else if(request.getParameter("isBudgetOrganizationEntered") != null || request.getParameter("isBudgetEntered") != null) {
      	
      	if (budgetOrganization.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
      		
      		errors.add("budgetOrganization",
      				new ActionMessage("budgetEntry.error.budgetOrganizationRequired"));
      		
      	}
      	
      	if (budgetName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
      		
      		errors.add("budgetName",
      				new ActionMessage("budgetEntry.error.budgetNameRequired"));
      		
      	}
      	
      }
         
      return errors;

   }
}