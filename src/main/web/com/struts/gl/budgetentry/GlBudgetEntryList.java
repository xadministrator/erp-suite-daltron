package com.struts.gl.budgetentry;

import java.io.Serializable;
import java.util.ArrayList;

public class GlBudgetEntryList implements Serializable {

   private Integer index = null;
   private String accountNumber = null;
   private String accountDescription = null;
   private String ruleType = null;
   private String ruleAmount = null;

   private GlBudgetEntryForm parentBean;
   private ArrayList periodList;
    
   public GlBudgetEntryList(GlBudgetEntryForm parentBean,
   		Integer index, 
   		String accountNumber,
		String accountDescription,
		String ruleType,
		String ruleAmount) {
		
		this.parentBean = parentBean;
		this.index = index;
		this.accountNumber = accountNumber;
		this.accountDescription = accountDescription;
		this.ruleType = ruleType;
		this.ruleAmount = ruleAmount;

   }

   public ArrayList getPeriodList() {
   	
   	  return periodList;   
   	
   }
   
   public void setPeriodList(ArrayList periodList) {
   	
   	   this.periodList = periodList;
   	
   }
   
   public Integer getIndex() {
   	
   	  return index;
   	
   }
   
   public void setIndex(Integer index) {
   	
   	  this.index = index;
   	
   }
   
   public String getAccountNumber() {
   	
   	  return accountNumber;
   	
   }
   
   public void setAccountNumber(String accountNumber) {
   	
   	  this.accountNumber = accountNumber;
   	
   }
   
   public String getRuleType() {
   	
   	  return ruleType;
   	
   }
   
   public void setRuleType(String ruleType) {
   	
   	  this.ruleType = ruleType;
   	
   }
   
   public String getRuleAmount() {
   	
   	  return ruleAmount;
   	
   }
   
   public void setRuleAmount(String ruleAmount) {
   	
   	  this.ruleAmount = ruleAmount;
   	
   }
   
   public String getAccountDescription() {
   	
   	  return accountDescription;
   }
   
   public void setAccountDescription(String accountDescription) {
   	
   	  this.accountDescription = accountDescription;
   	
   }

}