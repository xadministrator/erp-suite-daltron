package com.struts.gl.budgetentry;

import java.io.Serializable;

public class GlBudgetEntryPeriodList implements Serializable {
	
   private Integer index = null;
   private Integer periodCode = null;	
   private String period = null;
   private String periodAmount = null;

   private GlBudgetEntryForm parentBean;
    
   public GlBudgetEntryPeriodList(GlBudgetEntryForm parentBean,
		Integer index,
   		Integer periodCode,
   		String period,
		String periodAmount) {
		
		this.parentBean = parentBean;
		this.index = index;
		this.periodCode = periodCode;
		this.period = period;
		this.periodAmount = periodAmount;

   }

   public Integer getIndex() {

	   return index;

   }

   public void setIndex(Integer index) {

	   this.index = index;

   }

   public Integer getPeriodCode() {
   	
   	  return periodCode;
   	
   }
   
   public void setPeriodCode(Integer periodCode) {
   	
   	  this.periodCode = periodCode;
   	
   }
   
   public String getPeriod() {
   	
   	  return period;
   	
   }
   
   public void setPeriod(String period) {
   	
   	  this.period = period;
   	
   }
   
   public String getPeriodAmount() {
   	
   	  return periodAmount;
   	
   }
   
   public void setPeriodAmount(String periodAmount) {
   	
   	  this.periodAmount = periodAmount;
   	
   }

}