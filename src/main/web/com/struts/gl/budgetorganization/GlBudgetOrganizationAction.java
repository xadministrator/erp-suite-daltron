package com.struts.gl.budgetorganization;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlBudgetOrganizationController;
import com.ejb.txn.GlBudgetOrganizationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlBudgetOrganizationDetails;

public final class GlBudgetOrganizationAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlBudgetOrganizationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlBudgetOrganizationForm actionForm = (GlBudgetOrganizationForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_BUDGET_ORGANIZATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glBudgetOrganization");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlBudgetOrganizationController EJB
*******************************************************/

         GlBudgetOrganizationControllerHome homeBD = null;
         GlBudgetOrganizationController ejbBD = null;

         try {

            homeBD = (GlBudgetOrganizationControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlBudgetOrganizationControllerEJB", GlBudgetOrganizationControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlBudgetOrganizationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbBD = homeBD.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlBudgetOrganizationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl Bo Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glBudgetOrganization"));
	     
/*******************************************************
   -- Gl Bo Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glBudgetOrganization"));                  
         
/*******************************************************
   -- Gl Bo Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlBudgetOrganizationDetails details = new GlBudgetOrganizationDetails();
            details.setBoName(actionForm.getBudgetOrgName());
            details.setBoDescription(actionForm.getBudgetOrgDescription());
            details.setBoSegmentOrder(actionForm.getSegmentOrder());
            details.setBoPassword(actionForm.getPassword());

            try {
            	
            	ejbBD.addGlBoEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetOrganization.error.budgetOrganizationNameAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetOrganizationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl Bo Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl Bo Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlBudgetOrganizationList glBOList =
	            actionForm.getGlBOByIndex(actionForm.getRowSelected());
                        
            GlBudgetOrganizationDetails details = new GlBudgetOrganizationDetails();
            details.setBoCode(glBOList.getBudgetOrgCode());
            details.setBoName(actionForm.getBudgetOrgName());
            details.setBoDescription(actionForm.getBudgetOrgDescription());
            details.setBoSegmentOrder(actionForm.getSegmentOrder());
            details.setBoPassword(actionForm.getPassword());

            try {
            	
            	ejbBD.updateGlBoEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("budgetOrganization.error.budgetOrganizationNameAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetOrganizationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl Bo Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl Bo Edit Action --
*******************************************************/

         } else if (request.getParameter("glBOList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
            actionForm.showGlBORow(actionForm.getRowSelected());
            
            return mapping.findForward("glBudgetOrganization");  
            
            
/*******************************************************
 -- Gl Bo Account Action --
 *******************************************************/
            
         } else if (request.getParameter("glBOList[" +
         		actionForm.getRowSelected() + "].accountButton") != null){
         	
         	GlBudgetOrganizationList glBOList =
         		actionForm.getGlBOByIndex(actionForm.getRowSelected());
         	
         	String path = "/glBudgetAccountAssignment.do?forward=1" +
			"&organizationCode=" + glBOList.getBudgetOrgCode() +
			"&organizationName=" +  glBOList.getBudgetOrgName() + 
			"&segmentOrder=" +  glBOList.getSegmentOrder();
         	
         	return(new ActionForward(path));             
      
/*******************************************************
   -- Gl Bo Delete Action --
*******************************************************/

         } else if (request.getParameter("glBOList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlBudgetOrganizationList glBOList =
	            actionForm.getGlBOByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbBD.deleteGlBoEntry(glBOList.getBudgetOrgCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("budgetOrganization.error.budgetOrganizationAlreadyDeleted"));
               
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("budgetOrganization.error.deleteBudgetOrganizationAlreadyAssigned"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlBudgetOrganizationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            

/*******************************************************
   -- Gl Bo Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glBudgetOrganization");

            }
       	                          	                           	
            ArrayList list = null;
            Iterator i = null;

	        try {
	        	
	        	actionForm.clearGlBOList();
	        	
	        	actionForm.clearSegmentOrderList();
            	
            	list = ejbBD.getGenVsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setSegmentOrderList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
   
            			actionForm.setSegmentOrderList((String)i.next());

            	    }
            		
               }
            		        		    	
	           list = ejbBD.getGlBoAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
        	            			            	
	           	  GlBudgetOrganizationDetails details = (GlBudgetOrganizationDetails)i.next();
	            	
	              GlBudgetOrganizationList glBOList = new GlBudgetOrganizationList(actionForm,
	            	    details.getBoCode(),
	            	    details.getBoName(),
	            	    details.getBoDescription(),
	            	    details.getBoSegmentOrder(),
						details.getBoPassword());
	            	 	            	    
	              actionForm.saveGlBOList(glBOList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBudgetOrganizationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glBudgetOrganization"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlBudgetOrganizationAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}