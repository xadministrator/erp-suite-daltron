package com.struts.gl.budgetorganization;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlBudgetOrganizationForm extends ActionForm implements Serializable {
   
   private String budgetOrgName = null;
   private String budgetOrgDescription = null;
   private String segmentOrder = null;
   private ArrayList segmentOrderList = new ArrayList();
   private String password = null;
   private String confirmPassword = null;
   private String tableType = null;

   private String pageState = new String();
   private ArrayList glBOList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlBudgetOrganizationList getGlBOByIndex(int index) {

      return((GlBudgetOrganizationList)glBOList.get(index));

   }

   public Object[] getGlBOList() {

      return glBOList.toArray();

   }

   public int getGlBOListSize() {

      return glBOList.size();

   }

   public void saveGlBOList(Object newGlBOList) {

      glBOList.add(newGlBOList);

   }

   public void clearGlBOList() {
      glBOList.clear();
   }

   public void setRowSelected(Object selectedGlBOList, boolean isEdit) {

      this.rowSelected = glBOList.indexOf(selectedGlBOList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlBORow(int rowSelected) {
   	
       this.budgetOrgName = ((GlBudgetOrganizationList)glBOList.get(rowSelected)).getBudgetOrgName();
       this.budgetOrgDescription = ((GlBudgetOrganizationList)glBOList.get(rowSelected)).getBudgetOrgDescription();
       this.segmentOrder = ((GlBudgetOrganizationList)glBOList.get(rowSelected)).getSegmentOrder();
       this.password = ((GlBudgetOrganizationList)glBOList.get(rowSelected)).getPassword();

   }

   public void updateGlBORow(int rowSelected, Object newGlBOList) {

      glBOList.set(rowSelected, newGlBOList);

   }

   public void deleteGlBOList(int rowSelected) {

      glBOList.remove(rowSelected);

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getBudgetOrgName() {

      return budgetOrgName;

   }

   public void setBudgetOrgName(String budgetOrgName) {

      this.budgetOrgName = budgetOrgName;

   }
   
   public String getBudgetOrgDescription() {

      return budgetOrgDescription;

   }

   public void setBudgetOrgDescription(String budgetOrgDescription) {

      this.budgetOrgDescription = budgetOrgDescription;

   }   
   
   public String getSegmentOrder() {

      return segmentOrder;

   }

   public void setSegmentOrder(String segmentOrder) {

      this.segmentOrder = segmentOrder;

   }
   
   public ArrayList getSegmentOrderList() {
   	
   	  return segmentOrderList;
   	  
   }
   
   public void setSegmentOrderList(String segmentOrder) {
   	
   	  segmentOrderList.add(segmentOrder);
   	  
   }
   
   public void clearSegmentOrderList() {
   	
   	  segmentOrderList.clear();
   	  segmentOrderList.add(Constants.GLOBAL_BLANK);
   	  
   }
   
   public String getPassword() {
   	
   	  return password;
   	  
   }
   
   public void setPassword(String password) {
   	
   	  this.password = password;
   	  
   }
   
   public void setConfirmPassword(String confirmPassword) {
   	
   	  this.confirmPassword = confirmPassword;
   	
   }
   
   public String getConfirmPassword() {
   	
   	  return confirmPassword;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

		budgetOrgName = null;
		budgetOrgDescription = null;
		segmentOrder = Constants.GLOBAL_BLANK;
		password = null;
		confirmPassword = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(budgetOrgName)) {

            errors.add("budgetOrgName",
               new ActionMessage("budgetOrganization.error.budgetOrgNameRequired"));

         }
         
         if (Common.validateRequired(segmentOrder) || segmentOrder.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

            errors.add("segmentOrder",
               new ActionMessage("budgetOrganization.error.segmentOrderRequired"));

         }
         
         if (!Common.validateRequired(password) && Common.validateRequired(confirmPassword)) {
         	
         	errors.add("confirmPassword",
         			new ActionMessage("budgetOrganization.error.confirmPasswordRequired"));
         	
         }

         if (!Common.validateRequired(password) && !Common.validateRequired(confirmPassword) &&
         		!password.equals(confirmPassword)) {
         	
         	errors.add("confirmPassword",
         			new ActionMessage("budgetOrganization.error.passwordNotConfirmed"));
         	
         }
         
         
         if (!Common.validateRequired(budgetOrgName) && budgetOrgName.equals("ALL")) {

            errors.add("budgetOrgName",
               new ActionMessage("budgetOrganization.error.budgetOrgNameAllInvalid"));

         }

      }
         
      return errors;

   }
}