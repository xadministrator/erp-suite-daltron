package com.struts.gl.budgetorganization;

import java.io.Serializable;

public class GlBudgetOrganizationList implements Serializable {

   private Integer budgetOrgCode = null;
   private String budgetOrgName = null;
   private String budgetOrgDescription = null;
   private String segmentOrder = null;
   private String password = null;

   private String editButton = null;
   private String deleteButton = null;
   private String accountButton = null;
    
   private GlBudgetOrganizationForm parentBean;
    
   public GlBudgetOrganizationList(GlBudgetOrganizationForm parentBean,
		Integer budgetOrgCode,
		String budgetOrgName,
		String budgetOrgDescription,
		String segmentOrder,
		String password) {
		
		this.parentBean = parentBean;
		this.budgetOrgCode = budgetOrgCode;
        this.budgetOrgName = budgetOrgName;
        this.budgetOrgDescription = budgetOrgDescription;
        this.segmentOrder = segmentOrder;
        this.password = password;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setAccountButton(String accountButton) {

    parentBean.setRowSelected(this, true);

 }
   
   public Integer getBudgetOrgCode() {
   	
   	  return budgetOrgCode;
   	  
   }
   
   public String getBudgetOrgName() {
   	
   	  return budgetOrgName;
   	
   }
   
   public String getBudgetOrgDescription() {
   	
   	  return budgetOrgDescription;
   
   }
   
   public String getSegmentOrder() {
   	
   	  return segmentOrder;
   	  
   }
   
   public String getPassword() {
   	
   	  return password;
   	  
   }

}