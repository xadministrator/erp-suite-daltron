package com.struts.gl.calculation;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlFrgCALRowAlreadyHasAccountAssignmentException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlFrgCalculationController;
import com.ejb.txn.GlFrgCalculationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFrgCalculationDetails;
import com.util.GlFrgColumnDetails;
import com.util.GlFrgRowDetails;

public final class GlFrgCalculationAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFrgCalculationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFrgCalculationForm actionForm = (GlFrgCalculationForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FRG_CALCULATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFrgCalculation");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFrgCalculationController EJB
*******************************************************/

         GlFrgCalculationControllerHome homeCAL = null;
         GlFrgCalculationController ejbCAL = null;

         try {

            homeCAL = (GlFrgCalculationControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFrgCalculationControllerEJB", GlFrgCalculationControllerHome.class);      
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFrgCalculationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCAL = homeCAL.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFrgCalculationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();	
         
/*******************************************************
   -- Gl CAL Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFrgCalculation"));
	     
/*******************************************************
   -- Gl CAL Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFrgCalculation"));                  


/*******************************************************
   -- GL CAL Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlFrgCalculationDetails details = new GlFrgCalculationDetails();
            details.setCalSequenceNumber(Common.convertStringToInt(actionForm.getSequenceNumber()));
            details.setCalOperator(actionForm.getOperator());
            details.setCalConstant(Common.convertStringMoneyToDouble(actionForm.getConstant(), Constants.DEFAULT_PRECISION));
            details.setCalRowColName(actionForm.getRowColName());
            details.setCalSequenceLow(Common.convertStringToInt(actionForm.getSequenceLow()));
            details.setCalSequenceHigh(Common.convertStringToInt(actionForm.getSequenceHigh()));
            details.setCalType(actionForm.getCalcType());
            
            try {
            	
            	ejbCAL.addGlFrgCalEntry(details, 
            	   actionForm.getRowCode(), actionForm.getColumnCode(), user.getCmpCode());
            	
            } catch (GlobalRecordAlreadyExistException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("calculation.error.sequenceNumberAlreadyExist")); 	

            } catch (GlFrgCALRowAlreadyHasAccountAssignmentException ex) {

               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("calculation.error.rowAlreadyHasAccountAssignment"));                                             

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgCalculationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }             
            
/*******************************************************
   -- GL CAL Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- GL CAL Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           
            GlFrgCalculationList glCALList =
	            actionForm.getGlCALByIndex(actionForm.getRowSelected());

            GlFrgCalculationDetails details = new GlFrgCalculationDetails();
            details.setCalCode(glCALList.getCalculationCode());
            details.setCalSequenceNumber(Common.convertStringToInt(actionForm.getSequenceNumber()));
            details.setCalOperator(actionForm.getOperator());
            details.setCalConstant(Common.convertStringMoneyToDouble(actionForm.getConstant(), Constants.DEFAULT_PRECISION));
            details.setCalRowColName(actionForm.getRowColName());
            details.setCalSequenceLow(Common.convertStringToInt(actionForm.getSequenceLow()));
            details.setCalSequenceHigh(Common.convertStringToInt(actionForm.getSequenceHigh()));
            
            try {
            	
            	ejbCAL.updateGlFrgCalEntry(details, 
            	   actionForm.getRowCode(), actionForm.getColumnCode(), user.getCmpCode());
            	
            } catch (GlobalRecordAlreadyExistException ex) {

                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("calculation.error.sequenceNumberAlreadyExist"));  	

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgCalculationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- GL CAL Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- GL CAL Edit Action --
*******************************************************/

         } else if (request.getParameter("glCALList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlCALRow(actionForm.getRowSelected());
            
            return mapping.findForward("glFrgCalculation");
            
/*******************************************************
   -- GL CAL Delete Action --
*******************************************************/

         } else if (request.getParameter("glCALList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
             
            GlFrgCalculationList glCALList =
	            actionForm.getGlCALByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbCAL.deleteGlFrgCalEntry(glCALList.getCalculationCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("calculation.error.calculationAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlFrgCalculationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            } 

/******************************************************
   -- GL CAL Row Action --
*******************************************************/

         } else if (request.getParameter("rowButton") != null) {
         	        	
         	 String path = "/glFrgRowEntry.do"; 
			     
             return(new ActionForward(path));

/******************************************************
   -- GL CAL Row Action --
*******************************************************/

         } else if (request.getParameter("columnButton") != null) {
         	        	
         	 String path = "/glFrgColumnEntry.do"; 
			     
             return(new ActionForward(path));

/*******************************************************
   -- GL CAL Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFrgCalculation");

            }       
            
            
			GlFrgRowDetails rowDetails = null;
			GlFrgColumnDetails colDetails = null;
			String nullString = null;
						
			if (request.getParameter("forwardrowentry") != null) {
				
				rowDetails = ejbCAL.getGlFrgRowByRowCode(new Integer(request.getParameter("rowCode")), user.getCmpCode());

				actionForm.setRowCode(new Integer(request.getParameter("rowCode")));
				actionForm.setColumnCode(null);
				actionForm.setColumnName(nullString);
				actionForm.setRowName(rowDetails.getRowName());
				actionForm.setLineNumber(String.valueOf(rowDetails.getRowLineNumber()));
				actionForm.setRowSetCode(new Integer(request.getParameter("rowSetCode")));
				actionForm.setIsRow(true);
    			actionForm.setIsColumn(false);
    			actionForm.setCalcType(request.getParameter("calcType"));
    			
			} else if (request.getParameter("forwardcolumnentry") != null) {
			
				colDetails = ejbCAL.getGlFrgColByColCode(new Integer(request.getParameter("columnCode")), user.getCmpCode());

				actionForm.setColumnCode(new Integer(request.getParameter("columnCode")));
				actionForm.setRowCode(null);
				actionForm.setRowName(nullString);
				actionForm.setColumnName(colDetails.getColName());
				actionForm.setColumnSequenceNumber(String.valueOf(colDetails.getColSequenceNumber()));
				actionForm.setColumnSetCode(new Integer(request.getParameter("columnSetCode")));
				actionForm.setIsRow(false);
    			actionForm.setIsColumn(true);
    			
			} 

            ArrayList list = null;
            Iterator i = null;    	   
             
	        	try {
	        		
	        		if(actionForm.getRowName() != null && actionForm.getColumnName() == null) { 
	        	        
	        			actionForm.clearRowColNameList();
			        		
			        	list = ejbCAL.getGlFrgRowByRsCodeAndLessThanRowLineNumber(actionForm.getRowSetCode(), 
			        			Common.convertStringToInt(actionForm.getLineNumber()), actionForm.getCalcType(), user.getCmpCode());
			        	
			        	if (list == null  || list.size() == 0) {
			        		
			        		actionForm.setRowColNameList(Constants.GLOBAL_NO_RECORD_FOUND);
			        		
			        	} else {
			        		            		
			        		i = list.iterator();
			        		
			        		while (i.hasNext()) {
			        			
			        		    actionForm.setRowColNameList((String)i.next());
			        			
			        		}
			        		
			        	}
		        	
		            } else {
		            	
		            	actionForm.clearRowColNameList();
				    		
				    	list = ejbCAL.getGlFrgColByCsCodeAndLessThanColumnSequenceNumber(actionForm.getColumnSetCode(),
                                Common.convertStringToInt(actionForm.getColumnSequenceNumber()), user.getCmpCode());
				    	
				    	if (list == null  || list.size() == 0) {
				    		
				    		actionForm.setRowColNameList(Constants.GLOBAL_NO_RECORD_FOUND);
				    		
				    	} else {
				    		            		
				    		i = list.iterator();
				    		
				    		while (i.hasNext()) {
				    			
				    		    actionForm.setRowColNameList((String)i.next());
				    			
				    		}
				    		
				    	}
			    	
			    	}        		            	 		        	
			    	
			    	if(actionForm.getRowName() != null && actionForm.getColumnName() == null) {
			            
			            actionForm.clearGlCALList();
			            
			        	list = ejbCAL.getGlFrgCalByRowCode(actionForm.getRowCode(), actionForm.getCalcType(), user.getCmpCode());
			        	
				        i = list.iterator();
				        
				        while(i.hasNext()) {
				        	
				        	GlFrgCalculationDetails details = (GlFrgCalculationDetails)i.next();				      
				        	
				        	GlFrgCalculationList glCALList = new GlFrgCalculationList(actionForm,
				        	    details.getCalCode(),
				        	    String.valueOf(details.getCalSequenceNumber()),
				        	    details.getCalOperator(),
				        	    details.getCalConstant() == 0d ? null : Common.convertDoubleToStringMoney(details.getCalConstant(), Constants.DEFAULT_PRECISION),
				        	    details.getCalRowColName(),
				        	    details.getCalSequenceLow() == 0d ? null : String.valueOf(details.getCalSequenceLow()),
				        	    details.getCalSequenceHigh() == 0d ? null : String.valueOf(details.getCalSequenceHigh()));
				        	    
				            actionForm.saveGlCALList(glCALList);
				        	
				        }			        	
			        
			        } else {
			        	
			        	actionForm.clearGlCALList();
			         
			        	list = ejbCAL.getGlFrgCalByColCode(actionForm.getColumnCode(), user.getCmpCode());
			        	
				        i = list.iterator();
				        
				        while(i.hasNext()) {
				        	
				        	GlFrgCalculationDetails details = (GlFrgCalculationDetails)i.next();

				        	GlFrgCalculationList glCALList = new GlFrgCalculationList(actionForm,
				        	    details.getCalCode(),
				        	    String.valueOf(details.getCalSequenceNumber()),
				        	    details.getCalOperator(),
				        	    details.getCalConstant() == 0d ? null : Common.convertDoubleToStringMoney(details.getCalConstant(), Constants.DEFAULT_PRECISION),
				        	    details.getCalRowColName(),
				        	    details.getCalSequenceLow() == 0d ? null : String.valueOf(details.getCalSequenceLow()),
				        	    details.getCalSequenceHigh() == 0d ? null : String.valueOf(details.getCalSequenceHigh()));
				        	    
				            actionForm.saveGlCALList(glCALList);
				        	
				        }			        	
			        	
			        
			        }
			        
	            
		        } catch (GlobalNoRecordFoundException ex) {
		        			           	        	
		        } catch (EJBException ex) {
	
		           if (log.isInfoEnabled()) {
		
		              log.info("EJBException caught in GlFrgCalculationAction.execute(): " + ex.getMessage() +
		              " session: " + session.getId());
		              return mapping.findForward("cmnErrorPage"); 
		              
		           }
	
	            }                       

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glFrgCalculation"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(ArithmeticException e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFrgCalculationAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}