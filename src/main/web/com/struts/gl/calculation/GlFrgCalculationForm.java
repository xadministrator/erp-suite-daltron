package com.struts.gl.calculation;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlFrgCalculationForm extends ActionForm implements Serializable {

	private Integer rowCode = null;

	private Integer rowSetCode = null;

	private Integer columnSetCode = null;

	private String rowName = null;

	private String lineNumber = null;

	private Integer columnCode = null;

	private String columnName = null;

	private String columnSequenceNumber = null;

	private String sequenceNumber = null;

	private String operator = null;

	private ArrayList operatorList = new ArrayList();

	private String constant = null;

	private String rowColName = null;

	private ArrayList rowColNameList = new ArrayList();

	private String sequenceLow = null;

	private String sequenceHigh = null;

	private String tableType = null;

	private boolean isRow = false;

	private boolean isColumn = false;

	private String calcType = null;

	private String showDetailsButton = null;

	private String hideDetailsButton = null;

	private String saveButton = null;

	private String closeButton = null;

	private String updateButton = null;

	private String cancelButton = null;

	private String pageState = new String();

	private ArrayList glCALList = new ArrayList();

	private int rowSelected = 0;

	private String userPermission = new String();

	private String txnStatus = new String();

	public int getRowSelected() {

		return rowSelected;

	}

	public GlFrgCalculationList getGlCALByIndex(int index) {

		return ((GlFrgCalculationList) glCALList.get(index));

	}

	public Object[] getGlCALList() {

		return glCALList.toArray();

	}

	public int getGlCALListSize() {

		return glCALList.size();

	}

	public void saveGlCALList(Object newGlCALList) {

		glCALList.add(newGlCALList);

	}

	public void clearGlCALList() {

		glCALList.clear();

	}

	public void setRowSelected(Object selectedGlCALList, boolean isEdit) {

		this.rowSelected = glCALList.indexOf(selectedGlCALList);

		if (isEdit) {

			this.pageState = Constants.PAGE_STATE_EDIT;

		}

	}

	public void showGlCALRow(int rowSelected) {

		this.sequenceNumber = ((GlFrgCalculationList) glCALList
				.get(rowSelected)).getSequenceNumber();
		this.operator = ((GlFrgCalculationList) glCALList.get(rowSelected))
				.getOperator();
		this.constant = ((GlFrgCalculationList) glCALList.get(rowSelected))
				.getConstant();
		this.rowColName = ((GlFrgCalculationList) glCALList.get(rowSelected))
				.getRowColName();
		this.sequenceLow = ((GlFrgCalculationList) glCALList.get(rowSelected))
				.getSequenceLow();
		this.sequenceHigh = ((GlFrgCalculationList) glCALList.get(rowSelected))
				.getSequenceHigh();

	}

	public void updateGlCALRow(int rowSelected, Object newGlCALList) {

		glCALList.set(rowSelected, newGlCALList);

	}

	public void deleteGlCALList(int rowSelected) {

		glCALList.remove(rowSelected);

	}

	public void setUpdateButton(String updateButton) {

		this.updateButton = updateButton;

	}

	public void setCancelButton(String cancelButton) {

		this.cancelButton = cancelButton;

	}

	public void setSaveButton(String saveButton) {

		this.saveButton = saveButton;

	}

	public void setCloseButton(String closeButton) {

		this.closeButton = closeButton;

	}

	public void setShowDetailsButton(String showDetailsButton) {

		this.showDetailsButton = showDetailsButton;

	}

	public void setHideDetailsButton(String hideDetailsButton) {

		this.hideDetailsButton = hideDetailsButton;

	}

	public void setPageState(String pageState) {

		this.pageState = pageState;

	}

	public String getPageState() {

		return pageState;

	}

	public String getTxnStatus() {

		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}

	public void setTxnStatus(String txnStatus) {

		this.txnStatus = txnStatus;

	}

	public String getUserPermission() {

		return userPermission;

	}

	public void setUserPermission(String userPermission) {

		this.userPermission = userPermission;

	}

	public Integer getRowCode() {

		return rowCode;

	}

	public void setRowCode(Integer rowCode) {

		this.rowCode = rowCode;

	}

	public Integer getRowSetCode() {

		return rowSetCode;

	}

	public void setRowSetCode(Integer rowSetCode) {

		this.rowSetCode = rowSetCode;

	}

	public Integer getColumnSetCode() {

		return columnSetCode;

	}

	public void setColumnSetCode(Integer columnSetCode) {

		this.columnSetCode = columnSetCode;

	}

	public String getRowName() {

		return rowName;

	}

	public void setRowName(String rowName) {

		this.rowName = rowName;

	}

	public String getLineNumber() {

		return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

		this.lineNumber = lineNumber;

	}

	public Integer getColumnCode() {

		return columnCode;

	}

	public void setColumnCode(Integer columnCode) {

		this.columnCode = columnCode;

	}

	public String getColumnName() {

		return columnName;

	}

	public void setColumnName(String columnName) {

		this.columnName = columnName;

	}

	public String getColumnSequenceNumber() {

		return columnSequenceNumber;

	}

	public void setColumnSequenceNumber(String columnSequenceNumber) {

		this.columnSequenceNumber = columnSequenceNumber;

	}

	public String getSequenceNumber() {

		return sequenceNumber;

	}

	public void setSequenceNumber(String sequenceNumber) {

		this.sequenceNumber = sequenceNumber;

	}

	public String getOperator() {

		return operator;

	}

	public void setOperator(String operator) {

		this.operator = operator;

	}

	public ArrayList getOperatorList() {

		return operatorList;

	}

	public String getConstant() {

		return constant;

	}

	public void setConstant(String constant) {

		this.constant = constant;

	}

	public String getRowColName() {

		return rowColName;

	}

	public void setRowColName(String rowColName) {

		this.rowColName = rowColName;

	}

	public ArrayList getRowColNameList() {

		return rowColNameList;

	}

	public void setRowColNameList(String rowColName) {

		rowColNameList.add(rowColName);

	}

	public void clearRowColNameList() {

		rowColNameList.clear();
		rowColNameList.add(Constants.GLOBAL_BLANK);

	}

	public String getSequenceLow() {

		return sequenceLow;

	}

	public void setSequenceLow(String sequenceLow) {

		this.sequenceLow = sequenceLow;

	}

	public String getSequenceHigh() {

		return sequenceHigh;

	}

	public void setSequenceHigh(String sequenceHigh) {

		this.sequenceHigh = sequenceHigh;

	}

	public String getTableType() {

		return (tableType);

	}

	public void setTableType(String tableType) {

		this.tableType = tableType;

	}

	public boolean getIsRow() {

		return isRow;

	}

	public void setIsRow(boolean isRow) {

		this.isRow = isRow;

	}

	public boolean getIsColumn() {

		return isColumn;

	}

	public void setIsColumn(boolean isColumn) {

		this.isColumn = isColumn;

	}

	public String getCalcType() {

		return calcType;

	}

	public void setCalcType(String calcType) {

		this.calcType = calcType;

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		sequenceNumber = null;
		operatorList.clear();
		operatorList.add(Constants.GL_FRG_CAL_OPERATOR_TYPE_ENTER);
		operatorList.add(Constants.GL_FRG_CAL_OPERATOR_TYPE_AVERAGE);
		operatorList.add(Constants.GL_FRG_CAL_OPERATOR_TYPE_SUB);
		operatorList.add(Constants.GL_FRG_CAL_OPERATOR_TYPE_ADD);
		operatorList.add(Constants.GL_FRG_CAL_OPERATOR_TYPE_DIV);
		operatorList.add(Constants.GL_FRG_CAL_OPERATOR_TYPE_MUL);
		operatorList.add(Constants.GLOBAL_BLANK);
		operator = Constants.GLOBAL_BLANK;
		constant = null;
		rowColName = null;
		sequenceLow = null;
		sequenceHigh = null;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;

	}

	public ActionErrors validateFields(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null
				|| request.getParameter("updateButton") != null) {

			if (Common.validateRequired(sequenceNumber)) {

				errors.add("sequenceNumber", new ActionMessage(
						"calculation.error.sequenceNumberRequired"));

			}

			if (!Common.validateNumberFormat(sequenceNumber)) {

				errors.add("sequenceNumber", new ActionMessage(
						"calculation.error.sequenceNumberInvalid"));

			}

			if (operator.equals("AVERAGE")
					|| operator.equals(Constants.GLOBAL_BLANK)) {

				if (Common.validateRequired(sequenceLow)
						|| Integer.parseInt(sequenceLow) == 0) {

					errors.add("sequenceLow", new ActionMessage(
							"calculation.error.sequenceLowIsRequired"));

				}

				if (!Common.validateNumberFormat(sequenceLow)) {

					errors.add("sequenceLow", new ActionMessage(
							"calculation.error.sequenceLowInvalid"));

				}

				if (Common.validateRequired(sequenceHigh)
						|| Integer.parseInt(sequenceHigh) == 0) {

					errors.add("sequenceHigh", new ActionMessage(
							"calculation.error.sequenceHighIsRequired"));

				}

				if (!Common.validateNumberFormat(sequenceHigh)) {

					errors.add("sequenceHigh", new ActionMessage(
							"calculation.error.sequenceHighInvalid"));

				}

			} else {

				if (!Common.validateRequired(constant)
						&& !Common.validateRequired(rowColName)) {

					if (rowCode != null) {

						errors
								.add(
										"constant",
										new ActionMessage(
												"calculation.error.constantsOrRowNameMustBeNull"));

					} else {

						errors
								.add(
										"constant",
										new ActionMessage(
												"calculation.error.constantsOrColumnNameMustBeNull"));

					}

				}

				if (Common.validateRequired(constant)
						&& Common.validateRequired(rowColName)) {

					if (rowCode != null) {

						errors
								.add(
										"constant",
										new ActionMessage(
												"calculation.error.constantsOrRowNameMustBeEntered"));

					} else {

						errors
								.add(
										"constant",
										new ActionMessage(
												"calculation.error.constantsOrColumnNameMustBeEntered"));

					}

				}

				// if (!Common.validateRequired(constant) &&
				// Integer.parseInt(constant) == 0) {
				if (!Common.validateRequired(constant)
						&& Common.convertStringMoneyToDouble(constant,
								(short) 2) == 0) {

					errors.add("constant", new ActionMessage(
							"calculation.error.constantsMustNotBeZero"));

				}

			}
		}
		return errors;
	}
}