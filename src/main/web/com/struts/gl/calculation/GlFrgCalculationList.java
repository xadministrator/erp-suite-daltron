package com.struts.gl.calculation;

import java.io.Serializable;

public class GlFrgCalculationList implements Serializable {

   private Integer calculationCode = null;
   private String sequenceNumber = null;
   private String operator = null;
   private String constant = null;
   private String rowColName = null;
   private String sequenceLow = null;
   private String sequenceHigh = null;

   private String editButton = null;
   private String deleteButton = null;
    
   private GlFrgCalculationForm parentBean;
    
   public GlFrgCalculationList(GlFrgCalculationForm parentBean,
      Integer calculationCode,
      String sequenceNumber,
      String operator,
      String constant,
      String rowColName,
      String sequenceLow,
      String sequenceHigh) {

      this.parentBean = parentBean;
      this.calculationCode = calculationCode;
      this.sequenceNumber = sequenceNumber;
      this.operator = operator;
      this.constant = constant;
      this.rowColName = rowColName;
      this.sequenceLow = sequenceLow;
      this.sequenceHigh = sequenceHigh;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {
   	
   	  parentBean.setRowSelected(this, false);
   	
   }

   public Integer getCalculationCode() {

      return calculationCode;

   }
   
   public String getSequenceNumber() {
   	
   	  return sequenceNumber;
   	  
   }
   
   public String getOperator() {
   	
   	  return operator;
   	  
   }
   
   public String getConstant() {
   	
   	  return constant;
   	  
   }
   
   public String getRowColName() {
   	
   	  return rowColName;
   	  
   }
   
   public String getSequenceLow() {
   	
   	  return sequenceLow;
   	  
   }
   
   public String getSequenceHigh() {
   	
   	  return sequenceHigh;
   	  
   }
   
}