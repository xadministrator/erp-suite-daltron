package com.struts.gl.chartofaccounts;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlCOAAccountNumberAlreadyAssignedException;
import com.ejb.exception.GlCOAAccountNumberAlreadyDeletedException;
import com.ejb.exception.GlCOAAccountNumberAlreadyExistException;
import com.ejb.exception.GlCOAAccountNumberHasParentValueException;
import com.ejb.exception.GlCOAAccountNumberIsInvalidException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlChartOfAccountController;
import com.ejb.txn.GlChartOfAccountControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.AdResponsibilityDetails;
import com.util.GlChartOfAccountDetails;
import com.util.GlModChartOfAccountDetails;
import com.util.GlModFunctionalCurrencyDetails;

public final class GlChartOfAccountsAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
   
       GlChartOfAccountsForm actionForm = (GlChartOfAccountsForm)form;
       HttpSession session = request.getSession();
       try{
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          if (user != null) {
             if (log.isInfoEnabled()){
                log.info("GlChartOfAccountsAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             }
          }else{
             if (log.isInfoEnabled()){
	        log.info("User is not logged on in session " + session.getId());
             }
	     return(mapping.findForward("adLogon"));
          }
	  String frParam = Common.getUserPermission(user, Constants.GL_CHART_OF_ACCOUNTS_ID);
          if(frParam != null){
	    if(frParam.trim().equals(Constants.FULL_ACCESS)){
               ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	       if(!fieldErrors.isEmpty()){
	          saveErrors(request, new ActionMessages(fieldErrors));
		  return(mapping.findForward("glChartOfAccounts"));
	       }
	    }
            actionForm.setUserPermission(frParam.trim());
          }else{
	     actionForm.setUserPermission(Constants.NO_ACCESS);
	  }
	  
/*******************************************************
   Initialize GlChartOfAccountController EJB
*******************************************************/

          GlChartOfAccountControllerHome homeCOA = null;
          GlChartOfAccountController ejbCOA = null;
       
          try{
          	
	         homeCOA = (GlChartOfAccountControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlChartOfAccountControllerEJB", GlChartOfAccountControllerHome.class);
	         
          }catch(NamingException e){
             if(log.isInfoEnabled()){
	       log.info("NamingException caught in GlChartOfAccountsAction.execute(): " + e.getMessage() + 
	          " session: " + session.getId()); 
	     }
             return(mapping.findForward("cmnErrorPage")); 
          }
       
          try{
              ejbCOA = homeCOA.create();
          }catch(CreateException e){
             if(log.isInfoEnabled()){
                log.info("CreateException caught in GlChartOfAccountsAction.execute(): " + e.getMessage() + 
	           " session: " + session.getId());
             }
             return(mapping.findForward("cmnErrorPage"));
          }
	  
          ActionErrors errors = new ActionErrors();
	  
/*******************************************************
   -- GL COA Save Action --
*******************************************************/

	  if(request.getParameter("saveButton") != null && 
	  		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
	  	
	  	if (actionForm.getAccountCode() == null) {
	  		
	  		GlChartOfAccountDetails details = new GlChartOfAccountDetails(
	  				actionForm.getAccount(), actionForm.getDescription(), actionForm.getAccountType(),  actionForm.getTaxType(), 
	  				actionForm.getCitCategory(),actionForm.getSawCategory(),actionForm.getIitCategory(),
					Common.convertStringToSQLDate(actionForm.getEffectiveDateFrom()),
					Common.convertStringToSQLDate(actionForm.getEffectiveDateTo()),
					Common.convertBooleanToByte(actionForm.getEnabled()),
					Common.convertBooleanToByte(actionForm.getForRevaluation()));
	  		
	  		ArrayList branchList = new ArrayList();
	  		
	  		for(int i=0; i<actionForm.getGlBCOAListSize(); i++) {
	  			
	  			GlBranchCoaList glBcoaList = actionForm.getGlBCOAByIndex(i);
	  			
	  			if(glBcoaList.getBranchCheckbox() == true) {                                          
	  				
	  				AdBranchDetails mDetails = new AdBranchDetails();                                          
	  				
	  				mDetails.setBrAdCompany(user.getCmpCode());	     	        
	  				mDetails.setBrCode(glBcoaList.getBcoaCode());                    
	  				
	  				branchList.add(mDetails);
	  				
	  			}
	  		}             
	  		
	  		
	  		try{
	  			
	  			ejbCOA.addGlCoaEntry(details, branchList, actionForm.getCurrency(), user.getCmpCode());
	  			
	  		}catch(GlCOAAccountNumberIsInvalidException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountInvalid"));
	  		}catch(GlCOAAccountNumberAlreadyExistException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountAlreadyExists"));
	  			
	  		}catch (GlCOAAccountNumberHasParentValueException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountHasParentValue"));

	  		} catch (GlFCNoFunctionalCurrencyFoundException ex) {
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE,
	  					new ActionMessage("chartOfAccounts.error.currencyNotFound"));

	  		} catch (GlFCFunctionalCurrencyAlreadyAssignedException ex) {
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE,
	  					new ActionMessage("chartOfAccounts.error.defaultCompanyCurrencySelected"));

	  		}catch(EJBException ex){
	  			
	  			if(log.isInfoEnabled()){
	  				
	  				log.info("EJBException caught in GlChartOfAccountsAction.execute(): " + ex.getMessage() +
	  						" session: " + session.getId());
	  				
	  			}
	  			
	  			return(mapping.findForward("cmnErrorPage"));
	  			
	  		}
	  		
	  	} else {
	  		
	  		GlChartOfAccountDetails details = new GlChartOfAccountDetails(
	  				actionForm.getAccountCode(),
					actionForm.getAccount(), actionForm.getDescription(), actionForm.getAccountType(), actionForm.getTaxType(),
					actionForm.getCitCategory(),
					actionForm.getSawCategory(),
					actionForm.getIitCategory(),
					Common.convertStringToSQLDate(actionForm.getEffectiveDateFrom()),
					Common.convertStringToSQLDate(actionForm.getEffectiveDateTo()),
					Common.convertBooleanToByte(actionForm.getEnabled()),
					Common.convertBooleanToByte(actionForm.getForRevaluation()));
	  		
	  		ArrayList branchList = new ArrayList();
	  		
	  		for(int i=0; i<actionForm.getGlBCOAListSize(); i++) {
	  			
	  			GlBranchCoaList glBcoaList = actionForm.getGlBCOAByIndex(i);
	  			
	  			if(glBcoaList.getBranchCheckbox() == true) {                                          
	  				
	  				AdBranchDetails mDetails = new AdBranchDetails();                                          
	  				
	  				mDetails.setBrAdCompany(user.getCmpCode());	     	        
	  				mDetails.setBrCode(glBcoaList.getBcoaCode());                    
	  				
	  				branchList.add(mDetails);
	  				
	  			}
	  		} 
	  		
	  		AdResponsibilityDetails mdetails = ejbCOA.getAdRsByRsCode(new Integer(user.getCurrentResCode()));
	  		
	  		try {
	  			
	  			ejbCOA.updateGlCoaEntry(details, mdetails.getRsName(), branchList, actionForm.getCurrency(), user.getCmpCode());
	  			
	  		}catch(GlCOAAccountNumberAlreadyAssignedException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.updateAccountAlreadyAssigned", ex.getMessage()));
	  			
	  		}catch(GlCOAAccountNumberHasParentValueException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountHasParentValue"));
	  			
	  		}catch(GlCOAAccountNumberIsInvalidException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountInvalid"));
	  			
	  		}catch(GlCOAAccountNumberAlreadyExistException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountAlreadyExists"));
	  			
	  		}catch(GlCOAAccountNumberAlreadyDeletedException ex){
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountAlreadyDeleted"));
	  			
            } catch (GlFCNoFunctionalCurrencyFoundException ex) {
             	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("chartOfAccounts.error.currencyNotFound"));
                
	  		} catch (GlFCFunctionalCurrencyAlreadyAssignedException ex) {
	  			
	  			errors.add(ActionMessages.GLOBAL_MESSAGE,
	  					new ActionMessage("chartOfAccounts.error.defaultCompanyCurrencySelected"));

	  		}catch(EJBException ex){
	  			if(log.isInfoEnabled()){
	  				log.info("EJBException caught in GlChartOfAccountsAction.execute(): " + ex.getMessage() +
	  						" session: " + session.getId());
	  			}
	  			return(mapping.findForward("cmnErrorPage"));
	  		}
	  		
	  	}
	  	
/*******************************************************
   -- GL COA Close Action --
*******************************************************/
	  	
	  }else if(request.getParameter("closeButton") != null){
	     return(mapping.findForward("cmnMain"));
	      
/*******************************************************
   -- GL COA Account Enter Action --
*******************************************************/

	  }else if(request.getParameter("isAccountEntered") != null && 
	     request.getParameter("isAccountEntered").equals("true") && 
	     actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
	     	
	     	try {
             	
                 GlModChartOfAccountDetails mdetails = ejbCOA.getGlCoaDescAndAccountTypeByCoaAccountNumber(actionForm.getAccount(), user.getCmpCode());
                 
                 actionForm.setDescription(mdetails.getMcoaDescription());
                 actionForm.setAccountType(mdetails.getMcoaAccountType());
                 
             }catch(GlCOAAccountNumberHasParentValueException ex){
             	
		        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountHasParentValue"));
		        
		     }catch(GlCOAAccountNumberIsInvalidException ex){
		     	
		        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountInvalid"));                 	
		     		      	 
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in ArInvoiceEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }  	
            
            if (!errors.isEmpty()) {
              saveErrors(request, new ActionMessages(errors));
            }
                        
            return(mapping.findForward("glChartOfAccounts"));

		     
/*******************************************************
  -- GL COA Delete Action --
*******************************************************/

	   }else if(request.getParameter("deleteButton") != null &&
	      actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
	      
              try{
		         ejbCOA.deleteGlCoaEntry(actionForm.getAccountCode(), user.getCmpCode());
		         
		      }catch(GlCOAAccountNumberAlreadyAssignedException ex){
		      	
		         errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.deleteAccountAlreadyAssigned"));
		         
		      }catch(GlCOAAccountNumberAlreadyDeletedException ex){
		      	
		         errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("chartOfAccounts.error.accountAlreadyDeleted"));
		         
		      }catch(EJBException ex){
		      	
	                 if(log.isInfoEnabled()){
	                    log.info("EJBException caught in GlChartOfAccountsAction.execute(): " + ex.getMessage() +
		               " session: " + session.getId());
		         }
		         return(mapping.findForward("cmnErrorPage"));
		      }

/*******************************************************
   -- GL COA Load Action --
*******************************************************/

	   }
	   if(frParam != null){
	   	
	      if (!errors.isEmpty()) {
               saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glChartOfAccounts"));
          }         
          
          if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
          }
          
          try {
          	
          	actionForm.reset(mapping, request);
          	
          	ArrayList list = null;
          	Iterator i = null;
          	
          	actionForm.clearGlBCOAList();
          	
          	list = ejbCOA.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
          	
          	i = list.iterator();
          	
          	while(i.hasNext()) {
          		
          		AdBranchDetails details = (AdBranchDetails)i.next();
          		
          		GlBranchCoaList glBCOAList = new GlBranchCoaList(actionForm,
          				details.getBrCode(),
						details.getBrName());
          		
          		if ( request.getParameter("forward") == null )
          			glBCOAList.setBranchCheckbox(true);
          		
          		actionForm.saveGlBCOAList(glBCOAList);
          		
          	}

          	actionForm.clearCitCategoryList();
	        list = ejbCOA.getAdLvCitCategoryAll(user.getCmpCode());

        	if (list == null || list.size() == 0) {
        		
        		actionForm.setCitCategoryList(Constants.GLOBAL_BLANK);
        	
        	} else {
        		           		            		
        		i = list.iterator();
        		
        		while (i.hasNext()) {
        			actionForm.setCitCategoryList((String)i.next());
        		
        			
        		}
        		            		
        	}
        	
        	
        	
        	actionForm.clearSawCategoryList();
	        list = ejbCOA.getAdLvSAWCategoryAll(user.getCmpCode());
	        	
        	if (list == null || list.size() == 0) {
        		
        		actionForm.setSawCategoryList(Constants.GLOBAL_BLANK);

        	} else {
        		           		            		
        		i = list.iterator();
        		
        		while (i.hasNext()) {
        			actionForm.setSawCategoryList((String)i.next());

        			
        		}
        		            		
        	}
        		
        	
        //	actionForm.clearAccountCategoryList1();
	        actionForm.clearIitCategoryList();
	   //     list = ejbCOA.getAdLvAccountCategoryAll1(user.getCmpCode());
        	list = ejbCOA.getAdLvIiTCategoryAll(user.getCmpCode());
        	if (list == null || list.size() == 0) {
        		
        	
        	//	actionForm.setAccountCategoryList1(Constants.GLOBAL_BLANK);
        		actionForm.setIitCategoryList(Constants.GLOBAL_BLANK);
        	} else {
        		           		            		
        		i = list.iterator();
        		
        		while (i.hasNext()) {
        			
        		  //  actionForm.setAccountCategoryList1((String)i.next());
        			//System.out.println("actionForm.getAccountCategoryList1()="+actionForm.getAccountCategoryList1());
        		
        			actionForm.setIitCategoryList((String)i.next());
        			System.out.println("actionForm.setIitCategoryList()="+actionForm.getIitCategoryList());
            		
        		}
        		            		
        	}
          	
          	actionForm.clearCurrencyList();
          	
          	list = ejbCOA.getGlFcAllWithDefault(user.getCmpCode());
          	
          	actionForm.setCurrency(Constants.GLOBAL_BLANK);
          	
          	if (list == null  || list.size() == 0) {
          		
          		actionForm.setCurrency(Constants.GLOBAL_NO_RECORD_FOUND);
          		
          	} else {
          		
          		i = list.iterator();
          		
          		while (i.hasNext()) {
          			
          			GlModFunctionalCurrencyDetails mdetails = (GlModFunctionalCurrencyDetails)i.next();
          			actionForm.setCurrencyList(mdetails.getFcName());
          			
          		}
          		
          	}
          	
          } catch (GlobalNoRecordFoundException ex) {
              
          } catch (EJBException ex) {
              
              if (log.isInfoEnabled()) {
                  
                  log.info("EJBException caught in GlChartOfAccountAction.execute(): " + ex.getMessage() +
                          " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
              }
              
          }   
          
          try {
          	
          	if (request.getParameter("forward") != null) {
          		
          		GlModChartOfAccountDetails mdetails = ejbCOA.getGlCoaByCoaCode(
          				new Integer(request.getParameter("coaCode")), user.getCmpCode());
          		
          		actionForm.setAccountCode(mdetails.getMcoaCode());
          		actionForm.setCitCategory(mdetails.getMcoaCitCategory());
          		actionForm.setSawCategory(mdetails.getMcoaSawCategory());
          		actionForm.setIitCategory(mdetails.getMcoaIitCategory());
          	//	actionForm.setAccountCategory(mdetails.getMcoaAccountCategory());
          	//	actionForm.setAccountCategory1(mdetails.getMcoaAccountCategory1());
          		actionForm.setAccount(mdetails.getMcoaAccountNumber());
          		actionForm.setAccountType(mdetails.getMcoaAccountType());
          		actionForm.setTaxType(mdetails.getMcoaTaxType());
          		actionForm.setDescription(mdetails.getMcoaDescription());
          		actionForm.setEffectiveDateFrom(Common.convertSQLDateToString(mdetails.getMcoaDateFrom()));
          		actionForm.setEffectiveDateTo(Common.convertSQLDateToString(mdetails.getMcoaDateTo()));
          		actionForm.setEnabled(Common.convertByteToBoolean(mdetails.getMcoaEnable()));
          		actionForm.setCurrency(mdetails.getMcoaGlFuntionalCurrency() == null ?
          				Constants.GLOBAL_BLANK : mdetails.getMcoaGlFuntionalCurrency());
          		actionForm.setForRevaluation(Common.convertByteToBoolean(mdetails.getMcoaForRevaluation()));
          		
          		// retrieve branch coa
          		
          		ArrayList list = null;	        		
          		try {
          			
          			list = ejbCOA.getAdBrCoaAll(mdetails.getMcoaCode(), user.getCmpCode());
          			
          		} catch(GlobalNoRecordFoundException ex) { }
          		
          		Object[] obj = actionForm.getGlBCOAList();	        			        		
          		
          		for(int i=0; i<obj.length; i++) {	        		    
          			GlBranchCoaList glBrCoaList = (GlBranchCoaList)obj[i];
          			
          			if(list != null) {	        		       
          				Iterator brListIter = list.iterator();	     
          				
          				while(brListIter.hasNext()) {	        		            
          					AdBranchDetails details = (AdBranchDetails)brListIter.next();	
          					
          					if(details.getBrCode().equals(glBrCoaList.getBcoaCode())) {
          						
          						glBrCoaList.setBranchCheckbox(true);	        		                                  
          						break;
          						
          					}	        		            	        		           
          				}
          				
          			}	        		    	        		    
          			
          		}
          		
          		return(mapping.findForward("glChartOfAccounts"));
          		
          	}	                 
          	
          	actionForm.setAccountCode(null);
          	actionForm.setEnabled(true);
          	
          }catch(EJBException ex){
		      	
             if(log.isInfoEnabled()){
                log.info("EJBException caught in GlChartOfAccountsAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	         }
	         return(mapping.findForward("cmnErrorPage"));
	      }

	      

	      	
	      if ((request.getParameter("saveButton") != null || request.getParameter("deleteButton") != null) &&
		        actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
	
		       actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
		       
		  }

          	      
	      return(mapping.findForward("glChartOfAccounts"));
	      
	   } else {
	   	
	      errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
              saveErrors(request, new ActionMessages(errors));
	  
	      return(mapping.findForward("cmnMain"));
	      
	  } 
	    
      }catch(Exception e){

/*******************************************************
   Failed: Forward to error page
********************************************************/
         if(log.isInfoEnabled()){
		    log.info("Exception caught in GlChartOfAccountsAction.execute(): " + e.getMessage() + 
		       " session: " + session.getId());
		 }
	 	 return(mapping.findForward("cmnErrorPage"));
      } 
    }
}
