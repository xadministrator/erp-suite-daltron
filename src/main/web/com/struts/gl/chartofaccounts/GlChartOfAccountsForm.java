package com.struts.gl.chartofaccounts;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlChartOfAccountsForm extends ActionForm implements Serializable{

    private Integer accountCode = null;
	private String account = null;	
	private String accountType = null;

	private String effectiveDateFrom = null;
	private String effectiveDateTo = null;
	private boolean enabled = false;
    private String description = null;
	private String saveButton = null;
	private String closeButton = null;	
	private String deleteButton = null;
	private String pageState = new String();
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean isAccountEntered = false;
	
	private ArrayList glBCOAList = new ArrayList();

	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	
	private String taxType = null;
	private ArrayList taxTypeList = new ArrayList();
	
	private String citCategory = null;
	private ArrayList citCategoryList = new ArrayList();
	
	private String sawCategory = null;
	private ArrayList sawCategoryList = new ArrayList();
	
	
	private String iitCategory = null;
	private ArrayList iitCategoryList = new ArrayList();
	
	private boolean forRevaluation = false;
	
	public void setSaveButton(String saveButton){
	   this.saveButton = saveButton;
	}

	public void setCloseButton(String closeButton){
	   this.closeButton = closeButton;
	}
	
	public void setDeleteButton(String deleteButton){
		this.deleteButton = deleteButton;
	}

	public void setPageState(String pageState){
	   this.pageState = pageState;
	}

	public String getPageState(){
	   return(pageState);
	}
	
	public boolean getIsAccountEntered() {
   	
   	   return isAccountEntered;
   	
   }
   
   public void setIsAccountEntered(boolean isAccountEntered) {
   	
   	   this.isAccountEntered = isAccountEntered;
   	
   }

	public String getTxnStatus(){
	   String passTxnStatus = txnStatus;
	   txnStatus = Constants.GLOBAL_BLANK;
	   return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
	   this.txnStatus = txnStatus;
	}

    public Integer getAccountCode() {
    	
    	return accountCode;
    	
    }
    
    public void setAccountCode(Integer accountCode) {
    	
    	this.accountCode = accountCode;
    	
    }

    public String getAccount(){
	   return(account);
	}

	public void setAccount(String account){
	   this.account = account;
	}

	public String getAccountType(){
	  return(accountType);
	}

	public void setAccountType(String accountType){
	   this.accountType = accountType;
	}
	
	
		

	public String getEffectiveDateFrom(){
	   return(effectiveDateFrom);
	}

	public void setEffectiveDateFrom(String effectiveDateFrom){
	   this.effectiveDateFrom = effectiveDateFrom;
	}

	public String getEffectiveDateTo(){
	   return(effectiveDateTo);
	}

	public void setEffectiveDateTo(String effectiveDateTo){
	   this.effectiveDateTo = effectiveDateTo;
	}

	public boolean getEnabled(){
	   return(enabled);
	}

	public void setEnabled(boolean enabled){
	   this.enabled = enabled;
	}

	public String getDescription(){
	   return(description);
	}
	
	public void setDescription(String description){
	    this.description = description;
	}
	
	public Object[] getGlBCOAList(){
	    
	    return glBCOAList.toArray();
	    
	}
	
	public GlBranchCoaList getGlBCOAByIndex(int index){
	    
	    return ((GlBranchCoaList)glBCOAList.get(index));
	    
	}
	
	public int getGlBCOAListSize(){
	    
	    return(glBCOAList.size());
	    
	}
	
	public void saveGlBCOAList(Object newGlBCOAList){
	    
	    glBCOAList.add(newGlBCOAList);   	  
	    
	}
	
	public void clearGlBCOAList(){
	    
	    glBCOAList.clear();
	    
	}
	
	public void setGlBCOAList(ArrayList glBCOAList) {
	    
	    this.glBCOAList = glBCOAList;
	    
	}
	
	public String getUserPermission(){
	    return(userPermission);
	}

	public void setUserPermission(String userPermission){
	   this.userPermission = userPermission;
	}

	public String getCurrency() {
		
		return currency;
		
	}
	
	public void setCurrency(String currency) {
		
		this.currency = currency;
		
	}
	
	public ArrayList getCurrencyList() {
		
		return currencyList;
		
	}
	
	public void clearCurrencyList() {
		
		currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public void setCurrencyList(String currency) {
		
		currencyList.add(currency);
		
	}
	
	
	public String getTaxType() {

		return taxType ;   	

	}

	public void setTaxType(String taxType) {

		this.taxType = taxType;

	}

	public ArrayList getTaxTypeList() {

		return taxTypeList;

	}

	public String getCitCategory(){
		
		return(citCategory);
		
	}
	
	public void setCitCategory(String citCategory){
		
		this.citCategory = citCategory;
		
	}

	public ArrayList getCitCategoryList() {

		return(citCategoryList);

	}

	public void setCitCategoryList(String citCategory) {

		citCategoryList.add(citCategory);

	}

	public void clearCitCategoryList() {

		citCategoryList.clear();
		citCategoryList.add(Constants.GLOBAL_BLANK);

	}
	
	
	
	public String getSawCategory(){
		
		return(sawCategory);
		
	}
	
	
	public void setSawCategory(String sawCategory){
		
		this.sawCategory = sawCategory;
		
	}

	public ArrayList getSawCategoryList() {

		return(sawCategoryList);

	}

	public void setSawCategoryList(String sawCategory) {

		sawCategoryList.add(sawCategory);

	}

	public void clearSawCategoryList() {

		sawCategoryList.clear();
		sawCategoryList.add(Constants.GLOBAL_BLANK);
		
		
		

	}
	
	
	
	
	
	
	
	
	public String getIitCategory(){
		
		return(iitCategory);
		
	}
	
	public void setIitCategory(String iitCategory){
		
		this.iitCategory = iitCategory;
		
	}

	public ArrayList getIitCategoryList() {

		return(iitCategoryList);

	}

	public void setIitCategoryList(String iitCategory) {

		iitCategoryList.add(iitCategory);

	}

	public void clearIitCategoryList() {
		iitCategoryList.clear();
		iitCategoryList.add(Constants.GLOBAL_BLANK);
	}
	
	
	public boolean getForRevaluation(){
		
	   return(forRevaluation);
	   
	}

	public void setForRevaluation(boolean forRevaluation){
		
	   this.forRevaluation = forRevaluation;
	   
	}

    public void reset(ActionMapping mapping, HttpServletRequest request){
        
       for (int i=0; i<glBCOAList.size(); i++) {
            
	        GlBranchCoaList  list = (GlBranchCoaList)glBCOAList.get(i);
	        list.setBranchCheckbox(false);	       
            
       }  
       
       
	   account = null;
	   effectiveDateFrom = null;
	   effectiveDateTo = null;
	   enabled = false;
	   description = null;
	   saveButton = null;
	   closeButton = null;
	   deleteButton = null;
	   isAccountEntered = false;
	   forRevaluation = false;
	   
	   taxTypeList.clear();
	   taxTypeList.add(Constants.GLOBAL_BLANK);
	   taxTypeList.add("CAPITAL GOODS");
	   taxTypeList.add("SERVICES");
	   taxTypeList.add("OTHER CAPITAL GOODS");
	   taxType = Constants.GLOBAL_BLANK;
	   
	}

    public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
           ActionErrors errors = new ActionErrors();
           if(request.getParameter("saveButton") != null){
             if(Common.validateRequired(account)) {
                errors.add("account", new ActionMessage("chartOfAccounts.error.accountRequired"));
             }
	     if(Common.validateRequired(effectiveDateFrom)) {
	        errors.add("effectiveDateFrom", new ActionMessage("chartOfAccounts.error.effectiveDateFromRequired"));
	     }    
	     if(!Common.validateDateFormat(effectiveDateFrom)){
	        errors.add("effectiveDateFrom", new ActionMessage("chartOfAccounts.error.effectiveDateFromInvalid"));
	     }
	     if(!Common.validateDateFormat(effectiveDateTo)){
	        errors.add("effectiveDateTo", new ActionMessage("chartOfAccounts.error.effectiveDateToInvalid"));
	     }
	     if(!Common.validateDateFromTo(effectiveDateFrom, effectiveDateTo)){
	        errors.add("effectiveDateFrom", new ActionMessage("chartOfAccounts.error.effectiveDateFromToInvalid"));	
	     }
	     if(!Common.validateDateGreaterThanCurrent(effectiveDateTo)){
	        errors.add("effectiveDateTo", new ActionMessage("chartOfAccounts.error.effectiveDateToLessThanCurrent"));
	     }

	   }
	  return(errors);
	}
		
}
