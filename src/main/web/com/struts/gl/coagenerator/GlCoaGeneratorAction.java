package com.struts.gl.coagenerator;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlCOAAccountNumberAlreadyAssignedException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.txn.GlCoaGeneratorController;
import com.ejb.txn.GlCoaGeneratorControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.GenModValueSetDetails;
import com.util.GenValueSetValueDetails;
import com.util.GlChartOfAccountDetails;
import com.util.GlModFunctionalCurrencyDetails;

public final class GlCoaGeneratorAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlCoaGeneratorAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlCoaGeneratorForm actionForm = (GlCoaGeneratorForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_COA_GENERATOR_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  return mapping.findForward("glCoaGenerator");
                  
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlCoaGeneratorController EJB
*******************************************************/

         GlCoaGeneratorControllerHome homeCG = null;
         GlCoaGeneratorController ejbCG = null;

         try {

            homeCG = (GlCoaGeneratorControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlCoaGeneratorControllerEJB", GlCoaGeneratorControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlCoaGeneratorAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCG = homeCG.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlCoaGeneratorAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
/*******************************************************
   Call GlCoaGeneratorController EJB
   getGlFlSgmntSeparator
*******************************************************/
         
         char separator = Common.convertStringToChar(null);
         short numOfSgmnt = 0;
     	
         try {
         	
         	separator = ejbCG.getGenFlSgmntSeparator(user.getCmpCode());
         	numOfSgmnt = ejbCG.getGenFlNumberOfSegment(user.getCmpCode());
         	
         } catch(EJBException ex) {
         	
         	if (log.isInfoEnabled()) {
         		
         		log.info("EJBException caught in GlCoaGeneratorAction.execute() : " + ex.getMessage() +
         				" session: " + session.getId());
         	}
         	
         	return(mapping.findForward("cmnErrorPage"));
         	
         }
         
                 
/*******************************************************
   -- Gl CG Generate Action --
*******************************************************/

         if (request.getParameter("generateButton") != null &&
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	// get branches
         	ArrayList branchList = new ArrayList();
         	
         	for(int i=0; i<actionForm.getGlBCOAListSize(); i++) {
         		
         		GlBranchCoaList glBrCoaList = actionForm.getGlBCOAByIndex(i);
         		
         		if(glBrCoaList.getBranchCheckbox() == true) {                                          
         			
         			AdBranchDetails details = new AdBranchDetails();                                          
         			
         			details.setBrAdCompany(user.getCmpCode());	     	        
         			details.setBrCode(glBrCoaList.getBcoaCode());                    
         			
         			branchList.add(details);
         			
         		}
         		
         	}         
         	
         	ArrayList checkedSegments = new ArrayList();
         	short tempNumOfSgmnt = 1;
         	String tempSegment = null;
         	
         	for (int i = 0; i<actionForm.getGlCoaGenListSize(); i++) {
         		
         		GlCoaGeneratorList glCoaGenList = actionForm.getGlCoaGenByIndex(i);
         		
         		// get checked segments 
         		if(glCoaGenList.getSegmentChecked()) {
         			
         			checkedSegments.add(glCoaGenList);
         			
         			if(tempSegment == null || !tempSegment.equals(glCoaGenList.getSegmentTitle())) {
         				tempSegment = glCoaGenList.getSegmentTitle();
         				tempNumOfSgmnt++;
         			}
         			
         		}	
         			
         	}
         	
         	// check if accounts to be generated are valid
         	if (tempNumOfSgmnt < numOfSgmnt) {
         			
         			errors.add(ActionMessages.GLOBAL_MESSAGE,
         					new ActionMessage("coaGenerator.error.accountToGenerateInvalid"));
         			
         	}
         	
         	// get list of natural accounts
         	ArrayList accounts = new ArrayList();
         	
         	try {
         		
         		accounts = ejbCG.getGenNaturalAccountAll(actionForm.getNaturalAcctFrom(), actionForm.getNaturalAcctTo(), user.getCmpCode());
         		
         	}catch(GlobalNoRecordFoundException ex){		    	
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("coaGenerator.error.noRecordFound"));
         	}
         	
         	Iterator i = accounts.iterator();
         	
         	while(i.hasNext()){
         		
         		GenValueSetValueDetails vsvdetails = (GenValueSetValueDetails)i.next();
         		
         		ArrayList coaList = this.generateCoa(checkedSegments, vsvdetails, separator);
         		
         		// create coa
         		Iterator j = coaList.iterator();
         		
         		while(j.hasNext()){
         			
         			String chartOfAccount = (String)j.next();                	                
         			
         			GlChartOfAccountDetails details = new GlChartOfAccountDetails(    	            
         					chartOfAccount, null, null, null, null,null, null,
							Common.convertStringToSQLDate(actionForm.getEffectiveDateFrom()),
							Common.convertStringToSQLDate(actionForm.getEffectiveDateTo()),
							Common.convertBooleanToByte(actionForm.getEnabled()), (byte)1);
         			
         			try {
         				
         				ejbCG.generateGlCoa(details, new Integer(user.getCurrentResCode()), branchList, actionForm.getCurrency(), user.getCmpCode());

        	  		} catch (GlFCNoFunctionalCurrencyFoundException ex) {
        	  			
        	  			errors.add(ActionMessages.GLOBAL_MESSAGE,
        	  					new ActionMessage("coaGenerator.error.currencyNotFound"));

        	  		} catch (GlFCFunctionalCurrencyAlreadyAssignedException ex) {
        	  			
        	  			errors.add(ActionMessages.GLOBAL_MESSAGE,
        	  					new ActionMessage("coaGenerator.error.defaultCompanyCurrencySelected"));

        	  		} catch (GlCOAAccountNumberAlreadyAssignedException ex) {
        	  			
        	  			errors.add(ActionMessages.GLOBAL_MESSAGE,
        	  					new ActionMessage("coaGenerator.error.accountAlreadyAssigned", ex.getMessage()));

        	  		} catch (EJBException ex) {
         				
         				if (log.isInfoEnabled()) {
         					
         					log.info("EJBException caught in GlCoaGeneratorAction.execute() : " + ex.getMessage() +
         							" session: " + session.getId());
         					return mapping.findForward("cmnErrorPage"); 
         					
         				}
         				
         			}
         			
         		}
         		
         	}
         	
/*******************************************************
 	-- GL CG Delete Action --
********************************************************/
         	
         }else if(request.getParameter("deleteButton") != null &&
         		actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
         	
         	// get branches
            ArrayList branchList = new ArrayList();
            
            for(int i=0; i<actionForm.getGlBCOAListSize(); i++) {
                
                GlBranchCoaList glBrCoaList = actionForm.getGlBCOAByIndex(i);
                
                if(glBrCoaList.getBranchCheckbox() == true) {                                          
                    
                    AdBranchDetails details = new AdBranchDetails();                                          
                    
                    details.setBrAdCompany(user.getCmpCode());	     	        
                    details.setBrCode(glBrCoaList.getBcoaCode());                    
                    
                    branchList.add(details);
                    
                }
                
            }   
            
           ArrayList checkedSegments = new ArrayList();
           short tempNumOfSgmnt = 1;
           String tempSegment = null;
        	
           for (int i = 0; i<actionForm.getGlCoaGenListSize(); i++) {
          	
          		GlCoaGeneratorList glCoaGenList = actionForm.getGlCoaGenByIndex(i);
          		 
                // get checked segments 
                if(glCoaGenList.getSegmentChecked()) {
                	
                	checkedSegments.add(glCoaGenList);
              		
                    if(tempSegment == null || !tempSegment.equals(glCoaGenList.getSegmentTitle())) {
             			tempSegment = glCoaGenList.getSegmentTitle();
             			tempNumOfSgmnt++;
             		}
                    
                }
          			
          	}

           // check if accounts to be generated are valid
           if (tempNumOfSgmnt < numOfSgmnt) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("coaGenerator.error.accountToGenerateInvalid"));
           	
           }
        	
          	// get list of natural accounts
        	ArrayList accounts = new ArrayList();
        	
        	try {
        		
        		accounts = ejbCG.getGenNaturalAccountAll(actionForm.getNaturalAcctFrom(), actionForm.getNaturalAcctTo(), user.getCmpCode());
        		
        	}catch(GlobalNoRecordFoundException ex){		    	
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("coaGenerator.error.noRecordFound"));
         	
        	}
        	
       		Iterator i = accounts.iterator();
       		
          	while(i.hasNext()){
          		
          		GenValueSetValueDetails vsvdetails = (GenValueSetValueDetails)i.next();
          		
          		ArrayList coaList = this.generateCoa(checkedSegments, vsvdetails, separator);
           		
           		// create coa
           		Iterator j = coaList.iterator();
           		
           		while(j.hasNext()){
           			
           			String chartOfAccount = (String)j.next();                	                
           			
         			try{
         				
         				ejbCG.deleteGlCoa(chartOfAccount,user.getCmpCode());
         				
         			} catch (GlobalRecordAlreadyAssignedException ex){
         				
         				errors.add(ActionMessages.GLOBAL_MESSAGE,
                				new ActionMessage("coaGenerator.error.accountAlreadyAssigned", ex.getMessage()));
         				
         			}catch(EJBException ex){
         				
         				if(log.isInfoEnabled()){
         					log.info("EJBException caught in GlChartOfAccountsAction.execute() : " + ex.getMessage() +
         							" session: " + session.getId());
         				}
         				return(mapping.findForward("cmnErrorPage"));
         			}
         			
         		}
         		
         	}
         	
/*******************************************************
   -- Gl CG Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Gl CG Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
         	if (!errors.isEmpty()) {
         		
         		saveErrors(request, new ActionMessages(errors));
         		return mapping.findForward("glCoaGenerator");
         		
         	}
         	
         	if (request.getParameter("forward") == null &&
         			actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
         		saveErrors(request, new ActionMessages(errors));
         		
         		return mapping.findForward("cmnMain");	
         		
         	}
         	
         	actionForm.reset(mapping, request);
     		
         	String accountName = null;
     		
         	try {
         		
         		// segment list
         		actionForm.clearGlCoaGenList();
         		
         		ArrayList list = ejbCG.getGenVsAll(user.getCmpCode());                
         		Iterator i = list.iterator();
         		
         		while (i.hasNext()) {
         			
         			GenModValueSetDetails vsdetails = (GenModValueSetDetails)i.next();
         			
         			Iterator j  = null;
         			
         			// get segments that are not natural account
         			if(!Common.convertByteToBoolean(vsdetails.getVsSgNatural())){
         				
         				int ctr = 0;
         				ArrayList vsvList = ejbCG.getGenVsvAllByVsName(vsdetails.getVsName(), user.getCmpCode());
         				
         				j = vsvList.iterator();
         				
         				while (j.hasNext()){
         					
         					GenValueSetValueDetails vsvdetails = (GenValueSetValueDetails)j.next();
         					
         					GlCoaGeneratorList segment = new GlCoaGeneratorList(
         							actionForm,	new Integer(ctr++),
									vsvdetails.getVsvValue(), vsvdetails.getVsvDescription(),
									vsdetails.getVsName(), vsdetails.getVsSegmentNumber());
         					
         					actionForm.addGlCoaGenList(segment);
         					
         				}
         				
         			} else {
         				
         				if(actionForm.getNaturalAcctName() == null)
         					accountName = vsdetails.getVsName();
         				
         			}
         			
         		}
         		
         		if(actionForm.getGlCoaGenListSize() > 0) {
         			actionForm.setShowSegments(true);
         		} else {
         			actionForm.setShowSegments(false);
         		}
         		
         		// branch list
         		actionForm.clearGlBCOAList();
         		
         		list = ejbCG.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
         		i = list.iterator();
         		
         		while(i.hasNext()) {
         			
         			AdBranchDetails details = (AdBranchDetails)i.next();
         			
         			GlBranchCoaList glBCOAList = new GlBranchCoaList(actionForm,
         					details.getBrCode(), details.getBrName());
         			glBCOAList.setBranchCheckbox(true);
         			
         			actionForm.saveGlBCOAList(glBCOAList);
         			
         		}
         		
              	
              	actionForm.clearCurrencyList();
              	
              	list = ejbCG.getGlFcAllWithDefault(user.getCmpCode());
              	
              	actionForm.setCurrency(Constants.GLOBAL_BLANK);
              	
              	if (list == null  || list.size() == 0) {
              		
              		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
              		
              	} else {
              		
              		i = list.iterator();
              		
              		while (i.hasNext()) {
              			
              			GlModFunctionalCurrencyDetails mdetails = (GlModFunctionalCurrencyDetails)i.next();
              			actionForm.setCurrencyList(mdetails.getFcName());
              			
              		}
              		
              	}
         		
         	} catch (GlobalNoRecordFoundException ex) {
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in GlCoaGeneratorAction.execute(): -> loadAction" + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage"); 
         			
         		}
         		
         	}   
         	
         	if (!errors.isEmpty()) {

         		saveErrors(request, new ActionMessages(errors));
                
             } else {

                if (request.getParameter("generateButton") != null || request.getParameter("deleteButton") != null
                		&& actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                    actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

                }
             }
         	
         	
         	actionForm.setNaturalAcctName(accountName);
     		actionForm.setEnabled(true);
         	actionForm.setLineCount(0);                        
         	
         	this.setFormProperties(actionForm);                        
         	
         	return(mapping.findForward("glCoaGenerator"));
         	
         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlCoaGeneratorAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
             e.printStackTrace();
             
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
   private void setFormProperties(GlCoaGeneratorForm actionForm) {
	
	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
		actionForm.setEnableFields(true);
		actionForm.setShowSaveButton(true);

	} else {

		actionForm.setEnableFields(false);
		actionForm.setShowSaveButton(false);

	}
	
   }
   
   private ArrayList generateCoa(ArrayList checkedSegments, GenValueSetValueDetails details, char separator) {
   	
   	ArrayList coaList = new ArrayList();
   	
   	String account = details.getVsvValue();
   	short accountSgNumber = details.getVsvSegmentNumber();
   	
   	// temp list of coa to generate
   	ArrayList temp = new ArrayList();
   	temp.add(account);
   	
   	String coa = null;
   	short segmentNumber = 0;
   	
   	if(!checkedSegments.isEmpty()) {
   		
   		Iterator j = checkedSegments.iterator();
   		
   		while(j.hasNext()) {
   			
   			GlCoaGeneratorList checkedSegment = (GlCoaGeneratorList)j.next();
   			
   			if ((segmentNumber == 0) || (checkedSegment.getSegmentSgNumber() == segmentNumber)) {
   				
   				if(segmentNumber == 0)
   					segmentNumber = checkedSegment.getSegmentSgNumber();
   				
   				for(int ctr=0; ctr<temp.size(); ctr++){
   					
   					coa = (String)temp.get(ctr);;
   					
   					if(accountSgNumber < checkedSegment.getSegmentSgNumber()) {
   						
   						coa = coa + separator + checkedSegment.getSegment();
   						
   					} else {
   						
   						int index = coa.length() - account.length();
   						coa = coa.substring(0,index);
   						
   						coa = coa + checkedSegment.getSegment() + separator + account;
   						
   					}
   					
   					coaList.add(coa);
   					
   				}
   				
   			} else {
   				
   				segmentNumber = checkedSegment.getSegmentSgNumber();
   				temp = coaList;
   				coaList = new ArrayList();
   				
   				for(int ctr=0; ctr<temp.size(); ctr++){
   					
   					coa = (String)temp.get(ctr);
   					
   					if(accountSgNumber < checkedSegment.getSegmentSgNumber()) {
   						
   						coa = coa + separator + checkedSegment.getSegment();
   						
   					} else {
   						
   						int index = coa.length() - account.length();
   						coa = coa.substring(0,index);
   						
   						coa = coa + checkedSegment.getSegment() + separator + account;
   						
   					}
   					
   					coaList.add(coa);
   					
   				}
   				
   			}
   			
   		}
   		
   	} else {
   		
   		// support for single segments
   		coaList.add(account);
   		
   	}
   	
   	return coaList;
   	
   }
   
}