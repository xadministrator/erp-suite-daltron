package com.struts.gl.coagenerator;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlCoaGeneratorForm extends ActionForm implements Serializable {
	
	private String effectiveDateFrom = null;
	private String effectiveDateTo = null;
	private boolean enabled = false;
	private String naturalAcctFrom = null;
	private String naturalAcctFromDesc = null;
	private String naturalAcctTo = null;
	private String naturalAcctToDesc = null;
	private String naturalAcctName = null;	
	
	private String pageState = new String();
	private ArrayList glCoaGenList  = new ArrayList();
	private int rowSelected = 0;          
	private int columnSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private boolean enableFields = false;
	private boolean showSaveButton = false;
	private boolean showSegments = false;
	
	private String saveButton = null;
	private String closeButton = null;	
	private String deleteButton = null;   
	private String selectAllButton = null;
	
	private int lineCount = 0;
	
	private ArrayList glBCOAList = new ArrayList();

	private String currency = null;
	private ArrayList currencyList = new ArrayList();  	
	
	public int getLineCount(){
		
		return lineCount;
		
	}
	
	public void setLineCount(int lineCount){  	
		
		this.lineCount = lineCount;
		
	}    
	
	public GlCoaGeneratorList getGlCoaGenByIndex(int index) {
		
		return((GlCoaGeneratorList)glCoaGenList.get(index));
		
	}
	
	public Object[] getGlCoaGenList() {
		
		return glCoaGenList.toArray();
		
	}
	
	public int getGlCoaGenListSize() {
		
		return glCoaGenList.size();
		
	}
	
	public void addGlCoaGenList(Object newGlCoaGenList) {
		
		glCoaGenList.add(newGlCoaGenList);
		
	}
	
	public void clearGlCoaGenList() {
		
		glCoaGenList.clear();
		
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public String getEffectiveDateFrom(){
		
		return(effectiveDateFrom);
		
	}
	
	public void setEffectiveDateFrom(String effectiveDateFrom){
		
		this.effectiveDateFrom = effectiveDateFrom;
		
	}
	
	public String getEffectiveDateTo(){
		
		return(effectiveDateTo);
		
	}
	
	public void setEffectiveDateTo(String effectiveDateTo){
		
		this.effectiveDateTo = effectiveDateTo;
		
	}
	
	public boolean getEnabled(){
		
		return(enabled);
		
	}
	
	public void setEnabled(boolean enabled){
		
		this.enabled = enabled;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public boolean getShowSaveButton() {
		
		return showSaveButton;
		
	}
	
	public void setShowSaveButton(boolean showSaveButton) {
		
		this.showSaveButton = showSaveButton;
		
	}
	
	public void setSaveButton(String saveButton){
		
		this.saveButton = saveButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public void setDeleteButton(String deleteButton){
		
		this.deleteButton = deleteButton;
		
	}
	
	public void setColumnSelected(Object selectedGlCoaGenList, Object selectedGlCOASegList) {
		
		GlCoaGeneratorList coaGenList = (GlCoaGeneratorList) glCoaGenList.get(glCoaGenList.indexOf(selectedGlCoaGenList));
				
	}
	
	public int getColumnSelected() {
		
		return columnSelected;
		
	}
	
	public Object[] getGlBCOAList(){
		
		return glBCOAList.toArray();
		
	}
	
	public GlBranchCoaList getGlBCOAByIndex(int index){
		
		return ((GlBranchCoaList)glBCOAList.get(index));
		
	}
	
	public int getGlBCOAListSize(){
		
		return(glBCOAList.size());
		
	}
	
	public void saveGlBCOAList(Object newGlBCOAList){
		
		glBCOAList.add(newGlBCOAList);   	  
		
	}
	
	public void clearGlBCOAList(){
		
		glBCOAList.clear();
		
	}
	
	public void setGlBCOAList(ArrayList glBCOAList) {
		
		this.glBCOAList = glBCOAList;
		
	}
	
	public String getNaturalAcctFrom(){
		
		return naturalAcctFrom;
		
	}
	
	public void setNaturalAcctFrom(String naturalAcctFrom){
		
		this.naturalAcctFrom = naturalAcctFrom;
		
	}
	
	public String getNaturalAcctFromDesc(){
		
		return naturalAcctFromDesc;
		
	}
	
	public void setNaturalAcctFromDesc(String naturalAcctFromDesc){
		
		this.naturalAcctFromDesc = naturalAcctFromDesc;
		
	}
	
	public String getNaturalAcctTo(){
		
		return naturalAcctTo;
		
	}
	
	public void setNaturalAcctTo(String naturalAcctTo){
		
		this.naturalAcctTo = naturalAcctTo;
		
	}
	
	public String getNaturalAcctToDesc(){
		
		return naturalAcctToDesc;
		
	}
	
	public void setNaturalAcctToDesc(String naturalAcctToDesc){
		
		this.naturalAcctToDesc = naturalAcctToDesc;
		
	}
	
	public String getNaturalAcctName(){
		
		return naturalAcctName;
		
	}
	
	public void setNaturalAcctName(String naturalAcctName){
		
		this.naturalAcctName = naturalAcctName;
		
	}
	
	public boolean getShowSegments() {
		
		return showSegments;
		
	}
	
	public void setShowSegments(boolean showSegments) {
		
		this.showSegments = showSegments;
		
	}
	
	   public String getCurrency() {
	   	
	   	   return currency;
	   	
	   }
	   
	   public void setCurrency(String currency) {
	   	
	   	   this.currency = currency;
	   	
	   }
	   
	   public ArrayList getCurrencyList() {
	   	
	   	   return currencyList;
	   	
	   }
	   
	   public void clearCurrencyList() {
	   	
	   	   currencyList.clear();   	   
	   	   currencyList.add(Constants.GLOBAL_BLANK);
		   
	   }
	   
	   public void setCurrencyList(String currency) {
	   	
	   	   currencyList.add(currency);
	   	
	   }
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		effectiveDateFrom = Common.convertSQLDateToString(new java.util.Date());
		effectiveDateTo = null;
		naturalAcctFrom = null;
		naturalAcctFromDesc = null;
		naturalAcctTo = null;
		naturalAcctToDesc = null;
		naturalAcctName = null;
		enabled = false;
		saveButton = null;
		closeButton = null;
		deleteButton = null;
		
		for (int i=0; i<glCoaGenList.size(); i++) {
			
			GlCoaGeneratorList actionList = (GlCoaGeneratorList)glCoaGenList.get(i);
			
			actionList.setSegmentChecked(false);
			
		}
		
		
		for (int i=0; i<glBCOAList.size(); i++) {
			
			GlBranchCoaList list = (GlBranchCoaList)glBCOAList.get(i);
			list.setBranchCheckbox(false);	       
			
		}  
		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("generateButton") != null) {
			
			if(Common.validateRequired(effectiveDateFrom)) {
				errors.add("effectiveDateFrom", new ActionMessage("chartOfAccounts.error.effectiveDateFromRequired"));
			}    
			
			if(!Common.validateDateFormat(effectiveDateFrom)){
				errors.add("effectiveDateFrom", new ActionMessage("chartOfAccounts.error.effectiveDateFromInvalid"));
			}
			
			if(!Common.validateDateFormat(effectiveDateTo)){
				errors.add("effectiveDateTo", new ActionMessage("chartOfAccounts.error.effectiveDateToInvalid"));
			}
			
			if(!Common.validateDateFromTo(effectiveDateFrom, effectiveDateTo)){
				errors.add("effectiveDateFrom", new ActionMessage("chartOfAccounts.error.effectiveDateFromToInvalid"));	
			}
			
			if(!Common.validateDateGreaterThanCurrent(effectiveDateTo)){
				errors.add("effectiveDateTo", new ActionMessage("chartOfAccounts.error.effectiveDateToLessThanCurrent"));
			}
			
			if(Common.validateRequired(naturalAcctFromDesc) || Common.validateRequired(naturalAcctToDesc)) {
				
				if(Common.validateRequired(naturalAcctFromDesc)) naturalAcctFrom = null;
				if(Common.validateRequired(naturalAcctToDesc)) naturalAcctTo = null;
				
				errors.add("naturalAcctToDesc", new ActionMessage("coaGenerator.error.accountFromAccountToError"));
			}
			
		} else if (request.getParameter("deleteButton") != null) {
			
			if(Common.validateRequired(naturalAcctFromDesc) || Common.validateRequired(naturalAcctToDesc)) {
				
				if(Common.validateRequired(naturalAcctFromDesc)) naturalAcctFrom = null;
				if(Common.validateRequired(naturalAcctToDesc)) naturalAcctTo = null;
				
				errors.add("naturalAcctToDesc", new ActionMessage("coaGenerator.error.accountFromAccountToError"));
			}
			
		}
		
		return errors;
		
	}
}