package com.struts.gl.coagenerator;

import java.io.Serializable;

public class GlCoaGeneratorList implements Serializable {

   private Integer index = null;
   private String segment = null;
   private String segmentDescription = null;
   private String segmentTitle = null;
   private short segmentSgNumber = 0;
   private boolean segmentChecked = false;
      
   private GlCoaGeneratorForm parentBean;
    
   public GlCoaGeneratorList(GlCoaGeneratorForm parentBean,
   		Integer index, 
   		String segment,
		String segmentDescription,
		String segmentTitle,
		short segmentSgNumber) {
		
		this.parentBean = parentBean;
		this.index = index;
		this.segment = segment;
		this.segmentDescription = segmentDescription;
		this.segmentTitle = segmentTitle;
		this.segmentSgNumber = segmentSgNumber;
		
   }
   
   public void setColumnSelected(Object selectedGlCOASegList){  
       
       parentBean.setColumnSelected(this, selectedGlCOASegList);
       
    }      

   public Integer getIndex() {
   	
   	  return index;
   	
   }
   
   public void setIndex(Integer index) {
   	
   	  this.index = index;
   	
   }
   
   public String getSegment() {
   	
   	  return segment;
   	
   }
   
   public void setSegment(String segment) {
   	
   	  this.segment = segment;
   	
   }
  
   public String getSegmentDescription() {
   	
   	  return segmentDescription;
   	  
   }
   
   public void setSegmentDescription(String segmentDescription) {
   	
   	  this.segmentDescription = segmentDescription;
   	
   }
   
   public String getSegmentTitle(){
   	
   	  return segmentTitle;
   	  
   }
   
   public void setSegmentTitle(String segmentTitle){
   	
   	  this.segmentTitle = segmentTitle;
   	  
   }
   
   public short getSegmentSgNumber() {
   	
   	  return segmentSgNumber;
   	  
   }
   
   public void setSegmentSgNumber(short segmentSgNumber) {
   	
   	  this.segmentSgNumber = segmentSgNumber;
   	  
   }
   
   public boolean getSegmentChecked() {
   	
   	  return segmentChecked;
   	  
   }
   
   public void setSegmentChecked(boolean segmentChecked) {
   	
   	  this.segmentChecked = segmentChecked;
   	  
   }

}