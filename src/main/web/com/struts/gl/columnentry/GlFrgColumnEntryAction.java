package com.struts.gl.columnentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlFrgCOLColumnNameAlreadyExistException;
import com.ejb.exception.GlFrgCOLSequenceNumberAlreadyExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.GlFrgColumnEntryController;
import com.ejb.txn.GlFrgColumnEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModFrgColumnDetails;
import com.util.GlFrgColumnSetDetails;
import com.util.GlModFunctionalCurrencyDetails;

public final class GlFrgColumnEntryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFrgColumnEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFrgColumnEntryForm actionForm = (GlFrgColumnEntryForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FRG_COLUMN_ENTRY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFrgColumnEntry");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFrgColumnEntryController EJB
*******************************************************/

         GlFrgColumnEntryControllerHome homeCE = null;
         GlFrgColumnEntryController ejbCE = null;

         try {

            homeCE = (GlFrgColumnEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFrgColumnEntryControllerEJB", GlFrgColumnEntryControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFrgColumnEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCE = homeCE.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFrgColumnEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl Frg CE Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFrgColumnEntry"));
	     
/*******************************************************
   -- Gl Frg CE Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFrgColumnEntry"));                  
         
/*******************************************************
   -- Gl Frg CE Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlModFrgColumnDetails mdetails = new GlModFrgColumnDetails();
            mdetails.setColName(actionForm.getName());
            mdetails.setColPosition(Common.convertStringToInt(actionForm.getPosition()));
            mdetails.setColSequenceNumber(Common.convertStringToInt(actionForm.getSequenceNumber()));
            mdetails.setColFormatMask(actionForm.getFormatMask());
            mdetails.setColFactor(actionForm.getFactor());
            mdetails.setColAmountType(actionForm.getAmountType());
            mdetails.setColOffset(Common.convertStringToInt(actionForm.getOffset()));
            mdetails.setColOverrideRowCalculation(Common.convertBooleanToByte(actionForm.getOverrideRowCalculation()));
            mdetails.setColFcName(actionForm.getCurrency());
            
            try {
            	
            	ejbCE.addGlFrgColEntry(mdetails, actionForm.getColumnSetCode(), user.getCmpCode());
                
            } catch (GlFrgCOLColumnNameAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("columnEntry.error.columnNameAlreadyExist"));

            } catch (GlFrgCOLSequenceNumberAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("columnEntry.error.sequenceNumberAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgColumnEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl Frg CE Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl Frg CE Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            GlFrgColumnEntryList glCEList =
	            actionForm.getGlCEByIndex(actionForm.getRowSelected());
                        
            GlModFrgColumnDetails mdetails = new GlModFrgColumnDetails();
            mdetails.setColCode(glCEList.getColumnCode());
            mdetails.setColName(actionForm.getName());
            mdetails.setColPosition(Common.convertStringToInt(actionForm.getPosition()));
            mdetails.setColSequenceNumber(Common.convertStringToInt(actionForm.getSequenceNumber()));
            mdetails.setColFormatMask(actionForm.getFormatMask());
            mdetails.setColFactor(actionForm.getFactor());
            mdetails.setColAmountType(actionForm.getAmountType());
            mdetails.setColOffset(Common.convertStringToInt(actionForm.getOffset()));
            mdetails.setColOverrideRowCalculation(Common.convertBooleanToByte(actionForm.getOverrideRowCalculation()));
            mdetails.setColFcName(actionForm.getCurrency());
               
            try {
            	
            	ejbCE.updateGlFrgColEntry(mdetails, actionForm.getColumnSetCode(), user.getCmpCode());

            } catch (GlFrgCOLColumnNameAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("columnEntry.error.columnNameAlreadyExist"));

            } catch (GlFrgCOLSequenceNumberAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("columnEntry.error.sequenceNumberAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgColumnEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl Frg CE Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl Frg CE Edit Action --
*******************************************************/

         } else if (request.getParameter("glCEList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlCERow(actionForm.getRowSelected());
            
            return mapping.findForward("glFrgColumnEntry");  
           
	        
/*******************************************************
   -- Gl FRG Calculation Action --
*******************************************************/

         } else if (request.getParameter("glCEList[" +
            actionForm.getRowSelected() + "].calculationButton") != null){
            
            GlFrgColumnEntryList glCEList =
	            actionForm.getGlCEByIndex(actionForm.getRowSelected());

	        String path = "/glFrgCalculation.do?forwardcolumnentry=1" +
	           "&columnCode=" + glCEList.getColumnCode() + 
	           "&columnName=" +  glCEList.getName() + 
	           "&columnPosition=" +  glCEList.getPosition() +
	           "&columnSequenceNumber=" +  glCEList.getSequenceNumber() +
	           "&columnFormatMask=" +  glCEList.getFormatMask() +
	           "&columnFactor=" +  glCEList.getFactor() +
	           "&columnAmountType=" +  glCEList.getAmountType() +
	           "&columnOffset=" +  glCEList.getOffset() +
	           "&columnOverrideRowCalculation=" +  glCEList.getOverrideRowCalculation() +
	           "&columnSetCode=" +  actionForm.getColumnSetCode() + 
	           "&columnCurrency=" + actionForm.getCurrency();
	        
	        
	        return(new ActionForward(path));	                           
            
/*******************************************************
   -- Gl Frg CE Delete Action --
*******************************************************/

         } else if (request.getParameter("glCEList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlFrgColumnEntryList glCEList =
	            actionForm.getGlCEByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbCE.deleteGlFrgColEntry(glCEList.getColumnCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("columnEntry.error.columnAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlFrgColumnEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            

/******************************************************
   -- Gl AA Column Set Action --
*******************************************************/

         } else if (request.getParameter("columnSetButton") != null) {
         	        	
         	 String path = "/glFrgColumnSet.do"; 
			     
             return(new ActionForward(path));
            
/*******************************************************
   -- Gl Frg CE Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFrgColumnEntry");

            }
                                  	                        	                          	            	
            GlFrgColumnSetDetails csDetails = null;

	        if (request.getParameter("forward") != null) {

	            csDetails = ejbCE.getGlFrgCsByCsCode(new Integer(request.getParameter("columnSetCode")), user.getCmpCode());        		
	    	    actionForm.setColumnSetCode(new Integer(request.getParameter("columnSetCode")));

	        } else {
	     		
	            csDetails = ejbCE.getGlFrgCsByCsCode(actionForm.getColumnSetCode(), user.getCmpCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setColumnSetCode(actionForm.getColumnSetCode());
            actionForm.setColumnSetName(csDetails.getCsName());
            actionForm.setColumnSetDescription(csDetails.getCsDescription());                        	

            ArrayList list = null;
            Iterator i = null;
            
            

            try {

            	actionForm.clearCurrencyList();

            	list = ejbCE.getGlFcAllWithDefault(user.getCmpCode());

            	actionForm.setCurrency(Constants.GLOBAL_BLANK);

            	if (list == null  || list.size() == 0) {

            		actionForm.setCurrency(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();

            			actionForm.setCurrencyList(mFcDetails.getFcName());

            			if (mFcDetails.getFcSob() == 1) actionForm.setCurrency(mFcDetails.getFcName());

            		}

            	}
            	
            	actionForm.clearGlCEList();

            	list = ejbCE.getGlFrgColByCsCode(actionForm.getColumnSetCode(), user.getCmpCode()); 

            	i = list.iterator();

            	while(i.hasNext()) {

            		GlModFrgColumnDetails mdetails = (GlModFrgColumnDetails)i.next();

            		GlFrgColumnEntryList glCEList = new GlFrgColumnEntryList(actionForm,
            				mdetails.getColCode(),
            				mdetails.getColName(),
            				String.valueOf(mdetails.getColPosition()),
            				String.valueOf(mdetails.getColSequenceNumber()),
            				mdetails.getColFormatMask(),
            				mdetails.getColFactor(),
            				mdetails.getColAmountType(),
            				String.valueOf(mdetails.getColOffset()),
            				Common.convertByteToBoolean(mdetails.getColOverrideRowCalculation()),
            				mdetails.getColFcName());

            		actionForm.saveGlCEList(glCEList);

            	}

            } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgColumnEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glFrgColumnEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFrgColumnEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}