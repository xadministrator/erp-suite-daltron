package com.struts.gl.columnentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFrgColumnEntryForm extends ActionForm implements Serializable {

   private Integer columnSetCode = null;
   private String columnSetName = null;
   private String columnSetDescription = null;
   
   private String name = null;
   private String position = null;
   private String sequenceNumber = null;
   private String formatMask = null;
   private String factor = null;
   private ArrayList factorList = new ArrayList();
   private String amountType = null;
   private ArrayList amountTypeList = new ArrayList();
   private String offset = null;
   private String pageBreakAfter = null;
   private boolean overrideRowCalculation = false;   
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	 
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glCEList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlFrgColumnEntryList getGlCEByIndex(int index) {

      return((GlFrgColumnEntryList)glCEList.get(index));

   }

   public Object[] getGlCEList() {

      return glCEList.toArray();

   }

   public int getGlCEListSize() {

      return glCEList.size();

   }

   public void saveGlCEList(Object newGlCEList) {

      glCEList.add(newGlCEList);

   }

   public void clearGlCEList() {
      glCEList.clear();
   }

   public void setRowSelected(Object selectedGlCEList, boolean isEdit) {

      this.rowSelected = glCEList.indexOf(selectedGlCEList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlCERow(int rowSelected) {
   	
       this.name = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getName();
       this.position = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getPosition();
       this.sequenceNumber = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getSequenceNumber();
       this.formatMask = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getFormatMask();
       this.factor = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getFactor();
       this.amountType = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getAmountType();
       this.offset = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getOffset();
       this.overrideRowCalculation = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getOverrideRowCalculation();
       this.currency = ((GlFrgColumnEntryList)glCEList.get(rowSelected)).getCurrency();
       
   }

   public void updateGlCERow(int rowSelected, Object newGlCEList) {

      glCEList.set(rowSelected, newGlCEList);

   }

   public void deleteGlCEList(int rowSelected) {

      glCEList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getColumnSetCode() {
   	
   	  return columnSetCode;
   	
   }
   
   public void setColumnSetCode(Integer columnSetCode) {
   	
   	  this.columnSetCode = columnSetCode;
   	
   }

   public String getColumnSetName() {

      return columnSetName;

   }

   public void setColumnSetName(String columnSetName) {

      this.columnSetName = columnSetName;

   }
   
   public String getColumnSetDescription() {

      return columnSetDescription;

   }

   public void setColumnSetDescription(String columnSetDescription) {

      this.columnSetDescription = columnSetDescription;

   }   
   
   public String getName() {

      return name;

   }

   public void setName(String name) {

      this.name = name;

   }
   
   public String getPosition() {
   	
   	  return position;
   	  
   }
   
   public void setPosition(String position) {
   	
   	  this.position = position;
   	  
   }
   
   public String getSequenceNumber() {
   	
   	  return sequenceNumber;
   	  
   }
   
   public void setSequenceNumber(String sequenceNumber) {
   	
   	  this.sequenceNumber = sequenceNumber;
   	  
   }
   
   public String getFormatMask() {
   	
   	  return formatMask;
   	  
   }
   
   public void setFormatMask(String formatMask) {
   	
   	  this.formatMask = formatMask;
   	  
   }
   
   public String getFactor() {
   	
   	  return factor;
   	  
   }
   
   public void setFactor(String factor) {
   	
   	  this.factor = factor;
   	  
   }
   
   public ArrayList getFactorList() {
   	
   	   return factorList;
   	
   }    	

   public String getAmountType() {
   	
   	   return amountType;
   	   
   }
   
   public void setAmountType(String amountType) {
   	
   	   this.amountType = amountType;
   	   
   }  
   
   public ArrayList getAmountTypeList() {
   	
   	   return amountTypeList;
   	
   } 
   
   public String getOffset() {
   	
   	   return offset;
   	   
   }
   
   public void setOffset(String offset) {
   	
   	   this.offset = offset;
   	   
   }    
   
   public boolean getOverrideRowCalculation() {

      return overrideRowCalculation;

   }

   public void setOverrideRowCalculation(boolean overrideRowCalculation) {
   	
   	  this.overrideRowCalculation = overrideRowCalculation;

   }   
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public String getCurrency() {

	   return currency;

   }

   public void setCurrency(String currency) {

	   this.currency = currency;

   }

   public ArrayList getCurrencyList() {

	   return currencyList;

   }

   public void clearCurrencyList() {

	   currencyList.clear();

   }

   public void setCurrencyList(String currency) {

	   currencyList.add(currency);

   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

	   name = null;
	   position = null;
	   sequenceNumber = null;
	   formatMask = "#,##0.00;(#,##0.00)";
	   factorList.clear();
	   factorList.add(Constants.GL_FRG_CE_FACTOR_BILLIONS);
	   factorList.add(Constants.GL_FRG_CE_FACTOR_MILLIONS);
	   factorList.add(Constants.GL_FRG_CE_FACTOR_THOUSANDS);
	   factorList.add(Constants.GL_FRG_CE_FACTOR_UNITS);
	   factorList.add(Constants.GL_FRG_CE_FACTOR_PERCENTILES);
	   factor = Constants.GLOBAL_BLANK;
	   amountTypeList.clear();
	   amountTypeList.add("PTD-Actual");
	   amountTypeList.add("QTD-Actual");
	   amountTypeList.add("YTD-Actual");
	   amountTypeList.add("PTD-Budget");
	   amountTypeList.add("QTD-Budget");
	   amountTypeList.add("YTD-Budget");
	   amountType = Constants.GLOBAL_BLANK;
	   offset = "0";
	   overrideRowCalculation = false;
	   saveButton = null;
	   updateButton = null;
	   cancelButton = null;
	   closeButton = null;
	   showDetailsButton = null;
	   hideDetailsButton = null;		

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(name)) {

            errors.add("name",
               new ActionMessage("columnEntry.error.nameRequired"));

         }
         
         if (Common.validateRequired(position)) {

            errors.add("position",
               new ActionMessage("columnEntry.error.positionRequired"));

         }         


         if (!Common.validateNumberFormat(position)) {

            errors.add("position",
               new ActionMessage("columnEntry.error.positionInvalid"));

         }         

         if (Common.validateRequired(sequenceNumber)) {

            errors.add("sequenceNumber",
               new ActionMessage("columnEntry.error.sequenceNumberRequired"));

         }

         if (!Common.validateNumberFormat(sequenceNumber)) {

            errors.add("sequenceNumber",
               new ActionMessage("columnEntry.error.sequenceNumberInvalid"));

         }     
         
         if (!Common.validateNumberFormat(offset)) {

            errors.add("offset",
               new ActionMessage("columnEntry.error.offsetInvalid"));

         }                         
         
		 if(!Common.validateStringNumGreaterThanEqual(position, 350L)) {
		 	
		    errors.add("position", 
		       new ActionMessage("columnEntry.error.positionLessThanAllowedPosition"));
		 
		 }
		 
		 if (Common.validateRequired(currency)) {

			 errors.add("currency",
					 new ActionMessage("columnEntry.error.currencyRequired"));

		 }

      }
         
      return errors;

   }
}