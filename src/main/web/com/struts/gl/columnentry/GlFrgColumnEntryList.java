package com.struts.gl.columnentry;

import java.io.Serializable;

public class GlFrgColumnEntryList implements Serializable {

   private Integer columnCode = null;
   private String name = null;
   private String position = null;
   private String sequenceNumber = null;
   private String formatMask = null;
   private String factor = null;
   private String amountType = null;
   private String offset = null;
   private String pageBreakAfter = null;
   private boolean overrideRowCalculation = false;
   private String currency = null;

   private String editButton = null;
   private String deleteButton = null;
   private String calculationButton = null;
    
   private GlFrgColumnEntryForm parentBean;
    
   public GlFrgColumnEntryList(GlFrgColumnEntryForm parentBean,
		Integer columnCode,
		String name,
		String position,
		String sequenceNumber,
		String formatMask,
		String factor,
		String amountType,
		String offset,
		boolean overrideRowCalculation,
		String currency) {
		
		this.parentBean = parentBean;
		this.columnCode = columnCode;
        this.name = name;
        this.position = position;
        this.sequenceNumber = sequenceNumber;
        this.formatMask = formatMask;
        this.factor = factor;
        this.amountType = amountType;
        this.offset = offset;
        this.overrideRowCalculation = overrideRowCalculation;
        this.currency = currency;
        
   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }     

   public void setCalculationButton(String calculationButton) {

      parentBean.setRowSelected(this, true);

   } 

   public Integer getColumnCode() {
   	
   	 return columnCode;
   	 
   }
   
   public String getName() {
   	
   	 return name;
   	 
   }
   
   public String getPosition() {
   	
   	 return position;
   	 
   }
   
   public String getSequenceNumber() {
   	
   	 return sequenceNumber;
   	 
   }
   
   public String getFormatMask() {
   	
   	 return formatMask;
   	 
   }
   
   public String getFactor() {
   	
   	 return factor;
   	 
   }
   
   public String getAmountType() {
   	
   	 return amountType;
   	 
   }
   
   public String getOffset() {
   	
   	 return offset;
   	 
   }
   
   public boolean getOverrideRowCalculation() {
   	
   	 return overrideRowCalculation;
   	    
   }
   
   public String getCurrency() {

	   return currency;

   }
   
}