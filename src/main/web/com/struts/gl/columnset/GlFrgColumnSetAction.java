package com.struts.gl.columnset;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlFrgColumnSetController;
import com.ejb.txn.GlFrgColumnSetControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFrgColumnSetDetails;

public final class GlFrgColumnSetAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFrgColumnSetAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFrgColumnSetForm actionForm = (GlFrgColumnSetForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FRG_COLUMN_SET_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFrgColumnSet");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFrgColumnSetController EJB
*******************************************************/

         GlFrgColumnSetControllerHome homeCS = null;
         GlFrgColumnSetController ejbCS = null;

         try {

            homeCS = (GlFrgColumnSetControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFrgColumnSetControllerEJB", GlFrgColumnSetControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFrgColumnSetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbCS = homeCS.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFrgColumnSetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl CS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFrgColumnSet"));
	     
/*******************************************************
   -- Gl CS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFrgColumnSet"));                  
         
/*******************************************************
   -- Gl CS Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlFrgColumnSetDetails details = new GlFrgColumnSetDetails();
            details.setCsName(actionForm.getColumnSetName());
            details.setCsDescription(actionForm.getColumnSetDescription());            
            
            try {
            	
            	ejbCS.addGlFrgCsEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("columnSet.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgColumnSetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl CS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl CS Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            GlFrgColumnSetList glCSList =
	            actionForm.getGlCSByIndex(actionForm.getRowSelected());
                        
            GlFrgColumnSetDetails details = new GlFrgColumnSetDetails();
            details.setCsCode(glCSList.getColumnSetCode());
            details.setCsName(actionForm.getColumnSetName());
            details.setCsDescription(actionForm.getColumnSetDescription());            
            
            try {
            	
            	ejbCS.updateGlFrgCsEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("columnSet.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgColumnSetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl CS Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl CS Edit Action --
*******************************************************/

         } else if (request.getParameter("glCSList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlCSRow(actionForm.getRowSelected());
            
            return mapping.findForward("glFrgColumnSet");
            
/*******************************************************
   -- Gl FRG Columns Action --
*******************************************************/

         } else if (request.getParameter("glCSList[" +
            actionForm.getRowSelected() + "].columnsButton") != null){
            
            GlFrgColumnSetList glCSList =
	            actionForm.getGlCSByIndex(actionForm.getRowSelected());

	        String path = "/glFrgColumnEntry.do?forward=1" +
	           "&columnSetCode=" + glCSList.getColumnSetCode() + 
	           "&columnSetName=" +  glCSList.getColumnSetName() + 
	           "&columnSetDescription=" + glCSList.getColumnSetDescription(); 
	        
	        return(new ActionForward(path));            
            
/*******************************************************
   -- Gl CS Delete Action --
*******************************************************/

         } else if (request.getParameter("glCSList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlFrgColumnSetList glCSList =
	            actionForm.getGlCSByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbCS.deleteGlFrgCsEntry(glCSList.getColumnSetCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("columnSet.error.deleteColumnSetAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("columnSet.error.columnSetlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlFrgColumnSetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Gl CS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFrgColumnSet");

            }
            
            ArrayList list = null;
            Iterator i = null;

            actionForm.clearGlCSList(); 
                      	                        	                          	            	
	        try {	        
	           
	           list = ejbCS.getGlFrgCsAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
    	            			            	
	              GlFrgColumnSetDetails details = (GlFrgColumnSetDetails)i.next();
	            	
	              GlFrgColumnSetList glCSList = new GlFrgColumnSetList(actionForm,
	            	    details.getCsCode(),
	            	    details.getCsName(),
	            	    details.getCsDescription());
	            	 	            	    
	              actionForm.saveGlCSList(glCSList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
            
           if (log.isInfoEnabled()) {

              log.info("EJBException caught in GlFrgColumnSetAction.execute(): " + ex.getMessage() +
              " session: " + session.getId());
              return mapping.findForward("cmnErrorPage"); 
              
           }

        }
                  

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glFrgColumnSet"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFrgColumnSetAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}