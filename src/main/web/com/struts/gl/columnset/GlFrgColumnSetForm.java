package com.struts.gl.columnset;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFrgColumnSetForm extends ActionForm implements Serializable {

   private Integer columnSetCode = null;
   private String columnSetName = null;
   private String columnSetDescription = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;   
   private String pageState = new String();
   private ArrayList glCSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlFrgColumnSetList getGlCSByIndex(int index) {

      return((GlFrgColumnSetList)glCSList.get(index));

   }

   public Object[] getGlCSList() {

      return glCSList.toArray();

   }

   public int getGlCSListSize() {

      return glCSList.size();

   }

   public void saveGlCSList(Object newGlCSList) {

      glCSList.add(newGlCSList);

   }

   public void clearGlCSList() {
      glCSList.clear();
   }

   public void setRowSelected(Object selectedGlCSList, boolean isEdit) {

      this.rowSelected = glCSList.indexOf(selectedGlCSList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlCSRow(int rowSelected) {
   	
       this.columnSetName = ((GlFrgColumnSetList)glCSList.get(rowSelected)).getColumnSetName();
       this.columnSetDescription = ((GlFrgColumnSetList)glCSList.get(rowSelected)).getColumnSetDescription();
       
   }

   public void updateGlCSRow(int rowSelected, Object newGlCSList) {

      glCSList.set(rowSelected, newGlCSList);

   }

   public void deleteGlCSList(int rowSelected) {

      glCSList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getColumnSetCode() {
   	
   	  return columnSetCode;
   	
   }
   
   public void setColumnSetCode(Integer columnSetCode) {
   	
   	  this.columnSetCode = columnSetCode;
   	
   }

   public String getColumnSetName() {

      return columnSetName;

   }

   public void setColumnSetName(String columnSetName) {

      this.columnSetName = columnSetName;

   }
   
   public String getColumnSetDescription() {

      return columnSetDescription;

   }

   public void setColumnSetDescription(String columnSetDescription) {

      this.columnSetDescription = columnSetDescription;

   } 
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      columnSetName = null; 
      columnSetDescription = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(columnSetName)) {

            errors.add("columnSetName",
               new ActionMessage("columnSet.error.columnSetNameRequired"));

         }   

      }
         
      return errors;

   }
}