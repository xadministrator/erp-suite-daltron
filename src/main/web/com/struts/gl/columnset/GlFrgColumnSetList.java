package com.struts.gl.columnset;

import java.io.Serializable;

public class GlFrgColumnSetList implements Serializable {

   private Integer columnSetCode = null;
   private String columnSetName = null;
   private String columnSetDescription = null;
   
   private String editButton = null;
   private String deleteButton = null;
   private String columnsButton = null;
    
   private GlFrgColumnSetForm parentBean;
    
   public GlFrgColumnSetList(GlFrgColumnSetForm parentBean,
      Integer columnSetCode,
      String columnSetName,
      String columnSetDescription) {

      this.parentBean = parentBean;
      this.columnSetCode = columnSetCode;
      this.columnSetName = columnSetName;
      this.columnSetDescription = columnSetDescription;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   
   
   public void setColumnsButton(String columnsButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }

   public Integer getColumnSetCode() {

      return columnSetCode;

   }
    
   public String getColumnSetName() {
   	
   	 return columnSetName;
   	 
   }
   
   public String getColumnSetDescription() {
   	
   	 return columnSetDescription;
   	 
   } 
}