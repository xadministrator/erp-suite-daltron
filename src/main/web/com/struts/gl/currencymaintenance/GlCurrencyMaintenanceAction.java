package com.struts.gl.currencymaintenance;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyDeletedException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyExistException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.txn.GlCurrencyMaintenanceController;
import com.ejb.txn.GlCurrencyMaintenanceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFunctionalCurrencyDetails;

public final class GlCurrencyMaintenanceAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlCurrencyMaintenanceAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         String frParam = Common.getUserPermission(user, Constants.GL_CURRENCY_MAINTENANCE_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((GlCurrencyMaintenanceForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glCurrencyMaintenance"));
               }
            }
            ((GlCurrencyMaintenanceForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlCurrencyMaintenanceForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlCurrencyMaintenanceController EJB
*******************************************************/

         GlCurrencyMaintenanceControllerHome homeCM = null;
         GlCurrencyMaintenanceController ejbCM = null;

         try{
         	
            homeCM = (GlCurrencyMaintenanceControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlCurrencyMaintenanceControllerEJB", GlCurrencyMaintenanceControllerHome.class);
                
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlCurrencyMaintenanceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbCM = homeCM.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlCurrencyMaintenanceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl CM Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlCurrencyMaintenanceForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glCurrencyMaintenance"));
	     
/*******************************************************
   -- Gl CM Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlCurrencyMaintenanceForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glCurrencyMaintenance"));                  

/*******************************************************
   -- Gl  CM Save Action --
*******************************************************/

         } else if(request.getParameter("saveButton") != null &&
            ((GlCurrencyMaintenanceForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	    GlFunctionalCurrencyDetails details = new GlFunctionalCurrencyDetails(
	       ((GlCurrencyMaintenanceForm)form).getCurrencyName().trim().toUpperCase(),
	       ((GlCurrencyMaintenanceForm)form).getDescription().trim().toUpperCase(),
	       ((GlCurrencyMaintenanceForm)form).getCountry(),
	       Common.convertStringToChar(((GlCurrencyMaintenanceForm)form).getSymbol().trim()),
	       Common.convertStringToShort(((GlCurrencyMaintenanceForm)form).getPrecision()),
	       Common.convertStringToShort(((GlCurrencyMaintenanceForm)form).getExtendedPrecision()),
	       Common.convertStringMoneyToDouble(((GlCurrencyMaintenanceForm)form).getMinimumAccountableUnit(), 
	          Constants.DEFAULT_PRECISION),
	       Common.convertStringToSQLDate(((GlCurrencyMaintenanceForm)form).getEffectiveDateFrom()),
	       Common.convertStringToSQLDate(((GlCurrencyMaintenanceForm)form).getEffectiveDateTo()),
	       Common.convertBooleanToByte(((GlCurrencyMaintenanceForm)form).getEnabled()));

/*******************************************************
   Call GlCurrencyMaintenanceController EJB
   addGlFcEntry method
*******************************************************/

             try{
	        ejbCM.addGlFcEntry(details, user.getCmpCode());
	     }catch(GlFCFunctionalCurrencyAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("currencyMaintenance.error.currencyNameAlreadyExist"));
             }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlCurrencyMaintenanceAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                }
                return(mapping.findForward("cmnErrorPage"));
             }

/*******************************************************
   -- Gl  CM Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));
/*******************************************************
   -- Gl  CM Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
            ((GlCurrencyMaintenanceForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	    GlCurrencyMaintenanceList glCMList =
               ((GlCurrencyMaintenanceForm)form).getGlCMByIndex(((GlCurrencyMaintenanceForm) form).getRowSelected());

	    GlFunctionalCurrencyDetails details = new GlFunctionalCurrencyDetails(
	       glCMList.getCurrencyCode(),
               ((GlCurrencyMaintenanceForm)form).getCurrencyName().trim().toUpperCase(),
               ((GlCurrencyMaintenanceForm)form).getDescription().trim().toUpperCase(),
               ((GlCurrencyMaintenanceForm)form).getCountry(),
               Common.convertStringToChar(((GlCurrencyMaintenanceForm)form).getSymbol().trim()),
               Common.convertStringToShort(((GlCurrencyMaintenanceForm)form).getPrecision()),
               Common.convertStringToShort(((GlCurrencyMaintenanceForm)form).getExtendedPrecision()),
               Common.convertStringMoneyToDouble(((GlCurrencyMaintenanceForm)form).getMinimumAccountableUnit(),
	          Constants.DEFAULT_PRECISION),
               Common.convertStringToSQLDate(((GlCurrencyMaintenanceForm)form).getEffectiveDateFrom()),
               Common.convertStringToSQLDate(((GlCurrencyMaintenanceForm)form).getEffectiveDateTo()),
               Common.convertBooleanToByte(((GlCurrencyMaintenanceForm)form).getEnabled()));
			    
/*******************************************************
   Call GlCurrencyMaintenanceController EJB
   updateGlFcEntry method
*******************************************************/

             try{
                ejbCM.updateGlFcEntry(details, user.getCmpCode());
             }catch(GlFCFunctionalCurrencyAlreadyExistException ex){
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("currencyMaintenance.error.currencyNameAlreadyExist"));
             }catch(GlFCFunctionalCurrencyAlreadyAssignedException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
		   new ActionMessage("currencyMaintenance.error.updateCurrencyAlreadyAssigned"));
	     }catch(GlFCFunctionalCurrencyAlreadyDeletedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("currencyMaintenance.error.currencyAlreadyDeleted"));
             }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlCurrencyMaintenanceAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                }
                return(mapping.findForward("cmnErrorPage"));
             }


/*******************************************************
   -- Gl  CM Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl  CM Edit Action --
*******************************************************/

         }else if(request.getParameter("glCMList[" + 
            ((GlCurrencyMaintenanceForm)form).getRowSelected() + "].editButton") != null &&
            ((GlCurrencyMaintenanceForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlCurrencyMaintenanceForm properties
*******************************************************/

            ((GlCurrencyMaintenanceForm)form).showGlCMRow(((GlCurrencyMaintenanceForm) form).getRowSelected());
            return(mapping.findForward("glCurrencyMaintenance"));
/*******************************************************
   -- Gl  CM Delete Action --
*******************************************************/

         }else if(request.getParameter("glCMList[" +
            ((GlCurrencyMaintenanceForm)form).getRowSelected() + "].deleteButton") != null &&
            ((GlCurrencyMaintenanceForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            GlCurrencyMaintenanceList glCMList =
              ((GlCurrencyMaintenanceForm)form).getGlCMByIndex(((GlCurrencyMaintenanceForm) form).getRowSelected());

/*******************************************************
   Call GlCurrencyMaintenanceController EJB
   deleteGlFcEntry method
*******************************************************/

            try{
               ejbCM.deleteGlFcEntry(glCMList.getCurrencyCode(), user.getCmpCode());
            }catch(GlFCFunctionalCurrencyAlreadyAssignedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("currencyMaintenance.error.deleteCurrencyAlreadyAssigned"));
            }catch(GlFCFunctionalCurrencyAlreadyDeletedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("currencyMaintenance.error.currencyAlreadyDeleted"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlCurrencyMaintenanceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

/*******************************************************
   -- Gl  CM Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glCurrencyMaintenance"));
            }

            ((GlCurrencyMaintenanceForm)form).clearGlCMList();

/*******************************************************
   Call GlCurrencyMaintenanceController EJB getGlFcAll() method
   Populate ArrayList property from GlCurrencyMaintenanceController
   EJB getGlFcAll() method ArrayList return
*******************************************************/

            try{
               ArrayList ejbGlCMList = ejbCM.getGlFcAll(user.getCmpCode());
               Iterator i = ejbGlCMList.iterator();
               while(i.hasNext()){
                  GlFunctionalCurrencyDetails details = (GlFunctionalCurrencyDetails)i.next();
                  GlCurrencyMaintenanceList glCMList = new GlCurrencyMaintenanceList(((GlCurrencyMaintenanceForm)form),
                  details.getFcCode(),
                  details.getFcName(),
		  details.getFcDescription(),
		  details.getFcCountry(),
		  Common.convertCharToString(details.getFcSymbol()),
		  Common.convertShortToString(details.getFcPrecision()),
		  Common.convertShortToString(details.getFcExtendedPrecision()),
		  Common.convertDoubleToStringMoney(details.getFcMinimumAccountUnit(), Constants.DEFAULT_PRECISION),
		  Common.convertSQLDateToString(details.getFcDateFrom()),
		  Common.convertSQLDateToString(details.getFcDateTo()),
		  Common.convertByteToBoolean(details.getFcEnable()));
		  
                  ((GlCurrencyMaintenanceForm) form).saveGlCMList(glCMList);
              }
           }catch(GlFCNoFunctionalCurrencyFoundException ex){
              
           }catch(EJBException ex){
              if(log.isInfoEnabled()){
                 log.info("EJBException caught in GlCurrencyMaintenanceAction.execute(): " + ex.getMessage() +
                 " session: " + session.getId());
              }
	      return(mapping.findForward("cmnErrorPage"));
	   }

            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
            }else{
               if((request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null || 
                   request.getParameter("glCMList[" + 
                   ((GlCurrencyMaintenanceForm) form).getRowSelected() + "].deleteButton") != null) && 
                   ((GlCurrencyMaintenanceForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                   ((GlCurrencyMaintenanceForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
               }
            }

            ((GlCurrencyMaintenanceForm)form).reset(mapping, request);
            
	        if (((GlCurrencyMaintenanceForm)form).getTableType() == null) {
      		      	
	           ((GlCurrencyMaintenanceForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	      
                             
            ((GlCurrencyMaintenanceForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            ((GlCurrencyMaintenanceForm)form).setEnabled(true);
            return(mapping.findForward("glCurrencyMaintenance"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()){
             log.info("Exception caught in GlCurrencyMaintenanceAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
