package com.struts.gl.currencymaintenance;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlCurrencyMaintenanceForm extends ActionForm implements Serializable {

   private String currencyName = null;
   private String description = null;
   private String country = null;
   private ArrayList countryList = new ArrayList();
   private String symbol = null;
   private String precision = null;
   private ArrayList precisionList = new ArrayList();
   private String extendedPrecision = null;
   private ArrayList extendedPrecisionList = new ArrayList();
   private String minimumAccountableUnit = null;
   private String effectiveDateFrom = null;
   private String effectiveDateTo = null;
   private boolean enabled = false;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glCMList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   public int getRowSelected(){
      return rowSelected;
   }

   public GlCurrencyMaintenanceList getGlCMByIndex(int index){
      return((GlCurrencyMaintenanceList)glCMList.get(index));
   }

   public Object[] getGlCMList(){
      return(glCMList.toArray());
   }

   public int getGlCMListSize(){
      return(glCMList.size());
   }

   public void saveGlCMList(Object newGlCMList){
      glCMList.add(newGlCMList);
   }

   public void clearGlCMList(){
      glCMList.clear();
   }

   public void setRowSelected(Object selectedGlCMList, boolean isEdit){
      this.rowSelected = glCMList.indexOf(selectedGlCMList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showGlCMRow(int rowSelected){
      this.currencyName = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getCurrencyName();
      this.description = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getDescription();
      this.country = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getCountry();
      this.symbol = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getSymbol();
      this.precision = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getPrecision();
      this.extendedPrecision = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getExtendedPrecision();
      this.minimumAccountableUnit = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getMinimumAccountableUnit();
      this.effectiveDateFrom = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getEffectiveDateFrom();
      this.effectiveDateTo = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getEffectiveDateTo();
      this.enabled = ((GlCurrencyMaintenanceList)glCMList.get(rowSelected)).getEnabled();
   }

   public void updateGlCMRow(int rowSelected, Object newGlCMList){
      glCMList.set(rowSelected, newGlCMList);
   }

   public void deleteGlCMList(int rowSelected){
      glCMList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getCurrencyName(){
      return(currencyName);
   }

   public void setCurrencyName(String currencyName){
      this.currencyName = currencyName;
   }

   public String getDescription(){
      return(description);
   }

   public void setDescription(String description){
      this.description = description;
   }

   public String getCountry(){
      return(country);
   }

   public void setCountry(String country){
      this.country = country;
   }

   public ArrayList getCountryList(){
      return(countryList);
   }

   public String getSymbol(){
      return(symbol);
   }

   public void setSymbol(String symbol){
      this.symbol = symbol;
   }

   public String getPrecision(){
      return(precision);
   }

   public void setPrecision(String precision){
      this.precision = precision;
   }

   public ArrayList getPrecisionList(){
      return(precisionList);
   }

   public String getExtendedPrecision(){
      return(extendedPrecision);
   }

   public void setExtendedPrecision(String extendedPrecision){
      this.extendedPrecision = extendedPrecision;
   }

   public ArrayList getExtendedPrecisionList(){
      return(extendedPrecisionList);
   }

   public String getMinimumAccountableUnit(){
      return(minimumAccountableUnit);
   }

   public void setMinimumAccountableUnit(String minimumAccountableUnit){
      this.minimumAccountableUnit = minimumAccountableUnit;
   }

   public String getEffectiveDateFrom(){
      return(effectiveDateFrom);
   }

   public void setEffectiveDateFrom(String effectiveDateFrom){
      this.effectiveDateFrom = effectiveDateFrom;
   }

   public String getEffectiveDateTo(){
      return(effectiveDateTo);
   }

   public void setEffectiveDateTo(String effectiveDateTo){
      this.effectiveDateTo = effectiveDateTo;
   }

   public boolean getEnabled(){
      return(enabled);
   }

   public void setEnabled(boolean enabled){
      this.enabled = enabled;
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request){
      currencyName = null;
      description = null;
      country = null;
      countryList.clear();      
      countryList.add("AFGHANISTAN");
	  countryList.add("ALBANIA");
	  countryList.add("ALGERIA");
	  countryList.add("AMERICAN SAMOA");
	  countryList.add("ANDORRA");
	  countryList.add("ANGOLA");
	  countryList.add("ANGUILLA");
	  countryList.add("ANTARCTICA");
	  countryList.add("ANTIGUA");
	  countryList.add("ARGENTINA");
	  countryList.add("ARMENIA");
	  countryList.add("ARUBA");
	  countryList.add("AUSTRALIA");
	  countryList.add("AUSTRIA");
	  countryList.add("AZERBAIJAN");
	  countryList.add("BAHAMAS");
	  countryList.add("BAHRAIN");
	  countryList.add("BANGLADESH");
	  countryList.add("BARBADOS");
	  countryList.add("BELARUS");
	  countryList.add("BELGIUM");
	  countryList.add("BELIZE");
	  countryList.add("BENIN");
	  countryList.add("BERMUDA");
	  countryList.add("BHUTAN");
	  countryList.add("BOLIVIA");
	  countryList.add("BOSNIA AND HERZEGOVINA");
	  countryList.add("BOTSWANA");
	  countryList.add("BRAZIL");
	  countryList.add("BRITISH VIRGIN ISLANDS");
	  countryList.add("BRUNEI");
	  countryList.add("BULGARIA");
	  countryList.add("BURKINA FASO");
	  countryList.add("BURMA");
	  countryList.add("BURUNDI");
	  countryList.add("CAMBODIA");
	  countryList.add("CAMEROON");
	  countryList.add("CANADA");
	  countryList.add("CAPE VERDE");
	  countryList.add("CENTRAL AFRICAN REPUBLIC");
	  countryList.add("CHAD");
	  countryList.add("CHILE");
	  countryList.add("CHINA");
	  countryList.add("COLOMBIA");
	  countryList.add("COMOROS");
	  countryList.add("CONGO (ZAIRE)");
	  countryList.add("CONGO");
	  countryList.add("COOK ISLANDS");
	  countryList.add("COSTA RICA");
	  countryList.add("COTE D'IVOIRE");
	  countryList.add("CROATIA");
	  countryList.add("CUBA");
	  countryList.add("CYPRUS");
	  countryList.add("CZECH REPUBLIC");
	  countryList.add("DENMARK");
	  countryList.add("DJIBOUTI");
	  countryList.add("DOMINICA");
	  countryList.add("DOMINICAN REPUBLIC");
	  countryList.add("ECUADOR");
	  countryList.add("EGYPT");
	  countryList.add("EL SALVADOR");
	  countryList.add("EQUATORIAL GUINEA");
	  countryList.add("ERITREA");
	  countryList.add("ESTONIA");
	  countryList.add("ETHIOPIA");
	  countryList.add("FALKLAND ISLANDS");
	  countryList.add("FIJI");
	  countryList.add("FINLAND");
	  countryList.add("FRANCE");
	  countryList.add("FRENCH GUIANA");
	  countryList.add("FRENCH POLYNESIA");
	  countryList.add("GABON");
	  countryList.add("THE GAMBIA");
	  countryList.add("GAZA STRIP AND WEST BANK");
	  countryList.add("GEORGIA");
	  countryList.add("GERMANY");
	  countryList.add("GHANA");
	  countryList.add("GIBRALTAR");
	  countryList.add("GREECE");
	  countryList.add("GREENLAND");
	  countryList.add("GRENADA");
  	  countryList.add("GUADELOUPE");
	  countryList.add("GUAM");
	countryList.add("GUATEMALA");
	countryList.add("GUINEA");
	countryList.add("GUINEA-BISSAU");
	countryList.add("GUYANA");
	countryList.add("HAITI");
	countryList.add("THE HOLY SEE");
	countryList.add("HONDURAS");
	countryList.add("HONG KONG");
	countryList.add("HUNGARY");
	countryList.add("ICELAND");
	countryList.add("INDIA");
	countryList.add("INDONESIA");
	countryList.add("IRAN");
	countryList.add("IRAQ");
	countryList.add("IRELAND");
	countryList.add("ISRAEL");
	countryList.add("ITALY");
	countryList.add("IVORY COAST");
	countryList.add("JAMAICA");
	countryList.add("JAPAN");
	countryList.add("JORDAN");
	countryList.add("KAZAKHSTAN");
	countryList.add("KENYA");
	countryList.add("KIRIBATI");
	countryList.add("KUWAIT");
	countryList.add("KYRGYZSTAN");
	countryList.add("LAOS");
	countryList.add("LATVIA");
	countryList.add("LEBANON");
	countryList.add("LESOTHO");
	countryList.add("LIBERIA");
	countryList.add("LIBYA");
	countryList.add("LIECHTENSTEIN");
	countryList.add("LITHUANIA");
	countryList.add("LUXEMBOURG");
	countryList.add("MACAU");
	countryList.add("MACEDONIA");
	countryList.add("MADAGASCAR");
	countryList.add("MALAWI");
	countryList.add("MALAYSIA");
	countryList.add("MALDIVES");
	countryList.add("MALI");
	countryList.add("MALTA");
	countryList.add("MARSHALL ISLANDS");
	countryList.add("MARTINIQUE");
	countryList.add("MAURITANIA");
	countryList.add("MAURITIUS");
	countryList.add("MAYOTTE");
	countryList.add("MEXICO");
	countryList.add("FEDERATED STATES OF MICRONESIA");
	countryList.add("MOLDOVA");
	countryList.add("MONACO");
	countryList.add("MONGOLIA");
	countryList.add("MONTSERRAT");
	countryList.add("MOROCCO");
	countryList.add("MOZAMBIQUE");
	countryList.add("NAMIBIA");
	countryList.add("NAURU");
	countryList.add("NEPAL");
	countryList.add("NETHERLANDS");
	countryList.add("NETHERLANDS ANTILLES");
	countryList.add("NEW CALEDONIA");
	countryList.add("NEW ZEALAND");
	countryList.add("NICARAGUA");
	countryList.add("NIGER");
	countryList.add("NIGERIA");
	countryList.add("NORTH KOREA");
	countryList.add("NORTHERN MARIANA ISLANDS");
	countryList.add("NORWAY");
	countryList.add("OMAN");
	countryList.add("PAKISTAN");
	countryList.add("PALAU");
	countryList.add("PANAMA");
	countryList.add("PAPUA NEW GUINEA");
	countryList.add("PARAGUAY");
	countryList.add("PERU");
	countryList.add("PHILIPPINES");
	countryList.add("PITCAIRN ISLANDS");
	countryList.add("POLAND");
	countryList.add("PORTUGAL");
	countryList.add("PUERTO RICO");
	countryList.add("QATAR");
	countryList.add("REUNION");
	countryList.add("ROMANIA");
	countryList.add("RUSSIA");
	countryList.add("RWANDA");
	countryList.add("SAINT KITTS AND NEVIS");
	countryList.add("SAINT LUCIA");
	countryList.add("SAINT PIERRE AND MIQUELON");
	countryList.add("SAINT VINCENT AND THE GRENADINES");
	countryList.add("SAMOA");
	countryList.add("SAN MARINO");
	countryList.add("SAO TOME AND PRINCIPE");
	countryList.add("SAUDI ARABIA");
	countryList.add("SENEGAL");
	countryList.add("SERBIA AND MONTENEGRO");
	countryList.add("SEYCHELLES");
	countryList.add("SIERRA LEONE");
	countryList.add("SINGAPORE");
	countryList.add("SLOVAKIA");
	countryList.add("SLOVENIA");
	countryList.add("SOLOMON ISLANDS");
	countryList.add("SOMALIA");
	countryList.add("SOUTH AFRICA");
	countryList.add("SOUTH KOREA");
	countryList.add("SPAIN");
	countryList.add("SRI LANKA");
	countryList.add("SUDAN");
	countryList.add("SURINAME");
	countryList.add("SWAZILAND");
	countryList.add("SWEDEN");
	countryList.add("SWITZERLAND");
	countryList.add("SYRIA");
	countryList.add("TAIWAN");
	countryList.add("TAJIKISTAN");
	countryList.add("TANZANIA");
	countryList.add("THAILAND");
	countryList.add("TOGO");
	countryList.add("TONGA");
	countryList.add("TRINIDAD AND TOBAGO");
	countryList.add("TUNISIA");
	countryList.add("TURKEY");
	countryList.add("TURKMENISTAN");
	countryList.add("TURKS AND CAICOS ISLANDS");
	countryList.add("TUVALU");
	countryList.add("UGANDA");
	countryList.add("UKRAINE");
	countryList.add("UNITED ARAB EMIRATES");
	countryList.add("UNITED KINGDOM");
	countryList.add("UNITED STATES");
	countryList.add("UNITED STATES VIRGIN ISLANDS");
	countryList.add("URUGUAY");
	countryList.add("UZBEKISTAN");
	countryList.add("VANUATU");
	countryList.add("VENEZUELA");
	countryList.add("VIETNAM");
	countryList.add("WEST BANK AND GAZA STRIP");
	countryList.add("WESTERN SAHARA");
	countryList.add("YEMEN");
	countryList.add("YUGOSLAVIA");
	countryList.add("ZAIRE");
	countryList.add("ZAMBIA");
	countryList.add("ZIMBABWE");
    country = "PHILIPPINES";
      symbol = null;
      precision = null;
      extendedPrecision = null;
      extendedPrecisionList.clear();
      precisionList.clear();
      for(int i=2; i<7; i++){
         precisionList.add(String.valueOf(i));
	 	 extendedPrecisionList.add(String.valueOf(i));
      }
      precision = "2";
      extendedPrecision = "2";
      minimumAccountableUnit = null;
      effectiveDateFrom = null;
      effectiveDateTo = null;
      enabled = false;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
      showDetailsButton = null;
	  hideDetailsButton = null;
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(currencyName)){
            errors.add("currencyName",
               new ActionMessage("currencyMaintenance.error.currencyNameRequired"));
         }
         if(Common.validateRequired(symbol)){
            errors.add("symbol",
               new ActionMessage("currencyMaintenance.error.symbolRequired"));
         }
         if(Common.validateRequired(country)){
            errors.add("country",
               new ActionMessage("currencyMaintenance.error.countryRequired"));
         }
         if(!Common.validateStringExists(countryList, country)){
            errors.add("country",
               new ActionMessage("currencyMaintenance.error.countryInvalid"));
         }
         if(Common.validateRequired(precision)){
            errors.add("precision",
               new ActionMessage("currencyMaintenance.error.precisionRequired"));
         }
         if(!Common.validateStringExists(precisionList, precision)){
            errors.add("precision",
               new ActionMessage("currencyMaintenance.error.precisionInvalid"));
         }
         if(Common.validateRequired(extendedPrecision)){
            errors.add("extendedPrecision",
               new ActionMessage("currencyMaintenance.error.extendedPrecisionRequired"));
         }
         if(!Common.validateStringExists(extendedPrecisionList, extendedPrecision)){
            errors.add("extendedPrecision",
               new ActionMessage("currencyMaintenance.error.extendedPrecisionInvalid"));
         }
         if(Common.convertStringToShort(precision) > Common.convertStringToShort(extendedPrecision)){
	    errors.add("precision",
	       new ActionMessage("currencyMaintenance.error.precisionGreaterThanExtendedPrecision"));
	 }
	 if(Common.validateRequired(minimumAccountableUnit)){
            errors.add("minimumAccountableUnit",
               new ActionMessage("currencyMaintenance.error.minimumAccountableUnitRequired"));
         }
         if(Common.validateRequired(effectiveDateFrom)){
            errors.add("effectiveDateFrom",
               new ActionMessage("currencyMaintenance.error.effectiveDateFromRequired"));
         }
	 if(!Common.validateDateFormat(effectiveDateFrom)){
            errors.add("effectiveDateFrom", new ActionMessage("currencyMaintenance.error.effectiveDateFromInvalid"));
         }
         if(!Common.validateDateFormat(effectiveDateTo)){
            errors.add("effectiveDateTo", new ActionMessage("currencyMaintenance.error.effectiveDateToInvalid"));
         }
         if(!Common.validateDateFromTo(effectiveDateFrom, effectiveDateTo)){
            errors.add("effectiveDateFrom", new ActionMessage("currencyMaintenance.error.effectiveDateFromToInvalid"));
         }
         if(!Common.validateDateGreaterThanCurrent(effectiveDateTo)){
            errors.add("effectiveDateTo", new ActionMessage("currencyMaintenance.error.effectiveDateToLessThanCurrent"));
         }
      }
      return(errors);
   }
}
