package com.struts.gl.currencymaintenance;

import java.io.Serializable;

public class GlCurrencyMaintenanceList implements Serializable {

   private Integer currencyCode = null;
   private String currencyName = null;
   private String description = null;
   private String country = null;
   private String symbol = null;
   private String precision = null;
   private String extendedPrecision = null;
   private String minimumAccountableUnit = null;
   private String effectiveDateFrom = null;
   private String effectiveDateTo = null;
   private boolean enabled = false;

   private String deleteButton = null;
   private String editButton = null;
    
   private GlCurrencyMaintenanceForm parentBean;
    
   public GlCurrencyMaintenanceList(GlCurrencyMaintenanceForm parentBean,
      Integer currencyCode,
      String currencyName,
      String description,
      String country,
      String symbol,
      String precision,
      String extendedPrecision,
      String minimumAccountableUnit,
      String effectiveDateFrom,
      String effectiveDateTo,
      boolean enabled){

      this.parentBean = parentBean;
      this.currencyCode = currencyCode;
      this.currencyName = currencyName;
      this.description = description;
      this.country = country;
      this.symbol = symbol;
      this.precision = precision;
      this.extendedPrecision = extendedPrecision;
      this.minimumAccountableUnit = minimumAccountableUnit;
      this.effectiveDateFrom = effectiveDateFrom;
      this.effectiveDateTo = effectiveDateTo;
      this.enabled = enabled;
   }

   public void setDeleteButton(String deleteButton){
      parentBean.setRowSelected(this, false);
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public Integer getCurrencyCode(){
      return(currencyCode);
   }

   public String getCurrencyName(){
      return(currencyName);
   }

   public String getDescription(){
      return(description);
   }

   public String getCountry(){
      return(country);
   }

   public String getSymbol(){
      return(symbol);
   }

   public String getPrecision(){
      return(precision);
   }

   public String getExtendedPrecision(){
      return(extendedPrecision);
   }

   public String getMinimumAccountableUnit(){
      return(minimumAccountableUnit);
   }

   public String getEffectiveDateFrom(){
      return(effectiveDateFrom);
   }

   public String getEffectiveDateTo(){
      return(effectiveDateTo);
   }

   public boolean getEnabled(){
      return(enabled);
   }

}