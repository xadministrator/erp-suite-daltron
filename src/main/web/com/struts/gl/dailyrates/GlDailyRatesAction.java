package com.struts.gl.dailyrates;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlFCRFunctionalCurrencyRateAlreadyDeletedException;
import com.ejb.exception.GlFCRFunctionalCurrencyRateAlreadyExistException;
import com.ejb.exception.GlFCRNoFunctionalCurrencyRateFoundException;
import com.ejb.txn.GlDailyRateController;
import com.ejb.txn.GlDailyRateControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFunctionalCurrencyDetails;
import com.util.GlFunctionalCurrencyRateDetails;
import com.util.GlModFunctionalCurrencyRateDetails;

public final class GlDailyRatesAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
      
      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlDailyRatesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }
            
         } else {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlDailyRatesForm actionForm = (GlDailyRatesForm)form;
      
         String frParam = Common.getUserPermission(user, Constants.GL_DAILY_RATES_ID);
         
         if (frParam != null) {
         	
	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {
               	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glDailyRates"));
                  
               }
            }
            
            actionForm.setUserPermission(frParam.trim());
            
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize GlDailyRateController EJB
*******************************************************/

         GlDailyRateControllerHome homeDR = null;
         GlDailyRateController ejbDR = null;

         try{
         	
            homeDR = (GlDailyRateControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlDailyRateControllerEJB", GlDailyRateControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlDailyRatesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbDR = homeDR.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlDailyRatesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Gl  DR Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	
            	
            if (actionForm.getCurrencyRateCode() == null) {

	    		GlFunctionalCurrencyRateDetails details = new GlFunctionalCurrencyRateDetails(
	       			Common.convertStringMoneyToDouble(actionForm.getConversionRateUSD(), Constants.CONVERSION_RATE_PRECISION),
	                    Common.convertStringToSQLDate(actionForm.getConversionDate()));


		        try { 
		            ejbDR.addGlFcrEntry(details, actionForm.getCurrency(), user.getCmpCode());
		            
	            } catch (GlFCNoFunctionalCurrencyFoundException ex) {
	            	
			       errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("dailyRates.error.currencyNotFound"));
		                  
	            } catch(GlFCRFunctionalCurrencyRateAlreadyExistException ex) {
	            	
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  new ActionMessage("dailyRates.error.currencyAlreadyExist"));
	                  
	            } catch (EJBException ex) {
	            	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in GlDailyRatesAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  
	               }
	               
	               return(mapping.findForward("cmnErrorPage"));
	            }
	            
	     	} else {
	     		
	     		GlFunctionalCurrencyRateDetails details = new GlFunctionalCurrencyRateDetails(
	       			actionForm.getCurrencyRateCode(),
	       			Common.convertStringMoneyToDouble(actionForm.getConversionRateUSD(), Constants.CONVERSION_RATE_PRECISION),
	       				Common.convertStringToSQLDate(actionForm.getConversionDate()));

	             try { 
	             
		        	ejbDR.updateGlFcrEntry(details, actionForm.getCurrency(), user.getCmpCode());
		        	
	             } catch (GlFCNoFunctionalCurrencyFoundException ex) {
	             	
	                errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("dailyRates.error.currencyNotFound"));
	                   
	             } catch (GlFCRFunctionalCurrencyRateAlreadyExistException ex) {
	             	
	                errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("dailyRates.error.currencyAlreadyExist"));
	             } catch (GlFCRFunctionalCurrencyRateAlreadyDeletedException ex) {
	             	
	                errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("dailyRates.error.currencyAlreadyDeleted"));
	                   
	             } catch (EJBException ex) {
	             	
	                if (log.isInfoEnabled()) {
	                	
	                   log.info("EJBException caught in GlDailyRatesAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                }
	                return(mapping.findForward("cmnErrorPage"));
	             }	     			     		     		
	     	}
	       

/*******************************************************
   -- Gl  DR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Gl  DR Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	

            
	     	 try { 
	     	 
	       		 ejbDR.deleteGlFcrEntry(actionForm.getCurrencyRateCode(), user.getCmpCode());
             } catch (GlFCRFunctionalCurrencyRateAlreadyDeletedException ex) {
             	
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("dailyRates.error.currencyAlreadyDeleted"));
                   
             } catch (EJBException ex) {
             	
                if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in GlDailyRatesAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                }
                return(mapping.findForward("cmnErrorPage"));
             }
	     
/*******************************************************
   -- Gl  DR Load Action --
*******************************************************/

         }
         if(frParam != null){
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glDailyRates"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }

	    	actionForm.clearCurrencyList();
            Iterator i = null;
	    
            try {
            	
		       ArrayList ejbGlFCList = ejbDR.getGlFcAll(user.getCmpCode());
		        
		       i = ejbGlFCList.iterator();
		       
		       while (i.hasNext()) {
		       	
		          GlFunctionalCurrencyDetails details = (GlFunctionalCurrencyDetails)i.next();
		          
		          actionForm.setCurrencyList(details.getFcName());
	       	   }
	       	   
		    } catch (GlFCNoFunctionalCurrencyFoundException ex) {
		    	
		       actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
		       
		    } catch (EJBException ex) {
		    	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in GlDailyRatesAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	               }
	               return(mapping.findForward("cmnErrorPage"));
             }
             
             try {
          	
	          	 if (request.getParameter("forward") != null) {
	            	            		
		        	    GlModFunctionalCurrencyRateDetails mdetails = ejbDR.getGlFcrByFrCode(
		        		    new Integer(request.getParameter("currencyRateCode")), user.getCmpCode());
		        				        		
		        		actionForm.setCurrencyRateCode(mdetails.getFrCode());
		        		
		        		if (!actionForm.getCurrencyList().contains(mdetails.getFrFcName())) {					    						    	
					    	
		        			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
		        				
		        				actionForm.clearCurrencyList();
		        				
		        			}
					    	actionForm.setCurrencyList(mdetails.getFrFcName());
					    	
					    }		        		
		        		actionForm.setCurrency(mdetails.getFrFcName());
		        		        		
		        		actionForm.setConversionRateUSD(Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION));
		        		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getFrDate()));
		        		
		        		return(mapping.findForward("glDailyRates"));
		                       	
		        }
		        
		        actionForm.setCurrencyRateCode(null);
		     } catch(GlFCRNoFunctionalCurrencyRateFoundException ex) {
		     	
		     	 errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("dailyRates.error.currencyAlreadyDeleted"));
		     	
	         } catch(EJBException ex){
			      	
	             if(log.isInfoEnabled()){
	                log.info("EJBException caught in GlDailyRatesAction.execute(): " + ex.getMessage() +
		               " session: " + session.getId());
		         }
		         return(mapping.findForward("cmnErrorPage"));
		      }


            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               
            } else {
            	
               if ((request.getParameter("saveButton") != null || 
                   request.getParameter("deleteButton") != null) && 
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               }
            }

            actionForm.reset(mapping, request);

            return(mapping.findForward("glDailyRates"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
         
      } catch (Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if (log.isInfoEnabled()) {
          	
             log.info("Exception caught in GlDailyRatesAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
