package com.struts.gl.dailyrates;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlDailyRatesForm extends ActionForm implements Serializable {
   
   Integer currencyRateCode = null;
   private String conversionDate = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String conversionRateUSD = null;

   private String saveButton = null;
   private String closeButton = null;
   private String deleteButton = null;
   private String pageState = new String();
   private String userPermission = new String();
   private String txnStatus = new String();

   public void setDeleteButton(String deleteButton){
      this.deleteButton = deleteButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getCurrencyRateCode() {
   	
   	  return currencyRateCode;
   	
   }
   
   public void setCurrencyRateCode(Integer currencyRateCode) {
   	
   	  this.currencyRateCode = currencyRateCode;
   	
   }

   public String getConversionDate(){
      return(conversionDate);
   }

   public void setConversionDate(String conversionDate){
      this.conversionDate = conversionDate;
   }

   public String getCurrency(){
      return(currency);
   }

   public void setCurrency(String currency){
      this.currency = currency;
   }

   public ArrayList getCurrencyList(){
      return(currencyList);
   }

   public void setCurrencyList(String currency){
      currencyList.add(currency);
   }

   public void clearCurrencyList(){
      currencyList.clear();
      currencyList.add(Constants.GLOBAL_BLANK);
   }

   public String getConversionRateUSD(){
      return(conversionRateUSD);
   }

   public void setConversionRateUSD(String conversionRateUSD){
      this.conversionRateUSD = conversionRateUSD;
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
      conversionDate = null;
      currency = Constants.GLOBAL_BLANK;
      conversionRateUSD = null;
      saveButton = null;
      deleteButton = null;
      closeButton = null;
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null){
         if(Common.validateRequired(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("dailyRates.error.conversionDateRequired"));
         }
		 if(!Common.validateDateFormat(conversionDate)){
             errors.add("conversionDate", new ActionMessage("dailyRates.error.conversionDateInvalid"));
         }
         if(Common.validateRequired(currency)){
            errors.add("currency",
               new ActionMessage("dailyRates.error.currencyRequired"));
         }
         if(currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("currency",
               new ActionMessage("dailyRates.error.currencyRequired"));
         }
         if(!Common.validateStringExists(currencyList, currency)){
            errors.add("currency",
               new ActionMessage("dailyRates.error.currencyInvalid"));
         }
         if(Common.validateRequired(conversionRateUSD)){
            errors.add("conversionRateUSD",
               new ActionMessage("dailyRates.error.conversionRateUSDRequired"));
         }
	     if(!Common.validateMoneyRateFormat(conversionRateUSD)){
		    errors.add("conversionRateUSD",
		       new ActionMessage("dailyRates.error.conversionRateUSDInvalid"));
		 }
      }
      return(errors);
   }
}
