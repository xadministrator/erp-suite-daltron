package com.struts.gl.financialreportentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlFrgFinancialReportEntryController;
import com.ejb.txn.GlFrgFinancialReportEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFrgFinancialReportDetails;
import com.util.GlModFrgFinancialReportDetails;

public final class GlFrgFinancialReportEntryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFrgFinancialReportEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFrgFinancialReportEntryForm actionForm = (GlFrgFinancialReportEntryForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FRG_FINANCIAL_REPORT_ENTRY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFrgFinancialReportEntry");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFrgFinancialReportEntryController EJB
*******************************************************/

         GlFrgFinancialReportEntryControllerHome homeFRE = null;
         GlFrgFinancialReportEntryController ejbFRE = null;

         try {

            homeFRE = (GlFrgFinancialReportEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFrgFinancialReportEntryControllerEJB", GlFrgFinancialReportEntryControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFrgFinancialReportEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFRE = homeFRE.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFrgFinancialReportEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl Frg FRE Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFrgFinancialReportEntry"));
	     
/*******************************************************
   -- Gl Frg FRE Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFrgFinancialReportEntry"));          

/*******************************************************
   -- Gl Frg FRE Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlFrgFinancialReportDetails details = new GlFrgFinancialReportDetails();
            details.setFrName(actionForm.getName());
            details.setFrDescription(actionForm.getDescription());
            details.setFrTitle(actionForm.getTitle());
            details.setFrFontSize(Common.convertStringToInt(actionForm.getFontSize()));
            details.setFrFontStyle(actionForm.getFontStyle());
            details.setFrHorizontalAlign(actionForm.getHorizontalAlign());
            
            try {
            	
            	ejbFRE.addGlFrgFrEntry(details, actionForm.getRowName(), 
            	  actionForm.getColumnName(), user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("frgFinancialReportEntry.error.financialReportNameAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgFinancialReportEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl Frg FRE Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl Frg FRE Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            GlFrgFinancialReportEntryList glFREList =
	            actionForm.getGlFREByIndex(actionForm.getRowSelected());
          
            GlFrgFinancialReportDetails details = new GlFrgFinancialReportDetails();
            details.setFrCode(glFREList.getFinancialReportCode());
            details.setFrName(actionForm.getName());
            details.setFrDescription(actionForm.getDescription());
            details.setFrTitle(actionForm.getTitle());
            details.setFrFontSize(Common.convertStringToInt(actionForm.getFontSize()));
            details.setFrFontStyle(actionForm.getFontStyle());
            details.setFrHorizontalAlign(actionForm.getHorizontalAlign());
            
            try {
            	
            	ejbFRE.updateGlFrgFrEntry(details, actionForm.getRowName(), 
            	  actionForm.getColumnName(), user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("frgFinancialReportEntry.error.financialReportNameAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgFinancialReportEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl Frg FRE Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl Frg FRE Edit Action --
*******************************************************/

         } else if (request.getParameter("glFREList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlFRERow(actionForm.getRowSelected());
            
            return mapping.findForward("glFrgFinancialReportEntry");
 
/*******************************************************
   -- Gl Frg FRE Delete Action --
*******************************************************/

         } else if (request.getParameter("glFREList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	GlFrgFinancialReportEntryList glFREList =
         		actionForm.getGlFREByIndex(actionForm.getRowSelected());
         	
         	try { 
         		
         		ejbFRE.deleteGlFrgFrEntry(glFREList.getFinancialReportCode(), user.getCmpCode());
         		
         	} catch (GlobalRecordAlreadyDeletedException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("frgFinancialReportEntry.error.recordAlreadyDeleted"));
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in GlFrgFinancialReportEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			
         		}                
         	} 
         	
            
/*******************************************************
   -- Gl Frg FRE Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFrgFinancialReportEntry");

            }

            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       


            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	actionForm.clearGlFREList();
            	           	
            	actionForm.clearRowNameList();
            	
            	list = ejbFRE.getGlFrgRsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setRowNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setRowNameList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearColumnNameList();           	
            	
            	list = ejbFRE.getGlFrgCsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setColumnNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setColumnNameList((String)i.next());
            			
            		}
            		
            	}     
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgFinancialReportEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }         

            try {
		            	                        
		        list = ejbFRE.getGlFrgFrAll(user.getCmpCode());
		            
		        i = list.iterator();
		            
		        while(i.hasNext()) {
	        	            			            	
		            GlModFrgFinancialReportDetails mdetails = (GlModFrgFinancialReportDetails)i.next();
		            	
		            GlFrgFinancialReportEntryList glFREList = new GlFrgFinancialReportEntryList(actionForm,
	        	      mdetails.getFrCode(),	            	    
	                  mdetails.getFrName(),
	                  mdetails.getFrRowName(), 
	                  mdetails.getFrColumnName(),
	                  mdetails.getFrDescription(),
	                  mdetails.getFrTitle(),
					  Common.convertIntegerToString(new Integer(mdetails.getFrFontSize())),
					  mdetails.getFrFontStyle(),
					  mdetails.getFrHorizontalAlign());
		            	    
		              actionForm.saveGlFREList(glFREList);
		            	
		         }
		            
		    } catch (GlobalNoRecordFoundException ex) {
		        			        	
		    } catch (EJBException ex) {
                
                if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgFinancialReportEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
            if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glFrgFinancialReportEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFrgFinancialReportEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}