package com.struts.gl.financialreportentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFrgFinancialReportEntryForm extends ActionForm implements Serializable {

   private Integer financialReportCode = null;
   private String rowName = null;
   private ArrayList rowNameList = new ArrayList();
   private String columnName = null;
   private ArrayList columnNameList = new ArrayList();   
   private String name = null;
   private String description = null;
   private String title = null;
   private String tableType = null;
   private String fontSize = null;
   private String fontStyle = null;
   private ArrayList fontStyleList = new ArrayList(); 
   private String horizontalAlign = null;
   private ArrayList horizontalAlignList = new ArrayList();
   
   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glFREList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlFrgFinancialReportEntryList getGlFREByIndex(int index) {

      return((GlFrgFinancialReportEntryList)glFREList.get(index));

   }

   public Object[] getGlFREList() {

      return glFREList.toArray();

   }

   public int getGlFREListSize() {

      return glFREList.size();

   }

   public void saveGlFREList(Object newGlFREList) {

      glFREList.add(newGlFREList);

   }

   public void clearGlFREList() {
      glFREList.clear();
   }

   public void setRowSelected(Object selectedGlFREList, boolean isEdit) {

      this.rowSelected = glFREList.indexOf(selectedGlFREList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlFRERow(int rowSelected) {
   	
       this.name = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getName();
       this.rowName = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getRowName();
       this.columnName = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getColumnName();       
       this.description = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getDescription();
       this.title = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getTitle();
       this.fontSize = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getFontSize();
       this.fontStyle = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getFontStyle();
       this.horizontalAlign = ((GlFrgFinancialReportEntryList)glFREList.get(rowSelected)).getHorizontalAlign();
       
   }

   public void updateGlFRERow(int rowSelected, Object newGlFREList) {

      glFREList.set(rowSelected, newGlFREList);

   }

   public void deleteGlFREList(int rowSelected) {

      glFREList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getFinancialReportCode() {
   	
   	  return financialReportCode;
   	
   }
   
   public void setFinancialReportCode(Integer financialReportCode) {
   	
   	  this.financialReportCode = financialReportCode;
   	
   }

   public String getRowName() {

      return rowName;

   }

   public void setRowName(String rowName) {

      this.rowName = rowName;

   }
   
   public ArrayList getRowNameList() {
   	
   	   return rowNameList;
   	
   }   

   public void setRowNameList(String rowName) {

      rowNameList.add(rowName);

   }
   
   public void clearRowNameList() {

      rowNameList.clear();
      rowNameList.add(Constants.GLOBAL_BLANK);
   }


   public String getColumnName() {

      return columnName;

   }

   public void setColumnName(String columnName) {

      this.columnName = columnName;

   }
   
   public ArrayList getColumnNameList() {
   	
   	   return columnNameList;
   	
   }   

   public void setColumnNameList(String columnName) {

      columnNameList.add(columnName);

   }
   
   public void clearColumnNameList() {

      columnNameList.clear();
      columnNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getName() {

      return name;

   }

   public void setName(String name) {

      this.name = name;

   }

   public String getDescription() {

      return description;

   }

   public void setDescription(String description) {

      this.description = description;

   }

   public String getTitle() {

      return title;

   }

   public void setTitle(String title) {

      this.title = title;

   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public String getFontSize()  {
   	
   	  return fontSize;
   	  
   }
   
   public void setFontSize(String fontSize)  {
   	
   	  this.fontSize = fontSize;
   	  
   }
   
   public String getFontStyle() {
   	
   	  return fontStyle;
   	
   }
   
   public void setFontStyle(String fontStyle) {
   	
   	  this.fontStyle = fontStyle;
   	
   }
   
   public ArrayList getFontStyleList() {
   	
   	  return fontStyleList;
   	
   }   
   
   public void setFontStyleList(String fontStyle) {
   	
   	  fontStyleList.add(fontStyle);
   	
   }
   
   public void clearFontStyleList() {
   	
   	  fontStyleList.clear();
   	  fontStyleList.add(Constants.GLOBAL_BLANK);
   	  
   }
   
   public String getHorizontalAlign() {
   	
   	  return horizontalAlign;
   	  
   }
   
   public void setHorizontalAlign(String horizontalAlign) {
   	
   	  this.horizontalAlign = horizontalAlign;
   	  
   }
   
   public ArrayList getHorizontalAlignList() {
   	
   	   return horizontalAlignList;
   	   
   }
   
   public void setHorizontalAlignList(String horizontalAlign) {
   	
   	   horizontalAlignList.add(horizontalAlign);
   	
   }
   
   public void clearHorizontalAlignList() {
   	
   	   horizontalAlignList.clear();
   	   horizontalAlignList.add(Constants.GLOBAL_BLANK);
   	   
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
         
		rowName = Constants.GLOBAL_BLANK; 
		columnName = Constants.GLOBAL_BLANK;
		name = null;
		description = null;
		title = null;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
		showDetailsButton = null;
		hideDetailsButton = null;
		fontSize = "10";
		fontStyle = "Helvetica";
		fontStyleList.clear();
		fontStyleList.add("Helvetica");
		fontStyleList.add("Courier");
		fontStyleList.add("Times-Roman");
		horizontalAlign = "Left";
		horizontalAlignList.clear();
	    horizontalAlignList.add("Left");
	    horizontalAlignList.add("Center");
	    horizontalAlignList.add("Right");
	    horizontalAlignList.add("Justified");
	    
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(name)) {

            errors.add("name",
               new ActionMessage("frgFinancialReportEntry.error.nameRequired"));

         }

         if (Common.validateRequired(title)) {

            errors.add("title",
               new ActionMessage("frgFinancialReportEntry.error.titleRequired"));

         }
         
         if (Common.validateRequired(rowName)) {

            errors.add("rowName",
               new ActionMessage("frgFinancialReportEntry.error.rowNameRequired"));

         }
         
         
         if (Common.validateRequired(columnName)) {

            errors.add("columnName",
               new ActionMessage("frgFinancialReportEntry.error.columnNameRequired"));

         }
         
         if ((!Common.validateNumberFormat(fontSize)) || (Common.convertStringToInt(fontSize) > 12)) {
         	
         	errors.add("fontSize",
               new ActionMessage("frgFinancialReportEntry.error.fontSizeInvalid"));
         	
         } 
      	
         if (Common.validateRequired(fontSize)) {

            errors.add("fontSize",
               new ActionMessage("frgFinancialReportEntry.error.fontSizeRequired"));

         }
         
      }
         
      return errors;

   }
}