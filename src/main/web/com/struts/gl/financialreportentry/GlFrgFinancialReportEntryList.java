package com.struts.gl.financialreportentry;

import java.io.Serializable;

public class GlFrgFinancialReportEntryList implements Serializable {

   private Integer financialReportCode = null;
   private String name = null;
   private String rowName = null;
   private String columnName = null;
   private String description = null;
   private String title = null;
   private String fontSize = null;
   private String fontStyle = null;
   private String horizontalAlign = null;
   
   private String editButton = null;
   private String deleteButton = null;
    
   private GlFrgFinancialReportEntryForm parentBean;
    
   public GlFrgFinancialReportEntryList(GlFrgFinancialReportEntryForm parentBean,
		Integer financialReportCode,
		String name, 
		String rowName, 
		String columnName,
		String description,
		String title,
		String fontSize,
		String fontStyle,
		String horizontalAlign) {
		
		this.parentBean = parentBean;
        this.financialReportCode = financialReportCode;
        this.name = name;
        this.rowName = rowName;
        this.columnName = columnName;
        this.description = description;
        this.title = title;
        this.fontSize = fontSize;
        this.fontStyle = fontStyle;
        this.horizontalAlign = horizontalAlign;
        
   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }

   public void setDeleteButton(String deleteButton) {
   	
   	  parentBean.setRowSelected(this, false);
   	
   }

   public Integer getFinancialReportCode() {

      return financialReportCode;

   }
   
   public String getName() {
   	
   	  return name;
   	  
   }
   
   public String getRowName() {
   	
   	  return rowName;
   	  
   }
   
   public String getColumnName() {
   	
   	  return columnName;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }
   
   public String getTitle() {
   	
   	  return title;
   	  
   }
   
   public String getFontSize() {
   	
   	  return fontSize;
   	  
   }
   
   public String getFontStyle() {
   	
   	  return fontStyle;
   	  
   }

   public String getHorizontalAlign() {
   	
   	  return horizontalAlign;
   	  
   }
   
}