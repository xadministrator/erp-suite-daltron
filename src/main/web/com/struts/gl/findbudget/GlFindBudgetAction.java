package com.struts.gl.findbudget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlFindBudgetController;
import com.ejb.txn.GlFindBudgetControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModBudgetAmountCoaDetails;
import com.util.GlModBudgetAmountDetails;

public final class GlFindBudgetAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFindBudgetAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFindBudgetForm actionForm = (GlFindBudgetForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FIND_BUDGET_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFindBudget");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
/*******************************************************
   Initialize GlFindBudgetController EJB
*******************************************************/

         GlFindBudgetControllerHome homeFB = null;
         GlFindBudgetController ejbFB = null;

         try {

            homeFB = (GlFindBudgetControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFindBudgetControllerEJB", GlFindBudgetControllerHome.class);
            

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFindBudgetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFB = homeFB.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFindBudgetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }
         
         ActionErrors errors = new ActionErrors();  
                             
 /*******************************************************
  	-- Ap FC First Action --
 *******************************************************/ 

      if(request.getParameter("firstButton") != null) {
      	
      	actionForm.setLineCount(0);

/*******************************************************
   -- Ap FC Previous Action --
*******************************************************/ 

      } else if (request.getParameter("previousButton") != null) {
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Ap FC Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null) {
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Ap FC Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            
            // create criteria 
            
            if (request.getParameter("goButton") != null) {
                
	        	HashMap criteria = new HashMap();
	        	
	        	if (!Common.validateRequired(actionForm.getBudgetOrganization())) {
	        		
	        		criteria.put("budgetOrganization", actionForm.getBudgetOrganization());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getBudgetName())) {
	        		
	        		criteria.put("budgetName", actionForm.getBudgetName());
	        		
	        	}

	        	if (!Common.validateRequired(actionForm.getAccountNumber())) {

	        		criteria.put("accountNumber", actionForm.getAccountNumber());

	        	}

	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFB.getGlBgaSizeByCriteria(actionForm.getCriteria(), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            try {
            	
            	actionForm.clearGlFBList();
            	
            	ArrayList list = ejbFB.getGlBgaByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
	           
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		GlModBudgetAmountDetails mdetails = (GlModBudgetAmountDetails)i.next();
      		
            		ArrayList glBgaBudgetAmountCoaList = new ArrayList();
            		glBgaBudgetAmountCoaList = mdetails.getBgaBudgetAmountCoaList();
            		GlModBudgetAmountCoaDetails bcDetails = new GlModBudgetAmountCoaDetails();
            		bcDetails = (GlModBudgetAmountCoaDetails)glBgaBudgetAmountCoaList.get(0);
            		
            		GlFindBudgetList glFBList = new GlFindBudgetList(actionForm,
            		    mdetails.getBgaCode(),
            		    mdetails.getBgaBoName(),
            		    mdetails.getBgaBgtName(),
            		    bcDetails.getBcCoaAccountNumber(),
            		    bcDetails.getBcCoaAccountDescription());
            		    
            		actionForm.saveGlFBList(glFBList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findBudget.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFindBudgetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFindBudget");

            }
                        
            actionForm.reset(mapping, request);
            
            if(actionForm.getFirstLoading() == null) {
            	
            	actionForm.setFirstLoading("loaded");
            	
            }
            
            return(mapping.findForward("glFindBudget"));

/*******************************************************
   -- Ap FC Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ap FC Open Action --
*******************************************************/

         } else if (request.getParameter("glFBList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             GlFindBudgetList glFBList =
                actionForm.getGlFBByIndex(actionForm.getRowSelected());
                
             String path = "/glBudgetEntry.do?forward=1" +
			     "&budgetCode=" + glFBList.getBudgetCode();

			 return(new ActionForward(path));

/*******************************************************
   -- Ap FC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            actionForm.clearGlFBList();

            if (request.getParameter("goButton") != null) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }
            
            actionForm.clearGlFBList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	
            	actionForm.clearBudgetOrganizationList();
            	
            	list = ejbFB.getGlBoAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBudgetOrganizationList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
   
            			actionForm.setBudgetOrganizationList((String)i.next());

            	    }
            		
               }
            	
            	actionForm.clearBudgetNameList();
            	
            	list = ejbFB.getGlBgtAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBudgetNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
   
            			actionForm.setBudgetNameList((String)i.next());

            	    }
            		
               }
            
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFindBudgetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            if(actionForm.getFirstLoading() == null) {
            	
            	actionForm.setBudgetOrganization(Constants.GLOBAL_BLANK);
            	actionForm.setBudgetName(Constants.GLOBAL_BLANK);
            	actionForm.setOrderBy(Constants.GLOBAL_BLANK);   
            	
            	actionForm.setLineCount(0);
                actionForm.setDisableNextButton(true);
                actionForm.setDisablePreviousButton(true);
                actionForm.setDisableFirstButton(true);
                actionForm.setDisableLastButton(true);
                actionForm.reset(mapping, request);
                
                actionForm.setFirstLoading("loaded");
            	
            } else {
            	
            	try {
	            	
	            	actionForm.clearGlFBList();
	            	
	            	ArrayList newList = ejbFB.getGlBgaByCriteria(actionForm.getCriteria(),
	            	    actionForm.getOrderBy(),
	            	    new Integer(actionForm.getLineCount()), 
	            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), user.getCmpCode());
	            	
	            	// check if prev should be disabled
		           if (actionForm.getLineCount() == 0) {
		            	
		              actionForm.setDisablePreviousButton(true);
		              actionForm.setDisableFirstButton(true);
		            	
		           } else {
		           	
		           	  actionForm.setDisablePreviousButton(false);
		           	  actionForm.setDisableFirstButton(false);
		           	
		           }
		           
		           // check if next should be disabled
		           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
		           	  
		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);
		           	  
		           } else {
		           	  
		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);
		           	  
		           	  //remove last record
		           	newList.remove(newList.size() - 1);
		           	
		           }
		           
	            	Iterator j = newList.iterator();
	            	
	            	while (j.hasNext()) {
	            		
	            		GlModBudgetAmountDetails mdetails = (GlModBudgetAmountDetails)j.next();
	      		
	            		ArrayList glBgaBudgetAmountCoaList = new ArrayList();
	            		glBgaBudgetAmountCoaList = mdetails.getBgaBudgetAmountCoaList();
	            		GlModBudgetAmountCoaDetails bcDetails = new GlModBudgetAmountCoaDetails();
	            		bcDetails = (GlModBudgetAmountCoaDetails)glBgaBudgetAmountCoaList.get(0);
	            		
	            		GlFindBudgetList glFBList = new GlFindBudgetList(actionForm,
	            		    mdetails.getBgaCode(),
	            		    mdetails.getBgaBoName(),
	            		    mdetails.getBgaBgtName(),
	            		    bcDetails.getBcCoaAccountNumber(),
	            		    bcDetails.getBcCoaAccountDescription());
	            		    
	            		actionForm.saveGlFBList(glFBList);
	            		
	            	}

	            } catch (GlobalNoRecordFoundException ex) {
	               
	               // disable prev next buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  new ActionMessage("findBudget.error.noRecordFound"));

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in GlFindBudgetAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }

	            }
            	
            }
        
            return(mapping.findForward("glFindBudget"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFindBudgetAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}