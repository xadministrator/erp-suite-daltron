package com.struts.gl.findbudget;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class GlFindBudgetForm extends ActionForm implements Serializable {
   
   private String budgetOrganization = null;
   private ArrayList budgetOrganizationList = new ArrayList();	
   private String budgetName = null;
   private ArrayList budgetNameList = new ArrayList();
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();
   private String accountNumber = null;
   private String accountDescription = null;
   
   private String pageState = new String();
   private ArrayList glFBList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String firstButton = null;
   private String nextButton = null;
   private String previousButton = null;
   private String lastButton = null;
   
   private boolean disableFirstButton = false;
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();
   
   private String firstLoading = null;

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public GlFindBudgetList getGlFBByIndex(int index) {

      return((GlFindBudgetList)glFBList.get(index));

   }

   public Object[] getGlFBList() {

      return glFBList.toArray();

   }

   public int getGlFBListSize() {

      return glFBList.size();

   }

   public void saveGlFBList(Object newGlFBList) {

      glFBList.add(newGlFBList);

   }

   public void clearGlFBList() {
   	
      glFBList.clear();
      
   }

   public void setRowSelected(Object selectedGlFBList, boolean isEdit) {

      this.rowSelected = glFBList.indexOf(selectedGlFBList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }
  
   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }
   
   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }   

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getBudgetOrganization() {
   	
	  return budgetOrganization;
   
   }

   public void setBudgetOrganization(String budgetOrganization) {
   	
	  this.budgetOrganization = budgetOrganization;
   
   }

   public ArrayList getBudgetOrganizationList() {
	
   	  return budgetOrganizationList;
   
   }

   public void setBudgetOrganizationList(String budgetOrganization) {
   	
   	  budgetOrganizationList.add(budgetOrganization);
   
   }

   public void clearBudgetOrganizationList() {
   
   	  budgetOrganizationList.clear();
      budgetOrganizationList.add(Constants.GLOBAL_BLANK);
   
   }

   public String getBudgetName() {
   	
   	  return budgetName;
   	  
   }
   
   public void setBudgetName(String budgetName) {
   	
   	  this.budgetName = budgetName;
   	  
   }                         

   public ArrayList getBudgetNameList() {

      return budgetNameList;

   }
   
   public void setBudgetNameList(String budgetName) {
   	
   	  budgetNameList.add(budgetName);
   
   }

   public void clearBudgetNameList() {
   
   	  budgetNameList.clear();
   	  budgetNameList.add(Constants.GLOBAL_BLANK);
   
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getFirstLoading() {
   	
   		return firstLoading;
   }
   
   public void setFirstLoading(String firstLoading) {
   		
   		this.firstLoading = firstLoading;
   		
   }
   
   public String getAccountNumber() {

	   return accountNumber;

   }

   public void setAccountNumber(String accountNumber) {

	   this.accountNumber = accountNumber;

   }

   public String getAccountDescription() {

	   return accountDescription;

   }

   public void setAccountDescription(String accountDescription) {

	   this.accountDescription = accountDescription;

   }
   

   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	
      if (orderByList.isEmpty()) { 
      
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("BUDGET ORGANIZATION");
	      orderByList.add("BUDGET NAME");
	      
	  }     
	  

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      
      return errors;

   }
}