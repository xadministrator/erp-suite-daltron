package com.struts.gl.findbudget;

import java.io.Serializable;

public class GlFindBudgetList implements Serializable {

   private Integer budgetCode = null;
   private String budgetOrganization = null;
   private String budgetName = null;
   private String accountNumber = null;
   private String accountDescription = null;

   private String openButton = null;
       
   private GlFindBudgetForm parentBean;
    
   public GlFindBudgetList(GlFindBudgetForm parentBean,
	  Integer budgetCode,
	  String budgetOrganization,  
	  String budgetName,
	  String accountNumber,
	  String accountDescription) {

      this.parentBean = parentBean;
      this.budgetCode = budgetCode;
      this.budgetOrganization = budgetOrganization;
      this.budgetName = budgetName;
      this.accountNumber = accountNumber;
      this.accountDescription = accountDescription;
      
   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }
   
   public Integer getBudgetCode() {
   	
   	  return budgetCode;
   	  
   }
   
   public String getBudgetOrganization() {
   	
   	  return budgetOrganization;
   	  
   }

   public String getBudgetName() {

      return budgetName;

   }

   public String getAccountNumber() {

	   return accountNumber;

   }

   public String getAccountDescription() {

	   return accountDescription;

   }
   
}