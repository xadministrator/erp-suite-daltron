package com.struts.gl.findchartofaccount;

import java.io.Serializable;

public class GlBranchCoaList implements Serializable {
	
	private String bcoaName = null;
	private Integer bcoaCode = null;
	private boolean branchCheckbox = false;	
	
	private GlFindChartOfAccountForm parentBean;
	
	public GlBranchCoaList(GlFindChartOfAccountForm parentBean, Integer bcoaCode, 
			String bcoaName) {
		
		this.parentBean = parentBean;
		this.bcoaCode = bcoaCode;
		this.bcoaName = bcoaName;				
		
	}
	
	public String getBcoaName() {
		
		return bcoaName;
		
	}
	
	public void setBcoaName(String bcoaName) {
		
		this.bcoaName = bcoaName;
		
	}
	
	public Integer getBcoaCode() {
		
		return bcoaCode;
		
	}
	
	public void setBcoaCode(Integer bcoaCode) {
		
		this.bcoaCode = bcoaCode;
		
	}
	
	public boolean getBranchCheckbox() {
		
		return branchCheckbox;
		
	}
	
	public void setBranchCheckbox(boolean branchCheckbox) {
		
		this.branchCheckbox = branchCheckbox;
		
	}
	
}