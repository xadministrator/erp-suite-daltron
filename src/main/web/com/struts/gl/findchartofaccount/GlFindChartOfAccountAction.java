package com.struts.gl.findchartofaccount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlFindChartOfAccountController;
import com.ejb.txn.GlFindChartOfAccountControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.GenModValueSetValueDetails;
import com.util.GlModChartOfAccountDetails;

public final class GlFindChartOfAccountAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlFindChartOfAccountAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         
         GlFindChartOfAccountForm actionForm = (GlFindChartOfAccountForm)form;
              
         String frParam = null;
         
         if (request.getParameter("child") == null && request.getParameter("childTxn") == null) {
         
         	frParam = Common.getUserPermission(user, Constants.GL_FIND_CHART_OF_ACCOUNT_ID);
         
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
         
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  if (request.getParameter("child") != null) {
  
	             	 return(mapping.findForward("glFindChartOfAccountChild"));
	            	
	              } else if (request.getParameter("childTxn") != null) {

	             	 return(mapping.findForward("glFindChartOfAccountChildTxn"));
	            	
	              } else {

	              	 return(mapping.findForward("glFindChartOfAccount"));
	              	
	              }
               }
            }
            actionForm.setUserPermission(frParam.trim());
         }else{
            actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlChartOfAccountController EJB
*******************************************************/

         GlFindChartOfAccountControllerHome homeFC = null;
         GlFindChartOfAccountController ejbFC = null;

         try{
            
            homeFC = (GlFindChartOfAccountControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFindChartOfAccountControllerEJB", GlFindChartOfAccountControllerHome.class);
                
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlFindChartOfAccountAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbFC = homeFC.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlFindChartOfAccountAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl DS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        if (request.getParameter("child") != null) {

            	return(mapping.findForward("glFindChartOfAccountChild"));
            	
            } else if (request.getParameter("childTxn") != null){

            	return(mapping.findForward("glFindChartOfAccountChildTxn"));
            	
            } else {

            	return(mapping.findForward("glFindChartOfAccount"));
            	
            }
	     
/*******************************************************
   -- Gl DS Hide Details Action --
*******************************************************/	     
	        
         } else if (request.getParameter("hideDetailsButton") != null) { 
         	
         	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
         	
         	if (request.getParameter("child") != null) {

         		return(mapping.findForward("glFindChartOfAccountChild"));
         		
         	} else if (request.getParameter("childTxn") != null){

         		return(mapping.findForward("glFindChartOfAccountChildTxn"));
         		
         	} else {

         		return(mapping.findForward("glFindChartOfAccount"));
         		
         	}                  

/*******************************************************
    -- Gl  FC First Action --
 *******************************************************/ 

	     } else if(request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);	        
	 
/*******************************************************
   -- Gl  FC Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Gl  FC Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Gl  FC Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null ||
			(request.getParameter("goFSButton")!= null && request.getParameter("goFSButton").equals("true"))){
                                    
            if (request.getParameter("goButton") != null ||
            		(request.getParameter("goFSButton")!= null && request.getParameter("goFSButton").equals("true"))) {
                
                // create criteria 
                
	            HashMap criteria = new HashMap();
	            HashMap localCriteria = new HashMap();
		            
			    if(!Common.validateRequired(actionForm.getAccountFrom())){			       
			       localCriteria.put(new String("coaAccountNumberFrom"), actionForm.getAccountFrom().trim());
			    }
			    
			    if(!Common.validateRequired(actionForm.getAccountTo())){			       
			       localCriteria.put(new String("coaAccountNumberTo"), actionForm.getAccountTo().trim());
			    }
			    
			    ArrayList vsvDescList = new ArrayList();
			    
			    ArrayList genVsList = actionForm.getGenVsList();
			    Iterator i = genVsList.iterator();
			    
			    while (i.hasNext()) {
			    	
			    	GlFindChartOfAccountVsList valueSetValue = (GlFindChartOfAccountVsList)i.next();
			    	
			    	if (Common.validateRequired(valueSetValue.getValueSetValueDescription())) continue; 
			    	
			    	GenModValueSetValueDetails mdetails = new GenModValueSetValueDetails();
			    	mdetails.setVsvVsName(valueSetValue.getValueSetName());
			    	mdetails.setVsvDescription(valueSetValue.getValueSetValueDescription());
			    	
			    	vsvDescList.add(mdetails);
			    	
			    }
			    
			    localCriteria.put(new String("vsvDescList"), vsvDescList);
			    
			    if(!Common.validateRequired(actionForm.getDateFrom())) {
			       criteria.put(new String("coaDateFrom"), Common.convertStringToSQLDate(actionForm.getDateFrom()));
	            }
	            
	            if(!Common.validateRequired(actionForm.getDateTo())) {
			       criteria.put(new String("coaDateTo"), Common.convertStringToSQLDate(actionForm.getDateTo()));
	            }
	            
	            if(!Common.validateRequired(actionForm.getAccountType())) {
			       localCriteria.put(new String("coaAccountType"), actionForm.getAccountType());
	            }
            	
	            
            	
            	ArrayList adBrnchList = new ArrayList();
            	
	            if (request.getParameter("child") != null) {
                    
	            	criteria.put(new String("coaEnable"), new Byte(Common.convertBooleanToByte(true)));
	            	
		            for(int j=0; j < actionForm.getGlBCOAListSize(); j++) {
		            
		                GlBranchCoaList glBCOAList = actionForm.getGlBCOAByIndex(j);
		                
		                if(glBCOAList.getBranchCheckbox() == true) {
		                    
		                    AdBranchDetails details = new AdBranchDetails();	                    
		                    details.setBrCode(glBCOAList.getBcoaCode());
		                    adBrnchList.add(details);
		                }
		            }

	            } else if (request.getParameter("childTxn") != null) {
	            	
	            		criteria.put(new String("coaEnable"), new Byte(Common.convertBooleanToByte(true)));
	            	
	                    AdBranchDetails details = new AdBranchDetails();	                    
	                    details.setBrCode(new Integer(user.getCurrentBranch().getBrCode()));
	                    adBrnchList.add(details);

	            } else {

	            	criteria.put(new String("coaEnable"), new Byte(Common.convertBooleanToByte(actionForm.getEnable())));
	            	
		            for(int j=0; j < actionForm.getGlBCOAListSize(); j++) {
		                
		                GlBranchCoaList glBCOAList = actionForm.getGlBCOAByIndex(j);
		                
		                if(glBCOAList.getBranchCheckbox() == true) {
		                    
		                    AdBranchDetails details = new AdBranchDetails();	                    
		                    details.setBrCode(glBCOAList.getBcoaCode());
		                    adBrnchList.add(details);
		                }
		            }
	            	
	            }

	            localCriteria.put(new String("adBrnchList"), adBrnchList);
	             	            
	            // save criteria 
	            
	            actionForm.setLineCount(0);
	            actionForm.setCriteria(criteria);
	            actionForm.setLocalCriteria(localCriteria);
	            
	     	}
                                
            if(request.getParameter("lastButton") != null) {
                
            	int size = ejbFC.getGlCoaSizeByCriteria(actionForm.getCriteria(), 
     		           (String)actionForm.getLocalCriteria().get("coaAccountNumberFrom"), 
			           (String)actionForm.getLocalCriteria().get("coaAccountNumberTo"), 
			           (ArrayList)actionForm.getLocalCriteria().get("vsvDescList"), 
			           (String)actionForm.getLocalCriteria().get("coaAccountType"),
			           (ArrayList)actionForm.getLocalCriteria().get("adBrnchList"),
			           user.getCmpCode()).intValue();            	            	
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
           
            actionForm.clearGlFCList();

	        try{
	        	
		       ArrayList ejbGlFCList = ejbFC.getGlCoaByCriteria(actionForm.getCriteria(), 
		           (String)actionForm.getLocalCriteria().get("coaAccountNumberFrom"), 
		           (String)actionForm.getLocalCriteria().get("coaAccountNumberTo"), 
		           (ArrayList)actionForm.getLocalCriteria().get("vsvDescList"), 
		           (String)actionForm.getLocalCriteria().get("coaAccountType"), 
		           new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
		           (ArrayList)actionForm.getLocalCriteria().get("adBrnchList"), user.getCmpCode());
		       
		       // check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (ejbGlFCList.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  ejbGlFCList.remove(ejbGlFCList.size() - 1);
	           	
	           }
		       
		       Iterator i = ejbGlFCList.iterator();
               while(i.hasNext()){
	           		GlModChartOfAccountDetails mdetails = (GlModChartOfAccountDetails)i.next();	           			           		
	           		
	           		GlFindChartOfAccountList glFCList = new GlFindChartOfAccountList(actionForm, 
	           		    mdetails.getMcoaCode(),
	           		    mdetails.getMcoaAccountNumber(), 
	           		    mdetails.getMcoaAccountType(), 
	           		    Common.convertSQLDateToString(mdetails.getMcoaDateFrom()), 
	           		    Common.convertSQLDateToString(mdetails.getMcoaDateTo()),
	                    Common.convertByteToBoolean(mdetails.getMcoaEnable()),
	                    mdetails.getMcoaDescription());
			  	
			  	    actionForm.saveGlFCList(glFCList);
			   }
			   
		    }catch(GlobalNoRecordFoundException ex){		    	
		       // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
			   errors.add(ActionMessages.GLOBAL_MESSAGE,
		         new ActionMessage("findChartOfAccount.error.noRecordFound"));
		         
		    }catch(EJBException ex){
		       if(log.isInfoEnabled()){
		         log.info("EJBException caught in GlFindChartOfAccountAction.execute(): " + ex.getMessage() +
		        " session: " + session.getId());
	           }
	           return(mapping.findForward("cmnErrorPage"));
            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
               if (request.getParameter("child") != null) {

	              return(mapping.findForward("glFindChartOfAccountChild"));
	            	
	           } else if (request.getParameter("childTxn") != null) {

	              return(mapping.findForward("glFindChartOfAccountChildTxn"));
	            	
	           } else {

	           	  return(mapping.findForward("glFindChartOfAccount"));
	           	
	           }

            }
                        
            actionForm.reset(mapping, request);       
            actionForm.setEnable(true);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           
            
	        if (request.getParameter("child") != null) {

	        	return(mapping.findForward("glFindChartOfAccountChild"));
	        	
	        } else if (request.getParameter("childTxn") != null){

	        	return(mapping.findForward("glFindChartOfAccountChildTxn"));
	        	
	        } else {

	        	return(mapping.findForward("glFindChartOfAccount"));
	        	
	        }

/*******************************************************
   -- Gl  FC Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl  FC Open Action --
*******************************************************/

        }else if(request.getParameter("glFCList[" +
           actionForm.getRowSelected() + "].openButton") != null){

		   GlFindChartOfAccountList glFCList =
	              actionForm.getGlFCByIndex(actionForm.getRowSelected());
	
		   String path = "/glChartOfAccounts.do?forward=1" +
		      "&coaCode=" + glFCList.getCoaCode();
		   return(new ActionForward(path));
	    	    
/*******************************************************
   -- Gl  FC Load Action --
*******************************************************/

         }
         
         if(frParam != null){            
             
            actionForm.clearGlFCList();
                        
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearAccountTypeList();         	
            	
            	list = ejbFC.getGenQlAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setAccountTypeList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setAccountTypeList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearGenVsList();         	
            	
            	list = ejbFC.getGenVsAll(user.getCmpCode());
            	           		           		            		
        		i = list.iterator();
        		
        		while (i.hasNext()) {
        			        			
        		    GlFindChartOfAccountVsList valueSetValue = new GlFindChartOfAccountVsList(actionForm,
        		        (String)i.next(), null);
        		    
        			
        			actionForm.saveGenVsList(valueSetValue);
        			
        		}
        		
        		actionForm.clearGlBCOAList();
        		
        		list =  ejbFC.getAdBrResAll(new Integer(user.getCurrentResCode()), user.getCmpCode());
        		
        		i = list.iterator();
        		
        		while(i.hasNext()) {
                    
                    AdBranchDetails details = (AdBranchDetails)i.next();
                    
                    GlBranchCoaList glBCOAList = new GlBranchCoaList(actionForm,
                            details.getBrCode(),
                            details.getBrName());
                    
                    actionForm.saveGlBCOAList(glBCOAList);
                    
                }
            		
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFindChartOfAccountAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
	    	
            
	        if (actionForm.getTableType() == null) {
	        	
	        	Iterator j = actionForm.getGenVsList().iterator();
	            
	            while (j.hasNext()) {
	            	
	            	GlFindChartOfAccountVsList newList = (GlFindChartOfAccountVsList)j.next();
	            	
	            	newList.setValueSetValueDescription(null);
	            	
	            }
	        	
	        	actionForm.setAccountFrom(null);
	        	actionForm.setAccountTo(null);
	        	actionForm.setDateFrom(null);
	        	actionForm.setDateTo(null);
	        	actionForm.setAccountType(Constants.GLOBAL_BLANK);
	        	actionForm.setGoButton(null);
	        	actionForm.setEnable(true);
	        	
	        	actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	            
	            HashMap criteria = new HashMap();
	            criteria.put(new String("coaEnable"), new Byte((byte)1));
	            
	            actionForm.setCriteria(criteria);
	            
	            actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            } else {
            	
            	HashMap localCriteria = new HashMap();
		            
			    if(!Common.validateRequired(actionForm.getAccountFrom())){			       
			       localCriteria.put(new String("coaAccountNumberFrom"), actionForm.getAccountFrom().trim());
			    }
			    
			    if(!Common.validateRequired(actionForm.getAccountTo())){			       
			       localCriteria.put(new String("coaAccountNumberTo"), actionForm.getAccountTo().trim());
			    }
			    
			    ArrayList vsvDescList = new ArrayList();
			    
			    ArrayList genVsList = actionForm.getGenVsList();
			    Iterator k = genVsList.iterator();
			    
			    while (k.hasNext()) {
			    	
			    	GlFindChartOfAccountVsList valueSetValue = (GlFindChartOfAccountVsList)k.next();
			    	
			    	if (Common.validateRequired(valueSetValue.getValueSetValueDescription())) continue; 
			    	
			    	GenModValueSetValueDetails mdetails = new GenModValueSetValueDetails();
			    	mdetails.setVsvVsName(valueSetValue.getValueSetName());
			    	mdetails.setVsvDescription(valueSetValue.getValueSetValueDescription());
			    	
			    	vsvDescList.add(mdetails);
			    	
			    }
			    
			    localCriteria.put(new String("vsvDescList"), vsvDescList);
			    
			    
	            if(!Common.validateRequired(actionForm.getAccountType())) {
			       localCriteria.put(new String("coaAccountType"), actionForm.getAccountType());
	            }
	            
	            ArrayList adBrnchList = new ArrayList();
	            
	            Object[] adBrLists = actionForm.getGlBCOAList();
	            
	            if (request.getParameter("child") != null) {

	            	for(int j=0; j<adBrLists.length; j++) {
		                
		                GlBranchCoaList adBrList = actionForm.getGlBCOAByIndex(j);
		                
		                if(adBrList.getBranchCheckbox() == true) {
		                   
		                    AdBranchDetails mdetails = new AdBranchDetails();	                    
		                    mdetails.setBrCode(adBrList.getBcoaCode());
		                    adBrnchList.add(mdetails);
		                }
		            }	            	            
	            	
	            } else if (request.getParameter("childTxn") != null) {
	            	
                    AdBranchDetails mdetails = new AdBranchDetails();	                    
                    mdetails.setBrCode(new Integer(user.getCurrentBranch().getBrCode()));
                    adBrnchList.add(mdetails);
	            	
	            } else {
	            	
	            	for(int j=0; j<adBrLists.length; j++) {
		                
		                GlBranchCoaList adBrList = actionForm.getGlBCOAByIndex(j);
		                
		                if(adBrList.getBranchCheckbox() == true) {
		                   
		                    AdBranchDetails mdetails = new AdBranchDetails();	                    
		                    mdetails.setBrCode(adBrList.getBcoaCode());
		                    adBrnchList.add(mdetails);
		                }
		            }	            	            
	            	
	            }

	            localCriteria.put(new String("adBrnchList"), adBrnchList);
	            
	            // save criteria 
	            
	            actionForm.setLocalCriteria(localCriteria);
            	
	            HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("coaEnable")) {
            		
            		Byte b = (Byte)c.get("coaEnable");
            		
            		actionForm.setEnable(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
	            
            	try{
    	           
    		       ArrayList ejbGlFCList = ejbFC.getGlCoaByCriteria(actionForm.getCriteria(), 
    		           (String)actionForm.getLocalCriteria().get("coaAccountNumberFrom"), 
    		           (String)actionForm.getLocalCriteria().get("coaAccountNumberTo"), 
    		           (ArrayList)actionForm.getLocalCriteria().get("vsvDescList"), 
    		           (String)actionForm.getLocalCriteria().get("coaAccountType"), 
    		           new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
    		           (ArrayList)actionForm.getLocalCriteria().get("adBrnchList"), user.getCmpCode());
    		       
    		       // check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (ejbGlFCList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	  ejbGlFCList.remove(ejbGlFCList.size() - 1);
    	           	
    	           }
    		       
    		       Iterator j = ejbGlFCList.iterator();
                   while(j.hasNext()){
    	           		GlModChartOfAccountDetails mdetails = (GlModChartOfAccountDetails)j.next();
    	           		
    	           		GlFindChartOfAccountList glFCList = new GlFindChartOfAccountList(actionForm, 
    	           		    mdetails.getMcoaCode(),
    	           		    mdetails.getMcoaAccountNumber(), 
    	           		    mdetails.getMcoaAccountType(), 
    	           		    Common.convertSQLDateToString(mdetails.getMcoaDateFrom()), 
    	           		    Common.convertSQLDateToString(mdetails.getMcoaDateTo()),
    	                    Common.convertByteToBoolean(mdetails.getMcoaEnable()),
    	                    mdetails.getMcoaDescription());
    			  	
    			  	    actionForm.saveGlFCList(glFCList);
    			   }
    			   
    		    }catch(GlobalNoRecordFoundException ex){		    	
    		       // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
    			   errors.add(ActionMessages.GLOBAL_MESSAGE,
    		         new ActionMessage("findChartOfAccount.error.noRecordFound"));
    		         
    		    }catch(EJBException ex){
    		       if(log.isInfoEnabled()){
    		         log.info("EJBException caught in GlFindChartOfAccountAction.execute(): " + ex.getMessage() +
    		        " session: " + session.getId());
    	           }
    	           return(mapping.findForward("cmnErrorPage"));
                }
            	
            }
            
            if (request.getParameter("child") == null && request.getParameter("childTxn") == null) {
                        
            	return(mapping.findForward("glFindChartOfAccount"));
            	
            } else if (request.getParameter("child") != null){
            	
            	// this is a child
            	try {
            		
            		// save criteria
	        	
		        	actionForm.setLineCount(0);
		        	
		        	HashMap criteria = new HashMap();
		        	criteria.put(new String("coaEnable"), new Byte((byte)1));
		        	actionForm.setCriteria(criteria);	
		        	
		        	HashMap localCriteria = new HashMap();		        					    
		        	localCriteria.put(new String("vsvDescList"), new ArrayList());
		        	localCriteria.put(new String("adBrnchList"), new ArrayList());
		        	actionForm.setLocalCriteria(localCriteria);		        	
		        		        			        			        	
		        	
		        	actionForm.clearGlFCList();
		        	
		        	ArrayList newList = ejbFC.getGlCoaByCriteria(actionForm.getCriteria(), 
			           null, null, new ArrayList(), null, 
			           new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
			           new ArrayList(), user.getCmpCode());
			           
			       actionForm.setDisablePreviousButton(true);
			       actionForm.setDisableFirstButton(true);
	            		           
		           // check if next should be disabled
		           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
		           	  
		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);
		           	  
		           } else {
		           	  
		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);
		           	  
		           	  //remove last record
		           	  newList.remove(newList.size() - 1);
		           	
		           }
		           
		           i = newList.iterator();
	               while(i.hasNext()) {
	               	
		           		GlModChartOfAccountDetails mdetails = (GlModChartOfAccountDetails)i.next();
		           		
		           		GlFindChartOfAccountList glFCList = new GlFindChartOfAccountList(actionForm, 
		           		    mdetails.getMcoaCode(),
		           		    mdetails.getMcoaAccountNumber(), 
		           		    mdetails.getMcoaAccountType(), 
		           		    Common.convertSQLDateToString(mdetails.getMcoaDateFrom()), 
		           		    Common.convertSQLDateToString(mdetails.getMcoaDateTo()),
		                    Common.convertByteToBoolean(mdetails.getMcoaEnable()),
		                    mdetails.getMcoaDescription());
				  	
				  	    actionForm.saveGlFCList(glFCList);
				  	    
				   }
            		
            	} catch (GlobalNoRecordFoundException ex) {
               
	               // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	
	            } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in GlJournalPostAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
            	
            	
            	actionForm.setSelectedAccount(request.getParameter("selectedAccount"));
            	actionForm.setSelectedAccountDescription(request.getParameter("selectedAccountDescription"));
            	return(mapping.findForward("glFindChartOfAccountChild"));
            	
            } else {
            	
            	// this is a child transaction
            	try {
            		
            		// save criteria
	        	
		        	actionForm.setLineCount(0);
		        	
		        	HashMap criteria = new HashMap();
		        	criteria.put(new String("coaEnable"), new Byte((byte)1));
		        	actionForm.setCriteria(criteria);	

		        	ArrayList adBrnchList = new ArrayList();
		        		
                    AdBranchDetails details = new AdBranchDetails();	                    
                    details.setBrCode(new Integer(user.getCurrentBranch().getBrCode()));
                    adBrnchList.add(details);

		        	HashMap localCriteria = new HashMap();		        					    
		        	localCriteria.put(new String("vsvDescList"), new ArrayList());
		        	localCriteria.put(new String("adBrnchList"), adBrnchList);
		        	actionForm.setLocalCriteria(localCriteria);		        	
		        		        			        			        	
		        	actionForm.clearGlFCList();
		        	
		        	ArrayList newList = ejbFC.getGlCoaByCriteria(actionForm.getCriteria(), 
			           null, null, new ArrayList(), null, 
			           new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
			           adBrnchList, user.getCmpCode());
			           
			       actionForm.setDisablePreviousButton(true);
			       actionForm.setDisableFirstButton(true);
	            		           
		           // check if next should be disabled
		           if (newList.size() <= Constants.GLOBAL_MAX_LINES) {
		           	  
		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);
		           	  
		           } else {
		           	  
		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);
		           	  
		           	  //remove last record
		           	  newList.remove(newList.size() - 1);
		           	
		           }
		           
		           i = newList.iterator();
	               while(i.hasNext()) {
	               	
		           		GlModChartOfAccountDetails mdetails = (GlModChartOfAccountDetails)i.next();
		           		
		           		GlFindChartOfAccountList glFCList = new GlFindChartOfAccountList(actionForm, 
		           		    mdetails.getMcoaCode(),
		           		    mdetails.getMcoaAccountNumber(), 
		           		    mdetails.getMcoaAccountType(), 
		           		    Common.convertSQLDateToString(mdetails.getMcoaDateFrom()), 
		           		    Common.convertSQLDateToString(mdetails.getMcoaDateTo()),
		                    Common.convertByteToBoolean(mdetails.getMcoaEnable()),
		                    mdetails.getMcoaDescription());
				  	
				  	    actionForm.saveGlFCList(glFCList);
				  	    
				   }
            		
            	} catch (GlobalNoRecordFoundException ex) {
               
	               // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	
	            } catch (EJBException ex) {
	
	               if (log.isInfoEnabled()) {
	
	                  log.info("EJBException caught in GlJournalPostAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }
	
	            }
            	
            	
            	actionForm.setSelectedAccount(request.getParameter("selectedAccount"));
            	actionForm.setSelectedAccountDescription(request.getParameter("selectedAccountDescription"));
            	return(mapping.findForward("glFindChartOfAccountChildTxn"));
            	
            }

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){
      		
      		e.printStackTrace();
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()){
             log.info("Exception caught in GlFindChartOfAccountAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
