package com.struts.gl.findchartofaccount;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlFindChartOfAccountForm extends ActionForm implements Serializable {

   private String accountFrom = null;
   private String accountTo = null;
   private ArrayList genVsList = new ArrayList();
   private String dateFrom = null;
   private String dateTo = null;
   private String accountType = null;
   private ArrayList accountTypeList = new ArrayList();
   private boolean enable = true;
   private String goButton = null;
   private String closeButton = null;
   private String tableType = null;
   
   private String selectedAccount = null;
   private String selectedAccountDescription = null;
   private String sourceType = null;
   
   private String showDetailsButton = null;
   private String hideDetailsButton = null;
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();
   private HashMap localCriteria = new HashMap();
   
   private ArrayList glFCList = new ArrayList();
   private int rowSelected = 0;   
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private ArrayList glBCOAList = new ArrayList();
   
   private ArrayList glBrCOAList = new ArrayList();
      
   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public GlFindChartOfAccountList getGlFCByIndex(int index){
      return((GlFindChartOfAccountList)glFCList.get(index));
   }

   public Object[] getGlFCList(){
      return(glFCList.toArray());
   }

   public int getGlFCListSize(){
      return(glFCList.size());
   }

   public void saveGlFCList(Object newGlFCList){
      glFCList.add(newGlFCList);
   }

   public void clearGlFCList(){
      glFCList.clear();
   }

   public void setRowSelected(Object selectedGlFCList, boolean isEdit){
      this.rowSelected = glFCList.indexOf(selectedGlFCList);
   }

   public void updateGlFCRow(int rowSelected, Object newGlFCList){
      glFCList.set(rowSelected, newGlFCList);
   }

   public void deleteGlFCList(int rowSelected){
      glFCList.remove(rowSelected);
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getAccountFrom(){
	   return(accountFrom);
	}

	public void setAccountFrom(String accountFrom){
	   this.accountFrom = accountFrom;
	}
	
    public String getAccountTo(){
	   return(accountTo);
	}

	public void setAccountTo(String accountTo){
	   this.accountTo = accountTo;
	}
	
	public ArrayList getGenVsList() {
		
		return genVsList;
		
	}
	
	public void saveGenVsList(Object genValueSet) {
		
		genVsList.add(genValueSet);
	}
	
	public void clearGenVsList() {
		
		genVsList.clear();
		
	}

	public String getDateFrom(){
	   return(dateFrom);
	}

	public void setDateFrom(String dateFrom){
	   this.dateFrom = dateFrom;
	}

	public String getDateTo(){
	   return(dateTo);
	}

	public void setDateTo(String dateTo){
	   this.dateTo = dateTo;
	}
	
	public String getAccountType() {
		return accountType;
	}
	
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	
	public ArrayList getAccountTypeList() {
		return accountTypeList;
	}
	
	public void setAccountTypeList(String accountType) {
		accountTypeList.add(accountType);
	}
	
	public void clearAccountTypeList() {
		accountTypeList.clear();
		accountTypeList.add(Constants.GLOBAL_BLANK);
	}
	
	public boolean getEnable(){
	   return(enable);
	}

	public void setEnable(boolean enable){
	   this.enable = enable;
	}
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public HashMap getLocalCriteria() {
   	
   	  return localCriteria;
   	
   }
   
   public void setLocalCriteria(HashMap localCriteria) {
   	
   	  this.localCriteria = localCriteria;
   	
   }

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public String getSelectedAccount() {
   	
   	  return selectedAccount;
   	
   }
   
   public void setSelectedAccount(String selectedAccount) {
   	
   	  this.selectedAccount = selectedAccount;
   	
   }
   
   public String getSelectedAccountDescription() {
   	
   	  return selectedAccountDescription;
   	
   }
   
   public void setSelectedAccountDescription(String selectedAccountDescription) {
   	
   	  this.selectedAccountDescription = selectedAccountDescription;
   	
   }
   
   
   public String getSourceType() {
   	
   	  return sourceType;
   	
   }
   
   public void setSourceType(String sourceType) {
   	
   	  this.sourceType = sourceType;
   	
   }
   
	public Object[] getGlBCOAList(){
	    
	    return glBCOAList.toArray();
	    
	}
	
	public GlBranchCoaList getGlBCOAByIndex(int index){
	    
	    return ((GlBranchCoaList)glBCOAList.get(index));
	    
	}
	
	public int getGlBCOAListSize(){
	    
	    return(glBCOAList.size());
	    
	}
	
	public void saveGlBCOAList(Object newGlBCOAList){
	    
	    glBCOAList.add(newGlBCOAList);   	  
	    
	}
	
	public void clearGlBCOAList(){
	    
	    glBCOAList.clear();
	    
	}
	
	public void setGlBCOAList(ArrayList glBCOAList) {
	    
	    this.glBCOAList = glBCOAList;
	    
	}
	
	public int getGenVsListSize() {
		
		return genVsList.size();
	
	}
	
	public String getGoFSButton(){
		return goButton;
	}
	
	public Object[] getGlBrCOAList(){
	    
	    return glBrCOAList.toArray();
	    
	}
   
	public void reset(ActionMapping mapping, HttpServletRequest request){
	    
		glBrCOAList.clear();
		
		for (int i=0; i<glBCOAList.size(); i++) {
	        
	        GlBranchCoaList list = (GlBranchCoaList)glBCOAList.get(i);
	        list.setBranchCheckbox(false);
	        
	        GlBranchCoaList brCOA = new GlBranchCoaList(null,list.getBcoaCode(),null);
	        brCOA.setBranchCheckbox(false);
	        glBrCOAList.add(brCOA);
	    
		}
	    
	    enable = true;
	    
	    closeButton = null;
	    showDetailsButton = null;
	    hideDetailsButton = null;      
	    
	}

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      
      if(request.getParameter("goButton") != null){
	     if(!Common.validateDateFormat(dateFrom)){
	        errors.add("dateFrom", new ActionMessage("findChartOfAccount.error.dateFromInvalid"));
	     }
	     if(!Common.validateDateFormat(dateTo)){
	        errors.add("dateTo", new ActionMessage("findChartOfAccount.error.dateToInvalid"));
	     }   
	     if((Common.validateRequired(accountFrom) && !Common.validateRequired(accountTo)) ||
	        (Common.validateRequired(accountTo) && !Common.validateRequired(accountFrom))) {
	        errors.add("accountTo", new ActionMessage("findChartOfAccount.error.accountFromAccountToError"));
	     }
	  }
      return(errors);
   }

}
