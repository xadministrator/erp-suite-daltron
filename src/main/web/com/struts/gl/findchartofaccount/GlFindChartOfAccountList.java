package com.struts.gl.findchartofaccount;

import java.io.Serializable;

public class GlFindChartOfAccountList implements Serializable {

    private Integer coaCode = null;
	private String account = null;	
	private String accountType = null;
	private String effectiveDateFrom = null;
	private String effectiveDateTo = null;
	private boolean enabled = false;
	private String description = null;
	private String openButton = null;
		
	private GlFindChartOfAccountForm parentBean;

	public GlFindChartOfAccountList(GlFindChartOfAccountForm parentBean, Integer coaCode,
	     String account, String accountType, String effectiveDateFrom, String effectiveDateTo,
	     boolean enabled, String description){
	   this.parentBean = parentBean;
	   this.coaCode = coaCode;
	   this.account = account;
	   this.accountType = accountType;
	   this.effectiveDateFrom = effectiveDateFrom;
	   this.effectiveDateTo = effectiveDateTo;
	   this.enabled = enabled;
	   this.description = description;
	}

	public void setParentBean(GlFindChartOfAccountForm parentBean){
	   this.parentBean = parentBean;
	}

	public void setOpenButton(String openButton){
	   parentBean.setRowSelected(this, false);
	}
	
	public Integer getCoaCode(){
	   return(coaCode);
	}

	public void setCoaCode(Integer coaCode){
	   this.coaCode = coaCode;
	}
	
        public String getAccount(){
	   return(account);
	}

	public void setAccount(String account){
	   this.account = account;
	}

	public String getAccountType(){
	  return(accountType);
	}

	public void setAccountType(String accountType){
	   this.accountType = accountType;
	}

	public String getEffectiveDateFrom(){
	   return(effectiveDateFrom);
	}

	public void setEffectiveDateFrom(String effectiveDateFrom){
	   this.effectiveDateFrom = effectiveDateFrom;
	}

	public String getEffectiveDateTo(){
	   return(effectiveDateTo);
	}

	public void setEffectiveDateTo(String effectiveDateTo){
	   this.effectiveDateTo = effectiveDateTo;
	}

	public boolean getEnabled(){
	   return(enabled);
	}

	public void setEnabled(boolean enabled){
	   this.enabled = enabled;
	}
	
    public String getDescription(){
	   return(description);
	}

	public void setDescription(String description){
	   this.description = description;
	}

}
