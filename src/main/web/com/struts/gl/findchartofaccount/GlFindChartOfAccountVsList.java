package com.struts.gl.findchartofaccount;

import java.io.Serializable;

public class GlFindChartOfAccountVsList implements Serializable {

    private String valueSetName = null;
    private String valueSetValueDescription = null;
		
	private GlFindChartOfAccountForm parentBean;

	public GlFindChartOfAccountVsList(GlFindChartOfAccountForm parentBean, String valueSetName,
	   String valueSetValueDescription){
	   this.parentBean = parentBean;
	   this.valueSetName = valueSetName;
	   this.valueSetValueDescription = valueSetValueDescription;
	}

	public void setParentBean(GlFindChartOfAccountForm parentBean){
	   this.parentBean = parentBean;
	}

	public String getValueSetName() {
		
		return valueSetName;		
		
	}
	
	public void setValueSetName(String valueSetName) {
		
		this.valueSetName = valueSetName;
		
	}
	
	public String getValueSetValueDescription() {
		
		return valueSetValueDescription;
		
	}
	
	public void setValueSetValueDescription(String valueSetValueDescription) {
		
		this.valueSetValueDescription = valueSetValueDescription;
		
	}

}
