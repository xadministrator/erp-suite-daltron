package com.struts.gl.finddailyrate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlFindDailyRateController;
import com.ejb.txn.GlFindDailyRateControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModFunctionalCurrencyRateDetails;

public final class GlFindDailyRateAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlFindDailyRateAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         
         GlFindDailyRateForm actionForm = (GlFindDailyRateForm)form;
      
         String frParam = Common.getUserPermission(user, Constants.GL_FIND_DAILY_RATE_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glFindDailyRate"));
               }
            }
            actionForm.setUserPermission(frParam.trim());
         }else{
            actionForm.setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlChartOfAccountController EJB
*******************************************************/

         GlFindDailyRateControllerHome homeFDR = null;
         GlFindDailyRateController ejbFDR = null;

         try{
            
            homeFDR = (GlFindDailyRateControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFindDailyRateControllerEJB", GlFindDailyRateControllerHome.class);
                
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlFindDailyRateAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbFDR = homeFDR.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlFindDailyRateAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl FDR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFindDailyRate"));
	     
/*******************************************************
   -- Gl FDR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFindDailyRate"));                  

/*******************************************************
   -- Gl  FDR First Action --
*******************************************************/ 

	     } else if(request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);
	        	 
/*******************************************************
   -- Gl  FDR Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Gl  FDR Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 

/*******************************************************
   -- Gl  FDR Go Action --
*******************************************************/

         if(request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null){
                                    
            if (request.getParameter("goButton") != null) {
                
                // create criteria 
                
	            HashMap criteria = new HashMap();
		            
			    if(!Common.validateRequired(actionForm.getConversionDate())){			       
			       criteria.put(new String("frDate"), Common.convertStringToSQLDate(actionForm.getConversionDate()));
			    }
			    
			    if(!Common.validateRequired(actionForm.getCurrency())) {
			       criteria.put(new String("fcName"), actionForm.getCurrency());
	            }	           
	            	            
	            // save criteria 
	            
	            actionForm.setLineCount(0);
	            actionForm.setCriteria(criteria);
	            
	     	}
           
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFDR.getGlFrSizeByCriteria(actionForm.getCriteria(), user.getCmpCode()).intValue();

            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            actionForm.clearGlFDRList();

	        try{
	        	
		       ArrayList ejbGlFDRList = ejbFDR.getGlFrByCriteria(actionForm.getCriteria(), new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), user.getCmpCode());
		       
		       // check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	  
	           }
	           
	           // check if next should be disabled
	           if (ejbGlFDRList.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  ejbGlFDRList.remove(ejbGlFDRList.size() - 1);
	           	
	           }
		       
		       Iterator i = ejbGlFDRList.iterator();
               while(i.hasNext()){
	           		GlModFunctionalCurrencyRateDetails mdetails = (GlModFunctionalCurrencyRateDetails)i.next();
	           		
	           		GlFindDailyRateList glFDRList = new GlFindDailyRateList(actionForm,
	           		    mdetails.getFrCode(), 
	           		    Common.convertSQLDateToString(mdetails.getFrDate()),
	           		    mdetails.getFrFcName(),
	           		    Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION),
	           		    Common.convertDoubleToStringMoney(
		         			(1/mdetails.getFrXToUsd()), Constants.CONVERSION_RATE_PRECISION));
	           		    
			  	
			  	    actionForm.saveGlFDRList(glFDRList);
			   }
			   
		    }catch(GlobalNoRecordFoundException ex){		    			       		       
		       // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
			   errors.add(ActionMessages.GLOBAL_MESSAGE,
		         new ActionMessage("findDailyRate.error.noRecordFound"));
		         
		    }catch(EJBException ex){
		       if(log.isInfoEnabled()){
		         log.info("EJBException caught in GlFindDailyRateAction.execute(): " + ex.getMessage() +
		        " session: " + session.getId());
	           }
	           return(mapping.findForward("cmnErrorPage"));
            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFindDailyRate");

            }
                        
            actionForm.reset(mapping, request);          
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("glFindDailyRate"));

/*******************************************************
   -- Gl  FDR Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl  FDR Open Action --
*******************************************************/

        }else if(request.getParameter("glFDRList[" +
           actionForm.getRowSelected() + "].openButton") != null){

		   GlFindDailyRateList glFDRList =
	              actionForm.getGlFDRByIndex(actionForm.getRowSelected());
	
		   String path = "/glDailyRates.do?forward=1" +
		      "&currencyRateCode=" + glFDRList.getDailyRatesCode();
		   return(new ActionForward(path));
	    	    
/*******************************************************
   -- Gl  FDR Load Action --
*******************************************************/

         }
         
         if(frParam != null){           
         
            actionForm.clearCurrencyList();
            
            try {
            	
		       ArrayList list = ejbFDR.getGlFcAll(user.getCmpCode());
		        
		       Iterator i = list.iterator();
		       
		       while (i.hasNext()) {
		       	
		          String fcName = (String)i.next();
		          
		          actionForm.setCurrencyList(fcName);
	       	   }
	       	   		       
		    } catch (EJBException ex) {
		    	
	               if (log.isInfoEnabled()) {
	               	
	                  log.info("EJBException caught in GlFindDailyRateAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	               }
	               return(mapping.findForward("cmnErrorPage"));
            } 
             
            actionForm.clearGlFDRList();
	    	
            
	        if (actionForm.getTableType() == null) {
	        	
	        	actionForm.setConversionDate(null); 
	        	actionForm.setCurrency(Constants.GLOBAL_BLANK);
	        	actionForm.setGoButton(null);
	        
	        	actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	            
	        	
	            actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
	        } else {
	        	
	        	try{
	        		
	        	   actionForm.clearGlFDRList();
			       ArrayList ejbGlFDRList = ejbFDR.getGlFrByCriteria(actionForm.getCriteria(), 
			       		new Integer(actionForm.getLineCount()), new Integer(Constants.GLOBAL_MAX_LINES + 1), 
						user.getCmpCode());
			       
			       // check if prev should be disabled
		           if (actionForm.getLineCount() == 0) {
		            	
		              actionForm.setDisablePreviousButton(true);
		              actionForm.setDisableFirstButton(true);
		            	
		           } else {
		           	
		           	  actionForm.setDisablePreviousButton(false);
		           	  actionForm.setDisableFirstButton(false);
		           	
		           }
		           
		           // check if next should be disabled
		           if (ejbGlFDRList.size() <= Constants.GLOBAL_MAX_LINES) {
		           	  
		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);
		           	  
		           } else {
		           	  
		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);
		           	  
		           	  //remove last record
		           	  ejbGlFDRList.remove(ejbGlFDRList.size() - 1);
		           	
		           }
			       
			       Iterator i = ejbGlFDRList.iterator();
	               while(i.hasNext()){
		           		GlModFunctionalCurrencyRateDetails mdetails = (GlModFunctionalCurrencyRateDetails)i.next();
		           		
		           		GlFindDailyRateList glFDRList = new GlFindDailyRateList(actionForm,
		           		    mdetails.getFrCode(), 
		           		    Common.convertSQLDateToString(mdetails.getFrDate()),
		           		    mdetails.getFrFcName(),
		           		    Common.convertDoubleToStringMoney(mdetails.getFrXToUsd(), Constants.CONVERSION_RATE_PRECISION),
		           		    Common.convertDoubleToStringMoney(
			         			(1/mdetails.getFrXToUsd()), Constants.CONVERSION_RATE_PRECISION));
		           		    
				  	
				  	    actionForm.saveGlFDRList(glFDRList);
				   }
				   
			    }catch(GlobalNoRecordFoundException ex){		    			       		       
			       // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	               
				   errors.add(ActionMessages.GLOBAL_MESSAGE,
			         new ActionMessage("findDailyRate.error.noRecordFound"));
			         
			    }catch(EJBException ex){
			       if(log.isInfoEnabled()){
			         log.info("EJBException caught in GlFindDailyRateAction.execute(): " + ex.getMessage() +
			        " session: " + session.getId());
		           }
		           return(mapping.findForward("cmnErrorPage"));
	            }
	        	
	        }
            
            return(mapping.findForward("glFindDailyRate"));

         }else{
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()){
             log.info("Exception caught in GlFindDailyRateAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
