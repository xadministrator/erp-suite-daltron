package com.struts.gl.finddailyrate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFindDailyRateForm extends ActionForm implements Serializable {

   private String conversionDate = null;
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String goButton = null;
   private String closeButton = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();
   
   private ArrayList glFDRList = new ArrayList();
   private int rowSelected = 0;   
   private String userPermission = new String();
   private String txnStatus = new String();
      
   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public GlFindDailyRateList getGlFDRByIndex(int index){
      return((GlFindDailyRateList)glFDRList.get(index));
   }

   public Object[] getGlFDRList(){
      return(glFDRList.toArray());
   }

   public int getGlFDRListSize(){
      return(glFDRList.size());
   }

   public void saveGlFDRList(Object newGlFDRList){
      glFDRList.add(newGlFDRList);
   }

   public void clearGlFDRList(){
      glFDRList.clear();
   }

   public void setRowSelected(Object selectedGlFDRList, boolean isEdit){
      this.rowSelected = glFDRList.indexOf(selectedGlFDRList);
   }

   public void updateGlFDRRow(int rowSelected, Object newGlFDRList){
      glFDRList.set(rowSelected, newGlFDRList);
   }

   public void deleteGlFDRList(int rowSelected){
      glFDRList.remove(rowSelected);
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getConversionDate(){
	   return(conversionDate);
	}

	public void setConversionDate(String conversionDate){
	   this.conversionDate = conversionDate;
	}

	public String getCurrency(){
	   return(currency);
	}

	public void setCurrency(String currency){
	   this.currency = currency;
	}

	public ArrayList getCurrencyList(){
	   return(currencyList);
	}

	public void setCurrencyList(String currency){
	   currencyList.add(currency);
	}
	
	public void clearCurrencyList() {
		
		currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);
		
	}

	   
    public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
    }
   
    public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
    }
   
    public boolean getDisableNextButton() {
   
      return disableNextButton;
   
    }
   
    public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
    }
    
    public boolean getDisableFirstButton() {
    	
    	return disableFirstButton;
    	
    }
       
    public void setDisableFirstButton(boolean disableFirstButton) {
    	
    	this.disableFirstButton = disableFirstButton;
    	
    }
    
    public boolean getDisableLastButton() {
    	
    	return disableLastButton;
    	
    }
    
    public void setDisableLastButton(boolean disableLastButton) {
    	
    	this.disableLastButton = disableLastButton;
    	
    }
    
    public HashMap getCriteria() {
   	
   	   return criteria;
   	
    }
   
    public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
    } 

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
    	
      closeButton = null;
      showDetailsButton = null;
	  hideDetailsButton = null;      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      
      if(request.getParameter("goButton") != null){
	     if(!Common.validateDateFormat(conversionDate)){
	        errors.add("conversionDate", new ActionMessage("findDailyRate.error.conversionDateInvalid"));
	     }
	  }
      return(errors);
   }
}
