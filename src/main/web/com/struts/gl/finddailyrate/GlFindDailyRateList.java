package com.struts.gl.finddailyrate;

import java.io.Serializable;

public class GlFindDailyRateList implements Serializable {

   private Integer dailyRatesCode = null;
   private String conversionDate = null;
   private String currency = null;
   private String conversionRate = null;
   private String inverseRate = null;

   private String openButton = null;
    
   private GlFindDailyRateForm parentBean;
    
   public GlFindDailyRateList(GlFindDailyRateForm parentBean,
      Integer dailyRatesCode,
      String conversionDate,
      String currency,
      String conversionRate,
      String inverseRate){

      this.parentBean = parentBean;
      this.dailyRatesCode = dailyRatesCode;
      this.conversionDate = conversionDate;
      this.currency = currency;
      this.conversionRate = conversionRate;
      this.inverseRate = inverseRate;
   }

   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }

   public Integer getDailyRatesCode(){
      return(dailyRatesCode);
   }

   public String getConversionDate(){
      return(conversionDate);
   }

   public String getCurrency(){
      return(currency);
   }

   public String getConversionRate(){
      return(conversionRate);
   }

   public String getInverseRate(){
      return(inverseRate);
   }

}
