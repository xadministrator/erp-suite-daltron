package com.struts.gl.findjournal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlFindJournalController;
import com.ejb.txn.GlFindJournalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;

public final class GlFindJournalAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {
   	  
      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFindJournalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFindJournalForm actionForm = (GlFindJournalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FIND_JOURNAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFindJournal");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFindJournalController EJB
*******************************************************/

         GlFindJournalControllerHome homeFJR = null;
         GlFindJournalController ejbFJR = null;

         try {
          
            homeFJR = (GlFindJournalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFindJournalControllerEJB", GlFindJournalControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFindJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFJR = homeFJR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFindJournalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call GlFindJournalEntryController EJB
     getGlFcPrecisionUnit
  *******************************************************/
         short precisionUnit = 0;
         boolean enableJournalBatch = false; 
         
         try { 
         	
            precisionUnit = ejbFJR.getGlFcPrecisionUnit(user.getCmpCode());
            enableJournalBatch = Common.convertByteToBoolean(ejbFJR.getAdPrfEnableGlJournalBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableJournalBatch);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlFindJournalAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Gl FJR Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFindJournal"));
	     
/*******************************************************
   -- Gl FJR Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFindJournal"));         

/*******************************************************
   -- Gl FJR First Action --
*******************************************************/	     
	        
	     } else if(request.getParameter("firstButton") != null) {
	     	
	     	actionForm.setLineCount(0);
	     	
/*******************************************************
   -- Gl FJR Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Gl FJR Next Action --
*******************************************************/ 

         } else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
 
         }
         
/*******************************************************
   -- Gl FJR Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("journalName", actionForm.getName());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("category", actionForm.getCategory());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSource())) {
	        		
	        		criteria.put("source", actionForm.getSource());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getPosted())) {
	        		
	        		criteria.put("posted", actionForm.getPosted());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getApprovalStatus())) {
	        		
	        		criteria.put("approvalStatus", actionForm.getApprovalStatus());
	        		
	        	}
	        		        	
	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
           
            if (request.getParameter("lastButton") != null) {
            	
            	int size = ejbFJR.getGlJrSizeByCriteria(actionForm.getCriteria(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
			
            try {
            	
            	actionForm.clearGlFJRList();
            	
            	ArrayList list = ejbFJR.getGlJrByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		GlModJournalDetails mdetails = (GlModJournalDetails)i.next();
            		
            		GlFindJournalList jrList = new GlFindJournalList(
            			actionForm, 
            			mdetails.getJrCode(),
            			mdetails.getJrName(), 
            			mdetails.getJrDescription(),
            			Common.convertSQLDateToString(mdetails.getJrEffectiveDate()),
            			mdetails.getJrDocumentNumber(),
            			mdetails.getJrJcName(),
            			mdetails.getJrJsName(),
            			mdetails.getJrFcName(),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalDebit(), precisionUnit),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalCredit(), precisionUnit));
            			
            	   actionForm.saveGlFJRList(jrList);
            		
            	}
            	
            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findJournal.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFindJournalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFindJournal");

            }
            
            actionForm.reset(mapping, request);
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("glFindJournal")); 

/*******************************************************
   -- Gl FJR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl FJR Open Action --
*******************************************************/

         } else if (request.getParameter("glFJRList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             GlFindJournalList glJRList =
                actionForm.getGlFJRByIndex(actionForm.getRowSelected());

  	         String path = "/glJournalEntry.do?forward=1" +
			     "&journalCode=" + glJRList.getJournalCode();
		      
			   return(new ActionForward(path));

/*******************************************************
   -- Gl FJR Load Action --
*******************************************************/

         }
         
         
         if (frParam != null) {
         		
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFindJournal");

            }
            
            actionForm.clearGlFJRList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearCategoryList();           	
            	
            	list = ejbFJR.getGlJcAll(user.getCmpCode());
            	 	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCategoryList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearSourceList();           	
            	
            	list = ejbFJR.getGlJsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setSourceList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setSourceList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbFJR.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList(((GlModFunctionalCurrencyDetails)i.next()).getFcName());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbFJR.getGlOpenJbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBillEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("goButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            
	        if (actionForm.getTableType() == null || request.getParameter("findDraft") != null) {
	        	
	        	actionForm.setBatchName(Constants.GLOBAL_BLANK);
	            actionForm.setName(null);
	            actionForm.setDateFrom(null);
	            actionForm.setDateTo(null);
	            actionForm.setDocumentNumberFrom(null);
	            actionForm.setDocumentNumberTo(null);
	            actionForm.setCategory(Constants.GLOBAL_BLANK);
	            actionForm.setSource(Constants.GLOBAL_BLANK);
	            actionForm.setCurrency(Constants.GLOBAL_BLANK);
	            
	            actionForm.setPosted(Constants.GLOBAL_NO);
	            actionForm.setApprovalStatus("DRAFT");
	            actionForm.setOrderBy(Constants.GLOBAL_BLANK);  
	            
	            actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	            
	            HashMap criteria = new HashMap();
	            criteria.put("posted", actionForm.getPosted());
	            criteria.put("approvalStatus", actionForm.getApprovalStatus());
	            
	            actionForm.setCriteria(criteria);
	            
	            actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	            
	            if(request.getParameter("findDraft") != null) {
	            	
	            	return new ActionForward("/glFindJournal.do?goButton=1");
	            	
	            }
	        
            } else {
	        	
	        	try {
	        		
        			actionForm.clearGlFJRList();
                	
                	ArrayList glJrList = ejbFJR.getGlJrByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Constants.GLOBAL_MAX_LINES + 1), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
                	
                   // check if prev should be disabled
    	           if (actionForm.getLineCount() == 0) {
    	            	
    	              actionForm.setDisablePreviousButton(true);
    	              actionForm.setDisableFirstButton(true);
    	            	
    	           } else {
    	           	
    	           	  actionForm.setDisablePreviousButton(false);
    	           	  actionForm.setDisableFirstButton(false);
    	           	
    	           }
    	           
    	           // check if next should be disabled
    	           if (glJrList.size() <= Constants.GLOBAL_MAX_LINES) {
    	           	  
    	           	  actionForm.setDisableNextButton(true);
    	           	  actionForm.setDisableLastButton(true);
    	           	  
    	           } else {
    	           	  
    	           	  actionForm.setDisableNextButton(false);
    	           	  actionForm.setDisableLastButton(false);
    	           	  
    	           	  //remove last record
    	           	glJrList.remove(glJrList.size() - 1);
    	           	
    	           }
                	
                	Iterator j = glJrList.iterator();
                	
                	while (j.hasNext()) {
                		
                		GlModJournalDetails mdetails = (GlModJournalDetails)j.next();
                		
                		GlFindJournalList jrList = new GlFindJournalList(
                			actionForm, 
                			mdetails.getJrCode(),
                			mdetails.getJrName(), 
                			mdetails.getJrDescription(),
                			Common.convertSQLDateToString(mdetails.getJrEffectiveDate()),
                			mdetails.getJrDocumentNumber(),
                			mdetails.getJrJcName(),
                			mdetails.getJrJsName(),
                			mdetails.getJrFcName(),
                			Common.convertDoubleToStringMoney(mdetails.getJrTotalDebit(), precisionUnit),
                			Common.convertDoubleToStringMoney(mdetails.getJrTotalCredit(), precisionUnit));
                			
                	   actionForm.saveGlFJRList(jrList);
                		
                	}
                	
                } catch (GlobalNoRecordFoundException ex) {
                   
                   // disable prev, next, first & last buttons
    		       actionForm.setDisableNextButton(true);
                   actionForm.setDisableLastButton(true);
                   
                   if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
                   
                   errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("findJournal.error.noRecordFound"));

                } catch (EJBException ex) {

                   if (log.isInfoEnabled()) {

                      log.info("EJBException caught in GlFindJournalAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
                      return mapping.findForward("cmnErrorPage"); 
                      
                   }

                }
                
  	        }
	        
            return(mapping.findForward("glFindJournal"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFindJournalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();

          return mapping.findForward("cmnErrorPage");

       }

    }
}