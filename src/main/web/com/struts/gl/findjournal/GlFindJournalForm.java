package com.struts.gl.findjournal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFindJournalForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String name = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String source = null;
   private ArrayList sourceList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();   
   private String posted = null;
   private ArrayList postedList = new ArrayList(); 
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList(); 
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
  
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private ArrayList glFJRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
   		
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){
   	
   	  this.lineCount = lineCount;
   }

   public GlFindJournalList getGlFJRByIndex(int index){
      return((GlFindJournalList)glFJRList.get(index));
   }

   public Object[] getGlFJRList(){
      return(glFJRList.toArray());
   }

   public int getGlFJRListSize(){
      return(glFJRList.size());
   }

   public void saveGlFJRList(Object newGlFJRList){
      glFJRList.add(newGlFJRList);
   }

   public void clearGlFJRList(){
      glFJRList.clear();
   }

   public void setRowSelected(Object selectedGlFJRList, boolean isEdit){
      this.rowSelected = glFJRList.indexOf(selectedGlFJRList);
   }

   public void updateGlFJRRow(int rowSelected, Object newGlFJRList){
      glFJRList.set(rowSelected, newGlFJRList);
   }

   public void deleteGlFJRList(int rowSelected){
      glFJRList.remove(rowSelected);
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getBatchName() {
 	  return(batchName);
   }
 
   public void setBatchName(String batchName){
   
 	  this.batchName = batchName;
   }
 
   public ArrayList getBatchNameList() {
 	  return(batchNameList);
   }
 
   public void setBatchNameList(String batchName){
 	  batchNameList.add(batchName);
   }
 
   public void clearBatchNameList(){
 	  batchNameList.clear();
 	  batchNameList.add(Constants.GLOBAL_BLANK);
   }

   public String getName(){
      return(name);
   }

   public void setName(String name){
      this.name = name;
   }
   
   public String getDateFrom() {
   	  return(dateFrom);	  
   }
   
   public void setDateFrom(String dateFrom) {
   	
   	  this.dateFrom = dateFrom;
   }
   
   public String getDateTo() {
   	  return(dateTo);	  
   }
   
   public void setDateTo(String dateTo) {
   	  this.dateTo = dateTo;
   }
   
   public String getDocumentNumberFrom() {
   	  return(documentNumberFrom);
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom){
   	  this.documentNumberFrom = documentNumberFrom;
   }
   
   public String getDocumentNumberTo() {
   	  return(documentNumberTo);
   }
   
   public void setDocumentNumberTo(String documentNumberTo){
   	  this.documentNumberTo = documentNumberTo;
   }   
   
   public String getCategory() {
   	  return(category);
   }
   
   public void setCategory(String category){
   	  this.category = category;
   }
   
   public ArrayList getCategoryList() {
   	  return(categoryList);
   }
   
   public void setCategoryList(String category){
   	  categoryList.add(category);
   }
   
   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getSource() {
   	  return(source);
   }
   
   public void setSource(String source){
   	  this.source = source;
   }
   
   public ArrayList getSourceList() {
   	  return(sourceList);
   }
   
   public void setSourceList(String source){
   	  sourceList.add(source);
   }
   
   public void clearSourceList(){
   	  sourceList.clear();
   	  sourceList.add(Constants.GLOBAL_BLANK);
   }
   
   
   public String getCurrency() {
   	  return(currency);
   }
   
   public void setCurrency(String currency){
   	  this.currency = currency;
   }
   
   public ArrayList getCurrencyList() {
   	  return(currencyList);
   }
   
   public void setCurrencyList(String currency){
   	  currencyList.add(currency);
   }
   
   public void clearCurrencyList(){
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   }
      
   public String getPosted(){
   	  return(posted);
   }
   
   public void setPosted(String posted){
   	  this.posted = posted;
   }
   
   public ArrayList getPostedList(){
   	  return(postedList);
   }
   
   public String getApprovalStatus(){
   	  return(approvalStatus);
   }
   
   public void setApprovalStatus(String approvalStatus){
   	  this.approvalStatus = approvalStatus;
   }
   
   public ArrayList getApprovalStatusList(){
   	  return(approvalStatusList);
   }
   
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
      
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	   
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   } 
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
   	  
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add(Constants.GLOBAL_YES);
      postedList.add(Constants.GLOBAL_NO);
      
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("DOCUMENT NUMBER");
	      orderByList.add("CATEGORY");	      
	      orderByList.add("NAME");
	      orderByList.add("SOURCE");
	      
	  }
	  
	  showDetailsButton = null;
	  hideDetailsButton = null;
   	  
   }
   

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
       ActionErrors errors = new ActionErrors();
       if (request.getParameter("goButton") != null) {
       
	      if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("findJournal.error.dateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("findJournal.error.dateToInvalid"));
		
		  }    
       }
	       
       return(errors);
   }
}
