package com.struts.gl.findjournalbatch;

import java.io.Serializable;

public class GlFindJournalBatchList implements Serializable {

   private Integer journalBatchCode = null;
   private String batchName = null;
   private String description = null;
   private String status = null;
   private String dateCreated = null;
   private String createdBy = null;

   private String openButton = null;
    
   private GlFindJournalBatchForm parentBean;
    
   public GlFindJournalBatchList(GlFindJournalBatchForm parentBean,
      Integer journalBatchCode,
      String batchName,
      String description,
      String status,
      String dateCreated,
      String createdBy){

      this.parentBean = parentBean;
      this.journalBatchCode = journalBatchCode;
      this.batchName = batchName;
      this.description = description;
      this.status = status;
      this.dateCreated = dateCreated;
      this.createdBy = createdBy;
      
   }

   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }

   public Integer getJournalBatchCode(){
      return(journalBatchCode);
   }

   public String getBatchName(){
      return(batchName);
   }

   public String getDescription(){
      return(description);
   }

   public String getStatus(){
      return(status);
   }
   
   public String getDateCreated(){
      return(dateCreated);
   }

   public String getCreatedBy(){
      return(createdBy);
   }
   
}
