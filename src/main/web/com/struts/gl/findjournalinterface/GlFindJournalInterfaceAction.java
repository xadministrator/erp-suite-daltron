package com.struts.gl.findjournalinterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlFindJournalInterfaceController;
import com.ejb.txn.GlFindJournalInterfaceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlJournalInterfaceDetails;

public final class GlFindJournalInterfaceAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFindJournalInterfaceAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFindJournalInterfaceForm actionForm = (GlFindJournalInterfaceForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FIND_JOURNAL_INTERFACE_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFindJournalInterface");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
         ActionErrors errors = new ActionErrors();         

/*******************************************************
   Initialize GlFindJournalInterfaceController EJB
*******************************************************/

         GlFindJournalInterfaceControllerHome homeFJI = null;
         GlFindJournalInterfaceController ejbFB = null;

         try {
          
            homeFJI = (GlFindJournalInterfaceControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFindJournalInterfaceControllerEJB", GlFindJournalInterfaceControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFindJournalInterfaceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbFB = homeFJI.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFindJournalInterfaceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

/*******************************************************
   -- Gl FJI Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFindJournalInterface"));
	     
/*******************************************************
   -- Gl FJI Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFindJournalInterface"));         

/*******************************************************
  -- Gl FJI First Action --
*******************************************************/ 

	     } else if(request.getParameter("firstButton") != null){
	     	
	     	actionForm.setLineCount(0);	        
	     	
/*******************************************************
   -- Gl FJI Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Gl FJI Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 
         
/*******************************************************
   -- Gl FJI Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("firstButton") != null ||
			request.getParameter("lastButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("jriName", actionForm.getName());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDescription())) {
	        		
	        		criteria.put("jriDescription", actionForm.getDescription());
	        		
	        	}	        	
	        	
	        	if (!Common.validateRequired(actionForm.getEffectiveDate())) {
	        		
	        		criteria.put("jriEffectiveDate", Common.convertStringToSQLDate(actionForm.getEffectiveDate()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getJournalCategory())) {
	        		
	        		criteria.put("jriJournalCategory", actionForm.getJournalCategory());
	        		
	        	}	        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getJournalSource())) {
	        		
	        		criteria.put("jriJournalSource", actionForm.getJournalSource());
	        		
	        	}	        		        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getFunctionalCurrency())) {
	        		
	        		criteria.put("jriFunctionalCurrency", actionForm.getFunctionalCurrency());
	        		
	        	}	        		        		        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDateReversal())) {
	        		
	        		criteria.put("jriDateReversal", Common.convertStringToSQLDate(actionForm.getDateReversal()));
	        		
	        	}	        			        		        		        		        	
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumber())) {
	        		
	        		criteria.put("jriDocumentNumber", actionForm.getDocumentNumber());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getConversionDate())) {
	        		
	        		criteria.put("jriConversionDate", Common.convertStringToSQLDate(actionForm.getConversionDate()));
	        		
	        	}	        			  
	        	
	        	if (!Common.validateRequired(actionForm.getConversionRate())) {
	        		
	        		criteria.put("jriConversionRate", new Double(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), Constants.MONEY_RATE_PRECISION)));
	        		
	        	}	        
	        	
	        	if (actionForm.getReversed()) {
	        		
	        		criteria.put("jriReversed", new Byte(Common.convertBooleanToByte(actionForm.getReversed())));
	        		
	        	}	        			  	        				  	        	
	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	}
            
            if(request.getParameter("lastButton") != null) {
            	
            	int size = ejbFB.getGlJriSizeByCriteria(actionForm.getCriteria(), user.getCmpCode()).intValue();
            	
            	if((size % Constants.GLOBAL_MAX_LINES) != 0) {
            		
            		actionForm.setLineCount(size -(size % Constants.GLOBAL_MAX_LINES));
            		
            	} else {
            		
            		actionForm.setLineCount(size - Constants.GLOBAL_MAX_LINES);
            		
            	}
            	
            }
            
            try {
            	
            	actionForm.clearGlFJIList();
            	
            	ArrayList list = ejbFB.getGlJriByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	              actionForm.setDisableFirstButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	  actionForm.setDisableFirstButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  actionForm.setDisableLastButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  actionForm.setDisableLastButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		GlJournalInterfaceDetails details = (GlJournalInterfaceDetails)i.next();
            		
            		GlFindJournalInterfaceList glFJIList = new GlFindJournalInterfaceList(actionForm,
            		    details.getJriCode(),
            		    details.getJriName(),            		                		    
                        details.getJriDescription(),
                        details.getJriDocumentNumber(),
                        Common.convertSQLDateToString(details.getJriEffectiveDate()),
                        details.getJriJournalCategory(),
                        details.getJriJournalSource());
            		    
            		actionForm.saveGlFJIList(glFJIList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev, next, first & last buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               actionForm.setDisableFirstButton(true);
               actionForm.setDisableLastButton(true);
               
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findJournalInterface.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFindJournalInterfaceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFindJournalInterface");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("glFindJournalInterface")); 

/*******************************************************
   -- Gl FJI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl FJI Open Action --
*******************************************************/

         } else if (request.getParameter("glFJIList[" + 
            actionForm.getRowSelected() + "].openButton") != null) {
            	
             GlFindJournalInterfaceList glFJIList =
                actionForm.getGlFJIByIndex(actionForm.getRowSelected());

  	         String path = "/glJournalInterfaceMaintenance.do?forward=1" +
			     "&journalInterfaceCode=" + glFJIList.getJournalInterfaceCode();
		      
			   return(new ActionForward(path));

/*******************************************************
   -- Gl FJI Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFindJournalInterface");

            }
            
            actionForm.clearGlFJIList();
                        
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("goButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            
	        if (actionForm.getTableType() == null) {
	        	
	        	actionForm.setName(null);
	        	actionForm.setDescription(null);
	        	actionForm.setDocumentNumber(null);
	        	actionForm.setEffectiveDate(null);
	        	actionForm.setJournalCategory(null);
	        	actionForm.setJournalSource(null);
	        	actionForm.setConversionDate(null);
	        	actionForm.setConversionRate(null);
	        	actionForm.setFunctionalCurrency(null);
	        	actionForm.setReversed(false);
	        	actionForm.setDateReversal(null);
	        	actionForm.setOrderBy(Constants.GL_FJI_ORDER_BY_NAME);
	        	actionForm.setGoButton(null);
	        	
	        	actionForm.setLineCount(0);
	            actionForm.setDisableNextButton(true);
	            actionForm.setDisablePreviousButton(true);
	            actionForm.setDisableFirstButton(true);
	            actionForm.setDisableLastButton(true);
	            actionForm.reset(mapping, request);
	            
	            HashMap criteria = new HashMap();
	            criteria.put("jriReversed", new Byte((byte)0));
	            
	            actionForm.setCriteria(criteria);
	            
	        	actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
	        } else {
	        	
	        	HashMap c = actionForm.getCriteria();
            	
            	if(c.containsKey("jriReversed")) {
            		
            		Byte b = (Byte)c.get("jriReversed");
            		
            		actionForm.setReversed(Common.convertByteToBoolean(b.byteValue()));
            		
            	}
	        	
	        	try {
	            	
	            	actionForm.clearGlFJIList();
	            	
	            	ArrayList list = ejbFB.getGlJriByCriteria(actionForm.getCriteria(),
	            	    actionForm.getOrderBy(),
	            	    new Integer(actionForm.getLineCount()), 
	            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), user.getCmpCode());
	            	
	            	// check if prev should be disabled
		           if (actionForm.getLineCount() == 0) {
		            	
		              actionForm.setDisablePreviousButton(true);
		              actionForm.setDisableFirstButton(true);
		            	
		           } else {
		           	
		           	  actionForm.setDisablePreviousButton(false);
		           	  actionForm.setDisableFirstButton(false);
		           	
		           }
		           
		           // check if next should be disabled
		           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
		           	  
		           	  actionForm.setDisableNextButton(true);
		           	  actionForm.setDisableLastButton(true);
		           	  
		           } else {
		           	  
		           	  actionForm.setDisableNextButton(false);
		           	  actionForm.setDisableLastButton(false);
		           	  
		           	  //remove last record
		           	  list.remove(list.size() - 1);
		           	
		           }
	            	
	            	Iterator i = list.iterator();
	            	
	            	while (i.hasNext()) {
	            		
	            		GlJournalInterfaceDetails details = (GlJournalInterfaceDetails)i.next();
	            		
	            		GlFindJournalInterfaceList glFJIList = new GlFindJournalInterfaceList(actionForm,
	            		    details.getJriCode(),
	            		    details.getJriName(),            		                		    
	                        details.getJriDescription(),
	                        details.getJriDocumentNumber(),
	                        Common.convertSQLDateToString(details.getJriEffectiveDate()),
	                        details.getJriJournalCategory(),
	                        details.getJriJournalSource());
	            		    
	            		actionForm.saveGlFJIList(glFJIList);
	            		
	            	}

	            } catch (GlobalNoRecordFoundException ex) {
	               
	               // disable prev, next, first & last buttons
			       actionForm.setDisableNextButton(true);
	               actionForm.setDisableLastButton(true);
	               
	               if(actionForm.getLineCount() > 0) {
                   	
                   		actionForm.setDisableFirstButton(false);
                   		actionForm.setDisablePreviousButton(false);
                   		
                   } else {
                   	
                   		actionForm.setDisableFirstButton(true);
                   		actionForm.setDisablePreviousButton(true);
                   		
                   }
	               
	               errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  new ActionMessage("findJournalInterface.error.noRecordFound"));

	            } catch (EJBException ex) {

	               if (log.isInfoEnabled()) {

	                  log.info("EJBException caught in GlFindJournalInterfaceAction.execute(): " + ex.getMessage() +
	                  " session: " + session.getId());
	                  return mapping.findForward("cmnErrorPage"); 
	                  
	               }

	            }
	        	
	        }
            return(mapping.findForward("glFindJournalInterface"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFindJournalInterfaceAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}