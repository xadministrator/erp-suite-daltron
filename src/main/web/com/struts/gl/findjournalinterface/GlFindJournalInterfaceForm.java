package com.struts.gl.findjournalinterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFindJournalInterfaceForm extends ActionForm implements Serializable {

   private String name = null;
   private String description = null;
   private String documentNumber = null;
   private String effectiveDate = null;
   private String journalCategory = null;
   private String journalSource = null;
   private String functionalCurrency = null;
   private String dateReversal = null;
   private String conversionDate = null;
   private String conversionRate = null;
   private boolean reversed = false;
      
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	   
   private String closeButton = null;
   private String goButton = null;
   private String pageState = new String();
   private ArrayList glFJIList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected() {

      return rowSelected;

   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public GlFindJournalInterfaceList getGlFJIByIndex(int index) {

      return((GlFindJournalInterfaceList)glFJIList.get(index));

   }

   public Object[] getGlFJIList() {

      return glFJIList.toArray();

   }

   public int getGlFJIListSize() {

      return glFJIList.size();

   }

   public void saveGlFJIList(Object newGlFJIList) {

      glFJIList.add(newGlFJIList);

   }

   public void clearGlFJIList() {
      glFJIList.clear();
   }

   public void setRowSelected(Object selectedGlFJIList, boolean isEdit) {

      this.rowSelected = glFJIList.indexOf(selectedGlFJIList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showApFBRow(int rowSelected) {

   }

   public void updateApFBRow(int rowSelected, Object newGlFJIList) {

      glFJIList.set(rowSelected, newGlFJIList);

   }

   public void deleteGlFJIList(int rowSelected) {

      glFJIList.remove(rowSelected);

   }
   
   public void setGoButton(String goButton) {
   	
   	  this.goButton = goButton;
   	  
   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getName() {

      return name;

   }

   public void setName(String name) {

      this.name = name;

   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	  
   }
   
   public String getEffectiveDate() {
   	
   	  return effectiveDate;
   	  
   }
   
   public void setEffectiveDate(String effectiveDate) {
   	
   	  this.effectiveDate = effectiveDate;
   	  
   }
   
   public String getJournalCategory() {
   	
   	  return journalCategory;
   	  
   }
   
   public void setJournalCategory(String journalCategory) {
   	
   	  this.journalCategory = journalCategory;
   	  
   }
   
   public String getJournalSource() {
   	
   	  return journalSource;
   	  
   }
   
   public void setJournalSource(String journalSource) {
   	
   	  this.journalSource = journalSource;
   	  
   }
   
   public String getFunctionalCurrency() {
   	
   	  return functionalCurrency;
   	  
   }
   
   public void setFunctionalCurrency(String functionalCurrency) {
   	
   	  this.functionalCurrency = functionalCurrency;
   	  
   }
   
   public String getDateReversal() {
   	
   	  return dateReversal;
   	  
   }
   
   public void setDateReversal(String dateReversal) {
   	
   	  this.dateReversal = dateReversal;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public void setDocumentNumber(String documentNumber) {
   	
   	  this.documentNumber = documentNumber;
   	  
   }
   
   public String getConversionDate() {
   	
   	  return conversionDate;
   	  
   }
   	
   public void setConversionDate(String conversionDate) {
   	
   	  this.conversionDate = conversionDate;
   	  
   }
   
   public String getConversionRate() {
   	
   	  return conversionRate;
   	  
   }
   
   public void setConversionRate(String conversionRate) {
   	
   	  this.conversionRate = conversionRate;
   	  
   }   
   
   public boolean getReversed() {
   	
   	  return reversed;
   	  
   }
   
   public void setReversed(boolean reversed) {
   	
   	  this.reversed = reversed;
   	  
   } 

   public String getOrderBy() {
   	
   	  return orderBy;
   	  
   }
   
   public void setOrderBy(String orderBy) {
   	
   	  this.orderBy = orderBy;
   	  
   }
   
   public ArrayList getOrderByList() {
   	
   	  return orderByList;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   

   public void reset(ActionMapping mapping, HttpServletRequest request) {

   	  reversed = false;
   	
      orderByList.clear();
      orderByList.add(Constants.GL_FJI_ORDER_BY_NAME);
      orderByList.add(Constants.GL_FJI_ORDER_BY_DATE);
      
      closeButton = null;
      showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

         if (!Common.validateDateFormat(effectiveDate)) {

            errors.add("effectiveDate",
               new ActionMessage("findJournalInterface.error.effectiveDateInvalid"));

         }         
         
         if (!Common.validateDateFormat(dateReversal)) {

            errors.add("dateReversal",
               new ActionMessage("findJournalInterface.error.dateReversalInvalid"));

         }                  
         
         if (!Common.validateDateFormat(conversionDate)) {

            errors.add("conversionDate",
               new ActionMessage("findJournalInterface.error.conversionDateInvalid"));

         }                           
         
         if (!Common.validateNumberFormat(conversionRate)) {
         	
         	 errors.add("conversionRate",
               new ActionMessage("findJournalInterface.error.conversionRateInvalid"));
         	
         }                                    
                  
      }
      
      return errors;

   }
   
}