package com.struts.gl.findjournalinterface;

import java.io.Serializable;

public class GlFindJournalInterfaceList implements Serializable {

   private Integer journalInterfaceCode = null;
   private String name = null;
   private String description = null;
   private String documentNumber = null;
   private String effectiveDate = null;
   private String journalCategory = null;
   private String journalSource = null;
   
   private String openButton = null;
       
   private GlFindJournalInterfaceForm parentBean;
    
   public GlFindJournalInterfaceList(GlFindJournalInterfaceForm parentBean,
      Integer journalInterfaceCode,
      String name,
      String description,
      String documentNumber,
      String effectiveDate,
      String journalCategory,
      String journalSource) {

      this.parentBean = parentBean;
      this.journalInterfaceCode = journalInterfaceCode;
      this.name = name;
      this.description = description;
      this.documentNumber = documentNumber;
      this.effectiveDate = effectiveDate;
      this.journalCategory = journalCategory;
      this.journalSource = journalSource;
      
   }

   public void setOpenButton(String openButton) {

      parentBean.setRowSelected(this, false);

   }

   public Integer getJournalInterfaceCode() {

      return journalInterfaceCode;

   }
   
   public String getName() {
   	
   	  return name;
   	  
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getEffectiveDate() {
   	
   	  return effectiveDate;
   	  
   }
   
   public String getJournalCategory() {
   	
   	  return journalCategory;
   	  
   }
   
   public String getJournalSource() {
   	
   	  return journalSource;
   	  
   }
	   
}