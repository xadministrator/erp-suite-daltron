package com.struts.gl.findrecurringjournal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFindRecurringJournalForm extends ActionForm implements Serializable {

   private String name = null;
   private String nextRunDateFrom = null;
   private String nextRunDateTo = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList(); 
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private ArrayList glFRJList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   private String firstButton = null;
   private String lastButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private boolean disableFirstButton = false;
   private boolean disableLastButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public GlFindRecurringJournalList getGlFRJByIndex(int index){
      return((GlFindRecurringJournalList)glFRJList.get(index));
   }

   public Object[] getGlFRJList(){
      return(glFRJList.toArray());
   }

   public int getGlFRJListSize(){
      return(glFRJList.size());
   }

   public void saveGlFRJList(Object newGlFRJList){
      glFRJList.add(newGlFRJList);
   }

   public void clearGlFRJList(){
      glFRJList.clear();
   }

   public void setRowSelected(Object selectedGlFRJList, boolean isEdit){
      this.rowSelected = glFRJList.indexOf(selectedGlFRJList);
   }

   public void updateGlFRJRow(int rowSelected, Object newGlFRJList){
      glFRJList.set(rowSelected, newGlFRJList);
   }

   public void deleteGlFRJList(int rowSelected){
      glFRJList.remove(rowSelected);
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   } 
   
   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getName(){
      return(name);
   }

   public void setName(String name){
      this.name = name;
   }
   
   public String getNextRunDateFrom() {
   	  return(nextRunDateFrom);	  
   }
   
   public void setNextRunDateFrom(String nextRunDateFrom) {
   	  this.nextRunDateFrom = nextRunDateFrom;
   }
   
   public String getNextRunDateTo() {
   	  return(nextRunDateTo);	  
   }
   
   public void setNextRunDateTo(String nextRunDateTo) {
   	  this.nextRunDateTo = nextRunDateTo;
   }   
      
   public String getCategory() {
   	  return(category);
   }
   
   public void setCategory(String category){
   	  this.category = category;
   }
   
   public ArrayList getCategoryList() {
   	  return(categoryList);
   }
   
   public void setCategoryList(String category){
   	  categoryList.add(category);
   }
   
   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }   
      
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }    
      
   public boolean getDisableFirstButton() {
   	
   	  return disableFirstButton;
   	
   }
   
   public void setDisableFirstButton(boolean disableFirstButton) {
   	
   	  this.disableFirstButton = disableFirstButton;
   	  
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public boolean getDisableLastButton() {
   	
   	  return disableLastButton;
   	  
   }
   
   public void setDisableLastButton(boolean disableLastButton) {
   	
   	  this.disableLastButton = disableLastButton;
   	  
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   } 
      
   public void reset(ActionMapping mapping, HttpServletRequest request){
      
      if (orderByList.isEmpty()) {      
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.GL_RJ_ORDER_BY_NAME);
	      orderByList.add(Constants.GL_RJ_ORDER_BY_NEXT_RUN_DATE);
	      orderByList.add(Constants.GL_RJ_ORDER_BY_CATEGORY);
	                       
	  }
	  
      showDetailsButton = null;
	  hideDetailsButton = null;      	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
   	
       ActionErrors errors = new ActionErrors();
       
       if (request.getParameter("goButton") != null) {
      
	       if (!Common.validateDateFormat(nextRunDateFrom)) {
	
		     errors.add("nextRunDateFrom",
		        new ActionMessage("findRecurringJournal.error.nextRunDateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(nextRunDateTo)) {
		
		     errors.add("nextRunDateTo",
		        new ActionMessage("findRecurringJournal.error.nextRunDateToInvalid"));
		
		  } 
		  
	   }  	   
	   			        
      return(errors);
   }
}
