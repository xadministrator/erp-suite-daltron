package com.struts.gl.findrecurringjournal;

import java.io.Serializable;

public class GlFindRecurringJournalList implements Serializable {

   private Integer recurringJournalCode = null;
   private String name = null;
   private String description = null;
   private String nextRunDate = null;
   private String category = null;
   private String reminderSchedule = null;
   private String lastRunDate = null;
   private String totalDebit = null;
   private String totalCredit = null;
   
   private String openButton = null;
    
   private GlFindRecurringJournalForm parentBean;
    
   public GlFindRecurringJournalList(GlFindRecurringJournalForm parentBean,
      Integer recurringJournalCode,
      String name,
      String description,
      String nextRunDate,
      String category,
      String reminderSchedule,
      String lastRunDate,
      String totalDebit,
      String totalCredit){

      this.parentBean = parentBean;
      this.recurringJournalCode = recurringJournalCode;
      this.name = name;
      this.description = description;
      this.nextRunDate = nextRunDate;
      this.category = category;
      this.reminderSchedule = reminderSchedule;
      this.lastRunDate = lastRunDate;
      this.totalDebit = totalDebit;
      this.totalCredit = totalCredit;
   }
   
   public void setOpenButton(String openButton){
      parentBean.setRowSelected(this, false);
   }
   
   public Integer getRecurringJournalCode(){
      return(recurringJournalCode);
   }

   public String getName(){
      return(name);
   }
   
   public String getDescription(){
   	  return(description);
   }

   public String getNextRunDate(){
      return(nextRunDate);
   }   
   
   public String getCategory(){
      return(category);
   }
      
   public String getReminderSchedule(){
      return(reminderSchedule);
   }
   
   public String getLastRunDate(){
   	  return(lastRunDate);
   }

   public String getTotalDebit(){
      return(totalDebit);
   }

   public String getTotalCredit(){
      return(totalCredit);
   }
      
}

