package com.struts.gl.findsegment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.txn.GlFindSegmentController;
import com.ejb.txn.GlFindSegmentControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.AdBranchDetails;
import com.util.GenModValueSetValueDetails;


public class GlFindSegmentAction extends Action {
	
	   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

	   public ActionForward execute(ActionMapping mapping, ActionForm form,
	   		HttpServletRequest request, HttpServletResponse response) throws Exception {
	   	
	   	HttpSession session = request.getSession();
	   	
	   	try{
	   		
/*******************************************************************************
 * Check if user has a session
 ******************************************************************************/
	   		
	   		User user = (User) session.getAttribute(Constants.USER_KEY);
	   		if (user != null) {
	   			if (log.isInfoEnabled()){
	   				log.info("glFindSegmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
	   						"' performed this action on session " + session.getId());
	   			}
	   		}else{
	   			if (log.isInfoEnabled()){
	   				log.info("User is not logged on in session" + session.getId());
	   			}
	   			return(mapping.findForward("adLogon"));
	   		}
	   		
	   		GlFindSegmentForm actionForm = (GlFindSegmentForm)form;
	   		
	   		String frParam = null;
	   		
	   		if (request.getParameter("child") == null && request.getParameter("childTxn") == null &&
	   				request.getParameter("childCoaGen") == null) {
	   			
	   			frParam = Common.getUserPermission(user, Constants.GL_FIND_SEGMENT_ID);
	   			
	   		} else {
	   			
	   			frParam = Constants.FULL_ACCESS;
	   			
	   		}
	   		
	   		if(frParam != null){
	   			
	   			if(frParam.trim().equals(Constants.FULL_ACCESS)){
	   				
	   				ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	   				
	   				if(!fieldErrors.isEmpty()){
	   					
	   					saveErrors(request, new ActionMessages(fieldErrors));
	   					
	   					if (request.getParameter("child") != null) {
	   						
	   						return(mapping.findForward("glFindSegment"));
	   						
	   					} else if (request.getParameter("childTxn") != null) {
	   						
	   						return(mapping.findForward("glFindSegment"));
	   						
	   					} else if (request.getParameter("childCoaGen") != null) {
	   						
	   						return(mapping.findForward("glFindSegment"));
	   						
	   					} else {
	   						
	   						return(mapping.findForward("glFindSegment"));
	   						
	   					}
	   					
	   				}
	   				
	   			}
	   			
	   			actionForm.setUserPermission(frParam.trim());
	   			
	   		} else {
	   			
	   			actionForm.setUserPermission(Constants.NO_ACCESS);
	   			
	   		}
	   		
/*******************************************************************************
 * Initialize GlFindSegmentController EJB
 ******************************************************************************/
	   		
	   		GlFindSegmentControllerHome homeFC = null;
	   		GlFindSegmentController ejbFC = null;
	   		
	   		try {
	   			
	   			homeFC = (GlFindSegmentControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/GlFindSegmentControllerEJB", GlFindSegmentControllerHome.class);
	   			
	   		} catch(NamingException e) {
	   			
	   			if(log.isInfoEnabled()){
	   				
	   				log.info("NamingException caught in GlFindSegmentAction.execute(): " + e.getMessage() +
	   						" session: " + session.getId());
	   				
	   			}
	   			
	   			return(mapping.findForward("cmnErrorPage"));
	   			
	   		}
	   		
	   		try {
	   			
	   			ejbFC = homeFC.create();
	   			
	   		} catch(CreateException e) {
	   			
	   			if(log.isInfoEnabled()){
	   				
	   				log.info("CreateException caught in GlFindSegmentAction.execute(): " + e.getMessage() +
	   						" session: " + session.getId());
	   				
	   			}
	   			
	   			return(mapping.findForward("cmnErrorPage"));
	   			
	   		}
	   		
	   		ActionErrors errors = new ActionErrors();
	   		
/*******************************************************************************
 * -- Gl FC Close Action --
 ******************************************************************************/
	   		
	   		if(request.getParameter("closeButton") != null){
	   			
	   			return(mapping.findForward("cmnMain"));
	   			
	   		} 

/*******************************************************************************
 * -- Gl FC Load Action --
 ******************************************************************************/
	   		
	   		if(frParam != null){            
	   			
	   			actionForm.clearGlFSList();
	   			ArrayList list = null;
	   			Iterator i = null;
	   			
	   			if (request.getParameter("child") != null) {
	   				
	   				// this is a child
	   				try {
	   					
	   					// get Parameters
	   					actionForm.setLineCount(0);
	   					actionForm.setValueSetName(request.getParameter("valueSetName"));
	   					actionForm.setValueSetValueDescription(request.getParameter("valueSetValueDescription"));
	   					actionForm.setSelectedCoaSegmentNumber(Common.convertStringToShort(request.getParameter("selectedCoaSegmentNumber")) + 1);
	   					
	   					int numberOfSegment = request.getParameter("genVsListSize") != null ? Integer.parseInt(request.getParameter("genVsListSize")) : 0;
	   					ArrayList vsvDescList = new ArrayList();

	   					for (int ctr = 0; ctr < numberOfSegment; ctr++){
	   						
	   						GenModValueSetValueDetails mdetails = new GenModValueSetValueDetails();
	   						
	   						mdetails.setVsvVsName(request.getParameter("genVsList[" + ctr + "].valueSetValueName"));
	   						mdetails.setVsvDescription(request.getParameter("genVsList[" + ctr + "].valueSetValueDescription"));
	   						
	   						if(!Common.validateRequired(mdetails.getVsvDescription())) {
	   							
	   							vsvDescList.add(mdetails);
	   							
	   						}
	   						
	   					}
	   					
	   					int numberOfBranches = request.getParameter("glBCOAListSize") != null ? Integer.parseInt(request.getParameter("glBCOAListSize")) : 0;
	   					ArrayList branchList = new ArrayList();

	   					for (int ctr = 0; ctr < numberOfBranches; ctr++) {
	   						
	   						AdBranchDetails mdetails = new AdBranchDetails();
	   						
	   						if(request.getParameter("glBCOAList[" + ctr + "].bcoaCode") != null) {
	   							
	   							mdetails.setBrCode(new Integer(Integer.parseInt(request.getParameter("glBCOAList[" + ctr + "].bcoaCode"))));
		   						branchList.add(mdetails);
		   						
	   						}

	   					}
	   					
	   					ArrayList newList = new ArrayList();
	   					HashMap criteria = new HashMap();
	   					criteria = actionForm.getCriteria();
	   					
	   					try {
	   						
	   						newList = ejbFC.getGlValueSetValueByCriteria(criteria, vsvDescList, (short)actionForm.getSelectedCoaSegmentNumber(),
	   								branchList, user.getCmpCode());
	   						
	   						i = newList.iterator();
	   						while(i.hasNext()) {
	   							
	   							GenModValueSetValueDetails mdetails = (GenModValueSetValueDetails)i.next();
	   							
	   							GlFindSegmentList glFSList = new GlFindSegmentList(actionForm, 
	   									mdetails.getVsvValue(),
										mdetails.getVsvDescription());
	   							
	   							
	   							actionForm.saveGlFSList(glFSList);

	   						}
	   						
	   					}  catch (GlobalNoRecordFoundException ex) {
	   						
	   						errors.add(ActionMessages.GLOBAL_MESSAGE,
	   								new ActionMessage("findSegment.error.noRecordFound"));
	   						
	   					}
	   					
	   				} catch (EJBException ex) {
	   					
	   					if (log.isInfoEnabled()) {
	   						
	   						log.info("EJBException caught in GlFindSegmentAction.execute(): " + ex.getMessage() +
	   								" session: " + session.getId());
	   						return mapping.findForward("cmnErrorPage"); 
	   						
	   					}
	   					
	   				}
	   				
	   				actionForm.setSelectedCoaSegmentNumber(Common.convertStringToShort(request.getParameter("selectedCoaSegmentNumber")));              	
	   				actionForm.setSelectedValueSetName(request.getParameter("selectedValueSetName"));
	   				actionForm.setSelectedValueSetValueDescription(request.getParameter("selectedValueSetValueDescription"));
	   				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	   				
	   			} else if(request.getParameter("childTxn") != null) {
	   					   				
	   				// this is a child
	   				try {
	   					
	   					// get Parameters
	   					actionForm.setLineCount(0);
	   					actionForm.setValueSetName(request.getParameter("valueSetName"));
	   					actionForm.setValueSetValueDescription(request.getParameter("valueSetValueDescription"));
	   					actionForm.setSelectedCoaSegmentNumber(Common.convertStringToShort(request.getParameter("selectedCoaSegmentNumber")) + 1);
	   					
	   					int numberOfSegment = request.getParameter("genVsListSize") != null ? Integer.parseInt(request.getParameter("genVsListSize")) : 0;
	   					ArrayList vsvDescList = new ArrayList();

	   					for (int ctr = 0; ctr < numberOfSegment; ctr++){
	   						
	   						GenModValueSetValueDetails mdetails = new GenModValueSetValueDetails();
	   						
	   						mdetails.setVsvVsName(request.getParameter("genVsList[" + ctr + "].valueSetValueName"));
	   						mdetails.setVsvDescription(request.getParameter("genVsList[" + ctr + "].valueSetValueDescription"));
	   						
	   						if(!Common.validateRequired(mdetails.getVsvDescription())) {
	   							
	   							vsvDescList.add(mdetails);
	   							
	   						}
	   						
	   					}
	   					
	   					ArrayList branchList = new ArrayList();
	   					
	   					AdBranchDetails details = new AdBranchDetails();
	   					details.setBrCode(new Integer(user.getCurrentBranch().getBrCode()));
	   					
	   					branchList.add(details);
	   					
	   					ArrayList newList = new ArrayList();
	   					HashMap criteria = new HashMap();
	   					criteria = actionForm.getCriteria();
	   					
	   					try {
	   						
	   						newList = ejbFC.getGlValueSetValueByCriteria(criteria, vsvDescList, (short)actionForm.getSelectedCoaSegmentNumber(),
	   								branchList, user.getCmpCode());
	   						
	   						i = newList.iterator();
	   						while(i.hasNext()) {
	   							
	   							GenModValueSetValueDetails mdetails = (GenModValueSetValueDetails)i.next();
	   							
	   							GlFindSegmentList glFSList = new GlFindSegmentList(actionForm, 
	   									mdetails.getVsvValue(),
										mdetails.getVsvDescription());
	   							
	   							
	   							actionForm.saveGlFSList(glFSList);

	   						}
	   						
	   					}  catch (GlobalNoRecordFoundException ex) {
	   						
	   						errors.add(ActionMessages.GLOBAL_MESSAGE,
	   								new ActionMessage("findSegment.error.noRecordFound"));
	   						
	   					}
	   					
	   				} catch (EJBException ex) {
	   					
	   					if (log.isInfoEnabled()) {
	   						
	   						log.info("EJBException caught in GlFindSegmentAction.execute(): " + ex.getMessage() +
	   								" session: " + session.getId());
	   						return mapping.findForward("cmnErrorPage"); 
	   						
	   					}
	   					
	   				}
	   				
	   				actionForm.setSelectedCoaSegmentNumber(Common.convertStringToShort(request.getParameter("selectedCoaSegmentNumber")));              	
	   				actionForm.setSelectedValueSetName(request.getParameter("selectedValueSetName"));
	   				actionForm.setSelectedValueSetValueDescription(request.getParameter("selectedValueSetValueDescription"));
	   				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	   			} else if(request.getParameter("childCoaGen") != null){
	   				
	   				// coa generator
	   				try {
	   					
	   					// get Parameters
	   					actionForm.setLineCount(0);
	   					actionForm.setValueSetName(request.getParameter("valueSetName"));
	   					actionForm.setValueSetValueDescription(request.getParameter("valueSetValueDescription"));

	   					ArrayList segmentList = new ArrayList();
	   					
	   					try {
	   						
	   						segmentList = ejbFC.getGenVsvAllByVsNameAndVsvDesc( actionForm.getValueSetName(), 
	   								actionForm.getValueSetValueDescription(), user.getCmpCode());
	   						
	   						i = segmentList.iterator();
	   						while(i.hasNext()) {
	   							
	   							GenModValueSetValueDetails mdetails = (GenModValueSetValueDetails)i.next();
	   							
	   							GlFindSegmentList glFSList = new GlFindSegmentList(actionForm, 
	   									mdetails.getVsvValue(),
										mdetails.getVsvDescription());
	   							
	   							
	   							actionForm.saveGlFSList(glFSList);
	   							
	   						}
	   						
	   					}  catch (GlobalNoRecordFoundException ex) {
	   						
	   						errors.add(ActionMessages.GLOBAL_MESSAGE,
	   								new ActionMessage("findSegment.error.noRecordFound"));
	   						
	   					}
	   					
	   				} catch (EJBException ex) {
	   					
	   					if (log.isInfoEnabled()) {
	   						
	   						log.info("EJBException caught in GlFindSegmentAction.execute(): " + ex.getMessage() +
	   								" session: " + session.getId());
	   						return mapping.findForward("cmnErrorPage"); 
	   						
	   					}
	   					
	   				}
	   				
	   				actionForm.setSelectedValueSetName(request.getParameter("selectedValueSetName"));
	   				actionForm.setSelectedValueSetValueDescription(request.getParameter("selectedValueSetValueDescription"));
	   				actionForm.setSelectedValueSetValue(request.getParameter("selectedValueSetValue"));
	   				actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);

	   			}
	   			
	   			if(!errors.isEmpty()) {
	   				
	   				saveErrors(request, new ActionMessages(errors));
	   				
	   			}
	   			
	   			return(mapping.findForward("glFindSegment"));
	   			
	   		} else {
	   			
	   			errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	   			saveErrors(request, new ActionMessages(errors));
	   			
	   			return(mapping.findForward("cmnMain"));
	   			
	   		}
	   		
	   	} catch(Exception e) {
	   		
/*************************************************************
 * System Failed: Forward to error page
 *************************************************************/
	   		
	   		e.printStackTrace();
	   		
	   		if(log.isInfoEnabled()){
	   			
	   			log.info("Exception caught in GlFindSegmentAction.execute(): " + e.getMessage()
	   					+ " session: " + session.getId());
	   			
	   		}
	   		
	   		return(mapping.findForward("cmnErrorPage"));
	   		
	   	}
	}                 
                      
}
                   
             
	   