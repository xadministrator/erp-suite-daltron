package com.struts.gl.findsegment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlFindSegmentForm extends ActionForm implements Serializable {
	
	private String valueSetName = null;
	private String valueSetValueDescription = null;
	
	private String goButton = null;
	private String closeButton = null;
	private String tableType = null;
	
	private int selectedCoaSegmentNumber = 0;
	private String selectedValueSetName = null;
	private String selectedValueSetValueDescription = null;
	private String selectedValueSetValue = null;
	private String isItemSelected = null;
	
	private HashMap criteria = new HashMap();
	private ArrayList glFSList = new ArrayList();

	private int lineCount = 0;
	
	private String txnStatus = new String();
    private String userPermission = new String();
    private int rowSelected = 0; 
    
    public int getRowSelected(){

    	return rowSelected;
     
    }

    public GlFindSegmentList getGlFSByIndex(int index){
        
    	return((GlFindSegmentList)glFSList.get(index));
     
    }
    
    public int getSelectedCoaSegmentNumber() {
    	
    	return selectedCoaSegmentNumber;
    	
    }
    
    public void setSelectedCoaSegmentNumber(int selectedCoaSegmentNumber) {
    	
    	this.selectedCoaSegmentNumber = selectedCoaSegmentNumber;
    	
    }
    
    public String getValueSetValueDescription() {
		
		return valueSetValueDescription;
		
	}
	
	public void setValueSetValueDescription(String valueSetValueDescription) {
		
		this.valueSetValueDescription = valueSetValueDescription;
		
	}

	public String getValueSetName() {
		
		return valueSetName;
		
	}
	
	public void setValueSetName(String valueSetName) {
		
		this.valueSetName = valueSetName;
		
	}
	
	public Object[] getGlFSList(){
	
   		return(glFSList.toArray());
	   
   }

   public int getGlFSListSize(){
	
   		return(glFSList.size());
	  
   }

   public void saveGlFSList(Object newGlFSList){

   		glFSList.add(newGlFSList);
	   
   }

   public void clearGlFSList(){

   		glFSList.clear();
	   
   }
   


   public HashMap getCriteria() {
	   	
		return criteria;
	   	
	}
	   
	public void setCriteria(HashMap criteria) {
	   	
		this.criteria = criteria;
	   	
	}

	public String getUserPermission(){
	
		return(userPermission);
	
	}

	public void setUserPermission(String userPermission){
	
		this.userPermission = userPermission;
	
	}

	public String getSelectedValueSetName() {
	   	
		return selectedValueSetName;
	   	
	}
	   
	public void setSelectedValueSetName(String selectedValueSetName) {
	   	
		this.selectedValueSetName = selectedValueSetName;
	   	
	}
	   
	public String getSelectedValueSetValueDescription() {
	   	
		return selectedValueSetValueDescription;
	   	
	}
	   
	public void setSelectedValueSetValueDescription(String selectedValueSetValueDescription) {
	   	
		this.selectedValueSetValueDescription = selectedValueSetValueDescription;
	   	
	}

	public String setGoButton() {
		
		return goButton;
		
	}
	
	public void setGoButton(String goButton) {
		
		this.goButton = goButton;
		
	}
	
	public String getCloseButton() {
		
		return closeButton;
		
	}
	
	public void setCloseButton(String closeButton) {
		
		this.closeButton = closeButton;
		
	}
	
    public int getLineCount(){

    	return lineCount;   	 
	   
    }
	   
	public void setLineCount(int lineCount){  	
	
		this.lineCount = lineCount;
	   
	}

	public String getTxnStatus(){
		
	    String passTxnStatus = txnStatus;
	    txnStatus = Constants.GLOBAL_BLANK;
	    return(passTxnStatus);
	    
	}

	public void setTxnStatus(String txnStatus){
		
	    this.txnStatus = txnStatus;
	    
	}
	
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
	public String getSelectedValueSetValue() {
	   	
		return selectedValueSetValue;
	   	
	}
	   
	public void setSelectedValueSetValue(String selectedValueSetValue) {
	   	
		this.selectedValueSetValue = selectedValueSetValue;
	   	
	}
   
   public void reset(ActionMapping mapping, HttpServletRequest request){ 

   		valueSetName = null;
   		valueSetValueDescription = null;
   		
   		for (int i=0; i<glFSList.size(); i++) {
   		  	
   		  	  GlFindSegmentList actionList = (GlFindSegmentList)glFSList.get(i);
   		  	  actionList.setIsItemEntered(null);
   		  	
   		}
   		
   }		
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
	    ActionErrors errors = new ActionErrors();
	      
	    if(request.getParameter("goButton") != null){
	    	
	    	if(!Common.validateDateFormat(valueSetValueDescription)){
		    
	    		errors.add("segment", new ActionMessage("findSegment.error.invalidSegment"));
		    
	    	}
	        
		}
	    	    
	    return(errors);
	   
	}
	
}


