package com.struts.gl.findsegment;

import java.io.Serializable;


public class GlFindSegmentList implements Serializable {
	
    private String valueSetValue = null;
	private String valueSetValueDescription = null;
	
	private String isItemEntered = null;
	private GlFindSegmentForm parentBean;
	
	public GlFindSegmentList (GlFindSegmentForm parentBean,
			String valueSetValue,
			String valueSetValueDescription) {
		
		this.parentBean = parentBean;
		this.valueSetValue = valueSetValue;
		this.valueSetValueDescription = valueSetValueDescription;
	}
	
	public void setParentBean(GlFindSegmentForm parentBean){
		
		   this.parentBean = parentBean;
		   
	}
	
	public String getValueSetValue(){

		return valueSetValue;
	
	}

	public void setValueSetValue(String valueSetValue){
	
		this.valueSetValue = valueSetValue;
		
	}

	public String getValueSetValueDescription() {
		
		return valueSetValueDescription;
		
	}
	
	public void setValueSetValueDescription(String valueSetValueDescription) {
		
		this.valueSetValueDescription = valueSetValueDescription;
		
	}
	
   public String getIsItemEntered() {
	   	
   		return isItemEntered;
	   		
   }

   public void setIsItemEntered(String isItemEntered) {
	   	
		this.isItemEntered = isItemEntered;
	   
   }

	
}