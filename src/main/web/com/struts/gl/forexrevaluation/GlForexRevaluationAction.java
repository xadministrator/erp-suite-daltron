/*
 * Created on May 15, 2006
 *
 */
package com.struts.gl.forexrevaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlFCFunctionalCurrencyAlreadyAssignedException;
import com.ejb.exception.GlFCNoFunctionalCurrencyFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.txn.GlForexRevaluationController;
import com.ejb.txn.GlForexRevaluationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GenModSegmentDetails;
import com.util.GlModAccountingCalendarValueDetails;
import com.util.GlModFunctionalCurrencyDetails;

/**
 * @author Farrah S. Garing 
 *
 */
public class GlForexRevaluationAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************************************
* Check if user has a session
******************************************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()){
					
					log.info("glForexRevaluatioAction: Company '" + user.getCompany() + "' User '" +
						user.getUserName() + "' performed this action on session " + session.getId());
					
				}
				
			} else {
				
				if (log.isInfoEnabled()){
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			GlForexRevaluationForm actionForm = (GlForexRevaluationForm)form;
			
	        String frParam = Common.getUserPermission(user, Constants.GL_FOREX_REVALUATION_ID);
			
			if(frParam != null){
				
				if(frParam.trim().equals(Constants.FULL_ACCESS)){
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					
					if(!fieldErrors.isEmpty()){
						
						saveErrors(request, new ActionMessages(fieldErrors));

						return(mapping.findForward("glForexRevaluation"));
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************************************
* Initialize GlFindSegmentController EJB
******************************************************************************/
			
			GlForexRevaluationControllerHome homeFR = null;
			GlForexRevaluationController ejbFR = null;
			
			try {
				
				homeFR = (GlForexRevaluationControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/GlForexRevaluationControllerEJB", GlForexRevaluationControllerHome.class);
				
			} catch(NamingException e) {
				
				if(log.isInfoEnabled()){
					
					log.info("NamingException caught in GlForexRevaluationAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			try {
				
				ejbFR = homeFR.create();
				
			} catch(CreateException e) {
				
				if(log.isInfoEnabled()){
					
					log.info("CreateException caught in GlForexRevaluationAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
			ActionErrors errors = new ActionErrors();
			short precisionUnit = ejbFR.getGlFcPrecisionUnit(user.getCmpCode());
			boolean isInitialPrinting = false;
			
/*******************************************************************************
* -- Gl FR Close Action --
******************************************************************************/
			
			if(request.getParameter("closeButton") != null){
				
				return(mapping.findForward("cmnMain"));

/*******************************************************************************
* -- Gl FR Revalue Action --
******************************************************************************/
			} else if(request.getParameter("revalueButton") != null){
				
				try {
					
					HashMap criteria = new HashMap();
					
		        	if (!Common.validateRequired(actionForm.getAccountFrom())) {
		        		
		        		criteria.put("accountNumberFrom", actionForm.getAccountFrom());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getAccountTo())) {
		        		
		        		criteria.put("accountNumberTo", actionForm.getAccountTo());
		        		
		        	}
		        	
		        	if (!Common.validateRequired(actionForm.getCurrency())) {
		        		
		        		criteria.put("functionalCurrency", actionForm.getCurrency());
		        		
		        	}
					
					ejbFR.executeForexRevaluation(criteria,
						Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit),
						actionForm.getUnrealizedGainLossAccount(), Common.convertStringToInt(
							actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)),
						actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')), user.getUserName(),
						actionForm.getDocumentNumber(), new Integer(user.getCurrentBranch().getBrCode()),
						user.getCmpCode());
					
				} catch (GlobalNoRecordFoundException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("forexRevaluation.error.noRecordFound"));

				} catch (GlJREffectiveDatePeriodClosedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
						new ActionMessage("forexRevaluation.error.effectiveDatePeriodClosed", ex.getMessage()));
					
				} catch (GlJREffectiveDateNoPeriodExistException ex) {
					
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("forexRevaluation.error.effectiveDateNoPeriodExist", ex.getMessage()));
					
				} catch (GlJREffectiveDateViolationException ex) {
					
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
			                  new ActionMessage("forexRevaluation.error.effectiveDateViolation", ex.getMessage()));	
					
				} catch (GlobalDocumentNumberNotUniqueException ex) {
					
	           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("forexRevaluation.error.documentNumberNotUnique", ex.getMessage()));
					
				} catch (GlFCNoFunctionalCurrencyFoundException ex) {
					
    	  			errors.add(ActionMessages.GLOBAL_MESSAGE,
    	  					new ActionMessage("forexRevaluation.error.currencyNotFound"));
					
				} catch (GlFCFunctionalCurrencyAlreadyAssignedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
    	  					new ActionMessage("forexRevaluation.error.defaultCompanyCurrencySelected"));
					
				} catch (GlCOANoChartOfAccountFoundException ex) {
					
			          errors.add(ActionMessages.GLOBAL_MESSAGE,
			                 new ActionMessage("forexRevaluation.error.chartOfAccountNotFound", ex.getMessage()));
					
				} catch (GlobalAccountNumberInvalidException ex) {
					
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("forexRevaluation.error.accountNumberInvalid"));
		               
				} catch (GlobalRecordInvalidException ex) {
					
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("forexRevaluation.error.documentNumberIsRequired"));
		               
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in GlForexRevaluationAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return (mapping.findForward("glForexRevaluation"));
					
				} 
				
/*******************************************************************************
* -- Gl FR Print Action --
******************************************************************************/			
			}else if(request.getParameter("printButton") != null){
			
				actionForm.setReport(Constants.STATUS_SUCCESS);
				
				isInitialPrinting = true;

/*******************************************************************************
* -- Gl FR Load Action --
******************************************************************************/			
			}
			if(frParam != null){  
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("glForexRevaluation");
					
				}
				
				if (!isInitialPrinting) {
					
					try {
						
						actionForm.clearCurrencyList();
						
						ArrayList list = ejbFR.getGlFcAllWithDefault(user.getCmpCode());
						
						if (list == null  || list.size() == 0) {
							
							actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
							
						} else {
							
							Iterator i = list.iterator();
							
							while (i.hasNext()) {
								
								GlModFunctionalCurrencyDetails mdetails = (GlModFunctionalCurrencyDetails)i.next();
								
								if (mdetails.getFcSob() == 1) {
									
									actionForm.setCurrencyList(mdetails.getFcName());
									actionForm.setCurrency(mdetails.getFcName());
									
								} else {
									
									actionForm.setCurrencyList(mdetails.getFcName());
									
								}
								
							}
							
						}
						
						actionForm.clearPeriodList();           	
						
						list = ejbFR.getGlAcAllEditableOpenAndFutureEntry(user.getCmpCode());
						
						if (list == null || list.size() == 0) {
							
							actionForm.setPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
							
						} else {
							
							Iterator i = list.iterator();
							
							while (i.hasNext()) {
								
								GlModAccountingCalendarValueDetails mdetails = 
									(GlModAccountingCalendarValueDetails)i.next();
								
								actionForm.setPeriodList(mdetails.getAcvPeriodPrefix() +
										"-" + mdetails.getAcvYear());
								
							}       
							
						}
						
						list = ejbFR.getGenSgAll(user.getCmpCode());
						
						if (list == null || list.size() == 0) {
							
							
						} else {
							
							Iterator i = list.iterator();
							
							actionForm.setAccountFrom("");
							actionForm.setAccountTo("");
							
							while (i.hasNext()) {
								
								GenModSegmentDetails mdetails = 
									(GenModSegmentDetails)i.next();
								
								for (int j=0; j<mdetails.getSgMaxSize(); j++) {
									
									actionForm.setAccountFrom(actionForm.getAccountFrom() + "0");
									actionForm.setAccountTo(actionForm.getAccountTo() + "Z");
									
								}
								
								if (i.hasNext()) {
									
									actionForm.setAccountFrom(actionForm.getAccountFrom() +
											String.valueOf(mdetails.getSgFlSegmentSeparator()));
									actionForm.setAccountTo(actionForm.getAccountTo() +
											String.valueOf(mdetails.getSgFlSegmentSeparator()));
									
								}
								
							}
							
						} 
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in GlForexRevaluationAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}

					}
					
					if (!errors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(errors));
						
					} else {
						
						if (request.getParameter("revalueButton") != null) {
							
							actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
							
						}
						
					}
					
					actionForm.reset(mapping, request);
					
					actionForm.setConversionRate("1.000000");
					
				}
				
				return(mapping.findForward("glForexRevaluation"));
				
			} else {
				
	            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
	            saveErrors(request, new ActionMessages(errors));

	            return(mapping.findForward("cmnMain"));
			}
				
		} catch(Exception e) {
			
/*************************************************************
* System Failed: Forward to error page
*************************************************************/
			
			e.printStackTrace();
			
			if(log.isInfoEnabled()){
				
				log.info("Exception caught in GlForexRevaluationAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
				
			}
			
			return(mapping.findForward("cmnErrorPage"));
			
		}
		
	}
	
}
