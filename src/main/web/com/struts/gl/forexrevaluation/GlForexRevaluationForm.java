/*
 * Created on May 15, 2006
 *
 */
package com.struts.gl.forexrevaluation;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

/**
 * @author Farrah S. Garing 
 *
 */
public class GlForexRevaluationForm extends ActionForm implements Serializable {
	
	private String currency = null;
	private ArrayList currencyList = new ArrayList();
	private String conversionRate = null;	
	private String accountFrom = null;
	private String accountFromDescription = null;
	private String accountTo = null;
	private String accountToDescription = null;
	private String unrealizedGainLossAccount = null;
	private String unrealizedGainLossAccountDescription = null;
	private String period = null;
	private ArrayList periodList = new ArrayList();
	private String documentNumber = null;
	
	
	private String userPermission = new String();
	private String txnStatus = new String();	
	private String report = null;

	public String getCurrency() {
		
		return currency;
		
	}
	
	public void setCurrency(String currency) {
		
		this.currency = currency;
		
	}
	
	public ArrayList getCurrencyList() {
		
		return currencyList;
		
	}
	
	public void clearCurrencyList() {
		
		currencyList.clear();
		currencyList.add(Constants.GLOBAL_BLANK);
		
	}
	
	public void setCurrencyList(String currency) {
		
		currencyList.add(currency);
		
	}
	
	public String getConversionRate() {
		
		return conversionRate;
		
	}
	
	public void setConversionRate(String conversionRate) {
		
		this.conversionRate = conversionRate;
		
	}
	
	public String getAccountFrom() {
		
		return accountFrom;
		
	}
	
	public void setAccountFrom(String accountFrom) {
		
		this.accountFrom = accountFrom;
		
	}   
	
	public String getAccountFromDescription() {
		
		return accountFromDescription;
		
	}
	
	public void setAccountFromDescription(String accountFromDescription) {
		
		this.accountFromDescription = accountFromDescription;
		
	} 
	
	public String getAccountTo() {
		
		return accountTo;
		
	}
	
	public void setAccountTo(String accountTo) {
		
		this.accountTo = accountTo;
		
	}   
	
	public String getAccountToDescription() {
		
		return accountToDescription;
		
	}
	
	public void setAccountToDescription(String accountToDescription) {
		
		this.accountToDescription = accountToDescription;
		
	}
	
	public String getUnrealizedGainLossAccount() {
		
		return unrealizedGainLossAccount;
		
	}
	
	public void setUnrealizedGainLossAccount(String unrealizedGainLossAccount) {
		
		this.unrealizedGainLossAccount = unrealizedGainLossAccount;
		
	}   
	
	public String getUnrealizedGainLossAccountDescription() {
		
		return unrealizedGainLossAccountDescription;
		
	}
	
	public void setUnrealizedGainLossAccountDescription(String unrealizedGainLossAccountDescription) {
		
		this.unrealizedGainLossAccountDescription = unrealizedGainLossAccountDescription;
		
	}
	
	public String getPeriod() {
		
		return period;
		
	}
	
	public void setPeriod(String period) {
		
		this.period = period;
		
	}
	
	public ArrayList getPeriodList() {
		
		return periodList;
		
	}
	
	public void setPeriodList(String period) {
		
		periodList.add(period);
		
	}
	
	public void clearPeriodList() {
		
		periodList.clear();
		periodList.add(Constants.GLOBAL_BLANK);
	}
	
	public void setDocumentNumber(String documentNumber) {
		
		this.documentNumber = documentNumber;
		
	}
	
	public String getDocumentNumber() {
		
		return documentNumber;
		
	}	
	
	public String getUserPermission(){
		
		return(userPermission);
		
	}
	
	public void setUserPermission(String userPermission){
		
		this.userPermission = userPermission;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}

	public void reset(ActionMapping mapping, HttpServletRequest request){
		
		currency = Constants.GLOBAL_BLANK;
		documentNumber = null;
		conversionRate = null;	
		accountFromDescription = null;
		accountToDescription = null;
		unrealizedGainLossAccount = null;
		unrealizedGainLossAccountDescription = null;
		period = Constants.GLOBAL_BLANK;
		report = null;
		
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
		
		ActionErrors errors = new ActionErrors();
		
		if (request.getParameter("revalueButton") != null || request.getParameter("printButton") != null) {
			
			if(Common.validateRequired(currency)|| currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)){ 
				errors.add("currency",
						new ActionMessage("forexrevaluation.error.currencyMustBeEntered"));
			}
			
			if(Common.validateRequired(conversionRate)){ 
				errors.add("conversionRate",
						new ActionMessage("forexrevaluation.error.conversionRateMustBeEntered"));
			}
			
			if(!Common.validateMoneyRateFormat(conversionRate)){
				errors.add("conversionRate",
						new ActionMessage("forexrevaluation.error.conversionRateInvalid"));
			}
			
			if(Common.validateRequired(accountFrom)){ 
				errors.add("accountFrom",
						new ActionMessage("forexrevaluation.error.accountFromMustBeEntered"));
			}
			
			if(Common.validateRequired(accountTo)){ 
				errors.add("accountTo",
						new ActionMessage("forexrevaluation.error.accountToMustBeEntered"));
			}
			
			if(Common.validateRequired(unrealizedGainLossAccount)){ 
				errors.add("unrealizedGainLossAccount",
						new ActionMessage("forexrevaluation.error.unrealizedGainLossAccountMustBeEntered"));
			}
			
			if(Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND) ){ 
				errors.add("period",
						new ActionMessage("forexrevaluation.error.periodMustBeEntered"));
			}

		}

		return errors;
		
	}
	
}
