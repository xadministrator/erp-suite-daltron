package com.struts.gl.journalbatchcopy;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.GlJournalBatchCopyController;
import com.ejb.txn.GlJournalBatchCopyControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModJournalDetails;

public final class GlJournalBatchCopyAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlJournalBatchCopyAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlJournalBatchCopyForm actionForm = (GlJournalBatchCopyForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_BATCH_COPY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glJournalBatchCopy"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize GlJournalBatchCopyController EJB
*******************************************************/

         GlJournalBatchCopyControllerHome homeJBC = null;
         GlJournalBatchCopyController ejbJBC = null;

         try {
            
            homeJBC = (GlJournalBatchCopyControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalBatchCopyControllerEJB", GlJournalBatchCopyControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlJournalBatchCopyAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbJBC = homeJBC.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in GlJournalBatchCopyAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
  
 /*******************************************************
     Call GlJournalBatchCopy EJB
     getGlFcPrecisionUnit
  *******************************************************/
         
         short precisionUnit = 0;

         
         try { 
         	
            precisionUnit = ejbJBC.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlJournalBatchCopyAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	
         
/*******************************************************
   -- Gl JBC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glJournalBatchCopy"));
	     
/*******************************************************
   -- Gl JBC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glJournalBatchCopy"));  
	        
	     }
         
/*******************************************************
   -- Gl JBC Copy Action --
*******************************************************/

         if (request.getParameter("copyButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           
           ArrayList list = new ArrayList();
           
           for (int i=0; i<actionForm.getGlJBCListSize(); i++) {
           
              GlJournalBatchCopyList actionList = actionForm.getGlJBCByIndex(i);
              
              if (actionList.getCopy()) list.add(actionList.getJournalCode());
           
           }

           try {
           	
           	    ejbJBC.executeGlJrBatchCopy(list, actionForm.getBatchTo(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	           	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchCopy.error.transactionBatchClose")); 
           
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }

/*******************************************************
   -- Gl JBC Batch From Entered Action --
*******************************************************/

         } else if(!Common.validateRequired(request.getParameter("isBatchFromEntered"))) {
         
            try {
            
                actionForm.clearGlJBCList();
            
                ArrayList list = ejbJBC.getGlJrByJbName(actionForm.getBatchFrom(), user.getCmpCode());
                                                
                Iterator i = list.iterator();
                
                while (i.hasNext()) {
                
                   GlModJournalDetails mdetails = (GlModJournalDetails)i.next();
                   
                   GlJournalBatchCopyList actionList = new GlJournalBatchCopyList(actionForm,
                       mdetails.getJrCode(), mdetails.getJrName(),
                       mdetails.getJrDescription(), 
                       Common.convertSQLDateToString(mdetails.getJrEffectiveDate()),
                       mdetails.getJrDocumentNumber(),
                       mdetails.getJrJcName(),
                       mdetails.getJrJsName(),
                       mdetails.getJrFcName(),
                       Common.convertDoubleToStringMoney(mdetails.getJrTotalDebit(), precisionUnit),
                       Common.convertDoubleToStringMoney(mdetails.getJrTotalCredit(), precisionUnit));
                       
                   actionForm.saveGlJBCList(actionList);
                                                     
                }
                
            } catch (GlobalNoRecordFoundException ex) {
            
                errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchCopy.error.noJournalFound"));                    
            
            } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
           if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glJournalBatchCopy"));
               
           }
           
           return(mapping.findForward("glJournalBatchCopy"));

/*******************************************************
   -- Gl JBC Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Gl JBC Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glJournalBatchCopy"));
               
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearGlJBCList();
            	actionForm.clearBatchFromList();
            	actionForm.clearBatchToList();
            	
            	list = ejbJBC.getGlOpenJbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchFromList(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setBatchToList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			String batchName = (String)i.next();
            			
            		    actionForm.setBatchFromList(batchName);
            		    actionForm.setBatchToList(batchName);
            			
            		}
            		            		
            	}
            	            	
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalBatchCopyAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("copyButton") != null) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               
               }
            }

            actionForm.reset(mapping, request);
            
            if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }
            
            return(mapping.findForward("glJournalBatchCopy"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in GlJournalBatchCopyAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }
}