package com.struts.gl.journalbatchcopy;

import java.io.Serializable;

public class GlJournalBatchCopyList implements Serializable {

   private Integer journalCode = null;
   private String name = null;
   private String description = null;
   private String date = null;
   private String documentNumber = null;
   private String category = null;
   private String source = null;
   private String currency = null;
   private String totalDebit = null;
   private String totalCredit = null;

   private boolean copy = false;
    
   private GlJournalBatchCopyForm parentBean;
    
   public GlJournalBatchCopyList(GlJournalBatchCopyForm parentBean,
      Integer journalCode,
      String name,
      String description,
      String date,
      String documentNumber,
      String category,
      String source,
      String currency,
      String totalDebit,
      String totalCredit){

      this.parentBean = parentBean;
      this.journalCode = journalCode;
      this.name = name;
      this.description = description;
      this.date = date;
      this.documentNumber = documentNumber;
      this.category = category;
      this.source = source;
      this.currency = currency;
      this.totalDebit = totalDebit;
      this.totalCredit = totalCredit;
   }

   public Integer getJournalCode(){
      return(journalCode);
   }

   public String getName(){
      return(name);
   }

   public String getDescription(){
      return(description);
   }

   public String getDate(){
      return(date);
   }
   
   public String getDocumentNumber(){
      return(documentNumber);
   }

   public String getCategory(){
      return(category);
   }
   
   public String getSource(){
      return(source);
   }

   public String getCurrency(){
      return(currency);
   }   

   public String getTotalDebit(){
      return(totalDebit);
   }

   public String getTotalCredit(){
      return(totalCredit);
   }
   
   public boolean getCopy() {
   	  
   	  return copy;
   	
   }
   
   public void setCopy(boolean copy) {
   	
   	  this.copy = copy;
   	
   }

}
