package com.struts.gl.journalbatchentry;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.exception.GlobalTransactionBatchCloseException;
import com.ejb.txn.GlJournalBatchEntryController;
import com.ejb.txn.GlJournalBatchEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlJournalBatchDetails;

public final class GlJournalBatchEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlJournalBatchEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlJournalBatchEntryForm actionForm = (GlJournalBatchEntryForm)form;
	                       
         String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_BATCH_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glJournalBatchEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize GlJournalBatchEntryController EJB
*******************************************************/

         GlJournalBatchEntryControllerHome homeJB = null;
         GlJournalBatchEntryController ejbJB = null;

         try {
            
            homeJB = (GlJournalBatchEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalBatchEntryControllerEJB", GlJournalBatchEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlJournalBatchEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbJB = homeJB.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in GlJournalBatchEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl JB Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           GlJournalBatchDetails details = new GlJournalBatchDetails();
           
           details.setJbCode(actionForm.getJournalBatchCode());
           details.setJbName(actionForm.getBatchName());
           details.setJbDescription(actionForm.getDescription());
           details.setJbStatus(actionForm.getStatus());
           
           if (actionForm.getJournalBatchCode() == null) {
           	
           	   details.setJbCreatedBy(user.getUserName());
	           details.setJbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbJB.saveGlJbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.transactionBatchClose"));           	                      	
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
/*******************************************************
   -- Gl JB Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           GlJournalBatchDetails details = new GlJournalBatchDetails();
           
           details.setJbCode(actionForm.getJournalBatchCode());
           details.setJbName(actionForm.getBatchName());
           details.setJbDescription(actionForm.getDescription());
           details.setJbStatus(actionForm.getStatus());
           
           if (actionForm.getJournalBatchCode() == null) {
           	
           	   details.setJbCreatedBy(user.getUserName());
	           details.setJbDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	                      
           }
           
           
           try {
           	
           	    ejbJB.saveGlJbEntry(details, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlobalTransactionBatchCloseException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.transactionBatchClose"));           	                      	
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }         
           
           if (errors.isEmpty()) {
        	   
        	   String path = null;       
        	
    	   	   path = "/glJournalEntry.do?forwardBatch=1" +
		         "&batchName=" + actionForm.getBatchName();                	   	 
        	           	           	                      	   
        	   return(new ActionForward(path));
        	 
           }


/*******************************************************
   -- Gl JB Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbJB.deleteGlJbEntry(actionForm.getJournalBatchCode(), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.recordAlreadyDeleted"));
                    
            } catch (GlobalRecordAlreadyAssignedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.recordAlreadyAssigned"));
                    
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- Gl JB Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Gl JB Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glJournalBatchEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {            	
            	
            	if (request.getParameter("forward") != null) {
            		            			            	    
            		actionForm.setJournalBatchCode(new Integer(request.getParameter("journalBatchCode")));            		            		
            		
            		GlJournalBatchDetails details = ejbJB.getGlJbByJbCode(actionForm.getJournalBatchCode(), user.getCmpCode());
            		
            		actionForm.setBatchName(details.getJbName());
            		actionForm.setDescription(details.getJbDescription());
            		actionForm.setCreatedBy(details.getJbCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(details.getJbDateCreated()));
            		actionForm.setStatus(details.getJbStatus());            		
            					         
			        this.setFormProperties(actionForm);
			         
			        return (mapping.findForward("glJournalBatchEntry"));   			         
			         
            		
            	}            	
	                    		           			   	
            } catch(GlobalNoRecordFoundException ex) {            	            	
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalBatchEntry.error.journalBatchAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalBatchEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
			   if (request.getParameter("saveButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setJournalBatchCode(null);          
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setStatus("OPEN");
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("glJournalBatchEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in GlJournalBatchEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(GlJournalBatchEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
							
			if (actionForm.getJournalBatchCode() == null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(false);
				
			} else {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowDeleteButton(true);
				
			}									
						
		} else {
						
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowDeleteButton(false);
			
			
		}
				    
	}
	
}