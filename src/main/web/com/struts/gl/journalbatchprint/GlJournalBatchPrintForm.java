package com.struts.gl.journalbatchprint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalBatchPrintForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String name = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String source = null;
   private ArrayList sourceList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();   
   private String posted = null;
   private ArrayList postedList = new ArrayList(); 
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList(); 
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList();    
  
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private ArrayList glJBPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
    private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();
   
   private String report = null;
   private String journalEditListReport = null;  
   private ArrayList journalCodeList = null;
   
   
   public String getReport() {
   	
   	   return report;
   	
   }
   
   public void setReport(String report) {
   	
   	   this.report = report;
   	
   }
   
   public String getJournalEditListReport() {
   	
   	   return journalEditListReport;
   	
   }
   
   public void setJournalEditListReport(String journalEditListReport) {
   	
   	   this.journalEditListReport = journalEditListReport;
   	
   }
   
   public ArrayList getJournalCodeList() {   	  	
   	  return(journalCodeList);
   }
   
   public void setJournalCodeList(ArrayList journalCodeList) {
   	  this.journalCodeList = journalCodeList;
   }

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }

   public GlJournalBatchPrintList getGlJBPByIndex(int index){
      return((GlJournalBatchPrintList)glJBPList.get(index));
   }

   public Object[] getGlJBPList(){
      return(glJBPList.toArray());
   }

   public int getGlJBPListSize(){
      return(glJBPList.size());
   }

   public void saveGlJBPList(Object newGlJBPList){
      glJBPList.add(newGlJBPList);
   }

   public void clearGlJBPList(){
      glJBPList.clear();
   }

   public void setRowSelected(Object selectedGlJBPList, boolean isEdit){
      this.rowSelected = glJBPList.indexOf(selectedGlJBPList);
   }

   public void updateGlJBPRow(int rowSelected, Object newGlJBPList){
      glJBPList.set(rowSelected, newGlJBPList);
   }

   public void deleteGlJBPList(int rowSelected){
      glJBPList.remove(rowSelected);
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getName(){
      return(name);
   }

   public void setName(String name){
      this.name = name;
   }
   
   public String getDateFrom() {
   	  return(dateFrom);	  
   }
   
   public void setDateFrom(String dateFrom) {
   	  this.dateFrom = dateFrom;
   }
   
   public String getDateTo() {
   	  return(dateTo);	  
   }
   
   public void setDateTo(String dateTo) {
   	  this.dateTo = dateTo;
   }
   
   public String getDocumentNumberFrom() {
   	  return(documentNumberFrom);
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom){
   	  this.documentNumberFrom = documentNumberFrom;
   }
   
   public String getDocumentNumberTo() {
   	  return(documentNumberTo);
   }
   
   public void setDocumentNumberTo(String documentNumberTo){
   	  this.documentNumberTo = documentNumberTo;
   }   
   
   public String getCategory() {
   	  return(category);
   }
   
   public void setCategory(String category){
   	  this.category = category;
   }
   
   public ArrayList getCategoryList() {
   	  return(categoryList);
   }
   
   public void setCategoryList(String category){
   	  categoryList.add(category);
   }
   
   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getSource() {
   	  return(source);
   }
   
   public void setSource(String source){
   	  this.source = source;
   }
   
   public ArrayList getSourceList() {
   	  return(sourceList);
   }
   
   public void setSourceList(String source){
   	  sourceList.add(source);
   }
   
   public void clearSourceList(){
   	  sourceList.clear();
   	  sourceList.add(Constants.GLOBAL_BLANK);
   }
   
   
   public String getCurrency() {
   	  return(currency);
   }
   
   public void setCurrency(String currency){
   	  this.currency = currency;
   }
   
   public ArrayList getCurrencyList() {
   	  return(currencyList);
   }
   
   public void setCurrencyList(String currency){
   	  currencyList.add(currency);
   }
   
   public void clearCurrencyList(){
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   }
      
   public String getPosted(){
   	  return(posted);
   }
   
   public void setPosted(String posted){
   	  this.posted = posted;
   }
   
   public ArrayList getPostedList(){
   	  return(postedList);
   }
   
   public String getApprovalStatus(){
   	  return(approvalStatus);
   }
   
   public void setApprovalStatus(String approvalStatus){
   	  this.approvalStatus = approvalStatus;
   }
   
   public ArrayList getApprovalStatusList(){
   	  return(approvalStatusList);
   }
   
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }       
         
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
   	  batchName = Constants.GLOBAL_BLANK;
      name = null;
      dateFrom = null;
      dateTo = null;
      documentNumberFrom = null;
      documentNumberTo = null;
      category = Constants.GLOBAL_BLANK;
      source = Constants.GLOBAL_BLANK;
      currency = Constants.GLOBAL_BLANK;
      postedList.clear();
      postedList.add(Constants.GLOBAL_BLANK);
      postedList.add("YES");
      postedList.add("NO");
      posted = Constants.GLOBAL_NO;
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("DRAFT");
      approvalStatusList.add("N/A");
      approvalStatusList.add("PENDING");
      approvalStatusList.add("APPROVED");      
      approvalStatusList.add("REJECTED");
      approvalStatus = "DRAFT";
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add("CATEGORY");
	      orderByList.add("DOCUMENT NUMBER");
	      orderByList.add("NAME");
	      orderByList.add("SOURCE");
	      orderBy = Constants.GLOBAL_BLANK;                  
	      
	  }
	  
	  for (int i=0; i<glJBPList.size(); i++) {
	  	
	  	  GlJournalBatchPrintList actionList = (GlJournalBatchPrintList)glJBPList.get(i);
	  	  actionList.setPrint(false);
	  	
	  }
	  	  	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
       ActionErrors errors = new ActionErrors();
       
       if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("journalBatchPrint.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("journalBatchPrint.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("journalBatchPrint.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("journalBatchPrint.error.maxRowsInvalid"));
		
		   }
	 	  
      }
	       
      return(errors);
   }
}
