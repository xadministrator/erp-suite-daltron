package com.struts.gl.journalbatchsubmit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.GlJournalBatchSubmitController;
import com.ejb.txn.GlJournalBatchSubmitControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;

public final class GlJournalBatchSubmitAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlJournalBatchSubmitAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlJournalBatchSubmitForm actionForm = (GlJournalBatchSubmitForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_BATCH_SUBMIT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glJournalBatchSubmit");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlJournalBatchSubmitController EJB
*******************************************************/

         GlJournalBatchSubmitControllerHome homeJBS = null;
         GlJournalBatchSubmitController ejbJBS = null;

         try {
          
            homeJBS = (GlJournalBatchSubmitControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalBatchSubmitControllerEJB", GlJournalBatchSubmitControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlJournalBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbJBS = homeJBS.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlJournalBatchSubmitAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     Call GlJournalBatchSubmitController EJB
     getGlFcPrecisionUnit
  *******************************************************/

         short precisionUnit = 0;
         boolean enableJournalBatch = false; 
         
         try { 
         	
            precisionUnit = ejbJBS.getGlFcPrecisionUnit(user.getCmpCode());
            enableJournalBatch = Common.convertByteToBoolean(ejbJBS.getAdPrfEnableGlJournalBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableJournalBatch);
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlJournalBatchSubmitAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	

/*******************************************************
   -- Gl JBS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glJournalBatchSubmit"));
	     
/*******************************************************
   -- Gl JBS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glJournalBatchSubmit"));         

/*******************************************************
   -- Gl JBS Previous Action --
*******************************************************/ 

         } else if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Integer.parseInt(actionForm.getMaxRows()));
         	
/*******************************************************
   -- Gl JBS Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Integer.parseInt(actionForm.getMaxRows()));
         	
         } 
         
/*******************************************************
   -- Gl JBS Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap(); 
	        	
	        	if (!Common.validateRequired(actionForm.getBatchName())) {
	        		
	        		criteria.put("batchName", actionForm.getBatchName());
	        		
	        	}
                
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("journalName", actionForm.getName());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("category", actionForm.getCategory());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSource())) {
	        		
	        		criteria.put("source", actionForm.getSource());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}	        		        		        		        	
	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	        	// get query count
	        	
	        	try {
	        		
	        		ArrayList list = ejbJBS.getGlJrByCriteria(actionForm.getCriteria(),
                	    actionForm.getOrderBy(),
                	    new Integer(actionForm.getLineCount()), 
                	    new Integer(Integer.MAX_VALUE), 
                	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	        		
	        		actionForm.setQueryCount(String.valueOf(list.size()));
	        	
	        	} catch (GlobalNoRecordFoundException ex) {
	        		
	        		actionForm.setQueryCount("0");

	            } catch (EJBException ex) {

	                if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in GlJournalBatchSubmitAction.execute(): " + ex.getMessage() +
	                   " session: " + session.getId());
	                   return mapping.findForward("cmnErrorPage"); 
	                   
	                }

	            }
	        	
	     	}
            
            try {
            	
            	actionForm.clearGlJBSList();
            	
            	ArrayList list = ejbJBS.getGlJrByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		GlModJournalDetails mdetails = (GlModJournalDetails)i.next();
            		
            		String PERIOD_STATUS = null;
            		
            		if (mdetails.getJrAcvStatus() == 'F') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_FUTURE_ENTRY;
            			
            		} else if (mdetails.getJrAcvStatus() == 'O') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_OPEN;
            			
            		} else if (mdetails.getJrAcvStatus() == 'C') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_CLOSED;
            			
            		} else if (mdetails.getJrAcvStatus() == 'P') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_PERMANENTLY_CLOSED;
            			
            		}
            		
            		GlJournalBatchSubmitList jrList = new GlJournalBatchSubmitList(
            			actionForm, 
            			mdetails.getJrCode(),
            			mdetails.getJrName(), 
            			mdetails.getJrDescription(),
            			Common.convertSQLDateToString(mdetails.getJrEffectiveDate()),
            			mdetails.getJrDocumentNumber(),
            			mdetails.getJrJcName(),
            			mdetails.getJrJsName(),
            			PERIOD_STATUS,
            			mdetails.getJrFcName(),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalDebit(), precisionUnit),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalCredit(), precisionUnit));
            			
            	   actionForm.saveGlJBSList(jrList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("findJournal.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlJournalBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glJournalBatchSubmit");

            }
                        
            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            return(mapping.findForward("glJournalBatchSubmit")); 

/*******************************************************
   -- Gl JBS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl JBS Submit Action --
*******************************************************/

         } else if (request.getParameter("submitButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	
         	
            	
  	         // get posted journals
                        
		    for(int i=0; i<actionForm.getGlJBSListSize(); i++) {
		    
		       GlJournalBatchSubmitList actionList = actionForm.getGlJBSByIndex(i);
		    	
               if (actionList.getSubmit()) {
               	
               	     if (!actionList.getTotalDebit().equals(actionList.getTotalCredit())) {
               	     	
               	     	errors.add(ActionMessages.GLOBAL_MESSAGE,
  		                    new ActionMessage("journalBatchSubmit.error.journalNotBalance", actionList.getName()));
               	     	continue;
               	     	
               	     }
               	
               	     try {
               	     	
               	     	ejbJBS.executeGlJrBatchSubmit(actionList.getJournalCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteGlJBSList(i);
               	     	i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalBatchSubmit.error.recordAlreadyDeleted", actionList.getName()));
		             
		             } catch (GlobalTransactionAlreadyApprovedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalBatchSubmit.error.transactionAlreadyApproved", actionList.getName()));
		                  
		             } catch (GlobalTransactionAlreadyPendingException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalBatchSubmit.error.transactionAlreadyPending", actionList.getName()));
		                  
		             } catch (GlobalTransactionAlreadyPostedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalBatchSubmit.error.transactionAlreadyPosted", actionList.getName()));
		             
		             } catch (GlobalNoApprovalRequesterFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalBatchSubmit.error.noApprovalRequesterFound", actionList.getName()));
		                  
		             } catch (GlobalNoApprovalApproverFoundException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalBatchSubmit.error.noApprovalApproverFound", actionList.getName()));
		               
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalBatchSubmit.error.effectiveDatePeriodClosed", actionList.getName()));
		
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in GlJournalBatchSubmitAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	           
	       }
	       
	       if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glJournalBatchSubmit");

           }
	       
	       try {
	       
	            actionForm.setLineCount(0);
            	
            	actionForm.clearGlJBSList();
            	
            	ArrayList list = ejbJBS.getGlJrByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Integer.parseInt(actionForm.getMaxRows()) + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Integer.parseInt(actionForm.getMaxRows())) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		GlModJournalDetails mdetails = (GlModJournalDetails)i.next();
            		
            		String PERIOD_STATUS = null;
            		
            		if (mdetails.getJrAcvStatus() == 'F') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_FUTURE_ENTRY;
            			
            		} else if (mdetails.getJrAcvStatus() == 'O') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_OPEN;
            			
            		} else if (mdetails.getJrAcvStatus() == 'C') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_CLOSED;
            			
            		} else if (mdetails.getJrAcvStatus() == 'P') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_PERMANENTLY_CLOSED;
            			
            		}
            		
            		GlJournalBatchSubmitList jrList = new GlJournalBatchSubmitList(
            			actionForm, 
            			mdetails.getJrCode(),
            			mdetails.getJrName(), 
            			mdetails.getJrDescription(),
            			Common.convertSQLDateToString(mdetails.getJrEffectiveDate()),
            			mdetails.getJrDocumentNumber(),
            			mdetails.getJrJcName(),
            			mdetails.getJrJsName(),
            			PERIOD_STATUS,
            			mdetails.getJrFcName(),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalDebit(), precisionUnit),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalCredit(), precisionUnit));
            			
            	   actionForm.saveGlJBSList(jrList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlJournalBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
            
            return(mapping.findForward("glJournalBatchSubmit")); 
	       		      
		
/*******************************************************
   -- Gl JBS Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glJournalBatchSubmit");

            }
            
            actionForm.clearGlJBSList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearCategoryList();           	
            	
            	list = ejbJBS.getGlJcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCategoryList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearSourceList();           	
            	
            	list = ejbJBS.getGlJsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setSourceList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setSourceList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbJBS.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList(((GlModFunctionalCurrencyDetails)i.next()).getFcName());
            			
            		}
            		
            	}
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbJBS.getGlOpenJbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	} 
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlJournalBatchSubmitAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            } 
                                    
            actionForm.setLineCount(0);
            actionForm.setMaxRows(String.valueOf(Constants.GLOBAL_MAX_LINES));
            actionForm.setQueryCount(null);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            actionForm.reset(mapping, request);
                        
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            return(mapping.findForward("glJournalBatchSubmit"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlJournalBatchSubmitAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();

          return mapping.findForward("cmnErrorPage");

       }

    }
}