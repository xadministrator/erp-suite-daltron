package com.struts.gl.journalbatchsubmit;

import java.io.Serializable;

public class GlJournalBatchSubmitList implements Serializable {

   private Integer journalCode = null;
   private String name = null;
   private String description = null;
   private String date = null;
   private String documentNumber = null;
   private String category = null;
   private String source = null;
   private String periodStatus = null;
   private String currency = null;
   private String totalDebit = null;
   private String totalCredit = null;

   private boolean submit = false;
    
   private GlJournalBatchSubmitForm parentBean;
    
   public GlJournalBatchSubmitList(GlJournalBatchSubmitForm parentBean,
      Integer journalCode,
      String name,
      String description,
      String date,
      String documentNumber,
      String category,
      String source,
      String periodStatus,
      String currency,
      String totalDebit,
      String totalCredit){

      this.parentBean = parentBean;
      this.journalCode = journalCode;
      this.name = name;
      this.description = description;
      this.date = date;
      this.documentNumber = documentNumber;
      this.category = category;
      this.source = source;
      this.periodStatus = periodStatus;
      this.currency = currency;
      this.totalDebit = totalDebit;
      this.totalCredit = totalCredit;
   }
   
   public Integer getJournalCode(){
      return(journalCode);
   }

   public String getName(){
      return(name);
   }

   public String getDescription(){
      return(description);
   }

   public String getDate(){
      return(date);
   }
   
   public String getDocumentNumber(){
      return(documentNumber);
   }

   public String getCategory(){
      return(category);
   }
   
   public String getSource(){
      return(source);
   }
   
   public String getPeriodStatus(){
   	  return(periodStatus);
   }

   public String getCurrency(){
      return(currency);
   }   

   public String getTotalDebit(){
      return(totalDebit);
   }

   public String getTotalCredit(){
      return(totalCredit);
   }
   
   public boolean getSubmit() {
   	  return(submit);
   }

   public void setSubmit(boolean submit){
   	  this.submit = submit;   	
   }
   
}

