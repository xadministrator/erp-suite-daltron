package com.struts.gl.journalcategories;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJCJournalCategoryAlreadyAssignedException;
import com.ejb.exception.GlJCJournalCategoryAlreadyDeletedException;
import com.ejb.exception.GlJCJournalCategoryAlreadyExistException;
import com.ejb.exception.GlJCNoJournalCategoryFoundException;
import com.ejb.txn.GlJournalCategoryController;
import com.ejb.txn.GlJournalCategoryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlJournalCategoryDetails;

public final class GlJournalCategoriesAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
   
   
       HttpSession session = request.getSession();
       try{
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          if (user != null) {
             if (log.isInfoEnabled()){
                log.info("GlJournalCategoriesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             }
          }else{
             if (log.isInfoEnabled()){
	        log.info("User is not logged on in session " + session.getId());
             }
	     return(mapping.findForward("adLogon"));
          }

	  String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_CATEGORIES_ID);
          if(frParam != null){
	     if(frParam.trim().equals(Constants.FULL_ACCESS)){
	        ActionErrors fieldErrors = ((GlJournalCategoriesForm)form).validateFields(mapping, request);
	        if(!fieldErrors.isEmpty()){
                   saveErrors(request, new ActionMessages(fieldErrors));
		   return(mapping.findForward("glJournalCategories"));
	        }
             }
             ((GlJournalCategoriesForm)form).setUserPermission(frParam.trim());	
          }else{
	     ((GlJournalCategoriesForm)form).setUserPermission(Constants.NO_ACCESS);
	  }
	  
/*******************************************************
   Initialize GlJournalCategoryController EJB
*******************************************************/

          GlJournalCategoryControllerHome homeJC = null;
          GlJournalCategoryController ejbJC = null;
       
          try{
             
	     homeJC = (GlJournalCategoryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalCategoryControllerEJB", GlJournalCategoryControllerHome.class);
             
          }catch(NamingException e){
             if(log.isInfoEnabled()){
	       log.info("NamingException caught in GlJournalCategoriesAction.execute(): " + e.getMessage() + 
	          " session: " + session.getId()); 
	     }
             return(mapping.findForward("cmnErrorPage")); 
          }
       
          try{
              ejbJC = homeJC.create();
          }catch(CreateException e){
             if(log.isInfoEnabled()){
                log.info("CreateException caught in GlJournalCategoriesAction.execute(): " + e.getMessage() + 
	           " session: " + session.getId());
             }
             return(mapping.findForward("cmnErrorPage"));
          }
	  
          ActionErrors errors = new ActionErrors();
          
          
/*******************************************************
   -- Gl JC Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlJournalCategoriesForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glJournalCategories"));
	     
/*******************************************************
   -- Gl JC Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlJournalCategoriesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glJournalCategories"));                   
	  
/*******************************************************
   -- GL JC Save Action --
*******************************************************/

	  } else if(request.getParameter("saveButton") != null && 
	     ((GlJournalCategoriesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
	     GlJournalCategoryDetails details = new GlJournalCategoryDetails(
	        ((GlJournalCategoriesForm)form).getJournalCategoryName().trim().toUpperCase(),
		((GlJournalCategoriesForm)form).getDescription().trim().toUpperCase(),
		Common.getGlReversalMethodCode(((GlJournalCategoriesForm)form).getReversalMethod()));
	  
/*******************************************************
   Call GlJournalCategoryController EJB 
   addGlJcEntry method
*******************************************************/

	     try{
                ejbJC.addGlJcEntry(details, user.getCmpCode());
	     }catch(GlJCJournalCategoryAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("journalCategories.error.journalCategoryAlreadyExists"));
             }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlJournalCategoriesAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
	        }
                return(mapping.findForward("cmnErrorPage"));
	      } 


/*******************************************************
   -- GL JC Close Action --
*******************************************************/

	  }else if(request.getParameter("closeButton") != null){
	     return(mapping.findForward("cmnMain"));
	      
/*******************************************************
   -- GL JC Update Action --
*******************************************************/

	  }else if(request.getParameter("updateButton") != null && 
	     ((GlJournalCategoriesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     GlJournalCategoriesList glJCList = 
	        ((GlJournalCategoriesForm)form).getGlJCByIndex(((GlJournalCategoriesForm) form).getRowSelected());
	     
	     if (!((GlJournalCategoriesForm)form).getJournalCategoryName().equals(glJCList.getJournalCategoryName()) && (glJCList.getJournalCategoryName().equals("ACCOUNTS PAYABLES") ||
	    	glJCList.getJournalCategoryName().equals("BANK ADJUSTMENTS") ||
	    	glJCList.getJournalCategoryName().equals("BANK CHARGES") ||
	    	glJCList.getJournalCategoryName().equals("CHECKS") ||
	    	glJCList.getJournalCategoryName().equals("CREDIT MEMOS") ||
	    	glJCList.getJournalCategoryName().equals("DEBIT MEMOS") ||
	    	glJCList.getJournalCategoryName().equals("DEPOSITS") ||
	    	glJCList.getJournalCategoryName().equals("FINANCE CHARGES") ||
	    	glJCList.getJournalCategoryName().equals("FUND TRANSFERS") ||
	    	glJCList.getJournalCategoryName().equals("GENERAL") ||
	    	glJCList.getJournalCategoryName().equals("SALES INVOICES") ||
	    	glJCList.getJournalCategoryName().equals("SALES RECEIPTS") ||
	    	glJCList.getJournalCategoryName().equals("VOUCHERS") ||
			glJCList.getJournalCategoryName().equals("INVENTORY ADJUSTMENTS") ||
			glJCList.getJournalCategoryName().equals("INVENTORY ASSEMBLIES") ||
			glJCList.getJournalCategoryName().equals("ASSEMBLY TRANSFERS") ||
			glJCList.getJournalCategoryName().equals("STOCK ISSUANCES") ||
			glJCList.getJournalCategoryName().equals("RECEIVING ITEMS"))) {
	    	
	    	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("journalCategories.error.systemKeywordError"));
	    	
	    	saveErrors(request, new ActionMessages(errors));
            return(mapping.findForward("glJournalCategories"));
            
	    }
	    
	     GlJournalCategoryDetails details = new GlJournalCategoryDetails(
	        glJCList.getJournalCategoryCode(),
		((GlJournalCategoriesForm)form).getJournalCategoryName().trim().toUpperCase(),
                ((GlJournalCategoriesForm)form).getDescription().trim().toUpperCase(),
                Common.getGlReversalMethodCode(((GlJournalCategoriesForm)form).getReversalMethod()));
	       
/*******************************************************
   Call GlJournalCategoryController EJB updateGlJcEntry
   method
*******************************************************/

	     try {
                ejbJC.updateGlJcEntry(details, user.getCmpCode());
             }catch(GlJCJournalCategoryAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("journalCategories.error.journalCategoryAlreadyExists"));
	     }catch(GlJCJournalCategoryAlreadyAssignedException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("journalCategories.error.updateJournalCategoryAlreadyAssigned"));
             }catch(GlJCJournalCategoryAlreadyDeletedException ex){
                errors.add(ActionMessages.GLOBAL_MESSAGE, 
		   new ActionMessage("journalCategories.error.journalCategoryAlreadyDeleted"));
	     }catch(EJBException ex){
	        if(log.isInfoEnabled()){
		   log.info("EJBException caught in GlJournalCategoriesAction.execute(): " + ex.getMessage() +
	              " session: " + session.getId());
	        }
	        return(mapping.findForward("cmnErrorPage"));
	     }
	     
/*******************************************************
   -- GL JC Cancel Action --
*******************************************************/

	  }else if(request.getParameter("cancelButton") != null){
	       
/*******************************************************
   -- GL JC Edit Action --
*******************************************************/

	  }else if(request.getParameter("glJCList[" + 
             ((GlJournalCategoriesForm) form).getRowSelected() + "].editButton") != null && 
	     ((GlJournalCategoriesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){	     
	     
/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlJournalCategoriesForm properties
*******************************************************/

	        ((GlJournalCategoriesForm) form).showGlJCRow(((GlJournalCategoriesForm) form).getRowSelected());
		   
	         return(mapping.findForward("glJournalCategories"));
		     
/*******************************************************
  -- GL JC Delete Action --
*******************************************************/

	   }else if(request.getParameter("glJCList[" + 
	      ((GlJournalCategoriesForm) form).getRowSelected() + "].deleteButton") != null && 
	      ((GlJournalCategoriesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	      
	      GlJournalCategoriesList glJCList =
                 ((GlJournalCategoriesForm)form).getGlJCByIndex(((GlJournalCategoriesForm) form).getRowSelected());
                 
          if (glJCList.getJournalCategoryName().equals("ACCOUNTS PAYABLES") ||
	    	glJCList.getJournalCategoryName().equals("BANK ADJUSTMENTS") ||
	    	glJCList.getJournalCategoryName().equals("BANK CHARGES") ||
	    	glJCList.getJournalCategoryName().equals("CHECKS") ||
	    	glJCList.getJournalCategoryName().equals("CREDIT MEMOS") ||
	    	glJCList.getJournalCategoryName().equals("DEBIT MEMOS") ||
	    	glJCList.getJournalCategoryName().equals("DEPOSITS") ||
	    	glJCList.getJournalCategoryName().equals("FINANCE CHARGES") ||
	    	glJCList.getJournalCategoryName().equals("FUND TRANSFERS") ||
	    	glJCList.getJournalCategoryName().equals("GENERAL") ||
	    	glJCList.getJournalCategoryName().equals("SALES INVOICES") ||
	    	glJCList.getJournalCategoryName().equals("SALES RECEIPTS") ||
	    	glJCList.getJournalCategoryName().equals("VOUCHERS") ||
			glJCList.getJournalCategoryName().equals("INVENTORY ADJUSTMENTS") ||
			glJCList.getJournalCategoryName().equals("INVENTORY ASSEMBLIES") ||
			glJCList.getJournalCategoryName().equals("ASSEMBLY TRANSFERS") ||
			glJCList.getJournalCategoryName().equals("STOCK ISSUANCES") ||
			glJCList.getJournalCategoryName().equals("RECEIVING ITEMS")){
	    	
	    	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("journalCategories.error.systemKeywordError"));
	    	
	    	saveErrors(request, new ActionMessages(errors));
            return(mapping.findForward("glJournalCategories"));
            
	    }

/*******************************************************
   Call GlJournalCategoryController EJB deleteGlPtEntry
   method
*******************************************************/

              try{
	         ejbJC.deleteGlJcEntry(glJCList.getJournalCategoryCode(), user.getCmpCode());
	      }catch(GlJCJournalCategoryAlreadyAssignedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
		    new ActionMessage("journalCategories.error.deleteJournalCategoryAlreadyAssigned"));
	      }catch(GlJCJournalCategoryAlreadyDeletedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, 
		    new ActionMessage("journalCategories.error.journalCategoryAlreadyDeleted"));
	      }catch(EJBException ex){
                 if(log.isInfoEnabled()){
                    log.info("EJBException caught in GlJournalCategoriesAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	         }
	         return(mapping.findForward("cmnErrorPage"));
	      }	      

/*******************************************************
   GL JC Load Action
*******************************************************/

	   }
	   if(frParam != null){
              if (!errors.isEmpty()) {
	         saveErrors(request, new ActionMessages(errors));
		 return(mapping.findForward("glJournalCategories"));
	      }
	      
/*******************************************************
   Call GlJournalCategoryController EJB getGlJcAll() method
   Populate ArrayList property from method ArrayList return 
*******************************************************/

	      ((GlJournalCategoriesForm)form).clearGlJCList();
              try{
	          ArrayList ejbGlJCList = ejbJC.getGlJcAll(user.getCmpCode());
		  Iterator i = ejbGlJCList.iterator();
   	          while(i.hasNext()){
		     GlJournalCategoryDetails details = (GlJournalCategoryDetails)i.next();
		     GlJournalCategoriesList glJCList = new GlJournalCategoriesList(((GlJournalCategoriesForm)form),
		        details.getJcCode(),
		        details.getJcName(),
		        details.getJcDescription(),
		        Common.getGlReversalMethodDescription(details.getJcReversalMethod()));
			
		        ((GlJournalCategoriesForm) form).saveGlJCList(glJCList);
		  }
	      }catch(GlJCNoJournalCategoryFoundException ex){
	         
	      }catch(EJBException ex){
	         if(log.isInfoEnabled()){
	            log.info("EJBException caught in GlJournalCategoriesAction.execute(): " + ex.getMessage() + 
	               " session: " + session.getId());
		 }
		 return(mapping.findForward("cmnErrorPage"));
	      }

	      if(!errors.isEmpty()){
                 saveErrors(request, new ActionMessages(errors));
              }else{
                 if((request.getParameter("saveButton") != null || request.getParameter("updateButton") != null ||
                    request.getParameter("glJCList[" +
                    ((GlJournalCategoriesForm) form).getRowSelected() + "].deleteButton") != null) &&
		    ((GlJournalCategoriesForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                    ((GlJournalCategoriesForm)form).setTxnStatus(Constants.STATUS_SUCCESS);  
		 }
	      }

	      ((GlJournalCategoriesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
	      ((GlJournalCategoriesForm)form).reset(mapping, request);
	      
	        if (((GlJournalCategoriesForm)form).getTableType() == null) {
      		      	
	           ((GlJournalCategoriesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           	      
	      
	      return(mapping.findForward("glJournalCategories"));
   	   }else{
	      errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
              saveErrors(request, new ActionMessages(errors));
		   
	      return(mapping.findForward("cmnMain"));
	   }
	    
      }catch(Exception e){

/*******************************************************
   Failed: Forward to error page
********************************************************/

         if(log.isInfoEnabled()){
	    log.info("Exception caught in GlJournalCategoriesAction.execute(): " + e.getMessage() + " session: " 
	       + session.getId());
	 }
 	return(mapping.findForward("cmnErrorPage"));
      } 
    }
}
