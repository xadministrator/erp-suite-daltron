package com.struts.gl.journalcategories;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlJournalCategoriesForm extends ActionForm implements Serializable{

	private String journalCategoryName = null;	
	private String description = null;
	private String reversalMethod = null;
	private ArrayList reversalMethodList = new ArrayList();
    private String tableType = null;

    private String showDetailsButton = null;
    private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList glJCList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected(){
	   return rowSelected;
	}
	
        public GlJournalCategoriesList getGlJCByIndex(int index){
	   return((GlJournalCategoriesList)glJCList.get(index));
	}
	public Object[] getGlJCList(){
	    return(glJCList.toArray());
	}

	public int getGlJCListSize(){
	   return(glJCList.size());
	}

	public void saveGlJCList(Object newGlJCList){
	   glJCList.add(newGlJCList);
	}

	public void clearGlJCList(){
	   glJCList.clear();
	}
	
	public void setRowSelected(Object selectedGlJCList, boolean isEdit){
	   this.rowSelected = glJCList.indexOf(selectedGlJCList);
	   if(isEdit){
	      this.pageState = Constants.PAGE_STATE_EDIT;
	   }
	}
	
	public void showGlJCRow(int rowSelected){
	   this.journalCategoryName = ((GlJournalCategoriesList)glJCList.get(rowSelected)).getJournalCategoryName();
	   this.description = ((GlJournalCategoriesList)glJCList.get(rowSelected)).getDescription();
	   this.reversalMethod = ((GlJournalCategoriesList)glJCList.get(rowSelected)).getReversalMethod();
	}

	public void updateGlJCRow(int rowSelected, Object newGlJCList){
	   glJCList.set(rowSelected, newGlJCList);
	}

	public void deleteGlJCList(int rowSelected){
	   glJCList.remove(rowSelected);
	}
	
	public void setUpdateButton(String updateButton){
	   this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton){
	   this.cancelButton = cancelButton;
	}
	
	public void setSaveButton(String saveButton){
	   this.saveButton = saveButton;
	}

	public void setCloseButton(String closeButton){
	   this.closeButton = closeButton;
	}
	
    public void setShowDetailsButton(String showDetailsButton) {
		
	   this.showDetailsButton = showDetailsButton;
		
    }
    
    public void setHideDetailsButton(String hideDetailsButton) {
    	
       this.hideDetailsButton = hideDetailsButton;
    	
    }	

	public void setPageState(String pageState){
	   this.pageState = pageState;
	}

	public String getPageState(){
	    return(pageState);
	}

        public String getTxnStatus(){
           String passTxnStatus = txnStatus;
           txnStatus = Constants.GLOBAL_BLANK;
           return(passTxnStatus);
        }

	public void setTxnStatus(String txnStatus){
	   this.txnStatus = txnStatus;
	}

        public String getJournalCategoryName(){
	   return(journalCategoryName);
	}

	public void setJournalCategoryName(String journalCategoryName){
	   this.journalCategoryName = journalCategoryName;
	}

	public String getDescription(){
	   return(description);
	}

	public void setDescription(String description){
	   this.description = description;
	}

	public String getReversalMethod(){
	   return(reversalMethod);
	}

	public void setReversalMethod(String reversalMethod){
	   this.reversalMethod = reversalMethod;
	}

        public String getUserPermission(){
	   return(userPermission);
	}

	public void setUserPermission(String userPermission){
	   this.userPermission = userPermission;
	}

	public ArrayList getReversalMethodList(){
	   return(reversalMethodList);
	}
	
    public String getTableType() {
    	
       return(tableType);
    	
    }
    
    public void setTableType(String tableType) {
    	
       this.tableType = tableType;
    	
    }	

        public void reset(ActionMapping mapping, HttpServletRequest request){
	   journalCategoryName = null;
	   description = null;
	   saveButton = null;
	   closeButton = null;
	   updateButton = null;
	   cancelButton = null;
	   showDetailsButton = null;
	   hideDetailsButton = null;	   
	   reversalMethodList.clear();
           reversalMethodList.add(Constants.GL_REVERSAL_METHOD_SWITCHDRCR);
           reversalMethodList.add(Constants.GL_REVERSAL_METHOD_CHANGESIGN);
	   reversalMethod = Constants.GL_REVERSAL_METHOD_SWITCHDRCR;
        }

        public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
           ActionErrors errors = new ActionErrors();
           if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
             if(Common.validateRequired(journalCategoryName)) {
                errors.add("journalCategoryName", new ActionMessage("journalCategories.error.journalCategoryNameRequired"));
             }
	     if(Common.validateRequired(reversalMethod)){
	        errors.add("reversalMethod", new ActionMessage("journalCategories.error.reversalMethodRequired"));
	     }
	     if(!Common.validateStringExists(reversalMethodList, reversalMethod)){
	        errors.add("reversalMethod", new ActionMessage("journalCategories.error.reversalMethodInvalid"));
	     }
	   }
	  return(errors);
	}
		
}
