package com.struts.gl.journalcategories;

import java.io.Serializable;

public class GlJournalCategoriesList implements Serializable{

	private Integer journalCategoryCode = null;
	private String journalCategoryName = null;	
	private String description = null;
	private String reversalMethod = null;
	
	private String deleteButton = null;
	private String editButton = null;
	
	private GlJournalCategoriesForm parentBean;

	public GlJournalCategoriesList(GlJournalCategoriesForm parentBean, Integer journalCategoryCode, 
	      String journalCategoryName, String description, String reversalMethod){
	   this.parentBean = parentBean;
	   this.journalCategoryCode = journalCategoryCode;
	   this.journalCategoryName = journalCategoryName;
	   this.description = description;
	   this.reversalMethod = reversalMethod;
	}

	public void setParentBean(GlJournalCategoriesForm parentBean){
	   this.parentBean = parentBean;
	}

	public void setDeleteButton(String deleteButton){
	   parentBean.setRowSelected(this, false);
	}

	public void setEditButton(String editButton){
	   parentBean.setRowSelected(this, true);
	}

	public Integer getJournalCategoryCode(){
	   return(journalCategoryCode);
	}

        public String getJournalCategoryName(){
	   return(journalCategoryName);
	}

	public String getDescription(){
	   return(description);
	}

	public String getReversalMethod(){
	   return(reversalMethod);
	}
}
