package com.struts.gl.journalentry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.GlJLChartOfAccountNotFoundException;
import com.ejb.exception.GlJLChartOfAccountPermissionDeniedException;
import com.ejb.exception.GlJRDateReversalNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalConversionDateNotExistException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.txn.GlJournalEntryController;
import com.ejb.txn.GlJournalEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Image;
import com.struts.util.User;
import com.util.GlJournalDetails;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;
import com.util.GlModJournalLineDetails;

public final class GlJournalEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlJournalEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlJournalEntryForm actionForm = (GlJournalEntryForm)form;
	     
	     actionForm.setReport(null);
                           
	     //actionForm.setAttachment(null);
	     
         String frParam = null;
         
         if (request.getParameter("child") == null) {
         
         	frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_ENTRY_ID);
         
         } else {
         	
         	frParam = Constants.FULL_ACCESS;
         	
         }
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glJournalEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize GlJournalEntryController EJB
*******************************************************/

         GlJournalEntryControllerHome homeJR = null;
         GlJournalEntryController ejbJR = null;

         try {
            
            homeJR = (GlJournalEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalEntryControllerEJB", GlJournalEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlJournalEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbJR = homeJR.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in GlJournalEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();
         
/*******************************************************
   Call GlJournalEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         
         short precisionUnit = 0;
         short journalLineNumber = 0;
         boolean enableJournalBatch = false;
         boolean isInitialPrinting = false;
         boolean enableSuspensePosting = false;
         short quantityPrecision = 0;
         /*String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/gl/journalentry/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
         String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");*/
         
         try {
         	
            precisionUnit = ejbJR.getGlFcPrecisionUnit(user.getCmpCode());
            journalLineNumber = ejbJR.getAdPrfGlJournalLineNumber(user.getCmpCode());
            enableJournalBatch = Common.convertByteToBoolean(ejbJR.getAdPrfEnableGlJournalBatch(user.getCmpCode()));
            enableSuspensePosting = Common.convertByteToBoolean(ejbJR.getAdPrfEnableAllowSuspensePosting(user.getCmpCode()));
            actionForm.setShowBatchName(enableJournalBatch);
                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
											
															 
/*******************************************************
   -- Gl JR Save As Draft Action --
*******************************************************/

         if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	
           java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");               
           	
           GlJournalDetails details = new GlJournalDetails();
           
           details.setJrCode(actionForm.getJournalCode());
           details.setJrName(!Common.validateRequired(actionForm.getName()) ? 
               actionForm.getName() : 
               "JOURNAL " + formatter.format(new java.util.Date()));
           details.setJrDescription(actionForm.getDescription());
           details.setJrEffectiveDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setJrDateReversal(Common.convertStringToSQLDate(actionForm.getReversalDate()));
           details.setJrDocumentNumber(actionForm.getDocumentNumber());
           details.setJrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setJrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), (short)40));
           
           if (actionForm.getJournalCode() == null) {
           	
           	   details.setJrCreatedBy(user.getUserName());
	           details.setJrDateCreated(new java.util.Date());
	                      
           }
                                                                                                                        
           details.setJrLastModifiedBy(user.getUserName());
           details.setJrDateLastModified(new java.util.Date());
           
           ArrayList jlList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getGlJRListSize(); i++) {
           	
           	   GlJournalEntryList glJRList = actionForm.getGlJRByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(glJRList.getAccount()) &&
      	 	       Common.validateRequired(glJRList.getDebitAmount()) &&
      	 	       Common.validateRequired(glJRList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(glJRList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(glJRList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(glJRList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   GlModJournalLineDetails mdetails = new GlModJournalLineDetails();
           	   
           	   mdetails.setJlLineNumber((short)(lineNumber));
           	   mdetails.setJlDebit(isDebit);
           	   mdetails.setJlAmount(amount);
           	   mdetails.setJlCoaAccountNumber(glJRList.getAccount());
           	   mdetails.setJlDescription(glJRList.getDescription());
           	   
           	   jlList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);                     	
           
// validate attachment
           /*
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;
           
           if (!Common.validateRequired(filename1)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename1().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename1NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename1Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename1SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }
    	   
    	   if (!Common.validateRequired(filename2)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename2().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename2NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename2Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename2SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }
    	   
    	   if (!Common.validateRequired(filename3)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename3().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename3NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename3Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename3().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename3SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }
    	   
    	   if (!Common.validateRequired(filename4)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename4().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename4NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename4Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename4().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename4SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }*/

           try {
           	
           	    Integer journalCode = ejbJR.saveGlJrEntry(details, actionForm.getCategory(), 
           	        actionForm.getCurrency(), new Integer(user.getCurrentResCode()), actionForm.getSource(), 
           	        actionForm.getBatchName(), jlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	           	
           	    actionForm.setJournalCode(journalCode);
           	    
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.effectiveDateNoPeriodExist"));           	
                      	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.documentNumberNotUnique"));
           	           	
           } catch (GlJRDateReversalNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.dateReversalNoPeriodExist"));
           	
           } catch (GlJREffectiveDateViolationException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.effectiveDateViolation"));
           	
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.effectiveDatePeriodClosed"));
           	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.conversionDateNotExist"));
                               	
           } catch (GlJLChartOfAccountNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.chartOfAccountNotFound", ex.getMessage()));
           	
           } catch (GlJLChartOfAccountPermissionDeniedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.chartOfAccountPermissionDenied", ex.getMessage()));
                    
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.transactionAlreadyApproved"));
                    
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.transactionAlreadyPending"));
                    
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.transactionAlreadyPosted"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.noApprovalApproverFound"));
           	
           } catch (GlobalNoRecordFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.noRecordFound"));
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
//  save attachment
           /*
           if (!Common.validateRequired(filename1)) {     
        	   System.out.println("filename1: "+filename1);
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename1().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-1" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }
           
           if (!Common.validateRequired(filename2)) {     
        	   
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename2().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-2" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }
           
           if (!Common.validateRequired(filename3)) {     
        	   
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename3().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-3" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }
           
           if (!Common.validateRequired(filename4)) {     
        	   
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename4().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-4" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }*/
           
/*******************************************************
   -- Gl JR Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	
           java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");               
           	
           GlJournalDetails details = new GlJournalDetails();
           
           details.setJrCode(actionForm.getJournalCode());
           details.setJrName(!Common.validateRequired(actionForm.getName()) ? 
               actionForm.getName() : 
               "JOURNAL " + formatter.format(new java.util.Date()));
           details.setJrDescription(actionForm.getDescription());
           details.setJrEffectiveDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setJrDateReversal(Common.convertStringToSQLDate(actionForm.getReversalDate()));
           details.setJrDocumentNumber(actionForm.getDocumentNumber());
           details.setJrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setJrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), (short)40));
           
           if (actionForm.getJournalCode() == null) {
           	
           	   details.setJrCreatedBy(user.getUserName());
	           details.setJrDateCreated(new java.util.Date());
	                      
           }
           
           details.setJrLastModifiedBy(user.getUserName());
           details.setJrDateLastModified(new java.util.Date());
           
           ArrayList jlList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getGlJRListSize(); i++) {
           	
           	   GlJournalEntryList glJRList = actionForm.getGlJRByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(glJRList.getAccount()) &&
      	 	       Common.validateRequired(glJRList.getDebitAmount()) &&
      	 	       Common.validateRequired(glJRList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(glJRList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(glJRList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(glJRList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   GlModJournalLineDetails mdetails = new GlModJournalLineDetails();
           	   
           	   mdetails.setJlLineNumber((short)(lineNumber));
           	   mdetails.setJlDebit(isDebit);
           	   mdetails.setJlAmount(amount);
           	   mdetails.setJlCoaAccountNumber(glJRList.getAccount());
           	   mdetails.setJlDescription(glJRList.getDescription());
           	
           	   jlList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           
           if (!enableSuspensePosting && (TOTAL_DEBIT != TOTAL_CREDIT)) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.journalNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("glJournalEntry"));
           	
           }
           	
// validate attachment
           /*
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;*/
           /*
           if (!Common.validateRequired(filename1)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename1().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename1NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename1Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename1().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename1SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }
    	   
    	   if (!Common.validateRequired(filename2)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename2().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename2NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename2Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename2().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename2SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }
    	   
    	   if (!Common.validateRequired(filename3)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename3().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename3NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename3Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename3().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename3SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }
    	   
    	   if (!Common.validateRequired(filename4)) {                	       	    	
    	    	    	    	
    	    	if (actionForm.getFilename4().getFileSize() == 0) {
    	    		
    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("journalEntry.error.filename4NotFound"));
            			
    	    	} else {
    	    		
    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
   	    	           	    	
           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
           	    	               	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename4Invalid"));	
                    		
           	    	}
           	    	
           	    	InputStream is = actionForm.getFilename4().getInputStream();
           	    	           	    	
           	    	if (is.available() > maxAttachmentFileSize) {
           	    		
           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
                    		new ActionMessage("journalEntry.error.filename4SizeInvalid"));
           	    		
           	    	}
           	    	
           	    	is.close();	    		
    	    		
    	    	}
    	    	
    	    	if (!errors.isEmpty()) {
    	    		
    	    		saveErrors(request, new ActionMessages(errors));
           			return (mapping.findForward("glJournalEntryForm"));
    	    		
    	    	}    	    	
    	    	
    	   }*/

           
           try {
           	
           	    Integer journalCode = ejbJR.saveGlJrEntry(details, actionForm.getCategory(), 
           	        actionForm.getCurrency(), new Integer(user.getCurrentResCode()), actionForm.getSource(), 
           	        actionForm.getBatchName(), jlList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	                   	        
           	    actionForm.setJournalCode(journalCode);
           	           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.recordAlreadyDeleted"));
                               	
           } catch (GlJREffectiveDateNoPeriodExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.effectiveDateNoPeriodExist"));           	
                      	
           } catch (GlobalDocumentNumberNotUniqueException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.documentNumberNotUnique"));
           	           	
           } catch (GlJRDateReversalNoPeriodExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.dateReversalNoPeriodExist"));
           	
           } catch (GlJREffectiveDateViolationException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.effectiveDateViolation"));
           	
           } catch (GlJREffectiveDatePeriodClosedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.effectiveDatePeriodClosed"));
           	
           } catch (GlobalConversionDateNotExistException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.conversionDateNotExist"));
                               	
           } catch (GlJLChartOfAccountNotFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.chartOfAccountNotFound", ex.getMessage()));
           	
           } catch (GlJLChartOfAccountPermissionDeniedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.chartOfAccountPermissionDenied", ex.getMessage()));
                    
           } catch (GlobalTransactionAlreadyApprovedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.transactionAlreadyApproved"));
                    
           } catch (GlobalTransactionAlreadyPendingException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.transactionAlreadyPending"));
                    
           } catch (GlobalTransactionAlreadyPostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.transactionAlreadyPosted"));
                    
           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.noApprovalRequesterFound"));
                    
           } catch (GlobalNoApprovalApproverFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.noApprovalApproverFound"));
           	   
           } catch (GlobalNoRecordFoundException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.noRecordFound"));
           	
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           }
           
//  save attachment
           /*
           if (!Common.validateRequired(filename1)) {     
        	   System.out.println("filename1: "+filename1);
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename1().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-1" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }
           
           if (!Common.validateRequired(filename2)) {     
        	   
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename2().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-2" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }
           
           if (!Common.validateRequired(filename3)) {     
        	   
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename3().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-3" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }
           
           if (!Common.validateRequired(filename4)) {     
        	   
        	   if (errors.isEmpty()) {
        		   
        		   InputStream is = actionForm.getFilename4().getInputStream();
        		   
        		   new File(attachmentPath).mkdirs();
        		   
        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-4" + attachmentFileExtension);            	           	            	
        		   
        		   int c;        
        		   
        		   while ((c = is.read()) != -1) {
        			   
        			   fos.write((byte)c);
        			   
        		   }
        		   
        		   is.close();
        		   fos.close();
        		   
        	   }          	                
        	   
           }*/
           
/*******************************************************
	 -- Gl JR Print Action --
*******************************************************/             	
            	
         } else if (request.getParameter("printButton") != null) {
            	
            if(Common.validateRequired(actionForm.getApprovalStatus())) {	
                            
                   java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSSS");               
           	
		           GlJournalDetails details = new GlJournalDetails();
		           
		           details.setJrCode(actionForm.getJournalCode());
		           details.setJrName(!Common.validateRequired(actionForm.getName()) ? 
		               actionForm.getName() : 
		               "JOURNAL " + formatter.format(new java.util.Date()));
		           details.setJrDescription(actionForm.getDescription());
		           details.setJrEffectiveDate(Common.convertStringToSQLDate(actionForm.getDate()));
		           details.setJrDateReversal(Common.convertStringToSQLDate(actionForm.getReversalDate()));
		           details.setJrDocumentNumber(actionForm.getDocumentNumber());
		           details.setJrConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
		           details.setJrConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), (short)40));
		           
		           if (actionForm.getJournalCode() == null) {
		           	
		           	   details.setJrCreatedBy(user.getUserName());
			           details.setJrDateCreated(new java.util.Date());
			                      
		           }
		           
		           details.setJrLastModifiedBy(user.getUserName());
		           details.setJrDateLastModified(new java.util.Date());
		           
		           ArrayList jlList = new ArrayList();
		           int lineNumber = 0;  
		           
		           double TOTAL_DEBIT = 0;
		           double TOTAL_CREDIT = 0;         
		                      
		           for (int i = 0; i<actionForm.getGlJRListSize(); i++) {
		           	
		           	   GlJournalEntryList glJRList = actionForm.getGlJRByIndex(i);           	   
		           	              	   
		           	   if (Common.validateRequired(glJRList.getAccount()) &&
		      	 	       Common.validateRequired(glJRList.getDebitAmount()) &&
		      	 	       Common.validateRequired(glJRList.getCreditAmount())) continue;
		           	   
		           	   byte isDebit = 0;
				       double amount = 0d;
			
				       if (!Common.validateRequired(glJRList.getDebitAmount())) {
				       	
				          isDebit = 1;
				          amount = Common.convertStringMoneyToDouble(glJRList.getDebitAmount(), precisionUnit);
				          
				          TOTAL_DEBIT += amount;
				          
				       } else {
				       	
				          isDebit = 0;
				          amount = Common.convertStringMoneyToDouble(glJRList.getCreditAmount(), precisionUnit); 
				          
				          TOTAL_CREDIT += amount;
				       }
				       
				       lineNumber++;
		
		           	   GlModJournalLineDetails mdetails = new GlModJournalLineDetails();
		           	   
		           	   mdetails.setJlLineNumber((short)(lineNumber));
		           	   mdetails.setJlDebit(isDebit);
		           	   mdetails.setJlAmount(amount);
		           	   mdetails.setJlCoaAccountNumber(glJRList.getAccount());
		           	   mdetails.setJlDescription(glJRList.getDescription());
		           	   
		           	   jlList.add(mdetails);
		           	
		           }
		           
		           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
		           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);		           	           	
		           
//		         validate attachment
		           /*
		           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;           
		           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
		           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
		           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;*/
		           /*
		           if (!Common.validateRequired(filename1)) {                	       	    	
		    	    	    	    	
		    	    	if (actionForm.getFilename1().getFileSize() == 0) {
		    	    		
		    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		            			new ActionMessage("journalEntry.error.filename1NotFound"));
		            			
		    	    	} else {
		    	    		
		    	    		String fileExtension = filename1.substring(filename1.lastIndexOf("."));
		   	    	           	    	
		           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
		           	    	               	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename1Invalid"));	
		                    		
		           	    	}
		           	    	
		           	    	InputStream is = actionForm.getFilename1().getInputStream();
		           	    	           	    	
		           	    	if (is.available() > maxAttachmentFileSize) {
		           	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename1SizeInvalid"));
		           	    		
		           	    	}
		           	    	
		           	    	is.close();	    		
		    	    		
		    	    	}
		    	    	
		    	    	if (!errors.isEmpty()) {
		    	    		
		    	    		saveErrors(request, new ActionMessages(errors));
		           			return (mapping.findForward("glJournalEntryForm"));
		    	    		
		    	    	}    	    	
		    	    	
		    	   }
		    	   
		    	   if (!Common.validateRequired(filename2)) {                	       	    	
		    	    	    	    	
		    	    	if (actionForm.getFilename2().getFileSize() == 0) {
		    	    		
		    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		            			new ActionMessage("journalEntry.error.filename2NotFound"));
		            			
		    	    	} else {
		    	    		
		    	    		String fileExtension = filename2.substring(filename2.lastIndexOf("."));
		   	    	           	    	
		           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
		           	    	               	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename2Invalid"));	
		                    		
		           	    	}
		           	    	
		           	    	InputStream is = actionForm.getFilename2().getInputStream();
		           	    	           	    	
		           	    	if (is.available() > maxAttachmentFileSize) {
		           	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename2SizeInvalid"));
		           	    		
		           	    	}
		           	    	
		           	    	is.close();	    		
		    	    		
		    	    	}
		    	    	
		    	    	if (!errors.isEmpty()) {
		    	    		
		    	    		saveErrors(request, new ActionMessages(errors));
		           			return (mapping.findForward("glJournalEntryForm"));
		    	    		
		    	    	}    	    	
		    	    	
		    	   }
		    	   
		    	   if (!Common.validateRequired(filename3)) {                	       	    	
		    	    	    	    	
		    	    	if (actionForm.getFilename3().getFileSize() == 0) {
		    	    		
		    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		            			new ActionMessage("journalEntry.error.filename3NotFound"));
		            			
		    	    	} else {
		    	    		
		    	    		String fileExtension = filename3.substring(filename3.lastIndexOf("."));
		   	    	           	    	
		           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
		           	    	               	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename3Invalid"));	
		                    		
		           	    	}
		           	    	
		           	    	InputStream is = actionForm.getFilename3().getInputStream();
		           	    	           	    	
		           	    	if (is.available() > maxAttachmentFileSize) {
		           	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename3SizeInvalid"));
		           	    		
		           	    	}
		           	    	
		           	    	is.close();	    		
		    	    		
		    	    	}
		    	    	
		    	    	if (!errors.isEmpty()) {
		    	    		
		    	    		saveErrors(request, new ActionMessages(errors));
		           			return (mapping.findForward("glJournalEntryForm"));
		    	    		
		    	    	}    	    	
		    	    	
		    	   }
		    	   
		    	   if (!Common.validateRequired(filename4)) {                	       	    	
		    	    	    	    	
		    	    	if (actionForm.getFilename4().getFileSize() == 0) {
		    	    		
		    	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		            			new ActionMessage("journalEntry.error.filename4NotFound"));
		            			
		    	    	} else {
		    	    		
		    	    		String fileExtension = filename4.substring(filename4.lastIndexOf("."));
		   	    	           	    	
		           	    	if (!fileExtension.toUpperCase().equals(attachmentFileExtension.toUpperCase())) {           	    	    	           	    	    
		           	    	               	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename4Invalid"));	
		                    		
		           	    	}
		           	    	
		           	    	InputStream is = actionForm.getFilename4().getInputStream();
		           	    	           	    	
		           	    	if (is.available() > maxAttachmentFileSize) {
		           	    		
		           	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    		new ActionMessage("journalEntry.error.filename4SizeInvalid"));
		           	    		
		           	    	}
		           	    	
		           	    	is.close();	    		
		    	    		
		    	    	}
		    	    	
		    	    	if (!errors.isEmpty()) {
		    	    		
		    	    		saveErrors(request, new ActionMessages(errors));
		           			return (mapping.findForward("glJournalEntryForm"));
		    	    		
		    	    	}    	    	
		    	    	
		    	   }*/

		    	   
		           try {
		           	
		           	    Integer journalCode = ejbJR.saveGlJrEntry(details, actionForm.getCategory(), 
		           	        actionForm.getCurrency(), new Integer(user.getCurrentResCode()), actionForm.getSource(), 
		           	        actionForm.getBatchName(), jlList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		           	        
		           	    actionForm.setJournalCode(journalCode);
		           			           	
		           } catch (GlobalRecordAlreadyDeletedException ex) {
		           	
		           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.recordAlreadyDeleted"));
		                               	
		           } catch (GlJREffectiveDateNoPeriodExistException ex) {
		           	
		           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.effectiveDateNoPeriodExist"));           	
		                      	
		           } catch (GlobalDocumentNumberNotUniqueException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.documentNumberNotUnique"));
		           			           	
		           } catch (GlJRDateReversalNoPeriodExistException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.dateReversalNoPeriodExist"));
		           	
		           } catch (GlJREffectiveDateViolationException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.effectiveDateViolation"));
		           	
		           } catch (GlJREffectiveDatePeriodClosedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.effectiveDatePeriodClosed"));
		           	
		           } catch (GlobalConversionDateNotExistException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.conversionDateNotExist"));
		                    		           	
		           } catch (GlJLChartOfAccountNotFoundException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.chartOfAccountNotFound", ex.getMessage()));
		           	
		           } catch (GlJLChartOfAccountPermissionDeniedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.chartOfAccountPermissionDenied", ex.getMessage()));
		                    
		           } catch (GlobalTransactionAlreadyApprovedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.transactionAlreadyApproved"));
		                    
		           } catch (GlobalTransactionAlreadyPendingException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.transactionAlreadyPending"));
		                    
		           } catch (GlobalTransactionAlreadyPostedException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.transactionAlreadyPosted"));
		                    
		           } catch (GlobalNoApprovalRequesterFoundException ex) {
           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.noApprovalRequesterFound"));
		                    
		           } catch (GlobalNoApprovalApproverFoundException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.noApprovalApproverFound"));	
		           	   
		           } catch (GlobalNoRecordFoundException ex) {
		           	
		           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
		                    new ActionMessage("journalEntry.error.noRecordFound"));
		              		           	
		           } catch (EJBException ex) {
		           	    if (log.isInfoEnabled()) {
		               	
		                  log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		                }
		               
		               return(mapping.findForward("cmnErrorPage"));
		           }	            
		           
		           if (!errors.isEmpty()) {
            	
		               saveErrors(request, new ActionMessages(errors));
		               return(mapping.findForward("glJournalEntry"));
		               
		           }
		           
		           actionForm.setReport(Constants.STATUS_SUCCESS);
		           
		           isInitialPrinting = true;
		           
//		         save attachment
		           /*
		           if (!Common.validateRequired(filename1)) {     
		        	   System.out.println("filename1: "+filename1);
		        	   if (errors.isEmpty()) {
		        		   
		        		   InputStream is = actionForm.getFilename1().getInputStream();
		        		   
		        		   new File(attachmentPath).mkdirs();
		        		   
		        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-1" + attachmentFileExtension);            	           	            	
		        		   
		        		   int c;        
		        		   
		        		   while ((c = is.read()) != -1) {
		        			   
		        			   fos.write((byte)c);
		        			   
		        		   }
		        		   
		        		   is.close();
		        		   fos.close();
		        		   
		        	   }          	                
		        	   
		           }
		           
		           if (!Common.validateRequired(filename2)) {     
		        	   
		        	   if (errors.isEmpty()) {
		        		   
		        		   InputStream is = actionForm.getFilename2().getInputStream();
		        		   
		        		   new File(attachmentPath).mkdirs();
		        		   
		        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-2" + attachmentFileExtension);            	           	            	
		        		   
		        		   int c;        
		        		   
		        		   while ((c = is.read()) != -1) {
		        			   
		        			   fos.write((byte)c);
		        			   
		        		   }
		        		   
		        		   is.close();
		        		   fos.close();
		        		   
		        	   }          	                
		        	   
		           }
		           
		           if (!Common.validateRequired(filename3)) {     
		        	   
		        	   if (errors.isEmpty()) {
		        		   
		        		   InputStream is = actionForm.getFilename3().getInputStream();
		        		   
		        		   new File(attachmentPath).mkdirs();
		        		   
		        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-3" + attachmentFileExtension);            	           	            	
		        		   
		        		   int c;        
		        		   
		        		   while ((c = is.read()) != -1) {
		        			   
		        			   fos.write((byte)c);
		        			   
		        		   }
		        		   
		        		   is.close();
		        		   fos.close();
		        		   
		        	   }          	                
		        	   
		           }
		           
		           if (!Common.validateRequired(filename4)) {     
		        	   
		        	   if (errors.isEmpty()) {
		        		   
		        		   InputStream is = actionForm.getFilename4().getInputStream();
		        		   
		        		   new File(attachmentPath).mkdirs();
		        		   
		        		   FileOutputStream fos = new FileOutputStream(attachmentPath + actionForm.getJournalCode() + "-4" + attachmentFileExtension);            	           	            	
		        		   
		        		   int c;        
		        		   
		        		   while ((c = is.read()) != -1) {
		        			   
		        			   fos.write((byte)c);
		        			   
		        		   }
		        		   
		        		   is.close();
		        		   fos.close();
		        		   
		        	   }          	                
		        	   
		           }*/
                             	
            }  else {
            	
            	actionForm.setReport(Constants.STATUS_SUCCESS);
              	          	        
  	        	return(mapping.findForward("glJournalEntry"));
  	        	
            }          	          	        	        
                        
            

/*******************************************************
   -- Gl JR Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

         	try {
           	
           	    ejbJR.deleteGlJrEntry(actionForm.getJournalCode(), user.getUserName(), user.getCmpCode());
           	
         	} catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.recordAlreadyDeleted"));
                    
         	} catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- Gl JR Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Gl JR Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {
         	
         	int listSize = actionForm.getGlJRListSize();
                                   
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	GlJournalEntryList glJRList = new GlJournalEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null, null);
	        	
	        	actionForm.saveGlJRList(glJRList);
	        	
	        }	        
	        
	        return(mapping.findForward("glJournalEntry"));
	        
/*******************************************************
   -- Gl JR Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getGlJRListSize(); i++) {
           	
           	   GlJournalEntryList glJRList = actionForm.getGlJRByIndex(i);
           	   
           	   if (glJRList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteGlJRList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getGlJRListSize(); i++) {
           	
           	   GlJournalEntryList glJRList = actionForm.getGlJRByIndex(i);
           	   
           	   glJRList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("glJournalEntry"));

 /*******************************************************
-- Ap VOU View Attachment Action --
 *******************************************************/

         } /*else if(request.getParameter("viewAttachmentButton1") != null) {

        	 FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJournalCode() + "-1" + attachmentFileExtension);

        	 byte data[] = new byte[fis.available()];

        	 int ctr = 0;
        	 int c = 0;

        	 while ((c = fis.read()) != -1) {

        		 data[ctr] = (byte)c;
        		 ctr++;

        	 }
        	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));

        	 session.setAttribute(Constants.IMAGE_KEY, image);

        	 actionForm.setAttachment(Constants.STATUS_SUCCESS);

        	 if (request.getParameter("child") == null) {
        		 return (mapping.findForward("glJournalEntry"));         

        	 } else {
        		 return (mapping.findForward("glJournalEntryChild"));         

        	 }

         }  else if(request.getParameter("viewAttachmentButton2") != null) {



        	 FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJournalCode() + "-2" + attachmentFileExtension);

        	 byte data[] = new byte[fis.available()];

        	 int ctr = 0;
        	 int c = 0;

        	 while ((c = fis.read()) != -1) {

        		 data[ctr] = (byte)c;
        		 ctr++;

        	 }

        	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));

        	 session.setAttribute(Constants.IMAGE_KEY, image);

        	 actionForm.setAttachment(Constants.STATUS_SUCCESS);

        	 if (request.getParameter("child") == null) {

        		 return (mapping.findForward("glJournalEntry"));         

        	 } else {

        		 return (mapping.findForward("glJournalEntryChild"));         

        	 } 

         } else if(request.getParameter("viewAttachmentButton3") != null) {



        	 FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJournalCode() + "-3" + attachmentFileExtension);

        	 byte data[] = new byte[fis.available()];

        	 int ctr = 0;
        	 int c = 0;

        	 while ((c = fis.read()) != -1) {

        		 data[ctr] = (byte)c;
        		 ctr++;

        	 }

        	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));

        	 session.setAttribute(Constants.IMAGE_KEY, image);

        	 actionForm.setAttachment(Constants.STATUS_SUCCESS);

        	 if (request.getParameter("child") == null) {

        		 return (mapping.findForward("glJournalEntry"));         

        	 } else {

        		 return (mapping.findForward("glJournalEntryChild"));         

        	 }

         }  else if(request.getParameter("viewAttachmentButton4") != null) {



        	 FileInputStream fis = new FileInputStream(attachmentPath + actionForm.getJournalCode() + "-4" + attachmentFileExtension);

        	 byte data[] = new byte[fis.available()];

        	 int ctr = 0;
        	 int c = 0;

        	 while ((c = fis.read()) != -1) {

        		 data[ctr] = (byte)c;
        		 ctr++;

        	 }

        	 Image image = new Image(data, attachmentFileExtension.substring(1, attachmentFileExtension.length() - 1));

        	 session.setAttribute(Constants.IMAGE_KEY, image);

        	 actionForm.setAttachment(Constants.STATUS_SUCCESS);

        	 if (request.getParameter("child") == null) {

        		 return (mapping.findForward("glJournalEntry"));         

        	 } else {

        		 return (mapping.findForward("glJournalEntryChild"));         

        	 }*/

/*******************************************************
   -- Gl JR Conversion Date Enter Action --
*******************************************************/
         /*}*/ else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))) {

         	try {
         		System.out.println("Constants.MONEY_RATE_PRECISION: " + Constants.MONEY_RATE_PRECISION);
         		actionForm.setConversionRate(Common.convertDoubleToStringMoney(
         			ejbJR.getFrRateByFrNameAndFrDate(actionForm.getCurrency(),
             		Common.convertStringToSQLDate(actionForm.getConversionDate()), user.getCmpCode()), Constants.MONEY_RATE_PRECISION));

         	} catch (GlobalConversionDateNotExistException ex) {
         		
         		errors.add(ActionMessages.GLOBAL_MESSAGE,
         				new ActionMessage("journalEntry.error.conversionDateNotExist"));
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			
         		}
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	} 

         	if (!errors.isEmpty()) {
         		
         		saveErrors(request, new ActionMessages(errors));
         		
         	}
         	
         	return(mapping.findForward("glJournalEntry"));

/*******************************************************
   -- Gl JR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glJournalEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbJR.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			GlModFunctionalCurrencyDetails mFcDetails = (GlModFunctionalCurrencyDetails)i.next();
            			
            			actionForm.setCurrencyList(mFcDetails.getFcName());
            			
            			if (mFcDetails.getFcSob() == 1) {
            				
            				actionForm.setCurrency(mFcDetails.getFcName());
            				actionForm.setFunctionalCurrency(mFcDetails.getFcName());
            				
            			}            			
            		                			
            		}
            		
            	}
            	
            	actionForm.clearCategoryList();           	
            	
            	list = ejbJR.getGlJcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCategoryList((String)i.next());
            			
            		}
            		
            		actionForm.setCategory("GENERAL");
            		
            	}    
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbJR.getGlOpenJbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	}
            	
            	actionForm.setSource("MANUAL");
            	            	
            	if (request.getParameter("forwardBatch") != null) {
            		
            		actionForm.setBatchName(request.getParameter("batchName"));
            		
            	}
            	
            	actionForm.clearGlJRList();               	  	
            	
            	if (request.getParameter("forward") != null || isInitialPrinting) {
            		
            		if (request.getParameter("forward") != null) {
            			            	    
            			actionForm.setJournalCode(new Integer(request.getParameter("journalCode")));
            			
            		}
            		
            		GlModJournalDetails mdetails = ejbJR.getGlJrByJrCode(actionForm.getJournalCode(), user.getCmpCode());
            		
            		actionForm.setName(mdetails.getJrName());
            		actionForm.setDescription(mdetails.getJrDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getJrEffectiveDate()));
            		actionForm.setDocumentNumber(mdetails.getJrDocumentNumber());
            		actionForm.setCategory(mdetails.getJrJcName());
            		actionForm.setSource(mdetails.getJrJsName());
            		
            		if (!actionForm.getCurrencyList().contains(mdetails.getJrFcName())) {
            			
            			if (actionForm.getCurrencyList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {
            				
            				actionForm.clearCurrencyList();
            				
            			}
            			actionForm.setCurrencyList(mdetails.getJrFcName());
            			
            		}            		
            		actionForm.setCurrency(mdetails.getJrFcName());
            		
            		actionForm.setConversionDate(Common.convertSQLDateToString(mdetails.getJrConversionDate()));
            		actionForm.setConversionRate(Common.convertDoubleToStringMoney(mdetails.getJrConversionRate(), (short)40));
            		actionForm.setReversalDate(Common.convertSQLDateToString(mdetails.getJrDateReversal()));
            		actionForm.setReversed(mdetails.getJrReversed() == 1 ? "YES" : "NO");
            		actionForm.setApprovalStatus(mdetails.getJrApprovalStatus());
            		actionForm.setReasonForRejection(mdetails.getJrReasonForRejection());
            		actionForm.setPosted(mdetails.getJrPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getJrCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getJrDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getJrLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getJrDateLastModified()));            		
            		actionForm.setApprovedRejectedBy(mdetails.getJrApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getJrDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getJrPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getJrDatePosted()));          		
            		actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getJrTotalDebit(), precisionUnit));
            		actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getJrTotalCredit(), precisionUnit));
            		actionForm.setJournalFrozen(Common.convertByteToBoolean(mdetails.getJrFrozen()));
            		
            		if (!actionForm.getBatchNameList().contains(mdetails.getJrJbName())) {
            			
           		    	actionForm.setBatchNameList(mdetails.getJrJbName());
           		    	
           		    }           		    
            		actionForm.setBatchName(mdetails.getJrJbName());
            		            		
            		list = mdetails.getJrJlList();              		
            		         		            		            		
		            i = list.iterator();
		            
		            while (i.hasNext()) {
		            	
			           GlModJournalLineDetails mJlDetails = (GlModJournalLineDetails)i.next();
			           
				       String debitAmount = null;
				       String creditAmount = null;
				       
				       if (mJlDetails.getJlDebit() == 1) {
				       	
				          debitAmount = Common.convertDoubleToStringMoney(mJlDetails.getJlAmount(), precisionUnit);				          
				          
				       } else {
				       	
				          creditAmount = Common.convertDoubleToStringMoney(mJlDetails.getJlAmount(), precisionUnit);				          
				          
				       }
				       
				       
				       GlJournalEntryList glJLList = new GlJournalEntryList(actionForm,
				          mJlDetails.getJlCode(),
				          Common.convertShortToString(mJlDetails.getJlLineNumber()),
				          mJlDetails.getJlCoaAccountNumber(),
				          debitAmount, creditAmount,
				          mJlDetails.getJlCoaAccountDescription(), mJlDetails.getJlDescription());
				          actionForm.saveGlJRList(glJLList);
			        }	
			        
			        int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
			        
			        for (int x = list.size() + 1; x <= remainingList; x++) {
			        	
			        	GlJournalEntryList glJRList = new GlJournalEntryList(actionForm,
			        	    null, String.valueOf(x), null, null, null, null, null);
			        	
			        	actionForm.saveGlJRList(glJRList);
			        	
			         }	
			         
			         this.setFormProperties(actionForm, user.getCompany());
			         
			         if (request.getParameter("child") == null) {
			         	
			         	return (mapping.findForward("glJournalEntry"));         
			         	
			         } else {
			         	
			         	return (mapping.findForward("glJournalEntryChild"));   
			         	
			         }
            		
            	}
            	
	            for (int x = 1; x <= journalLineNumber; x++) {
			
			    	GlJournalEntryList glJRList = new GlJournalEntryList(actionForm,
			    	    null, String.valueOf(x), null, null, null, null, null);
			    	
			    	actionForm.saveGlJRList(glJRList);
			    	
	  	        }            	           			
            		           			   	
            } catch(GlobalNoRecordFoundException ex) {
            	
            	for (int x = 1; x <= journalLineNumber; x++) {
			
			    	GlJournalEntryList glJRList = new GlJournalEntryList(actionForm,
			    	    null, String.valueOf(x), null, null, null, null, null);
			    	
			    	actionForm.saveGlJRList(glJRList);
			    	
	  	        }
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalEntry.error.journalAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveSubmitButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                   try {
                   	
                   	   ArrayList list = ejbJR.getAdApprovalNotifiedUsersByJrCode(actionForm.getJournalCode(), user.getCmpCode());
                   	                      	   
                   	   if (list.isEmpty()) {
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));     
                   	   
                   	   } else if (list.contains("DOCUMENT POSTED")) {
                   	   	
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,	
                	   	       new ActionMessage("messages.documentPosted"));
                   	   	
                   	   } else {
                   	   	
                   	   	   Iterator i = list.iterator();
                   	   	   
                   	   	   String APPROVAL_USERS = "";
                   	   	   
                   	   	   while (i.hasNext()) {
                   	   	   	                   	   	   	                   	   	   	                      	   	   	   	
                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();
                   	   	       
                   	   	       if (i.hasNext()) {
                   	   	       	
                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";
                   	   	       	
                   	   	       }
                   	   	                          	   	       
                   	   	   	   	                   	   	   	                      	   	   	
                   	   	   }
                   	   	   
                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));     
                   	   	
                   	   }
                   	   
                   	   saveMessages(request, messages);
                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                   	
                  	
                   } catch(EJBException ex) {
	     	
		               if (log.isInfoEnabled()) {
		               	
		                  log.info("EJBException caught in GlJournalEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }
		               
		               return(mapping.findForward("cmnErrorPage"));
		               
		           } 
                   
               } else if (request.getParameter("saveAsDraftButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
                  	
                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
                  	
               }
               
            }
                        
            actionForm.reset(mapping, request);              
            
            actionForm.setJournalCode(null);          
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setConversionRate("1.000000");
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            
            this.setFormProperties(actionForm, user.getCompany());
            return(mapping.findForward("glJournalEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in GlJournalEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(GlJournalEntryForm actionForm, String adCompany) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getPosted().equals("NO")) {
				
				if (actionForm.getJournalCode() == null) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(false);
					
				} else if (actionForm.getJournalCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {
					
					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveAsDraftButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowDeleteButton(true);
				    
				    	
				} else if (actionForm.getApprovalStatus().equals("APPROVED") ||
				    actionForm.getApprovalStatus().equals("N/A") || actionForm.getApprovalStatus().equals("PENDING")) {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(true);
					
				} else {
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(false);
					
				}
				
				if (actionForm.getJournalFrozen()) {
					
					
					actionForm.setEnableFields(false);
					actionForm.setShowSaveAsDraftButton(false);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowDeleteButton(false);
					
				}
				
			} else {
				
				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(false);
			    actionForm.setShowSaveAsDraftButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				actionForm.setShowDeleteButton(false);
				
			}
			
//			 view attachment	
			if (actionForm.getJournalCode() != null) {
				/*
				MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

	            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/gl/journalentry/";
	            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
				File file = new File(attachmentPath + actionForm.getJournalCode() + "-1" + attachmentFileExtension);
				
				if (file.exists()) {
					
					actionForm.setShowViewAttachmentButton1(true);	
					
				} else {
					
					actionForm.setShowViewAttachmentButton1(false);	
					
				}			
				
				file = new File(attachmentPath + actionForm.getJournalCode() + "-2" + attachmentFileExtension);
				
				if (file.exists()) {
					
					actionForm.setShowViewAttachmentButton2(true);	
					
				} else {
					
					actionForm.setShowViewAttachmentButton2(false);	
					
				}
				
				file = new File(attachmentPath + actionForm.getJournalCode() + "-3" + attachmentFileExtension);
				
				if (file.exists()) {
					
					actionForm.setShowViewAttachmentButton3(true);	
					
				} else {
					
					actionForm.setShowViewAttachmentButton3(false);	
					
				}
				
				file = new File(attachmentPath + actionForm.getJournalCode() + "-4" + attachmentFileExtension);
				
				if (file.exists()) {
					
					actionForm.setShowViewAttachmentButton4(true);	
					
				} else {
					
					actionForm.setShowViewAttachmentButton4(false);	
					
				}*/							
				
			} else {
					/*		
				actionForm.setShowViewAttachmentButton1(false);				
				actionForm.setShowViewAttachmentButton2(false);
				actionForm.setShowViewAttachmentButton3(false);
				actionForm.setShowViewAttachmentButton4(false);*/
				
			}
			
			
		} else {
			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveAsDraftButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setShowDeleteButton(false);
			
			
		}				
		    
	}
	
}