package com.struts.gl.journalentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalEntryForm extends ActionForm implements Serializable {

   private Integer journalCode = null;
   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();
   private String name = null;
   private String description = null;
   private String date = null;
   private String documentNumber = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String source = null;   
   private String currency = null;
   private ArrayList currencyList = new ArrayList();
   private String conversionDate = null;
   private String conversionRate = null;
   private String reversalDate = null;
   private String reversed = null;
   private String approvalStatus = null;
   private String reasonForRejection = null;
   private String posted = null;
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;   
   private String approvedRejectedBy = null;
   private String dateApprovedRejected = null;
   private String postedBy = null;
   private String datePosted = null;
   private String functionalCurrency = null;
   private String totalDebit = null;
   private String totalCredit = null;
   private boolean journalFrozen = false;

   private ArrayList glJRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showBatchName = false;
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showSaveSubmitButton = false;
   private boolean showSaveAsDraftButton = false;
   private boolean showDeleteButton = false;
   private String isConversionDateEntered = null;

   private String report = null;

   /*private FormFile filename1 = null;
   private FormFile filename2 = null;
   private FormFile filename3 = null;
   private FormFile filename4 = null;

   private boolean showViewAttachmentButton1 = false;
   private boolean showViewAttachmentButton2 = false;
   private boolean showViewAttachmentButton3 = false;
   private boolean showViewAttachmentButton4 = false;

   private String attachment = null;*/
	
   public String getReport() {
   	
   	   return report;
   	
   }

   public void setReport(String report) {

	   this.report = report;

   }


   /*public String getAttachment() {

	   return attachment;

   }

   public void setAttachment(String attachment) {

	   this.attachment = attachment;

   }*/

      
   public int getRowSelected(){
      return rowSelected;
   }

   public GlJournalEntryList getGlJRByIndex(int index){
      return((GlJournalEntryList)glJRList.get(index));
   }

   public Object[] getGlJRList(){
      return(glJRList.toArray());
   }

   public int getGlJRListSize(){
      return(glJRList.size());
   }

   public void saveGlJRList(Object newGlJRList){
      glJRList.add(newGlJRList);
   }

   public void clearGlJRList(){
      glJRList.clear();
   }

   public void setRowSelected(Object selectedGlJRList, boolean isEdit){
      this.rowSelected = glJRList.indexOf(selectedGlJRList);
   }

   public void updateGlJRRow(int rowSelected, Object newGlJRList){
      glJRList.set(rowSelected, newGlJRList);
   }

   public void deleteGlJRList(int rowSelected){
      glJRList.remove(rowSelected);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getJournalCode() {
   	
   	  return journalCode;
   	
   }
   
   public void setJournalCode(Integer journalCode) {
   	
   	  this.journalCode = journalCode;
   	
   }
   
   public String getBatchName() {
   	
   	  return batchName;
   	
   }
   
   public void setBatchName(String batchName) {
   	
   	  this.batchName = batchName;
   	
   }
   
   public ArrayList getBatchNameList() {
   	
   	  return batchNameList;
   	
   }
   
   public void setBatchNameList(String batchName) {
   	
   	  batchNameList.add(batchName);
   	
   }
   
   public void clearBatchNameList() {   	 
   	
   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getName() {
   	  
   	  return name;
   	
   }
   
   public void setName(String name) {
   	
   	  this.name = name;
   	
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }
   
   public String getDate() {
   	  
   	  return date;
   	   	
   }
   
   public void setDate(String date) {
   	
   	  this.date = date;
   	
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	
   }
   
   public void setDocumentNumber(String documentNumber) {
   	
   	  this.documentNumber = documentNumber;
   	
   }
   
   public String getCategory() {
   	
   	  return category;
   	
   }
   
   public void setCategory(String category) {
   	
   	  this.category = category;
   	
   }
   
   public ArrayList getCategoryList() {
   	
   	  return categoryList;
   	
   }
   
   public void setCategoryList(String category) {
   	
   	  categoryList.add(category);
   	
   }
   
   public void clearCategoryList() {
   	
   	  categoryList.clear();
   	
   }
   
   public String getSource() {
   	
   	  return source;
   	
   }
   
   public void setSource(String source) {
   	
   	  this.source = source;
   	
   }  
   
   
   public String getCurrency() {
   	
   	   return currency;
   	
   }
   
   public void setCurrency(String currency) {
   	
   	   this.currency = currency;
   	
   }
   
   public ArrayList getCurrencyList() {
   	
   	   return currencyList;
   	
   }
   
   public void setCurrencyList(String currency) {
   	
   	   currencyList.add(currency);
   	
   }
   
   public void clearCurrencyList() {
   	
   	   currencyList.clear();
   	
   }
   
   public String getConversionDate() {
   	
   	   return conversionDate;
   	
   }
   
   public void setConversionDate(String conversionDate) {
   	
   	   this.conversionDate = conversionDate;
   	
   }
   
   public String getConversionRate() {
   	 
   	   return conversionRate;
   	
   }
   
   public void setConversionRate(String conversionRate) {
   	
   	   this.conversionRate = conversionRate;
   	
   }
   
   public String getReversalDate() {
   	
   	   return reversalDate;
   	
   }
   
   public void setReversalDate(String reversalDate) {
   	
   	   this.reversalDate = reversalDate;
   	
   }
   
   public String getReversed() {
   	
   	   return reversed;
   	
   }
   
   public void setReversed(String reversed) {
   	
   	   this.reversed = reversed;
   	
   }
   
   public String getApprovalStatus() {
   	
   	   return approvalStatus;
   	
   }
   
   public void setApprovalStatus(String approvalStatus) {
   	
   	   this.approvalStatus = approvalStatus;
   	
   }
   
   public String getReasonForRejection() {
   	
   	  return reasonForRejection;
   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
   
   public String getPosted() {
   	
   	   return posted;
   	
   }
   
   public void setPosted(String posted) {
   	
   	   this.posted = posted;
   	
   }

   public String getCreatedBy() {
   	
   	   return createdBy;
   	
   }
   
   public void setCreatedBy(String createdBy) {
   	
   	  this.createdBy = createdBy;
   	
   }
   
   public String getDateCreated() {
   	
   	   return dateCreated;
   	
   }
   
   public void setDateCreated(String dateCreated) {
   	
   	   this.dateCreated = dateCreated;
   	
   }   
   
   public String getLastModifiedBy() {
   	
   	  return lastModifiedBy;
   	
   }
   
   public void setLastModifiedBy(String lastModifiedBy) {
   	
   	  this.lastModifiedBy = lastModifiedBy;
   	
   }
   
   public String getDateLastModified() {
   	
   	  return dateLastModified;
   	
   }
   
   public void setDateLastModified(String dateLastModified) {
   	
   	  this.dateLastModified = dateLastModified;
   	
   }

   
   public String getApprovedRejectedBy() {
   	
   	  return approvedRejectedBy;
   	
   }
   
   public void setApprovedRejectedBy(String approvedRejectedBy) {
   	
   	  this.approvedRejectedBy = approvedRejectedBy;
   	
   }
   
   public String getDateApprovedRejected() {
   	
   	  return dateApprovedRejected;
   	
   }
   
   public void setDateApprovedRejected(String dateApprovedRejected) {
   	
   	  this.dateApprovedRejected = dateApprovedRejected;
   	
   }
   
   public String getPostedBy() {
   	
   	  return postedBy;
   	
   }
   
   public void setPostedBy(String postedBy) {
   	
   	  this.postedBy = postedBy;
   	
   }
   
   public String getDatePosted() {
   	
   	  return datePosted;
   	
   }
   
   public void setDatePosted(String datePosted) {
   	
   	  this.datePosted = datePosted;
   	
   }
      
   public String getFunctionalCurrency() {
   	
   	   return functionalCurrency;
   	
   }
   
   public void setFunctionalCurrency(String functionalCurrency) {
   	
   	   this.functionalCurrency = functionalCurrency;
   	
   }
   
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowSaveSubmitButton() {
   	
   	   return showSaveSubmitButton;
   	
   }
   
   public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {
   	
   	   this.showSaveSubmitButton = showSaveSubmitButton;
   	
   }
   
   public boolean getShowSaveAsDraftButton() {
   	
   	   return showSaveAsDraftButton;
   	
   }
   
   public void setShowSaveAsDraftButton(boolean showSaveAsDraftButton) {
   	
   	   this.showSaveAsDraftButton = showSaveAsDraftButton;
   	
   }
   
   public boolean getShowDeleteButton() {
   	
   	   return showDeleteButton;
   	
   }
   
   public void setShowDeleteButton(boolean showDeleteButton) {
   	
   	   this.showDeleteButton = showDeleteButton;
   	
   }  
      
   	public String getTotalDebit() {
   	
   		return totalDebit;
   		
   	}
   	
   	public void setTotalDebit(String totalDebit) {
   		
   		this.totalDebit = totalDebit;
   		
   	}
   	
   	public String getTotalCredit() {
   		
   		return totalCredit;
   		
   	}
   	
   	public void setTotalCredit(String totalCredit) {
   		
   		this.totalCredit = totalCredit;
   		
   	}
   	
   	public boolean getJournalFrozen() {
   		
   		return journalFrozen;
   		
   	}
   	
   	public void setJournalFrozen(boolean journalFrozen) {
   		
   		this.journalFrozen = journalFrozen;
   		
   	}

   	public String getIsConversionDateEntered(){
   		
   		return isConversionDateEntered;
   		
   	}
   	
/*
	public FormFile getFilename1() {

		return filename1;

	}

	public void setFilename1(FormFile filename1) {

		this.filename1 = filename1;

	}

	public FormFile getFilename2() {

		return filename2;

	}

	public void setFilename2(FormFile filename2) {

		this.filename2 = filename2;

	}

	public FormFile getFilename3() {
		
		return filename3;
		
	}
	
	public void setFilename3(FormFile filename3) {
		
		this.filename3 = filename3;
		
	}
	
	public FormFile getFilename4() {
		
		return filename4;
		
	}
	
	public void setFilename4(FormFile filename4) {
		
		this.filename4 = filename4;
		
	}
	
	public boolean getShowViewAttachmentButton1() {
		
		return showViewAttachmentButton1;
		
	}
	
	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {
		
		this.showViewAttachmentButton1 = showViewAttachmentButton1;
		
	}
	
	public boolean getShowViewAttachmentButton2() {
		
		return showViewAttachmentButton2;
		
	}
	
	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {
		
		this.showViewAttachmentButton2 = showViewAttachmentButton2;
		
	}
	
	public boolean getShowViewAttachmentButton3() {
		
		return showViewAttachmentButton3;
		
	}
	
	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {
		
		this.showViewAttachmentButton3 = showViewAttachmentButton3;
		
	}
	
	public boolean getShowViewAttachmentButton4() {
		
		return showViewAttachmentButton4;
		
	}
	
	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {
		
		this.showViewAttachmentButton4 = showViewAttachmentButton4;
		
	}*/
	

   public void reset(ActionMapping mapping, HttpServletRequest request){      
       
	   name = null;
	   description = null;
	   date = null;
	   documentNumber = null;  
	   
       conversionDate = null;
	   conversionRate = null;
	   reversalDate = null;
	   reversed = null;
	   approvalStatus = null;
	   reasonForRejection = null;
	   posted = null;
	   createdBy = null;
	   dateCreated = null;	   
	   lastModifiedBy = null;
	   dateLastModified = null;
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;
	   totalDebit = null;
	   totalCredit = null;
	   journalFrozen = false;
	   isConversionDateEntered = null;

	   /*filename1 = null;
	   filename2 = null;
	   filename3 = null;
	   filename4 = null;*/
	   	         
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
         request.getParameter("saveAsDraftButton") != null || 
         request.getParameter("printButton") != null){
      	
      	 if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
      	    showBatchName){
            errors.add("batchName",
               new ActionMessage("journalEntry.error.batchNameRequired"));
         }
      	
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("journalEntry.error.dateRequired"));
         }
         
		 if(!Common.validateDateFormat(date)){
	            errors.add("date", 
		       new ActionMessage("journalEntry.error.dateInvalid"));
		 }
		         
         if(Common.validateRequired(category) || category.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("category",
               new ActionMessage("journalEntry.error.categoryRequired"));
         }
         
         if(Common.validateRequired(currency) || currency.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {
            errors.add("currency",
               new ActionMessage("journalEntry.error.currencyRequired"));
         }
         
	 	 if(!Common.validateDateFormat(conversionDate)){
            errors.add("conversionDate",
               new ActionMessage("journalEntry.error.conversionDateInvalid"));
         }
         
	 	 if(!Common.validateMoneyRateFormat(conversionRate)){
            errors.add("conversionRate",
               new ActionMessage("journalEntry.error.conversionRateInvalid"));
         }
         
         if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionDate)){
	    	errors.add("conversionDate",
	       new ActionMessage("journalEntry.error.conversionDateMustBeNull"));
	 	 }
	 	 
		 if(currency.equals(functionalCurrency) && !Common.validateRequired(conversionRate) &&
		    Common.convertStringMoneyToDouble(conversionRate, (short)6) != 1d){ 
		    errors.add("conversionRate",
		       new ActionMessage("journalEntry.error.conversionRateMustBeNull"));
		 }

		 if((!currency.equals(functionalCurrency)) && Common.validateRequired(conversionRate) &&
		    Common.validateRequired(conversionDate)){
		    errors.add("conversionRate",
		       new ActionMessage("journalEntry.error.conversionRateOrDateMustBeEntered"));
		 }
         
	 	         		 		 
		 if(!Common.validateDateFormat(reversalDate)){
            errors.add("reversalDate",
               new ActionMessage("journalEntry.error.reversalDateInvalid"));
         }
         
         if(!Common.validateRequired(reversalDate) && Common.validateDateFormat(reversalDate) && 
            !Common.validateRequired(date) && Common.validateDateFormat(date) &&
            Common.convertStringToSQLDate(reversalDate).before(Common.convertStringToSQLDate(date))){
            errors.add("reversalDate",
               new ActionMessage("journalEntry.error.reversalDateMustBeAfterDate"));
         }
         
		 int numberOfLines = 0;
      	 
      	 Iterator i = glJRList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 GlJournalEntryList jrList = (GlJournalEntryList)i.next();      	 	 
      	 	       	 	 
      	 	 if (Common.validateRequired(jrList.getAccount()) &&
      	 	     Common.validateRequired(jrList.getDebitAmount()) &&
      	 	     Common.validateRequired(jrList.getCreditAmount())) continue;
      	 	     
      	 	 numberOfLines++;
      	 
	         if(Common.validateRequired(jrList.getAccount())){
	            errors.add("account",
	               new ActionMessage("journalEntry.error.accountRequired", jrList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(jrList.getDebitAmount())){
	            errors.add("debitAmount",
	               new ActionMessage("journalEntry.error.debitAmountInvalid", jrList.getLineNumber()));
	         }
	         if(!Common.validateMoneyFormat(jrList.getCreditAmount())){
	            errors.add("creditAmount",
	               new ActionMessage("journalEntry.error.creditAmountInvalid", jrList.getLineNumber()));
	         }
			 if(Common.validateRequired(jrList.getDebitAmount()) && Common.validateRequired(jrList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("journalEntry.error.debitCreditAmountRequired", jrList.getLineNumber()));
			 }
			 if(!Common.validateRequired(jrList.getDebitAmount()) && !Common.validateRequired(jrList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("journalEntry.error.debitCreditAmountMustBeNull", jrList.getLineNumber()));
		     }	    		     
		     
		          
	    }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("name",
               new ActionMessage("journalEntry.error.journalMustHaveLine"));
         	
         }	  	    
	    
	    	    	 
      } else if (!Common.validateRequired(request.getParameter("isConversionDateEntered"))){

      	if(!Common.validateDateFormat(conversionDate)){

      		errors.add("conversionDate", new ActionMessage("journalEntry.error.conversionDateInvalid"));

      	}

      }
      return(errors);	
   }
}
