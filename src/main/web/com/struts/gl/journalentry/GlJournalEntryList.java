package com.struts.gl.journalentry;

import java.io.Serializable;

public class GlJournalEntryList implements Serializable {

   private Integer journalLineCode = null;
   private String lineNumber = null;
   private String account = null;
   private String debitAmount = null;
   private String creditAmount = null;
   private String accountDescription = null;
   private String description = null;
   private boolean deleteCheckbox = false;
    
   private GlJournalEntryForm parentBean;
    
   public GlJournalEntryList(GlJournalEntryForm parentBean,
      Integer journalLineCode,
      String lineNumber,
      String account,
      String debitAmount,
      String creditAmount,
      String accountDescription,
	  String description){

      this.parentBean = parentBean;
      this.journalLineCode = journalLineCode;
      this.lineNumber = lineNumber;
      this.account = account;
      this.debitAmount = debitAmount;
      this.creditAmount = creditAmount;
      this.accountDescription = accountDescription;
      this.description = description;
   }
         
   public Integer getJournalLineCode(){
      return(journalLineCode);
   }

   public String getLineNumber(){
      return(lineNumber);
   }
   
   public void setLineNumber(String lineNumber) {
   	  this.lineNumber = lineNumber;
   }

   public String getAccount(){
      return(account);
   }
   
   public void setAccount(String account) {
   	  this.account = account;
   }

   public String getDebitAmount(){
      return(debitAmount);
   }
   
   public void setDebitAmount(String debitAmount) {
   	  this.debitAmount = debitAmount;
   }

   public String getCreditAmount(){
      return(creditAmount);
   }
   
   public void setCreditAmount(String creditAmount) {
   	  this.creditAmount = creditAmount;	
   }
   
   public String getAccountDescription(){
   	  return(accountDescription);
   }
   
   public void setAccountDescription(String accountDescription) {
   	  this.accountDescription = accountDescription;
   }
   
   public boolean getDeleteCheckbox() {   	
   	  return deleteCheckbox;   	
   }
   
   public void setDeleteCheckbox(boolean deleteCheckbox) {
   	  this.deleteCheckbox = deleteCheckbox;
   }
   
   public String getDescription(){
   	return(description);
   }
   
   public void setDescription(String description) {
   	this.description = description;
   }

}