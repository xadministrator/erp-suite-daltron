package com.struts.gl.journalimport;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.GlJournalImportController;
import com.ejb.txn.GlJournalImportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModAccountingCalendarValueDetails;

public final class GlJournalImportAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlJournalImportAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlJournalImportForm actionForm = (GlJournalImportForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_IMPORT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glJournalImport");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlJournalImportController EJB
*******************************************************/

         GlJournalImportControllerHome homeJI = null;
         GlJournalImportController ejbJI = null;

         try {

             homeJI = (GlJournalImportControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalImportControllerEJB", GlJournalImportControllerHome.class);
             
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlJournalImportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbJI = homeJI.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlJournalImportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Gl JI Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            try {
            
            	long importedJournals = ejbJI.executeGlJrImport(actionForm.getJournalSource(),
            	    actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')), 
		            Common.convertStringToInt(actionForm.getPeriod().substring(
		                actionForm.getPeriod().indexOf('-') + 1)), 
		            user.getUserName(), new Integer(user.getCurrentResCode()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		            
		        actionForm.setImportedJournals("IMPORTED JOURNALS: " + importedJournals);

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlJournalImportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 

               }

            }

/*******************************************************
   -- Gl JI Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));


/*******************************************************
   -- Gl JI Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glJournalImport");

            }

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearJournalSourceList();           	
            	
            	list = ejbJI.getGlJsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setJournalSourceList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setJournalSourceList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearPeriodList();           	
            	
            	list = ejbJI.getGlAcAllEditableOpenAndFutureEntry(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
          		           		            		
	          		i = list.iterator();
	            		
	           		while (i.hasNext()) {
	            			
	           			GlModAccountingCalendarValueDetails mdetails = 
	           			    (GlModAccountingCalendarValueDetails)i.next();
	            			    
	           		    actionForm.setPeriodList(mdetails.getAcvPeriodPrefix() +
	           		        "-" + mdetails.getAcvYear());
	            			
	           		}       
           		
           		}     	
          	           	                 	
             } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlJournalImportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("goButton") != null && 
                   actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);

            return(mapping.findForward("glJournalImport"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlJournalImportAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}