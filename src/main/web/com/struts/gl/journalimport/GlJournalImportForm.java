package com.struts.gl.journalimport;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalImportForm extends ActionForm implements Serializable {

   private String importedJournals = null;
   private String journalSource = null;
   private ArrayList journalSourceList = new ArrayList();
   private String period = null;
   private ArrayList periodList = new ArrayList();

   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private String userPermission = new String();
   private String txnStatus = new String();

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public String getImportedJournals() {

      return importedJournals;

   }

   public void setImportedJournals(String importedJournals) {

      this.importedJournals = importedJournals;

   }

   public String getJournalSource() {

      return journalSource;

   }

   public void setJournalSource(String journalSource) {

      this.journalSource = journalSource;

   }

   public ArrayList getJournalSourceList() {

      return journalSourceList;

   }

   public void setJournalSourceList(String journalSource) {

      journalSourceList.add(journalSource);

   }

   public void clearJournalSourceList() {

      journalSourceList.clear();
      journalSourceList.add(Constants.GLOBAL_BLANK);
   }

   public String getPeriod() {

      return period;

   }

   public void setPeriod(String period) {

      this.period = period;

   }

   public ArrayList getPeriodList() {

      return periodList;

   }

   public void setPeriodList(String period) {

      periodList.add(period);

   }

   public void clearPeriodList() {

      periodList.clear();
      periodList.add(Constants.GLOBAL_BLANK);
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      journalSource = Constants.GLOBAL_BLANK;
      period = Constants.GLOBAL_BLANK;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {

         if (Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND)) {

            errors.add("period",
               new ActionMessage("journalImport.error.periodRequired"));

         }

         if (!Common.validateStringExists(periodList, period)) {

            errors.add("period",
               new ActionMessage("journalImport.error.periodInvalid"));

         }

      }
      return errors;

   }
}