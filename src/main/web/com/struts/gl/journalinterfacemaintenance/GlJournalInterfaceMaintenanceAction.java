package com.struts.gl.journalinterfacemaintenance;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlJLINoJournalLineInterfacesFoundException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlJournalInterfaceMaintenanceController;
import com.ejb.txn.GlJournalInterfaceMaintenanceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlJournalInterfaceDetails;
import com.util.GlJournalLineInterfaceDetails;
import com.util.GlModJournalLineInterfaceDetails;

public final class GlJournalInterfaceMaintenanceAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlJournalInterfaceMaintenanceAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlJournalInterfaceMaintenanceForm actionForm = (GlJournalInterfaceMaintenanceForm)form;

         String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_INTERFACE_MAINTENANCE_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glJournalInterfaceMaintenance"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize GlJournalInterfaceMaintenanceController EJB
*******************************************************/

         GlJournalInterfaceMaintenanceControllerHome homeJRI = null;
         GlJournalInterfaceMaintenanceController ejbJRI = null;

         try {
            
            homeJRI = (GlJournalInterfaceMaintenanceControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalInterfaceMaintenanceControllerEJB", GlJournalInterfaceMaintenanceControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlJournalInterfaceMaintenanceAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbJRI = homeJRI.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in GlJournalInterfaceMaintenanceAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call GlJournalLineEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short journalLineNumber = 4;

         try {
         	
            precisionUnit = ejbJRI.getGlFcPrecisionUnit(user.getCmpCode());
            
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlJournalInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
											
															 
/*******************************************************
   -- Gl  JRI Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	
           GlJournalInterfaceDetails details = new GlJournalInterfaceDetails();

           details.setJriCode(actionForm.getJournalInterfaceCode());
           details.setJriName(actionForm.getName());
           details.setJriDescription(actionForm.getDescription());
           details.setJriEffectiveDate(Common.convertStringToSQLDate(actionForm.getEffectiveDate()));
           details.setJriJournalCategory(actionForm.getJournalCategory());
           details.setJriJournalSource(actionForm.getJournalSource());
           details.setJriFunctionalCurrency(actionForm.getCurrency());
           details.setJriDateReversal(Common.convertStringToSQLDate(actionForm.getDateReversal()));
           details.setJriDocumentNumber(actionForm.getDocumentNumber());
           details.setJriConversionDate(Common.convertStringToSQLDate(actionForm.getConversionDate()));
           details.setJriConversionRate(Common.convertStringMoneyToDouble(actionForm.getConversionRate(), precisionUnit));
           details.setJriFundStatus(Common.convertStringToChar(actionForm.getFundStatus()));
           details.setJriReversed(Common.convertBooleanToByte(actionForm.getReversed()));

           ArrayList list = new ArrayList();
           int lineNumber = 0;
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;                    
           
           for (int i = 0; i<actionForm.getGlJLIListSize(); i++) {
           	
           	   GlJournalInterfaceMaintenanceList glJLIList = actionForm.getGlJLIByIndex(i);
           	              	   
           	   if (Common.validateRequired(glJLIList.getAccount()) &&
      	 	       Common.validateRequired(glJLIList.getDebitAmount()) &&
      	 	       Common.validateRequired(glJLIList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(glJLIList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(glJLIList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(glJLIList.getCreditAmount(), precisionUnit);
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   GlJournalLineInterfaceDetails lineDetails = new GlJournalLineInterfaceDetails();
           	   lineDetails.setJliLineNumber((short)(lineNumber));
           	   lineDetails.setJliDebit(isDebit);
	  	  	   lineDetails.setJliAmount(amount);
	  	  	   lineDetails.setJliCoaAccountNumber(glJLIList.getAccount());

           	   list.add(lineDetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           
           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	  errors.add(ActionMessages.GLOBAL_MESSAGE,
           	      new ActionMessage("journalInterfaceMaintenance.error.journalNotBalance"));
           	      
           	  saveErrors(request, new ActionMessages(errors));
           	  return (mapping.findForward("glJournalInterfaceMaintenance"));
           	
           }
           
	       try {
	       	
	          ejbJRI.saveGlJriEntry(details, list, user.getCmpCode());
		         
           } catch (GlobalRecordAlreadyExistException ex) {
              
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("journalInterfaceMaintenance.error.journalNameAlreadyExists"));		         

	       } catch(GlCOANoChartOfAccountFoundException ex) {
	                       	
	          errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("journalInterfaceMaintenance.error.chartOfAccountNotFound", ex.getMessage()));
           
	       } catch(EJBException ex) {
	       	
	          if(log.isInfoEnabled()) {
	          	
                 log.info("EJBException caught in GlJournalInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
                    " session: " + session.getId());
                    
              }
              
              return(mapping.findForward("cmnErrorPage"));
              
	       }
	       
	       if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glJournalInterfaceMaintenance"));
               
           }
	       	    	    
/*******************************************************
   -- Gl  JRI Delete Action --
*******************************************************/	    
	    
         } else if(request.getParameter("deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	            	            
	     	 try { 
	     	 
	       		 ejbJRI.deleteGlJriEntry(actionForm.getJournalInterfaceCode(), user.getCmpCode());
	       		 
             } catch (EJBException ex) {
             	
                if (log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in GlJournalInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                }
                
                return(mapping.findForward("cmnErrorPage"));
                                                
             }	
             
             return(mapping.findForward("cmnMain"));    

/*******************************************************
   -- Gl  JRI Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Gl  JRI Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {
         	
         	int listSize = actionForm.getGlJLIListSize();
                                   
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	GlJournalInterfaceMaintenanceList glJLIList = new GlJournalInterfaceMaintenanceList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null);
	        	
	        	actionForm.saveGlJLIList(glJLIList);
	        	
	        }
	        
	        return(mapping.findForward("glJournalInterfaceMaintenance"));
	        
/*******************************************************
   -- Gl  JRI Delete Lines Action --
*******************************************************/	    	        

         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getGlJLIListSize(); i++) {
           	
           	   GlJournalInterfaceMaintenanceList glJLIList = actionForm.getGlJLIByIndex(i);
           	   
           	   if (glJLIList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteGlJLIList(i);
           	   	   i--;
           	   	
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getGlJLIListSize(); i++) {
           	
           	   GlJournalInterfaceMaintenanceList glJLIList = actionForm.getGlJLIByIndex(i);
           	   
           	   glJLIList.setLineNumber(String.valueOf(i+1));
           	   
            }            

	        return(mapping.findForward("glJournalInterfaceMaintenance"));           
                      
/*******************************************************
   -- Gl  JRI Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glJournalInterfaceMaintenance"));
               
            }

	     if (request.getParameter("forward") != null) {
	     	
            GlJournalInterfaceDetails details = ejbJRI.getGlJriByJriCode(
               new Integer(request.getParameter("journalInterfaceCode")), user.getCmpCode());	     	
	     	            
            actionForm.setJournalInterfaceCode(details.getJriCode());
            actionForm.setName(details.getJriName());
            actionForm.setDescription(details.getJriDescription());
            actionForm.setEffectiveDate(Common.convertSQLDateToString(details.getJriEffectiveDate()));
            actionForm.setJournalCategory(details.getJriJournalCategory());
            actionForm.setJournalSource(details.getJriJournalSource());
            actionForm.setCurrency(details.getJriFunctionalCurrency());
            actionForm.setDateReversal(Common.convertSQLDateToString(details.getJriDateReversal()));
            actionForm.setDocumentNumber(details.getJriDocumentNumber());
            actionForm.setConversionDate(Common.convertSQLDateToString(details.getJriConversionDate()));
            actionForm.setConversionRate(Common.convertDoubleToStringMoney(details.getJriConversionRate(), precisionUnit));
            actionForm.setFundStatus(Common.convertCharToString(details.getJriFundStatus()));
            actionForm.setReversed(Common.convertByteToBoolean(details.getJriReversed()));
            	        
         }
         
	     actionForm.clearGlJLIList();
	     Iterator i = null;
	     ArrayList ejbGlJLIList = null;

	     try { 
	     
	        ejbGlJLIList = ejbJRI.getGlJliByJriCode(actionForm.getJournalInterfaceCode(), user.getCmpCode());
	        
            i = ejbGlJLIList.iterator();
            
            while (i.hasNext()) {
            	
	           GlModJournalLineInterfaceDetails mdetails = (GlModJournalLineInterfaceDetails)i.next();
		       String debitAmount = null;
		       String creditAmount = null;
		       
		       if (mdetails.getJliDebit() == 1) {
		       	
		          debitAmount = Common.convertDoubleToStringMoney(mdetails.getJliAmount(), precisionUnit);
		          
		       } else {
		       	
		          creditAmount = Common.convertDoubleToStringMoney(mdetails.getJliAmount(), precisionUnit);
		          
		       }
		       
		       
		       GlJournalInterfaceMaintenanceList glJLIList = new GlJournalInterfaceMaintenanceList(actionForm,
		          mdetails.getJliCode(),
		          Common.convertShortToString(mdetails.getJliLineNumber()),
		          mdetails.getJliCoaAccountNumber(),
		          debitAmount, creditAmount,
		          mdetails.getJliCoaDescription());
		          actionForm.saveGlJLIList(glJLIList);
	        }	
	        
	        int remainingList = journalLineNumber - (ejbGlJLIList.size() % journalLineNumber) + ejbGlJLIList.size();
	        
	        for (int x = ejbGlJLIList.size() + 1; x <= remainingList; x++) {
	        	
	        	GlJournalInterfaceMaintenanceList glJLIList = new GlJournalInterfaceMaintenanceList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null);
	        	
	        	actionForm.saveGlJLIList(glJLIList);
	        	
	         }	        
	        
	        
	           
	     } catch(GlJLINoJournalLineInterfacesFoundException ex) {
	     	
	         for (int x = 1; x <= journalLineNumber; x++) {
	        	
	        	GlJournalInterfaceMaintenanceList glJLIList = new GlJournalInterfaceMaintenanceList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null);
	        	
	        	actionForm.saveGlJLIList(glJLIList);
	        	
	         }		     	
	     	
	     } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlJournalInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
          }         
          

          if (!errors.isEmpty()) {
          	
             saveErrors(request, new ActionMessages(errors));
             
          } else {
          	
             if (request.getParameter("saveButton") != null && 
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               }
               
            }

            actionForm.reset(mapping, request);            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("glJournalInterfaceMaintenance"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in GlJournalInterfaceMaintenanceAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }
}
