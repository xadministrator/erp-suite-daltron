package com.struts.gl.journalinterfacemaintenance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalInterfaceMaintenanceForm extends ActionForm implements Serializable {

   private Integer journalInterfaceCode = null;
   private String name = null;
   private String description = null;
   private String effectiveDate = null;
   private String journalCategory = null;
   private String journalSource = null;
   private String currency = null;      
   private String dateReversal = null;
   private String documentNumber = null;
   private String conversionDate = null;
   private String conversionRate = null;
   private String fundStatus = null;
   private boolean reversed = false;

   private String saveButton = null;
   private String closeButton = null;
   private String deleteButton = null;
   private String addLinesButton = null;
   private String deleteLinesButton = null;
   private String pageState = new String();
   private ArrayList glJLIList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private boolean disableFields = false;

   public int getRowSelected() {
   	
      return rowSelected; 
   }

   public GlJournalInterfaceMaintenanceList getGlJLIByIndex(int index) {
   	
      return((GlJournalInterfaceMaintenanceList)glJLIList.get(index));
      
   }

   public Object[] getGlJLIList() {
   	
      return(glJLIList.toArray());
      
   }

   public int getGlJLIListSize() {
   	
      return(glJLIList.size());
      
   }

   public void saveGlJLIList(Object newGlJLIList) {
   	
      glJLIList.add(newGlJLIList);
      
   }

   public void clearGlJLIList() {
   	
      glJLIList.clear();
      
   }

   public void setRowSelected(Object selectedGlJLIList, boolean isEdit) {
   	
      this.rowSelected = glJLIList.indexOf(selectedGlJLIList);
      
      if(isEdit) {
      	
         this.pageState = Constants.PAGE_STATE_EDIT;
         
      }
      
   }

   public void updateGlJLRow(int rowSelected, Object newGlJLIList) {
   	
      glJLIList.set(rowSelected, newGlJLIList);
      
   }

   public void deleteGlJLIList(int rowSelected) {
   	
      glJLIList.remove(rowSelected);
      
   }

   public void setSaveButton(String saveButton) {
   	
      this.saveButton = saveButton;
      
   }
   
   public void setDeleteButton(String deleteButton) {
   	
   	  this.deleteButton = deleteButton;
   	  
   }

   public void setCloseButton(String closeButton) {
   	
      this.closeButton = closeButton;
      
   }

   public void setAddLinesButton(String addLinesButton) {
   	
   	  this.addLinesButton = addLinesButton;   	
   	  
   }
   
   public void setDeleteLinesButton(String deleteLinesButton) {
   	
   	  this.deleteLinesButton = deleteLinesButton;
   	  
   }

   public void setPageState(String pageState) {
   	
      this.pageState = pageState;
      
   }

   public String getPageState() {
   	
      return(pageState);
      
   }

   public String getTxnStatus() {
   	
      String passTxnStatus = txnStatus;      
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
      
   }

   public void setTxnStatus(String txnStatus) {
   	
      this.txnStatus = txnStatus;
      
   }

   public String getUserPermission() {
   	
      return(userPermission);
      
   }

   public void setUserPermission(String userPermission) {
   	
      this.userPermission = userPermission;
      
   }

   public Integer getJournalInterfaceCode() {
   	
      return(journalInterfaceCode);
      
   }   

   public void setJournalInterfaceCode(Integer journalInterfaceCode) {
   	
      this.journalInterfaceCode = journalInterfaceCode;
      
   }

   public String getName() {
   	
      return name;
      
   }

   public void setName(String name) {
   	
      this.name = name;
      
   }
   
   public String getDescription() {
   	
   	  return description;
   	  
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	  
   }

   public String getEffectiveDate() {
   	
      return effectiveDate; 
      
   }

   public void setEffectiveDate(String effectiveDate) {
   	
      this.effectiveDate = effectiveDate;
      
   }
   
   public String getJournalCategory() {
   	
   	  return journalCategory;
   	  
   }
   
   public void setJournalCategory(String journalCategory) {
   	
   	  this.journalCategory = journalCategory;
   	  
   }
   
   public String getJournalSource() {
   	
   	  return journalSource;
   	  
   }
   
   public void setJournalSource(String journalSource) {
   	
   	  this.journalSource = journalSource;
   	  
   }

   public String getCurrency() {
   	
      return currency;
      
   }

   public void setCurrency(String currency) {
   	
      this.currency = currency;
      
   }
   
   public String getDateReversal() {
   	
   	  return dateReversal;
   	  
   }
   
   public void setDateReversal(String dateReversal) {
   	
   	  this.dateReversal = dateReversal;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public void setDocumentNumber(String documentNumber) {
   	
   	  this.documentNumber = documentNumber;
   	  
   }
   
   public String getConversionDate() {
   	
      return conversionDate;
      
   }
   
   public void setConversionDate(String conversionDate) {
   	
   	  this.conversionDate = conversionDate;
   	  
   }
   
   public String getConversionRate() {
   	
   	  return conversionRate;
   	  
   }
   
   public void setConversionRate(String conversionRate) {
   	
   	  this.conversionRate = conversionRate;
   	  
   }
   
   public String getFundStatus() {
   	
   	  return fundStatus;
   	  
   }
   
   public void setFundStatus(String fundStatus) {
   
      this.fundStatus = fundStatus;
   
   }
   
   public boolean getReversed() {
   	
   	  return reversed;
   	  
   }
   
   public void setReversed(boolean reversed) {
   	
   	  this.reversed = reversed;
   	  
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {
   	      
      saveButton = null;    
      deleteButton = null;  
      closeButton = null;
      addLinesButton = null;
      deleteLinesButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
   	
      ActionErrors errors = new ActionErrors();
    
      if(request.getParameter("saveButton") != null) {
      	 
      	 Iterator i = glJLIList.iterator();
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 GlJournalInterfaceMaintenanceList jliList = (GlJournalInterfaceMaintenanceList)i.next();
      	 	 
      	 	 if (Common.validateRequired(jliList.getAccount()) &&
      	 	     Common.validateRequired(jliList.getDebitAmount()) &&
      	 	     Common.validateRequired(jliList.getCreditAmount())) continue;
      	 
	         if(Common.validateRequired(jliList.getAccount())) {
	         	
	            errors.add("account",
	               new ActionMessage("journalInterfaceMaintenance.error.accountRequired", jliList.getLineNumber()));
	               
	         }
	         
		 	 if(!Common.validateMoneyFormat(jliList.getDebitAmount())) {
		 	 	
	            errors.add("debitAmount",
	               new ActionMessage("journalInterfaceMaintenance.error.debitAmountInvalid", jliList.getLineNumber()));
	               
	         }
	         
	         if(!Common.validateMoneyFormat(jliList.getCreditAmount())) {
	         	
	            errors.add("creditAmount",
	               new ActionMessage("journalInterfaceMaintenance.error.creditAmountInvalid", jliList.getLineNumber()));
	               
	         }
	         
			 if(Common.validateRequired(jliList.getDebitAmount()) && Common.validateRequired(jliList.getCreditAmount())) {
			 	
		            errors.add("journalLineAmounts",
		               new ActionMessage("journalInterfaceMaintenance.error.debitCreditAmountRequired", jliList.getLineNumber()));
		               
			 }
			 
			 if(!Common.validateRequired(jliList.getDebitAmount()) && !Common.validateRequired(jliList.getCreditAmount())) {
			 	
		            errors.add("journalLineAmounts",
		               new ActionMessage("journalInterfaceMaintenance.error.debitCreditAmountMustBeNull", jliList.getLineNumber()));
		               
		     }			     
		       
	     }
	    	 
         if (Common.validateRequired(effectiveDate)) {

               errors.add("effectiveDate",
                  new ActionMessage("journalInterfaceMaintenance.error.effectiveDateRequired"));

         }			 		            	    
         
         if (Common.validateRequired(journalCategory)) {

               errors.add("journalCategory",
                  new ActionMessage("journalInterfaceMaintenance.error.journalCategoryRequired"));

         }			 		            	            
         
         if (Common.validateRequired(journalSource)) {

               errors.add("journalCategory",
                  new ActionMessage("journalInterfaceMaintenance.error.journalSourceRequired"));

         }			 		            	          
         
         if (Common.validateRequired(currency)) {

               errors.add("currency",
                  new ActionMessage("journalInterfaceMaintenance.error.functionalCurrencyRequired"));

         }			 		            	     
         
         if (Common.validateRequired(fundStatus)) {

               errors.add("fundStatus",
                  new ActionMessage("journalInterfaceMaintenance.error.fundStatusRequired"));

         }			 		            	           
         
         if (Common.validateRequired(conversionRate)) {

               errors.add("conversionRate",
                  new ActionMessage("journalInterfaceMaintenance.error.conversionRateRequired"));

         }			 		            	                           
         
         if (!Common.validateDateFormat(effectiveDate)) {

            errors.add("effectiveDate",
               new ActionMessage("journalInterfaceMaintenance.error.effectiveDateInvalid"));

         }
         
         if (!Common.validateDateFormat(dateReversal)) {

            errors.add("dateReversal",
               new ActionMessage("journalInterfaceMaintenance.error.dateReversalInvalid"));

         }         
         
         if (!Common.validateDateFormat(conversionDate)) {

            errors.add("conversionDate",
               new ActionMessage("journalInterfaceMaintenance.error.conversionDateInvalid"));

         }                  
                  
         if (!Common.validateNumberFormat(conversionRate)) {

            errors.add("conversionRate",
               new ActionMessage("journalInterfaceMaintenance.error.conversionRateInvalid"));

         }                                                 
	    	 
      }
      
      return(errors);	
      
   }
   
}
