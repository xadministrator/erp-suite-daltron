package com.struts.gl.journalinterfacemaintenance;

import java.io.Serializable;

public class GlJournalInterfaceMaintenanceList implements Serializable {

   private Integer journalLineInterfaceCode = null;
   private String lineNumber = null;
   private String account = null;
   private String debitAmount = null;
   private String creditAmount = null;
   private String accountDescription = null;
   
   private boolean deleteCheckbox = false;
    
   private GlJournalInterfaceMaintenanceForm parentBean;
    
   public GlJournalInterfaceMaintenanceList(GlJournalInterfaceMaintenanceForm parentBean,
      Integer journalLineInterfaceCode,
      String lineNumber,
      String account,
      String debitAmount,
      String creditAmount,
      String accountDescription) {

      this.parentBean = parentBean;
      this.journalLineInterfaceCode = journalLineInterfaceCode;
      this.lineNumber = lineNumber;
      this.account = account;
      this.debitAmount = debitAmount;
      this.creditAmount = creditAmount;
      this.accountDescription = accountDescription;
   }
   
   public Integer getJournalLineInterfaceCode() {
   	
      return(journalLineInterfaceCode);
      
   }

   public String getLineNumber() {
   	
      return(lineNumber);
      
   }
   
   public void setLineNumber(String lineNumber) {
   	
   	  this.lineNumber = lineNumber;
   	  
   }

   public String getAccount() {
   	
      return(account);
      
   }
   
   public void setAccount(String account) {
   	
   	  this.account = account;
   	  
   }

   public String getDebitAmount() {
   	
      return(debitAmount);
      
   }
   
   public void setDebitAmount(String debitAmount) {
   	
   	  this.debitAmount = debitAmount;
   	  
   }

   public String getCreditAmount() {
   	
      return(creditAmount);
      
   }
   
   public void setCreditAmount(String creditAmount) {
   	
   	  this.creditAmount = creditAmount;	
   	  
   }
   
   public String getAccountDescription() {
   	
   	  return(accountDescription);
   	  
   }
   
   public void setAccountDescription(String accountDescription) {
   	
   	  this.accountDescription = accountDescription;
   	  
   }
   
   public boolean getDeleteCheckbox() {   	
   
   	  return deleteCheckbox;   	
   	  
   }
   
   public void setDeleteCheckbox(boolean deleteCheckbox) {
   	
   	  this.deleteCheckbox = deleteCheckbox;
   	  
   }

}