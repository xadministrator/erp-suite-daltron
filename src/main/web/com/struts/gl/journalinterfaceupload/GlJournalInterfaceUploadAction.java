package com.struts.gl.journalinterfaceupload;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.Ostermiller.util.CSVParser;
import com.ejb.exception.GlJRIJournalNotBalanceException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.ejb.txn.GlJournalInterfaceUploadControllerHome;
import com.ejb.txn.GlJournalInterfaceUploadController;
import com.util.GlJournalInterfaceDetails;
import com.util.GlJournalLineInterfaceDetails;

public final class GlJournalInterfaceUploadAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {

        HttpSession session = request.getSession();
        GlJournalInterfaceUploadForm actionForm = (GlJournalInterfaceUploadForm)form;
        
        try {

/*******************************************************
   Check if user has a session
*******************************************************/

        User user = (User) session.getAttribute(Constants.USER_KEY);

        if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlJournalInterfaceUploadAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
                
            }

        } else {

            if (log.isInfoEnabled()) {

                log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

        }
        
        String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_INTERFACE_UPLOAD_ID);

        if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

		        ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	            if (!fieldErrors.isEmpty()) {
	
	                saveErrors(request, new ActionMessages(fieldErrors));
	
	                return mapping.findForward("glJournalInterfaceUpload");
	                
	            }

            }

            actionForm.setUserPermission(frParam.trim());

        } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

        }

/*******************************************************
   Initialize GlJournalInterfaceUploadController EJB
*******************************************************/

        GlJournalInterfaceUploadControllerHome homeJIU = null;
        GlJournalInterfaceUploadController ejbJIU = null;

        try {

            homeJIU = (GlJournalInterfaceUploadControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalInterfaceUploadControllerEJB", GlJournalInterfaceUploadControllerHome.class);
             
        } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlJournalInterfaceUploadAction.execute(): " + e.getMessage() +
                " session: " + session.getId());
                
            }

            return mapping.findForward("cmnErrorPage");

        }

        try {

            ejbJIU = homeJIU.create();
            
        } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlJournalInterfaceUploadAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            
            return mapping.findForward("cmnErrorPage");

        }

        ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Gl JIU Go Action --
*******************************************************/

        if (request.getParameter("goButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {            
            
            
            String fileName = actionForm.getFilename().getFileName();
            String dateString = null;
            String timeString = null;
            String referenceNumberString = null;
            
            
            if (fileName.substring(fileName.indexOf(".") + 1, fileName.length()).equals("csv")) {
            	
            	// get journal header
            	
            	StringTokenizer st = new StringTokenizer(fileName, "_");
            	
            	if (st.countTokens() == 1 || st.countTokens() > 3) {
            		
            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                  		new ActionMessage("journalInterfaceUpload.error.filenameInvalid"));
                    saveErrors(request, new ActionMessages(errors));

                    return(mapping.findForward("glJournalInterfaceUpload"));
                        
                }
            		            		            	            	
            	// get date
            	
				dateString = st.nextToken();
				
				if (dateString != null && dateString.length() == 8) {
				
					dateString = dateString.substring(4, 6) + "/" + dateString.substring(6, 8) + "/" + dateString.substring(0, 4);
					
					if(!Common.validateDateFormat(dateString)) {
	                
	                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
	                    saveErrors(request, new ActionMessages(errors));
	
	                    return(mapping.findForward("glJournalInterfaceUpload"));
	                
	                }
                
                } else {
                	
                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
	                saveErrors(request, new ActionMessages(errors));
	
	                return(mapping.findForward("glJournalInterfaceUpload"));
	                
	            }
				
				// get time
				
				timeString = st.nextToken();
				
				if (st.hasMoreTokens()) {

					if (timeString != null && timeString.length() == 6) {
										
						timeString = timeString.substring(0, 2) + ":" + timeString.substring(2, 4) + ":" + timeString.substring(4, 6);
						
						if(!Common.validateTimeFormat(timeString)) {
			                
			                errors.add(ActionMessages.GLOBAL_MESSAGE, 
			                	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
			                saveErrors(request, new ActionMessages(errors));
			
			                return(mapping.findForward("glJournalInterfaceUpload"));
			                
			            }
			            
	                } else {
	                	
	                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
		                saveErrors(request, new ActionMessages(errors));
		
		                return(mapping.findForward("glJournalInterfaceUpload"));
		                
		            }
		            
		        } else {
		        	
					if (timeString != null && (timeString.length() - 4) == 6) {
										
						timeString = timeString.substring(0, 2) + ":" + timeString.substring(2, 4) + ":" + timeString.substring(4, 6);
						
						if(!Common.validateTimeFormat(timeString)) {
			                
			                errors.add(ActionMessages.GLOBAL_MESSAGE, 
			                	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
			                saveErrors(request, new ActionMessages(errors));
			
			                return(mapping.findForward("glJournalInterfaceUpload"));
			                
			            }
			            
	                } else {
	                	
	                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
		                saveErrors(request, new ActionMessages(errors));
		
		                return(mapping.findForward("glJournalInterfaceUpload"));
		                
		            }
		            
                }		            
		        			        		            
	            // get reference number             	
                                
                if (st.hasMoreTokens()) {
                	
                	referenceNumberString = st.nextToken();
                	referenceNumberString = referenceNumberString.substring(0, referenceNumberString.length() - 4);
                	
                }
								
				GlJournalInterfaceDetails jriDetails = new GlJournalInterfaceDetails();
				
				jriDetails.setJriName("BUS REVENUE " + " " + dateString + " " + timeString);
				
				if (referenceNumberString != null && referenceNumberString.length() >= 1) {
					
					jriDetails.setJriDescription("REF NO. " + referenceNumberString);
										
				} else {
										
					jriDetails.setJriDescription(null);
					
				}
					
				jriDetails.setJriEffectiveDate(Common.convertStringToSQLDate(dateString));
            	
            	// get journal lines
            	
            	ArrayList glJournalLineInterfaceList = new ArrayList();

            	int c, k;
            	
            	String keyString = "Some things are better left unread.";
            	            	            	
            	StringReader key = new StringReader(keyString);
            	            	            	
            	key.mark(keyString.length() + 1);
            	
            	InputStream is = actionForm.getFilename().getInputStream();
            	
            	byte data[] = new byte[is.available()];
            	int ctr = 0;
                while ((c = is.read()) != -1) {
                	
	                if ((k = key.read()) == -1 ) {
		
		            	key.reset();
						k = key.read();
						
	    			}	        	
	        
	    			data[ctr] = (byte)(c ^ k);
	    			ctr++;
	    
				}
	
				ByteArrayInputStream bis = new ByteArrayInputStream(data);
   	               	               	            
   	            CSVParser csvParser = new CSVParser(bis);
            	            
	            String[][] values = csvParser.getAllValues();	            	            
	            
	            for (int i=0; i<values.length; i++) {
	            	
	            	GlJournalLineInterfaceDetails jliDetails = new GlJournalLineInterfaceDetails();
                    	            		            		            		            	
                    if (values[i][0].equals("0") || values[i][0].equals("1")) {

						jliDetails.setJliDebit(new Byte(values[i][0]).byteValue());
	                  		
	                } else {	

						errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    	new ActionMessage("journalInterfaceUpload.error.invalidDebitCreditValue", fileName));
		                saveErrors(request, new ActionMessages(errors));
		
		                return(mapping.findForward("glJournalInterfaceUpload"));	            		
	            		
	            	}
	            	
		            if (Common.validateJournalIntefaceUploadMoneyFormat(values[i][1]) && values[i][1].length() > 0) {

	                    jliDetails.setJliAmount(new Double(values[i][1]).doubleValue());
	                    		                
		            } else {		 
	            	
	            		errors.add(ActionMessages.GLOBAL_MESSAGE, 
		                    new ActionMessage("journalInterfaceUpload.error.amountInvalid", fileName));
		                saveErrors(request, new ActionMessages(errors));
		                
		                return(mapping.findForward("glJournalInterfaceUpload"));    
	            		
	            	}
	            	
	            	jliDetails.setJliCoaAccountNumber(values[i][2]);
	            	
	            	glJournalLineInterfaceList.add(jliDetails);	            	

				}
				
				try {
            
            	    ejbJIU.saveGlJriEntry(jriDetails, glJournalLineInterfaceList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	    
            	} catch (GlobalRecordAlreadyExistException ex) {
            		
            		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  		new ActionMessage("journalInterfaceUpload.error.recordAlreadyExists", fileName));
            		
            	} catch (GlobalAccountNumberInvalidException ex) {
            		
            		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  		new ActionMessage("journalInterfaceUpload.error.accountNumberInvalid", fileName));
            		
            	} catch (GlJRIJournalNotBalanceException ex) {
            		
            		errors.add(ActionMessages.GLOBAL_MESSAGE,
                  		new ActionMessage("journalInterfaceUpload.error.journalNotBalance", fileName));

	            } catch (EJBException ex) {
	
	                if (log.isInfoEnabled()) {
	
	                    log.info("EJBException caught in GlJournalInterfaceUploadAction.execute(): " + ex.getMessage() +
	                    " session: " + session.getId());
	                    return mapping.findForward("cmnErrorPage"); 
	
	                }
	
	            }
	            
	            bis.close();
            	
            } else if (fileName.substring(fileName.indexOf(".") + 1, fileName.length()).equals("zip")) {
            	
            	ZipInputStream zipInputStream = new ZipInputStream(actionForm.getFilename().getInputStream());
            	
            	int BUFFER = 2048;
            	            	 	
            	ByteArrayInputStream bis = null;
    	  		    	  
		    	ZipEntry entry;
		    	  
		    	while ((entry = zipInputStream.getNextEntry()) != null) {
		    		
		    	    int count;
		    	  	  
		    	    byte data[] = new byte[BUFFER];
		    	  	     	  	      	  	  
		    	    while ((count = zipInputStream.read(data, 0, BUFFER)) != -1) {
		    	  	  	
		    	   	    bis = new ByteArrayInputStream(data, 0, count);
		    	  	  	
		    	    }
		    	    
		    	    int c, k;
            	
	            	String keyString = "Some things are better left unread.";
	            	            	            	
	            	StringReader key = new StringReader(keyString);
	            	            	            	
	            	key.mark(keyString.length() + 1);
		    	    
		    	    byte bisData[] = new byte[bis.available()];
	            	int ctr = 0;
	                while ((c = bis.read()) != -1) {
	                	
		                if ((k = key.read()) == -1 ) {
			
			            	key.reset();
							k = key.read();
							
		    			}	        	
		        
		    			bisData[ctr] = (byte)(c ^ k);
		    			ctr++;
		    
					}
		
					ByteArrayInputStream bais = new ByteArrayInputStream(bisData);
		    	   
		    	    // get journal header
            	
	                StringTokenizer st = new StringTokenizer(entry.getName(), "_");
	            	
	            	if (st.countTokens() == 1 || st.countTokens() > 3) {
	            		
	            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  		new ActionMessage("journalInterfaceUpload.error.filenameInvalid"));
	                    saveErrors(request, new ActionMessages(errors));
	
	                    return(mapping.findForward("glJournalInterfaceUpload"));
	                        
	                }
	            		            		            	            	
	            	// get date
	            	
					dateString = st.nextToken();
					
					if (dateString != null && dateString.length() == 8) {
					
						dateString = dateString.substring(4, 6) + "/" + dateString.substring(6, 8) + "/" + dateString.substring(0, 4);
						
						if(!Common.validateDateFormat(dateString)) {
		                
		                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
		                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
		                    saveErrors(request, new ActionMessages(errors));
		
		                    return(mapping.findForward("glJournalInterfaceUpload"));
		                
		                }
	                
	                } else {
	                	
	                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
	                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
		                saveErrors(request, new ActionMessages(errors));
		
		                return(mapping.findForward("glJournalInterfaceUpload"));
		                
		            }
					
					// get time
					
					timeString = st.nextToken();
					
					if (st.hasMoreTokens()) {
	
						if (timeString != null && timeString.length() == 6) {
											
							timeString = timeString.substring(0, 2) + ":" + timeString.substring(2, 4) + ":" + timeString.substring(4, 6);
							
							if(!Common.validateTimeFormat(timeString)) {
				                
				                errors.add(ActionMessages.GLOBAL_MESSAGE, 
				                	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
				                saveErrors(request, new ActionMessages(errors));
				
				                return(mapping.findForward("glJournalInterfaceUpload"));
				                
				            }
				            
		                } else {
		                	
		                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
		                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
			                saveErrors(request, new ActionMessages(errors));
			
			                return(mapping.findForward("glJournalInterfaceUpload"));
			                
			            }
			            
			        } else {
			        	
						if (timeString != null && (timeString.length() - 4) == 6) {
											
							timeString = timeString.substring(0, 2) + ":" + timeString.substring(2, 4) + ":" + timeString.substring(4, 6);
							
							if(!Common.validateTimeFormat(timeString)) {
				                
				                errors.add(ActionMessages.GLOBAL_MESSAGE, 
				                	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
				                saveErrors(request, new ActionMessages(errors));
				
				                return(mapping.findForward("glJournalInterfaceUpload"));
				                
				            }
				            
		                } else {
		                	
		                    errors.add(ActionMessages.GLOBAL_MESSAGE, 
		                    	new ActionMessage("journalInterfaceUpload.error.invalidDateFormat"));
			                saveErrors(request, new ActionMessages(errors));
			
			                return(mapping.findForward("glJournalInterfaceUpload"));
			                
			            }
			            
	                }		            
				   				  					
				    // get reference number
					
				    if (st.hasMoreTokens()) {
						
				        referenceNumberString = st.nextToken();
					    referenceNumberString = referenceNumberString.substring(0, referenceNumberString.length() - 4);
						
				    }				   
									   					
				    GlJournalInterfaceDetails jriDetails = new GlJournalInterfaceDetails();
					
				    jriDetails.setJriName("BUS REVENUE " + " " + dateString + " " + timeString);
				   
				    if (referenceNumberString != null && referenceNumberString.length() >= 1) {
					
					    jriDetails.setJriDescription("REF NO. " + referenceNumberString);
										
				    } else {
										
					    jriDetails.setJriDescription(null);
					
				    }
				   
				    jriDetails.setJriEffectiveDate(Common.convertStringToSQLDate(dateString));
	            	
	                // get journal lines
	            
	                ArrayList glJournalLineInterfaceList = new ArrayList();
	            	
	   	            CSVParser csvParser = new CSVParser(bais);
	            	            
		            String[][] values = csvParser.getAllValues();
		            
		            for (int i=0; i<values.length; i++) {
	            	
		            	GlJournalLineInterfaceDetails jliDetails = new GlJournalLineInterfaceDetails();
	                    	            		            		            		            	
	                    if (values[i][0].equals("0") || values[i][0].equals("1")) {
	
							jliDetails.setJliDebit(new Byte(values[i][0]).byteValue());
		                  		
		                } else {	
	
							errors.add(ActionMessages.GLOBAL_MESSAGE, 
		                    	new ActionMessage("journalInterfaceUpload.error.invalidDebitCreditValue", entry.getName()));
			                saveErrors(request, new ActionMessages(errors));
			
			                return(mapping.findForward("glJournalInterfaceUpload"));	            		
		            		
		            	}
		            	
			            if (Common.validateJournalIntefaceUploadMoneyFormat(values[i][1]) && values[i][1].length() > 0) {
	
		                    jliDetails.setJliAmount(new Double(values[i][1]).doubleValue());
		                    		                
			            } else {		 
		            	
		            		errors.add(ActionMessages.GLOBAL_MESSAGE, 
			                    new ActionMessage("journalInterfaceUpload.error.amountInvalid", entry.getName()));
			                saveErrors(request, new ActionMessages(errors));
			                
			                return(mapping.findForward("glJournalInterfaceUpload"));    
		            		
		            	}
		            	
		            	jliDetails.setJliCoaAccountNumber(values[i][2]);
		            	
		            	glJournalLineInterfaceList.add(jliDetails);	            	

					}
					
				    try {
	            
	            	    ejbJIU.saveGlJriEntry(jriDetails, glJournalLineInterfaceList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	    
	                } catch (GlobalRecordAlreadyExistException ex) {
	            		
	            		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  		new ActionMessage("journalInterfaceUpload.error.recordAlreadyExists", entry.getName()));
	            		
	                } catch (GlobalAccountNumberInvalidException ex) {
	            		
	            		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  		new ActionMessage("journalInterfaceUpload.error.accountNumberInvalid", entry.getName()));
	            		
	                } catch (GlJRIJournalNotBalanceException ex) {
	            		
	            		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                  		new ActionMessage("journalInterfaceUpload.error.journalNotBalance", entry.getName()));
	
		            } catch (EJBException ex) {
		
		                if (log.isInfoEnabled()) {
		
		                    log.info("EJBException caught in GlJournalInterfaceUploadAction.execute(): " + ex.getMessage() +
		                    " session: " + session.getId());
		                    return mapping.findForward("cmnErrorPage"); 
		
		                }
		
		            }		    	   		    	   
		    	  	  		    	  	      	  	  
		            bis.close();
		    	}
		    	
		    	zipInputStream.close();            	          	            	            	            	
            	
            } else {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                	new ActionMessage("journalInterfaceUpload.error.filenameInvalid"));
            	
            }       
            
/*******************************************************
   -- Gl JIU Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));


/*******************************************************
   -- Gl JIU Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glJournalInterfaceUpload");

            }
            
            if (request.getParameter("goButton") != null && 
                actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

            }

            actionForm.reset(mapping, request);

            return(mapping.findForward("glJournalInterfaceUpload"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
      	
      	  e.printStackTrace();

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlJournalInterfaceUploadAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          return mapping.findForward("cmnErrorPage");

       }

    }
}