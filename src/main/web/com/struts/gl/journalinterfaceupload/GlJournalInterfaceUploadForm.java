package com.struts.gl.journalinterfaceupload;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalInterfaceUploadForm extends ActionForm implements Serializable {

   private FormFile filename = null;

   private String goButton = null;
   private String closeButton = null;
   private String pageState = new String();
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public void setGoButton(String goButton) {

      this.goButton = goButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }

   public FormFile getFilename() {
   	
   	  return filename;
   	
   }
   
   public void setFilename(FormFile filename) {
   	
   	  this.filename = filename;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request) {

      filename = null;
      goButton = null;
      closeButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {
      	
	      if(Common.validateRequired(filename.getFileName())){
		     errors.add("filename",
	     	   new ActionMessage("journalInterfaceUpload.error.filenameRequired"));
	 	  }
	 	  
	  }

      return errors;

   }
}