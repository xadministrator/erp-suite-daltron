package com.struts.gl.journalpost;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalPostForm extends ActionForm implements Serializable {

   private String batchName = null;
   private ArrayList batchNameList = new ArrayList();	
   private String name = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String source = null;
   private ArrayList sourceList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();   
   private String approvalStatus = null;
   private ArrayList approvalStatusList = new ArrayList(); 
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
  
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private boolean showBatchName = false;
   private ArrayList glJPList = new ArrayList();
   private int rowSelected = 0;
   private int rowILISelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
    private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   private String maxRows = null;
   private String queryCount = null;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getRowILISelected(){
    return rowILISelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }
   
   public String getMaxRows(){
 	  return maxRows;   	 
   }
 
   public void setMaxRows(String maxRows){  	
 	  this.maxRows = maxRows;
   }
   
   public String getQueryCount(){
	  return queryCount;   	 
   }

   public void setQueryCount(String queryCount){  	
	  this.queryCount = queryCount;
   }
   

   public GlJournalPostList getGlJPByIndex(int index){
      return((GlJournalPostList)glJPList.get(index));
   }

   public Object[] getGlJPList(){
      return(glJPList.toArray());
   }

   public int getGlJPListSize(){
      return(glJPList.size());
   }

   public void saveGlJPList(Object newGlJPList){
      glJPList.add(newGlJPList);
   }

   public void clearGlJPList(){
      glJPList.clear();
   }

   public void setRowSelected(Object selectedGlJPList, boolean isEdit){
      this.rowSelected = glJPList.indexOf(selectedGlJPList);
   }

   public void updateGlJPRow(int rowSelected, Object newGlJPList){
      glJPList.set(rowSelected, newGlJPList);
   }

   public void deleteGlJPList(int rowSelected){
      glJPList.remove(rowSelected);
   }
   
   public void showGlJPRow(int rowSelected) {

   }
   
   public void setRowILISelected(Object selectedArILIList, boolean isEdit){
   	
    this.rowILISelected = glJPList.indexOf(selectedArILIList);
    
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getBatchName() {
	  return(batchName);
   }

   public void setBatchName(String batchName){
	  this.batchName = batchName;
   }

   public ArrayList getBatchNameList() {
	  return(batchNameList);
   }

   public void setBatchNameList(String batchName){
	  batchNameList.add(batchName);
   }

   public void clearBatchNameList(){
	  batchNameList.clear();
	  batchNameList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getName(){
      return(name);
   }

   public void setName(String name){
      this.name = name;
   }
   
   public String getDateFrom() {
   	  return(dateFrom);	  
   }
   
   public void setDateFrom(String dateFrom) {
   	  this.dateFrom = dateFrom;
   }
   
   public String getDateTo() {
   	  return(dateTo);	  
   }
   
   public void setDateTo(String dateTo) {
   	  this.dateTo = dateTo;
   }
   
   public String getDocumentNumberFrom() {
   	  return(documentNumberFrom);
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom){
   	  this.documentNumberFrom = documentNumberFrom;
   }
   
   public String getDocumentNumberTo() {
   	  return(documentNumberTo);
   }
   
   public void setDocumentNumberTo(String documentNumberTo){
   	  this.documentNumberTo = documentNumberTo;
   }   
   
   public String getCategory() {
   	  return(category);
   }
   
   public void setCategory(String category){
   	  this.category = category;
   }
   
   public ArrayList getCategoryList() {
   	  return(categoryList);
   }
   
   public void setCategoryList(String category){
   	  categoryList.add(category);
   }
   
   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getSource() {
   	  return(source);
   }
   
   public void setSource(String source){
   	  this.source = source;
   }
   
   public ArrayList getSourceList() {
   	  return(sourceList);
   }
   
   public void setSourceList(String source){
   	  sourceList.add(source);
   }
   
   public void clearSourceList(){
   	  sourceList.clear();
   	  sourceList.add(Constants.GLOBAL_BLANK);
   }
   
   
   public String getCurrency() {
   	  return(currency);
   }
   
   public void setCurrency(String currency){
   	  this.currency = currency;
   }
   
   public ArrayList getCurrencyList() {
   	  return(currencyList);
   }
   
   public void setCurrencyList(String currency){
   	  currencyList.add(currency);
   }
   
   public void clearCurrencyList(){
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   }
        
   public String getApprovalStatus(){
   	  return(approvalStatus);
   }
   
   public void setApprovalStatus(String approvalStatus){
   	  this.approvalStatus = approvalStatus;
   }
   
   public ArrayList getApprovalStatusList(){
   	  return(approvalStatusList);
   }
   
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   } 
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
   	  batchName = Constants.GLOBAL_BLANK;
      name = null;
      dateFrom = null;
      dateTo = null;
      documentNumberFrom = null;
      documentNumberTo = null;
      category = Constants.GLOBAL_BLANK;
      source = Constants.GLOBAL_BLANK;
      currency = Constants.GLOBAL_BLANK;
      approvalStatusList.clear();
      approvalStatusList.add(Constants.GLOBAL_BLANK);
      approvalStatusList.add("APPROVED");
      approvalStatusList.add("N/A");
      approvalStatus = Constants.GLOBAL_BLANK;
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.GL_JR_ORDER_BY_CATEGORY);
	      orderByList.add(Constants.GL_JR_ORDER_BY_DOCUMENT_NUMBER);
	      orderByList.add(Constants.GL_JR_ORDER_BY_NAME);
	      orderByList.add(Constants.GL_JR_ORDER_BY_SOURCE);
	      orderBy = Constants.GLOBAL_BLANK;                  
	      
	  }
	  
	  for (int i=0; i<glJPList.size(); i++) {
	  	
	  	  GlJournalPostList actionList = (GlJournalPostList)glJPList.get(i);
	  	  
	  	  	actionList.setPost(false);
	  	
	  }
	  
      previousButton = null;
      nextButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
       ActionErrors errors = new ActionErrors();
      
       if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null || 
       	   request.getParameter("previousButton") != null) {   
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("journalPost.error.dateFromInvalid"));
		
		   }         
		 
	 	   if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("journalPost.error.dateToInvalid"));
		
		   }
	 	  
	 	   if (Common.validateRequired(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("journalPost.error.maxRowsRequired"));
		
		   }
	 	  
	 	   if (!Common.validateNumberFormat(maxRows)) {
	 		
		     errors.add("maxRows",
		        new ActionMessage("journalPost.error.maxRowsInvalid"));
		
		   }
	 	  
      }                 
	       
      return(errors);
   }
}
