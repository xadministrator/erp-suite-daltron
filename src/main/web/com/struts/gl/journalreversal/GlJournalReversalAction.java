package com.struts.gl.journalreversal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlJRJournalAlreadyReversedException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlJournalReversalController;
import com.ejb.txn.GlJournalReversalControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModFunctionalCurrencyDetails;
import com.util.GlModJournalDetails;

public final class GlJournalReversalAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlJournalReversalAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlJournalReversalForm actionForm = (GlJournalReversalForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_REVERSAL_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glJournalReversal");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlJournalReversalController EJB
*******************************************************/

         GlJournalReversalControllerHome homeJR = null;
         GlJournalReversalController ejbJR = null;

         try {
          
            homeJR = (GlJournalReversalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalReversalControllerEJB", GlJournalReversalControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlJournalReversalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbJR = homeJR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlJournalReversalAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbJR.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlJournalReversalAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	    

/*******************************************************
   -- Gl JR Previous Action --
*******************************************************/ 

         if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Gl JR Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 
         
/*******************************************************
   -- Gl JR Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null || request.getParameter("forward") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("journalName", actionForm.getName());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDateFrom())) {
	        		
	        		criteria.put("dateFrom", Common.convertStringToSQLDate(actionForm.getDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDateTo())) {
	        		
	        		criteria.put("dateTo", Common.convertStringToSQLDate(actionForm.getDateTo()));
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberFrom())) {
	        		
	        		criteria.put("documentNumberFrom", actionForm.getDocumentNumberFrom());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getDocumentNumberTo())) {
	        		
	        		criteria.put("documentNumberTo", actionForm.getDocumentNumberTo());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReversalDateFrom())) {
	        		
	        		criteria.put("reversalDateFrom", Common.convertStringToSQLDate(actionForm.getReversalDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getReversalDateTo())) {
	        		
	        		criteria.put("reversalDateTo", Common.convertStringToSQLDate(actionForm.getReversalDateTo()));
	        		
	        	}
	        	
	        	
	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("category", actionForm.getCategory());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getSource())) {
	        		
	        		criteria.put("source", actionForm.getSource());
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getCurrency())) {
	        		
	        		criteria.put("currency", actionForm.getCurrency());
	        		
	        	}
	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	} else if (request.getParameter("forward") != null) {
	     		
	     		HashMap criteria = new HashMap();
	     		
	     		criteria.put("reversalDateFrom", Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	     		criteria.put("reversalDateTo", Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	     		
	     		// save criteria
	     		
	     		actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	     		
	     	}
            
            try {
            	
            	actionForm.clearGlJRList();
            	
            	ArrayList list = ejbJR.getGlJrReversibleByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	
            	while (i.hasNext()) {
            		
            		GlModJournalDetails mdetails = (GlModJournalDetails)i.next();
            		
            		String PERIOD_STATUS = null;
            		
            		if (mdetails.getJrAcvStatus() == 'F') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_FUTURE_ENTRY;
            			
            		} else if (mdetails.getJrAcvStatus() == 'O') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_OPEN;
            			
            		} else if (mdetails.getJrAcvStatus() == 'C') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_CLOSED;
            			
            		} else if (mdetails.getJrAcvStatus() == 'P') {
            			
            			PERIOD_STATUS = Constants.GL_PERIOD_STATUS_PERMANENTLY_CLOSED;
            			
            		}
            		
            		GlJournalReversalList jrList = new GlJournalReversalList(
            			actionForm, 
            			mdetails.getJrCode(),
            			mdetails.getJrName(), 
            			Common.convertSQLDateToString(mdetails.getJrEffectiveDate()),
            			mdetails.getJrDocumentNumber(),
            			Common.convertSQLDateToString(mdetails.getJrDateReversal()),
            			mdetails.getJrDescription(),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalDebit(), precisionUnit),
            			Common.convertDoubleToStringMoney(mdetails.getJrTotalCredit(), precisionUnit));
            			
            	   actionForm.saveGlJRList(jrList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("journalReversal.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlJournalReversalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glJournalReversal");

            }
                        
            actionForm.reset(mapping, request); 
            return mapping.findForward("glJournalReversal");                      

/*******************************************************
   -- Gl JR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl JR Reversal Action --
*******************************************************/

         } else if (request.getParameter("reverseButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	             
  	         // get reversed journals
                        
		    for(int i=0; i<actionForm.getGlJRListSize(); i++) {
		    
		       GlJournalReversalList actionList = actionForm.getGlJRByIndex(i);
		    	
               if (actionList.getReverse()) {
               	
               	     try {
               	     	
               	     	ejbJR.executeGlJrReverse(actionList.getJournalCode(), 
               	     	actionList.getNewDocNum(), 
               	     	Common.convertStringToSQLDate(actionList.getReversalDate()),
               	     	user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
               	     	
               	     	actionForm.deleteGlJRList(i);
               	     	i--;
               	  
			         } catch (GlobalRecordAlreadyExistException ex) {
			       	
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalReversal.error.recordAlreadyExist", actionList.getName()));	
			       	
			         } catch (GlJREffectiveDateNoPeriodExistException ex) {
			       	
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalReversal.error.effectiveDateNoPeriodExist", actionList.getName()));	
			       	
			         } catch (GlobalDocumentNumberNotUniqueException ex) {
			       	   
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalReversal.error.documentNumberNotUnique", actionList.getName()));	
			       	 
			         } catch (GlJREffectiveDateViolationException ex) {
			       	
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalReversal.error.effectiveDateViolation", actionList.getName()));	
			       	
			         } catch (GlJRJournalAlreadyReversedException ex) {
			       	
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalReversal.error.journalAlreadyReversed", actionList.getName()));	
			       	
			         } catch (GlobalRecordAlreadyDeletedException ex) {
			       	   
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalReversal.error.recordAlreadyDeleted", actionList.getName()));	
			       
			         } catch (GlJREffectiveDatePeriodClosedException ex) {
			       	   
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("journalReversal.error.effectiveDatePeriodClosed", actionList.getName()));
		                  
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in GlJournalReversalAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 	               
	            
	           }
	           
	       }		      
		
/*******************************************************
   -- Gl JR Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glJournalReversal");

            }
            
            actionForm.clearGlJRList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearCategoryList();           	
            	
            	list = ejbJR.getGlJcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCategoryList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearSourceList();           	
            	
            	list = ejbJR.getGlJsAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setSourceList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setSourceList((String)i.next());
            			
            		}
            		
            	}
            	
            	actionForm.clearCurrencyList();           	
            	
            	list = ejbJR.getGlFcAllWithDefault(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCurrencyList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCurrencyList(((GlModFunctionalCurrencyDetails)i.next()).getFcName());
            			
            		}
            		
            	}
            	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlJournalReversalAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }             
            
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("reverseButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            actionForm.reset(mapping, request);
            actionForm.setLineCount(0);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
	                       
            return(mapping.findForward("glJournalReversal"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlJournalReversalAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();

          return mapping.findForward("cmnErrorPage");

       }

    }
}