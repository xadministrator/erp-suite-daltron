package com.struts.gl.journalreversal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalReversalForm extends ActionForm implements Serializable {

   private String name = null;
   private String dateFrom = null;
   private String dateTo = null;
   private String documentNumberFrom = null;
   private String documentNumberTo = null;
   private String reversalDateFrom = null;
   private String reversalDateTo = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String source = null;
   private ArrayList sourceList = new ArrayList();
   private String currency = null;
   private ArrayList currencyList = new ArrayList();   
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 

   private ArrayList glJRList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
    private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public GlJournalReversalList getGlJRByIndex(int index){
      return((GlJournalReversalList)glJRList.get(index));
   }

   public Object[] getGlJRList(){
      return(glJRList.toArray());
   }

   public int getGlJRListSize(){
      return(glJRList.size());
   }

   public void saveGlJRList(Object newGlJRList){
      glJRList.add(newGlJRList);
   }

   public void clearGlJRList(){
      glJRList.clear();
   }

   public void setRowSelected(Object selectedGlJRList, boolean isEdit){
      this.rowSelected = glJRList.indexOf(selectedGlJRList);
   }

   public void updateGlJRRow(int rowSelected, Object newGlJRList){
      glJRList.set(rowSelected, newGlJRList);
   }

   public void deleteGlJRList(int rowSelected){
      glJRList.remove(rowSelected);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getName(){
      return(name);
   }

   public void setName(String name){
      this.name = name;
   }
   
   public String getDateFrom() {
   	  return(dateFrom);	  
   }
   
   public void setDateFrom(String dateFrom) {
   	  this.dateFrom = dateFrom;
   }
   
   public String getDateTo() {
   	  return(dateTo);	  
   }
   
   public void setDateTo(String dateTo) {
   	  this.dateTo = dateTo;
   }
   
   public String getDocumentNumberFrom() {
   	  return(documentNumberFrom);
   }
   
   public void setDocumentNumberFrom(String documentNumberFrom){
   	  this.documentNumberFrom = documentNumberFrom;
   }
   
   public String getDocumentNumberTo() {
   	  return(documentNumberTo);
   }
   
   public void setDocumentNumberTo(String documentNumberTo){
   	  this.documentNumberTo = documentNumberTo;
   }   

   public String getReversalDateFrom() {
   	  return(reversalDateFrom);
   }
   
   public void setReversalDateFrom(String reversalDateFrom){
   	  this.reversalDateFrom = reversalDateFrom;
   }
   
   public String getReversalDateTo() {
   	  return(reversalDateTo);
   }
   
   public void setReversalDateTo(String reversalDateTo){
   	  this.reversalDateTo = reversalDateTo;
   }  
   
   public String getCategory() {
   	  return(category);
   }
   
   public void setCategory(String category){
   	  this.category = category;
   }
   
   public ArrayList getCategoryList() {
   	  return(categoryList);
   }
   
   public void setCategoryList(String category){
   	  categoryList.add(category);
   }
   
   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getSource() {
   	  return(source);
   }
   
   public void setSource(String source){
   	  this.source = source;
   }
   
   public ArrayList getSourceList() {
   	  return(sourceList);
   }
   
   public void setSourceList(String source){
   	  sourceList.add(source);
   }
   
   public void clearSourceList(){
   	  sourceList.clear();
   	  sourceList.add(Constants.GLOBAL_BLANK);
   }
   
   
   public String getCurrency() {
   	  return(currency);
   }
   
   public void setCurrency(String currency){
   	  this.currency = currency;
   }
   
   public ArrayList getCurrencyList() {
   	  return(currencyList);
   }
   
   public void setCurrencyList(String currency){
   	  currencyList.add(currency);
   }
   
   public void clearCurrencyList(){
   	  currencyList.clear();
   	  currencyList.add(Constants.GLOBAL_BLANK);
   }
   
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
      name = null;
      dateFrom = null;
      dateTo = null;
      documentNumberFrom = null;
      documentNumberTo = null;
      reversalDateFrom = null;
      reversalDateTo = null;
      category = Constants.GLOBAL_BLANK;
      source = Constants.GLOBAL_BLANK;
      currency = Constants.GLOBAL_BLANK;
      if (orderByList.isEmpty()) {
      	
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.GL_JR_ORDER_BY_CATEGORY);
	      orderByList.add(Constants.GL_JR_ORDER_BY_DOCUMENT_NUMBER);
	      orderByList.add(Constants.GL_JR_ORDER_BY_NAME);
	      orderByList.add(Constants.GL_JR_ORDER_BY_SOURCE);
	      orderBy = Constants.GLOBAL_BLANK;                  
	      
      }
      previousButton = null;
      nextButton = null;
         
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
   	
       ActionErrors errors = new ActionErrors();
       
	   if(request.getParameter("goButton") != null) {       
      
	       if (!Common.validateDateFormat(dateFrom)) {
	
		     errors.add("dateFrom",
		        new ActionMessage("journalReversal.error.dateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(dateTo)) {
		
		     errors.add("dateTo",
		        new ActionMessage("journalReversal.error.dateToInvalid"));
		
		  }                  
		 		  
	       if (!Common.validateDateFormat(reversalDateFrom)) {
	
		     errors.add("reversalDateFrom",
		        new ActionMessage("journalReversal.error.dateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(reversalDateTo)) {
		
		     errors.add("reversalDateTo",
		        new ActionMessage("journalReversal.error.dateToInvalid"));
		
		  }
		  
      }

	  if(request.getParameter("reverseButton") != null) { 

		  Iterator i = glJRList.iterator();      	 
		  	 
		  while (i.hasNext()) {
		  	 	
		   	 GlJournalReversalList jrList = (GlJournalReversalList)i.next();      	 	 	 
		   	 
		   	 if (!jrList.getReverse()) continue;
		  	 
		     if(!Common.validateNumberFormat(jrList.getNewDocNum())){
		         errors.add("newDocNum",
		           new ActionMessage("journalReversal.error.newDocNumInvalid", jrList.getName()));
		     }
		     
		     if(Common.validateRequired(jrList.getReversalDate())){
		        errors.add("reversalDate",
		           new ActionMessage("journalReversal.error.reversalDateRequired", jrList.getName()));
		     }	     
			 
			 if(!Common.validateDateFormat(jrList.getReversalDate())){
		         errors.add("reversalDate",
		           new ActionMessage("journalReversal.error.reversalDateInvalid", jrList.getName()));
		     }    		     
		      
		  }
		  
      }
      
      return(errors);
   }
}
