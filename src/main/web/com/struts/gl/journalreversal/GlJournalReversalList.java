package com.struts.gl.journalreversal;

import java.io.Serializable;

public class GlJournalReversalList implements Serializable {

   private Integer journalCode = null;
   private String name = null;
   private String date = null;
   private String documentNumber = null;  
   private String reversalDate = null; 
   private String description = null;
   private String totalDebit = null;
   private String totalCredit = null;
   private String newDocNum = null;

   private boolean reverse = false;
    
   private GlJournalReversalForm parentBean;
    
   public GlJournalReversalList(GlJournalReversalForm parentBean,
      Integer journalCode,
      String name,
      String date,
      String documentNumber,
      String reversalDate,
      String description,
      String totalDebit,
      String totalCredit){

      this.parentBean = parentBean;
      this.journalCode = journalCode;
      this.name = name;
      this.date = date;
      this.documentNumber = documentNumber;
      this.reversalDate = reversalDate;
      this.description = description;
      this.totalDebit = totalDebit;
      this.totalCredit = totalCredit;

   }
   
   public Integer getJournalCode(){
      return(journalCode);
   }

   public String getName(){
      return(name);
   }

   public String getDate(){
      return(date);
   }
   
   public String getDocumentNumber(){
      return(documentNumber);
   }
   
   public String getReversalDate(){
   	  return(reversalDate);
   }
   
   public void setReversalDate(String reversalDate){
   	  this.reversalDate = reversalDate;
   }

   public String getDescription(){
      return(description);
   }

   public String getTotalDebit(){
      return(totalDebit);
   }

   public String getTotalCredit(){
      return(totalCredit);
   }
   
   public String getNewDocNum(){
   	  return(newDocNum);
   }
   
   public void setNewDocNum(String newDocNum){
   	  this.newDocNum = newDocNum;
   }
   
   public boolean getReverse() {
   	  return(reverse);
   }

   public void setReverse(boolean reverse){
   	  this.reverse = reverse;   	
   }
   
}

