package com.struts.gl.journalsources;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJSJournalSourceAlreadyAssignedException;
import com.ejb.exception.GlJSJournalSourceAlreadyDeletedException;
import com.ejb.exception.GlJSJournalSourceAlreadyExistException;
import com.ejb.exception.GlJSNoJournalSourceFoundException;
import com.ejb.txn.GlJournalSourceController;
import com.ejb.txn.GlJournalSourceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlJournalSourceDetails;

public final class GlJournalSourcesAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlJournalSourcesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         String frParam = Common.getUserPermission(user, Constants.GL_JOURNAL_SOURCES_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((GlJournalSourcesForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glJournalSources"));
               }
            }
            ((GlJournalSourcesForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlJournalSourcesForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlJournalSourceController EJB
*******************************************************/

         GlJournalSourceControllerHome homeJS = null;
         GlJournalSourceController ejbJS = null;

         try{
            
            homeJS = (GlJournalSourceControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlJournalSourceControllerEJB", GlJournalSourceControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlJournalSourcesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbJS = homeJS.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlJournalSourcesAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl DS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlJournalSourcesForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glJournalSources"));
	     
/*******************************************************
   -- Gl DS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlJournalSourcesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glJournalSources"));                  

/*******************************************************
   -- Gl  JS Save Action --
*******************************************************/

         } else if(request.getParameter("saveButton") != null &&
            ((GlJournalSourcesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            GlJournalSourceDetails details = new GlJournalSourceDetails(
	       ((GlJournalSourcesForm)form).getJournalSourceName().trim().toUpperCase(),
	       ((GlJournalSourcesForm)form).getDescription().trim().toUpperCase(),
	       Common.convertBooleanToByte(((GlJournalSourcesForm)form).getFreezeJournals()),
	       Common.convertBooleanToByte(((GlJournalSourcesForm)form).getRequireJournalApproval()),
	       Common.getGlEffectiveDateRuleCode(((GlJournalSourcesForm)form).getEffectiveDateRule()));



/*******************************************************
   Call GlJournalSourcesController EJB
   addGlJsEntry method
*******************************************************/

              try{
	         ejbJS.addGlJsEntry(details, user.getCmpCode());
	      }catch(GlJSJournalSourceAlreadyExistException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("journalSources.error.journalSourceAlreadyExists"));
              }catch(EJBException ex){
                 if(log.isInfoEnabled()){
                    log.info("EJBException caught in GlJournalSourcesAction.execute(): " + ex.getMessage() +
                    " session: " + session.getId());
                 }
                 return(mapping.findForward("cmnErrorPage"));
              }
	      
/*******************************************************
   -- Gl  JS Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));
/*******************************************************
   -- Gl  JS Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
            ((GlJournalSourcesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	    GlJournalSourcesList glJSList =
              ((GlJournalSourcesForm)form).getGlJSByIndex(((GlJournalSourcesForm) form).getRowSelected());
	    
	    if (!((GlJournalSourcesForm)form).getJournalSourceName().equals(glJSList.getJournalSourceName()) && (glJSList.getJournalSourceName().equals("ACCOUNTS PAYABLES") ||
	    	glJSList.getJournalSourceName().equals("ACCOUNTS RECEIVABLES") ||
	    	glJSList.getJournalSourceName().equals("CASH MANAGEMENT") ||
	    	glJSList.getJournalSourceName().equals("JOURNAL REVERSAL") ||
	    	glJSList.getJournalSourceName().equals("MANUAL") ||
	    	glJSList.getJournalSourceName().equals("RECURRING") || 
			glJSList.getJournalSourceName().equals("INVENTORY"))) {
	    	
	    	errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("journalSources.error.systemKeywordError"));
	    	
	    	saveErrors(request, new ActionMessages(errors));
            return(mapping.findForward("glJournalSources"));
            
	    }
	    
	    if (!String.valueOf(((GlJournalSourcesForm)form).getFreezeJournals()).equals(glJSList.getJournalSourceName()) && 
	        glJSList.getJournalSourceName().equals("MANUAL")) {
		    	
		    errors.add(ActionMessages.GLOBAL_MESSAGE,
	             new ActionMessage("journalSources.error.systemKeywordJournalFreezeError"));
		    	
		    saveErrors(request, new ActionMessages(errors));
	        return(mapping.findForward("glJournalSources"));
	            
		}

            GlJournalSourceDetails details = new GlJournalSourceDetails(glJSList.getJournalSourceCode(),
	      ((GlJournalSourcesForm)form).getJournalSourceName().trim().toUpperCase(),
              ((GlJournalSourcesForm)form).getDescription().trim().toUpperCase(),
	      Common.convertBooleanToByte(((GlJournalSourcesForm)form).getFreezeJournals()),
	      Common.convertBooleanToByte(((GlJournalSourcesForm)form).getRequireJournalApproval()),
	      Common.getGlEffectiveDateRuleCode(((GlJournalSourcesForm)form).getEffectiveDateRule()));

/*******************************************************
   Call GlJournalSourcesController EJB
   updateGlJsEntry method
*******************************************************/

            try{
	       ejbJS.updateGlJsEntry(details, user.getCmpCode());
	    }catch(GlJSJournalSourceAlreadyExistException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("journalSources.error.journalSourceAlreadyExists"));
	    }catch(GlJSJournalSourceAlreadyAssignedException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("journalSources.error.updateJournalSourceAlreadyAssigned"));
	    }catch(GlJSJournalSourceAlreadyDeletedException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("journalSources.error.journalSourceAlreadyDeleted"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlJournalSourcesAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }


/*******************************************************
   -- Gl  JS Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl  JS Edit Action --
*******************************************************/

         }else if(request.getParameter("glJSList[" + 
            ((GlJournalSourcesForm)form).getRowSelected() + "].editButton") != null &&
            ((GlJournalSourcesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlJournalSourcesForm properties
*******************************************************/

            ((GlJournalSourcesForm)form).showGlJSRow(((GlJournalSourcesForm) form).getRowSelected());
            return(mapping.findForward("glJournalSources"));
	    
/*******************************************************
   -- Gl  JS Delete Action --
*******************************************************/

         }else if(request.getParameter("glJSList[" +
            ((GlJournalSourcesForm)form).getRowSelected() + "].deleteButton") != null &&
            ((GlJournalSourcesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
 
            GlJournalSourcesList glJSList =
              ((GlJournalSourcesForm)form).getGlJSByIndex(((GlJournalSourcesForm) form).getRowSelected());
              
            if (glJSList.getJournalSourceName().equals("ACCOUNTS PAYABLES") ||
		    	glJSList.getJournalSourceName().equals("ACCOUNTS RECEIVABLES") ||
		    	glJSList.getJournalSourceName().equals("CASH MANAGEMENT") ||
		    	glJSList.getJournalSourceName().equals("JOURNAL REVERSAL") ||
		    	glJSList.getJournalSourceName().equals("MANUAL") ||
		    	glJSList.getJournalSourceName().equals("RECURRING") ||
				glJSList.getJournalSourceName().equals("INVENTORY")) {
		    	
		    	errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   new ActionMessage("journalSources.error.systemKeywordError"));
		    	
		    	saveErrors(request, new ActionMessages(errors));
	            return(mapping.findForward("glJournalSources"));
	            
		    }

/*******************************************************
   Call GlJournalSourcesController EJB
   deleteGlJsEntry method
*******************************************************/

            try{
	       ejbJS.deleteGlJsEntry(glJSList.getJournalSourceCode(), user.getCmpCode());
	    }catch(GlJSJournalSourceAlreadyAssignedException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
	          new ActionMessage("journalSources.error.deleteJournalSourceAlreadyAssigned"));
	    }catch(GlJSJournalSourceAlreadyDeletedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	          new ActionMessage("journalSources.error.journalSourceAlreadyDeleted"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlJournalSourcesAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

/*******************************************************
   -- Gl  JS Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glJournalSources"));
            }

            
/*******************************************************
   Call GlJournalSourceController EJB getGlJcAll() method
   Populate ArrayList property from method ArrayList return
*******************************************************/

             ((GlJournalSourcesForm)form).clearGlJSList();
	     
             try{
	        ArrayList ejbGlJSList = ejbJS.getGlJsAll(user.getCmpCode());
	        Iterator i = ejbGlJSList.iterator();
	        while(i.hasNext()){
	           GlJournalSourceDetails details = (GlJournalSourceDetails)i.next();
	           GlJournalSourcesList glJSList = new GlJournalSourcesList(((GlJournalSourcesForm)form),
                      details.getJsCode(),
                      details.getJsName(),
                      details.getJsDescription(),
		      Common.convertByteToBoolean(details.getJsFreezeJournal()),
		      Common.convertByteToBoolean(details.getJsJournalApproval()),
                      Common.getGlEffectiveDateRuleDescription(details.getJsEffectiveDateRule()));

                      ((GlJournalSourcesForm) form).saveGlJSList(glJSList);
                }
             }catch(GlJSNoJournalSourceFoundException ex){
                
             }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlJournalSourcesAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                }
                return(mapping.findForward("cmnErrorPage"));
             }
	    
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
            }else{
               if((request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null || 
                   request.getParameter("glJSList[" + 
                   ((GlJournalSourcesForm) form).getRowSelected() + "].deleteButton") != null) && 
                   ((GlJournalSourcesForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                   ((GlJournalSourcesForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
               }
            }

            ((GlJournalSourcesForm)form).reset(mapping, request);
            
	        if (((GlJournalSourcesForm)form).getTableType() == null) {
      		      	
	           ((GlJournalSourcesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            ((GlJournalSourcesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("glJournalSources"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()){
             log.info("Exception caught in GlJournalSourcesAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
