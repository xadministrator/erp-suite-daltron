package com.struts.gl.journalsources;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlJournalSourcesForm extends ActionForm implements Serializable {

   private String journalSourceName = null;
   private String description = null;
   private boolean freezeJournals = false;
   private boolean requireJournalApproval = false;
   private String effectiveDateRule = null;
   private ArrayList effectiveDateRuleList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glJSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected(){
      return rowSelected;
   }

   public GlJournalSourcesList getGlJSByIndex(int index){
      return((GlJournalSourcesList)glJSList.get(index));
   }

   public Object[] getGlJSList(){
      return(glJSList.toArray());
   }

   public int getGlJSListSize(){
      return(glJSList.size());
   }

   public void saveGlJSList(Object newGlJSList){
      glJSList.add(newGlJSList);
   }

   public void clearGlJSList(){
      glJSList.clear();
   }

   public void setRowSelected(Object selectedGlJSList, boolean isEdit){
      this.rowSelected = glJSList.indexOf(selectedGlJSList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showGlJSRow(int rowSelected){
      this.journalSourceName = ((GlJournalSourcesList)glJSList.get(rowSelected)).getJournalSourceName();
      this.description = ((GlJournalSourcesList)glJSList.get(rowSelected)).getDescription();
      this.freezeJournals = ((GlJournalSourcesList)glJSList.get(rowSelected)).getFreezeJournals();
      this.requireJournalApproval = ((GlJournalSourcesList)glJSList.get(rowSelected)).getRequireJournalApproval();
      this.effectiveDateRule = ((GlJournalSourcesList)glJSList.get(rowSelected)).getEffectiveDateRule();
   }

   public void updateGlJSRow(int rowSelected, Object newGlJSList){
      glJSList.set(rowSelected, newGlJSList);
   }

   public void deleteGlJSList(int rowSelected){
      glJSList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getJournalSourceName(){
      return(journalSourceName);
   }

   public void setJournalSourceName(String journalSourceName){
      this.journalSourceName = journalSourceName;
   }

   public String getDescription(){
      return(description);
   }

   public void setDescription(String description){
      this.description = description;
   }

   public boolean getFreezeJournals(){
      return(freezeJournals);
   }

   public void setFreezeJournals(boolean freezeJournals){
      this.freezeJournals = freezeJournals;
   }

   public boolean getRequireJournalApproval(){
      return(requireJournalApproval);
   }

   public void setRequireJournalApproval(boolean requireJournalApproval){
      this.requireJournalApproval = requireJournalApproval;
   }

   public String getEffectiveDateRule(){
      return(effectiveDateRule);
   }

   public void setEffectiveDateRule(String effectiveDateRule){
      this.effectiveDateRule = effectiveDateRule;
   }

   public ArrayList getEffectiveDateRuleList(){
      return(effectiveDateRuleList);
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
      journalSourceName = null;
      description = null;
      freezeJournals = false;
      requireJournalApproval = false;
      effectiveDateRule = null;
      effectiveDateRuleList.clear();
      effectiveDateRuleList.add(Constants.GL_EFFECTIVE_DATE_RULE_FAIL);
      effectiveDateRuleList.add(Constants.GL_EFFECTIVE_DATE_RULE_LEAVE_ALONE);
      effectiveDateRuleList.add(Constants.GL_EFFECTIVE_DATE_RULE_ROLL_DATE);
      effectiveDateRule = Constants.GL_EFFECTIVE_DATE_RULE_FAIL;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(journalSourceName)){
            errors.add("journalSourceName",
               new ActionMessage("journalSources.error.journalSourceNameRequired"));
         }
         if(Common.validateRequired(effectiveDateRule)){
            errors.add("effectiveDateRule",
               new ActionMessage("journalSources.error.effectiveDateRuleRequired"));
         }
         if(!Common.validateStringExists(effectiveDateRuleList, effectiveDateRule)){
            errors.add("effectiveDateRule",
               new ActionMessage("journalSources.error.effectiveDateRuleInvalid"));
         }
      }
      return(errors);
   }
}
