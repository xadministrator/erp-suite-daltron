package com.struts.gl.journalsources;

import java.io.Serializable;

public class GlJournalSourcesList implements Serializable {

   private Integer journalSourceCode = null;
   private String journalSourceName = null;
   private String description = null;
   private boolean freezeJournals = false;
   private boolean requireJournalApproval = false;
   private String effectiveDateRule = null;

   private String deleteButton = null;
   private String editButton = null;
    
   private GlJournalSourcesForm parentBean;
    
   public GlJournalSourcesList(GlJournalSourcesForm parentBean,
      Integer journalSourceCode,
      String journalSourceName,
      String description,
      boolean freezeJournals,
      boolean requireJournalApproval,
      String effectiveDateRule){

      this.parentBean = parentBean;
      this.journalSourceCode = journalSourceCode;
      this.journalSourceName = journalSourceName;
      this.description = description;
      this.freezeJournals = freezeJournals;
      this.requireJournalApproval = requireJournalApproval;
      this.effectiveDateRule = effectiveDateRule;
   }

   public void setDeleteButton(String deleteButton){
      parentBean.setRowSelected(this, false);
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public Integer getJournalSourceCode(){
      return(journalSourceCode);
   }

   public String getJournalSourceName(){
      return(journalSourceName);
   }

   public String getDescription(){
      return(description);
   }

   public boolean getFreezeJournals(){
      return(freezeJournals);
   }

   public boolean getRequireJournalApproval(){
      return(requireJournalApproval);
   }

   public String getEffectiveDateRule(){
      return(effectiveDateRule);
   }

}