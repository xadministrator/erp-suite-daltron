package com.struts.gl.opencloseperiods;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoSetOfBookFoundException;
import com.ejb.txn.GlOpenClosePeriodController;
import com.ejb.txn.GlOpenClosePeriodControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlAccountingCalendarValueDetails;

public final class GlOpenClosePeriodsAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlOpenClosePeriodsAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         String frParam = Common.getUserPermission(user, Constants.GL_OPEN_CLOSE_PERIODS_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((GlOpenClosePeriodsForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glOpenClosePeriods"));
               }
            }
            ((GlOpenClosePeriodsForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlOpenClosePeriodsForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlOpenClosePeriodController EJB
*******************************************************/

         GlOpenClosePeriodControllerHome homeOCP = null;
         GlOpenClosePeriodController ejbOCP = null;

         try{
            
            homeOCP = (GlOpenClosePeriodControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlOpenClosePeriodControllerEJB", GlOpenClosePeriodControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlOpenClosePeriodsAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbOCP = homeOCP.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlOpenClosePeriodsAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Gl DS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlOpenClosePeriodsForm) form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glOpenClosePeriods"));
	     
/*******************************************************
   -- Gl DS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlOpenClosePeriodsForm) form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glOpenClosePeriods"));         

/*******************************************************
   -- Gl  OCP Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));
	    
/*******************************************************
   -- Gl  OCP Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
            ((GlOpenClosePeriodsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

           GlOpenClosePeriodsList glOCPList =
              ((GlOpenClosePeriodsForm)form).getGlOCPByIndex(((GlOpenClosePeriodsForm) form).getRowSelected());

	    GlAccountingCalendarValueDetails details = new GlAccountingCalendarValueDetails(
	       glOCPList.getPeriodCode(),
	       glOCPList.getPeriod(),
	       Common.convertStringToShort(glOCPList.getQuarterNumber()),
	       Common.convertStringToShort(glOCPList.getPeriodNumber()),
	       Common.convertStringToSQLDate(glOCPList.getDateFrom()),
	       Common.convertStringToSQLDate(glOCPList.getDateTo()),
	       Common.getGlPeriodStatusCode(((GlOpenClosePeriodsForm)form).getStatus()));

/*******************************************************
   Call GlOpenClosePeriodController EJB
   updateGlAcvStatus method
*******************************************************/
   
             try{
	        ejbOCP.updateGlAcvStatus(details, 
		   (new Integer(((GlOpenClosePeriodsForm) form).getYear())).intValue(), user.getCmpCode());
	     }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlOpenClosePeriodsAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                }
                return(mapping.findForward("cmnErrorPage"));
	     }

/*******************************************************
   -- Gl  OCP Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl  OCP Edit Action --
*******************************************************/

         }else if(request.getParameter("glOCPList[" + 
            ((GlOpenClosePeriodsForm)form).getRowSelected() + "].editButton") != null &&
            ((GlOpenClosePeriodsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
            	
            GlOpenClosePeriodsList glOCPList =
               ((GlOpenClosePeriodsForm)form).getGlOCPByIndex(((GlOpenClosePeriodsForm) form).getRowSelected());
            	
            	
            try {
            	
            	boolean needsConfirmation = ejbOCP.isGlAcvPriorPeriodStatusNeedConfirm(glOCPList.getPeriodCode(), new Integer(((GlOpenClosePeriodsForm)form).getYear()).intValue(), user.getCmpCode());
            	
            	if (needsConfirmation) {
            		
            		((GlOpenClosePeriodsForm)form).setNeedsConfirmation("true");            		
            		
            	} else {
            		
            		((GlOpenClosePeriodsForm)form).setNeedsConfirmation(null);
            		
            	}
            	
            	((GlOpenClosePeriodsForm)form).setCurrentStatus(glOCPList.getStatus());
            
	        
		    } catch(EJBException ex) {
		    	
                if(log.isInfoEnabled()) {
                	
                   log.info("EJBException caught in GlOpenClosePeriodsAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                                      
                }
                
                return(mapping.findForward("cmnErrorPage"));
	        }
            

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlOpenClosePeriodsForm properties
*******************************************************/                        
	       
            ((GlOpenClosePeriodsForm)form).clearStatusList();
	    switch(Common.getGlPeriodStatusCode(glOCPList.getStatus())){
	       case Constants.GL_PERIOD_STATUS_N:
	           ((GlOpenClosePeriodsForm)form).setStatusList(Constants.GL_PERIOD_STATUS_OPEN);
		   ((GlOpenClosePeriodsForm)form).setStatusList(Constants.GL_PERIOD_STATUS_FUTURE_ENTRY);
		   break;
	        case Constants.GL_PERIOD_STATUS_F:
		   ((GlOpenClosePeriodsForm)form).setStatusList(Constants.GL_PERIOD_STATUS_OPEN);
		   break;
	        case Constants.GL_PERIOD_STATUS_O:
		   ((GlOpenClosePeriodsForm)form).setStatusList(Constants.GL_PERIOD_STATUS_CLOSED);
		   ((GlOpenClosePeriodsForm)form).setStatusList(Constants.GL_PERIOD_STATUS_PERMANENTLY_CLOSED);
		   break;
		case Constants.GL_PERIOD_STATUS_C:
		   ((GlOpenClosePeriodsForm)form).setStatusList(Constants.GL_PERIOD_STATUS_OPEN);
		   ((GlOpenClosePeriodsForm)form).setStatusList(Constants.GL_PERIOD_STATUS_PERMANENTLY_CLOSED);
		   break;
		case Constants.GL_PERIOD_STATUS_P:
		   errors.add(ActionMessages.GLOBAL_MESSAGE, 
		      new ActionMessage("openClosePeriods.error.selectedPeriodPermanentlyClosed"));
		   saveErrors(request, new ActionMessages(errors));
		   ((GlOpenClosePeriodsForm)form).setPageState(Constants.PAGE_STATE_SAVE);
		   return(mapping.findForward("glOpenClosePeriods"));
		default:
		
	    }
            ((GlOpenClosePeriodsForm)form).showGlOCPRow(((GlOpenClosePeriodsForm) form).getRowSelected());
            return(mapping.findForward("glOpenClosePeriods"));

/*******************************************************
   -- Gl  OCP Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glOpenClosePeriods"));
            }
	    
	    ((GlOpenClosePeriodsForm)form).clearGlOCPList();
	    
	    if(request.getParameter("goButton") == null && request.getParameter("updateButton") == null &&
	       request.getParameter("cancelButton") == null){

	       ((GlOpenClosePeriodsForm)form).clearYearList();
	       try{
	          ArrayList ejbGlYRList = ejbOCP.getEditableYears(user.getCmpCode());
		  	  Iterator i = ejbGlYRList.iterator();
			  while(i.hasNext()){
			     ((GlOpenClosePeriodsForm)form).setYearList(String.valueOf(((Integer)i.next()).intValue()));
			  }
	       }catch(GlobalNoSetOfBookFoundException ex){
	          errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("openClosePeriods.error.noSetOfBookFound"));
              saveErrors(request, new ActionMessages(errors));
	       }catch(EJBException ex){
			  if(log.isInfoEnabled()){
			     log.info("EJBException caught in GlOpenClosePeriodsAction.execute(): " + ex.getMessage() +
			     " session: " + session.getId());
			  }
			  return(mapping.findForward("cmnErrorPage"));
		   }
		   
		   ((GlOpenClosePeriodsForm)form).setYear(null);
		   
	    }

/*******************************************************
  Call GlOpenClosePeriodController EJB
  getGlAcvByGlAcWithStatus() method
  Populate ArrayList property from
  method return
*******************************************************/
            if(((GlOpenClosePeriodsForm)form).getYear() != null){

	       try{
	          ArrayList ejbGlOCPList = ejbOCP.getGlAcvByYear(
		     (new Integer(((GlOpenClosePeriodsForm) form).getYear())).intValue(), user.getCmpCode());
	          Iterator i = ejbGlOCPList.iterator();
	          while(i.hasNext()){
	             GlAccountingCalendarValueDetails details = (GlAccountingCalendarValueDetails)i.next();
		     	 GlOpenClosePeriodsList glOCPList = new GlOpenClosePeriodsList(((GlOpenClosePeriodsForm)form),
			        details.getAcvCode(),
			        Common.getGlPeriodStatusDescription(details.getAcvStatus()),
			        details.getAcvPeriodPrefix(),
			        Common.convertShortToString(details.getAcvPeriodNumber()),
			        Common.convertShortToString(details.getAcvQuarter()),
			        Common.convertSQLDateToString(details.getAcvDateFrom()),
			        Common.convertSQLDateToString(details.getAcvDateTo()));

		    	 ((GlOpenClosePeriodsForm)form).saveGlOCPList(glOCPList);
	          }

	       }catch(EJBException ex){
                  if(log.isInfoEnabled()){
                     log.info("EJBException caught in GlOpenClosePeriodsAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  }
                  return(mapping.findForward("cmnErrorPage"));
               }

	    
               if(!errors.isEmpty()){
                  saveErrors(request, new ActionMessages(errors));
               }else{
                  if(request.getParameter("updateButton") != null && 
                      ((GlOpenClosePeriodsForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                      ((GlOpenClosePeriodsForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
                  }
               }
	    }

            ((GlOpenClosePeriodsForm)form).reset(mapping, request);
            
	        if (((GlOpenClosePeriodsForm)form).getTableType() == null) {
      		      	
	           ((GlOpenClosePeriodsForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            ((GlOpenClosePeriodsForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("glOpenClosePeriods"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()){
             log.info("Exception caught in GlOpenClosePeriodsAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
