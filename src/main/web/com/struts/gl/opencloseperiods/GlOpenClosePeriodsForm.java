package com.struts.gl.opencloseperiods;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlOpenClosePeriodsForm extends ActionForm implements Serializable {

   private String year = null;
   private ArrayList yearList = new ArrayList();
   private String status = null;
   private ArrayList statusList = new ArrayList();
   private String period = null;
   private String periodNumber = null;
   private String quarterNumber = null;
   private String dateFrom = null;
   private String dateTo = null;   
   private String needsConfirmation = null;
   private String currentStatus = null;
   
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String closeButton = null;
   private String goButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glOCPList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   public int getRowSelected(){
      return rowSelected;
   }

   public GlOpenClosePeriodsList getGlOCPByIndex(int index){
      return((GlOpenClosePeriodsList)glOCPList.get(index));
   }

   public Object[] getGlOCPList(){
      return(glOCPList.toArray());
   }

   public int getGlOCPListSize(){
      return(glOCPList.size());
   }

   public void saveGlOCPList(Object newGlOCPList){
      glOCPList.add(newGlOCPList);
   }

   public void clearGlOCPList(){
      glOCPList.clear();
   }

   public void setRowSelected(Object selectedGlOCPList, boolean isEdit){
      this.rowSelected = glOCPList.indexOf(selectedGlOCPList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showGlOCPRow(int rowSelected){
      this.status = ((GlOpenClosePeriodsList)glOCPList.get(rowSelected)).getStatus();
      this.period = ((GlOpenClosePeriodsList)glOCPList.get(rowSelected)).getPeriod();
      this.periodNumber = ((GlOpenClosePeriodsList)glOCPList.get(rowSelected)).getPeriodNumber();
      this.quarterNumber = ((GlOpenClosePeriodsList)glOCPList.get(rowSelected)).getQuarterNumber();
      this.dateFrom = ((GlOpenClosePeriodsList)glOCPList.get(rowSelected)).getDateFrom();
      this.dateTo = ((GlOpenClosePeriodsList)glOCPList.get(rowSelected)).getDateTo();
   }

   public void updateGlOCPRow(int rowSelected, Object newGlOCPList){
      glOCPList.set(rowSelected, newGlOCPList);
   }

   public void deleteGlOCPList(int rowSelected){
      glOCPList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setGoButton(String goButton){
      this.goButton = goButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getYear(){
      return(year);
   }

   public void setYear(String year){
      this.year = year;
   }

   public ArrayList getYearList(){
      return(yearList);
   }

   public void setYearList(String year){
      yearList.add(year);
   }

   public void clearYearList(){
      yearList.clear();
      yearList.add(Constants.GLOBAL_BLANK);
   }

   public String getStatus(){
      return(status);
   }

   public void setStatus(String status){
      this.status = status;
   }

   public ArrayList getStatusList(){
      return(statusList);
   }

   public void clearStatusList(){
      statusList.clear();
      statusList.add(Constants.GLOBAL_BLANK);
   }

   public void setStatusList(String status){
      statusList.add(status);
   }

   public String getPeriod(){
      return(period);
   }

   public void setPeriod(String period){
      this.period = period;
   }

   public String getPeriodNumber(){
      return(periodNumber);
   }

   public void setPeriodNumber(String periodNumber){
      this.periodNumber = periodNumber;
   }

   public String getQuarterNumber(){
      return(quarterNumber);
   }

   public void setQuarterNumber(String quarterNumber){
      this.quarterNumber = quarterNumber;
   }
   
   public String getDateFrom(){
      return(dateFrom);
   }

   public void setDateFrom(String dateFrom){
      this.dateFrom = dateFrom;
   }

   public String getDateTo(){
      return(dateTo);
   }

   public void setDateTo(String dateTo){
      this.dateTo = dateTo;
   }

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }
   
   public String getNeedsConfirmation() {
   	
   	  return needsConfirmation;
   	
   }
   
   public void setNeedsConfirmation(String needsConfirmation) {
   	
   	  this.needsConfirmation = needsConfirmation;
   	
   }
   
   public String getCurrentStatus() {
   	
   	  return currentStatus;
   	
   }
   
   public void setCurrentStatus(String currentStatus) {
   	
   	  this.currentStatus = currentStatus;
   	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
      status = null;
      period = null;
      periodNumber = null;
      quarterNumber = null;
      dateFrom = null;
      dateTo = null;
      needsConfirmation = null;
      currentStatus = null;
      goButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("goButton") != null){
         if(Common.validateRequired(year)){
            errors.add("year",
               new ActionMessage("openClosePeriods.error.yearRequired"));
         }
      }
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(year)){
	    errors.add("year",
               new ActionMessage("openClosePeriods.error.yearRequired"));	   
	 }
         if(Common.validateRequired(status)){
            errors.add("status",
               new ActionMessage("openClosePeriods.error.statusRequired"));
         }
         if(!Common.validateStringExists(statusList, status)){
            errors.add("status",
               new ActionMessage("openClosePeriods.error.statusInvalid"));
         }
      }
      return(errors);
   }
}
