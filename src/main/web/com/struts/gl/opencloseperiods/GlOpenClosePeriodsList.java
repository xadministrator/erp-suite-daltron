package com.struts.gl.opencloseperiods;

import java.io.Serializable;

public class GlOpenClosePeriodsList implements Serializable {

   private Integer periodCode = null;
   private String status = null;
   private String period = null;
   private String quarterNumber = null;
   private String periodNumber = null;
   private String dateFrom = null;
   private String dateTo = null;

   private String deleteButton = null;
   private String editButton = null;
    
   private GlOpenClosePeriodsForm parentBean;
    
   public GlOpenClosePeriodsList(GlOpenClosePeriodsForm parentBean,
      Integer periodCode,
      String status,
      String period,
      String periodNumber,
      String quarterNumber,
      String dateFrom,
      String dateTo){

      this.parentBean = parentBean;
      this.periodCode = periodCode;
      this.status = status;
      this.period = period;
      this.periodNumber = periodNumber;
      this.quarterNumber = quarterNumber;
      this.dateFrom = dateFrom;
      this.dateTo = dateTo;
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public Integer getPeriodCode(){
      return(periodCode);
   }

   public String getStatus(){
      return(status);
   }

   public String getPeriod(){
      return(period);
   }

   public String getPeriodNumber(){
      return(periodNumber);
   }

   public String getQuarterNumber(){
      return(quarterNumber);
   }

   public String getDateFrom(){
      return(dateFrom);
   }

   public String getDateTo(){
      return(dateTo);
   }


}
