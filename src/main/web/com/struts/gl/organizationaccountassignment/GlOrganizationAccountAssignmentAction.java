package com.struts.gl.organizationaccountassignment;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlARAccountNumberOfSegmentInvalidException;
import com.ejb.exception.GlARAccountRangeAlreadyDeletedException;
import com.ejb.exception.GlARAccountRangeInvalidException;
import com.ejb.exception.GlARAccountRangeNoAccountFoundException;
import com.ejb.exception.GlARAccountRangeOverlappedException;
import com.ejb.exception.GlARNoAccountRangeFoundException;
import com.ejb.exception.GlARResponsibilityNotAllowedException;
import com.ejb.exception.GlORGNoOrganizationFoundException;
import com.ejb.txn.GlOrganizationAccountAssignmentController;
import com.ejb.txn.GlOrganizationAccountAssignmentControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlAccountRangeDetails;
import com.util.GlOrganizationDetails;

public final class GlOrganizationAccountAssignmentAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlOrganizationAccountAssignmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         String frParam = Common.getUserPermission(user, Constants.GL_ORGANIZATION_ACCOUNT_ASSIGNMENT_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((GlOrganizationAccountAssignmentForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glOrganizationAccountAssignment"));
               }
            }
            ((GlOrganizationAccountAssignmentForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlOrganizationAccountAssignmentForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlOrganizationAccountAssignmentController EJB
*******************************************************/

         GlOrganizationAccountAssignmentControllerHome homeAR = null;
         GlOrganizationAccountAssignmentController ejbAR = null;

         try{
            
            homeAR = (GlOrganizationAccountAssignmentControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlOrganizationAccountAssignmentControllerEJB", GlOrganizationAccountAssignmentControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlOrganizationAccountAssignmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbAR = homeAR.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlOrganizationAccountAssignmentAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();


/*******************************************************
   -- Gl DS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlOrganizationAccountAssignmentForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glOrganizationAccountAssignment"));
	     
/*******************************************************
   -- Gl DS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlOrganizationAccountAssignmentForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glOrganizationAccountAssignment"));         

/*******************************************************
   -- Gl  AR Save Action --
*******************************************************/

         } else if(request.getParameter("saveButton") != null &&
            ((GlOrganizationAccountAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            /*** Generate Line Number ***/
	    int listSize = ((GlOrganizationAccountAssignmentForm)form).getGlARListSize();
	    short lineNumber = 0;

	    if(listSize != 0){
	       for(int i=0; i<listSize; i++){
	         GlOrganizationAccountAssignmentList glARList = ((GlOrganizationAccountAssignmentForm)form).getGlARByIndex(i);
	         lineNumber = (short)Math.max(lineNumber, (new Integer(glARList.getLineNumber())).intValue());
	       }
	       lineNumber = (short)(lineNumber + 1);
	    }else{
	       lineNumber = 1;
	    }


	    GlAccountRangeDetails details = new GlAccountRangeDetails(lineNumber, 
	       ((GlOrganizationAccountAssignmentForm)form).getAccountLow().trim(),
	       ((GlOrganizationAccountAssignmentForm)form).getAccountHigh().trim());

	    try{
	       ejbAR.addGlArEntry(details, ((GlOrganizationAccountAssignmentForm)form).getOrganization(), 
	          new Integer(user.getCurrentResCode()), user.getCmpCode());
	    }catch(GlORGNoOrganizationFoundException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE, 
	          new ActionMessage("organizationAccountAssignment.error.organizationNotFound"));
	    }catch(GlARAccountRangeOverlappedException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationAccountAssignment.error.accountRangeOverlapped"));			
	    }catch(GlARAccountNumberOfSegmentInvalidException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationAccountAssignment.error.numberOfSegmentInvalid"));
	    }catch(GlARAccountRangeNoAccountFoundException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationAccountAssignment.error.noAccountFound"));
            }catch(GlARResponsibilityNotAllowedException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationAccountAssignment.error.responsibilityNotAllowed"));
	    }catch(GlARAccountRangeInvalidException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationAccountAssignment.error.accountRangeInvalid"));
	    }catch(EJBException ex){
	       if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlOrganizationAccountAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

/*******************************************************
   -- Gl  AR Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));
	    
/*******************************************************
   -- Gl  AR Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
            ((GlOrganizationAccountAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	     GlOrganizationAccountAssignmentList glARList =
                ((GlOrganizationAccountAssignmentForm)form).getGlARByIndex(
		   ((GlOrganizationAccountAssignmentForm) form).getRowSelected());
			     
             GlAccountRangeDetails details = new GlAccountRangeDetails(glARList.getAccountRangeCode(),
	        Common.convertStringToShort(glARList.getLineNumber()),
                ((GlOrganizationAccountAssignmentForm)form).getAccountLow().trim(),
                ((GlOrganizationAccountAssignmentForm)form).getAccountHigh().trim());
             
	      try{
                 ejbAR.updateGlArEntry(details, ((GlOrganizationAccountAssignmentForm)form).getOrganization(), 
		    new Integer(user.getCurrentResCode()), user.getCmpCode());
              }catch(GlORGNoOrganizationFoundException ex){
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("organizationAccountAssignment.error.organizationNotFound"));
              }catch(GlARAccountRangeOverlappedException ex){
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("organizationAccountAssignment.error.accountRangeOverlapped"));
              }catch(GlARAccountNumberOfSegmentInvalidException ex){
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("organizationAccountAssignment.error.numberOfSegmentInvalid"));
              }catch(GlARAccountRangeNoAccountFoundException ex){
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("organizationAccountAssignment.error.noAccountFound"));
              }catch(GlARResponsibilityNotAllowedException ex){
                 errors.add(ActionMessages.GLOBAL_MESSAGE,
	            new ActionMessage("organizationAccountAssignment.error.responsibilityNotAllowed"));
	      }catch(GlARAccountRangeInvalidException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
	            new ActionMessage("organizationAccountAssignment.error.accountRangeInvalid"));
              }catch(GlARAccountRangeAlreadyDeletedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("organizationAccountAssignment.error.organizationAccountAssignmentAlreadyDeleted"));
              }catch(EJBException ex){
                 if(log.isInfoEnabled()){
                    log.info("EJBException caught in GlOrganizationAccountAssignmentAction.execute(): " + ex.getMessage() +
                    " session: " + session.getId());
                 }
                 return(mapping.findForward("cmnErrorPage"));
              }
				 
/*******************************************************
   -- Gl  AR Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl  AR Edit Action --
*******************************************************/

         }else if(request.getParameter("glARList[" + 
            ((GlOrganizationAccountAssignmentForm)form).getRowSelected() + "].editButton") != null &&
            ((GlOrganizationAccountAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlOrganizationAccountAssignmentForm properties
*******************************************************/

            ((GlOrganizationAccountAssignmentForm)form).showGlARRow(
	       ((GlOrganizationAccountAssignmentForm) form).getRowSelected());
            return(mapping.findForward("glOrganizationAccountAssignment"));
	    
/*******************************************************
   -- Gl  AR Delete Action --
*******************************************************/

         }else if(request.getParameter("glARList[" +
            ((GlOrganizationAccountAssignmentForm)form).getRowSelected() + "].deleteButton") != null &&
            ((GlOrganizationAccountAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){


            GlOrganizationAccountAssignmentList glARList =
               ((GlOrganizationAccountAssignmentForm)form).getGlARByIndex(
               ((GlOrganizationAccountAssignmentForm) form).getRowSelected());
					       
            try{
	       ejbAR.deleteGlArEntry(glARList.getAccountRangeCode(), new Integer(user.getCurrentResCode()), user.getCmpCode());
	    }catch(GlARAccountRangeAlreadyDeletedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationAccountAssignment.error.organizationAccountAssignmentAlreadyDeleted"));
            }catch(GlARResponsibilityNotAllowedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationAccountAssignment.error.responsibilityNotAllowed"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlOrganizationAccountAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }
	    
/*******************************************************
   -- Gl  AR Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glOrganizationAccountAssignment"));
            }

	     ((GlOrganizationAccountAssignmentForm)form).clearGlARList();

            if(request.getParameter("saveButton") == null && request.getParameter("updateButton") == null &&
	       request.getParameter("goButton") == null && request.getParameter("cancelButton") == null &&
               request.getParameter("glARList[" +
	       ((GlOrganizationAccountAssignmentForm) form).getRowSelected() + "].deleteButton") == null){
	       
               ((GlOrganizationAccountAssignmentForm)form).clearOrganizationList();
	       try{
	          ArrayList ejbGlORGList = ejbAR.getGlOrgAll(user.getCmpCode());
	          Iterator i = ejbGlORGList.iterator();
	          while(i.hasNext()){
	             ((GlOrganizationAccountAssignmentForm)form).setOrganizationList(
	             ((GlOrganizationDetails)i.next()).getOrgName());
	          }
	       }catch(GlORGNoOrganizationFoundException ex){
	          ((GlOrganizationAccountAssignmentForm)form).setOrganizationList(Constants.GLOBAL_NO_RECORD_FOUND);
	       }catch(EJBException ex){
	          if(log.isInfoEnabled()){
	             log.info("EJBException caught in GlOrganizationAccountAssignmentAction.execute(): " +
	             ex.getMessage() + " session: " + session.getId());
	          }
	          return(mapping.findForward("cmnErrorPage"));
	       }
	       ((GlOrganizationAccountAssignmentForm)form).setOrganization(null);
	       ((GlOrganizationAccountAssignmentForm)form).setDescription(null);
	    }
	    
	    if(((GlOrganizationAccountAssignmentForm)form).getOrganization() != null){
	       try{
	          GlOrganizationDetails details = ejbAR.getGlOrgDescriptionByGlOrgName(
	          ((GlOrganizationAccountAssignmentForm)form).getOrganization(), user.getCmpCode());
	          ((GlOrganizationAccountAssignmentForm)form).setDescription(details.getOrgDescription());
	       }catch(GlORGNoOrganizationFoundException ex){
	          errors.add(ActionMessages.GLOBAL_MESSAGE,
	             new ActionMessage("organizationAccountAssignment.error.organizationtNotFound"));
	       }catch(EJBException ex){
	          if(log.isInfoEnabled()){
	             log.info("EJBException caught in GlOrganizationAccountAssignmentAction.execute(): " +
	             ex.getMessage() + " session: " + session.getId());
	          }
	          return(mapping.findForward("cmnErrorPage"));
	       }
	       if(!errors.isEmpty()){
	          saveErrors(request, new ActionMessages(errors));
	            return(mapping.findForward("glOrganizationAccountAssignment"));
	       }


               try{
	          ArrayList ejbGlARList = ejbAR.getGlArByGlOrgName(
		     ((GlOrganizationAccountAssignmentForm)form).getOrganization(), user.getCmpCode());
		  Iterator i = ejbGlARList.iterator();
		  GlOrganizationAccountAssignmentList glARList = null;
		  while(i.hasNext()){
		     GlAccountRangeDetails details = (GlAccountRangeDetails)i.next();
		     glARList = new GlOrganizationAccountAssignmentList(((GlOrganizationAccountAssignmentForm)form),
		        details.getArCode(),
			Common.convertShortToString(details.getArLine()),
			details.getArAccountLow(),
			details.getArAccountHigh());
	             ((GlOrganizationAccountAssignmentForm)form).saveGlARList(glARList);
		  }
	       }catch(GlORGNoOrganizationFoundException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("organizationAccountAssignment.error.organizationtNotFound"));			
	       }catch(GlARNoAccountRangeFoundException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("organizationAccountAssignment.error.noAccountRangeFound"));		
	       }catch(EJBException ex){
	          if(log.isInfoEnabled()){
                     log.info("EJBException caught in GlOrganizationAccountAssignmentAction.execute(): " +
                     ex.getMessage() + " session: " + session.getId());
                  }
                  return(mapping.findForward("cmnErrorPage"));								
	       }
 
               if(!errors.isEmpty()){
                  saveErrors(request, new ActionMessages(errors));
               }else{
                  if((request.getParameter("saveButton") != null || 
                      request.getParameter("updateButton") != null || 
                      request.getParameter("glARList[" + 
                      ((GlOrganizationAccountAssignmentForm) form).getRowSelected() + "].deleteButton") != null) && 
                      ((GlOrganizationAccountAssignmentForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                      ((GlOrganizationAccountAssignmentForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
                  }
               }
	    }

            ((GlOrganizationAccountAssignmentForm)form).reset(mapping, request);
            
	        if (((GlOrganizationAccountAssignmentForm)form).getTableType() == null) {
      		      	
	           ((GlOrganizationAccountAssignmentForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            ((GlOrganizationAccountAssignmentForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("glOrganizationAccountAssignment"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()){
             log.info("Exception caught in GlOrganizationAccountAssignmentAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
