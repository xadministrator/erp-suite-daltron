package com.struts.gl.organizationaccountassignment;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlOrganizationAccountAssignmentForm extends ActionForm implements Serializable {

   private String organization = null;
   private ArrayList organizationList = new ArrayList();
   private String description = null;
   private String accountLow = null;
   private String accountHigh = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glARList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   public int getRowSelected(){
      return rowSelected;
   }

   public GlOrganizationAccountAssignmentList getGlARByIndex(int index){
      return((GlOrganizationAccountAssignmentList)glARList.get(index));
   }

   public Object[] getGlARList(){
      return(glARList.toArray());
   }

   public int getGlARListSize(){
      return(glARList.size());
   }

   public void saveGlARList(Object newGlARList){
      glARList.add(newGlARList);
   }

   public void clearGlARList(){
      glARList.clear();
   }

   public void setRowSelected(Object selectedGlARList, boolean isEdit){
      this.rowSelected = glARList.indexOf(selectedGlARList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showGlARRow(int rowSelected){
      this.accountLow = ((GlOrganizationAccountAssignmentList)glARList.get(rowSelected)).getAccountLow();
      this.accountHigh = ((GlOrganizationAccountAssignmentList)glARList.get(rowSelected)).getAccountHigh();
   }

   public void updateGlARRow(int rowSelected, Object newGlARList){
      glARList.set(rowSelected, newGlARList);
   }

   public void deleteGlARList(int rowSelected){
      glARList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getOrganization(){
      return(organization);
   }

   public void setOrganization(String organization){
      this.organization = organization;
   }

   public ArrayList getOrganizationList(){
      return(organizationList);
   }

   public void setOrganizationList(String organization){
      organizationList.add(organization);
   }

   public void clearOrganizationList(){
      organizationList.clear();
      organizationList.add(Constants.GLOBAL_BLANK);
   }

   public String getDescription(){
      return(description);
   }

   public void setDescription(String description){
      this.description = description;
   }

   public String getAccountLow(){
      return(accountLow);
   }

   public void setAccountLow(String accountLow){
      this.accountLow = accountLow;
   }

   public String getAccountHigh(){
      return(accountHigh);
   }

   public void setAccountHigh(String accountHigh){
      this.accountHigh = accountHigh;
   }
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request){
      accountLow = null;
      accountHigh = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("goButton") != null){
         if(Common.validateRequired(organization) || organization.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
	    errors.add("organization",
               new ActionMessage("organizationAccountAssignment.error.organizationRequired"));
	 }
      }
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(organization) || organization.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("organization",
               new ActionMessage("organizationAccountAssignment.error.organizationRequired"));
         }
         if(!Common.validateStringExists(organizationList, organization)){
            errors.add("organization",
               new ActionMessage("organizationAccountAssignment.error.organizationInvalid"));
         }
         if(Common.validateRequired(accountLow)){
            errors.add("accountLow",
               new ActionMessage("organizationAccountAssignment.error.accountLowRequired"));
         }
         if(Common.validateRequired(accountHigh)){
            errors.add("accountHigh",
               new ActionMessage("organizationAccountAssignment.error.accountHighRequired"));
         }
      }
      return(errors);
   }
}
