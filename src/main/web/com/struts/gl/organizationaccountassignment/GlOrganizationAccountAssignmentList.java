package com.struts.gl.organizationaccountassignment;

import java.io.Serializable;

public class GlOrganizationAccountAssignmentList implements Serializable {

   private Integer accountRangeCode = null;
   private String lineNumber = null;
   private String accountLow = null;
   private String accountHigh = null;

   private String deleteButton = null;
   private String editButton = null;
    
   private GlOrganizationAccountAssignmentForm parentBean;
    
   public GlOrganizationAccountAssignmentList(GlOrganizationAccountAssignmentForm parentBean,
      Integer accountRangeCode,
      String lineNumber,
      String accountLow,
      String accountHigh){

      this.parentBean = parentBean;
      this.accountRangeCode = accountRangeCode;
      this.lineNumber = lineNumber;
      this.accountLow = accountLow;
      this.accountHigh = accountHigh;
   }

   public void setDeleteButton(String deleteButton){
      parentBean.setRowSelected(this, false);
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public Integer getAccountRangeCode(){
      return(accountRangeCode);
   }

   public String getLineNumber(){
      return(lineNumber);
   }

   public String getAccountLow(){
      return(accountLow);
   }

   public String getAccountHigh(){
      return(accountHigh);
   }

}