package com.struts.gl.organizationentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlORGNoOrganizationCodeFoundException;
import com.ejb.exception.GlORGNoOrganizationFoundException;
import com.ejb.exception.GlORGOrganizationAlreadyAssignedException;
import com.ejb.exception.GlORGOrganizationAlreadyDeletedException;
import com.ejb.exception.GlORGOrganizationAlreadyExistException;
import com.ejb.txn.GlOrganizationController;
import com.ejb.txn.GlOrganizationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlOrganizationDetails;

public final class GlOrganizationEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	 

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlOrganizationEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         String frParam = Common.getUserPermission(user, Constants.GL_ORGANIZATION_ENTRY_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((GlOrganizationEntryForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glOrganizationEntry"));
               }
            }
            ((GlOrganizationEntryForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlOrganizationEntryForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlOrganizationController EJB
*******************************************************/

         GlOrganizationControllerHome homeORG = null;
         GlOrganizationController ejbORG = null;

         try{
            
            homeORG = (GlOrganizationControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlOrganizationControllerEJB", GlOrganizationControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlOrganizationEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbORG = homeORG.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlOrganizationEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Gl DS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlOrganizationEntryForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glOrganizationEntry"));
	     
/*******************************************************
   -- Gl DS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlOrganizationEntryForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glOrganizationEntry"));         

/*******************************************************
   -- Gl  ORG Save Action --
*******************************************************/

         } else if(request.getParameter("saveButton") != null &&
            ((GlOrganizationEntryForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	    GlOrganizationDetails details = new GlOrganizationDetails(
	       ((GlOrganizationEntryForm)form).getOrganizationName().trim().toUpperCase(),
	       ((GlOrganizationEntryForm)form).getDescription().trim().toUpperCase(),
	       Common.convertStringToInteger(((GlOrganizationEntryForm)form).getMasterCode()));

/*******************************************************
   Call GlOrganizationController EJB
   addGlOrgEntry method
*******************************************************/

             try{
	        ejbORG.addGlOrgEntry(details, user.getCmpCode());
	     }catch(GlORGNoOrganizationCodeFoundException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
	           new ActionMessage("organizationEntry.error.organizationCodeNotFound"));
	     }catch(GlORGOrganizationAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("organizationEntry.error.organizationNameAlreadyExist"));
	     }catch(EJBException ex){
	        if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlOrganizationEntryAction.execute(): " + ex.getMessage() +
                   " session: " + session.getId());
                }
                return(mapping.findForward("cmnErrorPage"));
	     }  

/*******************************************************
   -- Gl  ORG Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));
/*******************************************************
   -- Gl  ORG Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
            ((GlOrganizationEntryForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	    GlOrganizationEntryList glORGList =
               ((GlOrganizationEntryForm)form).getGlORGByIndex(((GlOrganizationEntryForm) form).getRowSelected());

           GlOrganizationDetails details = new GlOrganizationDetails(
	       glORGList.getOrganizationCode(),
               ((GlOrganizationEntryForm)form).getOrganizationName().trim().toUpperCase(),
               ((GlOrganizationEntryForm)form).getDescription().trim().toUpperCase(),
               Common.convertStringToInteger(((GlOrganizationEntryForm)form).getMasterCode()));

/*******************************************************
   Call GlOrganizationController EJB
   updateGlOrgEntry method  
*******************************************************/

           try{
              ejbORG.updateGlOrgEntry(details, user.getCmpCode());
           }catch(GlORGNoOrganizationCodeFoundException ex){
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("organizationEntry.error.organizationCodeNotFound"));
           }catch(GlORGOrganizationAlreadyExistException ex){
              errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("organizationEntry.error.organizationNameAlreadyExist"));
           }catch(GlORGOrganizationAlreadyAssignedException ex){
	      errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("organizationEntry.error.updateOrganizationAlreadyAssigned"));
	   }catch(GlORGOrganizationAlreadyDeletedException ex){
	      errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("organizationEntry.error.organizationAlreadyDeleted"));
           }catch(EJBException ex){
              if(log.isInfoEnabled()){
                 log.info("EJBException caught in GlOrganizationEntryAction.execute(): " + ex.getMessage() +
                 " session: " + session.getId());
              }
              return(mapping.findForward("cmnErrorPage"));
           }
            
/*******************************************************
   -- Gl  ORG Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl  ORG Edit Action --
*******************************************************/

         }else if(request.getParameter("glORGList[" + 
            ((GlOrganizationEntryForm)form).getRowSelected() + "].editButton") != null &&
            ((GlOrganizationEntryForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlOrganizationEntryForm properties
*******************************************************/

            ((GlOrganizationEntryForm)form).showGlORGRow(((GlOrganizationEntryForm) form).getRowSelected());
            return(mapping.findForward("glOrganizationEntry"));
/*******************************************************
   -- Gl  ORG Delete Action --
*******************************************************/

         }else if(request.getParameter("glORGList[" +
            ((GlOrganizationEntryForm)form).getRowSelected() + "].deleteButton") != null &&
            ((GlOrganizationEntryForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            GlOrganizationEntryList glORGList =
               ((GlOrganizationEntryForm)form).getGlORGByIndex(((GlOrganizationEntryForm) form).getRowSelected());

/*******************************************************
   Call GlOrganizationController EJB
   deleteGlOrgEntry method
*******************************************************/

            try{
	       ejbORG.deleteGlOrgEntry(glORGList.getOrganizationCode(), user.getCmpCode());
	    }catch(GlORGOrganizationAlreadyAssignedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationEntry.error.deleteOrganizationAlreadyAssigned"));
            }catch(GlORGOrganizationAlreadyDeletedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationEntry.error.organizationAlreadyDeleted"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlOrganizationEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }
	    
/*******************************************************
   -- Gl  ORG Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glOrganizationEntry"));
            }

/*******************************************************
   Call GlOrganizationController EJB 
   getGlOrgAll() method
   Populate currencyList and ArrayList property
   from method ArrayList return
*******************************************************/

            ((GlOrganizationEntryForm)form).clearMasterCodeList();
	    ((GlOrganizationEntryForm)form).clearGlORGList();

	    try{
	       ArrayList ejbGlORGList = ejbORG.getGlOrgAll(user.getCmpCode());
	       Iterator i = ejbGlORGList.iterator();
	       while(i.hasNext()){
	          GlOrganizationDetails details = (GlOrganizationDetails)i.next();
	          ((GlOrganizationEntryForm)form).setMasterCodeList(Common.convertIntegerToString(details.getOrgCode()));
		  GlOrganizationEntryList glORGList = new GlOrganizationEntryList(
		     ((GlOrganizationEntryForm)form),
		     details.getOrgCode(),
		     Common.convertIntegerToString(details.getOrgCode()),
		     details.getOrgName(),
		     details.getOrgDescription(),
		     Common.convertIntegerToString(details.getOrgMasterCode()));
		     
		  ((GlOrganizationEntryForm)form).saveGlORGList(glORGList);
	       }
	    }catch(GlORGNoOrganizationFoundException ex){
	        ((GlOrganizationEntryForm)form).setMasterCodeList(Constants.GLOBAL_NO_RECORD_FOUND);
	        
	    }catch(EJBException ex){
	       if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlOrganizationEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
	    }


            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
            }else{
               if((request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null || 
                   request.getParameter("glORGList[" + 
                   ((GlOrganizationEntryForm) form).getRowSelected() + "].deleteButton") != null) && 
                   ((GlOrganizationEntryForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                   ((GlOrganizationEntryForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
               }
            }

            ((GlOrganizationEntryForm)form).reset(mapping, request);
            
	        if (((GlOrganizationEntryForm)form).getTableType() == null) {
      		      	
	           ((GlOrganizationEntryForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            ((GlOrganizationEntryForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("glOrganizationEntry"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
          if(log.isInfoEnabled()){
             log.info("Exception caught in GlOrganizationEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
