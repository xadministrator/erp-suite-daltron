package com.struts.gl.organizationentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlOrganizationEntryForm extends ActionForm implements Serializable {

   private String organizationName = null;
   private String description = null;
   private String masterCode = null;
   private ArrayList masterCodeList = new ArrayList();
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glORGList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   public int getRowSelected(){
      return rowSelected;
   }

   public GlOrganizationEntryList getGlORGByIndex(int index){
      return((GlOrganizationEntryList)glORGList.get(index));
   }

   public Object[] getGlORGList(){
      return(glORGList.toArray());
   }

   public int getGlORGListSize(){
      return(glORGList.size());
   }

   public void saveGlORGList(Object newGlORGList){
      glORGList.add(newGlORGList);
   }

   public void clearGlORGList(){
      glORGList.clear();
   }

   public void setRowSelected(Object selectedGlORGList, boolean isEdit){
      this.rowSelected = glORGList.indexOf(selectedGlORGList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showGlORGRow(int rowSelected){
      this.organizationName = ((GlOrganizationEntryList)glORGList.get(rowSelected)).getOrganizationName();
      this.description = ((GlOrganizationEntryList)glORGList.get(rowSelected)).getDescription();
      this.masterCode = ((GlOrganizationEntryList)glORGList.get(rowSelected)).getMasterCode();
   }

   public void updateGlORGRow(int rowSelected, Object newGlORGList){
      glORGList.set(rowSelected, newGlORGList);
   }

   public void deleteGlORGList(int rowSelected){
      glORGList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getOrganizationName(){
      return(organizationName);
   }

   public void setOrganizationName(String organizationName){
      this.organizationName = organizationName;
   }

   public String getDescription(){
      return(description);
   }

   public void setDescription(String description){
      this.description = description;
   }

   public String getMasterCode(){
      return(masterCode);
   }

   public void setMasterCode(String masterCode){
      this.masterCode = masterCode;
   }

   public ArrayList getMasterCodeList(){
      return(masterCodeList);
   }

   public void setMasterCodeList(String masterCode){
      masterCodeList.add(masterCode);
   }

   public void clearMasterCodeList(){
      masterCodeList.clear();
      masterCodeList.add(Constants.GLOBAL_BLANK);
   }

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
      organizationName = null;
      description = null;
      masterCode = Constants.GLOBAL_BLANK;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(organizationName)){
            errors.add("organizationName",
               new ActionMessage("organizationEntry.error.organizationNameRequired"));
         }
         if(masterCode.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("masterCode",
               new ActionMessage("organizationEntry.error.masterCodeRequired"));
         }else{
            if(request.getParameter("updateButton") != null && 
	       (getGlORGByIndex(getRowSelected())).getOrganizationCode().equals(
	       new Integer(Common.convertStringToInt(masterCode)))){
	       errors.add("masterCode",
	          new ActionMessage("organizationEntry.error.masterCodeMustNotEqualToOrganizationCode"));
	    }
	 }
         if(!Common.validateStringExists(masterCodeList, masterCode)){
            errors.add("masterCode",
               new ActionMessage("organizationEntry.error.masterCodeInvalid"));
         }
      }
      return(errors);
   }
}
