package com.struts.gl.organizationentry;

import java.io.Serializable;

public class GlOrganizationEntryList implements Serializable {

   private Integer organizationCode = null;
   private String organizationCodeDisplay = null;
   private String organizationName = null;
   private String description = null;
   private String masterCode = null;

   private String deleteButton = null;
   private String editButton = null;
    
   private GlOrganizationEntryForm parentBean;
    
   public GlOrganizationEntryList(GlOrganizationEntryForm parentBean,
      Integer organizationCode,
      String organizationCodeDisplay,
      String organizationName,
      String description,
      String masterCode){

      this.parentBean = parentBean;
      this.organizationCode = organizationCode;
      this.organizationCodeDisplay = organizationCodeDisplay;
      this.organizationName = organizationName;
      this.description = description;
      this.masterCode = masterCode;
   }

   public void setDeleteButton(String deleteButton){
      parentBean.setRowSelected(this, false);
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public Integer getOrganizationCode(){
      return(organizationCode);
   }

   public String getOrganizationCodeDisplay(){
      return(organizationCodeDisplay);
   }

   public String getOrganizationName(){
      return(organizationName);
   }

   public String getDescription(){
      return(description);
   }

   public String getMasterCode(){
      return(masterCode);
   }

}