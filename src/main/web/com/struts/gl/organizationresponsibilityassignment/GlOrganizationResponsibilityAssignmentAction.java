package com.struts.gl.organizationresponsibilityassignment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlORGNoOrganizationFoundException;
import com.ejb.exception.GlRESNoResponsibilityFoundException;
import com.ejb.exception.GlRESResponsibilityAlreadyDeletedException;
import com.ejb.exception.GlRESResponsibilityAlreadyExistException;
import com.ejb.txn.GlResponsibilityController;
import com.ejb.txn.GlResponsibilityControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlOrganizationDetails;
import com.util.GlResponsibilityDetails;

public final class GlOrganizationResponsibilityAssignmentAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlOrganizationResponsibilityAssignmentAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         String frParam = Common.getUserPermission(user, Constants.GL_ORGANIZATION_RESPONSIBILITY_ASSIGNMENT_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = 
		    ((GlOrganizationResponsibilityAssignmentForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glOrganizationResponsibilityAssignment"));
               }
            }
            ((GlOrganizationResponsibilityAssignmentForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlOrganizationResponsibilityAssignmentForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlResponsibilityController EJB
*******************************************************/

         GlResponsibilityControllerHome homeRES = null;
         GlResponsibilityController ejbRES = null;

         try{
            
            homeRES = (GlResponsibilityControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlResponsibilityControllerEJB", GlResponsibilityControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " + 
		e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbRES = homeRES.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " + 
		e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl RES Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlOrganizationResponsibilityAssignmentForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glOrganizationResponsibilityAssignment"));
	     
/*******************************************************
   -- Gl RES Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlOrganizationResponsibilityAssignmentForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glOrganizationResponsibilityAssignment"));                  

/*******************************************************
   -- Gl  RES Save Action --
*******************************************************/

         } else if(request.getParameter("saveButton") != null &&
            ((GlOrganizationResponsibilityAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            GlResponsibilityDetails details = null; 
            try{
               details = new GlResponsibilityDetails(
	          Common.getAdResCodeByResName((DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY), 
		     ((GlOrganizationResponsibilityAssignmentForm)form).getResponsibility(), user.getCmpCode()),
	          Common.convertBooleanToByte(((GlOrganizationResponsibilityAssignmentForm)form).getEnabled()));
	    }catch(SQLException sqle){
	       if(sqle.getMessage().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("organizationResponsibiltyAssignment.error.resNameAlreadyDeleted"));
                  saveErrors(request, new ActionMessages(errors));
                  return(mapping.findForward("glOrganizationResponsibilityAssignment"));
               }
               if(log.isInfoEnabled()){
                  log.info("SQLException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " + 
		  sqle.getMessage() + " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
	    }

	    try{
	       ejbRES.addGlResEntry(details, ((GlOrganizationResponsibilityAssignmentForm)form).getOrganization(), user.getCmpCode());
	    }catch(GlRESResponsibilityAlreadyExistException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationResponsibilityAssignment.error.responsibilityAlreadyExists"));
	    }catch(GlORGNoOrganizationFoundException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationResponsibilityAssignment.error.organizationNotFound"));
	    }catch(EJBException ex){
	       if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
	    }

	    

	    
/*******************************************************
   -- Gl  RES Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));
/*******************************************************
   -- Gl  RES Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
            ((GlOrganizationResponsibilityAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            GlResponsibilityDetails details = null;
            try{
               details = new GlResponsibilityDetails(
                  Common.getAdResCodeByResName((DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY),
                  ((GlOrganizationResponsibilityAssignmentForm)form).getResponsibility(), user.getCmpCode()),
                  Common.convertBooleanToByte(((GlOrganizationResponsibilityAssignmentForm)form).getEnabled()));
            }catch(SQLException sqle){
               if(sqle.getMessage().equals(Constants.GLOBAL_NO_RECORD_FOUND)){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("organizationResponsibiltyAssignment.error.resNameAlreadyDeleted"));
                  saveErrors(request, new ActionMessages(errors));
                  return(mapping.findForward("glOrganizationResponsibilityAssignment"));
               }
               if(log.isInfoEnabled()){
                  log.info("SQLException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " +
                  sqle.getMessage() + " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

            try{
	       ejbRES.updateGlResEntry(details, ((GlOrganizationResponsibilityAssignmentForm)form).getOrganization(), user.getCmpCode());
	    }catch(GlRESResponsibilityAlreadyDeletedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationResponsibilityAssignment.error.responsibilityAlreadyDeleted"));
            }catch(GlORGNoOrganizationFoundException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationResponsibilityAssignment.error.organizationNotFound"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }
	    
/*******************************************************
   -- Gl  RES Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl  RES Edit Action --
*******************************************************/

         }else if(request.getParameter("glRESList[" + 
            ((GlOrganizationResponsibilityAssignmentForm)form).getRowSelected() + "].editButton") != null &&
            ((GlOrganizationResponsibilityAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlOrganizationResponsibilityAssignmentForm properties
*******************************************************/

            ((GlOrganizationResponsibilityAssignmentForm)form).showGlRESRow(
	       ((GlOrganizationResponsibilityAssignmentForm) form).getRowSelected());
            return(mapping.findForward("glOrganizationResponsibilityAssignment"));
	    
/*******************************************************
   -- Gl  RES Delete Action --
*******************************************************/

         }else if(request.getParameter("glRESList[" +
            ((GlOrganizationResponsibilityAssignmentForm)form).getRowSelected() + "].deleteButton") != null &&
            ((GlOrganizationResponsibilityAssignmentForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){


            GlOrganizationResponsibilityAssignmentList glRESList =
               ((GlOrganizationResponsibilityAssignmentForm)form).getGlRESByIndex(
	       ((GlOrganizationResponsibilityAssignmentForm) form).getRowSelected());

	    try{
	       ejbRES.deleteGlResEntry(Common.getAdResCodeByResName(
	          (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY),
		  glRESList.getResponsibility().trim(), user.getCmpCode()), user.getCmpCode());
	    }catch(GlRESResponsibilityAlreadyDeletedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("organizationResponsibilityAssignment.error.responsibilityAlreadyDeleted"));
	    }catch(SQLException ex){
	       if(log.isInfoEnabled()){
	          log.info("SQLException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " 
		  + ex.getMessage() + " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlOrganizationResponsibilityAssignmentAction.execute(): "
		  + ex.getMessage() + " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }


/*******************************************************
   -- Gl  RES Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glOrganizationResponsibilityAssignment"));
            }

	    ((GlOrganizationResponsibilityAssignmentForm)form).clearGlRESList();
	    ((GlOrganizationResponsibilityAssignmentForm) form).clearResponsibilityList();

            if(request.getParameter("saveButton") == null && request.getParameter("updateButton") == null &&
               request.getParameter("goButton") == null && request.getParameter("cancelButton") == null &&
               request.getParameter("glRESList[" +
               ((GlOrganizationResponsibilityAssignmentForm) form).getRowSelected() + "].deleteButton") == null){
	       
	       /*** Populate  organizationList property **/
	       ((GlOrganizationResponsibilityAssignmentForm)form).clearOrganizationList();
	       try{
	          ArrayList ejbGlORGList = ejbRES.getGlOrgAll(user.getCmpCode());
	          Iterator i = ejbGlORGList.iterator();
	          while(i.hasNext()){
	             ((GlOrganizationResponsibilityAssignmentForm)form).setOrganizationList(
	             ((GlOrganizationDetails)i.next()).getOrgName());
	          }
	       }catch(GlORGNoOrganizationFoundException ex){
	          ((GlOrganizationResponsibilityAssignmentForm)form).setOrganizationList(Constants.GLOBAL_NO_RECORD_FOUND);
	       }catch(EJBException ex){
	          if(log.isInfoEnabled()){
	             log.info("EJBException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " +
	             ex.getMessage() + " session: " + session.getId());
	          }
	          return(mapping.findForward("cmnErrorPage"));
	       }
	       ((GlOrganizationResponsibilityAssignmentForm)form).setOrganization(null);
	       ((GlOrganizationResponsibilityAssignmentForm)form).setDescription(null);
	    }

	    
            /*** Populate ArrayList property **/
	    if(((GlOrganizationResponsibilityAssignmentForm)form).getOrganization() != null){
	       try{
	    	   
	    	   System.out.println("finding organization ass list");
	    	   System.out.println("param 1: " + ((GlOrganizationResponsibilityAssignmentForm)form).getOrganization());
	          GlOrganizationDetails details = ejbRES.getGlOrgDescriptionByOrgName(
	             ((GlOrganizationResponsibilityAssignmentForm)form).getOrganization(), user.getCmpCode());
	          
	          
	             ((GlOrganizationResponsibilityAssignmentForm)form).setDescription(details.getOrgDescription());
	             
	       }catch(GlORGNoOrganizationFoundException ex){
	    	   
	          errors.add(ActionMessages.GLOBAL_MESSAGE,
	             new ActionMessage("organizationResponsibilityAssignment.error.organizationNotFound"));
	          
	      }catch(EJBException ex){
	    	  
	    	  System.out.println("why error");
	         if(log.isInfoEnabled()){
	            log.info("EJBException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " +
	            ex.getMessage() + " session: " + session.getId());
	         }
	         return(mapping.findForward("cmnErrorPage"));
	      }
	      if(!errors.isEmpty()){
	    	  
	    	  System.out.println("empty?");
	         saveErrors(request, new ActionMessages(errors));
	         return(mapping.findForward("glOrganizationResponsibilityAssignment"));
	      }
	     
	      /*** Populate responsibilityList property **/
	      ArrayList resNameList = Common.getAdResAll(
                    (DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY), user.getCmpCode());
              		
              if(resNameList.size() > 0){
                 Iterator i = resNameList.iterator();
                 while(i.hasNext()){
                	
                    String resName = (String)i.next();
                    
                    System.out.println("res:" + resName);
                   ((GlOrganizationResponsibilityAssignmentForm) form).setResponsibilityList(resName);
                 }
              }else{
            	  System.out.println("res no list");
                 ((GlOrganizationResponsibilityAssignmentForm) form).setResponsibilityList(Constants.GLOBAL_NO_RECORD_FOUND);
              }
	      
	      /*** Populate ArrayList property **/
	      try{
	         ArrayList ejbGlRESList = ejbRES.getGlResByOrgName(
	            ((GlOrganizationResponsibilityAssignmentForm)form).getOrganization(), user.getCmpCode());
	         Iterator i = ejbGlRESList.iterator();
	         GlOrganizationResponsibilityAssignmentList glRESList = null;
	         
	         while(i.hasNext()){
	        	
	            GlResponsibilityDetails details = (GlResponsibilityDetails)i.next();
	          
	            System.out.println("get res list code " + details.getResCode()	);
	            glRESList = new GlOrganizationResponsibilityAssignmentList(
			    ((GlOrganizationResponsibilityAssignmentForm)form),
		            Common.getAdResByResCode((DataSource)servlet.getServletContext().getAttribute(Globals.DATA_SOURCE_KEY), 
			       details.getResCode(), user.getCmpCode()),
			    Common.convertByteToBoolean(details.getResEnable()));
		            ((GlOrganizationResponsibilityAssignmentForm)form).saveGlRESList(glRESList);
	         }
	         
	      }catch(GlORGNoOrganizationFoundException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
	            new ActionMessage("organizationResponsibilityAssignment.error.organizationNotFound"));
	      }catch(GlRESNoResponsibilityFoundException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE,
	            new ActionMessage("organizationResponsibilityAssignment.error.noResponsibilityFound"));
          }catch(SQLException sqle){
        	  System.out.println("error res list");
	         if(log.isInfoEnabled()){
		    log.info("SQLException caught in GlResponsibilityAssignmentAction.execute(): " + sqle.getMessage() +
		    " session: " + session.getId());
		 }
		 return(mapping.findForward("cmnErrorPage"));
	      }catch(EJBException ex){
	         if(log.isInfoEnabled()){
	            log.info("EJBException caught in GlOrganizationResponsibilityAssignmentAction.execute(): " +
		    ex.getMessage() + " session: " + session.getId());
                 }
                 return(mapping.findForward("cmnErrorPage"));
              }
	    
              if(!errors.isEmpty()){
                 saveErrors(request, new ActionMessages(errors));
              }else{
                 if((request.getParameter("saveButton") != null || 
                     request.getParameter("updateButton") != null || 
                     request.getParameter("glRESList[" + 
                     ((GlOrganizationResponsibilityAssignmentForm) form).getRowSelected() + "].deleteButton") != null) && 
                     ((GlOrganizationResponsibilityAssignmentForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                     ((GlOrganizationResponsibilityAssignmentForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
                 }
              }
	   }

           ((GlOrganizationResponsibilityAssignmentForm)form).reset(mapping, request);
           
	        if (((GlOrganizationResponsibilityAssignmentForm)form).getTableType() == null) {
      		      	
	           ((GlOrganizationResponsibilityAssignmentForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                      
           
           ((GlOrganizationResponsibilityAssignmentForm)form).setPageState(Constants.PAGE_STATE_SAVE);
           return(mapping.findForward("glOrganizationResponsibilityAssignment"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()){
             log.info("Exception caught in GlOrganizationResponsibilityAssignmentAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
