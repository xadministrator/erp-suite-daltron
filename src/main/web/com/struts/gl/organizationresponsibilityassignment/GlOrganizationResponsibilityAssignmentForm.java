package com.struts.gl.organizationresponsibilityassignment;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlOrganizationResponsibilityAssignmentForm extends ActionForm implements Serializable {

   private String organization = null;
   private ArrayList organizationList = new ArrayList();
   private String description = null;
   private String responsibility = null;
   private ArrayList responsibilityList = new ArrayList();
   private boolean enabled = false;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glRESList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   public int getRowSelected(){
      return rowSelected;
   }

   public GlOrganizationResponsibilityAssignmentList getGlRESByIndex(int index){
      return((GlOrganizationResponsibilityAssignmentList)glRESList.get(index));
   }

   public Object[] getGlRESList(){
      return(glRESList.toArray());
   }

   public int getGlRESListSize(){
      return(glRESList.size());
   }

   public void saveGlRESList(Object newGlRESList){
      glRESList.add(newGlRESList);
   }

   public void clearGlRESList(){
      glRESList.clear();
   }

   public void setRowSelected(Object selectedGlRESList, boolean isEdit){
      this.rowSelected = glRESList.indexOf(selectedGlRESList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showGlRESRow(int rowSelected){
      this.responsibility = ((GlOrganizationResponsibilityAssignmentList)glRESList.get(rowSelected)).getResponsibility();
      this.enabled = ((GlOrganizationResponsibilityAssignmentList)glRESList.get(rowSelected)).getEnabled();
   }

   public void updateGlRESRow(int rowSelected, Object newGlRESList){
      glRESList.set(rowSelected, newGlRESList);
   }

   public void deleteGlRESList(int rowSelected){
      glRESList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getOrganization(){
      return(organization);
   }

   public void setOrganization(String organization){
      this.organization = organization;
   }

   public ArrayList getOrganizationList(){
      return(organizationList);
   }

   public void setOrganizationList(String organization){
      organizationList.add(organization);
   }

   public void clearOrganizationList(){
      organizationList.clear();
      organizationList.add(Constants.GLOBAL_BLANK);
   }


   public String getDescription(){
      return(description);
   }

   public void setDescription(String description){
      this.description = description;
   }

   public String getResponsibility(){
      return(responsibility);
   }

   public void setResponsibility(String responsibility){
      this.responsibility = responsibility;
   }

   public ArrayList getResponsibilityList(){
      return(responsibilityList);
   }

   public void setResponsibilityList(String responsibility){
      responsibilityList.add(responsibility);
   }

   public void clearResponsibilityList(){
      responsibilityList.clear();
      responsibilityList.add(Constants.GLOBAL_BLANK);
   }

   public boolean getEnabled(){
      return(enabled);
   }

   public void setEnabled(boolean enabled){
      this.enabled = enabled;
   }

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
      responsibility = Constants.GLOBAL_BLANK;
      enabled = false;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("goButton") != null){
         if(Common.validateRequired(organization) || organization.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("organization",
               new ActionMessage("organizationResponsibilityAssignment.error.organizationRequired"));
          }
      }
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(organization) || organization.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("organization",
               new ActionMessage("organizationResponsibilityAssignment.error.organizationRequired"));
         }
         if(Common.validateRequired(responsibility)){
            errors.add("responsibility",
               new ActionMessage("organizationResponsibilityAssignment.error.responsibilityRequired"));
         }
         if(responsibility.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("responsibility",
               new ActionMessage("organizationResponsibilityAssignment.error.responsibilityRequired"));
         }
         if(!Common.validateStringExists(responsibilityList, responsibility)){
            errors.add("responsibility",
               new ActionMessage("organizationResponsibilityAssignment.error.responsibilityInvalid"));
         }
      }
      if(request.getParameter("updateButton") != null){
         GlOrganizationResponsibilityAssignmentList glRESList = getGlRESByIndex(getRowSelected());
         if(!glRESList.getResponsibility().equals(getResponsibility())){
            errors.add("responsibilityName",
               new ActionMessage("organizationResponsibilityAssignment.error.responsibilityUpdateInvalid"));
         }
      }
      return(errors);
   }
}
