package com.struts.gl.organizationresponsibilityassignment;

import java.io.Serializable;

public class GlOrganizationResponsibilityAssignmentList implements Serializable {

   private String responsibility = null;
   private boolean enabled = false;

   private String deleteButton = null;
   private String editButton = null;
    
   private GlOrganizationResponsibilityAssignmentForm parentBean;
    
   public GlOrganizationResponsibilityAssignmentList(GlOrganizationResponsibilityAssignmentForm parentBean,
      String responsibility,
      boolean enabled){

      this.parentBean = parentBean;
      this.responsibility = responsibility;
      this.enabled = enabled;
   }

   public void setDeleteButton(String deleteButton){
      parentBean.setRowSelected(this, false);
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public String getResponsibility(){
      return(responsibility);
   }

   public boolean getEnabled(){
      return(enabled);
   }

}
