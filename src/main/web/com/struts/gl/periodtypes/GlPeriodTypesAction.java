package com.struts.gl.periodtypes;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlPTNoPeriodTypeFoundException;
import com.ejb.exception.GlPTPeriodTypeAlreadyAssignedException;
import com.ejb.exception.GlPTPeriodTypeAlreadyDeletedException;
import com.ejb.exception.GlPTPeriodTypeAlreadyExistException;
import com.ejb.txn.GlPeriodTypeController;
import com.ejb.txn.GlPeriodTypeControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlPeriodTypeDetails;

public final class GlPeriodTypesAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
   
   
       HttpSession session = request.getSession();
       try{
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          if (user != null) {
             if (log.isInfoEnabled()){
                log.info("GlPeriodTypesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             }
          }else{
             if (log.isInfoEnabled()){
	        log.info("User is not logged on in session " + session.getId());
             }
	     return(mapping.findForward("adLogon"));
          }

	  String frParam = Common.getUserPermission(user, Constants.GL_PERIOD_TYPES_ID);
          if(frParam != null){
	     if(frParam.trim().equals(Constants.FULL_ACCESS)){
	        ActionErrors fieldErrors = ((GlPeriodTypesForm)form).validateFields(mapping, request);
	        if(!fieldErrors.isEmpty()){
                   saveErrors(request, new ActionMessages(fieldErrors));
		   return(mapping.findForward("glPeriodTypes"));
	        }
             }
             ((GlPeriodTypesForm)form).setUserPermission(frParam.trim());	
          }else{
	     ((GlPeriodTypesForm)form).setUserPermission(Constants.NO_ACCESS);
	  }
	  
/*******************************************************
   Initialize GlPeriodTypeController EJB
*******************************************************/

          GlPeriodTypeControllerHome homePT = null;
          GlPeriodTypeController ejbPT = null;
       
          try{
             
             homePT = (GlPeriodTypeControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlPeriodTypeControllerEJB", GlPeriodTypeControllerHome.class);
             
          }catch(NamingException e){
             if(log.isInfoEnabled()){
	       log.info("NamingException caught in GlPeriodTypesAction.execute(): " + e.getMessage() + 
	          " session: " + session.getId()); 
	     }
             return(mapping.findForward("cmnErrorPage")); 
          }
       
          try{
              ejbPT = homePT.create();
          }catch(CreateException e){
             if(log.isInfoEnabled()){
                log.info("CreateException caught in GlPeriodTypesAction.execute(): " + e.getMessage() + 
	           " session: " + session.getId());
             }
             return(mapping.findForward("cmnErrorPage"));
          }
	  
          ActionErrors errors = new ActionErrors();
	  
/*******************************************************
   -- Gl PT Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlPeriodTypesForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glPeriodTypes"));
	     
/*******************************************************
   -- Gl PT Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlPeriodTypesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glPeriodTypes"));         	  
	  
/*******************************************************
   -- GL PT Save Action --
*******************************************************/

	  } else if(request.getParameter("saveButton") != null && 
	     ((GlPeriodTypesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
	     GlPeriodTypeDetails details = new GlPeriodTypeDetails(
	        ((GlPeriodTypesForm)form).getPeriodTypeName().trim().toUpperCase(),
		((GlPeriodTypesForm)form).getDescription().trim().toUpperCase(),
		Common.convertStringToShort(((GlPeriodTypesForm)form).getPeriodsPerYear()),
		Common.getGlYearTypeCode(((GlPeriodTypesForm)form).getYearType()));
	  
/*******************************************************
   Call GlPeriodTypeController EJB 
   addGlPtEntry(GlPeriodTypeDetails details) method
*******************************************************/

	     try{
                ejbPT.addGlPtEntry(details, user.getCmpCode());
	     }catch(GlPTPeriodTypeAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("periodTypes.error.periodTypeAlreadyExists"));
             }catch(EJBException ex){
                if(log.isInfoEnabled()){
                   log.info("EJBException caught in GlPeriodTypesAction.execute(): " + ex.getMessage() +
                      " session: " + session.getId());
	        }
                return(mapping.findForward("cmnErrorPage"));
	      } 

	     
/*******************************************************
   -- GL PT Close Action --
*******************************************************/

	  }else if(request.getParameter("closeButton") != null){
	     return(mapping.findForward("cmnMain"));
	      
/*******************************************************
   -- GL PT Update Action --
*******************************************************/

	  }else if(request.getParameter("updateButton") != null && 
	     ((GlPeriodTypesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     GlPeriodTypesList glPTList = 
	        ((GlPeriodTypesForm)form).getGlPTByIndex(((GlPeriodTypesForm) form).getRowSelected());
	    
	     GlPeriodTypeDetails details = new GlPeriodTypeDetails(
	        glPTList.getPeriodTypeCode(),
                ((GlPeriodTypesForm)form).getPeriodTypeName().trim().toUpperCase(),
                ((GlPeriodTypesForm)form).getDescription().trim().toUpperCase(),
                Common.convertStringToShort(((GlPeriodTypesForm)form).getPeriodsPerYear()),
                Common.getGlYearTypeCode(((GlPeriodTypesForm)form).getYearType()));
	       
/*******************************************************
   Call GlPeriodTypeController EJB updateGlPtEntry(
   GlPeriodTypeDetails details) method
*******************************************************/

	     try {
                ejbPT.updateGlPtEntry(details, user.getCmpCode());
             }catch(GlPTPeriodTypeAlreadyExistException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("periodTypes.error.periodTypeAlreadyExists"));
	     }catch(GlPTPeriodTypeAlreadyAssignedException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("periodTypes.error.updatePeriodTypeAlreadyAssigned"));
             }catch(GlPTPeriodTypeAlreadyDeletedException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("periodTypes.error.periodTypeAlreadyDeleted"));
	     }catch(EJBException ex){
	        if(log.isInfoEnabled()){
		   log.info("EJBException caught in GlPeriodTypesAction.execute(): " + ex.getMessage() +
	              " session: " + session.getId());
	        }
	        return(mapping.findForward("cmnErrorPage"));
	     }
	     
/*******************************************************
   -- GL PT Cancel Action --
*******************************************************/

	  }else if(request.getParameter("cancelButton") != null){
	       
/*******************************************************
   -- GL PT Edit Action --
*******************************************************/

	  }else if(request.getParameter("glPTList[" + 
             ((GlPeriodTypesForm) form).getRowSelected() + "].editButton") != null && 
	     ((GlPeriodTypesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){	     
	     
/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlPeriodTypesForm properties
*******************************************************/

	        ((GlPeriodTypesForm) form).showGlPTRow(((GlPeriodTypesForm) form).getRowSelected());
		   
	         return(mapping.findForward("glPeriodTypes"));
		     
/*******************************************************
  -- GL PT Delete Action --
*******************************************************/

	   }else if(request.getParameter("glPTList[" + 
	      ((GlPeriodTypesForm) form).getRowSelected() + "].deleteButton") != null && 
	      ((GlPeriodTypesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	      
	      GlPeriodTypesList glPTList =
                 ((GlPeriodTypesForm)form).getGlPTByIndex(((GlPeriodTypesForm) form).getRowSelected());

/*******************************************************
   Call GlPeriodTypeController EJB deleteGlPtEntry(
   Integer PT_CODE)
*******************************************************/

              try{
	         ejbPT.deleteGlPtEntry(glPTList.getPeriodTypeCode(), user.getCmpCode());
	      }catch(GlPTPeriodTypeAlreadyAssignedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("periodTypes.error.deletePeriodTypeAlreadyAssigned"));
	      }catch(GlPTPeriodTypeAlreadyDeletedException ex){
	         errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("periodTypes.error.periodTypeAlreadyDeleted"));
	      }catch(EJBException ex){
                 if(log.isInfoEnabled()){
                    log.info("EJBException caught in GlPeriodTypesAction.execute(): " + ex.getMessage() +
	               " session: " + session.getId());
	         }
	         return(mapping.findForward("cmnErrorPage"));
	      }
	      

/*******************************************************
   GL PT Load Action
*******************************************************/

	   }
	   if(frParam != null){
              if (!errors.isEmpty()) {
	         saveErrors(request, new ActionMessages(errors));
		 return(mapping.findForward("glPeriodTypes"));
	      }
              ((GlPeriodTypesForm)form).clearGlPTList();
	      
/*******************************************************
   Call GlPeriodTypeController EJB getGlPtAll() method
   Populate ArrayList property from GlPeriodTypeController
   EJB getGlPtAll() method ArrayList return 
*******************************************************/

              try{
	          ArrayList ejbGlPTList = ejbPT.getGlPtAll(user.getCmpCode());
		  Iterator i = ejbGlPTList.iterator();
   	          while(i.hasNext()){
		     GlPeriodTypeDetails details = (GlPeriodTypeDetails)i.next();
		     GlPeriodTypesList glPTList = new GlPeriodTypesList(((GlPeriodTypesForm)form),
		        details.getPtCode(),
		        details.getPtName(),
		        Common.convertShortToString(details.getPtPeriodPerYear()),
		        details.getPtDescription(),
		        Common.getGlYearTypeDescription(details.getPtYearType()));
		        ((GlPeriodTypesForm) form).saveGlPTList(glPTList);
		  }
	      }catch(GlPTNoPeriodTypeFoundException ex){
	         
	      }catch(EJBException ex){
	         if(log.isInfoEnabled()){
	            log.info("EJBException caught in GlPeriodTypesAction.execute(): " + ex.getMessage() + 
	               " session: " + session.getId());
		 }
		 return(mapping.findForward("cmnErrorPage"));
	      }

	      if(!errors.isEmpty()){
                 saveErrors(request, new ActionMessages(errors));
              }else{
                 if((request.getParameter("saveButton") != null || request.getParameter("updateButton") != null ||
                    request.getParameter("glPTList[" +
                    ((GlPeriodTypesForm) form).getRowSelected() + "].deleteButton") != null) &&
		    ((GlPeriodTypesForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                    ((GlPeriodTypesForm)form).setTxnStatus(Constants.STATUS_SUCCESS);  
		 }
	      }

	      ((GlPeriodTypesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
	      ((GlPeriodTypesForm)form).reset(mapping, request);
	      
	        if (((GlPeriodTypesForm)form).getTableType() == null) {
      		      	
	           ((GlPeriodTypesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           	      
	      
	      
	      return(mapping.findForward("glPeriodTypes"));
   	   }else{
	      errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
              saveErrors(request, new ActionMessages(errors));
		   
	      return(mapping.findForward("cmnMain"));
	   }
	    
      }catch(Exception e){

/*******************************************************
   Failed: Forward to error page
********************************************************/

         if(log.isInfoEnabled()){
	    log.info("Exception caught in GlPeriodTypesAction.execute(): " + e.getMessage() + " session: " 
	       + session.getId());
	 }
 	return(mapping.findForward("cmnErrorPage"));
      } 
    }
}
