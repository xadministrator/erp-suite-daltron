package com.struts.gl.periodtypes;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlPeriodTypesForm extends ActionForm implements Serializable{

	private String periodTypeName = null;	
	private String periodsPerYear = null;
	private ArrayList periodsPerYearList = new ArrayList();
	private String description = null;
	private String yearType = null;
	private ArrayList yearTypeList = new ArrayList();
    private String tableType = null;

    private String showDetailsButton = null;
    private String hideDetailsButton = null;	
	private String saveButton = null;
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String pageState = new String();
	private ArrayList glPTList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected(){
	   return rowSelected;
	}
	
    public GlPeriodTypesList getGlPTByIndex(int index){
	   return((GlPeriodTypesList)glPTList.get(index));
	}
	public Object[] getGlPTList(){
	    return(glPTList.toArray());
	}

	public int getGlPTListSize(){
	   return(glPTList.size());
	}

	public void saveGlPTList(Object newGlPTList){
	   glPTList.add(newGlPTList);
	}

	public void clearGlPTList(){
	   glPTList.clear();
	}
	
	public void setRowSelected(Object selectedGlPTList, boolean isEdit){
	   this.rowSelected = glPTList.indexOf(selectedGlPTList);
	   if(isEdit){
	      this.pageState = Constants.PAGE_STATE_EDIT;
	   }
	}
	
	public void showGlPTRow(int rowSelected){
	   this.periodTypeName = ((GlPeriodTypesList)glPTList.get(rowSelected)).getPeriodTypeName();
	   this.periodsPerYear = ((GlPeriodTypesList)glPTList.get(rowSelected)).getPeriodsPerYear();
	   this.description = ((GlPeriodTypesList)glPTList.get(rowSelected)).getDescription();
	   this.yearType = ((GlPeriodTypesList)glPTList.get(rowSelected)).getYearType();
	}

	public void updateGlPTRow(int rowSelected, Object newGlPTList){
	   glPTList.set(rowSelected, newGlPTList);
	}

	public void deleteGlPTList(int rowSelected){
	   glPTList.remove(rowSelected);
	}
	
	public void setUpdateButton(String updateButton){
	   this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton){
	   this.cancelButton = cancelButton;
	}
	
	public void setSaveButton(String saveButton){
	   this.saveButton = saveButton;
	}

	public void setCloseButton(String closeButton){
	   this.closeButton = closeButton;
	}
	
    public void setShowDetailsButton(String showDetailsButton) {
		
	   this.showDetailsButton = showDetailsButton;
		
    }
    
    public void setHideDetailsButton(String hideDetailsButton) {
    	
       this.hideDetailsButton = hideDetailsButton;
    	
    }	

	public void setPageState(String pageState){
	   this.pageState = pageState;
	}

	public String getPageState(){
	    return(pageState);
	}

        public String getTxnStatus(){
           String passTxnStatus = txnStatus;
           txnStatus = Constants.GLOBAL_BLANK;
           return(passTxnStatus);
        }

	public void setTxnStatus(String txnStatus){
	   this.txnStatus = txnStatus;
	}

        public String getPeriodTypeName(){
	   return(periodTypeName);
	}

	public void setPeriodTypeName(String periodTypeName){
	   this.periodTypeName = periodTypeName;
	}

	public String getPeriodsPerYear(){
	  return(periodsPerYear);
	}

	public void setPeriodsPerYear(String periodsPerYear){
	   this.periodsPerYear = periodsPerYear;
	}

	public String getDescription(){
	   return(description);
	}

	public void setDescription(String description){
	   this.description = description;
	}

	public String getYearType(){
	   return(yearType);
	}

	public void setYearType(String yearType){
	   this.yearType = yearType;
	}

        public String getUserPermission(){
	   return(userPermission);
	}

	public void setUserPermission(String userPermission){
	   this.userPermission = userPermission;
	}

	public ArrayList getPeriodsPerYearList(){
	   return(periodsPerYearList);
	}

	public ArrayList getYearTypeList(){
	   return(yearTypeList);
	}
	
    public String getTableType() {
    	
       return(tableType);
    	
    }
    
    public void setTableType(String tableType) {
    	
       this.tableType = tableType;
    	
    }	

        public void reset(ActionMapping mapping, HttpServletRequest request){
	   periodTypeName = null;
	   description = null;
	   saveButton = null;
	   closeButton = null;
	   updateButton = null;
	   cancelButton = null;
	   periodsPerYearList.clear();
	   yearTypeList.clear();
           for(int i=1; i<367; i++){
              periodsPerYearList.add(String.valueOf(i));
           }
	   periodsPerYear = "13";
           yearTypeList.add(Constants.GL_YEAR_TYPE_FISCAL_YEAR);
           yearTypeList.add(Constants.GL_YEAR_TYPE_CALENDAR_YEAR);
	   yearType = Constants.GL_YEAR_TYPE_CALENDAR_YEAR;
	   showDetailsButton = null;
	   hideDetailsButton = null;	   
	   
        }

        public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
           ActionErrors errors = new ActionErrors();
           if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
             if(Common.validateRequired(periodTypeName)) {
                errors.add("periodTypeName", new ActionMessage("periodTypes.error.periodTypeNameRequired"));
             }
	     if(Common.validateRequired(periodsPerYear)){
	        errors.add("periodsPerYear", new ActionMessage("periodTypes.error.periodsPerYearRequired"));
	     }
	     if(!Common.validateNumberFormat(periodsPerYear)){
	        errors.add("periodsPerYear", new ActionMessage("periodTypes.error.periodsPerYearInvalid"));
	     }
	     if(!Common.validateStringNumLessThanEqual(periodsPerYear, 366)){
	        errors.add("periodsPerYear", new ActionMessage("periodTypes.error.periodsPerYearInvalid"));
	     }
	     if(Common.validateRequired(yearType)){
	        errors.add("yearType", new ActionMessage("periodTypes.error.yearTypeRequired"));
	     }
	     if(!Common.validateStringExists(yearTypeList, yearType)){
	        errors.add("yearType", new ActionMessage("periodTypes.error.yearTypeInvalid"));
	     }
	   }
	  return(errors);
	}
		
}
