package com.struts.gl.periodtypes;

import java.io.Serializable;

public class GlPeriodTypesList implements Serializable{

	private Integer periodTypeCode = null;
	private String periodTypeName = null;	
	private String periodsPerYear = null;
	private String description = null;
	private String yearType = null;
	
	private String deleteButton = null;
	private String editButton = null;
	
	private GlPeriodTypesForm parentBean;

	public GlPeriodTypesList(GlPeriodTypesForm parentBean, Integer periodTypeCode, String periodTypeName, 
	      String periodsPerYear, String description, String yearType){
	   this.parentBean = parentBean;
	   this.periodTypeCode = periodTypeCode;
	   this.periodTypeName = periodTypeName;
	   this.periodsPerYear = periodsPerYear;
	   this.description = description;
	   this.yearType = yearType;
	}

	public void setParentBean(GlPeriodTypesForm parentBean){
	   this.parentBean = parentBean;
	}

	public void setDeleteButton(String deleteButton){
	   parentBean.setRowSelected(this, false);
	}

	public void setEditButton(String editButton){
	   parentBean.setRowSelected(this, true);
	}

	public Integer getPeriodTypeCode(){
	   return(periodTypeCode);
	}

	public void setPeriodTypeCode(Integer periodTypeCode){
	   this.periodTypeCode = periodTypeCode;
	}

        public String getPeriodTypeName(){
	   return(periodTypeName);
	}

	public void setPeriodTypeName(String periodTypeName){
	   this.periodTypeName = periodTypeName;
	}

	public String getPeriodsPerYear(){
	   return(periodsPerYear);
	}

	public void setPeriodsPerYear(String periodsPerYear){
	   this.periodsPerYear = periodsPerYear;
	}

	public String getDescription(){
	   return(description);
	}

	public void setDescription(String description){
	   this.description = description;
	}

	public String getYearType(){
	   return(yearType);
	}

	public void setYearType(String yearType){
	   this.yearType = yearType;
	}
	
}
