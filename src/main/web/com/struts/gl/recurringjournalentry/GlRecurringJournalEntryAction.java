package com.struts.gl.recurringjournalentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlRecurringJournalEntryController;
import com.ejb.txn.GlRecurringJournalEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModRecurringJournalDetails;
import com.util.GlModRecurringJournalLineDetails;
import com.util.GlRecurringJournalDetails;

public final class GlRecurringJournalEntryAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         
         if (user != null) {
         	
            if (log.isInfoEnabled()) {
            	
                log.info("GlRecurringJournalEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());
                   
            }
            
         }else{
         	
            if (log.isInfoEnabled()) {
            	
               log.info("User is not logged on in session" + session.getId());
               
            }
            
            return(mapping.findForward("adLogon"));
            
         }
         
         GlRecurringJournalEntryForm actionForm = (GlRecurringJournalEntryForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_RECURRING_JOURNAL_ENTRY_ID);
         
         if (frParam != null) {
         	
	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {
	      	
	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
	         
               if (!fieldErrors.isEmpty()) {
             	
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glRecurringJournalEntry"));
                
               }
             
            }
          
            actionForm.setUserPermission(frParam.trim());
          
         } else {
         	
            actionForm.setUserPermission(Constants.NO_ACCESS);
            
         }

/*******************************************************
   Initialize GlRecurringJournalEntryController EJB
*******************************************************/

         GlRecurringJournalEntryControllerHome homeRJ = null;
         GlRecurringJournalEntryController ejbRJ = null;

         try {
            
            homeRJ = (GlRecurringJournalEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlRecurringJournalEntryControllerEJB", GlRecurringJournalEntryControllerHome.class);
            
         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlRecurringJournalEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {
         	
            ejbRJ = homeRJ.create();
            
         } catch(CreateException e) {
         	
            if(log.isInfoEnabled()) {
            	
                log.info("CreateException caught in GlRecurringJournalEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   Call GlRecurringJournalEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/

         short precisionUnit = 0;
         short journalLineNumber = 0;
         boolean enableJournalBatch = false;
         
         try {
         	
            precisionUnit = ejbRJ.getGlFcPrecisionUnit(user.getCmpCode());
            journalLineNumber = ejbRJ.getAdPrfGlJournalLineNumber(user.getCmpCode());  
            enableJournalBatch = Common.convertByteToBoolean(ejbRJ.getAdPrfEnableGlJournalBatch(user.getCmpCode()));
            actionForm.setShowBatchName(enableJournalBatch);

                    
         } catch(EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlRecurringJournalEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }
            
            return(mapping.findForward("cmnErrorPage"));
            
         }
											
															 
/*******************************************************
   -- Gl RJ Save Action --
*******************************************************/

         if (request.getParameter("saveButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
           	           	
           GlRecurringJournalDetails details = new GlRecurringJournalDetails();
           details.setRjCode(actionForm.getRecurringJournalCode());
           details.setRjName(actionForm.getName());
           details.setRjDescription(actionForm.getDescription());
		   details.setRjSchedule(actionForm.getReminderSchedule());
 		   details.setRjNextRunDate(Common.convertStringToSQLDate(actionForm.getNextRunDate()));
 		   details.setRjUserName1(actionForm.getNotifiedUser1());
 		   details.setRjUserName2(actionForm.getNotifiedUser2());
 		   details.setRjUserName3(actionForm.getNotifiedUser3());
 		   details.setRjUserName4(actionForm.getNotifiedUser4());
 		   details.setRjUserName5(actionForm.getNotifiedUser5());
           
           ArrayList rjlList = new ArrayList();
           int lineNumber = 0;  
           
           double TOTAL_DEBIT = 0;
           double TOTAL_CREDIT = 0;         
                      
           for (int i = 0; i<actionForm.getGlRJListSize(); i++) {
           	
           	   GlRecurringJournalEntryList glRJList = actionForm.getGlRJByIndex(i);           	   
           	              	   
           	   if (Common.validateRequired(glRJList.getAccount()) &&
      	 	       Common.validateRequired(glRJList.getDebitAmount()) &&
      	 	       Common.validateRequired(glRJList.getCreditAmount())) continue;
           	   
           	   byte isDebit = 0;
		       double amount = 0d;
	
		       if (!Common.validateRequired(glRJList.getDebitAmount())) {
		       	
		          isDebit = 1;
		          amount = Common.convertStringMoneyToDouble(glRJList.getDebitAmount(), precisionUnit);
		          
		          TOTAL_DEBIT += amount;
		          
		       } else {
		       	
		          isDebit = 0;
		          amount = Common.convertStringMoneyToDouble(glRJList.getCreditAmount(), precisionUnit); 
		          
		          TOTAL_CREDIT += amount;
		       }
		       
		       lineNumber++;

           	   GlModRecurringJournalLineDetails mdetails = new GlModRecurringJournalLineDetails();
           	   
           	   mdetails.setRjlLineNumber((short)(lineNumber));
           	   mdetails.setRjlDebit(isDebit);
           	   mdetails.setRjlAmount(amount);
           	   mdetails.setRjlCoaAccountNumber(glRJList.getAccount());
           	   
           	   rjlList.add(mdetails);
           	
           }
           
           TOTAL_DEBIT = Common.roundIt(TOTAL_DEBIT, precisionUnit);
           TOTAL_CREDIT = Common.roundIt(TOTAL_CREDIT, precisionUnit);
           
           if (TOTAL_DEBIT != TOTAL_CREDIT) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringJournalEntry.error.recurringJournalNotBalance"));
                    
               saveErrors(request, new ActionMessages(errors));
               return (mapping.findForward("glRecurringJournalEntry"));
           	
           }
           	
           
           try {
           	
           	    ejbRJ.saveGlRjEntry(details, actionForm.getCategory(), actionForm.getBatchName(),
           	        rjlList, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
           	
           } catch (GlobalRecordAlreadyExistException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringJournalEntry.error.recordAlreadyExist"));
           	
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringJournalEntry.error.recordAlreadyDeleted"));                               	
                                          	
           } catch (GlobalAccountNumberInvalidException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringJournalEntry.error.accountNumberInvalid", ex.getMessage()));           
           	
           } catch (EJBException ex) {
           	
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlRecurringJournalEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- Gl RJ Close Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {
         	
            try {
           	
           	    ejbRJ.deleteGlRjEntry(actionForm.getRecurringJournalCode(), user.getCmpCode());
           	   
           } catch (GlobalRecordAlreadyDeletedException ex) {
           	
           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringJournalEntry.error.recordAlreadyDeleted"));
                    
           } catch (EJBException ex) {
           	    if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlRecurringJournalEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                }
               
               return(mapping.findForward("cmnErrorPage"));
           } 

/*******************************************************
   -- Gl RJ Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {
         	
            return(mapping.findForward("cmnMain"));
            		       
/*******************************************************
   -- Gl RJ Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {
         	
         	int listSize = actionForm.getGlRJListSize();
                                   
            for (int x = listSize + 1; x <= listSize + journalLineNumber; x++) {
	        	
	        	GlRecurringJournalEntryList glRJList = new GlRecurringJournalEntryList(actionForm,
	        	    null, String.valueOf(x), null, null, null, null);
	        	
	        	actionForm.saveGlRJList(glRJList);
	        	
	        }	        
	        
	        return(mapping.findForward("glRecurringJournalEntry"));
	        
/*******************************************************
   -- Gl RJ Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {
         	
         	for (int i = 0; i<actionForm.getGlRJListSize(); i++) {
           	
           	   GlRecurringJournalEntryList glRJList = actionForm.getGlRJByIndex(i);
           	   
           	   if (glRJList.getDeleteCheckbox()) {
           	   	
           	   	   actionForm.deleteGlRJList(i);
           	   	   i--;
           	   }
           	   
            }
            
            for (int i = 0; i<actionForm.getGlRJListSize(); i++) {
           	
           	   GlRecurringJournalEntryList glRJList = actionForm.getGlRJByIndex(i);
           	   
           	   glRJList.setLineNumber(String.valueOf(i+1));
           	   
            }
	        
	        return(mapping.findForward("glRecurringJournalEntry"));

/*******************************************************
   -- Gl RJ Load Action --
*******************************************************/

         }
         
         if (frParam != null) {
         	
            if (!errors.isEmpty()) {
            	
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glRecurringJournalEntry"));
               
            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
            
            try {
            	
            	ArrayList list = null;
            	Iterator i = null;
            	
            	actionForm.clearNotifiedUser1List();           	
            	actionForm.clearNotifiedUser2List();           	
            	actionForm.clearNotifiedUser3List();           	
            	actionForm.clearNotifiedUser4List();           	
            	actionForm.clearNotifiedUser5List();           	
            	
            	list = ejbRJ.getAdUsrAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setNotifiedUser1List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser2List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser3List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser4List(Constants.GLOBAL_NO_RECORD_FOUND);
            		actionForm.setNotifiedUser5List(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            			String userName = (String)i.next();
            			actionForm.setNotifiedUser1List(userName);
            			actionForm.setNotifiedUser2List(userName);
            			actionForm.setNotifiedUser3List(userName);
            			actionForm.setNotifiedUser4List(userName);
            			actionForm.setNotifiedUser5List(userName);
            		                			
            		}
            		
            	}
            	
            	actionForm.clearCategoryList();           	
            	
            	list = ejbRJ.getGlJcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCategoryList((String)i.next());
            			
            		}
            		
            		actionForm.setCategory("GENERAL");
            		
            	}   
            	
            	actionForm.clearBatchNameList();           	
            	
            	list = ejbRJ.getGlOpenJbAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setBatchNameList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setBatchNameList((String)i.next());
            			
            		}
            		            		
            	}
            	            	
            	actionForm.clearGlRJList();               	  	
            	
            	if (request.getParameter("forward") != null) {
            		
            		actionForm.setRecurringJournalCode(new Integer(request.getParameter("recurringJournalCode")));
            		
            		GlModRecurringJournalDetails mdetails = ejbRJ.getGlRjByRjCode(actionForm.getRecurringJournalCode(), user.getCmpCode());
            		
            		actionForm.setName(mdetails.getRjName());
            		actionForm.setDescription(mdetails.getRjDescription());
            		actionForm.setTotalDebit(Common.convertDoubleToStringMoney(mdetails.getRjTotalDebit(), precisionUnit));
            		actionForm.setTotalCredit(Common.convertDoubleToStringMoney(mdetails.getRjTotalCredit(), precisionUnit));
            		actionForm.setCategory(mdetails.getRjJcName());
            		actionForm.setReminderSchedule(mdetails.getRjSchedule());
            		actionForm.setNextRunDate(Common.convertSQLDateToString(mdetails.getRjNextRunDate()));
            		actionForm.setLastRunDate(Common.convertSQLDateToString(mdetails.getRjLastRunDate()));
            		actionForm.setNotifiedUser1(mdetails.getRjUserName1());
            		actionForm.setNotifiedUser2(mdetails.getRjUserName2());
            		actionForm.setNotifiedUser3(mdetails.getRjUserName3());
            		actionForm.setNotifiedUser4(mdetails.getRjUserName4());
            		actionForm.setNotifiedUser5(mdetails.getRjUserName5());
            		            		
            		if (!actionForm.getBatchNameList().contains(mdetails.getRjJbName())) {
            			
           		    	actionForm.setBatchNameList(mdetails.getRjJbName());
           		    	
           		    }           		    
            		actionForm.setBatchName(mdetails.getRjJbName());
            		
            		list = mdetails.getRjRjlList();              		
            		         		            		            		
		            i = list.iterator();
		            
		            while (i.hasNext()) {
		            	
			           GlModRecurringJournalLineDetails mRjlDetails = (GlModRecurringJournalLineDetails)i.next();
			           
				       String debitAmount = null;
				       String creditAmount = null;
				       
				       if (mRjlDetails.getRjlDebit() == 1) {
				       	
				          debitAmount = Common.convertDoubleToStringMoney(mRjlDetails.getRjlAmount(), precisionUnit);				          
				          
				       } else {
				       	
				          creditAmount = Common.convertDoubleToStringMoney(mRjlDetails.getRjlAmount(), precisionUnit);				          
				          
				       }
				       
				       
				       GlRecurringJournalEntryList glRJLList = new GlRecurringJournalEntryList(actionForm,
				          mRjlDetails.getRjlCode(),
				          Common.convertShortToString(mRjlDetails.getRjlLineNumber()),
				          mRjlDetails.getRjlCoaAccountNumber(),
				          debitAmount, creditAmount,
				          mRjlDetails.getRjlCoaAccountDescription());
				          actionForm.saveGlRJList(glRJLList);
			        }	
			        
			        int remainingList = journalLineNumber - (list.size() % journalLineNumber) + list.size();
			        
			        for (int x = list.size() + 1; x <= remainingList; x++) {
			        	
			        	GlRecurringJournalEntryList glRJList = new GlRecurringJournalEntryList(actionForm,
			        	    null, String.valueOf(x), null, null, null, null);
			        	
			        	actionForm.saveGlRJList(glRJList);
			        	
			         }	
			         
			         this.setFormProperties(actionForm);
			         return (mapping.findForward("glRecurringJournalEntry"));         
            		
            	}
            	
	            for (int x = 1; x <= journalLineNumber; x++) {
			
			    	GlRecurringJournalEntryList glRJList = new GlRecurringJournalEntryList(actionForm,
			    	    null, String.valueOf(x), null, null, null, null);
			    	
			    	actionForm.saveGlRJList(glRJList);
			    	
	  	        }            	           			
            		           			   	
            } catch(GlobalNoRecordFoundException ex) {
            	
            	for (int x = 1; x <= journalLineNumber; x++) {
			
			    	GlRecurringJournalEntryList glRJList = new GlRecurringJournalEntryList(actionForm,
			    	    null, String.valueOf(x), null, null, null, null);
			    	
			    	actionForm.saveGlRJList(glRJList);
			    	
	  	        }
            	
            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("recurringJournalEntry.error.journalAlreadyDeleted"));
            		
            } catch(EJBException ex) {
	     	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlRecurringJournalEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
           } 

	               

            if (!errors.isEmpty()) {
          	
               saveErrors(request, new ActionMessages(errors));
             
            } else {
          	
               if (request.getParameter("saveButton") != null && 
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
               }
               
            }
                        
            actionForm.reset(mapping, request);              

            actionForm.setRecurringJournalCode(null);          
            actionForm.setNextRunDate(Common.convertSQLDateToString(new java.util.Date()));            
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("glRecurringJournalEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
            
         }
         
      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
 
         if (log.isInfoEnabled()) {
      	
            log.info("Exception caught in GlRecurringJournalEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }
         
         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));
         
      }
   }


	private void setFormProperties(GlRecurringJournalEntryForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			if (actionForm.getRecurringJournalCode() != null) {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
				actionForm.setShowDeleteButton(true);
				
				
			} else {
				
				actionForm.setEnableFields(true);
				actionForm.setShowSaveButton(true);
				actionForm.setShowAddLinesButton(true);
				actionForm.setShowDeleteLinesButton(true);
				actionForm.setShowDeleteButton(false);
				
			}
			
			
			
		} else {
			
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setShowDeleteButton(false);
			
			
		}
		    
	}
	
}