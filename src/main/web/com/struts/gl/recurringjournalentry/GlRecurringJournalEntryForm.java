package com.struts.gl.recurringjournalentry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlRecurringJournalEntryForm extends ActionForm implements Serializable {

   private Integer recurringJournalCode = null;
   private String name = null;
   private String description = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList();
   private String totalDebit = null;
   private String totalCredit = null;
   private String reminderSchedule = null;
   private ArrayList reminderScheduleList = new ArrayList();
   private String nextRunDate = null;
   private String lastRunDate = null;
   private String notifiedUser1 = null;
   private ArrayList notifiedUser1List = new ArrayList();
   private String notifiedUser2 = null;
   private ArrayList notifiedUser2List = new ArrayList();
   private String notifiedUser3 = null;
   private ArrayList notifiedUser3List = new ArrayList();
   private String notifiedUser4 = null;
   private ArrayList notifiedUser4List = new ArrayList();
   private String notifiedUser5 = null;
   private ArrayList notifiedUser5List = new ArrayList();   
   
   private ArrayList glRJList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   private boolean enableFields = false;
   private boolean showAddLinesButton = false;
   private boolean showDeleteLinesButton = false;
   private boolean showSaveButton = false;
   private boolean showDeleteButton = false;
   
   private String batchName = null;
   private boolean showBatchName = false;
   private ArrayList batchNameList = new ArrayList();
      
   public int getRowSelected(){
      return rowSelected;
   }

   public GlRecurringJournalEntryList getGlRJByIndex(int index){
      return((GlRecurringJournalEntryList)glRJList.get(index));
   }

   public Object[] getGlRJList(){
      return(glRJList.toArray());
   }

   public int getGlRJListSize(){
      return(glRJList.size());
   }

   public void saveGlRJList(Object newGlRJList){
      glRJList.add(newGlRJList);
   }

   public void clearGlRJList(){
      glRJList.clear();
   }

   public void setRowSelected(Object selectedGlRJList, boolean isEdit){
      this.rowSelected = glRJList.indexOf(selectedGlRJList);
   }

   public void updateGlRJRow(int rowSelected, Object newGlRJList){
      glRJList.set(rowSelected, newGlRJList);
   }

   public void deleteGlRJList(int rowSelected){
      glRJList.remove(rowSelected);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public Integer getRecurringJournalCode() {
   	
   	  return recurringJournalCode;
   	
   }
   
   public void setRecurringJournalCode(Integer recurringJournalCode) {
   	
   	  this.recurringJournalCode = recurringJournalCode;
   	
   }
   
   public String getName() {
   	  
   	  return name;
   	
   }
   
   public void setName(String name) {
   	
   	  this.name = name;
   	
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }   
   
   public String getCategory() {
   	
   	  return category;
   	
   }
   
   public void setCategory(String category) {
   	
   	  this.category = category;
   	
   }
   
   public ArrayList getCategoryList() {
   	
   	   return categoryList;
   	
   }
   
   public void setCategoryList(String category) {
   	
   	   categoryList.add(category);
   	
   }
   
   public void clearCategoryList() {
   	
   	   categoryList.clear();
   	
   }
   
   public String getTotalDebit() {
   		
   	  return totalDebit;
   	
   }
   
   public void setTotalDebit(String totalDebit) {
   	
   	  this.totalDebit = totalDebit;
   	
   }
   
   public String getTotalCredit() {
		
	  return totalCredit;
	
	}
	
	public void setTotalCredit(String totalCredit) {
		
		  this.totalCredit = totalCredit;
		
	}
   
   public String getReminderSchedule() {
   	
   	   return reminderSchedule;
   	
   }
   
   public void setReminderSchedule(String reminderSchedule) {
   	
   	   this.reminderSchedule = reminderSchedule;
   	
   }
   
   public ArrayList getReminderScheduleList() {
   	
   	   return reminderScheduleList;
   	
   }
   
   public String getNextRunDate() {
   	
   	   return nextRunDate;
   	
   }
   
   public void setNextRunDate(String nextRunDate) {
   	
   	   this.nextRunDate = nextRunDate;
   	
   }
   
   public String getLastRunDate() {
   	
   	   return lastRunDate;
   	
   }
   
   public void setLastRunDate(String lastRunDate) {
   	
   	   this.lastRunDate = lastRunDate;
   	
   }
   
   public String getNotifiedUser1() {
   	
   	  return notifiedUser1;
   	
   }
   
   public void setNotifiedUser1(String notifiedUser1) {
   	
   	  this.notifiedUser1 = notifiedUser1;
   	
   }
   
   public ArrayList getNotifiedUser1List() {
   	
   	   return notifiedUser1List;
   	
   }
   
   public void setNotifiedUser1List(String notifiedUser1) {
   	
   	   notifiedUser1List.add(notifiedUser1);
   	
   }
   
   public void clearNotifiedUser1List() {
   	
   	   notifiedUser1List.clear();
   	   notifiedUser1List.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public String getNotifiedUser2() {
   	
   	  return notifiedUser2;
   	
   }
   
   public void setNotifiedUser2(String notifiedUser2) {
   	
   	  this.notifiedUser2 = notifiedUser2;
   	
   }
   
   public ArrayList getNotifiedUser2List() {
   	
   	   return notifiedUser2List;
   	
   }
   
   public void setNotifiedUser2List(String notifiedUser2) {
   	
   	   notifiedUser2List.add(notifiedUser2);
   	
   }
   
   public void clearNotifiedUser2List() {
   	
   	   notifiedUser2List.clear();
   	   notifiedUser2List.add(Constants.GLOBAL_BLANK);
   	
   }   
   
   public String getNotifiedUser3() {
   	
   	  return notifiedUser3;
   	
   }
   
   public void setNotifiedUser3(String notifiedUser3) {
   	
   	  this.notifiedUser3 = notifiedUser3;
   	
   }
   
   public ArrayList getNotifiedUser3List() {
   	
   	   return notifiedUser3List;
   	
   }
   
   public void setNotifiedUser3List(String notifiedUser3) {
   	
   	   notifiedUser3List.add(notifiedUser3);
   	
   }
   
   public void clearNotifiedUser3List() {
   	
   	   notifiedUser3List.clear();
   	   notifiedUser3List.add(Constants.GLOBAL_BLANK);
   	
   } 
   
   
   public String getNotifiedUser4() {
   	
   	  return notifiedUser4;
   	
   }
   
   public void setNotifiedUser4(String notifiedUser4) {
   	
   	  this.notifiedUser4 = notifiedUser4;
   	
   }
   
   public ArrayList getNotifiedUser4List() {
   	
   	   return notifiedUser4List;
   	
   }
   
   public void setNotifiedUser4List(String notifiedUser4) {
   	
   	   notifiedUser4List.add(notifiedUser4);
   	
   }
   
   public void clearNotifiedUser4List() {
   	
   	   notifiedUser4List.clear();
   	   notifiedUser4List.add(Constants.GLOBAL_BLANK);
   	
   }  
   
   public String getNotifiedUser5() {
   	
   	  return notifiedUser5;
   	
   }
   
   public void setNotifiedUser5(String notifiedUser5) {
   	
   	  this.notifiedUser5 = notifiedUser5;
   	
   }
   
   public ArrayList getNotifiedUser5List() {
   	
   	   return notifiedUser5List;
   	
   }
   
   public void setNotifiedUser5List(String notifiedUser5) {
   	
   	   notifiedUser5List.add(notifiedUser5);
   	
   }
   
   public void clearNotifiedUser5List() {
   	
   	   notifiedUser5List.clear();
   	   notifiedUser5List.add(Constants.GLOBAL_BLANK);
   	
   }
     
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
   
   public boolean getShowAddLinesButton() {
   	
   	   return showAddLinesButton;
   	
   }
   
   public void setShowAddLinesButton(boolean showAddLinesButton) {
   	
   	   this.showAddLinesButton = showAddLinesButton;
   	
   }
   
   public boolean getShowDeleteLinesButton() {
   	
   	   return showDeleteLinesButton;
   	
   }
   
   public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
   	
   	   this.showDeleteLinesButton = showDeleteLinesButton;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
   
   public boolean getShowDeleteButton() {
   	
   	   return showDeleteButton;
   	
   }
   
   public void setShowDeleteButton(boolean showDeleteButton) {
   	
   	   this.showDeleteButton = showDeleteButton;
   	
   }  
   
   public String getBatchName() {
   	
   	return batchName;
   	
   }
   
   public void setBatchName(String batchName) {
   	
   	this.batchName = batchName;
   	
   }
   
   public boolean getShowBatchName() {
   	
   	   return showBatchName;
   	
   }
   
   public void setShowBatchName(boolean showBatchName) {
   	
   	   this.showBatchName = showBatchName;
   	
   }
     
   public ArrayList getBatchNameList() {
   	
   	  return batchNameList;
   	
   }
   
   public void setBatchNameList(String batchName) {
   	
   	  batchNameList.add(batchName);
   	
   }
   
   public void clearBatchNameList() {   	 
   	
   	  batchNameList.clear();
   	  batchNameList.add(Constants.GLOBAL_BLANK);
   	
   }
      
   public void reset(ActionMapping mapping, HttpServletRequest request){      
       
       name = null;
       description = null;      
       totalDebit = null;
       totalCredit = null;
       reminderScheduleList.clear();
       reminderScheduleList.add(Constants.GL_RJ_SCHEDULE_DAILY);
       reminderScheduleList.add(Constants.GL_RJ_SCHEDULE_WEEKLY);
       reminderScheduleList.add(Constants.GL_RJ_SCHEDULE_SEMI_MONTHLY);
       reminderScheduleList.add(Constants.GL_RJ_SCHEDULE_MONTHLY);
       reminderScheduleList.add(Constants.GL_RJ_SCHEDULE_QUARTERLY);
       reminderScheduleList.add(Constants.GL_RJ_SCHEDULE_SEMI_ANNUALLY);
       reminderScheduleList.add(Constants.GL_RJ_SCHEDULE_ANNUALLY);
       reminderSchedule = Constants.GL_RJ_SCHEDULE_MONTHLY;
       notifiedUser1 = Constants.GLOBAL_BLANK;
       notifiedUser2 = Constants.GLOBAL_BLANK;
       notifiedUser3 = Constants.GLOBAL_BLANK;
       notifiedUser4 = Constants.GLOBAL_BLANK;
       notifiedUser5 = Constants.GLOBAL_BLANK;
       nextRunDate = null;
       lastRunDate = null;
       batchName = null;
   
	   
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null){
      	
      	 if(Common.validateRequired(name)){
            errors.add("name",
               new ActionMessage("recurringJournalEntry.error.nameRequired"));
         }
         		          
         if(Common.validateRequired(category) || category.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("category",
               new ActionMessage("recurringJournalEntry.error.categoryRequired"));
         }
         
         if (Common.validateRequired(notifiedUser1)) {
         	errors.add("notifiedUser1",
               new ActionMessage("recurringJournalEntry.error.notifiedUser1Required"));
         	         	
         }
         
         if (Common.validateRequired(nextRunDate)) {
         	errors.add("nextRunDate",
               new ActionMessage("recurringJournalEntry.error.nextRunDateRequired"));
         	         	
         }
         
         if (!Common.validateDateFormat(nextRunDate)) {
         	errors.add("nextRunDate",
               new ActionMessage("recurringJournalEntry.error.nextRunDateInvalid"));
         } 
         
         if((Common.validateRequired(batchName) || batchName.equals(Constants.GLOBAL_NO_RECORD_FOUND)) &&
          	    showBatchName){
                errors.add("batchName",
                   new ActionMessage("recurringJournalEntry.error.batchNameRequired"));
         }
                 
         
         int numberOfLines = 0;
      	 
      	 Iterator i = glRJList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 GlRecurringJournalEntryList rjList = (GlRecurringJournalEntryList)i.next();      	 	 
      	 	       	 	 
      	 	 if (Common.validateRequired(rjList.getAccount()) &&
      	 	     Common.validateRequired(rjList.getDebitAmount()) &&
      	 	     Common.validateRequired(rjList.getCreditAmount())) continue;
      	 	     
      	 	 numberOfLines++;
      	 
	         if(Common.validateRequired(rjList.getAccount())){
	            errors.add("account",
	               new ActionMessage("recurringJournalEntry.error.accountRequired", rjList.getLineNumber()));
	         }
		 	 if(!Common.validateMoneyFormat(rjList.getDebitAmount())){
	            errors.add("debitAmount",
	               new ActionMessage("recurringJournalEntry.error.debitAmountInvalid", rjList.getLineNumber()));
	         }
	         if(!Common.validateMoneyFormat(rjList.getCreditAmount())){
	            errors.add("creditAmount",
	               new ActionMessage("recurringJournalEntry.error.creditAmountInvalid", rjList.getLineNumber()));
	         }
			 if(Common.validateRequired(rjList.getDebitAmount()) && Common.validateRequired(rjList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("recurringJournalEntry.error.debitCreditAmountRequired", rjList.getLineNumber()));
			 }
			 if(!Common.validateRequired(rjList.getDebitAmount()) && !Common.validateRequired(rjList.getCreditAmount())){
		            errors.add("journalLineAmounts",
		               new ActionMessage("recurringJournalEntry.error.debitCreditAmountMustBeNull", rjList.getLineNumber()));
		     }	    		     
		     
		          
	    }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("name",
               new ActionMessage("recurringJournalEntry.error.recurringJournalMustHaveLine"));
         	
         }	  	    
	    
	    	    	 
      }
      return(errors);	
   }
}
