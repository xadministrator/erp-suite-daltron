package com.struts.gl.recurringjournalgeneration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlJREffectiveDateViolationException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.GlRecurringJournalGenerationController;
import com.ejb.txn.GlRecurringJournalGenerationControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModRecurringJournalDetails;

public final class GlRecurringJournalGenerationAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlRecurringJournalGenerationAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlRecurringJournalGenerationForm actionForm = (GlRecurringJournalGenerationForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_RECURRING_JOURNAL_GENERATION_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glRecurringJournalGeneration");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlRecurringJournalGenerationController EJB
*******************************************************/

         GlRecurringJournalGenerationControllerHome homeRJ = null;
         GlRecurringJournalGenerationController ejbRJ = null;

         try {
          
            homeRJ = (GlRecurringJournalGenerationControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlRecurringJournalGenerationControllerEJB", GlRecurringJournalGenerationControllerHome.class);

         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlRecurringJournalGenerationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbRJ = homeRJ.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlRecurringJournalGenerationAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

         short precisionUnit = 0;
         
         try { 
         	
            precisionUnit = ejbRJ.getGlFcPrecisionUnit(user.getCmpCode());
            	                  
         } catch (EJBException ex) {
         	
            if (log.isInfoEnabled()) {
            	
               log.info("EJBException caught in GlRecurringJournalGenerationAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               
            }
            
            return(mapping.findForward("cmnErrorPage"));
         }	


/*******************************************************
   -- Gl RJ Previous Action --
*******************************************************/ 

         if(request.getParameter("previousButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() - Constants.GLOBAL_MAX_LINES);
         	
/*******************************************************
   -- Gl RJ Next Action --
*******************************************************/ 

         }else if(request.getParameter("nextButton") != null){
         	
         	actionForm.setLineCount(actionForm.getLineCount() + Constants.GLOBAL_MAX_LINES);
         	
         } 
         
/*******************************************************
   -- Gl RJ Go Action --
*******************************************************/

         if (request.getParameter("goButton") != null  || request.getParameter("nextButton") != null ||
            request.getParameter("previousButton") != null ||
            request.getParameter("forward") != null) {
            
             // create criteria 
            
            if (request.getParameter("goButton") != null) {
            	
	        	HashMap criteria = new HashMap();            	
                
	        	if (!Common.validateRequired(actionForm.getName())) {
	        		
	        		criteria.put("name", actionForm.getName());
	        		
	        	}
	        		        	
	        	if (!Common.validateRequired(actionForm.getNextRunDateFrom())) {
	        		
	        		criteria.put("nextRunDateFrom", Common.convertStringToSQLDate(actionForm.getNextRunDateFrom()));
	        		
	        	}
	        	
	        	if (!Common.validateRequired(actionForm.getNextRunDateTo())) {
	        		
	        		criteria.put("nextRunDateTo", Common.convertStringToSQLDate(actionForm.getNextRunDateTo()));
	        		
	        	}	        		        	
	        		        	
	        	if (!Common.validateRequired(actionForm.getCategory())) {
	        		
	        		criteria.put("category", actionForm.getCategory());
	        		
	        	}	        	
	        	      	
	        	
	        	// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	        	
	     	} else if (request.getParameter("forward") != null) {
	     		
	     		HashMap criteria = new HashMap();
	     		
	     		criteria.put("nextRunDateTo", Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
	     		
	     		// save criteria
	        	
	        	actionForm.setLineCount(0);
	        	actionForm.setCriteria(criteria);
	     		
	     	}
            
            try {
            	
            	actionForm.clearGlRJList();
            	
            	ArrayList list = ejbRJ.getGlRjByCriteria(actionForm.getCriteria(),
            	    actionForm.getOrderBy(),
            	    new Integer(actionForm.getLineCount()), 
            	    new Integer(Constants.GLOBAL_MAX_LINES + 1), 
            	    new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	// check if prev should be disabled
	           if (actionForm.getLineCount() == 0) {
	            	
	              actionForm.setDisablePreviousButton(true);
	            	
	           } else {
	           	
	           	  actionForm.setDisablePreviousButton(false);
	           	
	           }
	           
	           // check if next should be disabled
	           if (list.size() <= Constants.GLOBAL_MAX_LINES) {
	           	  
	           	  actionForm.setDisableNextButton(true);
	           	  
	           } else {
	           	  
	           	  actionForm.setDisableNextButton(false);
	           	  
	           	  //remove last record
	           	  list.remove(list.size() - 1);
	           	
	           }
            	
            	Iterator i = list.iterator();
            	String nullString = null;
            	
            	while (i.hasNext()) {
            		
            		GlModRecurringJournalDetails mdetails = (GlModRecurringJournalDetails)i.next();
            		
            		
            		GlRecurringJournalGenerationList rjList = new GlRecurringJournalGenerationList(
            			actionForm,
            			mdetails.getRjCode(),
            			mdetails.getRjName(),
            			Common.convertSQLDateToString(mdetails.getRjNewNextRunDate()),
            			nullString,
            			Common.convertSQLDateToString(mdetails.getRjNextRunDate()),
            			mdetails.getRjSchedule(),
            			Common.convertSQLDateToString(mdetails.getRjLastRunDate()),            			
            			Common.convertDoubleToStringMoney(mdetails.getRjTotalDebit(), precisionUnit),
            			Common.convertDoubleToStringMoney(mdetails.getRjTotalCredit(), precisionUnit));
            			
            	   actionForm.saveGlRJList(rjList);
            		
            	}

            } catch (GlobalNoRecordFoundException ex) {
               
               // disable prev next buttons
		       actionForm.setDisableNextButton(true);
               actionForm.setDisablePreviousButton(true);
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("recurringJournalGeneration.error.noRecordFound"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlRecurringJournalGenerationAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glRecurringJournalGeneration");

            }
                        
            actionForm.reset(mapping, request);            	                               
            
            return(mapping.findForward("glRecurringJournalGeneration")); 

/*******************************************************
   -- Gl RJ Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl RJ Generate Action --
*******************************************************/

         } else if (request.getParameter("generateButton") != null  &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            	

  	         // get posted journals
  	        
		    for(int i=0; i<actionForm.getGlRJListSize(); i++) {
		    
		       GlRecurringJournalGenerationList actionList = actionForm.getGlRJByIndex(i);
		    	
               if (actionList.getGenerate()) {
               	
               	     try {
               	     	
               	     	ejbRJ.executeGlRjGeneration(actionList.getRecurringJournalCode(), 
               	     	   Common.convertStringToSQLDate(actionList.getNextRunDate()), 
               	     	   actionList.getDocumentNumber(),
               	     	   Common.convertStringToSQLDate(actionList.getJournalDate()), user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());               	     	
               	     	
               	     	actionForm.deleteGlRJList(i);
               	        i--;
               	  
		             } catch (GlobalRecordAlreadyDeletedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("recurringJournalGeneration.error.recordAlreadyDeleted", actionList.getName()));
		             
		             } catch (GlJREffectiveDateNoPeriodExistException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("recurringJournalGeneration.error.journalDateNoPeriodExist", actionList.getName()));
		                  
		             } catch (GlobalDocumentNumberNotUniqueException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("recurringJournalGeneration.error.documentNumberNotUnique", actionList.getName()));
		             
		             } catch (GlJREffectiveDateViolationException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("recurringJournalGeneration.error.journalDateViolation", actionList.getName()));
		                  
		             } catch (GlJREffectiveDatePeriodClosedException ex) {
		               		               
		               errors.add(ActionMessages.GLOBAL_MESSAGE,
		                  new ActionMessage("recurringJournalGeneration.error.journalDatePeriodClosed", actionList.getName()));
		
		             } catch (EJBException ex) {
		
		               if (log.isInfoEnabled()) {
		
		                  log.info("EJBException caught in GlRecurringJournalGenerationAction.execute(): " + ex.getMessage() +
		                  " session: " + session.getId());
		                  return mapping.findForward("cmnErrorPage"); 
		                  
		               }
		
		            } 		                           
	            
	           }
	           
	       }		      
		
/*******************************************************
   -- Gl RJ Load Action --
*******************************************************/

         }
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glRecurringJournalGeneration");

            }
            
            actionForm.clearGlRJList();
            ArrayList list = null;
            Iterator i = null;
            
            try {
            	    
            	actionForm.clearCategoryList();           	
            	
            	list = ejbRJ.getGlJcAll(user.getCmpCode());
            	
            	if (list == null || list.size() == 0) {
            		
            		actionForm.setCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
            		
            	} else {
            		           		            		
            		i = list.iterator();
            		
            		while (i.hasNext()) {
            			
            		    actionForm.setCategoryList((String)i.next());
            			
            		}
            		
            	}            	
            	           	
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlBillEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }             
            
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("generateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }
            
            actionForm.reset(mapping, request);
            actionForm.setLineCount(0);
            actionForm.setDisableNextButton(true);
            actionForm.setDisablePreviousButton(true);
            	        
            return(mapping.findForward("glRecurringJournalGeneration"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if (log.isInfoEnabled()) {

             log.info("Exception caught in GlRecurringJournalGenerationAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }

          e.printStackTrace();

          return mapping.findForward("cmnErrorPage");

       }

    }
}