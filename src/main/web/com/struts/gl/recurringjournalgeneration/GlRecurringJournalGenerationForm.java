package com.struts.gl.recurringjournalgeneration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlRecurringJournalGenerationForm extends ActionForm implements Serializable {

   private String name = null;
   private String nextRunDateFrom = null;
   private String nextRunDateTo = null;
   private String category = null;
   private ArrayList categoryList = new ArrayList(); 
   private String orderBy = null;
   private ArrayList orderByList = new ArrayList(); 
    
   private ArrayList glRJList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private String nextButton = null;
   private String previousButton = null;
   
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   
   private int lineCount = 0;
   
   private HashMap criteria = new HashMap();

   public int getRowSelected(){
      return rowSelected;
   }
   
   public int getLineCount(){
   	  return lineCount;   	 
   }
   
   public void setLineCount(int lineCount){  	
   	  this.lineCount = lineCount;
   }

   public GlRecurringJournalGenerationList getGlRJByIndex(int index){
      return((GlRecurringJournalGenerationList)glRJList.get(index));
   }

   public Object[] getGlRJList(){
      return(glRJList.toArray());
   }

   public int getGlRJListSize(){
      return(glRJList.size());
   }

   public void saveGlRJList(Object newGlRJList){
      glRJList.add(newGlRJList);
   }

   public void clearGlRJList(){
      glRJList.clear();
   }

   public void setRowSelected(Object selectedGlRJList, boolean isEdit){
      this.rowSelected = glRJList.indexOf(selectedGlRJList);
   }

   public void updateGlRJRow(int rowSelected, Object newGlRJList){
      glRJList.set(rowSelected, newGlRJList);
   }

   public void deleteGlRJList(int rowSelected){
      glRJList.remove(rowSelected);
   }
   
   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getName(){
      return(name);
   }

   public void setName(String name){
      this.name = name;
   }
   
   public String getNextRunDateFrom() {
   	  return(nextRunDateFrom);	  
   }
   
   public void setNextRunDateFrom(String nextRunDateFrom) {
   	  this.nextRunDateFrom = nextRunDateFrom;
   }
   
   public String getNextRunDateTo() {
   	  return(nextRunDateTo);	  
   }
   
   public void setNextRunDateTo(String nextRunDateTo) {
   	  this.nextRunDateTo = nextRunDateTo;
   }   
      
   public String getCategory() {
   	  return(category);
   }
   
   public void setCategory(String category){
   	  this.category = category;
   }
   
   public ArrayList getCategoryList() {
   	  return(categoryList);
   }
   
   public void setCategoryList(String category){
   	  categoryList.add(category);
   }
   
   public void clearCategoryList(){
   	  categoryList.clear();
   	  categoryList.add(Constants.GLOBAL_BLANK);
   }   
      
   public String getOrderBy(){
   	  return(orderBy);
   }
   
   public void setOrderBy(String orderBy){
   	  this.orderBy = orderBy;
   }
   
   public ArrayList getOrderByList(){
   	  return(orderByList);
   }    
      
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	   return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	   this.criteria = criteria;
   	
   }
      
   public void reset(ActionMapping mapping, HttpServletRequest request){
      name = null;
      nextRunDateFrom = null;
      nextRunDateTo = null;
      category = Constants.GLOBAL_BLANK;      
      
      if (orderByList.isEmpty()) { 
      
	      orderByList.clear();
	      orderByList.add(Constants.GLOBAL_BLANK);
	      orderByList.add(Constants.GL_RJ_ORDER_BY_NAME);
	      orderByList.add(Constants.GL_RJ_ORDER_BY_NEXT_RUN_DATE);
	      orderByList.add(Constants.GL_RJ_ORDER_BY_CATEGORY);
	      orderBy = Constants.GLOBAL_BLANK;                  
	      
	  }
	  
      previousButton = null;
      nextButton = null;	
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
   	
       ActionErrors errors = new ActionErrors();
       
       if (request.getParameter("goButton") != null) {
      
	       if (!Common.validateDateFormat(nextRunDateFrom)) {
	
		     errors.add("nextRunDateFrom",
		        new ActionMessage("recurringJournalGeneration.error.nextRunDateFromInvalid"));
		
		  }         
		 
	 	  if (!Common.validateDateFormat(nextRunDateTo)) {
		
		     errors.add("nextRunDateTo",
		        new ActionMessage("recurringJournalGeneration.error.nextRunDateToInvalid"));
		
		  } 
		  
	   }  
	   
	   if (request.getParameter("generateButton") != null) {
	   
		   Iterator i = glRJList.iterator();      	 
	      	 
	      	 while (i.hasNext()) {
	      	 	
	      	 	 GlRecurringJournalGenerationList rjList = (GlRecurringJournalGenerationList)i.next();      	 	 
	      	 	       	 	 
	      	 	 if (!rjList.getGenerate()) continue;
	      	 	     	      	 
		         if(Common.validateRequired(rjList.getNextRunDate())){
		            errors.add("nextRunDate",
		               new ActionMessage("recurringJournalGeneration.error.nextRunDateRequired", rjList.getName()));
		         }
			 	 if(!Common.validateDateFormat(rjList.getNextRunDate())){
		            errors.add("nextRunDate",
		               new ActionMessage("recurringJournalGeneration.error.nextRunDateInvalid", rjList.getName()));
		         }  
		         if(Common.validateRequired(rjList.getJournalDate())){
		            errors.add("journalDate",
		               new ActionMessage("recurringJournalGeneration.error.journalDateRequired", rjList.getName()));
		         }
			 	 if(!Common.validateDateFormat(rjList.getJournalDate())){
		            errors.add("journalDate",
		               new ActionMessage("recurringJournalGeneration.error.journalDateInvalid", rjList.getName()));
		         } 		     
			     
			          
		    }               	 
		    
      }
			        
      return(errors);
   }
}
