package com.struts.gl.recurringjournalgeneration;

import java.io.Serializable;

public class GlRecurringJournalGenerationList implements Serializable {

   private Integer recurringJournalCode = null;
   private String name = null;
   private String nextRunDate = null;
   private String documentNumber = null;
   private String journalDate = null;
   private String reminderSchedule = null;
   private String lastRunDate = null;
   private String totalDebit = null;
   private String totalCredit = null;

   private boolean generate = false;
    
   private GlRecurringJournalGenerationForm parentBean;
    
   public GlRecurringJournalGenerationList(GlRecurringJournalGenerationForm parentBean,
      Integer recurringJournalCode,
      String name,
      String nextRunDate,
      String documentNumber,
      String journalDate,
      String reminderSchedule,
      String lastRunDate,
      String totalDebit,
      String totalCredit){

      this.parentBean = parentBean;
      this.recurringJournalCode = recurringJournalCode;
      this.name = name;
      this.nextRunDate = nextRunDate;
      this.documentNumber = documentNumber;
      this.journalDate = journalDate;
      this.reminderSchedule = reminderSchedule;
      this.lastRunDate = lastRunDate;
      this.totalDebit = totalDebit;
      this.totalCredit = totalCredit;
   }
   
   public Integer getRecurringJournalCode(){
      return(recurringJournalCode);
   }

   public String getName(){
      return(name);
   }

   public String getNextRunDate(){
      return(nextRunDate);
   }
   
   public void setNextRunDate(String nextRunDate) {
   	  this.nextRunDate = nextRunDate;   	
   }
   
   public String getDocumentNumber(){
      return(documentNumber);
   }
   
   public void setDocumentNumber(String documentNumber) {
   	  this.documentNumber = documentNumber;
   }

   public String getJournalDate(){
      return(journalDate);
   }
   
   public void setJournalDate(String journalDate) {
   	  this.journalDate = journalDate;
   }
   
   public String getReminderSchedule(){
      return(reminderSchedule);
   }
   
   public String getLastRunDate(){
   	  return(lastRunDate);
   }

   public String getTotalDebit(){
      return(totalDebit);
   }

   public String getTotalCredit(){
      return(totalCredit);
   }
   
   public boolean getGenerate() {
   	  return(generate);
   }

   public void setGenerate(boolean generate){
   	  this.generate = generate;   	
   }
   
}

