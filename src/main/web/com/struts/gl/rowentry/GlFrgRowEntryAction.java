package com.struts.gl.rowentry;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlFrgROWLineNumberAlreadyExistException;
import com.ejb.exception.GlFrgROWRowNameAlreadyExistException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.GlFrgRowEntryController;
import com.ejb.txn.GlFrgRowEntryControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFrgRowDetails;
import com.util.GlFrgRowSetDetails;

public final class GlFrgRowEntryAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFrgRowEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFrgRowEntryForm actionForm = (GlFrgRowEntryForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FRG_ROW_ENTRY_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFrgRowEntry");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFrgRowEntryController EJB
*******************************************************/

         GlFrgRowEntryControllerHome homeRE = null;
         GlFrgRowEntryController ejbRE = null;

         try {

            homeRE = (GlFrgRowEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFrgRowEntryControllerEJB", GlFrgRowEntryControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFrgRowEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbRE = homeRE.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFrgRowEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl DS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFrgRowEntry"));
	     
/*******************************************************
   -- Gl DS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFrgRowEntry"));                  
         
/*******************************************************
   -- Gl Frg RE Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlFrgRowDetails details = new GlFrgRowDetails();
            details.setRowLineNumber(Common.convertStringToInt(actionForm.getLineNumber()));
            details.setRowName(actionForm.getName());
            details.setRowIndent(Common.convertStringToInt(actionForm.getIndent()));
            details.setRowLineToSkipBefore(Common.convertStringToInt(actionForm.getLineToSkipBefore()));
            details.setRowLineToSkipAfter(Common.convertStringToInt(actionForm.getLineToSkipAfter()));
            details.setRowUnderlineCharacterBefore(Common.convertStringToInt(actionForm.getUnderlineCharacterBefore()));
            details.setRowUnderlineCharacterAfter(Common.convertStringToInt(actionForm.getUnderlineCharacterAfter()));
            details.setRowPageBreakBefore(Common.convertStringToInt(actionForm.getPageBreakBefore()));
            details.setRowPageBreakAfter(Common.convertStringToInt(actionForm.getPageBreakAfter()));
            details.setRowOverrideColumnCalculation(Common.convertBooleanToByte(actionForm.getOverrideColumnCalculation()));
            details.setRowHideRow(Common.convertBooleanToByte(actionForm.getHideRow()));
            details.setRowFontStyle(actionForm.getFontStyle());
            details.setRowHorizontalAlign(actionForm.getHorizontalAlign());
            
            try {
            	
            	ejbRE.addGlFrgRowEntry(details, actionForm.getRowSetCode(), user.getCmpCode());
                
            } catch (GlFrgROWRowNameAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("rowEntry.error.rowNameAlreadyExist"));

            } catch (GlFrgROWLineNumberAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("rowEntry.error.lineNumberAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgRowEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl Frg RE Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl Frg RE Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            GlFrgRowEntryList glREList =
	            actionForm.getGlREByIndex(actionForm.getRowSelected());
                        
            GlFrgRowDetails details = new GlFrgRowDetails();
            details.setRowCode(glREList.getRowCode());
            details.setRowLineNumber(Common.convertStringToInt(actionForm.getLineNumber()));
            details.setRowName(actionForm.getName());
            details.setRowIndent(Common.convertStringToInt(actionForm.getIndent()));
            details.setRowLineToSkipBefore(Common.convertStringToInt(actionForm.getLineToSkipBefore()));
            details.setRowLineToSkipAfter(Common.convertStringToInt(actionForm.getLineToSkipAfter()));
            details.setRowUnderlineCharacterBefore(Common.convertStringToInt(actionForm.getUnderlineCharacterBefore()));
            details.setRowUnderlineCharacterAfter(Common.convertStringToInt(actionForm.getUnderlineCharacterAfter()));
            details.setRowPageBreakBefore(Common.convertStringToInt(actionForm.getPageBreakBefore()));
            details.setRowPageBreakAfter(Common.convertStringToInt(actionForm.getPageBreakAfter()));
            details.setRowOverrideColumnCalculation(Common.convertBooleanToByte(actionForm.getOverrideColumnCalculation()));
            details.setRowHideRow(Common.convertBooleanToByte(actionForm.getHideRow()));
            details.setRowFontStyle(actionForm.getFontStyle());
            details.setRowHorizontalAlign(actionForm.getHorizontalAlign());
            
            try {
            	
            	ejbRE.updateGlFrgRowEntry(details, actionForm.getRowSetCode(), user.getCmpCode());

            } catch (GlFrgROWRowNameAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("rowEntry.error.rowNameAlreadyExist"));

            } catch (GlFrgROWLineNumberAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("rowEntry.error.lineNumberAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgRowEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl Frg RE Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl Frg RE Edit Action --
*******************************************************/

         } else if (request.getParameter("glREList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlRERow(actionForm.getRowSelected());
            
            return mapping.findForward("glFrgRowEntry");  
            
/*******************************************************
   -- Gl FRG Account Action --
*******************************************************/

         } else if (request.getParameter("glREList[" +
            actionForm.getRowSelected() + "].accountButton") != null){
            
            GlFrgRowEntryList glREList =
	            actionForm.getGlREByIndex(actionForm.getRowSelected());

	        String path = "/glFrgAccountAssignment.do?forward=1" +
	           "&rowCode=" + glREList.getRowCode() +
	           "&rowLineNumber=" +  glREList.getLineNumber() + 
	           "&rowName=" +  glREList.getName() + 
	           "&rowIndent=" + glREList.getIndent() +
	           "&rowLineToSkipBefore=" + glREList.getLineToSkipBefore() +
	           "&rowLineToSkipAfter=" + glREList.getLineToSkipAfter() +
	           "&rowUnderlineCharacterBefore=" + glREList.getUnderlineCharacterBefore() +
	           "&rowUnderlineCharacterAfter=" + glREList.getUnderlineCharacterAfter() +
	           "&rowPageBreakBefore=" + glREList.getPageBreakBefore() +
	           "&rowPageBreakAfter=" + glREList.getPageBreakAfter() +
	           "&rowOverrideColumnCalculation=" + glREList.getOverrideColumnCalculation() +
	           "&rowHideRow=" + glREList.getHideRow() +
	           "&rowFontStyle=" + glREList.getFontStyle() +
	           "&rowHorizontalAlign=" + glREList.getHorizontalAlign();
	        
	        return(new ActionForward(path)); 
	        
/*******************************************************
   -- Gl FRG Calculation1 Action --
*******************************************************/

         } else if (request.getParameter("glREList[" +
            actionForm.getRowSelected() + "].calculationButton1") != null){
            
            GlFrgRowEntryList glREList =
	            actionForm.getGlREByIndex(actionForm.getRowSelected());

	        String path = "/glFrgCalculation.do?forwardrowentry=1" +
	           "&rowCode=" + glREList.getRowCode() +
	           "&rowLineNumber=" +  glREList.getLineNumber() + 
	           "&rowName=" +  glREList.getName() + 
	           "&rowIndent=" + glREList.getIndent() +
	           "&rowLineToSkipBefore=" + glREList.getLineToSkipBefore() +
	           "&rowLineToSkipAfter=" + glREList.getLineToSkipAfter() +
	           "&rowUnderlineCharacterBefore=" + glREList.getUnderlineCharacterBefore() +
	           "&rowUnderlineCharacterAfter=" + glREList.getUnderlineCharacterAfter() +
	           "&rowPageBreakBefore=" + glREList.getPageBreakBefore() +
	           "&rowPageBreakAfter=" + glREList.getPageBreakAfter() +
	           "&rowOverrideColumnCalculation=" + glREList.getOverrideColumnCalculation() + 
	           "&rowHideRow=" + glREList.getHideRow() +
	           "&rowFontStyle=" + glREList.getFontStyle() + 
	           "&rowSetCode=" + actionForm.getRowSetCode() +
	           "&rowHorizontalAlign=" + glREList.getHorizontalAlign() +
	           "&calcType=C1"; 
	        
	        return(new ActionForward(path));	                           

/*******************************************************
	-- Gl FRG Calculation2 Action --
*******************************************************/

          } else if (request.getParameter("glREList[" +
             actionForm.getRowSelected() + "].calculationButton2") != null){
        	  
        	  System.out.println("Calculation2 Action");
             
             GlFrgRowEntryList glREList =
 	            actionForm.getGlREByIndex(actionForm.getRowSelected());

 	        String path = "/glFrgCalculation.do?forwardrowentry=1" +
 	           "&rowCode=" + glREList.getRowCode() +
 	           "&rowLineNumber=" +  glREList.getLineNumber() + 
 	           "&rowName=" +  glREList.getName() + 
 	           "&rowIndent=" + glREList.getIndent() +
 	           "&rowLineToSkipBefore=" + glREList.getLineToSkipBefore() +
 	           "&rowLineToSkipAfter=" + glREList.getLineToSkipAfter() +
 	           "&rowUnderlineCharacterBefore=" + glREList.getUnderlineCharacterBefore() +
 	           "&rowUnderlineCharacterAfter=" + glREList.getUnderlineCharacterAfter() +
 	           "&rowPageBreakBefore=" + glREList.getPageBreakBefore() +
 	           "&rowPageBreakAfter=" + glREList.getPageBreakAfter() +
 	           "&rowOverrideColumnCalculation=" + glREList.getOverrideColumnCalculation() +
 	           "&rowHideRow=" + glREList.getHideRow() +
 	           "&rowFontStyle=" + glREList.getFontStyle() + 
 	           "&rowSetCode=" + actionForm.getRowSetCode() +
 	           "&rowHorizontalAlign=" + glREList.getHorizontalAlign() +
 	           "&calcType=C2"; 
 	        
 	        return(new ActionForward(path));	                           
                 
	        
/*******************************************************
   -- Gl Frg RE Delete Action --
*******************************************************/

         } else if (request.getParameter("glREList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlFrgRowEntryList glREList =
	            actionForm.getGlREByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbRE.deleteGlFrgRowEntry(glREList.getRowCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("rowEntry.error.rowAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlFrgRowEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            

/******************************************************
   -- Gl AA Row Set Action --
*******************************************************/

         } else if (request.getParameter("rowSetButton") != null) {
         	        	
         	 String path = "/glFrgRowSet.do"; 
			     
             return(new ActionForward(path));
            
/*******************************************************
   -- Gl Frg RE Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFrgRowEntry");

            }
                                  	                        	                          	            	
            GlFrgRowSetDetails rsDetails = null;

	        if (request.getParameter("forward") != null) {

	            rsDetails = ejbRE.getGlFrgRowSetByRowSetCode(new Integer(request.getParameter("rowSetCode")), user.getCmpCode());        		
	    	    actionForm.setRowSetCode(new Integer(request.getParameter("rowSetCode")));

	        } else {
	     		
	            rsDetails = ejbRE.getGlFrgRowSetByRowSetCode(actionForm.getRowSetCode(), user.getCmpCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setRowSetCode(actionForm.getRowSetCode());
            actionForm.setRowSetName(rsDetails.getRsName());
            actionForm.setRowSetDescription(rsDetails.getRsDescription());                        	

            ArrayList list = null;
            Iterator i = null;
            
            try {
            	
            	actionForm.clearGlREList();
            	
            	list = ejbRE.getGlFrgRowByRowSetCode(actionForm.getRowSetCode(), user.getCmpCode()); 
            	
            	i = list.iterator();
            	
            	while(i.hasNext()) {
            		
            		GlFrgRowDetails details = (GlFrgRowDetails)i.next();
            		
            		GlFrgRowEntryList glREList = new GlFrgRowEntryList(actionForm,
            				details.getRowCode(),
							String.valueOf(details.getRowLineNumber()),
							details.getRowName(),
							String.valueOf(details.getRowIndent()),
							String.valueOf(details.getRowLineToSkipBefore()),
							String.valueOf(details.getRowLineToSkipAfter()),
							String.valueOf(details.getRowUnderlineCharacterBefore()),
							String.valueOf(details.getRowUnderlineCharacterAfter()),
							String.valueOf(details.getRowPageBreakBefore()),
							String.valueOf(details.getRowPageBreakAfter()),
							Common.convertByteToBoolean(details.getRowOverrideColumnCalculation()),
							Common.convertByteToBoolean(details.getRowHideRow()),
							details.getRowFontStyle(),
							details.getRowHorizontalAlign());
            		
            		actionForm.saveGlREList(glREList);
            		
            	}
            	
            } catch (GlobalNoRecordFoundException ex) {
	        	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgRowEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glFrgRowEntry"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFrgRowEntryAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}