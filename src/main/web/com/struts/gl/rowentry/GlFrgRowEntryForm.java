package com.struts.gl.rowentry;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFrgRowEntryForm extends ActionForm implements Serializable {

   private Integer rowSetCode = null;
   private String rowSetName = null;
   private String rowSetDescription = null;
   
   private String lineNumber = null;
   private String name = null;
   private String indent = null;
   private String lineToSkipBefore = null;
   private String lineToSkipAfter = null;
   private String underlineCharacterBefore = null;
   private String underlineCharacterAfter = null;
   private String pageBreakBefore = null;
   private String pageBreakAfter = null;
   private boolean overrideColumnCalculation = false;
   private String fontStyle = null;
   private ArrayList fontStyleList = new ArrayList();
   private String tableType = null;
   private String horizontalAlign = null;
   private ArrayList horizontalAlignList = new ArrayList();
   
   private boolean hideRow = false;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	 
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glREList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlFrgRowEntryList getGlREByIndex(int index) {

      return((GlFrgRowEntryList)glREList.get(index));

   }

   public Object[] getGlREList() {

      return glREList.toArray();

   }

   public int getGlREListSize() {

      return glREList.size();

   }

   public void saveGlREList(Object newGlREList) {

      glREList.add(newGlREList);

   }

   public void clearGlREList() {
      glREList.clear();
   }

   public void setRowSelected(Object selectedGlREList, boolean isEdit) {

      this.rowSelected = glREList.indexOf(selectedGlREList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlRERow(int rowSelected) {
   	
       this.lineNumber = ((GlFrgRowEntryList)glREList.get(rowSelected)).getLineNumber();
       this.name = ((GlFrgRowEntryList)glREList.get(rowSelected)).getName();
       this.indent = ((GlFrgRowEntryList)glREList.get(rowSelected)).getIndent();
       this.lineToSkipBefore = ((GlFrgRowEntryList)glREList.get(rowSelected)).getLineToSkipBefore();
       this.lineToSkipAfter = ((GlFrgRowEntryList)glREList.get(rowSelected)).getLineToSkipAfter();
       this.underlineCharacterBefore = ((GlFrgRowEntryList)glREList.get(rowSelected)).getUnderlineCharacterBefore();
       this.underlineCharacterAfter = ((GlFrgRowEntryList)glREList.get(rowSelected)).getUnderlineCharacterAfter();
       this.pageBreakBefore = ((GlFrgRowEntryList)glREList.get(rowSelected)).getPageBreakBefore();
       this.pageBreakAfter = ((GlFrgRowEntryList)glREList.get(rowSelected)).getPageBreakAfter();
       this.overrideColumnCalculation = ((GlFrgRowEntryList)glREList.get(rowSelected)).getOverrideColumnCalculation();
       this.hideRow = ((GlFrgRowEntryList)glREList.get(rowSelected)).getHideRow();
       this.fontStyle = ((GlFrgRowEntryList)glREList.get(rowSelected)).getFontStyle();
       this.horizontalAlign = ((GlFrgRowEntryList)glREList.get(rowSelected)).getHorizontalAlign();
       
   }

   public void updateGlRERow(int rowSelected, Object newGlREList) {

      glREList.set(rowSelected, newGlREList);

   }

   public void deleteGlREList(int rowSelected) {

      glREList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getRowSetCode() {
   	
   	  return rowSetCode;
   	
   }
   
   public void setRowSetCode(Integer rowSetCode) {
   	
   	  this.rowSetCode = rowSetCode;
   	
   }

   public String getRowSetName() {

      return rowSetName;

   }

   public void setRowSetName(String rowSetName) {

      this.rowSetName = rowSetName;

   }
   
   public String getRowSetDescription() {

      return rowSetDescription;

   }

   public void setRowSetDescription(String rowSetDescription) {

      this.rowSetDescription = rowSetDescription;

   }   

   public String getLineNumber() {

      return lineNumber;

   }

   public void setLineNumber(String lineNumber) {

      this.lineNumber = lineNumber;

   }
   
   public String getName() {

      return name;

   }

   public void setName(String name) {

      this.name = name;

   }
    
   public String getIndent() {

      return indent;

   }

   public void setIndent(String indent) {

      this.indent = indent;

   }   
   
   public String getLineToSkipBefore() {

      return lineToSkipBefore;

   }

   public void setLineToSkipBefore(String lineToSkipBefore) {

      this.lineToSkipBefore = lineToSkipBefore;

   }
   
   public String getLineToSkipAfter() {

      return lineToSkipAfter;

   }

   public void setLineToSkipAfter(String lineToSkipAfter) {

      this.lineToSkipAfter = lineToSkipAfter;

   }   

   public String getUnderlineCharacterBefore() {

      return underlineCharacterBefore;

   }

   public void setUnderlineCharacterBefore(String underlineCharacterBefore) {

      this.underlineCharacterBefore = underlineCharacterBefore;

   }

   public String getUnderlineCharacterAfter() {

      return underlineCharacterAfter;

   }

   public void setUnderlineCharacterAfter(String underlineCharacterAfter) {

      this.underlineCharacterAfter = underlineCharacterAfter;

   }


   public String getPageBreakBefore() {

      return pageBreakBefore;

   }

   public void setPageBreakBefore(String pageBreakBefore) {
   	
   	  this.pageBreakBefore = pageBreakBefore;

   }

   public String getPageBreakAfter() {

      return pageBreakAfter;

   }

   public void setPageBreakAfter(String pageBreakAfter) {
   	
   	  this.pageBreakAfter = pageBreakAfter;

   }
   
   public boolean getOverrideColumnCalculation() {

      return overrideColumnCalculation;

   }

   public void setOverrideColumnCalculation(boolean overrideColumnCalculation) {
   	
   	  this.overrideColumnCalculation = overrideColumnCalculation;

   }   

   public String getFontStyle() {

      return fontStyle;

   }

   public void setFontStyle(String fontStyle) {

      this.fontStyle = fontStyle;

   }

   public ArrayList getFontStyleList() {
   	
   	   return fontStyleList;
   	
   } 
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public String getHorizontalAlign() {
   	
   	  return horizontalAlign;
   	  
   }
   
   public void setHorizontalAlign(String horizontalAlign) {
   	
   	  this.horizontalAlign = horizontalAlign;
   	  
   }
   
   public ArrayList getHorizontalAlignList() {
   	
   	   return horizontalAlignList;
   	   
   }
   
   public void setHorizontalAlignList(String horizontalAlign) {
   	
   	   horizontalAlignList.add(horizontalAlign);
   	
   }
   
   public void clearHorizontalAlignList() {
   	
   	   horizontalAlignList.clear();
   	   horizontalAlignList.add(Constants.GLOBAL_BLANK);
   	   
   }
   
   public boolean getHideRow() {

      return hideRow;

   }

   public void setHideRow(boolean hideRow) {
   	
   	  this.hideRow = hideRow;

   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

		lineNumber = null;  
		name = null;
		indent = null;
		lineToSkipBefore = null;
		lineToSkipAfter = null;
		underlineCharacterBefore = null;
		underlineCharacterAfter = null;
		pageBreakBefore = null;
		pageBreakAfter = null;
		overrideColumnCalculation = false;
		fontStyleList.clear();
		fontStyleList.add(Constants.GL_FRG_RE_FONT_BOLD);
		fontStyleList.add(Constants.GL_FRG_RE_FONT_ITALIC);
		fontStyleList.add(Constants.GL_FRG_RE_FONT_NORMAL);
		fontStyle = Constants.GL_FRG_RE_FONT_NORMAL;
		saveButton = null;
		updateButton = null;
		cancelButton = null;
		closeButton = null;
	    showDetailsButton = null;
	    hideDetailsButton = null;
	    horizontalAlign = "Left";
	    horizontalAlignList.clear();
	    horizontalAlignList.add("Left");
	    horizontalAlignList.add("Center");
	    horizontalAlignList.add("Right");
	    horizontalAlignList.add("Justified");
	    hideRow = false;
	    
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(lineNumber)) {

            errors.add("lineNumber",
               new ActionMessage("rowEntry.error.lineNumberRequired"));

         }

         if (!Common.validateNumberFormat(lineNumber)) {

            errors.add("lineNumber",
               new ActionMessage("rowEntry.error.lineNumberInvalid"));

         }                    

         if (Common.validateRequired(name)) {

            errors.add("name",
               new ActionMessage("rowEntry.error.nameRequired"));

         }
         
         if (Common.validateRequired(fontStyle)) {

            errors.add("fontStyle",
               new ActionMessage("rowEntry.error.fontStyleRequired"));

         } 
         
         if (!Common.validateNumberFormat(indent)) {

            errors.add("indent",
               new ActionMessage("rowEntry.error.indentInvalid"));

         }   
         
         if (!Common.validateNumberFormat(lineToSkipBefore)) {

            errors.add("lineToSkipBefore",
               new ActionMessage("rowEntry.error.lineToSkipBeforeInvalid"));

         }   
         
         if (!Common.validateNumberFormat(lineToSkipAfter)) {

            errors.add("lineToSkipAfter",
               new ActionMessage("rowEntry.error.lineToSkipAfterInvalid"));

         } 
         
         if (!Common.validateNumberFormat(underlineCharacterBefore)) {

            errors.add("underlineCharacterBefore",
               new ActionMessage("rowEntry.error.underlineCharacterBeforeInvalid"));

         }      
         
         if (!Common.validateNumberFormat(underlineCharacterAfter)) {

            errors.add("underlineCharacterAfter",
               new ActionMessage("rowEntry.error.underlineCharacterAfterInvalid"));

         }    
         
         if (!Common.validateNumberFormat(pageBreakBefore)) {

            errors.add("pageBreakBefore",
               new ActionMessage("rowEntry.error.pageBreakBeforeInvalid"));

         }      
         
         if (!Common.validateNumberFormat(pageBreakAfter)) {

            errors.add("pageBreakAfter",
               new ActionMessage("rowEntry.error.pageBreakAfterInvalid"));

         }                        
                                        
         
		 if(!Common.validateStringNumLessThanEqual(indent, 100L)) {
		 	
		    errors.add("indent", 
		       new ActionMessage("rowEntry.error.indentGreaterThanAllowedIndent"));
		 
		 }         
         
         
		 if(!Common.validateStringNumLessThanEqual(lineToSkipBefore, 50L)) {
		 	
		    errors.add("lineToSkipBefore", 
		       new ActionMessage("rowEntry.error.lineToSkipBeforeGreaterThanAllowedLine"));
		 
		 }  
		 
		 if(!Common.validateStringNumLessThanEqual(lineToSkipAfter, 50L)) {
		 	
		    errors.add("lineToSkipAfter", 
		       new ActionMessage("rowEntry.error.lineToSkipAfterGreaterThanAllowedLine"));
		 
		 } 
		 
		 if(!Common.validateStringNumLessThanEqual(underlineCharacterBefore, 4L)) {
		 	
		    errors.add("underlineCharacterBefore", 
		       new ActionMessage("rowEntry.error.underlineCharacterBeforeGreaterThanAllowedUnderLine"));
		 
		 } 
		 
		 if(!Common.validateStringNumLessThanEqual(underlineCharacterAfter, 4L)) {
		 	
		    errors.add("underlineCharacterAfter", 
		       new ActionMessage("rowEntry.error.underlineCharacterAfterGreaterThanAllowedUnderLine"));
		 
		 } 	
		 
		 if(!Common.validateStringNumLessThanEqual(pageBreakBefore, 4L)) {
		 	
		    errors.add("pageBreakBefore", 
		       new ActionMessage("rowEntry.error.pageBreakBeforeGreaterThanAllowedPageBreak"));
		 
		 }

		 if(!Common.validateStringNumLessThanEqual(pageBreakAfter, 4L)) {
		 	
		    errors.add("pageBreakAfter", 
		       new ActionMessage("rowEntry.error.pageBreakAfterGreaterThanAllowedPageBreak"));
		 
		 }		  		 	 		 		                                                   

      }
         
      return errors;

   }
}