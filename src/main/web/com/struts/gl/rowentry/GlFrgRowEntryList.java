package com.struts.gl.rowentry;

import java.io.Serializable;

public class GlFrgRowEntryList implements Serializable {

   private Integer rowCode = null;
   private String lineNumber = null;
   private String name = null;
   private String indent = null;
   private String lineToSkipBefore = null;
   private String lineToSkipAfter = null;
   private String underlineCharacterBefore = null;
   private String underlineCharacterAfter = null;
   private String pageBreakBefore = null;
   private String pageBreakAfter = null;
   private boolean overrideColumnCalculation = false;
   private boolean hideRow = false;
   private String fontStyle = null;
   private String horizontalAlign = null;

   private String editButton = null;
   private String deleteButton = null;
   private String accountButton = null;
   private String calculationButton1 = null;
   private String calculationButton2 = null;
    
   private GlFrgRowEntryForm parentBean;
    
   public GlFrgRowEntryList(GlFrgRowEntryForm parentBean,
		Integer rowCode,
		String lineNumber,
		String name,
		String indent,
		String lineToSkipBefore,
		String lineToSkipAfter,
		String underlineCharacterBefore,
		String underlineCharacterAfter,
		String pageBreakBefore,
		String pageBreakAfter,
		boolean overrideColumnCalculation,
		boolean hideRow,
		String fontStyle,
		String horizontalAlign) {
		
		this.parentBean = parentBean;
		this.rowCode = rowCode;
        this.lineNumber = lineNumber;
        this.name = name;
        this.indent = indent;
        this.lineToSkipBefore = lineToSkipBefore;
        this.lineToSkipAfter = lineToSkipAfter;
        this.underlineCharacterBefore = underlineCharacterBefore;
        this.underlineCharacterAfter = underlineCharacterAfter;
        this.pageBreakBefore = pageBreakBefore;
        this.pageBreakAfter = pageBreakAfter;
        this.overrideColumnCalculation = overrideColumnCalculation;
        this.hideRow = hideRow;
        this.fontStyle = fontStyle;
        this.horizontalAlign = horizontalAlign;
        
   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   

   public void setAccountButton(String accountButton) {

      parentBean.setRowSelected(this, true);

   }    

   public void setCalculationButton1(String calculationButton1) {

      parentBean.setRowSelected(this, true);

   } 

   public void setCalculationButton2(String calculationButton2) {

      parentBean.setRowSelected(this, true);

   } 

   public Integer getRowCode() {

      return rowCode;

   }
   
   public String getLineNumber() {
       
       return lineNumber;
       
   }

   public String getName() {

      return name;

   }

   public String getIndent() {

      return indent;

   }
   
   public String getLineToSkipBefore() {
   	
   	  return lineToSkipBefore;
   	  
   }
   
   public String getLineToSkipAfter() {
   	
   	  return lineToSkipAfter;
   	  
   }
   
   public String getUnderlineCharacterBefore() {
   	
   	  return underlineCharacterBefore;
   	  
   }
   
   public String getUnderlineCharacterAfter() {
   	
   	  return underlineCharacterAfter;
   	  
   }
   
   public String getPageBreakBefore() {
   	
   	  return pageBreakBefore;
   	  
   }
   
   public String getPageBreakAfter() {
   	
   	  return pageBreakAfter;
   	  
   } 
   
   public boolean getOverrideColumnCalculation() {
   	
   	  return overrideColumnCalculation;
   	  
   } 
   
   public boolean getHideRow() {
	   
	  return hideRow;
	   
   }
   
   public String getFontStyle() {
   	
   	  return fontStyle;
   	  
   } 
   
   public String getHorizontalAlign() {
   	
   	  return horizontalAlign;
   	  
   }

}