package com.struts.gl.rowset;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlFrgRowSetController;
import com.ejb.txn.GlFrgRowSetControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlFrgRowSetDetails;

public final class GlFrgRowSetAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlFrgRowSetAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlFrgRowSetForm actionForm = (GlFrgRowSetForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_FRG_ROW_SET_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glFrgRowSet");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlFrgRowSetController EJB
*******************************************************/

         GlFrgRowSetControllerHome homeRS = null;
         GlFrgRowSetController ejbRS = null;

         try {

            homeRS = (GlFrgRowSetControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlFrgRowSetControllerEJB", GlFrgRowSetControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlFrgRowSetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbRS = homeRS.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlFrgRowSetAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl RS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glFrgRowSet"));
	     
/*******************************************************
   -- Gl RS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glFrgRowSet"));                  
         
/*******************************************************
   -- Gl RS Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            GlFrgRowSetDetails details = new GlFrgRowSetDetails();
            details.setRsName(actionForm.getRowSetName());
            details.setRsDescription(actionForm.getRowSetDescription());            
            
            try {
            	
            	ejbRS.addGlFrgRsEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("rowSet.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgRowSetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }

/*******************************************************
   -- Gl RS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl RS Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
            
            GlFrgRowSetList glRSList =
	            actionForm.getGlRSByIndex(actionForm.getRowSelected());
                        
            GlFrgRowSetDetails details = new GlFrgRowSetDetails();
            details.setRsCode(glRSList.getRowSetCode());
            details.setRsName(actionForm.getRowSetName());
            details.setRsDescription(actionForm.getRowSetDescription());            
            
            try {
            	
            	ejbRS.updateGlFrgRsEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("rowSet.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlFrgRowSetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
   -- Gl RS Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Gl RS Edit Action --
*******************************************************/

         } else if (request.getParameter("glRSList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlRSRow(actionForm.getRowSelected());
            
            return mapping.findForward("glFrgRowSet");
            
/*******************************************************
   -- Gl FRG Rows Action --
*******************************************************/

         } else if (request.getParameter("glRSList[" +
            actionForm.getRowSelected() + "].rowsButton") != null){
            
            GlFrgRowSetList glRSList =
	            actionForm.getGlRSByIndex(actionForm.getRowSelected());

	        String path = "/glFrgRowEntry.do?forward=1" +
	           "&rowSetCode=" + glRSList.getRowSetCode() + 
	           "&rowSetName=" +  glRSList.getRowSetName() + 
	           "&rowSetDescription=" + glRSList.getRowSetDescription(); 
	        
	        return(new ActionForward(path));            

/*******************************************************
   -- Gl FRG Copy Action --
*******************************************************/

         } else if (request.getParameter("glRSList[" +
                 actionForm.getRowSelected() + "].copyButton") != null &&
                 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)){
        	 
	 	     try {
	 	    	 
	        	 GlFrgRowSetList glRSList =
	        		 actionForm.getGlRSByIndex(actionForm.getRowSelected());
	        	 GlFrgRowSetDetails details = new GlFrgRowSetDetails();
	        	 
	        	 details.setRsCode(glRSList.getRowSetCode());
	        	 details.setRsName(glRSList.getRowSetName());
	        	 details.setRsDescription(glRSList.getRowSetDescription());
	        	 
	        	 ejbRS.copyGlFrgRsEntry(details, user.getCmpCode());
	             
	                 
 	         } catch (EJBException ex) {
	 	        	 
 	        	 if (log.isInfoEnabled()) {
	 	        		 
 	        		 log.info("EJBException caught in GlFrgRowSetAction.execute(): " + ex.getMessage() +
 	        				 " session: " + session.getId());
	               
 	        		 return mapping.findForward("cmnErrorPage");
	 	        		 
 	        	 }
	 	        	 
 	         }
        	 
/*******************************************************
   -- Gl RS Delete Action --
*******************************************************/

         } else if (request.getParameter("glRSList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlFrgRowSetList glRSList =
	            actionForm.getGlRSByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbRS.deleteGlFrgRsEntry(glRSList.getRowSetCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("rowSet.error.deleteRowSetAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("rowSet.error.rowSetlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlFrgRowSetAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
            
/*******************************************************
   -- Gl RS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glFrgRowSet");

            }
            
            ArrayList list = null;
            Iterator i = null;

            actionForm.clearGlRSList(); 
                      	                        	                          	            	
	        try {	        
	           
	           list = ejbRS.getGlFrgRsAll(user.getCmpCode()); 
	           
	           i = list.iterator();
	            
	           while(i.hasNext()) {
    	            			            	
	              GlFrgRowSetDetails details = (GlFrgRowSetDetails)i.next();
	            	
	              GlFrgRowSetList glRSList = new GlFrgRowSetList(actionForm,
	            	    details.getRsCode(),
	            	    details.getRsName(),
	            	    details.getRsDescription());
	            	 	            	    
	              actionForm.saveGlRSList(glRSList);
	            	
	           }
	            
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {
            
           if (log.isInfoEnabled()) {

              log.info("EJBException caught in GlFrgRowSetAction.execute(): " + ex.getMessage() +
              " session: " + session.getId());
              return mapping.findForward("cmnErrorPage"); 
              
           }

        }
                  

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glFrgRowSet"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlFrgRowSetAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}