package com.struts.gl.rowset;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlFrgRowSetForm extends ActionForm implements Serializable {

   private Integer rowSetCode = null;
   private String rowSetName = null;
   private String rowSetDescription = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;   
   private String pageState = new String();
   private ArrayList glRSList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();
   
   public int getRowSelected() {

      return rowSelected;

   }

   public GlFrgRowSetList getGlRSByIndex(int index) {

      return((GlFrgRowSetList)glRSList.get(index));

   }

   public Object[] getGlRSList() {

      return glRSList.toArray();

   }

   public int getGlRSListSize() {

      return glRSList.size();

   }

   public void saveGlRSList(Object newGlRSList) {

      glRSList.add(newGlRSList);

   }

   public void clearGlRSList() {
      glRSList.clear();
   }

   public void setRowSelected(Object selectedGlRSList, boolean isEdit) {

      this.rowSelected = glRSList.indexOf(selectedGlRSList);

      if (isEdit) {

         this.pageState = Constants.PAGE_STATE_EDIT;

      }

   }

   public void showGlRSRow(int rowSelected) {
   	
       this.rowSetName = ((GlFrgRowSetList)glRSList.get(rowSelected)).getRowSetName();
       this.rowSetDescription = ((GlFrgRowSetList)glRSList.get(rowSelected)).getRowSetDescription();
       
   }

   public void updateGlRSRow(int rowSelected, Object newGlRSList) {

      glRSList.set(rowSelected, newGlRSList);

   }

   public void deleteGlRSList(int rowSelected) {

      glRSList.remove(rowSelected);

   }

   public void setUpdateButton(String updateButton) {

      this.updateButton = updateButton;

   }

   public void setCancelButton(String cancelButton) {

      this.cancelButton = cancelButton;

   }

   public void setSaveButton(String saveButton) {

      this.saveButton = saveButton;

   }

   public void setCloseButton(String closeButton) {

      this.closeButton = closeButton;

   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public Integer getRowSetCode() {
   	
   	  return rowSetCode;
   	
   }
   
   public void setRowSetCode(Integer rowSetCode) {
   	
   	  this.rowSetCode = rowSetCode;
   	
   }

   public String getRowSetName() {

      return rowSetName;

   }

   public void setRowSetName(String rowSetName) {

      this.rowSetName = rowSetName;

   }
   
   public String getRowSetDescription() {

      return rowSetDescription;

   }

   public void setRowSetDescription(String rowSetDescription) {

      this.rowSetDescription = rowSetDescription;

   } 
   
   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }   
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

      rowSetName = null; 
      rowSetDescription = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;

   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {

         if (Common.validateRequired(rowSetName)) {

            errors.add("rowSetName",
               new ActionMessage("rowSet.error.rowSetNameRequired"));

         }   

      }
         
      return errors;

   }
}