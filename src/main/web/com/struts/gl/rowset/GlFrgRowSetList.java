package com.struts.gl.rowset;

import java.io.Serializable;

public class GlFrgRowSetList implements Serializable {

   private Integer rowSetCode = null;
   private String rowSetName = null;
   private String rowSetDescription = null;
   
   private String editButton = null;
   private String deleteButton = null;
   private String rowsButton = null;
    
   private GlFrgRowSetForm parentBean;
    
   public GlFrgRowSetList(GlFrgRowSetForm parentBean,
      Integer rowSetCode,
      String rowSetName,
      String rowSetDescription) {

      this.parentBean = parentBean;
      this.rowSetCode = rowSetCode;
      this.rowSetName = rowSetName;
      this.rowSetDescription = rowSetDescription;

   }

   public void setEditButton(String editButton) {

      parentBean.setRowSelected(this, true);

   }
   
   public void setDeleteButton(String deleteButton) {

      parentBean.setRowSelected(this, true);

   }   
   
   public void setRowsButton(String rowsButton) {
   	
   	  parentBean.setRowSelected(this, true);
   	  
   }
   
   public void setCopyButton(String copyButton) {
	   	
   	  parentBean.setRowSelected(this, true);
   	  
   }

   public Integer getRowSetCode() {

      return rowSetCode;

   }
    
   public String getRowSetName() {
   	
   	 return rowSetName;
   	 
   }
   
   public String getRowSetDescription() {
   	
   	 return rowSetDescription;
   	 
   } 
}