package com.struts.gl.staticreport;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyAssignedException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordAlreadyExistException;
import com.ejb.txn.GlStaticReportController;
import com.ejb.txn.GlStaticReportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.Report;
import com.struts.util.User;
import com.util.GlStaticReportDetails;

public final class GlStaticReportAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlStaticReportAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlStaticReportForm actionForm = (GlStaticReportForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_STATIC_REPORT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glStaticReport");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }
         
         //reset attachment
         
         actionForm.setStaticReport(null);
         
/*******************************************************
   Initialize GlStaticReportController EJB
*******************************************************/

         GlStaticReportControllerHome homeSR = null;
         GlStaticReportController ejbSR = null;

         try {

            homeSR = (GlStaticReportControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlStaticReportControllerEJB", GlStaticReportControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlStaticReportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbSR = homeSR.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlStaticReportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();

         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/gl/staticreports/";
         String  staticReportFileExtension = appProperties.getMessage("app.staticReportPdfFileExtension");
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
         
/*******************************************************
   -- Ad RS Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        actionForm.setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glStaticReport"));
	     
/*******************************************************
   -- Ad RS Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glStaticReport"));                  
         
/*******************************************************
   -- Ad RS Save Action --
*******************************************************/

         } else if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            String filename = actionForm.getFile().getFileName();
            
            if (!Common.validateRequired(filename)) {                	       	    	
            	
            	if (actionForm.getFile().getFileSize() == 0) {
            		
            		errors.add(ActionMessages.GLOBAL_MESSAGE,
            				new ActionMessage("staticReport.error.fileNotFound"));
            		
            	} else {
            		
            		String fileExtension = filename.substring(filename.lastIndexOf("."));

            		if (!fileExtension.toUpperCase().equals(staticReportFileExtension.toUpperCase())) {           	    	    	           	    	    
            			
            			errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("staticReport.error.fileInvalid"));
            			
            		}
            		
            		InputStream is = actionForm.getFile().getInputStream();
            		
            		if (is.available() > maxAttachmentFileSize) {
            			
            			errors.add(ActionMessages.GLOBAL_MESSAGE,
            					new ActionMessage("staticReport.error.fileSizeInvalid"));
            			
            		}
            		
            		is.close();	    		
            		
            	}
            	
            	if (!errors.isEmpty()) {
            		
            		saveErrors(request, new ActionMessages(errors));
            		return (mapping.findForward("glStaticReport"));
            		
            	}    	    	
            	
            }            

         	
            GlStaticReportDetails details = new GlStaticReportDetails();
            details.setSrName(actionForm.getStaticReportName());
            details.setSrDescription(actionForm.getDescription());
            details.setSrFileName(attachmentPath   +  actionForm.getStaticReportName() + staticReportFileExtension);
            details.setSrDateFrom(Common.convertStringToSQLDate(actionForm.getDateFrom()));
            details.setSrDateTo(Common.convertStringToSQLDate(actionForm.getDateTo()));
            
            try {
            	
            	ejbSR.addGlSrEntry(details, user.getCmpCode());
                
            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("staticReport.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlStaticReportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {
            	
            	saveErrors(request, new ActionMessages(errors));
            	return (mapping.findForward("glStaticReport"));
            	
            } 
       
            // save staticreport
            
            if (!Common.validateRequired(filename)) {     
            	
            	if (errors.isEmpty()) {
            		
            		InputStream is = actionForm.getFile().getInputStream();
            		
            		new File(attachmentPath).mkdirs();
            		
            		FileOutputStream fos = new FileOutputStream(attachmentPath   +  actionForm.getStaticReportName() + staticReportFileExtension);

            		int c;        
            		
            		while ((c = is.read()) != -1) {
            			
            			fos.write((byte)c);
            			
            		}
            		
            		is.close();
            		fos.close();
            	}          	                
            	
          }

/*******************************************************
   -- Ar RS Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ar RS Update Action --
*******************************************************/

         } else if (request.getParameter("updateButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            
            GlStaticReportList glSRList =
	            actionForm.getGlSRByIndex(actionForm.getRowSelected());
            
            GlStaticReportDetails details = new GlStaticReportDetails();
            details.setSrCode(glSRList.getStaticReportCode());
            details.setSrName(actionForm.getStaticReportName());
            details.setSrDescription(actionForm.getDescription());
            details.setSrDateFrom(Common.convertStringToSQLDate(actionForm.getDateFrom()));
            details.setSrDateTo(Common.convertStringToSQLDate(actionForm.getDateTo()));
            
            try {
            	
            	ejbSR.updateGlSrEntry(details, user.getCmpCode());

            } catch (GlobalRecordAlreadyExistException ex) {
                
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                 new ActionMessage("staticReport.error.recordAlreadyExist"));

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlStaticReportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
            if (!errors.isEmpty()) {
            	
            	saveErrors(request, new ActionMessages(errors));
            	return (mapping.findForward("glStaticReport"));
            	
            } 

/*******************************************************
   -- Ad RS Cancel Action --
*******************************************************/

         } else if (request.getParameter("cancelButton") != null) {

/*******************************************************
   -- Ad RS Edit Action --
*******************************************************/

         } else if (request.getParameter("glSRList[" + 
            actionForm.getRowSelected() + "].editButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            actionForm.showGlSRRow(actionForm.getRowSelected());
            
            if (actionForm.getFileName() != null ){
            	
            	if ( new java.io.File(actionForm.getFileName()).exists()) {
            		
            		actionForm.setShowViewButton(true);
            		
            	} else {
            		
            		actionForm.setShowViewButton(false);
            		
            	}
            	
            }
            
            return mapping.findForward("glStaticReport");
            
/*******************************************************
   -- Ad RS User StaticReport Action --
*******************************************************/

         } else if (request.getParameter("glSRList[" +
            actionForm.getRowSelected() + "].userStaticReportButton") != null){

	        GlStaticReportList glSRList =
               actionForm.getGlSRByIndex(actionForm.getRowSelected());

	        String path = "/glUserStaticReport.do?forward=1" +
	           "&staticReportCode=" + glSRList.getStaticReportCode() + 
	           "&staticReportName=" +  glSRList.getStaticReportName() + 
	           "&description=" + glSRList.getDescription(); 

	        return(new ActionForward(path));            
            
/*******************************************************
   -- Ad RS Delete Action --
*******************************************************/

         } else if (request.getParameter("glSRList[" +
            actionForm.getRowSelected() + "].deleteButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
 
            GlStaticReportList glSRList =
              actionForm.getGlSRByIndex(actionForm.getRowSelected());

            try {
            	
	            ejbSR.deleteGlSrEntry(glSRList.getStaticReportCode(), user.getCmpCode());
	            
	        } catch (GlobalRecordAlreadyAssignedException ex) {
	        	
	            errors.add(ActionMessages.GLOBAL_MESSAGE,
	               new ActionMessage("staticReport.error.deleteStaticReportAlreadyAssigned"));
	            
	        } catch (GlobalRecordAlreadyDeletedException ex) {
	        	
               errors.add(ActionMessages.GLOBAL_MESSAGE,
	              new ActionMessage("staticReport.error.staticReportAlreadyDeleted"));
	           
            } catch(EJBException ex) {
            	
               if (log.isInfoEnabled()) {
               	
                  log.info("EJBException caught in GlStaticReportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               
               return(mapping.findForward("cmnErrorPage"));
               
            }            
/*******************************************************
   -- Ad RS View Action --
*******************************************************/
            
         } else if((request.getParameter("viewButton") != null) ){

         	try {
         		
         		FileInputStream fis = new FileInputStream(actionForm.getGlSRByIndex(actionForm.getRowSelected()).getFileName());
         		
         		byte data[] = new byte[fis.available()];
         		
         		int ctr = 0;
         		int c = 0;
         		
         		while ((c = fis.read()) != -1) {
         			
         			data[ctr] = (byte)c;
         			ctr++;
         			
         		}

         		Report report = new Report();
         		
         		report.setViewType(Constants.REPORT_VIEW_TYPE_PDF);
         		report.setBytes(data);

    		    session.setAttribute(Constants.REPORT_KEY, report);
             	actionForm.setStaticReport(Constants.STATUS_SUCCESS);

         	} catch(Exception ex) {
         		
         		ex.printStackTrace();
         		
         		if(log.isInfoEnabled()) {
         			
         			log.info("Exception caught in AdRepApprovalListAction.execute(): " + ex.getMessage() +
         					" session: " + session.getId());
         			
         		}
         		
         		
         		return(mapping.findForward("cmnErrorPage"));
         		
         	}

            return mapping.findForward("glStaticReport");
            
/*******************************************************
   -- Ar RS Load Action --
*******************************************************/

         }
         

         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glStaticReport");

            }

            try {

            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearGlSRList();	        
            	
            	list = ejbSR.getGlSrAll(user.getCmpCode()); 
            	
            	i = list.iterator();
            	
            	while(i.hasNext()) {
            		
            		GlStaticReportDetails details = (GlStaticReportDetails)i.next();
            		
            		GlStaticReportList glSRList = new GlStaticReportList(actionForm, details.getSrCode(),
            				details.getSrName(), details.getSrDescription(), details.getSrFileName(),
							Common.convertSQLDateToString(details.getSrDateFrom()),
							Common.convertSQLDateToString(details.getSrDateTo()));
            		
            		actionForm.saveGlSRList(glSRList);
            		
            	}
            	
            	
	        } catch (GlobalNoRecordFoundException ex) {
	        		        	
	        } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlStaticReportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.reset(mapping, request);
            
	        if (actionForm.getTableType() == null) {
      		      	
	           actionForm.setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                                  
            
            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            if (actionForm.getFileName() != null ){
            	
            	if ( new java.io.File(actionForm.getFileName()).exists()) {
            		
            		actionForm.setShowViewButton(true);
            		
            	} else {
            		
            		actionForm.setShowViewButton(false);
            		
            	}
            	
            }

            return(mapping.findForward("glStaticReport"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {
      	
    	e.printStackTrace();
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlStaticReportAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}