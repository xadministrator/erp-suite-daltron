package com.struts.gl.staticreport;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlStaticReportForm extends ActionForm implements Serializable {
	
	private Integer staticReportCode = null;
	private String staticReportName = null;
	private String description = null;
	private String dateFrom = null;
	private String dateTo = null;
	private String tableType = null;
	
	private FormFile file = null;
	private String fileName = null;
	
	private String pageState = new String();
	private ArrayList glSRList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private boolean showViewButton = false;
	
	private String staticReport = null;
	
	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public GlStaticReportList getGlSRByIndex(int index) {
		
		return((GlStaticReportList)glSRList.get(index));
		
	}
	
	public Object[] getGlSRList() {
		
		return glSRList.toArray();
		
	}
	
	public int getGlSRListSize() {
		
		return glSRList.size();
		
	}
	
	public void saveGlSRList(Object newGlSRList) {
		
		glSRList.add(newGlSRList);
		
	}
	
	public void clearGlSRList() {
		glSRList.clear();
	}
	
	public void setRowSelected(Object selectedGlSRList, boolean isEdit) {
		
		this.rowSelected = glSRList.indexOf(selectedGlSRList);
		
		if (isEdit) {
			
			this.pageState = Constants.PAGE_STATE_EDIT;
			
		}
		
	}
	
	public void showGlSRRow(int rowSelected) {
		
		this.staticReportName = ((GlStaticReportList)glSRList.get(rowSelected)).getStaticReportName();
		this.description = ((GlStaticReportList)glSRList.get(rowSelected)).getDescription();
		this.fileName =((GlStaticReportList)glSRList.get(rowSelected)).getFileName();
		this.dateFrom = ((GlStaticReportList)glSRList.get(rowSelected)).getDateFrom();
		this.dateTo = ((GlStaticReportList)glSRList.get(rowSelected)).getDateTo();
		
	}
	
	public void updateGlSRRow(int rowSelected, Object newGlSRList) {
		
		glSRList.set(rowSelected, newGlSRList);
		
	}
	
	public void deleteGlSRList(int rowSelected) {
		
		glSRList.remove(rowSelected);
		
	}
	
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getStaticReportCode() {
		
		return staticReportCode;
		
	}
	
	public void setStaticReportCode(Integer staticReportCode) {
		
		this.staticReportCode = staticReportCode;
		
	}
	
	public String getStaticReportName() {
		
		return staticReportName;
		
	}
	
	public void setStaticReportName(String staticReportName) {
		
		this.staticReportName = staticReportName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom) {
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo) {
		
		this.dateTo = dateTo;
		
	}   
	
	public String getTableType() {
		
		return(tableType);
		
	}
	
	public void setTableType(String tableType) {
		
		this.tableType = tableType;
		
	}         
	
	public String getStaticReport() {
		
		return staticReport;
		
	}
	
	public void setStaticReport(String staticReport) {
		
		this.staticReport = staticReport;
		
	}
	
	public boolean getShowViewButton() {
		
		return showViewButton;
		
	}
	
	public void setShowViewButton(boolean showViewButton) {
		
		this.showViewButton = showViewButton;
		
	}
	
	public FormFile getFile() {
		
		return file;
		
	}
	
	public void setFile(FormFile file) {
		
		this.file = file;

	}
	
	public String getFileName() {
		
		return fileName;
		
	}
	
	public void setFileName(String fileName) {
		
		this.fileName = fileName;

	}   	
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		file = null;   
		fileName = null;
		staticReportName = null; 
		description = null;
		dateFrom = null;
		dateTo = null;
		
		
	}
	
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("saveButton") != null || request.getParameter("updateButton") != null) {
			
			if (Common.validateRequired(staticReportName)) {
				
				errors.add("staticReportName",
						new ActionMessage("staticReport.error.staticReportNameRequired"));
				
			}
			
			if (Common.validateRequired(dateFrom)) {
				
				errors.add("dateFrom",
						new ActionMessage("staticReport.error.dateFromRequired"));
				
			}
			
			if (!Common.validateDateFormat(dateFrom)) {
				
				errors.add("dateFrom",
						new ActionMessage("staticReport.error.dateFromInvalid"));
				
			}         
			
			if (!Common.validateDateFormat(dateTo)) {
				
				errors.add("dateTo",
						new ActionMessage("staticReport.error.dateToInvalid"));
				
			}
			
			if ((request.getParameter("saveButton") != null) && (Common.validateRequired(file.getFileName()))) {
				
				errors.add("fileName",
						new ActionMessage("staticReport.error.fileRequired"));
				
			}

		}

		return errors;
		
	}
}