package com.struts.gl.staticreport;

import java.io.Serializable;

public class GlStaticReportList implements Serializable {
	
	private Integer staticReportCode = null;
	private String staticReportName = null;
	private String description = null;
	private String dateFrom = null;
	private String dateTo = null;
	
	private String fileName = null;
	
	private String editButton = null;
	private String deleteButton = null;
	private String userStaticReportButton = null;
	
	private GlStaticReportForm parentBean;
	
	public GlStaticReportList(GlStaticReportForm parentBean,
			Integer staticReportCode,
			String staticReportName,
			String description,
			String fileName,
			String dateFrom,
			String dateTo) {
		
		this.parentBean = parentBean;
		this.staticReportCode = staticReportCode;
		this.staticReportName = staticReportName;
		this.description = description;
		this.fileName = fileName;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		
	}
	
	public void setEditButton(String editButton) {
		
		parentBean.setRowSelected(this, true);
		
	}
	
	public void setDeleteButton(String deleteButton) {
		
		parentBean.setRowSelected(this, false);
		
	}   
	
	public void setUserButton(String userButton) {
		
		parentBean.setRowSelected(this, false);
		
	}
	
	public Integer getStaticReportCode() {
		
		return staticReportCode;
		
	}
	
	public String getStaticReportName() {
		
		return staticReportName;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String getDateFrom() {
		
		return dateFrom;
		
	}
	
	public String getDateTo() {
		
		return dateTo;
		
	}
	
	public String getFileName() {
		
		return fileName;
		
	}
	
	public void setFileName(String fileName) {
		
		this.fileName = fileName;
		
	}
	
	public void setUserStaticReportButton(String userStaticReportButton) {
		
		parentBean.setRowSelected(this, true);
		
	}
	
}