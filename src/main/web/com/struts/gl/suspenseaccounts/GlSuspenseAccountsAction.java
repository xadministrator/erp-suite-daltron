package com.struts.gl.suspenseaccounts;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlCOANoChartOfAccountFoundException;
import com.ejb.exception.GlJCNoJournalCategoryFoundException;
import com.ejb.exception.GlJSNoJournalSourceFoundException;
import com.ejb.exception.GlSANoSuspenseAccountFoundException;
import com.ejb.exception.GlSASourceCategoryCombinationAlreadyExistException;
import com.ejb.exception.GlSASuspenseAccountAlreadyDeletedException;
import com.ejb.exception.GlSASuspenseAccountAlreadyExistException;
import com.ejb.txn.GlSuspenseAccountController;
import com.ejb.txn.GlSuspenseAccountControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlJournalCategoryDetails;
import com.util.GlJournalSourceDetails;
import com.util.GlModSuspenseAccountDetails;
import com.util.GlSuspenseAccountDetails;

public final class GlSuspenseAccountsAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlSuspenseAccountsAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         String frParam = Common.getUserPermission(user, Constants.GL_SUSPENSE_ACCOUNTS_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((GlSuspenseAccountsForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glSuspenseAccounts"));
               }
            }
            ((GlSuspenseAccountsForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlSuspenseAccountsForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlSuspenseAccountController EJB
*******************************************************/

         GlSuspenseAccountControllerHome homeSA = null;
         GlSuspenseAccountController ejbSA = null;

         try{
            
            homeSA = (GlSuspenseAccountControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlSuspenseAccountControllerEJB", GlSuspenseAccountControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlSuspenseAccountsAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbSA = homeSA.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlSuspenseAccountsAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl SA Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlSuspenseAccountsForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glSuspenseAccounts"));
	     
/*******************************************************
   -- Gl SA Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlSuspenseAccountsForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glSuspenseAccounts"));                  

/*******************************************************
   -- Gl  SA Save Action --
*******************************************************/

         } else if(request.getParameter("saveButton") != null &&
            ((GlSuspenseAccountsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	    GlSuspenseAccountDetails details = new GlSuspenseAccountDetails(
	       ((GlSuspenseAccountsForm)form).getSuspenseAccountName().trim().toUpperCase(),
	       ((GlSuspenseAccountsForm)form).getDescription().trim().toUpperCase());

	    try {
	       ejbSA.addGlSaEntry(details, ((GlSuspenseAccountsForm)form).getJournalCategory(),
	          ((GlSuspenseAccountsForm)form).getJournalSource(), ((GlSuspenseAccountsForm)form).getAccount().trim(), user.getCmpCode());
	    }catch(GlSASuspenseAccountAlreadyExistException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("suspenseAccounts.error.suspenseAccountNameAlreadyExists"));	
	    }catch(GlJSNoJournalSourceFoundException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("suspenseAccounts.error.journalSourceNotFound"));
	    }catch(GlJCNoJournalCategoryFoundException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("suspenseAccounts.error.journalCategoryNotFound"));
	    }catch(GlCOANoChartOfAccountFoundException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("suspenseAccounts.error.chartOfAccountNotFound"));
	    }catch(GlSASourceCategoryCombinationAlreadyExistException ex){
	       errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("suspenseAccounts.error.sourceCategoryCombinationAlreadyExists"));
	    }catch(EJBException ex){
       	       if(log.isInfoEnabled()){
	          log.info("EJBException caught in GlSuspenseAccountsAction.execute(): " + ex.getMessage() +
	          " session: " + session.getId());
	       }
	       return(mapping.findForward("cmnErrorPage"));
	    }

/*******************************************************
   -- Gl  SA Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));
/*******************************************************
   -- Gl  SA Update Action --
*******************************************************/

         }else if(request.getParameter("updateButton") != null &&
            ((GlSuspenseAccountsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            GlSuspenseAccountsList glSAList =
               ((GlSuspenseAccountsForm)form).getGlSAByIndex(((GlSuspenseAccountsForm) form).getRowSelected());

	    GlSuspenseAccountDetails details = new GlSuspenseAccountDetails(
	       glSAList.getSuspenseAccountCode(),
	       ((GlSuspenseAccountsForm)form).getSuspenseAccountName().trim().toUpperCase(),
	       ((GlSuspenseAccountsForm)form).getDescription().trim().toUpperCase());

            try {
               ejbSA.updateGlSaEntry(details, ((GlSuspenseAccountsForm)form).getJournalCategory(),
                 ((GlSuspenseAccountsForm)form).getJournalSource(), ((GlSuspenseAccountsForm)form).getAccount().trim(), user.getCmpCode());
               }catch(GlSASuspenseAccountAlreadyExistException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("suspenseAccounts.error.suspenseAccountNameAlreadyExists"));
               }catch(GlJSNoJournalSourceFoundException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("suspenseAccounts.error.journalSourceNotFound"));
               }catch(GlJCNoJournalCategoryFoundException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("suspenseAccounts.error.journalCategoryNotFound"));
               }catch(GlCOANoChartOfAccountFoundException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("suspenseAccounts.error.chartOfAccountNotFound"));
               }catch(GlSASourceCategoryCombinationAlreadyExistException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("suspenseAccounts.error.sourceCategoryCombinationAlreadyExists"));
	       }catch(GlSASuspenseAccountAlreadyDeletedException ex){
                  errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("suspenseAccounts.error.suspenseAccountAlreadyDeleted"));
               }catch(EJBException ex){
                  if(log.isInfoEnabled()){
                     log.info("EJBException caught in GlSuspenseAccountsAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
                  }
                  return(mapping.findForward("cmnErrorPage"));
               }
	       
/*******************************************************
   -- Gl  SA Cancel Action --
*******************************************************/

         }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl  SA Edit Action --
*******************************************************/

         }else if(request.getParameter("glSAList[" + 
            ((GlSuspenseAccountsForm)form).getRowSelected() + "].editButton") != null &&
            ((GlSuspenseAccountsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlSuspenseAccountsForm properties
*******************************************************/

            ((GlSuspenseAccountsForm)form).showGlSARow(((GlSuspenseAccountsForm) form).getRowSelected());
            return(mapping.findForward("glSuspenseAccounts"));
/*******************************************************
   -- Gl  SA Delete Action --
*******************************************************/

         }else if(request.getParameter("glSAList[" +
            ((GlSuspenseAccountsForm)form).getRowSelected() + "].deleteButton") != null &&
            ((GlSuspenseAccountsForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){


            GlSuspenseAccountsList glSAList =
               ((GlSuspenseAccountsForm)form).getGlSAByIndex(((GlSuspenseAccountsForm) form).getRowSelected());

	    try{   
               ejbSA.deleteGlSaEntry(glSAList.getSuspenseAccountCode(), user.getCmpCode());
            }catch(GlSASuspenseAccountAlreadyDeletedException ex){
               errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("suspenseAccounts.error.suspenseAccountAlreadyDeleted"));
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlSuspenseAccountsAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }
	       
/*******************************************************
   -- Gl  SA Load Action --
*******************************************************/

         }
         if(frParam != null){
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glSuspenseAccounts"));
            }

            /*** Populate journalSourceList ArrayList **/
	    ((GlSuspenseAccountsForm) form).clearJournalSourceList();
	    Iterator i = null;
	    try{
	       ArrayList ejbGlJSList = ejbSA.getGlJsAll(user.getCmpCode());
	       i = ejbGlJSList.iterator();
	       while(i.hasNext()){
	          ((GlSuspenseAccountsForm) form).setJournalSourceList(
		     ((GlJournalSourceDetails)i.next()).getJsName());
	       }
	    }catch(GlJSNoJournalSourceFoundException ex){
	       ((GlSuspenseAccountsForm) form).setJournalSourceList(Constants.GLOBAL_NO_RECORD_FOUND);
	    }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlSuspenseAccountsAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

	    /*** Populate journalCategoryList ArrayList **/
	    ((GlSuspenseAccountsForm) form).clearJournalCategoryList();
            try{
	       ArrayList ejbGlJCList = ejbSA.getGlJcAll(user.getCmpCode());
	       i = ejbGlJCList.iterator();
	       while(i.hasNext()){
	          ((GlSuspenseAccountsForm) form).setJournalCategoryList(
	          ((GlJournalCategoryDetails)i.next()).getJcName());
               }
            }catch(GlJCNoJournalCategoryFoundException ex){
               ((GlSuspenseAccountsForm) form).setJournalCategoryList(Constants.GLOBAL_NO_RECORD_FOUND);
            }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlSuspenseAccountsAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

	    /*** Populate main ArrayList property **/
	    ((GlSuspenseAccountsForm) form).clearGlSAList();
	    try{
	       ArrayList ejbGlSAList = ejbSA.getGlSaAll(user.getCmpCode());
	       i = ejbGlSAList.iterator();
	       while(i.hasNext()){
	          GlModSuspenseAccountDetails mdetails = (GlModSuspenseAccountDetails)i.next();
		  GlSuspenseAccountsList glSAList = new GlSuspenseAccountsList(((GlSuspenseAccountsForm) form),
		     mdetails.getSaCode(),
		     mdetails.getSaName(),
		     mdetails.getSaDescription(),
		     mdetails.getSaJsName(),
		     mdetails.getSaJcName(),
		     mdetails.getSaCoaAccountNumber(),
			 mdetails.getSaCoaAccountDescription());
		  ((GlSuspenseAccountsForm) form).saveGlSAList(glSAList);
	       }
	    }catch(GlSANoSuspenseAccountFoundException ex){
	       
	    }catch(EJBException ex){
               if(log.isInfoEnabled()){
                  log.info("EJBException caught in GlSuspenseAccountsAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
               }
               return(mapping.findForward("cmnErrorPage"));
            }

            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
            }else{
               if((request.getParameter("saveButton") != null || 
                   request.getParameter("updateButton") != null || 
                   request.getParameter("glSAList[" + 
                   ((GlSuspenseAccountsForm) form).getRowSelected() + "].deleteButton") != null) && 
                   ((GlSuspenseAccountsForm) form).getUserPermission().equals(Constants.FULL_ACCESS)){

                   ((GlSuspenseAccountsForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
               }
            }

            ((GlSuspenseAccountsForm)form).reset(mapping, request);
            
	        if (((GlSuspenseAccountsForm)form).getTableType() == null) {
      		      	
	           ((GlSuspenseAccountsForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	                       
            
            ((GlSuspenseAccountsForm)form).setPageState(Constants.PAGE_STATE_SAVE);
            return(mapping.findForward("glSuspenseAccounts"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()){
             log.info("Exception caught in GlSuspenseAccountsAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
