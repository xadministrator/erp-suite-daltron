package com.struts.gl.suspenseaccounts;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlSuspenseAccountsForm extends ActionForm implements Serializable {

   private String suspenseAccountName = null;
   private String description = null;
   private String journalSource = null;
   private ArrayList journalSourceList = new ArrayList();
   private String journalCategory = null;
   private ArrayList journalCategoryList = new ArrayList();
   private String account = null;
   private String accountDescription = null;
   private String tableType = null;

   private String showDetailsButton = null;
   private String hideDetailsButton = null;	
   private String saveButton = null;
   private String closeButton = null;
   private String updateButton = null;
   private String cancelButton = null;
   private String pageState = new String();
   private ArrayList glSAList = new ArrayList();
   private int rowSelected = 0;
   private String userPermission = new String();
   private String txnStatus = new String();

   public int getRowSelected(){
      return rowSelected;
   }

   public GlSuspenseAccountsList getGlSAByIndex(int index){
      return((GlSuspenseAccountsList)glSAList.get(index));
   }

   public Object[] getGlSAList(){
      return(glSAList.toArray());
   }

   public int getGlSAListSize(){
      return(glSAList.size());
   }

   public void saveGlSAList(Object newGlSAList){
      glSAList.add(newGlSAList);
   }

   public void clearGlSAList(){
      glSAList.clear();
   }

   public void setRowSelected(Object selectedGlSAList, boolean isEdit){
      this.rowSelected = glSAList.indexOf(selectedGlSAList);
      if(isEdit){
         this.pageState = Constants.PAGE_STATE_EDIT;
      }
   }

   public void showGlSARow(int rowSelected){
      this.suspenseAccountName = ((GlSuspenseAccountsList)glSAList.get(rowSelected)).getSuspenseAccountName();
      this.description = ((GlSuspenseAccountsList)glSAList.get(rowSelected)).getDescription();
      this.journalSource = ((GlSuspenseAccountsList)glSAList.get(rowSelected)).getJournalSource();
      this.journalCategory = ((GlSuspenseAccountsList)glSAList.get(rowSelected)).getJournalCategory();
      this.account = ((GlSuspenseAccountsList)glSAList.get(rowSelected)).getAccount();
      this.accountDescription = ((GlSuspenseAccountsList)glSAList.get(rowSelected)).getAccountDescription();
   }

   public void updateGlSARow(int rowSelected, Object newGlSAList){
      glSAList.set(rowSelected, newGlSAList);
   }

   public void deleteGlSAList(int rowSelected){
      glSAList.remove(rowSelected);
   }

   public void setUpdateButton(String updateButton){
      this.updateButton = updateButton;
   }

   public void setCancelButton(String cancelButton){
      this.cancelButton = cancelButton;
   }

   public void setSaveButton(String saveButton){
      this.saveButton = saveButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }
   
   public void setShowDetailsButton(String showDetailsButton) {
		
	  this.showDetailsButton = showDetailsButton;
		
   }
    
   public void setHideDetailsButton(String hideDetailsButton) {
    	
      this.hideDetailsButton = hideDetailsButton;
    	
   }   

   public void setPageState(String pageState){
      this.pageState = pageState;
   }

   public String getPageState(){
      return(pageState);
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }

   public String getSuspenseAccountName(){
      return(suspenseAccountName);
   }

   public void setSuspenseAccountName(String suspenseAccountName){
      this.suspenseAccountName = suspenseAccountName;
   }

   public String getDescription(){
      return(description);
   }

   public void setDescription(String description){
      this.description = description;
   }

   public String getJournalSource(){
      return(journalSource);
   }

   public void setJournalSource(String journalSource){
      this.journalSource = journalSource;
   }

   public ArrayList getJournalSourceList(){
      return(journalSourceList);
   }

   public void setJournalSourceList(String journalSource){
      journalSourceList.add(journalSource);
   }

   public void clearJournalSourceList(){
      journalSourceList.clear();
      journalSourceList.add(Constants.GLOBAL_BLANK);
   }

   public String getJournalCategory(){
      return(journalCategory);
   }

   public void setJournalCategory(String journalCategory){
      this.journalCategory = journalCategory;
   }

   public ArrayList getJournalCategoryList(){
      return(journalCategoryList);
   }

   public void setJournalCategoryList(String journalCategory){
      journalCategoryList.add(journalCategory);
   }

   public void clearJournalCategoryList(){
      journalCategoryList.clear();
      journalCategoryList.add(Constants.GLOBAL_BLANK);
   }

   public String getAccount(){
      return(account);
   }

   public void setAccount(String account){
      this.account = account;
   }
   
   public String getAccountDescription(){
      return(accountDescription);
   }

   public void setAccountDescription(String accountDescription){
      this.accountDescription = accountDescription;
   }

   public String getTableType() {
    	
      return(tableType);
    	
   }
    
   public void setTableType(String tableType) {
    	
      this.tableType = tableType;
    	
   }

   public void reset(ActionMapping mapping, HttpServletRequest request){
      suspenseAccountName = null;
      description = null;
      journalSource = Constants.GLOBAL_BLANK;
      journalCategory = Constants.GLOBAL_BLANK;
      account = null;
      accountDescription = null;
      saveButton = null;
      updateButton = null;
      cancelButton = null;
      closeButton = null;
	  showDetailsButton = null;
	  hideDetailsButton = null;      
      
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveButton") != null || request.getParameter("updateButton") != null){
         if(Common.validateRequired(suspenseAccountName)){
            errors.add("suspenseAccountName",
               new ActionMessage("suspenseAccounts.error.suspenseAccountNameRequired"));
         }
         if(Common.validateRequired(journalSource)){
            errors.add("journalSource",
               new ActionMessage("suspenseAccounts.error.journalSourceRequired"));
         }
         if(journalSource.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("journalSource",
               new ActionMessage("suspenseAccounts.error.journalSourceRequired"));
         }
         if(!Common.validateStringExists(journalSourceList, journalSource)){
            errors.add("journalSource",
               new ActionMessage("suspenseAccounts.error.journalSourceInvalid"));
         }
         if(Common.validateRequired(journalCategory)){
            errors.add("journalCategory",
               new ActionMessage("suspenseAccounts.error.journalCategoryRequired"));
         }
         if(journalCategory.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
            errors.add("journalCategory",
               new ActionMessage("suspenseAccounts.error.journalCategoryRequired"));
         }
         if(!Common.validateStringExists(journalCategoryList, journalCategory)){
            errors.add("journalCategory",
               new ActionMessage("suspenseAccounts.error.journalCategoryInvalid"));
         }
         if(Common.validateRequired(account)){
            errors.add("account",
               new ActionMessage("suspenseAccounts.error.accountRequired"));
         }
      }
      return(errors);
   }
}