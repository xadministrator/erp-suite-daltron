package com.struts.gl.suspenseaccounts;

import java.io.Serializable;

public class GlSuspenseAccountsList implements Serializable {

   private Integer suspenseAccountCode = null;
   private String suspenseAccountName = null;
   private String description = null;
   private String journalSource = null;
   private String journalCategory = null;
   private String account = null;
   private String accountDescription = null;

   private String deleteButton = null;
   private String editButton = null;
    
   private GlSuspenseAccountsForm parentBean;
    
   public GlSuspenseAccountsList(GlSuspenseAccountsForm parentBean,
      Integer suspenseAccountCode,
      String suspenseAccountName,
      String description,
      String journalSource,
      String journalCategory,
      String account,
	  String accountDescription){

      this.parentBean = parentBean;
      this.suspenseAccountCode = suspenseAccountCode;
      this.suspenseAccountName = suspenseAccountName;
      this.description = description;
      this.journalSource = journalSource;
      this.journalCategory = journalCategory;
      this.account = account;
      this.accountDescription = accountDescription;
   }

   public void setDeleteButton(String deleteButton){
      parentBean.setRowSelected(this, false);
   }

   public void setEditButton(String editButton){
      parentBean.setRowSelected(this, true);
   }

   public Integer getSuspenseAccountCode(){
      return(suspenseAccountCode);
   }

   public String getSuspenseAccountName(){
      return(suspenseAccountName);
   }

   public String getDescription(){
      return(description);
   }

   public String getJournalSource(){
      return(journalSource);
   }

   public String getJournalCategory(){
      return(journalCategory);
   }

   public String getAccount(){
      return(account);
   }
   
   public String getAccountDescription() {
   	   return(accountDescription);
   }

}