package com.struts.gl.taxinterfacemaintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.txn.GlTaxInterfaceMaintenanceController;
import com.ejb.txn.GlTaxInterfaceMaintenanceControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModTaxInterfaceDetails;

public final class GlTaxInterfaceMaintenanceAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	
	
	public ActionForward execute(ActionMapping mapping,  ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		HttpSession session = request.getSession();
		
		try {
			
/*******************************************************
   Check if user has a session
*******************************************************/
			
			User user = (User) session.getAttribute(Constants.USER_KEY);
			
			if (user != null) {
				
				if (log.isInfoEnabled()) {
					
					log.info("GlTaxInterfaceMaintenanceAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
							"' performed this action on session " + session.getId());
				}
				
			} else {
				
				if (log.isInfoEnabled()) {
					
					log.info("User is not logged on in session" + session.getId());
					
				}
				
				return(mapping.findForward("adLogon"));
				
			}
			
			GlTaxInterfaceMaintenanceForm actionForm = (GlTaxInterfaceMaintenanceForm)form;
			
			String frParam = Common.getUserPermission(user, Constants.GL_TAX_INTERFACE_MAINTENANCE_ID);
			
			if (frParam != null) {
				
				if (frParam.trim().equals(Constants.FULL_ACCESS)) {
					
					ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
					if (!fieldErrors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(fieldErrors));
						
						return mapping.findForward("glTaxInterfaceMaintenance");
					}
					
				}
				
				actionForm.setUserPermission(frParam.trim());
				
			} else {
				
				actionForm.setUserPermission(Constants.NO_ACCESS);
				
			}
			
/*******************************************************
   Initialize GlTaxInterfaceMaintenanceController EJB
*******************************************************/
			
			GlTaxInterfaceMaintenanceControllerHome homeTIM = null;
			GlTaxInterfaceMaintenanceController ejbTIM = null;
			
			try {
				
				homeTIM = (GlTaxInterfaceMaintenanceControllerHome)com.util.EJBHomeFactory.
				lookUpHome("ejb/GlTaxInterfaceMaintenanceControllerEJB", GlTaxInterfaceMaintenanceControllerHome.class);
				
			} catch (NamingException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("NamingException caught in GlTaxInterfaceMaintenanceAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
				}
				
				return mapping.findForward("cmnErrorPage");
				
			}
			
			try {
				
				ejbTIM = homeTIM.create();
				
			} catch (CreateException e) {
				
				if (log.isInfoEnabled()) {
					
					log.info("CreateException caught in GlTaxInterfaceMaintenanceAction.execute(): " + e.getMessage() +
							" session: " + session.getId());
					
				}
				return mapping.findForward("cmnErrorPage");
				
			}
			
			ActionErrors errors = new ActionErrors();
			
/*******************************************************
   Call GlTaxInterfaceMaintenanceController EJB
   getGlFcPrecisionUnit
*******************************************************/
			
			short precisionUnit = 0;
			
			try {
				
				precisionUnit = ejbTIM.getGlFcPrecisionUnit(user.getCmpCode());
				
			} catch(EJBException ex) {
				
				if (log.isInfoEnabled()) {
					
					log.info("EJBException caught in GlTaxInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
							" session: " + session.getId());
				}
				
				return(mapping.findForward("cmnErrorPage"));
				
			}
			
/*******************************************************
   -- Gl TIM Previous Action --
*******************************************************/ 
			
			if(request.getParameter("previousButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() - actionForm.getMaxRows());         
				
/*******************************************************
   -- Gl TIM Next Action --
*******************************************************/ 
				
			} else if(request.getParameter("nextButton") != null){
				
				actionForm.setLineCount(actionForm.getLineCount() + actionForm.getMaxRows());
			
			}
			
/*******************************************************
   -- Gl TIM Go Action --
*******************************************************/

			if (request.getParameter("goButton") != null || request.getParameter("nextButton") != null ||
					request.getParameter("previousButton") != null) {
				
				// create criteria 
				
				if (request.getParameter("goButton") != null) {
					
					HashMap criteria = new HashMap();
					
					if (!Common.validateRequired(actionForm.getDocumentType())) {
						
						criteria.put("documentType", actionForm.getDocumentType());
						
					}
					
					if (!Common.validateRequired(actionForm.getDocNumFrom())) {
						
						criteria.put("docNumFrom", actionForm.getDocNumFrom());
						
					}
					
					if (!Common.validateRequired(actionForm.getDocNumTo())) {
						
						criteria.put("docNumTo", actionForm.getDocNumTo());
						
					}
					
					if (!Common.validateRequired(actionForm.getRefNumFrom())) {
						
						criteria.put("refNumFrom", actionForm.getRefNumFrom());
						
					}
					
					if (!Common.validateRequired(actionForm.getRefNumTo())) {
						
						criteria.put("refNumTo", actionForm.getRefNumTo());
						
					}	        	
					
					if (!Common.validateRequired(actionForm.getSubledgerFrom())) {
						
						criteria.put("subledgerFrom", actionForm.getSubledgerFrom());
						
					}
					
					if (!Common.validateRequired(actionForm.getSubledgerTo())) {
						
						criteria.put("subledgerTo", actionForm.getSubledgerTo());
						
					}
					
					// save criteria
					
					actionForm.setLineCount(0);
					actionForm.setCriteria(criteria);
					
				}
				
				try {
					
					actionForm.clearGlTIMList();
					
					ArrayList list = ejbTIM.getGlTiByCriteria(actionForm.getCriteria(),
							new Integer(actionForm.getLineCount()), new Integer(actionForm.getMaxRows() + 1), 
							new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
					// check if prev should be disabled
					if (actionForm.getLineCount() == 0) {
						
						actionForm.setDisablePreviousButton(true);
						
					} else {
						
						actionForm.setDisablePreviousButton(false);
						
					}
					
					// check if next should be disabled
					if (list.size() <= actionForm.getMaxRows()) {
						
						actionForm.setDisableNextButton(true);
						
					} else {
						
						actionForm.setDisableNextButton(false);
						
						//remove last record
						list.remove(list.size() - 1);
						
					}
					
					short lineNumber = (short)actionForm.getLineCount();
					Iterator i = list.iterator();
					
					while (i.hasNext()) {
						
						GlModTaxInterfaceDetails mdetails = (GlModTaxInterfaceDetails)i.next();
						
						lineNumber++;
						
						GlTaxInterfaceMaintenanceList timList = new GlTaxInterfaceMaintenanceList(actionForm,
								mdetails.getTiCode(),
								mdetails.getTiDocumentType(),
								Common.convertSQLDateToString(mdetails.getTiTxnDate()),
								mdetails.getTiTxnDocumentNumber(),
								mdetails.getTiTxnReferenceNumber(),
								mdetails.getTiTxlCoaNumber(),
								mdetails.getTiTaxAmount(),
								mdetails.getTiNetAmount(),
								Common.convertByteToBoolean(mdetails.getTiIsArDocument()),
								Common.convertByteToBoolean(mdetails.getTiEditGlDocument()),
								mdetails.getTiSlSubledgerCode(),
								mdetails.getTiTcName(),
								mdetails.getTiWtcName(),
								actionForm.getArCustomerList(),
								actionForm.getApSupplierList(),
								actionForm.getArTaxCodeList(),
								actionForm.getApTaxCodeList(),
								actionForm.getArWithholdingTaxCodeList(),
								actionForm.getApWithholdingTaxCodeList(),
								mdetails.getTiTxlCode(),
								Common.convertShortToString(lineNumber));
						
						actionForm.saveGlTIMList(timList);
						
					}
					
				} catch (GlobalNoRecordFoundException ex) {
					
					// disable prev next buttons
					actionForm.setDisableNextButton(true);
					actionForm.setDisablePreviousButton(true);
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("taxInterfaceMaintenance.error.noRecordFound"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in GlTaxInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
				actionForm.reset(mapping, request);
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("glTaxInterfaceMaintenance");
					
				}
				
				return(mapping.findForward("glTaxInterfaceMaintenance"));
			
/*******************************************************
   -- Gl TIM Save Action --
*******************************************************/
			
			} else if (request.getParameter("glTIMList[" + 
					actionForm.getRowSelected() + "].saveButton") != null &&
					actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
				
				GlTaxInterfaceMaintenanceList glTIMList = actionForm.getGlTIMByIndex(actionForm.getRowSelected());
				
				GlModTaxInterfaceDetails mdetails = new GlModTaxInterfaceDetails();
				
				mdetails.setTiCode(glTIMList.getTaxInterfaceCode());
				mdetails.setTiDocumentType(glTIMList.getDocumentType());
				mdetails.setTiIsArDocument(Common.convertBooleanToByte(glTIMList.getIsArCheckbox()));
				mdetails.setTiTxlCode(glTIMList.getTxlCode());
				
				if(glTIMList.getTaxCode()!= null && !glTIMList.getTaxCode().equals("")){
					
					mdetails.setTiTcName(glTIMList.getTaxCode());
					mdetails.setTiWtcName(null);
					
				} else if(glTIMList.getWithholdingTaxCode()!= null && !glTIMList.getWithholdingTaxCode().equals("")) {
				
					mdetails.setTiTcName(null);
					mdetails.setTiWtcName(glTIMList.getWithholdingTaxCode());
				
				}
				
				mdetails.setTiTaxAmount(glTIMList.getTaxAmount());
				mdetails.setTiSlSubledgerCode(glTIMList.getSubledgerCode());
				mdetails.setTiTxnReferenceNumber(glTIMList.getReferenceNumber());
				
				try {
					
					ejbTIM.saveGlTiMaintenance(mdetails, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
					
				} catch (GlobalRecordAlreadyDeletedException ex) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE,
							new ActionMessage("taxInterfaceMaintenance.error.recordAlreadyDeleted"));
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in GlTaxInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}
				
/*******************************************************
   -- Gl TIM IsArCheckbox Enter Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("glTIMList[" + 
					actionForm.getRowSelected() + "].isArChecked"))) {
				
				return(mapping.findForward("glTaxInterfaceMaintenance"));
				
/*******************************************************
   -- Gl TIM Tax Code Enter Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("glTIMList[" + 
					actionForm.getRowSelected() + "].isTaxCodeEntered"))) {
				
				GlTaxInterfaceMaintenanceList glTIMList = actionForm.getGlTIMByIndex(actionForm.getRowSelected());
				
				try {
					
					// populate net amount
					
					double rate = 0d;
					
					if(glTIMList.getIsArCheckbox())
						rate = (ejbTIM.getArTcRate(glTIMList.getTaxCode(), user.getCmpCode())) /100;
					else
						rate = (ejbTIM.getApTcRate(glTIMList.getTaxCode(), user.getCmpCode())) /100;
					
					glTIMList.setNetAmount(glTIMList.getTaxAmount() / rate);
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in GlTaxInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}  
				
				return(mapping.findForward("glTaxInterfaceMaintenance"));
		
/*******************************************************
   -- Gl TIM Withholding Tax Code Enter Action --
*******************************************************/
				
			} else if(!Common.validateRequired(request.getParameter("glTIMList[" + 
					actionForm.getRowSelected() + "].isWithholdingTaxCodeEntered"))) {
				
				GlTaxInterfaceMaintenanceList glTIMList = actionForm.getGlTIMByIndex(actionForm.getRowSelected());
				
				try {
					
					// populate net amount
					
					double rate = 0d;
					
					if(glTIMList.getIsArCheckbox())
						rate = (ejbTIM.getArWtcRate(glTIMList.getWithholdingTaxCode(), user.getCmpCode())) /100;
					else
						rate = (ejbTIM.getApWtcRate(glTIMList.getWithholdingTaxCode(), user.getCmpCode())) /100;
					
					glTIMList.setNetAmount(glTIMList.getTaxAmount() / rate); 
					
				} catch (EJBException ex) {
					
					if (log.isInfoEnabled()) {
						
						log.info("EJBException caught in GlTaxInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
								" session: " + session.getId());
						return mapping.findForward("cmnErrorPage"); 
						
					}
					
				}  
				
				return(mapping.findForward("glTaxInterfaceMaintenance"));
				
/*******************************************************
   -- Gl TIM Close Action --
*******************************************************/
				
			} else if (request.getParameter("closeButton") != null) {
				
				return(mapping.findForward("cmnMain"));
				
			}

/*******************************************************
   -- Gl Bga Load Action --
*******************************************************/
				
			if (frParam != null) {
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					return mapping.findForward("glTaxInterfaceMaintenance");
					
				}
				
				if (request.getParameter("forward") == null &&
						actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
					
					errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
					saveErrors(request, new ActionMessages(errors));
					
					return mapping.findForward("cmnMain");	
					
				}
				
				if (!errors.isEmpty()) {
					
					saveErrors(request, new ActionMessages(errors));
					
				} else {
					
					if (request.getParameter("glTIMList[" + actionForm.getRowSelected() + "].saveButton") != null
							&& actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
						
						actionForm.setTxnStatus(Constants.STATUS_SUCCESS);
						
					}

				}
				
				ArrayList list = null;
				Iterator i = null;
				
				// first loading
				
				if(actionForm.getFirstLoading()) {
				
					// supplier list
					
					actionForm.clearApSupplierList();
					list = ejbTIM.getApSpplrAll(user.getCmpCode());
					i = list.iterator();
					
					while(i.hasNext()) {
						String apSupplier = (String)i.next();
						actionForm.setApSupplierList(apSupplier);
					}
					
					// customer list
					
					actionForm.clearArCustomerList();
					list = ejbTIM.getArCstmrAll(user.getCmpCode());
					i = list.iterator();
					
					while(i.hasNext()) {
						String arCustomer = (String)i.next();
						actionForm.setArCustomerList(arCustomer);
					}
					
					// ap tax code
					
					actionForm.clearApTaxCodeList();
					list = ejbTIM.getApTcAll(user.getCmpCode());
					i = list.iterator();
					
					while(i.hasNext()) {
						String apTaxCode = (String)i.next();
						actionForm.setApTaxCodeList(apTaxCode);
					}
					
					// ar tax code
					
					actionForm.clearArTaxCodeList();
					list = ejbTIM.getArTcAll(user.getCmpCode());
					i = list.iterator();
					
					while(i.hasNext()) {
						String arTaxCode = (String)i.next();
						actionForm.setArTaxCodeList(arTaxCode);
					}
					
					// ap withholding tax code
					
					actionForm.clearApWithholdingTaxCodeList();
					list = ejbTIM.getApWtcAll(user.getCmpCode());
					i = list.iterator();
					
					while(i.hasNext()) {
						String apWithholdingTaxCode = (String)i.next();
						actionForm.setApWithholdingTaxCodeList(apWithholdingTaxCode);
					}
					
					// ar withholding tax code
					
					actionForm.clearArWithholdingTaxCodeList();
					list = ejbTIM.getArWtcAll(user.getCmpCode());
					i = list.iterator();
					
					while(i.hasNext()) {
						String arWithholdingTaxCode = (String)i.next();
						actionForm.setArWithholdingTaxCodeList(arWithholdingTaxCode);
					}
					
					// form properties
					
					actionForm.setDocumentType(Constants.GLOBAL_BLANK);
			   		actionForm.setDocNumFrom(null);
			   		actionForm.setDocNumTo(null);
			   		actionForm.setRefNumFrom(null);
			   		actionForm.setRefNumTo(null);
			   		actionForm.setSubledgerFrom(null);
			   		actionForm.setSubledgerTo(null);
			   		actionForm.setCreatedBy(null);
			   		actionForm.setDateCreated(null);
			   		actionForm.setLastModifiedBy(null);
			   		actionForm.setDateLastModified(null);
			   		
			   		actionForm.setMaxRows(10);
			   		actionForm.setDisablePreviousButton(true);
			   		actionForm.setDisableNextButton(true);
				    
//			   		for (int ctr=0; ctr<actionForm.getGlTIMListSize(); ctr++) {
//				    	
//				    	GlTaxInterfaceMaintenanceList actionList = (GlTaxInterfaceMaintenanceList)actionForm.getGlTIMByIndex(ctr);
//				    	actionList.setIsArCheckboxEntered(null);
//				    	actionList.setIsTaxCodeEntered(null);
//				    	actionList.setIsWithholdingTaxCodeEntered(null);
//				    	
//				    }
			   		
			   		actionForm.clearGlTIMList();
					
			   		actionForm.reset(mapping, request);
					actionForm.setFirstLoading(false);
					
				} else {
					
					try {
						
						actionForm.clearGlTIMList();
						
						list = ejbTIM.getGlTiByCriteria(actionForm.getCriteria(),
								new Integer(actionForm.getLineCount()), new Integer(actionForm.getMaxRows() + 1), 
								new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
						
						// check if prev should be disabled
						if (actionForm.getLineCount() == 0) {
							
							actionForm.setDisablePreviousButton(true);
							
						} else {
							
							actionForm.setDisablePreviousButton(false);
							
						}
						
						// check if next should be disabled
						if (list.size() <= actionForm.getMaxRows()) {
							
							actionForm.setDisableNextButton(true);
							
						} else {
							
							actionForm.setDisableNextButton(false);
							
							//remove last record
							list.remove(list.size() - 1);
							
						}
						
						short lineNumber = (short)actionForm.getLineCount();
						i = list.iterator();
						
						while (i.hasNext()) {
							
							GlModTaxInterfaceDetails mdetails = (GlModTaxInterfaceDetails)i.next();
							
							lineNumber++;
							
							GlTaxInterfaceMaintenanceList timList = new GlTaxInterfaceMaintenanceList(actionForm,
									mdetails.getTiCode(),
									mdetails.getTiDocumentType(),
									Common.convertSQLDateToString(mdetails.getTiTxnDate()),
									mdetails.getTiTxnDocumentNumber(),
									mdetails.getTiTxnReferenceNumber(),
									mdetails.getTiTxlCoaNumber(),
									mdetails.getTiTaxAmount(),
									mdetails.getTiNetAmount(),
									Common.convertByteToBoolean(mdetails.getTiIsArDocument()),
									Common.convertByteToBoolean(mdetails.getTiEditGlDocument()),
									mdetails.getTiSlSubledgerCode(),
									mdetails.getTiTcName(),
									mdetails.getTiWtcName(),
									actionForm.getArCustomerList(),
									actionForm.getApSupplierList(),
									actionForm.getArTaxCodeList(),
									actionForm.getApTaxCodeList(),
									actionForm.getArWithholdingTaxCodeList(),
									actionForm.getApWithholdingTaxCodeList(),
									mdetails.getTiTxlCode(),
									Common.convertShortToString(lineNumber));
							
							actionForm.saveGlTIMList(timList);
							
						}
						
					} catch (GlobalNoRecordFoundException ex) {
						
						// disable prev next buttons
						actionForm.setDisableNextButton(true);
						actionForm.setDisablePreviousButton(true);
						errors.add(ActionMessages.GLOBAL_MESSAGE,
								new ActionMessage("taxInterfaceMaintenance.error.noRecordFound"));
						
					} catch (EJBException ex) {
						
						if (log.isInfoEnabled()) {
							
							log.info("EJBException caught in GlTaxInterfaceMaintenanceAction.execute(): " + ex.getMessage() +
									" session: " + session.getId());
							return mapping.findForward("cmnErrorPage"); 
							
						}
						
					}
					
					actionForm.reset(mapping, request);
					
					if (!errors.isEmpty()) {
						
						saveErrors(request, new ActionMessages(errors));
						return mapping.findForward("glTaxInterfaceMaintenance");
						
					}
					
				}
				
				this.setFormProperties(actionForm);
				
				return(mapping.findForward("glTaxInterfaceMaintenance"));
				
			} else {
				
				errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
				saveErrors(request, new ActionMessages(errors));
				
				return(mapping.findForward("cmnMain"));
				
			}
			
		} catch(Exception e) {
			
/*******************************************************
   System Failed: Forward to error page 
*******************************************************/
			
			e.printStackTrace();
			
			if (log.isInfoEnabled()) {
				
				log.info("Exception caught in GlTaxInterfaceMaintenanceAction.execute(): " + e.getMessage()
						+ " session: " + session.getId());
			}
			
			return mapping.findForward("cmnErrorPage");
			
		}
		
	}
	
	private void setFormProperties(GlTaxInterfaceMaintenanceForm actionForm) {
		
		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
			actionForm.setEnableFields(true);
			actionForm.setShowSaveButton(true);
			
		} else {
			
			actionForm.setEnableFields(false);
			actionForm.setShowSaveButton(false);
			
		}
		
	}
}