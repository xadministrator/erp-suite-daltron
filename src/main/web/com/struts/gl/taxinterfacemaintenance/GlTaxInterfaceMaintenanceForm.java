package com.struts.gl.taxinterfacemaintenance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlTaxInterfaceMaintenanceForm extends ActionForm implements Serializable {
   
   private String documentType = null;
   private ArrayList documentTypeList = new ArrayList();
   private String docNumFrom = null;
   private String docNumTo = null;
   private String refNumFrom = null;
   private String refNumTo = null;
   private String subledgerFrom = null;
   private String subledgerTo = null;
	
   private String createdBy = null;
   private String dateCreated = null;
   private String lastModifiedBy = null;
   private String dateLastModified = null;  

   private String pageState = new String();
   private String userPermission = new String();
   private String txnStatus = new String();
   
   private ArrayList glTIMList = new ArrayList();
   private int rowSelected = 0;
   private ArrayList arCustomerList = new ArrayList();
   private ArrayList apSupplierList = new ArrayList();
   private ArrayList arTaxCodeList = new ArrayList();
   private ArrayList arWithholdingTaxCodeList = new ArrayList();
   private ArrayList apTaxCodeList = new ArrayList();
   private ArrayList apWithholdingTaxCodeList = new ArrayList();
   
   private boolean enableFields = false;
   private boolean showSaveButton = false;
   private String nextButton = null;
   private String previousButton = null;
   private boolean disableNextButton = false;
   private boolean disablePreviousButton = false;
   private int lineCount = 0;
   private int maxRows = 0;
   private boolean firstLoading = true;
   
   private HashMap criteria = new HashMap();
   
   public boolean getFirstLoading() {
   	
   	  return firstLoading;
   	  
   }
   
   public void setFirstLoading(boolean firstLoading) {
   	
   	  this.firstLoading = firstLoading;
   	  
   }
   
   public void setPageState(String pageState) {

      this.pageState = pageState;

   }

   public String getPageState() {

      return pageState;

   }

   public String getTxnStatus() {

      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return passTxnStatus;
   }

   public void setTxnStatus(String txnStatus) {

      this.txnStatus = txnStatus;

   }

   public String getUserPermission() {

      return userPermission;

   }

   public void setUserPermission(String userPermission) {

      this.userPermission = userPermission;

   }
   
   public String getDocumentType() {
   	
   	  return documentType;
   	  
   }
   
   public void setDocumentType(String documentType) {
   	
   	  this.documentType = documentType;
   	  
   }
   
   public ArrayList getDocumentTypeList() {
   	
   	  return documentTypeList;
   	  
   }
   
   public void setDocumentTypeList(ArrayList documentTypeList) {

   	  this.documentTypeList = documentTypeList;
   	  
   }
   
   public void clearDocumentTypeList() {
   	
   	  documentTypeList.clear();
   	  documentTypeList.add(Constants.GLOBAL_BLANK);
      
   }
   
   public String getDocNumFrom() {
   	
   	  return docNumFrom;
   	  
   }
   
   public void setDocNumFrom(String docNumFrom) {
   	
   	  this.docNumFrom = docNumFrom;
   	  
   }
   
   public String getDocNumTo() {
   	
   	  return docNumTo;
   	  
   }
   
   public void setDocNumTo(String docNumTo) {
   	
   	  this.docNumTo = docNumTo;
   	  
   }
   
   public String getRefNumFrom() {
   	
   	  return refNumFrom;
   	  
   }
   
   public void setRefNumFrom(String refNumFrom) {
   	 
   	  this.refNumFrom = refNumFrom;
   	  
   }
   
   public String getRefNumTo() {
   	
   	  return refNumTo;
   	  
   }
   
   public void setRefNumTo(String refNumTo) {
   	 
   	  this.refNumTo = refNumTo;
   	  
   } 
   
   public String getSubledgerFrom() {
   	
   	  return subledgerFrom;
   	  
   }
   
   public void setSubledgerFrom(String subledgerFrom) {
   	 
   	  this.subledgerFrom = subledgerFrom;
   	  
   }
   
   public String getSubledgerTo() {
   	
   	  return subledgerTo;
   	  
   }
   
   public void setSubledgerTo(String subledgerTo) {
   	 
   	  this.subledgerTo = subledgerTo;
   	  
   } 
   
   public String getCreatedBy() {
   	
   	   return createdBy;
   	
   }
   
   public void setCreatedBy(String createdBy) {
   	
   	  this.createdBy = createdBy;
   	
   }
   
   public String getDateCreated() {
   	
   	   return dateCreated;
   	
   }
   
   public void setDateCreated(String dateCreated) {
   	
   	   this.dateCreated = dateCreated;
   	
   }   
   
   public String getLastModifiedBy() {
   	
   	  return lastModifiedBy;
   	
   }
   
   public void setLastModifiedBy(String lastModifiedBy) {
   	
   	  this.lastModifiedBy = lastModifiedBy;
   	
   }
   
   public String getDateLastModified() {
   	
   	  return dateLastModified;
   	
   }
   
   public void setDateLastModified(String dateLastModified) {
   	
   	  this.dateLastModified = dateLastModified;
   	
   }
   
   public boolean getEnableFields() {
   	
   	   return enableFields;
   	
   }
   
   public void setEnableFields(boolean enableFields) {
   	
   	   this.enableFields = enableFields;
   	
   }
   
   public boolean getShowSaveButton() {
   	
   	   return showSaveButton;
   	
   }
   
   public void setShowSaveButton(boolean showSaveButton) {
   	
   	   this.showSaveButton = showSaveButton;
   	
   }
   
   public GlTaxInterfaceMaintenanceList getGlTIMByIndex(int index){
   	
   	  return (GlTaxInterfaceMaintenanceList)glTIMList.get(index);
   	  
   }
   
   public Object[] getGlTIMList(){
   	
   	  return glTIMList.toArray();
   	  
   }
   
   public int getGlTIMListSize(){
   	
   	  return glTIMList.size();
   	  
   }
   
   public void saveGlTIMList(Object newGlTIMList){
   	
   	  glTIMList.add(newGlTIMList);
   	 
   }
   
   public void clearGlTIMList(){
   	
   	  glTIMList.clear();
   	  
   }
   
   public void setRowSelected(Object selectedGlTIMList, boolean isEdit){
   	
   	  this.rowSelected = glTIMList.indexOf(selectedGlTIMList);

   }
   
   public int getRowSelected(){
    
   	  return rowSelected;
   	  
   }
   
   public void updateGlTIMRow(int rowSelected, Object newGlTIMList){
   	
   	  glTIMList.set(rowSelected, newGlTIMList);
   	  
   }
   
   public void deleteGlTIMList(int rowSelected){
   	
   	  glTIMList.remove(rowSelected);
   	  
   }
   
   public int getLineCount(){
   	
   	  return lineCount;
   	
   }
   
   public void setLineCount(int lineCount){  	
   	
   	  this.lineCount = lineCount;
   	
   }
   
   public int getMaxRows(){
   	
   	  return maxRows;
   	
   }
   
   public void setMaxRows(int maxRows){  	
   	
   	  this.maxRows = maxRows;
   	
   }
   
   public boolean getDisablePreviousButton() {
   	
   	  return disablePreviousButton;
   	
   }
   
   public void setDisablePreviousButton(boolean disablePreviousButton) {
   	
   	  this.disablePreviousButton = disablePreviousButton;
   	
   }
   
   public boolean getDisableNextButton() {
   
      return disableNextButton;
   
   }
   
   public void setDisableNextButton(boolean disableNextButton) {
   	
   	  this.disableNextButton = disableNextButton;
   	
   }
   
   public HashMap getCriteria() {
   	
   	  return criteria;
   	
   }
   
   public void setCriteria(HashMap criteria) {
   	
   	  this.criteria = criteria;
   	
   }
   
   public ArrayList getArCustomerList() {
   	
   	  return arCustomerList;
   	  
   }
   
   public void setArCustomerList(String customer) {
   	
   	  this.arCustomerList.add(customer);
   	  
   }
   
   public ArrayList getApSupplierList() {
   	
   	  return apSupplierList;
   	  
   }
   
   public void setApSupplierList(String supplier) {
   	
   	  this.apSupplierList.add(supplier);
   	  
   }
   
   public ArrayList getArTaxCodeList() {
   	
   	  return arTaxCodeList;
   	  
   }
   
   public void setArTaxCodeList(String arTaxCode) {
   	
   	  this.arTaxCodeList.add(arTaxCode);
   	  
   }
   
   public ArrayList getApTaxCodeList() {
   	
   	  return apTaxCodeList;
   	  
   }
   
   public void setApTaxCodeList(String apTaxCode) {
   	
   	  this.apTaxCodeList.add(apTaxCode);
   	  
   }
   
   public ArrayList getArWithholdingTaxCodeList() {
   	
   	  return arWithholdingTaxCodeList;
   	  
   }
   
   public void setArWithholdingTaxCodeList(String arWithholdingTaxCode) {
   	
   	  this.arWithholdingTaxCodeList.add(arWithholdingTaxCode);
   	  
   }
   
   public ArrayList getApWithholdingTaxCodeList() {
   	
   	  return apWithholdingTaxCodeList;
   	  
   }
   
   public void setApWithholdingTaxCodeList(String apWithholdingTaxCode) {
   	
   	  this.apWithholdingTaxCodeList.add(apWithholdingTaxCode);
   	  
   }
   
   public void clearArCustomerList() {
   	
   	  arCustomerList.clear();
   	  arCustomerList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public void clearApSupplierList() {
   	
   	  apSupplierList.clear();
      apSupplierList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public void clearArTaxCodeList() {
   	
   	  arTaxCodeList.clear();
   	  arTaxCodeList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public void clearApTaxCodeList() {
   	
   	  apTaxCodeList.clear();
   	  apTaxCodeList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public void clearArWithholdingTaxCodeList() {
   	
   	  arWithholdingTaxCodeList.clear();
   	  arWithholdingTaxCodeList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public void clearApWithholdingTaxCodeList() {
   	
   	  apWithholdingTaxCodeList.clear();
   	  apWithholdingTaxCodeList.add(Constants.GLOBAL_BLANK);
   	
   }
   
   public void reset(ActionMapping mapping, HttpServletRequest request) {

   		documentTypeList.clear();
   		documentTypeList.add(Constants.GLOBAL_BLANK);
   		documentTypeList.add("GL JOURNAL");
   		documentTypeList.add("AP VOUCHER");
   		documentTypeList.add("AP DEBIT MEMO");
   		documentTypeList.add("AP CHECK");
   		documentTypeList.add("AP RECEIVING ITEM");
   		documentTypeList.add("AR INVOICE");
   		documentTypeList.add("AR CREDIT MEMO");
   		documentTypeList.add("AR RECEIPT");
   		
   		for (int i=0; i<glTIMList.size(); i++) {
   			
   			GlTaxInterfaceMaintenanceList actionList = (GlTaxInterfaceMaintenanceList)glTIMList.get(i);
   			
   			actionList.setIsArCheckbox(false);
   			
   		} 
   		
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {

      ActionErrors errors = new ActionErrors();
      if (request.getParameter("goButton") != null) {}

      if (request.getParameter("glTIMList[" + 
			getRowSelected() + "].saveButton") != null) {
      	
      	GlTaxInterfaceMaintenanceList timList = (GlTaxInterfaceMaintenanceList)getGlTIMByIndex(getRowSelected());      	 	 
      	
      	if(Common.validateRequired(timList.getReferenceNumber()) || timList.getReferenceNumber().equals(Constants.GLOBAL_BLANK)){
      		errors.add("referenceNumber",
      				new ActionMessage("taxInterfaceMaintenance.error.referenceNumberRequired", timList.getLineNumber()));
      	}
      	
      	if(Common.validateRequired(timList.getSubledgerCode()) || timList.getSubledgerCode().equals(Constants.GLOBAL_BLANK)){
      		errors.add("subledgerCode",
      				new ActionMessage("taxInterfaceMaintenance.error.subledgerCodeRequired", timList.getLineNumber()));
      	}
      	
      	if(Common.validateRequired(timList.getTaxCode()) && Common.validateRequired(timList.getWithholdingTaxCode())){
      		errors.add("taxCode",
      				new ActionMessage("taxInterfaceMaintenance.error.taxCodeRequired", timList.getLineNumber()));
      	}
      	
      	if(!Common.validateRequired(timList.getTaxCode()) && !Common.validateRequired(timList.getWithholdingTaxCode())){
      		errors.add("taxCode",
      				new ActionMessage("taxInterfaceMaintenance.error.taxCodeRequired", timList.getLineNumber()));
      	}
	 		
	  }
         
      return errors;

   }
}