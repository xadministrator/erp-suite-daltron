package com.struts.gl.taxinterfacemaintenance;

import java.io.Serializable;
import java.util.ArrayList;

public class GlTaxInterfaceMaintenanceList implements Serializable {

   private Integer taxInterfaceCode = null;
   private String documentType = null;
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String accountNumber = null;
   private double taxAmount = 0d;
   private double netAmount = 0d;
   private boolean isArCheckbox = false;
   private boolean editGlDoc = false;
   private String subledgerCode = null;
   private ArrayList customerList = new ArrayList();
   private ArrayList supplierList = new ArrayList();
   private String taxCode = null;
   private ArrayList arTaxCodeList = new ArrayList();
   private ArrayList apTaxCodeList = new ArrayList();
   private String withholdingTaxCode = null;
   private ArrayList arWithholdingTaxCodeList = new ArrayList();
   private ArrayList apWithholdingTaxCodeList = new ArrayList();
   private Integer txlCode = null;
   
   private String lineNumber = null;
   private String isTaxCodeEntered = null;
   private String isWithholdingTaxCodeEntered = null;
   private String isArCheckboxEntered = null;
   
   private GlTaxInterfaceMaintenanceForm parentBean;
    
   public GlTaxInterfaceMaintenanceList(GlTaxInterfaceMaintenanceForm parentBean,
   		Integer taxInterfaceCode,
   		String documentType,
		String date,
		String documentNumber,
		String referenceNumber,
		String accountNumber,
		double taxAmount,
		double netAmount,
		boolean isArCheckbox,
		boolean editGlDoc,
		String subledgerCode,
		String taxCode,
		String withholdingTaxCode,
		ArrayList customerList,
		ArrayList supplierList,
		ArrayList arTaxCodeList,
		ArrayList apTaxCodeList,
		ArrayList arWithholdingTaxCodeList,
		ArrayList apWithholdingTaxCodeList,
		Integer txlCode,
		String lineNumber) {
		
		this.parentBean = parentBean;
		this.taxInterfaceCode = taxInterfaceCode;
		this.documentType = documentType;
		this.date = date;
		this.documentNumber = documentNumber;
		this.referenceNumber = referenceNumber;
		this.accountNumber = accountNumber;
		this.taxAmount = taxAmount;
		this.netAmount = netAmount;
		this.isArCheckbox = isArCheckbox;
		this.editGlDoc = editGlDoc;
		this.subledgerCode = subledgerCode;
		this.taxCode = taxCode;
		this.withholdingTaxCode = withholdingTaxCode;
		this.txlCode = txlCode;
		this.customerList = customerList;
		this.supplierList = supplierList;
		this.arTaxCodeList = arTaxCodeList;
		this.arWithholdingTaxCodeList = arWithholdingTaxCodeList;
		this.apTaxCodeList = apTaxCodeList;
		this.apWithholdingTaxCodeList = apWithholdingTaxCodeList;
		this.lineNumber = lineNumber;
		
   }

   public Integer getTaxInterfaceCode() {
   	
   	  return taxInterfaceCode;
   	  
   }
   
   public void setTaxInterfaceCode(Integer taxInterfaceCode) {
   	
   	  this.taxInterfaceCode = taxInterfaceCode;
   	  
   }
   
   public String getDocumentType() {
   	
   	  return documentType;
   		
   }	
   
   public void setDocumentType(String documentType) {
   	
   	  this.documentType = documentType;
   	  
   }
   
   public String getDate() {
   	
   	  return date;
   	  
   }
   
   public void setDate(String date) {
   	
   	  this.date = date;
   	  
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public void setDocumentNumber(String documentNumber) {
   	
   	  this.documentNumber = documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public void setReferenceNumber(String referenceNumber) {
   	
   	  this.referenceNumber = referenceNumber;
   	  
   }
   
   public String getAccountNumber() {
   	
   	  return accountNumber;
   	
   }
   
   public void setAccountNumber(String accountNumber) {
   	
   	  this.accountNumber = accountNumber;
   	
   }
   
   public double getTaxAmount() {
   	
   	  return taxAmount;
   	  
   }
   
   public void setTaxAmount(double taxAmount) {
   	
   	  this.taxAmount = taxAmount;
   	  
   }
   
   public double getNetAmount() {
   	
   	  return netAmount;
   	  
   }
   
   public void setNetAmount(double netAmount) {
   	
   	  this.netAmount = netAmount;
   	  
   }
   
   public boolean getIsArCheckbox() {
   	
   	  return isArCheckbox;
   	  
   }
   
   public void setIsArCheckbox(boolean isArCheckbox) {
   	
   	  this.isArCheckbox = isArCheckbox;
   	     	  
   }
   
   public boolean getEditGlDoc() {
   	
   	  return editGlDoc;
   	  
   }
   
   public void setEditGlDoc(boolean editGlDoc) {
   	
   	  this.editGlDoc = editGlDoc;
   	  
   }
   
   public String getSubledgerCode() {
   	
   	  return subledgerCode;
   	  
   }

   public void setSubledgerCode(String subledgerCode) {
   	
   	  this.subledgerCode = subledgerCode;
   	  
   }
   
   public String getTaxCode() {
   	
   	  return taxCode;
   	  
   }
   
   public void setTaxCode(String taxCode) {
   	
   	  this.taxCode = taxCode;
   	  
   }
   
   public String getWithholdingTaxCode() {
   	
   	  return withholdingTaxCode;
   	  
   }
   
   public void setWithholdingTaxCode(String withholdingTaxCode) {
   	
   	  this.withholdingTaxCode = withholdingTaxCode;
   	  
   }
   
   public Integer getTxlCode() {
   	
   	  return txlCode;
   	  
   }
   
   public void setTxlCode(Integer txlCode) {
   	
   	  this.txlCode = txlCode;
   	  
   }
   
   public ArrayList getCustomerList() {
   	  
   	  return customerList;
   	  
   }
   
   public void setCustomerList(ArrayList customerList) {
   	
   	  this.customerList = customerList;
   	  
   }
   
   public ArrayList getSupplierList() {
   	
   	  return supplierList;
   	
   }
   
   public void setSupplierList(ArrayList supplierList) {
   	
   	this.supplierList = supplierList;
   	
   }
   
   public ArrayList getArTaxCodeList() {
   	
   	  return arTaxCodeList;
   	
   }
   
   public void setArTaxCodeList(ArrayList arTaxCodeList) {
   	
   	  this.arTaxCodeList = arTaxCodeList;
   	
   }
 
   public ArrayList getArWithholdingTaxCodeList() {
   	
   	  return arWithholdingTaxCodeList;
   	
   }
   
   public void setArWithholdingTaxCodeList(ArrayList arWithholdingTaxCodeList) {
   	
   	  this.arWithholdingTaxCodeList = arWithholdingTaxCodeList;
   	
   }
   
   public ArrayList getApTaxCodeList() {
   	
   	  return apTaxCodeList;
   	
   }
   
   public void setApTaxCodeList(ArrayList apTaxCodeList) {
   	
   	  this.apTaxCodeList = apTaxCodeList;
   	
   }
 
   public ArrayList getApWithholdingTaxCodeList() {
   	
   	  return apWithholdingTaxCodeList;
   	
   }
   
   public void setApWithholdingTaxCodeList(ArrayList apWithholdingTaxCodeList) {
   	
   	  this.apWithholdingTaxCodeList = apWithholdingTaxCodeList;
   	
   }
   
   public String getIsTaxCodeEntered() {
   	
   	  return isTaxCodeEntered;
   	  
   }
   
   public void setIsTaxCodeEntered(String isTaxCodeEntered) {
   	
   	  if (isTaxCodeEntered != null && isTaxCodeEntered.equals("true")) {
   		
   		 parentBean.setRowSelected(this, false);
   		
   	  }
   	
   	  isTaxCodeEntered = null;
   	  
   }
   
   public String getIsWithholdingTaxCodeEntered() {
   	
   	  return isWithholdingTaxCodeEntered;
   	  
   }
   
   public void setIsWithholdingTaxCodeEntered(String isWithholdingTaxCodeEntered) {
   	
   	  if (isWithholdingTaxCodeEntered != null && isWithholdingTaxCodeEntered.equals("true")) {
   		
   		 parentBean.setRowSelected(this, false);
   		
   	  }
   	
   	  isWithholdingTaxCodeEntered = null;
   	  
   }
   
   public String getIsArCheckboxEntered() {
   	
   	  return isArCheckboxEntered;
   	  
   }
   
   public void setIsArCheckboxEntered(String isArCheckboxEntered) {
   	
   	  if (isArCheckboxEntered != null) {
   		
   		 parentBean.setRowSelected(this, false);
   		
   	  }
   	
   	  isArCheckboxEntered = null;
   	
   }
   
   public String getLineNumber() {
   	
   	  return lineNumber;
   	
   }
   
   public void setLineNumber(String lineNumber) {
   	
   	  this.lineNumber = lineNumber;
   	
   }
   
}