package com.struts.gl.taxinterfacerun;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.GlTaxInterfaceRunController;
import com.ejb.txn.GlTaxInterfaceRunControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;

public final class GlTaxInterfaceRunAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlTaxInterfaceRunAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlTaxInterfaceRunForm actionForm = (GlTaxInterfaceRunForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_TAX_INTERFACE_RUN_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glTaxInterfaceRun");
                  
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlTaxInterfaceRunController EJB
*******************************************************/

         GlTaxInterfaceRunControllerHome homeTIR = null;
         GlTaxInterfaceRunController ejbTIR = null;

         try {

            homeTIR = (GlTaxInterfaceRunControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlTaxInterfaceRunControllerEJB", GlTaxInterfaceRunControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlTaxInterfaceRunAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
                
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbTIR = homeTIR.create();
            
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlTaxInterfaceRunAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Gl TIR Run Action --
*******************************************************/

         if (request.getParameter("runButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
         	try {
         		
         		ejbTIR.runTaxInterface(actionForm.getDocumentType(), Common.convertStringToSQLDate(actionForm.getDateFrom()),
         				Common.convertStringToSQLDate(actionForm.getDateTo()), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
         		
         	} catch (EJBException ex) {
         		
         		if (log.isInfoEnabled()) {
         			
         			log.info("EJBException caught in GlTaxInterfaceRunAction.execute() : " + ex.getMessage() +
         					" session: " + session.getId());
         			return mapping.findForward("cmnErrorPage"); 
         			
         		}
         		
         	}
         	
         	
/*******************************************************
   -- Gl TIR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
                        
/*******************************************************
   -- Gl TIR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glTaxInterfaceRun");

            }
            
            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {
                	                	
                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));	
                saveErrors(request, new ActionMessages(errors));
              
                return mapping.findForward("cmnMain");	
                
            }
       	    
	      
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("runButton") != null
               		&& actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
               
            }

            actionForm.reset(mapping, request);
            actionForm.setDateFrom(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setDateTo(Common.convertSQLDateToString(new java.util.Date()));
            
            this.setFormProperties(actionForm);
            return(mapping.findForward("glTaxInterfaceRun"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlTaxInterfaceRunAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
             e.printStackTrace();
             
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
   private void setFormProperties(GlTaxInterfaceRunForm actionForm) {
	
	if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
			
		actionForm.setEnableFields(true);
		actionForm.setShowRunButton(true);

	} else {

		actionForm.setEnableFields(false);
		actionForm.setShowRunButton(false);

	}
	
   }
   
}