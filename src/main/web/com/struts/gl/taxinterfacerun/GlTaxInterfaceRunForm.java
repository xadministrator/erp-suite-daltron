package com.struts.gl.taxinterfacerun;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlTaxInterfaceRunForm extends ActionForm implements Serializable {
   
	private String dateFrom = null;
	private String dateTo = null;
	private String documentType = null;
	private ArrayList documentTypeList  = new ArrayList();
	
	private String pageState = new String();
	private String userPermission = new String();
	private String txnStatus = new String();
	
	private boolean enableFields = false;
	private boolean showRunButton = false;
	
	private String runButton = null;
	private String closeButton = null;	
	private String deleteButton = null;
	
	
	public String getDateFrom(){
		
		return dateFrom;
		
	}
	
	public void setDateFrom(String dateFrom){
		
		this.dateFrom = dateFrom;
		
	}
	
	public String getDateTo(){
		
		return dateTo;
		
	}
	
	public void setDateTo(String dateTo){
		
		this.dateTo = dateTo;
		
	}
	
	public String getDocumentType() {
		
		return documentType;
		
	}
	
	public void setDocumentType(String documentType) {
		
		this.documentType = documentType;
		
	}
	
	public ArrayList getDocumentTypeList() {
		
		return documentTypeList;
		
	}
	
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public boolean getEnableFields() {
		
		return enableFields;
		
	}
	
	public void setEnableFields(boolean enableFields) {
		
		this.enableFields = enableFields;
		
	}
	
	public boolean getShowRunButton() {
		
		return showRunButton;
		
	}
	
	public void setShowRunButton(boolean showRunButton) {
		
		this.showRunButton = showRunButton;
		
	}
	
	public void setRunButton(String runButton){
		
		this.runButton = runButton;
		
	}
	
	public void setCloseButton(String closeButton){
		
		this.closeButton = closeButton;
		
	}
	
	public void setDeleteButton(String deleteButton){
		
		this.deleteButton = deleteButton;
		
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		dateFrom = (null);
		dateTo = null;
		documentTypeList.clear();
		documentTypeList.add(Constants.GLOBAL_BLANK);
		documentTypeList.add("GL JOURNAL");
		documentTypeList.add("AP VOUCHER");
		documentTypeList.add("AP DEBIT MEMO");
		documentTypeList.add("AP CHECK");
		documentTypeList.add("AP RECEIVING ITEM");
		documentTypeList.add("AR INVOICE");
		documentTypeList.add("AR CREDIT MEMO");
		documentTypeList.add("AR RECEIPT");
		documentType = Constants.GLOBAL_BLANK;
		runButton = null;
		closeButton = null;
		deleteButton = null;
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("runButton") != null) {
			
			if(Common.validateRequired(dateFrom)) {
				errors.add("dateFrom", new ActionMessage("taxInterface.error.dateFromRequired"));
			}    
			
			if(Common.validateRequired(dateTo)) {
				errors.add("dateTo", new ActionMessage("taxInterface.error.dateToRequired"));
			}
			
			if(!Common.validateDateFormat(dateFrom)){
				errors.add("dateFrom", new ActionMessage("taxInterface.error.dateFromInvalid"));
			}
			
			if(!Common.validateDateFormat(dateTo)){
				errors.add("dateTo", new ActionMessage("taxInterface.error.dateToInvalid"));
			}
			
			if(!Common.validateDateFromTo(dateFrom, dateTo)){
				errors.add("dateFrom", new ActionMessage("taxInterface.error.dateFromToInvalid"));	
			}
			
		}
		
		return errors;
		
	}
}