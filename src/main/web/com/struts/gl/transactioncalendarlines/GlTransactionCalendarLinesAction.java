package com.struts.gl.transactioncalendarlines;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlTCNoTransactionCalendarFoundException;
import com.ejb.exception.GlTCVNoTransactionCalendarValueFoundException;
import com.ejb.exception.GlTCVTransactionCalendarValueAlreadyDeletedException;
import com.ejb.txn.GlTransactionCalendarValueController;
import com.ejb.txn.GlTransactionCalendarValueControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlTransactionCalendarDetails;
import com.util.GlTransactionCalendarValueDetails;

public final class GlTransactionCalendarLinesAction extends Action {
	
	private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

    public ActionForward execute(ActionMapping mapping,  ActionForm form,
         HttpServletRequest request, HttpServletResponse response)
	 throws Exception { 
   
   
       HttpSession session = request.getSession();
       try{
       
/*******************************************************
   Check if user has a session
*******************************************************/

          User user = (User) session.getAttribute(Constants.USER_KEY);
          if (user != null) {
             if (log.isInfoEnabled()){
                log.info("GlTransactionCalendarLinesAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                     "' performed this action on session " + session.getId());
             }
          }else{
             if (log.isInfoEnabled()){
	        log.info("User is not logged on in session " + session.getId());
             }
	     return(mapping.findForward("adLogon"));
          }

          String frParam = Common.getUserPermission(user, Constants.GL_TRANSACTION_CALENDAR_LINES_ID);
          if(frParam != null){
  	     if(frParam.trim().equals(Constants.FULL_ACCESS)){
	        ActionErrors fieldErrors = ((GlTransactionCalendarLinesForm)form).validateFields(mapping, request);
		if(!fieldErrors.isEmpty()){
		   saveErrors(request, new ActionMessages(fieldErrors));
		   return(mapping.findForward("glTransactionCalendarLines"));
		}
	     }
	     ((GlTransactionCalendarLinesForm)form).setUserPermission(frParam.trim());
	  }else{
	     ((GlTransactionCalendarLinesForm)form).setUserPermission(Constants.NO_ACCESS);
	  }

	 ActionErrors errors = new ActionErrors();
	 
/*******************************************************
   Initialize GlTransactionCalendarValues EJB
*******************************************************/

          GlTransactionCalendarValueControllerHome homeTCV = null;
          GlTransactionCalendarValueController ejbTCV = null;
       
          try{
             
             homeTCV = (GlTransactionCalendarValueControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlTransactionCalendarValueControllerEJB", GlTransactionCalendarValueControllerHome.class);
             
          }catch(NamingException e){
             if(log.isInfoEnabled()){
	       log.info("NamingException caught in GlTransactionCalendarValueAction.execute(): " + e.getMessage() + 
	          " session: " + session.getId()); 
	     }
             return(mapping.findForward("cmnErrorPage")); 
          }
       
          try{
              ejbTCV = homeTCV.create();
          }catch(CreateException e){
             if(log.isInfoEnabled()){
                log.info("CreateException caught in GlTransactionCalendarValueAction.execute(): " + e.getMessage() + 
	           " session: " + session.getId());
             }
             return(mapping.findForward("cmnErrorPage"));
          }
	  

/*******************************************************
   -- Gl TCV Show Details Action --
*******************************************************/

         if (request.getParameter("showDetailsButton") != null) {
      		      	
	        ((GlTransactionCalendarLinesForm)form).setTableType(Constants.GLOBAL_DETAILED);
	     
	        return(mapping.findForward("glTransactionCalendarLines"));
	     
/*******************************************************
   -- Gl TCV Hide Details Action --
*******************************************************/	     
	     
	     } else if (request.getParameter("hideDetailsButton") != null) { 
	  
	        ((GlTransactionCalendarLinesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	        
	        return(mapping.findForward("glTransactionCalendarLines"));         
	     
/*******************************************************
   -- Gl TCV Close Action --
*******************************************************/

	  }else if(request.getParameter("closeButton") != null){
	     return(mapping.findForward("cmnMain"));
	      
/*******************************************************
   -- Gl TCV Update Action --
*******************************************************/

	  }else if(request.getParameter("updateButton") != null &&
	     ((GlTransactionCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

	     GlTransactionCalendarLinesList glTCVList = ((GlTransactionCalendarLinesForm)form).getGlTCVByIndex(
	        ((GlTransactionCalendarLinesForm)form).getRowSelected());     
	     GlTransactionCalendarValueDetails details = new GlTransactionCalendarValueDetails(
	        glTCVList.getTransactionCalendarLinesCode(), 
		Common.convertBooleanToByte(((GlTransactionCalendarLinesForm)form).getBusinessDay()));

/*******************************************************
   Call TransactionCalendarValues EJB updateGlTcvEntry(
   GlTransactionCalendarLinesList details) method
********************************************************/

             try{
	        ejbTCV.updateGlTcvEntry(details, user.getCmpCode());
	     }catch(GlTCVTransactionCalendarValueAlreadyDeletedException ex){
	        errors.add(ActionMessages.GLOBAL_MESSAGE,
                   new ActionMessage("transactionCalendarLines.error.transactionAlreadyDeleted"));
             }catch(EJBException ex){
	        if(log.isInfoEnabled()){
	           log.info("EJBException caught in GlAccountingCalendarAction.execute(): " + ex.getMessage() +
	           " session: " + session.getId());
	        }
	      return(mapping.findForward("cmnErrorPage"));
	     } 
             	     
/*******************************************************
   -- Gl TCV Cancel Action --
*******************************************************/

	  }else if(request.getParameter("cancelButton") != null){

/*******************************************************
   -- Gl TCV Edit Action --
*******************************************************/

	  }else if(request.getParameter("glTCVList[" + 
             ((GlTransactionCalendarLinesForm) form).getRowSelected() + "].editButton") != null &&
	     ((GlTransactionCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
	     
/*******************************************************
   Retrieve selected row from the ArrayList variable
   and populate GlTransactionCalendarLinesForm properties
*******************************************************/

	       ((GlTransactionCalendarLinesForm) form).showGlTCVRow(((GlTransactionCalendarLinesForm) form).getRowSelected());
		return(mapping.findForward("glTransactionCalendarLines"));
		     
/*******************************************************
   -- Gl TCV Load/Go Action --
*******************************************************/

	   }
	     if(frParam != null){
		    if (!errors.isEmpty()) {
                       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glTransactionCalendarLines"));
                    }
										      
		    ((GlTransactionCalendarLinesForm)form).clearGlTCVList();


/*******************************************************
   Call GlTransactionCalendarValues EJB getGlTcAll()
   method
********************************************************/
 
                if(request.getParameter("loadDefaultButton") == null && request.getParameter("updateButton") == null &&
                    request.getParameter("goButton") == null && request.getParameter("cancelButton") == null){			    
                    ((GlTransactionCalendarLinesForm)form).clearTransactionCalendarList();
		    try{
		       ArrayList ejbGlTCList = ejbTCV.getGlTcAll(user.getCmpCode());
		       Iterator i = ejbGlTCList.iterator();
		       while(i.hasNext()){
			   ((GlTransactionCalendarLinesForm)form).setTransactionCalendarList(
			      ((GlTransactionCalendarDetails)i.next()).getTcName());
		       }
		    }catch(GlTCNoTransactionCalendarFoundException ex){
		       ((GlTransactionCalendarLinesForm)form).setTransactionCalendarList(Constants.GLOBAL_NO_RECORD_FOUND);
		    }catch(EJBException ex){
		       if(log.isInfoEnabled()){
		          log.info("EJBException caught in GlTransactionCalendarLinesAction.execute(): " + 
			     ex.getMessage() + " session: " + session.getId());
		       }
		       return(mapping.findForward("cmnErrorPage"));
		    }
		    ((GlTransactionCalendarLinesForm)form).setTransactionCalendar(null);
		    ((GlTransactionCalendarLinesForm)form).setDescription(null);
		}   

/*******************************************************
Call GlTransactionCalendarValues EJB
getGlTcDescriptionByGlTcName(String TCV_TC_NM)
method
******************************************************/
                if(((GlTransactionCalendarLinesForm)form).getTransactionCalendar() != null){    
		    try{
		       GlTransactionCalendarDetails details = ejbTCV.getGlTcDescriptionByGlTcName(
		          ((GlTransactionCalendarLinesForm)form).getTransactionCalendar(), user.getCmpCode());
		        ((GlTransactionCalendarLinesForm)form).setDescription(details.getTcDescription());
		    }catch(GlTCNoTransactionCalendarFoundException ex){
		        errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("transactionCalendarLines.error.noTransactionCalendarFound"));
		    }catch(EJBException ex){
		        if(log.isInfoEnabled()){
                           log.info("EJBException caught in GlTransactionCalendarLinesAction.execute(): " +
                              ex.getMessage() + " session: " + session.getId());
		        }
		        return(mapping.findForward("cmnErrorPage"));
		    }
		    if(!errors.isEmpty()){
                       saveErrors(request, new ActionMessages(errors));
		       return(mapping.findForward("glTransactionCalendarLines"));
                    }
		    
/*******************************************************
   Call GlTransactionCalendarValues EJB getGlTcvByGlTcName(
   String TCV_AC_NM) method
********************************************************/

	            try{
                       ArrayList ejbGlTCVList = ejbTCV.getGlTcvByGlTcName(
                       ((GlTransactionCalendarLinesForm)form).getTransactionCalendar(), user.getCmpCode());
		       Iterator i = ejbGlTCVList.iterator();
		       GlTransactionCalendarLinesList glTCVList = null;
		       while(i.hasNext()){
		          GlTransactionCalendarValueDetails details = (GlTransactionCalendarValueDetails)i.next();
		          glTCVList = new GlTransactionCalendarLinesList(((GlTransactionCalendarLinesForm)form),
			     details.getTcvCode(),
			     Common.convertSQLDateToString(details.getTcvDate()),
			     Common.getDayOfWeek(details.getTcvDayOfWeek()),
			     Common.convertByteToBoolean(details.getTcvBusinessDay()));
		          ((GlTransactionCalendarLinesForm)form).saveGlTCVList(glTCVList);
		       }
		    }catch(GlTCVNoTransactionCalendarValueFoundException ex){
		        errors.add(ActionMessages.GLOBAL_MESSAGE,
                           new ActionMessage("transactionCalendarLines.error.noTransactionCalendarLineFound"));
 		    }catch(GlTCNoTransactionCalendarFoundException ex){
                        errors.add(ActionMessages.GLOBAL_MESSAGE,
		           new ActionMessage("transactionCalendarLines.error.noTransactionCalendarFound"));
		    }catch(EJBException ex){
                       if(log.isInfoEnabled()){
                          log.info("EJBException caught in GlTransactionCalendarLinesAction.execute(): " +
                             ex.getMessage() + " session: " + session.getId());
                       }
                       return(mapping.findForward("cmnErrorPage"));
                    }
	            if(!errors.isEmpty()){
		       saveErrors(request, new ActionMessages(errors));			                    
	            }else{
		       if((request.getParameter("loadDefaultButton") != null || request.getParameter("updateButton") != null)
		          && ((GlTransactionCalendarLinesForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){
		          ((GlTransactionCalendarLinesForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
		       }
		    }
		 }
 	            ((GlTransactionCalendarLinesForm)form).setPageState(Constants.PAGE_STATE_SAVE);
		    ((GlTransactionCalendarLinesForm)form).reset(mapping, request);
		    
	        if (((GlTransactionCalendarLinesForm)form).getTableType() == null) {
      		      	
	           ((GlTransactionCalendarLinesForm)form).setTableType(Constants.GLOBAL_SUMMARIZED);
	          
            }	           		    
		    
		    return(mapping.findForward("glTransactionCalendarLines"));
		}else{
		   errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                   saveErrors(request, new ActionMessages(errors));
		   
		   return(mapping.findForward("cmnMain"));
		}
      }catch(Exception e){

/*******************************************************
   Failed: Forward to error page
********************************************************/

         if(log.isInfoEnabled()){
	    log.info("Exception caught in GlTransactionCalendarLinesAction.execute(): " + e.getMessage() + " session: " 
	       + session.getId());
	 }
 	return(mapping.findForward("cmnErrorPage"));
      } 
    }
}
