package com.struts.gl.transactioncalendarlines;


import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;

public class GlTransactionCalendarLinesForm extends ActionForm implements Serializable{

	private String transactionCalendar = null;
	private ArrayList tcList = new ArrayList();
	private String description = null;
	private String date = null;
	private String dayOfWeek = null;
	private boolean businessDay = false;	
    private String tableType = null;

    private String showDetailsButton = null;
    private String hideDetailsButton = null;	
	private String closeButton = null;
	private String updateButton = null;
	private String cancelButton = null;
	private String goButton = null;
	private String pageState = new String();
	private ArrayList glTCVList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public int getRowSelected(){
	   return rowSelected;
	}
	
        public GlTransactionCalendarLinesList getGlTCVByIndex(int index){
	   return((GlTransactionCalendarLinesList)glTCVList.get(index));
	}
	public Object[] getGlTCVList(){
	    return(glTCVList.toArray());
	}

	public int getGlTCVListSize(){
	   return(glTCVList.size());
	}

	public void saveGlTCVList(Object newGlTCVList){
	   glTCVList.add(newGlTCVList);
	}

	public void clearGlTCVList(){
	   glTCVList.clear();
	}
	
	public void setRowSelected(Object selectedGlTCVList, boolean isEdit){
	   this.rowSelected = glTCVList.indexOf(selectedGlTCVList);
	   if(isEdit){
	      this.pageState = Constants.PAGE_STATE_EDIT;
	   }
	}
	
	public void showGlTCVRow(int rowSelected){
	   this.date = ((GlTransactionCalendarLinesList)glTCVList.get(rowSelected)).getDate();
	   this.dayOfWeek = ((GlTransactionCalendarLinesList)glTCVList.get(rowSelected)).getDayOfWeek();
	   this.businessDay = ((GlTransactionCalendarLinesList)glTCVList.get(rowSelected)).getBusinessDay();
	}

	public void updateGlTCVRow(int rowSelected, Object newGlTCVList){
	   glTCVList.set(rowSelected, newGlTCVList);
	}

	public void deleteGlTCVList(int rowSelected){
	   glTCVList.remove(rowSelected);
	}
	
	public void setUpdateButton(String updateButton){
	   this.updateButton = updateButton;
	}
	
	public void setCancelButton(String cancelButton){
	   this.cancelButton = cancelButton;
	}
	
	public void setGoButton(String goButton){
	   this.goButton = goButton;
	}

	public void setCloseButton(String closeButton){
	   this.closeButton = closeButton;
	}

    public void setShowDetailsButton(String showDetailsButton) {
		
  	   this.showDetailsButton = showDetailsButton;
		
    }
    
    public void setHideDetailsButton(String hideDetailsButton) {
    	
       this.hideDetailsButton = hideDetailsButton;
    	
    }

	public void setPageState(String pageState){
	   this.pageState = pageState;
	}

	public String getPageState(){
	    return(pageState);
	}

        public String getTxnStatus(){
	   String passTxnStatus = txnStatus;
	   txnStatus = Constants.GLOBAL_BLANK;
	   return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
	   this.txnStatus = txnStatus;
	}

        public String getUserPermission(){
	   return(userPermission);
	}

	public void setUserPermission(String userPermission){
	   this.userPermission = userPermission;
	}

	public String getTransactionCalendar(){
	   return(transactionCalendar);
	}

	public void setTransactionCalendar(String transactionCalendar){
	   this.transactionCalendar = transactionCalendar;
	}

	public ArrayList getTcList(){
	   return(tcList);
	}

	public void setTransactionCalendarList(String transactionCalendar){
	   tcList.add(transactionCalendar);
	}

	public void clearTransactionCalendarList(){
	   tcList.clear();
	   tcList.add(Constants.GLOBAL_BLANK);
	}

	public String getDescription(){
	   return(description); 
	}
	
	public void setDescription(String description){
	   this.description = description;
	}
  
	public String getDate(){
	   return(date);
	}

	public void setDate(String date){
	   this.date = date;
	}

	public String getDayOfWeek(){
	   return(dayOfWeek);
	}

	public void setDayOfWeek(String dayOfWeek){
	   this.dayOfWeek = dayOfWeek;
	}

	public boolean getBusinessDay(){
	   return(businessDay);
	}

        public void setBusinessDay(boolean businessDay){
	   this.businessDay = businessDay;
	}

    public String getTableType() {
    	
       return(tableType);
    	
    }
    
    public void setTableType(String tableType) {
    	
       this.tableType = tableType;
    	
    }
    public void reset(ActionMapping mapping, HttpServletRequest request){	   
	   closeButton = null;
	   goButton = null;
	   updateButton = null;
	   cancelButton = null;
	   date = null;
	   dayOfWeek = null;
	   businessDay = false;
	   showDetailsButton = null;
	   hideDetailsButton = null;           

        }

        public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
           ActionErrors errors = new ActionErrors();
	   if(request.getParameter("goButton") != null || request.getParameter("loadDefaultButton") != null ||
	      request.getParameter("updateButton") != null){
	      if(Common.validateRequired(transactionCalendar) || 
	         transactionCalendar.equals(Constants.GLOBAL_NO_RECORD_FOUND)){ 
		 
	         errors.add("transactionCalendar", 
		    new ActionMessage("transactionCalendarLines.error.transactionCalendarRequired"));
              }
           }

	  return(errors);
	}
		
}
