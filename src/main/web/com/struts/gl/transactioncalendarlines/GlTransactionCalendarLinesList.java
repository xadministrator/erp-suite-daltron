package com.struts.gl.transactioncalendarlines;

import java.io.Serializable;

public class GlTransactionCalendarLinesList implements Serializable{

	private Integer transactionCalendarLinesCode = null;
	private String date = null;	
	private String dayOfWeek = null;
	private boolean businessDay = false;
	
	private String editButton = null;
	
	private GlTransactionCalendarLinesForm parentBean;

	public GlTransactionCalendarLinesList(GlTransactionCalendarLinesForm parentBean, Integer transactionCalendarLinesCode,
	   String date, String dayOfWeek, boolean businessDay){
	   
	   this.parentBean = parentBean;
	   this.transactionCalendarLinesCode = transactionCalendarLinesCode;
	   this.date = date;
	   this.dayOfWeek = dayOfWeek;
	   this.businessDay = businessDay;
	}

	public void setParentBean(GlTransactionCalendarLinesForm parentBean){
	   this.parentBean = parentBean;
	}

	public void setEditButton(String editButton){
	   parentBean.setRowSelected(this, true);
	}

        public Integer getTransactionCalendarLinesCode(){
	   return(transactionCalendarLinesCode);
	}
	
	public String getDate(){
	   return(date);
	}

	public String getDayOfWeek(){
	   return(dayOfWeek);
	}

	public boolean getBusinessDay(){
	   return(businessDay);
	}
}
