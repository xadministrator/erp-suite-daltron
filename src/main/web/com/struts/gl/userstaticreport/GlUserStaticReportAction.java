package com.struts.gl.userstaticreport;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.GlUserStaticReportController;
import com.ejb.txn.GlUserStaticReportControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlModUserStaticReportDetails;
import com.util.GlStaticReportDetails;

public final class GlUserStaticReportAction extends Action {

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("GlUserStaticReportAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         GlUserStaticReportForm actionForm = (GlUserStaticReportForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.GL_USER_STATIC_REPORT_ID);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("glUserStaticReport");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize GlUserStaticReportController EJB
*******************************************************/

         GlUserStaticReportControllerHome homeUSTR = null;
         GlUserStaticReportController ejbUSTR = null;

         try {

            homeUSTR = (GlUserStaticReportControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlUserStaticReportControllerEJB", GlUserStaticReportControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in GlUserStaticReportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

            ejbUSTR = homeUSTR.create();
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in GlUserStaticReportAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
 /*******************************************************
     -- Gl USTR Save Action --
  *******************************************************/
         
         if (request.getParameter("saveButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {
         	
            ArrayList list = new ArrayList();
            
            for (int i=0; i<actionForm.getGlUSTRListSize(); i++) {
            
                GlUserStaticReportList ustrList = actionForm.getGlUSTRByIndex(i);
                
                if (ustrList.getSelect() == false) continue;
	           	   
	           	   GlModUserStaticReportDetails mdetails = new GlModUserStaticReportDetails();
	           	   
	           	   mdetails.setUstrUsrCode(ustrList.getUserCode()); 
	           	   
	           	   list.add(mdetails);
	           	   
            }
            
            try {
            	
            	ejbUSTR.saveGlUstrEntry(list, actionForm.getStaticReportCode(), user.getCmpCode());

            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GLUserStaticReportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }


/*******************************************************
   -- Gl USTR Close Action --
*******************************************************/

         } else if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));
            
/*******************************************************
   -- Gl USTR Static Report Action --
*******************************************************/
            
         } else if (request.getParameter("backButton") != null) {
         	
         		return(new ActionForward("/glStaticReport.do"));
         		
/*******************************************************
   -- Gl USTR Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("glUserStaticReport");

            }
                                  	                        	                          	            	
            GlStaticReportDetails rDetails = null;

	        if (request.getParameter("forward") != null) {

	            rDetails = ejbUSTR.getGlSrBySrCode(new Integer(request.getParameter("staticReportCode")));        		
	    	    actionForm.setStaticReportCode(new Integer(request.getParameter("staticReportCode")));

	        } else {
	     		
	            rDetails = ejbUSTR.getGlSrBySrCode(actionForm.getStaticReportCode());
	     		
	        }
                              	                              	            		            	        	            		
    	    actionForm.setStaticReportCode(actionForm.getStaticReportCode());
            actionForm.setStaticReportName(rDetails.getSrName());
            actionForm.setDescription(rDetails.getSrDescription());

            ArrayList list = null;
            Iterator i = null;
                       

	        try {	        	
	        	
	        	actionForm.clearGlUSTRList();

        		ArrayList selectedGlUSTRList = new ArrayList();
            	selectedGlUSTRList = ejbUSTR.getGlUstrBySrCode(actionForm.getStaticReportCode(), user.getCmpCode()); 
	        	list = ejbUSTR.getAdUsrAll(user.getCmpCode());
            	i = list.iterator();
            	
            	while(i.hasNext()) {

            		GlModUserStaticReportDetails mdetails = (GlModUserStaticReportDetails)i.next();
            		
            		GlUserStaticReportList glUSTRList = new GlUserStaticReportList(actionForm,
            				mdetails.getUstrUsrCode(),
							mdetails.getUstrUsrName());
            		
            		Iterator j = selectedGlUSTRList.iterator();
					
					while (j.hasNext()){

            			GlModUserStaticReportDetails selectedDetails = (GlModUserStaticReportDetails) j.next();
            			
            			if (selectedDetails.getUstrUsrCode() == mdetails.getUstrUsrCode()){
            				
            				glUSTRList.setSelect(true);
            				
            			}
            			
            		}
            		
            		actionForm.saveGlUSTRList(glUSTRList);
            		
            	}

	        } catch (EJBException ex) {
	        	
	        	if (log.isInfoEnabled()) {	        		log.info("EJBException caught in GLUserStaticReportAction.execute(): " + ex.getMessage() +
	        				" session: " + session.getId());
	        		return mapping.findForward("cmnErrorPage"); 
	        		
	        	}
	        	
	        }          
			
			try {
				
			
			} catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in GlUserStaticReportAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
		    
            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            actionForm.setPageState(Constants.PAGE_STATE_SAVE);
            
            return(mapping.findForward("glUserStaticReport"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(ArithmeticException e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in GlUserStaticReportAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}