package com.struts.gl.userstaticreport;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.struts.util.Constants;


public class GlUserStaticReportForm extends ActionForm implements Serializable {
	
	private Integer staticReportCode = null;
	private String staticReportName = null;
	private String description = null;
	
	private String pageState = new String();
	private ArrayList glUSTRList = new ArrayList();
	
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();

	public int getRowSelected() {
		
		return rowSelected;
		
	}
	
	public GlUserStaticReportList getGlUSTRByIndex(int index) {
		
		return((GlUserStaticReportList)glUSTRList.get(index));
		
	}
	
	public Object[] getGlUSTRList() {
		
		return glUSTRList.toArray();
		
	}
	
	public int getGlUSTRListSize() {
		
		return glUSTRList.size();
		
	}   
	
	public void saveGlUSTRList(Object newGlUSTRist) {
		
		glUSTRList.add(newGlUSTRist);
		
	}
	
	public int getGlUSTRIndexOf(Object obj) {
		
		return glUSTRList.indexOf(obj);
		
	}
	
	public void clearGlUSTRList() {
		
		glUSTRList.clear();
	}
	
	public void setPageState(String pageState) {
		
		this.pageState = pageState;
		
	}
	
	public String getPageState() {
		
		return pageState;
		
	}
	
	public String getTxnStatus() {
		
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
	}
	
	public void setTxnStatus(String txnStatus) {
		
		this.txnStatus = txnStatus;
		
	}
	
	public String getUserPermission() {
		
		return userPermission;
		
	}
	
	public void setUserPermission(String userPermission) {
		
		this.userPermission = userPermission;
		
	}
	
	public Integer getStaticReportCode() {
		
		return staticReportCode;
		
	}
	
	public void setStaticReportCode(Integer staticReportCode) {
		
		this.staticReportCode = staticReportCode;
		
	}
	
	public String getStaticReportName() {
		
		return staticReportName;
		
	}
	
	public void setStaticReportName(String staticReportName) {
		
		this.staticReportName = staticReportName;   	  
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	

	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		
		for (int i=0; i<glUSTRList.size(); i++) {
			
			GlUserStaticReportList actionList = (GlUserStaticReportList)glUSTRList.get(i);
			actionList.setSelect(false);
			
		}
		
	}
	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
		
		ActionErrors errors = new ActionErrors();
		
		return errors;
		
	}
}