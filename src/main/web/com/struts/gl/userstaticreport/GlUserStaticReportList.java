package com.struts.gl.userstaticreport;

import java.io.Serializable;

public class GlUserStaticReportList implements Serializable {

   private Integer userCode = null;
   private String userName = null;
   private boolean select = false;
    
   private GlUserStaticReportForm parentBean;
    
   public GlUserStaticReportList(GlUserStaticReportForm parentBean,
      Integer userCode,
      String userName) {

      this.parentBean = parentBean;
      this.userCode = userCode;
      this.userName = userName;

   }
   
   public Integer getUserCode() {
    
    return userCode;
    
   }

   public String getUserName() {
       
       return userName;
       
   }

   public boolean getSelect() {

      return select;

   }
   
   public void setSelect(boolean select) {
   	
   	   this.select = select;
   	
   }

}