package com.struts.gl.yearendclosing;

import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.exception.GlDepreciationAlreadyMadeForThePeriodException;
import com.ejb.exception.GlDepreciationPeriodInvalidException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalNotAllTransactionsArePostedException;
import com.ejb.txn.GlYearEndClosingController;
import com.ejb.txn.GlYearEndClosingControllerHome;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.util.GlAccountingCalendarDetails;
import com.util.GlModAccountingCalendarValueDetails;

public final class GlYearEndClosingAction extends Action{
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());	

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);
         if (user != null) {
            if (log.isInfoEnabled()){
                log.info("GlYearEndClosingAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }
         }else{
            if (log.isInfoEnabled()){
               log.info("User is not logged on in session" + session.getId());
            }
            return(mapping.findForward("adLogon"));
         }
         GlYearEndClosingForm actionForm = (GlYearEndClosingForm)form;
         String frParam = Common.getUserPermission(user, Constants.GL_YEAR_END_CLOSING_ID);
         if(frParam != null){
	      if(frParam.trim().equals(Constants.FULL_ACCESS)){
	         ActionErrors fieldErrors = ((GlYearEndClosingForm)form).validateFields(mapping, request);
               if(!fieldErrors.isEmpty()){
                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("glYearEndClosing"));
               }
            }
            ((GlYearEndClosingForm)form).setUserPermission(frParam.trim());
         }else{
            ((GlYearEndClosingForm)form).setUserPermission(Constants.NO_ACCESS);
         }

/*******************************************************
   Initialize GlYearEndClosingController EJB
*******************************************************/

            GlYearEndClosingControllerHome homeYC = null;
            GlYearEndClosingController ejbYC = null;

         try{
            
            homeYC = (GlYearEndClosingControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/GlYearEndClosingControllerEJB", GlYearEndClosingControllerHome.class);
            
         }catch(NamingException e){
            if(log.isInfoEnabled()){
                log.info("NamingException caught in GlYearEndClosingAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try{
            ejbYC = homeYC.create();
         }catch(CreateException e){
            if(log.isInfoEnabled()){
                log.info("CreateException caught in GlYearEndClosingAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         ActionErrors errors = new ActionErrors();

/*******************************************************
   -- Gl  YC Execute Closing Action --
*******************************************************/

         if(request.getParameter("executeClosingButton") != null &&
            ((GlYearEndClosingForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            try {
            	
		       ejbYC.executeGlYearEndClosing(((GlYearEndClosingForm)form).getAccountingCalendarCode(), user.getCmpCode());
		       
		    } catch (GlobalNotAllTransactionsArePostedException ex) {
           	
           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("yearEndClosing.error.notAllTransactionsArePosted"));
           	   
		    } catch (GlobalAccountNumberInvalidException ex) {
	           	
		    	errors.add(ActionMessages.GLOBAL_MESSAGE,
		    			new ActionMessage("yearEndClosing.error.accountNumberInvalid"));
		    	
		    } catch(EJBException ex) {
		        if(log.isInfoEnabled()){
		 		   log.info("EJBException caught in GlYearEndClosingAction.execute(): " + ex.getMessage() + 
				   " session: " + session.getId());
				}
				return(mapping.findForward("cmnErrorPage"));
	    	}
		    
/*******************************************************
-- Gl  YC Execute Closing Action --
*******************************************************/

         }else if(request.getParameter("executeFADepreciationExecuteButton") != null &&
        		 ((GlYearEndClosingForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

        	 try {
        		 System.out.println(((GlYearEndClosingForm)form).getAccountingCalendarCode());
        		 System.out.println(actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')));
        		 System.out.println(Common.convertStringToInt(actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)));
        		 System.out.println(user.getUserName());
        		 System.out.println(new Integer(user.getCurrentBranch().getBrCode()));

        		 ejbYC.executeFixedAssetDepreciation(((GlYearEndClosingForm)form).getAccountingCalendarCode(), 
        				 actionForm.getPeriod().substring(0, actionForm.getPeriod().indexOf('-')),
      		           Common.convertStringToInt(actionForm.getPeriod().substring(actionForm.getPeriod().indexOf('-') + 1)),
      		           user.getUserName(), new Integer(user.getCurrentResCode()), new Integer(user.getCurrentBranch().getBrCode()),
      		           user.getCmpCode());

        	 } catch (GlJREffectiveDateNoPeriodExistException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("yearEndClosing.error.effectiveDateNoPeriodExist"));           	

        	 } catch (GlJREffectiveDatePeriodClosedException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("yearEndClosing.error.effectiveDatePeriodClosed"));

        	 }catch (GlobalNoRecordFoundException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("journalEntry.error.noRecordFound"));

        	 }catch (GlobalAccountNumberInvalidException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("yearEndClosing.error.accountNumberInvalid"));

        	 }catch (GlDepreciationAlreadyMadeForThePeriodException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("yearEndClosing.error.depreciationAlreadyMadeForThePeriod"));

        	 }catch (GlDepreciationPeriodInvalidException ex) {

        		 errors.add(ActionMessages.GLOBAL_MESSAGE,
        				 new ActionMessage("yearEndClosing.error.depreciationPeriodInvalid"));

        	 } catch(EJBException ex) {
        		 if(log.isInfoEnabled()){
        			 log.info("EJBException caught in GlYearEndClosingAction.execute(): " + ex.getMessage() + 
        					 " session: " + session.getId());
        		 }
        		 return(mapping.findForward("cmnErrorPage"));
        	 }


               
/*******************************************************
   -- Gl  YC Close Action --
*******************************************************/

         }else if(request.getParameter("closeButton") != null){
            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Gl  YC Load Action --
*******************************************************/

         }
         if(frParam != null){

             try{
             	ArrayList list = null;			       			       
 		        Iterator i = null;
 		        
             	actionForm.clearPeriodList();           	
             	
             	list = ejbYC.getGlReportableAcvAll(user.getCmpCode());
             	
             	if (list == null || list.size() == 0) {
             		
             		actionForm.setPeriodList(Constants.GLOBAL_NO_RECORD_FOUND);
             		
             	} else {
             		           		            		
             		i = list.iterator();
             		
             		while (i.hasNext()) {
             			
             		    GlModAccountingCalendarValueDetails mdetails = 
             		        (GlModAccountingCalendarValueDetails)i.next();
             		        
             		    actionForm.setPeriodList(mdetails.getAcvPeriodPrefix() + "-" + 
             		        mdetails.getAcvYear());
             		        
             		    if (mdetails.getAcvCurrent()) {
             		    	
             		    	actionForm.setPeriod(mdetails.getAcvPeriodPrefix() + "-" + 
             		            mdetails.getAcvYear());
             		    	
             		    }
             			
             		}
             			            		
             	}    
             }catch(Exception e){
             	
             }
             //actionForm.reset(mapping, request);
            if(!errors.isEmpty()){
               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("glYearEndClosing"));
            }
            
            
            try {
            	
            	GlAccountingCalendarDetails details = ejbYC.getGlAcForClosing(user.getCmpCode());
            	
            	((GlYearEndClosingForm)form).setAccountingCalendarCode(details.getAcCode());
                ((GlYearEndClosingForm)form).setAccountingCalendar(details.getAcName());
                ((GlYearEndClosingForm)form).setDescription(details.getAcDescription());
                
            } catch(GlobalNoRecordFoundException ex) {
            	            	            	
            } catch(EJBException ex) {
		        if(log.isInfoEnabled()){
		 		   log.info("EJBException caught in GlYearEndClosingAction.execute(): " + ex.getMessage() + 
				   " session: " + session.getId());
				}
				return(mapping.findForward("cmnErrorPage"));
	    	}
            
            

            if(request.getParameter("executeClosingButton") != null && 
                ((GlYearEndClosingForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

                ((GlYearEndClosingForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
            }

            if(request.getParameter("executeFADepreciationExecuteButton") != null && 
            		((GlYearEndClosingForm)form).getUserPermission().equals(Constants.FULL_ACCESS)){

            	((GlYearEndClosingForm)form).setTxnStatus(Constants.STATUS_SUCCESS);
            }
            
            return(mapping.findForward("glYearEndClosing"));

         }else{
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));
         }
      }catch(Exception e){

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

          if(log.isInfoEnabled()){
             log.info("Exception caught in GlYearEndClosingAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
          }
          return(mapping.findForward("cmnErrorPage"));
       }
    }
}
