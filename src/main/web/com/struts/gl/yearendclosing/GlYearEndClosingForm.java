package com.struts.gl.yearendclosing;

import java.io.Serializable;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class GlYearEndClosingForm extends ActionForm implements Serializable {
   
   private Integer accountingCalendarCode = null;
   private String accountingCalendar = null;
   private String description = null;
   private String executeClosingButton = null;
   private String closeButton = null;
   private String userPermission = new String();
   private String txnStatus = new String();
   private String period = null;
   private ArrayList periodList = new ArrayList();
	
   public Integer getAccountingCalendarCode() {
   	
   	  return accountingCalendarCode;
   	
   }
   
   public void setAccountingCalendarCode(Integer accountingCalendarCode) {
   	
   	  this.accountingCalendarCode = accountingCalendarCode;
   	
   }
   
   public String getAccountingCalendar() {
   	
   	  return accountingCalendar;
   	
   }
   
   public void setAccountingCalendar(String accountingCalendar) {
   	 
   	  this.accountingCalendar = accountingCalendar;
   	
   }
   
   public String getDescription() {
   	
   	  return description;
   	
   }
   
   public void setDescription(String description) {
   	
   	  this.description = description;
   	
   }

   public void setExecuteClosingButton(String executeClosingButton){
      this.executeClosingButton = executeClosingButton;
   }

   public void setCloseButton(String closeButton){
      this.closeButton = closeButton;
   }

   public String getTxnStatus(){
      String passTxnStatus = txnStatus;
      txnStatus = Constants.GLOBAL_BLANK;
      return(passTxnStatus);
   }

   public void setTxnStatus(String txnStatus){
      this.txnStatus = txnStatus;
   }

   public String getUserPermission(){
      return(userPermission);
   }

   public void setUserPermission(String userPermission){
      this.userPermission = userPermission;
   }
   
   public String getPeriod(){
		return(period);
	}
	
	public void setPeriod(String period){
		this.period = period;
	}
	
	public ArrayList getPeriodList(){
		return(periodList);
	}
	
	public void setPeriodList(String period){
		periodList.add(period);
	}
	
	public void clearPeriodList(){
		periodList.clear();
		periodList.add(Constants.GLOBAL_BLANK);
	}

   public void reset(ActionMapping mapping, HttpServletRequest request){
   	
   	  accountingCalendar = null;
   	  description = null;
      executeClosingButton = null;
      closeButton = null;
   }

   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
	   ActionErrors errors = new ActionErrors();
	   if (request.getParameter("executeFADepreciationExecuteButton") != null) {
		   if(Common.validateRequired(period) || period.equals(Constants.GLOBAL_NO_RECORD_FOUND)){
			   errors.add("period", new ActionMessage("detailTrialBalance.error.periodRequired"));
		   }
		   if(!Common.validateStringExists(periodList, period)){
			   errors.add("period", new ActionMessage("detailTrialBalance.error.periodInvalid"));
		   }
	   }
	   
      return(errors);
   }
}
