package com.struts.hr.synchronizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.ejb.txn.HrSynchronizerController;
import com.ejb.txn.HrSynchronizerControllerHome;

import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;

import sun.net.www.protocol.http.HttpURLConnection;

public final class HrSynchronizerAction extends Action {
	
   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());
	
   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      
      
      
      
      try {

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("HrSynchronizerAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                "' performed this action on session " + session.getId());
            }

         } else {

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }
         
         HrSynchronizerForm actionForm = (HrSynchronizerForm)form;
         
         String frParam = Common.getUserPermission(user, Constants.HR_SYNCHRONIZER);

         if (frParam != null) {

	      if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	         ActionErrors fieldErrors = actionForm.validateFields(mapping, request);
               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));

                  return mapping.findForward("hrSynchronizer");
               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize hrSynchronizerController EJB
*******************************************************/

         
         HrSynchronizerControllerHome homeSYNC = null;
         HrSynchronizerController ejbSYNC = null;
         

         try {

        	 homeSYNC = (HrSynchronizerControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/HrSynchronizerControllerEJB", HrSynchronizerControllerHome.class);
            
         } catch (NamingException e) {

            if (log.isInfoEnabled()) {

                log.info("NamingException caught in HrSynchronizeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }

            return mapping.findForward("cmnErrorPage");

         }

         try {

        	 ejbSYNC = homeSYNC.create();
        	 
         } catch (CreateException e) {

            if (log.isInfoEnabled()) {

                log.info("CreateException caught in HrSynchronizeAction.execute(): " + e.getMessage() +
               " session: " + session.getId());

            }
            return mapping.findForward("cmnErrorPage");

         }

         ActionErrors errors = new ActionErrors();
         
/*******************************************************
   -- Ad AS Close Action --
*******************************************************/

         if (request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Ad AS Save Action --
*******************************************************/

         } else if (request.getParameter("synchronizeButton") != null &&
            actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

            System.out.println("synchronizeButton");

            try {
            	ejbSYNC.setHrPayrollPeriod(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setHrDeployedBranch(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setHrEmployees(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	ejbSYNC.setCstSplEmployees(user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	ejbSYNC.setCstSplClients(user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	//ejbSYNC.setHrAppliedDeductions(user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	
            	//ejbSYNC.setHrAppliedCashBondInsDeductions(user.getUserName(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            } catch (EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in HrSynchronizeAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }
            
/*******************************************************
	 -- Ad AS Approval Document Action --
*******************************************************/

         // forward to approval document module             	
            	
/*         } else if (request.getParameter("approvalDocumentButton") != null) {

			return(new ActionForward("/adApprovalDocument.do"));     */        
            
/*******************************************************
   -- Ad AS Load Action --
*******************************************************/

         }
         
         if (frParam != null) {

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return mapping.findForward("hrSynchronizer");

            }
                         	                          	            	
	        try {
	        	
            	
	        } catch (EJBException ex) {
                
               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in HrSynchronizerAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
                  return mapping.findForward("cmnErrorPage"); 
                  
               }

            }          

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               
            } else {

               if (request.getParameter("saveButton") != null) {

                   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }
            }

            return(mapping.findForward("hrSynchronizer"));

         } else {
         	
            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page 
*******************************************************/

         if (log.isInfoEnabled()) {

             log.info("Exception caught in HrSynchronizerAction.execute(): " + e.getMessage()
                + " session: " + session.getId());
         }

          return mapping.findForward("cmnErrorPage");

      }

   }
   
}