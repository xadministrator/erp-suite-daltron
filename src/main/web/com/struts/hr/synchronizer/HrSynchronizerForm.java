package com.struts.hr.synchronizer;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.struts.util.Common;
import com.struts.util.Constants;


public class HrSynchronizerForm extends ActionForm implements Serializable {

	private Integer storedprocedureCode = null;
	
    private boolean enableGlGeneralLedgerReport = false;
    private String nameGlGeneralLedgerReport = null;
    
    private boolean enableGlTrialBalanceReport = false;
    private String nameGlTrialBalanceReport = null;
    
    private boolean enableGlIncomeStatementReport = false;
    private String nameGlIncomeStatementReport = null;
    
    
    private boolean enableGlBalanceSheetReport = false;
    private String nameGlBalanceSheetReport = null;
    
    
    private boolean enableArStatementOfAccountReport = false;
    private String nameArStatementOfAccountReport = null;
    
    private String synchronizeButton = null;
    
	private String saveButton = null;
	private String closeButton = null;
	private String userPermission = new String();
	private String txnStatus = new String();
	
	public String getTxnStatus() {
	
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return passTxnStatus;
		
	}
	
	public void setTxnStatus(String txnStatus) {
	
	  	this.txnStatus = txnStatus;
	
	}
	
	public String getUserPermission() {
	
	  	return userPermission;
	
	}
	
	public void setUserPermission(String userPermission) {
	
	  	this.userPermission = userPermission;
	
	}

	public Integer getStoredProcedureCode() {
		
		return storedprocedureCode;
		
	}
	
	public void setStoredProcedureCode(Integer storedprocedureCode) {
		
		this.storedprocedureCode = storedprocedureCode;
		
	}
	
	public boolean getEnableGlGeneralLedgerReport() {
		
		return enableGlGeneralLedgerReport;
		
	}
	
	
	
	
	public void setEnableGlGeneralLedgerReport(boolean enableGlGeneralLedgerReport) {
		
		this.enableGlGeneralLedgerReport = enableGlGeneralLedgerReport;
		
	}
	
	
	
	public String getNameGlGeneralLedgerReport() {
		
		return nameGlGeneralLedgerReport;
		
	}
	
	public void setNameGlGeneralLedgerReport(String nameGlGeneralLedgerReport) {
		
		this.nameGlGeneralLedgerReport = nameGlGeneralLedgerReport;
		
	}
	
	
	
	
	
	
	

	public boolean getEnableGlTrialBalanceReport() {
		
		return enableGlTrialBalanceReport;
		
	}
	
	public void setEnableGlTrialBalanceReport(boolean enableGlTrialBalanceReport) {
		
		this.enableGlTrialBalanceReport = enableGlTrialBalanceReport;
		
	}

	
	public String getNameGlTrialBalanceReport() {
		
		return nameGlTrialBalanceReport;
		
	}
	
	public void setNameGlTrialBalanceReport(String nameGlTrialBalanceReport) {
		
		this.nameGlTrialBalanceReport = nameGlTrialBalanceReport;
		
	}
	
	
	
	
	
	
	
	public boolean getEnableGlIncomeStatementReport() {
		
		return enableGlIncomeStatementReport;
		
	}
	
	public void setEnableGlIncomeStatementReport(boolean enableGlIncomeStatementReport) {
		
		this.enableGlIncomeStatementReport = enableGlIncomeStatementReport;
		
	}
	
	
	
	public String getNameGlIncomeStatementReport() {
		
		return nameGlIncomeStatementReport;
		
	}
	
	public void setNameGlIncomeStatementReport(String nameGlIncomeStatementReport) {
		
		this.nameGlIncomeStatementReport = nameGlIncomeStatementReport;
		
	}
	
	
public boolean getEnableGlBalanceSheetReport() {
		
		return enableGlBalanceSheetReport;
		
	}
	
	public void setEnableGlBalanceSheetReport(boolean enableGlBalanceSheetReport) {
		
		this.enableGlBalanceSheetReport = enableGlBalanceSheetReport;
		
	}
	
	
	
	public String getNameGlBalanceSheetReport() {
		
		return nameGlBalanceSheetReport;
		
	}
	
	public void setNameGlBalanceSheetReport(String nameGlBalanceSheetReport) {
		
		this.nameGlBalanceSheetReport = nameGlBalanceSheetReport;
		
	}
	
	
        public boolean getEnableArStatementOfAccountReport() {
		
		return enableArStatementOfAccountReport;
		
	}
	
	
	
	
	public void setEnableArStatementOfAccountReport(boolean enableArStatementOfAccountReport){
		
		this.enableArStatementOfAccountReport = enableArStatementOfAccountReport;
		
	}
	
	
	
	public String getNameArStatementOfAccountReport() {
		
		return nameArStatementOfAccountReport;
		
	}
	
	public void setNameArStatementOfAccountReport(String nameArStatementOfAccountReport) {
		
		this.nameArStatementOfAccountReport = nameArStatementOfAccountReport;
		
	}
        
	public void reset(ActionMapping mapping, HttpServletRequest request) {		
		
		enableGlGeneralLedgerReport = false;
		nameGlGeneralLedgerReport = null;
		
		
		enableGlTrialBalanceReport = false;
		nameGlTrialBalanceReport= null;
		
		enableGlIncomeStatementReport = false;
		nameGlIncomeStatementReport = null;
		
		enableGlBalanceSheetReport= false;
		nameGlBalanceSheetReport = null;
                
                enableArStatementOfAccountReport = false;
                nameArStatementOfAccountReport = null;
   
		
	}



	
	public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request) {
	
		ActionErrors errors = new ActionErrors();
		if (request.getParameter("synchronizeButton") != null) {
	
			
	  	}
	     
	  	return errors;
	
	}
	
}