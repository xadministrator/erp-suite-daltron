package com.struts.inv.adjustmententry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;

import com.ejb.exception.AdPRFCoaGlVarianceAccountNotFoundException;
import com.ejb.exception.GlJREffectiveDateNoPeriodExistException;
import com.ejb.exception.GlJREffectiveDatePeriodClosedException;
import com.ejb.exception.GlobalAccountNumberInvalidException;
import com.ejb.exception.GlobalBranchAccountNumberInvalidException;
import com.ejb.exception.GlobalDocumentNumberNotUniqueException;
import com.ejb.exception.GlobalExpiryDateNotFoundException;
import com.ejb.exception.GlobalInvItemLocationNotFoundException;
import com.ejb.exception.GlobalInvTagMissingException;
import com.ejb.exception.GlobalInvTagExistingException;
import com.ejb.exception.GlobalInventoryDateException;
import com.ejb.exception.GlobalJournalNotBalanceException;
import com.ejb.exception.GlobalMiscInfoIsRequiredException;
import com.ejb.exception.GlobalNoApprovalApproverFoundException;
import com.ejb.exception.GlobalNoApprovalRequesterFoundException;
import com.ejb.exception.GlobalNoRecordFoundException;
import com.ejb.exception.GlobalRecordAlreadyDeletedException;
import com.ejb.exception.GlobalRecordInvalidException;
import com.ejb.exception.GlobalTransactionAlreadyApprovedException;
import com.ejb.exception.GlobalTransactionAlreadyPendingException;
import com.ejb.exception.GlobalTransactionAlreadyPostedException;
import com.ejb.exception.InvTagSerialNumberAlreadyExistException;
import com.ejb.exception.InvTagSerialNumberNotFoundException;
import com.ejb.txn.InvAdjustmentEntryController;
import com.ejb.txn.InvAdjustmentEntryControllerHome;
import com.ejb.txn.InvApprovalController;
import com.ejb.txn.InvApprovalControllerHome;
import com.ejb.txn.InvItemEntryController;
import com.ejb.txn.InvItemEntryControllerHome;
import com.struts.ap.purchaseorderentry.ApPurchaseOrderEntryList;
import com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionApproverList;
import com.struts.ap.receivingitementry.ApReceivingItemEntryTagList;
import com.struts.util.ApproverList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.struts.util.User;
import com.struts.util.Image;
import com.util.AdModApprovalQueueDetails;
import com.util.ApModSupplierDetails;
import com.util.InvAdjustmentDetails;
import com.util.InvModAdjustmentDetails;
import com.util.InvModAdjustmentLineDetails;
import com.util.InvModTagListDetails;
import com.util.InvModUnitOfMeasureDetails;

public final class InvAdjustmentEntryAction extends Action{

   private org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getFactory().getInstance(this.getClass().getName());

   public ActionForward execute(ActionMapping mapping,  ActionForm form,
      HttpServletRequest request, HttpServletResponse response)
      throws Exception {

      HttpSession session = request.getSession();
      InvAdjustmentEntryForm actionForm = (InvAdjustmentEntryForm)form;

      try{

/*******************************************************
   Check if user has a session
*******************************************************/

         User user = (User) session.getAttribute(Constants.USER_KEY);

         if (user != null) {

            if (log.isInfoEnabled()) {

                log.info("InvAdjustmentEntryAction: Company '" + user.getCompany() + "' User '" + user.getUserName() +
                   "' performed this action on session " + session.getId());

            }

         }else{

            if (log.isInfoEnabled()) {

               log.info("User is not logged on in session" + session.getId());

            }

            return(mapping.findForward("adLogon"));

         }

         // reset report

         actionForm.setReport(null);
         actionForm.setAttachment(null);
         actionForm.setAttachmentPDF(null);
         actionForm.setAttachmentDownload(null);
         String frParam = Common.getUserPermission(user, Constants.INV_ADJUSTMENT_ENTRY_ID);

         if (frParam != null) {

	        if (frParam.trim().equals(Constants.FULL_ACCESS)) {

	           ActionErrors fieldErrors = actionForm.validateFields(mapping, request);

               if (!fieldErrors.isEmpty()) {

                  saveErrors(request, new ActionMessages(fieldErrors));
                  return(mapping.findForward("invAdjustmentEntry"));

               }

            }

            actionForm.setUserPermission(frParam.trim());

         } else {

            actionForm.setUserPermission(Constants.NO_ACCESS);

         }

/*******************************************************
   Initialize invAdjustmentEntryController EJB
*******************************************************/

         InvAdjustmentEntryControllerHome homeADJ = null;
         InvAdjustmentEntryController ejbADJ = null;
         
         InvApprovalControllerHome homeIA =null;
         InvApprovalController ejbIA = null;
         
         InvItemEntryControllerHome homeII = null;
         InvItemEntryController ejbII = null;

         try {

            homeADJ = (InvAdjustmentEntryControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvAdjustmentEntryControllerEJB", InvAdjustmentEntryControllerHome.class);
            
            homeIA = (InvApprovalControllerHome)com.util.EJBHomeFactory.
                lookUpHome("ejb/InvApprovalControllerEJB", InvApprovalControllerHome.class);
                
             homeII = (InvItemEntryControllerHome)com.util.EJBHomeFactory.
                    lookUpHome("ejb/InvItemEntryControllerEJB", InvItemEntryControllerHome.class);

         } catch(NamingException e) {
            if(log.isInfoEnabled()){
                log.info("NamingException caught in InvAdjustmentEntryAction.execute(): " + e.getMessage() +
               " session: " + session.getId());
            }
            return(mapping.findForward("cmnErrorPage"));
         }

         try {

            ejbADJ = homeADJ.create();
            
            ejbIA = homeIA.create();
            
            ejbII = homeII.create();


         } catch(CreateException e) {

            if(log.isInfoEnabled()) {

                log.info("CreateException caught in InvAdjustmentEntryAction.execute(): " + e.getMessage() +
                   " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

         ActionErrors errors = new ActionErrors();
         ActionMessages messages = new ActionMessages();

/*******************************************************
   Call InvAdjustmentEntryController EJB
   getGlFcPrecisionUnit
*******************************************************/
         MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");
         short precisionUnit = 0;
         short costPrecisionUnit = 0;
         short quantityPrecisionUnit = 0;
         short adjustmentLineNumber = 0;
         boolean isInitialPrinting = false;
         String attachmentPath = appProperties.getMessage("app.attachmentPath") + user.getCompany() + "/inv/Adjustment/";
         long maxAttachmentFileSize = new Long(appProperties.getMessage("app.maxAttachmentFileSize")).longValue();
		 String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
		 String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");
		 boolean useSupplierPulldown = true;
         try {

            precisionUnit = ejbADJ.getGlFcPrecisionUnit(user.getCmpCode());
        	 costPrecisionUnit = ejbADJ.getInvGpCostPrecisionUnit(user.getCmpCode());
            quantityPrecisionUnit = ejbADJ.getInvGpQuantityPrecisionUnit(user.getCmpCode());
        	 //quantityPrecisionUnit =(short)(10);
            if (isAccessedByDevice(request.getHeader("user-agent"))) adjustmentLineNumber=100;
            else adjustmentLineNumber = ejbADJ.getInvGpInventoryLineNumber(user.getCmpCode());
            useSupplierPulldown = Common.convertByteToBoolean(ejbADJ.getAdPrfApUseSupplierPulldown(user.getCmpCode()));
            actionForm.setUseSupplierPulldown(useSupplierPulldown);

         } catch(EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
                  " session: " + session.getId());
            }

            return(mapping.findForward("cmnErrorPage"));

         }

/*******************************************************
-- Inv ADJ Save and Submit Mobile Action --
*******************************************************/

         if (request.getParameter("saveSubmitButton2") != null &&
        		 actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                 InvModAdjustmentDetails details = new InvModAdjustmentDetails();

                 details.setAdjCode(actionForm.getAdjustmentCode());
                 details.setAdjDocumentNumber(actionForm.getDocumentNumber());
                 details.setAdjReferenceNumber("");
                 details.setAdjDescription("");
                 details.setAdjDate(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));
                 details.setAdjType(actionForm.getType());
                 details.setAdjCreatedBy(actionForm.getCreatedBy());
                 details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
                 details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
                 details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
                 details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
                 details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
                 details.setAdjPurchaseUnit(actionForm.getPurchaseUnit());

                 if (actionForm.getAdjustmentCode() == null) {

                     details.setAdjCreatedBy(user.getUserName());
                     details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

                 }

                 details.setAdjLastModifiedBy(user.getUserName());
                 details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

                 ArrayList alList = new ArrayList();

                 for (int i = 0; i<actionForm.getInvADJListSize(); i++) {

                 	   InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(i);

                 	  if ((invADJList.getPartNumber().equals(""))) continue;

                 	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
                 	  if(details.getAdjType().equals("ISSUANCE"))
                  	   {
                  	     if(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit)>0)
                  	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
                  	     else
                  	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
                  	   }else
                  	   { mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));}

                 	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
                 	   mdetails.setAlQcNumber(invADJList.getQcNumber());
                 	   mdetails.setAlQcExpiryDate(Common.convertStringToSQLDate(invADJList.getQcExpiryDate()));
                 	   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));
                 	   mdetails.setAlUomName(invADJList.getUnit());
                 	   mdetails.setAlLocName(invADJList.getLocation());
                 	   mdetails.setAlIiName(invADJList.getItemName());
                 	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
                 	   mdetails.setAlMisc(invADJList.getMisc());
                 	   mdetails.setAlPartNumber(invADJList.getPartNumber());

                 	   String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
                 	   String misc=invADJList.getMisc();
                 	   String[] arrayMisc = misc.split("_");
                 	   int qty= Common.convertStringToInt(arrayMisc[0]);

                 	   misc = arrayMisc[1];
                 	   arrayMisc = misc.split("@");
                 	   propertyCode = arrayMisc[0].split(",");

                 	   misc = arrayMisc[1];
                 	   arrayMisc = misc.split("<");
                 	   serialNumber = arrayMisc[0].split(",");

    	 	      	   misc = arrayMisc[1];
    	 	      	   arrayMisc = misc.split(">");
    	 	      	   specs = arrayMisc[0].split(",");

    	 	      	   misc = arrayMisc[1];
    	 	      	   arrayMisc = misc.split(";");
    	 	      	   custodian = arrayMisc[0].split(",");
    	 	      	   //expiryDate = arrayMisc[1].split(",");

    	 	      	   misc = arrayMisc[1];
    	 	      	   arrayMisc = misc.split("%");
    	 	      	   System.out.println("5 misc = "+misc);
    	 	      	   expiryDate = arrayMisc[0].split(",");
    	 	      	   tgDocumentNumber = arrayMisc[1].split(",");

                   	   ArrayList tagList = new ArrayList();
                   	   for (int y = 0; y<qty; y++) {
                   		   InvModTagListDetails tgDetails = new InvModTagListDetails();

                   		   tgDetails.setTgPropertyCode(propertyCode[y].trim());
                		   tgDetails.setTgSpecs(specs[y].trim());
                		   tgDetails.setTgSerialNumber(serialNumber[y]);
                		   tgDetails.setTgCustodian(custodian[y].trim());
                		   tgDetails.setTgExpiryDate(Common.convertStringToSQLDate(expiryDate[y].trim()));
                		   tgDetails.setTgDocumentNumber(tgDocumentNumber[y].trim());
                   		   tagList.add(tgDetails);
                   		   mdetails.setAlTagList(tagList);
                   	   }

                 	   alList.add(mdetails);

                 }

                 try {

                 	    ejbADJ.saveInvAdjEntryMobile(details, alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

                 } catch (GlobalDocumentNumberNotUniqueException ex) {

                 		errors.add(ActionMessages.GLOBAL_MESSAGE,
                 			new ActionMessage("invAdjustmentEntry.error.documentNumberNotUnique"));

                 } catch (GlobalRecordAlreadyDeletedException ex) {

                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));

                 } catch (GlobalAccountNumberInvalidException ex) {

                 	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.accountNumberInvalid"));

                 } catch (GlobalTransactionAlreadyApprovedException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.transactionAlreadyApproved"));

                 } catch (GlobalTransactionAlreadyPendingException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPending"));

                 } catch (GlobalTransactionAlreadyPostedException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPosted"));

                 } catch (GlobalNoApprovalRequesterFoundException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.noApprovalRequesterFound"));

                 } catch (GlobalNoApprovalApproverFoundException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.noApprovalApproverFound"));

                 } catch (GlobalInvItemLocationNotFoundException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.noItemLocationFound", ex.getMessage()));

                 } catch (GlJREffectiveDateNoPeriodExistException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.effectiveDateNoPeriodExist"));

                 } catch (GlJREffectiveDatePeriodClosedException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.effectiveDatePeriodClosed"));

                 } catch (GlobalJournalNotBalanceException ex) {

                 	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                          new ActionMessage("invAdjustmentEntry.error.journalNotBalance"));

                 } catch (GlobalInventoryDateException ex) {

                	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                			new ActionMessage("invAdjustmentEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

                 } catch(GlobalBranchAccountNumberInvalidException ex) {

                 		errors.add(ActionMessages.GLOBAL_MESSAGE,
                			new ActionMessage("invAdjustmentEntry.error.branchAccountNumberInvalid", ex.getMessage()));

                 } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

                    errors.add(ActionMessages.GLOBAL_MESSAGE,
                            new ActionMessage("invAdjustmentEntry.error.noNegativeInventoryCostingCOA"));

                 } catch (GlobalExpiryDateNotFoundException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));

                 }catch (GlobalMiscInfoIsRequiredException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

                 }catch (GlobalRecordInvalidException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			   new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

                 }catch (EJBException ex) {
              	   if (log.isInfoEnabled()) {

              		   log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
              				   " session: " + session.getId());
              	   }

                     return(mapping.findForward("cmnErrorPage"));
                 }

/*******************************************************
   -- Inv ADJ Save As Draft Action --
*******************************************************/

         } else if (request.getParameter("saveAsDraftButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

           InvAdjustmentDetails details = new InvAdjustmentDetails();

           details.setAdjCode(actionForm.getAdjustmentCode());
           details.setAdjDocumentNumber(actionForm.getDocumentNumber());
           details.setAdjReferenceNumber(actionForm.getReferenceNumber());
           details.setAdjDescription(actionForm.getDescription());
           details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
           details.setAdjType(actionForm.getType());
           details.setAdjCreatedBy(actionForm.getCreatedBy());
           details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
           details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
           details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
           details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
           details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
           details.setAdjNotedBy(actionForm.getNotedBy());
           System.out.println("setAdjNotedBy : " + actionForm.getNotedBy());
           if (actionForm.getAdjustmentCode() == null) {

               details.setAdjCreatedBy(user.getUserName());
               details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

           }

           details.setAdjLastModifiedBy(user.getUserName());
           details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

           ArrayList alList = new ArrayList();

           for (int i = 0; i<actionForm.getInvADJListSize(); i++) {

           	   InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(i);

	           if (Common.validateRequired(invADJList.getLocation()) &&
	         	 	     Common.validateRequired(invADJList.getItemName()) &&
	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
	         	 	     Common.validateRequired(invADJList.getUnit()) &&
	         	 	     Common.validateRequired(invADJList.getUnitCost()) &&
	         	 	   Common.validateRequired(invADJList.getQcNumber())
	        		   ) continue;

           	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();

           	   if(details.getAdjType().equals("ISSUANCE"))
           	   {
           	     if(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit)>0)
           	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
           	     else
           	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
           	   }else
           	   { mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));}

           	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
           	   mdetails.setAlQcNumber(invADJList.getQcNumber());
           	   mdetails.setAlQcExpiryDate(Common.convertStringToSQLDate(invADJList.getQcExpiryDate()));
           	   System.out.println("unit cost : " + mdetails.getAlUnitCost());
           	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
           	   mdetails.setAlUomName(invADJList.getUnit());
           	   mdetails.setAlLocName(invADJList.getLocation());
           	   mdetails.setAlIiName(invADJList.getItemName());
           	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
           	   mdetails.setAlMisc(invADJList.getMisc());
           	   System.out.println("invADJList.getMisc() : " + invADJList.getMisc());






           	   ArrayList tagList = new ArrayList();
         	   //TODO:save and submit taglist
         	   boolean isTraceMisc = ejbADJ.getInvTraceMisc(invADJList.getItemName(), user.getCmpCode());

				String misc= invADJList.getMisc();

      	   if (isTraceMisc){

      		   String tagType = "";
      		 if(actionForm.getType().equals("ISSUANCE") || actionForm.getType().equals("WASTAGE")
  				   ||Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit) < 0){
      			tagType = "OUT";
	  		   }else{
	  			 tagType = "IN";
	  		   }


      		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), tagType);
      		   mdetails.setAlTagList(tagList);

      	   }











           	   alList.add(mdetails);

           }
           String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
           String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
           String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
           String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

           if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		

	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		

	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		
	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename4NotFound"));

	   	    	} else {

	   	    	

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   	}
	   	   	
	   	   int adjCode = 0;
	   	   	
           try {

           	   adjCode =  ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), actionForm.getSupplier(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

           } catch (GlobalDocumentNumberNotUniqueException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invAdjustmentEntry.error.documentNumberNotUnique"));

           } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));

           } catch (GlobalAccountNumberInvalidException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.accountNumberInvalid"));

           } catch (GlobalTransactionAlreadyApprovedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.transactionAlreadyApproved"));

           } catch (GlobalTransactionAlreadyPendingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPending"));

           } catch (GlobalTransactionAlreadyPostedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPosted"));

           } catch (GlobalNoApprovalRequesterFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.noApprovalRequesterFound"));

           } catch (GlobalNoApprovalApproverFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.noApprovalApproverFound"));

           } catch (GlobalInvItemLocationNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.noItemLocationFound", ex.getMessage()));

           } catch (GlobalInvTagMissingException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.invTagInventoriableError", ex.getMessage()));

           } catch (InvTagSerialNumberNotFoundException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.invTagSerialNumberDoesNotExistsError", ex.getMessage()));

           }catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.journalNotBalance"));

           } catch (GlobalInventoryDateException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {

           		errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentEntry.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

              errors.add(ActionMessages.GLOBAL_MESSAGE,
                      new ActionMessage("invAdjustmentEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalExpiryDateNotFoundException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));

           }catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

           }catch (GlobalRecordInvalidException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

           }catch (EJBException ex) {
        	   if (log.isInfoEnabled()) {

        		   log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
        				   " session: " + session.getId());
        	   }

               return(mapping.findForward("cmnErrorPage"));
           }

// save attachment

         
          if (!Common.validateRequired(filename1)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename1().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();


    	    		File fileTest = new File(attachmentPath + adjCode + "-1.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjCode + "-1-" + filename1);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjCode + "-1-" + filename1);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename2().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();

    	    	

    	    		File fileTest = new File(attachmentPath + adjCode + "-2.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjCode + "-2-" + filename2);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjCode+ "-2-" + filename2);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename3().getInputStream();

    	        

    	        	new File(attachmentPath).mkdirs();

    	    		

    	    		File fileTest = new File(attachmentPath + adjCode + "-3.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjCode + "-3-" + filename3);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjCode + "-3-" + filename3);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename4().getInputStream();

    	        	
    	        	new File(attachmentPath).mkdirs();


    	    		File fileTest = new File(attachmentPath + adjCode + "-4.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjCode+ "-4-" + filename4);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjCode + "-4-" + filename4);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }


/*******************************************************
   -- Inv ADJ Save & Submit Action --
*******************************************************/

         } else if (request.getParameter("saveSubmitButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	InvAdjustmentDetails details = new InvAdjustmentDetails();

            details.setAdjCode(actionForm.getAdjustmentCode());
            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
            details.setAdjDescription(actionForm.getDescription());
            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
            details.setAdjType(actionForm.getType());
            details.setAdjCreatedBy(actionForm.getCreatedBy());
            details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
            details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
            details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
            details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
            details.setAdjNotedBy(actionForm.getNotedBy());
            if (actionForm.getAdjustmentCode() == null) {

            		details.setAdjCreatedBy(user.getUserName());
            		details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

            }

            details.setAdjLastModifiedBy(user.getUserName());
            details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

            ArrayList alList = new ArrayList();


            for (int i = 0; i<actionForm.getInvADJListSize(); i++) {

            	InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(i);

 	           	if (Common.validateRequired(invADJList.getLocation()) &&

 	           			Common.validateRequired(invADJList.getItemName()) &&
 	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
 	         	 	     Common.validateRequired(invADJList.getUnit()) &&
 	         	 	     Common.validateRequired(invADJList.getUnitCost()) &&
 	         	 	     Common.validateRequired(invADJList.getQcExpiryDate()) &&
 	         	 	     Common.validateRequired(invADJList.getQcNumber())) continue;

            	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();
               	   if(details.getAdjType().equals("ISSUANCE"))
               	   {
               	     if(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit)>0)
               	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
               	     else
               	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
               	   }else
               	   {

               		   if (details.getAdjType().equals("WASTAGE") && Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit)>0) {
               			   mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
               		   }
               		   else mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));}


            	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
            	   mdetails.setAlQcNumber(invADJList.getQcNumber());
            	   mdetails.setAlQcExpiryDate(Common.convertStringToSQLDate(invADJList.getQcExpiryDate()));
            	   //mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble((invADJList.getAdjustBy()), quantityPrecisionUnit));
            	   double x =(Common.convertStringMoneyToDouble((invADJList.getAdjustBy()), quantityPrecisionUnit));
            	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
            	   mdetails.setAlUomName(invADJList.getUnit());
            	   mdetails.setAlLocName(invADJList.getLocation());
            	   mdetails.setAlIiName(invADJList.getItemName());
            	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));

            	   mdetails.setAlMisc(invADJList.getMisc());
            	   System.out.print(invADJList.getMisc()+"123"+x);

               	   ArrayList tagList = new ArrayList();
             	   //TODO:save and submit taglist
             	   boolean isTraceMisc = ejbADJ.getInvTraceMisc(invADJList.getItemName(), user.getCmpCode());
    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
    				String misc= invADJList.getMisc();

          	   if (isTraceMisc){

          		   String tagType = "";
          		 if(actionForm.getType().equals("ISSUANCE") || actionForm.getType().equals("WASTAGE")
      				   ||Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit) < 0){
          			tagType = "OUT";
    	  		   }else{
    	  			 tagType = "IN";
    	  		   }


          		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), tagType);
          		   mdetails.setAlTagList(tagList);

          	   }
        	   alList.add(mdetails);

            }
            //validate attachment
            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

            if (!Common.validateRequired(filename1)) {

	   	    	if (actionForm.getFilename1().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename1NotFound"));

	   	    	} else {

	   	    		

	          	  
	          	    	InputStream is = actionForm.getFilename1().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename1SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename2)) {

	   	    	if (actionForm.getFilename2().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename2NotFound"));

	   	    	} else {

	   	    		

	   	    		
	          	    	InputStream is = actionForm.getFilename2().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename2SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename3)) {

	   	    	if (actionForm.getFilename3().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename3NotFound"));

	   	    	} else {

	   	    		
	          	    	InputStream is = actionForm.getFilename3().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename3SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   }

	   	   if (!Common.validateRequired(filename4)) {

	   	    	if (actionForm.getFilename4().getFileSize() == 0) {

	   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	           			new ActionMessage("invAdjustmentEntry.error.filename4NotFound"));

	   	    	} else {

	   	    	

	          	    	InputStream is = actionForm.getFilename4().getInputStream();

	          	    	if (is.available() > maxAttachmentFileSize) {

	          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
	                   		new ActionMessage("invAdjustmentEntry.error.filename4SizeInvalid"));

	          	    	}

	          	    	is.close();

	   	    	}

	   	    	if (!errors.isEmpty()) {

	   	    		saveErrors(request, new ActionMessages(errors));
	          			return (mapping.findForward("invAdjustmentEntry"));

	   	    	}

	   	   	}

			Integer adjustmentCode = 0;
            try {

            	    adjustmentCode = ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), actionForm.getSupplier(), alList, false, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
            	    actionForm.setAdjustmentCode(adjustmentCode);

            } catch (GlobalDocumentNumberNotUniqueException ex) {

               		errors.add(ActionMessages.GLOBAL_MESSAGE,
               			new ActionMessage("invAdjustmentEntry.error.documentNumberNotUnique"));

            } catch (GlobalRecordAlreadyDeletedException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
            			new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));

            } catch (GlobalAccountNumberInvalidException ex) {

            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentEntry.error.accountNumberInvalid"));

            } catch (GlobalTransactionAlreadyApprovedException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyApproved"));

            } catch (GlobalTransactionAlreadyPendingException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPending"));

            } catch (GlobalTransactionAlreadyPostedException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPosted"));

            } catch (GlobalNoApprovalRequesterFoundException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentEntry.error.noApprovalRequesterFound"));

            } catch (GlobalNoApprovalApproverFoundException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentEntry.error.noApprovalApproverFound"));

            } catch (GlobalInvItemLocationNotFoundException ex) {

               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                        new ActionMessage("invAdjustmentEntry.error.noItemLocationFound", ex.getMessage()));

            } catch (GlobalInvTagMissingException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                     new ActionMessage("invAdjustmentEntry.error.invTagInventoriableError", ex.getMessage()));

            } catch (InvTagSerialNumberNotFoundException ex) {

            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
            			   new ActionMessage("invAdjustmentEntry.error.invTagSerialNumberDoesNotExistsError", ex.getMessage()));

            } catch (GlJREffectiveDateNoPeriodExistException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.effectiveDateNoPeriodExist"));

           } catch (GlJREffectiveDatePeriodClosedException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.effectiveDatePeriodClosed"));

           } catch (GlobalJournalNotBalanceException ex) {

           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.journalNotBalance"));

           } catch (GlobalInventoryDateException ex) {

          	   errors.add(ActionMessages.GLOBAL_MESSAGE,
          			new ActionMessage("invAdjustmentEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

           } catch(GlobalBranchAccountNumberInvalidException ex) {

           	errors.add(ActionMessages.GLOBAL_MESSAGE,
           			new ActionMessage("invAdjustmentEntry.error.branchAccountNumberInvalid", ex.getMessage()));

           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("invAdjustmentEntry.error.noNegativeInventoryCostingCOA"));

           } catch (GlobalExpiryDateNotFoundException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));

           }catch (GlobalMiscInfoIsRequiredException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

           }catch (GlobalRecordInvalidException ex) {

        	   errors.add(ActionMessages.GLOBAL_MESSAGE,
        			   new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

           }catch (EJBException ex) {
        	   if (log.isInfoEnabled()) {

        		   log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
        				   " session: " + session.getId());
        	   }

           	return(mapping.findForward("cmnErrorPage"));
           }
         // save attachment

          
         
          if (!Common.validateRequired(filename1)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename1().getInputStream();

    	        

    	        	new File(attachmentPath).mkdirs();


    	        	

    	    		File fileTest = new File(attachmentPath + adjustmentCode+ "-1.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-1-" + filename1);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode + "-1-" + filename1);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename2().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();

    	    		

    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-2.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode+ "-2-" + filename2);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode + "-2-" + filename2);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename3().getInputStream();

    	        

    	        	new File(attachmentPath).mkdirs();

    	    	


    	    		File fileTest = new File(attachmentPath + adjustmentCode+ "-3.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode+ "-3-" + filename3);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode + "-3-" + filename3);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename4().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();

    	    	


    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-4.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-4-" + filename4);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode+ "-4-" + filename4);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }


/*******************************************************
   -- Inv ADJ Journal Action --
*******************************************************/

         } else if (request.getParameter("journalButton") != null &&
           actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

         	if(Common.validateRequired(actionForm.getApprovalStatus())) {

	         	InvAdjustmentDetails details = new InvAdjustmentDetails();

	            details.setAdjCode(actionForm.getAdjustmentCode());
	            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
	            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
	            details.setAdjDescription(actionForm.getDescription());
	            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
	            details.setAdjType(actionForm.getType());
	            details.setAdjCreatedBy(actionForm.getCreatedBy());
	            details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
	            details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
	            details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
	            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
	            details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));

	            if (actionForm.getAdjustmentCode() == null) {

	            		details.setAdjCreatedBy(user.getUserName());
	            		details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

	            }

	            details.setAdjLastModifiedBy(user.getUserName());
	            details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

	            ArrayList alList = new ArrayList();

	            for (int i = 0; i<actionForm.getInvADJListSize(); i++) {

	            	InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(i);

	 	           	if (Common.validateRequired(invADJList.getLocation()) &&
	 	         	 	     Common.validateRequired(invADJList.getItemName()) &&
	 	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnit()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnitCost()) &&
	 	         	 	     Common.validateRequired(invADJList.getQcExpiryDate()) &&
	 	         	 	     Common.validateRequired(invADJList.getQcNumber())
	 	           			) continue;

	            	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();

	            	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
	            	   mdetails.setAlQcNumber(invADJList.getQcNumber());
	            	   mdetails.setAlQcExpiryDate(Common.convertStringToSQLDate(invADJList.getQcExpiryDate()));
	            	   if(details.getAdjType().equals("ISSUANCE"))
	               	   {
	               	     if(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit)>0)
	               	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
	               	     else
	               	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
	               	   }else
	               	   { mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));}
	            	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
	            	   mdetails.setAlUomName(invADJList.getUnit());
	            	   mdetails.setAlLocName(invADJList.getLocation());
	            	   mdetails.setAlIiName(invADJList.getItemName());
	            	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
	            	   mdetails.setAlMisc(invADJList.getMisc());


	               	   ArrayList tagList = new ArrayList();
	             	   //TODO:save and submit taglist
	             	   boolean isTraceMisc = ejbADJ.getInvTraceMisc(invADJList.getItemName(), user.getCmpCode());
	    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
	    				String misc= invADJList.getMisc();

	          	   if (isTraceMisc){

	          		   String tagType = "";
	          		 if(actionForm.getType().equals("ISSUANCE") || actionForm.getType().equals("WASTAGE")
	      				   ||Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit) < 0){
	          			tagType = "OUT";
	    	  		   }else{
	    	  			 tagType = "IN";
	    	  		   }


	          		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), tagType);
	          		   mdetails.setAlTagList(tagList);

	          	   }
	            	   alList.add(mdetails);

	            }
	            //validate attachment
	            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	            if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename1NotFound"));

		   	    	} else {


		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename2NotFound"));

		   	    	} else {

		   	    		

		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename4NotFound"));

		   	    	} else {

		   	    		

		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   	}
		   	   	
		   	   	Integer adjustmentCode = 0;
	            try {

	            	    adjustmentCode = ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), actionForm.getSupplier(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	    actionForm.setAdjustmentCode(adjustmentCode);

	            } catch (GlobalDocumentNumberNotUniqueException ex) {

	               	  errors.add(ActionMessages.GLOBAL_MESSAGE,
	               			new ActionMessage("invAdjustmentEntry.error.documentNumberNotUnique"));

	            }  catch (GlobalRecordAlreadyDeletedException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));

	            } catch (GlobalAccountNumberInvalidException ex) {

	            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.accountNumberInvalid"));

	            } catch (GlobalTransactionAlreadyApprovedException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyApproved"));

	            } catch (GlobalTransactionAlreadyPendingException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPending"));

	            } catch (GlobalTransactionAlreadyPostedException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPosted"));

	            } catch (GlobalNoApprovalRequesterFoundException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.noApprovalRequesterFound"));

	            } catch (GlobalNoApprovalApproverFoundException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.noApprovalApproverFound"));

	            } catch (GlobalInvItemLocationNotFoundException ex) {

	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invAdjustmentEntry.error.noItemLocationFound", ex.getMessage()));

	            } catch (GlobalInvTagMissingException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.invTagInventoriableError", ex.getMessage()));

	            }/*catch (GlobalInvTagExistingException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.invTagNonInventoriableError", ex.getMessage()));

	            }*/catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.journalNotBalance"));

	           } catch (GlobalInventoryDateException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("invAdjustmentEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

	           } catch(GlobalBranchAccountNumberInvalidException ex) {

	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invAdjustmentEntry.error.branchAccountNumberInvalid", ex.getMessage()));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("invAdjustmentEntry.error.noNegativeInventoryCostingCOA"));

	            } catch (GlobalExpiryDateNotFoundException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));

	            }catch (GlobalMiscInfoIsRequiredException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	            }catch (GlobalRecordInvalidException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

	            }catch (EJBException ex) {
	            	    if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
	                      " session: " + session.getId());
	                 }

	                return(mapping.findForward("cmnErrorPage"));
	            }

	            if (!errors.isEmpty()) {

		               saveErrors(request, new ActionMessages(errors));
		               return(mapping.findForward("invAdjustmentEntry"));

		        }
	         // save attachment

	           
          if (!Common.validateRequired(filename1)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename1().getInputStream();

    	        

    	        	new File(attachmentPath).mkdirs();


    	        	


    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-1.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-1-" + filename1);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode+ "-1-" + filename1);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename2().getInputStream();

    	        

    	        	new File(attachmentPath).mkdirs();

    	    		
    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-2.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-2-" + filename2);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath +adjustmentCode + "-2-" + filename2);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename3().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();

    	    		


    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-3.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode+ "-3-" + filename3);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode+ "-3-" + filename3);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename4().getInputStream();

    	        

    	        	new File(attachmentPath).mkdirs();



    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-4.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-4-" + filename4);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath +adjustmentCode + "-4-" + filename4);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	  	 }



         	}


            String path = "/invJournal.do?forward=1" +
		     "&transactionCode=" + actionForm.getAdjustmentCode() +
		     "&transaction=ADJUSTMENT" +
		     "&transactionNumber=" + actionForm.getReferenceNumber() +
		     "&transactionDate=" + actionForm.getDate() +
		     "&transactionEnableFields=" + actionForm.getEnableFields();

            return(new ActionForward(path));

/*******************************************************
   -- Inv ADJ Print Action --
*******************************************************/

         } else if (request.getParameter("printButton") != null) {

             if(Common.validateRequired(actionForm.getApprovalStatus())) {

                 InvAdjustmentDetails details = new InvAdjustmentDetails();

                details.setAdjCode(actionForm.getAdjustmentCode());
 	            details.setAdjDocumentNumber(actionForm.getDocumentNumber());
 	            details.setAdjReferenceNumber(actionForm.getReferenceNumber());
 	            details.setAdjDescription(actionForm.getDescription());
 	            details.setAdjDate(Common.convertStringToSQLDate(actionForm.getDate()));
 	            details.setAdjType(actionForm.getType());
 	            details.setAdjCreatedBy(actionForm.getCreatedBy());
 	            details.setAdjDateCreated(Common.convertStringToSQLDate(actionForm.getDateCreated()));
 	            details.setAdjLastModifiedBy(actionForm.getLastModifiedBy());
 	            details.setAdjDateLastModified(Common.convertStringToSQLDate(actionForm.getDateLastModified()));
 	            details.setAdjVoid(Common.convertBooleanToByte(actionForm.getAdjustmentVoid()));
 	            details.setAdjIsCostVariance(Common.convertBooleanToByte(actionForm.getCostVariance()));
 	            details.setAdjNotedBy(actionForm.getNotedBy());
 	            System.out.println("NOTED BY ME:00 "+actionForm.getNotedBy());

 	            if (actionForm.getAdjustmentCode() == null) {

 	            		details.setAdjCreatedBy(user.getUserName());
 	            		details.setAdjDateCreated(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

 	            }

 	           details.setAdjLastModifiedBy(user.getUserName());
	           details.setAdjDateLastModified(Common.convertStringToSQLDate(Common.convertSQLDateToString(new java.util.Date())));

	            ArrayList alList = new ArrayList();


	            for (int i = 0; i<actionForm.getInvADJListSize(); i++) {

	            	InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(i);

	 	           	if (Common.validateRequired(invADJList.getLocation()) &&
	 	         	 	     Common.validateRequired(invADJList.getItemName()) &&
	 	         	 	     Common.validateRequired(invADJList.getAdjustBy()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnit()) &&
	 	         	 	     Common.validateRequired(invADJList.getUnitCost()) &&
	 	         	 	     Common.validateRequired(invADJList.getQcExpiryDate()) &&
	 	         	 	     Common.validateRequired(invADJList.getQcNumber())
	 	           			) continue;

	            	   InvModAdjustmentLineDetails mdetails = new InvModAdjustmentLineDetails();

	            	   mdetails.setAlUnitCost(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit));
	            	   mdetails.setAlQcNumber(invADJList.getQcNumber());
	            	   mdetails.setAlQcExpiryDate(Common.convertStringToSQLDate(invADJList.getQcExpiryDate()));
	            	   if(details.getAdjType().equals("ISSUANCE"))
	               	   {
	               	     if(Common.convertStringMoneyToDouble(invADJList.getUnitCost(), costPrecisionUnit)>0)
	               	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
	               	     else
	               	    	mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit)*-1);
	               	   }else
	               	   { mdetails.setAlAdjustQuantity(Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit));}
	            	   mdetails.setAlCode(invADJList.getAdjustmentLineCode());
	            	   mdetails.setAlUomName(invADJList.getUnit());
	            	   mdetails.setAlLocName(invADJList.getLocation());
	            	   mdetails.setAlIiName(invADJList.getItemName());
	            	   mdetails.setAlLineNumber(Common.convertStringToShort(invADJList.getLineNumber()));
	            	   mdetails.setAlMisc(invADJList.getMisc());

	               	   ArrayList tagList = new ArrayList();
	             	   //TODO:save and submit taglist
	             	   boolean isTraceMisc = ejbADJ.getInvTraceMisc(invADJList.getItemName(), user.getCmpCode());
	    	           	String[] propertyCode, serialNumber, specs, custodian, expiryDate, tgDocumentNumber;
	    				String misc= invADJList.getMisc();

	          	   if (isTraceMisc){

	          		   String tagType = "";
	          		 if(actionForm.getType().equals("ISSUANCE") || actionForm.getType().equals("WASTAGE")
	      				   ||Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit) < 0){
	          			tagType = "OUT";
	    	  		   }else{
	    	  			 tagType = "IN";
	    	  		   }


	          		   tagList = Common.convertMiscToInvModTagListDetailsList(misc, actionForm.getDate(), tagType);
	          		   mdetails.setAlTagList(tagList);

	          	   }
	            	   alList.add(mdetails);

	            }
	            //validate attachment
	            String filename1 =  actionForm.getFilename1() != null ? actionForm.getFilename1().getFileName() : null;
	            String filename2 =  actionForm.getFilename2() != null ? actionForm.getFilename2().getFileName() : null;
	            String filename3 =  actionForm.getFilename3() != null ? actionForm.getFilename3().getFileName() : null;
	            String filename4 =  actionForm.getFilename4() != null ? actionForm.getFilename4().getFileName() : null;

	            if (!Common.validateRequired(filename1)) {

		   	    	if (actionForm.getFilename1().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename1NotFound"));

		   	    	} else {

		   	    	

		          	    	InputStream is = actionForm.getFilename1().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename1SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename2)) {

		   	    	if (actionForm.getFilename2().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename2NotFound"));

		   	    	} else {

		   	    
		          	    	InputStream is = actionForm.getFilename2().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename2SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename3)) {

		   	    	if (actionForm.getFilename3().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename3NotFound"));

		   	    	} else {

		   	    		

		          	    	InputStream is = actionForm.getFilename3().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename3SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   }

		   	   if (!Common.validateRequired(filename4)) {

		   	    	if (actionForm.getFilename4().getFileSize() == 0) {

		   	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		           			new ActionMessage("invAdjustmentEntry.error.filename4NotFound"));

		   	    	} else {

		   	    	
		          	    	InputStream is = actionForm.getFilename4().getInputStream();

		          	    	if (is.available() > maxAttachmentFileSize) {

		          	    		errors.add(ActionMessages.GLOBAL_MESSAGE,
		                   		new ActionMessage("invAdjustmentEntry.error.filename4SizeInvalid"));

		          	    	}

		          	    	is.close();

		   	    	}

		   	    	if (!errors.isEmpty()) {

		   	    		saveErrors(request, new ActionMessages(errors));
		          			return (mapping.findForward("invAdjustmentEntry"));

		   	    	}

		   	   	}

			  Integer adjustmentCode = 0;
	            try {

	            	    adjustmentCode = ejbADJ.saveInvAdjEntry(details, actionForm.getAdjustmentAccount(), actionForm.getSupplier(), alList, true, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	            	    actionForm.setAdjustmentCode(adjustmentCode);

	            } catch (GlobalDocumentNumberNotUniqueException ex) {

	               	  errors.add(ActionMessages.GLOBAL_MESSAGE,
	               			new ActionMessage("invAdjustmentEntry.error.documentNumberNotUnique"));

	            }  catch (GlobalRecordAlreadyDeletedException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));

	            } catch (GlobalAccountNumberInvalidException ex) {

	            	    errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.accountNumberInvalid"));

	            } catch (GlobalTransactionAlreadyApprovedException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyApproved"));

	            } catch (GlobalTransactionAlreadyPendingException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPending"));

	            } catch (GlobalTransactionAlreadyPostedException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.transactionAlreadyPosted"));

	            } catch (GlobalNoApprovalRequesterFoundException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.noApprovalRequesterFound"));

	            } catch (GlobalNoApprovalApproverFoundException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.noApprovalApproverFound"));

	            } catch (GlobalInvItemLocationNotFoundException ex) {

	               	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                        new ActionMessage("invAdjustmentEntry.error.noItemLocationFound", ex.getMessage()));

	            } catch (GlobalInvTagMissingException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.invTagInventoriableError", ex.getMessage()));

	            }/*catch (GlobalInvTagExistingException ex) {

	            	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                     new ActionMessage("invAdjustmentEntry.error.invTagNonInventoriableError", ex.getMessage()));

	            }*/catch (GlJREffectiveDateNoPeriodExistException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.effectiveDateNoPeriodExist"));

	           } catch (GlJREffectiveDatePeriodClosedException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.effectiveDatePeriodClosed"));

	           } catch (GlobalJournalNotBalanceException ex) {

	           	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	                    new ActionMessage("invAdjustmentEntry.error.journalNotBalance"));

	           } catch (GlobalInventoryDateException ex) {

              	   errors.add(ActionMessages.GLOBAL_MESSAGE,
              			new ActionMessage("invAdjustmentEntry.error.dateMustNotBeGreaterThanCurrentDateOrLessThanLatestDate", ex.getMessage()));

	           } catch(GlobalBranchAccountNumberInvalidException ex) {

	           		errors.add(ActionMessages.GLOBAL_MESSAGE,
	          			new ActionMessage("invAdjustmentEntry.error.branchAccountNumberInvalid", ex.getMessage()));

	           } catch (AdPRFCoaGlVarianceAccountNotFoundException ex) {

	              errors.add(ActionMessages.GLOBAL_MESSAGE,
	                      new ActionMessage("invAdjustmentEntry.error.noNegativeInventoryCostingCOA"));

	            } catch (GlobalExpiryDateNotFoundException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInvalid", ex.getMessage()));

	            }catch (GlobalMiscInfoIsRequiredException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("errors.MiscInfoRequire", ex.getMessage()));

	            }catch (GlobalRecordInvalidException ex) {

	         	   errors.add(ActionMessages.GLOBAL_MESSAGE,
	         			   new ActionMessage("invoiceEntry.error.insufficientStocks", ex.getMessage()));

	            }catch (EJBException ex) {
	            	    if (log.isInfoEnabled()) {

	                   log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
	                      " session: " + session.getId());
	                 }

	                return(mapping.findForward("cmnErrorPage"));
	            }

	         // save attachment

	            
          if (!Common.validateRequired(filename1)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename1().getInputStream();

    	        

    	        	new File(attachmentPath).mkdirs();




    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-1.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-1-" + filename1);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath +adjustmentCode + "-1-" + filename1);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename2)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename2().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();


    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-2.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-2-" + filename2);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode + "-2-" + filename2);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename3)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename3().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();



    	    		File fileTest = new File(attachmentPath + adjustmentCode + "-3.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode + "-3-" + filename3);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath +adjustmentCode + "-3-" + filename3);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }

    	   if (!Common.validateRequired(filename4)) {

    	        if (errors.isEmpty()) {

    	        	InputStream is = actionForm.getFilename4().getInputStream();

    	        	

    	        	new File(attachmentPath).mkdirs();

    	    	


    	    		File fileTest = new File(attachmentPath + adjustmentCode+ "-4.txt");
    	        	FileWriter writer = new FileWriter(fileTest);
    	            writer.write(adjustmentCode+ "-4-" + filename4);
    	            writer.close();

    	    		FileOutputStream fos = new FileOutputStream(attachmentPath + adjustmentCode + "-4-" + filename4);

    	    		int c;

	            	while ((c = is.read()) != -1) {

	            		fos.write((byte)c);

	            	}

	            	is.close();
					fos.close();

    	        }

    	   }



	            if (!errors.isEmpty()) {

		               saveErrors(request, new ActionMessages(errors));
		               return(mapping.findForward("invAdjustmentEntry"));

		        }

                 actionForm.setReport(Constants.STATUS_SUCCESS);

                 isInitialPrinting = true;

             }  else {

                 actionForm.setReport(Constants.STATUS_SUCCESS);

                 return(mapping.findForward("invAdjustmentEntry"));

             }

 /*******************************************************
     -- Ap PO Supplier Enter Action --
  *******************************************************/

       } else if(!Common.validateRequired(request.getParameter("isSupplierEntered"))) {

       	try {

               ApModSupplierDetails mdetails = ejbADJ.getApSplBySplSupplierCode(actionForm.getSupplier(), user.getCmpCode());


               actionForm.setSupplierName(mdetails.getSplName());



           } catch (GlobalNoRecordFoundException ex) {


         	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                  new ActionMessage("purchaseOrderEntry.error.supplierNoRecordFound"));
              saveErrors(request, new ActionMessages(errors));

           } catch (EJBException ex) {

             if (log.isInfoEnabled()) {

                log.info("EJBException caught in ApPurchaseOrderEntryAction.execute(): " + ex.getMessage() +
                " session: " + session.getId());
                return mapping.findForward("cmnErrorPage");

             }

          }

           return(mapping.findForward("invAdjustmentEntry"));

/*******************************************************
   -- Inv ADJ Delete Action --
*******************************************************/

         } else if(request.getParameter("deleteButton") != null) {

            try {

           	    ejbADJ.deleteInvAdjEntry(actionForm.getAdjustmentCode(), user.getUserName(), user.getCmpCode());

            } catch (GlobalRecordAlreadyDeletedException ex) {

           	    errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));

            } catch (EJBException ex) {

           	    if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());

                }

                return(mapping.findForward("cmnErrorPage"));

            }

/*******************************************************
   -- Inv ADJ Close Action --
*******************************************************/

         } else if(request.getParameter("closeButton") != null) {

            return(mapping.findForward("cmnMain"));

/*******************************************************
   -- Inv ADJ Add Lines Action --
*******************************************************/

         } else if(request.getParameter("addLinesButton") != null) {

         	int listSize = actionForm.getInvADJListSize();
         	int lineNumber = 0;

            for (int x = listSize + 1; x <= listSize + adjustmentLineNumber; x++) {

            	ArrayList comboItem = new ArrayList();

	        	InvAdjustmentEntryList invADJList = new InvAdjustmentEntryList(actionForm,
	        	    null, new Integer(++lineNumber).toString(), null, null, null, null, null,
					null, null, null, null , null);

	        	invADJList.setLocationList(actionForm.getLocationList());

	        	invADJList.setUnitList(Constants.GLOBAL_BLANK);
	        	invADJList.setUnitList("Select Item First");

	        	actionForm.saveInvADJList(invADJList);

	        }

	        for (int i = 0; i<actionForm.getInvADJListSize(); i++ ) {

           	   InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(i);

           	   invADJList.setLineNumber(new Integer(i+1).toString());

            }

	        return(mapping.findForward("invAdjustmentEntry"));

/*******************************************************
   -- Inv ADJ Delete Lines Action --
*******************************************************/

         } else if(request.getParameter("deleteLinesButton") != null) {

         	for (int i = 0; i<actionForm.getInvADJListSize(); i++) {

           	   InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(i);

           	   if (invADJList.isDeleteCheckbox()) {

           	   		actionForm.deleteInvADJList(i);
           	   		i--;
           	   }

            }

	        return(mapping.findForward("invAdjustmentEntry"));
/*******************************************************
   -- INV ADJ View Attachment Action --
 *******************************************************/

	     } else if(request.getParameter("viewAttachmentButton1") != null) {

        	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getAdjustmentCode() + "-1.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals("jpg")) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}
	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("invAdjustmentEntry"));

	         } else {

	           return (mapping.findForward("invAdjustmentEntryChild"));

	        }



	      } else if(request.getParameter("viewAttachmentButton2") != null) {

	    	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getAdjustmentCode() + "-2.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals(attachmentFileExtension)) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}


	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("invAdjustmentEntry"));

	         } else {

	           return (mapping.findForward("invAdjustmentEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton3") != null) {

	    	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getAdjustmentCode() + "-3.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals(attachmentFileExtension)) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}

	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("invAdjustmentEntry"));

	         } else {

	           return (mapping.findForward("invAdjustmentEntryChild"));

	        }


	      } else if(request.getParameter("viewAttachmentButton4") != null) {

	    	  String filename = "";

	    	  	File fileRef = new File(attachmentPath + actionForm.getAdjustmentCode() + "-4.txt");


	    	  	if(fileRef.exists()) {
	    	  	  BufferedReader br = new BufferedReader(new FileReader(fileRef));

	    	  	  String st;
	    	  	  while ((st = br.readLine()) != null) {
	    	  		filename = st;
	    	  	  }
	    	  	}


	    	  	System.out.println("filepath:" + attachmentPath  + filename);
	    	  	File file = new File( attachmentPath  + filename );

	    	  	if(file.exists()) {

	    	  		String fileExtension = filename.substring(filename.lastIndexOf("."));

	    	  		System.out.println(fileExtension + " <== file extension?");
		    	  	FileInputStream fis = new FileInputStream(attachmentPath + filename);

		    	  	byte data[] = new byte[fis.available()];

		    	  	int ctr = 0;
		    	  	int c = 0;

		    	  	while ((c = fis.read()) != -1) {

			      		data[ctr] = (byte)c;
			      		ctr++;

		    	  	}

		    	  	if (fileExtension.equals(attachmentFileExtension)) {
				      	Image image = new Image(data, fileExtension.substring(1, fileExtension.length() - 1));
				      	System.out.println("jpg");

				      	session.setAttribute(Constants.IMAGE_KEY, image);
				      	actionForm.setAttachment(Constants.STATUS_SUCCESS);
		    	  	} else if (fileExtension.equals(attachmentFileExtensionPDF) ){
		    	  		System.out.println("pdf");

		    	  		session.setAttribute(Constants.PDF_REPORT_KEY, file);
		    	  		actionForm.setAttachmentPDF(Constants.STATUS_SUCCESS);
		    	  	} else {
		    	  		Object obj = data;

		    	  		session.setAttribute(Constants.DOWNLOAD_REPORT_KEY, obj);
		    	  		session.setAttribute("DownloadFileName",filename);
		    	  		actionForm.setAttachmentDownload(Constants.STATUS_SUCCESS);
		    	  	}





	    	  	}
	         if (request.getParameter("child") == null) {

	           return (mapping.findForward("invAdjustmentEntry"));

	         } else {

	           return (mapping.findForward("invAdjustmentEntryChild"));

	        }
/*******************************************************
    -- Inv ADJ Item Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invADJList[" +
        actionForm.getRowSelected() + "].isItemEntered"))) {
        System.out.println("isItemEntered---------------->");
      	InvAdjustmentEntryList invADJList =
      		actionForm.getInvADJByIndex(actionForm.getRowSelected());

      	try {

      		// populate unit field class

      		ArrayList uomList = new ArrayList();
      		uomList = ejbADJ.getInvUomByIiName(invADJList.getItemName(), user.getCmpCode());

      		invADJList.clearUnitList();
      		invADJList.setUnitList(Constants.GLOBAL_BLANK);

  			Iterator i = uomList.iterator();
      		while (i.hasNext()) {

      			InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)i.next();

      			invADJList.setUnitList(mUomDetails.getUomName());

      			if (mUomDetails.isDefault()) {

      				invADJList.setUnit(mUomDetails.getUomName());

      			}
      		}



      		 //TODO: populate taglist with empty values

      		invADJList.clearTagList();


     		boolean isTraceMisc =  ejbADJ.getInvTraceMisc(invADJList.getItemName(), user.getCmpCode());


     		invADJList.setIsTraceMisc(isTraceMisc);
      		System.out.println(isTraceMisc + "<== trace misc item enter action");
      		if (isTraceMisc){
      			invADJList.clearTagList();

      			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), invADJList.getAdjustBy());


      			invADJList.setMisc(misc);

      		}



      		// populate unit cost field

      		if (!Common.validateRequired(invADJList.getItemName()) && !Common.validateRequired(invADJList.getUnit())) {

	      		//double unitCost = ejbADJ.getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDateAndQty(invADJList.getItemName(), invADJList.getUnit(), invADJList.getLocation(),
	      		//		Common.convertStringToSQLDate(actionForm.getDate()), 1d, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
	      		
	      		double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invADJList.getItemName(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                
	      		
	      		invADJList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
	      		invADJList.setEnableUnitCost(true);

      		}

      		// populate quantity and default adjust by if necessary

      		if (!Common.validateRequired(invADJList.getLocation()) && !Common.validateRequired(invADJList.getItemName()) &&
      		    !Common.validateRequired(invADJList.getUnit())) {

		  		invADJList.setAdjustBy(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));

		  	}


          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("invAdjustmentEntry"));

/*******************************************************
    -- Inv ADJ Location Enter Action --
 *******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invADJList[" +
        actionForm.getRowSelected() + "].isLocationEntered"))) {

      	InvAdjustmentEntryList invADJList =
      		actionForm.getInvADJByIndex(actionForm.getRowSelected());

      	try {

      		// populate quantity and default adjust by if necessary

      		if (!Common.validateRequired(invADJList.getLocation()) && !Common.validateRequired(invADJList.getItemName()) &&
      		    !Common.validateRequired(invADJList.getUnit())) {

		  		invADJList.setAdjustBy(Common.convertDoubleToStringMoney(0d, quantityPrecisionUnit));
		  		invADJList.setEnableUnitCost(true);

		  	}


          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("invAdjustmentEntry"));

/*******************************************************
    -- Inv ADJ Adjust By Action --
*******************************************************/

      } else if(!Common.validateRequired(request.getParameter("invADJList[" +
        actionForm.getRowSelected() + "].isAdjustByEntered"))) {

      	InvAdjustmentEntryList invADJList =
      		actionForm.getInvADJByIndex(actionForm.getRowSelected());

      	try {


      		// refresh quantity and calculate quantity

      		if (!Common.validateRequired(invADJList.getLocation()) && !Common.validateRequired(invADJList.getItemName()) &&
      		    !Common.validateRequired(invADJList.getUnit())) {
      			System.out.println("invADJList.getAdjustBy()-" + invADJList.getAdjustBy());
	      		double itemAdjustBy = Common.convertStringMoneyToDouble(invADJList.getAdjustBy(), quantityPrecisionUnit);

	      		// populate unit cost field

	      		if (itemAdjustBy != 0) {

		      	//	double unitCost = ejbADJ.getInvIiUnitCostByIiNameAndUomNameAndLocNameAndDateAndQty(invADJList.getItemName(), invADJList.getUnit(), invADJList.getLocation(),
		      	//			Common.convertStringToSQLDate(actionForm.getDate()), itemAdjustBy, new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());
		      	
		      	       double unitCost = ejbII.getInvIiUnitCostByIiNameAndUomName(invADJList.getItemName(), invADJList.getUnit(),Common.convertStringToSQLDate(actionForm.getDate()), user.getCmpCode());
                
		      	
		      		invADJList.setUnitCost(Common.convertDoubleToStringMoney(unitCost, costPrecisionUnit));
		      		invADJList.setEnableUnitCost(true);

	      		} else {

	      			invADJList.setUnitCost(Common.convertDoubleToStringMoney(0d, costPrecisionUnit));
		      		invADJList.setEnableUnitCost(false);

	      		}

	        }System.out.println(invADJList.getTagListSize() + "<== invADJList.getTagListSize() before ");
      		//TODO: populate taglist with empty values
      		invADJList.clearTagList();
      		boolean isTraceMisc = ejbADJ.getInvTraceMisc(invADJList.getItemName(), user.getCmpCode());
      		System.out.println(isTraceMisc + "<== trace misc adjustBy enter action");
      		String misc = invADJList.getAdjustBy() + "_";
        	String propertyCode = "";
        	String specs = "";
        	String serialNumber = "";
        	String custodian = "";
        	String expiryDate = "";
        	String tgDocumentNumber = "";
      		if (isTraceMisc == true){
      			if (invADJList.getTagListSize() == 0
	      				|| invADJList.getTagListSize()<Common.convertStringToDouble(invADJList.getAdjustBy())) {

					/*for (int x=0; x<=Math.abs(Common.convertStringToDouble(invADJList.getAdjustBy())); x++) {

	        			InvAdjustmentEntryTagList apTagList = new InvAdjustmentEntryTagList(invADJList, "", "", "", "", "", "");
	        			invADJList.saveTagList(apTagList);
					}*/
      				for (int x=0; x<=Math.abs(Common.convertStringToDouble(invADJList.getAdjustBy())); x++) {
            			/*
    					InvModTagListDetails tgDetails = new InvModTagListDetails();

               		   	propertyCode = tgDetails.getTgPropertyCode();
               		   	specs = tgDetails.getTgSpecs();
               		   	serialNumber = tgDetails.getTgSerialNumber();
               		   	custodian = tgDetails.getTgCustodian();
               		   	expiryDate = Common.convertSQLDateToString(tgDetails.getTgExpiryDate());
               		   	tgDocumentNumber = tgDetails.getTgDocumentNumber();*/
               		   	if (expiryDate == null){
               		   		expiryDate = "";
               		   	}
               		   	propertyCode += " ,";
               		    specs += " ,";
               		    serialNumber += " ,";
               		    custodian += " ,";
               		    expiryDate += " ,";
               		    tgDocumentNumber += " ,";

               		   	//mdetails.setAlTagList(tagList);

    				}
    				misc = 	invADJList.getAdjustBy()+"_"+
    						propertyCode.substring(0,propertyCode.length()-1)+"@"+
    				   		serialNumber.substring(0,serialNumber.length()-1)+"<"+
    				   		specs.substring(0,specs.length()-1)+">"+
    				   		custodian.substring(0,custodian.length()-1)+";"+
    				   		expiryDate.substring(0,expiryDate.length()-1)+"%"+
    				   		tgDocumentNumber.substring(0,tgDocumentNumber.length()-1);
    				   		//expiryDate.substring(0,expiryDate.length()-1);
				}

	      		System.out.println(invADJList.getTagListSize() + "<== invADJList.getTagListSize() after ");
      		}
      		invADJList.setMisc(misc);

          } catch (EJBException ex) {

            if (log.isInfoEnabled()) {

               log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
               " session: " + session.getId());
               return mapping.findForward("cmnErrorPage");

            }

         }

         return(mapping.findForward("invAdjustmentEntry"));


/*******************************************************
   -- Inv ADJ Load Action --
*******************************************************/

         }

         if (frParam != null) {

         	// Errors on saving

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));
               return(mapping.findForward("invAdjustmentEntry"));

            }

            if (request.getParameter("forward") == null &&
                actionForm.getUserPermission().equals(Constants.QUERY_ONLY)) {

                errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
                saveErrors(request, new ActionMessages(errors));

                return mapping.findForward("cmnMain");

            }

            try {

            	// location combo box

            	ArrayList list = null;
            	Iterator i = null;

            	actionForm.clearLocationList();

            	list = ejbADJ.getInvLocAll(user.getCmpCode());

            	if (list == null || list.size() == 0) {

            		actionForm.setLocationList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		i = list.iterator();

            		while (i.hasNext()) {

            		    actionForm.setLocationList((String)i.next());

            		}

            	}


            	if(actionForm.getUseSupplierPulldown()) {

            		actionForm.clearSupplierList();

            		list = ejbADJ.getApSplAll(new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		if (list == null || list.size() == 0) {

            			actionForm.setSupplierList(Constants.GLOBAL_NO_RECORD_FOUND);

            		} else {

            			i = list.iterator();

            			while (i.hasNext()) {

            				actionForm.setSupplierList((String)i.next());

            			}

            		}

            	}

            	actionForm.clearUserList();

            	ArrayList userList = ejbADJ.getAdUsrAll(user.getCmpCode());

            	if (userList == null || userList.size() == 0) {

            		actionForm.setUserList(Constants.GLOBAL_NO_RECORD_FOUND);

            	} else {

            		Iterator x = userList.iterator();

            		while (x.hasNext()) {

            			actionForm.setUserList((String)x.next());

            		}

            	}

            	actionForm.clearInvADJList();
            	actionForm.clearAdjAPRList();
            	actionForm.clearApproverList();

            	if (request.getParameter("forward") != null || request.getParameter("forwardWastage") != null
            			 || request.getParameter("forwardVariance") != null || isInitialPrinting) {

            	    if (request.getParameter("forward") != null || request.getParameter("forwardWastage") != null
               			 || request.getParameter("forwardVariance") != null) {

            	        actionForm.setAdjustmentCode(new Integer(request.getParameter("adjustmentCode")));

            	    }

            		InvModAdjustmentDetails mdetails = ejbADJ.getInvAdjByAdjCode(actionForm.getAdjustmentCode(), new Integer(user.getCurrentBranch().getBrCode()), user.getCmpCode());

            		actionForm.setDocumentNumber(mdetails.getAdjDocumentNumber());
            		actionForm.setReferenceNumber(mdetails.getAdjReferenceNumber());
            		actionForm.setDescription(mdetails.getAdjDescription());
            		actionForm.setDate(Common.convertSQLDateToString(mdetails.getAdjDate()));
            		actionForm.setType(mdetails.getAdjType());
            		actionForm.setApprovalStatus(mdetails.getAdjApprovalStatus());
            		actionForm.setPosted(mdetails.getAdjPosted() == 1 ? "YES" : "NO");
            		actionForm.setCreatedBy(mdetails.getAdjCreatedBy());
            		actionForm.setDateCreated(Common.convertSQLDateToString(mdetails.getAdjDateCreated()));
            		actionForm.setLastModifiedBy(mdetails.getAdjLastModifiedBy());
            		actionForm.setDateLastModified(Common.convertSQLDateToString(mdetails.getAdjDateLastModified()));
            		actionForm.setApprovedRejectedBy(mdetails.getAdjApprovedRejectedBy());
            		actionForm.setDateApprovedRejected(Common.convertSQLDateToString(mdetails.getAdjDateApprovedRejected()));
            		actionForm.setPostedBy(mdetails.getAdjPostedBy());
            		actionForm.setDatePosted(Common.convertSQLDateToString(mdetails.getAdjDatePosted()));
            		actionForm.setAdjustmentAccount(mdetails.getAdjCoaAccountNumber());
            		actionForm.setAdjustmentAccountDescription(mdetails.getAdjCoaAccountDescription());
            		actionForm.setReasonForRejection(mdetails.getAdjReasonForRejection());
            		actionForm.setAdjustmentVoid(Common.convertByteToBoolean(mdetails.getAdjVoid()));
            		actionForm.setCostVariance(Common.convertByteToBoolean(mdetails.getAdjIsCostVariance()));



            		actionForm.setNotedBy(mdetails.getAdjNotedBy());


            		System.out.println("class--"+InvAdjustmentEntryControllerHome.class);
            		System.out.println("actionForm.getNotedBy--"+actionForm.getNotedBy());


            		if (!actionForm.getSupplierList().contains(mdetails.getAdjSplSupplierCode())) {

            			if (actionForm.getSupplierList().contains(Constants.GLOBAL_NO_RECORD_FOUND)) {

            				actionForm.clearSupplierList();

            			}
            			actionForm.setSupplierList(mdetails.getAdjSplSupplierCode());

            		}
            		actionForm.setSupplier(mdetails.getAdjSplSupplierCode());
            		actionForm.setSupplierName(mdetails.getAdjSplSupplierName());

					/*
            		list = mdetails.getAdjAPRList();

            		i = list.iterator();

            		while (i.hasNext()) {

            			AdModApprovalQueueDetails mPrAPRDetails = (AdModApprovalQueueDetails)i.next();

            			InvAdjustmentApproverList invAdjAPRList = new InvAdjustmentApproverList(actionForm,
            					Common.convertSQLDateAndTimeToString(mPrAPRDetails.getAqApprovedDate()), mPrAPRDetails.getAqApproverName(), mPrAPRDetails.getAqStatus());

            			actionForm.saveAdjAPRList(invAdjAPRList);
            		}
					*/
					
					list = ejbIA.getInvApproverList("INV ADJUSTMENT", mdetails.getAdjCode(), user.getCmpCode());

            		System.out.println("approver list size is: " + list.size());
            		i = list.iterator();

            		while (i.hasNext()) {

            			AdModApprovalQueueDetails mPrAPRDetails = (AdModApprovalQueueDetails)i.next();

						ApproverList approverList = new ApproverList(actionForm,
            					Common.convertSQLDateAndTimeToString(mPrAPRDetails.getAqApprovedDate()), mPrAPRDetails.getAqApproverName(), mPrAPRDetails.getAqStatus());
					
            			actionForm.saveApproverList(approverList);
            		}







            		list = mdetails.getAdjAlList();

		            i = list.iterator();

		            int lineNumber = 0;

		            //int negation=0;
		            while (i.hasNext()) {

		            	InvModAdjustmentLineDetails mAlDetails = (InvModAdjustmentLineDetails)i.next();
		            	double adjustBy = mAlDetails.getAlAdjustQuantity();
		            	if(mdetails.getAdjType().equals("ISSUANCE") && mAlDetails.getAlAdjustQuantity() < 0)
		            		adjustBy = mAlDetails.getAlAdjustQuantity() * -1;

		            	InvAdjustmentEntryList adjAlList = new InvAdjustmentEntryList(actionForm,
		            		mAlDetails.getAlCode(), new Integer(++lineNumber).toString(),
							mAlDetails.getAlLocName(), mAlDetails.getAlIiName(), mAlDetails.getAlIiDescription(),
							actionForm.getType().equals("ISSUANCE") ? Common.convertDoubleToStringMoney(Math.abs(mAlDetails.getAlAdjustQuantity()), quantityPrecisionUnit) :Common.convertDoubleToStringMoney((mAlDetails.getAlAdjustQuantity()), quantityPrecisionUnit),
							mAlDetails.getAlUomName(),
							Common.convertDoubleToStringMoney(mAlDetails.getAlUnitCost(), costPrecisionUnit),
							mAlDetails.getAlQcNumber(),
							Common.convertSQLDateToString(mAlDetails.getAlQcExpiryDate()),
							mAlDetails.getAlMisc(), mAlDetails.getAlPartNumber());

		            	adjAlList.setLocationList(actionForm.getLocationList());



		            	 boolean isTraceMisc = ejbADJ.getInvTraceMisc(mAlDetails.getAlIiName(), user.getCmpCode());
		            	 adjAlList.setIsTraceMisc(isTraceMisc);

		            	 adjAlList.clearTagList();

		            	 ArrayList tagList = new ArrayList();
                         if(isTraceMisc) {


                    		 tagList = mAlDetails.getAlTagList();



                        	if(tagList.size()>0) {

                        		String misc = Common.convertInvModTagListDetailsListToMisc(tagList, String.valueOf(mAlDetails.getAlAdjustQuantity()));
                        		adjAlList.setMisc(misc);

                        	}else {
                        		if(mAlDetails.getAlMisc()==null) {
                        			String misc = Common.convertInvModTagListDetailsListToMisc(new ArrayList(), String.valueOf(mAlDetails.getAlAdjustQuantity()));
                        			adjAlList.setMisc(misc);
                        		}
                        	}



                         }



		            	adjAlList.clearUnitList();
		            	ArrayList unitList = ejbADJ.getInvUomByIiName(mAlDetails.getAlIiName(), user.getCmpCode());
		            	Iterator unitListIter = unitList.iterator();
		            	while (unitListIter.hasNext()) {

		            		InvModUnitOfMeasureDetails mUomDetails = (InvModUnitOfMeasureDetails)unitListIter.next();
		            		adjAlList.setUnitList(mUomDetails.getUomName());

		            	}

		            	actionForm.saveInvADJList(adjAlList);

			        }

			        int remainingList = adjustmentLineNumber - (list.size() % adjustmentLineNumber) + list.size();

			        for (int x = list.size() + 1; x <= remainingList; x++) {

			        	ArrayList comboItem = new ArrayList();

			        	InvAdjustmentEntryList invADJList = new InvAdjustmentEntryList(actionForm,
				        	    null, new Integer(x).toString(), null, null, null, null, null,
								null, null, null, null, null);

			        	invADJList.setLocationList(actionForm.getLocationList());

			        	invADJList.setUnitList(Constants.GLOBAL_BLANK);
			        	invADJList.setUnitList("Select Item First");

			        	actionForm.saveInvADJList(invADJList);

			         }

			         this.setFormProperties(actionForm, precisionUnit, user.getCompany());

			         if (request.getParameter("child") == null) {

			         	return (mapping.findForward("invAdjustmentEntry"));

			         } else {

			         	return (mapping.findForward("invAdjustmentEntryChild"));

			         }

            	}

            	// Populate line when not forwarding

            	if (isAccessedByDevice(request.getHeader("user-agent"))) {
	            	for (int x = 1; x <= adjustmentLineNumber; x++) {

	            		InvAdjustmentEntryList invADJList = new InvAdjustmentEntryList(actionForm,
				        	    null, new Integer(x).toString(), null, null, null, null, null,
								null, null, null, null, null);

			        	invADJList.setLocationList(actionForm.getLocationList());

			        	invADJList.setUnitList(Constants.GLOBAL_BLANK);
			        	invADJList.setUnitList("Select Item First");

			        	/*for (int y=1; y<=20; y++) {

	            			InvAdjustmentEntryTagList invAdjTagList = new InvAdjustmentEntryTagList(invADJList, "", "", "", "", "", "");
	            			invADJList.saveTagList(invAdjTagList);

	            		}
			        	System.out.println(invADJList.getTagListSize() + "<== tagListSize when not forwarding");*/
			        	actionForm.saveInvADJList(invADJList);

		  	        }
            	}



            	int listSize = actionForm.getInvADJListSize();
             	int lineNumber = 0;

                for (int x = listSize + 1; x <= listSize + adjustmentLineNumber; x++) {

                	ArrayList comboItem = new ArrayList();

    	        	InvAdjustmentEntryList invADJList = new InvAdjustmentEntryList(actionForm,
    	        	    null, new Integer(++lineNumber).toString(), null, null, null, null, null,
    					null, null, null, null , null);

    	        	invADJList.setLocationList(actionForm.getLocationList());

    	        	invADJList.setUnitList(Constants.GLOBAL_BLANK);
    	        	invADJList.setUnitList("Select Item First");

    	        	actionForm.saveInvADJList(invADJList);

    	        }

    	        for (int x = 0; x < actionForm.getInvADJListSize(); x++ ) {

               	   InvAdjustmentEntryList invADJList = actionForm.getInvADJByIndex(x);

               	   invADJList.setLineNumber(new Integer(x+1).toString());

               	   InvAdjustmentApproverList invAPRList = new InvAdjustmentApproverList(actionForm, null, "APPROVER", "STATUS");
               	   actionForm.saveAdjAPRList(invAPRList);

                }


            } catch(GlobalNoRecordFoundException ex) {

            	errors.add(ActionMessages.GLOBAL_MESSAGE,
                    new ActionMessage("invAdjustmentEntry.error.recordAlreadyDeleted"));

            } catch(EJBException ex) {

               if (log.isInfoEnabled()) {

                  log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
                     " session: " + session.getId());
               }

               return(mapping.findForward("cmnErrorPage"));

           }

            // Errors on loading

            if (!errors.isEmpty()) {

               saveErrors(request, new ActionMessages(errors));

            } else {

               if (request.getParameter("saveSubmitButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                   try {

                   		ArrayList list = ejbADJ.getAdApprovalNotifiedUsersByAdjCode(actionForm.getAdjustmentCode(), user.getCmpCode());

                   	   if (list.isEmpty()) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForPosting"));

                   	   } else if (list.contains("DOCUMENT POSTED")) {

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                	   	       new ActionMessage("messages.documentPosted"));

                   	   } else {

                   	   	   Iterator i = list.iterator();

                   	   	   String APPROVAL_USERS = "";

                   	   	   while (i.hasNext()) {

                   	   	       APPROVAL_USERS = APPROVAL_USERS + (String) i.next();

                   	   	       if (i.hasNext()) {

                   	   	       	   APPROVAL_USERS = APPROVAL_USERS + ", ";

                   	   	       }


                   	   	   }

                   	   	   messages.add(ActionMessages.GLOBAL_MESSAGE,
                   	   	       new ActionMessage("messages.documentSentForApproval", APPROVAL_USERS));

                   	   }

                   	   saveMessages(request, messages);
                   	   actionForm.setTxnStatus(Constants.STATUS_SUCCESS);


                   } catch(EJBException ex) {

		               if (log.isInfoEnabled()) {

		                  log.info("EJBException caught in InvAdjustmentEntryAction.execute(): " + ex.getMessage() +
		                     " session: " + session.getId());
		               }

		               return(mapping.findForward("cmnErrorPage"));

		           }

               } else if (request.getParameter("saveAsDraftButton") != null &&
                  actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

                  actionForm.setTxnStatus(Constants.STATUS_SUCCESS);

               }


            }

            actionForm.reset(mapping, request);

            actionForm.setAdjustmentCode(null);
            actionForm.setDate(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setPosted("NO");
            actionForm.setDateCreated(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setCreatedBy(user.getUserName());
            actionForm.setLastModifiedBy(user.getUserName());
            actionForm.setDateLastModified(Common.convertSQLDateToString(new java.util.Date()));
            actionForm.setNotedBy(ejbADJ.getNotedByMe(user.getCmpCode()));
            this.setFormProperties(actionForm, precisionUnit, user.getCompany());

/*******************************************************
-- Check if the request was sent by a mobile device --
*******************************************************/

            if (isAccessedByDevice(request.getHeader("user-agent"))) {

            	System.out.println("Mobile Device Detected.");
            	return(mapping.findForward("invAdjustmentEntry2"));

            }
            else return(mapping.findForward("invAdjustmentEntry"));


         } else {

            errors.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("errors.responsibilityAccessNotAllowed"));
            saveErrors(request, new ActionMessages(errors));

            return(mapping.findForward("cmnMain"));

         }

      } catch(Exception e) {

/*******************************************************
   System Failed: Forward to error page
*******************************************************/

         if (log.isInfoEnabled()) {

            log.info("Exception caught in InvAdjustmentEntryAction.execute(): " + e.getMessage()
               + " session: " + session.getId());
         }

         e.printStackTrace();
         return(mapping.findForward("cmnErrorPage"));

      }
   }


	private void setFormProperties(InvAdjustmentEntryForm actionForm, short costPrecisionUnit, String adCompany) {

		if (actionForm.getUserPermission().equals(Constants.FULL_ACCESS)) {

			if (actionForm.getPosted().equals("NO")) {

				if (actionForm.getAdjustmentCode() == null) {

					actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveDraftButton(true);
					actionForm.setShowDeleteButton(false);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowAdjustmentVoid(false);
					for (int i=0; i<actionForm.getInvADJListSize(); i++) {

					  	  InvAdjustmentEntryList actionList = actionForm.getInvADJByIndex(i);
					  	  actionList.setUnitCost(null);
					  	  actionList.setEnableUnitCost(false);

					}
				

				} else if (actionForm.getAdjustmentCode() != null &&
				    Common.validateRequired(actionForm.getApprovalStatus())) {

				    actionForm.setEnableFields(true);
					actionForm.setShowSaveSubmitButton(true);
					actionForm.setShowSaveDraftButton(true);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(true);
					actionForm.setShowDeleteLinesButton(true);
					actionForm.setShowAdjustmentVoid(false);
					
				
					for (int i=0; i<actionForm.getInvADJListSize(); i++) {

					  	  InvAdjustmentEntryList actionList = actionForm.getInvADJByIndex(i);
					  	  if (Common.convertStringMoneyToDouble(actionList.getAdjustBy(), (short)8) < 0) {

					  	      if (Common.validateRequired(actionList.getUnitCost()))
					  	          actionList.setUnitCost(Common.convertDoubleToStringMoney(0d, costPrecisionUnit));

						  	  actionList.setEnableUnitCost(false);
					  	  } else actionList.setEnableUnitCost(true);

					}
					

				} else {

					actionForm.setEnableFields(false);
					actionForm.setShowSaveSubmitButton(false);
					actionForm.setShowSaveDraftButton(false);
					actionForm.setShowDeleteButton(true);
					actionForm.setShowAddLinesButton(false);
					actionForm.setShowDeleteLinesButton(false);
					actionForm.setShowAdjustmentVoid(false);
						
				}

			} else {

				actionForm.setEnableFields(false);
				actionForm.setShowSaveSubmitButton(true);
				actionForm.setShowSaveDraftButton(false);
				actionForm.setShowDeleteButton(false);
				actionForm.setShowAddLinesButton(false);
				actionForm.setShowDeleteLinesButton(false);
				if(actionForm.getAdjustmentVoid()){
					actionForm.setShowAdjustmentVoid(false);
					actionForm.setShowSaveSubmitButton(false);
				}else{
					actionForm.setShowAdjustmentVoid(true);
					actionForm.setShowSaveSubmitButton(true);
				}


			}



		} else {


			actionForm.setEnableFields(false);
			actionForm.setShowSaveSubmitButton(false);
			actionForm.setShowSaveDraftButton(false);
			actionForm.setShowDeleteButton(false);
			actionForm.setShowAddLinesButton(false);
			actionForm.setShowDeleteLinesButton(false);
			actionForm.setShowAdjustmentVoid(false);

		}

		// view attachment

		if (actionForm.getAdjustmentCode() != null) {

			MessageResources appProperties = MessageResources.getMessageResources("com.ApplicationResources");

            String attachmentPath = appProperties.getMessage("app.attachmentPath") + adCompany + "/inv/Adjustment/";
            String attachmentFileExtension = appProperties.getMessage("app.attachmentFileExtension");
            String attachmentFileExtensionPDF = appProperties.getMessage("app.staticReportPdfFileExtension");

            if(new File(attachmentPath + actionForm.getAdjustmentCode() + "-1.txt").exists()) {
 				actionForm.setShowViewAttachmentButton1(true);

 			}else {
 				actionForm.setShowViewAttachmentButton1(false);
 			}


 			if(new File(attachmentPath + actionForm.getAdjustmentCode() + "-2.txt").exists()) {
 				actionForm.setShowViewAttachmentButton2(true);

 			}else {
 				actionForm.setShowViewAttachmentButton2(false);
 			}

 			if(new File(attachmentPath + actionForm.getAdjustmentCode() + "-3.txt").exists()) {
 				actionForm.setShowViewAttachmentButton3(true);

 			}else {
 				actionForm.setShowViewAttachmentButton3(false);
 			}
 			if(new File(attachmentPath + actionForm.getAdjustmentCode() + "-4.txt").exists()) {
 				actionForm.setShowViewAttachmentButton4(true);

 			}else {
 				actionForm.setShowViewAttachmentButton4(false);
 			}


		} else {

			actionForm.setShowViewAttachmentButton1(false);
			actionForm.setShowViewAttachmentButton2(false);
			actionForm.setShowViewAttachmentButton3(false);
			actionForm.setShowViewAttachmentButton4(false);

		}

	}

	private Boolean isAccessedByDevice(String user_agent) {
		if (user_agent!=null) {
			if (user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows CE)")||
				user_agent.equals("Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)")||
				user_agent.equals("Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)")
			) return true;
			return false;
		}
		else return false;
	}

}