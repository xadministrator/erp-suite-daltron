package com.struts.inv.adjustmententry;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import com.struts.ap.purchaserequisitionentry.ApPurchaseRequisitionApproverList;
import com.struts.util.Common;
import com.struts.util.Constants;
import com.util.Debug;


public class InvAdjustmentEntryForm extends ActionForm implements Serializable {

	private Integer adjustmentCode = null;
	private String supplier = null;
	private ArrayList supplierList = new ArrayList();  
	private String supplierName = null;
	private boolean useSupplierPulldown = true;  
	private String isSupplierEntered  = null;
	private String type = null;
	private ArrayList typeList = new ArrayList();
	private String date = null;
	private String documentNumber = null;
	private String referenceNumber = null;
	private String description = null;
	private String adjustmentAccount = null;
	private String adjustmentAccountDescription = null;
	private String notedBy = null;
	private String approvalStatus = null;
	private String posted = null;
	private String createdBy = null;
	private String dateCreated = null;
	private String lastModifiedBy = null;
	private String dateLastModified = null;   
	private String approvedRejectedBy = null;
	private String dateApprovedRejected = null;
	private String postedBy = null;
	private String datePosted = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String reasonForRejection = null;
	private boolean adjustmentVoid = false;
	private boolean costVariance = false;
	
	private FormFile filename1 = null;
	private FormFile filename2 = null;
	private FormFile filename3 = null;
	private FormFile filename4 = null;
   
	private ArrayList invADJList = new ArrayList();
	private ArrayList userList = new ArrayList();
	private ArrayList adjAPRList = new ArrayList();
	private int rowSelected = 0;
	private String userPermission = new String();
	private String txnStatus = new String();
	private boolean enableFields = false;
	private boolean showAddLinesButton = false;
	private boolean showDeleteLinesButton = false;
	private boolean showSaveSubmitButton = false;
	private boolean showSaveDraftButton = false;
	private boolean showDeleteButton = false;
	private boolean showAdjustmentVoid = false;
	private boolean showViewAttachmentButton1 = false;
	private boolean showViewAttachmentButton2 = false;
	private boolean showViewAttachmentButton3 = false;
	private boolean showViewAttachmentButton4 = false;
	
	private String report = null;
	private String attachment = null;
	private String attachmentPDF = null; 
	private String attachmentDownload = null;
	private boolean purchaseUnit = false;
	
	private ArrayList approverList = new ArrayList();
	
	public int getRowSelected(){
		return rowSelected;
	}
	
	public String getAttachmentPDF() {
	   	
	   	   return attachmentPDF;	   	
	}
	   
	public void setAttachmentPDF(String attachmentPDF) {
	   	
		this.attachmentPDF = attachmentPDF;		
	}
	
	public String getAttachment() {
	   	
	   	   return attachment;	   	
	}
	   
	public void setAttachment(String attachment) {
	   	
		this.attachment = attachment;		
	}
	public boolean getShowViewAttachmentButton1() {
	   	   
	   	   return showViewAttachmentButton1;
	   	
	}
	
  public String getAttachmentDownload() {

   	   return attachmentDownload;

   }

   public void setAttachmentDownload(String attachmentDownload) {

   	   this.attachmentDownload = attachmentDownload;

   }
   
	public void setShowViewAttachmentButton1(boolean showViewAttachmentButton1) {
   	
		this.showViewAttachmentButton1 = showViewAttachmentButton1;
   	
	}
   
	public boolean getShowViewAttachmentButton2() {
   	   
		return showViewAttachmentButton2;
   	   
	}
   
	public void setShowViewAttachmentButton2(boolean showViewAttachmentButton2) {
   	
		this.showViewAttachmentButton2 = showViewAttachmentButton2;
   	
	}
   
	public boolean getShowViewAttachmentButton3() {
   	   
		return showViewAttachmentButton3;
   	
	}
   
	public void setShowViewAttachmentButton3(boolean showViewAttachmentButton3) {
   	
		this.showViewAttachmentButton3 = showViewAttachmentButton3;
   	
	}
   
	public boolean getShowViewAttachmentButton4() {
   	   
		return showViewAttachmentButton4;
		
	}
   
	public void setShowViewAttachmentButton4(boolean showViewAttachmentButton4) {
   	
		this.showViewAttachmentButton4 = showViewAttachmentButton4;
   	
	}
	public FormFile getFilename1() {
	   	
	   	  return filename1;
	   	
	   }
	   
	   public void setFilename1(FormFile filename1) {
	   	  
	   	  this.filename1 = filename1;
	   	
	   }
	   
	   public FormFile getFilename2() {
	   	
	   	  return filename2;
	   	
	   }
	   
	   public void setFilename2(FormFile filename2) {
	   
	   	  this.filename2 = filename2;
	   	
	   }
	   
	   public FormFile getFilename3() {
	   	
	   	  return filename3;
	   	
	   }
	   
	   public void setFilename3(FormFile filename3) {
	   	  
	   	  this.filename3 = filename3;
	   	
	   }
	   
	   public FormFile getFilename4() {
	   	
	   	  return filename4;
	   	
	   }
	   
	   public void setFilename4(FormFile filename4) {
	   	  
	   	  this.filename4 = filename4;
	   	
	   }
	
	public InvAdjustmentEntryList getInvADJByIndex(int index){
		return((InvAdjustmentEntryList)invADJList.get(index));
	}

	public Object[] getInvADJList(){
		return(invADJList.toArray());
	}

	public int getInvADJListSize(){
		return(invADJList.size());
	}

	public void saveInvADJList(Object newInvADJList){
		invADJList.add(newInvADJList);
	}

	public void clearInvADJList(){
		invADJList.clear();
	}

	public void setRowSelected(Object selectedInvADJList, boolean isEdit){
		this.rowSelected = invADJList.indexOf(selectedInvADJList);
	}

	public void updateInvADJRow(int rowSelected, Object newInvADJList){
		invADJList.set(rowSelected, newInvADJList);
	}

	public void deleteInvADJList(int rowSelected){
		invADJList.remove(rowSelected);
	}

	public String getTxnStatus(){
		String passTxnStatus = txnStatus;
		txnStatus = Constants.GLOBAL_BLANK;
		return(passTxnStatus);
	}

	public void setTxnStatus(String txnStatus){
		this.txnStatus = txnStatus;
	}

	public String getUserPermission(){
		return(userPermission);
	}

	public void setUserPermission(String userPermission){
		this.userPermission = userPermission;
	}
     
	public String getAdjustmentAccount() {
		return adjustmentAccount;
	}
	
	public void setAdjustmentAccount(String adjustmentAccount) {
		this.adjustmentAccount = adjustmentAccount;
	}
	
	public String getAdjustmentAccountDescription() {
		return adjustmentAccountDescription;
	}
	
	public void setAdjustmentAccountDescription(String adjustmentAccountDescription) {
		this.adjustmentAccountDescription = adjustmentAccountDescription;
	}
	
	public String getNotedBy() {
		return notedBy;
	}
	
	public void setNotedBy(String notedBy) {
		this.notedBy = notedBy;
	}
	
	public Integer getAdjustmentCode() {
		return adjustmentCode;
	}
	
	public void setAdjustmentCode(Integer adjustmentCode) {
		this.adjustmentCode = adjustmentCode;
	}
	
	public String getApprovalStatus() {
		return approvalStatus;
	}
	
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}
	
	public String getApprovedRejectedBy() {
		return approvedRejectedBy;
	}
	
	public void setApprovedRejectedBy(String approvedRejectedBy) {
		this.approvedRejectedBy = approvedRejectedBy;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getSupplier() {
	   	
	   	  return supplier;
	   	
	   }
	   
	   public void setSupplier(String supplier) {
	   	
	   	  this.supplier = supplier;
	   	
	   }

	   public ArrayList getSupplierList() {
	   	
	   	  return supplierList;
	   	
	   }
	   
	   public void setSupplierList(String supplier) {
	   	
	   	  supplierList.add(supplier);
	   	
	   }
	   
	   public void clearSupplierList() {
	   	
	   	  supplierList.clear();
	   	  supplierList.add(Constants.GLOBAL_BLANK);
	   	
	   }
	   
	   
	   public String getIsSupplierEntered() {
		   	
	   	   return isSupplierEntered;
	   	
	   }
	   
	   public boolean getUseSupplierPulldown() {
	   	
	   	  return useSupplierPulldown;
	   	  
	   }
	   
	   public void setUseSupplierPulldown(boolean useSupplierPulldown) {
	   	
	   	  this. useSupplierPulldown = useSupplierPulldown;
	   	
	   }
	   
	   public String getSupplierName() {
			
			return supplierName;
			
		}
		
		public void setSupplierName(String supplierName) {
			
			this.supplierName = supplierName;
			
		}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}	
	
	public ArrayList getTypeList() {
		return typeList;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDateApprovedRejected() {
		return dateApprovedRejected;
	}
	
	public void setDateApprovedRejected(String dateApprovedRejected) {
		this.dateApprovedRejected = dateApprovedRejected;
	}
	
	public String getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(String dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public String getDateLastModified() {
		return dateLastModified;
	}
	
	public void setDateLastModified(String dateLastModified) {
		this.dateLastModified = dateLastModified;
	}
	
	public String getDatePosted() {
		return datePosted;
	}
	
	public void setDatePosted(String datePosted) {
		this.datePosted = datePosted;
	}
	
	public boolean getEnableFields() {
		return enableFields;
	}
	
	public void setEnableFields(boolean enableFields) {
		this.enableFields = enableFields;
	}
	
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}	
	
	public ArrayList getLocationList() {
		return locationList;
	}
	
	public void setLocationList(String locationList) {
		this.locationList.add(locationList);
	}
	
	public void clearLocationList() {
	   	
   	  locationList.clear();
   	  locationList.add(Constants.GLOBAL_BLANK);
	   	
	}
	
	public String getReasonForRejection() {
	   	
	   	  return reasonForRejection;
	   	  
   }
   
   public void setReasonForRejection(String reasonForRejection) {
   	
   	  this.reasonForRejection = reasonForRejection;
   	  
   }
	
   public ArrayList getUserList() {
		
		return userList;
		
  }
	
  public void setUserList(String user) {
		
	   userList.add(user);
		
  }
  
  public void clearUserList() {
	   	
	   userList.clear();
	   userList.add(Constants.GLOBAL_BLANK);
	   	
  }
	public String getPosted() {
		return posted;
	}
	
	public void setPosted(String posted) {
		this.posted = posted;
	}
	
	public String getPostedBy() {
		return postedBy;
	}
	
	public void setPostedBy(String postedBy) {
		this.postedBy = postedBy;
	}
	
	public String getDocumentNumber() {
		return documentNumber;
	}
	
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	
	public String getReferenceNumber() {
		return referenceNumber;
	}
	
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean isShowAddLinesButton() {
		return showAddLinesButton;
	}
	
	public void setShowAddLinesButton(boolean showAddLinesButton) {
		this.showAddLinesButton = showAddLinesButton;
	}
	
	public boolean isShowDeleteLinesButton() {
		return showDeleteLinesButton;
	}
	
	public void setShowDeleteLinesButton(boolean showDeleteLinesButton) {
		this.showDeleteLinesButton = showDeleteLinesButton;
	}
	
	public boolean getShowSaveSubmitButton() {
		return showSaveSubmitButton;
	}
	
	public void setShowSaveSubmitButton(boolean showSaveSubmitButton) {
		this.showSaveSubmitButton = showSaveSubmitButton;
	}
	
	public boolean getShowSaveDraftButton() {
		return showSaveDraftButton;
	}
	
	public void setShowSaveDraftButton(boolean showSaveDraftButton) {
		this.showSaveDraftButton = showSaveDraftButton;
	}
	
	
	public boolean getShowDeleteButton() {
		
		return showDeleteButton;
		
	}
	
	public void setShowDeleteButton(boolean showDeleteButton) {
		
		this.showDeleteButton = showDeleteButton;
		
	}
	
	
	public boolean getShowAdjustmentVoid() {
		
		return showAdjustmentVoid;
		
	}
	
	public void setShowAdjustmentVoid(boolean showAdjustmentVoid) {
		
		this.showAdjustmentVoid = showAdjustmentVoid;
		
	}
	
	public String getReport() {
		
		return report;
		
	}
	
	public void setReport(String report) {
		
		this.report = report;
		
	}

	public boolean getAdjustmentVoid() {
		
		return adjustmentVoid;
		
	}
	
	public void setAdjustmentVoid(boolean adjustmentVoid) {
		
		this.adjustmentVoid = adjustmentVoid;
		
	}
	
	public boolean getCostVariance() {
		
		return adjustmentVoid;
		
	}
	
	public void setCostVariance(boolean costVariance) {
		
		this.costVariance = costVariance;
		
	}
	
	public boolean getPurchaseUnit() {
		
		return purchaseUnit;
	
	}
	
	public void setPurchaseUnit(boolean purchaseUnit) {
		
		this.purchaseUnit = purchaseUnit;
		
	}
	
	
	public Object[] getAdjAPRList(){

		return(adjAPRList.toArray());

	}

	public void saveAdjAPRList(Object newAdjAPRList){

		adjAPRList.add(newAdjAPRList);

	}

	public void deleteAdjAPRList(int rowSelected){

		adjAPRList.remove(rowSelected);

	}

	public int getAdjAPRListSize(){

		return(adjAPRList.size());

	}

	public void clearAdjAPRList(){

		adjAPRList.clear();

	}

	public InvAdjustmentApproverList getAdjAPRByIndex(int index){

		return((InvAdjustmentApproverList)adjAPRList.get(index));

	}
	
		public Object[] getApproverList(){

		return(approverList.toArray());

	}

	public void saveApproverList(Object newApprover){

		approverList.add(newApprover);

	}

	public void deleteApproverList(int rowSelected){

		approverList.remove(rowSelected);

	}

	public int getApproverListSize(){

		return(approverList.size());

	}

	public void clearApproverList(){

		approverList.clear();

	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request){    
		
		supplier = Constants.GLOBAL_BLANK;
		isSupplierEntered = null;
		supplierName = Constants.GLOBAL_BLANK;
       
       location = Constants.GLOBAL_BLANK;
       typeList.clear();
       typeList.add(Constants.GLOBAL_BLANK);
       typeList.add("GENERAL");
       typeList.add("WASTAGE");
       typeList.add("VARIANCE");
       typeList.add("ISSUANCE");
       typeList.add("PRODUCTION");
     
       type = "GENERAL";
	   date = null;
	   documentNumber = null;
	   referenceNumber = null;
	   description = null;
	   adjustmentAccount = null;
	   adjustmentAccountDescription = null;
	   approvalStatus = null;
	   posted = null;
	   createdBy = null;
	   filename1 = null;
	   filename2 = null;
	   filename3 = null;
	   filename4 = null;
	   dateCreated = null;
	   lastModifiedBy = null;
	   dateLastModified = null;   
	   approvedRejectedBy = null;
	   dateApprovedRejected = null;
	   postedBy = null;
	   datePosted = null;	
	   reasonForRejection = null;
	   adjustmentVoid = false;
	   costVariance = false;
	   attachment = null;
	   attachmentPDF = null;
	   attachmentDownload = null;
	   for (int i=0; i<invADJList.size(); i++) {
	  	
	  	  InvAdjustmentEntryList actionList = (InvAdjustmentEntryList)invADJList.get(i);
	  	  actionList.setIsItemEntered(null);
	  	  actionList.setIsLocationEntered(null);
	  	  actionList.setIsAdjustByEntered(null);
	  	
	  }
	   
	   for (int i=0; i<adjAPRList.size(); i++) {

		  InvAdjustmentApproverList actionList = (InvAdjustmentApproverList)adjAPRList.get(i);
	  	  actionList.setApproverName(null);
	  	  actionList.setStatus(null);
	  	  
	  	  

	  }
	   


   }

	public ArrayList getExpiryDateStr(String misc, int ctr) throws Exception {
		   //ActionErrors errors = new ActionErrors();

		   Debug.print("ApReceivingItemControllerBean getExpiryDates " + misc);
		   String separator = "$";
		   
		   // Remove first $ character
		   misc = misc.substring(1);
		   
		   // Counter
		   int start = 0;
		   int nextIndex = misc.indexOf(separator, start);
		   int length = nextIndex - start;	
		   //y = new Integer(Integer.parseInt(misc.substring(start, start + length)));
		   System.out.println("ctr :" + ctr);
		   
		   /*if(y==0)
			   return new ArrayList();*/
		   
		   ArrayList miscList = new ArrayList();

		   for(int x=0; x<ctr; x++) {
			   try {	        	

				   // Date
				   start = nextIndex + 1;
				   nextIndex = misc.indexOf(separator, start);
				   length = nextIndex - start;
				   /*SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				   sdf.setLenient(false);*/
				   String checker = misc.substring(start, start + length);
				   if(checker!=""&& checker != " "){
					   miscList.add(checker);	 
				   }else{
					   miscList.add("null");
				   }
				   
				   //System.out.println(misc.substring(start, start + length));
			   } catch (Exception ex) {
				   
			   }

		   	   }
		   return miscList;
	}
	
   public ActionErrors validateFields(ActionMapping mapping, HttpServletRequest request){
      ActionErrors errors = new ActionErrors();
      if(request.getParameter("saveSubmitButton") != null || 
	     request.getParameter("saveAsDraftButton") != null ||
	     request.getParameter("journalButton") != null ||
		 request.getParameter("printButton") != null) { 
      	
      	 if(Common.validateRequired(type)){
            errors.add("type",
               new ActionMessage("invAdjustmentEntry.error.typeRequired"));
         }
      	 
      	 if(Common.validateRequired(description)){
             errors.add("description",
                new ActionMessage("invAdjustmentEntry.error.descriptionIsRequired"));
          }
      	
      	 if(Common.validateRequired(date)){
            errors.add("date",
               new ActionMessage("invAdjustmentEntry.error.dateRequired"));
         }
         
		 if(!Common.validateDateFormat(date)){
	        errors.add("date", 
		       new ActionMessage("invAdjustmentEntry.error.dateInvalid"));
		 }	 	         
		 
		 System.out.println(date.lastIndexOf("/"));
      	 System.out.println(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 int year = Integer.parseInt(date.substring(date.lastIndexOf("/")+1, (date.lastIndexOf("/")+5)));
      	 if(year<1900){
      		 errors.add("date", 
      				 new ActionMessage("purchaseOrderEntry.error.dateInvalid"));
      	 }
      	 
		 if(Common.validateRequired(adjustmentAccount)){
            errors.add("adjustmentAccount",
               new ActionMessage("invAdjustmentEntry.error.adjustmentAccountRequired"));
         }		 		
         
         int numberOfLines = 0;
      	 
      	 Iterator i = invADJList.iterator();      	 
      	 
      	 while (i.hasNext()) {
      	 	
      	 	 InvAdjustmentEntryList ilList = (InvAdjustmentEntryList)i.next();      	 	 
			 
      	 	 if (Common.validateRequired(ilList.getLocation()) &&
      	 	     Common.validateRequired(ilList.getItemName()) &&
      	 	     Common.validateRequired(ilList.getAdjustBy()) &&
      	 	     Common.validateRequired(ilList.getUnit()) &&
      	 	     Common.validateRequired(ilList.getUnitCost())) continue;
      	 	     
      	 	 numberOfLines++;
      	 /*
      	 	try{
      	 		String separator = "$";
      	 		String misc = "";
      		   // Remove first $ character
      		   misc = ilList.getMisc().substring(1);
      		   
      		   // Counter
      		   int start = 0;
      		   int nextIndex = misc.indexOf(separator, start);
      		   int length = nextIndex - start;	
      		   int counter;
      		   counter = Integer.parseInt(misc.substring(start, start + length));
      		   
      	 		ArrayList miscList = this.getExpiryDateStr(ilList.getMisc(), counter);
      	 		System.out.println("rilList.getMisc() : " + ilList.getMisc());
      	 		Iterator mi = miscList.iterator();
     	 		
      	 		int ctrError = 0;
      	 		int ctr = 0;
      	 		
      	 		while(mi.hasNext()){
      	 				String miscStr = (String)mi.next();
      	 			    
      	 				if(!Common.validateDateFormat(miscStr)){
      	 					errors.add("date", 
      	 							new ActionMessage("receivingItemEntry.error.expiryDateInvalid"));
      	 					ctrError++;
      	 				}
      	 				
      	 				System.out.println("miscStr: "+miscStr);
      	 				if(miscStr=="null"){
      	 					ctrError++;
      	 				}
      	 		}

      	 			if(ctrError>0 && ctrError!=miscList.size()){
      	 				errors.add("date", 
      	      			  new ActionMessage("receivingItemEntry.error.expiryDateNullInvalid"));
      	 			}
      	 			
      	 	}catch(Exception ex){
  	  	    	//ex.printStackTrace();
  	  	    }*/
      	 	
	         if(Common.validateRequired(ilList.getLocation()) || ilList.getLocation().equals(Constants.GLOBAL_BLANK)){
	            errors.add("location",
	            	new ActionMessage("invAdjustmentEntry.error.locationRequired", ilList.getLineNumber()));
	         }
	         if(Common.validateRequired(ilList.getItemName()) || ilList.getItemName().equals(Constants.GLOBAL_BLANK)){
	            errors.add("itemName",
	            	new ActionMessage("invAdjustmentEntry.error.itemNameRequired", ilList.getLineNumber()));
	         }
	         if(Common.validateRequired(ilList.getAdjustBy())) {
	         	errors.add("adjustBy",
	         		new ActionMessage("invAdjustmentEntry.error.adjustByRequired", ilList.getLineNumber()));
	         }
		 	 if(!Common.validateNumberFormat(ilList.getAdjustBy())){
	            errors.add("adjustBy",
	               new ActionMessage("invAdjustmentEntry.error.adjustByInvalid", ilList.getLineNumber()));
	         }
		 	 if(!Common.validateRequired(ilList.getAdjustBy()) && Common.convertStringMoneyToDouble(ilList.getAdjustBy(), (short)3) == 0) {
		        errors.add("adjustBy",
		           new ActionMessage("invAdjustmentEntry.error.zeroQuantityNotAllowed", ilList.getLineNumber()));
		     }
		 	 if(Common.validateRequired(ilList.getUnit()) || ilList.getUnit().equals(Constants.GLOBAL_BLANK)){
	            errors.add("unit",
	            	new ActionMessage("invAdjustmentEntry.error.unitRequired", ilList.getLineNumber()));
	         }
		 	 if(Common.validateRequired(ilList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("invAdjustmentEntry.error.unitCostRequired", ilList.getLineNumber()));
	         }
		 	 System.out.println("ilList.getUnitCost()="+ilList.getUnitCost());
		 	 if(!Common.validateMoneyFormat(ilList.getUnitCost())){
	            errors.add("unitCost",
	               new ActionMessage("invAdjustmentEntry.error.unitCostInvalid", ilList.getLineNumber()));
	         }	        
       
	    }
	    
	    if (numberOfLines == 0) {
         	
         	errors.add("adjustment",
               new ActionMessage("invAdjustmentEntry.error.adjustmentMustHaveLine"));
         	
        }	  	    
	    
	    	    	 
      } 
      
      return(errors);	
   }
}
