package com.struts.inv.adjustmententry;

import java.io.Serializable;
import java.util.ArrayList;

import com.struts.ap.receivingitementry.ApReceivingItemEntryTagList;

public class InvAdjustmentEntryList implements Serializable {

	private Integer adjustmentLineCode = null;
	private String lineNumber = null;
	private String location = null;
	private ArrayList locationList = new ArrayList();
	private String itemName = null;
	private String itemDescription = null;
	private String adjustBy = null;
	private String unit = null;
	private ArrayList unitList = new ArrayList();
	private ArrayList tagList = new ArrayList();
	private String unitCost = null;
	private String qcNumber = null;
	private String qcExpiryDate = null;
	private boolean deleteCheckbox = false;

	private String isItemEntered = null;
	private String isLocationEntered = null;
	private String isAdjustByEntered = null;
	private boolean enableUnitCost = false;
	private String misc = null;
	private String partNumber = null;
	private boolean isTraceMisc = false;

	private InvAdjustmentEntryForm parentBean;

	public InvAdjustmentEntryList(InvAdjustmentEntryForm parentBean,
		Integer adjustmentLineCode, String lineNumber, String location,
		String itemName, String itemDescription, String adjustBy, String unit,
		String unitCost, String qcNumber, String qcExpiryDate, String misc, String partNumber) {

		this.parentBean = parentBean;
		this.adjustmentLineCode = adjustmentLineCode;
		this.lineNumber = lineNumber;
		this.location = location;
		this.itemName = itemName;
		this.itemDescription = itemDescription;
		this.adjustBy = adjustBy;
		this.unit = unit;
		this.unitCost = unitCost;
		this.qcNumber = qcNumber;
		this.qcExpiryDate = qcExpiryDate;
		this.misc = misc;
		this.partNumber = partNumber;

	}

	public InvAdjustmentEntryForm getParentBean() {

		return parentBean;

	}

	public void setParentBean(InvAdjustmentEntryForm parentBean) {

		this.parentBean = parentBean;

	}

	public Integer getAdjustmentLineCode() {

		return adjustmentLineCode;

	}

	public String getAdjustBy() {

		return adjustBy;

	}

	public void setAdjustBy(String adjustBy) {

		this.adjustBy = adjustBy;

	}

	public boolean isDeleteCheckbox() {

		return deleteCheckbox;

	}

	public void setDeleteCheckbox(boolean deleteCheckbox) {

		this.deleteCheckbox = deleteCheckbox;

	}

	public String getIsItemEntered() {

		return isItemEntered;

	}

	public void setIsItemEntered(String isItemEntered) {

		if (isItemEntered != null && isItemEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isItemEntered = null;

	}

	public String getIsLocationEntered() {

		return isLocationEntered;

	}

	public void setIsLocationEntered(String isLocationEntered) {

		if (isLocationEntered != null && isLocationEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isLocationEntered = null;

	}

	public String getIsAdjustByEntered() {

		return isAdjustByEntered;

	}

	public void setIsAdjustByEntered(String isAdjustByEntered) {

		if (isAdjustByEntered != null && isAdjustByEntered.equals("true")) {

			parentBean.setRowSelected(this, false);

		}

		isAdjustByEntered = null;

	}

	public String getItemName() {

		return itemName;

	}

	public void setItemName(String itemName) {

		this.itemName = itemName;

	}

	public String getItemDescription() {

		return itemDescription;

	}

	public void setItemDescription(String itemDescription) {

		this.itemDescription = itemDescription;

	}

	public InvAdjustmentEntryTagList getTagListByIndex(int index){
	      return((InvAdjustmentEntryTagList)tagList.get(index));
	}

	public Object[] getTagList(){
		return(tagList.toArray());
	}
	public int getTagListSize(){
	      return(tagList.size());
	}

	public void saveTagList(Object newTagList){
		tagList.add(newTagList);
	}

	public void clearTagList(){
		tagList.clear();
	}

	public String getLineNumber() {

		return lineNumber;

	}

	public void setLineNumber(String lineNumber) {

		this.lineNumber = lineNumber;

	}

	public String getLocation() {

		return location;

	}

	public void setLocation(String location) {

		this.location = location;

	}

	public ArrayList getLocationList() {

		return locationList;

	}

	public void setLocationList(ArrayList locationList) {

		this.locationList = locationList;

	}

	public String getUnit() {

		return unit;

	}

	public ArrayList getUnitList() {

		return unitList;

	}

	public void setUnitList(String unit) {

		unitList.add(unit);

	}

	public void clearUnitList() {

		unitList.clear();

	}

	public void setUnit(String unit) {

		this.unit = unit;

	}

	public String getUnitCost() {

	    return unitCost;

	}

	public void setUnitCost(String unitCost) {

	    this.unitCost = unitCost;

	}

	public String getQcNumber() {

		return qcNumber;

	}

	public void setQcNumber(String qcNumber) {

		this.qcNumber = qcNumber;

	}

	public String getQcExpiryDate() {

		return qcExpiryDate;

	}

	public void setQcExpiryDate(String qcExpiryDate) {

		this.qcExpiryDate = qcExpiryDate;

	}

	public boolean getEnableUnitCost() {

		return enableUnitCost;

	}

	public void setEnableUnitCost(boolean enableUnitCost) {

		this.enableUnitCost = enableUnitCost;

	}

	public String getMisc() {

		return misc;

	}

	public void setMisc(String misc) {

		this.misc = misc;

	}

	public String getPartNumber() {

		return partNumber;

	}

	public void setPartNumber(String partNumber) {

		this.partNumber = partNumber;

	}


	public boolean getIsTraceMisc() {

		return isTraceMisc;

	}

	public void setIsTraceMisc(boolean isTraceMisc) {

		this.isTraceMisc = isTraceMisc;

	}

}
