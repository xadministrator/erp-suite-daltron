package com.struts.inv.adjustmentpost;

import java.io.Serializable;

public class InvAdjustmentPostList implements Serializable {

   private Integer adjustmentCode = null;
   private String date = null;
   private String documentNumber = null;
   private String referenceNumber = null;
   private String amount = null;
   
   private boolean post = false;
       
   private InvAdjustmentPostForm parentBean;
    
   public InvAdjustmentPostList(InvAdjustmentPostForm parentBean,
      Integer adjustmentCode,
      String date,
	  String documentNumber,
      String referenceNumber,
      String amount) {

      this.parentBean = parentBean;
      this.adjustmentCode = adjustmentCode;
      this.date = date;
      this.documentNumber = documentNumber;
      this.referenceNumber = referenceNumber;
      this.amount = amount;
      
   }

   public Integer getAdjustmentCode() {

      return adjustmentCode;

   }
   
   public String getDate() {
   
      return date;
      
   }
   
   public String getDocumentNumber() {
   	
   	  return documentNumber;
   	  
   }
   
   public String getReferenceNumber() {
   	
   	  return referenceNumber;
   	  
   }
   
   public String getAmount() {
   
      return amount;
   
   }

   public boolean getPost() {
   	
   	  return post;
   	
   }
   
   public void setPost(boolean post) {
   	
   	  this.post = post;
   	
   }
      
   
}